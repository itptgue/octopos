package com.ssti.enterprise.pos.presentation.screens.report.purchase;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.purchase.PurchaseAnalysisTool;

public class Default extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
		{
    		super.doBuildTemplate(data, context);
    		super.setParams(data);
    		
	        context.put("dStart", dStart);
	        context.put("dEnd", dEnd);
	        context.put("purchaseanalysis", PurchaseAnalysisTool.getInstance());
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
		}    
    }  
}

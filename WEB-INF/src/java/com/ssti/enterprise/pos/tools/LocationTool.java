package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.security.PermissionSet;

import com.ssti.enterprise.pos.manager.LocationManager;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationCtl;
import com.ssti.enterprise.pos.om.LocationCtlPeer;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: LocationTool.java,v 1.18 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: LocationTool.java,v $
 * 2016-06-08
 * 
 * - add method hasAccess to check whether username has access to location
 * 
 * </pre><br>
 */
public class LocationTool extends BaseTool 
{
	public static List getAllLocation()
		throws Exception
	{
		return LocationManager.getInstance().getAllLocation();
	}
	
	public static List getLocation(String _sID)
    	throws Exception
    {
		Location oLoc = getLocationByID(_sID);
		List vData = new ArrayList(1);
		if (oLoc != null) vData.add(oLoc);
        return vData;
	}
	
	public static Location getLocationByID(String _sID)
    	throws Exception
    {
		return getLocationByID (_sID, null);
	}

	public static Location getLocationByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return LocationManager.getInstance().getLocationByID(_sID, _oConn);
	}	

	public static Location getLocationByCode(String _sCode)
		throws Exception
	{
		return LocationManager.getInstance().getLocationByCode(_sCode, null);
	}	

	public static String getDescriptionByID(String _sID)
    	throws Exception
    {
		Location oLocation = getLocationByID (_sID);
		if (oLocation != null) {
			return oLocation.getDescription();
		}
		return "";
	}

	public static String getLocationNameByID(String _sID)
    	throws Exception
    {
		return getLocationNameByID (_sID, null);
	}

	public static String getLocationNameByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Location oLocation = getLocationByID (_sID, _oConn);
		if (oLocation != null) {
			return oLocation.getLocationName();
		}
		return "";
	}

	public static String getLocationCodeByID(String _sID)
		throws Exception
	{
		return getLocationCodeByID (_sID, null);
	}
	
	public static String getLocationCodeByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Location oLocation = getLocationByID (_sID, _oConn);
		if (oLocation != null) {
			return oLocation.getLocationCode();
		}
		return "";
	}
	
	public static int getInventoryType(String _sID, Connection _oConn)
		throws Exception
	{
		Location oLocation = getLocationByID (_sID, _oConn);
		if (oLocation != null) {
			return oLocation.getInventoryType();
		}
		return i_INV_NORMAL;
	}	
	
	public static String getLocationAddressByID(String _sID)
    	throws Exception
    {
		Location oLocation = getLocationByID (_sID);
		if (oLocation != null) {
			return oLocation.getLocationAddress();
		}
		return "";
	}	

	public static String getCodeByID(String _sID)
    	throws Exception
    {
		Location oLocation = getLocationByID(_sID);
		if (oLocation != null) {
			return oLocation.getLocationCode();
		}
		return "";
	}

	public static String getIDByCode(String _sCode)
    	throws Exception
    {
		Location oLocation = getLocationByCode(_sCode);
		if (oLocation != null) {
			return oLocation.getLocationId();
		}
		return "";
	}
	
	public static List getBySiteID(String _sSiteID)
		throws Exception
	{
	    return LocationManager.getInstance().getBySiteID(_sSiteID, null);
	}

	public static List getByUserName(String _sUserName, PermissionSet _vPerms)		
		throws Exception
	{
		if (_vPerms != null)
		{
			return getByUserName(_sUserName, _vPerms.containsName("Access All Location"), _vPerms.containsName("Access All Site"));
		}
		return getByUserName(_sUserName, false, false);
	}
	
	public static List getByUserName(String _sUserName, boolean _bAllLoc, boolean _bAllSite)		
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sUserName))
		{
			Employee oEmp = EmployeeTool.getEmployeeByUsername(_sUserName);
			if (oEmp != null && StringUtil.isNotEmpty(oEmp.getLocationId()))
			{
				Location oLoc = LocationTool.getLocationByID(oEmp.getLocationId());
				if (oLoc != null)
				{
					if (_bAllLoc && !_bAllSite)
					{
						System.out.println("By Site ID : " + oLoc.getSiteId());
						return getBySiteID(oLoc.getSiteId());					
					}
					else if (!_bAllLoc)
					{
						List vLoc = new ArrayList(1);
						vLoc.add(oLoc);
						return vLoc;
					}
				}
			}
		}
		return getAllLocation();
	}

	public static boolean hasAccess(String _sUserName, String _sLocationID, boolean _bAllLoc, boolean _bAllSite)		
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sUserName))
		{
			Employee oEmp = EmployeeTool.getEmployeeByUsername(_sUserName);
			if (oEmp != null && StringUtil.isNotEmpty(oEmp.getLocationId()))
			{
				Location oLoc = LocationTool.getLocationByID(oEmp.getLocationId());
				if (oLoc != null)
				{
					if (_bAllLoc && !_bAllSite)
					{
						List vLocs = getBySiteID(oLoc.getSiteId());
						for(int i = 0; i < vLocs.size(); i++)
						{
							Location d = (Location) vLocs.get(i);
							if(StringUtil.isEqual(d.getId(), _sLocationID))
							{
								return true;
							}
						}
						return false;
					}
					else if (!_bAllLoc)
					{
						if(StringUtil.isEqual(oLoc.getId(), _sLocationID))
						{
							return true;
						}
						return false;
					}
				}
			}
		}
		return true;
	}

	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _sSiteID
	 * @param _iLocType
	 * @return
	 * @throws Exception
	 */
	public static List find(int _iCond, 
							String _sKeywords, 
							String _sSiteID, 
							int _iLocType)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    
		if (StringUtil.isNotEmpty(_sSiteID)) oCrit.add(LocationPeer.SITE_ID, _sSiteID);
	    
	    if (_iLocType > 0) oCrit.add(LocationPeer.LOCATION_TYPE, _iLocType);
	    
	    if (StringUtil.isNotEmpty(_sKeywords))
	    {
	    	oCrit.setIgnoreCase(true);
		    if (_iCond == 1) 
		    {
				oCrit.add(LocationPeer.LOCATION_NAME, (Object)_sKeywords, Criteria.ILIKE);
			}
		    if (_iCond == 2) 
		    {
		    	oCrit.add(LocationPeer.LOCATION_CODE, (Object)_sKeywords, Criteria.ILIKE);
		    }
		    if (_iCond == 3) 
		    {
		    	oCrit.add(LocationPeer.DESCRIPTION, (Object)_sKeywords, Criteria.ILIKE);
		    }
		    if (_iCond == 4) 
		    {
		    	oCrit.add(LocationPeer.LOCATION_ADDRESS, (Object)_sKeywords, Criteria.ILIKE);
		    }
	    }
	    return LocationPeer.doSelect(oCrit);
	}
	
	public static List getLocationByType(int _iType)
		throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(LocationPeer.LOCATION_TYPE, _iType);
        oCrit.addAscendingOrderByColumn(LocationPeer.LOCATION_CODE);
		return LocationPeer.doSelect(oCrit);
	}
	
	public static String getTypeString(int _iType)
		throws Exception
	{
		if (_iType == i_HEAD_OFFICE) return LocaleTool.getString("head_office");
		if (_iType == i_WAREHOUSE) return LocaleTool.getString("warehouse");
		if (_iType == i_CONSIGNMENT) return LocaleTool.getString("consignment");
		if (_iType == i_STORE && PreferenceTool.medicalInstalled())
		{
			return LocaleTool.getString("installation");			
		}
		else if (_iType == i_STORE && !PreferenceTool.medicalInstalled())

		{
			return LocaleTool.getString("store");
		}		
		return "";
	}			
	
	/**
	 * 
	 * @param _iSyncType
	 * @return List of updated locations since last ho 2 store
	 * @throws Exception
	 */
	public static List getUpdatedLocations ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) 
		{
			oCrit.add(LocationPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
		}
		return LocationPeer.doSelect(oCrit);
	}	
    
    //-------------------------------------------------------------------------
    // Location Control Table
    //-------------------------------------------------------------------------
    public static LocationCtl getLocCtl(String _sID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(LocationCtlPeer.LOCATION_CTL_ID,_sID);
        List v  = LocationCtlPeer.doSelect(oCrit);
        if (v.size() > 0)
        {
            return (LocationCtl)v.get(0);
        }
        return null;        
    }

    public static List findLocCtl(String _sLocID, String _sCode, String _sName, String _sTransCode, String _sTblName)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        if (StringUtil.isNotEmpty(_sLocID)) oCrit.add(LocationCtlPeer.LOCATION_ID, _sLocID);
        if (StringUtil.isNotEmpty(_sCode))
        {
            oCrit.addJoin(LocationCtlPeer.LOCATION_ID, LocationPeer.LOCATION_ID);
            oCrit.add (LocationPeer.LOCATION_CODE, (Object)_sCode, Criteria.ILIKE);
        }
        if (StringUtil.isNotEmpty(_sName))
        {
            oCrit.addJoin(LocationCtlPeer.LOCATION_ID, LocationPeer.LOCATION_ID);
            oCrit.add (LocationPeer.LOCATION_NAME, (Object)_sName, Criteria.ILIKE);
        }
        if (StringUtil.isNotEmpty(_sTransCode)) oCrit.add (LocationCtlPeer.TRANS_CODE, (Object)_sTransCode, Criteria.ILIKE);
        if (StringUtil.isNotEmpty(_sTblName)) oCrit.add (LocationCtlPeer.CTL_TABLE, (Object)_sTblName, Criteria.ILIKE);
        return LocationCtlPeer.doSelect(oCrit);
    }
}
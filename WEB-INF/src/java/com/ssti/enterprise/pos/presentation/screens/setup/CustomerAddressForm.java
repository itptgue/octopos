package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CustomerAddress;
import com.ssti.enterprise.pos.om.CustomerAddressPeer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

public class CustomerAddressForm extends CustomerSecureScreen
{
	public static int i_SAVE_ADDRESS = 1;
	public static int i_DEL_ADDRESS  = 2;
	public static int i_VIEW_ADDRESS = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_SAVE_ADDRESS)
			{
				saveAddress(data,context);
			}
			else if (iOp == i_DEL_ADDRESS)
			{
				delAddress(data,context);
			}
			else if (iOp == i_VIEW_ADDRESS)
			{
				viewAddress(data,context);				
			}
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx);
    	}
    }

	private void saveAddress(RunData data, Context context) 
	{
		try 
		{
			String sCTID = data.getParameters().getString("CustomerAddressId");
			String sID = data.getParameters().getString("ID");
			CustomerAddress oCA = CustomerTool.getCustomerAddressByID(sCTID);
			System.out.println(oCA);
			boolean bNew = false;
			if (oCA == null)
			{
				oCA = new CustomerAddress();
				bNew = true;
			}
			System.out.println("new " + bNew);
			data.getParameters().setProperties(oCA);
			if (bNew) 
			{
				oCA.setCustomerAddressId(IDGenerator.generateSysID());
				oCA.setCustomerId(sID);
			}
			oCA.save();			
			data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_SAVE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
	private void delAddress(RunData data, Context context) 
	{
		try 
		{
			String sCTID = data.getParameters().getString("CustomerAddressId");
			if (StringUtil.isNotEmpty(sCTID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerAddressPeer.CUSTOMER_ADDRESS_ID, sCTID);
				CustomerAddressPeer.doDelete(oCrit);
				data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
			}
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
	private void viewAddress(RunData data, Context context) 
	{
		try 
		{
			String sADID = data.getParameters().getString("CustomerAddressId");
			if (StringUtil.isNotEmpty(sADID))
			{
				context.put("Address", CustomerTool.getCustomerAddressByID(sADID));
			}
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
}

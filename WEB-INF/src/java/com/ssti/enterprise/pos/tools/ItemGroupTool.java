package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemGroupPeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemGroupTool.java,v 1.10 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemGroupTool.java,v $
 * Revision 1.10  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class ItemGroupTool extends BaseTool 
{
	public static List getAllItemGroup()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		return ItemGroupPeer.doSelect(oCrit);
    }

	public static ItemGroup getItemGroupByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(ItemGroupPeer.ITEM_GROUP_ID, _sID);
		List vData = ItemGroupPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (ItemGroup)vData.get(0);
		}
		return null;
	}
	
	public static List getItemGroupByGroupID(String _sID)
		throws Exception
	{
		return getItemGroupByGroupID(_sID, null);
	}	
	
	public static List getItemGroupByGroupID(String _sID, Connection _oConn)
    	throws Exception
    {
		return ItemManager.getInstance().getItemGroupByGroupID(_sID, _oConn);
	}
	
	public static List getByGroupAndItemID(String _sGID, String _sIID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemGroupPeer.GROUP_ID, _sGID);
        oCrit.add(ItemGroupPeer.ITEM_ID, _sIID);
		return ItemGroupPeer.doSelect(oCrit, _oConn);
	}

	public static ItemGroup getItemGroup(String _sGID, String _sIID, Connection _oConn)
		throws Exception
	{
		List vData = getByGroupAndItemID (_sGID, _sIID, _oConn);
		if (vData.size() > 0)
		{
			return (ItemGroup)vData.get(0);
		}
		return null;
	}

	public static double getQty(String _sGID, String _sIID, Connection _oConn)
		throws Exception
	{
		ItemGroup oGrp = getItemGroup(_sGID, _sIID, _oConn);
		if (oGrp != null) return oGrp.getQty().doubleValue();
		return 1;
	}
	
	public static String getGroupID(String _sItemID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemGroupPeer.ITEM_ID, _sItemID);
        List vIg = ItemGroupPeer.doSelect(oCrit);
        if (vIg.size() > 0)
        {
			return ((ItemGroup)vIg.get(0)).getItemGroupId();
		}
		else 
		{
			return "";
		}
	}

	public static void delGroupItemByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemGroupPeer.ITEM_GROUP_ID, _sID);
		ItemGroupPeer.doDelete (oCrit);
	}
	
	/**
	 * get item group from new items created in store
	 * 
	 * @param _vItems List of new items
	 * @param _oConn
	 * @return List of new item_group created in store
	 * @throws Exception
	 */
	public static List getUpdatedGroups(List _vItems, Connection _oConn)
		throws Exception
	{
		if (_vItems != null && _vItems.size() > 0)
		{
			List vIDs = new ArrayList(_vItems.size());
			for (int i = 0; i < _vItems.size(); i++)
			{
				Item oItem = (Item) _vItems.get(i);
				if (oItem.getItemType() == i_GROUPING)
				{
					vIDs.add(oItem.getItemId());
				}
			}
			if (vIDs.size() > 0) 
			{
				Criteria oCrit = new Criteria();
				oCrit.addIn(ItemGroupPeer.GROUP_ID, vIDs);
				return ItemGroupPeer.doSelect(oCrit, _oConn);
			}
		}
		return new ArrayList(1);
	}			
}


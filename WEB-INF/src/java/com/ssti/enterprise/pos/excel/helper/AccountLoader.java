package com.ssti.enterprise.pos.excel.helper;

import java.io.InputStream;
import java.util.Date;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.manager.AccountManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountType;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.AccountTypeTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/AccountLoader.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: AccountLoader.java,v 1.12 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: AccountLoader.java,v $
 * 2015-10-01
 * - change update account data to update currency if account not exists in transacitons
 */
public class AccountLoader extends BaseExcelHelper
{    
	private static Log log = LogFactory.getLog ( AccountLoader.class );
	
	private StringBuilder m_oResult     = new StringBuilder();
	private StringBuilder m_oUpdated    = new StringBuilder("\nUpdated Account : \n");
	private StringBuilder m_oRejected   = new StringBuilder("\nRejected Account : \n");
	private StringBuilder m_oNewAccount = new StringBuilder("\nNew Account : \n");
	private StringBuilder m_oError   	= new StringBuilder("\nERROR Account : \n");
	
	private String m_sUserName;
	private int m_iRejected   = 0;
	private int m_iError   	  = 0;
	private int m_iUpdated    = 0;
	private int m_iNewAccount = 0;
	private int m_iTotalRows  = 0;
	private boolean m_bStart  = false;
	
	//headers used to validate excel template
	private static final String[] aHeaders = {"Account Code", "Account Name", "Description", "Account Type", 
	                                          "Parent Code", "Currency", "Opening Balance", "As Of"};
	
	public AccountLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	public String getResult()
	{
		m_oResult.append (m_oNewAccount);
		m_oResult.append ("\nTotal New Account : ");
		m_oResult.append (m_iNewAccount);
		m_oResult.append ("\n");
		m_oResult.append (m_oUpdated);
		m_oResult.append ("\nTotal Account Updated : ");		
		m_oResult.append (m_iUpdated);
		m_oResult.append ("\n");
		m_oResult.append (m_oRejected);
		m_oResult.append ("\nTotal Account Rejected : ");		
		m_oResult.append (m_iRejected);
		m_oResult.append ("\n");		
		m_oResult.append ("\nTotal Account ERROR : ");		
		m_oResult.append (m_iError);

		return m_oResult.toString();
	}
	
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		
		log.debug ("Num of rows : " + m_iTotalRows); 
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		log.debug ("Current Row : " + iRow); 

    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			short iFirstCellIdx = oRow.getFirstCellNum();
				HSSFCell oCell = oRow.getCell(iFirstCellIdx);
				if (!m_bStart) 
				{
					if (oCell.getStringCellValue() != null && oCell.getStringCellValue().equals(aHeaders[0])) 
					{ 
						if (validateHeader(oRow)) 
						{
						    m_bStart = true; 
					    }
                    }	
				}   
				else 
				{
					updateAccountData (oRow, iFirstCellIdx);
				}
			}
    	} 
    }

    public boolean validateHeader (HSSFRow _oRow)	
    	throws Exception
    {
        short iFirst = _oRow.getFirstCellNum();
        short iLast  = _oRow.getLastCellNum();
        
        int iHeader = 0;
        int iHeaderLength = aHeaders.length; 
        
        //to prevent generated excel from account page display array out of bound
        //since generated excel add another col named Current Balance
        if (iHeaderLength < (iLast - iFirst)) iLast = (short) (iFirst + iHeaderLength);        
        
        for (short i = iFirst; i < iLast; i++)
        {
            log.debug ("Requested Header : '" + aHeaders[iHeader] + "' Value In Excel File : '" + getString(_oRow, i) + "'");
            if (!aHeaders[iHeader].equals(getString(_oRow, i)))
            {
                throw new NestableException ("File Header Mismatch, Requested : '" + 
                	aHeaders[iHeader] + "' In Excel : '" + getString(_oRow, i) + "'");
            }
            iHeader++;
        }
        return true;
    }

    public boolean validateAccount (Account _oAccount, 
    								AccountType _oType, 
									Account _oParent,
                                    String _sAccountCode, 
									String _sAccountType, 
									String _sParentCode, 
									String _sCurrencyCode)
        throws Exception
    {
        boolean bValid = true;
        boolean bNoted = false;
        boolean bExist = false;
        
        //if exist then check whether account ever involved in any gl transaction expect opening balance transaction
        if (_oAccount != null)
        {
            bExist = GlTransactionTool.isExistInTransaction (_oAccount.getAccountId(), 
            			GlAttributes.i_GL_TRANS_OPENING_BALANCE, false);
        }
        
        //check whether account type name is correct
        if (_oType == null)
        {
            m_oRejected.append (_sAccountCode + " Account Type '" + _sAccountType + "' Is Invalid \n"); 
            bValid = false;    
            if (!bNoted) { bNoted = true; m_iRejected++; }
        }
        else 
        {
            //if exist in gl trans validate that account type must not changed between old and new values
            if (bExist)
            {
                if (_oType.getAccountType() != _oAccount.getAccountType())
                {
                    m_oRejected.append (_sAccountCode + 
                    	" Account Type May Not Changed, Because It Is Already Used In Transaction \n"); 
                    
                    bValid = false;    
                    if (!bNoted) { bNoted = true; m_iRejected++; }
                }
            }            
        }
        
        //validate parent Code
        if (StringUtil.isNotEmpty(_sParentCode))
        {
            if (_oParent == null)
            {
                m_oRejected.append (_sAccountCode + " Parent Account Code '" + _sParentCode + "' Is Invalid \n"); 
                bValid = false;   
                if (!bNoted) { bNoted = true; m_iRejected++; }
            }
        }
        //validate currency
        if (StringUtil.isNotEmpty(_sCurrencyCode))
        {
            Currency oCurrency = CurrencyTool.getCurrencyByCode(_sCurrencyCode);
            if (oCurrency == null)
            {
                m_oRejected.append (_sAccountCode + " Currency Code '" + _sCurrencyCode + "' Is Invalid \n"); 
                bValid = false;   
                if (!bNoted) { bNoted = true; m_iRejected++; }
            }
        }
        if (!bValid) m_oRejected.append ("-----------------\n"); 

        if (!bValid) log.debug ("Account " + _sAccountCode + " Invalid : " + m_oRejected);
        
        return bValid;
    }
    
    private void updateAccountData (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {            		
		if (getString(_oRow, _iIdx) != null && 
				!getString(_oRow, _iIdx).equals(aHeaders[0]))				
		{
			short i = 0;
			
			String sAccountCode  = getString(_oRow, _iIdx + i); i++;
			String sAccountName  = getString(_oRow, _iIdx + i); i++;
			String sDescription  = getString(_oRow, _iIdx + i); i++;
			String sAccountType  = getString(_oRow, _iIdx + i); i++;
			String sParentCode   = getString(_oRow, _iIdx + i); i++;
			String sCurrencyCode = getString(_oRow, _iIdx + i); i++;
			String sOpeningBal   = getString(_oRow, _iIdx + i); i++;
			String sAsDate       = getString(_oRow, _iIdx + i); i++;
			String sAltCode      = getString(_oRow, _iIdx + i); i++;

			Date dAsDate = CustomParser.parseDate(sAsDate);	           
			if (dAsDate == null) dAsDate = PreferenceTool.getStartDate();
			if (dAsDate == null) dAsDate = new Date();
			dAsDate = DateUtil.getStartOfDayDate(dAsDate);
			
			Account oAccount   = AccountTool.getAccountByCode(sAccountCode, false);
            Account oParent    = AccountTool.getAccountByCode(sParentCode, false);
			AccountType oType  = AccountTypeTool.getAccountTypeByName(sAccountType);
            Currency oCurrency = CurrencyTool.getCurrencyByCode(sCurrencyCode);

            log.debug("ROW " + _oRow.getRowNum());
			if (validateAccount (oAccount, oType, oParent, sAccountCode, sAccountType, sParentCode, sCurrencyCode))
			{								
				if (oAccount == null) 
				{	
					try
					{
						log.debug ( sAccountCode + " Not Exist, create new Code");
						m_iNewAccount++;
						m_oNewAccount.append (m_iNewAccount);					
						m_oNewAccount.append (". ");
						m_oNewAccount.append (sAccountCode);
						m_oNewAccount.append ("\t");
						m_oNewAccount.append (sAccountName);
						m_oNewAccount.append ("\n");
						
						oAccount = new Account();
						oAccount.setAccountId (IDGenerator.generateSysID(false));
						oAccount.setAccountCode(sAccountCode); 
						oAccount.setAccountName(sAccountName); 
						oAccount.setDescription(sDescription); 
						oAccount.setAccountType(oType.getAccountType()); 					
						oAccount.setParentId(AccountTool.getIDByCode(sParentCode, false));
						oAccount.setAccountLevel(AccountTool.setChildLevel(oAccount.getParentId()));
						oAccount.setHasChild(false);
						oAccount.setStatus(true);
						oAccount.setNormalBalance (oType.getNormalBalance());
						if (oCurrency != null) oAccount.setCurrencyId (oCurrency.getCurrencyId());
						else oAccount.setCurrencyId (CurrencyTool.getDefaultCurrency().getCurrencyId());
						
						//do not set opening balance from excel
						oAccount.setOpeningBalance (Attributes.bd_ZERO); 
						oAccount.setAsDate (dAsDate);
                        oAccount.setAltCode(sAltCode);
						oAccount.save();
						AccountManager.getInstance().refreshCache(oAccount);
						
						AccountTool.updateParentPosition (oParent, null);
					}
					catch (Exception _oEx)
					{
						log.error(_oEx);
						
						m_iError++;
						m_oError.append (m_iError);					
						m_oError.append (". ");
						m_oError.append (sAccountCode);
						m_oError.append ("\t");
						m_oError.append (sAccountName);
						m_oError.append ("\t(New Account) ERROR : " + _oEx.getMessage());
						m_oError.append ("\n");						
					}
				}
				else 
				{
					try
					{
						Account oOld = oAccount.copy();						
						log.debug (sAccountCode + " Account Code Exist ");
						m_iUpdated++;
						m_oUpdated.append (m_iUpdated);					
						m_oUpdated.append (". ");
						m_oUpdated.append (sAccountCode);
						m_oUpdated.append ("\t");
						m_oUpdated.append (sAccountName);
						m_oUpdated.append ("\n");
						oAccount.setAccountName(sAccountName); 
						oAccount.setDescription(sDescription); 
						oAccount.setAccountType(oType.getAccountType()); 
						oAccount.setNormalBalance (oType.getNormalBalance());

						//allow update currency if not currently exists in trans
						if(!GlTransactionTool.isExistInTransaction(oAccount.getAccountId(),  -1, false))
						{
							if (oCurrency != null) oAccount.setCurrencyId (oCurrency.getCurrencyId());
							else oAccount.setCurrencyId (CurrencyTool.getDefaultCurrency().getCurrencyId());
						}
						
						//TODO:check for problem updating parent ID and account level
						oAccount.setParentId(AccountTool.getIDByCode(sParentCode, false));
						oAccount.setAccountLevel(AccountTool.setChildLevel(oAccount.getParentId()));
                        oAccount.setAltCode(sAltCode);
                        if(dAsDate != null && AccountTool.isAsDateValid(dAsDate, oAccount.getAccountId()))
                        {
                        	oAccount.setAsDate(dAsDate);
                        }
						oAccount.save();
						AccountManager.getInstance().refreshCache(oAccount);
						UpdateHistoryTool.createHistory(oOld, oAccount, m_sUserName, null);
					}
					catch (Exception _oEx)
					{
						log.error(_oEx);
						
						m_iError++;
						m_oError.append (m_iError);					
						m_oError.append (". ");
						m_oError.append (sAccountCode);
						m_oError.append ("\t");
						m_oError.append (sAccountName);
						m_oError.append ("\t(Existing Account) ERROR : " + _oEx.getMessage());
						m_oError.append ("\n");						
					}                    
				}				
			}
		}    
    }    
}

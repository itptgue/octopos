package com.ssti.enterprise.medical.presentation.screens.transaction;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.CustomerMarginTool;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalAttributes;
import com.ssti.enterprise.pos.presentation.screens.transaction.ItemCodeLookup;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.framework.tools.StringUtil;

/**
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Item Code Lookup Screen Handler
 * <br>
 *
 * $@author  Author: Albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: MedItemCodeLookup.java,v $
 * Revision 1.1  2009/05/04 01:37:51  albert
 * *** empty log message ***
 *
 * Revision 1.23  2008/08/17 02:19:49  albert
 * *** empty log message ***
 *
 * 
 * </pre><br>
 */
public class MedItemCodeLookup extends ItemCodeLookup
{   
	private static final Log log = LogFactory.getLog(MedItemCodeLookup.class);

    public void doBuildTemplate(RunData data, Context context)
    {
		super.doBuildTemplate(data, context);
		MedConfigTool.setContextTool(context);
		if (m_oItem != null)
		{
			try 
			{
				String sCustID = data.getParameters().getString("CustomerId", "");
				String sLocID = data.getParameters().getString("LocationId", "");
				String sFrom = data.getParameters().getString("FromTransaction", "");

				int iType = data.getParameters().getInt("TxType",0);
				if(StringUtil.containsIgnoreCase(sFrom, "pos")  || iType == MedicalAttributes.i_NON_PRES) 	 iType = MedicalAttributes.i_NON_PRES;
				if(StringUtil.containsIgnoreCase(sFrom, "pres") || iType == MedicalAttributes.i_PRES_NORMAL) iType = MedicalAttributes.i_PRES_NORMAL;
				if(StringUtil.containsIgnoreCase(sFrom, "pwd")  || iType == MedicalAttributes.i_PRES_POWDER) iType = MedicalAttributes.i_PRES_POWDER;
				
				boolean bRfeeApplied = CustomerMarginTool.applyRfee(sCustID, sLocID, m_oItem, iType);
				boolean bPackApplied = CustomerMarginTool.applyPack(sCustID, sLocID, m_oItem, iType);
				
				//override context variable dDetQty (Existing Qty) 
				context.put("dDetQty", getExistingQty(data));
				
				context.put("CustomerID", sCustID);				
				context.put("RfeeApplied", bRfeeApplied);				
				context.put("PackApplied", bPackApplied);				
				context.put("IsPrescription", MedConfigTool.isPrescriptionItem(m_oItem));
			} 
			catch (Exception _oEx) 
			{		
				log.error(_oEx);
				_oEx.printStackTrace();
			}
		}
    }
    
    /**
     * 
     * @param data
     * @return
     * @throws Exception
     */
    private double getExistingQty (RunData data)
    	throws Exception
    {
    	double dDetQty = 0;
    	List vDetail = (List) data.getSession().getAttribute(MedicalAttributes.s_MTD); 
	    if (vDetail != null && m_oItem != null)
	    {
	    	dDetQty = BaseTool.countQtyPerItem(vDetail, m_oItem.getItemId(), "qty"); 
	    }
		return dDetQty;
    }
}

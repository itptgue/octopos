package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.DiscountPeer;
import com.ssti.enterprise.pos.om.PwpBuyPeer;
import com.ssti.enterprise.pos.om.PwpGetPeer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-03-08
 * - add method findCustomDiscount to get custom discount from DB and put to cache
 * 
 * </pre><br>
 */
public class DiscountManager
{
	private static Log log = LogFactory.getLog(DiscountManager.class);
	        
    private static DiscountManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "discount";    

    private DiscountManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static DiscountManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (DiscountManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new DiscountManager();
                }
            }
        }
        return m_oInstance;
    }

    public Discount getDiscountByID(String _sID, Connection _oConn)
		throws Exception
	{
    	String sKey = "DISCID" + _sID;
    	Cache oCache = m_oManager.getCache(s_CACHE);        
    	Element oElement = oCache.get(sKey);
        if (oElement == null)
        {
        	Criteria oCrit = new Criteria();
        	oCrit.add(DiscountPeer.DISCOUNT_ID, _sID);
        	List vData = DiscountPeer.doSelect(oCrit, _oConn);
        	if (vData.size() > 0) {
        		Discount oDisc = (Discount) vData.get(0);
        		oCache.put(new Element(sKey, oDisc));
        		return oDisc;
        	}
        }
        else
        {
        	return (Discount) oElement.getValue();
        }
    	return null;
	}
    
    /**
     * Find Item / Detail Discount
     * 
     * @param _sLocID
     * @param _sItemID
     * @param _sCustTypeID
     * @return Discount List
     * @throws Exception
     */
    public List findDiscount (String _sLocID,                                      
                              String _sItemID, 
                              String _sCustID,
                              String _sPTypeID,
                              List _vPTypeIDs,
                              Date _dTransDate)
        throws Exception
    {
        String sDate = CustomFormatter.formatCustomDateTime(_dTransDate,"yyMMddHH");
        String sKey = "DISC-" + _sLocID + "-" + _sItemID + "-" + _sCustID + "-" + _sPTypeID + "-" + sDate;
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            log.debug("From DB " + sKey);
            List vData = DiscountTool.queryDiscount(_sLocID,_sItemID,_sCustID,_sPTypeID,_vPTypeIDs,_dTransDate,-1);
            oCache.put(new Element (sKey, (Serializable) vData)); 
            return vData;
        }
        else 
        {
            log.debug("From CACHE " + sKey);
            return (List) oElement.getValue();
        }
    }
    
    /**
     * Find Total Discount
     * 
     * @param _sLocID      
     * @param _sCustID
     * @return Discount List
     * @throws Exception
     */
    public List findTotalDiscount(String _sLocID,                                      
                              	  String _sCustID,
                              	  String _sPTypeID,
                              	  List _vPTypeIDs,
                              	  Date _dTransDate,
                              	  boolean _bCoupon)
        throws Exception
    {
    	String sPref = "TDISC-";
        if(_bCoupon) sPref = "CDISC-";
    	String sDate = CustomFormatter.formatCustomDateTime(_dTransDate,"yyMMddHH");
        String sKey = sPref + _sLocID + "-" + _sCustID + "-" + _sPTypeID + "-" + sDate;        
        if (_vPTypeIDs.size() > 0)
        {
        	for(int i = 0; i < _vPTypeIDs.size(); i++)
        	{
        		sKey += (String) _vPTypeIDs.get(i);
        	}
        }
        
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            log.debug("From DB " + sKey);
            List vData = DiscountTool.queryTotalDiscount(_sLocID,_sCustID,_sPTypeID, _vPTypeIDs, _dTransDate,_bCoupon);
            oCache.put(new Element (sKey, (Serializable) vData)); 
            return vData;
        }
        else 
        {
            log.debug("From CACHE " + sKey);
            return (List) oElement.getValue();
        }
    }
    
    public List findActivePaymentTypeDisc(Date _dTrans, String _sCustID, String _sLocID) 
		throws Exception
	{
    	String sPref = "PTYPEDISC-";
    	String sDate = CustomFormatter.formatCustomDateTime(_dTrans,"yyMMdd");
        String sKey = sPref + _sLocID + "-" + _sCustID + "-" +  sDate;
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            log.debug("From DB " + sKey);
        	Criteria oCrit = new Criteria();
    		oCrit.add(DiscountPeer.PAYMENT_TYPE_ID,(Object)"",Criteria.NOT_EQUAL);
    		Date dToday = new Date();
    		if(StringUtil.isNotEmpty(_sLocID))
    		{
                Criteria.Criterion oLoc1 = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, (Object)("%"+_sLocID+"%"), Criteria.ILIKE);
                Criteria.Criterion oLoc2 = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID,"",Criteria.EQUAL);
                oCrit.add(oLoc1.or(oLoc2));
    		}
    		if(StringUtil.isNotEmpty(_sCustID))
    		{
    			String sCustCode = CustomerTool.getCodeByID(_sCustID);
    			String sCustTypeID = CustomerTool.getCustomerTypeByID(_sCustID); 
    			Criteria.Criterion oCust1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "%"+sCustCode+"%", Criteria.ILIKE);
                Criteria.Criterion oCust2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMERS, "", Criteria.EQUAL);

                Criteria.Criterion oCustType1 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, sCustTypeID ,Criteria.EQUAL);
                Criteria.Criterion oCustType2 = oCrit.getNewCriterion(DiscountPeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);

                oCrit.add(oCust1.or(oCust2));
                oCrit.add(oCustType1.or(oCustType2));                
    		}
    		if(_dTrans == null) _dTrans = new Date();
            oCrit.add(DiscountPeer.EVENT_BEGIN_DATE, DateUtil.getStartOfDayDate(_dTrans), Criteria.LESS_EQUAL);
            oCrit.and(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(_dTrans), Criteria.GREATER_EQUAL);
    		log.debug("Payment Type Disc Discount Criteria : " + oCrit);
    		List vData = DiscountPeer.doSelect(oCrit);
            
            oCache.put(new Element (sKey, (Serializable) vData)); 
            return vData;
        }
        else 
        {
            log.debug("From CACHE " + sKey);
            return (List) oElement.getValue();
        }
	}

    /**
     * find custom discount
     * 
     * @param _sLocID
     * @param _sCustID
     * @param _sPTypeID
     * @param _vPTypeIDs
     * @param _dTransDate
     * @return
     * @throws Exception
     */
    public List findCustomDiscount(String _sLocID,                                      
                              	   String _sCustID,
                              	   String _sPTypeID,
                              	   List _vPTypeIDs,
                              	   Date _dTransDate)
        throws Exception
    {
    	String sPref = "CL-DISC-";
    	String sDate = CustomFormatter.formatCustomDateTime(_dTransDate,"yyMMddHH");
        String sKey = sPref + _sLocID + "-" + _sCustID + "-" + _sPTypeID + "-" + sDate;        
        if (_vPTypeIDs.size() > 0)
        {
        	for(int i = 0; i < _vPTypeIDs.size(); i++)
        	{
        		sKey += (String) _vPTypeIDs.get(i);
        	}
        }
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            log.debug("From DB " + sKey);
            List vData = DiscountTool.queryCustomDiscount(_sLocID,_sCustID,_sPTypeID, _vPTypeIDs, _dTransDate);
            oCache.put(new Element (sKey, (Serializable) vData)); 
            return vData;
        }
        else 
        {
            log.debug("From CACHE " + sKey);
            return (List) oElement.getValue();
        }
    }

	public List getBuyByID (String _sPWPID, Connection _oConn)
    	throws Exception
    {
        String sKey = "PwpBuy" + _sPWPID;
        Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
            Criteria oCrit = new Criteria();
            oCrit.add(PwpBuyPeer.PWP_ID, _sPWPID);
			List vData = PwpBuyPeer.doSelect(oCrit, _oConn);
			oCache.put(new Element (sKey, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}

    public List getGetByID (String _sPWPID, Connection _oConn)
        throws Exception
    {
        String sKey = "PwpGet" + _sPWPID;
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            Criteria oCrit = new Criteria();
            oCrit.add(PwpGetPeer.PWP_ID, _sPWPID);
            List vData = PwpGetPeer.doSelect(oCrit, _oConn);
            oCache.put(new Element (sKey, (Serializable) vData)); 
            return vData;
        }
        else 
        {
            return (List) oElement.getValue();
        }
    }

	//THIS METHOD MUST BE CALLED AFTER SAVE PROCESS     
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);        
		oCache.remove("PwpBuy" + _sID);		
		oCache.remove("PwpGet" + _sID);		
		oCache.removeAll();
    }   
	
	public List getByItem(String _sItemID, String _sItemCode, String _sLocID)
		throws Exception
	{
		if(StringUtil.isEmpty(_sItemCode) && StringUtil.isNotEmpty(_sItemID))
		{
			_sItemCode = ItemTool.getItemCodeByID(_sItemID);
		}    		

		String sPref = "DISCBYITEM-ID-";
        String sKey = sPref + _sItemID + "-CD-" + _sItemCode + "-LOCID-" + _sLocID;
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            log.debug("From DB " + sKey);
        	Criteria oCrit = new Criteria();
    		Date dToday = new Date(); 
    		List vData = new ArrayList(1);
    		
    		if(StringUtil.isNotEmpty(_sItemCode))
    		{
        		if(StringUtil.isNotEmpty(_sLocID))
        		{
                    Criteria.Criterion oLoc1  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "%"+_sLocID+"%", Criteria.ILIKE);
                    Criteria.Criterion oLoc2  = oCrit.getNewCriterion(DiscountPeer.LOCATION_ID, "", Criteria.EQUAL);
                    oCrit.add(oLoc1.or(oLoc2));
        		}    			
                Criteria.Criterion oItem1 = oCrit.getNewCriterion(DiscountPeer.ITEMS, _sItemCode, Criteria.EQUAL);        //002 i.e we want disc for PLU 002
                Criteria.Criterion oItem2 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + _sItemCode, Criteria.ILIKE); //001,002
                Criteria.Criterion oItem3 = oCrit.getNewCriterion(DiscountPeer.ITEMS, _sItemCode + ",%", Criteria.ILIKE); //002,003
                Criteria.Criterion oItem4 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "%," + _sItemCode + ",%", Criteria.ILIKE); //001,002,003            
                //Criteria.Criterion oItem5 = oCrit.getNewCriterion(DiscountPeer.ITEMS, "", Criteria.EQUAL);
                oCrit.and(oItem1.or(oItem2).or(oItem3).or(oItem4));
                oCrit.add(DiscountPeer.EVENT_BEGIN_DATE, DateUtil.getStartOfDayDate(dToday), Criteria.LESS_EQUAL);
                oCrit.and(DiscountPeer.EVENT_END_DATE, DateUtil.getEndOfDayDate(dToday), Criteria.GREATER_EQUAL);
        		log.debug("By Item Discount Criteria : " + oCrit);
        		vData = DiscountPeer.doSelect(oCrit);
        		oCache.put(new Element (sKey, (Serializable) vData));
    		}             
            return vData;
        }
        else 
        {
            log.debug("From CACHE " + sKey);
            return (List) oElement.getValue();
        }
	}		
	
    /**
     * @deprecated
     * Find Item / Detail Discount
     * 
     * @param _sLocID
     * @param _sItemID
     * @param _sCustTypeID
     * @return Discount List
     * @throws Exception
     */
    public List findMultiDiscount(String _sLocID,                                      
                              	  String _sItemID, 
                              	  String _sCustID,
                              	  String _sPTypeID,
                              	  Date _dTransDate)
        throws Exception
    {
        String sDate = CustomFormatter.formatCustomDateTime(_dTransDate,"yyMMddHH");
        String sKey = "MDISC-" + _sLocID + "-" + _sItemID + "-" + _sCustID + "-" + _sPTypeID + "-" + sDate;
        Cache oCache = m_oManager.getCache(s_CACHE);        
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            log.debug("From DB " + sKey);
            List vData = DiscountTool.queryDiscount(_sLocID,_sItemID,_sCustID,_sPTypeID,null,_dTransDate,DiscountTool.i_BY_MULTI);
            oCache.put(new Element (sKey, (Serializable) vData)); 
            return vData;
        }
        else 
        {
            log.debug("From CACHE " + sKey);
            return (List) oElement.getValue();
        }
    }
}
package com.ssti.enterprise.pos.presentation.screens.report.sales;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.report.SalesReportTool;

public class VendorItemSalesReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	        if (data.getParameters().getInt("Op") == 1)
    	    {
	    	    setParams(data);
	    	    context.put("salesreport",SalesReportTool.getInstance());
            }
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
		}    
    }    
}


package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class PurchaseInvoiceExp
    extends com.ssti.enterprise.pos.om.BasePurchaseInvoiceExp
    implements Persistent
{
	Account account = null;
	public Account getAccount()
	{
		if (StringUtil.isNotEmpty(getAccountId()))
		{
			try
			{
				account = AccountTool.getAccountByID(getAccountId());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return account;
	}
	
	public String getAccountCode()
	{
		getAccount();
		if (account != null) return account.getAccountCode();
		return "";
	}
	
	public String getAccountName()
	{
		getAccount();
		if (account != null) return account.getAccountName();
		return "";
	}			
}

package com.ssti.enterprise.pos.presentation.screens;

import java.io.File;
import java.util.Calendar;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Default Screen Class
 * <br>
 *
 * $@author  Author: LionZ $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class UploadLicense extends SecureScreen
{
	protected boolean isAuthorized (RunData data)  
    	throws Exception 
    {	        
        return true;
    }
    
	public void doBuildTemplate(RunData data, Context context)
    {
    	int op = data.getParameters().getInt("op");
    	String pwd = data.getParameters().getString("password");
    	FileItem oLF = data.getParameters().getFileItem("licFile");
    	if(op == 1 && oLF != null && checkPwd(pwd))
    	{    		
			String fileName  = oLF.getName();					
			if (StringUtil.isNotEmpty(fileName)) 
			{
				String sSeparator  = System.getProperty("file.separator");			
				String sFilePath   = IOTool.getRealPath(data, "/WEB-INF/");		
				int iSeparatorIndex = fileName.lastIndexOf (sSeparator.charAt(0));
				if (iSeparatorIndex > 0)
				{
					fileName = fileName.substring (iSeparatorIndex);				
				}
				String sOriFile = sFilePath + sSeparator + fileName;		
				String sLicFile = sFilePath + sSeparator + "license.dat";
				
				log.debug("File Path:" + sOriFile + " :" + sLicFile);
				try 
				{
					oLF.write (new File (sOriFile)); //save license file
					oLF.write (new File (sLicFile)); //save license file
					data.setMessage("License File Upload Success");
				} 
				catch (Exception e) {
					String err = "ERROR:" + e.getMessage();
					log.error(err, e);
					data.setMessage(err);
				}				
    		}        	
    	}
    }

	private boolean checkPwd(String pwd) {
		Calendar c = Calendar.getInstance();
		int iDay = c.get(Calendar.DAY_OF_MONTH) * 3 - 2;
		int iMon = c.get(Calendar.MONTH) * 7 - 2;
		String sPwd = 
			StringUtil.formatNumberString(Integer.toString(iDay),2) + 
			StringUtil.formatNumberString(Integer.toString(iMon),2);	

		if(StringUtil.equals(pwd, "crossfire123!@#") || 
		   StringUtil.equals(pwd, "cr0ssf1r3!@#") ||
		   StringUtil.equals(pwd, sPwd)) return true;
		return false;
	}
}

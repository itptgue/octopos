package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to BatchLocation
 */
public abstract class BaseBatchLocation extends BaseObject
{
    /** The Peer class */
    private static final BatchLocationPeer peer =
        new BatchLocationPeer();

        
    /** The value for the batchLocationId field */
    private String batchLocationId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the batchNo field */
    private String batchNo;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the expiredDate field */
    private Date expiredDate;
      
    /** The value for the lastUpdate field */
    private Date lastUpdate;
  
    
    /**
     * Get the BatchLocationId
     *
     * @return String
     */
    public String getBatchLocationId()
    {
        return batchLocationId;
    }

                        
    /**
     * Set the value of BatchLocationId
     *
     * @param v new value
     */
    public void setBatchLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.batchLocationId, v))
              {
            this.batchLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BatchNo
     *
     * @return String
     */
    public String getBatchNo()
    {
        return batchNo;
    }

                        
    /**
     * Set the value of BatchNo
     *
     * @param v new value
     */
    public void setBatchNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.batchNo, v))
              {
            this.batchNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpiredDate
     *
     * @return Date
     */
    public Date getExpiredDate()
    {
        return expiredDate;
    }

                        
    /**
     * Set the value of ExpiredDate
     *
     * @param v new value
     */
    public void setExpiredDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.expiredDate, v))
              {
            this.expiredDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdate
     *
     * @return Date
     */
    public Date getLastUpdate()
    {
        return lastUpdate;
    }

                        
    /**
     * Set the value of LastUpdate
     *
     * @param v new value
     */
    public void setLastUpdate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdate, v))
              {
            this.lastUpdate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BatchLocationId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("LocationId");
              fieldNames.add("BatchNo");
              fieldNames.add("Description");
              fieldNames.add("Qty");
              fieldNames.add("ExpiredDate");
              fieldNames.add("LastUpdate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BatchLocationId"))
        {
                return getBatchLocationId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("BatchNo"))
        {
                return getBatchNo();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("ExpiredDate"))
        {
                return getExpiredDate();
            }
          if (name.equals("LastUpdate"))
        {
                return getLastUpdate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BatchLocationPeer.BATCH_LOCATION_ID))
        {
                return getBatchLocationId();
            }
          if (name.equals(BatchLocationPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(BatchLocationPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(BatchLocationPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(BatchLocationPeer.BATCH_NO))
        {
                return getBatchNo();
            }
          if (name.equals(BatchLocationPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(BatchLocationPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(BatchLocationPeer.EXPIRED_DATE))
        {
                return getExpiredDate();
            }
          if (name.equals(BatchLocationPeer.LAST_UPDATE))
        {
                return getLastUpdate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBatchLocationId();
            }
              if (pos == 1)
        {
                return getItemId();
            }
              if (pos == 2)
        {
                return getItemCode();
            }
              if (pos == 3)
        {
                return getLocationId();
            }
              if (pos == 4)
        {
                return getBatchNo();
            }
              if (pos == 5)
        {
                return getDescription();
            }
              if (pos == 6)
        {
                return getQty();
            }
              if (pos == 7)
        {
                return getExpiredDate();
            }
              if (pos == 8)
        {
                return getLastUpdate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BatchLocationPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BatchLocationPeer.doInsert((BatchLocation) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BatchLocationPeer.doUpdate((BatchLocation) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key batchLocationId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBatchLocationId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBatchLocationId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBatchLocationId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public BatchLocation copy() throws TorqueException
    {
        return copyInto(new BatchLocation());
    }
  
    protected BatchLocation copyInto(BatchLocation copyObj) throws TorqueException
    {
          copyObj.setBatchLocationId(batchLocationId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setLocationId(locationId);
          copyObj.setBatchNo(batchNo);
          copyObj.setDescription(description);
          copyObj.setQty(qty);
          copyObj.setExpiredDate(expiredDate);
          copyObj.setLastUpdate(lastUpdate);
  
                    copyObj.setBatchLocationId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BatchLocationPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("BatchLocation\n");
        str.append("-------------\n")
           .append("BatchLocationId      : ")
           .append(getBatchLocationId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("BatchNo              : ")
           .append(getBatchNo())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("ExpiredDate          : ")
           .append(getExpiredDate())
           .append("\n")
           .append("LastUpdate           : ")
           .append(getLastUpdate())
           .append("\n")
        ;
        return(str.toString());
    }
}

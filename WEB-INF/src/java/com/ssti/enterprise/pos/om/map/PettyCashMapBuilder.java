package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PettyCashMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PettyCashMapBuilder";

    /**
     * Item
     * @deprecated use PettyCashPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "petty_cash";
    }

  
    /**
     * petty_cash.PETTY_CASH_ID
     * @return the column name for the PETTY_CASH_ID field
     * @deprecated use PettyCashPeer.petty_cash.PETTY_CASH_ID constant
     */
    public static String getPettyCash_PettyCashId()
    {
        return "petty_cash.PETTY_CASH_ID";
    }
  
    /**
     * petty_cash.PETTY_CASH_NO
     * @return the column name for the PETTY_CASH_NO field
     * @deprecated use PettyCashPeer.petty_cash.PETTY_CASH_NO constant
     */
    public static String getPettyCash_PettyCashNo()
    {
        return "petty_cash.PETTY_CASH_NO";
    }
  
    /**
     * petty_cash.INCOMING_OUTGOING
     * @return the column name for the INCOMING_OUTGOING field
     * @deprecated use PettyCashPeer.petty_cash.INCOMING_OUTGOING constant
     */
    public static String getPettyCash_IncomingOutgoing()
    {
        return "petty_cash.INCOMING_OUTGOING";
    }
  
    /**
     * petty_cash.PETTY_CASH_TYPE_ID
     * @return the column name for the PETTY_CASH_TYPE_ID field
     * @deprecated use PettyCashPeer.petty_cash.PETTY_CASH_TYPE_ID constant
     */
    public static String getPettyCash_PettyCashTypeId()
    {
        return "petty_cash.PETTY_CASH_TYPE_ID";
    }
  
    /**
     * petty_cash.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PettyCashPeer.petty_cash.LOCATION_ID constant
     */
    public static String getPettyCash_LocationId()
    {
        return "petty_cash.LOCATION_ID";
    }
  
    /**
     * petty_cash.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use PettyCashPeer.petty_cash.TRANSACTION_DATE constant
     */
    public static String getPettyCash_TransactionDate()
    {
        return "petty_cash.TRANSACTION_DATE";
    }
  
    /**
     * petty_cash.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use PettyCashPeer.petty_cash.REFERENCE_NO constant
     */
    public static String getPettyCash_ReferenceNo()
    {
        return "petty_cash.REFERENCE_NO";
    }
  
    /**
     * petty_cash.PETTY_CASH_AMOUNT
     * @return the column name for the PETTY_CASH_AMOUNT field
     * @deprecated use PettyCashPeer.petty_cash.PETTY_CASH_AMOUNT constant
     */
    public static String getPettyCash_PettyCashAmount()
    {
        return "petty_cash.PETTY_CASH_AMOUNT";
    }
  
    /**
     * petty_cash.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use PettyCashPeer.petty_cash.USER_NAME constant
     */
    public static String getPettyCash_UserName()
    {
        return "petty_cash.USER_NAME";
    }
  
    /**
     * petty_cash.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PettyCashPeer.petty_cash.DESCRIPTION constant
     */
    public static String getPettyCash_Description()
    {
        return "petty_cash.DESCRIPTION";
    }
  
    /**
     * petty_cash.EMPLOYEE_ID
     * @return the column name for the EMPLOYEE_ID field
     * @deprecated use PettyCashPeer.petty_cash.EMPLOYEE_ID constant
     */
    public static String getPettyCash_EmployeeId()
    {
        return "petty_cash.EMPLOYEE_ID";
    }
  
    /**
     * petty_cash.STATUS
     * @return the column name for the STATUS field
     * @deprecated use PettyCashPeer.petty_cash.STATUS constant
     */
    public static String getPettyCash_Status()
    {
        return "petty_cash.STATUS";
    }
  
    /**
     * petty_cash.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use PettyCashPeer.petty_cash.CANCEL_BY constant
     */
    public static String getPettyCash_CancelBy()
    {
        return "petty_cash.CANCEL_BY";
    }
  
    /**
     * petty_cash.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use PettyCashPeer.petty_cash.CANCEL_DATE constant
     */
    public static String getPettyCash_CancelDate()
    {
        return "petty_cash.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("petty_cash");
        TableMap tMap = dbMap.getTable("petty_cash");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("petty_cash.PETTY_CASH_ID", "");
                          tMap.addColumn("petty_cash.PETTY_CASH_NO", "");
                            tMap.addColumn("petty_cash.INCOMING_OUTGOING", Integer.valueOf(0));
                          tMap.addColumn("petty_cash.PETTY_CASH_TYPE_ID", "");
                          tMap.addColumn("petty_cash.LOCATION_ID", "");
                          tMap.addColumn("petty_cash.TRANSACTION_DATE", new Date());
                          tMap.addColumn("petty_cash.REFERENCE_NO", "");
                            tMap.addColumn("petty_cash.PETTY_CASH_AMOUNT", bd_ZERO);
                          tMap.addColumn("petty_cash.USER_NAME", "");
                          tMap.addColumn("petty_cash.DESCRIPTION", "");
                          tMap.addColumn("petty_cash.EMPLOYEE_ID", "");
                            tMap.addColumn("petty_cash.STATUS", Integer.valueOf(0));
                          tMap.addColumn("petty_cash.CANCEL_BY", "");
                          tMap.addColumn("petty_cash.CANCEL_DATE", new Date());
          }
}

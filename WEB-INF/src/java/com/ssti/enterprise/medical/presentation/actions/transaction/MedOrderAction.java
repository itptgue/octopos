package com.ssti.enterprise.medical.presentation.actions.transaction;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.MedOrder;
import com.ssti.enterprise.medical.presentation.screens.transaction.MedOrderTrans;
import com.ssti.enterprise.medical.tools.MedOrderTool;
import com.ssti.enterprise.pos.presentation.actions.transaction.TransactionSecureAction;
import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source$
 * Purpose: Sales Transaction Action Class 
 *
 * @author  $Author$
 * @version $Id$
 * @see MedOrder
 *
 * $Log$
 * 
 * 2017-03-13
 * - Change method doSave, call InvoicePayment validatePayment to make sure invoice payment data 
 *   is inline with MedicalSales data
 *
 */

public class MedOrderAction extends TransactionSecureAction
{
	private static final String s_TR = MedOrderTrans.s_MSO;
	private static final String s_TD = MedOrderTrans.s_MSD;
	
	private static final String s_VIEW_PERM   = "View Sales Order";
	private static final String s_CREATE_PERM = "Create Sales Order";
	private static final String s_CANCEL_PERM = "Cancel Sales Order";
	
    private static final String[] a_CREATE_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM   = {s_SALES_PERM, s_VIEW_PERM};
    	
	private static Log log = LogFactory.getLog ( MedOrderAction.class );
	
    private HttpSession oSes = null;
    private MedOrder oTR;
    private List vTD;
    
    private static final int i_SAVE   = 1;
    private static final int i_UPDATE = 2;
    private static final int i_CANCEL = 3;
    
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getInt("save") == i_SAVE || 
           data.getParameters().getInt("save") == i_UPDATE)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
        else if (data.getParameters().getInt("save") == i_CANCEL)
		{
        	return isAuthorized (data, a_CANCEL_PERM);
		}
    	else
    	{
            return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {	
    	if (data.getParameters().getInt("save") == i_SAVE || 
    		data.getParameters().getInt("save") == i_UPDATE) 
    	{
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("save") == i_CANCEL) 
    	{
    		doDelete (data,context);
    	}
    	else 
    	{
    		doFind (data,context);
    	}
    }
    
    public void doFind ( RunData data, Context context )
        throws Exception
    {
    	super.doFind(data);
    	
    	String sCustomerID = data.getParameters().getString("CustomerId", "");
    	String sLocationID = data.getParameters().getString("LocationId", "");
    	String sCashierName = data.getParameters().getString("CashierName", "");
		int iStatus = data.getParameters().getInt("Status");
		
		LargeSelect vTR = MedOrderTool.findData (iCond, sKeywords, dStart, dEnd,
		                                            sCustomerID, sLocationID, iStatus, 
													iLimit, iGroupBy, sCashierName, sCurrencyID);
		
		data.getUser().setTemp("findMedOrderRes", vTR);	
    }
    
	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{
			
			initSession ( data );
			MedOrderTool.setHeaderProperties (oTR, vTD, data);
			
			log.debug("oMTR " + oTR );
			log.debug("vMTD " + vTD );
			
			prepareTransData ( data );
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			
			//save preparation failed. still ok to retry
			data.setMessage(_oEx.getMessage());
			return;
		}
		
		try 
		{
			MedOrderTool.saveData (oTR, vTD);		
        	data.setMessage(LocaleTool.getString("so_save_success"));
        	resetSession ( data );
        	context.put(s_PRINT_READY, Boolean.valueOf(true));
        }
        catch (Exception _oEx) 
        {
        	//must create new trans, no retry allowed 
			handleError (data, LocaleTool.getString("so_save_failed"), _oEx);
        }
    }
    
    public void doDelete ( RunData data, Context context )
        throws Exception
    {
		try 
		{
			initSession (data);	
			MedOrderTool.cancelSales(oTR, vTD, data.getUser().getName(), null);
        	data.setMessage(LocaleTool.getString("so_cancel_success"));
        	oSes.setAttribute (s_TR, oTR);
			oSes.setAttribute (s_TD, vTD);
        }
        catch (Exception _oEx) 
		{
			handleError (data, LocaleTool.getString("so_cancel_failed"), _oEx);
        }
    }
    
    private void initSession ( RunData data ) 
    	throws Exception
    {
    	oSes = data.getSession();
		if ( oSes.getAttribute (s_TR) == null ||
			 oSes.getAttribute (s_TD) == null ) 
		{
			throw new Exception ("Order Data Invalid");
		}    
		oTR = (MedOrder) oSes.getAttribute (s_TR);
		vTD = (List) oSes.getAttribute (s_TD);		
    }
    
    private void prepareTransData ( RunData data ) 
    	throws Exception
    {
    	//check transaction data 
    	if ( oTR.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 0"); } //-- should never happen 
		if ( vTD.size () < 1) {throw new Exception ("Total Item Purchased < 1"); } //-- should never happen 	
    }

    private void resetSession (RunData data) 
    	throws Exception
    {
		oSes.setAttribute(s_TR, oTR);
		oSes.setAttribute(s_TD, vTD);
    }
}
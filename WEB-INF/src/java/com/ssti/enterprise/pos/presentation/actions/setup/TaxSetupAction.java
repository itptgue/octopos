package com.ssti.enterprise.pos.presentation.actions.setup;

import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.TaxManager;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.TaxPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class TaxSetupAction extends POSSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

    public void doInsert(RunData data, Context context)
        throws Exception
    {
        try
        {
            Tax oTax = new Tax();
            setProperties(oTax, data);
            oTax.setTaxId (IDGenerator.generateSysID());
            oTax.save();
            TaxManager.getInstance().refreshCache(oTax);
            data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
                data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
            }
            else
            {
                data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
            }
        }
    }

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
        try
        {
            Tax oTax = TaxTool.getTaxByID(data.getParameters().getString("ID"));
            Tax oOld = oTax.copy();
            setProperties (oTax, data);
            oTax.save();
            TaxManager.getInstance().refreshCache(oTax);
            UpdateHistoryTool.createHistory(oOld, oTax, data.getUser().getName(), null);
            data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
                data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
            }
            else
            {
                data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
            }
        }
    }

    public void doDelete(RunData data, Context context)
        throws Exception
    {
        try
        {
            String sID = data.getParameters().getString("ID");
            if(SqlUtil.validateFKRef("customer","default_tax_id",sID)
                && SqlUtil.validateFKRef("vendor","default_tax_id",sID)
                && SqlUtil.validateFKRef("item","tax_id",sID)
                && SqlUtil.validateFKRef("sales_transaction_detail","tax_id",sID)
                && SqlUtil.validateFKRef("purchase_order_detail","tax_id",sID)
                && SqlUtil.validateFKRef("purchase_receipt_detail","tax_id",sID))
            {
            	Tax oTax = TaxTool.getTaxByID(data.getParameters().getString("ID"));            	
            	String sCode = TaxTool.getCodeByID(sID);
                Criteria oCrit = new Criteria();
                oCrit.add(TaxPeer.TAX_ID, sID);
                TaxPeer.doDelete(oCrit);                                
                TaxManager.getInstance().refreshCache(sID, sCode);
                UpdateHistoryTool.createDelHistory(oTax, data.getUser().getName(), null);
                data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
            }
            else
            {
                data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
            }
        }
        catch (Exception _oEx)
        {
            data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

    private void setProperties (Tax _oTax, RunData data)
    {
        try
        {
            data.getParameters().setProperties(_oTax);
            boolean bIsDefault = data.getParameters().getBoolean("DefaultPurchaseTax");
            if(bIsDefault)
            {
                Criteria oCrit = new Criteria();
                oCrit.add(TaxPeer.DEFAULT_PURCHASE_TAX,bIsDefault);
                List vTax = TaxPeer.doSelect(oCrit);
                if(vTax.size()>0)
                {
                    Tax oTaxTemp = new Tax();
                    oTaxTemp = (Tax)vTax.get(0);
                    oTaxTemp.setDefaultPurchaseTax(false);
                    oTaxTemp.save();
                }
            }
            
            bIsDefault = data.getParameters().getBoolean("DefaultSalesTax");
            if(bIsDefault)
            {
                Criteria oCrit = new Criteria();
                oCrit.add(TaxPeer.DEFAULT_SALES_TAX,bIsDefault);
                List vTax = TaxPeer.doSelect(oCrit);
                if(vTax.size()>0)
                {
                    Tax oTaxTemp = new Tax();
                    oTaxTemp = (Tax)vTax.get(0);
                    oTaxTemp.setDefaultSalesTax(false);
                    oTaxTemp.save();
                }
            }
        }
        catch (Exception _oEx)
        {
            data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
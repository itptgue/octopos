package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.PaymentTypeManager;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.PaymentTypePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PaymentTypeTool.java,v 1.16 2008/06/29 07:11:57 albert Exp $ <br>
 *
 * <pre>
 * $Log: PaymentTypeTool.java,v $
 * Revision 1.16  2008/06/29 07:11:57  albert
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/27 02:22:24  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/02/23 14:44:43  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class PaymentTypeTool extends BaseTool 
{
	public static List getAllPaymentType()
		throws Exception
	{
		return PaymentTypeManager.getInstance().getAllPaymentType();
	}

	public static PaymentType getPaymentTypeByID(String _sID)
    	throws Exception
    {
		return getPaymentTypeByID(_sID, null);
	}

	public static PaymentType getPaymentTypeByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return PaymentTypeManager.getInstance().getPaymentTypeByID(_sID, _oConn);
	}

	public static String getCodeByID(String _sID)
		throws Exception
	{
		return getPaymentTypeCodeByID(_sID);
	}
	
	public static String getPaymentTypeCodeByID(String _sID)
    	throws Exception
    {
		PaymentType oPayment = getPaymentTypeByID(_sID);
		if (oPayment != null) return oPayment.getPaymentTypeCode();
		return "";
	}

	public static String getDescriptionByID(String _sID)
		throws Exception
	{
		PaymentType oPayment = getPaymentTypeByID(_sID);
		if ( oPayment != null) {
			return oPayment.getDescription();
		}
		return "";
	}	
	
	public static boolean isCreditPaymentType (String _sID)
    	throws Exception
    {
		return isCreditPaymentType (_sID, null);
	}

	public static boolean isCreditPaymentType (String _sID, Connection _oConn)
    	throws Exception
    {
		PaymentType oPayment = getPaymentTypeByID(_sID, _oConn);
		if (oPayment != null) return oPayment.getIsCredit();
		return false;
	}	
	
	public static List getPaymentTypeIDByStatus(boolean _bIsCredit)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PaymentTypePeer.IS_CREDIT, _bIsCredit);
		List vData = PaymentTypePeer.doSelect(oCrit);
		List vID = new ArrayList(vData.size());
		for(int i=0; i < vData.size(); i++)
		{
			String sID = ((PaymentType)vData.get(i)).getPaymentTypeId();
			vID.add( sID );
		}
		return vID;
	}

	public static PaymentType getDefaultPaymentType()
    	throws Exception
    {
		List vData = getAllPaymentType();
        if (vData.size() > 0) {
			return (PaymentType) vData.get(0);
		}
		return null;
	}

	public static PaymentType getMultiType()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PaymentTypePeer.IS_MULTIPLE_PAYMENT, true);
		 List vData = PaymentTypePeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (PaymentType) vData.get(0);
		}
		return null;
	}

	/**
	 * @param _sCode
	 * @return payment type
	 */
	public static PaymentType getPaymentTypeByCode(String _sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (PaymentTypePeer.PAYMENT_TYPE_CODE, _sCode);
		List vData = PaymentTypePeer.doSelect(oCrit);
		if (vData.size()>0)
		{
			return (PaymentType)vData.get(0);
		}
		return null;
	}
	
	public static String getIDByCode(String _sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (PaymentTypePeer.PAYMENT_TYPE_CODE, _sCode);
		List vData = PaymentTypePeer.doSelect(oCrit);
		if (vData.size()>0)
		{
			return ((PaymentType)vData.get(0)).getPaymentTypeId();
		}
		return "";
	}
	
	public static PaymentTypeBankTool getPaymentTypeBankTool()
	{
		return PaymentTypeBankTool.getInstance();
	}
	
	public static PaymentBillsTool getPaymentBillsTool()
	{
		return PaymentBillsTool.getInstance();
	}

	public static PaymentVoucherTool getPaymentVoucherTool()
	{
		return PaymentVoucherTool.getInstance();
	}
	
	public static String getTransTypeStr(int _iType)
	{
		if(_iType == 1) return LocaleTool.getString("all");
		if(_iType == 2) return LocaleTool.getString("sales_transaction");
		if(_iType == 3) return LocaleTool.getString("purchase_transaction");	
		return "";		
	}
	

}

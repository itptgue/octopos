package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.PreferenceTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/PurchaseSecureScreen.java,v $
 * Purpose: Purchase Transaction Secure screen, super class for all purchase transaction screen
 * implements TransactionAttributes to hold various transaction related constants
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseSecureScreen.java,v 1.2 2008/03/15 12:39:33 albert Exp $
 *
 * $Log: PurchaseSecureScreen.java,v $
 * Revision 1.2  2008/03/15 12:39:33  albert
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/13 03:07:07  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/02/15 03:29:38  albert
 * *** empty log message ***
 *
 *
 */
public class PurchaseSecureScreen extends TransactionSecureScreen 
{ 	
    protected static final boolean b_PURCHASE_MERGE_DETAIL = PreferenceTool.getPurchMergeSameItem();
    protected static final boolean b_PURCHASE_USE_FOB_COURIER = PreferenceTool.getPurchFobCourier();
    protected static final boolean b_PURCHASE_FIRST_LAST = PreferenceTool.getPurchFirstlineInsert();
    
    protected static final String s_PURCHASE_COMMA_SCALE  = "PurchasecommaScale";
    protected int i_MERGE_DET = 1;       
    
	public void setContext(RunData data, Context context)
	{
    	//set config in context
    	context.put(s_MULTI_CURRENCY, b_PURCHASE_MULTI_CURRENCY);
    	context.put(s_PURCHASE_COMMA_SCALE, i_PURCHASE_COMMA_SCALE);
    	context.put(s_RELATED_EMPLOYEE, b_PURCHASE_RELATED_EMPLOYEE);
    	context.put(s_USE_FOB_COURIER, b_PURCHASE_USE_FOB_COURIER);
    	
    	if(hasPermission(data, "Access All Vendor"))
    	{
    	    context.put(s_RELATED_EMPLOYEE, false);
        }
	}
	
	public void doBuildTemplate(RunData data, Context context)
	{
		i_MERGE_DET = PreferenceTool.getPurchMergeItem();		
    	setContext(data, context);    	
    	super.doBuildTemplate(data, context);
	}		
}

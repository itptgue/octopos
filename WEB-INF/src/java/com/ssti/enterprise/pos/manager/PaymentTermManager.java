package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PaymentEdcConfig;
import com.ssti.enterprise.pos.om.PaymentEdcConfigPeer;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentTermPeer;
import com.ssti.framework.tools.StringUtil;

public class PaymentTermManager
{   
	private static Log log = LogFactory.getLog(PaymentTermManager.class);
	 
    private static PaymentTermManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private PaymentTermManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static PaymentTermManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (PaymentTermManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new PaymentTermManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllPaymentTerm ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllPaymentTerm");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addDescendingOrderByColumn (PaymentTermPeer.IS_DEFAULT);	
			oCrit.addAscendingOrderByColumn (PaymentTermPeer.PAYMENT_TERM_CODE);
			oCrit.add(PaymentTermPeer.IS_EDC, false);
			List vData = PaymentTermPeer.doSelect(oCrit);
			oCache.put(new Element ("AllPaymentTerm", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	

	public List getAll ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllPaymentTermIncEDC");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addDescendingOrderByColumn (PaymentTermPeer.IS_DEFAULT);	
			oCrit.addAscendingOrderByColumn (PaymentTermPeer.PAYMENT_TERM_CODE);
			List vData = PaymentTermPeer.doSelect(oCrit);
			oCache.put(new Element ("AllPaymentTermIncEDC", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	
	
	public List getEDC ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllPaymentTermEDC");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (PaymentTermPeer.PAYMENT_TERM_CODE);
			oCrit.add(PaymentTermPeer.IS_EDC, true);
			List vData = PaymentTermPeer.doSelect(oCrit);
			oCache.put(new Element ("AllPaymentTermEDC", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}		

	public PaymentTerm getPaymentTermByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("PaymentTerm" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(PaymentTermPeer.PAYMENT_TERM_ID, _sID);
				List vData = PaymentTermPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("PaymentTerm" + _sID, (Serializable)vData.get(0))); 
					return (PaymentTerm) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (PaymentTerm) oElement.getValue();
		}
	}
	
	public List findEDCConfig (String _sPTID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("FindEDCConfig" + _sPTID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (PaymentEdcConfigPeer.MERCHANT_ID);	
			oCrit.add(PaymentEdcConfigPeer.PAYMENT_TERM_ID, _sPTID);
			List vData = PaymentEdcConfigPeer.doSelect(oCrit);
			oCache.put(new Element ("FindEDCConfig" + _sPTID, (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	
	
	public PaymentEdcConfig getEDCConfig (String _sPTID, String _sLocID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("EDCConfig" + _sPTID + _sLocID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(PaymentEdcConfigPeer.PAYMENT_TERM_ID, _sPTID);
			oCrit.add(PaymentEdcConfigPeer.LOCATION_ID, _sLocID);			
			oCrit.addAscendingOrderByColumn (PaymentEdcConfigPeer.MERCHANT_ID);				
			List vData = PaymentEdcConfigPeer.doSelect(oCrit);
			PaymentEdcConfig oData = null;
			if (vData.size() > 0)
			{
				oData = (PaymentEdcConfig) vData.get(0);				
			}
			oCache.put(new Element ("EDCConfig" + _sPTID + _sLocID, (Serializable)oData));
			return oData;
		}
		else {
			return (PaymentEdcConfig) oElement.getValue();
		}
	}		

	//THIS METHOD MUST BE CALLED AFTER PAYMENT TERM SAVE PROCESS     
	public void refreshCache (PaymentTerm _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getPaymentTermId())) 
        {        	
            oCache.remove("PaymentTerm" + _oData.getPaymentTermId());
            oCache.put(new Element ("PaymentTerm" + _oData.getPaymentTermId(), _oData));
            oCache.remove("FindEDCConfig" + _oData.getPaymentTermId());
        }
        oCache.remove("AllPaymentTerm");
        oCache.remove("AllPaymentTermIncEDC");    
        oCache.remove("AllPaymentTermEDC"); 
        oCache.remove("FindEDCConfig");
    }
	
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("PaymentTerm" + _sID);
        oCache.remove("FindEDCConfig" + _sID);
        oCache.remove("AllPaymentTerm");
        oCache.remove("AllPaymentTermIncEDC");
        oCache.remove("AllPaymentTermEDC");
        oCache.remove("FindEDCConfig");
    }    

	public void refreshEDCConfig (String _sID, String _sLocID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        oCache.remove("EDCConfig" + _sID + _sLocID);
    }    
}
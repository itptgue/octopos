package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseReceiptDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseReceiptDetailMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseReceiptDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_receipt_detail";
    }

  
    /**
     * purchase_receipt_detail.PURCHASE_RECEIPT_DETAIL_ID
     * @return the column name for the PURCHASE_RECEIPT_DETAIL_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.PURCHASE_RECEIPT_DETAIL_ID constant
     */
    public static String getPurchaseReceiptDetail_PurchaseReceiptDetailId()
    {
        return "purchase_receipt_detail.PURCHASE_RECEIPT_DETAIL_ID";
    }
  
    /**
     * purchase_receipt_detail.PURCHASE_ORDER_DETAIL_ID
     * @return the column name for the PURCHASE_ORDER_DETAIL_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.PURCHASE_ORDER_DETAIL_ID constant
     */
    public static String getPurchaseReceiptDetail_PurchaseOrderDetailId()
    {
        return "purchase_receipt_detail.PURCHASE_ORDER_DETAIL_ID";
    }
  
    /**
     * purchase_receipt_detail.PURCHASE_RECEIPT_ID
     * @return the column name for the PURCHASE_RECEIPT_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.PURCHASE_RECEIPT_ID constant
     */
    public static String getPurchaseReceiptDetail_PurchaseReceiptId()
    {
        return "purchase_receipt_detail.PURCHASE_RECEIPT_ID";
    }
  
    /**
     * purchase_receipt_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.INDEX_NO constant
     */
    public static String getPurchaseReceiptDetail_IndexNo()
    {
        return "purchase_receipt_detail.INDEX_NO";
    }
  
    /**
     * purchase_receipt_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.ITEM_ID constant
     */
    public static String getPurchaseReceiptDetail_ItemId()
    {
        return "purchase_receipt_detail.ITEM_ID";
    }
  
    /**
     * purchase_receipt_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.ITEM_CODE constant
     */
    public static String getPurchaseReceiptDetail_ItemCode()
    {
        return "purchase_receipt_detail.ITEM_CODE";
    }
  
    /**
     * purchase_receipt_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.ITEM_NAME constant
     */
    public static String getPurchaseReceiptDetail_ItemName()
    {
        return "purchase_receipt_detail.ITEM_NAME";
    }
  
    /**
     * purchase_receipt_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.DESCRIPTION constant
     */
    public static String getPurchaseReceiptDetail_Description()
    {
        return "purchase_receipt_detail.DESCRIPTION";
    }
  
    /**
     * purchase_receipt_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.UNIT_ID constant
     */
    public static String getPurchaseReceiptDetail_UnitId()
    {
        return "purchase_receipt_detail.UNIT_ID";
    }
  
    /**
     * purchase_receipt_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.UNIT_CODE constant
     */
    public static String getPurchaseReceiptDetail_UnitCode()
    {
        return "purchase_receipt_detail.UNIT_CODE";
    }
  
    /**
     * purchase_receipt_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.QTY constant
     */
    public static String getPurchaseReceiptDetail_Qty()
    {
        return "purchase_receipt_detail.QTY";
    }
  
    /**
     * purchase_receipt_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.QTY_BASE constant
     */
    public static String getPurchaseReceiptDetail_QtyBase()
    {
        return "purchase_receipt_detail.QTY_BASE";
    }
  
    /**
     * purchase_receipt_detail.RETURNED_QTY
     * @return the column name for the RETURNED_QTY field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.RETURNED_QTY constant
     */
    public static String getPurchaseReceiptDetail_ReturnedQty()
    {
        return "purchase_receipt_detail.RETURNED_QTY";
    }
  
    /**
     * purchase_receipt_detail.BILLED_QTY
     * @return the column name for the BILLED_QTY field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.BILLED_QTY constant
     */
    public static String getPurchaseReceiptDetail_BilledQty()
    {
        return "purchase_receipt_detail.BILLED_QTY";
    }
  
    /**
     * purchase_receipt_detail.ITEM_PRICE_IN_PO
     * @return the column name for the ITEM_PRICE_IN_PO field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.ITEM_PRICE_IN_PO constant
     */
    public static String getPurchaseReceiptDetail_ItemPriceInPo()
    {
        return "purchase_receipt_detail.ITEM_PRICE_IN_PO";
    }
  
    /**
     * purchase_receipt_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.TAX_ID constant
     */
    public static String getPurchaseReceiptDetail_TaxId()
    {
        return "purchase_receipt_detail.TAX_ID";
    }
  
    /**
     * purchase_receipt_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.TAX_AMOUNT constant
     */
    public static String getPurchaseReceiptDetail_TaxAmount()
    {
        return "purchase_receipt_detail.TAX_AMOUNT";
    }
  
    /**
     * purchase_receipt_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.DISCOUNT constant
     */
    public static String getPurchaseReceiptDetail_Discount()
    {
        return "purchase_receipt_detail.DISCOUNT";
    }
  
    /**
     * purchase_receipt_detail.LAST_PURCHASE
     * @return the column name for the LAST_PURCHASE field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.LAST_PURCHASE constant
     */
    public static String getPurchaseReceiptDetail_LastPurchase()
    {
        return "purchase_receipt_detail.LAST_PURCHASE";
    }
  
    /**
     * purchase_receipt_detail.COST_PER_UNIT
     * @return the column name for the COST_PER_UNIT field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.COST_PER_UNIT constant
     */
    public static String getPurchaseReceiptDetail_CostPerUnit()
    {
        return "purchase_receipt_detail.COST_PER_UNIT";
    }
  
    /**
     * purchase_receipt_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.SUB_TOTAL constant
     */
    public static String getPurchaseReceiptDetail_SubTotal()
    {
        return "purchase_receipt_detail.SUB_TOTAL";
    }
  
    /**
     * purchase_receipt_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.PROJECT_ID constant
     */
    public static String getPurchaseReceiptDetail_ProjectId()
    {
        return "purchase_receipt_detail.PROJECT_ID";
    }
  
    /**
     * purchase_receipt_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use PurchaseReceiptDetailPeer.purchase_receipt_detail.DEPARTMENT_ID constant
     */
    public static String getPurchaseReceiptDetail_DepartmentId()
    {
        return "purchase_receipt_detail.DEPARTMENT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_receipt_detail");
        TableMap tMap = dbMap.getTable("purchase_receipt_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_receipt_detail.PURCHASE_RECEIPT_DETAIL_ID", "");
                          tMap.addColumn("purchase_receipt_detail.PURCHASE_ORDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "purchase_receipt_detail.PURCHASE_RECEIPT_ID", "" , "purchase_receipt" ,
                "purchase_receipt_id");
                            tMap.addColumn("purchase_receipt_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("purchase_receipt_detail.ITEM_ID", "");
                          tMap.addColumn("purchase_receipt_detail.ITEM_CODE", "");
                          tMap.addColumn("purchase_receipt_detail.ITEM_NAME", "");
                          tMap.addColumn("purchase_receipt_detail.DESCRIPTION", "");
                          tMap.addColumn("purchase_receipt_detail.UNIT_ID", "");
                          tMap.addColumn("purchase_receipt_detail.UNIT_CODE", "");
                            tMap.addColumn("purchase_receipt_detail.QTY", bd_ZERO);
                            tMap.addColumn("purchase_receipt_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("purchase_receipt_detail.RETURNED_QTY", bd_ZERO);
                            tMap.addColumn("purchase_receipt_detail.BILLED_QTY", bd_ZERO);
                            tMap.addColumn("purchase_receipt_detail.ITEM_PRICE_IN_PO", bd_ZERO);
                          tMap.addColumn("purchase_receipt_detail.TAX_ID", "");
                            tMap.addColumn("purchase_receipt_detail.TAX_AMOUNT", bd_ZERO);
                          tMap.addColumn("purchase_receipt_detail.DISCOUNT", "");
                            tMap.addColumn("purchase_receipt_detail.LAST_PURCHASE", bd_ZERO);
                            tMap.addColumn("purchase_receipt_detail.COST_PER_UNIT", bd_ZERO);
                            tMap.addColumn("purchase_receipt_detail.SUB_TOTAL", bd_ZERO);
                          tMap.addColumn("purchase_receipt_detail.PROJECT_ID", "");
                          tMap.addColumn("purchase_receipt_detail.DEPARTMENT_ID", "");
          }
}

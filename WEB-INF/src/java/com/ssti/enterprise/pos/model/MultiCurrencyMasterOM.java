package com.ssti.enterprise.pos.model;

import java.math.BigDecimal;

public interface MultiCurrencyMasterOM extends TransactionMasterOM
{
	public String getCurrencyId();	
	public BigDecimal getCurrencyRate();
	public BigDecimal getFiscalRate();
}

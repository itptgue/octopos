package com.ssti.enterprise.pos.excel.helper;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * change updateList, call UpdateHistoryTool.createHistory
 * 
 * </pre><br>
 */
public class DiscountLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( DiscountLoader.class );
	
	private StringBuilder m_oRejected = new StringBuilder("\nRejected Discount : \n");

	protected String sHeading = "Promo Code";

	private String m_sUserName;
	private int m_iRejected   = 0;
	private int m_iTotalRows = 0;
	private boolean m_bStart = false;
	private boolean m_bRejected = false;
	
	public DiscountLoader (String _sUserName)
	{
		super.sHeading = this.sHeading;
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	public String getResult()
	{
		
		m_oResult.append (m_oNewData);
		m_oResult.append ("\nTotal New Discount : ");
		m_oResult.append (m_iNewData);
		m_oResult.append ("\n");
		m_oResult.append (m_oUpdated);
		m_oResult.append ("\nTotal Discount Updated : ");
		m_oResult.append (m_iUpdated);
		m_oResult.append ("\n");
		m_oResult.append (m_oRejected);
		m_oResult.append ("\nTotal Discount Rejected : ");
		m_oResult.append (m_iRejected);
		m_oResult.append ("\n");
		m_oResult.append (m_oError);
		m_oResult.append ("\nTotal ERROR : ");
		m_oResult.append (m_iError);

		return m_oResult.toString();
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
    	StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
    	
		log.debug (" updateList : " + getString(_oRow, _iIdx));
    	
		short i = 0;
		String sPromoCode = getString(_oRow, _iIdx + i); i++;		
		String sDiscCode = getString(_oRow, _iIdx + i); i++;
		String sDiscType = getString(_oRow, _iIdx + i); i++;
		String sDescription = getString(_oRow, _iIdx + i); i++;
		
		log.debug (" Discount Code : " + sDiscCode);
		log.debug (" Description : " + sDescription);
		
		if (StringUtil.isNotEmpty(sDiscCode))
		{
			boolean bNewData = false;
			boolean bValid = true;
			
			try
			{	
				Discount oDisc = DiscountTool.getDiscountByCode(sDiscCode);
				Discount oOld = null;
				if (oDisc == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					bNewData = true;
					oDisc = new Discount();
					oDisc.setDiscountId (IDGenerator.generateSysID(false));
					oDisc.setPromoCode(sPromoCode);
					oDisc.setDiscountCode(sDiscCode); 
				}
				else
				{
					oOld = oDisc.copy();
				}
				oDisc.setDescription (sDescription);

				int iType = DiscountTool.getTypeByStr(sDiscType);
				if (iType >= DiscountTool.i_BY_EVENT && iType <= DiscountTool.i_BY_TOTAL || iType == DiscountTool.i_BY_PRICE)
				{
					oDisc.setDiscountType(iType);
				}
				else
				{
					bValid = validateString("Discount Type", "", oEmpty);
				}
				String sLocationCode = getString(_oRow, _iIdx + i); i++;			
				oDisc.setLocationId(LocationTool.getIDByCode(sLocationCode));
				if (StringUtil.isNotEmpty(sLocationCode))
				{
					bValid = validateString("Location Code", oDisc.getLocationId(), oEmpty);				
				}
				
				String sCustTypeCode = getString(_oRow, _iIdx + i); i++;
				oDisc.setCustomerTypeId(CustomerTypeTool.getIDByCode(sCustTypeCode));
				if (StringUtil.isNotEmpty(sCustTypeCode))
				{
					bValid = validateString("Customer Type Code", oDisc.getCustomerTypeId(), oEmpty);				
				}

				String sCustomers = getString(_oRow, _iIdx + i); i++;
				oDisc.setCustomers(sCustomers);

				String sPaymentType = getString(_oRow, _iIdx + i); i++;
				oDisc.setPaymentTypeId(PaymentTypeTool.getIDByCode(sPaymentType));
				
				String sItemSKU = getString(_oRow, _iIdx + i); i++;
				oDisc.setItemSku(sItemSKU);
				
				String sItems = getString(_oRow, _iIdx + i); i++;
				if (StringUtil.isNotEmpty(sItems))
		        {
		            List vCode = StringUtil.toList(sItems,",");
		            for (int j = 0; j < vCode.size(); j++)
		            {
		                String sCode = (String)vCode.get(j);
		                if (ItemTool.getItemByCode(sCode) == null)
		                {
		                	bValid = validateString("Item ", sCode, oEmpty);
		                	//throw new Exception("Item " + sCode + " Not Found");
		                }
		            }
		        }
				oDisc.setItems(sItems);

				String sKategori = getString(_oRow, _iIdx + i); i++;
				oDisc.setKategoriId(KategoriTool.getKategoriIDFromCodeList(sKategori));
				
//				String sItemCode = getString(_oRow, _iIdx + i); i++;
//				Item oItem = ItemTool.getItemByCode(sItemCode);
//				String sItemID = "";
//				if (oItem != null) sItemID = oItem.getItemId();
//				oDisc.setItemId(sItemID);
//				
				String sEvtBegin = getString(_oRow, _iIdx + i); i++;
				String sEvtEnd = getString(_oRow, _iIdx + i); i++;
				
				oDisc.setEventBeginDate(CustomParser.parseDate(sEvtBegin));
				oDisc.setEventEndDate(DateUtil.getEndOfDayIncSec(CustomParser.parseDate(sEvtEnd)));

				String sEvtDisc = getString(_oRow, _iIdx + i); i++;
				oDisc.setEventDiscount(sEvtDisc);
				
				if (iType == DiscountTool.i_BY_EVENT || iType == DiscountTool.i_BY_EVTQTY)
				{
					bValid = validateString("Event Begin", CustomFormatter.formatDate(oDisc.getEventBeginDate()), oEmpty);				
					bValid = validateString("Event End",  CustomFormatter.formatDate(oDisc.getEventEndDate()), oEmpty);									
					bValid = validateString("Event Discount", oDisc.getEventDiscount(), oEmpty);				
				}
				
				oDisc.setQtyAmount1(getBigDecimal(_oRow, _iIdx + i)); i++;
				oDisc.setDiscountValue1(getString(_oRow, _iIdx + i)); i++;
				oDisc.setQtyAmount2(getBigDecimal(_oRow, _iIdx + i)); i++;
				oDisc.setDiscountValue2(getString(_oRow, _iIdx + i)); i++;
				oDisc.setQtyAmount3(getBigDecimal(_oRow, _iIdx + i)); i++;
				oDisc.setDiscountValue3(getString(_oRow, _iIdx + i)); i++;
				oDisc.setQtyAmount4(getBigDecimal(_oRow, _iIdx + i)); i++;
				oDisc.setDiscountValue4(getString(_oRow, _iIdx + i)); i++;
				oDisc.setQtyAmount5(getBigDecimal(_oRow, _iIdx + i)); i++;
				oDisc.setDiscountValue5(getString(_oRow, _iIdx + i)); i++;				
				
				if(bValid)
				{
					oDisc.setUpdateDate(new Date());
					oDisc.setIsTotalDiscount();
					
					DiscountTool.validate(oDisc);					
					oDisc.save();
					if(oOld != null) UpdateHistoryTool.createHistory(oOld, oDisc, m_sUserName, null);
					if(bNewData)
					{
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ".", 6));					
						m_oNewData.append (StringUtil.left(sDiscCode,15));
						m_oNewData.append (StringUtil.left(sDescription,30));
						m_oNewData.append ("\n");
					}
					else
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ".", 6));					
						m_oUpdated.append (StringUtil.left(sDiscCode,15));
						m_oUpdated.append (StringUtil.left(sDescription,30));
						m_oUpdated.append ("\n");
					}
				}
				else
				{
					m_iRejected++;

					m_oRejected.append (StringUtil.left(m_iRejected + ".", 6));					
					m_oRejected.append (StringUtil.left(sDiscCode,15));
					m_oRejected.append (StringUtil.left(sDescription,30));
					if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }					
					m_oRejected.append ("\n");
					
				}
			}
			catch (Exception _oEx)
			{
				m_iError++;
				m_oError.append (StringUtil.left(m_iError + ".", 6));					
				m_oError.append (StringUtil.left(sDiscCode,15));
				m_oError.append (StringUtil.left(sDescription,30));
				m_oError.append (_oEx.getMessage());				
				m_oError.append ("\n");
				
				log.error (_oEx);
				_oEx.printStackTrace();
			}
		}
    }
}

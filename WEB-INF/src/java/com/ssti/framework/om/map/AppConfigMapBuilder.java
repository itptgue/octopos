package com.ssti.framework.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AppConfigMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.AppConfigMapBuilder";

    /**
     * Item
     * @deprecated use AppConfigPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "app_config";
    }

  
    /**
     * app_config.APP_CONFIG_ID
     * @return the column name for the APP_CONFIG_ID field
     * @deprecated use AppConfigPeer.app_config.APP_CONFIG_ID constant
     */
    public static String getAppConfig_AppConfigId()
    {
        return "app_config.APP_CONFIG_ID";
    }
  
    /**
     * app_config.PART
     * @return the column name for the PART field
     * @deprecated use AppConfigPeer.app_config.PART constant
     */
    public static String getAppConfig_Part()
    {
        return "app_config.PART";
    }
  
    /**
     * app_config.NAME
     * @return the column name for the NAME field
     * @deprecated use AppConfigPeer.app_config.NAME constant
     */
    public static String getAppConfig_Name()
    {
        return "app_config.NAME";
    }
  
    /**
     * app_config.VALUE
     * @return the column name for the VALUE field
     * @deprecated use AppConfigPeer.app_config.VALUE constant
     */
    public static String getAppConfig_Value()
    {
        return "app_config.VALUE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("app_config");
        TableMap tMap = dbMap.getTable("app_config");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("app_config.APP_CONFIG_ID", "");
                          tMap.addColumn("app_config.PART", "");
                          tMap.addColumn("app_config.NAME", "");
                          tMap.addColumn("app_config.VALUE", "");
          }
}

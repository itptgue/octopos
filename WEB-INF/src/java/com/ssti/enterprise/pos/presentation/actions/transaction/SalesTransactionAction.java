package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.SalesInvoiceLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.enterprise.pos.tools.sales.VoidTransTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Sales Transaction Action Class 
 * @see SalesTransaction
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesTransactionAction.java,v 1.32 2009/05/04 01:58:24 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesTransactionAction.java,v $
 * 
 * 2015-09-17
 * - change findData add parameter SalesID to filter by salesman
 *
 * </pre><br>
 */
public class SalesTransactionAction extends TransactionSecureAction
{	
	private static final String s_VIEW_PERM   = "View Sales Invoice";
	private static final String s_CREATE_PERM = "Create Sales Invoice";
	private static final String s_CANCEL_PERM = "Cancel Sales Invoice";
	private static final String s_UPDATE_PERM = "Update Sales Invoice";
	
    private static final String[] a_CREATE_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM = {s_SALES_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_UPDATE_PERM = {s_SALES_PERM, s_VIEW_PERM, s_UPDATE_PERM};
    private static final String[] a_VIEW_PERM   = {s_SALES_PERM, s_VIEW_PERM};
    	
	private static Log log = LogFactory.getLog ( SalesTransactionAction.class );
	
    private HttpSession oSes = null;
    private SalesTransaction oTR;
    private List vTD;
    private List vVD;
    private List vPayment;
    
    private static final int i_SAVE   = 1;
    private static final int i_UPDATE = 2;
    private static final int i_CANCEL = 3;
    private static final int i_DELETE = 4;
    private static final int i_SAVEEDIT = 5;
        
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        int iSave = data.getParameters().getInt("save");
        if(iSave == i_SAVE || 
           iSave == i_UPDATE || 
           iSave == i_SAVEEDIT)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
        
        else if (iSave == i_CANCEL || 
                 iSave == i_DELETE)
		{
        	return isAuthorized (data, a_CANCEL_PERM);
		}
    	else
    	{
            return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform (RunData data, Context context)
        throws Exception
    {	
        int iSave = data.getParameters().getInt("save");
    	if (iSave == i_SAVE || 
    		iSave == i_UPDATE) 
    	{
    		doSave (data,context);
    	}
    	else if (iSave == i_CANCEL) 
    	{
    		doCancel (data,context);
    	}
        else if (iSave == i_DELETE) 
        {
            doDelTrans(data,context);
        }    
        
        else if (iSave == i_SAVEEDIT) 
        {
            doSaveEdit(data,context);
        }    
    	else 
    	{
    		doFind (data,context);
    	}
    }
    
    public void doFind (RunData data, Context context)
        throws Exception
    {
    	super.doFind(data);
    	
    	String sCustomerID = data.getParameters().getString("CustomerId", "");
    	String sLocationID = data.getParameters().getString("LocationId", "");
    	String sCashierName = data.getParameters().getString("CashierName", "");
    	String sSalesID = data.getParameters().getString("SalesId"); 
		int iStatus = data.getParameters().getInt("Status");
		int iPaymentStatus = data.getParameters().getInt("PaymentStatus");
		
		LargeSelect vTR = TransactionTool.findData (iCond, sKeywords, dStart, dEnd,
		                                            sCustomerID, sLocationID, iStatus, iPaymentStatus,
													iLimit, iGroupBy, sSalesID, sCashierName, sCurrencyID);
		
		data.getUser().setTemp("findSalesResult", vTR);	
    }
    
	public void doSave (RunData data, Context context)
        throws Exception
    {
		try
		{
			initSession(data);			
			if(data.getParameters().getBoolean("bUpdateDetail", false))
            {
                TransactionTool.updateDetail (vTD,data);
            }
			TransactionTool.setHeaderProperties (oTR, vTD, data);
			prepareTransData(data);
			//update vPayment as new session data
			vPayment = (List) oSes.getAttribute (s_PMT);
			
			//validate invoice payment amount and trans total amount
			InvoicePaymentTool.validatePayment(oTR, vPayment);	
		}
		catch (Exception _oEx)
		{
			//save preparation failed. still ok to retry
			data.setMessage(_oEx.getMessage());
			return;
		}
		
		try 
		{
			TransactionTool.saveData(oTR, vTD, vPayment);		
        	data.setMessage(LocaleTool.getString("si_save_success"));
        	resetSession (data);
        	context.put(s_PRINT_READY, Boolean.valueOf(true));
        }
        catch (Exception _oEx) 
        {
        	//must create new trans, no retry allowed 
			handleError (data, LocaleTool.getString("si_save_failed"), _oEx);
        }
        
        //save void trans
        VoidTransTool.saveVoid(oTR, vVD);
    }
    
	public void doSaveEdit (RunData data, Context context)
	    throws Exception
	{
	    try
	    {
            String sInvNo = data.getParameters().getString("EditInvNo");
            log.debug("Save Edit Invoice: " + sInvNo);
            if (StringUtil.isNotEmpty(sInvNo))
            {
    	        initSession(data);
    	        TransactionTool.updateDetail (vTD,data);
    	        TransactionTool.setHeaderProperties (oTR, vTD, data);
    			//update vPayment as new session data
    			vPayment = (List) oSes.getAttribute (s_PMT);
    
    	        prepareTransData(data);	        	        
                oTR.setInvoiceNo(sInvNo);
                oTR.setStatus(i_PROCESSED);
    
    	        TransactionTool.saveEdit(oTR, vTD, vPayment, data.getUser().getName());      
    	        data.setMessage(LocaleTool.getString("si_save_success") + " (EDIT)");
    	        resetSession (data);
    	        context.put(s_PRINT_READY, Boolean.valueOf(true));
                
                //reset state
                data.getParameters().setString("op","99");
            }
            else
            {
                data.setMessage("No Invoice To Edit");
            }
	    }
	    catch (Exception _oEx) 
	    {
	        //must create new trans, no retry allowed 
	        handleError (data, LocaleTool.getString("si_save_failed"), _oEx);
	    }
	}
    
    public void doCancel (RunData data, Context context)
        throws Exception
    {
		try 
		{
			initSession (data);	
			TransactionTool.cancelSales(oTR, vTD, data.getUser().getName(), null);
        	data.setMessage(LocaleTool.getString("si_cancel_success"));
        	oSes.setAttribute (s_TR, oTR);
			oSes.setAttribute (s_TD, vTD);
        }
        catch (Exception _oEx) 
		{
			handleError (data, LocaleTool.getString("si_cancel_failed"), _oEx);
        }
    }
    
    public void doDelTrans (RunData data, Context context)
        throws Exception
    {
        try 
        {
            initSession(data);
            TransactionTool.deleteTrans(oTR.getTransactionId());
            
            oSes.setAttribute(s_TR, null);           
            oSes.setAttribute(s_TD, null);
            oSes.setAttribute(s_VD, null);
            oSes.setAttribute(s_PMT,null);
        }
        catch (Exception _oEx) 
        {
            handleError (data, LocaleTool.getString("si_cancel_failed"), _oEx);
        }
    }
    
    private void initSession (RunData data) 
    	throws Exception
    {
    	synchronized (this)
    	{
    		oSes = data.getSession();
    		if (oSes.getAttribute (s_TR) == null || oSes.getAttribute (s_TD) == null) 
    		{
    			throw new Exception ("Invalid Transaction Data ");
    		}    
    		oTR = (SalesTransaction) oSes.getAttribute (s_TR);
    		vTD = (List) oSes.getAttribute (s_TD);		
    		if (oSes.getAttribute (s_PMT) != null)
    		{
    			vPayment = (List) oSes.getAttribute (s_PMT);
    		}
    		if (oSes.getAttribute (s_VD) != null)
    		{
    			vVD = (List) oSes.getAttribute (s_VD);		
    		}
    	}
    }
    
    private void prepareTransData (RunData data) 
    	throws Exception
    {
    	//check transaction data 
    	if ( oTR.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 0"); } //-- should never happen 
		if ( vTD.size () < 1) {throw new Exception ("Total Item Purchased < 1"); } //-- should never happen 	
    }

    private void resetSession (RunData data) 
    	throws Exception
    {
    	synchronized (this)
    	{
			oSes.setAttribute(s_TR, oTR);			
			oSes.setAttribute(s_TD, vTD);
			oSes.setAttribute(s_VD, vVD);
			oSes.setAttribute(s_PMT, vPayment);
    	}
    }
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
            
            String sLoader = data.getParameters().getString("loader");
            TransactionLoader oLoader = null;
            if (StringUtil.isNotEmpty(sLoader))
            {
                oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
            }
            else
            {
                oLoader = new SalesInvoiceLoader(data.getUser().getName());
            }
            
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}    
}
package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.DateUtil;

public class PurchaseOrderClose extends PurchaseOrderTransaction
{
	private static final int i_FIND  = 1;
	private static final int i_CLOSE = 2;
	
    public void doBuildTemplate(RunData data, Context context)
    {
   		int iOp = data.getParameters().getInt("op");
    	setContext(data, context);
    	try 
    	{
    		if (iOp == i_FIND)
    		{
    	    	int iCond = data.getParameters().getInt(s_COND);
    	    	String sKeywords = data.getParameters().getString(s_KEY);
    	    	Date dStart = DateUtil.parseStartDate (data.getParameters().getString(s_START_DATE) );
    	    	Date dEnd = DateUtil.parseEndDate (data.getParameters().getString(s_END_DATE));
    	    	Date dExpStart = DateUtil.parseStartDate (data.getParameters().getString("ExpStart") );
    	    	Date dExpEnd = DateUtil.parseEndDate (data.getParameters().getString("ExpEnd"));    	    	
    	    	String sCurrencyID = data.getParameters().getString(s_CURRENCY_ID);	
    	    	String sLocationID = data.getParameters().getString(s_LOCATION_ID);
    	    	String sVendorID = data.getParameters().getString("VendorId", "");
    	    	boolean bAccessAllVendor = data.getParameters().getBoolean("AccessAllVendor");
    			int iStatus = data.getParameters().getInt("Status");

    			List vPO = PurchaseOrderTool.findExpired(
    					iCond,sKeywords,dStart,dEnd,dExpStart,dExpEnd,sVendorID,iStatus);
    			context.put("vPO",vPO);
    		}
    		else if (iOp == i_CLOSE)
    		{
    			List vToClose = new ArrayList(10);
    			int iTotal = data.getParameters().getInt("total");
    			for (int i = 0; i < iTotal; i++)
    			{
    				boolean bClose = data.getParameters().getBoolean("cb" + i);
    				if (bClose)
    				{
    					String sID = data.getParameters().getString("id" + i,"");
    					PurchaseOrder oPO = PurchaseOrderTool.getHeaderByID(sID);
    					if (oPO != null)
    					{
    						PurchaseOrderTool.closePO(oPO,data.getUser().getName());
    					}
    				}
    			}
    			data.setMessage(LocaleTool.getString("po_close_success"));
    		}
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}
package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to InventoryLocation
 */
public abstract class BaseInventoryLocation extends BaseObject
{
    /** The Peer class */
    private static final InventoryLocationPeer peer =
        new InventoryLocationPeer();

        
    /** The value for the inventoryLocationId field */
    private String inventoryLocationId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the currentQty field */
    private BigDecimal currentQty;
      
    /** The value for the startStock field */
    private BigDecimal startStock;
      
    /** The value for the itemCost field */
    private BigDecimal itemCost;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the lastIn field */
    private Date lastIn;
      
    /** The value for the lastOut field */
    private Date lastOut;
  
    
    /**
     * Get the InventoryLocationId
     *
     * @return String
     */
    public String getInventoryLocationId()
    {
        return inventoryLocationId;
    }

                        
    /**
     * Set the value of InventoryLocationId
     *
     * @param v new value
     */
    public void setInventoryLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inventoryLocationId, v))
              {
            this.inventoryLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrentQty
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrentQty()
    {
        return currentQty;
    }

                        
    /**
     * Set the value of CurrentQty
     *
     * @param v new value
     */
    public void setCurrentQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currentQty, v))
              {
            this.currentQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StartStock
     *
     * @return BigDecimal
     */
    public BigDecimal getStartStock()
    {
        return startStock;
    }

                        
    /**
     * Set the value of StartStock
     *
     * @param v new value
     */
    public void setStartStock(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.startStock, v))
              {
            this.startStock = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCost
     *
     * @return BigDecimal
     */
    public BigDecimal getItemCost()
    {
        return itemCost;
    }

                        
    /**
     * Set the value of ItemCost
     *
     * @param v new value
     */
    public void setItemCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCost, v))
              {
            this.itemCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastIn
     *
     * @return Date
     */
    public Date getLastIn()
    {
        return lastIn;
    }

                        
    /**
     * Set the value of LastIn
     *
     * @param v new value
     */
    public void setLastIn(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastIn, v))
              {
            this.lastIn = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastOut
     *
     * @return Date
     */
    public Date getLastOut()
    {
        return lastOut;
    }

                        
    /**
     * Set the value of LastOut
     *
     * @param v new value
     */
    public void setLastOut(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastOut, v))
              {
            this.lastOut = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("InventoryLocationId");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("CurrentQty");
              fieldNames.add("StartStock");
              fieldNames.add("ItemCost");
              fieldNames.add("UpdateDate");
              fieldNames.add("LastIn");
              fieldNames.add("LastOut");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("InventoryLocationId"))
        {
                return getInventoryLocationId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("CurrentQty"))
        {
                return getCurrentQty();
            }
          if (name.equals("StartStock"))
        {
                return getStartStock();
            }
          if (name.equals("ItemCost"))
        {
                return getItemCost();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("LastIn"))
        {
                return getLastIn();
            }
          if (name.equals("LastOut"))
        {
                return getLastOut();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(InventoryLocationPeer.INVENTORY_LOCATION_ID))
        {
                return getInventoryLocationId();
            }
          if (name.equals(InventoryLocationPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(InventoryLocationPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(InventoryLocationPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(InventoryLocationPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(InventoryLocationPeer.CURRENT_QTY))
        {
                return getCurrentQty();
            }
          if (name.equals(InventoryLocationPeer.START_STOCK))
        {
                return getStartStock();
            }
          if (name.equals(InventoryLocationPeer.ITEM_COST))
        {
                return getItemCost();
            }
          if (name.equals(InventoryLocationPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(InventoryLocationPeer.LAST_IN))
        {
                return getLastIn();
            }
          if (name.equals(InventoryLocationPeer.LAST_OUT))
        {
                return getLastOut();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getInventoryLocationId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getLocationName();
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getCurrentQty();
            }
              if (pos == 6)
        {
                return getStartStock();
            }
              if (pos == 7)
        {
                return getItemCost();
            }
              if (pos == 8)
        {
                return getUpdateDate();
            }
              if (pos == 9)
        {
                return getLastIn();
            }
              if (pos == 10)
        {
                return getLastOut();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(InventoryLocationPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        InventoryLocationPeer.doInsert((InventoryLocation) this, con);
                        setNew(false);
                    }
                    else
                    {
                        InventoryLocationPeer.doUpdate((InventoryLocation) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key inventoryLocationId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setInventoryLocationId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setInventoryLocationId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getInventoryLocationId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public InventoryLocation copy() throws TorqueException
    {
        return copyInto(new InventoryLocation());
    }
  
    protected InventoryLocation copyInto(InventoryLocation copyObj) throws TorqueException
    {
          copyObj.setInventoryLocationId(inventoryLocationId);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setCurrentQty(currentQty);
          copyObj.setStartStock(startStock);
          copyObj.setItemCost(itemCost);
          copyObj.setUpdateDate(updateDate);
          copyObj.setLastIn(lastIn);
          copyObj.setLastOut(lastOut);
  
                    copyObj.setInventoryLocationId((String)null);
                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public InventoryLocationPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("InventoryLocation\n");
        str.append("-----------------\n")
           .append("InventoryLocationId  : ")
           .append(getInventoryLocationId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("CurrentQty           : ")
           .append(getCurrentQty())
           .append("\n")
           .append("StartStock           : ")
           .append(getStartStock())
           .append("\n")
           .append("ItemCost             : ")
           .append(getItemCost())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("LastIn               : ")
           .append(getLastIn())
           .append("\n")
           .append("LastOut              : ")
           .append(getLastOut())
           .append("\n")
        ;
        return(str.toString());
    }
}

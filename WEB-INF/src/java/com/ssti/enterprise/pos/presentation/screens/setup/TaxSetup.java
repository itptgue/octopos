package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.TaxTool;

public class TaxSetup extends POSSetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
    		context.put("Taxes", TaxTool.getAllTax());
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}

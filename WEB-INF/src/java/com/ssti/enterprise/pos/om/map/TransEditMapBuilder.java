package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TransEditMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TransEditMapBuilder";

    /**
     * Item
     * @deprecated use TransEditPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "trans_edit";
    }

  
    /**
     * trans_edit.TRANS_EDIT_ID
     * @return the column name for the TRANS_EDIT_ID field
     * @deprecated use TransEditPeer.trans_edit.TRANS_EDIT_ID constant
     */
    public static String getTransEdit_TransEditId()
    {
        return "trans_edit.TRANS_EDIT_ID";
    }
  
    /**
     * trans_edit.TRANS_ID
     * @return the column name for the TRANS_ID field
     * @deprecated use TransEditPeer.trans_edit.TRANS_ID constant
     */
    public static String getTransEdit_TransId()
    {
        return "trans_edit.TRANS_ID";
    }
  
    /**
     * trans_edit.TRANS_NO
     * @return the column name for the TRANS_NO field
     * @deprecated use TransEditPeer.trans_edit.TRANS_NO constant
     */
    public static String getTransEdit_TransNo()
    {
        return "trans_edit.TRANS_NO";
    }
  
    /**
     * trans_edit.TRANS_TYPE
     * @return the column name for the TRANS_TYPE field
     * @deprecated use TransEditPeer.trans_edit.TRANS_TYPE constant
     */
    public static String getTransEdit_TransType()
    {
        return "trans_edit.TRANS_TYPE";
    }
  
    /**
     * trans_edit.EDIT_DATE
     * @return the column name for the EDIT_DATE field
     * @deprecated use TransEditPeer.trans_edit.EDIT_DATE constant
     */
    public static String getTransEdit_EditDate()
    {
        return "trans_edit.EDIT_DATE";
    }
  
    /**
     * trans_edit.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use TransEditPeer.trans_edit.USER_NAME constant
     */
    public static String getTransEdit_UserName()
    {
        return "trans_edit.USER_NAME";
    }
  
    /**
     * trans_edit.FIELD_NAME
     * @return the column name for the FIELD_NAME field
     * @deprecated use TransEditPeer.trans_edit.FIELD_NAME constant
     */
    public static String getTransEdit_FieldName()
    {
        return "trans_edit.FIELD_NAME";
    }
  
    /**
     * trans_edit.OLD_VALUE
     * @return the column name for the OLD_VALUE field
     * @deprecated use TransEditPeer.trans_edit.OLD_VALUE constant
     */
    public static String getTransEdit_OldValue()
    {
        return "trans_edit.OLD_VALUE";
    }
  
    /**
     * trans_edit.NEW_VALUE
     * @return the column name for the NEW_VALUE field
     * @deprecated use TransEditPeer.trans_edit.NEW_VALUE constant
     */
    public static String getTransEdit_NewValue()
    {
        return "trans_edit.NEW_VALUE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("trans_edit");
        TableMap tMap = dbMap.getTable("trans_edit");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("trans_edit.TRANS_EDIT_ID", "");
                          tMap.addColumn("trans_edit.TRANS_ID", "");
                          tMap.addColumn("trans_edit.TRANS_NO", "");
                            tMap.addColumn("trans_edit.TRANS_TYPE", Integer.valueOf(0));
                          tMap.addColumn("trans_edit.EDIT_DATE", new Date());
                          tMap.addColumn("trans_edit.USER_NAME", "");
                          tMap.addColumn("trans_edit.FIELD_NAME", "");
                          tMap.addColumn("trans_edit.OLD_VALUE", "");
                          tMap.addColumn("trans_edit.NEW_VALUE", "");
          }
}

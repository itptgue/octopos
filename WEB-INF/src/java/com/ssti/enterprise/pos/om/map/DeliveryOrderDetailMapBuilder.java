package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DeliveryOrderDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.DeliveryOrderDetailMapBuilder";

    /**
     * Item
     * @deprecated use DeliveryOrderDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "delivery_order_detail";
    }

  
    /**
     * delivery_order_detail.DELIVERY_ORDER_DETAIL_ID
     * @return the column name for the DELIVERY_ORDER_DETAIL_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.DELIVERY_ORDER_DETAIL_ID constant
     */
    public static String getDeliveryOrderDetail_DeliveryOrderDetailId()
    {
        return "delivery_order_detail.DELIVERY_ORDER_DETAIL_ID";
    }
  
    /**
     * delivery_order_detail.SALES_ORDER_DETAIL_ID
     * @return the column name for the SALES_ORDER_DETAIL_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.SALES_ORDER_DETAIL_ID constant
     */
    public static String getDeliveryOrderDetail_SalesOrderDetailId()
    {
        return "delivery_order_detail.SALES_ORDER_DETAIL_ID";
    }
  
    /**
     * delivery_order_detail.DELIVERY_ORDER_ID
     * @return the column name for the DELIVERY_ORDER_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.DELIVERY_ORDER_ID constant
     */
    public static String getDeliveryOrderDetail_DeliveryOrderId()
    {
        return "delivery_order_detail.DELIVERY_ORDER_ID";
    }
  
    /**
     * delivery_order_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.INDEX_NO constant
     */
    public static String getDeliveryOrderDetail_IndexNo()
    {
        return "delivery_order_detail.INDEX_NO";
    }
  
    /**
     * delivery_order_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.ITEM_ID constant
     */
    public static String getDeliveryOrderDetail_ItemId()
    {
        return "delivery_order_detail.ITEM_ID";
    }
  
    /**
     * delivery_order_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.ITEM_CODE constant
     */
    public static String getDeliveryOrderDetail_ItemCode()
    {
        return "delivery_order_detail.ITEM_CODE";
    }
  
    /**
     * delivery_order_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.ITEM_NAME constant
     */
    public static String getDeliveryOrderDetail_ItemName()
    {
        return "delivery_order_detail.ITEM_NAME";
    }
  
    /**
     * delivery_order_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.DESCRIPTION constant
     */
    public static String getDeliveryOrderDetail_Description()
    {
        return "delivery_order_detail.DESCRIPTION";
    }
  
    /**
     * delivery_order_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.UNIT_ID constant
     */
    public static String getDeliveryOrderDetail_UnitId()
    {
        return "delivery_order_detail.UNIT_ID";
    }
  
    /**
     * delivery_order_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.UNIT_CODE constant
     */
    public static String getDeliveryOrderDetail_UnitCode()
    {
        return "delivery_order_detail.UNIT_CODE";
    }
  
    /**
     * delivery_order_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.QTY constant
     */
    public static String getDeliveryOrderDetail_Qty()
    {
        return "delivery_order_detail.QTY";
    }
  
    /**
     * delivery_order_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.QTY_BASE constant
     */
    public static String getDeliveryOrderDetail_QtyBase()
    {
        return "delivery_order_detail.QTY_BASE";
    }
  
    /**
     * delivery_order_detail.RETURNED_QTY
     * @return the column name for the RETURNED_QTY field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.RETURNED_QTY constant
     */
    public static String getDeliveryOrderDetail_ReturnedQty()
    {
        return "delivery_order_detail.RETURNED_QTY";
    }
  
    /**
     * delivery_order_detail.BILLED_QTY
     * @return the column name for the BILLED_QTY field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.BILLED_QTY constant
     */
    public static String getDeliveryOrderDetail_BilledQty()
    {
        return "delivery_order_detail.BILLED_QTY";
    }
  
    /**
     * delivery_order_detail.ITEM_PRICE_IN_SO
     * @return the column name for the ITEM_PRICE_IN_SO field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.ITEM_PRICE_IN_SO constant
     */
    public static String getDeliveryOrderDetail_ItemPriceInSo()
    {
        return "delivery_order_detail.ITEM_PRICE_IN_SO";
    }
  
    /**
     * delivery_order_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.TAX_ID constant
     */
    public static String getDeliveryOrderDetail_TaxId()
    {
        return "delivery_order_detail.TAX_ID";
    }
  
    /**
     * delivery_order_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.TAX_AMOUNT constant
     */
    public static String getDeliveryOrderDetail_TaxAmount()
    {
        return "delivery_order_detail.TAX_AMOUNT";
    }
  
    /**
     * delivery_order_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.DISCOUNT constant
     */
    public static String getDeliveryOrderDetail_Discount()
    {
        return "delivery_order_detail.DISCOUNT";
    }
  
    /**
     * delivery_order_detail.COST_PER_UNIT
     * @return the column name for the COST_PER_UNIT field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.COST_PER_UNIT constant
     */
    public static String getDeliveryOrderDetail_CostPerUnit()
    {
        return "delivery_order_detail.COST_PER_UNIT";
    }
  
    /**
     * delivery_order_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.SUB_TOTAL constant
     */
    public static String getDeliveryOrderDetail_SubTotal()
    {
        return "delivery_order_detail.SUB_TOTAL";
    }
  
    /**
     * delivery_order_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.PROJECT_ID constant
     */
    public static String getDeliveryOrderDetail_ProjectId()
    {
        return "delivery_order_detail.PROJECT_ID";
    }
  
    /**
     * delivery_order_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.DEPARTMENT_ID constant
     */
    public static String getDeliveryOrderDetail_DepartmentId()
    {
        return "delivery_order_detail.DEPARTMENT_ID";
    }
  
    /**
     * delivery_order_detail.EMPLOYEE_ID
     * @return the column name for the EMPLOYEE_ID field
     * @deprecated use DeliveryOrderDetailPeer.delivery_order_detail.EMPLOYEE_ID constant
     */
    public static String getDeliveryOrderDetail_EmployeeId()
    {
        return "delivery_order_detail.EMPLOYEE_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("delivery_order_detail");
        TableMap tMap = dbMap.getTable("delivery_order_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("delivery_order_detail.DELIVERY_ORDER_DETAIL_ID", "");
                          tMap.addColumn("delivery_order_detail.SALES_ORDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "delivery_order_detail.DELIVERY_ORDER_ID", "" , "delivery_order" ,
                "delivery_order_id");
                            tMap.addColumn("delivery_order_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("delivery_order_detail.ITEM_ID", "");
                          tMap.addColumn("delivery_order_detail.ITEM_CODE", "");
                          tMap.addColumn("delivery_order_detail.ITEM_NAME", "");
                          tMap.addColumn("delivery_order_detail.DESCRIPTION", "");
                          tMap.addColumn("delivery_order_detail.UNIT_ID", "");
                          tMap.addColumn("delivery_order_detail.UNIT_CODE", "");
                            tMap.addColumn("delivery_order_detail.QTY", bd_ZERO);
                            tMap.addColumn("delivery_order_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("delivery_order_detail.RETURNED_QTY", bd_ZERO);
                            tMap.addColumn("delivery_order_detail.BILLED_QTY", bd_ZERO);
                            tMap.addColumn("delivery_order_detail.ITEM_PRICE_IN_SO", bd_ZERO);
                          tMap.addColumn("delivery_order_detail.TAX_ID", "");
                            tMap.addColumn("delivery_order_detail.TAX_AMOUNT", bd_ZERO);
                          tMap.addColumn("delivery_order_detail.DISCOUNT", "");
                            tMap.addColumn("delivery_order_detail.COST_PER_UNIT", bd_ZERO);
                            tMap.addColumn("delivery_order_detail.SUB_TOTAL", bd_ZERO);
                          tMap.addColumn("delivery_order_detail.PROJECT_ID", "");
                          tMap.addColumn("delivery_order_detail.DEPARTMENT_ID", "");
                          tMap.addColumn("delivery_order_detail.EMPLOYEE_ID", "");
          }
}

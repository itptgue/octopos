package com.ssti.enterprise.pos.presentation.actions.periodic;

import java.io.File;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.SynchronizationTool;

public class UploadSyncDataAction extends UploadSecureAction
{    
	private static Log log = LogFactory.getLog(UploadSyncDataAction.class);

    public void doUploadfile (RunData data, Context context)
        throws Exception
    {
		try 
		{
			String sFilePath = SynchronizationTool.getStoreDataPath();			
			FileItem oFileItem = data.getParameters().getFileItem("SyncDataFileLocation");
			String sFileName = oFileItem.getName();
			sFileName = sFileName.substring (sFileName.lastIndexOf ('\\'));
			oFileItem.write(new File (sFilePath + sFileName));
			data.setMessage("Daily Synchronization Data Successfully Uploaded To Head Office");			
        }
        catch (Exception _oEx) 
        {
        	String sError = "Upload Sync Data from Store To HO Failed : " + _oEx.getMessage();
        	log.error ( sError, _oEx);
        	data.setMessage (sError);
        }
    }
}
package com.ssti.enterprise.pos.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.PreferenceManager;
import com.ssti.enterprise.pos.om.CompanyData;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Preference;
import com.ssti.enterprise.pos.om.SysConfig;
import com.ssti.enterprise.pos.om.UserPref;
import com.ssti.enterprise.pos.om.UserPrefPeer;
import com.ssti.enterprise.pos.tools.pref.UserPreference;
import com.ssti.framework.db.DBHistoryTool;
import com.ssti.framework.om.CardioConfig;
import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.AppConfigTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.CardioConfigTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.SerializationUtil;
import com.ssti.framework.tools.StringUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for Preference OM
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PreferenceTool.java,v 1.39 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: PreferenceTool.java,v $
 * 
 * 2017-09-24 
 * - add method getLocale(int _iLangID) and getLocale(String _sLang) @see Locale.getLanguage 
 * 
 * 2017-02-23
 * - add method getReportingServer (check in Application.properties reporting.server property value)
 *   if value true then this is Readonly reporting server data, used in AppMenu.vm, StoreMenu.vm to hide
 *   Reporting Menu 
 * - add method getReportingServerURL (check in Application.properties reporting.server.url property value)
 *   value like i.e http://ip:8080/RPT. if this instance act as ReportingServer then value must be empty
 *   if not then   reporting.server.url must point to reporting.server
 * - add method getShowReport check if reporting.server = false then show report, if reporting.server = true
 *   and url empty then this is the reporting server / show report  
 * 
 * 2017-01-07
 * - add method ediInstalled, isEDISystem (to know wether current Retailsoft should act AS EDI system)
 * - add method getBooleanFromConfig safe method to get boolean value from properties configuration
 * - change all CONFIG.getBoolean into getBooleanFromConfig
 * 
 * 2016-12-27
 * - add method getProcessTransferAtReceive use SysConfig ConfirmAtToLoc, if value true then 
 *   ItemTransfer movement will be done when Confirm Receive not when Confirm Sent
 * 
 * 2016-12-11
 * - add method useLocalPOSClient to get whether printer_host in CardioConfig is remote-local / not
 *   see also DeviceConfigTool, PrintServlet, RemotePrintServlet
 * - Used by TransCommonJS.vm, PurchaseCommonJS.vm, InvCommonJS.vm printBarcodeLabel
 * 
 * 2016-10-25
 * - add method getSalesUseCustomPricingUse and getSalesUseCustomPricingTpl in config-schema.xml
 * 
 * 2016-08-01
 * - add getAddressTool, to get AddressTool instance from template
 * 
 * 2016-07-11
 * - add method getLastUpdate to read lastUpdate 
 * 
 * 2016-03-05
 * - add method hasRoleAccess to check whether a String contains role in data.ACL.Roles.Names Set 
 * 
 * 2016-01-10
 * - add method clearCache to clear all cache data
 * </pre><br>
 */
public class PreferenceTool implements AppAttributes
{
	 static final Locale[] a_LOCALE = {Locale.ENGLISH, 
									   Locale.ENGLISH, 
										 new Locale("id"), 
									  	 Locale.FRENCH,
									  	 Locale.GERMAN,
										 Locale.CHINESE, 
										 Locale.JAPANESE};
	
	private static Log log = LogFactory.getLog(PreferenceTool.class);

	//static methods	
	public static Preference getPreference()
    	throws Exception
    {
		return PreferenceManager.getInstance().getPreference();
	}

	public static SysConfig getSysConfig()
	{
		try 
		{
			return SysConfigTool.getSysConfig();			
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
		}
		return new SysConfig();
	}
	
	public static CardioConfig getCardioConfig()
	{
		try 
		{
			return CardioConfigTool.getCardioConfig();			
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
		}
		return new CardioConfig();
	}

	public static void setLocationInContext (RunData data, Context context)
	{
		try 
		{
			boolean bAllLoc = false;
			boolean bAllSite = false;
			String sUserName = "";
			if (data != null)
			{
				sUserName = data.getUser().getName();
				bAllLoc = data.getACL().getPermissions().containsName("Access All Location");
				bAllSite = data.getACL().getPermissions().containsName("Access All Site");
			}
			context.put(SYSLOC, PreferenceTool.getLocationID());
			context.put(USRLOC, PreferenceTool.getLocationID(sUserName));		
			context.put(ACCLOC, Boolean.valueOf(bAllLoc));
			context.put(ACCSITE,Boolean.valueOf(bAllSite));
		} 
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}
	}
		
	public static String getLocationID()
		throws Exception
	{
	    Preference oPref = getPreference ();
	    if (oPref != null) {
			return oPref.getLocationId();
		}
		return "";
	}
	
	/**
	 * get LocationID for specific user name, used in POS
	 * 
	 * @param _sUserName
	 * @return Location ID for specific username 
	 * @throws Exception
	 */
	public static String getLocationID(String _sUserName)
		throws Exception
	{
	    Employee oEmp = EmployeeTool.getEmployeeByUsername(_sUserName);
	    if (oEmp != null)
	    {
	    	if (StringUtil.isNotEmpty(oEmp.getLocationId()))
	    	{
	    		return oEmp.getLocationId();
	    	}
	    }
	    return getLocationID();
	}
	
	public static Location getLocation()
		throws Exception
	{
		return LocationTool.getLocationByID(getLocationID());
	}
	
	public static String getLocationName()
    	throws Exception
    {
        return LocationTool.getLocationNameByID (getLocationID());
	}

	public static String getLocationCode()
    	throws Exception
    {
		return LocationTool.getLocationCodeByID (getLocationID());
	}
	
    public static int getLocationInventoryType()
    	throws Exception
    {
		return LocationTool.getInventoryType(getLocationID(), null);
	}

	public static int getLocationFunction()
    	throws Exception
    {
        Preference oPref = getPreference ();
        if (oPref != null) {
			return oPref.getLocationFunction();
		}
		return -1;
	}	
	
	public static boolean isHO()
		throws Exception
	{
	    if (getLocationFunction() == AppAttributes.i_HEAD_OFFICE) return true;
		return false;
	}	

	public static boolean isStore()
		throws Exception
	{
	    if (getLocationFunction() == AppAttributes.i_STORE) return true;
		return false;
	}	
		
	public static int getCostingMethod()
    	throws Exception
    {
        Preference oPref = getPreference ();
        if (oPref != null) {
			return oPref.getCostingMethod();
		}
		return -1;
	}
		
	public static String getHOIPAddress()
    	throws Exception
    {
        Preference oPref = getPreference ();
        if (oPref != null) {
			return oPref.getHqIpAddress();
		}
		return "";
	}
		
	public static CompanyData getCompany()
    	throws Exception
    {
        return PreferenceManager.getInstance().getCompany();
	}

	public static void refreshPref()
    	throws Exception
    {
		PreferenceManager.getInstance().refreshCache();
    }

	public static Locale getLocale()
	{		
		try
		{
			return a_LOCALE[getPreference().getLanguageId()];
		}
		catch(Exception _oEx)
		{
			return a_LOCALE[0];
		}
	}	

	public static Locale getLocale(RunData data)
	{		
		try
		{
			if(data != null)
			{
				int iLangID = data.getParameters().getInt("langid",-1);
				if(iLangID >= 0)
				{
					return getLocale(iLangID);
				}
			}
			return getLocale();
		}
		catch(Exception _oEx)
		{
			return a_LOCALE[0];
		}
	}	

	public static Locale getLocale(int _iLangID)
	{		
		try
		{
			if(_iLangID >= 0 && _iLangID < a_LOCALE.length)
			{
				return a_LOCALE[_iLangID];
			}
			return getLocale();
		}
		catch(Exception _oEx)
		{
			return a_LOCALE[0];
		}
	}

	public static Locale getLocale(String _sLocale)
	{		
		try
		{
			for(int i = 0; i < a_LOCALE.length; i++)
			{
				if(StringUtil.isNotEmpty(_sLocale) && 
				   StringUtil.containsIgnoreCase(a_LOCALE[i].getLanguage(), _sLocale))
				{
					return a_LOCALE[i];
				}
			}
			return getLocale();
		}
		catch(Exception _oEx)
		{
			return a_LOCALE[0];
		}
	}

	
	public static Date getStartDate()
		throws Exception
	{
		Preference oPref = getPreference();
		if (oPref != null)
		{
			return oPref.getStartDate();
		}
		return null;
	}
	
	//-------------------------------------------------------------------------
	// USER PREF CONFIGURATION
	//-------------------------------------------------------------------------
	
	//user preference related task
	public static UserPref getUserPref(String _sUserName) 
		throws Exception
	{
		Criteria oCrit = new Criteria ();
		oCrit.add (UserPrefPeer.USER_NAME, _sUserName); 
		List vData = UserPrefPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (UserPref) vData.get(0);
		}
		return null;
	}

	public static UserPreference getUserPreference(RunData data) 
		throws Exception
	{
		UserPreference oPref = (UserPreference) data.getSession().getAttribute(AppAttributes.s_USER_PREF);
		if (oPref != null)
		{
			return oPref;
		}	
		else
		{
			UserPref oData = getUserPref (data.getUser().getName());
			if (oData != null)
			{
				try
				{
					oPref = (UserPreference) SerializationUtil.deserialize(oData.getPrefObj());
				}
				catch (Exception _oEx)
				{
					log.error("Failed to retreive User Preference, " + 
							  " probably due to a improper dump/load or bad value in DB, message : " + 
							  _oEx.getMessage());
				}
				data.getSession().setAttribute(AppAttributes.s_USER_PREF, oPref);
				return oPref;
			}
			return null;
		}
	}	
	
	public static void saveUserPref(UserPreference _oPref, RunData data) 
		throws Exception
	{
		UserPref oUserPref = getUserPref(_oPref.getUserName());
		if (oUserPref == null)
		{
			oUserPref = new UserPref();
			oUserPref.setUserPrefId(IDGenerator.generateSysID());
			oUserPref.setUserName(_oPref.getUserName());
		}
		oUserPref.setPrefObj(SerializationUtil.serialize(_oPref));
		oUserPref.save();
		data.getSession().setAttribute(AppAttributes.s_USER_PREF, _oPref);
	}		

	public static void updateReportUserPref(String _sKey, String _sRpt, String _sTitle, boolean _bAdd, RunData data) 
		throws Exception
	{
		UserPreference oPref = getUserPreference(data);
		Map mRpt = null;
		Map mSelRpt = null;
		if(oPref == null)
		{
			oPref = new UserPreference();	
			oPref.setUserName(data.getUser().getName());
		}
		else
		{
			mRpt = oPref.getReportMap();		
		}
		if(mRpt == null)
		{
			mRpt = new HashMap();
		}
		else
		{
			mSelRpt = (Map) mRpt.get(_sKey);
		}
		if (mSelRpt == null)
		{
			mSelRpt = new HashMap();
		}
		if (_bAdd)
		{
			mSelRpt.put(_sRpt, _sTitle);
		}
		else
		{
			mSelRpt.remove(_sRpt);
		}
		mRpt.put(_sKey, mSelRpt);
		oPref.setReportMap(mRpt);
		System.out.println(mRpt);
		saveUserPref(oPref, data);
	}		

	
	//-------------------------------------------------------------------------
	// CARDIOCONFIG CONFIGURATION
	//-------------------------------------------------------------------------	
	//called in VM
	public static String getPrintDetailLinePerPage()
	{
		return getCardioConfig().getPrinterDetailLine();
	}
	
	public static boolean useBarcodePrinter()
	{
		return Boolean.valueOf(getCardioConfig().getBarcodePrinterUse());
	}	
	
	public static boolean useLocalPOSClient()
	{
		if(StringUtil.equalsIgnoreCase(getCardioConfig().getPrinterHost(),"remote-local"))
		{
			return true;
		}
		return false;
	}	
	//-------------------------------------------------------------------------
	// SYSCONFIG CONFIGURATION
	//-------------------------------------------------------------------------	
	public static String getDefaultPOSScreen()
		throws Exception
	{
		if (medicalInstalled() && !getSysConfig().getPosDefaultScreen().startsWith("Med"))
		{
			return "MedicalSalesTrans.vm";
		}
		return getSysConfig().getPosDefaultScreen();
	}
	
	public static String getPOSDetailLinePerPage()
	{
		return getSysConfig().getPosPrinterDetailLine();
	}
	
	public static String getPicturePath()
	{
		return getSysConfig().getPicturePath();
	}
	
	public static boolean validateOpenCashier()
	{
		return getBoolean(getSysConfig().getPosOpenCashierValidation());
	}

	public static boolean validateChangePrice()
	{
		return getBoolean(getSysConfig().getPosChangeValidation());
	}

	public static boolean validateDelete()
	{
		return getBoolean(getSysConfig().getPosDeleteValidation());
	}	

	public static boolean posConfirmQty()
	{
		return getBoolean(getSysConfig().getPosConfirmQty());
	}

	public static boolean posConfirmCost()
	{
		return getBoolean(getSysConfig().getPosConfirmCost());
	}
	
	public static boolean posSaveDirectPrint()
	{
		return getBoolean(getSysConfig().getPosSaveDirectPrint());
	}
	
	public static boolean posDirectAdd()
	{
		return getBoolean(getSysConfig().getPosDirectAdd());
	}

	public static int posRoundingTo()
	{
		return getInt(getSysConfig().getPosRoundingTo());
	}

	public static int posRoundingMode()
	{
		return getInt(getSysConfig().getPosRoundingMode());
	}
	
	public static String posSpcDiscCode()
	{
		return getSysConfig().getPosSpcDiscCode();
	}	
	
	public static boolean syncFullInventory()
	{
		return getBoolean(getSysConfig().getSyncFullInventory());
	}
	
	public static boolean syncAllowStoreReceipt()
	{
		return getBoolean(getSysConfig().getSyncAllowStoreReceipt());
	}	

    public static boolean syncDailyClosing()
    {
        return getBoolean(getSysConfig().getSyncDailyClosing());
    }
    
	public static boolean useLocationCtlTable()
	{
		return getBoolean(getSysConfig().getUseLocationCtltable());
	}	

	public static boolean useCustomerTrPrefix()
	{
		return getBoolean(getSysConfig().getUseCustomerTrPrefix());
	}	

	public static boolean useCustomerTax()
	{
		return getBoolean(getSysConfig().getUseCustomerTax());
	}	

	public static boolean useCustomerLocation()
	{
		return getBoolean(getSysConfig().getUseCustomerLocation());
	}	
	
	public static boolean useVendorTrPrefix()
	{
		return getBoolean(getSysConfig().getUseVendorTrPrefix());
	}	
	
	public static boolean useVendorTax()
	{
		return getBoolean(getSysConfig().getUseVendorTax());
	}	

	public static boolean useCfInOut()
	{
		return getBoolean(getSysConfig().getUseCfInOut());
	}	

	public static boolean usePdt()
	{
		return getBoolean(getSysConfig().getUsePdt());
	}	
	
	public static boolean useBom()
	{
		return getBoolean(getSysConfig().getUseBom());
	}	

	public static boolean getProcessTransferAtReceive()
	{
		return getBoolean(getSysConfig().getConfirmTrfAtToloc());
	}
	
	public static boolean getAllowImportSO()
		throws Exception
	{
		return getBoolean(getSysConfig().getSiAllowImportSo());
	}		

	public static int getDefaultSearchCond()
	{
		return getInt(getSysConfig().getDefaultSearchCond());
	}		
	//NEW AS OF 20/11/2010
	public static int getCustCodeLength()
	{			
		return getInt(getSysConfig().getCustCodeLength());
	}	

	public static int getVendCodeLength()
	{			
		return getInt(getSysConfig().getVendCodeLength());
	}	

	public static int getOthrCodeLength()
	{			
		return getInt(getSysConfig().getOthrCodeLength());
	}	

	public static int getItemCodeLength()
	{			
		return getInt(getSysConfig().getItemCodeLength());
	}
	
	public static int getCustMaxSbMode()
	{			
		return getInt(getSysConfig().getCustMaxSbMode());
	}	
	
	public static int getVendMaxSbMode()
	{			
		return getInt(getSysConfig().getVendMaxSbMode());
	}	

	public static int getPurchSortBy()
	{			
		return getInt(getSysConfig().getPurchSortBy());
	}	
	
	public static int getSalesSortBy()
	{			
		return getInt(getSysConfig().getSalesSortBy());
	}	
	
	public static boolean getPurchInclusiveTax()
	{			
		return getBoolean(getSysConfig().getPurchInclusiveTax());
	}	
	
	public static boolean getSalesInclusiveTax()
	{			
		return getBoolean(getSysConfig().getSalesInclusiveTax());
	}	
	
	public static boolean getPurchMultiCurrency()
	{			
		return getBoolean(getSysConfig().getPurchMultiCurrency());
	}	
	
	public static boolean getSalesMultiCurrency()
	{			
		return getBoolean(getSysConfig().getSalesMultiCurrency());
	}	
	
	public static boolean getPurchPrQtyGtPo()
	{			
		return getBoolean(getSysConfig().getPurchPrQtyGtPo());
	}	

	public static boolean getPurchPrQtyEqualPo()
	{			
		return getBoolean(getSysConfig().getPurchPrQtyEqualPo());
	}	
	
	public static boolean getPurchMergeSameItem()
	{			
		return getBoolean(getSysConfig().getPurchMergeSameItem());
	}	
	
	public static int getPurchMergeItem()
	{			
		String sMerge = getSysConfig().getPurchMergeSameItem();
		if(StringUtil.isEqual(sMerge,"true")) sMerge = "1";
		if(StringUtil.isEqual(sMerge,"false")) sMerge = "0";		
		return Integer.valueOf(sMerge);
	}		
	
	public static boolean getSalesMergeSameItem()
	{			
		return getBoolean(getSysConfig().getSalesMergeSameItem());
	}
	
	public static int getSalesMergeItem()
	{			
		String sMerge = getSysConfig().getSalesMergeSameItem();
		if(StringUtil.isEqual(sMerge,"true")) sMerge = "1";
		if(StringUtil.isEqual(sMerge,"false")) sMerge = "0";		
		return Integer.valueOf(sMerge);
	}

	public static boolean getPurchFobCourier()
	{			
		return getBoolean(getSysConfig().getPurchFobCourier());
	}	
	
	public static boolean getSalesFobCourier()
	{			
		return getBoolean(getSysConfig().getSalesFobCourier());
	}	
	
	public static boolean getPurchFirstlineInsert()
	{			
		return getBoolean(getSysConfig().getPurchFirstlineInsert());
	}	
	
	public static boolean getSalesFirstlineInsert()
	{			
		return getBoolean(getSysConfig().getSalesFirstlineInsert());
	}	

	public static boolean getInvenFirstlineInsert()
	{			
		return getBoolean(getSysConfig().getInvenFirstlineInsert());
	}	
	
	public static int getPurchCommaScale()
	{			
		return getInt(getSysConfig().getPurchCommaScale());
	}

	public static int getSalesCommaScale()
	{			
		return getInt(getSysConfig().getSalesCommaScale());
	}

	public static int getInvenCommaScale()
	{			
		return getInt(getSysConfig().getInvenCommaScale());
	}
	
	public static boolean getPurchLimitByEmp()
	{			
		return getBoolean(getSysConfig().getPurchLimitByEmp());
	}

	public static boolean getPIAllowImportPO()
		throws Exception
	{
		return getBoolean(getSysConfig().getPiAllowImportPo());
	}			
	
	public static int getPiLastPurchMethod()
	{
		if (StringUtil.isNumeric(getSysConfig().getPiLastPurchMethod())) 
		{
			return Integer.valueOf(getSysConfig().getPiLastPurchMethod());
		}
		return 1;
	}
	
	public static boolean getPiUpdateLastPurchase()
		throws Exception
	{
		if (StringUtil.equalsIgnoreCase(getSysConfig().getPiUpdateLastPurchase(), "false") || 
			StringUtil.equalsIgnoreCase(getSysConfig().getPiUpdateLastPurchase(), "true"))
		{
			return Boolean.valueOf(getSysConfig().getPiUpdateLastPurchase());
		}
		return true;
	}	

	public static boolean getPiUpdateSalesPrice()
		throws Exception
	{
		if (StringUtil.equalsIgnoreCase(getSysConfig().getPiUpdateSalesPrice(), "false") || 
			StringUtil.equalsIgnoreCase(getSysConfig().getPiUpdateSalesPrice(), "true"))
		{
			return Boolean.valueOf(getSysConfig().getPiUpdateSalesPrice());
		}
		return false;
	}

	public static boolean getPiUpdatePrCost()
		throws Exception
	{
		if (StringUtil.equalsIgnoreCase(getSysConfig().getPiUpdatePrCost(), "false") || 
			StringUtil.equalsIgnoreCase(getSysConfig().getPiUpdatePrCost(), "true"))
		{
			return Boolean.valueOf(getSysConfig().getPiUpdatePrCost());
		}
		return true;
	}
	
	/**
	 * @@deprecated
	 */	 
	public static boolean purchaseRelatedEmployee()
	{
		return getPurchLimitByEmp();
	}		

	public static boolean getSalesUseSalesman()
	{			
		return getBoolean(getSysConfig().getSalesUseSalesman());
	}

	public static boolean getSalesSalesmanPeritem()
	{			
		return getBoolean(getSysConfig().getSalesSalesmanPeritem());
	}
	
	public static boolean getSalesUseMinPrice()
	{			
		return getBoolean(getSysConfig().getSalesUseMinPrice());
	}
	
	public static boolean getSalesAllowMultipmtDisc()
	{
		return getBoolean(getSysConfig().getSalesAllowMultipmtDisc());		
	}

	public static boolean getSalesCustomPricingUse()
	{
		return getBoolean(getSysConfig().getSalesCustomPricingUse());						
	}

	public static String getSalesCustomPricingTpl()
	{
		return getSysConfig().getSalesCustomPricingTpl();						
	}
	
	private static boolean getBoolean(String s)
	{			
		try
		{
			return Boolean.valueOf(s);
		}
		catch (Exception e){}
		return true;
	}	
	
	private static int getInt(String s)
	{			
		try
		{
			return Integer.valueOf(s);
		}
		catch (Exception e){}
		return 1;
	}	

	//-------------------------------------------------------------------------
	// PROPERTIES CONFIGURATION
	//-------------------------------------------------------------------------
	
	public static Configuration getConfiguration()
		throws Exception
	{
		return CONFIG;
	}

	public static boolean getBooleanFromConfig(String _sKey)
	{
		if(CONFIG.containsKey(_sKey))
		{
			try 
			{
				return CONFIG.getBoolean(_sKey);	
			} 
			catch (Exception e) 
			{
				log.error("Error CFG Key [" + _sKey + "] : " + e.getMessage(), e);
			}
		}
		return false;
	}
	
	public static String getInvoiceScreen()
		throws Exception
	{
		if (getLocationFunction() == AppAttributes.i_HEAD_OFFICE)
		{
			return getDefaultInvScreen();
		}
		return getDefaultPOSScreen();
	}

	public static String getDefaultInvScreen()
		throws Exception
	{
		return getSysConfig().getSiDefaultScreen();
	}		
	
	public static String getCurrentDatabase ()
	{
		return CONFIG.getString("retailsoft.db.name");
	}

	public static boolean prwInstalled()
	{
		return PointRewardTool.isInstalled();	
	}

	public static boolean useSerialNo()
	{
		return false;
	}
	
	public static boolean useBatchNo()
	{
		return true;
	}

	public static boolean syncInstalled()
	{
		return false;
	}	

	public static boolean medicalInstalled()
	{
		return true;
	}	

	public static int medicalType()
	{
		return 1;
	}	

		
	public static boolean pendingPrint(String _sID)
	{
		if (StringUtil.isNotEmpty(_sID))
		{
			return getBooleanFromConfig("application.pendingprint");
		}
		return false;
	}	
	
	public static boolean allowTransImport()
	{
		return getBooleanFromConfig("transaction.allow.excel.import");
	}
	
	public static boolean getMultiDept()
	{
		return false;
	}
		
	public static boolean getReportingServer()
	{
		return getBooleanFromConfig("reporting.server");
	}

	public static String getReportingServerURL()
	{
		String sKey = "reporting.server.url";
		if(CONFIG.containsKey(sKey))
		{
			try 
			{
				return CONFIG.getString("reporting.server.url");	
			} 
			catch (Exception e) 
			{
				log.error("Error CFG Key [" + sKey + "] : " + e.getMessage(), e);
			}
		}
		return "";
	}	

	public static boolean getShowReport()
	{
		if(!getReportingServer()) 
		{
			return true;
		}
		else //use reporting server
		{
			//empty reporting server URL means this is the reporting server
			//not empty reporting server URL means another server act as reporting server
			if(getReportingServer() && StringUtil.isEmpty(getReportingServerURL())) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	//-------------------------------------------------------------------------
	// END CONFIGURATION
	//-------------------------------------------------------------------------
	
	public static String loadSkin(RunData data, InputStream _oInput)    	
		throws Exception
	{
		String sDestination = IOTool.getRealPath(data, "/");
		log.debug("Destination : " + sDestination);
		StringBuilder oResult = new StringBuilder();
		
		byte[] bBuffer = new byte[1024];
		ZipInputStream oZIPInput = null;
		ZipEntry oZipEntry;
		oZIPInput = new ZipInputStream(_oInput);
		
		oZipEntry = oZIPInput.getNextEntry();
		while (oZipEntry != null) 
		{ 
			String sEntryName = oZipEntry.getName();
			oResult.append("ZIP Entry : ").append(sEntryName);
			int n;
			
			FileOutputStream oFileOutput;
			File oNewFile = new File(sDestination + sEntryName);
			
			if(oNewFile.isDirectory())
			{
				oResult.append(" [directory]");
				//File dir = new File(sDestination + sEntryName);
				//dir.mkdirs();
			}                
			else
			{                
				oResult.append(" [file]");
				oFileOutput = new FileOutputStream(sDestination + sEntryName);
				while ((n = oZIPInput.read(bBuffer, 0, 1024)) > -1)
				{
					oFileOutput.write(bBuffer, 0, n);
				}
				oFileOutput.close(); 
				oZIPInput.closeEntry();
			}
			oResult.append("\n");
			oZipEntry = oZIPInput.getNextEntry();			
		}		
		oZIPInput.close();
		return oResult.toString();
    }	
	
	//cashier password
	public static boolean validateRandomPassword(String _sPwd) 
		throws Exception
	{
		Preference oPref = getPreference();
		if (StringUtil.isNotEmpty(_sPwd) && _sPwd.equals(oPref.getCashierPassword()))
		{
			return true;
		}
		return false;
	}
	
	
	//Version
	public static String getDBVersion() 
		throws Exception
	{
		return DBHistoryTool.getLatestVersion();
	}

	public static String getAppVersion() 
	{
		return AppConfigTool.getAppVersion(PreferenceTool.class);
	}

	public static String getCardioVersion() 
	{
		return AppConfigTool.getAppVersion(StringUtil.class);
	}	
	
	//System properties
	public static Properties getSysProps()
	{
		return System.getProperties();
	}
	
	public static String getSysProp(String key)
	{
		return System.getProperty(key);
	}	
	
	//cache
	public static void clearCache(String _sName)
	{
		String[] aCache = {
			"item","find_item","item_field","item_group","account","account_type",
			"inventory_location","master","customer","vendor","fadepr","discount"
		};		
		try
		{
			CacheManager mgr = CacheManager.getInstance();			
			for (int i = 0; i < aCache.length; i++)
			{
				if (StringUtil.isEmpty(_sName) || 
					(StringUtil.isNotEmpty(_sName) && StringUtil.isEqual(_sName, aCache[i])))
				{
					Cache c = mgr.getCache(aCache[i]);	
					if (c != null) c.removeAll();
				}
			}	
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}		
	}
	
	/**
	 * 
	 * @param _sAllowedRoles i.e String "Administrator,Cashier,External"
	 * @param _aclRoleNames get from data.ACL.Roles.Names
	 * @return
	 */
	public static boolean hasRoleAccess(String _sAllowedRoles, Set _aclRoleNames)
	{
		if(StringUtil.isNotEmpty(_sAllowedRoles) && _aclRoleNames != null)
		{
			Iterator iter = _aclRoleNames.iterator();
			while (iter.hasNext())
			{
				String role = (String)iter.next();
				if(_sAllowedRoles.contains(role))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	public static String getLastUpdate(RunData data)	
	{
		try 
		{
			String lastUpdate = IOTool.getRealPath(data, "/lastupdate");
			String last = IOTool.readFileContent(lastUpdate);
			return StringUtil.remove(last, Attributes.s_LINE_SEPARATOR);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return CustomFormatter.formatCustomDateTime(DateUtil.getTodayDate(), "yymmdd");
	}
	
	public static AddressTool getAddressTool()
	{
		return AddressTool.getInstance();
	}
}
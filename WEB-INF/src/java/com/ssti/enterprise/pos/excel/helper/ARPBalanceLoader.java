package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

public class ARPBalanceLoader extends BaseExcelLoader
{
	protected int m_iTotalTrans = 0;
	protected int m_iTransSuccess = 0;
	protected int m_iTransRejected = 0;
	protected int m_iTransError = 0;
	
	protected StringBuilder oReject = null;
	
	String sCode     = "";
    String sName     = "";
    String sInvNo    = "";
    BigDecimal bdAmt = bd_ZERO;
    String sLocation = "";    
    String sAsOf     = "";
    String sDue      = "";
    String sCurrency = "";
    BigDecimal bdRate = bd_ZERO;
    BigDecimal bdFisc = bd_ZERO;
    String sTypeCode = "";
    String sTermCode = "";
    String sCreateBy = "";    
    String sOBItem   = "";
    String sTaxCode	 = "";
    String sRemark   = "";
    String sSalesman = "";
    
    Customer oCust  = null;
    Vendor oVend    = null;
    Location oLoc   = null;
    Currency oCurr  = null;
    Item oItem		= null;
    Date dAsOf 	    = null;
    Date dDue 	    = null;
    String sSalesID = null;
    String sTypeID  = null;
    String sTermID  = null;
    Tax oTax		= null;

	protected void setParam(HSSFRow _oRow, short _iIdx) throws Exception 
	{
		short i = 0;
		//Customer Code	Customer Name	Invoice No	Invoice Amount	As Of	Location Due Date	Currency	Currency Rate	Fiscal Rate	Payment Type	Payment Term	OB Item Salesman

		sCode     = getString(_oRow, _iIdx + i); i++;
		sName     = getString(_oRow, _iIdx + i); i++;
		sInvNo    = getString(_oRow, _iIdx + i); i++;	
		bdAmt     = getBigDecimal(_oRow, _iIdx + i); i++;
		sAsOf     = getString(_oRow, _iIdx + i); i++;
		sLocation = getString(_oRow, _iIdx + i); i++;
		sDue      = getString(_oRow, _iIdx + i); i++;
		sCurrency = getString(_oRow, _iIdx + i); i++;							
		bdRate    = getBigDecimal(_oRow, _iIdx + i); i++;
		bdFisc    = getBigDecimal(_oRow, _iIdx + i); i++;
		sTypeCode = getString(_oRow, _iIdx + i); i++;							
		sTermCode = getString(_oRow, _iIdx + i); i++;							
		sCreateBy = getString(_oRow, _iIdx + i); i++;							
		sOBItem   = getString(_oRow, _iIdx + i); i++;	
		sTaxCode  = getString(_oRow, _iIdx + i); i++;
		sRemark   = getString(_oRow, _iIdx + i); i++;
		sSalesman = getString(_oRow, _iIdx + i); i++;	
		
	    oReject = new StringBuilder();
	}

	protected void setObject() 
		throws Exception
	{
		oCust = CustomerTool.getCustomerByCode(sCode);
		oVend = VendorTool.getVendorByCode(sCode);
	    oLoc  = LocationTool.getLocationByCode(sLocation);
	    dAsOf = CustomParser.parseDate(sAsOf);
	    dDue  = CustomParser.parseDate(sDue);			    
	    oCurr = CurrencyTool.getCurrencyByCode(sCurrency);
	    oItem = ItemTool.getItemByCode(sOBItem);
	    oTax  = TaxTool.getByCode(sTaxCode);
	    
	    sSalesID = EmployeeTool.getIDByName(sSalesman);
	    sTypeID = PaymentTypeTool.getIDByCode(sTypeCode);
	    sTermID = PaymentTermTool.getIDByCode(sTermCode);
	    
	    if (oLoc == null) oLoc = PreferenceTool.getLocation();
	    if (dAsOf == null) dAsOf = PreferenceTool.getStartDate();
	    if (oItem == null) oItem = ItemTool.getItemByCode("OB");
	    if (StringUtil.isEmpty(sCreateBy)) sCreateBy = m_sUserName;
	}
    
	protected boolean validate(boolean bValid)
	{
		if (oLoc  == null) {bValid = false; oReject.append("Location ").append(sLocation).append(" Not Found\n");}
		if (dAsOf == null) {bValid = false; oReject.append("As Of ").append(sAsOf).append(" Empty/Not Parsable\n");}
		if (oCurr == null) {bValid = false; oReject.append("Currency ").append(sCurrency).append(" Not Found\n");}
		if (oItem == null) {bValid = false; oReject.append("OB Item ").append(sOBItem).append(" Not Found\n");}
		if (oTax  == null) {bValid = false; oReject.append("Tax ").append(sTaxCode).append(" Not Found\n");}
		return bValid;
	}
	
	protected void logSuccess()
	{
    	m_iUpdated++;
    	m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
    	m_oUpdated.append (StringUtil.left(sCode,20));
    	m_oUpdated.append (StringUtil.left(sName,30));
    	m_oUpdated.append (StringUtil.left(CustomFormatter.formatAccounting(bdAmt),20));                	
    	m_oUpdated.append (StringUtil.left(sAsOf,20));                	                	
    	m_oUpdated.append ("\n");
	}
	
	protected void logError(Exception _oEx)
	{
    	m_iError++;
    	m_oError.append (StringUtil.left(m_iError + ". ", 5));										
    	m_oError.append (StringUtil.left(sCode,20));
    	m_oError.append (StringUtil.left(sName,30));
    	m_oError.append (StringUtil.left(CustomFormatter.formatAccounting(bdAmt),20));                	
    	m_oError.append (_oEx.getMessage());                	                	
    	m_oError.append ("\n");
    	
    	if (log.isDebugEnabled())
    	{
    		_oEx.printStackTrace();
    	}
	}
	
	protected void logReject()
	{
	    m_iRejected++;
	    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
	    m_oRejected.append (StringUtil.left(sCode,20));
	    m_oRejected.append (StringUtil.left(sName,30));
	    m_oRejected.append (StringUtil.left(CustomFormatter.formatAccounting(bdAmt),20));                	
    	m_oRejected.append (StringUtil.left(sAsOf,20));                	                	
	    m_oRejected.append (oReject);
	    m_oRejected.append ("\n");
	}
	
	@Override
	protected void updateList(HSSFRow _oRow, short _iIdx) throws Exception 
	{		
	}
}

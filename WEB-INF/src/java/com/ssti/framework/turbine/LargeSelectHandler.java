package com.ssti.framework.turbine;

import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * @see org.apache.torque.util.LargeSelect
 * Query large dataset per page
 * <br>
 * 
 * @author  $Author: albert $ <br>
 * @version $Id: LargeSelectHandler.java,v 1.6 2007/02/23 14:14:11 albert Exp $ <br>
 * 
 * <pre>
 * $Log: LargeSelectHandler.java,v $
 * Revision 1.6  2007/02/23 14:14:11  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class LargeSelectHandler 
{
	/**
	 * handle large select parameter 
	 * @param data
	 * @param context
	 * @param _sTempName
	 * @param _sContextName
	 * @throws Exception
	 */
    public static void handleLargeSelectParameter ( RunData data, 
    											  	Context context, 
    											  	String _sTempName, 
    											    String _sContextName)
    	throws Exception
    {
    	String op = data.getParameters().getString("op", "");
    	
    	if (StringUtil.isEmpty(op))
    	{
    		data.getUser().setTemp(_sTempName, null);
    	}
    	else 
    	{    			
			LargeSelect oLargeData = (LargeSelect) data.getUser().getTemp(_sTempName);			

	    	int iPage = data.getParameters().getInt("page", 1);
			
    		if (op.equals("next"))
    		{
				if (oLargeData != null) {
					context.put (_sContextName, oLargeData.getNextResults());
				}
    		}
    		else if (op.equals("prev"))
    		{
				if (oLargeData != null) {
					context.put (_sContextName, oLargeData.getPreviousResults());
				}
    		}
        	
    		else if (op.equals("page"))
    		{
				if (oLargeData != null) {
					context.put (_sContextName, oLargeData.getPage(iPage));
				}
    		}
			context.put ("LargeData", oLargeData);    			
    	}
	}
}
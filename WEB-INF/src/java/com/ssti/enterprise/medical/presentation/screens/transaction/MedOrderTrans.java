package com.ssti.enterprise.medical.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.MedOrder;
import com.ssti.enterprise.medical.om.MedOrderDetail;
import com.ssti.enterprise.medical.om.MedicineSales;
import com.ssti.enterprise.medical.om.MedicineSalesDetail;
import com.ssti.enterprise.medical.om.PresPowderDetail;
import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.medical.om.PrescriptionDetail;
import com.ssti.enterprise.medical.tools.DoctorTool;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedOrderTool;
import com.ssti.enterprise.medical.tools.MedicalAttributes;
import com.ssti.enterprise.medical.tools.MedicalSalesTool;
import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.medical.tools.PrescriptionTool;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.presentation.screens.transaction.SalesSecureScreen;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.pwp.PWPTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.IDGenerator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-06-13
 * - initial Version
 *
 * </pre><br>
 */
public class MedOrderTrans extends SalesSecureScreen implements MedicalAttributes
{
	public static final String s_MSO = "mso";
	public static final String s_MSD = "msoDet";

	protected static final Log log = LogFactory.getLog(MedOrderTrans.class);	
	protected static final String[] a_PERM = {"View Sales Order"};
	
    //start screen method
    protected static final int i_OP_ADD_DETAIL  = 1;
    protected static final int i_OP_DEL_DETAIL  = 2;
	protected static final int i_OP_NEW_TRANS   = 3;
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
	
	protected static final int i_OP_REFRESH_DETAIL = 10;
	protected static final int i_OP_REFRESH_MASTER = 11; //only when no promo exists if promo then should refresh detail
	
	protected static final int i_OP_CONFIRM_DELIVERY = 20; //set status to 4 DELIVERED
	protected static final int i_OP_TO_INVOICE = 21; //set copy data to pending SI 
	
	protected String m_sClientIP = "";    
    protected MedOrder oTR = null;    
    protected List vTD = null;
    
    protected HttpSession oSes;
    protected boolean b_UPDATEABLE_DETAIL = false; 
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	m_sClientIP = data.getRemoteAddr();
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {	    	
    	super.doBuildTemplate(data, context);
    	
    	int iOp = data.getParameters().getInt("op");
		b_UPDATEABLE_DETAIL = data.getParameters().getBoolean("bUpdateDetail", false);
		initSession (data);
		
		try 
		{	    	
			if (iOp == i_OP_ADD_DETAIL && !b_REFRESH) {
				addDetailData (data);
			}	
			else if (iOp == i_OP_DEL_DETAIL && !b_REFRESH) {
				delDetailData (data);
			}	
			else if ( iOp == i_OP_NEW_TRANS ) {
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS ) {
				getData (data);
			}
			else if (iOp == i_OP_REFRESH_DETAIL && !b_REFRESH) {
				refreshDetail (data);
			}	
			else if (iOp == i_OP_REFRESH_MASTER && !b_REFRESH) {
				refreshMaster (data);
			}
			else if (iOp == i_OP_CONFIRM_DELIVERY && !b_REFRESH) {				
				confirmDelivery(data);
			}
			else if (iOp == i_OP_TO_INVOICE && !b_REFRESH) {
				toInvoice(data);
			}			
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage("ERROR : " + _oEx.getMessage());
		}

		context.put(s_MSO, oSes.getAttribute (s_MSO));
		context.put(s_MSD, oSes.getAttribute (s_MSD));    	
    	
    	//set Invoice related variable in context
    	setInvoiceContext(data, context);
    	
    	//set med related variable & tools in context
    	MedConfigTool.setContextTool(context);
    }

	protected void initSession ( RunData data )
	{	
		oSes = data.getSession();
		if (oSes.getAttribute (s_MSO) == null) 
		{
			createNewTrans(data);
		}
		oTR = (MedOrder) oSes.getAttribute (s_MSO);
		vTD = (List) oSes.getAttribute (s_MSD);	
	}

	/**
     * refresh detail and clear discount to reflect payment type change
     * 
     * @param data
     * @throws Exception
     */
	private void refreshMaster(RunData data)
		throws Exception
	{
        if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail(vTD, data);
        data.getParameters().setProperties(oTR);	
						
        MedOrderTool.setHeaderProperties(oTR, vTD, data);
		oSes.setAttribute (s_TD, vTD); 
		oSes.setAttribute (s_TR, oTR);     
	}
	
    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
        data.getParameters().setProperties (oTR);	
        
        int iItemType = data.getParameters().getInt("ItemType");
        if (iItemType == MedOrderTool.i_PRS_TYPE)
        {
        	//set header with null data so if pres discount exist not overriden
			String sPRSID = data.getParameters().getString("ItemId");
        	addPrescription(sPRSID); 
        	MedOrderTool.setHeaderProperties (oTR, vTD, null);
        }
        else
        {
        	addItem(data);
        	MedOrderTool.setHeaderProperties (oTR, vTD, data);
        }
		oSes.setAttribute (s_MSD, vTD); 
		oSes.setAttribute (s_MSO, oTR);     
    }
    
    void addItem(RunData data)
    	throws Exception
    {
    	String sItemID = data.getParameters().getString("ItemId");
    	int iTxType = data.getParameters().getInt("TxType", i_NON_PRES);
    	if (StringUtil.isNotEmpty(sItemID))
    	{
			Item oItem = ItemTool.getItemByID(sItemID);
			if (oItem != null)
			{
				MedOrderDetail oTD = new MedOrderDetail();
				data.getParameters().setProperties (oTD);	
				String sItemName = data.getParameters().getString("ItemName");
				if (!StringUtil.isEqual(sItemName,oItem.getItemName()))
				{
					oTD.setDescription(sItemName);
				}
				
				oTD.setItemCode (oItem.getItemCode());
				oTD.setItemName(oItem.getItemName());
				//oTD.setDescription(oItem.getDescription());

				//oTD.setDescription(oItem.getDescription());
				oTD.setTxType(iTxType);
				oTD.setQoh(data.getParameters().getBigDecimal("currentQty",bd_ZERO));
				
				if (oTD.getQtyBase() == null || oTD.getQtyBase().equals(bd_ZERO)) 
				{
					oTD.setQtyBase (UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), oTD.getQty()));
				}
				
				if(iTxType == i_PRES_NORMAL) //add prescription directly from POS not via Prescription Module
				{
					oTD.setPrescriptionDetailId(IDGenerator.generateSysID()); //generate prescription_detail_id					
				}
				if(iTxType == i_PRES_POWDER) //add powder directly from POS not via Prescription Module
				{					
					if(PowderTool.isPowderItem(oTD.getItemId()))
					{
						oTD.setPrescriptionDetailId(IDGenerator.generateSysID()); //generate presc_detail_id 
						oTD.setParentId(""); //powder item has empty parent
						oTD.setRfee( Double.toString(PowderTool.calculateRfee(oTD.getItemId(), oTD.getQty().doubleValue())) );
						oTD.setPackaging ( Double.toString(PowderTool.calculatePack(oTD.getItemId(), oTD.getQty().doubleValue())) );
						oTD.setIndexNo(countPowder() + 1);
					}
					else
					{
						//get nearest Powder Item
						oTD.setParentId(getClosestPowderItem());
						if(StringUtil.empty(oTD.getParentId()))
						{
							throw new Exception(LocaleTool.getString("powder_pack_item") + " " + LocaleTool.getString("may_not_empty"));
						}
						oTD.setPrescriptionDetailId(oTD.getParentId()); //use closest powder_item presc_detail_id
						oTD.setIndexNo(countPowder());
					}
				}
				
				//validate price first before add into detail
				TransactionTool.validatePrice(oTD.getItemId(), oTD.getItemPrice().doubleValue(), 
					oTD.getDiscount(), oTR.getLocationId(), oTR.getCustomerId());
				
				System.out.println(oTD);
				
				if (!isExist (oTD, data)) 
				{
				    if (b_SALES_FIRST_LAST)
				    {
					    vTD.add (0, oTD);
					}
					else
					{
					    vTD.add (oTD);
					}
				}   
			}
    	}
    }
    
    void addPrescription(String _sID)
		throws Exception
	{		 
		if (StringUtil.isNotEmpty(_sID))
		{
	    	Prescription oPRS = PrescriptionTool.getHeaderByID(_sID, null);
			if (oPRS != null)
			{
				oTR.setPatientName(oPRS.getPatientName());
				oTR.setDoctorName(DoctorTool.getDoctorNameByID(oPRS.getDoctorId()));
				List vPRSD = PrescriptionTool.getDetailsByID(_sID, null);
				for (int i = 0; i < vPRSD.size(); i++)
				{
					PrescriptionDetail oPRSD = (PrescriptionDetail) vPRSD.get(i);				
					List vPWDD = oPRSD.getPresPowderDetails();
					if (vPWDD != null && vPWDD.size() > 0)
					{
						for (int j = 0; j < vPWDD.size(); j++)
						{
							MedOrderDetail oTD = new MedOrderDetail();
							PresPowderDetail oPWDD = (PresPowderDetail) vPWDD.get(j);
							
							BeanUtil.setBean2BeanProperties(oPWDD, oTD);
							
							oTD.setDescription(oPRSD.getDescription());
							oTD.setItemType(MedOrderTool.i_PRS_TYPE);
							oTD.setQty(oPWDD.getBilledQty());
							oTD.setQtyBase(oPWDD.getBilledQty());
							oTD.setItemCost (bd_ZERO); //calculate later
							oTD.setTxType(i_PRES_POWDER);
							
							//TAX
							Tax oTax = TaxTool.getDefaultSalesTax();
							oTD.setTaxId(oTax.getTaxId());
							oTD.setTaxAmount(oTax.getAmount());
							
							if (j == (vPWDD.size() - 1))
							{
								oTD.setRfee(oPRSD.getRfee());
								oTD.setPackaging(oPRSD.getPackaging());
							}
							
							log.debug("PWDD TYPE : " + oTD);
							if (b_SALES_FIRST_LAST)
							{
								vTD.add (0, oTD);
							}
							else
							{
								vTD.add (oTD);
							}
						}
					}
					else
					{
						MedOrderDetail oTD = new MedOrderDetail();
	
						BeanUtil.setBean2BeanProperties(oPRSD, oTD);
						oTD.setItemType(MedOrderTool.i_PRS_TYPE);
						
						oTD.setItemName (oPRSD.getItemName()); //previously oPRSD.getDesc
						oTD.setItemCost (bd_ZERO); //calculate later						
						oTD.setTxType(i_PRES_NORMAL);
						
						//TAX
						Tax oTax = TaxTool.getDefaultSalesTax();
						oTD.setTaxId(oTax.getTaxId());
						oTD.setTaxAmount(oTax.getAmount());
						
						log.debug("PRS TYPE : " + oTD);
						if (b_SALES_FIRST_LAST)
						{
							vTD.add (0, oTD);
						}
						else
						{
							vTD.add (oTD);
						}
					}
				}
				//set discount
				setDiscountFromPres(oPRS, true);
			}
		}
	}
    
    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	int iNo = data.getParameters().getInt("No") - 1;
		if (iNo >= 0 && iNo < vTD.size()) 
		{
			MedOrderDetail oTD = (MedOrderDetail) vTD.get(iNo);
			String sPresID = oTD.getPrescriptionId();
			oTR.setTotalExpense(bd_ZERO);
			if(StringUtil.isNotEmpty(sPresID))
			{
				removePres(sPresID);
				MedOrderTool.setHeaderProperties (oTR, vTD, null);
			}
			else
			{
				if(PowderTool.isPowderItem(oTD.getItemId()) && StringUtil.isNotEmpty(oTD.getPrescriptionDetailId()))
				{
					removePowder(oTD); //remove MedPOS powder input via POS
				}
				else
				{
					vTD.remove (iNo);
				}								
				MedOrderTool.setHeaderProperties (oTR, vTD, data);
			}
			oSes.setAttribute(s_MSD, vTD);
			oSes.setAttribute(s_MSO, oTR); 
		}    	
    }
    
    protected void removePres(String sPresID) throws Exception
    {
		Iterator iter = vTD.iterator();
		while(iter.hasNext())
		{
			MedOrderDetail oMTD = (MedOrderDetail)iter.next();										
			if (StringUtil.isEqual(sPresID,oMTD.getPrescriptionId()))
			{
				iter.remove();
			}
		}
		Prescription oPRS = PrescriptionTool.getHeaderByID(sPresID, null);
		setDiscountFromPres(oPRS, false);
		oTR.setPatientName("");
		oTR.setDoctorName("");
    }
    
    /**
     * powder from MedPOS prescription input
     * 
     * @param sPresID
     * @throws Exception
     */
    protected void removePowder(MedOrderDetail _oTD) throws Exception
    {
		Iterator iter = vTD.iterator();
		while(iter.hasNext())
		{
			MedOrderDetail oMTD = (MedOrderDetail)iter.next();										
			if (StringUtil.isEqual(oMTD.getParentId(),_oTD.getPrescriptionDetailId()) || 
				StringUtil.equals(oMTD.getPrescriptionDetailId(), _oTD.getPrescriptionDetailId()))
			{
				iter.remove();
			}			
		}
    }     
    
    protected void setDiscountFromPres(Prescription _oPRS, boolean _bAdd)
    {
		//set discount
		double dPresDisc = _oPRS.getTotalDiscount().doubleValue();
		if (dPresDisc > 0)
		{
			if (!_bAdd) dPresDisc = dPresDisc * -1;
			String sDiscPct = oTR.getTotalDiscountPct();	
			if (!StringUtil.contains(sDiscPct, "%"))
			{
				double dDiscPct= Double.parseDouble(sDiscPct);
				dDiscPct = dDiscPct + dPresDisc;
				oTR.setTotalDiscountPct(Double.toString(dDiscPct));
				oTR.setTotalDiscount(new BigDecimal(dDiscPct));
			}
		}
    }
    
    protected void createNewTrans ( RunData data ) 
    {
    	synchronized (this) 
    	{
        	oTR = new MedOrder();
        	vTD = new ArrayList();
        	
        	//initialize inclusive tax
        	oTR.setIsInclusiveTax(b_SALES_TAX_INCLUSIVE);
        	
        	oSes.setAttribute (s_MSO,  oTR);
    		oSes.setAttribute (s_MSD,  vTD);    		
		}
    }
    
    protected boolean isExist (MedOrderDetail _oTD, RunData data) 
    	throws Exception
    {
    	boolean bIsExist = true; 
    	String sCustomerID = data.getParameters().getString("CustomerId","");
    	for (int i = 0; i < vTD.size(); i++) 
    	{
    		MedOrderDetail oExistTD = (MedOrderDetail) vTD.get (i);
    		if ( oExistTD.getItemId().equals(_oTD.getItemId()) && 
    		     oExistTD.getUnitId().equals(_oTD.getUnitId()) &&
    		     oExistTD.getTaxId().equals(_oTD.getTaxId()) &&
     		    (oExistTD.getDiscount().equals(_oTD.getDiscount()) || oExistTD.isQtyDiscount() || oExistTD.isPwpDiscount() || oExistTD.isMultiUnit()) &&     		    
     		    !oExistTD.isSpecialDiscByAmount() &&
 				(oExistTD.getItemPrice().intValue() == _oTD.getItemPrice().intValue()) &&    		     
    		     oExistTD.getPrescriptionId().equals(_oTD.getPrescriptionId()) &&
    		     oExistTD.getPrescriptionDetailId().equals(_oTD.getPrescriptionDetailId()) &&
				(oExistTD.getItemPrice().doubleValue() == _oTD.getItemPrice().doubleValue()) ) 
    		{
       		    if(b_SALES_MERGE_DETAIL || oExistTD.isPwpDiscount() || oExistTD.isMultiUnit())
    		    {
				    mergeData (oExistTD, _oTD);
				    //apply discount setup to the existing detail data
				    //if discount on sales trans was 0 then apply Disc from discount setup
				    if(oExistTD.getDiscount().equals("0"))
				    {
    			        applyDiscount (oTR, oExistTD, true); //apply discount after merge
    			    }
				}
				else
				{
	    	        //apply discount to the new detail data
	    	        //if discount on sales trans was 0 then apply Disc from discount setup
	    	        if(_oTD.getDiscount().equals("0"))
	    	        {
	    	            applyDiscount (oTR, _oTD, false);
	    	        }				
					bIsExist = false;
				}
				return bIsExist;    			
    		}		
    	}
    	
    	//if there is no same item on list or  no item in list exist
    	bIsExist = false;
    	//apply discount to the new detail data
    	//if discount on sales trans was 0 then apply Disc from discount setup
    	if(_oTD.getDiscount().equals("0"))
    	{
    	    applyDiscount (oTR, _oTD, false);
    	}
    	return bIsExist;
    } 

    /**
     * 
     * @param _oTR
     * @param _oTD
     * @throws Exception
     */
    protected void applyDiscount(MedOrder _oTR, MedOrderDetail _oTD, boolean _bMerge)
        throws Exception
    {
        String sLocID = _oTR.getLocationId();
        String sItemID = _oTD.getItemId();
        String sCustID = _oTR.getCustomerId();
        String sPTypeID = _oTR.getPaymentTypeId();
        Date dDate = _oTR.getTransactionDate();     
        
        log.debug("****** FIND DISCOUNT **********");
        log.debug("Loc   : " + LocationTool.getLocationCodeByID(sLocID));
        log.debug("Cust  : " + CustomerTool.getCodeByID(sCustID));
        log.debug("Item  : " + ItemTool.getItemCodeByID(sItemID));
        
        Discount oDisc =  DiscountTool.findDiscount(sLocID, sItemID, sCustID, sPTypeID, dDate);
        boolean bPWP = false;
        boolean bSKU = false;
        boolean bMUL = false;
        
        if (oDisc != null)
        {
            if (StringUtil.isNotEmpty(oDisc.getItemSku()))
            {
                bSKU = true;
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_PWP)
            {
                bPWP = true; 
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_MULTI)
            {
                bPWP = true; 
            }            
            if (bSKU)
            {
                applySKUDiscount(_oTD,oDisc,_bMerge);   
            }
            else if(bPWP)
            {
                applyPWPDiscount(_oTD,oDisc);
            }
            else if(bMUL)
            {
                applyMULDiscount(_oTD,oDisc);
            }            
            else
            {
                applySTDDiscount(_oTD,oDisc);
            }
            _oTD.setDiscountId(oDisc.getDiscountId());
        }
    }    
    
    private void applySKUDiscount(MedOrderDetail _oTD,
                                  Discount _oDisc,
                                  boolean _bMerge)
        throws Exception
    {
        log.debug ("****** SKU DISCOUNT **********");
        List vSKU = new ArrayList(vTD.size());
        double dTotalSKUQty = 0;
        for (int i = 0; i < vTD.size(); i++) 
        {
            MedOrderDetail oExistTD = (MedOrderDetail) vTD.get (i);
            Item oItem = ItemTool.getItemByID(oExistTD.getItemId());
            if (oItem.getItemSku().equals(_oDisc.getItemSku()))
            {
                vSKU.add(oExistTD);
                dTotalSKUQty += oExistTD.getQtyBase().doubleValue();
            }
        }
        if (!_bMerge)
        {
            vSKU.add(_oTD);
            dTotalSKUQty += _oTD.getQty().doubleValue();
        }
        for (int i = 0; i < vSKU.size(); i++) 
        {
            MedOrderDetail oExistTD = (MedOrderDetail) vSKU.get (i);
            oExistTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, dTotalSKUQty));
            recalculateTD(oExistTD);
        }
    }
    
    private void applyPWPDiscount(MedOrderDetail _oTD,Discount _oDisc)
        throws Exception
    {
        log.debug ("****** PWP DISCOUNT **********");
        PWPTool.updateGetDiscAmount(_oDisc, vTD, _oTD);
        recalculateTD(_oTD);
    }

    private void applySTDDiscount(MedOrderDetail _oTD,Discount _oDisc)
        throws Exception
    {
        log.debug ("****** NORMAL DISCOUNT **********");        
        _oTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, _oTD.getQty().doubleValue()));
        recalculateTD(_oTD);
    }

    /**
     * apply multi unit discount validate with existing buy qty
     * 
     * @param _oTD
     * @throws Exception
     */
    private void applyMULDiscount(MedOrderDetail _oTD,Discount _oDisc)
    	throws Exception
    {    	    	
    	log.debug ("****** MULTI UNIT DISCOUNT **********");
		if(_oDisc != null)		
		{			
			int iBuy = _oDisc.getBuyQty().intValue();
			int iGet = _oDisc.getGetQty().intValue();
			int iPack = iBuy + iGet;
			int iQty = _oTD.getQty().intValue(); 
			
			int iAvail = (int) (iQty / iPack);
			int iDisc = iAvail * iGet;
			double dAmt = Calculator.mul(_oTD.getItemPrice(),iDisc);			
			
			_oTD.setDiscount(new BigDecimal(dAmt).toString());
			_oTD.setDiscountId(_oDisc.getDiscountId());	
			recalculateTD(_oTD);
		}
	}
	
    private void recalculateTD(MedOrderDetail _oTD)
        throws Exception
    {        
        double dSubTotal =  _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();            
        double dSubTotalDisc = 0;
        dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), new Double(dSubTotal));            
        dSubTotal = dSubTotal - dSubTotalDisc;
        double dSubTotalTax = (_oTD.getTaxAmount().doubleValue() / 100) * dSubTotal;
        
        //dSubTotal = dSubTotal;//+ dSubTotalTax;
        _oTD.setSubTotalDisc (new BigDecimal (dSubTotalDisc));
        _oTD.setSubTotalTax  (new BigDecimal (dSubTotalTax));
        _oTD.setSubTotal     (new BigDecimal (dSubTotal));
        
        log.debug("Discount : " + _oTD.getDiscount());
        log.debug("SubTotal : " + _oTD.getSubTotal());
    }      
    
    protected void mergeData (MedOrderDetail _oOldTD, MedOrderDetail _oNewTD) 
    	throws Exception
    {		
    	double dQty 		= _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
    	
    	String sDiscount 	= _oNewTD.getDiscount();
    	double dTotalSales  = dQty * _oNewTD.getItemPrice().doubleValue();
    	double dTotalDisc   = Calculator.calculateDiscount(sDiscount,dTotalSales);
    	double dTaxAmount   = _oNewTD.getTaxAmount().doubleValue()/100;
    	
    	dTotalSales 		 = dTotalSales - dTotalDisc;
    	double dTotalTax 	 = dTotalSales * dTaxAmount;
    	double dSubTotal 	 = dTotalSales; //+ dTotalTax;
    	double dSubTotalCost = dQty * _oNewTD.getItemCost().doubleValue();
    	
    	//return RFEE to percentage or default mode so it will be recalculated 
    	//by set header properties
    	_oOldTD.setRfee			( _oNewTD.getRfee());
    	_oOldTD.setPackaging	( _oNewTD.getPackaging());   
    	
    	_oOldTD.setQty 			( new BigDecimal (dQty));    	
    	_oOldTD.setQtyBase		( UnitTool.getBaseQty(_oOldTD.getItemId(), _oOldTD.getUnitId(), _oOldTD.getQty()));
		_oOldTD.setDiscount 	( sDiscount  );    	
		_oOldTD.setTaxAmount 	( new BigDecimal (dTaxAmount * 100)  );    	
		
		_oOldTD.setSubTotalCost ( new BigDecimal (dSubTotalCost));
    	_oOldTD.setSubTotalDisc ( new BigDecimal (dTotalDisc)   );
    	_oOldTD.setSubTotalTax 	( new BigDecimal (dTotalTax)    );
    	_oOldTD.setSubTotal     ( new BigDecimal (dSubTotal)    );
    	_oNewTD = null;
    }        
    
    protected void getData ( RunData data ) 
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{
    		    if(data.getParameters().getInt("save",-1) < 0)
    		    {
    			    oSes.setAttribute (s_MSO, MedOrderTool.getHeaderByID (sID));
				    oSes.setAttribute (s_MSD, MedOrderTool.getDetailsByID (sID));
    		    }
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }

	/**
     * refresh detail and clear discount to reflect payment type change
     * 
     * @param data
     * @throws Exception
     */
	private void refreshDetail(RunData data)
		throws Exception
	{
        if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail (vTD, data);
        data.getParameters().setProperties (oTR);	
						
		MedOrderTool.setHeaderProperties (oTR, vTD,data);
		oSes.setAttribute (s_MSD, vTD); 
		oSes.setAttribute (s_MSO, oTR);     
	}
	
	public void confirmDelivery(RunData data)
		throws Exception
	{		
		System.out.println("oTR " + oTR);
		if(oTR.getStatus() == i_PROCESSED)
		{
			if(StringUtil.isNotEmpty(data.getParameters().getString("CourierId")))
			{
				oTR.setCourierId(data.getParameters().getString("CourierId"));
			}
			if(StringUtil.isNotEmpty(data.getParameters().getString("ShipTo")))
			{
				oTR.setShipTo(data.getParameters().getString("ShipTo"));
			}		
			oTR.setDeliveryNo(data.getParameters().getString("DeliveryNo"));
			oTR.setStatus(MedOrderTool.i_MO_DELIVERED);
			oTR.save();
			oSes.setAttribute (s_MSO,  oTR);
			data.setMessage(LocaleTool.getString("do_save_success"));
		}		
	}
	
	public void toInvoice(RunData data)
		throws Exception
	{		
		if(oTR.getStatus() == i_PROCESSED || oTR.getStatus() == MedOrderTool.i_MO_DELIVERED)
		{
			MedicineSales oSI = MedicalSalesTool.getHeaderByID(oTR.getMedOrderId());
			if(oSI == null)
			{
				oSI = new MedicineSales();
				BeanUtil.setBean2BeanProperties(oTR, oSI);
				oSI.setMedicineSalesId(oTR.getMedOrderId());
				oSI.setInvoiceNo("");
				oSI.setStatus(i_PENDING);
				oSI.setNew(true);			
				oSI.setCashierName(data.getUser().getName());
				
				List vSID = new ArrayList(vTD.size());
				for(int i = 0; i < vTD.size(); i++)
				{
					MedOrderDetail oMOD = (MedOrderDetail) vTD.get(i);
					MedicineSalesDetail oSID = new MedicineSalesDetail();
					BeanUtil.setBean2BeanProperties(oMOD, oSID);
					oSID.setMedicineSalesId(oTR.getMedOrderId());
					oSID.setMedicineSalesDetailId(oMOD.getMedOrderDetailId());
					vSID.add(oSID);
				}	
				MedicalSalesTool.saveData(oSI, vSID, new ArrayList());
				oSes.setAttribute(s_MTR, oSI);
				oSes.setAttribute(s_MTD, vSID);	
				
				oTR.setStatus(MedOrderTool.i_MO_INVOICED);
				oTR.save();
				data.setMessage("SUCCESS");
			}
			else
			{
				data.setMessage(LocaleTool.getString("sales_invoice") + " " + LocaleTool.getString("already_exist"));
				oTR.setStatus(MedOrderTool.i_MO_INVOICED);
				oTR.save();
			}
		}		
	}
	
	/**
	 * this method is used to set parent id for powder, by finding closest powder item
	 * 
	 * @return prescriptionDetailId of closest powder item 
	 */
	private String getClosestPowderItem()
	{
		if(vTD != null)
		{
			if(b_SALES_FIRST_LAST)
			{
				for (int i = 0; i < vTD.size(); i++)
				{
					MedOrderDetail oMSD = (MedOrderDetail) vTD.get(i);
					System.out.println(i + " PowderItem: " +  PowderTool.isPowderItem(oMSD.getItemId()) +  "  "+oMSD.getTxType() +" Item: " +oMSD.getItemName());
					if(oMSD.getTxType() == i_PRES_POWDER && 
					   StringUtil.empty(oMSD.getParentId()) && 
					   PowderTool.isPowderItem(oMSD.getItemId()))
					{
						return oMSD.getPrescriptionDetailId();
					}
				}
			}
			else
			{				
				for (int i = (vTD.size() - 1); i >= 0; i--)
				{
					MedOrderDetail oMSD = (MedOrderDetail) vTD.get(i);
					if(oMSD.getTxType() == i_PRES_POWDER && 
					   StringUtil.empty(oMSD.getParentId()) && 
					   PowderTool.isPowderItem(oMSD.getItemId()))
					{
						return oMSD.getPrescriptionDetailId();
					}
				}
			}
		}
		return "";
	}
	
	private int countPowder()
	{
		int no = 0;
		if(vTD != null)
		{			
			for (int i = 0; i < vTD.size(); i++)
			{
				MedOrderDetail oMSD = (MedOrderDetail) vTD.get(i);
				if(oMSD.getTxType() == i_PRES_POWDER && 
				   StringUtil.empty(oMSD.getParentId()) && 
				   PowderTool.isPowderItem(oMSD.getItemId()))
				{
					no = no + 1;
				}
			}			
		}
		return no;
	}
}

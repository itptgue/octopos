package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import com.ssti.enterprise.pos.manager.CustomFieldManager;
import com.ssti.enterprise.pos.om.CustomField;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CustomFieldTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: CustomFieldTool.java,v $
 * Revision 1.8  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/10 07:44:49  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class CustomFieldTool extends BaseTool 
{
	private static CustomFieldTool instance = null;
	
	public static synchronized CustomFieldTool getInstance()
	{
		if (instance == null) instance = new CustomFieldTool();
		return instance;
	}
	
	public static List getAllCustomField()
		throws Exception
	{
		return CustomFieldManager.getInstance().getAllCustomField();
	}
	
	public static CustomField getCustomFieldByID(String _sID)
		throws Exception
	{
		return CustomFieldManager.getInstance().getCustomFieldByID (_sID, null);
	}
	
	public static CustomField getCustomFieldByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return CustomFieldManager.getInstance().getCustomFieldByID(_sID, _oConn);
	}
}

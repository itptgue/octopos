package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemInventoryMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemInventoryMapBuilder";

    /**
     * Item
     * @deprecated use ItemInventoryPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_inventory";
    }

  
    /**
     * item_inventory.ITEM_INVENTORY_ID
     * @return the column name for the ITEM_INVENTORY_ID field
     * @deprecated use ItemInventoryPeer.item_inventory.ITEM_INVENTORY_ID constant
     */
    public static String getItemInventory_ItemInventoryId()
    {
        return "item_inventory.ITEM_INVENTORY_ID";
    }
  
    /**
     * item_inventory.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use ItemInventoryPeer.item_inventory.LOCATION_ID constant
     */
    public static String getItemInventory_LocationId()
    {
        return "item_inventory.LOCATION_ID";
    }
  
    /**
     * item_inventory.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use ItemInventoryPeer.item_inventory.LOCATION_NAME constant
     */
    public static String getItemInventory_LocationName()
    {
        return "item_inventory.LOCATION_NAME";
    }
  
    /**
     * item_inventory.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemInventoryPeer.item_inventory.ITEM_ID constant
     */
    public static String getItemInventory_ItemId()
    {
        return "item_inventory.ITEM_ID";
    }
  
    /**
     * item_inventory.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use ItemInventoryPeer.item_inventory.ITEM_CODE constant
     */
    public static String getItemInventory_ItemCode()
    {
        return "item_inventory.ITEM_CODE";
    }
  
    /**
     * item_inventory.MAXIMUM_QTY
     * @return the column name for the MAXIMUM_QTY field
     * @deprecated use ItemInventoryPeer.item_inventory.MAXIMUM_QTY constant
     */
    public static String getItemInventory_MaximumQty()
    {
        return "item_inventory.MAXIMUM_QTY";
    }
  
    /**
     * item_inventory.MINIMUM_QTY
     * @return the column name for the MINIMUM_QTY field
     * @deprecated use ItemInventoryPeer.item_inventory.MINIMUM_QTY constant
     */
    public static String getItemInventory_MinimumQty()
    {
        return "item_inventory.MINIMUM_QTY";
    }
  
    /**
     * item_inventory.REORDER_POINT
     * @return the column name for the REORDER_POINT field
     * @deprecated use ItemInventoryPeer.item_inventory.REORDER_POINT constant
     */
    public static String getItemInventory_ReorderPoint()
    {
        return "item_inventory.REORDER_POINT";
    }
  
    /**
     * item_inventory.RACK_LOCATION
     * @return the column name for the RACK_LOCATION field
     * @deprecated use ItemInventoryPeer.item_inventory.RACK_LOCATION constant
     */
    public static String getItemInventory_RackLocation()
    {
        return "item_inventory.RACK_LOCATION";
    }
  
    /**
     * item_inventory.ACTIVE
     * @return the column name for the ACTIVE field
     * @deprecated use ItemInventoryPeer.item_inventory.ACTIVE constant
     */
    public static String getItemInventory_Active()
    {
        return "item_inventory.ACTIVE";
    }
  
    /**
     * item_inventory.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use ItemInventoryPeer.item_inventory.UPDATE_DATE constant
     */
    public static String getItemInventory_UpdateDate()
    {
        return "item_inventory.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_inventory");
        TableMap tMap = dbMap.getTable("item_inventory");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_inventory.ITEM_INVENTORY_ID", "");
                          tMap.addColumn("item_inventory.LOCATION_ID", "");
                          tMap.addColumn("item_inventory.LOCATION_NAME", "");
                          tMap.addColumn("item_inventory.ITEM_ID", "");
                          tMap.addColumn("item_inventory.ITEM_CODE", "");
                            tMap.addColumn("item_inventory.MAXIMUM_QTY", bd_ZERO);
                            tMap.addColumn("item_inventory.MINIMUM_QTY", bd_ZERO);
                            tMap.addColumn("item_inventory.REORDER_POINT", bd_ZERO);
                          tMap.addColumn("item_inventory.RACK_LOCATION", "");
                          tMap.addColumn("item_inventory.ACTIVE", Boolean.TRUE);
                          tMap.addColumn("item_inventory.UPDATE_DATE", new Date());
          }
}

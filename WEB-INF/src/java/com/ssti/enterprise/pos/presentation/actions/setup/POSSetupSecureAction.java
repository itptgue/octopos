package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureAction;

public class POSSetupSecureAction extends SecureAction
{
	private static final String[] a_PERM = {"Setup POS Data", "View POS Data"};	
	private static final String[] a_UPDATE_PERM = {"Setup POS Data", "View POS Data", "Update POS Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doFind",null) != null)
        {
            return isAuthorized(data, a_PERM);
        }
        else if(data.getParameters().getString("eventSubmit_doInsert",null)!= null ||
                data.getParameters().getString("eventSubmit_doUpdate",null)!= null ||
                data.getParameters().getString("eventSubmit_doDelete",null)!= null)
        {
        	return isAuthorized(data, a_UPDATE_PERM);
        }	
        return isAuthorized(data, a_PERM);
    }	
}

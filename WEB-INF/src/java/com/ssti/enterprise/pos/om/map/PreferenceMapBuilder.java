package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PreferenceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PreferenceMapBuilder";

    /**
     * Item
     * @deprecated use PreferencePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "preference";
    }

  
    /**
     * preference.PREFERENCE_ID
     * @return the column name for the PREFERENCE_ID field
     * @deprecated use PreferencePeer.preference.PREFERENCE_ID constant
     */
    public static String getPreference_PreferenceId()
    {
        return "preference.PREFERENCE_ID";
    }
  
    /**
     * preference.LANGUAGE_ID
     * @return the column name for the LANGUAGE_ID field
     * @deprecated use PreferencePeer.preference.LANGUAGE_ID constant
     */
    public static String getPreference_LanguageId()
    {
        return "preference.LANGUAGE_ID";
    }
  
    /**
     * preference.HQ_HOSTNAME
     * @return the column name for the HQ_HOSTNAME field
     * @deprecated use PreferencePeer.preference.HQ_HOSTNAME constant
     */
    public static String getPreference_HqHostname()
    {
        return "preference.HQ_HOSTNAME";
    }
  
    /**
     * preference.HQ_IP_ADDRESS
     * @return the column name for the HQ_IP_ADDRESS field
     * @deprecated use PreferencePeer.preference.HQ_IP_ADDRESS constant
     */
    public static String getPreference_HqIpAddress()
    {
        return "preference.HQ_IP_ADDRESS";
    }
  
    /**
     * preference.COSTING_METHOD
     * @return the column name for the COSTING_METHOD field
     * @deprecated use PreferencePeer.preference.COSTING_METHOD constant
     */
    public static String getPreference_CostingMethod()
    {
        return "preference.COSTING_METHOD";
    }
  
    /**
     * preference.SITE_ID
     * @return the column name for the SITE_ID field
     * @deprecated use PreferencePeer.preference.SITE_ID constant
     */
    public static String getPreference_SiteId()
    {
        return "preference.SITE_ID";
    }
  
    /**
     * preference.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PreferencePeer.preference.LOCATION_ID constant
     */
    public static String getPreference_LocationId()
    {
        return "preference.LOCATION_ID";
    }
  
    /**
     * preference.INVENTORY_TYPE
     * @return the column name for the INVENTORY_TYPE field
     * @deprecated use PreferencePeer.preference.INVENTORY_TYPE constant
     */
    public static String getPreference_InventoryType()
    {
        return "preference.INVENTORY_TYPE";
    }
  
    /**
     * preference.LOCATION_FUNCTION
     * @return the column name for the LOCATION_FUNCTION field
     * @deprecated use PreferencePeer.preference.LOCATION_FUNCTION constant
     */
    public static String getPreference_LocationFunction()
    {
        return "preference.LOCATION_FUNCTION";
    }
  
    /**
     * preference.AUDIT_LOG
     * @return the column name for the AUDIT_LOG field
     * @deprecated use PreferencePeer.preference.AUDIT_LOG constant
     */
    public static String getPreference_AuditLog()
    {
        return "preference.AUDIT_LOG";
    }
  
    /**
     * preference.START_DATE
     * @return the column name for the START_DATE field
     * @deprecated use PreferencePeer.preference.START_DATE constant
     */
    public static String getPreference_StartDate()
    {
        return "preference.START_DATE";
    }
  
    /**
     * preference.CASHIER_PASSWORD
     * @return the column name for the CASHIER_PASSWORD field
     * @deprecated use PreferencePeer.preference.CASHIER_PASSWORD constant
     */
    public static String getPreference_CashierPassword()
    {
        return "preference.CASHIER_PASSWORD";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("preference");
        TableMap tMap = dbMap.getTable("preference");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("preference.PREFERENCE_ID", "");
                            tMap.addColumn("preference.LANGUAGE_ID", Integer.valueOf(0));
                          tMap.addColumn("preference.HQ_HOSTNAME", "");
                          tMap.addColumn("preference.HQ_IP_ADDRESS", "");
                            tMap.addColumn("preference.COSTING_METHOD", Integer.valueOf(0));
                          tMap.addColumn("preference.SITE_ID", "");
                          tMap.addColumn("preference.LOCATION_ID", "");
                            tMap.addColumn("preference.INVENTORY_TYPE", Integer.valueOf(0));
                            tMap.addColumn("preference.LOCATION_FUNCTION", Integer.valueOf(0));
                          tMap.addColumn("preference.AUDIT_LOG", Boolean.TRUE);
                          tMap.addColumn("preference.START_DATE", new Date());
                          tMap.addColumn("preference.CASHIER_PASSWORD", "");
          }
}

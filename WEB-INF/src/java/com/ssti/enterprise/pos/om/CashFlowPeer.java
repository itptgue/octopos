package com.ssti.enterprise.pos.om;

import com.ssti.enterprise.pos.model.TransactionPeerOM;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Mon Feb 07 14:16:35 ICT 2005]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class CashFlowPeer
    extends com.ssti.enterprise.pos.om.BaseCashFlowPeer
    implements TransactionPeerOM
{
	public String getTableName()
	{
		return TABLE_NAME;
	}
	
	public String getIDColumn() 
	{
		return CASH_FLOW_ID;
	}
	
	public String getNoColumn() 
	{
		return TRANSACTION_NO;
	}
	
	public String getDateColumn() 
	{
		return EXPECTED_DATE;
	}
	
	public String getCreateByColumn() {
		return USER_NAME;
	}	
	
	public String getStatusColumn() 
	{
		return STATUS;
	}
}

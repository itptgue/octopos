/** 
   customer_common.js
   common javascript function for customer data query
   
   2016-06-10  
   - initial commit
 */
function getCustomerDataByID() 
{    
    var url = sTPL + 'transaction,CustomerDataLookup.vm';    
    var param = '&CustomerId=' + document.frm.CustomerId.value;    
    jQuery.post(url, param, function(resp){eval(resp);});
}
function handleCustomerNameKeypress (e) 
{
    var f = document.frm;
    if (f.CustomerName && f.CustomerName.value != '') 
    {
        if (getKey(e) == 13) {getCustomerDataByName();}
    }    
}
function handleCustomerCodeKeypress (e) 
{
    var f = document.frm;
    if (f.CustomerCode && f.CustomerCode.value != '') 
    {
        if (getKey(e) == 13) {getCustomerDataByCode();}
    }    
}
function getCustomerDataByName () 
{
    var url = sTPL + 'transaction,CustomerNameLookup.vm';
    var param = '&CustomerName=' + document.frm.CustomerName.value; 
    jQuery.post(url, param, function(resp){eval(resp);});
}
function getCustomerDataByCode () 
{
    var url = sTPL + 'transaction,CustomerNameLookup.vm';
    var param = '&CustomerCode=' + document.frm.CustomerCode.value; 
    jQuery.post(url, param, function(resp){eval(resp);});
}
function viewCustomerNameSelector () 
{
    var url = sTPL = 'transaction,CustomerNameSelector.vm';     
    var wndCust = window.open(url, 'wndCust', 
        'toolbar=no,scrollbars=yes,menubar=no,resizable=yes,width=720,height=600,left=20,top=20');
    wndCust.focus();            
} 

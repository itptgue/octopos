package com.ssti.enterprise.pos.tools;

import java.io.File;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mashape.unirest.http.Unirest;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.framework.mail.MailSender;
import com.ssti.framework.mail.MailSender.OAuth2Provider;
import com.ssti.framework.security.OAuth2SaslClientFactory;
import com.ssti.framework.tools.StringUtil;
import com.sun.mail.smtp.SMTPTransport;

public class MailTool 
{
	private static Log log = LogFactory.getLog ( MailTool.class );
	
	public static final String EMAIL_SVR = MailSender.GMAIL_SMTP;
	public static final String EMAIL_USR = "retailsoft.smtp@gmail.com";
	public static final String EMAIL_PWD = "crossfire123456";

    private static String HO_EMAIL_SVR = "";
    private static String HO_EMAIL_USR = "";
    private static String HO_EMAIL_PWD = "";

    private static String STORE_EMAIL_SVR = "";
    private static String STORE_EMAIL_USR = "";
    private static String STORE_EMAIL_PWD = "";

    static
    {
    	try 
    	{
    		//HO EMAIL CONFIG
    	    HO_EMAIL_SVR = PreferenceTool.getSysConfig().getEmailServer();
    	    HO_EMAIL_USR = PreferenceTool.getSysConfig().getEmailUser();
    	    HO_EMAIL_PWD = PreferenceTool.getSysConfig().getEmailPwd();			
    	    
    	    STORE_EMAIL_SVR = PreferenceTool.getSysConfig().getSyncStoreEmailServer();
    	    STORE_EMAIL_USR = PreferenceTool.getSysConfig().getSyncStoreEmailUser();
    	    STORE_EMAIL_PWD = PreferenceTool.getSysConfig().getSyncStoreEmailPwd();
    	    
    		if (StringUtil.isEmpty(HO_EMAIL_SVR)) HO_EMAIL_SVR = EMAIL_SVR ;
    		if (StringUtil.isEmpty(HO_EMAIL_USR)) HO_EMAIL_USR = EMAIL_USR;
    		if (StringUtil.isEmpty(HO_EMAIL_PWD)) HO_EMAIL_PWD = EMAIL_PWD;

    		if (StringUtil.isEmpty(STORE_EMAIL_SVR)) STORE_EMAIL_SVR = EMAIL_SVR ;
    		if (StringUtil.isEmpty(STORE_EMAIL_USR)) STORE_EMAIL_USR = EMAIL_USR;
    		if (StringUtil.isEmpty(STORE_EMAIL_PWD)) STORE_EMAIL_PWD = EMAIL_PWD;
		} 
    	catch (Exception e) 
    	{
    		e.printStackTrace();
    	}
    }
    
    public static void sendToStore(String to, String subject, String body, String att)
    	throws Exception
    {
    	String[] aTo = {to};
    	String sFrom = PreferenceTool.getLocationName();
    	
    	MailSender oMS = new MailSender(HO_EMAIL_SVR, HO_EMAIL_USR, HO_EMAIL_PWD, sFrom, aTo, subject, body, att);
    	oMS.send();    	
    }

    public static void sendToHO(String subject, String body, String att)
		throws Exception
	{
    	String sFrom = PreferenceTool.getLocationName();
		if (StringUtil.isEmpty(PreferenceTool.getSysConfig().getSyncHoEmail()))
		{
			throw new Exception ("HO Email not set in Application Configuration");
		}
		String[] aTo = {PreferenceTool.getSysConfig().getSyncHoEmail()};		
		MailSender oMS = new MailSender(STORE_EMAIL_SVR, STORE_EMAIL_USR, STORE_EMAIL_PWD, sFrom, aTo, subject, body, att);
		oMS.send();
	}
    
    public static String[] sendToAllStore(String subject, String body, String att)
		throws Exception
	{
    	String sFrom = PreferenceTool.getLocationName();
    	List vLoc = LocationTool.getLocationByType(AppAttributes.i_STORE);
    	List vEmail = new ArrayList(vLoc.size());
    	for (int i = 0; i < vLoc.size(); i++)
    	{
    		Location oLoc = (Location) vLoc.get(i);
    		if (StringUtil.isNotEmpty(oLoc.getLocationEmail()) && StringUtil.contains(oLoc.getLocationEmail(),"@"))
    		{
    			vEmail.add(oLoc.getLocationEmail());
    		}
    	}
	
		String[] aTo = new String[vEmail.size()];
    	for (int i = 0; i < vEmail.size(); i++)
    	{
    		String sEmail = (String) vEmail.get(i);
    		aTo[i] = sEmail;
    	}    	
		MailSender oMS = new MailSender(HO_EMAIL_SVR, HO_EMAIL_USR, HO_EMAIL_PWD, sFrom, aTo, subject, body, att);
		oMS.send();		
		return aTo;
	}
    
    public static String[] sendHOEmail(String sFrom, String[] aTo, String subject, String body, String att, RecipientType recType)
		throws Exception
	{   
    	if(StringUtil.isEmpty(sFrom)) sFrom = PreferenceTool.getCompany().getCompanyName();
    	MailSender oMS = new MailSender(HO_EMAIL_SVR, HO_EMAIL_USR, HO_EMAIL_PWD, sFrom, aTo, subject, body, att);
    	oMS.setRecType(recType);
    	oMS.send();
		return aTo;
	}  
	
    public static String[] sendEmail(String _from,
								 	 String _fromEmail,
								 	 String[] aTo, 
									 String subject, 
									 String body,
									 String attFile, 
									 File[] fileAtt, 
									 RecipientType recType,
									 String _refToken)
    	throws Exception
    {
    	if(StringUtil.equalsIgnoreCase(HO_EMAIL_SVR,MailSender.GMAIL_SMTP))
    	{
    		sendViaGmail(_from, _fromEmail, aTo, subject, body, attFile, null, recType, "");
    	}
    	else
    	{
    		MailSender oMS = new MailSender(HO_EMAIL_SVR, HO_EMAIL_USR, HO_EMAIL_PWD, _from, aTo, subject, body, attFile);
    		oMS.setRecType(recType);
    		oMS.send();
    	}
		return aTo;
    }
    
    public static String[] sendViaGmail(String[] aTo, 
										String subject, 
										String body,
										String attFile, 
										File[] fileAtt, 
										RecipientType recType)
	    throws Exception
	{
    	return sendViaGmail("Retailsoft Mailer", HO_EMAIL_USR, aTo, subject, body, attFile, fileAtt, recType, "");
	}
	    
    public static String[] sendViaGmail(String _from,
    									String _sender,
    									String[] aTo, 
    									String subject, String body,
    									String attFile, 
    									File[] fileAtt, 
    									RecipientType recType,
    									String _refToken)
		throws Exception
	{        

//		  STEP 1, Request Code to Google, Receive Callback    	
//    	  https://accounts.google.com/o/oauth2/v2/auth?
//    		  	 scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&
//    			 access_type=offline&
//    			 include_granted_scopes=true&
//    			 state=state_parameter_passthrough_value&
//    			 redirect_uri=http%3A%2F%2Foauth2.example.com%2Fcallback&
//    			 response_type=code&
//    			 client_id=290865583651-6au9qolhog9i0gq7befmo9rb37crnc0i.apps.googleusercontent.com

//    	python oauth2.py --user=xxxx@gmail.com  --client_id=290865583651-6au9qolhog9i0gq7befmo9rb37crnc0i.apps.googleusercontent.com     --client_secret=5oiTZjJee-CE2N_lenOLsRPe   --generate_oauth2_token
//      To authorize token, visit this url and follow the directions:
//		https://accounts.google.com/o/oauth2/auth?client_id=290865583651-6au9qolhog9i0gq7befmo9rb37crnc0i.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&response_type=code&scope=https%3A%2F%2Fmail.google.com%2F
//		Refresh token 1/5Km93fYgVwk0g4RPvF4N_QnLz2dAGQ2EYZhczwFm5uw

    	
    	//https://www.googleapis.com/oauth2/v4/token
    	//code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7&
    	//client_id=your_client_id&
    	//client_secret=your_client_secret&
    	//redirect_uri=https://oauth2.example.com/code&
    	//grant_type=authorization_code
    	    	
    	//ClientID:290865583651-6au9qolhog9i0gq7befmo9rb37crnc0i.apps.googleusercontent.com
    	//ClientSecret:5oiTZjJee-CE2N_lenOLsRPe
    	//RefreshToken: 1/Z-aCscXMkZHfdQ5ln2y4yq4vzB7Z2l3MG5CDnEKgpCRHwL2BbniTlW3T3flE5WaU
    	//Access Token: ya29.GluhBGMZ9M8JFNv5P9WKyBAyi4WyWz-4E8mt3ExYZZaLoojfnJfOuFkwXEa0J1Qj_7J3lWkgWZTGyym3-zpVhc26f7exkq625FpkkrZQk3_6wBNvUk7osj72K6VI
    	
    	String from = "Retailsoft Mailer";
    	String sender = HO_EMAIL_USR;
    	String refToken = "1/Z-aCscXMkZHfdQ5ln2y4yq4vzB7Z2l3MG5CDnEKgpCRHwL2BbniTlW3T3flE5WaU"; //retailsoft.smtp@gmail.com
    	
    	if(StringUtil.isNotEmpty(_sender)) sender = _sender;
    	if(StringUtil.isNotEmpty(_from)) from = _from;
    	if(!StringUtil.equalsIgnoreCase(StringUtil.trim(sender), "retailsoft.smtp@gmail.com"))
    	{
    		if(StringUtil.isNotEmpty(_refToken)) refToken = _refToken;  
    		else refToken = HO_EMAIL_PWD; //put ref token in password
    	}
    	
    	String url = "https://www.googleapis.com/oauth2/v4/token"+
    				 "?client_id=290865583651-6au9qolhog9i0gq7befmo9rb37crnc0i.apps.googleusercontent.com" + 
			    	 "&client_secret=5oiTZjJee-CE2N_lenOLsRPe"+
			    	 "&refresh_token="+ refToken +
			    	 "&grant_type=refresh_token";
    	System.out.println("google url" + url);
    	String key = Unirest.post(url).asJson().getBody().getObject().getString("access_token");
    	HO_EMAIL_PWD = key;    	
    	
    	Security.addProvider(new OAuth2Provider());
    	
    	Properties props = new Properties();
    	props.put("mail.smtp.starttls.enable", "true");
    	props.put("mail.smtp.starttls.required", "true");
    	props.put("mail.smtp.sasl.enable", "true");
    	props.put("mail.smtp.sasl.mechanisms", "XOAUTH2");
    	props.put(OAuth2SaslClientFactory.OAUTH_TOKEN_PROP, HO_EMAIL_PWD);
    	
    	Session session = Session.getInstance(props);
    	session.setDebug(true);

    	final URLName unusedUrlName = null;
    	
    	SMTPTransport transport = new SMTPTransport(session, unusedUrlName);
    	//If the password is non-null, SMTP tries to do AUTH LOGIN.
    	final String emptyPassword = "";
    	transport.connect(MailSender.GMAIL_SMTP, 587, sender, emptyPassword);
    	
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from + "<" + sender + ">"));
		msg.setSubject(subject);
		
		InternetAddress[] toAddress = new InternetAddress[aTo.length];
		for (int i = 0; i < aTo.length; i++)
		{
			if (StringUtil.isNotEmpty(aTo[i]))
			{
			    toAddress[i] = new InternetAddress(aTo[i]);					
			}
		}
		
		msg.setRecipients(recType, toAddress);
		
		//add atleast simple body
		MimeBodyPart mimeBody = new MimeBodyPart();
		
	    Pattern p = Pattern.compile("<([^\\s>/]+)");
	    Matcher m = p.matcher(body);
	    boolean bhtml = false;
	    while(m.find()) 
	    {
	    	bhtml = true;
	    	String tag = m.group(1);
	        //System.out.println(tag);
	    }
		
		if(bhtml)
		{
			mimeBody.setContent(body, "text/html");
		}
		else
		{
			mimeBody.setText(body);
		}

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(mimeBody);
		
		//do attachment
		if (StringUtil.isNotEmpty(attFile))
		{
			File attachment = new File(attFile);

			MimeBodyPart mimeAttachment = new MimeBodyPart();
			FileDataSource dataSource = new FileDataSource(attachment);
			mimeAttachment.setDataHandler(new DataHandler(dataSource));
			mimeAttachment.setFileName(attachment.getName());
			mimeAttachment.setDisposition(MimeBodyPart.ATTACHMENT);				
			multipart.addBodyPart(mimeAttachment);
		}
		
		if(fileAtt != null && fileAtt.length > 0)
		{
			for(int i = 0; i < fileAtt.length; i++)
			{
				File attachment = fileAtt[i];

				MimeBodyPart mimeAttachment = new MimeBodyPart();
				FileDataSource dataSource = new FileDataSource(attachment);
				mimeAttachment.setDataHandler(new DataHandler(dataSource));
				mimeAttachment.setFileName(attachment.getName());
				mimeAttachment.setDisposition(MimeBodyPart.ATTACHMENT);				
				multipart.addBodyPart(mimeAttachment);
			}
		}
		
		msg.setContent(multipart);
		
		transport.sendMessage(msg, toAddress);
		
		return aTo;
	}    
    
    /**
     * send SMS 
     * 
     * @param url
     * @param phn
     * @param msg
     * @return
     * @throws Exception
     */
    public static String sendSMS(String url, String phn, String msg)
    	throws Exception
    {
    	if(!StringUtil.empty(url))
    	{
    		if(StringUtil.contains(url, "{{phn}}") && !StringUtil.empty(phn)) url = StringUtil.replace(url, "{{phn}}", phn);
    		if(StringUtil.contains(url, "{{msg}}") && !StringUtil.empty(msg)) url = StringUtil.replace(url, "{{msg}}", msg);    		
    		log.info("Sending SMS:" + url);
    		return Unirest.get(url).asString().getBody();
    	}
    	return "";
    }
}


package org.apache.turbine.flux.modules.actions.permission;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.flux.modules.actions.FluxAction;
import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.EntityExistsException;
import org.apache.turbine.util.security.UnknownEntityException;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: E:/CVS/REPO/POS/WEB-INF/src/java/org/apache/turbine/flux/modules/actions/permission/FluxPermissionAction.java,v $
 * Purpose: 
 *
 * @author  $Author: Albert $
 * @version $Id: FluxPermissionAction.java,v 1.5 2005/04/08 06:56:48 Albert Exp $
 *
 * $Log: FluxPermissionAction.java,v $
 * Revision 1.5  2005/04/08 06:56:48  Albert
 * *** empty log message ***
 *
 * Revision 1.4  2005/03/29 02:34:02  Albert
 * *** empty log message ***
 *
 */
public class FluxPermissionAction extends FluxAction
{
	private static Log log = LogFactory.getLog(FluxPermissionAction.class);

    public void doPerform(RunData data, Context context)
        throws Exception
    {
        data.setMessage(LocaleTool.getString("action_notfound"));
    }
	
    public void doInsert(RunData data, Context context)
        throws Exception
    {
        org.apache.turbine.om.security.Permission permission = TurbineSecurity.getPermissionInstance(null);
        data.getParameters().setProperties(permission);
        String name = data.getParameters().getString("name");
        try
        {
        	
            TurbineSecurity.addPermission(permission);
            data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch(EntityExistsException _oEx)
        {
            context.put("name", name);
            context.put("permission", permission);
            data.setMessage(LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());            
        }
    }

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
        org.apache.turbine.om.security.Permission permission = 
        		TurbineSecurity.getPermissionByName(data.getParameters().getString("oldname"));
        try
        {
            TurbineSecurity.renamePermission(permission, data.getParameters().getString("name"));
            data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch(UnknownEntityException _oEx)
        {
            data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());            
        	log.error (_oEx);
        }
    }

    public void doDelete(RunData data, Context context)
	    throws Exception
	{
	    org.apache.turbine.om.security.Permission permission = TurbineSecurity.getPermissionByName(data.getParameters().getString("name"));
	    try
	    {
	        TurbineSecurity.removePermission(permission);
	        data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
	    }
	    catch(UnknownEntityException _oEx)
	    {
	        log.error (_oEx);
	        data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());            
	    }
	}
}
package com.ssti.enterprise.pos.presentation.screens.report.payable;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;

public class DebitMemoReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	try 
    	{
	    	context.put ("vTrans", 
	    		DebitMemoTool.findTrans(sVendorID,  sBankID, sLocationID, sCFTID, dStart, dEnd, iStatus));
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Payment Report Failed : " + _oEx.getMessage());
    	}    
    }    
}

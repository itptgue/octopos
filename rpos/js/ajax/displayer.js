var Displayer = {}
Displayer.Base = function() {};
Displayer.Base.prototype = 
{
  base_initialize: function(element, update, options) 
  {
    this.element     = $(element); 
    this.update      = $(update);  
    this.has_focus   = false; 
    this.changed     = false; 
    this.active      = false; 
    this.index       = 0;     
    this.entry_count = 0;

    this.options            = options || {};
    this.options.tokens     = this.options.tokens || new Array();
    this.options.frequency  = this.options.frequency || 0.4;
    this.options.min_chars  = this.options.min_chars || 1;  
    this.options.onShow     = this.options.onShow || 
      
      function(element, update)
      { 
        if(!update.style.position || update.style.position=='absolute') {
          update.style.position = 'absolute';
            var offsets = Position.cumulativeOffset(element);
            update.style.left = offsets[0] + 'px';
            update.style.top  = (offsets[1] + element.offsetHeight) + 'px';
            update.style.width = element.offsetWidth + 'px';
        }
        new Effect.Appear(update,{duration:0.15});
      };
    
    this.options.onHide = this.options.onHide || 
    function(element, update){new Effect.Fade(update,{duration:0.15})};
    
    if(this.options.indicator) this.indicator = $(this.options.indicator);

    if (typeof(this.options.tokens) == 'string') 
      this.options.tokens = new Array(this.options.tokens);
       
    this.observer = null;
    
    Element.hide(this.update);
    
    Event.observe(this.element, "blur", this.onBlur.bindAsEventListener(this));
    Event.observe(this.element, "keypress", this.onKeyPress.bindAsEventListener(this));
  },

  show: function() 
  {
    if(this.update.style.display=='none') this.options.onShow(this.element, this.update);
    if(!this.iefix && (navigator.appVersion.indexOf('MSIE')>0) && this.update.style.position=='absolute') {
      new Insertion.After(this.update, 
       '<iframe id="' + this.update.id + '_iefix" '+
       'style="display:none;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" ' +
       'src="javascript:false;" frameborder="0" scrolling="no"></iframe>');
      this.iefix = $(this.update.id+'_iefix');
    }
    if(this.iefix) {
      Position.clone(this.update, this.iefix);
      this.iefix.style.zIndex = 1;
      this.update.style.zIndex = 2;
      Element.show(this.iefix);
    }
  },
  
  hide: function() 
  {
    if(this.update.style.display=='') this.options.onHide(this.element, this.update);
    if(this.iefix) Element.hide(this.iefix);
  },
  
  startIndicator: function() 
  {
    if(this.indicator) Element.show(this.indicator);
  },
  
  stopIndicator: function() 
  {
    if(this.indicator) Element.hide(this.indicator);
  },

  onKeyPress: function(event) 
  {
    if(this.active)      
      switch(event.keyCode) {
       case Event.KEY_TAB:
       case Event.KEY_RETURN:
         Event.stop(event);
       case Event.KEY_ESC:
         this.hide();
         this.active = false;
         return;
       case Event.KEY_LEFT:
       case Event.KEY_RIGHT:
         return;
      }
    else 
      if(event.keyCode==Event.KEY_TAB || event.keyCode==Event.KEY_RETURN) 
         return;    
    
    this.changed = true;
    this.has_focus = true;
    
    if(this.observer) clearTimeout(this.observer);
      this.observer = 
        setTimeout(this.onObserverEvent.bind(this), this.options.frequency*1000);
    
  },
  
  onHover: function(event) 
  {
    Event.stop(event);
  },
  
  onClick: function(event) 
  {
    Event.stop(event);
  },
  
  onBlur: function(event) 
  {
    // needed to make click events working
    setTimeout(this.hide.bind(this), 250);
    this.has_focus = false;
    this.active = false;     
  }, 
  
  render: function() 
  {
    this.show();
    this.active = true;
  },
  
  
  displayPage: function(resultPage) {
    if(!this.changed && this.has_focus) {
      this.update.innerHTML = resultPage;
      Element.cleanWhitespace(this.update);
      Element.cleanWhitespace(this.update.firstChild);
      
      this.stopIndicator();
      this.render();
    }
  },

  onObserverEvent: function() 
  {
    this.changed = false;   
    if(this.getEntry().length >= 0) 
    {
      this.startIndicator();
      this.getPage();
    } 
    else 
    {
      this.active = false;
      this.hide();
    }
  },

  getEntry: function() 
  {
    var token_pos = this.findLastToken();
    if (token_pos != -1)
      var ret = this.element.value.substr(token_pos + 1).replace(/^\s+/,'').replace(/\s+$/,'');
    else
      var ret = this.element.value;
    
    return /\n/.test(ret) ? '' : ret;
  },

  findLastToken: function() 
  {
    var last_token_pos = -1;

    for (var i=0; i<this.options.tokens.length; i++) {
      var this_token_pos = this.element.value.lastIndexOf(this.options.tokens[i]);
      if (this_token_pos > last_token_pos)
        last_token_pos = this_token_pos;
    }
    return last_token_pos;
  }
}

Ajax.Displayer = Class.create();
Object.extend(Object.extend(Ajax.Displayer.prototype, Displayer.Base.prototype), {
  initialize: function(element, update, url, options) 
  {
	this.base_initialize(element, update, options);
    
    this.options.asynchronous  = true;
    this.options.onComplete    = this.onComplete.bind(this)
    this.options.method        = 'post';
    this.options.defaultParams = this.options.parameters || null;
    this.url                   = url;
  },
  
  getPage: function() {
    entry = encodeURIComponent(this.element.name) + '=' + 
      encodeURIComponent(this.getEntry());
      
    this.options.parameters = this.options.callback ?
      this.options.callback(this.element, entry) : entry;
        
    if(this.options.defaultParams) 
      this.options.parameters += '&' + this.options.defaultParams;
    
    new Ajax.Request(this.url, this.options);
  },
  
  onComplete: function(request) {
    this.displayPage(request.responseText);
  }
  
});
package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to InvoiceMessage
 */
public abstract class BaseInvoiceMessage extends BaseObject
{
    /** The Peer class */
    private static final InvoiceMessagePeer peer =
        new InvoiceMessagePeer();

        
    /** The value for the invoiceMessageId field */
    private String invoiceMessageId;
      
    /** The value for the startDate field */
    private Date startDate;
      
    /** The value for the endDate field */
    private Date endDate;
      
    /** The value for the title field */
    private String title;
                                                
    /** The value for the contents field */
    private String contents = "";
                                                
    /** The value for the customerTypeId field */
    private String customerTypeId = "";
                                                
    /** The value for the locationId field */
    private String locationId = "";
      
    /** The value for the lastUpdateBy field */
    private String lastUpdateBy;
      
    /** The value for the lastUpdate field */
    private Date lastUpdate;
  
    
    /**
     * Get the InvoiceMessageId
     *
     * @return String
     */
    public String getInvoiceMessageId()
    {
        return invoiceMessageId;
    }

                        
    /**
     * Set the value of InvoiceMessageId
     *
     * @param v new value
     */
    public void setInvoiceMessageId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceMessageId, v))
              {
            this.invoiceMessageId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StartDate
     *
     * @return Date
     */
    public Date getStartDate()
    {
        return startDate;
    }

                        
    /**
     * Set the value of StartDate
     *
     * @param v new value
     */
    public void setStartDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.startDate, v))
              {
            this.startDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EndDate
     *
     * @return Date
     */
    public Date getEndDate()
    {
        return endDate;
    }

                        
    /**
     * Set the value of EndDate
     *
     * @param v new value
     */
    public void setEndDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.endDate, v))
              {
            this.endDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Title
     *
     * @return String
     */
    public String getTitle()
    {
        return title;
    }

                        
    /**
     * Set the value of Title
     *
     * @param v new value
     */
    public void setTitle(String v) 
    {
    
                  if (!ObjectUtils.equals(this.title, v))
              {
            this.title = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Contents
     *
     * @return String
     */
    public String getContents()
    {
        return contents;
    }

                        
    /**
     * Set the value of Contents
     *
     * @param v new value
     */
    public void setContents(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contents, v))
              {
            this.contents = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerTypeId
     *
     * @return String
     */
    public String getCustomerTypeId()
    {
        return customerTypeId;
    }

                        
    /**
     * Set the value of CustomerTypeId
     *
     * @param v new value
     */
    public void setCustomerTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerTypeId, v))
              {
            this.customerTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateBy
     *
     * @return String
     */
    public String getLastUpdateBy()
    {
        return lastUpdateBy;
    }

                        
    /**
     * Set the value of LastUpdateBy
     *
     * @param v new value
     */
    public void setLastUpdateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateBy, v))
              {
            this.lastUpdateBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdate
     *
     * @return Date
     */
    public Date getLastUpdate()
    {
        return lastUpdate;
    }

                        
    /**
     * Set the value of LastUpdate
     *
     * @param v new value
     */
    public void setLastUpdate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdate, v))
              {
            this.lastUpdate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("InvoiceMessageId");
              fieldNames.add("StartDate");
              fieldNames.add("EndDate");
              fieldNames.add("Title");
              fieldNames.add("Contents");
              fieldNames.add("CustomerTypeId");
              fieldNames.add("LocationId");
              fieldNames.add("LastUpdateBy");
              fieldNames.add("LastUpdate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("InvoiceMessageId"))
        {
                return getInvoiceMessageId();
            }
          if (name.equals("StartDate"))
        {
                return getStartDate();
            }
          if (name.equals("EndDate"))
        {
                return getEndDate();
            }
          if (name.equals("Title"))
        {
                return getTitle();
            }
          if (name.equals("Contents"))
        {
                return getContents();
            }
          if (name.equals("CustomerTypeId"))
        {
                return getCustomerTypeId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LastUpdateBy"))
        {
                return getLastUpdateBy();
            }
          if (name.equals("LastUpdate"))
        {
                return getLastUpdate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(InvoiceMessagePeer.INVOICE_MESSAGE_ID))
        {
                return getInvoiceMessageId();
            }
          if (name.equals(InvoiceMessagePeer.START_DATE))
        {
                return getStartDate();
            }
          if (name.equals(InvoiceMessagePeer.END_DATE))
        {
                return getEndDate();
            }
          if (name.equals(InvoiceMessagePeer.TITLE))
        {
                return getTitle();
            }
          if (name.equals(InvoiceMessagePeer.CONTENTS))
        {
                return getContents();
            }
          if (name.equals(InvoiceMessagePeer.CUSTOMER_TYPE_ID))
        {
                return getCustomerTypeId();
            }
          if (name.equals(InvoiceMessagePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(InvoiceMessagePeer.LAST_UPDATE_BY))
        {
                return getLastUpdateBy();
            }
          if (name.equals(InvoiceMessagePeer.LAST_UPDATE))
        {
                return getLastUpdate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getInvoiceMessageId();
            }
              if (pos == 1)
        {
                return getStartDate();
            }
              if (pos == 2)
        {
                return getEndDate();
            }
              if (pos == 3)
        {
                return getTitle();
            }
              if (pos == 4)
        {
                return getContents();
            }
              if (pos == 5)
        {
                return getCustomerTypeId();
            }
              if (pos == 6)
        {
                return getLocationId();
            }
              if (pos == 7)
        {
                return getLastUpdateBy();
            }
              if (pos == 8)
        {
                return getLastUpdate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(InvoiceMessagePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        InvoiceMessagePeer.doInsert((InvoiceMessage) this, con);
                        setNew(false);
                    }
                    else
                    {
                        InvoiceMessagePeer.doUpdate((InvoiceMessage) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key invoiceMessageId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setInvoiceMessageId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setInvoiceMessageId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getInvoiceMessageId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public InvoiceMessage copy() throws TorqueException
    {
        return copyInto(new InvoiceMessage());
    }
  
    protected InvoiceMessage copyInto(InvoiceMessage copyObj) throws TorqueException
    {
          copyObj.setInvoiceMessageId(invoiceMessageId);
          copyObj.setStartDate(startDate);
          copyObj.setEndDate(endDate);
          copyObj.setTitle(title);
          copyObj.setContents(contents);
          copyObj.setCustomerTypeId(customerTypeId);
          copyObj.setLocationId(locationId);
          copyObj.setLastUpdateBy(lastUpdateBy);
          copyObj.setLastUpdate(lastUpdate);
  
                    copyObj.setInvoiceMessageId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public InvoiceMessagePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("InvoiceMessage\n");
        str.append("--------------\n")
           .append("InvoiceMessageId     : ")
           .append(getInvoiceMessageId())
           .append("\n")
           .append("StartDate            : ")
           .append(getStartDate())
           .append("\n")
           .append("EndDate              : ")
           .append(getEndDate())
           .append("\n")
           .append("Title                : ")
           .append(getTitle())
           .append("\n")
           .append("Contents             : ")
           .append(getContents())
           .append("\n")
           .append("CustomerTypeId       : ")
           .append(getCustomerTypeId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LastUpdateBy         : ")
           .append(getLastUpdateBy())
           .append("\n")
           .append("LastUpdate           : ")
           .append(getLastUpdate())
           .append("\n")
        ;
        return(str.toString());
    }
}

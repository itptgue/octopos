package com.ssti.enterprise.pos.presentation.screens.report.custom.system;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import bsh.EvalError;
import bsh.Interpreter;

import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/custom/Default.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: Default.java,v 1.4 2009/05/04 02:01:17 albert Exp $
 *
 * $Log: Default.java,v $
 *
 */

public class BSHScriptTester extends SecureScreen
{		
	public void doBuildTemplate (RunData data, Context context)
	{			
		int op = data.getParameters().getInt("op");
		if (op == 1)
		{
			String script = data.getParameters().getString("script");
			if (StringUtil.isNotEmpty(script))
			{
				try 
				{
					Interpreter i = new Interpreter();
					i.eval(script);
					Object oResult = i.eval(script);
					data.setMessage("Script Evaluation Result : " + oResult); 
				} 
				catch (EvalError e) 
				{
					data.setMessage("ERROR: Evaluating Script : " + e.getMessage()); 				
					e.printStackTrace();
				}
			}
		}
	}
}

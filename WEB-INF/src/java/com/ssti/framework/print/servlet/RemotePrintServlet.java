package com.ssti.framework.print.servlet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import com.ssti.framework.networking.SimpleMessageServer;
import com.ssti.framework.pos.displayer.Displayer;
import com.ssti.framework.pos.displayer.DisplayerTool;
import com.ssti.framework.pos.drawer.DrawerTool;
import com.ssti.framework.print.helper.PrintAttributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: I/O Servlet to use in client application <br>
 * to access printer and any devices in other client's port
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: RemotePrintServlet.java,v 1.9 2008/06/29 07:11:11 albert Exp $ <br>
 *
 * <pre>
 * $Log: RemotePrintServlet.java,v $
 * 
 * 2016-12-11
 * - Change doPost output to text/plain
 *   1. SUCCESS if everything is OK
 *   2. ERROR: if error happend will return with error message and stack trace
 *   3. Remote re-routing capabilities, must use remote-local setting in server printer.host
 *   4. Remove network & firewall configuration bottleneck with remote-local
 *   
 * </pre><br>
 */
public class RemotePrintServlet extends HttpServlet implements PrintAttributes
{    
    private String m_sPort = "";
    private String m_sDocs = "";
    private boolean m_bDebug = false;
    
    private static final String s_CONFIG  = "/POS.properties";
    
    private static final String s_PRINTER_PORT = "printer.port";
    private static final String s_HOST_CONFIG = "printer.host";
    private static final String s_SERVER_IP = "printer.host.ip";
    private static final String s_CUTPAPER = "printer.cutpaper.command";
    private static final String s_MSG_SERVER = "use.message.server";
    
    private static Properties m_oPropConfig = null;
    private boolean m_bRemote = false;
    private static String m_sServerIP = "localhost";    
    private static StringBuilder m_sConf;
    private static DrawerTool m_oDrawer;
    private static byte[] m_bCutPaper = null;
    
    public void init()
    	throws ServletException
    {
    	m_sConf = new StringBuilder();
    	m_sConf.append("\n** POS Client Config File : " + getServletContext().getRealPath(s_CONFIG));
    	
    	try
		{
    		if (m_oPropConfig == null)
    		{
    			m_oPropConfig = new Properties(); 
    			m_oPropConfig.load(new FileInputStream(getServletContext().getRealPath(s_CONFIG)));
    		}
    		m_bRemote = m_oPropConfig.getProperty(s_HOST_CONFIG,"local").equals("remote");
    		m_sServerIP = m_oPropConfig.getProperty(s_SERVER_IP,"localhost");
    		m_sPort = m_oPropConfig.getProperty(s_PRINTER_PORT,"LPT1");
    		m_oDrawer = new DrawerTool(m_oPropConfig);    		
    		
    		m_sConf.append("\nPrinter Port : " + m_sPort); 		
    		m_sConf.append("\nDrawer Config : \n");						
    		m_sConf.append(m_oDrawer.displayConfig());			
    		m_sConf.append("\nDisplayer Config : \n");						
    		m_sConf.append(DisplayerTool.getInstance(m_oPropConfig).displayConfig());			
    		System.out.println (m_sConf);
    		
    		if(m_oPropConfig.containsKey("debug.mode"))
    		{
    			m_bDebug = Boolean.valueOf(m_oPropConfig.getProperty("debug.mode"));
    		}
    	}
    	catch (Exception _oEx)
		{
    		_oEx.printStackTrace();
    		throw new ServletException ("Initializing RemotePrintServlet Failed : " + _oEx.getMessage());
		}
    	
    	try 
    	{
    		if (m_oPropConfig != null && Boolean.valueOf(m_oPropConfig.getProperty(s_MSG_SERVER)))
    		{
	    		SimpleMessageServer messageServer = new SimpleMessageServer();
	    		messageServer.start();
    		}
		} 
    	catch(Exception _oEx) 
    	{
    		System.err.println("ERROR Starting Message Server: " + _oEx.getMessage());
    		_oEx.printStackTrace();
		}
	}
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException 
	{
		try
		{
			if(request.getParameter(s_DEST) != null)
			{
				doPost(request, response);
			}
			else
			{
				PrintWriter out = response.getWriter(); 
				setHeader(response);      				
				out.println(m_sConf);
				out.flush();			
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
		}
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException 
	{
		PrintWriter out = null;
	    try 
	    {
	    	setHeader(response);
	    	out = response.getWriter();
	    	
	    	int iType = Integer.parseInt(request.getParameter(s_DEST));

	    	String sType = "PRINTER";
	    	if(iType == i_DISPLAYER) sType = "DISPLAYER";
	    	if(iType == i_DRAWER)    sType = "DRAWER";
	    	
	    	System.out.println(new Date() + 
				" - POS CLIENT RECEIVED REQUEST FROM: " + request.getRemoteAddr() + " REQUEST TYPE/DEST: " + sType);

	    	//now everything goes to local device
	    	if (iType == i_PRINTER)
	    	{
	    		printText (request, response);
	    	}
	    	if (iType == i_DISPLAYER)
	    	{
	    		displayText (request, response);
	    	}
	    	if (iType == i_DRAWER)
	    	{
	    		openDrawer();
	    	}	    	
	        out.println("SUCCESS");
	    }
	    catch (Exception _oEx) 
		{	        	        	    	
	    	out.println("ERROR:" + _oEx.getMessage());	    	
	    	StackTraceElement[] el = _oEx.getStackTrace();
	    	for(int i = 0;i < el.length; i++)
	    	{	    	
	    		if(i <= 3) out.println(el[i]);
	    		if(i <= 7) System.err.println(el[i]);
	    	}
	    }
	}

	private void setHeader(HttpServletResponse response)
	{
    	response.setHeader("Access-Control-Allow-Origin","*");
    	response.addHeader("Access-Control-Allow-Methods", "GET, POST");
        response.setContentType("text/plain");	        
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    private void printText (HttpServletRequest request, HttpServletResponse response) 
    	throws Exception
    {
    	//prepare data
    	m_sDocs = request.getParameter(s_DOCS);

    	if(m_bDebug)
    	{
    		System.out.println(s_DOCS +":");
    		System.out.println(m_sDocs);
    	}
    	
    	System.out.println("PrintServlet.printText");   
    	FileOutputStream oPrintStream = null;
    	try
    	{
    		//try to open drawer
    		//openDrawer();

    		//reroute again
        	//remote printing through HTTP
        	if (m_bRemote &&  m_sServerIP != null && 
        		!m_sServerIP.equals("localhost") &&
    			!m_sServerIP.equals("127.0.0.1")) //prevent forever looping from invalid config
        	{
        		System.out.println("Remote Printing to : " + m_sServerIP);
        		reroute();
        	}
        	else
        	{
        		System.out.println("Local Printing to : " + m_sPort);
        		
	    		//print to port
	    		oPrintStream = new FileOutputStream (m_sPort);
	    		byte[] aContent = m_sDocs.getBytes();
	    		oPrintStream.write(aContent);
        	}
        	
    		//cutpaper
    		//if (m_bCutPaper != null) oPrintStream.write (m_bCutPaper);   			
    	}
    	catch (Exception _oEx) 
    	{
	    	StackTraceElement[] el = _oEx.getStackTrace();
	    	for(int i = 0;i < el.length; i++)
	    	{	    	
	    		if(i <= 7) System.err.println(el[i]);
	    	}
	    	
    		String sMsg = "Printing to " + m_sPort + " Failed, ERROR : "  + _oEx.getMessage();
    		System.err.println(sMsg);
    		throw new Exception(sMsg, _oEx);
    	}		
    	finally
    	{
    		if(oPrintStream != null)
    		{
    			oPrintStream.close();
    			System.out.println("Close Port " + m_sPort);
    		}
    		else
    		{
    			System.out.println("Port is not opened");
    		}
    	}    	
    }
    
    private void reroute()
    	throws Exception
    {
    	String sClientURL = s_CLIENT_HTTP + m_sServerIP + s_CLIENT_PORT;
		try 
		{
			System.out.println ("Remote Printer URL To : " + sClientURL);
			
			PostMethod oPost =  new PostMethod (sClientURL);
			oPost.addParameter(RemotePrintServlet.s_DEST, Integer.valueOf(i_PRINTER).toString());
			oPost.addParameter(RemotePrintServlet.s_DOCS, m_sDocs);
			HttpClient oHTTPClient = new HttpClient();
			oHTTPClient.executeMethod(oPost);
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new Exception("Remote Printing To " + sClientURL + " Failed, ERROR : "  + _oEx.getMessage(), _oEx);				   		    		
		}
    }
    
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    private void displayText (HttpServletRequest request, HttpServletResponse response) 
		throws Exception
	{	
		try 
		{
	    	//prepare data
	    	int iCmd = Integer.parseInt(request.getParameter(s_CMD));
	    		    	
	    	System.out.println("PrintServlet.displayText, Command: " + iCmd);
	    	Displayer oDisplayer = DisplayerTool.getInstance(m_oPropConfig);
	    	if (iCmd == i_DISPLAY)
	    	{	    		
	    		System.out.println("[Display]");
	    		int iLine = 1;	
	    		String sDocs = "";
		    	if (request.getParameter("line") != null && request.getParameter(s_DOCS) != null) 
		    	{
		    		iLine = Integer.parseInt(request.getParameter("line"));
		    		sDocs = request.getParameter(s_DOCS);
		    		
		    		System.out.println("[Display] Line: " + iLine);
			    	System.out.println("[Display] Docs: " + sDocs);

		    		oDisplayer.display(iLine, sDocs);		    	
		    	}	    		
		    	else
		    	{		    		
		    		System.out.println("[Display] Line1: " + request.getParameter("line1"));
			    	System.out.println("[Display] Line2: " + request.getParameter("line2"));

		    		oDisplayer.display(request.getParameter("line1"), request.getParameter("line2"));		
		    	}	    		
	    	}
	    	else if (iCmd == i_RESET)
	    	{
	    		System.out.println("[Reset]");
	    		oDisplayer.resetScreen();	    		
	    	}
	    	else if (iCmd == i_CLEAR)
	    	{
	    		System.out.println("[Clear]");
	    		oDisplayer.clearScreen();	    		    		
	    	}
	    	else if (iCmd == i_WELCOME)
	    	{
	    		System.out.println("[Welcome]");
	    		System.out.println("[Welcome] Line1: " + request.getParameter("line1"));
		    	System.out.println("[Welcome] Line2: " + request.getParameter("line2"));
	    		
	    		oDisplayer.welcome(request.getParameter("line1"), request.getParameter("line2"));	    		    		
	    	}   	
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new Exception("Displaying " + m_sDocs + " To Displayer Failed, ERROR : " + _oEx.getMessage(), _oEx);				   
		}
	}	

    private void openDrawer()
	{
    	System.out.println("PrintServlet.openDrawer");    	
		if (m_oDrawer.useDrawer()) 
		{
			try
			{
				System.out.println("Open Drawer, Config : " + m_oDrawer.displayConfig()); 
				m_oDrawer.openDrawer();
			}
			catch (Exception _oEx)
			{
				System.out.println("Open Drawer FAILED : " + _oEx.getMessage());    			
				_oEx.printStackTrace();
			}
		}
	}
}

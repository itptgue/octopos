package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class CreditMemoView extends TransactionSecureScreen
{
	protected static final String[] a_PERM = {"View Credit Memo"};

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
        try 
    	{
        	super.doBuildTemplate(data, context);
        	LargeSelectHandler.handleLargeSelectParameter (data, context, "findCreditMemoResult", "CreditMemos");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}

package com.ssti.framework.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VillageMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.VillageMapBuilder";

    /**
     * Item
     * @deprecated use VillagePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "village";
    }

  
    /**
     * village.VILLAGE_ID
     * @return the column name for the VILLAGE_ID field
     * @deprecated use VillagePeer.village.VILLAGE_ID constant
     */
    public static String getVillage_VillageId()
    {
        return "village.VILLAGE_ID";
    }
  
    /**
     * village.DISTRICT_ID
     * @return the column name for the DISTRICT_ID field
     * @deprecated use VillagePeer.village.DISTRICT_ID constant
     */
    public static String getVillage_DistrictId()
    {
        return "village.DISTRICT_ID";
    }
  
    /**
     * village.VILLAGE
     * @return the column name for the VILLAGE field
     * @deprecated use VillagePeer.village.VILLAGE constant
     */
    public static String getVillage_Village()
    {
        return "village.VILLAGE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("village");
        TableMap tMap = dbMap.getTable("village");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("village.VILLAGE_ID", "");
                          tMap.addForeignKey(
                "village.DISTRICT_ID", "" , "district" ,
                "district_id");
                          tMap.addColumn("village.VILLAGE", "");
          }
}

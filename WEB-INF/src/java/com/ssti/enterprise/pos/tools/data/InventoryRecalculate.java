package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.Torque;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.InventoryLocationPeer;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class InventoryRecalculate extends BaseValidator implements AppAttributes
{
	static Log log = LogFactory.getLog(InventoryRecalculate.class);

	StringBuilder oSB = new StringBuilder();

	/**
	 * 
	 * @param _sFrom
	 * @param _sTo
	 * @throws Exception
	 */
	public void recalculate(String _sItemCode, String _sLocID) 
		throws Exception 
	{	
		Criteria oCrit = new Criteria();
		oCrit.add(ItemPeer.ITEM_TYPE, AppAttributes.i_INVENTORY_PART);
		oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
		if(StringUtil.isNotEmpty(_sItemCode))
		{
			oCrit.add(ItemPeer.ITEM_CODE,_sItemCode);
		}
		List vITM = ItemPeer.doSelect(oCrit);
		for (int i = 0; i < vITM.size(); i++)
		{
			Item oItem = (Item) vITM.get(i);			
			try 
			{
				Connection oConn = BaseTool.beginTrans();
				StopWatch s = new StopWatch();
				s.start("item");
				InventoryTransactionTool.recalculateAll(oItem.getItemId(),_sLocID,oConn);
				BaseTool.commit(oConn);
				s.stop("item");
				oSB.append(i + " ITEM : " + oItem.getItemCode() + " RECALCULATED IN " + s.result("item")).append("\n");										
			} 
			catch (Exception e) 
			{
				BaseTool.rollback(oConn);
				oSB.append("ITEM: " + oItem.getItemCode() + "ERROR : " + e.getMessage());
				e.printStackTrace();
			}
		}
	}	
	
    public void recalculate(String _sItemCode, String _sLocID, String _sTransNo, double _dCost) 
        throws Exception 
    {   
        Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.ITEM_TYPE, AppAttributes.i_INVENTORY_PART);
        oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
        if(StringUtil.isNotEmpty(_sItemCode))
        {
            oCrit.add(ItemPeer.ITEM_CODE,_sItemCode);
        }
        List vITM = ItemPeer.doSelect(oCrit);
        for (int i = 0; i < vITM.size(); i++)
        {
            Item oItem = (Item) vITM.get(i);            
            try 
            {
                Connection oConn = BaseTool.beginTrans();
                StopWatch s = new StopWatch();
                s.start("item");
                InventoryTransactionTool.recalculateFrom(oItem.getItemId(),_sLocID,_sTransNo,_dCost,oConn);
                BaseTool.commit(oConn);
                s.stop("item");
                oSB.append(i + " ITEM : " + oItem.getItemCode() + " RECALCULATED IN " + s.result("item")).append("\n");                                     
            } 
            catch (Exception e) 
            {
                BaseTool.rollback(oConn);
                oSB.append("ITEM: " + oItem.getItemCode() + "ERROR : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
	public String getResult()
	{
		if (oSB != null)
		{
			return oSB.toString();
		}
		return "";
	}
	
	public void updateInvLocFromInvTrans(String _sItemCode, String _sLocID) 
        throws Exception 
    {   
		Connection oConn = Torque.getConnection();
		
        Criteria oCrit = new Criteria();
        oCrit.addAscendingOrderByColumn(InventoryLocationPeer.ITEM_CODE);
        oCrit.add(InventoryLocationPeer.LOCATION_ID,_sLocID);
        if(StringUtil.isNotEmpty(_sItemCode))
        {
            oCrit.add(InventoryLocationPeer.ITEM_CODE,(Object)_sItemCode, Criteria.ILIKE);
        }        
        List vITM = InventoryLocationPeer.doSelect(oCrit, oConn);
        for (int i = 0; i < vITM.size(); i++)
        {
        	InventoryLocation oIL = (InventoryLocation) vITM.get(i);            
            try 
            {
                StopWatch s = new StopWatch();
                s.start("item");
                
                InventoryTransaction oIT = 
                	InventoryTransactionTool.getLastTrans(oIL.getItemId(), oIL.getLocationId(), new Date(), oConn);
                
                if (oIT != null)
                {
                	oIL.setCurrentQty(oIT.getQtyBalance());
                	double dQty = oIT.getQtyBalance().doubleValue();
                	double dCost = oIT.getCost().doubleValue();
                	if (dQty > 0)
                	{
                		dCost = Calculator.div(oIT.getValueBalance(), oIT.getQtyBalance());
                	}
                	oIL.setItemCost(new BigDecimal(dCost));
                }
                oIL.save(oConn);
                
                s.stop("item");
                oSB.append(i + " ITEM : " + oIL.getItemCode() + " INV LOC QTY (" + oIL.getCurrentQty() + ") UPDATED IN " + s.result("item")).append("\n");                                     
            } 
            catch (Exception e) 
            {
                oSB.append("ITEM: " + oIL.getItemCode() + "ERROR : " + e.getMessage());
                e.printStackTrace();
            }
        }
        try
		{
			oConn.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
    }
}

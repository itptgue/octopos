package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class IssueReceiptMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.IssueReceiptMapBuilder";

    /**
     * Item
     * @deprecated use IssueReceiptPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "issue_receipt";
    }

  
    /**
     * issue_receipt.ISSUE_RECEIPT_ID
     * @return the column name for the ISSUE_RECEIPT_ID field
     * @deprecated use IssueReceiptPeer.issue_receipt.ISSUE_RECEIPT_ID constant
     */
    public static String getIssueReceipt_IssueReceiptId()
    {
        return "issue_receipt.ISSUE_RECEIPT_ID";
    }
  
    /**
     * issue_receipt.ADJUSTMENT_TYPE_ID
     * @return the column name for the ADJUSTMENT_TYPE_ID field
     * @deprecated use IssueReceiptPeer.issue_receipt.ADJUSTMENT_TYPE_ID constant
     */
    public static String getIssueReceipt_AdjustmentTypeId()
    {
        return "issue_receipt.ADJUSTMENT_TYPE_ID";
    }
  
    /**
     * issue_receipt.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use IssueReceiptPeer.issue_receipt.TRANSACTION_DATE constant
     */
    public static String getIssueReceipt_TransactionDate()
    {
        return "issue_receipt.TRANSACTION_DATE";
    }
  
    /**
     * issue_receipt.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use IssueReceiptPeer.issue_receipt.TRANSACTION_NO constant
     */
    public static String getIssueReceipt_TransactionNo()
    {
        return "issue_receipt.TRANSACTION_NO";
    }
  
    /**
     * issue_receipt.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use IssueReceiptPeer.issue_receipt.TRANSACTION_TYPE constant
     */
    public static String getIssueReceipt_TransactionType()
    {
        return "issue_receipt.TRANSACTION_TYPE";
    }
  
    /**
     * issue_receipt.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use IssueReceiptPeer.issue_receipt.LOCATION_ID constant
     */
    public static String getIssueReceipt_LocationId()
    {
        return "issue_receipt.LOCATION_ID";
    }
  
    /**
     * issue_receipt.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use IssueReceiptPeer.issue_receipt.LOCATION_NAME constant
     */
    public static String getIssueReceipt_LocationName()
    {
        return "issue_receipt.LOCATION_NAME";
    }
  
    /**
     * issue_receipt.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use IssueReceiptPeer.issue_receipt.USER_NAME constant
     */
    public static String getIssueReceipt_UserName()
    {
        return "issue_receipt.USER_NAME";
    }
  
    /**
     * issue_receipt.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use IssueReceiptPeer.issue_receipt.DESCRIPTION constant
     */
    public static String getIssueReceipt_Description()
    {
        return "issue_receipt.DESCRIPTION";
    }
  
    /**
     * issue_receipt.STATUS
     * @return the column name for the STATUS field
     * @deprecated use IssueReceiptPeer.issue_receipt.STATUS constant
     */
    public static String getIssueReceipt_Status()
    {
        return "issue_receipt.STATUS";
    }
  
    /**
     * issue_receipt.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use IssueReceiptPeer.issue_receipt.ACCOUNT_ID constant
     */
    public static String getIssueReceipt_AccountId()
    {
        return "issue_receipt.ACCOUNT_ID";
    }
  
    /**
     * issue_receipt.COST_ADJUSTMENT
     * @return the column name for the COST_ADJUSTMENT field
     * @deprecated use IssueReceiptPeer.issue_receipt.COST_ADJUSTMENT constant
     */
    public static String getIssueReceipt_CostAdjustment()
    {
        return "issue_receipt.COST_ADJUSTMENT";
    }
  
    /**
     * issue_receipt.CONFIRM_BY
     * @return the column name for the CONFIRM_BY field
     * @deprecated use IssueReceiptPeer.issue_receipt.CONFIRM_BY constant
     */
    public static String getIssueReceipt_ConfirmBy()
    {
        return "issue_receipt.CONFIRM_BY";
    }
  
    /**
     * issue_receipt.CONFIRM_DATE
     * @return the column name for the CONFIRM_DATE field
     * @deprecated use IssueReceiptPeer.issue_receipt.CONFIRM_DATE constant
     */
    public static String getIssueReceipt_ConfirmDate()
    {
        return "issue_receipt.CONFIRM_DATE";
    }
  
    /**
     * issue_receipt.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use IssueReceiptPeer.issue_receipt.CANCEL_BY constant
     */
    public static String getIssueReceipt_CancelBy()
    {
        return "issue_receipt.CANCEL_BY";
    }
  
    /**
     * issue_receipt.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use IssueReceiptPeer.issue_receipt.CANCEL_DATE constant
     */
    public static String getIssueReceipt_CancelDate()
    {
        return "issue_receipt.CANCEL_DATE";
    }
  
    /**
     * issue_receipt.INTERNAL_TRANSFER
     * @return the column name for the INTERNAL_TRANSFER field
     * @deprecated use IssueReceiptPeer.issue_receipt.INTERNAL_TRANSFER constant
     */
    public static String getIssueReceipt_InternalTransfer()
    {
        return "issue_receipt.INTERNAL_TRANSFER";
    }
  
    /**
     * issue_receipt.REQUEST_ID
     * @return the column name for the REQUEST_ID field
     * @deprecated use IssueReceiptPeer.issue_receipt.REQUEST_ID constant
     */
    public static String getIssueReceipt_RequestId()
    {
        return "issue_receipt.REQUEST_ID";
    }
  
    /**
     * issue_receipt.CROSS_ENTITY_ID
     * @return the column name for the CROSS_ENTITY_ID field
     * @deprecated use IssueReceiptPeer.issue_receipt.CROSS_ENTITY_ID constant
     */
    public static String getIssueReceipt_CrossEntityId()
    {
        return "issue_receipt.CROSS_ENTITY_ID";
    }
  
    /**
     * issue_receipt.CROSS_ENTITY_CODE
     * @return the column name for the CROSS_ENTITY_CODE field
     * @deprecated use IssueReceiptPeer.issue_receipt.CROSS_ENTITY_CODE constant
     */
    public static String getIssueReceipt_CrossEntityCode()
    {
        return "issue_receipt.CROSS_ENTITY_CODE";
    }
  
    /**
     * issue_receipt.CROSS_TRANS_ID
     * @return the column name for the CROSS_TRANS_ID field
     * @deprecated use IssueReceiptPeer.issue_receipt.CROSS_TRANS_ID constant
     */
    public static String getIssueReceipt_CrossTransId()
    {
        return "issue_receipt.CROSS_TRANS_ID";
    }
  
    /**
     * issue_receipt.CROSS_TRANS_NO
     * @return the column name for the CROSS_TRANS_NO field
     * @deprecated use IssueReceiptPeer.issue_receipt.CROSS_TRANS_NO constant
     */
    public static String getIssueReceipt_CrossTransNo()
    {
        return "issue_receipt.CROSS_TRANS_NO";
    }
  
    /**
     * issue_receipt.CROSS_CONFIRM_BY
     * @return the column name for the CROSS_CONFIRM_BY field
     * @deprecated use IssueReceiptPeer.issue_receipt.CROSS_CONFIRM_BY constant
     */
    public static String getIssueReceipt_CrossConfirmBy()
    {
        return "issue_receipt.CROSS_CONFIRM_BY";
    }
  
    /**
     * issue_receipt.CROSS_CONFIRM_DATE
     * @return the column name for the CROSS_CONFIRM_DATE field
     * @deprecated use IssueReceiptPeer.issue_receipt.CROSS_CONFIRM_DATE constant
     */
    public static String getIssueReceipt_CrossConfirmDate()
    {
        return "issue_receipt.CROSS_CONFIRM_DATE";
    }
  
    /**
     * issue_receipt.FILE_NAME
     * @return the column name for the FILE_NAME field
     * @deprecated use IssueReceiptPeer.issue_receipt.FILE_NAME constant
     */
    public static String getIssueReceipt_FileName()
    {
        return "issue_receipt.FILE_NAME";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("issue_receipt");
        TableMap tMap = dbMap.getTable("issue_receipt");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("issue_receipt.ISSUE_RECEIPT_ID", "");
                          tMap.addColumn("issue_receipt.ADJUSTMENT_TYPE_ID", "");
                          tMap.addColumn("issue_receipt.TRANSACTION_DATE", new Date());
                          tMap.addColumn("issue_receipt.TRANSACTION_NO", "");
                            tMap.addColumn("issue_receipt.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("issue_receipt.LOCATION_ID", "");
                          tMap.addColumn("issue_receipt.LOCATION_NAME", "");
                          tMap.addColumn("issue_receipt.USER_NAME", "");
                          tMap.addColumn("issue_receipt.DESCRIPTION", "");
                            tMap.addColumn("issue_receipt.STATUS", Integer.valueOf(0));
                          tMap.addColumn("issue_receipt.ACCOUNT_ID", "");
                          tMap.addColumn("issue_receipt.COST_ADJUSTMENT", Boolean.TRUE);
                          tMap.addColumn("issue_receipt.CONFIRM_BY", "");
                          tMap.addColumn("issue_receipt.CONFIRM_DATE", new Date());
                          tMap.addColumn("issue_receipt.CANCEL_BY", "");
                          tMap.addColumn("issue_receipt.CANCEL_DATE", new Date());
                          tMap.addColumn("issue_receipt.INTERNAL_TRANSFER", Boolean.TRUE);
                          tMap.addColumn("issue_receipt.REQUEST_ID", "");
                          tMap.addColumn("issue_receipt.CROSS_ENTITY_ID", "");
                          tMap.addColumn("issue_receipt.CROSS_ENTITY_CODE", "");
                          tMap.addColumn("issue_receipt.CROSS_TRANS_ID", "");
                          tMap.addColumn("issue_receipt.CROSS_TRANS_NO", "");
                          tMap.addColumn("issue_receipt.CROSS_CONFIRM_BY", "");
                          tMap.addColumn("issue_receipt.CROSS_CONFIRM_DATE", new Date());
                          tMap.addColumn("issue_receipt.FILE_NAME", "");
          }
}

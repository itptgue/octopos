package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.Torque;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.GlTransactionCancel;
import com.ssti.enterprise.pos.om.GlTransactionPeer;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.gl.AccountBalanceTool;
import com.ssti.enterprise.pos.tools.gl.AccountInvalidException;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base Journal Tool
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BaseJournalTool.java,v 1.18 2008/06/29 07:15:44 albert Exp $ <br>
 *
 * <pre>
 * $Log: BaseJournalTool.java,v $
 * Revision 1.18  2008/06/29 07:15:44  albert
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/03 03:14:35  albert
 * *** empty log message ***
 *
 * Revision 1.16  2008/01/11 06:40:47  albert
 * *** empty log message ***
 *
 * Revision 1.15  2007/04/17 13:09:25  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/03/05 16:10:03  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public abstract class BaseJournalTool extends BaseTool implements GlAttributes
{
	private static Log log = LogFactory.getLog ( BaseJournalTool.class );
	
	//-------------------------------------------------------------------------
	//general journal properties
	//-------------------------------------------------------------------------
	protected Connection m_oConn = null;
	
	protected String m_sTransID = null;
	protected String m_sTransNo = null;
	protected String m_sUserName = null;
	protected String m_sLocationID = null;
	protected String m_sRemark = null;
	
	protected int m_iSubLedger = 0;
	protected String m_sSubLedgerID = "";
	
	protected Date m_dTransDate = null;	
	protected Date m_dCreate = null;

	protected Period m_oPeriod = null;
	
	protected void setGLTrans (List _vGLTrans)
	{
		for (int i = 0; i < _vGLTrans.size(); i++)
		{
			GlTransaction oGLTrans = (GlTransaction) _vGLTrans.get(i);
			oGLTrans.setTransactionId    (m_sTransID);
			oGLTrans.setTransactionNo    (m_sTransNo);
			oGLTrans.setGlTransactionNo  (m_sTransNo);
			oGLTrans.setTransactionDate  (m_dTransDate);		
			oGLTrans.setPeriodId         (m_oPeriod.getPeriodId());
			
			oGLTrans.setUserName         (m_sUserName);
			oGLTrans.setCreateDate       (m_dCreate);
			
			if (StringUtil.isNotEmpty(m_sSubLedgerID) && m_iSubLedger > 0)
			{
				oGLTrans.setSubLedgerId(m_sSubLedgerID);
				oGLTrans.setSubLedgerType(m_iSubLedger);
			}
			
			//item transfer location is different between from and to so do not set here
			if (oGLTrans.getTransactionType() != i_GL_TRANS_ITEM_TRANSFER)
			{
				oGLTrans.setLocationId (m_sLocationID);
				oGLTrans.setDescription(m_sRemark);
			}
			_vGLTrans.set(i, oGLTrans);
		}
	}
	//end
	
	public void validate(Account _oAccount, String _sCode, String _sField)
		throws Exception
	{		
		if (_oAccount == null)
		{
			StringBuilder oError = new StringBuilder();
			oError.append ("GL Account setting in " )
				  .append (_sField)
				  .append (" with code : ")
				  .append (_sCode)
				  .append (" is Invalid (null). ")
			;	  
			log.error(oError.toString());
			throw new AccountInvalidException (oError.toString());
		}
		else //validate if parent Account
		{
			if (_oAccount.getHasChild())
			{
				StringBuilder oError = new StringBuilder();
				oError.append ("Account " )
					  .append (_oAccount.getAccountCode())
					  .append (" - ")
					  .append (_oAccount.getAccountName())
					  .append (" is Parent Account. ")
				;	  
				log.error(oError.toString());
				throw new AccountInvalidException (oError.toString());
			}
		}
	}	
	
    /**
     * add journal to list
     *
     * @param _oTrans GlTransaction to add
     * @param _vGLTrans List of vGLTrans to add to
     */    
	protected static void addJournal (GlTransaction _oTrans, Account _oAcc, List _vGLTrans) 
    	throws Exception
    {
        if (!isExist ( _oTrans, _oAcc, _vGLTrans))
        {
            _vGLTrans.add (_oTrans);
        }
	}

    /**
     * check whether current gl trans already exist in list 
     * if true then merge it
     *
     * @param _oTrans GlTransaction to validate
     * @param _vGLTrans List of vGLTrans
     */    
	protected static boolean isExist(GlTransaction _oTrans, Account _oAcc, List _vGLTrans) 
        throws Exception
    {
        for (int i = 0; i < _vGLTrans.size(); i++)
        {
            GlTransaction oExist = (GlTransaction) _vGLTrans.get(i);
            if ( _oTrans.getAccountId().equals(oExist.getAccountId()) && 
                 _oTrans.getCurrencyId().equals(oExist.getCurrencyId()) &&
                 _oTrans.getLocationId().equals(oExist.getLocationId()) &&                 
                 _oTrans.getDescription().equals(oExist.getDescription()) &&
                 _oTrans.getCurrencyRate().doubleValue() == oExist.getCurrencyRate().doubleValue() &&                 
                 _oTrans.getDebitCredit() == oExist.getDebitCredit())
            {
            	if (PreferenceTool.getMultiDept() && _oAcc.getAccountType() >= i_REVENUE) //if P&L account 
            	{
            		//validate whether project & department equals
            		if (_oTrans.getDepartmentId().equals(oExist.getDepartmentId()) && 
            			_oTrans.getProjectId().equals(oExist.getProjectId()))
	            	{
		                mergeJournal (oExist, _oTrans);
		                return true;
	            	}            			
            	}
            	else
            	{
	                mergeJournal (oExist, _oTrans);
	                return true;
            	}
            }
        }    
        return false;
    }
    
    /**
     * merge journal with same account ID and debit credit entry
     * add amount and amount base in new gl trans into the existing
     *
     * @param _oExist Existing Certain ACCOUNT GlTransaction in List
     * @param _oNew New Certain ACCOUNT GlTransaction to be merged with existing
     */    
     
	protected static void mergeJournal (GlTransaction _oExist, GlTransaction _oNew) 
    	throws Exception
    {
        double dExistAmount     = _oExist.getAmount().doubleValue();
        double dExistAmountBase = _oExist.getAmountBase().doubleValue();
        double dNewAmount       = _oNew.getAmount().doubleValue();
        double dNewAmountBase   = _oNew.getAmountBase().doubleValue();
        
        dExistAmount     = dExistAmount + dNewAmount;
        dExistAmountBase = dExistAmountBase + dNewAmountBase;
        
        _oExist.setAmount     ( new BigDecimal (dExistAmount)     );
        _oExist.setAmountBase ( new BigDecimal (dExistAmountBase) );            
	}
	

	/**
	 * delete transaction Journal (GLTransaction) and update each account balance
	 * 
	 * @param _iTransType TransType in AppAttributes
	 * @param _sTransID TransactionID
	 * @param _oConn
	 * @throws Exception
	 */	
	public static void deleteJournal(int _iTransType, String _sTransID, Connection _oConn) 
    	throws Exception
    {
		validate(_oConn);
		List vOldGLTrans = null;
        try
        {
        	vOldGLTrans = GlTransactionTool.getDataByTypeAndTransID(_iTransType, _sTransID, _oConn);
        	if (vOldGLTrans.size() > 0)
        	{
        		//check date before delete
        		for (int i = 0; i < vOldGLTrans.size(); i++)
	            {
        			GlTransaction oGLTrans = (GlTransaction) vOldGLTrans.get(i);
        			if(oGLTrans.getTransactionType() != i_GL_TRANS_JOURNAL_VOUCHER) //JV is used in End year 
        			{
        				validateDate(oGLTrans.getTransactionDate(), _oConn); 
        			}
	            }
        		//rollback balance
        		AccountBalanceTool.rollbackBalance(vOldGLTrans, _oConn);
	                        
        		//delete gl trans
	            for (int i = 0; i < vOldGLTrans.size(); i++)
	            {
	            	GlTransaction oGLTrans = (GlTransaction) vOldGLTrans.get(i);
	            	Criteria oCrit = new Criteria();
	            	oCrit.add (GlTransactionPeer.GL_TRANSACTION_ID, oGLTrans.getGlTransactionId());
	            	GlTransactionPeer.doDelete(oCrit, _oConn);
	            	
	            	if (log.isDebugEnabled()) log.debug ("oGLTrans TO DELETE : " + oGLTrans );
	            }
        	}
        }
        catch (Exception _oEx)
        {
            log.error(_oEx);
			_oEx.printStackTrace();
            throw new JournalFailedException ("Delete Transaction Journal Failed : " + _oEx.getMessage(), _oEx);
        }   
        
        backupJournal(vOldGLTrans);
	}	

    /**
     * delete transaction Journal (GLTransaction) and update each account balance
     * 
     * @param _iTransType TransType in AppAttributes
     * @param _sTransID TransactionID
     * @param _oConn
     * @throws Exception
     */ 
    public static void deleteJournal(int _iTransType, String _sTransID, String _sTransNo, Connection _oConn) 
        throws Exception
    {
        validate(_oConn);
        List vOldGLTrans = null;
        try
        {
            vOldGLTrans = GlTransactionTool.getDataByTypeAndTransID(_iTransType, _sTransID, _sTransNo, _oConn);
            if (vOldGLTrans.size() > 0)
            {
            	//check date before delete
        		for (int i = 0; i < vOldGLTrans.size(); i++)
	            {
        			GlTransaction oGLTrans = (GlTransaction) vOldGLTrans.get(i);
	            	validateDate(oGLTrans.getTransactionDate(), _oConn);
	            }
                AccountBalanceTool.rollbackBalance(vOldGLTrans, _oConn);                            
                for (int i = 0; i < vOldGLTrans.size(); i++)
                {
                    GlTransaction oGLTrans = (GlTransaction) vOldGLTrans.get(i);
                    Criteria oCrit = new Criteria();
                    oCrit.add (GlTransactionPeer.GL_TRANSACTION_ID, oGLTrans.getGlTransactionId());
                    GlTransactionPeer.doDelete(oCrit, _oConn);
                    
                    if (log.isDebugEnabled()) log.debug ("oGLTrans TO DELETE : " + oGLTrans );
                }
            }
        }
        catch (Exception _oEx)
        {
            log.error(_oEx);
            _oEx.printStackTrace();
            throw new JournalFailedException ("Delete Transaction Journal Failed : " + _oEx.getMessage(), _oEx);
        }           
        backupJournal(vOldGLTrans);
    }   
    
	/**
	 * backup cancelled / deleted journal to gl_transaction_cancel table
	 * 
	 * @param vOldGLTrans
	 */
	private static void backupJournal (List vOldGLTrans)
	{
        //Copy cancelled journal to gl_transaction cancel
        if (vOldGLTrans != null && vOldGLTrans.size() > 0)
        {
        	Connection oCancelConn = null;
        	try 
        	{
        		oCancelConn = Torque.getConnection();
	            for (int i = 0; i < vOldGLTrans.size(); i++)
	            {
	            	GlTransactionCancel oCancelled = new GlTransactionCancel();
	            	GlTransaction oGLTrans = (GlTransaction) vOldGLTrans.get(i);
	            	BeanUtil.setBean2BeanProperties(oGLTrans, oCancelled);
	            	oCancelled.save(oCancelConn);	            	
	            }
			} 
        	catch (Exception _oEx) 
        	{
                log.error(_oEx);
                _oEx.printStackTrace();
			}
        	finally
        	{
        		if (oCancelConn != null)
        		{
        			Torque.closeConnection(oCancelConn);
        		}
        	}
        }
	}
}

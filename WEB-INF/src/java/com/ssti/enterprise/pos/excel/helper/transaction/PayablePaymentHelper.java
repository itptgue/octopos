package com.ssti.enterprise.pos.excel.helper.transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.ApPaymentDetail;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;

public class PayablePaymentHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Payable Payment Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {                
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        String sKeywords = (String) getFromParam(oParam, "Keywords", null);        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        String sLocationID = (String) getFromParam(oParam, "LocationId", null);
        String sVendorID = (String) getFromParam(oParam, "VendorId", null);
        int iStatus = (Integer) getFromParam(oParam, "Status", Integer.class);
        int iCFStatus = (Integer) getFromParam(oParam, "CfStatus", Integer.class);        
        String sCurrencyID = (String) getFromParam(oParam, "CurrencyId", null);
        Date dStartDue = CustomParser.parseDate((String)getFromParam(oParam, "StartDue", null));
        Date dEndDue = CustomParser.parseDate((String)getFromParam(oParam, "EndDue", null));
        
        List vData = PayablePaymentTool.findTrans(iCond, sKeywords, sVendorID, sLocationID, dStart, dEnd, 
        										  dStartDue, dEndDue, iStatus, iCFStatus, sCurrencyID);
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	ApPayment oTR = (ApPayment) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = PayablePaymentTool.getDetailsByID(oTR.getApPaymentId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		ApPaymentDetail oTD = (ApPaymentDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}

        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Vendor Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Bank Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Due Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cash Flow Type"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Reference No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Issuer Dest"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;
		
		if (TransactionAttributes.b_SALES_MULTI_CURRENCY)
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Currency Rate"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Fiscal Rate"); iCol++;
		}
    }
    
	private void createMasterContent(HSSFRow oRow, ApPayment oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getApPaymentNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, VendorTool.getVendorByID(oTR.getVendorId()).getVendorCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, BankTool.getBankByID(oTR.getBankId()).getBankCode()); iCol++;		
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getDueDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, CashFlowTypeTool.getTypeCodeByID(oTR.getCashFlowTypeId(),null)); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getReferenceNo()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getBankIssuer()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;
		
		if (TransactionAttributes.b_SALES_MULTI_CURRENCY)
		{
			createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTR.getCurrencyId())); iCol++;
			createCell(oWB, oRow, (short)iCol, oTR.getCurrencyRate().toString()); iCol++;	
			createCell(oWB, oRow, (short)iCol, oTR.getFiscalRate().toString()); iCol++;	
		}
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Invoice No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Amount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Invoice Rate"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Discount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Discount Account"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Debit Memo No"); iCol++;		
		if (PreferenceTool.getMultiDept())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Department"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Project"); iCol++;
		}
    }
    
    private void createDetailContent(HSSFRow oRow, ApPaymentDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	BigDecimal dDisc = oTD.getDiscountAmount();
    	if (dDisc == null) dDisc = bd_ZERO;
    	
    	BigDecimal dInvRate = oTD.getInvoiceRate();
    	if (dInvRate == null) dDisc = bd_ONE;
    	
    	createCell(oWB, oRow, (short)iCol, oTD.getPurchaseInvoiceNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getPaymentAmount().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, dInvRate.toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, dDisc.toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, AccountTool.getCodeByID(oTD.getDiscountAccountId())); iCol++;
		
		//TODO: display memo
		createCell(oWB, oRow, (short)iCol, ""); iCol++;		
    }
}

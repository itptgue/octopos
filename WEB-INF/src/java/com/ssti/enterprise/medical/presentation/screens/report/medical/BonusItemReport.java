package com.ssti.enterprise.medical.presentation.screens.report.medical;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseAnalysisTool;

public class BonusItemReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	        if (data.getParameters().getInt("Op") == 1)
    	    {
	    	    setParams(data);
	        	
	    	    String sKategoriID = data.getParameters().getString("KategoriId");
	    	    String sVendorID = data.getParameters().getString("VendorId","");
	    	    String sColor = data.getParameters().getString("Color","");
	    	    
	    	    List vResult = PurchaseAnalysisTool.getPurchaseItem(
	    	    	dStart, dEnd, sLocationID, sKategoriID, "", sVendorID, sColor, "","","", i_TRANS_PROCESSED);
	    	    
	    	    context.put ("vResult", vResult);
            }
	        MedConfigTool.setContextTool(context);
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
		}    
    }    
}

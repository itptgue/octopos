
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class Principal
    extends com.ssti.enterprise.pos.om.BasePrincipal
    implements Persistent,MasterOM
{

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return getPrincipalId();
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return getPrincipalCode();
	}

	public String getName() {
		// TODO Auto-generated method stub
		return getPrincipalName();
	}

}

package com.ssti.enterprise.pos.tools.purchase;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoiceExp;
import com.ssti.enterprise.pos.om.PurchaseInvoiceExpPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoiceFreight;
import com.ssti.enterprise.pos.om.PurchaseInvoiceFreightPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoicePeer;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.PurchaseReceiptPeer;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.AccountPayableTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryCostingTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.enterprise.pos.tools.journal.PurchaseJournalTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for PurchaseInvoice and PurchaseInvoiceDetail OM
 * @see PurchaseOrderTool, PurchaseReceiptTool, PurchaseInvoice, PurchaseInvoiceDetail
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseInvoiceTool.java,v 1.35 2009/05/04 02:04:54 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseInvoiceTool.java,v $
 * 
 * 2018-09-08
 * - Change in saveData, now create mPR before create journals
 * - Change in @see InventoryCostingTool add setUpdatePRCost and getUpdatePRCost to check whether PI should update PR Cost
 * - Change in @see PurchaseJournalTool, add Constructor Param bUpdatePRCost
 * 
 * 2018-08-14
 * - Refactor updateDetail method
 * - Refactor setHeader method 
 * - add method countTotal to final count AP amount from CostPerUnit
 * 
 * 2018-08-14
 * - change method calculateLastPurchase, set LastPurchase field, last purchase now in base unit 
 * 
 * 2018-06-26
 * - add method getLastPIDetByItemID
 * - change method saveData, use new InventoryCostingTool class for creating inventory transaction journal
 * - change method saveData, use new InventoryCostingTool class for creating inventory transaction journal
 * - change method cancelTrans, use new InventoryCostingTool class for deleting inventory transaction journal
 *
 * </pre><br>
 */
public class PurchaseInvoiceTool extends BaseTool 
{
	private static final Log log = LogFactory.getLog ( PurchaseInvoiceTool.class );
	
	public static final String s_ITEM = new StringBuilder(PurchaseInvoicePeer.PURCHASE_INVOICE_ID).append(",")
	.append(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID).append(",")
	.append(PurchaseInvoiceDetailPeer.ITEM_ID).toString();

	public static final String s_TAX = new StringBuilder(PurchaseInvoicePeer.PURCHASE_INVOICE_ID).append(",")
	.append(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID).append(",")
	.append(PurchaseInvoiceDetailPeer.TAX_ID).toString();
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), PurchaseInvoicePeer.PURCHASE_INVOICE_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), PurchaseInvoicePeer.REMARK);          
		m_FIND_PEER.put (Integer.valueOf(i_VENDOR	  ), PurchaseInvoicePeer.VENDOR_ID);
		m_FIND_PEER.put (Integer.valueOf(i_VEND_SI	  ), PurchaseInvoicePeer.VENDOR_INVOICE_NO);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), PurchaseInvoicePeer.CREATE_BY);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), PurchaseInvoicePeer.CREATE_BY);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), PurchaseInvoicePeer.REFERENCE_NO);             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), PurchaseInvoicePeer.LOCATION_ID);             
		
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), PurchaseInvoicePeer.PAYMENT_TYPE_ID);
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), PurchaseInvoicePeer.PAYMENT_TERM_ID);
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), PurchaseInvoicePeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), PurchaseInvoicePeer.FOB_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), PurchaseInvoicePeer.CURRENCY_ID);
        m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), PurchaseInvoicePeer.BANK_ID);		
        m_FIND_PEER.put (Integer.valueOf(i_PR_NO   	  ), PurchaseInvoiceDetailPeer.PURCHASE_RECEIPT_ID);	
        m_FIND_PEER.put (Integer.valueOf(i_PO_NO   	  ), PurchaseInvoiceDetailPeer.PURCHASE_ORDER_ID);	
                                            
        addInv(m_FIND_PEER, s_ITEM);
	}   
	
	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), PurchaseInvoicePeer.PURCHASE_INVOICE_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), PurchaseInvoicePeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), PurchaseInvoicePeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), PurchaseInvoicePeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), PurchaseInvoicePeer.CREATE_BY);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), PurchaseInvoicePeer.CREATE_BY);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), PurchaseInvoicePeer.PAYMENT_TYPE_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), PurchaseInvoicePeer.CURRENCY_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), PurchaseInvoicePeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), PurchaseInvoicePeer.LOCATION_ID); 
	}
    
    public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, true, _iSelected);}

	public static PurchaseInvoice getHeaderByID(String _sID)
		throws Exception
	{
		return getHeaderByID (_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return PurchaseInvoice object
	 * @throws Exception
	 */
	public static PurchaseInvoice getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseInvoicePeer.PURCHASE_INVOICE_ID, _sID);
        List vData = PurchaseInvoicePeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
			return (PurchaseInvoice) vData.get(0);
		}
		return null;
	}

	/**
	 * 
	 * @param _sInvoiceNo
	 * @return PurchaseInvoiceId
	 * @throws Exception
	 */
	public static String getIDByInvoiceNo(String _sInvoiceNo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseInvoicePeer.PURCHASE_INVOICE_NO, 
				(Object) SqlUtil.like (PurchaseInvoicePeer.PURCHASE_INVOICE_NO, _sInvoiceNo), Criteria.CUSTOM);
		oCrit.setLimit(1);
		List vData = PurchaseInvoicePeer.doSelect (oCrit);
		if (vData.size() > 0) 
		{
			return ((PurchaseInvoice) vData.get(0)).getPurchaseInvoiceId();
		}
		return "";
	}
	
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
		return getDetailsByID(_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of Purchase Invoice Details
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, _sID);
	    oCrit.addAscendingOrderByColumn(PurchaseInvoiceDetailPeer.INDEX_NO);	    
		return PurchaseInvoiceDetailPeer.doSelect(oCrit, _oConn);
	}
	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return PurchaseInvoiceDetail object
	 * @throws Exception
	 */
	public static PurchaseInvoiceDetail getDetailByDetailID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_DETAIL_ID, _sID);
	    List vData = PurchaseInvoiceDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) return (PurchaseInvoiceDetail) vData.get(0);
	    return null;
	}		
	
	private static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, _sID);
		PurchaseInvoiceDetailPeer.doDelete (oCrit, _oConn);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of Purchase Invoice Exps
	 * @throws Exception
	 */
	public static List getExpensesByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, _sID);
		return PurchaseInvoiceExpPeer.doSelect(oCrit, _oConn);
	}

	private static void deleteExpensesByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseInvoiceExpPeer.PURCHASE_INVOICE_ID, _sID);
		PurchaseInvoiceExpPeer.doDelete (oCrit, _oConn);
	}	
	
	public static List getDetailsByPurchaseReceiptID(String _sID)
    	throws Exception
    {
        return getDetailsByPurchaseReceiptID(_sID, null);
    }

	public static List getDetailsByPurchaseReceiptID(String _sID,Connection _oConn)
    	throws Exception
    {	
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_RECEIPT_ID, _sID);
        return PurchaseInvoiceDetailPeer.doSelect(oCrit,_oConn);
	}

	/**
	 * 
	 * @param _sID
	 * @return List of PurchaseInvoiceFreight
	 * @throws Exception
	 */
	public static List getFreightByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, _sID);
		return PurchaseInvoiceFreightPeer.doSelect(oCrit, _oConn);
	}	
	
	private static void deleteFreightByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID, _sID);
		PurchaseInvoiceFreightPeer.doDelete (oCrit, _oConn);
	}	
	
	/**
	 * check whether a PR is already exist in any PI
	 * 
	 * @param _sPRID
	 * @return whether PR already invoiced
	 * @throws Exception
	 */
	public static boolean isPRInvoiced(String _sPRID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseInvoicePeer.STATUS, i_PI_CANCELLED, Criteria.NOT_EQUAL);
		oCrit.addJoin(PurchaseInvoicePeer.PURCHASE_INVOICE_ID,PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID);
	    oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_RECEIPT_ID, _sPRID);
		List vData = PurchaseInvoiceDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return true;
		}
		return false;
	}

	public static String getPaymentStatusString (int _iStatusID)
	{
		if (_iStatusID == i_PAYMENT_PAID)
		{
			return LocaleTool.getString("paid");
		}
		return LocaleTool.getString("invoiced");
	}	

	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_PI_PENDING)   return LocaleTool.getString("pending");
		if (_iStatusID == i_PI_PROCESSED) return LocaleTool.getString("processed");
		if (_iStatusID == i_PI_CANCELLED) return LocaleTool.getString("cancelled");
		return "";
	}
	
	public static PurchaseInvoice getLastPIByItemID(String _sItemID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (PurchaseInvoiceDetailPeer.ITEM_ID, _sItemID);
		oCrit.addJoin (PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, PurchaseInvoicePeer.PURCHASE_INVOICE_ID);
		oCrit.addDescendingOrderByColumn (PurchaseInvoicePeer.PURCHASE_INVOICE_DATE);
		oCrit.add(PurchaseInvoicePeer.STATUS, i_PROCESSED);
		oCrit.setLimit(1);
		List vData = PurchaseInvoicePeer.doSelect (oCrit);
		if (vData.size() > 0) 
		{
			return (PurchaseInvoice) vData.get(0);
		}
		return null;
	}
	
	public static PurchaseInvoiceDetail getLastPIDetByItemID(String _sItemID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (PurchaseInvoiceDetailPeer.ITEM_ID, _sItemID);
		oCrit.addJoin(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, PurchaseInvoicePeer.PURCHASE_INVOICE_ID);
		oCrit.addDescendingOrderByColumn (PurchaseInvoicePeer.PURCHASE_INVOICE_DATE);
		oCrit.add(PurchaseInvoicePeer.STATUS, i_PROCESSED);
		oCrit.setLimit(1);
		List vData = PurchaseInvoiceDetailPeer.doSelect (oCrit);
		if (vData.size() > 0) 
		{
			return (PurchaseInvoiceDetail) vData.get(0);
		}
		return null;
	}
	
	/**
	 * get pi details by pr detail ID, used by pi report
	 * 
	 * @param _sPRDetID
	 * @return get List of PurchaseInvoiceDetail
	 * @throws Exception
	 */
	public static List getDetailsByPRDetailID(String _sPRDetID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_RECEIPT_DETAIL_ID,_sPRDetID);
		List vData = PurchaseInvoiceDetailPeer.doSelect (oCrit);
		return vData;
	}

	/**
	 * set header properties
	 * 
	 * @param _oPI
	 * @param _vPID
	 * @param data
	 * @throws Exception
	 */
    public static void setHeaderProperties (PurchaseInvoice _oPI, List _vPID, RunData data)
    	throws Exception
    {
    	setHeaderProperties(_oPI, _vPID, null, data);
    }
    
    /**
     * set header properties
     * @param _oPI
     * @param _vPID
     * @param _vPIE
     * @param data
     * @throws Exception
     */
    public static void setHeaderProperties (PurchaseInvoice _oPI, List _vPID, List _vPIE, RunData data)
    	throws Exception
    {
    	ValueParser formData = null;
    	if (data != null) formData = data.getParameters();
		try
		{
			if (formData != null)
			{
				formData.setProperties(_oPI);
			}
			
			String sVendorName = "";
			Vendor oVendor = VendorTool.getVendorByID(_oPI.getVendorId());
			if (oVendor != null) sVendorName = oVendor.getVendorName();
			
			_oPI.setVendorName	   (sVendorName);
			
			int iDueDays = PaymentTermTool.getNetPaymentDaysByID(_oPI.getPaymentTermId());
			
			_oPI.setPaymentDueDate (DateUtil.addDays(_oPI.getTransactionDate(), iDueDays));    		
    		_oPI.setTotalQty 	   (new BigDecimal (countQty(_vPID)));
    		_oPI.setTotalDiscount  (countDiscount (_vPID));
    		_oPI.setTotalAmount    (countSubTotal (_vPID));
    		
    		if (formData != null)
    		{
	    		_oPI.setTotalExpense 	  	(formData.getBigDecimal("TotalExpense"));
				_oPI.setPurchaseInvoiceDate	(CustomParser.parseDate(formData.getString("PurchaseInvoiceDate")));
				_oPI.setDueDate				(CustomParser.parseDate(formData.getString("DueDate")));
	            _oPI.setIsInclusiveTax  	(formData.getBoolean("IsInclusiveTax", false));
	            _oPI.setIsTaxable			(formData.getBoolean("IsTaxable", false));    					
    		}
            //count total amount after total discount percentage
    		String sTotalInvDisc = "0";
    		if(StringUtil.isEmpty(_oPI.getTotalDiscountPct()) && formData != null)
    		{
			    sTotalInvDisc = formData.getString("TotalDiscountPct","0").trim();
			}
			else
			{
			    sTotalInvDisc = _oPI.getTotalDiscountPct().trim();
		    }
			
			BigDecimal bdTotalAmount = _oPI.getTotalAmount();    		
			if (StringUtil.isNotEmpty(sTotalInvDisc) && !StringUtil.equals(sTotalInvDisc, "0")) 
            { 
				//count total discount from total amount
				BigDecimal bdTotalItemDisc = _oPI.getTotalDiscount();
				BigDecimal bdTotalInvDisc = new BigDecimal(Calculator.calculateDiscount(sTotalInvDisc, bdTotalAmount))
												.setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
			 	_oPI.setTotalDiscount(bdTotalItemDisc.add(bdTotalInvDisc));
			 	bdTotalAmount = _oPI.getTotalAmount().subtract(bdTotalInvDisc); //because total amount already minus item discount
			 	updateCostPerUnitByInvDisc(sTotalInvDisc, _vPID, _oPI);
            }			
			_oPI.setTotalTax(countTax(_oPI, _vPID));
				
			//freight cost is not taxed if freight cost is taxed then put this line before countTax
			updateCostByFreightCost(_oPI, _vPID);			
			
			bdTotalAmount = countTotal(_oPI, _vPID); //count total again after applying invoice discount
			
            //count Total Amount with tax
			//if (!_oPI.getIsInclusiveTax()) //sub total now exclude tax
			//{
			bdTotalAmount = bdTotalAmount.add(_oPI.getTotalTax());
			//}
			
            if (StringUtil.isEqual(_oPI.getVendorId(),_oPI.getFreightVendorId()))
            {            	
            	bdTotalAmount = bdTotalAmount.add(_oPI.getTotalExpense());
            }                      
            
            calculateLastPurchase(_oPI, _vPID);            
            if (_vPIE != null && _vPIE.size() > 0)
            {
            	bdTotalAmount.add(new BigDecimal(_oPI.getTotalAccExp(_vPIE)));             	
            }
            _oPI.setTotalAmount (bdTotalAmount); 
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
    /**
     * update detail from form submit 
     * 
     * @param _vPID
     * @param data
     * @throws Exception
     */
    public static void updateDetail(List _vPID, RunData data)
    	throws Exception
    {
    	ValueParser formData = data.getParameters();    	
		try
		{			
            BigDecimal bdRate = formData.getBigDecimal("CurrencyRate",bd_ONE);
            boolean bIsIncTax = formData.getBoolean("IsInclusiveTax",false);
            
			for (int i = 0; i < _vPID.size(); i++)
    		{
				int iIdx = i + 1;
				PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail) _vPID.get (i);
				
				log.info ("PID Before update : "  + oPID);	
                //prevent screen refresh after add item
                if (formData.getString("ItemPrice" + iIdx) != null)
                {                									
	       			double dBaseValue = UnitTool.getBaseValue(oPID.getItemId(), oPID.getUnitId());
	                double dQtyBase = oPID.getQty().doubleValue() * dBaseValue; 
	                
	    			oPID.setQtyBase 	 (new BigDecimal(dQtyBase));
	    			oPID.setItemPrice    (formData.getBigDecimal("ItemPrice"	+ iIdx) );
	    			oPID.setTaxId        (formData.getString    ("TaxId"		+ iIdx) );	    								
					oPID.setDiscount     (formData.getString	("Discount"		+ iIdx)	);
					oPID.setWeight       (formData.getBigDecimal("Weight"		+ iIdx) );					
					oPID.setTaxAmount    (TaxTool.getTaxByID(oPID.getTaxId()).getAmount());					
					oPID.setSubTotalExp  (formData.getBigDecimal("SubTotalExp"	+ iIdx) );

	                BigDecimal bdSubTax = bd_ZERO;
					BigDecimal bdSubTotal = oPID.getItemPrice().multiply(oPID.getQty());
					BigDecimal bdSubDisc = new BigDecimal(Calculator.calculateDiscount(oPID.getDiscount(), bdSubTotal));
					bdSubTotal = bdSubTotal.subtract(bdSubDisc);
					if(oPID.getTax().getAmount().doubleValue() > 0)
					{			
						if(bIsIncTax)
						{							
							bdSubTax = bdSubTotal.subtract(new BigDecimal(bdSubTotal.doubleValue() / ((100 + oPID.getTaxAmount().doubleValue()) / 100)));
							bdSubTotal = bdSubTotal.subtract(bdSubTax);
						}
						else
						{
							bdSubTax = new BigDecimal(bdSubTotal.doubleValue() * (oPID.getTaxAmount().doubleValue() / 100));
						}												
						bdSubTax = bdSubTax.setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
						bdSubTotal = bdSubTotal.setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
					}
					
					oPID.setSubTotalTax(bdSubTax);
					oPID.setSubTotalDisc(bdSubDisc);
					oPID.setSubTotal(bdSubTotal);
					if(oPID.getQtyBase().doubleValue() > 0)
					{
						BigDecimal bdCostPerUnit = bdSubTotal.divide(oPID.getQtyBase(),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP).multiply(bdRate);
						oPID.setCostPerUnit(bdCostPerUnit);
					}
					log.info ("PID After update : " + oPID);
                }									
    		}	
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Invoice Detail Data Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
    /**
     * calculate last purchase with several method such as:
     * <ul>
     *   <li>ITEM PRICE METHOD last purchase is purely from item price
     *   <li>DISCOUNT METHOD last purchase is from item price minus item discount
     *   <li>DISCOUNT ALL METHOD last purchase is from item price 
     *       minus item discount and total disc (prorated)
     *   <li>TAX_INC METHOD last purchase from item price + tax 
     * </ul>
     * 
     * @param _vPID
     * @param _iLastPurchaseMethod
     */
    private static void calculateLastPurchase(PurchaseInvoice _oPI, List _vPID)
    {
        for(int i = 0; i<_vPID.size(); i++)
        {
            PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail)_vPID.get(i);
            double dLastPur = oPID.getCostPerUnit().doubleValue();   //default use cost per unit            
            double dPIQty = oPID.getQty().doubleValue();
            double dUnitConv = 1;                                		
    		if (StringUtil.equals(oPID.getUnitId(),oPID.getItem().getPurchaseUnitId())) //purchase is in base unit
    		{
    			dUnitConv = oPID.getItem().getUnitConversion().doubleValue();
    		}
    		
    		if(PreferenceTool.getPiLastPurchMethod() == i_ITEM_PRICE_METHOD) 
    		{    
    			dLastPur = oPID.getItemPrice().doubleValue() / dUnitConv;
    			if(!b_PURCHASE_TAX_INCLUSIVE) 
    			{
    				//if exclusive tax, but current PI is Inclusive then calculate the Price Before Tax else no need
    				//because last purchase will be same with item price
    				dLastPur = TaxTool.calculateBeforeTax(oPID.getTaxId(), _oPI.getIsInclusiveTax(), dLastPur);
    			}    			
    		}
    		else if(PreferenceTool.getPiLastPurchMethod() == i_DISCOUNT_METHOD && dPIQty > 0)
    		{

    			dLastPur = oPID.getItemPrice().doubleValue() * oPID.getQty().doubleValue();
    			dLastPur = dLastPur - oPID.getSubTotalDisc().doubleValue();
    			dLastPur = dLastPur / oPID.getQty().doubleValue();
    			dLastPur = dLastPur / dUnitConv;   
    		}
    		else if(PreferenceTool.getPiLastPurchMethod() == i_DISCOUNT_ALL_METHOD)
    		{
    			dLastPur = oPID.getCostPerUnit().doubleValue();	            
    		}
    		else if(PreferenceTool.getPiLastPurchMethod() == i_TAX_INC_METHOD && dPIQty > 0)
    		{
    			dLastPur = (oPID.getCostPerUnit().doubleValue() * oPID.getQtyBase().doubleValue());
    			dLastPur = dLastPur + (dLastPur  * oPID.getTaxAmount().doubleValue() / 100);
    			dLastPur = dLastPur / oPID.getQtyBase().doubleValue();
    		} 
    		else if(PreferenceTool.getPiLastPurchMethod() == 5) //PRICE + TAX EXCL DISC
    		{
    			dLastPur = oPID.getItemPrice().doubleValue() / dUnitConv;
    			if(!_oPI.getIsInclusiveTax()) 
    			{
    				double dPrice = oPID.getItemPrice().doubleValue();
    				dLastPur = (dPrice + (dPrice * oPID.getTaxAmount().doubleValue() / 100)) / dUnitConv;    				
    			}
    		} 
    		
            oPID.setLastPurchase (new BigDecimal(dLastPur).setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));
        }
    }
    
    /**
     * Since using discount is 2 level then CostPerUnit in each 
     * PurchaseInvoiceDetail should be updated
     * 
     * @param _sDisc
     * @param _vPID
     * @param _oPI
     * @throws Exception
     */
    private static void updateCostPerUnitByInvDisc (String _sDisc, List _vPID, PurchaseInvoice _oPI)
        throws Exception
	{
    	double dTotalCost = countCost(_vPID);
    	//double dTotalQty  = _oPI.getTotalQty().doubleValue() ;
    	
		for (int i = 0; i < _vPID.size(); i++)
		{
			PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail) _vPID.get(i);
			
			double dCost = oPID.getCostPerUnit().doubleValue();
			//double dQty = oDet.getQtyBase().doubleValue();
			double dDisc = Calculator.calculateDiscount(_sDisc, dCost);
		    // discount is not in percentage
		    if(!_sDisc.contains(Calculator.s_PCT))
		    {
		    	//complete step was : dCost = dCost - (dDisc * (dSubTotalCost / dTotalCost) / dQty);
		    	if (dTotalCost > 0)
		    	{
		    		dCost = dCost - (dDisc * (dCost / dTotalCost) * _oPI.getCurrencyRate().doubleValue());
		    	}
		    }
		    else
		    {
		    	dCost = dCost - dDisc;
		    }		
		    
//		    if (_oPI.getIsInclusiveTax())
//		    {	
//		    	dCost = dCost  / ((oPID.getTaxAmount().doubleValue() + 100) / 100);
//		    }
            oPID.setCostPerUnit (new BigDecimal(dCost).setScale(4,BigDecimal.ROUND_HALF_UP));
		}		
	}

    /**
     * 
     * update cost if freight cost is allocated as item cost
     * 
     * @param _oPI
     * @param _vDetails
     * @throws Exception
     */
    private static void updateCostByFreightCost(PurchaseInvoice _oPI, List _vDetails)
        throws Exception
	{		
    	double dTotalExpense = _oPI.getTotalExpense().doubleValue();

        if (_oPI.getFreightAsCost() && dTotalExpense > 0) //if freight set as cost and freight exist
        {
    		String sFreightCurrID  = _oPI.getFreightCurrencyId();
    		Currency oCurr = CurrencyTool.getCurrencyByID(sFreightCurrID, null);
    		if (!oCurr.getIsDefault()) 
    		{
    			dTotalExpense = dTotalExpense * _oPI.getCurrencyRate().doubleValue();
    		}
    		
        	double dTotalCost = countCost(_vDetails);
        	for (int i = 0; i < _vDetails.size(); i++)
        	{
        		PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) _vDetails.get(i);
        		double dCost = oDet.getCostPerUnit().doubleValue();
        		if (dTotalCost > 0)
        		{
        			//total expense is pro-rated towards value of PI item cost per unit
        			dCost = dCost + (dTotalExpense * (dCost / dTotalCost));
        		}
        		oDet.setCostPerUnit (new BigDecimal (dCost).setScale(4,BigDecimal.ROUND_HALF_UP));           
        	}		
        }
	}
    
    /**
     * 
     * @param _sTransID
     * @param _sItemID
     * @return sum of item total qty in details
     * @throws Exception
     */
	public static double countTotalItemQty (String _sTransID, String _sItemID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, _sTransID);
        oCrit.add(PurchaseInvoiceDetailPeer.ITEM_ID, _sItemID);
        List vData = PurchaseInvoiceDetailPeer.doSelect(oCrit);
        if (vData.size() > 0) 
        {
        	PurchaseInvoiceDetail oDetail = (PurchaseInvoiceDetail) vData.get(0);
        	return oDetail.getQty().doubleValue();
        }
        return 0;
	}
	
	/**
	 * 
	 * @param _vDetails
	 * @return sum of total cost per unit in detail
	 */
	public static double countCost(List _vDetails) 
	{		
        double dTotalCost = 0;
        for (int i = 0; i < _vDetails.size(); i++)
        {
            PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) _vDetails.get(i);
            dTotalCost += oDet.getCostPerUnit().doubleValue() * oDet.getQtyBase().doubleValue();
        }
        return dTotalCost;
	}
	
	/**
	 * 
	 * @param _vDetails
	 * @return sum of qty base 
	 * @throws Exception
	 */
	public static double countQty (List _vDetails)
		throws Exception
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) _vDetails.get(i);
			if (oDet.getQtyBase() == null) //if from add detail
			{
				oDet.setQtyBase (UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty()));
			}
			dQty += oDet.getQty().doubleValue();
		}
		return dQty;
	}

    private static BigDecimal countTax (PurchaseInvoice _oPI, List _vDetails)
    {
        BigDecimal bdAmt = bd_ZERO;
        for (int i = 0; i < _vDetails.size(); i++)
        {
            PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) _vDetails.get(i);
            if(StringUtil.isNotEmpty(_oPI.getTotalDiscountPct()) && !StringUtil.equals(_oPI.getTotalDiscountPct(),"0"))
            {
            	BigDecimal bdCost = oDet.getCostPerUnit().divide(_oPI.getCurrencyRate(),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP).multiply(oDet.getQtyBase()); 				
            	BigDecimal bdTax = oDet.getTaxAmount().divide(new BigDecimal(100),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP);
            	bdTax = bdTax.multiply(bdCost).setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
	            oDet.setSubTotalTax	(bdTax);            
            }
            bdAmt = bdAmt.add(oDet.getSubTotalTax());
        }
        return bdAmt;
    }

	public static BigDecimal countDiscount (List _vDetails)
	{
		BigDecimal bdAmt = bd_ZERO;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) _vDetails.get(i);			
			bdAmt = bdAmt.add(oDet.getSubTotalDisc());
		}
		return bdAmt;
	}

	/**
	 * count total temporarily from item price - item disc - tax if is inclusive 
	 * 
	 * @param _vDetails
	 * @return
	 */
	public static BigDecimal countSubTotal (List _vDetails)
	{
		BigDecimal bdAmt = bd_ZERO;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) _vDetails.get(i);									
			bdAmt = bdAmt.add(oDet.getSubTotal());
		}
		return bdAmt;
	} 

	/**
	 * count total for the last time so total amount will be the same with GL amount
	 * @param _oPI
	 * @param _vDetails
	 * @return
	 */
	public static BigDecimal countTotal (PurchaseInvoice _oPI, List _vDetails)
	{
		BigDecimal bdAmt = bd_ZERO;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) _vDetails.get(i);	
			bdAmt = bdAmt.add(oDet.getCostPerUnit().multiply(oDet.getQtyBase()).divide(_oPI.getCurrencyRate(),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP));
		}
		return bdAmt;
	} 

    //end count methods

    
    /**
     * update paid amount from DP
     * 
     * @param _sID
     * @param _dPaidAmount
     * @param _dDPAmount
     * @param _oConn
     * @throws Exception
     */
    public static void updatePaidAmount(String _sID, 
    									double _dPaidAmount, 
    									double _dDPAmount, 
    									boolean _bTaxPayment, 
    									Connection _oConn)
		throws Exception
	{
		PurchaseInvoice oPI = getHeaderByID(_sID, _oConn);
		if (oPI != null) 
		{
			Currency oCurr = CurrencyTool.getCurrencyByID(oPI.getCurrencyId(), _oConn);
			double dTotalAmount = oPI.getTotalAmount().doubleValue();
			if (!oCurr.getIsDefault()) dTotalAmount = dTotalAmount - oPI.getTotalTax().doubleValue();			
			double dPaidAmount = oPI.getPaidAmount().doubleValue() + _dPaidAmount + _dDPAmount;
			
			if (!_bTaxPayment)
			{				
				if(Calculator.precise(dPaidAmount,1) > Calculator.precise(dTotalAmount,1))
				{
					String sMsg = " PI " + oPI.getPurchaseInvoiceNo() + " Total Paid Amount " + CustomFormatter.fmt(dPaidAmount) + 
								  " Greater Than Total Amount " + CustomFormatter.fmt(dTotalAmount);					  
					throw new NestableException (sMsg);					
				}
				if(Calculator.precise(dTotalAmount) <= Calculator.precise(dPaidAmount))
				{
					oPI.setPaymentStatus(i_PAYMENT_PAID);
				}
				else
				{
					oPI.setPaymentStatus(i_PAYMENT_OPEN);				
				}
			}
			oPI.setPaidAmount(new BigDecimal(dPaidAmount));
			oPI.save(_oConn);
		}
	}
    
    /**
     * used by invoice payment lookup
     * 
     * @param _sInvNo
     * @param _dStart
     * @param _dDueStart
     * @param _sVendorID
     * @param _sPaymentTypeID
     * @param _sCurrencyID
     * @return list of processed but not paid yet credit trans
     * @throws Exception
     */
	public static List findCreditTrans (String _sInvNo, 
										Date _dStart, 
										Date _dEnd, 
										Date _dDueStart,
										Date _dDueEnd,
								 		String _sVendorID, 
								 		String _sLocationID,
										String _sPaymentTypeID, 
										String _sCurrencyID,
										boolean _bTax) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sInvNo)) 
		{
			oCrit.add(PurchaseInvoicePeer.PURCHASE_INVOICE_NO, 
				(Object) SqlUtil.like (PurchaseInvoicePeer.PURCHASE_INVOICE_NO, _sInvNo), Criteria.CUSTOM);
		}
		transDateCriteria(oCrit, PurchaseInvoicePeer.PURCHASE_INVOICE_DATE, _dStart, _dEnd);
		transDateCriteria(oCrit, PurchaseInvoicePeer.PAYMENT_DUE_DATE, _dDueStart, _dDueEnd);
		
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseInvoicePeer.VENDOR_ID, _sVendorID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(PurchaseInvoicePeer.LOCATION_ID, _sLocationID);	
		}		
		if (StringUtil.isNotEmpty(_sPaymentTypeID)) 
		{
			oCrit.add(PurchaseInvoicePeer.PAYMENT_TYPE_ID, _sPaymentTypeID);	
		}
		if (StringUtil.isNotEmpty(_sCurrencyID)) 
		{
		    oCrit.add(PurchaseInvoicePeer.CURRENCY_ID, _sCurrencyID);	
	    }
		oCrit.add(PurchaseInvoicePeer.STATUS, i_PI_PROCESSED);
		if (!_bTax)
		{
			oCrit.add(PurchaseInvoicePeer.PAYMENT_STATUS, i_PAYMENT_OPEN);
		}
		else
		{
			Criteria.Criterion oPmtProc = oCrit.getNewCriterion(PurchaseInvoicePeer.PAYMENT_STATUS, i_PAYMENT_PROCESSED, Criteria.EQUAL);
			Criteria.Criterion oAmtLE  = oCrit.getNewCriterion(PurchaseInvoicePeer.TOTAL_AMOUNT,(Object) SqlUtil.buildCompareQuery 
					(PurchaseInvoicePeer.PAID_AMOUNT, PurchaseInvoicePeer.TOTAL_AMOUNT, "<") , Criteria.CUSTOM);

			Criteria.Criterion oPmtOpen = oCrit.getNewCriterion(PurchaseInvoicePeer.PAYMENT_STATUS, i_PAYMENT_OPEN, Criteria.EQUAL);
			Criteria.Criterion oAmtZero = oCrit.getNewCriterion(PurchaseInvoicePeer.PAID_AMOUNT, bd_ZERO, Criteria.EQUAL);
			
			oCrit.add(oPmtProc.and(oAmtLE));
			oCrit.or(oPmtOpen.and(oAmtZero));
			
			log.debug(oCrit);
			/*
			oCrit.add(PurchaseInvoicePeer.PAYMENT_STATUS, i_PAYMENT_PROCESSED);
			oCrit.add(PurchaseInvoicePeer.TOTAL_AMOUNT,(Object) SqlUtil.buildCompareQuery 
				(PurchaseInvoicePeer.PAID_AMOUNT, PurchaseInvoicePeer.TOTAL_AMOUNT, "<") , Criteria.CUSTOM);			
			*/
		}
		oCrit.addAscendingOrderByColumn(PurchaseInvoicePeer.PURCHASE_INVOICE_DATE);		
		return PurchaseInvoicePeer.doSelect(oCrit);
	}
	
	/**
	 * 
	 * @param _oPR
	 * @param _vPRD
	 * @param _vPID
	 * @param _sPOID
	 * @param _sVendorID
	 * @throws Exception
	 */
	public static void mapPRDetailToPIDetail (PurchaseReceipt _oPR,
											  List _vPRD, 
											  List _vPID, 
											  String _sPOID, 
											  String _sVendorID)
		throws Exception
	{
		//_oDet.setPurchaseOrderDetailId	(_oPODet.getPurchaseOrderDetailId());	
		for (int i = 0; i < _vPRD.size(); i++)
    	{
    		PurchaseReceiptDetail oPRD  = (PurchaseReceiptDetail) _vPRD.get(i);
    		PurchaseInvoiceDetail oPID  = new PurchaseInvoiceDetail();
    		oPID.setPurchaseOrderId	  		(_sPOID);
        	oPID.setPurchaseReceiptId 		(oPRD.getPurchaseReceiptId());
        	oPID.setPurchaseReceiptDetailId (oPRD.getPurchaseReceiptDetailId());
        	
        	double dQty = oPRD.getQty().doubleValue() - oPRD.getReturnedQty().doubleValue();
        	double dQtyBase = dQty * UnitTool.getBaseValue(oPRD.getItemId(), oPRD.getUnitId());        	
        	
        	oPID.setItemId      (oPRD.getItemId());
        	oPID.setItemCode    (oPRD.getItemCode());
        	oPID.setItemName    (oPRD.getItemName());
        	oPID.setDescription (oPRD.getDescription());
        	oPID.setUnitId		(oPRD.getUnitId());
        	oPID.setUnitCode	(oPRD.getUnitCode());
	       	oPID.setTaxId		(oPRD.getTaxId());
        	oPID.setTaxAmount	(TaxTool.getAmountByID(oPID.getTaxId()));
	       	oPID.setQty			(new BigDecimal(dQty));
	       	oPID.setQtyBase		(new BigDecimal(dQtyBase));
	       	oPID.setReturnedQty	(bd_ZERO);
	       	oPID.setCostPerUnit	(oPRD.getCostPerUnit());
        	oPID.setWeight		(bd_ZERO);
        	oPID.setSubTotalExp	(bd_ZERO);
	       	
	       	double dTotalPurchase = 0;
	       	//count total Purchase per detail
	       	if(StringUtil.isEmpty(oPRD.getPurchaseOrderDetailId()))
	       	{
	       	    //if the item not from PO then get price from price list and check the qty
	       	    //if qty on receipt different from purchase qty unit on item then convert price to base then add price
	       	    //to that value
	       	    Item oItem = ItemTool.getItemByID(oPID.getItemId());	       	    
	       	    oItem = ItemTool.findPurchaseItemByCode(oItem.getItemCode(), _sVendorID, _oPR.getReceiptDate());
	       	    if (oItem != null)
	       	    {
	       	    	//TODO: check again
	       	    	if(oPID.getUnitId().equals(oItem.getPurchaseUnitId()))
	       	    	{
	       	    		oPID.setItemPrice (oItem.getItemPrice());
	       	    		oPID.setDiscount (oItem.getDiscountAmount());
	       	    	}
	       	    	else
	       	    	{
	       	    		double dBaseValueItem = UnitTool.getBaseValue(oPID.getItemId(),oItem.getPurchaseUnitId()); //get base value on item
	       	    		double dBaseValuePurchase = UnitTool.getBaseValue(oPID.getItemId(),oPID.getUnitId()); //get base value on purchase
	       	    		if(dBaseValueItem != 0)
	       	    		{
	       	    			double dItemPrice = oItem.getItemPrice().doubleValue() / dBaseValueItem; //get price base on item
	       	    			dItemPrice = dItemPrice * dBaseValuePurchase;   //get purchase price
	       	    			oPID.setItemPrice  (new BigDecimal(dItemPrice));
	       	    			oPID.setDiscount   ("0");   //discount will set to 0 if purchase qty not same as item purchase qty
	       	    		}
	       	    	}
	       	    }
	        }
	        else
	        {
        	    oPID.setItemPrice (oPRD.getItemPriceInPo());
        	    oPID.setDiscount  (oPRD.getDiscount());
        	}
	       	
        	dTotalPurchase = (oPID.getItemPrice().doubleValue() * oPID.getQty().doubleValue());
        	
        	oPID.setTaxId		 (oPRD.getTaxId());
        	oPID.setTaxAmount	 (oPRD.getTaxAmount());
        	oPID.setSubTotalTax	 (bd_ZERO); // will calculate later
        	oPID.setDepartmentId (oPRD.getDepartmentId());
        	oPID.setProjectId    (oPRD.getProjectId());
        	
        	double dTotalDisc = Calculator.calculateDiscount(oPID.getDiscount(),dTotalPurchase);
        	dTotalPurchase = dTotalPurchase - dTotalDisc;
        	
        	oPID.setSubTotalDisc (new BigDecimal(dTotalDisc));
		    oPID.setSubTotal	 (new BigDecimal(dTotalPurchase));
		    
        	_vPID.add(oPID);
    	}
	}
	//-------------------------------------------------------------------------
	//	END from RECEIPT  methods
	//-------------------------------------------------------------------------	

	//-------------------------------------------------------------------------
	// FROM PO methods
	//-------------------------------------------------------------------------
	/**
	 * 
	 * @param _oPO
	 * @param _vPOD
	 * @param _vPID
	 * @param _sPOID
	 * @param _sVendorID
	 * @throws Exception
	 */
	public static void mapPODetailToPIDetail (PurchaseOrder _oPO,
											  List _vPOD, 
											  List _vPID)
		throws Exception
	{
		//_oDet.setPurchaseOrderDetailId	(_oPODet.getPurchaseOrderDetailId());	
		for (int i = 0; i < _vPOD.size(); i++)
    	{
    		PurchaseOrderDetail oPOD = (PurchaseOrderDetail) _vPOD.get(i);
    		PurchaseInvoiceDetail oPID  = new PurchaseInvoiceDetail();
        	oPID.setPurchaseOrderId (oPOD.getPurchaseOrderId());
        	oPID.setPurchaseReceiptId (oPOD.getPurchaseOrderId());
        	oPID.setPurchaseReceiptDetailId (oPOD.getPurchaseOrderDetailId());
        	
        	double dQty = oPOD.getQty().doubleValue();
        	double dQtyBase = dQty * UnitTool.getBaseValue(oPOD.getItemId(), oPOD.getUnitId());        	
        	
        	oPID.setItemId      (oPOD.getItemId());
        	oPID.setItemCode    (oPOD.getItemCode());
        	oPID.setItemName    (oPOD.getItemName());
        	oPID.setDescription (oPOD.getDescription());
        	oPID.setUnitId		(oPOD.getUnitId());
        	oPID.setUnitCode	(oPOD.getUnitCode());
	       	oPID.setTaxId		(oPOD.getTaxId());
        	oPID.setTaxAmount	(TaxTool.getAmountByID(oPID.getTaxId()));
	       	oPID.setQty			(new BigDecimal(dQty));
	       	oPID.setQtyBase		(new BigDecimal(dQtyBase));
	       	oPID.setReturnedQty	(bd_ZERO);
	       	oPID.setCostPerUnit	(oPOD.getCostPerUnit());
        	oPID.setWeight		(bd_ZERO);
        	oPID.setSubTotalExp	(bd_ZERO);
	       	oPID.setItemPrice   (oPOD.getItemPrice());
	       	oPID.setDiscount    (oPOD.getDiscount());
	       	
	       	double dTotalPurchase = oPID.getItemPrice().doubleValue() * oPID.getQty().doubleValue();
        	
        	oPID.setTaxId		 (oPOD.getTaxId());
        	oPID.setTaxAmount	 (oPOD.getTaxAmount());
        	oPID.setSubTotalTax	 (bd_ZERO); // will calculate later
        	oPID.setDepartmentId (oPOD.getDepartmentId());
        	oPID.setProjectId    (oPOD.getProjectId());
        	
        	double TotalDisc = Calculator.calculateDiscount(oPID.getDiscount(),dTotalPurchase);
        	dTotalPurchase = dTotalPurchase - TotalDisc;
        	
        	oPID.setSubTotalDisc (new BigDecimal(TotalDisc));
		    oPID.setSubTotal	 (new BigDecimal(dTotalPurchase));
		    
        	_vPID.add(oPID);
    	}
	}
	//-------------------------------------------------------------------------
	// END from PO methods
	//-------------------------------------------------------------------------
		
    /**
     * this method is used to set due date on the Purchase Receipt
     *
     * @param _oPI - Purchase Receipt Object 
     */
	private static void setDueDate (PurchaseInvoice _oPI, Connection _oConn) 
		throws Exception 
	{			
		int iDueDays = PaymentTermTool.getNetPaymentDaysByID (_oPI.getPaymentTermId(), _oConn);
		Calendar oCal = new GregorianCalendar ();
		oCal.setTime (_oPI.getPurchaseInvoiceDate());
		oCal.add (Calendar.DATE, iDueDays);
		_oPI.setPaymentDueDate (oCal.getTime());
		_oPI.setDueDate(oCal.getTime());
		_oPI.setPaymentStatus (i_PAYMENT_OPEN);
	}
	
	/**
	 * Process Invoice Payment
	 * 
	 * @param _oPI
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processPayment (PurchaseInvoice _oPI, Connection _oConn)
		throws Exception
	{
		double dPaid = _oPI.getPaidAmount().doubleValue();
		double dTotal = _oPI.getTotalAmount().doubleValue();
		double dUnpaid = dTotal - dPaid;

    	//if any unpaid amount
    	if (dUnpaid != 0)
    	{
    		boolean bMulti = !CurrencyTool.getCurrencyByID(_oPI.getCurrencyId(), _oConn).getIsDefault();
    		
    		//create AP Journal
    		AccountPayableTool.createAPEntryFromInvoice(_oPI, bMulti, _oConn);	
    	}
    	
		//the purchase invoice paid half or fully directly from PI
    	if (dPaid > 0)
    	{
    		//set PI paid amount back to ZERO because it will be updated by PP
    		_oPI.setPaidAmount(bd_ZERO);
    		_oPI.save(_oConn);
    		
    		//create cash flow transaction & cash receipt journal
    		PayablePaymentTool.createFromPI(_oPI, dPaid, _oConn);
    	}
    }
	
	/**
	 * save data, create inventory, ap and gl journal
	 * 
	 * @param _oPI
	 * @param _vPID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (PurchaseInvoice _oPI, List _vPID, List _vPIF, List _vPIE, Connection _oConn)
		throws Exception
	{
		boolean bNew = false;
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try
		{
			validateDate(_oPI.getPurchaseInvoiceDate(), _oConn);
			
			if(StringUtil.isEmpty(_oPI.getPurchaseInvoiceId())) bNew = true;			
			
			processSavePIData (_oPI, _vPID, _vPIF, _vPIE, _oConn);
            if(_oPI.getStatus() == i_PI_PROCESSED)
            {

				//update pr billed qty, then checked if all invoiced then 
	            //set billed -> true status -> INVOICED
			    Map mPR = PurchaseReceiptTool.updatePRFromPI(_oPI, _vPID, false, _oConn);			    
			    
				//create inventory transaction and update inventory location
				InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
				oCT.setUpdatePRCost(mPR);
				oCT.createFromPI(_oPI, _vPID);

				//update PO if exist
				PurchaseOrderTool.updatePOFromPI(_vPID, false, false, _oConn);
            	
            	//Journal To GL
				if (b_USE_GL)
				{
					PurchaseJournalTool oJournal = new PurchaseJournalTool (_oPI, oCT.getUpdatePRCost(), _oConn);
					oJournal.createPIJournal (_oPI, _vPID, _vPIF, _vPIE, mPR);	    		
				}
				
				//process payment and AP jounrnal
				processPayment (_oPI, _oConn);	            
            }
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx)
		{
			//restore object state to unsaved
			_oPI.setPurchaseInvoiceNo("");
			_oPI.setStatus(i_PI_PENDING);				

			if (bNew)
			{
				_oPI.setNew(true);
				_oPI.setPurchaseInvoiceId("");
			}
			
			if (bStartTrans) 
			{
				rollback (_oConn);				
			}
			log.error(_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * Process save PI Data
	 * 
	 * @param _oPI
	 * @param _vPID
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSavePIData (PurchaseInvoice _oPI, List _vPID, List _vPIF, List _vPIE, Connection _oConn) 
		throws Exception
	{			
		if (StringUtil.isEmpty(_oPI.getPurchaseInvoiceId())) 
    	{
        	_oPI.setPurchaseInvoiceId (IDGenerator.generateSysID());			
        	_oPI.setNew(true);
        }

		validateID (getHeaderByID(_oPI.getPurchaseInvoiceId(), _oConn), "purchaseInvoiceNo");

		String sLocationID = _oPI.getLocationId();
		if (StringUtil.isEmpty(sLocationID)) sLocationID = PreferenceTool.getLocationID();
		Location oLocation = LocationTool.getLocationByID (sLocationID, _oConn);
		String sLocCode = oLocation.getLocationCode();
		_oPI.setLocationName(oLocation.getLocationName());
		
		//process purchase invoice no
	    if (_oPI.getPurchaseInvoiceNo() == null) _oPI.setPurchaseInvoiceNo ("");
		if ( _oPI.getStatus () == i_PI_PROCESSED && StringUtil.isEmpty(_oPI.getPurchaseInvoiceNo())) 
		{
    		_oPI.setPurchaseInvoiceNo (
    			LastNumberTool.generateByVendor(_oPI.getVendorId(),_oPI.getLocationId(),sLocCode,s_PI_FORMAT,LastNumberTool.i_PURCHASE_INVOICE, _oConn)
    		);
    		validateNo(PurchaseInvoicePeer.PURCHASE_INVOICE_NO, _oPI.getPurchaseInvoiceNo(), 
    				   PurchaseInvoicePeer.class, _oConn);
        }
		
		setDueDate (_oPI, _oConn);
		_oPI.save (_oConn);
		
		//process detail save
		if (StringUtil.isNotEmpty(_oPI.getPurchaseInvoiceId()))
		{
			deleteDetailsByID (_oPI.getPurchaseInvoiceId(), _oConn);
			deleteExpensesByID (_oPI.getPurchaseInvoiceId(), _oConn);
		}	
		
		renumber(_vPID);
		
		PurchaseReceipt oPR = null;		
        for (int i = 0; i < _vPID.size(); i++ )
		{        	
        	PurchaseInvoiceDetail oTD = (PurchaseInvoiceDetail) _vPID.get (i);
			oTD.setModified (true);
			oTD.setNew (true);			
			
			if(StringUtil.isNotEmpty(oTD.getPurchaseReceiptId()))
			{
				if(oPR == null || (oPR != null && !StringUtil.equals(oPR.getPurchaseReceiptId(), oTD.getPurchaseReceiptId()))) 
				{
					oPR = PurchaseReceiptTool.getHeaderByID(oTD.getPurchaseReceiptId(), _oConn);
					if(oPR.getStatus() != i_PR_APPROVED)
					{
						throw new Exception ("Invalid PR " + oPR.getReceiptNo() + " Status: " + oPR.getStatusStr());
					}
				}
			}
			
	    	oTD.setPurchaseInvoiceDetailId (IDGenerator.generateSysID());
    		oTD.setPurchaseInvoiceId (_oPI.getPurchaseInvoiceId());
        	
			if (oTD.getReturnedQty() == null) oTD.setReturnedQty ( bd_ZERO);
			
			Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
        	if (oItem == null) //validate item
        	{
        		throw new Exception("Invalid Item in PI: " + oTD.getItemCode() + 
        							" Please delete then add item again ");
        	}
			
			//validate batch & serial if this PI is not from PR
			if (StringUtil.isEmpty(oTD.getPurchaseReceiptId()) && 
				StringUtil.isEmpty(oTD.getPurchaseReceiptDetailId()))
			{
				BatchTransactionTool.validateBatch(oTD.getItemId(), oTD.getBatchTransaction(), _oConn);
				ItemSerialTool.validateSerial(oTD.getItemId(), oTD.getQty().doubleValue(), oTD.getSerialTrans(), _oConn);				
			}        	
			oTD.save (_oConn);
			
			//update last purchase if pi confirmed
        	if (StringUtil.isNotEmpty(_oPI.getPurchaseInvoiceNo()) && oTD.getLastPurchase().doubleValue() > 0)
        	{
        		ItemTool.updateLastPurchase(_oPI.getPurchaseInvoiceNo(), _oPI.getCurrencyId(),
        		    oTD.getItemId(), _oPI.getCreateBy(), oTD.getUnitId(),  
        		    	oTD.getLastPurchase().doubleValue(), _oPI.getCurrencyRate().doubleValue(), _oConn);
        	}
        }
        
        for ( int i = 0; i < _vPIE.size(); i++ )
		{
        	PurchaseInvoiceExp oPIE = (PurchaseInvoiceExp) _vPIE.get (i);
			oPIE.setModified (true);
			oPIE.setNew (true);

			Account oAccount = AccountTool.getAccountByID(oPIE.getAccountId(), _oConn);
        	if (oAccount == null) //validate item
        	{
        		throw new Exception("Invalid Account in PI: " + oPIE.getAccountCode() + 
        							" Please delete then add Account again ");
        	}			
	    	oPIE.setPurchaseInvoiceExpId (IDGenerator.generateSysID());
    		oPIE.setPurchaseInvoiceId (_oPI.getPurchaseInvoiceId());        				
			oPIE.save (_oConn);						
        }
        
		//process freight save
		if (StringUtil.isNotEmpty(_oPI.getPurchaseInvoiceId()))
		{
			deleteFreightByID (_oPI.getPurchaseInvoiceId(), _oConn);
		}	
		
        for ( int i = 0; i < _vPIF.size(); i++ )
		{
        	PurchaseInvoiceFreight oPIF = (PurchaseInvoiceFreight) _vPIF.get (i);
	    	oPIF.setPurchaseInvoiceFreightId (IDGenerator.generateSysID());
	    	oPIF.setPurchaseInvoiceId (_oPI.getPurchaseInvoiceId());			
			oPIF.setModified (true);
			oPIF.setNew (true);
	    	oPIF.save (_oConn);
        }        
	}

	/**
	 * 
	 * @param _oPI
	 * @param _vPID
	 * @param _vPIF
	 * @param _sEditBy
	 * @throws Exception
	 */
	public static void saveEdit (PurchaseInvoice _oPI, List _vPID, List _vPIF, List _vPIE, String _sEditBy)   
		throws Exception 
	{
		//set database connection
		boolean bNew = false;
		boolean bStartTrans = true;
		Connection oConn = beginTrans();
		validate(oConn);
		try 
		{
			if(_oPI != null && _vPID != null)
			{
				String sRemark = _oPI.getRemark();
				cancelPI(_oPI,_vPID,_vPIF,"", oConn);
				deleteTrans(_oPI.getPurchaseInvoiceId(),oConn);

				StringBuilder oSB = new StringBuilder();
				oSB.append(sRemark)
				.append("\nEdited By: ").append(_sEditBy)
				.append("\nEdit Date: ").append(CustomFormatter.formatDateTime(new Date()));

				_oPI.setStatus(i_PROCESSED);
				_oPI.setRemark(oSB.toString());
				_oPI.setNew(true);
				saveData(_oPI,_vPID,_vPIF,_vPIE,oConn);
			}            
			commit (oConn);
		}
		catch (Exception _oEx) 
		{            
			rollback (oConn);
			log.error (_oEx.getMessage(), _oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	} 
	

	/**
	 * Cancel Purchase Invoice
	 * 
	 * @param _sID Purchase Invoice ID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelPI (PurchaseInvoice _oPI, 
								 List _vPID, 
								 List _vPIF,
								 String _sCancelBy, 
								 Connection _oConn)
    	throws Exception
	{
		boolean bStartTrans = false;
		try
		{
			if (_oConn == null) 
			{
				_oConn = beginTrans();
				bStartTrans = true;
			}	    		
			//validate date
			validateDate(_oPI.getPurchaseInvoiceDate(), _oConn);

			//check whether purchase invoice exist in any payable payment
			if (!PayablePaymentTool.isInvoicePaid(_oPI.getPurchaseInvoiceId(), _oConn))
			{
				if (!PurchaseReturnTool.isReturned(_oPI.getPurchaseInvoiceId(), _oConn))
				{
					if(_oPI.getStatus() == i_PI_PROCESSED)
					{
						//1. delete invTrans
						InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
						oCT.delete(_oPI, _oPI.getPurchaseInvoiceId(), i_INV_TRANS_PURCHASE_INVOICE); 						
						
						//2.1 undo PO if from PO
						PurchaseOrderTool.updatePOFromPI(_vPID, true, false, _oConn);
						
						//2.2 undo PR status	        	
						PurchaseReceiptTool.updatePRFromPI(_oPI, _vPID, true, _oConn);
						
						//3. delete journal
						if (b_USE_GL)
						{
							PurchaseJournalTool.deletePurchaseJournal
							(GlAttributes.i_GL_TRANS_PURCHASE_INVOICE, _oPI.getPurchaseInvoiceId(), _oConn);                
						}
						
						//4. cancel if there is any payment through cash flow 
						CashFlowTool.cancelCashFlowByTransID(_oPI.getPurchaseInvoiceId(), _sCancelBy, _oConn);
						
						//5. cancel any AP Journal
						AccountPayableTool.rollbackAP(_oPI.getPurchaseInvoiceId(), _oConn);
					}
					_oPI.setRemark(cancelledBy(_oPI.getRemark(), _sCancelBy));
					_oPI.setStatus(i_PI_CANCELLED);
					_oPI.save(_oConn);
				}
				else
				{
					throw new NestableException (LocaleTool.getString("inv_returned"));					
				}		
			}
			else
			{
				throw new NestableException (LocaleTool.getString("inv_paid"));
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx)
		{
			_oPI.setStatus(i_PROCESSED);
			
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);		
		}
	}

    /**
     * delete only called from SAVE EDIT
     * 
     * @param _sID
     * @param _oConn
     * @throws Exception
     */
    private static void deleteTrans(String _sID, Connection _oConn)
        throws Exception
    {        
        try 
        {
            Criteria oCrit = new Criteria();
            oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID,_sID);
            PurchaseInvoiceDetailPeer.doDelete(oCrit,_oConn);
            
            oCrit = new Criteria();
            oCrit.add(PurchaseInvoiceFreightPeer.PURCHASE_INVOICE_ID,_sID);
            PurchaseInvoiceFreightPeer.doDelete(oCrit,_oConn);
              
            oCrit = new Criteria();
            oCrit.add(PurchaseInvoicePeer.PURCHASE_INVOICE_ID,_sID);
            PurchaseInvoicePeer.doDelete(oCrit,_oConn);
            
        }
        catch (Exception _oEx) 
        {
            throw new NestableException("Delete Transaction Failed: " + _oEx.getMessage(), _oEx);
        }
    }
	
	public static List getUnpaidInvoices(String _sID)
		throws Exception
	{
        return getDueToInvoices (null, null, _sID);
	}
	
	/**
	 * update return qty from purchase return detail
	 * @param _sPIID
	 * @param _vReturnDetail
	 * @param _bCancel
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateReturnQty(String _sPIID, List _vReturnDetail, boolean _bCancel, Connection _oConn )
		throws Exception
	{
        try
        {
            for(int i = 0; i < _vReturnDetail.size(); i++)
            {
                PurchaseReturnDetail oPRetDet = (PurchaseReturnDetail) _vReturnDetail.get(i);
                PurchaseInvoiceDetail oPIDet = 
                	getDetailByDetailID (oPRetDet.getTransactionDetailId(), _oConn);
                if (oPIDet != null)
                {
                    double dLastReturnedQty = oPIDet.getReturnedQty().doubleValue();
					double dReturnedQty = oPRetDet.getQty().doubleValue();
					if (_bCancel) dReturnedQty = dReturnedQty * -1;
                    oPIDet.setReturnedQty(new BigDecimal(dLastReturnedQty + dReturnedQty));
                    oPIDet.save(_oConn);
                }
            }
        }
        catch(Exception _oEx)
		{
		 	String sMsg = "Update Returned Qty From Purchase Return Error : " + _oEx.getMessage();
		 	_oEx.printStackTrace();
		 	log.error(sMsg, _oEx);
		 	throw new NestableException (sMsg, _oEx);
		}
	}
	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sVendorID
	 * @param _sLocationID
	 * @param _iPaymentStatus
	 * @param _iStatus
	 * @param _iLimit
	 * @param _sCurrencyID
	 * @return query result in LargeSelect
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond, 
										String _sKeywords, 
										Date _dStart, 
										Date _dEnd, 
								        String _sVendorID, 
								        String _sLocationID,
										int _iPaymentStatus,
										int _iStatus, 
										int _iLimit, 
										String _sCurrencyID) 
    	throws Exception
    {
		
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			PurchaseInvoicePeer.PURCHASE_INVOICE_DATE, _dStart, _dEnd, _sCurrencyID 
		);

		if (_iCond == i_PO_NO && StringUtil.isNotEmpty(_sKeywords))
		{
			oCrit.addJoin(PurchaseInvoicePeer.PURCHASE_INVOICE_ID, PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID);				
			oCrit.addJoin(PurchaseInvoiceDetailPeer.PURCHASE_ORDER_ID, PurchaseOrderPeer.PURCHASE_ORDER_ID);
			oCrit.add(PurchaseOrderPeer.PURCHASE_ORDER_NO, (Object)_sKeywords ,Criteria.ILIKE);		
			oCrit.setDistinct();
		}
		if (_iCond == i_PR_NO && StringUtil.isNotEmpty(_sKeywords))
		{
			oCrit.addJoin(PurchaseInvoicePeer.PURCHASE_INVOICE_ID, PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID);				
			oCrit.addJoin(PurchaseInvoiceDetailPeer.PURCHASE_RECEIPT_ID, PurchaseReceiptPeer.PURCHASE_RECEIPT_ID);
			oCrit.add(PurchaseReceiptPeer.RECEIPT_NO, (Object)_sKeywords ,Criteria.ILIKE);	
			oCrit.setDistinct();
		}
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseInvoicePeer.VENDOR_ID, _sVendorID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(PurchaseInvoicePeer.LOCATION_ID, _sLocationID);	
		}
		if (_iPaymentStatus > 0) 
		{
			oCrit.add(PurchaseInvoicePeer.PAYMENT_STATUS, _iPaymentStatus);
		}
		if (_iStatus > 0) 
		{
			oCrit.add(PurchaseInvoicePeer.STATUS, _iStatus);
		}	
		oCrit.addAscendingOrderByColumn(PurchaseInvoicePeer.PURCHASE_INVOICE_DATE);		
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.PurchaseInvoicePeer");
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sVendorID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sCreateBy,
								  String _sCurrencyID) 
	throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				PurchaseInvoicePeer.PURCHASE_INVOICE_DATE, _dStart, _dEnd, _sCurrencyID);   
		
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseInvoicePeer.VENDOR_ID, _sVendorID);	
		}
		if(StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(PurchaseInvoicePeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sCreateBy))
		{
			oCrit.add(PurchaseInvoicePeer.CREATE_BY, _sCreateBy);
		}        
		if(_iStatus > 0)
		{
			oCrit.add(PurchaseInvoicePeer.STATUS, _iStatus);
		} 
		log.debug(oCrit);
		return PurchaseInvoicePeer.doSelect(oCrit);
	}


	/////////////////////////////////////////////////////////////////////////////////////
	//report method to group data by item
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static PurchaseInvoice filterTransByPurchaseInvoiceID (List _vPI, String _sPIID)
    	throws Exception
    {

		PurchaseInvoice oPI = null;
		for (int i = 0; i < _vPI.size(); i++)
		{
			oPI = (PurchaseInvoice) _vPI.get(i);
			if ( oPI.getPurchaseInvoiceId().equals(_sPIID) )
			{
				return oPI;
			}
		}
		return oPI;
	}
	public static List getTransDetails (List _vPI)
    	throws Exception
    {
        return getTransDetails(_vPI,"");
    }

	public static List getTransDetails (List _vPI, String _sItemName)
    	throws Exception
    {
		List vPIID = new ArrayList (_vPI.size());
		for (int i = 0; i < _vPI.size(); i++)
		{
			PurchaseInvoice oPI = (PurchaseInvoice) _vPI.get(i);
			vPIID.add (oPI.getPurchaseInvoiceId());
		}
		Criteria oCrit = new Criteria();
		
		if(StringUtil.isNotEmpty(_sItemName))
        {
            oCrit.add(PurchaseInvoiceDetailPeer.ITEM_NAME,
                (Object) SqlUtil.like (PurchaseInvoiceDetailPeer.ITEM_NAME, _sItemName,false,true), Criteria.CUSTOM);
        }
        
		if(vPIID.size() > 0)
		{
		    oCrit.addIn (PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, vPIID);
		}
		return PurchaseInvoiceDetailPeer.doSelect(oCrit);
	}

	
	public static List getDueToInvoices (Date _dStart, Date _dEnd, String _sVendorID) 
    	throws Exception
	{
		Criteria oCrit = new Criteria();
        if (_dStart != null) 
        {
            oCrit.add(PurchaseInvoicePeer.PAYMENT_DUE_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        }
        if (_dEnd != null) 
        {
            oCrit.and(PurchaseInvoicePeer.PAYMENT_DUE_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        }
        oCrit.add(PurchaseInvoicePeer.STATUS, i_PROCESSED);
		oCrit.add(PurchaseInvoicePeer.PAYMENT_STATUS, i_PAYMENT_OPEN);
        if (StringUtil.isNotEmpty(_sVendorID))
        {
            oCrit.add(PurchaseInvoicePeer.VENDOR_ID, _sVendorID); 
        }
        log.debug(oCrit);
		return PurchaseInvoicePeer.doSelect(oCrit);
	}

	public static List getPRNo (String _sPIID) 
		throws Exception
	{
	    StringBuilder oSB = new StringBuilder();
	    oSB.append("SELECT DISTINCT pr.receipt_no AS receipt_no, po.purchase_order_no AS order_no");
	    oSB.append(" FROM purchase_order po, purchase_receipt pr, purchase_invoice_detail pid");
	    oSB.append(" WHERE pid.purchase_receipt_id = pr.purchase_receipt_id ");
	    oSB.append(" AND pr.purchase_order_id = po.purchase_order_id ");
	    oSB.append(" AND pid.purchase_invoice_id = '").append(_sPIID).append("'");
		log.debug(oSB.toString());	    	  
		List vResult = new ArrayList();		
		try 
		{
		    List v = BasePeer.executeQuery(oSB.toString());
		    for (int i = 0; i < v.size(); i++)
		    {
		    	Record oRec = (Record) v.get(i);
		    	String[] s = new String[2];
		    	s[0] = oRec.getValue("receipt_no").asString();
		    	s[1] = oRec.getValue("order_no").asString();
		    	vResult.add(s);
		    }	    
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx.getMessage(), _oEx);
		}
		return vResult;
	}
	
	public static String displayNo (List v, boolean _bPO) 
		throws Exception
	{
		String sNo = "";
		int idx = 0;
		if (_bPO) idx = 1;
		for(int i = 0; i < v.size(); i++)
		{
			String[] s = (String[]) v.get(i);
			sNo += s[idx];
			if (i != 0 && i != (v.size() - 1))
			{
				sNo += ",";
			}
		}
		return sNo;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//end report methods
	/////////////////////////////////////////////////////////////////////////////////////

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
    	throws Exception
    {
		return getPrevOrNextID(PurchaseInvoicePeer.class, PurchaseInvoicePeer.PURCHASE_INVOICE_ID, _sID, _bIsNext);
	}		
	//end next prev
}
package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.Date;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.ReturnReason;
import com.ssti.enterprise.pos.om.ReturnReasonPeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.ReturnReasonTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.IDGenerator;
/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change update & del, call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class ReturnReasonSetup extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		context.put("returnreason", ReturnReasonTool.getInstance());
		try
		{
			if (iOp == i_SETUP_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", ReturnReasonTool.getByID(sID));
    		}
			if (iOp == i_SETUP_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				ReturnReason oData = ReturnReasonTool.getByID(sID);
				ReturnReason oOld = null;
				if (oData == null)
				{
					oData = new ReturnReason();
					oData.setReturnReasonId(IDGenerator.generateSysID());					
				}		
				else
				{
					oOld = oData.copy();
				}
				data.getParameters().setProperties(oData);
				oData.setLastUpdate(new Date());
				oData.setLastUpdateBy(data.getUser().getName());				
				oData.save();				
				if(oOld != null) UpdateHistoryTool.createHistory(oOld, oData, data.getUser().getName(), null);
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_SETUP_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				ReturnReason oData = ReturnReasonTool.getByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(ReturnReasonPeer.RETURN_REASON_ID, oData.getReturnReasonId());
					ReturnReasonPeer.doDelete(oCrit);
					UpdateHistoryTool.createDelHistory(oData, data.getUser().getName(), null);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}
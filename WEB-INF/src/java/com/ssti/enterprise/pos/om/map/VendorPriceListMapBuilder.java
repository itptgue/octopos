package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VendorPriceListMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VendorPriceListMapBuilder";

    /**
     * Item
     * @deprecated use VendorPriceListPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "vendor_price_list";
    }

  
    /**
     * vendor_price_list.VENDOR_PRICE_LIST_ID
     * @return the column name for the VENDOR_PRICE_LIST_ID field
     * @deprecated use VendorPriceListPeer.vendor_price_list.VENDOR_PRICE_LIST_ID constant
     */
    public static String getVendorPriceList_VendorPriceListId()
    {
        return "vendor_price_list.VENDOR_PRICE_LIST_ID";
    }
  
    /**
     * vendor_price_list.VENDOR_PL_CODE
     * @return the column name for the VENDOR_PL_CODE field
     * @deprecated use VendorPriceListPeer.vendor_price_list.VENDOR_PL_CODE constant
     */
    public static String getVendorPriceList_VendorPlCode()
    {
        return "vendor_price_list.VENDOR_PL_CODE";
    }
  
    /**
     * vendor_price_list.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use VendorPriceListPeer.vendor_price_list.VENDOR_ID constant
     */
    public static String getVendorPriceList_VendorId()
    {
        return "vendor_price_list.VENDOR_ID";
    }
  
    /**
     * vendor_price_list.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use VendorPriceListPeer.vendor_price_list.CREATE_DATE constant
     */
    public static String getVendorPriceList_CreateDate()
    {
        return "vendor_price_list.CREATE_DATE";
    }
  
    /**
     * vendor_price_list.EXPIRED_DATE
     * @return the column name for the EXPIRED_DATE field
     * @deprecated use VendorPriceListPeer.vendor_price_list.EXPIRED_DATE constant
     */
    public static String getVendorPriceList_ExpiredDate()
    {
        return "vendor_price_list.EXPIRED_DATE";
    }
  
    /**
     * vendor_price_list.REMARK
     * @return the column name for the REMARK field
     * @deprecated use VendorPriceListPeer.vendor_price_list.REMARK constant
     */
    public static String getVendorPriceList_Remark()
    {
        return "vendor_price_list.REMARK";
    }
  
    /**
     * vendor_price_list.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use VendorPriceListPeer.vendor_price_list.CURRENCY_ID constant
     */
    public static String getVendorPriceList_CurrencyId()
    {
        return "vendor_price_list.CURRENCY_ID";
    }
  
    /**
     * vendor_price_list.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use VendorPriceListPeer.vendor_price_list.UPDATE_DATE constant
     */
    public static String getVendorPriceList_UpdateDate()
    {
        return "vendor_price_list.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("vendor_price_list");
        TableMap tMap = dbMap.getTable("vendor_price_list");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("vendor_price_list.VENDOR_PRICE_LIST_ID", "");
                          tMap.addColumn("vendor_price_list.VENDOR_PL_CODE", "");
                          tMap.addColumn("vendor_price_list.VENDOR_ID", "");
                          tMap.addColumn("vendor_price_list.CREATE_DATE", new Date());
                          tMap.addColumn("vendor_price_list.EXPIRED_DATE", new Date());
                          tMap.addColumn("vendor_price_list.REMARK", "");
                          tMap.addColumn("vendor_price_list.CURRENCY_ID", "");
                          tMap.addColumn("vendor_price_list.UPDATE_DATE", new Date());
          }
}

package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to GeneralLedger
 */
public abstract class BaseGeneralLedger extends BaseObject
{
    /** The Peer class */
    private static final GeneralLedgerPeer peer =
        new GeneralLedgerPeer();

        
    /** The value for the generalLedgerId field */
    private String generalLedgerId;
      
    /** The value for the periodId field */
    private String periodId;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the departmentId field */
    private String departmentId;
      
    /** The value for the projectId field */
    private String projectId;
      
    /** The value for the openingBalance field */
    private BigDecimal openingBalance;
      
    /** The value for the totalDebitAmount field */
    private BigDecimal totalDebitAmount;
      
    /** The value for the totalCreditAmount field */
    private BigDecimal totalCreditAmount;
      
    /** The value for the endingBalance field */
    private BigDecimal endingBalance;
      
    /** The value for the budgetAmount field */
    private BigDecimal budgetAmount;
      
    /** The value for the variance field */
    private BigDecimal variance;
  
    
    /**
     * Get the GeneralLedgerId
     *
     * @return String
     */
    public String getGeneralLedgerId()
    {
        return generalLedgerId;
    }

                        
    /**
     * Set the value of GeneralLedgerId
     *
     * @param v new value
     */
    public void setGeneralLedgerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.generalLedgerId, v))
              {
            this.generalLedgerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PeriodId
     *
     * @return String
     */
    public String getPeriodId()
    {
        return periodId;
    }

                        
    /**
     * Set the value of PeriodId
     *
     * @param v new value
     */
    public void setPeriodId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.periodId, v))
              {
            this.periodId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDebitAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDebitAmount()
    {
        return totalDebitAmount;
    }

                        
    /**
     * Set the value of TotalDebitAmount
     *
     * @param v new value
     */
    public void setTotalDebitAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDebitAmount, v))
              {
            this.totalDebitAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalCreditAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalCreditAmount()
    {
        return totalCreditAmount;
    }

                        
    /**
     * Set the value of TotalCreditAmount
     *
     * @param v new value
     */
    public void setTotalCreditAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalCreditAmount, v))
              {
            this.totalCreditAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EndingBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getEndingBalance()
    {
        return endingBalance;
    }

                        
    /**
     * Set the value of EndingBalance
     *
     * @param v new value
     */
    public void setEndingBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.endingBalance, v))
              {
            this.endingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BudgetAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getBudgetAmount()
    {
        return budgetAmount;
    }

                        
    /**
     * Set the value of BudgetAmount
     *
     * @param v new value
     */
    public void setBudgetAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.budgetAmount, v))
              {
            this.budgetAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Variance
     *
     * @return BigDecimal
     */
    public BigDecimal getVariance()
    {
        return variance;
    }

                        
    /**
     * Set the value of Variance
     *
     * @param v new value
     */
    public void setVariance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.variance, v))
              {
            this.variance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("GeneralLedgerId");
              fieldNames.add("PeriodId");
              fieldNames.add("AccountId");
              fieldNames.add("LocationId");
              fieldNames.add("DepartmentId");
              fieldNames.add("ProjectId");
              fieldNames.add("OpeningBalance");
              fieldNames.add("TotalDebitAmount");
              fieldNames.add("TotalCreditAmount");
              fieldNames.add("EndingBalance");
              fieldNames.add("BudgetAmount");
              fieldNames.add("Variance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("GeneralLedgerId"))
        {
                return getGeneralLedgerId();
            }
          if (name.equals("PeriodId"))
        {
                return getPeriodId();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("TotalDebitAmount"))
        {
                return getTotalDebitAmount();
            }
          if (name.equals("TotalCreditAmount"))
        {
                return getTotalCreditAmount();
            }
          if (name.equals("EndingBalance"))
        {
                return getEndingBalance();
            }
          if (name.equals("BudgetAmount"))
        {
                return getBudgetAmount();
            }
          if (name.equals("Variance"))
        {
                return getVariance();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(GeneralLedgerPeer.GENERAL_LEDGER_ID))
        {
                return getGeneralLedgerId();
            }
          if (name.equals(GeneralLedgerPeer.PERIOD_ID))
        {
                return getPeriodId();
            }
          if (name.equals(GeneralLedgerPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(GeneralLedgerPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(GeneralLedgerPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(GeneralLedgerPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(GeneralLedgerPeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(GeneralLedgerPeer.TOTAL_DEBIT_AMOUNT))
        {
                return getTotalDebitAmount();
            }
          if (name.equals(GeneralLedgerPeer.TOTAL_CREDIT_AMOUNT))
        {
                return getTotalCreditAmount();
            }
          if (name.equals(GeneralLedgerPeer.ENDING_BALANCE))
        {
                return getEndingBalance();
            }
          if (name.equals(GeneralLedgerPeer.BUDGET_AMOUNT))
        {
                return getBudgetAmount();
            }
          if (name.equals(GeneralLedgerPeer.VARIANCE))
        {
                return getVariance();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getGeneralLedgerId();
            }
              if (pos == 1)
        {
                return getPeriodId();
            }
              if (pos == 2)
        {
                return getAccountId();
            }
              if (pos == 3)
        {
                return getLocationId();
            }
              if (pos == 4)
        {
                return getDepartmentId();
            }
              if (pos == 5)
        {
                return getProjectId();
            }
              if (pos == 6)
        {
                return getOpeningBalance();
            }
              if (pos == 7)
        {
                return getTotalDebitAmount();
            }
              if (pos == 8)
        {
                return getTotalCreditAmount();
            }
              if (pos == 9)
        {
                return getEndingBalance();
            }
              if (pos == 10)
        {
                return getBudgetAmount();
            }
              if (pos == 11)
        {
                return getVariance();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(GeneralLedgerPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        GeneralLedgerPeer.doInsert((GeneralLedger) this, con);
                        setNew(false);
                    }
                    else
                    {
                        GeneralLedgerPeer.doUpdate((GeneralLedger) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key generalLedgerId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setGeneralLedgerId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setGeneralLedgerId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getGeneralLedgerId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public GeneralLedger copy() throws TorqueException
    {
        return copyInto(new GeneralLedger());
    }
  
    protected GeneralLedger copyInto(GeneralLedger copyObj) throws TorqueException
    {
          copyObj.setGeneralLedgerId(generalLedgerId);
          copyObj.setPeriodId(periodId);
          copyObj.setAccountId(accountId);
          copyObj.setLocationId(locationId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setProjectId(projectId);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setTotalDebitAmount(totalDebitAmount);
          copyObj.setTotalCreditAmount(totalCreditAmount);
          copyObj.setEndingBalance(endingBalance);
          copyObj.setBudgetAmount(budgetAmount);
          copyObj.setVariance(variance);
  
                    copyObj.setGeneralLedgerId((String)null);
                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public GeneralLedgerPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("GeneralLedger\n");
        str.append("-------------\n")
           .append("GeneralLedgerId      : ")
           .append(getGeneralLedgerId())
           .append("\n")
           .append("PeriodId             : ")
           .append(getPeriodId())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("TotalDebitAmount     : ")
           .append(getTotalDebitAmount())
           .append("\n")
           .append("TotalCreditAmount    : ")
           .append(getTotalCreditAmount())
           .append("\n")
           .append("EndingBalance        : ")
           .append(getEndingBalance())
           .append("\n")
           .append("BudgetAmount         : ")
           .append(getBudgetAmount())
           .append("\n")
           .append("Variance             : ")
           .append(getVariance())
           .append("\n")
        ;
        return(str.toString());
    }
}

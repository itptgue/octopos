package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PrincipalMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PrincipalMapBuilder";

    /**
     * Item
     * @deprecated use PrincipalPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "principal";
    }

  
    /**
     * principal.PRINCIPAL_ID
     * @return the column name for the PRINCIPAL_ID field
     * @deprecated use PrincipalPeer.principal.PRINCIPAL_ID constant
     */
    public static String getPrincipal_PrincipalId()
    {
        return "principal.PRINCIPAL_ID";
    }
  
    /**
     * principal.PRINCIPAL_CODE
     * @return the column name for the PRINCIPAL_CODE field
     * @deprecated use PrincipalPeer.principal.PRINCIPAL_CODE constant
     */
    public static String getPrincipal_PrincipalCode()
    {
        return "principal.PRINCIPAL_CODE";
    }
  
    /**
     * principal.PRINCIPAL_NAME
     * @return the column name for the PRINCIPAL_NAME field
     * @deprecated use PrincipalPeer.principal.PRINCIPAL_NAME constant
     */
    public static String getPrincipal_PrincipalName()
    {
        return "principal.PRINCIPAL_NAME";
    }
  
    /**
     * principal.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PrincipalPeer.principal.DESCRIPTION constant
     */
    public static String getPrincipal_Description()
    {
        return "principal.DESCRIPTION";
    }
  
    /**
     * principal.LOGO
     * @return the column name for the LOGO field
     * @deprecated use PrincipalPeer.principal.LOGO constant
     */
    public static String getPrincipal_Logo()
    {
        return "principal.LOGO";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("principal");
        TableMap tMap = dbMap.getTable("principal");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("principal.PRINCIPAL_ID", "");
                          tMap.addColumn("principal.PRINCIPAL_CODE", "");
                          tMap.addColumn("principal.PRINCIPAL_NAME", "");
                          tMap.addColumn("principal.DESCRIPTION", "");
                          tMap.addColumn("principal.LOGO", "");
          }
}

package com.ssti.enterprise.pos.presentation.screens.report.cashbank;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.BankBookTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/cashbank/BankBook.java,v $
 * Purpose: this class is BankBook Report Screen
 *
 * @author  $Author: albert $
 * @version $Id: BankBook.java,v 1.5 2008/06/29 07:07:48 albert Exp $
 * @see BankBook
 *
 * $Log: BankBook.java,v $
 * 2015-09-28
 * - Change move dStart set so null value allowed and can query bank balance by due date or by trans date only
 */

public class BankBook extends CashBankReport
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		setParams(data);
    		
    		Date dToday = DateUtil.getTodayDate();
    					
	    	String sStartDef = CustomFormatter.formatDate(DateUtil.getStartOfMonthDate(dToday));
	    	String sEndDef = CustomFormatter.formatDate(DateUtil.getEndOfDayDate(dToday));
	    	sStart = data.getParameters().getString("StartDate", sStartDef);
	    	sEnd = data.getParameters().getString("EndDate", sEndDef);	
		    dStart = DateUtil.parseStartDate(sStart);
		    dEnd = DateUtil.parseEndDate(sEnd);
			
			Date dDueFrom = DateUtil.parseStartDate(data.getParameters().getString("StartDue",sStartDef));
			Date dDueTo = DateUtil.parseEndDate(data.getParameters().getString("EndDue",sEndDef));
			
			double dStartBalance = BankBookTool.getBalanceAsOf (sBankID, dStart);
			List vCF = CashFlowTool.getCashFlowByBankID(sBankID, dStart, dEnd, dDueFrom, dDueTo);
			context.put ("CashFlows", vCF);
			context.put ("dStartBalance", new BigDecimal (dStartBalance));
    	}
    	catch (Exception _oEx) 
    	{	
    		log.error(_oEx);
    	}    
    }    
}

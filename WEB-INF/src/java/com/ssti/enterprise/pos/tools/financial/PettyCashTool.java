package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.manager.PettyCashManager;
import com.ssti.enterprise.pos.om.PettyCash;
import com.ssti.enterprise.pos.om.PettyCashBalance;
import com.ssti.enterprise.pos.om.PettyCashBalancePeer;
import com.ssti.enterprise.pos.om.PettyCashPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PettyCashTool.java,v 1.18 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: PettyCashTool.java,v $
 * Revision 1.18  2009/05/04 02:04:13  albert
 * *** empty log message ***
 *
 * Revision 1.17  2008/06/29 07:12:27  albert
 * *** empty log message ***
 *
 * Revision 1.16  2008/04/14 09:16:19  albert
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/03 03:04:26  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PettyCashTool extends BaseTool
{
    private static Log log = LogFactory.getLog(PettyCashTool.class);

	protected static Map m_FIND_PEER = new HashMap();
	
    static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), PettyCashPeer.PETTY_CASH_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), PettyCashPeer.DESCRIPTION);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), PettyCashPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), PettyCashPeer.REFERENCE_NO); 
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), PettyCashPeer.LOCATION_ID); 
	}   

	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);} 	
    
    static PettyCashTool instance = null;
	
	public static synchronized PettyCashTool getInstance() 
	{
		if (instance == null) instance = new PettyCashTool();
		return instance;
	}
	
    private static final int i_PC_PROCESSED = 1;   
    private static final int i_PC_CANCELLED = 2;
    
	//static methods
	public static List getAllPettyCash()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return PettyCashPeer.doSelect(oCrit);
	}

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return PettyCash
	 * @throws Exception
	 */
	public static PettyCash getPettyCashByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PettyCashPeer.PETTY_CASH_ID, _sID);
        List vData = PettyCashPeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (PettyCash) vData.get(0);
		}
		return null;
	}

	public static PettyCashBalance getBalanceByLocationID(String _sLocationID)
		throws Exception
	{
		return getBalanceByLocationID(_sLocationID, null);
	}
	
	/**
	 * 
	 * @param _sLocationID
	 * @param _oConn
	 * @return PettyCashBalance
	 * @throws Exception
	 */
	public static PettyCashBalance getBalanceByLocationID(String _sLocationID, Connection _oConn)
    	throws Exception
    {
		return PettyCashManager.getInstance().getBalanceByLocationID(_sLocationID, _oConn);
	}

	/**
	 * get Balance For Update
	 * @param _sLocationID
	 * @param _oConn
	 * @return PettyCashBalance
	 * @throws Exception
	 */
	private static PettyCashBalance getForUpdate(String _sLocationID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PettyCashBalancePeer.LOCATION_ID, _sLocationID);
        oCrit.setForUpdate(true);
        List vData = PettyCashBalancePeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
			return (PettyCashBalance) vData.get(0);
		}
		return null;
	}
	
	/**
	 * 
	 * @param _oPC
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (PettyCash _oPC, Connection _oConn)
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null)
	    {
		    _oConn = beginTrans ();
		    bStartTrans = true;
		}
		try
		{
			//validate date
			validateDate(_oPC.getTransactionDate(), _oConn);
			
            if (StringUtil.isEmpty(_oPC.getPettyCashId()))
            {
                _oPC.setPettyCashId(IDGenerator.generateSysID());			
                validateID(getPettyCashByID(_oPC.getPettyCashId(), _oConn), "pettyCashNo");
            }
            
		    if (StringUtil.isEmpty(_oPC.getPettyCashNo()))
		    {
				String sLocCode = LocationTool.getLocationCodeByID(_oPC.getLocationId(), _oConn);
			    _oPC.setPettyCashNo(LastNumberTool.get(s_PC_FORMAT, LastNumberTool.i_PETTY_CASH, sLocCode, _oConn));
			    validateNo(PettyCashPeer.PETTY_CASH_NO,_oPC.getPettyCashNo(),PettyCashPeer.class, _oConn);
		    }
			_oPC.setStatus(i_PC_PROCESSED);
			_oPC.save (_oConn);
			updateBalance (_oPC, _oConn);
			if (bStartTrans) 
			{
				commit(_oConn);
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans)
			{
				rollback (_oConn);
			}
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}


	/**
	 * 
	 * @param _oPC
	 * @param _sCancelBy
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelPettyCash(PettyCash _oPC, String _sCancelBy, Connection _oConn) 
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null)
	    {
		    _oConn = beginTrans();
		    bStartTrans = true;
		}
		try
		{
			if (_oPC != null)
			{
				//validate date
				validateDate(_oPC.getTransactionDate(), _oConn);

				_oPC.setDescription(cancelledBy(_oPC.getDescription(), _sCancelBy));
				_oPC.setStatus(i_PC_CANCELLED);
				_oPC.save (_oConn);
				
				_oPC.setPettyCashAmount(new BigDecimal(_oPC.getPettyCashAmount().doubleValue() * -1));			
				updateBalance (_oPC, _oConn);
				if (bStartTrans) 
				{
					commit(_oConn);
				}
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}		
	}
	
	/**
	 * 
	 * @param _oData
	 * @param _oConn
	 * @throws Exception
	 */
	private static void updateBalance (PettyCash _oData, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		PettyCashBalance oBalance = getForUpdate(_oData.getLocationId(), _oConn);
		if(oBalance != null)
		{
			double dLastAccountBalance = oBalance.getPettyCashBalance().doubleValue();
			double dAmount = _oData.getPettyCashAmount().doubleValue();
			if(_oData.getIncomingOutgoing() == i_DEPOSIT)
			{
				oBalance.setPettyCashBalance(new BigDecimal(dLastAccountBalance + dAmount));
			}
			else
			{
				oBalance.setPettyCashBalance(new BigDecimal(dLastAccountBalance - dAmount));
			}	
	    	oBalance.save(_oConn);
		}
		else
		{
			oBalance = new PettyCashBalance();
			oBalance.setPettyCashBalanceId  (IDGenerator.generateSysID());
			oBalance.setLocationId	(_oData.getLocationId ());
			double dAmount = _oData.getPettyCashAmount().doubleValue();
	
	    	if(_oData.getIncomingOutgoing() == i_DEPOSIT)
			{	
				oBalance.setPettyCashBalance  (new BigDecimal(dAmount));
	    		oBalance.setOpeningBalance    (new BigDecimal(dAmount));
			}
			else
			{
				oBalance.setPettyCashBalance  (new BigDecimal(0 - dAmount));
	    		oBalance.setOpeningBalance    (new BigDecimal(0 - dAmount));
	    	}	
	    	oBalance.save(_oConn);
	    }
		PettyCashManager.getInstance().refreshBalance(_oData.getLocationId());
	}		

	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _sTypeID
	 * @param _sEmployeeID
	 * @param _dStart
	 * @param _dEnd
	 * @param _iLimit
	 * @return
	 * @throws Exception
	 */
	public static LargeSelect findData(int _iCond, 
			   						   String _sKeywords, 
									   String _sTypeID, 
									   String _sEmployeeID, 
									   String _sLocationID,
									   Date _dStart, 
									   Date _dEnd,
									   int _iStatus,
									   int _iLimit)
    	throws Exception
    {
		Criteria oCrit =  buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			PettyCashPeer.TRANSACTION_DATE, _dStart, _dEnd, null);
		
		if (StringUtil.isNotEmpty(_sTypeID))
		{
			oCrit.add(PettyCashPeer.PETTY_CASH_TYPE_ID,_sTypeID);
		}
		if (StringUtil.isNotEmpty(_sEmployeeID))
		{
			oCrit.add(PettyCashPeer.EMPLOYEE_ID, _sEmployeeID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(PettyCashPeer.LOCATION_ID, _sLocationID);
		}		
		if (_iStatus > 0)
		{
			oCrit.add(PettyCashPeer.STATUS, _iStatus);
		}
		return new LargeSelect(oCrit,_iLimit,"com.ssti.enterprise.pos.om.PettyCashPeer");
	}
	
	public static String getStatus (int _iStatus)
	{
		if (_iStatus == 1) return LocaleTool.getString("processed");
		if (_iStatus == 2) return LocaleTool.getString("cancelled");
		return "";
	}	
	//-------------------------------------------------------------------------
	//report methods
	//-------------------------------------------------------------------------
    public static List getPettyCashByLocationID(String _sLocationID, String _sTypeID, Date _dFrom, Date _dTo)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add (PettyCashPeer.LOCATION_ID, _sLocationID);
		if (StringUtil.isNotEmpty(_sTypeID))
		{
			oCrit.add (PettyCashPeer.PETTY_CASH_TYPE_ID, _sTypeID);
		}
		oCrit.add (PettyCashPeer.STATUS, i_PC_PROCESSED);
		if (_dFrom != null)
		{
			oCrit.add(PettyCashPeer.TRANSACTION_DATE, _dFrom, Criteria.GREATER_EQUAL);
		}
		if (_dTo != null)
		{
			oCrit.and(PettyCashPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		}        
        return PettyCashPeer.doSelect(oCrit);
	}	

	public static double filterTotalAmountByType(List _vData, int _iType)
    	throws Exception
    {
        
        double dTotal = 0;
        for (int i = 0; i < _vData.size(); i++)
        {
            PettyCash oCash = (PettyCash)_vData.get(i);
            if (oCash != null)
            {
                if (oCash.getIncomingOutgoing() == _iType)
                {
                    dTotal = dTotal + oCash.getPettyCashAmount().doubleValue();
                }
            }
        }
        return dTotal;
	}

	public static double getBalanceAsOf(String _sLocationID, Date _dAsOf)
		throws Exception
	{
	    List vData = getPettyCashByLocationID (_sLocationID, "", _dAsOf, new Date());
	    double dBalance = 0;
	    
	    PettyCashBalance oBalance = getBalanceByLocationID (_sLocationID);
	    if (oBalance != null)
	    {
	        dBalance = oBalance.getPettyCashBalance().doubleValue();
	    }
        double dDeposit = filterTotalAmountByType (vData, i_DEPOSIT);
	    double dWithdrawal = filterTotalAmountByType (vData, i_WITHDRAWAL);
	    dBalance = dBalance - dDeposit + dWithdrawal;
        return dBalance;
	}
    
	public static List getPettyCashTypeList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			PettyCash oTrans = (PettyCash) _vData.get(i);			
			if (!isPettyCashTypeInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isPettyCashTypeInList (List _vTrans, PettyCash _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			PettyCash oTrans = (PettyCash) _vTrans.get(i);			
			if (oTrans.getPettyCashTypeId().equals(_oTrans.getPettyCashTypeId())) {			
				return true;
			}
		}
		return false;
	}
		
	public static List filterTransByPettyCashTypeID (List _vTrans, String _sPettyCashTypeID) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		PettyCash oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (PettyCash ) oIter.next();
			if (!oTrans.getPettyCashTypeId().equals(_sPettyCashTypeID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}

    //next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PettyCashPeer.class, PettyCashPeer.PETTY_CASH_ID, _sID, _bIsNext);
	}
    
    /**
     * get petty cash created in store
     * 
     * @return petty cash created in store since last store 2 ho
     * @throws Exception
     */
    public static List getStoreTrans()
        throws Exception
    {
        Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
        Criteria oCrit = new Criteria();
        if (dLastSyncDate  != null) {
            oCrit.add(PettyCashPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
        }
        //only get IN transfer
        oCrit.add(PettyCashPeer.LOCATION_ID, PreferenceTool.getLocationID());       
        oCrit.add(PettyCashPeer.STATUS, i_PROCESSED);
        return PettyCashPeer.doSelect(oCrit);
    }
}
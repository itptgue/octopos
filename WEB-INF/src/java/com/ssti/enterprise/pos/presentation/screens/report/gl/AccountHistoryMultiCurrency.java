package com.ssti.enterprise.pos.presentation.screens.report.gl;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.gl.SubLedgerTool;

public class AccountHistoryMultiCurrency extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
        context.put ("gltransaction", GlTransactionTool.getInstance());   
        context.put ("subledgertool", SubLedgerTool.getInstance());   
    }    
}

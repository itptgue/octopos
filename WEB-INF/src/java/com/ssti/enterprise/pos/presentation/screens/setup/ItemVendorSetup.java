package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.ItemVendorTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class ItemVendorSetup extends ItemSetup
{
    public void doBuildTemplate(RunData data, Context context)
    {
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			String sItemID = data.getParameters().getString("ItemId");
			String sVendID = data.getParameters().getString("VendorId");
			boolean bPref = data.getParameters().getBoolean("PreferedVendor");
			if (iOp == 1)
			{
				ItemVendorTool.save(sItemID, sVendID, bPref);
	    		data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			else if (iOp == 2)
			{
				sID = data.getParameters().getString("id");
				ItemVendorTool.delete(sID, null);
	    		data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
			}
			context.put("itemvendor", ItemVendorTool.getInstance());
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_SAVE_FAILED) + _oEx.getMessage());
    	}
    }
}
package com.ssti.enterprise.pos.presentation.screens.transaction.pointreward;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.PointTransLoader;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/pointreward/PointRewardTransaction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PointRewardTransaction.java,v 1.6 2009/05/04 02:03:06 albert Exp $
 *
 * $Log: PointRewardTransaction.java,v $
 * Revision 1.6  2009/05/04 02:03:06  albert
 * *** empty log message ***
 *
 * Revision 1.5  2008/08/17 02:19:44  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/07/10 04:49:31  albert
 * *** empty log message ***
 *
 * Revision 1.3  2006/03/29 08:15:12  albert
 * *** empty log message ***
 *
 * Revision 1.2  2005/08/21 14:35:35  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/03/28 09:18:23  Albert
 * *** empty log message ***
 *
 */

public class ImportPointTrans extends TransactionSecureScreen
{   	
    //start screen methods
	private static final String[] a_PERM = {"View Sales Invoice"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {	
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("Op");
    	if(data.getParameters().getInt("save") == 1)
    	{
	    	try 
			{
				FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
				InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
				PointTransLoader oLoader = new PointTransLoader(data.getUser().getName());
				oLoader.loadData (oBufferStream);
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
		    	context.put ("LoadResult", oLoader.getResult());
			}
			catch (Exception _oEx) 
			{
				data.setMessage ( LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
				log.error(_oEx);
				_oEx.printStackTrace();
			}
    	}
    }
}

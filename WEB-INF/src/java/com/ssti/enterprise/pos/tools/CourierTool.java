package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import com.ssti.enterprise.pos.manager.FOBCourierManager;
import com.ssti.enterprise.pos.om.Courier;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CourierTool.java,v 1.9 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: CourierTool.java,v $
 * Revision 1.9  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/07/10 04:51:48  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class CourierTool extends BaseTool 
{
	public static List getAllCourier()
		throws Exception
	{
		return FOBCourierManager.getInstance().getAllCourier();
	}
	
	public static Courier getCourierByID(String _sID)
    	throws Exception
    {
		return FOBCourierManager.getInstance().getCourierByID(_sID, null);
	}
	
	public static Courier getCourierByID(String _sID, Connection _oConn)
		throws Exception
	{
		return FOBCourierManager.getInstance().getCourierByID(_sID, _oConn);
	}

	public static String getCourierNameByID(String _sID)
		throws Exception
	{
		Courier oCourier = getCourierByID (_sID);
		if (oCourier != null)
		{
			return oCourier.getCourierName();
		}
		return "";
	}

	public static BigDecimal getShippingPriceByID(String _sID)
		throws Exception
	{
		Courier oCourier = getCourierByID (_sID);
		if (oCourier != null)
		{
			return oCourier.getShippingPrice();
		}
		return null;
	}
}
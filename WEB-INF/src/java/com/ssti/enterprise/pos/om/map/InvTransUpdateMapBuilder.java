package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class InvTransUpdateMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.InvTransUpdateMapBuilder";

    /**
     * Item
     * @deprecated use InvTransUpdatePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "inv_trans_update";
    }

  
    /**
     * inv_trans_update.UPDATE_ID
     * @return the column name for the UPDATE_ID field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.UPDATE_ID constant
     */
    public static String getInvTransUpdate_UpdateId()
    {
        return "inv_trans_update.UPDATE_ID";
    }
  
    /**
     * inv_trans_update.INVTRANS_ID
     * @return the column name for the INVTRANS_ID field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.INVTRANS_ID constant
     */
    public static String getInvTransUpdate_InvtransId()
    {
        return "inv_trans_update.INVTRANS_ID";
    }
  
    /**
     * inv_trans_update.TX_TYPE
     * @return the column name for the TX_TYPE field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.TX_TYPE constant
     */
    public static String getInvTransUpdate_TxType()
    {
        return "inv_trans_update.TX_TYPE";
    }
  
    /**
     * inv_trans_update.GL_TYPE
     * @return the column name for the GL_TYPE field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.GL_TYPE constant
     */
    public static String getInvTransUpdate_GlType()
    {
        return "inv_trans_update.GL_TYPE";
    }
  
    /**
     * inv_trans_update.TRANS_ID
     * @return the column name for the TRANS_ID field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.TRANS_ID constant
     */
    public static String getInvTransUpdate_TransId()
    {
        return "inv_trans_update.TRANS_ID";
    }
  
    /**
     * inv_trans_update.TRANSDET_ID
     * @return the column name for the TRANSDET_ID field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.TRANSDET_ID constant
     */
    public static String getInvTransUpdate_TransdetId()
    {
        return "inv_trans_update.TRANSDET_ID";
    }
  
    /**
     * inv_trans_update.TRANS_NO
     * @return the column name for the TRANS_NO field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.TRANS_NO constant
     */
    public static String getInvTransUpdate_TransNo()
    {
        return "inv_trans_update.TRANS_NO";
    }
  
    /**
     * inv_trans_update.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.ITEM_ID constant
     */
    public static String getInvTransUpdate_ItemId()
    {
        return "inv_trans_update.ITEM_ID";
    }
  
    /**
     * inv_trans_update.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.LOCATION_ID constant
     */
    public static String getInvTransUpdate_LocationId()
    {
        return "inv_trans_update.LOCATION_ID";
    }
  
    /**
     * inv_trans_update.INV_ACCOUNT
     * @return the column name for the INV_ACCOUNT field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.INV_ACCOUNT constant
     */
    public static String getInvTransUpdate_InvAccount()
    {
        return "inv_trans_update.INV_ACCOUNT";
    }
  
    /**
     * inv_trans_update.OTH_ACCOUNT
     * @return the column name for the OTH_ACCOUNT field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.OTH_ACCOUNT constant
     */
    public static String getInvTransUpdate_OthAccount()
    {
        return "inv_trans_update.OTH_ACCOUNT";
    }
  
    /**
     * inv_trans_update.OLD_SUBCOST
     * @return the column name for the OLD_SUBCOST field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.OLD_SUBCOST constant
     */
    public static String getInvTransUpdate_OldSubcost()
    {
        return "inv_trans_update.OLD_SUBCOST";
    }
  
    /**
     * inv_trans_update.NEW_SUBCOST
     * @return the column name for the NEW_SUBCOST field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.NEW_SUBCOST constant
     */
    public static String getInvTransUpdate_NewSubcost()
    {
        return "inv_trans_update.NEW_SUBCOST";
    }
  
    /**
     * inv_trans_update.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.UPDATE_DATE constant
     */
    public static String getInvTransUpdate_UpdateDate()
    {
        return "inv_trans_update.UPDATE_DATE";
    }
  
    /**
     * inv_trans_update.UPDATE_BY
     * @return the column name for the UPDATE_BY field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.UPDATE_BY constant
     */
    public static String getInvTransUpdate_UpdateBy()
    {
        return "inv_trans_update.UPDATE_BY";
    }
  
    /**
     * inv_trans_update.UPDATE_TX
     * @return the column name for the UPDATE_TX field
     * @deprecated use InvTransUpdatePeer.inv_trans_update.UPDATE_TX constant
     */
    public static String getInvTransUpdate_UpdateTx()
    {
        return "inv_trans_update.UPDATE_TX";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("inv_trans_update");
        TableMap tMap = dbMap.getTable("inv_trans_update");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("inv_trans_update.UPDATE_ID", "");
                          tMap.addColumn("inv_trans_update.INVTRANS_ID", "");
                            tMap.addColumn("inv_trans_update.TX_TYPE", Integer.valueOf(0));
                            tMap.addColumn("inv_trans_update.GL_TYPE", Integer.valueOf(0));
                          tMap.addColumn("inv_trans_update.TRANS_ID", "");
                          tMap.addColumn("inv_trans_update.TRANSDET_ID", "");
                          tMap.addColumn("inv_trans_update.TRANS_NO", "");
                          tMap.addColumn("inv_trans_update.ITEM_ID", "");
                          tMap.addColumn("inv_trans_update.LOCATION_ID", "");
                          tMap.addColumn("inv_trans_update.INV_ACCOUNT", "");
                          tMap.addColumn("inv_trans_update.OTH_ACCOUNT", "");
                            tMap.addColumn("inv_trans_update.OLD_SUBCOST", bd_ZERO);
                            tMap.addColumn("inv_trans_update.NEW_SUBCOST", bd_ZERO);
                          tMap.addColumn("inv_trans_update.UPDATE_DATE", new Date());
                          tMap.addColumn("inv_trans_update.UPDATE_BY", "");
                          tMap.addColumn("inv_trans_update.UPDATE_TX", "");
          }
}

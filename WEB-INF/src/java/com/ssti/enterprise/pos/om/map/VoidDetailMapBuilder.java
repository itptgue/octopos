package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VoidDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VoidDetailMapBuilder";

    /**
     * Item
     * @deprecated use VoidDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "void_detail";
    }

  
    /**
     * void_detail.VOID_DETAIL_ID
     * @return the column name for the VOID_DETAIL_ID field
     * @deprecated use VoidDetailPeer.void_detail.VOID_DETAIL_ID constant
     */
    public static String getVoidDetail_VoidDetailId()
    {
        return "void_detail.VOID_DETAIL_ID";
    }
  
    /**
     * void_detail.SALES_TRANSACTION_ID
     * @return the column name for the SALES_TRANSACTION_ID field
     * @deprecated use VoidDetailPeer.void_detail.SALES_TRANSACTION_ID constant
     */
    public static String getVoidDetail_SalesTransactionId()
    {
        return "void_detail.SALES_TRANSACTION_ID";
    }
  
    /**
     * void_detail.TRANS_NO
     * @return the column name for the TRANS_NO field
     * @deprecated use VoidDetailPeer.void_detail.TRANS_NO constant
     */
    public static String getVoidDetail_TransNo()
    {
        return "void_detail.TRANS_NO";
    }
  
    /**
     * void_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use VoidDetailPeer.void_detail.ITEM_ID constant
     */
    public static String getVoidDetail_ItemId()
    {
        return "void_detail.ITEM_ID";
    }
  
    /**
     * void_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use VoidDetailPeer.void_detail.ITEM_CODE constant
     */
    public static String getVoidDetail_ItemCode()
    {
        return "void_detail.ITEM_CODE";
    }
  
    /**
     * void_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use VoidDetailPeer.void_detail.ITEM_NAME constant
     */
    public static String getVoidDetail_ItemName()
    {
        return "void_detail.ITEM_NAME";
    }
  
    /**
     * void_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use VoidDetailPeer.void_detail.DESCRIPTION constant
     */
    public static String getVoidDetail_Description()
    {
        return "void_detail.DESCRIPTION";
    }
  
    /**
     * void_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use VoidDetailPeer.void_detail.UNIT_ID constant
     */
    public static String getVoidDetail_UnitId()
    {
        return "void_detail.UNIT_ID";
    }
  
    /**
     * void_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use VoidDetailPeer.void_detail.UNIT_CODE constant
     */
    public static String getVoidDetail_UnitCode()
    {
        return "void_detail.UNIT_CODE";
    }
  
    /**
     * void_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use VoidDetailPeer.void_detail.QTY constant
     */
    public static String getVoidDetail_Qty()
    {
        return "void_detail.QTY";
    }
  
    /**
     * void_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use VoidDetailPeer.void_detail.QTY_BASE constant
     */
    public static String getVoidDetail_QtyBase()
    {
        return "void_detail.QTY_BASE";
    }
  
    /**
     * void_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use VoidDetailPeer.void_detail.ITEM_PRICE constant
     */
    public static String getVoidDetail_ItemPrice()
    {
        return "void_detail.ITEM_PRICE";
    }
  
    /**
     * void_detail.CASHIER_NAME
     * @return the column name for the CASHIER_NAME field
     * @deprecated use VoidDetailPeer.void_detail.CASHIER_NAME constant
     */
    public static String getVoidDetail_CashierName()
    {
        return "void_detail.CASHIER_NAME";
    }
  
    /**
     * void_detail.VOID_DATE
     * @return the column name for the VOID_DATE field
     * @deprecated use VoidDetailPeer.void_detail.VOID_DATE constant
     */
    public static String getVoidDetail_VoidDate()
    {
        return "void_detail.VOID_DATE";
    }
  
    /**
     * void_detail.VOID_BY
     * @return the column name for the VOID_BY field
     * @deprecated use VoidDetailPeer.void_detail.VOID_BY constant
     */
    public static String getVoidDetail_VoidBy()
    {
        return "void_detail.VOID_BY";
    }
  
    /**
     * void_detail.VOID_REMARK
     * @return the column name for the VOID_REMARK field
     * @deprecated use VoidDetailPeer.void_detail.VOID_REMARK constant
     */
    public static String getVoidDetail_VoidRemark()
    {
        return "void_detail.VOID_REMARK";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("void_detail");
        TableMap tMap = dbMap.getTable("void_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("void_detail.VOID_DETAIL_ID", "");
                          tMap.addColumn("void_detail.SALES_TRANSACTION_ID", "");
                          tMap.addColumn("void_detail.TRANS_NO", "");
                          tMap.addColumn("void_detail.ITEM_ID", "");
                          tMap.addColumn("void_detail.ITEM_CODE", "");
                          tMap.addColumn("void_detail.ITEM_NAME", "");
                          tMap.addColumn("void_detail.DESCRIPTION", "");
                          tMap.addColumn("void_detail.UNIT_ID", "");
                          tMap.addColumn("void_detail.UNIT_CODE", "");
                            tMap.addColumn("void_detail.QTY", bd_ZERO);
                            tMap.addColumn("void_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("void_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("void_detail.CASHIER_NAME", "");
                          tMap.addColumn("void_detail.VOID_DATE", new Date());
                          tMap.addColumn("void_detail.VOID_BY", "");
                          tMap.addColumn("void_detail.VOID_REMARK", "");
          }
}

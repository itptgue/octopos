package com.ssti.framework.parser;

import java.math.BigDecimal;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Barcode data to handle complex barcode (qty and code is combined in single barcode)
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BarcodeData.java,v 1.3 2007/02/23 14:13:02 albert Exp $ <br>
 *
 * <pre>
 * $Log: BarcodeData.java,v $
 * Revision 1.3  2007/02/23 14:13:02  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class BarcodeData
{   
    private boolean Complex;
    private String ItemCode;
    private BigDecimal Qty;
    private BigDecimal Price;
    
    public boolean getComplex ()  {return Complex ;   }    
    public String getItemCode ()  {return ItemCode;   }
    public BigDecimal getQty ()   {return Qty     ;   }
    public BigDecimal getPrice () {return Price   ;   }

    public void setComplex  (boolean _bComplex  ) {Complex   = _bComplex  ; }
    public void setItemCode (String  _sItemCode ) {ItemCode  = _sItemCode ; }
    public void setQty      (BigDecimal  _dQty  ) {Qty       = _dQty      ; }
    public void setPrice    (BigDecimal  _dPrice) {Price     = _dPrice    ; }
    
    public String toString()
    {
        StringBuilder sData = new StringBuilder();
        sData.append ("Item Code : ")
             .append (ItemCode)
             .append ("\nQty : ")
             .append (getQty())
             .append ("\nPrice : ")
             .append (getPrice())
             .append ("\nComplex : ")
             .append (Complex);
        return sData.toString();
    }
}
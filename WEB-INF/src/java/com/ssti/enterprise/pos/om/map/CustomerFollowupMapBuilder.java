package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerFollowupMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerFollowupMapBuilder";

    /**
     * Item
     * @deprecated use CustomerFollowupPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_followup";
    }

  
    /**
     * customer_followup.CUSTOMER_FOLLOWUP_ID
     * @return the column name for the CUSTOMER_FOLLOWUP_ID field
     * @deprecated use CustomerFollowupPeer.customer_followup.CUSTOMER_FOLLOWUP_ID constant
     */
    public static String getCustomerFollowup_CustomerFollowupId()
    {
        return "customer_followup.CUSTOMER_FOLLOWUP_ID";
    }
  
    /**
     * customer_followup.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerFollowupPeer.customer_followup.CUSTOMER_ID constant
     */
    public static String getCustomerFollowup_CustomerId()
    {
        return "customer_followup.CUSTOMER_ID";
    }
  
    /**
     * customer_followup.CUSTOMER_CONTACT_ID
     * @return the column name for the CUSTOMER_CONTACT_ID field
     * @deprecated use CustomerFollowupPeer.customer_followup.CUSTOMER_CONTACT_ID constant
     */
    public static String getCustomerFollowup_CustomerContactId()
    {
        return "customer_followup.CUSTOMER_CONTACT_ID";
    }
  
    /**
     * customer_followup.CONTACT_NAME
     * @return the column name for the CONTACT_NAME field
     * @deprecated use CustomerFollowupPeer.customer_followup.CONTACT_NAME constant
     */
    public static String getCustomerFollowup_ContactName()
    {
        return "customer_followup.CONTACT_NAME";
    }
  
    /**
     * customer_followup.CONTACT_DATE
     * @return the column name for the CONTACT_DATE field
     * @deprecated use CustomerFollowupPeer.customer_followup.CONTACT_DATE constant
     */
    public static String getCustomerFollowup_ContactDate()
    {
        return "customer_followup.CONTACT_DATE";
    }
  
    /**
     * customer_followup.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use CustomerFollowupPeer.customer_followup.USER_NAME constant
     */
    public static String getCustomerFollowup_UserName()
    {
        return "customer_followup.USER_NAME";
    }
  
    /**
     * customer_followup.CONTACT_TOPIC
     * @return the column name for the CONTACT_TOPIC field
     * @deprecated use CustomerFollowupPeer.customer_followup.CONTACT_TOPIC constant
     */
    public static String getCustomerFollowup_ContactTopic()
    {
        return "customer_followup.CONTACT_TOPIC";
    }
  
    /**
     * customer_followup.CONTACT_REMARK
     * @return the column name for the CONTACT_REMARK field
     * @deprecated use CustomerFollowupPeer.customer_followup.CONTACT_REMARK constant
     */
    public static String getCustomerFollowup_ContactRemark()
    {
        return "customer_followup.CONTACT_REMARK";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_followup");
        TableMap tMap = dbMap.getTable("customer_followup");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_followup.CUSTOMER_FOLLOWUP_ID", "");
                          tMap.addForeignKey(
                "customer_followup.CUSTOMER_ID", "" , "customer" ,
                "customer_id");
                          tMap.addColumn("customer_followup.CUSTOMER_CONTACT_ID", "");
                          tMap.addColumn("customer_followup.CONTACT_NAME", "");
                          tMap.addColumn("customer_followup.CONTACT_DATE", new Date());
                          tMap.addColumn("customer_followup.USER_NAME", "");
                          tMap.addColumn("customer_followup.CONTACT_TOPIC", "");
                          tMap.addColumn("customer_followup.CONTACT_REMARK", "");
          }
}

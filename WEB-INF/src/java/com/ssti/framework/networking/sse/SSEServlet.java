package com.ssti.framework.networking.sse;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.om.security.TurbineUser;

import com.ssti.framework.tools.StringUtil;


/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: MVCServlet.java,v 1.2 2007/02/23 14:12:55 albert Exp $ <br>
 *
 * <pre>
 * $Log: MVCServlet.java,v $
 * 
 * From http://cjihrig.com/blog/the-server-side-of-server-sent-events/
 * </pre><br>
 *
 */ 

@WebServlet(asyncSupported = true)

public class SSEServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Log log = LogFactory.getLog(SSEServlet.class);
	
	static final String SEND = "send";
	static final String SUBS = "subs";
		
	SseBroadcaster broadcaster = new SseBroadcaster();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		Scanner scanner = new Scanner(req.getInputStream());
//		StringBuilder sb = new StringBuilder();
//		while(scanner.hasNextLine()) {
//			sb.append(scanner.nextLine());
//		}
//		System.out.println("sb = " + sb);
//		broadcaster.broadcast("message",sb.toString());
		doGet(req,resp);
	}
	
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
    	HttpSession ses = req.getSession();    	
    	req.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
    	String op = req.getParameter("op");
    	if(StringUtil.isEqual(op, SEND))    	
    	{
    		String id = req.getParameter("id");  
    		String msg = req.getParameter("msg");
    		System.out.println("broadcast:" + id + " msg:" + msg);
    		broadcaster.broadcast(id, "message",msg);
    	}
    	else if (StringUtil.isEmpty(op) || StringUtil.isEqual(op,SUBS))
    	{
    		TurbineUser usr = (TurbineUser) ses.getAttribute("turbine.user");
    		if(usr != null)
    		{
	    		broadcaster.addSubscriber(usr.getName(), req);
    		}
    	}
    }

	@Override
	public void destroy() {
		broadcaster.close();
		super.destroy();
	}

}


package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;
import com.ssti.framework.tools.StringUtil;

public class IssueReceiptTransaction extends TransactionSecureScreen
{
	protected static final String[] a_PERM = {"View Issue Receipt"};
	
	protected boolean isAuthorized(RunData data)
        throws Exception
    {
		return isAuthorized (data, a_PERM);
    }

    //start screen methods	
	protected static final int i_OP_ADD_DETAIL  = 1;                                 
	protected static final int i_OP_DEL_DETAIL  = 2;                                 
	protected static final int i_OP_NEW_TRANS   = 3;                                 
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
	protected static final int i_OP_ADD_ITEMS   = 5;                                 
	
	protected static final int i_OP_COPY_TRANS  = 6;
	protected static final int i_OP_IMPORT_FROM_EXCEL  = 8;			
	  	
  	protected IssueReceipt oIR = null;
    protected List vIRD = null;
    protected HttpSession oSes;  
  
  	public void doBuildTemplate(RunData data, Context context)
  	{
  		super.doBuildTemplate(data, context);
  		
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH) {
				addDetailData (data);
			}	
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH) {
				delDetailData (data);
			}	
			else if ( iOp == i_OP_NEW_TRANS ) {
				createNewTrans ();
			}
			else if (iOp == i_OP_VIEW_TRANS ) {
				getData (data, context);
			}
			else if (iOp == i_OP_ADD_ITEMS && !b_REFRESH)
			{
				addItems (data);
			}						
			else if (iOp == i_OP_IMPORT_FROM_EXCEL && !b_REFRESH) {
				//getData (data, context);
			}
			else if (iOp == i_OP_COPY_TRANS && !b_REFRESH) {
				copyTrans (data);
			}
			else if (iOp == i_OP_IMPORT_FROM_PDT && !b_REFRESH)
			{
				//importPDTFile(data, context);
			}
		}
		catch (Exception _oEx) 
		{
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_IR, oSes.getAttribute (s_IR));
    	context.put(s_IRD, oSes.getAttribute (s_IRD));
    }
    
	protected void initSession ( RunData data )
	{	
		synchronized(this)
		{
			oSes = data.getSession();
			if ( oSes.getAttribute (s_IR) == null ) 
			{
				oSes.setAttribute (s_IR, new IssueReceipt());
				oSes.setAttribute (s_IRD, new ArrayList());
			}
			oIR  = (IssueReceipt) oSes.getAttribute (s_IR);
			vIRD = (List) oSes.getAttribute (s_IRD);
		}
	}

    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
    	IssueReceiptTool.updateDetail(data, vIRD);
    	IssueReceiptDetail oDetail = new IssueReceiptDetail();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			data.getParameters().setProperties (oDetail);	
			oDetail.setItemCode (oItem.getItemCode());
		}
		else
		{
			return;
		}				
		
		if (!isExist (oDetail)) 
		{
	    	if (b_INVENTORY_FIRST_LAST)
	    	{
				vIRD.add (0, oDetail);
			}
			else
			{
				vIRD.add (oDetail);
			}
		}
		IssueReceiptTool.setHeader(data, oIR);
		
		oSes.setAttribute (s_IR, oIR); 
		oSes.setAttribute (s_IRD, vIRD);     
    }	
	    
    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	IssueReceiptTool.updateDetail(data, vIRD);
    	int iDelNo = data.getParameters().getInt("No") - 1;
		if (vIRD.size() > iDelNo) 
		{
			vIRD.remove (iDelNo);
			oSes.setAttribute (s_IRD, vIRD);		
		}    	
		IssueReceiptTool.setHeader(data, oIR);
    }
    
    protected void createNewTrans () 
    {
    	synchronized (this)
    	{
    		oIR = new IssueReceipt();
    		vIRD = new ArrayList();
    		
    		oSes.setAttribute (s_IR,  oIR);
    		oSes.setAttribute (s_IRD, vIRD);
    	}
    }
    
    protected boolean isExist (IssueReceiptDetail _oData) 
    {
    	for (int i = 0; i < vIRD.size(); i++) 
    	{
    		IssueReceiptDetail oExistData = (IssueReceiptDetail) vIRD.get (i);
    		if ( !PreferenceTool.useBatchNo() &&
    			 oExistData.getItemCode().equals(_oData.getItemCode()) && 
    			 oExistData.getItemId().equals(_oData.getItemId()) &&
    			 oExistData.getCost().doubleValue() == _oData.getCost().doubleValue())  
    		{
				mergeData (oExistData, _oData);
				return true;    			
    		}		
    	}
    	return false;
    } 
    
    protected void mergeData (IssueReceiptDetail _oOldData, IssueReceiptDetail _oNewData) 
    {		
    	double dQty = _oOldData.getQtyChanges().doubleValue() + _oNewData.getQtyChanges().doubleValue();
    	_oOldData.setQtyChanges (new BigDecimal(dQty));    	
    	_oNewData = null;
    }
    
 	protected void getData ( RunData data, Context context ) 
 		throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{
    			oIR = IssueReceiptTool.getHeaderByID (sID);
    			vIRD = IssueReceiptTool.getDetailsByID (sID);
    			oSes.setAttribute (s_IR, oIR);
				oSes.setAttribute (s_IRD, vIRD);
				
				//load tmp batch
    			if (oIR != null && oIR.getStatus() == i_PENDING)
    			{
    				BatchTransactionTool.setTmpBatch(sID, i_INV_TRANS_RECEIPT_UNPLANNED, vIRD, null);
    			}
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }

	/**
	 * @param data
	 */
	protected void copyTrans(RunData data) 
		throws Exception
	{
		try
		{
			String sTransID = data.getParameters().getString("TransId");
	    	IssueReceiptTool.updateDetail(data, vIRD);
			
			if (StringUtil.isNotEmpty (sTransID))
			{			
				List vTrans = IssueReceiptTool.getDetailsByID(sTransID);
				vIRD = new ArrayList(vTrans.size());
				for (int i = 0; i < vTrans.size(); i++)
				{
					IssueReceiptDetail oDet = (IssueReceiptDetail) vTrans.get(i);
					IssueReceiptDetail oNewDet = oDet.copy();
					vIRD.add(oNewDet);
				}
				oSes.setAttribute (s_IR, oIR);
				oSes.setAttribute (s_IRD, vIRD);
			}
			IssueReceiptTool.setHeader(data, oIR);		
		}
		catch (Exception _oEx)
		{
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
		}
	}
	
	/**
	 * Add items from query string, we sent item id in 00123,00124 
	 * 
	 * @param data
	 * @throws Exception
	 */
	protected void addItems(RunData data)
	throws Exception
	{
		//save current updated qty first
		IssueReceiptTool.updateDetail(data, vIRD);
		
		String sItemID = data.getParameters().getString ("items");		
		log.debug (sItemID);
		if (sItemID != null && !sItemID.equals(""))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while (oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddItem (sItemID, data);
				}
			}
			else 
			{
				processAddItem (sItemID, data);
			}
		}
		IssueReceiptTool.setHeader(data, oIR);
	}
	
	protected void processAddItem ( String _sItemID, RunData data)
	throws Exception
	{
		IssueReceiptDetail oDetail = new IssueReceiptDetail();
		Item oItem = ItemTool.getItemByID(_sItemID);
		if (oItem != null)
		{
			double dCost = InventoryLocationTool.getItemCost(_sItemID, data.getParameters().getString("LocationId")).doubleValue();
			if (dCost == 0)
			{
				dCost = oItem.getLastPurchasePrice().doubleValue() * oItem.getLastPurchaseRate().doubleValue();
			}
			oDetail.setItemId(oItem.getItemId());
			oDetail.setItemCode (oItem.getItemCode());
			oDetail.setQtyBase(bd_ONE);
			oDetail.setQtyChanges(bd_ONE);			
			oDetail.setCost(new BigDecimal(dCost));
			oDetail.setUnitId(oItem.getUnitId());
			oDetail.setUnitCode(oItem.getUnit().getUnitCode());
		}
		else
		{
			return;
		}				
		
		if (!isExist (oDetail)) 
		{
	    	if (b_INVENTORY_FIRST_LAST)
	    	{
				vIRD.add (0, oDetail);
			}
			else
			{
				vIRD.add (oDetail);
			}
		}
	}
}

package com.ssti.enterprise.pos.tools.inventory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;

public class InventoryCutOffTool extends BaseTool 
{
	static InventoryCutOffTool instance = null;
	
	public static synchronized InventoryCutOffTool getInstance() 
	{
		if (instance == null) instance = new InventoryCutOffTool();
		return instance;
	}		
	
//	inventory_transaction_id | character varying(30)        | not null
//	 transaction_id           | character varying(30)       | not null
//	 transaction_no           | character varying(20)       | not null default ''::character varying
//	 transaction_detail_id    | character varying(30)       | not null default ''::character varying
//	 transaction_type         | integer                     | not null
//	 transaction_date         | timestamp without time zone | not null
//	 location_id              | character varying(30)       | not null
//	 location_name            | character varying(50)       | not null
//	 item_id                  | character varying(30)       | not null
//	 item_code                | character varying(20)       | not null
//	 qty_changes              | numeric(12,4)               | not null
//	 price                    | numeric(16,4)               | not null
//	 cost                     | numeric(16,4)               | not null
//	 total_cost               | numeric(18,4)               | not null default 0
//	 qty_balance              | numeric(16,4)               | not null default 0
//	 value_balance            | numeric(18,4)               | not null default 0
//	 description              | text                        | not null
//	 create_date              | timestamp without time zone | 
//	 update_date              | timestamp without time zone | 

	
	public static void createCutOffInvTrans(Date dDT)
		throws Exception
	{
		Connection oConn = BaseTool.beginTrans();
		Statement stmt = oConn.createStatement();
		StringBuilder oSEL = new StringBuilder();
		oSEL.append(" SELECT item_id, location_id, item_code, location_name, inventory_location_id, current_qty, item_cost ")
			.append(" FROM inventory_location WHERE current_qty > 0 order by item_id, location_id")
		;
		ResultSet rs = stmt.executeQuery(oSEL.toString());
		StringBuilder oSQL = new StringBuilder();
		oSQL.append(" INSERT INTO inventory_transaction (inventory_transaction_id, transaction_id, transaction_no, transaction_detail_id, ") 
		    .append(" transaction_type, transaction_date, location_id, location_name, ")
		    .append(" item_id, item_code, qty_changes, price, cost, total_cost, ") 
		    .append(" qty_balance, value_balance, description, create_date, update_date) ")
		    .append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
		;
		
		if(dDT == null)
		{
			dDT = DateUtil.getTodayDate();
		}
		java.sql.Date dSQDT = new java.sql.Date(dDT.getTime());
		
		String sDT = CustomFormatter.formatCustomDateTime(dDT, "yyyyMMdd");
		String sTxNo = "CO-" + sDT;
		String sDesc = "Data Cut off " + CustomFormatter.formatDateTime(dDT);
		PreparedStatement pstmt = oConn.prepareStatement(oSQL.toString());		
		while(rs.next())
		{			
			String sItemID = rs.getString(1);
			String sLocID = rs.getString(2);
			String sItemCD = rs.getString(3);
			String sLocNM = rs.getString(4);
			String sInvID = rs.getString(5);
			double dQty = rs.getDouble(6);
			double dCost = rs.getDouble(7);
			double dVal = dQty * dCost;
						
			pstmt.setString(1, sInvID);
			pstmt.setString(2, sInvID);
			pstmt.setString(3, sTxNo);
			pstmt.setString(4, sInvID);
			pstmt.setInt(5, i_INV_TRANS_RECEIPT_UNPLANNED);
			pstmt.setDate(6, dSQDT);
			pstmt.setString(7, sLocID);
			pstmt.setString(8, sLocNM);
			pstmt.setString(9, sItemID);
			pstmt.setString(10, sItemCD);
			pstmt.setDouble(11, dQty);
			pstmt.setDouble(12, dCost);
			pstmt.setDouble(13, dCost);			
			pstmt.setDouble(14, dVal);
			pstmt.setDouble(15, dQty);
			pstmt.setDouble(16, dVal);
			pstmt.setString(17, sDesc);
			pstmt.setDate  (18, dSQDT);
			pstmt.setDate  (19, dSQDT);
			
			System.out.println(" Processing: " + sItemCD);
			
			pstmt.executeUpdate();			
		}
		BaseTool.commit(oConn);
	}
}

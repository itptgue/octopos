
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Mon Feb 07 14:16:35 ICT 2005]
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class CashFlowJournal
    extends com.ssti.enterprise.pos.om.BaseCashFlowJournal
    implements Persistent
{
    public Account getAccount()   
        throws Exception
    {
        if (StringUtil.isNotEmpty(getAccountId()))
        {
            return AccountTool.getAccountByID(getAccountId());
        }
        return null;
    }
    
    public CashFlowType getCFT()   
        throws Exception
    {
        if (StringUtil.isNotEmpty(getCashFlowTypeId()))
        {
            return CashFlowTypeTool.getTypeByID(getCashFlowTypeId(),null);
        }
        return null;
    }

    public Currency getCurrency()   
        throws Exception
    {
        if (StringUtil.isNotEmpty(getCurrencyId()))
        {
            return CurrencyTool.getCurrencyByID(getCurrencyId());
        }
        return null;
    }
    
    public Location getLocation()   
        throws Exception
    {
        if (StringUtil.isNotEmpty(getLocationId()))
        {
            return LocationTool.getLocationByID(getLocationId());
        }
        return null;
    }          
}

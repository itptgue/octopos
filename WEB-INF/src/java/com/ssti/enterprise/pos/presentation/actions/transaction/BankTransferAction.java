package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.BankTransferLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

public class BankTransferAction extends TransactionSecureAction
{
   	private static final String s_VIEW_PERM    = "View Cash Management";
	private static final String s_CREATE_PERM  = "Create Cash Management";
	private static final String s_CANCEL_PERM  = "Cancel Cash Management";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doInsert",null) != null)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
        if(data.getParameters().getString("eventSubmit_doCancel",null) != null)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
            String sLoader = data.getParameters().getString("loader");
            TransactionLoader oLoader = null;
            if (StringUtil.isNotEmpty(sLoader))
            {
                oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
            }
            else
            {
                oLoader = new BankTransferLoader(data.getUser().getName());
            }            
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
			context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
			log.error ( sError, _oEx );
			data.setMessage (sError);
		}
	} 
}
package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DiscountCouponMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.DiscountCouponMapBuilder";

    /**
     * Item
     * @deprecated use DiscountCouponPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "discount_coupon";
    }

  
    /**
     * discount_coupon.DISCOUNT_COUPON_ID
     * @return the column name for the DISCOUNT_COUPON_ID field
     * @deprecated use DiscountCouponPeer.discount_coupon.DISCOUNT_COUPON_ID constant
     */
    public static String getDiscountCoupon_DiscountCouponId()
    {
        return "discount_coupon.DISCOUNT_COUPON_ID";
    }
  
    /**
     * discount_coupon.DISCOUNT_ID
     * @return the column name for the DISCOUNT_ID field
     * @deprecated use DiscountCouponPeer.discount_coupon.DISCOUNT_ID constant
     */
    public static String getDiscountCoupon_DiscountId()
    {
        return "discount_coupon.DISCOUNT_ID";
    }
  
    /**
     * discount_coupon.COUPON_NO
     * @return the column name for the COUPON_NO field
     * @deprecated use DiscountCouponPeer.discount_coupon.COUPON_NO constant
     */
    public static String getDiscountCoupon_CouponNo()
    {
        return "discount_coupon.COUPON_NO";
    }
  
    /**
     * discount_coupon.COUPON_VALUE
     * @return the column name for the COUPON_VALUE field
     * @deprecated use DiscountCouponPeer.discount_coupon.COUPON_VALUE constant
     */
    public static String getDiscountCoupon_CouponValue()
    {
        return "discount_coupon.COUPON_VALUE";
    }
  
    /**
     * discount_coupon.COUPON_TYPE
     * @return the column name for the COUPON_TYPE field
     * @deprecated use DiscountCouponPeer.discount_coupon.COUPON_TYPE constant
     */
    public static String getDiscountCoupon_CouponType()
    {
        return "discount_coupon.COUPON_TYPE";
    }
  
    /**
     * discount_coupon.CREATE_TRANS_ID
     * @return the column name for the CREATE_TRANS_ID field
     * @deprecated use DiscountCouponPeer.discount_coupon.CREATE_TRANS_ID constant
     */
    public static String getDiscountCoupon_CreateTransId()
    {
        return "discount_coupon.CREATE_TRANS_ID";
    }
  
    /**
     * discount_coupon.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use DiscountCouponPeer.discount_coupon.CREATE_DATE constant
     */
    public static String getDiscountCoupon_CreateDate()
    {
        return "discount_coupon.CREATE_DATE";
    }
  
    /**
     * discount_coupon.VALID_FROM
     * @return the column name for the VALID_FROM field
     * @deprecated use DiscountCouponPeer.discount_coupon.VALID_FROM constant
     */
    public static String getDiscountCoupon_ValidFrom()
    {
        return "discount_coupon.VALID_FROM";
    }
  
    /**
     * discount_coupon.VALID_TO
     * @return the column name for the VALID_TO field
     * @deprecated use DiscountCouponPeer.discount_coupon.VALID_TO constant
     */
    public static String getDiscountCoupon_ValidTo()
    {
        return "discount_coupon.VALID_TO";
    }
  
    /**
     * discount_coupon.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use DiscountCouponPeer.discount_coupon.PAYMENT_TYPE_ID constant
     */
    public static String getDiscountCoupon_PaymentTypeId()
    {
        return "discount_coupon.PAYMENT_TYPE_ID";
    }
  
    /**
     * discount_coupon.USE_TRANS_ID
     * @return the column name for the USE_TRANS_ID field
     * @deprecated use DiscountCouponPeer.discount_coupon.USE_TRANS_ID constant
     */
    public static String getDiscountCoupon_UseTransId()
    {
        return "discount_coupon.USE_TRANS_ID";
    }
  
    /**
     * discount_coupon.USE_CASHIER
     * @return the column name for the USE_CASHIER field
     * @deprecated use DiscountCouponPeer.discount_coupon.USE_CASHIER constant
     */
    public static String getDiscountCoupon_UseCashier()
    {
        return "discount_coupon.USE_CASHIER";
    }
  
    /**
     * discount_coupon.USE_DATE
     * @return the column name for the USE_DATE field
     * @deprecated use DiscountCouponPeer.discount_coupon.USE_DATE constant
     */
    public static String getDiscountCoupon_UseDate()
    {
        return "discount_coupon.USE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("discount_coupon");
        TableMap tMap = dbMap.getTable("discount_coupon");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("discount_coupon.DISCOUNT_COUPON_ID", "");
                          tMap.addColumn("discount_coupon.DISCOUNT_ID", "");
                          tMap.addColumn("discount_coupon.COUPON_NO", "");
                            tMap.addColumn("discount_coupon.COUPON_VALUE", bd_ZERO);
                            tMap.addColumn("discount_coupon.COUPON_TYPE", Integer.valueOf(0));
                          tMap.addColumn("discount_coupon.CREATE_TRANS_ID", "");
                          tMap.addColumn("discount_coupon.CREATE_DATE", new Date());
                          tMap.addColumn("discount_coupon.VALID_FROM", new Date());
                          tMap.addColumn("discount_coupon.VALID_TO", new Date());
                          tMap.addColumn("discount_coupon.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("discount_coupon.USE_TRANS_ID", "");
                          tMap.addColumn("discount_coupon.USE_CASHIER", "");
                          tMap.addColumn("discount_coupon.USE_DATE", new Date());
          }
}

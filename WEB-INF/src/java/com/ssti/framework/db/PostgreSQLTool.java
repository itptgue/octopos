package com.ssti.framework.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.Turbine;

import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.DataStreamer;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PostgreSQLTool.java,v 1.3 2008/08/17 02:17:02 albert Exp $ <br>
 *
 * <pre>
 * $Log: PostgreSQLTool.java,v $
 * Revision 1.3  2008/08/17 02:17:02  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/02/23 14:11:39  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public class PostgreSQLTool implements DBTool 
{
	static final Log log = LogFactory.getLog(PostgreSQLTool.class); 
	
	//default values
	private static String s_DB_USER = "root";
	private static String s_DB_PWD =  "crossfire";
	
	private static String s_USER = "-U" + s_DB_USER;

	private static String m_sDBName = "pos";
	private String m_sFile = "data_backup.sql";
	private String m_sPath = "/DBServer/bin/";
	private String m_sInit = "/AppServer/scripts/init/init-db.sql";
	
	//sb to save operation result
	private StringBuilder m_oResult = new StringBuilder();
	private StringBuilder m_oError = new StringBuilder();

	public PostgreSQLTool  (String _sDBName, String _sFile)
	{
		init (_sDBName, "", "", _sFile, "");
	}
	
	public PostgreSQLTool  (String _sDBName, String _sFile, String _sPath)
	{
		init (_sDBName, "", "", _sFile, _sPath);
	}
	
	public PostgreSQLTool  (String _sDBName, String _sUser, String _sPwd ,String _sFile, String _sPath)
	{
		init (_sDBName, _sUser, _sPwd, _sFile, _sPath);	
	}
	
	/**
	 * 
	 * @param _sDBName the DB name targeted for operations
	 * @param _sUser DB username 
	 * @param _sPwd DB password
	 * @param _sFile the SQL file that will be invoked against the DB (empty used for new DB)
	 * @param _sPath the executable path to the DBServer Client (mysql client)
	 *        in retailsoft mostly located in retailsoft.home/DBServer/bin
	 */
	private void init (String _sDBName, String _sUser, String _sPwd ,String _sFile, String _sPath)
	{
		if (StringUtil.isNotEmpty(_sUser)) {s_DB_USER = _sUser; s_USER = "-U" + s_DB_USER;}
			
		if (StringUtil.isNotEmpty(_sDBName)) m_sDBName = _sDBName;
		if (StringUtil.isNotEmpty(_sFile)) m_sFile = _sFile;					
		
		if (StringUtil.isNotEmpty(_sPath))
		{
			m_sPath = _sPath;
			m_sInit = _sPath + m_sInit;			
		}
		else
		{
			String sPath = System.getProperty(Attributes.s_HOME_ENV);

			if (IOTool.isWindows())
			{
				sPath = Turbine.getApplicationRoot();
				log.info ("getting path value from Turbine.getApplicationRoot() : " + sPath);
				m_sPath  = sPath + m_sPath;
			}
	
	    	if ((IOTool.isUnix() || IOTool.isMac()))
	    	{
	    		m_sPath = "/opt/postgres/bin/";
	    	}			
		}
		
		//correct path according to OS specific separator
		m_sPath = m_sPath.replace("/", s_FILE_SEPARATOR);
		m_sInit = m_sInit.replace("/", s_FILE_SEPARATOR);

		m_sPath = m_sPath.replace("\\", s_FILE_SEPARATOR);
		m_sInit = m_sInit.replace("\\", s_FILE_SEPARATOR);
		
		log.info ("db:" + m_sDBName + " file: " +  m_sFile + " exec: " + m_sPath + " init: " + m_sInit );		
	}
	
	public String getResult() 
	{
		return m_oResult.toString();
	}	
	
	/**
	 * pg_dump
	 */
  	public void backup() 
  	{
		try 
    	{ 			
			List oCmd = new ArrayList();
			
			oCmd.add(m_sPath + "pg_dump");
			oCmd.add(s_USER);
			oCmd.add("-c");
			oCmd.add("-f" + m_sFile );							
			oCmd.add(m_sDBName);
			
			log.info (oCmd);
			
			ProcessBuilder oPB = new ProcessBuilder(oCmd);
			
			Process oProc = oPB.start(); 
			
			DataStreamer oErrorStreamer  = new DataStreamer(oProc.getErrorStream(), "ERROR", m_oError);            
			DataStreamer oOutStreamer  = new DataStreamer(oProc.getInputStream(), "OUTPUT", m_oResult);            
				
			oErrorStreamer.start();
			oOutStreamer.start();
			
			int iExitCode = oProc.waitFor();
			
			if (iExitCode == 0) 
			{
				m_oResult.append("\nDatabase '" + m_sDBName + "' Back Up Successful ");
				m_oResult.append("\n\nBackup File: " + m_sFile);				
				log.info(m_oResult.toString());
			}
			else 
			{
				m_oResult.insert(0, 
				    "\nData Back Up Failed Error Code : " + iExitCode + System.getProperty("line.separator"));
				
				log.error(m_oError);
			}
		}
  		catch (Exception _oEx)
  		{
			log.error ("PGDump Process Failed : " + _oEx.getMessage());
			m_oResult.append("Backup Process Failed : " + _oEx.getMessage());
			_oEx.printStackTrace();
  		}
  	} 

  	private String m_sTempBatch = "";
  	
  	/**
  	 * restore a db using a temp batch file
  	 */
  	public void restore() 
  	{
		try 
    	{ 	
			List oCmd = new ArrayList();
			
			oCmd.add(m_sPath + "psql");
			oCmd.add(s_USER);
			oCmd.add("-q");							
			oCmd.add("-f" + m_sFile );							
			oCmd.add(m_sDBName);
			
			log.info (oCmd);
			
			ProcessBuilder oPB = new ProcessBuilder(oCmd);
			Process oProc = oPB.start(); 
			
			DataStreamer oErrorStreamer  = new DataStreamer(oProc.getErrorStream(), "ERROR", m_oError);            
			DataStreamer oOutStreamer  = new DataStreamer(oProc.getInputStream(), "OUTPUT", m_oResult);            
				
			oErrorStreamer.start();
			oOutStreamer.start();
			
			int iExitCode = oProc.waitFor();
			
			if (iExitCode == 0) 
			{
				m_oResult.append("\nData File : ").append(m_sFile)
						.append(" Restored Successfully To Database '" + m_sDBName + "'");
				
				log.info(m_oResult.toString());
			}
			else 
			{
				m_oResult.insert(0, 
				    "\nData Restore Failed Error Code : " + iExitCode + s_LINE_SEPARATOR);
				
				log.error(m_oError);
			}

		}
  		catch (Exception _oEx)
  		{
			log.error ("PSQL Failed : " + _oEx.getMessage());
			m_oResult.append("Restore Process Failed : " + _oEx.getMessage());
			_oEx.printStackTrace();
  		}
  	}

  	/**
  	 * create a newDB
  	 */
  	public void newDB() 
  	{
		try 
    	{ 	
			List oCmd = new ArrayList();
			
			oCmd.add(m_sPath + "createdb");
			oCmd.add(s_USER);
			oCmd.add("-q");			
			oCmd.add(m_sDBName);
			
			log.info(oCmd);
			
			ProcessBuilder oPB = new ProcessBuilder(oCmd);
			
			Process oProc = oPB.start(); 
			
			DataStreamer oErrorStreamer  = new DataStreamer(oProc.getErrorStream(), "ERROR", m_oResult);            
			DataStreamer oOutStreamer  = new DataStreamer(oProc.getInputStream(), "OUTPUT", m_oResult);            
				
			oErrorStreamer.start();
			oOutStreamer.start();
			
			int iExitCode = oProc.waitFor();
			
			if (iExitCode == 0) 
			{
				m_oResult.append("\nDatabase '").append(m_sDBName).append("' Created Successfully ");
				log.info(m_oResult.toString());
			}
			else 
			{
				m_oResult.insert(0, 
				    "\nCreate New DB Failed Error Code : " + iExitCode + s_LINE_SEPARATOR);

				log.error(m_oResult);
			}
			
			m_sFile = m_sInit;
			restore();
		}
  		catch (Exception _oEx)
  		{
			log.error ("PostgreSQL Create DB Failed : " + _oEx.getMessage());
			m_oResult.append("PostgreSQL Create DB Failed : " + _oEx.getMessage());
			_oEx.printStackTrace();
  		}
  	}       
}

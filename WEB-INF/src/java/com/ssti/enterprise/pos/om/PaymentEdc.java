
package com.ssti.enterprise.pos.om;


import java.util.Date;

import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class PaymentEdc
    extends com.ssti.enterprise.pos.om.BasePaymentEdc
    implements Persistent, MasterOM
{

	@Override
	public String getId() {
		return getPaymentEdcId();
	}

	@Override
	public String getCode() {
		getEDC();
		if(paymentTerm != null) return paymentTerm.getCode();
		return "";
	}

	PaymentTerm paymentTerm = null;	
	public PaymentTerm getEDC()
	{
		if(paymentTerm == null && StringUtil.isNotEmpty(getPaymentTermId()))
		{
			try
			{
				paymentTerm = PaymentTermTool.getPaymentTermByID(getPaymentTermId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return paymentTerm;
	}    
	
//	public String getCardTypeStr()
//	{
//		//return PaymentTypeTool.getCardTypeStr(getCardType());
//	}
	
	public boolean getIsActive()
	{
		if(getStartDate() != null && getEndDate() != null)
		{
			Date dNow = DateUtil.getTodayDate();
			if(getStartDate().after(dNow) && getEndDate().before(dNow))
			{
				return true;
			}
		}
		return false;
	}
}

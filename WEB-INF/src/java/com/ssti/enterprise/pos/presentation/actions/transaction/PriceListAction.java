package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.PriceListLoader;
import com.ssti.enterprise.pos.manager.PriceListDetailManager;
import com.ssti.enterprise.pos.om.PriceList;
import com.ssti.enterprise.pos.om.PriceListDetailPeer;
import com.ssti.enterprise.pos.om.PriceListPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class PriceListAction extends TransactionSecureAction
{
   	private static final String s_VIEW_PERM    = "View Customer Price List";
	private static final String s_UPDATE_PERM  = "Update Customer Price List";
	
    private static final String[] a_UPDATE_PERM  = {s_VIEW_PERM, s_UPDATE_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doInsert",null) != null ||
           data.getParameters().getString("eventSubmit_doUpdate",null) != null ||
           data.getParameters().getString("eventSubmit_doDelete",null) != null )
        {
        	return isAuthorized (data, a_UPDATE_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    	int iOp = data.getParameters().getInt("op");
    	if (iOp == 1)
    	{
    		doInsert(data,context);
    	}
    	else if (iOp == 2)
    	{
    		doAdjprice(data,context);
    	}
    	else if (iOp == 3)
    	{
    		doUpdate(data,context);
    	}
    	else if (iOp == 4)
    	{
    		doDelete(data,context);
    	}
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{

			PriceList oPL = new PriceList();
			setProperties (oPL, data);
			//Generate Sys ID                                   			
			oPL.setPriceListId ( IDGenerator.generateSysID() );			
			oPL.setUpdateDate (new Date());
		    oPL.save();
		    
		    PriceListDetailManager.getInstance().refreshCache(oPL);
		    if(data.getParameters().getBoolean("ImportAllItem"))
			{
				PriceListTool.importAllItem(oPL.getPriceListId());
			}
		    data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{			
			PriceList oPL = PriceListTool.getPriceListByID(data.getParameters().getString ("ID"));
		    PriceList oOld = oPL.copy();		    
			setProperties (oPL, data);
		    oPL.setUpdateDate (new Date());
		    oPL.save();
		    PriceListDetailManager.getInstance().refreshCache(oPL);
		    UpdateHistoryTool.createHistory(oOld,oPL, data.getUser().getName(), null);		    
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

    public void doDelete(RunData data, Context context)
        throws Exception
    {
    	String sID = data.getParameters().getString ("ID");
    	try
    	{    		
    		PriceList oPL = PriceListTool.getPriceListByID(sID);
	    	Criteria oCrit = new Criteria();
	        oCrit.add(PriceListDetailPeer.PRICE_LIST_ID, sID);
	        PriceListDetailPeer.doDelete(oCrit);
	        oCrit.add(PriceListPeer.PRICE_LIST_ID, sID);
	        PriceListPeer.doDelete(oCrit);
	        PriceListDetailManager.getInstance().refreshCache();
	        UpdateHistoryTool.createDelHistory(oPL, data.getUser().getName(), null);		        
        	data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

    public void doAdjprice(RunData data, Context context)
	    throws Exception
	{
		try
		{
			PriceList oPL = PriceListTool.getPriceListByID(data.getParameters().getString ("ID"));
		    setProperties (oPL, data);
		    oPL.setUpdateDate (new Date());
		    oPL.save();
		    
		    PriceListTool.adjustPrice(oPL, data.getParameters().getInt("AdjType"));
		    
	    	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
	    }
	    catch (Exception _oEx)
	    {
	    	_oEx.printStackTrace();
	    	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	    }
	}
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			PriceListLoader oLoader = new PriceListLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    	PriceListDetailManager.getInstance().refreshCache();
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}	

    private void setProperties (PriceList  _oPL,  RunData data)
    {
		try
		{			
			data.getParameters().setProperties(_oPL);
			if (_oPL.getPriceListType() == 1) 
			{
				_oPL.setCustomerId (""); 
			}
			else 
			{
				_oPL.setCustomerTypeId (""); 			
			}
			_oPL.setCreateDate(DateUtil.getStartOfDayDate(CustomParser.parseDate (data.getParameters().getString ("CreateDate"))));
			_oPL.setExpiredDate(DateUtil.getEndOfDayDate(CustomParser.parseDate (data.getParameters().getString ("ExpiredDate"))));
			
			StringBuilder oLoc = new StringBuilder();
			String[] aLoc = data.getParameters().getStrings("locationId");
			for (int i = 0; i < aLoc.length; i++)
			{
				String sLoc = aLoc[i];
				if (sLoc.equals(""))
				{
					oLoc.append("");
					break;
				}
				else 
				{
					oLoc.append(sLoc);
					if (i != (aLoc.length - 1))
					{
						oLoc.append(",");
					}
				}
			}
			_oPL.setLocationId(oLoc.toString());
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) + _oEx.getMessage());
    	}
    }
}

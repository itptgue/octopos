package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.FifoIn;
import com.ssti.enterprise.pos.om.FifoInPeer;
import com.ssti.enterprise.pos.om.FifoOut;
import com.ssti.enterprise.pos.om.FifoOutPeer;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.SqlUtil;

public class FIFOTool extends BaseTool 
{
	static FIFOTool instance = null;
	
	public static synchronized FIFOTool getInstance() 
	{
		if (instance == null) instance = new FIFOTool();
		return instance;
	}		
	
	/**
	 * Create FIFO IN from InventoryTransaction Record
	 * 
	 * @param _oInv
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveFIFOIn(InventoryTransaction _oInv, Connection _oConn) throws Exception
	{
		FifoIn oIn = new FifoIn();
		BeanUtil.setBean2BeanProperties(_oInv, oIn);
		oIn.setNew(true);
		oIn.setModified(true);
		oIn.setFifoInId(IDGenerator.generateSysID());
		oIn.setItemCost(_oInv.getCost());
		oIn.setQty(_oInv.getQtyChanges());
		oIn.setOutQty(bd_ZERO);
		oIn.setLastUpdate(new Date());
		oIn.save(_oConn);
	}
	
	/**
	 * get FIFO IN Record with Exists Qty
	 * 
	 * @param _sItemID
	 * @param _dDate
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getExists(String _sItemID, Date _dDate, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(FifoInPeer.TRANSACTION_DATE, _dDate, Criteria.LESS_EQUAL);
		oCrit.add(FifoInPeer.ITEM_ID, _sItemID);
		oCrit.add(FifoInPeer.OUT_QTY,(Object) 
			SqlUtil.buildCompareQuery (FifoInPeer.OUT_QTY, FifoInPeer.QTY, "<") , Criteria.CUSTOM);
		oCrit.addAscendingOrderByColumn(FifoInPeer.TRANSACTION_DATE);
		log.debug(oCrit);
		return FifoInPeer.doSelect(oCrit);
	}	
	
	/**
	 * Create FIFO Out Record from Inventory Transaction
	 * Update FIFO In Out Qty
	 * 
	 * @param _oInv
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List createOut(InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		List vOut = new ArrayList();
		double dQty = _oInv.getQtyChanges().doubleValue() * -1; //trans qty
		double dLeft = dQty; //processed trans qty - fifo in
		double dTotalCost = 0;
		
		log.debug("Create FIFO OUT, QTY: " +dQty);
				
		List vIn = getExists(_oInv.getItemId(), _oInv.getTransactionDate(), null); 
		for (int i = 0; i < vIn.size(); i++)
		{
			log.debug(i + " LEFT: " + dLeft);
			if (dLeft > 0)
			{
				FifoIn oIn = (FifoIn) vIn.get(i);
				double dIn = oIn.getQty().doubleValue(); 
				double dInOut = oIn.getOutQty().doubleValue();
				double dInLeft = dIn - dInOut;
				double dOut = dLeft; //set OUT to All
				
				log.debug(i + " - IN: " + dIn);
				log.debug(i + " - IN OUT: " + dInOut);
				log.debug(i + " - IN LEFT: " + dInLeft);				
				
				if (dInLeft >= dLeft) //FIFO IN QTY Enough
				{
					log.debug(i + " -- IN LEFT >= LEFT ");				
					log.debug(i + " -- OUT " + dOut);
					
					dLeft = 0;					
				}
				else if(dLeft > dInLeft) //FIFO IN QTY NOT Enough must get from next FIFO IN
				{
					dOut = dInLeft;
					dLeft = dLeft - dInLeft;

					log.debug(i + " -x LEFT > IN LEFT ");				
					log.debug(i + " -x OUT " + dOut);
					log.debug(i + " -x LEFT " + dLeft);					
				}
				dInOut = dInOut + dOut;				
				
				log.debug(i + " - IN OUT: " + dInOut);
				
				oIn.setOutQty(new BigDecimal(dInOut));				
				oIn.save(_oConn);

				dTotalCost += dOut * oIn.getItemCost().doubleValue();
				log.debug(i + " - OUT: " + dOut);
				log.debug(i + " - COST: " + oIn.getItemCost());

				FifoOut oOut = new FifoOut();
				oOut.setFifoOutId(IDGenerator.generateSysID());
				oOut.setLocationId(_oInv.getLocationId());
				oOut.setLocationName(_oInv.getLocationName());
				oOut.setItemId(_oInv.getItemId());
				oOut.setQty(new BigDecimal(dOut));
				oOut.setItemCost(oIn.getItemCost());
				oOut.setLastUpdate(new Date());
				oOut.setTransactionId(_oInv.getTransactionId());
				oOut.setTransactionDate(_oInv.getTransactionDate());
				oOut.setInventoryTransactionId(_oInv.getInventoryTransactionId());
				oOut.setFifoInId(oIn.getFifoInId());
				oOut.save(_oConn);
				
				vOut.add(oOut);
			}	//end if		
		}//end for	
		
		double dInvCost = dTotalCost / dQty;
		_oInv.setCost(new BigDecimal(dInvCost));
		
		double dPrevTotal = _oInv.getTotalCost().doubleValue();
		double dValBal = _oInv.getValueBalance().doubleValue();
		dTotalCost = dTotalCost * -1;
		
		log.debug("* PREV TOTAL COST: " + dPrevTotal);
		log.debug("* PREV VALUE BAL: " + dValBal);		
		
		if (dPrevTotal != dTotalCost)
		{			
			double dDif = dTotalCost - dPrevTotal;
			dValBal += dDif;
			log.debug("* DIFFERENCE : " + dDif);
			log.debug("* TOTAL COST : " + dTotalCost);
			log.debug("* VALUE BAL  : " + dValBal);		
		}
		_oInv.setTotalCost(new BigDecimal(dTotalCost));
		_oInv.setValueBalance(new BigDecimal(dValBal));
		_oInv.save(_oConn);
		
		return vOut;
	}
	
	/**
	 * get FIFO IN Record By FIFO IN ID
	 * 
	 * @param _sINID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	static FifoIn getInByID(String _sINID, Connection _oConn) throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(FifoInPeer.FIFO_IN_ID, _sINID);
		List vIn = FifoInPeer.doSelect(oCrit,_oConn);
		if (vIn.size() > 0) return (FifoIn) vIn.get(0);
		return null;
	}

	/**
	 * get FIFO IN RECORD By TransID
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	static List getInByTransID(String _sTransID, Connection _oConn)throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(FifoInPeer.TRANSACTION_ID, _sTransID);
		return FifoInPeer.doSelect(oCrit,_oConn);
	}

	/**
	 * get FIFO OUT RECORD By TransID
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	static List getOutByTransID(String _sTransID, Connection _oConn) throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(FifoOutPeer.TRANSACTION_ID, _sTransID);
		return FifoOutPeer.doSelect(oCrit,_oConn);
	}
	
	/**
	 * Delete FIFO Out Record
	 * 
	 * @param _sOutID
	 * @param _oConn
	 * @throws Exception
	 */
	static void delOut(String _sOutID, Connection _oConn) throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(FifoOutPeer.FIFO_OUT_ID, _sOutID);
		FifoOutPeer.doDelete(oCrit,_oConn);
	}

	/**
	 * FIFO OUT Transaction Cancel
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelOut(String _sTransID, Connection _oConn)
		throws Exception
	{
		List vOut = getOutByTransID(_sTransID,_oConn);	
		for (int i = 0; i < vOut.size(); i++)
		{
			FifoOut oOut = (FifoOut) vOut.get(i);
			FifoIn oIn = getInByID(oOut.getFifoInId(), _oConn);
			if (oIn != null)
			{
				double dInOut = oIn.getOutQty().doubleValue() - oOut.getQty().doubleValue(); 
				oIn.setOutQty(new BigDecimal(dInOut));
				oIn.save(_oConn);
			}
			delOut(oOut.getFifoOutId(), _oConn);
		}
	}

	/**
	 * Delete FIFO IN RECORD
	 * 
	 * @param _sInID
	 * @param _oConn
	 * @throws Exception
	 */
	static void delIn(String _sInID, Connection _oConn) throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(FifoInPeer.FIFO_IN_ID, _sInID);
		FifoInPeer.doDelete(oCrit,_oConn);
	}
	
	/**
	 * Cancel FIFO IN Transaction
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelIn(String _sTransID, Connection _oConn) throws Exception
	{
		List vIn = getInByTransID(_sTransID,_oConn);	
		System.out.println("vFIFOIN: " + vIn);

		for (int i = 0; i < vIn.size(); i++)
		{
			FifoIn oIn = (FifoIn) vIn.get(i);
			if (oIn.getOutQty().doubleValue() > 0)
			{
				log.error("ERROR, FIFO IN ALREADY USED: " + oIn);
				throw new Exception("Can Not Cancel Transaction, FIFO Qty & Cost already used! ");
			}
			else
			{
				delIn(oIn.getFifoInId(), _oConn);
			}
		}		
	}
}

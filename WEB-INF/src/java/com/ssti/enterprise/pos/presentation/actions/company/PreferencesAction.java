package com.ssti.enterprise.pos.presentation.actions.company;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Preference;
import com.ssti.enterprise.pos.om.PreferencePeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SiteTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-02
 * - change doSave call UpdateHistoryTool
 * </pre><br>
 */
public class PreferencesAction extends CompanySecureAction
{    
	private static Log log = LogFactory.getLog(PreferencesAction.class);

	public void doSave(RunData data, Context context)
        throws Exception
    {
		try 
		{
			Preference oOld = null;
			Preference oPref = null;
			String sPrefID = data.getParameters().getString("PreferenceID",null);
			if (StringUtil.isEmpty(sPrefID)) 
			{
				oPref = new Preference();
			}
			else 
			{
				Criteria oCrit = new Criteria();
				oCrit.add (PreferencePeer.PREFERENCE_ID, sPrefID); 
				List vData = PreferencePeer.doSelect (oCrit);
				oPref = (Preference) vData.get(0);
				oOld = oPref.copy();
			}
			data.getParameters().setProperties(oPref);
			
			if (StringUtil.isEmpty(sPrefID))
			{
				//Generate Sys ID								
				oPref.setPreferenceId (IDGenerator.generateSysID());
			}

			oPref.setCostingMethod (SiteTool.getCostingMethodByID (oPref.getSiteId()) );
			oPref.setInventoryType (LocationTool.getInventoryType (oPref.getLocationId(), null) );
	        oPref.save();
	        
	        if(oOld != null)
	        {
	        	UpdateHistoryTool.createHistory(oOld, oPref, data.getUser().getName(), null);
	        }
	        
	        PreferenceTool.refreshPref();
        	context.put("Preferences", oPref); 
        	data.setMessage (LocaleTool.getString("pref_saved"));
        }
        catch (Exception _oEx) 
        {
        	String 	sError = LocaleTool.getString("pref_save_failed") + _oEx.getMessage();
			log.error ( sError, _oEx);			        	
        	data.setMessage (sError);
        }
    }
	
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			String sResult = PreferenceTool.loadSkin(data, oBufferStream);
			data.setMessage("Skin Set Successfully");			
	    	context.put ("LoadResult", sResult);
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage ("Set Skin Failed, ERROR: " + _oEx.getMessage());
		}
	}	
}
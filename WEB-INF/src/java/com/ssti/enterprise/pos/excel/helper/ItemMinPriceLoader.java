package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemMinPrice;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemMinPriceTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemLoader.java,v 1.23 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemLoader.java,v $
 * Revision 1.23  2009/05/04 01:38:29  albert
 * *** empty log message ***
 *
 *
 */
public class ItemMinPriceLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( ItemMinPriceLoader.class );

	private static final String s_START_HEADING = "Item Code";	
		
	private String m_sUserName;
	private int m_iStartRow = 0;
	private int m_iTotalRows = 0;
	
	public ItemMinPriceLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
		Item oOldItemRef = null;		
		if (started (_oRow, _iIdx))				
		{
			short i = 0;
			String sItemCode = getString(_oRow, _iIdx + i); i++;			
			String sLocCode = getString(_oRow, _iIdx + i); i++;			
			String sCustType = getString(_oRow, _iIdx + i); i++;						
			BigDecimal dMinPrice = getBigDecimal(_oRow, _iIdx + i);
			if (StringUtil.isNotEmpty(sItemCode))
			{
				Item oItem = ItemTool.getItemByCode(sItemCode);
				String sItemID = oItem.getItemId();
				String sLocID = LocationTool.getIDByCode(sLocCode);
				String sCTypeID = CustomerTypeTool.getIDByCode(sCustType);
				if (oItem != null)
				{
					ItemMinPrice oIMP = ItemMinPriceTool.getItemMinPrice(sItemID, sLocID, sCTypeID);
					if (oIMP != null)
					{
						oIMP.setMinPrice(dMinPrice);
						oIMP.save();
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ".", 5));					
						m_oUpdated.append (StringUtil.left(sItemCode,15));
						m_oUpdated.append (StringUtil.left(sLocCode,12));
						m_oUpdated.append (StringUtil.left(sCustType,12));
						m_oUpdated.append ("\n");
					}
					else
					{
						oIMP = new ItemMinPrice();
						oIMP.setItemMinPriceId(IDGenerator.generateSysID());
						oIMP.setItemId(sItemID);
						oIMP.setLocationId(sLocID);
						oIMP.setCustomerTypeId(sCTypeID);
						oIMP.setMinPrice(dMinPrice);
						oIMP.save();
					    
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ".", 5));					
						m_oNewData.append (StringUtil.left(sItemCode,15));
						m_oNewData.append (StringUtil.left(sLocCode,12));
						m_oNewData.append (StringUtil.left(sCustType,12));
						m_oNewData.append ("\n");
					}
					oIMP.save();
				}
			}
			else
			{
				m_iRejected++;
				m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				m_oRejected.append (StringUtil.left(sItemCode,15));
				m_oRejected.append (StringUtil.left(sLocCode,12));
				m_oRejected.append (StringUtil.left(sCustType,12));
				m_oRejected.append (StringUtil.left("NOT FOUND",12));
				m_oRejected.append ("\n");
			}
		}    
    }
}

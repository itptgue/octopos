package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to OfflineTrans
 */
public abstract class BaseOfflineTrans extends BaseObject
{
    /** The Peer class */
    private static final OfflineTransPeer peer =
        new OfflineTransPeer();

        
    /** The value for the docId field */
    private String docId;
                                          
    /** The value for the docType field */
    private int docType = 1;
                                                
    /** The value for the ipAddress field */
    private String ipAddress = "";
                                                
    /** The value for the jsonDoc field */
    private String jsonDoc = "";
      
    /** The value for the receiveDate field */
    private Date receiveDate;
      
    /** The value for the processDate field */
    private Date processDate;
                                                
    /** The value for the processBy field */
    private String processBy = "";
                                                
    /** The value for the processLog field */
    private String processLog = "";
                                          
    /** The value for the status field */
    private int status = 1;
                                                
    /** The value for the transId field */
    private String transId = "";
  
    
    /**
     * Get the DocId
     *
     * @return String
     */
    public String getDocId()
    {
        return docId;
    }

                        
    /**
     * Set the value of DocId
     *
     * @param v new value
     */
    public void setDocId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.docId, v))
              {
            this.docId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DocType
     *
     * @return int
     */
    public int getDocType()
    {
        return docType;
    }

                        
    /**
     * Set the value of DocType
     *
     * @param v new value
     */
    public void setDocType(int v) 
    {
    
                  if (this.docType != v)
              {
            this.docType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IpAddress
     *
     * @return String
     */
    public String getIpAddress()
    {
        return ipAddress;
    }

                        
    /**
     * Set the value of IpAddress
     *
     * @param v new value
     */
    public void setIpAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.ipAddress, v))
              {
            this.ipAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JsonDoc
     *
     * @return String
     */
    public String getJsonDoc()
    {
        return jsonDoc;
    }

                        
    /**
     * Set the value of JsonDoc
     *
     * @param v new value
     */
    public void setJsonDoc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jsonDoc, v))
              {
            this.jsonDoc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReceiveDate
     *
     * @return Date
     */
    public Date getReceiveDate()
    {
        return receiveDate;
    }

                        
    /**
     * Set the value of ReceiveDate
     *
     * @param v new value
     */
    public void setReceiveDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.receiveDate, v))
              {
            this.receiveDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProcessDate
     *
     * @return Date
     */
    public Date getProcessDate()
    {
        return processDate;
    }

                        
    /**
     * Set the value of ProcessDate
     *
     * @param v new value
     */
    public void setProcessDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.processDate, v))
              {
            this.processDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProcessBy
     *
     * @return String
     */
    public String getProcessBy()
    {
        return processBy;
    }

                        
    /**
     * Set the value of ProcessBy
     *
     * @param v new value
     */
    public void setProcessBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.processBy, v))
              {
            this.processBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProcessLog
     *
     * @return String
     */
    public String getProcessLog()
    {
        return processLog;
    }

                        
    /**
     * Set the value of ProcessLog
     *
     * @param v new value
     */
    public void setProcessLog(String v) 
    {
    
                  if (!ObjectUtils.equals(this.processLog, v))
              {
            this.processLog = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransId
     *
     * @return String
     */
    public String getTransId()
    {
        return transId;
    }

                        
    /**
     * Set the value of TransId
     *
     * @param v new value
     */
    public void setTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transId, v))
              {
            this.transId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DocId");
              fieldNames.add("DocType");
              fieldNames.add("IpAddress");
              fieldNames.add("JsonDoc");
              fieldNames.add("ReceiveDate");
              fieldNames.add("ProcessDate");
              fieldNames.add("ProcessBy");
              fieldNames.add("ProcessLog");
              fieldNames.add("Status");
              fieldNames.add("TransId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DocId"))
        {
                return getDocId();
            }
          if (name.equals("DocType"))
        {
                return Integer.valueOf(getDocType());
            }
          if (name.equals("IpAddress"))
        {
                return getIpAddress();
            }
          if (name.equals("JsonDoc"))
        {
                return getJsonDoc();
            }
          if (name.equals("ReceiveDate"))
        {
                return getReceiveDate();
            }
          if (name.equals("ProcessDate"))
        {
                return getProcessDate();
            }
          if (name.equals("ProcessBy"))
        {
                return getProcessBy();
            }
          if (name.equals("ProcessLog"))
        {
                return getProcessLog();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("TransId"))
        {
                return getTransId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(OfflineTransPeer.DOC_ID))
        {
                return getDocId();
            }
          if (name.equals(OfflineTransPeer.DOC_TYPE))
        {
                return Integer.valueOf(getDocType());
            }
          if (name.equals(OfflineTransPeer.IP_ADDRESS))
        {
                return getIpAddress();
            }
          if (name.equals(OfflineTransPeer.JSON_DOC))
        {
                return getJsonDoc();
            }
          if (name.equals(OfflineTransPeer.RECEIVE_DATE))
        {
                return getReceiveDate();
            }
          if (name.equals(OfflineTransPeer.PROCESS_DATE))
        {
                return getProcessDate();
            }
          if (name.equals(OfflineTransPeer.PROCESS_BY))
        {
                return getProcessBy();
            }
          if (name.equals(OfflineTransPeer.PROCESS_LOG))
        {
                return getProcessLog();
            }
          if (name.equals(OfflineTransPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(OfflineTransPeer.TRANS_ID))
        {
                return getTransId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDocId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getDocType());
            }
              if (pos == 2)
        {
                return getIpAddress();
            }
              if (pos == 3)
        {
                return getJsonDoc();
            }
              if (pos == 4)
        {
                return getReceiveDate();
            }
              if (pos == 5)
        {
                return getProcessDate();
            }
              if (pos == 6)
        {
                return getProcessBy();
            }
              if (pos == 7)
        {
                return getProcessLog();
            }
              if (pos == 8)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 9)
        {
                return getTransId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(OfflineTransPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        OfflineTransPeer.doInsert((OfflineTrans) this, con);
                        setNew(false);
                    }
                    else
                    {
                        OfflineTransPeer.doUpdate((OfflineTrans) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key docId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDocId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDocId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDocId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public OfflineTrans copy() throws TorqueException
    {
        return copyInto(new OfflineTrans());
    }
  
    protected OfflineTrans copyInto(OfflineTrans copyObj) throws TorqueException
    {
          copyObj.setDocId(docId);
          copyObj.setDocType(docType);
          copyObj.setIpAddress(ipAddress);
          copyObj.setJsonDoc(jsonDoc);
          copyObj.setReceiveDate(receiveDate);
          copyObj.setProcessDate(processDate);
          copyObj.setProcessBy(processBy);
          copyObj.setProcessLog(processLog);
          copyObj.setStatus(status);
          copyObj.setTransId(transId);
  
                    copyObj.setDocId((String)null);
                                                                  
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public OfflineTransPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("OfflineTrans\n");
        str.append("------------\n")
           .append("DocId                : ")
           .append(getDocId())
           .append("\n")
           .append("DocType              : ")
           .append(getDocType())
           .append("\n")
           .append("IpAddress            : ")
           .append(getIpAddress())
           .append("\n")
           .append("JsonDoc              : ")
           .append(getJsonDoc())
           .append("\n")
           .append("ReceiveDate          : ")
           .append(getReceiveDate())
           .append("\n")
           .append("ProcessDate          : ")
           .append(getProcessDate())
           .append("\n")
           .append("ProcessBy            : ")
           .append(getProcessBy())
           .append("\n")
           .append("ProcessLog           : ")
           .append(getProcessLog())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("TransId              : ")
           .append(getTransId())
           .append("\n")
        ;
        return(str.toString());
    }
}

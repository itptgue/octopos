package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.ItemInventoryManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemInventoryLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemInventoryLoader.java,v 1.1 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemInventoryLoader.java,v $
 * Revision 1.1  2009/05/04 01:38:29  albert
 * *** empty log message ***
 *
 *
 */
public class ItemInventoryLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( ItemInventoryLoader.class );

	private static final String s_START_HEADING = "Item Code";	
		
	private String m_sUserName;
	private int m_iStartRow = 0;
	private int m_iTotalRows = 0;
	
	public ItemInventoryLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
		Item oOldItemRef = null;
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))				
		{
			short i = 0;
			String sItemCode = getString(_oRow, _iIdx + i); i++;
			String sItemName = getString(_oRow, _iIdx + i); i++;
			String sLocCode = getString(_oRow, _iIdx + i); i++;
			BigDecimal bdAvg = getBigDecimal(_oRow, _iIdx + i); i++;
			BigDecimal bdQty = getBigDecimal(_oRow, _iIdx + i); i++;

			boolean bNewItem = false;
			Location oLoc = LocationTool.getLocationByCode(sLocCode);
			Item oItem = ItemTool.getItemByCode(sItemCode);
			if (StringUtil.isNotEmpty(sItemCode) && oItem != null && oLoc != null)
			{
				boolean bValid = true;				
				ItemInventory oItemInv = 
					ItemInventoryTool.getItemInventory(oItem.getItemId(), oLoc.getLocationId());
				
				if (oItemInv == null)
				{
					bNewItem = true;
					oItemInv = new ItemInventory();
					oItemInv.setItemInventoryId(IDGenerator.generateSysID());
					oItemInv.setItemId(oItem.getItemId());
					oItemInv.setItemCode(oItem.getItemCode());
					oItemInv.setLocationId(oLoc.getLocationId());
					oItemInv.setLocationName(oLoc.getLocationName());					
				}
				
				oItemInv.setUpdateDate(new Date());
				
				log.debug("Column " + (_iIdx + i) + " min stock : " + getBigDecimal (_oRow, _iIdx + i));            									            	
				oItemInv.setMinimumQty (getBigDecimal (_oRow, _iIdx + i)); i++;

				log.debug("Column " + (_iIdx + i) + " reorder : " + getBigDecimal (_oRow, _iIdx + i));            									
				oItemInv.setReorderPoint (getBigDecimal (_oRow, _iIdx + i)); i++;

				log.debug("Column " + (_iIdx + i) + " max stock : " + getBigDecimal (_oRow, _iIdx + i));            									
				oItemInv.setMaximumQty(getBigDecimal (_oRow, _iIdx + i)); i++;
				
				log.debug("Column " + (_iIdx + i) + " rack location : " + getString(_oRow, _iIdx + i));            																
				oItemInv.setRackLocation(getString(_oRow, _iIdx + i)); i++;

				boolean bActive = true;
				log.debug("Column " + (_iIdx + i) + " active : " + getString(_oRow, _iIdx + i));            																
				String sActive = getString(_oRow, _iIdx + i); i++;
				if (StringUtil.isEqual(sActive, Boolean.FALSE.toString())) bActive = false;
				oItemInv.setActive(bActive); i++;

				if(bValid)
				{
				    oItemInv.save();		
				    ItemInventoryManager.getInstance().refreshCache(oItemInv);
				    if(bNewItem)
				    {
					    m_iNewData++;
					    m_oNewData.append (StringUtil.left(m_iNewData + ".", 5));					
					    m_oNewData.append (StringUtil.left(sItemCode,15));
					    m_oNewData.append (StringUtil.left(sItemName,30));
					    m_oNewData.append ("\n");
				    }
				    else
				    {
				        m_iUpdated++;
					    m_oUpdated.append (StringUtil.left(m_iUpdated + ".", 5));					
					    m_oUpdated.append (StringUtil.left(sItemCode,15));
					    m_oUpdated.append (StringUtil.left(sItemName,30));
					    m_oUpdated.append ("\n");
				    }
				}		
			}
			else
			{
			    m_iRejected++;
			    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
			    m_oRejected.append (StringUtil.left(sItemCode,15));
			    m_oRejected.append (StringUtil.left(sItemName,20));
			    m_oRejected.append (StringUtil.left(sLocCode,20));			    
			    if (oEmpty.length() > s_EMPTY_REJECT.length())
			    {				    
			    	m_oRejected.append (oEmpty);
			    }
			    if (oLength.length() > s_LENGTH_REJECT.length())
			    {
			    	m_oRejected.append ("; ");				    
			    	m_oRejected.append (oLength);
			    }
			    m_oRejected.append ("\n");
			}	
		}    
    }
}

package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.PayablePaymentLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PayablePaymentAction.java,v 1.26 2009/05/04 01:58:24 albert Exp $ <br>
 *
 * <pre>
 * $Log: PayablePaymentAction.java,v $
 * Revision 1.26  2009/05/04 01:58:24  albert
 * *** empty log message ***
 *
 * Revision 1.25  2007/11/09 01:12:15  albert
 * *** empty log message ***
 *
 * Revision 1.24  2007/07/02 15:38:12  albert
 * *** empty log message ***
 *
 * Revision 1.23  2007/06/07 16:47:26  albert
 * *** empty log message ***
 *
 * Revision 1.22  2007/05/10 23:22:42  albert
 * *** empty log message ***
 *
 * Revision 1.21  2007/03/12 08:54:49  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PayablePaymentAction extends TransactionSecureAction
{
   	private static final String s_VIEW_PERM    = "View Payable Payment";
	private static final String s_CREATE_PERM  = "Create Payable Payment";
	private static final String s_CANCEL_PERM  = "Cancel Payable Payment";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};

    public boolean isAuthorized ( RunData data )
		throws Exception
	{
		if (data.getParameters().getInt("save") == 1) {
			return isAuthorized (data, a_CREATE_PERM);
		}
		if (data.getParameters().getInt("save") == 2) {
			return isAuthorized (data, a_CANCEL_PERM);
		}
		else 
		{
			return isAuthorized (data, a_VIEW_PERM);
		}    
	}
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	if (data.getParameters().getInt("save") == 1) {
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("save") == 2) 
    	{
    		doCancel (data,context);
    	}    	
    	else 
    	{
    		doFind (data,context);
    	}
    }

    public void doFind ( RunData data, Context context )
        throws Exception
    {		
		super.doFind(data);
		String sVendorID = data.getParameters().getString ("VendorId");
		Date dStartDue = CustomParser.parseDate(data.getParameters().getString("StartDue"));
		Date dEndDue = CustomParser.parseDate(data.getParameters().getString("EndDue"));
		int iStatus = data.getParameters().getInt("Status");
		int iCFStatus = data.getParameters().getInt("CfStatus");		
		
        LargeSelect vTR = PayablePaymentTool.findData(
        	iCond, sKeywords, sVendorID, sLocationID, dStart, dEnd, 
        	dStartDue, dEndDue, iLimit, iStatus, iCFStatus, sCurrencyID);
        
        data.getUser().setTemp("findPayablePaymentResult", vTR);
    }

	public void doUpdate ( RunData data, Context context )
        throws Exception
    {
		doSave (data, context);
    }

	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{
			HttpSession oSes = data.getSession();            
            ApPayment oAP = (ApPayment) oSes.getAttribute (s_AP);	
			List vAPD = (List) oSes.getAttribute (s_APD);
			List vDebitMemo = (List) oSes.getAttribute (s_APDP);
			
			data.getParameters().setProperties (oAP);
			oAP.setApPaymentDate (CustomParser.parseDate(data.getParameters().getString("ApPaymentDate")));
			oAP.setApPaymentDueDate (CustomParser.parseDate(data.getParameters().getString("ApPaymentDueDate")));
			oAP.setTotalDownPayment(new BigDecimal(data.getParameters().getDouble("TotalDownPayment")));
			oAP.setVendorName(VendorTool.getVendorNameByID(data.getParameters().getString("VendorId")));
		
			PayablePaymentTool.saveData (oAP, vAPD, vDebitMemo, null);
			data.setMessage(LocaleTool.getString("app_save_success"));
        }
        catch (Exception _oEx) 
		{
        	handleError (data, LocaleTool.getString("app_save_failed"), _oEx);
        }
    }

	public void doCancel ( RunData data, Context context )
	    throws Exception
	{
		String sID = data.getParameters().getString("ApPaymentId");
		ApPayment oAP = (ApPayment) data.getSession().getAttribute(s_AP);
		List vAPD = (List) data.getSession().getAttribute(s_APD);
		try
		{
			if (oAP == null || vAPD == null)
			{
				oAP = PayablePaymentTool.getHeaderByID(sID);
				vAPD = PayablePaymentTool.getDetailsByID(sID);
			}
			if (oAP != null && oAP.getStatus() != i_PAYMENT_CANCELLED && vAPD != null)
			{			
				PayablePaymentTool.cancelTransaction(oAP, vAPD, data.getUser().getName());			
				data.getSession().setAttribute(s_AP, oAP);
				data.getSession().setAttribute(s_APD, vAPD);
				
		    	data.setMessage(LocaleTool.getString("app_cancel_success"));        	
			}
			else
			{
				throw new Exception ("AP Payment / details is not found ");
			}
	    }
	    catch (Exception _oEx) 
		{
        	handleError (data, LocaleTool.getString("app_cancel_failed"), _oEx);
	    }
	}
	
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
	     	TransactionLoader oLoader = null;
	     	String sLoader = data.getParameters().getString("loader");
            if (StringUtil.isNotEmpty(sLoader))
            {
                oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
                oLoader.setUserName(data.getUser().getName());
            }
            else
            {
            	oLoader = new PayablePaymentLoader(data.getUser().getName());
            }
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}  
	}
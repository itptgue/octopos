package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to GlConfig
 */
public abstract class BaseGlConfig extends BaseObject
{
    /** The Peer class */
    private static final GlConfigPeer peer =
        new GlConfigPeer();

        
    /** The value for the glConfigId field */
    private String glConfigId;
      
    /** The value for the directSalesAccount field */
    private String directSalesAccount;
      
    /** The value for the defaultFreightAccount field */
    private String defaultFreightAccount;
      
    /** The value for the inventoryAccount field */
    private String inventoryAccount;
      
    /** The value for the salesAccount field */
    private String salesAccount;
      
    /** The value for the salesReturnAccount field */
    private String salesReturnAccount;
      
    /** The value for the itemDiscountAccount field */
    private String itemDiscountAccount;
      
    /** The value for the cogsAccount field */
    private String cogsAccount;
      
    /** The value for the purchaseReturnAccount field */
    private String purchaseReturnAccount;
      
    /** The value for the expenseAccount field */
    private String expenseAccount;
      
    /** The value for the unbilledGoodsAccount field */
    private String unbilledGoodsAccount;
      
    /** The value for the openingBalance field */
    private String openingBalance;
      
    /** The value for the retainedEarning field */
    private String retainedEarning;
      
    /** The value for the creditMemoAccount field */
    private String creditMemoAccount;
      
    /** The value for the debitMemoAccount field */
    private String debitMemoAccount;
      
    /** The value for the deliveryOrderAccount field */
    private String deliveryOrderAccount;
      
    /** The value for the posRoundingAccount field */
    private String posRoundingAccount;
      
    /** The value for the arPaymentTemp field */
    private String arPaymentTemp;
      
    /** The value for the apPaymentTemp field */
    private String apPaymentTemp;
      
    /** The value for the prPiVariance field */
    private String prPiVariance;
  
    
    /**
     * Get the GlConfigId
     *
     * @return String
     */
    public String getGlConfigId()
    {
        return glConfigId;
    }

                        
    /**
     * Set the value of GlConfigId
     *
     * @param v new value
     */
    public void setGlConfigId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.glConfigId, v))
              {
            this.glConfigId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DirectSalesAccount
     *
     * @return String
     */
    public String getDirectSalesAccount()
    {
        return directSalesAccount;
    }

                        
    /**
     * Set the value of DirectSalesAccount
     *
     * @param v new value
     */
    public void setDirectSalesAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.directSalesAccount, v))
              {
            this.directSalesAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultFreightAccount
     *
     * @return String
     */
    public String getDefaultFreightAccount()
    {
        return defaultFreightAccount;
    }

                        
    /**
     * Set the value of DefaultFreightAccount
     *
     * @param v new value
     */
    public void setDefaultFreightAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultFreightAccount, v))
              {
            this.defaultFreightAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InventoryAccount
     *
     * @return String
     */
    public String getInventoryAccount()
    {
        return inventoryAccount;
    }

                        
    /**
     * Set the value of InventoryAccount
     *
     * @param v new value
     */
    public void setInventoryAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inventoryAccount, v))
              {
            this.inventoryAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAccount
     *
     * @return String
     */
    public String getSalesAccount()
    {
        return salesAccount;
    }

                        
    /**
     * Set the value of SalesAccount
     *
     * @param v new value
     */
    public void setSalesAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAccount, v))
              {
            this.salesAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesReturnAccount
     *
     * @return String
     */
    public String getSalesReturnAccount()
    {
        return salesReturnAccount;
    }

                        
    /**
     * Set the value of SalesReturnAccount
     *
     * @param v new value
     */
    public void setSalesReturnAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesReturnAccount, v))
              {
            this.salesReturnAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemDiscountAccount
     *
     * @return String
     */
    public String getItemDiscountAccount()
    {
        return itemDiscountAccount;
    }

                        
    /**
     * Set the value of ItemDiscountAccount
     *
     * @param v new value
     */
    public void setItemDiscountAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemDiscountAccount, v))
              {
            this.itemDiscountAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CogsAccount
     *
     * @return String
     */
    public String getCogsAccount()
    {
        return cogsAccount;
    }

                        
    /**
     * Set the value of CogsAccount
     *
     * @param v new value
     */
    public void setCogsAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cogsAccount, v))
              {
            this.cogsAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseReturnAccount
     *
     * @return String
     */
    public String getPurchaseReturnAccount()
    {
        return purchaseReturnAccount;
    }

                        
    /**
     * Set the value of PurchaseReturnAccount
     *
     * @param v new value
     */
    public void setPurchaseReturnAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseReturnAccount, v))
              {
            this.purchaseReturnAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpenseAccount
     *
     * @return String
     */
    public String getExpenseAccount()
    {
        return expenseAccount;
    }

                        
    /**
     * Set the value of ExpenseAccount
     *
     * @param v new value
     */
    public void setExpenseAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.expenseAccount, v))
              {
            this.expenseAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnbilledGoodsAccount
     *
     * @return String
     */
    public String getUnbilledGoodsAccount()
    {
        return unbilledGoodsAccount;
    }

                        
    /**
     * Set the value of UnbilledGoodsAccount
     *
     * @param v new value
     */
    public void setUnbilledGoodsAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unbilledGoodsAccount, v))
              {
            this.unbilledGoodsAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return String
     */
    public String getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(String v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RetainedEarning
     *
     * @return String
     */
    public String getRetainedEarning()
    {
        return retainedEarning;
    }

                        
    /**
     * Set the value of RetainedEarning
     *
     * @param v new value
     */
    public void setRetainedEarning(String v) 
    {
    
                  if (!ObjectUtils.equals(this.retainedEarning, v))
              {
            this.retainedEarning = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreditMemoAccount
     *
     * @return String
     */
    public String getCreditMemoAccount()
    {
        return creditMemoAccount;
    }

                        
    /**
     * Set the value of CreditMemoAccount
     *
     * @param v new value
     */
    public void setCreditMemoAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.creditMemoAccount, v))
              {
            this.creditMemoAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DebitMemoAccount
     *
     * @return String
     */
    public String getDebitMemoAccount()
    {
        return debitMemoAccount;
    }

                        
    /**
     * Set the value of DebitMemoAccount
     *
     * @param v new value
     */
    public void setDebitMemoAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.debitMemoAccount, v))
              {
            this.debitMemoAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryOrderAccount
     *
     * @return String
     */
    public String getDeliveryOrderAccount()
    {
        return deliveryOrderAccount;
    }

                        
    /**
     * Set the value of DeliveryOrderAccount
     *
     * @param v new value
     */
    public void setDeliveryOrderAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryOrderAccount, v))
              {
            this.deliveryOrderAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PosRoundingAccount
     *
     * @return String
     */
    public String getPosRoundingAccount()
    {
        return posRoundingAccount;
    }

                        
    /**
     * Set the value of PosRoundingAccount
     *
     * @param v new value
     */
    public void setPosRoundingAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.posRoundingAccount, v))
              {
            this.posRoundingAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ArPaymentTemp
     *
     * @return String
     */
    public String getArPaymentTemp()
    {
        return arPaymentTemp;
    }

                        
    /**
     * Set the value of ArPaymentTemp
     *
     * @param v new value
     */
    public void setArPaymentTemp(String v) 
    {
    
                  if (!ObjectUtils.equals(this.arPaymentTemp, v))
              {
            this.arPaymentTemp = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApPaymentTemp
     *
     * @return String
     */
    public String getApPaymentTemp()
    {
        return apPaymentTemp;
    }

                        
    /**
     * Set the value of ApPaymentTemp
     *
     * @param v new value
     */
    public void setApPaymentTemp(String v) 
    {
    
                  if (!ObjectUtils.equals(this.apPaymentTemp, v))
              {
            this.apPaymentTemp = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrPiVariance
     *
     * @return String
     */
    public String getPrPiVariance()
    {
        return prPiVariance;
    }

                        
    /**
     * Set the value of PrPiVariance
     *
     * @param v new value
     */
    public void setPrPiVariance(String v) 
    {
    
                  if (!ObjectUtils.equals(this.prPiVariance, v))
              {
            this.prPiVariance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("GlConfigId");
              fieldNames.add("DirectSalesAccount");
              fieldNames.add("DefaultFreightAccount");
              fieldNames.add("InventoryAccount");
              fieldNames.add("SalesAccount");
              fieldNames.add("SalesReturnAccount");
              fieldNames.add("ItemDiscountAccount");
              fieldNames.add("CogsAccount");
              fieldNames.add("PurchaseReturnAccount");
              fieldNames.add("ExpenseAccount");
              fieldNames.add("UnbilledGoodsAccount");
              fieldNames.add("OpeningBalance");
              fieldNames.add("RetainedEarning");
              fieldNames.add("CreditMemoAccount");
              fieldNames.add("DebitMemoAccount");
              fieldNames.add("DeliveryOrderAccount");
              fieldNames.add("PosRoundingAccount");
              fieldNames.add("ArPaymentTemp");
              fieldNames.add("ApPaymentTemp");
              fieldNames.add("PrPiVariance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("GlConfigId"))
        {
                return getGlConfigId();
            }
          if (name.equals("DirectSalesAccount"))
        {
                return getDirectSalesAccount();
            }
          if (name.equals("DefaultFreightAccount"))
        {
                return getDefaultFreightAccount();
            }
          if (name.equals("InventoryAccount"))
        {
                return getInventoryAccount();
            }
          if (name.equals("SalesAccount"))
        {
                return getSalesAccount();
            }
          if (name.equals("SalesReturnAccount"))
        {
                return getSalesReturnAccount();
            }
          if (name.equals("ItemDiscountAccount"))
        {
                return getItemDiscountAccount();
            }
          if (name.equals("CogsAccount"))
        {
                return getCogsAccount();
            }
          if (name.equals("PurchaseReturnAccount"))
        {
                return getPurchaseReturnAccount();
            }
          if (name.equals("ExpenseAccount"))
        {
                return getExpenseAccount();
            }
          if (name.equals("UnbilledGoodsAccount"))
        {
                return getUnbilledGoodsAccount();
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("RetainedEarning"))
        {
                return getRetainedEarning();
            }
          if (name.equals("CreditMemoAccount"))
        {
                return getCreditMemoAccount();
            }
          if (name.equals("DebitMemoAccount"))
        {
                return getDebitMemoAccount();
            }
          if (name.equals("DeliveryOrderAccount"))
        {
                return getDeliveryOrderAccount();
            }
          if (name.equals("PosRoundingAccount"))
        {
                return getPosRoundingAccount();
            }
          if (name.equals("ArPaymentTemp"))
        {
                return getArPaymentTemp();
            }
          if (name.equals("ApPaymentTemp"))
        {
                return getApPaymentTemp();
            }
          if (name.equals("PrPiVariance"))
        {
                return getPrPiVariance();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(GlConfigPeer.GL_CONFIG_ID))
        {
                return getGlConfigId();
            }
          if (name.equals(GlConfigPeer.DIRECT_SALES_ACCOUNT))
        {
                return getDirectSalesAccount();
            }
          if (name.equals(GlConfigPeer.DEFAULT_FREIGHT_ACCOUNT))
        {
                return getDefaultFreightAccount();
            }
          if (name.equals(GlConfigPeer.INVENTORY_ACCOUNT))
        {
                return getInventoryAccount();
            }
          if (name.equals(GlConfigPeer.SALES_ACCOUNT))
        {
                return getSalesAccount();
            }
          if (name.equals(GlConfigPeer.SALES_RETURN_ACCOUNT))
        {
                return getSalesReturnAccount();
            }
          if (name.equals(GlConfigPeer.ITEM_DISCOUNT_ACCOUNT))
        {
                return getItemDiscountAccount();
            }
          if (name.equals(GlConfigPeer.COGS_ACCOUNT))
        {
                return getCogsAccount();
            }
          if (name.equals(GlConfigPeer.PURCHASE_RETURN_ACCOUNT))
        {
                return getPurchaseReturnAccount();
            }
          if (name.equals(GlConfigPeer.EXPENSE_ACCOUNT))
        {
                return getExpenseAccount();
            }
          if (name.equals(GlConfigPeer.UNBILLED_GOODS_ACCOUNT))
        {
                return getUnbilledGoodsAccount();
            }
          if (name.equals(GlConfigPeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(GlConfigPeer.RETAINED_EARNING))
        {
                return getRetainedEarning();
            }
          if (name.equals(GlConfigPeer.CREDIT_MEMO_ACCOUNT))
        {
                return getCreditMemoAccount();
            }
          if (name.equals(GlConfigPeer.DEBIT_MEMO_ACCOUNT))
        {
                return getDebitMemoAccount();
            }
          if (name.equals(GlConfigPeer.DELIVERY_ORDER_ACCOUNT))
        {
                return getDeliveryOrderAccount();
            }
          if (name.equals(GlConfigPeer.POS_ROUNDING_ACCOUNT))
        {
                return getPosRoundingAccount();
            }
          if (name.equals(GlConfigPeer.AR_PAYMENT_TEMP))
        {
                return getArPaymentTemp();
            }
          if (name.equals(GlConfigPeer.AP_PAYMENT_TEMP))
        {
                return getApPaymentTemp();
            }
          if (name.equals(GlConfigPeer.PR_PI_VARIANCE))
        {
                return getPrPiVariance();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getGlConfigId();
            }
              if (pos == 1)
        {
                return getDirectSalesAccount();
            }
              if (pos == 2)
        {
                return getDefaultFreightAccount();
            }
              if (pos == 3)
        {
                return getInventoryAccount();
            }
              if (pos == 4)
        {
                return getSalesAccount();
            }
              if (pos == 5)
        {
                return getSalesReturnAccount();
            }
              if (pos == 6)
        {
                return getItemDiscountAccount();
            }
              if (pos == 7)
        {
                return getCogsAccount();
            }
              if (pos == 8)
        {
                return getPurchaseReturnAccount();
            }
              if (pos == 9)
        {
                return getExpenseAccount();
            }
              if (pos == 10)
        {
                return getUnbilledGoodsAccount();
            }
              if (pos == 11)
        {
                return getOpeningBalance();
            }
              if (pos == 12)
        {
                return getRetainedEarning();
            }
              if (pos == 13)
        {
                return getCreditMemoAccount();
            }
              if (pos == 14)
        {
                return getDebitMemoAccount();
            }
              if (pos == 15)
        {
                return getDeliveryOrderAccount();
            }
              if (pos == 16)
        {
                return getPosRoundingAccount();
            }
              if (pos == 17)
        {
                return getArPaymentTemp();
            }
              if (pos == 18)
        {
                return getApPaymentTemp();
            }
              if (pos == 19)
        {
                return getPrPiVariance();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(GlConfigPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        GlConfigPeer.doInsert((GlConfig) this, con);
                        setNew(false);
                    }
                    else
                    {
                        GlConfigPeer.doUpdate((GlConfig) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key glConfigId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setGlConfigId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setGlConfigId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getGlConfigId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public GlConfig copy() throws TorqueException
    {
        return copyInto(new GlConfig());
    }
  
    protected GlConfig copyInto(GlConfig copyObj) throws TorqueException
    {
          copyObj.setGlConfigId(glConfigId);
          copyObj.setDirectSalesAccount(directSalesAccount);
          copyObj.setDefaultFreightAccount(defaultFreightAccount);
          copyObj.setInventoryAccount(inventoryAccount);
          copyObj.setSalesAccount(salesAccount);
          copyObj.setSalesReturnAccount(salesReturnAccount);
          copyObj.setItemDiscountAccount(itemDiscountAccount);
          copyObj.setCogsAccount(cogsAccount);
          copyObj.setPurchaseReturnAccount(purchaseReturnAccount);
          copyObj.setExpenseAccount(expenseAccount);
          copyObj.setUnbilledGoodsAccount(unbilledGoodsAccount);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setRetainedEarning(retainedEarning);
          copyObj.setCreditMemoAccount(creditMemoAccount);
          copyObj.setDebitMemoAccount(debitMemoAccount);
          copyObj.setDeliveryOrderAccount(deliveryOrderAccount);
          copyObj.setPosRoundingAccount(posRoundingAccount);
          copyObj.setArPaymentTemp(arPaymentTemp);
          copyObj.setApPaymentTemp(apPaymentTemp);
          copyObj.setPrPiVariance(prPiVariance);
  
                    copyObj.setGlConfigId((String)null);
                                                                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public GlConfigPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("GlConfig\n");
        str.append("--------\n")
           .append("GlConfigId           : ")
           .append(getGlConfigId())
           .append("\n")
           .append("DirectSalesAccount   : ")
           .append(getDirectSalesAccount())
           .append("\n")
            .append("DefaultFreightAccount   : ")
           .append(getDefaultFreightAccount())
           .append("\n")
           .append("InventoryAccount     : ")
           .append(getInventoryAccount())
           .append("\n")
           .append("SalesAccount         : ")
           .append(getSalesAccount())
           .append("\n")
           .append("SalesReturnAccount   : ")
           .append(getSalesReturnAccount())
           .append("\n")
           .append("ItemDiscountAccount  : ")
           .append(getItemDiscountAccount())
           .append("\n")
           .append("CogsAccount          : ")
           .append(getCogsAccount())
           .append("\n")
            .append("PurchaseReturnAccount   : ")
           .append(getPurchaseReturnAccount())
           .append("\n")
           .append("ExpenseAccount       : ")
           .append(getExpenseAccount())
           .append("\n")
           .append("UnbilledGoodsAccount   : ")
           .append(getUnbilledGoodsAccount())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("RetainedEarning      : ")
           .append(getRetainedEarning())
           .append("\n")
           .append("CreditMemoAccount    : ")
           .append(getCreditMemoAccount())
           .append("\n")
           .append("DebitMemoAccount     : ")
           .append(getDebitMemoAccount())
           .append("\n")
           .append("DeliveryOrderAccount   : ")
           .append(getDeliveryOrderAccount())
           .append("\n")
           .append("PosRoundingAccount   : ")
           .append(getPosRoundingAccount())
           .append("\n")
           .append("ArPaymentTemp        : ")
           .append(getArPaymentTemp())
           .append("\n")
           .append("ApPaymentTemp        : ")
           .append(getApPaymentTemp())
           .append("\n")
           .append("PrPiVariance         : ")
           .append(getPrPiVariance())
           .append("\n")
        ;
        return(str.toString());
    }
}

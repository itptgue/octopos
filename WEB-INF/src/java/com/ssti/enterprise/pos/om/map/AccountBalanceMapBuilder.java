package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AccountBalanceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AccountBalanceMapBuilder";

    /**
     * Item
     * @deprecated use AccountBalancePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "account_balance";
    }

  
    /**
     * account_balance.ACCOUNT_BALANCE_ID
     * @return the column name for the ACCOUNT_BALANCE_ID field
     * @deprecated use AccountBalancePeer.account_balance.ACCOUNT_BALANCE_ID constant
     */
    public static String getAccountBalance_AccountBalanceId()
    {
        return "account_balance.ACCOUNT_BALANCE_ID";
    }
  
    /**
     * account_balance.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use AccountBalancePeer.account_balance.ACCOUNT_ID constant
     */
    public static String getAccountBalance_AccountId()
    {
        return "account_balance.ACCOUNT_ID";
    }
  
    /**
     * account_balance.BALANCE
     * @return the column name for the BALANCE field
     * @deprecated use AccountBalancePeer.account_balance.BALANCE constant
     */
    public static String getAccountBalance_Balance()
    {
        return "account_balance.BALANCE";
    }
  
    /**
     * account_balance.PRIME_BALANCE
     * @return the column name for the PRIME_BALANCE field
     * @deprecated use AccountBalancePeer.account_balance.PRIME_BALANCE constant
     */
    public static String getAccountBalance_PrimeBalance()
    {
        return "account_balance.PRIME_BALANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("account_balance");
        TableMap tMap = dbMap.getTable("account_balance");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("account_balance.ACCOUNT_BALANCE_ID", "");
                          tMap.addColumn("account_balance.ACCOUNT_ID", "");
                            tMap.addColumn("account_balance.BALANCE", bd_ZERO);
                            tMap.addColumn("account_balance.PRIME_BALANCE", bd_ZERO);
          }
}

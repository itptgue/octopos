package com.ssti.framework.tools;

import java.io.File;
import java.net.URL;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * upload related utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: UploadTool.java,v 1.3 2007/02/23 14:14:05 albert Exp $ <br>
 * 
 * <pre>
 * $Log: UploadTool.java,v $
 * Revision 1.3  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class UploadTool
{    		
	static final Log log = LogFactory.getLog(UploadTool.class);
	
    boolean m_bHeader = false;
	String m_sURL  = "";
	String m_sFile = "";
	String m_sName = "";
	String m_sTarget = "";
    
    File m_oFile;
    URL m_oURL;
    int m_iStatus = 0;

    StringBuilder m_sResult = new StringBuilder();
    
    String userName = "";
    String password = "";
    
    public UploadTool (String _sURL, String _sFileName, 
    				   String _sName,  String _sUserName, 
					   String _sPassword, String _sTargetPath) throws Exception
	{
    	m_sURL = _sURL;
    	m_oURL = new URL (m_sURL);
    	
    	m_sFile = _sFileName;
    	m_oFile = new File(_sFileName);
    	
    	m_sName = _sName;
    	
    	if (m_oFile == null) throw new Exception ("File " + _sFileName + " Not Found ");
    	if (m_oURL == null) throw new Exception ("URL " + _sURL + " is Invalid ");
    	
    	userName = _sUserName;
    	password = _sPassword;
    	
    	m_sTarget = _sTargetPath;
    	
    	log.info("URL : " + m_oURL);
    	log.info("File : " + m_oFile.getAbsolutePath());
	}    
    
    public int getStatus () 
    {
        return m_iStatus;
    }

    public String getResult() 
    {
        return m_sResult.toString();
    }
    
	public String uploadData ()
		throws Exception
	{
        PostMethod filePost = new PostMethod(m_sURL);
        try 
		{        	
        	m_sResult.append("Uploading " + m_oFile.getAbsolutePath() + " To " + m_sURL + " Target " + m_sTarget);
        	
        	//filePost.addParameter("targetpath", m_sTarget);
        	Part[] parts = {new FilePart(m_sName, m_oFile), new StringPart("targetpath", m_sTarget)};
        	filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
        	
        	HttpClient client = new HttpClient();
        	client.getHttpConnectionManager().getParams().setConnectionTimeout(60000);

        	Credentials defaultcreds = new UsernamePasswordCredentials(userName, password);
        	client.getState().setCredentials(new AuthScope(m_oURL.getHost(), 
        		m_oURL.getPort(), AuthScope.ANY_REALM), defaultcreds);        	

            m_iStatus = client.executeMethod(filePost);
            if (m_iStatus == HttpStatus.SC_OK) 
            {
            	System.out.println (filePost.getStatusLine());
                m_sResult.append("\n\nUpload Complete Response From Server: \n\n");
                m_sResult.append(filePost.getResponseBodyAsString());
            } 
            else 
            {
            	m_sResult.append("\n\nHTTP STATUS ERROR: Upload Failed, Status : \n\n"); 
            	m_sResult.append(HttpStatus.getStatusText(m_iStatus));
            	m_sResult.append("\n\nResponse From Server : \n\n"); 
                m_sResult.append(filePost.getResponseBodyAsString());            	
            }
        } 
        catch (Exception _oEx) 
		{
        	m_sResult.append("\n\nERROR : " + _oEx.getMessage());
        	log.error(_oEx);
            _oEx.printStackTrace();
            throw new Exception (_oEx);
        } 
        finally 
		{
            filePost.releaseConnection();
        }
        return m_sResult.toString();
	}
}

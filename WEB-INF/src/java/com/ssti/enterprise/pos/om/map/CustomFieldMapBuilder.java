package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomFieldMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomFieldMapBuilder";

    /**
     * Item
     * @deprecated use CustomFieldPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "custom_field";
    }

  
    /**
     * custom_field.CUSTOM_FIELD_ID
     * @return the column name for the CUSTOM_FIELD_ID field
     * @deprecated use CustomFieldPeer.custom_field.CUSTOM_FIELD_ID constant
     */
    public static String getCustomField_CustomFieldId()
    {
        return "custom_field.CUSTOM_FIELD_ID";
    }
  
    /**
     * custom_field.FIELD_NAME
     * @return the column name for the FIELD_NAME field
     * @deprecated use CustomFieldPeer.custom_field.FIELD_NAME constant
     */
    public static String getCustomField_FieldName()
    {
        return "custom_field.FIELD_NAME";
    }
  
    /**
     * custom_field.FIELD_DEFAULT_VALUE
     * @return the column name for the FIELD_DEFAULT_VALUE field
     * @deprecated use CustomFieldPeer.custom_field.FIELD_DEFAULT_VALUE constant
     */
    public static String getCustomField_FieldDefaultValue()
    {
        return "custom_field.FIELD_DEFAULT_VALUE";
    }
  
    /**
     * custom_field.FIELD_TYPE
     * @return the column name for the FIELD_TYPE field
     * @deprecated use CustomFieldPeer.custom_field.FIELD_TYPE constant
     */
    public static String getCustomField_FieldType()
    {
        return "custom_field.FIELD_TYPE";
    }
  
    /**
     * custom_field.FIELD_NO
     * @return the column name for the FIELD_NO field
     * @deprecated use CustomFieldPeer.custom_field.FIELD_NO constant
     */
    public static String getCustomField_FieldNo()
    {
        return "custom_field.FIELD_NO";
    }
  
    /**
     * custom_field.FIELD_GROUP
     * @return the column name for the FIELD_GROUP field
     * @deprecated use CustomFieldPeer.custom_field.FIELD_GROUP constant
     */
    public static String getCustomField_FieldGroup()
    {
        return "custom_field.FIELD_GROUP";
    }
  
    /**
     * custom_field.KATEGORI_ID
     * @return the column name for the KATEGORI_ID field
     * @deprecated use CustomFieldPeer.custom_field.KATEGORI_ID constant
     */
    public static String getCustomField_KategoriId()
    {
        return "custom_field.KATEGORI_ID";
    }
  
    /**
     * custom_field.REQUIRED
     * @return the column name for the REQUIRED field
     * @deprecated use CustomFieldPeer.custom_field.REQUIRED constant
     */
    public static String getCustomField_Required()
    {
        return "custom_field.REQUIRED";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("custom_field");
        TableMap tMap = dbMap.getTable("custom_field");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("custom_field.CUSTOM_FIELD_ID", "");
                          tMap.addColumn("custom_field.FIELD_NAME", "");
                          tMap.addColumn("custom_field.FIELD_DEFAULT_VALUE", "");
                            tMap.addColumn("custom_field.FIELD_TYPE", Integer.valueOf(0));
                            tMap.addColumn("custom_field.FIELD_NO", Integer.valueOf(0));
                          tMap.addColumn("custom_field.FIELD_GROUP", "");
                          tMap.addColumn("custom_field.KATEGORI_ID", "");
                          tMap.addColumn("custom_field.REQUIRED", Boolean.TRUE);
          }
}

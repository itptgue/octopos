package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Site
 */
public abstract class BaseSite extends BaseObject
{
    /** The Peer class */
    private static final SitePeer peer =
        new SitePeer();

        
    /** The value for the siteId field */
    private String siteId;
      
    /** The value for the siteName field */
    private String siteName;
      
    /** The value for the costingMethod field */
    private int costingMethod;
      
    /** The value for the description field */
    private String description;
  
    
    /**
     * Get the SiteId
     *
     * @return String
     */
    public String getSiteId()
    {
        return siteId;
    }

                        
    /**
     * Set the value of SiteId
     *
     * @param v new value
     */
    public void setSiteId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.siteId, v))
              {
            this.siteId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SiteName
     *
     * @return String
     */
    public String getSiteName()
    {
        return siteName;
    }

                        
    /**
     * Set the value of SiteName
     *
     * @param v new value
     */
    public void setSiteName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.siteName, v))
              {
            this.siteName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CostingMethod
     *
     * @return int
     */
    public int getCostingMethod()
    {
        return costingMethod;
    }

                        
    /**
     * Set the value of CostingMethod
     *
     * @param v new value
     */
    public void setCostingMethod(int v) 
    {
    
                  if (this.costingMethod != v)
              {
            this.costingMethod = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("SiteId");
              fieldNames.add("SiteName");
              fieldNames.add("CostingMethod");
              fieldNames.add("Description");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("SiteId"))
        {
                return getSiteId();
            }
          if (name.equals("SiteName"))
        {
                return getSiteName();
            }
          if (name.equals("CostingMethod"))
        {
                return Integer.valueOf(getCostingMethod());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(SitePeer.SITE_ID))
        {
                return getSiteId();
            }
          if (name.equals(SitePeer.SITE_NAME))
        {
                return getSiteName();
            }
          if (name.equals(SitePeer.COSTING_METHOD))
        {
                return Integer.valueOf(getCostingMethod());
            }
          if (name.equals(SitePeer.DESCRIPTION))
        {
                return getDescription();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getSiteId();
            }
              if (pos == 1)
        {
                return getSiteName();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getCostingMethod());
            }
              if (pos == 3)
        {
                return getDescription();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(SitePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        SitePeer.doInsert((Site) this, con);
                        setNew(false);
                    }
                    else
                    {
                        SitePeer.doUpdate((Site) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key siteId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setSiteId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setSiteId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getSiteId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Site copy() throws TorqueException
    {
        return copyInto(new Site());
    }
  
    protected Site copyInto(Site copyObj) throws TorqueException
    {
          copyObj.setSiteId(siteId);
          copyObj.setSiteName(siteName);
          copyObj.setCostingMethod(costingMethod);
          copyObj.setDescription(description);
  
                    copyObj.setSiteId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public SitePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Site\n");
        str.append("----\n")
           .append("SiteId               : ")
           .append(getSiteId())
           .append("\n")
           .append("SiteName             : ")
           .append(getSiteName())
           .append("\n")
           .append("CostingMethod        : ")
           .append(getCostingMethod())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
        ;
        return(str.toString());
    }
}

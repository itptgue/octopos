package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AdminLogMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AdminLogMapBuilder";

    /**
     * Item
     * @deprecated use AdminLogPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "admin_log";
    }

  
    /**
     * admin_log.ADMIN_LOG_ID
     * @return the column name for the ADMIN_LOG_ID field
     * @deprecated use AdminLogPeer.admin_log.ADMIN_LOG_ID constant
     */
    public static String getAdminLog_AdminLogId()
    {
        return "admin_log.ADMIN_LOG_ID";
    }
  
    /**
     * admin_log.TRANS_DATE
     * @return the column name for the TRANS_DATE field
     * @deprecated use AdminLogPeer.admin_log.TRANS_DATE constant
     */
    public static String getAdminLog_TransDate()
    {
        return "admin_log.TRANS_DATE";
    }
  
    /**
     * admin_log.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use AdminLogPeer.admin_log.USER_NAME constant
     */
    public static String getAdminLog_UserName()
    {
        return "admin_log.USER_NAME";
    }
  
    /**
     * admin_log.SQL_CMD
     * @return the column name for the SQL_CMD field
     * @deprecated use AdminLogPeer.admin_log.SQL_CMD constant
     */
    public static String getAdminLog_SqlCmd()
    {
        return "admin_log.SQL_CMD";
    }
  
    /**
     * admin_log.SQL_MSG
     * @return the column name for the SQL_MSG field
     * @deprecated use AdminLogPeer.admin_log.SQL_MSG constant
     */
    public static String getAdminLog_SqlMsg()
    {
        return "admin_log.SQL_MSG";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("admin_log");
        TableMap tMap = dbMap.getTable("admin_log");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("admin_log.ADMIN_LOG_ID", "");
                          tMap.addColumn("admin_log.TRANS_DATE", new Date());
                          tMap.addColumn("admin_log.USER_NAME", "");
                          tMap.addColumn("admin_log.SQL_CMD", "");
                          tMap.addColumn("admin_log.SQL_MSG", "");
          }
}

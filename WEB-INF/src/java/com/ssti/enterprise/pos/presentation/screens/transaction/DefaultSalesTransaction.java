package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.ItemMinPriceTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.enterprise.pos.tools.pwp.PWPTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.enterprise.pos.tools.sales.VoidTransTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.SortingTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Sales Transaction Screen Class 
 * @see SalesTransaction
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DefaultSalesTransaction.java,v 1.20 2009/05/04 02:02:36 albert Exp $ <br>
 *
 * <pre>
 * $Log: DefaultSalesTransaction.java,v $
 * Revision 1.20  2009/05/04 02:02:36  albert
 * 
 * 2017-06-23
 * - change getData method load invoice payment from DB, to handle invoice pending 
 *   with multi payment from sync error 
 * - see change in {@link InvoicePaymentTool#} processPayment, now will delete from DB 
 *   if InvoicePayment is exists
 * 
 * 2016-10-25
 * - change isExist method add applyCustomPricing @see {@link ItemMinPriceTool#}
 * 
 * 2016-09-21
 * - change multi unit logic to much simpler logic using discount amount
 *   1. remove method applyMultiUnitDiscount and applyMultiUnitGet 
 *   2. add method applyMULDiscount for multi unit
 * - change PWP logic using discount amount 
 *   1. Change method applyPWPDiscount to call PWPTool.updateDiscAmount
 * 
 * 2016-09-07
 * - move displayer logic to JS, remove method displayTotal, resetScreen
 * - fix method applyMultiUnitGet add else if item not in multi unit disc 
 *   then reset iMultiUnitGetQty var
 * 
 * 2016-03-08
 * - change method doBuildTemplate, findCustomDiscount and put in context
 * 
 * 2015-04-29
 * - change method importDOData add validation isDOInActiveSI to check whether this  
 *   DO exists in another pending / processed SI
 * 
 * 2015-03-02
 * change method isExist modify rule to allow merge if (discount not equal or isQtyDiscount)
 * 
 * 2015-02-28
 * - Change method refreshDetail Related to Multi Unit. Must Refresh all multi unit process
 * - Change method applyPWPDiscount set discount in TransDet 100% if pwp price is 0
 * 
 * </pre><br>
 */
public class DefaultSalesTransaction extends SalesSecureScreen
{
	protected static final Log log = LogFactory.getLog(DefaultSalesTransaction.class);
	
	protected static final String[] a_PERM = {"Sales Transaction", "View Sales Invoice"};
    			
    protected static final int i_OP_ADD_DETAIL  = 1;
    protected static final int i_OP_DEL_DETAIL  = 2;
	protected static final int i_OP_NEW_TRANS   = 3;
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
	protected static final int i_OP_IMPORT_FROM_DO = 5;
	protected static final int i_OP_COPY_TRANS   = 6;
	protected static final int i_OP_IMPORT_FROM_SO = 7;
	
	protected static final int i_OP_REFRESH_DETAIL = 10;
	protected static final int i_OP_REFRESH_MASTER = 11; //only when no promo exists if promo then should refresh detail
	
	protected String m_sClientIP = "";    
    protected SalesTransaction oTR = null;    
    protected List vTD = null;
    protected List vVD = null;
    protected List vPMT = null;
    protected HttpSession oSes;
    protected boolean b_UPDATEABLE_DETAIL = false;     
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	m_sClientIP = data.getRemoteAddr();
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {	    	
    	super.doBuildTemplate(data, context);
    	
    	int iOp = data.getParameters().getInt("op");
		b_UPDATEABLE_DETAIL = data.getParameters().getBoolean("bUpdateDetail", false);
		initSession (data);
				
		try 
		{		
			//validate open cashier
	    	CashierBalanceTool.validateOpenCashier(data, context);
	    	
			if (iOp == i_OP_ADD_DETAIL && !b_REFRESH) {
				addDetailData (data);
			}	
			else if (iOp == i_OP_DEL_DETAIL && !b_REFRESH) {
				delDetailData (data);
			}	
			else if (iOp == i_OP_NEW_TRANS) {
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS) {
				getData (data);
			}
			else if (iOp == i_OP_IMPORT_FROM_DO && !b_REFRESH) {
				importDOData (data);
			}
			else if (iOp == i_OP_COPY_TRANS) {
				copyTrans (data);
			}
			else if (iOp == i_OP_IMPORT_FROM_SO && !b_REFRESH) {
				importSOData (data);
			}
			else if (iOp == i_OP_REFRESH_DETAIL && !b_REFRESH) {
				refreshDetail (data);
			}
			else if (iOp == i_OP_REFRESH_MASTER && !b_REFRESH) {
				refreshMaster (data);
			}			
			
			//set custom discount in templates
			Discount oCustomDisc = DiscountTool.findCustomDiscount(oTR);
			context.put("CustomDisc", oCustomDisc);			
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage("ERROR : " + _oEx.getMessage());
		}
		context.put(s_TR, oSes.getAttribute (s_TR));    	
    	context.put(s_TD, oSes.getAttribute (s_TD));
    	context.put(s_PMT, oSes.getAttribute (s_PMT));
    	    	
    	//set Invoice related variable in context
    	setInvoiceContext(data, context);    	
    }

	protected void initSession ( RunData data )
	{	
		synchronized (this)
		{
			oSes = data.getSession();
			if (oSes.getAttribute (s_TR) == null) 
			{
				createNewTrans(data);
			}
			oTR = (SalesTransaction) oSes.getAttribute (s_TR);
			vTD = (List) oSes.getAttribute(s_TD);
			vVD = (List) oSes.getAttribute(s_VD);
			vPMT = (List) oSes.getAttribute(s_PMT);
		}
	}
    
    protected void addDetailData (RunData data) 
    	throws Exception
    {
        if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail (vTD, data);
        data.getParameters().setProperties (oTR);	
        
		SalesTransactionDetail oTD = new SalesTransactionDetail();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			data.getParameters().setProperties (oTD);	
			
			oTD.setItemCode (oItem.getItemCode());
			oTD.setItemName (oItem.getItemName());
			//oTD.setDescription(oItem.getDescription());
		}
		else
		{
			return;
		}

		double dPrice = oTD.getItemPrice().doubleValue() * oTR.getCurrencyRate().doubleValue();		
		//validate price first before add into detail
		TransactionTool.validatePrice(oTD.getItemId(), dPrice, 
				oTD.getDiscount(), oTR.getLocationId(), oTR.getCustomerId());		
		
		processAddDetail(oTD);
		
		TransactionTool.setHeaderProperties (oTR, vTD,data);
		oSes.setAttribute (s_TD, vTD); 
		oSes.setAttribute (s_TR, oTR);     
    }
    
    /**
     * check and apply discount / merge / insert TD
     * 
     * @param _oTD
     * @throws Exception
     */
    protected void processAddDetail(SalesTransactionDetail _oTD)
    	throws Exception
    {		
		//check discount here
		if (!isExist (_oTD)) 
		{
			_oTD.setIndexNo(vTD.size());
		    if (b_SALES_FIRST_LAST)
		    {
			    vTD.add (0, _oTD);
			}
			else
			{
			    vTD.add (_oTD);
			}
		}	
		
    	//if use custom pricing
    	//apply price update here	
    	if(PreferenceTool.getSalesCustomPricingUse())
    	{
    		ItemMinPriceTool.applyCustomPricing(oTR, vTD, _oTD);
    	}    	
    }
        
    /**
     * refresh detail and clear discount to reflect payment type change
     * 
     * @param data
     * @throws Exception
     */
	private void refreshDetail(RunData data)
		throws Exception
	{
        if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail (vTD, data);
        data.getParameters().setProperties (oTR);	

		List vTDC = (List) ((ArrayList)vTD).clone();
		vTD = new ArrayList();
		vTDC = SortingTool.sort(vTDC,"indexNo");
		Iterator iter = vTDC.iterator();
		while(iter.hasNext())
		{
			SalesTransactionDetail oTD = (SalesTransactionDetail) iter.next();	
			if(!oTD.getDiscountId().equals(PreferenceTool.posSpcDiscCode()))
			{
				//if not multi unit and special discount then reset disc
				oTD.setDiscountId("");
				oTD.setDiscount("0");
				oTD.setSubTotalDisc(bd_ZERO);
			}
			processAddDetail(oTD);
			iter.remove();
		}
		vTDC = null;
		
		oTR.setTotalDiscountPct("0");						
		TransactionTool.setHeaderProperties (oTR, vTD,data);
		oSes.setAttribute (s_TD, vTD); 
		oSes.setAttribute (s_TR, oTR);     
	}

    /**
     * refresh detail and clear discount to reflect payment type change
     * 
     * @param data
     * @throws Exception
     */
	private void refreshMaster(RunData data)
		throws Exception
	{
        if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail (vTD, data);
        data.getParameters().setProperties (oTR);	
						
		TransactionTool.setHeaderProperties (oTR, vTD,data);
		oSes.setAttribute (s_TD, vTD); 
		oSes.setAttribute (s_TR, oTR);     
	}
	
    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	int iNo = data.getParameters().getInt("No") - 1;
        if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail (vTD,data);
        
		if (iNo >= 0 && iNo < vTD.size()) 
		{
			SalesTransactionDetail oTD = (SalesTransactionDetail) vTD.get(iNo);
			if (oTD != null)
			{
				String sDiscID = oTD.getDiscountId();
				if(StringUtil.isNotEmpty(sDiscID) && 
				   oTD.getDiscountData() != null &&
				   (oTD.getDiscountData().getDiscountType() == DiscountTool.i_BY_MULTI || 
				    oTD.getDiscountData().getDiscountType() == DiscountTool.i_BY_PWP))
				{
					delByDiscID(sDiscID);
				}
				else if(StringUtil.isNotEmpty(oTD.getDeliveryOrderId()))
				{
					delByDOID(oTD.getDeliveryOrderId());
				}
				else
				{
					VoidTransTool.setVoid(oTR, oTD, vVD);				
					oSes.setAttribute(s_VD, vVD);
					vTD.remove (iNo);
					oSes.setAttribute(s_TD, vTD);
				}
			}
			TransactionTool.setHeaderProperties (oTR, vTD, data);
			oSes.setAttribute(s_TR, oTR); 
		}    	
    }
    
    private void delByDiscID(String _sDiscID)
	{
    	log.debug("vTD before DEL: " + vTD.size());
    	Iterator iter = vTD.iterator();
    	while(iter.hasNext())
    	{
    		SalesTransactionDetail oTD = (SalesTransactionDetail) iter.next();
        	log.debug("oTD : " + oTD.getDiscountId() + " Del Disc ID " + _sDiscID);

    		if(StringUtil.isEqual(oTD.getDiscountId(), _sDiscID))
    		{
        		VoidTransTool.setVoid(oTR, oTD, vVD);				
        		oSes.setAttribute(s_VD, vVD);
            	log.debug("vTD DEL : " + oTD.getDiscountId());        		
    			iter.remove();
    		}
    	}
    	log.debug("vTD after DEL: " + vTD.size());
		oSes.setAttribute(s_TD, vTD);
	}

    private void delByDOID(String _sDOID)
	{
    	log.debug("DEL BY DO: vTD before: " + vTD.size());
    	Iterator iter = vTD.iterator();
    	while(iter.hasNext())
    	{
    		SalesTransactionDetail oTD = (SalesTransactionDetail) iter.next();
        	log.debug("oTD : " + oTD.getDeliveryOrderId() + " Del Disc ID " + _sDOID);

    		if(StringUtil.isEqual(oTD.getDeliveryOrderId(), _sDOID))
    		{
    			iter.remove();
    			//VoidTransTool.setVoid(oTR, oTD, vVD);				
        		//oSes.setAttribute(s_VD, vVD);
            	//log.debug("vTD DEL : " + oTD.getDeliveryOrderId());        		    			
    		}
    	}
    	log.debug("DEL BY DO: vTD after: " + vTD.size());
		oSes.setAttribute(s_TD, vTD);
	}
    
    protected void createNewTrans ( RunData data ) 
    {
    	synchronized (this) 
    	{
    		vVD = VoidTransTool.newTransVoid(oTR, vVD);
    		oTR = new SalesTransaction();
    		vTD = new ArrayList();
    		vPMT = new ArrayList();

    		//initialize inclusive tax
    		oTR.setIsInclusiveTax(b_SALES_TAX_INCLUSIVE);
    		
	    	oSes.setAttribute (s_TR,  oTR);
			oSes.setAttribute (s_TD,  vTD);
			oSes.setAttribute (s_VD,  vVD);
			oSes.setAttribute (s_PMT, vPMT);
			oSes.setAttribute (s_PMT_VCH, new ArrayList());
    	}
    }
        
    /**
     * validate if item exists in transaction
     * also check if discount exists
     * 
     * @param _oTD
     * @return
     * @throws Exception
     */
    protected boolean isExist (SalesTransactionDetail _oTD) 
    	throws Exception
    {
    	boolean bIsExist = true;     	
    	for (int i = 0; i < vTD.size(); i++) 
    	{
    		SalesTransactionDetail oExistTD = (SalesTransactionDetail) vTD.get (i);
    		if ( oExistTD.getItemId().equals(_oTD.getItemId()) && 
    		     oExistTD.getUnitId().equals(_oTD.getUnitId()) &&
    		     oExistTD.getTaxId().equals(_oTD.getTaxId()) &&
    		     oExistTD.getDescription().equals(_oTD.getDescription()) &&
    		    (oExistTD.getDiscount().equals(_oTD.getDiscount()) || oExistTD.isQtyDiscount() || oExistTD.isPwpDiscount() || oExistTD.isMultiUnit()) &&      		    
    		    !_oTD.isSpecialDiscByAmount() &&
    		    !oExistTD.isSpecialDiscByAmount() &&
				(oExistTD.getItemPrice().intValue() == _oTD.getItemPrice().intValue()) &&
				 StringUtil.isEqual(oExistTD.getEmployeeId(), _oTD.getEmployeeId()) &&
   		     	 StringUtil.isEmpty(oExistTD.getDeliveryOrderDetailId())) 
    		{    			
       		    if(b_SALES_MERGE_DETAIL || oExistTD.isPwpDiscount() || oExistTD.isMultiUnit()) //multi unit and PWP must merge
    		    {       		    	
				    mergeData (oExistTD, _oTD);
				    //apply discount setup to the existing detail data
				    //if discount on sales trans was 0 then apply Disc from discount setup
				    if(oExistTD.getDiscount().equals("0"))
				    {
    			        applyDiscount (oTR, oExistTD, true); //apply discount after merge
    			    }
				}
				else
				{
	    	        //apply discount to the new detail data
	    	        //if discount on sales trans was 0 then apply Disc from discount setup
	    	        if(_oTD.getDiscount().equals("0") && StringUtil.isEmpty(_oTD.getDiscountId()))
	    	        {
	    	            applyDiscount (oTR, _oTD, false);
	    	        }				
					bIsExist = false;
				}
				return bIsExist;    			
    		}		
    	}
    	
    	//if there is no same item on list or  no item in list exist
    	bIsExist = false;
    	//apply discount to the new detail data
    	//if discount on sales trans was 0 then apply Disc from discount setup
    	if(_oTD.getDiscount().equals("0") && StringUtil.isEmpty(_oTD.getDiscountId()))
    	{
    	    applyDiscount (oTR, _oTD, false);
    	}
    	return bIsExist;
    } 

    protected void mergeData (SalesTransactionDetail _oOldTD, SalesTransactionDetail _oNewTD) 
        throws Exception
    {       
        double dQty         = _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
        
        //existing discount will use from new TD
        String sDiscount    = _oNewTD.getDiscount();
        double dTotalSales  = dQty * _oNewTD.getItemPrice().doubleValue();
        double dTotalDisc   = Calculator.calculateDiscount(sDiscount,dTotalSales);
        double dTaxAmount   = _oNewTD.getTaxAmount().doubleValue()/100;
        
        dTotalSales          = dTotalSales - dTotalDisc;
        double dTotalTax     = dTotalSales * dTaxAmount;
        double dSubTotal     = dTotalSales; //+ dTotalTax;
        double dSubTotalCost = dQty * _oNewTD.getItemCost().doubleValue();
        
        _oOldTD.setQty          ( new BigDecimal (dQty));       
        _oOldTD.setQtyBase      ( UnitTool.getBaseQty(_oOldTD.getItemId(), _oOldTD.getUnitId(), _oOldTD.getQty()));
        _oOldTD.setDiscount     ( sDiscount  );     
        _oOldTD.setTaxAmount    ( new BigDecimal (dTaxAmount * 100)  );     
        
        _oOldTD.setSubTotalCost ( new BigDecimal (dSubTotalCost));
        _oOldTD.setSubTotalDisc ( new BigDecimal (dTotalDisc)   );
        _oOldTD.setSubTotalTax  ( new BigDecimal (dTotalTax)    );
        _oOldTD.setSubTotal     ( new BigDecimal (dSubTotal)    );
        _oNewTD = null;
    }
    
    /**
     * apply discount to sales transaction
     * 
     * @param _oTR
     * @param _oTD
     * @throws Exception
     */
	protected void applyDiscount(SalesTransaction _oTR, SalesTransactionDetail _oTD, boolean _bMerge)
    	throws Exception
    {
		String sLocID = _oTR.getLocationId();
		String sItemID = _oTD.getItemId();
        String sCustID = _oTR.getCustomerId();
        String sPTypeID = _oTR.getPaymentTypeId();
		Date dDate = _oTR.getTransactionDate();		
        List vPTypeIDs = _oTR.getPaymentTypeIDs();
		
        log.debug("****** FIND DISCOUNT **********");
        log.debug("Loc   : " + LocationTool.getLocationCodeByID(sLocID));
        log.debug("Cust  : " + CustomerTool.getCodeByID(sCustID));
        log.debug("Item  : " + ItemTool.getItemCodeByID(sItemID));
        log.debug("Pmt   : " + PaymentTypeTool.getCodeByID(sItemID));
        
        Discount oDisc =  DiscountTool.findDiscount(sLocID, sItemID, sCustID, sPTypeID, vPTypeIDs, dDate);
        boolean bPWP = false;
		boolean bSKU = false;
		boolean bMUL = false;	
		boolean bPRC = false;
		
		if (oDisc != null)
		{
            if (StringUtil.isNotEmpty(oDisc.getItemSku()))
            {
                bSKU = true;
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_PWP)
            {
                bPWP = true; 
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_MULTI)
            {
            	bMUL = true; 
            }  
            if (oDisc.getDiscountType() == DiscountTool.i_BY_PRICE)
            {
            	bPRC = true; 
            }  
            
            if (bSKU)
			{
				applySKUDiscount(_oTD,oDisc,_bMerge);	
			}
            else if(bPWP)
            {
                applyPWPDiscount(_oTD,oDisc);
            }
            else if(bMUL)
            {
                applyMULDiscount(_oTD,oDisc);
            }     
			else
			{
			    applySTDDiscount(_oTD,oDisc);
			}
            _oTD.setDiscountId(oDisc.getDiscountId());
		}
	}    
    
	private void applySKUDiscount(SalesTransactionDetail _oTD,
								  Discount _oDisc,
								  boolean _bMerge)
		throws Exception
	{
        log.debug ("****** SKU DISCOUNT **********");
		List vSKU = new ArrayList(vTD.size());
		double dTotalSKUQty = 0;
		for (int i = 0; i < vTD.size(); i++) 
    	{
    		SalesTransactionDetail oExistTD = (SalesTransactionDetail) vTD.get (i);
    		Item oItem = ItemTool.getItemByID(oExistTD.getItemId());
    		if (oItem.getItemSku().equals(_oDisc.getItemSku()))
    		{
    			vSKU.add(oExistTD);
    			dTotalSKUQty += oExistTD.getQtyBase().doubleValue();
    		}
    	}
		if (!_bMerge)
		{
			vSKU.add(_oTD);
			dTotalSKUQty += _oTD.getQty().doubleValue();
		}
		for (int i = 0; i < vSKU.size(); i++) 
    	{
    		SalesTransactionDetail oExistTD = (SalesTransactionDetail) vSKU.get (i);
    		oExistTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, dTotalSKUQty));
            recalculateTD(oExistTD);
    	}
	}
    
	private void applyPWPDiscount(SalesTransactionDetail _oTD,Discount _oDisc)
	    throws Exception
	{
        log.debug ("****** PWP DISCOUNT **********");
        PWPTool.updateGetDiscAmount(_oDisc, vTD, _oTD);
        recalculateTD(_oTD);
	}

    private void applySTDDiscount(SalesTransactionDetail _oTD,Discount _oDisc)
        throws Exception
    {
        log.debug ("****** NORMAL DISCOUNT **********");        
        _oTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, _oTD.getQty().doubleValue(), _oTD.getItemPrice().doubleValue()));
        recalculateTD(_oTD);
    }

    /**
     * apply multi unit discount validate with existing buy qty
     * 
     * @param _oTD
     * @throws Exception
     */
    private void applyMULDiscount(SalesTransactionDetail _oTD,Discount _oDisc)
    	throws Exception
    {    	    	
    	log.debug ("****** MULTI UNIT DISCOUNT **********");
		if(_oDisc != null)		
		{			
			int iBuy = _oDisc.getBuyQty().intValue();
			int iGet = _oDisc.getGetQty().intValue();
			int iPack = iBuy + iGet;
			int iQty = _oTD.getQty().intValue(); 
			
			int iAvail = (int) (iQty / iPack);
			int iDisc = iAvail * iGet;
			double dAmt = Calculator.mul(_oTD.getItemPrice(),iDisc);			
			
			_oTD.setDiscount(new BigDecimal(dAmt).toString());
			_oTD.setDiscountId(_oDisc.getDiscountId());	
			recalculateTD(_oTD);
		}
	}
     
    private void recalculateTD(SalesTransactionDetail _oTD)
        throws Exception
    {        
        double dSubTotal =  _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();            
        double dSubTotalDisc = 0;
        dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), dSubTotal, _oTD.getQty());            
        dSubTotal = dSubTotal - dSubTotalDisc;
        double dSubTotalTax = (_oTD.getTaxAmount().doubleValue() / 100) * dSubTotal;
        
        //dSubTotal = dSubTotal;//+ dSubTotalTax;
        _oTD.setSubTotalDisc (new BigDecimal (dSubTotalDisc));
        _oTD.setSubTotalTax  (new BigDecimal (dSubTotalTax));
        _oTD.setSubTotal     (new BigDecimal (dSubTotal));
        
        log.debug("Discount : " + _oTD.getDiscount());
        log.debug("SubTotal : " + _oTD.getSubTotal());
    }        
    
    private boolean isExist (String _sDOID)
    {
    	for (int i = 0; i < vTD.size(); i++)
    	{
    		SalesTransactionDetail oExistPID = (SalesTransactionDetail) vTD.get (i);
    		if ( (oExistPID.getDeliveryOrderId().equals(_sDOID)) )
    		{
				return true;
    		}
    	}
    	return false;
    }
    
    protected void getData ( RunData data ) 
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	int iSave = data.getParameters().getInt("save",-1);
        if (StringUtil.isNotEmpty(sID)) 
    	{   
    	    try 
    		{
                synchronized (this)
                {
                    oTR = TransactionTool.getHeaderByID (sID);
                    vTD = TransactionTool.getDetailsByID (sID);
                    vVD = VoidTransTool.getVoidTrans(sID,null);                    
                    
                    if(iSave < 0)
        		    {
        			    oSes.setAttribute (s_TR, oTR);
    				    oSes.setAttribute (s_TD, vTD);
    				    oSes.setAttribute (s_VD, vVD);
    				    
    					//payment session must also be set to new 
    				    vPMT = InvoicePaymentTool.getTransPayment(sID);
    				    oSes.setAttribute (s_PMT, vPMT);    				    
        		    }
                }
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }
    
    /**
     * import DO data
     * 
     * @param data
     * @throws Exception
     */
    private void importDOData(RunData data)
    	throws Exception
    {
    	boolean bDirectFromDO = data.getParameters().getBoolean("FromDO",false);
    	if(bDirectFromDO)
    	{
    		createNewTrans(data);
    	}
    	
        if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail (vTD,data);
        
    	String sDOID = data.getParameters().getString("DoId");
    	String sSOID = "";
    	List vDOD = null;
    	
    	if(!DeliveryOrderTool.isDOInActiveSI(sDOID)) //check whether DO exists in pending / processed SI
    	{    	
	    	DeliveryOrder oDO = DeliveryOrderTool.getHeaderByID(sDOID);    	
			if(oDO != null) sSOID = oDO.getSalesOrderId(); 		
			if (!isExist (sDOID))
			{
			    String sCustomerID = data.getParameters().getString("CustomerId","");
				vDOD = DeliveryOrderTool.getDetailsByID(sDOID);
				TransactionTool.mapDODetailToSIDetail(vDOD ,vTD ,sSOID, sCustomerID);
			}
			TransactionTool.setHeaderProperties(oTR, vTD, data);
	
			if(oDO != null)
	        {		
			    oTR.setPaymentTypeId(oDO.getPaymentTypeId());
			    oTR.setPaymentTermId(oDO.getPaymentTermId());
	
			    oTR.setFobId(oDO.getFobId());
			    oTR.setCourierId(oDO.getCourierId());
			    oTR.setShipTo(oDO.getShipTo());
	
			    oTR.setSalesId(oDO.getSalesId());
				oTR.setTotalDiscountPct(oDO.getTotalDiscountPct());
				
			    oTR.setIsTaxable(oDO.getIsTaxable());
				oTR.setIsInclusiveTax(oDO.getIsInclusiveTax());
				
				oTR.setTotalExpense(oDO.getEstimatedFreight());
				oTR.setIsInclusiveFreight(oDO.getIsInclusiveFreight());
				oTR.setFreightAccountId(oDO.getFreightAccountId());
				oTR.setLocationId(oDO.getLocationId());
				
				StringBuilder oRemark = new StringBuilder();
				if (StringUtil.isNotEmpty(oTR.getRemark()))
				{
					oRemark.append(oTR.getRemark());
					oRemark.append("\n");
				}
				oRemark.append(oDO.getRemark());
				oTR.setRemark(oRemark.toString());
	        }
    	}
    	else
    	{
    		data.setMessage("DO Exists in another SI, can not import DO ");
    	}
		oSes.setAttribute (s_TD, vTD);
		oSes.setAttribute (s_TR, oTR);
    	
    }   
    
    /**
     * import DO data
     * 
     * @param data
     * @throws Exception
     */
    private void importSOData ( RunData data )
    	throws Exception
    {
    	if(b_UPDATEABLE_DETAIL) TransactionTool.updateDetail (vTD,data);
    	
    	String sSOID = data.getParameters().getString("SoId");
    	List vSOD = null;
    	if (StringUtil.isNotEmpty(sSOID))
    	{
    		SalesOrder oSO = SalesOrderTool.getHeaderByID(sSOID);    	
    		if (oSO != null)
    		{
    			String sCustomerID = data.getParameters().getString("CustomerId","");
    			vSOD = SalesOrderTool.getDetailsByID(sSOID);
    			vTD = new ArrayList(vSOD.size());
    			TransactionTool.mapSODetailToSIDetail(vSOD ,vTD ,sSOID, sCustomerID);    		
    			TransactionTool.setHeaderProperties(oTR, vTD, data);    		
    			
    			oTR.setCustomerId(sCustomerID);
    			oTR.setCustomerName(CustomerTool.getCustomerNameByID(sCustomerID));
    			oTR.setPaymentTypeId(oSO.getPaymentTypeId());
    			oTR.setPaymentTermId(oSO.getPaymentTermId());
    			
    			oTR.setFobId(oSO.getFobId());
    			oTR.setCourierId(oSO.getCourierId());
    			oTR.setShipTo(oSO.getShipTo());
    			
    			oTR.setSalesId(oSO.getSalesId());
    			oTR.setTotalDiscountPct(oSO.getTotalDiscountPct());
    			
    			oTR.setIsTaxable(oSO.getIsTaxable());
    			oTR.setIsInclusiveTax(oSO.getIsInclusiveTax());
    			
    			oTR.setTotalExpense(oSO.getEstimatedFreight());
    			oTR.setIsInclusiveFreight(oSO.getIsInclusiveFreight());
    			oTR.setFreightAccountId(oSO.getFreightAccountId());
    			
    			StringBuilder oRemark = new StringBuilder();
    			if (StringUtil.isNotEmpty(oTR.getRemark()))
    			{
    				oRemark.append(oTR.getRemark());
    				oRemark.append("\n");
    			}
    			oRemark.append(oSO.getRemark());
    			oTR.setRemark(oRemark.toString());
    			
    			oSes.setAttribute (s_TD, vTD);
    			oSes.setAttribute (s_TR, oTR);
    		}
    	}
    }   
    
    /**
     * copy Trans
     * 
     * @param data
     * @throws Exception
     */
    private void copyTrans ( RunData data )
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	SalesTransaction oTRC = TransactionTool.getHeaderByID(sID);
		
		if (oTRC != null && StringUtil.isNotEmpty (sID))
		{			
			List vTrans = TransactionTool.getDetailsByID(sID);
			vTD = new ArrayList(vTrans.size());
			for (int i = 0; i < vTrans.size(); i++)
			{
				SalesTransactionDetail oDet = (SalesTransactionDetail) vTrans.get(i);
				SalesTransactionDetail oNewDet = oDet.copy();
				vTD.add(oNewDet);
			}
		}
		TransactionTool.setHeaderProperties(oTR, vTD, data);
		
		oSes.setAttribute (s_TD, vTD);
		oSes.setAttribute (s_TR, oTR);
    }     
}

package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemUpdateHistory
 */
public abstract class BaseItemUpdateHistory extends BaseObject
{
    /** The Peer class */
    private static final ItemUpdateHistoryPeer peer =
        new ItemUpdateHistoryPeer();

        
    /** The value for the itemUpdateHistoryId field */
    private String itemUpdateHistoryId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the updatePart field */
    private String updatePart;
      
    /** The value for the oldValue field */
    private String oldValue;
      
    /** The value for the newValue field */
    private String newValue;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the updateDescription field */
    private String updateDescription;
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the ItemUpdateHistoryId
     *
     * @return String
     */
    public String getItemUpdateHistoryId()
    {
        return itemUpdateHistoryId;
    }

                        
    /**
     * Set the value of ItemUpdateHistoryId
     *
     * @param v new value
     */
    public void setItemUpdateHistoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemUpdateHistoryId, v))
              {
            this.itemUpdateHistoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdatePart
     *
     * @return String
     */
    public String getUpdatePart()
    {
        return updatePart;
    }

                        
    /**
     * Set the value of UpdatePart
     *
     * @param v new value
     */
    public void setUpdatePart(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updatePart, v))
              {
            this.updatePart = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OldValue
     *
     * @return String
     */
    public String getOldValue()
    {
        return oldValue;
    }

                        
    /**
     * Set the value of OldValue
     *
     * @param v new value
     */
    public void setOldValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.oldValue, v))
              {
            this.oldValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NewValue
     *
     * @return String
     */
    public String getNewValue()
    {
        return newValue;
    }

                        
    /**
     * Set the value of NewValue
     *
     * @param v new value
     */
    public void setNewValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.newValue, v))
              {
            this.newValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDescription
     *
     * @return String
     */
    public String getUpdateDescription()
    {
        return updateDescription;
    }

                        
    /**
     * Set the value of UpdateDescription
     *
     * @param v new value
     */
    public void setUpdateDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDescription, v))
              {
            this.updateDescription = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemUpdateHistoryId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("UpdatePart");
              fieldNames.add("OldValue");
              fieldNames.add("NewValue");
              fieldNames.add("UserName");
              fieldNames.add("UpdateDescription");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemUpdateHistoryId"))
        {
                return getItemUpdateHistoryId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("UpdatePart"))
        {
                return getUpdatePart();
            }
          if (name.equals("OldValue"))
        {
                return getOldValue();
            }
          if (name.equals("NewValue"))
        {
                return getNewValue();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("UpdateDescription"))
        {
                return getUpdateDescription();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemUpdateHistoryPeer.ITEM_UPDATE_HISTORY_ID))
        {
                return getItemUpdateHistoryId();
            }
          if (name.equals(ItemUpdateHistoryPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemUpdateHistoryPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(ItemUpdateHistoryPeer.UPDATE_PART))
        {
                return getUpdatePart();
            }
          if (name.equals(ItemUpdateHistoryPeer.OLD_VALUE))
        {
                return getOldValue();
            }
          if (name.equals(ItemUpdateHistoryPeer.NEW_VALUE))
        {
                return getNewValue();
            }
          if (name.equals(ItemUpdateHistoryPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(ItemUpdateHistoryPeer.UPDATE_DESCRIPTION))
        {
                return getUpdateDescription();
            }
          if (name.equals(ItemUpdateHistoryPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemUpdateHistoryId();
            }
              if (pos == 1)
        {
                return getItemId();
            }
              if (pos == 2)
        {
                return getItemCode();
            }
              if (pos == 3)
        {
                return getUpdatePart();
            }
              if (pos == 4)
        {
                return getOldValue();
            }
              if (pos == 5)
        {
                return getNewValue();
            }
              if (pos == 6)
        {
                return getUserName();
            }
              if (pos == 7)
        {
                return getUpdateDescription();
            }
              if (pos == 8)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemUpdateHistoryPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemUpdateHistoryPeer.doInsert((ItemUpdateHistory) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemUpdateHistoryPeer.doUpdate((ItemUpdateHistory) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemUpdateHistoryId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemUpdateHistoryId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemUpdateHistoryId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemUpdateHistoryId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemUpdateHistory copy() throws TorqueException
    {
        return copyInto(new ItemUpdateHistory());
    }
  
    protected ItemUpdateHistory copyInto(ItemUpdateHistory copyObj) throws TorqueException
    {
          copyObj.setItemUpdateHistoryId(itemUpdateHistoryId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setUpdatePart(updatePart);
          copyObj.setOldValue(oldValue);
          copyObj.setNewValue(newValue);
          copyObj.setUserName(userName);
          copyObj.setUpdateDescription(updateDescription);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setItemUpdateHistoryId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemUpdateHistoryPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemUpdateHistory\n");
        str.append("-----------------\n")
           .append("ItemUpdateHistoryId  : ")
           .append(getItemUpdateHistoryId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("UpdatePart           : ")
           .append(getUpdatePart())
           .append("\n")
           .append("OldValue             : ")
           .append(getOldValue())
           .append("\n")
           .append("NewValue             : ")
           .append(getNewValue())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("UpdateDescription    : ")
           .append(getUpdateDescription())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

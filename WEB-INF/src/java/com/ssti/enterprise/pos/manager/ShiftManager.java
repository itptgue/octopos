package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Shift;
import com.ssti.enterprise.pos.om.ShiftPeer;
import com.ssti.framework.tools.StringUtil;

public class ShiftManager
{   
	private static Log log = LogFactory.getLog(ShiftManager.class);
	 
    private static ShiftManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private ShiftManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static ShiftManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (ShiftManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new ShiftManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllShift ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("AllShift");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(ShiftPeer.SHIFT_CODE);
			List vData = ShiftPeer.doSelect(oCrit);
			oCache.put(new Element ("AllShift", (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public Shift getShiftByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Shift" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(ShiftPeer.SHIFT_ID, _sID);
				List vData = ShiftPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("Shift" + _sID, (Serializable)vData.get(0))); 
					return (Shift) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Shift) oElement.getValue();
		}
	}

	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Shift _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getShiftId()) ) 
        {
            oCache.remove("Shift" + _oData.getShiftId());
        }
        oCache.put(new Element ("Shift" + _oData.getShiftId(), _oData));
        oCache.remove("AllShift");
    }
	
	//for deleete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("Shift" + _sID);
        oCache.remove("AllShift");
    } 
}
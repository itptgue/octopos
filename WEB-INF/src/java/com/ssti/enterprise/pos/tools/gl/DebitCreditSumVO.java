package com.ssti.enterprise.pos.tools.gl;

import java.util.List;

import com.workingdogs.village.Record;

public class DebitCreditSumVO implements GlAttributes
{
	double primeDebit = 0;
	double primeCredit = 0;	
	double baseDebit = 0;
	double baseCredit = 0;
	
	public double getBaseCredit() {
		return baseCredit;
	}
	public void setBaseCredit(double baseCredit) {
		this.baseCredit = baseCredit;
	}
	public double getBaseDebit() {
		return baseDebit;
	}
	public void setBaseDebit(double baseDebit) {
		this.baseDebit = baseDebit;
	}
	public double getPrimeCredit() {
		return primeCredit;
	}
	public void setPrimeCredit(double primeCredit) {
		this.primeCredit = primeCredit;
	}
	public double getPrimeDebit() {
		return primeDebit;
	}
	public void setPrimeDebit(double primeDebit) {
		this.primeDebit = primeDebit;
	}
	
	public void setFromRecord(List _vRec)
		throws Exception
	{
		if (_vRec != null && _vRec.size() > 0)
	    {
			for (int i = 0; i < _vRec.size(); i++)
			{
		        Record oData = (Record)_vRec.get(i);
		        int iType = oData.getValue(3).asInt();
		        if (iType == i_DEBIT)
		        {
		        	baseDebit =  oData.getValue(1).asDouble();            
		        	primeDebit =  oData.getValue(2).asDouble();            
		        }
		        else if (iType == i_CREDIT)
		        {
		        	baseCredit =  oData.getValue(1).asDouble();            
		        	primeCredit =  oData.getValue(2).asDouble();		        	
		        }
			}
	    }
	}
	
	public double[] getDebit()
	{
		double[] d = new double[2];
		d[i_BASE] = baseDebit;
		d[i_PRIME] = primeDebit;
		return d;
	}
	
	public double[] getCredit()
	{
		double[] d = new double[2];
		d[i_BASE] = baseCredit;
		d[i_PRIME] = primeCredit;
		return d;
	}
	
	
}

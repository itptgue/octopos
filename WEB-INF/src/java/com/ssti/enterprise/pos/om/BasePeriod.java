package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Period
 */
public abstract class BasePeriod extends BaseObject
{
    /** The Peer class */
    private static final PeriodPeer peer =
        new PeriodPeer();

        
    /** The value for the periodId field */
    private String periodId;
      
    /** The value for the beginDate field */
    private Date beginDate;
      
    /** The value for the endDate field */
    private Date endDate;
      
    /** The value for the month field */
    private int month;
      
    /** The value for the year field */
    private int year;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the isClosed field */
    private boolean isClosed;
      
    /** The value for the periodCode field */
    private int periodCode;
                                                
    /** The value for the closedBy field */
    private String closedBy = "";
      
    /** The value for the closedDate field */
    private Date closedDate;
                                                
    /** The value for the remark field */
    private String remark = "";
                                                
    /** The value for the closeTransId field */
    private String closeTransId = "";
                                                
    /** The value for the deprTransId field */
    private String deprTransId = "";
      
    /** The value for the isFaClosed field */
    private boolean isFaClosed;
                                                
    /** The value for the faClosedBy field */
    private String faClosedBy = "";
      
    /** The value for the faClosedDate field */
    private Date faClosedDate;
                                                
    /** The value for the faClosingRemark field */
    private String faClosingRemark = "";
      
    /** The value for the isYearClosed field */
    private boolean isYearClosed;
                                                
    /** The value for the closeYearTransId field */
    private String closeYearTransId = "";
                                                
    /** The value for the closeYearRemark field */
    private String closeYearRemark = "";
  
    
    /**
     * Get the PeriodId
     *
     * @return String
     */
    public String getPeriodId()
    {
        return periodId;
    }

                        
    /**
     * Set the value of PeriodId
     *
     * @param v new value
     */
    public void setPeriodId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.periodId, v))
              {
            this.periodId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BeginDate
     *
     * @return Date
     */
    public Date getBeginDate()
    {
        return beginDate;
    }

                        
    /**
     * Set the value of BeginDate
     *
     * @param v new value
     */
    public void setBeginDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.beginDate, v))
              {
            this.beginDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EndDate
     *
     * @return Date
     */
    public Date getEndDate()
    {
        return endDate;
    }

                        
    /**
     * Set the value of EndDate
     *
     * @param v new value
     */
    public void setEndDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.endDate, v))
              {
            this.endDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Month
     *
     * @return int
     */
    public int getMonth()
    {
        return month;
    }

                        
    /**
     * Set the value of Month
     *
     * @param v new value
     */
    public void setMonth(int v) 
    {
    
                  if (this.month != v)
              {
            this.month = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Year
     *
     * @return int
     */
    public int getYear()
    {
        return year;
    }

                        
    /**
     * Set the value of Year
     *
     * @param v new value
     */
    public void setYear(int v) 
    {
    
                  if (this.year != v)
              {
            this.year = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsClosed
     *
     * @return boolean
     */
    public boolean getIsClosed()
    {
        return isClosed;
    }

                        
    /**
     * Set the value of IsClosed
     *
     * @param v new value
     */
    public void setIsClosed(boolean v) 
    {
    
                  if (this.isClosed != v)
              {
            this.isClosed = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PeriodCode
     *
     * @return int
     */
    public int getPeriodCode()
    {
        return periodCode;
    }

                        
    /**
     * Set the value of PeriodCode
     *
     * @param v new value
     */
    public void setPeriodCode(int v) 
    {
    
                  if (this.periodCode != v)
              {
            this.periodCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClosedBy
     *
     * @return String
     */
    public String getClosedBy()
    {
        return closedBy;
    }

                        
    /**
     * Set the value of ClosedBy
     *
     * @param v new value
     */
    public void setClosedBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.closedBy, v))
              {
            this.closedBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClosedDate
     *
     * @return Date
     */
    public Date getClosedDate()
    {
        return closedDate;
    }

                        
    /**
     * Set the value of ClosedDate
     *
     * @param v new value
     */
    public void setClosedDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.closedDate, v))
              {
            this.closedDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CloseTransId
     *
     * @return String
     */
    public String getCloseTransId()
    {
        return closeTransId;
    }

                        
    /**
     * Set the value of CloseTransId
     *
     * @param v new value
     */
    public void setCloseTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.closeTransId, v))
              {
            this.closeTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeprTransId
     *
     * @return String
     */
    public String getDeprTransId()
    {
        return deprTransId;
    }

                        
    /**
     * Set the value of DeprTransId
     *
     * @param v new value
     */
    public void setDeprTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.deprTransId, v))
              {
            this.deprTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsFaClosed
     *
     * @return boolean
     */
    public boolean getIsFaClosed()
    {
        return isFaClosed;
    }

                        
    /**
     * Set the value of IsFaClosed
     *
     * @param v new value
     */
    public void setIsFaClosed(boolean v) 
    {
    
                  if (this.isFaClosed != v)
              {
            this.isFaClosed = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FaClosedBy
     *
     * @return String
     */
    public String getFaClosedBy()
    {
        return faClosedBy;
    }

                        
    /**
     * Set the value of FaClosedBy
     *
     * @param v new value
     */
    public void setFaClosedBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.faClosedBy, v))
              {
            this.faClosedBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FaClosedDate
     *
     * @return Date
     */
    public Date getFaClosedDate()
    {
        return faClosedDate;
    }

                        
    /**
     * Set the value of FaClosedDate
     *
     * @param v new value
     */
    public void setFaClosedDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.faClosedDate, v))
              {
            this.faClosedDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FaClosingRemark
     *
     * @return String
     */
    public String getFaClosingRemark()
    {
        return faClosingRemark;
    }

                        
    /**
     * Set the value of FaClosingRemark
     *
     * @param v new value
     */
    public void setFaClosingRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.faClosingRemark, v))
              {
            this.faClosingRemark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsYearClosed
     *
     * @return boolean
     */
    public boolean getIsYearClosed()
    {
        return isYearClosed;
    }

                        
    /**
     * Set the value of IsYearClosed
     *
     * @param v new value
     */
    public void setIsYearClosed(boolean v) 
    {
    
                  if (this.isYearClosed != v)
              {
            this.isYearClosed = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CloseYearTransId
     *
     * @return String
     */
    public String getCloseYearTransId()
    {
        return closeYearTransId;
    }

                        
    /**
     * Set the value of CloseYearTransId
     *
     * @param v new value
     */
    public void setCloseYearTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.closeYearTransId, v))
              {
            this.closeYearTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CloseYearRemark
     *
     * @return String
     */
    public String getCloseYearRemark()
    {
        return closeYearRemark;
    }

                        
    /**
     * Set the value of CloseYearRemark
     *
     * @param v new value
     */
    public void setCloseYearRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.closeYearRemark, v))
              {
            this.closeYearRemark = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PeriodId");
              fieldNames.add("BeginDate");
              fieldNames.add("EndDate");
              fieldNames.add("Month");
              fieldNames.add("Year");
              fieldNames.add("Description");
              fieldNames.add("IsClosed");
              fieldNames.add("PeriodCode");
              fieldNames.add("ClosedBy");
              fieldNames.add("ClosedDate");
              fieldNames.add("Remark");
              fieldNames.add("CloseTransId");
              fieldNames.add("DeprTransId");
              fieldNames.add("IsFaClosed");
              fieldNames.add("FaClosedBy");
              fieldNames.add("FaClosedDate");
              fieldNames.add("FaClosingRemark");
              fieldNames.add("IsYearClosed");
              fieldNames.add("CloseYearTransId");
              fieldNames.add("CloseYearRemark");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PeriodId"))
        {
                return getPeriodId();
            }
          if (name.equals("BeginDate"))
        {
                return getBeginDate();
            }
          if (name.equals("EndDate"))
        {
                return getEndDate();
            }
          if (name.equals("Month"))
        {
                return Integer.valueOf(getMonth());
            }
          if (name.equals("Year"))
        {
                return Integer.valueOf(getYear());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("IsClosed"))
        {
                return Boolean.valueOf(getIsClosed());
            }
          if (name.equals("PeriodCode"))
        {
                return Integer.valueOf(getPeriodCode());
            }
          if (name.equals("ClosedBy"))
        {
                return getClosedBy();
            }
          if (name.equals("ClosedDate"))
        {
                return getClosedDate();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("CloseTransId"))
        {
                return getCloseTransId();
            }
          if (name.equals("DeprTransId"))
        {
                return getDeprTransId();
            }
          if (name.equals("IsFaClosed"))
        {
                return Boolean.valueOf(getIsFaClosed());
            }
          if (name.equals("FaClosedBy"))
        {
                return getFaClosedBy();
            }
          if (name.equals("FaClosedDate"))
        {
                return getFaClosedDate();
            }
          if (name.equals("FaClosingRemark"))
        {
                return getFaClosingRemark();
            }
          if (name.equals("IsYearClosed"))
        {
                return Boolean.valueOf(getIsYearClosed());
            }
          if (name.equals("CloseYearTransId"))
        {
                return getCloseYearTransId();
            }
          if (name.equals("CloseYearRemark"))
        {
                return getCloseYearRemark();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PeriodPeer.PERIOD_ID))
        {
                return getPeriodId();
            }
          if (name.equals(PeriodPeer.BEGIN_DATE))
        {
                return getBeginDate();
            }
          if (name.equals(PeriodPeer.END_DATE))
        {
                return getEndDate();
            }
          if (name.equals(PeriodPeer.MONTH))
        {
                return Integer.valueOf(getMonth());
            }
          if (name.equals(PeriodPeer.YEAR))
        {
                return Integer.valueOf(getYear());
            }
          if (name.equals(PeriodPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PeriodPeer.IS_CLOSED))
        {
                return Boolean.valueOf(getIsClosed());
            }
          if (name.equals(PeriodPeer.PERIOD_CODE))
        {
                return Integer.valueOf(getPeriodCode());
            }
          if (name.equals(PeriodPeer.CLOSED_BY))
        {
                return getClosedBy();
            }
          if (name.equals(PeriodPeer.CLOSED_DATE))
        {
                return getClosedDate();
            }
          if (name.equals(PeriodPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(PeriodPeer.CLOSE_TRANS_ID))
        {
                return getCloseTransId();
            }
          if (name.equals(PeriodPeer.DEPR_TRANS_ID))
        {
                return getDeprTransId();
            }
          if (name.equals(PeriodPeer.IS_FA_CLOSED))
        {
                return Boolean.valueOf(getIsFaClosed());
            }
          if (name.equals(PeriodPeer.FA_CLOSED_BY))
        {
                return getFaClosedBy();
            }
          if (name.equals(PeriodPeer.FA_CLOSED_DATE))
        {
                return getFaClosedDate();
            }
          if (name.equals(PeriodPeer.FA_CLOSING_REMARK))
        {
                return getFaClosingRemark();
            }
          if (name.equals(PeriodPeer.IS_YEAR_CLOSED))
        {
                return Boolean.valueOf(getIsYearClosed());
            }
          if (name.equals(PeriodPeer.CLOSE_YEAR_TRANS_ID))
        {
                return getCloseYearTransId();
            }
          if (name.equals(PeriodPeer.CLOSE_YEAR_REMARK))
        {
                return getCloseYearRemark();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPeriodId();
            }
              if (pos == 1)
        {
                return getBeginDate();
            }
              if (pos == 2)
        {
                return getEndDate();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getMonth());
            }
              if (pos == 4)
        {
                return Integer.valueOf(getYear());
            }
              if (pos == 5)
        {
                return getDescription();
            }
              if (pos == 6)
        {
                return Boolean.valueOf(getIsClosed());
            }
              if (pos == 7)
        {
                return Integer.valueOf(getPeriodCode());
            }
              if (pos == 8)
        {
                return getClosedBy();
            }
              if (pos == 9)
        {
                return getClosedDate();
            }
              if (pos == 10)
        {
                return getRemark();
            }
              if (pos == 11)
        {
                return getCloseTransId();
            }
              if (pos == 12)
        {
                return getDeprTransId();
            }
              if (pos == 13)
        {
                return Boolean.valueOf(getIsFaClosed());
            }
              if (pos == 14)
        {
                return getFaClosedBy();
            }
              if (pos == 15)
        {
                return getFaClosedDate();
            }
              if (pos == 16)
        {
                return getFaClosingRemark();
            }
              if (pos == 17)
        {
                return Boolean.valueOf(getIsYearClosed());
            }
              if (pos == 18)
        {
                return getCloseYearTransId();
            }
              if (pos == 19)
        {
                return getCloseYearRemark();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PeriodPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PeriodPeer.doInsert((Period) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PeriodPeer.doUpdate((Period) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key periodId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPeriodId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPeriodId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPeriodId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Period copy() throws TorqueException
    {
        return copyInto(new Period());
    }
  
    protected Period copyInto(Period copyObj) throws TorqueException
    {
          copyObj.setPeriodId(periodId);
          copyObj.setBeginDate(beginDate);
          copyObj.setEndDate(endDate);
          copyObj.setMonth(month);
          copyObj.setYear(year);
          copyObj.setDescription(description);
          copyObj.setIsClosed(isClosed);
          copyObj.setPeriodCode(periodCode);
          copyObj.setClosedBy(closedBy);
          copyObj.setClosedDate(closedDate);
          copyObj.setRemark(remark);
          copyObj.setCloseTransId(closeTransId);
          copyObj.setDeprTransId(deprTransId);
          copyObj.setIsFaClosed(isFaClosed);
          copyObj.setFaClosedBy(faClosedBy);
          copyObj.setFaClosedDate(faClosedDate);
          copyObj.setFaClosingRemark(faClosingRemark);
          copyObj.setIsYearClosed(isYearClosed);
          copyObj.setCloseYearTransId(closeYearTransId);
          copyObj.setCloseYearRemark(closeYearRemark);
  
                    copyObj.setPeriodId((String)null);
                                                                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PeriodPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Period\n");
        str.append("------\n")
           .append("PeriodId             : ")
           .append(getPeriodId())
           .append("\n")
           .append("BeginDate            : ")
           .append(getBeginDate())
           .append("\n")
           .append("EndDate              : ")
           .append(getEndDate())
           .append("\n")
           .append("Month                : ")
           .append(getMonth())
           .append("\n")
           .append("Year                 : ")
           .append(getYear())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("IsClosed             : ")
           .append(getIsClosed())
           .append("\n")
           .append("PeriodCode           : ")
           .append(getPeriodCode())
           .append("\n")
           .append("ClosedBy             : ")
           .append(getClosedBy())
           .append("\n")
           .append("ClosedDate           : ")
           .append(getClosedDate())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("CloseTransId         : ")
           .append(getCloseTransId())
           .append("\n")
           .append("DeprTransId          : ")
           .append(getDeprTransId())
           .append("\n")
           .append("IsFaClosed           : ")
           .append(getIsFaClosed())
           .append("\n")
           .append("FaClosedBy           : ")
           .append(getFaClosedBy())
           .append("\n")
           .append("FaClosedDate         : ")
           .append(getFaClosedDate())
           .append("\n")
           .append("FaClosingRemark      : ")
           .append(getFaClosingRemark())
           .append("\n")
           .append("IsYearClosed         : ")
           .append(getIsYearClosed())
           .append("\n")
           .append("CloseYearTransId     : ")
           .append(getCloseYearTransId())
           .append("\n")
           .append("CloseYearRemark      : ")
           .append(getCloseYearRemark())
           .append("\n")
        ;
        return(str.toString());
    }
}

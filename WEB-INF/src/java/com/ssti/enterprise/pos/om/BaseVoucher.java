package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Voucher
 */
public abstract class BaseVoucher extends BaseObject
{
    /** The Peer class */
    private static final VoucherPeer peer =
        new VoucherPeer();

        
    /** The value for the voucherId field */
    private String voucherId;
      
    /** The value for the voucherCode field */
    private String voucherCode;
      
    /** The value for the description field */
    private String description;
                                          
    /** The value for the amount field */
    private int amount = 1;
  
    
    /**
     * Get the VoucherId
     *
     * @return String
     */
    public String getVoucherId()
    {
        return voucherId;
    }

                                              
    /**
     * Set the value of VoucherId
     *
     * @param v new value
     */
    public void setVoucherId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.voucherId, v))
              {
            this.voucherId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated VoucherNo
        if (collVoucherNos != null)
        {
            for (int i = 0; i < collVoucherNos.size(); i++)
            {
                ((VoucherNo) collVoucherNos.get(i))
                    .setVoucherId(v);
            }
        }
                                }
  
    /**
     * Get the VoucherCode
     *
     * @return String
     */
    public String getVoucherCode()
    {
        return voucherCode;
    }

                        
    /**
     * Set the value of VoucherCode
     *
     * @param v new value
     */
    public void setVoucherCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.voucherCode, v))
              {
            this.voucherCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return int
     */
    public int getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(int v) 
    {
    
                  if (this.amount != v)
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collVoucherNos
     */
    protected List collVoucherNos;

    /**
     * Temporary storage of collVoucherNos to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initVoucherNos()
    {
        if (collVoucherNos == null)
        {
            collVoucherNos = new ArrayList();
        }
    }

    /**
     * Method called to associate a VoucherNo object to this object
     * through the VoucherNo foreign key attribute
     *
     * @param l VoucherNo
     * @throws TorqueException
     */
    public void addVoucherNo(VoucherNo l) throws TorqueException
    {
        getVoucherNos().add(l);
        l.setVoucher((Voucher) this);
    }

    /**
     * The criteria used to select the current contents of collVoucherNos
     */
    private Criteria lastVoucherNosCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getVoucherNos(new Criteria())
     *
     * @throws TorqueException
     */
    public List getVoucherNos() throws TorqueException
    {
              if (collVoucherNos == null)
        {
            collVoucherNos = getVoucherNos(new Criteria(10));
        }
        return collVoucherNos;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Voucher has previously
     * been saved, it will retrieve related VoucherNos from storage.
     * If this Voucher is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getVoucherNos(Criteria criteria) throws TorqueException
    {
              if (collVoucherNos == null)
        {
            if (isNew())
            {
               collVoucherNos = new ArrayList();
            }
            else
            {
                        criteria.add(VoucherNoPeer.VOUCHER_ID, getVoucherId() );
                        collVoucherNos = VoucherNoPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(VoucherNoPeer.VOUCHER_ID, getVoucherId());
                            if (!lastVoucherNosCriteria.equals(criteria))
                {
                    collVoucherNos = VoucherNoPeer.doSelect(criteria);
                }
            }
        }
        lastVoucherNosCriteria = criteria;

        return collVoucherNos;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getVoucherNos(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getVoucherNos(Connection con) throws TorqueException
    {
              if (collVoucherNos == null)
        {
            collVoucherNos = getVoucherNos(new Criteria(10), con);
        }
        return collVoucherNos;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Voucher has previously
     * been saved, it will retrieve related VoucherNos from storage.
     * If this Voucher is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getVoucherNos(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collVoucherNos == null)
        {
            if (isNew())
            {
               collVoucherNos = new ArrayList();
            }
            else
            {
                         criteria.add(VoucherNoPeer.VOUCHER_ID, getVoucherId());
                         collVoucherNos = VoucherNoPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(VoucherNoPeer.VOUCHER_ID, getVoucherId());
                             if (!lastVoucherNosCriteria.equals(criteria))
                 {
                     collVoucherNos = VoucherNoPeer.doSelect(criteria, con);
                 }
             }
         }
         lastVoucherNosCriteria = criteria;

         return collVoucherNos;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Voucher is new, it will return
     * an empty collection; or if this Voucher has previously
     * been saved, it will retrieve related VoucherNos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Voucher.
     */
    protected List getVoucherNosJoinVoucher(Criteria criteria)
        throws TorqueException
    {
                    if (collVoucherNos == null)
        {
            if (isNew())
            {
               collVoucherNos = new ArrayList();
            }
            else
            {
                              criteria.add(VoucherNoPeer.VOUCHER_ID, getVoucherId());
                              collVoucherNos = VoucherNoPeer.doSelectJoinVoucher(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(VoucherNoPeer.VOUCHER_ID, getVoucherId());
                                    if (!lastVoucherNosCriteria.equals(criteria))
            {
                collVoucherNos = VoucherNoPeer.doSelectJoinVoucher(criteria);
            }
        }
        lastVoucherNosCriteria = criteria;

        return collVoucherNos;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VoucherId");
              fieldNames.add("VoucherCode");
              fieldNames.add("Description");
              fieldNames.add("Amount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VoucherId"))
        {
                return getVoucherId();
            }
          if (name.equals("VoucherCode"))
        {
                return getVoucherCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Amount"))
        {
                return Integer.valueOf(getAmount());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VoucherPeer.VOUCHER_ID))
        {
                return getVoucherId();
            }
          if (name.equals(VoucherPeer.VOUCHER_CODE))
        {
                return getVoucherCode();
            }
          if (name.equals(VoucherPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(VoucherPeer.AMOUNT))
        {
                return Integer.valueOf(getAmount());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVoucherId();
            }
              if (pos == 1)
        {
                return getVoucherCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getAmount());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VoucherPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VoucherPeer.doInsert((Voucher) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VoucherPeer.doUpdate((Voucher) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collVoucherNos != null)
            {
                for (int i = 0; i < collVoucherNos.size(); i++)
                {
                    ((VoucherNo) collVoucherNos.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key voucherId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setVoucherId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setVoucherId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVoucherId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Voucher copy() throws TorqueException
    {
        return copyInto(new Voucher());
    }
  
    protected Voucher copyInto(Voucher copyObj) throws TorqueException
    {
          copyObj.setVoucherId(voucherId);
          copyObj.setVoucherCode(voucherCode);
          copyObj.setDescription(description);
          copyObj.setAmount(amount);
  
                    copyObj.setVoucherId((String)null);
                              
                                      
                            
        List v = getVoucherNos();
        for (int i = 0; i < v.size(); i++)
        {
            VoucherNo obj = (VoucherNo) v.get(i);
            copyObj.addVoucherNo(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VoucherPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Voucher\n");
        str.append("-------\n")
           .append("VoucherId            : ")
           .append(getVoucherId())
           .append("\n")
           .append("VoucherCode          : ")
           .append(getVoucherCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}

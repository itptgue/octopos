package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountReceivable;
import com.ssti.enterprise.pos.om.AccountReceivablePeer;
import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AccountReceivableTool.java,v 1.15 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: AccountReceivableTool.java,v $
 * 
 * 2015-10-18
 * - override getTransactionHistory add parameter in main method to query by list of id
 * - override filterAmountByType add parameter _dBefore in main method to enable filter by date before
 *   so we only need to query all transaction history once  
 * </pre><br>
 */
public class AccountReceivableTool extends BaseTool
{	
    private static Log log = LogFactory.getLog(AccountReceivableTool.class);
    
    private static int[] a_TRANS_TYPE = {
        i_AR_TRANS_SALES,     
        i_AR_TRANS_CASH_RECEIPT,     
        i_AR_TRANS_CREDIT_MEMO,             
        //i_AR_TRANS_SALES_CANCEL,             
        i_AR_TRANS_OPENING_BALANCE          
    };

    private static AccountReceivable getAccountReceivableByID (String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(AccountReceivablePeer.ACCOUNT_RECEIVABLE_ID, _sID);
		List vData = AccountReceivablePeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (AccountReceivable) vData.get(0);
		}
		return null;	
	}
    
	public static List getDataByTransID (String _sTransID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(AccountReceivablePeer.TRANSACTION_ID, _sTransID);
		return AccountReceivablePeer.doSelect (oCrit, _oConn);
	}

    public static List getDataByTransID (String _sTransID, String _sTransNo, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(AccountReceivablePeer.TRANSACTION_ID, _sTransID);
        oCrit.add(AccountReceivablePeer.TRANSACTION_NO, _sTransNo);        
        return AccountReceivablePeer.doSelect (oCrit, _oConn);
    }

    /**
     * check whether customer has AR trans
     * @param _sCustomerID
     * @return
     */
	public static boolean isTransExist(String _sCustomerID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(AccountReceivablePeer.CUSTOMER_ID, _sCustomerID);
		oCrit.setLimit(1);
		List vData = AccountReceivablePeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			return true;
		}
		return false;
	}	 	
	
	/**
	 * update customer opening balance by creating AR transaction with type OB
	 * 
	 * @param _oCustomer
	 * @param _bIsUpdate
	 * @param _sUserName
	 * @throws Exception
	 */
    public static void updateOpeningBalance (Customer _oCustomer, boolean _bIsUpdate, String _sUserName)
        throws Exception
    {       
	    Connection oConn = null;
	    try
	    {
	    	oConn = beginTrans ();    	
	    	String sDesc = "AR Opening Balance ";        
	    	if (_bIsUpdate) sDesc = "Update AR Opening Balance ";
	    	sDesc += _oCustomer.getCustomerCode();
	    	
	    	AccountReceivable oOldAR = null;
	    	AccountReceivable oAR = null;
	    	if (StringUtil.isNotEmpty(_oCustomer.getObTransId())) //if ob trans id exist then get old AR
	    	{
	    		oAR = getAccountReceivableByID(_oCustomer.getObTransId(), oConn);
	    		if (oAR != null)
	    		{
	    			oOldAR = oAR.copy();
	    			oAR.setModified(true);
	    		}
	    	}
	    	if (oAR == null)
	    	{
	    		oAR = new AccountReceivable();
	    		oAR.setAccountReceivableId (IDGenerator.generateSysID());	
		    	oAR.setTransactionId	("");
		    	oAR.setTransactionNo	("");   
		    	oAR.setCustomerId		(_oCustomer.getCustomerId());
		    	oAR.setCustomerName	    (_oCustomer.getCustomerName());
	    	}
	    	
	    	double dAmountBase = _oCustomer.getOpeningBalance().doubleValue() * _oCustomer.getObRate().doubleValue();
	    	
	    	oAR.setTransactionType  (i_AR_TRANS_OPENING_BALANCE);
	    	oAR.setCurrencyId       (_oCustomer.getDefaultCurrencyId());
	    	oAR.setCurrencyRate     (_oCustomer.getObRate());
	    	oAR.setAmount           (_oCustomer.getOpeningBalance());	    
	    	oAR.setAmountBase       (new BigDecimal(dAmountBase));
	    	oAR.setTransactionDate	(_oCustomer.getAsDate());
	    	oAR.setDueDate			(_oCustomer.getAsDate());
	    	oAR.setRemark			(sDesc);
	    	oAR.save(oConn);
	    	
	    	CustomerBalanceTool.updateBalance (oOldAR, oAR, oConn);
	    	updateOpeningBalanceGLAccount (oOldAR, oAR, _sUserName, oConn);
	    	
	    	_oCustomer.setObTransId(oAR.getAccountReceivableId());
	    	_oCustomer.save(oConn);

		    commit(oConn);    
	    }
	    catch (Exception _oEx)
	    {
	    	log.error (_oEx);	    	
	    	rollback (oConn);
	    	throw new NestableException (LocaleTool.getString("ob_failed")  + _oEx.getMessage(), _oEx);
	    }
    }

	/**
	 * create customer openingbalance journal from AR
	 * 
	 * @param _oAR
	 * @param _sUserName
	 * @param _oConn
	 * @throws Exception
	 */
	private static void updateOpeningBalanceGLAccount(AccountReceivable _oOldAR, 
													  AccountReceivable _oAR, 
													  String _sUserName, 
													  Connection _oConn) 
		throws Exception
	{		
		Account oARAccount = AccountTool.getAccountReceivable(_oAR.getCurrencyId(), _oAR.getCustomerId(), _oConn);
		Account oEquity = AccountTool.getOpeningBalanceEquity(_oConn);
		double dARAmount = oARAccount.getOpeningBalance().doubleValue();
		
		if (_oOldAR != null)
		{
			//if old ap exists then substract ap amount from old value
			dARAmount = dARAmount - _oOldAR.getAmount().doubleValue();  
		}
		//sum all current AR amount to total account ob
		//total AR account opening balance value is not used in JV
		dARAmount = dARAmount + _oAR.getAmount().doubleValue();
		
		oARAccount.setOpeningBalance(new BigDecimal(dARAmount)); 
		oARAccount.setAsDate(PreferenceTool.getStartDate()); //set default to start date		
		oARAccount.setObTransId(""); //set ob trans id to empty because 1 AR/AR acc is multiple ob trans id
		oARAccount.setObRate(_oAR.getCurrencyRate()); 
		oARAccount.save(_oConn); //will not saved again in JV
		
		//create account ob jvm pass AR as sub ledger
        JournalVoucherTool.createAccountOB(oARAccount, oEquity, _sUserName, _oAR, i_SUB_LEDGER_AR, _oConn);		
	}

	public static void createAREntryFromSales (SalesTransaction _oTR, InvoicePayment _oPmt, Connection _oConn)
		throws Exception
	{
		boolean bMulti = !CurrencyTool.getCurrencyByID(_oTR.getCurrencyId(), _oConn).getIsDefault();
		createAREntryFromSales(_oTR, _oPmt, bMulti, _oConn);
	}
	
	/**
	 * AR Subledger Journal
	 * 
	 * @param _oTR Sales Invoice
	 * @param _oPmt Invoice Payment
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createAREntryFromSales (SalesTransaction _oTR, 
											   InvoicePayment _oPmt, 
											   boolean _bMulti, 
											   Connection _oConn)
    	throws Exception
    {

		double dAmount = _oPmt.getPaymentAmount().doubleValue();		
		double dTotalTax = _oTR.getTotalTax().doubleValue();
		double dFiscalRate = _oTR.getFiscalRate().doubleValue();
		if (_bMulti) 
		{
			dAmount = dAmount - dTotalTax;
			if (dFiscalRate == 0 || dFiscalRate == 1) 
			{
				dFiscalRate = CurrencyTool.getFiscalRate(_oTR.getCurrencyId(), _oConn);
			}
		}
		double dAmountBase = dAmount * _oTR.getCurrencyRate().doubleValue();
	    
		AccountReceivable oAR = new AccountReceivable();
		oAR.setAccountReceivableId(IDGenerator.generateSysID  ());	
		oAR.setTransactionId	(_oTR.getSalesTransactionId   ());	
        oAR.setTransactionNo	(_oTR.getInvoiceNo  		  ());
        oAR.setTransactionType  (i_AR_TRANS_SALES );
	    oAR.setCustomerId		(_oTR.getCustomerId	          ());
	    oAR.setCustomerName	    (_oTR.getCustomerName		  ());
	    oAR.setAmount           (new BigDecimal(dAmount)		);
	    oAR.setAmountBase       (new BigDecimal(dAmountBase)    );
	    oAR.setCurrencyId       (_oTR.getCurrencyId           ());
	    oAR.setCurrencyRate     (_oTR.getCurrencyRate         ());
        oAR.setTransactionDate	(_oTR.getTransactionDate	  ());
        oAR.setDueDate			(_oPmt.getDueDate			  ());
        oAR.setRemark			(_oTR.getRemark 			  ());
        oAR.setLocationId		(_oTR.getLocationId			  ());
		oAR.save(_oConn);
		
		CustomerBalanceTool.updateBalance (oAR, _oConn);
		
		if (_bMulti && dTotalTax > 0) //create separate TAX AP Journal
		{
			AccountReceivable oTaxAcc = new AccountReceivable();			
			oTaxAcc.setAccountReceivableId (IDGenerator.generateSysID());	
			oTaxAcc.setTransactionId	(_oTR.getSalesTransactionId());	
		    oTaxAcc.setTransactionNo	(_oTR.getInvoiceNo());
		    oTaxAcc.setTransactionType  (i_AR_TRANS_SALES);
			oTaxAcc.setCustomerId		(_oTR.getCustomerId());
			oTaxAcc.setCustomerName	 	(_oTR.getCustomerName());
			oTaxAcc.setCurrencyId       (_oTR.getCurrencyId());
			oTaxAcc.setCurrencyRate     (new BigDecimal(dFiscalRate));	    
			oTaxAcc.setAmount           (new BigDecimal(dTotalTax));
			oTaxAcc.setAmountBase       (new BigDecimal(dTotalTax * dFiscalRate));
		    oTaxAcc.setTransactionDate	(_oTR.getTransactionDate ());
		    oTaxAcc.setDueDate			(_oTR.getDueDate());
		    oTaxAcc.setRemark			(_oTR.getRemark() + " [" + LocaleTool.getString("sales_tax") + "]");
		    oTaxAcc.setLocationId		(_oTR.getLocationId());
			oTaxAcc.save(_oConn);			
			
			CustomerBalanceTool.updateBalance (oTaxAcc, _oConn);
		}
	}

	/**
	 * Credit Memo Subledger journal 
	 * 
	 * @param _oData
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createAREntryFromCreditMemo(CreditMemo _oData, Connection _oConn)
		throws Exception
	{
		AccountReceivable oAR = new AccountReceivable ();
		oAR.setAccountReceivableId(IDGenerator.generateSysID());	
		oAR.setTransactionId	(_oData.getCreditMemoId  	());	
        oAR.setTransactionNo	(_oData.getCreditMemoNo  	());
        oAR.setTransactionType  (i_AR_TRANS_CREDIT_MEMO);
	    oAR.setCustomerId		(_oData.getCustomerId	    ());
	    oAR.setCustomerName	    (_oData.getCustomerName	    ());
	    oAR.setAmount           (_oData.getAmount		    ());
	    oAR.setAmountBase       (_oData.getAmountBase		());
	    oAR.setCurrencyId       (_oData.getCurrencyId       ());
	    oAR.setCurrencyRate     (_oData.getCurrencyRate     ());	    
        oAR.setTransactionDate	(_oData.getTransactionDate	());
        oAR.setDueDate			(_oData.getDueDate		    ());
        oAR.setRemark			(_oData.getRemark 			());
        oAR.setLocationId		(_oData.getLocationId		());
		oAR.save(_oConn);
        CustomerBalanceTool.updateBalance (oAR, _oConn);
	}

	/**
	 * AR Payment subledger journal
	 * 
	 * @param _oData
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createAREntryFromARPayment(ArPayment _oData, Connection _oConn)
		throws Exception
	{
		AccountReceivable oAR = new AccountReceivable ();
		oAR.setAccountReceivableId(IDGenerator.generateSysID());	
		oAR.setTransactionId	(_oData.getArPaymentId  ());	
        oAR.setTransactionNo	(_oData.getArPaymentNo  ());
        oAR.setTransactionType  (i_AR_TRANS_CASH_RECEIPT);
	    oAR.setCustomerId		(_oData.getCustomerId	());
	    oAR.setCustomerName	 	(_oData.getCustomerName	());
	    oAR.setCurrencyId       (_oData.getCurrencyId   ());
	    oAR.setCurrencyRate     (_oData.getCurrencyRate ());	    

	    double dAmount =  _oData.getTotalAmount().doubleValue() + _oData.getTotalDiscount().doubleValue();
	    double dAmountBase =  dAmount * _oData.getCurrencyRate().doubleValue();
	    
	    oAR.setAmount           (new BigDecimal(dAmount));
	    oAR.setAmountBase       (new BigDecimal(dAmountBase));
        oAR.setTransactionDate	(_oData.getArPaymentDate	());
        oAR.setDueDate			(_oData.getArPaymentDueDate ());
        oAR.setRemark			(_oData.getRemark 			());
        oAR.setLocationId		(_oData.getLocationId		());
		oAR.save(_oConn);
        
        CustomerBalanceTool.updateBalance (oAR, _oConn);
	}

	/**
	 * rollback existing transaction journal
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void rollbackAR(String _sTransID, Connection _oConn)
		throws Exception
	{
		List vAR = getDataByTransID (_sTransID, _oConn);
		for (int i = 0; i < vAR.size(); i++)
		{
			AccountReceivable oAR = (AccountReceivable) vAR.get(i);
			if (oAR != null)
			{
				double dAmount = oAR.getAmount().doubleValue();
				double dAmountBase = oAR.getAmountBase().doubleValue();
				oAR.setAmount(new BigDecimal (dAmount * -1));
				oAR.setAmountBase(new BigDecimal(dAmountBase * -1));
				
				CustomerBalanceTool.updateBalance(oAR, _oConn);
				
				Criteria oCrit = new Criteria();
				oCrit.add(AccountReceivablePeer.ACCOUNT_RECEIVABLE_ID, oAR.getAccountReceivableId());
				AccountReceivablePeer.doDelete(oCrit, _oConn);
			}		
		}
	}
    
    /**
     * rollback existing transaction journal
     * 
     * @param _sTransID
     * @param _sTransNo
     * @param _oConn
     * @throws Exception
     */
    public static void rollbackAR(String _sTransID, String _sTransNo, Connection _oConn)
        throws Exception
    {
        List vAR = getDataByTransID (_sTransID, _sTransNo, _oConn);
        for (int i = 0; i < vAR.size(); i++)
        {
            AccountReceivable oAR = (AccountReceivable) vAR.get(i);
            if (oAR != null)
            {
                double dAmount = oAR.getAmount().doubleValue();
                double dAmountBase = oAR.getAmountBase().doubleValue();
                oAR.setAmount(new BigDecimal (dAmount * -1));
                oAR.setAmountBase(new BigDecimal(dAmountBase * -1));
                
                CustomerBalanceTool.updateBalance(oAR, _oConn);
                
                Criteria oCrit = new Criteria();
                oCrit.add(AccountReceivablePeer.ACCOUNT_RECEIVABLE_ID, oAR.getAccountReceivableId());
                AccountReceivablePeer.doDelete(oCrit, _oConn);
            }       
        }
    }

	/////////////////////////////////////////////////////////////////////////////////////
	//report methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * get List of AR Journal Transaction
	 *  
	 * @param _dStart
	 * @param _dEnd
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionInDateRange (Date _dStart, Date _dEnd) 
    	throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(AccountReceivablePeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        oCrit.and(AccountReceivablePeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);        
		return AccountReceivablePeer.doSelect(oCrit);
	}

	/**
	 * get List of AR Journal Transaction
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _iType
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionHistory (Date _dStart, 
									          Date _dEnd,
									          String _sCustomerID,
									          int _iType) 
		throws Exception
	{
		return getTransactionHistory (_dStart, _dEnd, "", _sCustomerID, "", _iType); 
	}

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _vIDs
	 * @param _sLocationID
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionHistory (Date _dStart, 
	                                          Date _dEnd,
	                                          List _vIDs,
	                                          String _sLocationID) 
    	throws Exception
	{
		return getTransactionHistory(_dStart, _dEnd, _vIDs, "", "", _sLocationID, -1);
	}

	/**
	 * old method to get trans history
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerTypeID
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iType
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionHistory (Date _dStart, 
	                                          Date _dEnd,
	                                          String _sCustomerTypeID,
	                                          String _sCustomerID,
	                                          String _sLocationID,
	                                          int _iType) 
    	throws Exception
	{
		return getTransactionHistory(_dStart, _dEnd, null, _sCustomerTypeID, _sCustomerID, _sLocationID, _iType);
	}
	
	/**
	 * get List of AR Journal Transaction
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _vIDs List of customerId
	 * @param _sCustomerTypeID
	 * @param _sCustomerID
	 * @param _iType
	 * @return
	 * @throws Exception
	 */
	public static List getTransactionHistory (Date _dStart, 
	                                          Date _dEnd,
	                                          List _vIDs,
	                                          String _sCustomerTypeID,
	                                          String _sCustomerID,
	                                          String _sLocationID,
	                                          int _iType) 
    	throws Exception
	{
		Criteria oCrit = new Criteria();
		if (_dStart != null)
        {
            oCrit.add(AccountReceivablePeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        }
		if (_dEnd != null)
        {
            oCrit.and(AccountReceivablePeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);        
		}
		if (StringUtil.isNotEmpty(_sCustomerTypeID))
		{
            oCrit.addJoin(AccountReceivablePeer.CUSTOMER_ID, CustomerPeer.CUSTOMER_ID);
            oCrit.add(CustomerPeer.CUSTOMER_TYPE_ID, _sCustomerTypeID);
		}
		if(_vIDs != null)
		{
			oCrit.addIn(AccountReceivablePeer.CUSTOMER_ID, _vIDs);
		}
		if (StringUtil.isNotEmpty(_sCustomerID))
		{
            oCrit.add(AccountReceivablePeer.CUSTOMER_ID, _sCustomerID);        
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
            oCrit.add(AccountReceivablePeer.LOCATION_ID, _sLocationID);        
		}		
		if (_iType > 0)
		{
            oCrit.add(AccountReceivablePeer.TRANSACTION_TYPE, _iType);        
		} 
		oCrit.addAscendingOrderByColumn (AccountReceivablePeer.TRANSACTION_DATE);
		return AccountReceivablePeer.doSelect(oCrit);
	}	
	
	/**
	 * check whether this trans type add to AR or substract AR Balance
	 * 
	 * @param _iTransactionType
	 * @return
	 */
	public static final boolean isAdd (Integer _iTransactionType) 
	{
		if ((_iTransactionType.intValue() == i_AR_TRANS_SALES)  ||
		    (_iTransactionType.intValue() == i_AR_TRANS_OPENING_BALANCE)) 
        {
            return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return trans type list
	 * @throws Exception
	 */
	public static int[] getTransTypeList () 
		throws Exception
	{		
	    return a_TRANS_TYPE;
	}	
	
	/**
	 * get trans type name 
	 * 
	 * @param _iTransType
	 * @return localized transaction name
	 */
	public static final String getTransTypeName (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);
		
		if (iType == i_AR_TRANS_SALES          ) return LocaleTool.getString("sales_invoice");
		if (iType == i_AR_TRANS_CASH_RECEIPT   ) return LocaleTool.getString("receivable_payment");
		if (iType == i_AR_TRANS_CREDIT_MEMO    ) return LocaleTool.getString("credit_memo");
		if (iType == i_AR_TRANS_OPENING_BALANCE) return LocaleTool.getString("opening_balance");
		return "";
	}
	
	/**
	 * get trans screen
	 * 
	 * @param _iTransType
	 * @return
	 * @throws Exception transaction screen
	 */
	public static final String getTransactionScreen (Integer _iTransType) 
		throws Exception
	{
		int iType = Calculator.toInt(_iTransType);

		if (iType == i_AR_TRANS_SALES           ) return "transaction," + PreferenceTool.getInvoiceScreen();
		if (iType == i_AR_TRANS_CASH_RECEIPT    ) return s_SCREEN_AR_PAYMENT;
		if (iType == i_AR_TRANS_CREDIT_MEMO     ) return s_SCREEN_CREDIT_MEMO;
		if (iType == i_AR_TRANS_OPENING_BALANCE ) return s_SCREEN_JOURNAL_VOUCHER;
		return "#";
	}

	/**
	 * filter total amount
	 * 
	 * @param _vData
	 * @param _iType
	 * @return double [total amountbase, total amount]
	 * @throws Exception
	 */
	public static double[] filterTotalAmountByType(List _vData, int _iType)
    	throws Exception
    {
		return filterTotalAmountByType(_vData, null, _iType);
    }
	
	/**
	 * filter total amount
	 * 
	 * @param _vData
	 * @param _iType
	 * @return double [total amountbase, total amount]
	 * @throws Exception
	 */
	public static double[] filterTotalAmountByType(List _vData, Date _dBefore, int _iType)
    	throws Exception
    {        
        double[] dTotal = new double[2];
        for (int i = 0; i < _vData.size(); i++)
        {
            AccountReceivable oARP = (AccountReceivable)_vData.get(i);
            if (oARP != null && (_dBefore == null || _dBefore != null && DateUtil.isBefore(oARP.getTransactionDate(), _dBefore)))
            {
                //AR transaction that add value to ar Balance
                if (_iType == 1)
                {
                    if (oARP.getTransactionType () == i_AR_TRANS_SALES 
                    	|| oARP.getTransactionType () == i_AR_TRANS_OPENING_BALANCE)
                    {
                        dTotal[i_BASE] = dTotal[i_BASE] + oARP.getAmountBase().doubleValue();
                        dTotal[i_PRIME] = dTotal[i_PRIME] + oARP.getAmount().doubleValue();
                    }
                }
                //AR transaction that minus value to ar Balance
                else if (_iType == 2)
                {

                    if (oARP.getTransactionType () == i_AR_TRANS_CASH_RECEIPT 
                    	|| oARP.getTransactionType () == i_AR_TRANS_CREDIT_MEMO)  
                        //|| oARP.getTransactionType () == i_AR_TRANS_SALES_CANCEL
                    {
                        dTotal[i_BASE] = dTotal[i_BASE] + oARP.getAmountBase().doubleValue();
                        dTotal[i_PRIME] = dTotal[i_PRIME] + oARP.getAmount().doubleValue();
                    }
                }
            }
        }
        return dTotal;
	}	

	/////////////////////////////////////////////////////////////////////////////////////
	//end report methods
	/////////////////////////////////////////////////////////////////////////////////////
	public static double countTotalOwing(List _vTrans, String _sCurrencyID,Date _dAsOf, 
									     int _iMode, int _iDayFrom, int _iDayTo)
        throws Exception
	{
		return countTotalOwing(_vTrans, "", _sCurrencyID, _dAsOf, _iMode, _iDayFrom, _iDayTo);
	}
	
    /**
     * count total owing from list of transaction
     *  
     * @param _vTrans
     * @param _sCustomerID
     * @param _sCurrencyID
     * @param _dAsOf
     * @param _iMode
     * @param _iDayFrom
     * @param _iDayTo
     * @return
     * @throws Exception
     */
	public static double countTotalOwing(List _vTrans, 
										 String _sCustomerID, 
										 String _sCurrencyID, 
										 Date _dAsOf, 
	                                     int _iMode, 
	                                     int _iDayFrom, 
	                                     int _iDayTo)
		throws Exception
	{
	    double dTotal = 0;
	    double dPaid  = 0;
	    double dOwe   = 0;
	    double dTotalOwe = 0;
	    Date dAgingDate  = null;
	    
	    for (int i = 0; i < _vTrans.size(); i++)
	    {
            SalesTransaction oTrans = (SalesTransaction) _vTrans.get(i);
            
            if (oTrans.getCurrencyId().equals(_sCurrencyID) && 
            	(StringUtil.isEmpty(_sCustomerID) || 
            	(StringUtil.isNotEmpty(_sCustomerID) && StringUtil.isEqual(oTrans.getCustomerId(), _sCustomerID))) )
            {
                //aging start from transaction date 
                if (_iMode == 1)
                {
                    dAgingDate = oTrans.getTransactionDate();
                }
                //aging start from due date
                else 
                {
                    dAgingDate = oTrans.getDueDate();
                }
                
                dTotal = oTrans.getTotalAmount().doubleValue(); 
                dPaid  = oTrans.getPaidAmount().doubleValue();
                dOwe   = dTotal - dPaid;
                
                if (_iDayFrom > 0 && _iDayTo > 0)
                {
                    int iAging = DateUtil.countAging( _dAsOf, dAgingDate);                    
                    if (iAging >= _iDayFrom && iAging <= _iDayTo)
                    {                
                        dTotalOwe = dTotalOwe + dOwe;
                    }
                }
                else if (_iDayFrom < 0 && _iDayTo < 0)
                {
                    int iAging = DateUtil.countAging( _dAsOf, dAgingDate);                    
                    if (iAging == 0)
                    {                
                        dTotalOwe = dTotalOwe + dOwe;
                    }
                }                
                else if(_iDayFrom == 0 && _iDayTo == 0)
                {
                    dTotalOwe = dTotalOwe + dOwe;
                }
            }
        }
	    return dTotalOwe;
	}	
}


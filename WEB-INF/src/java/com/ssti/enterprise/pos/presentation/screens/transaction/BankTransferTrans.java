package com.ssti.enterprise.pos.presentation.screens.transaction;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankTransfer;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.BankTransferTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashManagement.java,v 1.17 2008/06/29 07:10:31 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashManagement.java,v $
 * Revision 1.17  2008/06/29 07:10:31  albert
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/03 02:01:40  albert
 * *** empty log message ***
 *
 * Revision 1.15  2007/11/09 01:13:28  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/07/20 14:34:21  albert
 * *** empty log message ***
 *
 * Revision 1.13  2007/05/08 11:55:11  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class BankTransferTrans extends TransactionSecureScreen
{
    private static final String s_VIEW_PERM    = "View Cash Management";
    private static final String s_CREATE_PERM  = "Create Cash Management";
    private static final String s_CANCEL_PERM  = "Cancel Cash Management";
    
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
    
    protected static final int i_OP_SETBANK    = 1;
    protected static final int i_OP_NEW  	   = 3;
	protected static final int i_OP_VIEW       = 4;
    protected static final int i_OP_COPY       = 6;
    
	protected static final int i_OP_SAVE       = 8;
    protected static final int i_OP_CANCEL     = 9;
    
    static final String s_MSG_EQUAL = LocaleTool.getString("cf_msg_equal");
    static final String s_MSG_BANK  = LocaleTool.getString("cf_msg_bank");
    
    protected BankTransfer oBT = null;    
    protected HttpSession oSes;
    static final String s_BT = "bt";
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        int iOp = data.getParameters().getInt("op"); 
        if (iOp == i_OP_SAVE)
        {
            return isAuthorized (data, a_CREATE_PERM);
        }
        else if (iOp == i_OP_CANCEL)
        {
            return isAuthorized (data, a_CANCEL_PERM);
        }
    	return isAuthorized (data, a_VIEW_PERM);
    }

    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);    	
    	int iOp = data.getParameters().getInt("op"); 
    	initSession (data);
    	    	
		if(b_REFRESH)
		{
			data.setMessage("Invalid Page Session");
			iOp = i_OP_NEW;
		}

		try
		{
			
			
            if (iOp == i_OP_SETBANK)
            {
                setBank (data);
            }
            else if (iOp == i_OP_NEW)
			{
				newTrans (data);
			}
			else if (iOp == i_OP_VIEW)
			{
				viewTrans (data, context);
			}            
            else if (iOp == i_OP_COPY)
            {
                copyTrans (data);
            }                        
            else if (iOp == i_OP_SAVE)
            {
                saveTrans (data, context);
            }
            else if (iOp == i_OP_CANCEL)
            {
                cancelTrans (data, context);
            }            
		}
		catch (Exception _oEx)
		{
		    _oEx.printStackTrace();
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_BT, oSes.getAttribute (s_BT));
        context.put(BankTransferTool.s_CTX, BankTransferTool.getInstance());        
    }
    
    private void initSession ( RunData data )
	{	
    	synchronized(this)
    	{
			oSes = data.getSession();
			if ( oSes.getAttribute (s_BT) == null ) 
			{
				oSes.setAttribute (s_BT, new BankTransfer());
			}
			oBT = (BankTransfer) oSes.getAttribute (s_BT);
    	}
	}

    private void setBank (RunData data) 
        throws Exception
    {        
        data.getParameters().setProperties(oBT);
        if(StringUtil.isNotEmpty(oBT.getOutBankId()) && StringUtil.isEmpty(oBT.getOutLocId()))
        {
            Bank oBank = oBT.getOutBank();
            if (oBank != null) 
            {
                oBT.setOutLocId(oBank.getLocationId());
            }
        }
        if(StringUtil.isNotEmpty(oBT.getInBankId()) && StringUtil.isEmpty(oBT.getInLocId()))
        {
            Bank oBank = oBT.getInBank();
            if (oBank != null) 
            {
                oBT.setInLocId(oBank.getLocationId());
            }
        }
    }
    
    private void newTrans ( RunData data ) 
		throws Exception
	{
    	synchronized(this)
    	{
			oBT = new BankTransfer();
			oSes.setAttribute (s_BT, oBT);
    	}
	}
    
    private void viewTrans (RunData data, Context context)
        throws Exception
    {
        String sID = data.getParameters().getString("id");
        if (StringUtil.isNotEmpty(sID)) 
        {
            try
            {
                synchronized (this)
                {
                    oBT = BankTransferTool.getHeaderByID (sID);
                    oSes.setAttribute (s_BT, oBT);
                }
            }
            catch (Exception _oEx)
            {
                throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
            }
        }
    }
    
    private void saveTrans (RunData data, Context context)
    	throws Exception
    {
        try
        {
            data.getParameters().setProperties(oBT);

            boolean bValid = false;
            String sOut = isValid(oBT.getOutBankId(), oBT.getOutAccId());
            String sIn = isValid(oBT.getInBankId(), oBT.getInAccId());

            if(sOut.equals("") && sIn.equals("")) 
            {
                BankTransferTool.saveData(oBT,null);
                data.setMessage(LocaleTool.getString("cf_save_success"));                
            }
            else
            {
                data.setMessage(sOut + " " + sIn);
            } 
            //data.getResponse().sendRedirect(data.getScreenTemplate());
        }
        catch (Exception _oEx)
        {
            data.setMessage(LocaleTool.getString("cf_save_failed") + _oEx.getMessage());
            log.error(_oEx);
        }
    }

    private void cancelTrans (RunData data, Context context)
        throws Exception
    {
        try
        {
            BankTransferTool.cancelTrans(oBT,data.getUser().getName());
            data.setMessage(LocaleTool.getString(LocaleTool.getString("cf_cancel_success")));
        }
        catch (Exception _oEx)
        {
            data.setMessage(LocaleTool.getString("cf_cancel_failed") + _oEx.getMessage());
            log.error(_oEx);
        }
    }
    
    private String isValid (String _sBankID, String _sAccID) 
    	throws Exception
    {
        Bank oBank = BankTool.getBankByID(_sBankID);

        if (oBank == null) return s_MSG_BANK;

        String sBankAccID = oBank.getAccountId();        
        if (sBankAccID.equals(_sAccID)) return s_MSG_EQUAL;
        
        Account oAcc = AccountTool.getAccountByID(_sAccID);        
        if (oAcc != null)
        {
        	if (oAcc.getAccountType() == 1) //if CASH && used by other CASH/BANK must set subsidiary
        	{
                if (AccountTool.isAccountUsedByCashBank(_sAccID))
                {
                    return LocaleTool.getString("gl_msg_cash");
                }
        	}
        	if (oAcc.getAccountType() == 2) //if AR
        	{
        		String sAR = AccountTool.getAccountReceivable(oBT.getCurrencyId(), "", null).getAccountId(); 
        		if (oAcc.getAccountType() == 2 && oAcc.getAccountId().equals(sAR))
        		{
        			return s_GL_MSG_AR_AP;
        		}
        	}
        	else if (oAcc.getAccountType() == 7) //if AP
        	{
        		String sAP = AccountTool.getAccountPayable(oBT.getCurrencyId(), "", null).getAccountId(); 
        		if (oAcc.getAccountType() == 2 && oAcc.getAccountId().equals(sAP))
        		{
        			return s_GL_MSG_AR_AP;
        		}
        	}
        	else if (oAcc.getAccountType() == 3) //if Inventory
        	{
        		if (AccountTool.isAccountUsedByInventory(oAcc.getAccountId()))
        		{
        			return s_GL_MSG_INV;
        		}
        	}        
        }
    	return "";
    } 
    
    private void copyTrans (RunData data)
        throws Exception
    {
        String sID = data.getParameters().getString("id");
        BankTransfer oBTC = BankTransferTool.getHeaderByID(sID);
        if (oBTC != null)
        {
            oBT = new BankTransfer();
            BeanUtil.setBean2BeanProperties(oBTC,oBT);
            oBT.setTransNo("");
            oBT.setBankTransferId("");
            oBT.setStatus(i_PENDING);
            oSes.setAttribute (s_BT, oBT);
        }
    }
}

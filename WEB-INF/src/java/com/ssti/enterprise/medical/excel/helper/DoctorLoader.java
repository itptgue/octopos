package com.ssti.enterprise.medical.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.medical.manager.DoctorManager;
import com.ssti.enterprise.medical.om.Doctor;
import com.ssti.enterprise.medical.tools.DoctorTool;
import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.framework.tools.StringUtil;

public class DoctorLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (DoctorLoader.class );
	
	public static final String s_START_HEADING = "Doctor Code";
	
	public DoctorLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCode = getString(_oRow, _iIdx + i); i++;
			String sName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sCode))
			{				
				Doctor oData = DoctorTool.getDoctorByCode(sCode);
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Doctor Code : " + sCode);
				
				if (oData == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					oData = new Doctor();
					oData.setDoctorId (IDGenerator.generateSysID());
					oData.setDoctorCode(sCode); 	
					bNew = true;
				}
				else 
				{
					bNew = false;
				}
				oData.setDoctorName(sName);
				oData.setDescription(""); 
				bValid = validateString("Doctor Name", oData.getDoctorName(), 100, oEmpty, oLength);

				oData.setSpecialization(getString(_oRow, _iIdx + i)); i++;
				oData.setPracticeAddress(getString(_oRow, _iIdx + i)); i++;
				oData.setHomeAddress(getString(_oRow, _iIdx + i)); i++;
				oData.setPhone1(getString(_oRow, _iIdx + i)); i++;
				oData.setPhone2(getString(_oRow, _iIdx + i)); i++;
				
				String sInt = getString(_oRow, _iIdx + i); i++;
				if (StringUtil.isNotEmpty(sInt) && StringUtil.equalsIgnoreCase(sInt, "true"))
				{
					oData.setIsInternal(true); 
				}
				String sLocCode = getString(_oRow, _iIdx + i); i++;				
				oData.setLocationId(LocationTool.getIDByCode(sLocCode)); 
				oData.setUserName(getString(_oRow, _iIdx + i)); i++;				
				oData.save();
                DoctorManager.getInstance().refreshCache(oData);
            	
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sCode,30));
						m_oNewData.append (StringUtil.left(sName,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sCode,30));
						m_oUpdated.append (StringUtil.left(sName,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sCode,30));
				    m_oRejected.append (StringUtil.left(sName,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}    
		
	}
}

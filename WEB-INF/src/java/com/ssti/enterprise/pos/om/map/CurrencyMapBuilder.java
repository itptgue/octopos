package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CurrencyMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CurrencyMapBuilder";

    /**
     * Item
     * @deprecated use CurrencyPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "currency";
    }

  
    /**
     * currency.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use CurrencyPeer.currency.CURRENCY_ID constant
     */
    public static String getCurrency_CurrencyId()
    {
        return "currency.CURRENCY_ID";
    }
  
    /**
     * currency.CURRENCY_CODE
     * @return the column name for the CURRENCY_CODE field
     * @deprecated use CurrencyPeer.currency.CURRENCY_CODE constant
     */
    public static String getCurrency_CurrencyCode()
    {
        return "currency.CURRENCY_CODE";
    }
  
    /**
     * currency.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use CurrencyPeer.currency.DESCRIPTION constant
     */
    public static String getCurrency_Description()
    {
        return "currency.DESCRIPTION";
    }
  
    /**
     * currency.SYMBOL
     * @return the column name for the SYMBOL field
     * @deprecated use CurrencyPeer.currency.SYMBOL constant
     */
    public static String getCurrency_Symbol()
    {
        return "currency.SYMBOL";
    }
  
    /**
     * currency.IS_ACTIVE
     * @return the column name for the IS_ACTIVE field
     * @deprecated use CurrencyPeer.currency.IS_ACTIVE constant
     */
    public static String getCurrency_IsActive()
    {
        return "currency.IS_ACTIVE";
    }
  
    /**
     * currency.IS_DEFAULT
     * @return the column name for the IS_DEFAULT field
     * @deprecated use CurrencyPeer.currency.IS_DEFAULT constant
     */
    public static String getCurrency_IsDefault()
    {
        return "currency.IS_DEFAULT";
    }
  
    /**
     * currency.AP_ACCOUNT
     * @return the column name for the AP_ACCOUNT field
     * @deprecated use CurrencyPeer.currency.AP_ACCOUNT constant
     */
    public static String getCurrency_ApAccount()
    {
        return "currency.AP_ACCOUNT";
    }
  
    /**
     * currency.AR_ACCOUNT
     * @return the column name for the AR_ACCOUNT field
     * @deprecated use CurrencyPeer.currency.AR_ACCOUNT constant
     */
    public static String getCurrency_ArAccount()
    {
        return "currency.AR_ACCOUNT";
    }
  
    /**
     * currency.SD_ACCOUNT
     * @return the column name for the SD_ACCOUNT field
     * @deprecated use CurrencyPeer.currency.SD_ACCOUNT constant
     */
    public static String getCurrency_SdAccount()
    {
        return "currency.SD_ACCOUNT";
    }
  
    /**
     * currency.RG_ACCOUNT
     * @return the column name for the RG_ACCOUNT field
     * @deprecated use CurrencyPeer.currency.RG_ACCOUNT constant
     */
    public static String getCurrency_RgAccount()
    {
        return "currency.RG_ACCOUNT";
    }
  
    /**
     * currency.UG_ACCOUNT
     * @return the column name for the UG_ACCOUNT field
     * @deprecated use CurrencyPeer.currency.UG_ACCOUNT constant
     */
    public static String getCurrency_UgAccount()
    {
        return "currency.UG_ACCOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("currency");
        TableMap tMap = dbMap.getTable("currency");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("currency.CURRENCY_ID", "");
                          tMap.addColumn("currency.CURRENCY_CODE", "");
                          tMap.addColumn("currency.DESCRIPTION", "");
                          tMap.addColumn("currency.SYMBOL", "");
                          tMap.addColumn("currency.IS_ACTIVE", Boolean.TRUE);
                          tMap.addColumn("currency.IS_DEFAULT", Boolean.TRUE);
                          tMap.addColumn("currency.AP_ACCOUNT", "");
                          tMap.addColumn("currency.AR_ACCOUNT", "");
                          tMap.addColumn("currency.SD_ACCOUNT", "");
                          tMap.addColumn("currency.RG_ACCOUNT", "");
                          tMap.addColumn("currency.UG_ACCOUNT", "");
          }
}

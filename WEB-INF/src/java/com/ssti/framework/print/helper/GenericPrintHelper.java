package com.ssti.framework.print.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;

import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * For General Text Printing i.e Reports
 * <br>
 * 
 * @author  $Author: albert $ <br>
 * @version $Id: GenericPrintHelper.java,v 1.2 2008/03/04 02:16:35 albert Exp $ <br>
 *
 * <pre>
 * $Log: GenericPrintHelper.java,v $
 * Revision 1.2  2008/03/04 02:16:35  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class GenericPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper
{   
	static final Log log = LogFactory.getLog(GenericPrintHelper.class);
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {   
    	checkTemplate (oParam);
    	
    	if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) 
    	{		
    		Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );    	
    	}
    	return oWriter.toString();
    }
   	
   	/**
	 * get Printed Text (use detail per page)
	 */
	 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
    	return getPrintedText(oParam, _iPrintType, _oSession);
    }
}

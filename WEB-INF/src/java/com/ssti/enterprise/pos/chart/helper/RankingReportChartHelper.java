package com.ssti.enterprise.pos.chart.helper;

import java.awt.GradientPaint;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Pie3DPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.DefaultCategoryDataset;
import org.jfree.data.DefaultPieDataset;
import org.jfree.text.TextBlockAnchor;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;
import org.jfree.util.Rotation;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseAnalysisTool;
import com.ssti.enterprise.pos.tools.report.InventoryReportTool;
import com.ssti.enterprise.pos.tools.sales.SalesAnalysisTool;
import com.ssti.framework.chart.helper.BaseChartHelper;
import com.ssti.framework.chart.helper.ChartHelper;
import com.ssti.framework.tools.CustomParser;

public class RankingReportChartHelper 
	extends BaseChartHelper implements ChartHelper 
{    
	Log log = LogFactory.getLog(RankingReportChartHelper.class);
	
    private JFreeChart chart;
    private int iName      = 1;	//number of array that will use to show on legend chart
    private int iValue     = 2;	//number of array that will use to show on chart value
    private int iType      = 1;
    private int iWidth     = i_WIDTH;
    private int iHeight    = i_HEIGHT;
    private int iLimit     = 10;
    private int iItemType  = 1; //use by item, inventory_movement
    private int iOrderBy   = 1; //use by inventory_movement
    
    private int iRankBy = 1; //iType new variable name
    private int iRankType = 1; //iOrderBy new variable name
    
    private boolean bLegend  = true;
    
    private String sBGColor1 = "white";
    private String sBGColor2 = "orange";
    
    private String sTitle = "Top ";
    private String sKategori = "";
    private String sCTypeID = "";
    private String sCustID = "";    
    private String sLocationID = "";
    private String sKategoriID = "";
    private String sCashier = "";
    private String sSalesID = "";
    
    private String sName = "";
    
    public String getChartFileName (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] aStartDate  = (String[]) oParam.get("StartDate");
		String[] aEndDate	 = (String[]) oParam.get("EndDate");
		String[] aWidth		 = (String[]) oParam.get("Width");
		String[] aHeight	 = (String[]) oParam.get("Height");
		String[] aType	 	 = (String[]) oParam.get("Type");
		String[] aKategori	 = (String[]) oParam.get("Kategori");
		String[] aLimit	 	 = (String[]) oParam.get("Limit");
		String[] aLegend	 = (String[]) oParam.get("Legend");
		String[] aBGColor1	 = (String[]) oParam.get("BGColor1");
		String[] aBGColor2	 = (String[]) oParam.get("BGColor2");
		String[] aItemType	 = (String[]) oParam.get("ItemType");
		String[] aOrderBy	 = (String[]) oParam.get("OrderBy");
		
		String[] aCustID	 = (String[]) oParam.get("CustomerId");		
		String[] aCTypeID	 = (String[]) oParam.get("CustomerTypeId");
        String[] aLocationID = (String[]) oParam.get("LocationID");
        String[] aKategoriID = (String[]) oParam.get("KategoriID");
        String[] aCashier 	 = (String[]) oParam.get("Cashier");
        String[] aSalesID 	 = (String[]) oParam.get("SalesID");

        String[] aRankBy 	 = (String[]) oParam.get("RankBy");
        String[] aRankType 	 = (String[]) oParam.get("RankType");
        
		Date dStart    = CustomParser.parseDate (aStartDate[0]);
		Date dEnd 	   = CustomParser.parseDate (aEndDate[0]);

		if (aType     != null) iType 	 = Integer.parseInt (aType[0]);
		if (aWidth    != null) iWidth    = Integer.parseInt (aWidth[0]);
		if (aHeight   != null) iHeight   = Integer.parseInt (aHeight[0]);
		if (aLimit    != null) iLimit    = Integer.parseInt (aLimit[0]);
		if (aItemType != null) iItemType = Integer.parseInt (aItemType[0]);
		if (aLegend   != null) bLegend   = Boolean.valueOf  (aLegend[0]).booleanValue();
		if (aBGColor1 != null) sBGColor1 = aBGColor1[0];
		if (aBGColor2 != null) sBGColor2 = aBGColor2[0];
		if (aOrderBy  != null) iOrderBy  = Integer.parseInt (aOrderBy[0]);
		if (aCTypeID  != null) sCTypeID  = aCTypeID [0];

		if (aLocationID  != null && aLocationID.length > 0) sLocationID  = aLocationID [0];
		if (aKategoriID  != null && aKategoriID.length > 0) sKategoriID  = aKategoriID [0];

		if (aCustID  != null && aCustID.length > 0) sCustID  = aCustID [0];
		if (aCTypeID != null && aCTypeID.length > 0) sCTypeID  = aCTypeID [0];
		
		if (aCashier  != null && aCashier.length > 0) sCashier  = aCashier [0];
		if (aSalesID  != null && aSalesID.length > 0) sSalesID  = aSalesID [0];

		if (aRankBy  != null && aRankBy.length > 0) iRankBy  = Integer.parseInt(aRankBy [0]);
		if (aRankType  != null && aRankType.length > 0) iRankType  = Integer.parseInt(aRankType [0]);			
		
		List vData = null;
		sKategori = aKategori[0];
		
		//SALES
		if (sKategori.equals("Kategori")) 
		{
			iValue = iRankBy; iType = 4;
			vData = SalesAnalysisTool.getRanking(true, "k.kategori_id", dStart, dEnd, iLimit, 
				iRankBy, iRankType, sLocationID,  sKategoriID, sCustID, sCTypeID, sCashier, sSalesID);
		}   
		else if (sKategori.equals("Item")) 
		{			
			iValue = iRankBy; iType = 4;
			vData = SalesAnalysisTool.getRanking(false, "sd.item_id", dStart, dEnd, iLimit, 
				iRankBy, iRankType, sLocationID, sKategoriID, sCustID, sCTypeID, sCashier, sSalesID);			
		}
		else if (sKategori.equals("Customer")) 
		{
			iValue = iRankBy; iType = 4;
			vData = SalesAnalysisTool.getRanking(false, "s.customer_id", dStart, dEnd, iLimit, 
				iRankBy, iRankType, sLocationID, sKategoriID, sCustID, sCTypeID, sCashier, sSalesID);
		}
		else if (sKategori.equals("Cashier")) 
		{
			iValue = iRankBy; iType = 4;	
			vData = SalesAnalysisTool.getRanking(false, "s.cashier_name", dStart, dEnd, iLimit, 
				iRankBy, iRankType, sLocationID, sKategoriID, sCustID, sCTypeID, sCashier, sSalesID);
		}
		else if (sKategori.equals("Salesman")) 
		{
			iValue = iRankBy; iType = 4;
			vData = SalesAnalysisTool.getRanking(false, "s.sales_id", dStart, dEnd, iLimit, 
				iRankBy, iRankType, sLocationID, sKategoriID, sCustID, sCTypeID, sCashier, sSalesID);
		}
		else if (sKategori.equals("SalesDetail")) 
		{
			iValue = iRankBy; iType = 4;
			vData = SalesAnalysisTool.getRanking(false, "sd.employee_id", dStart, dEnd, iLimit, 
				iRankBy, iRankType, sLocationID, sKategoriID, sCustID, sCTypeID, sCashier, sSalesID);
		}
		
		//PURCHASE
		else if (sKategori.equals("ItemOrdered")) {
			vData = PurchaseAnalysisTool.getItemRankingAnalysis (dStart, dEnd, iLimit, iType);
			iName = 2;
			if (iType == 1) iValue = 3;
            if (iType == 2) iValue = 4;
			iType = 4;
		}
		else if (sKategori.equals("ItemReceived")) {
			vData = PurchaseAnalysisTool.getItemReceivedAnalysis(dStart, dEnd, iLimit);
			iName = 2;
			iValue = 3;
			iType = 4;
		}
		else if (sKategori.equals("Vendor")) {
			vData = PurchaseAnalysisTool.getVendorRankingAnalysis(dStart, dEnd, iLimit, iType);
			iName = 2;
			if (iType == 1) iValue = 3;
            if (iType == 2) iValue = 4;
			iType = 4;
		}
		else if (sKategori.equals("InventoryMovement")) {
			vData = InventoryReportTool.getTopMovementRanking(dStart, dEnd, iLimit, iItemType, iOrderBy, sLocationID);	
			iName = 2;
			if (iOrderBy == 2) iValue = 3; else iValue = 4;
			
		}
    	else if (sKategori.equals("InventoryTurnover")) {
			vData = InventoryReportTool.getTopTurnoverRanking(dStart, dEnd, iLimit, iItemType, iOrderBy, sLocationID);	
			iName = 2;
			if (iOrderBy == 2) iValue = 3; else iValue = 4;
			
		}

		sTitle = sTitle + " " + (Integer.valueOf(iLimit)).toString() + " " + sKategori;

		if (iType == 1) {
			create3DPieChart(vData);
		}
		if (iType == 2) {
			createPieChart(vData);
		}		
		if (iType == 3) {
			createVerticalBarChart(vData, sKategori);
		}		
		if (iType == 4) {
			create3DVerticalBarChart(vData, sKategori);
		}
        return ServletUtilities.saveChartAsPNG(chart, iWidth, iHeight, _oSession);
    }

	private DefaultPieDataset createPieDS (List _vData) 
		throws Exception
	{
		DefaultPieDataset data = new DefaultPieDataset();		
		if (_vData != null)
		{
			for (int i = 0; i < _vData.size(); i++)
			{
	    		List oData = (List) _vData.get(i);
	    		String sName = (String) oData.get(iName);
	    		BigDecimal oValue = (BigDecimal) oData.get(iValue);
	    		
	    		if (sKategori.equals("Kategori"))
	    		{
	    			sName = KategoriTool.getLongDescription ((String)oData.get(0));
				}
	    		else if (sKategori.equals("Item") || sKategori.equals("InventoryMovement"))
	    		{
	    			sName = (String)oData.get(1) +  " "  + (String)oData.get(2);
				}
				
				data.setValue(sName, oValue);        		
			}
		}
		return data;
	}

	private DefaultCategoryDataset createCategoryDS (List _vData) 
		throws Exception
	{
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		if (_vData != null)
		{
			for (int i = 0; i < _vData.size(); i++)
			{
	    		List oData = (List) _vData.get(i);
	    		BigDecimal oValue = (BigDecimal) oData.get(iValue);
	    		
	    		if (sKategori.equals("Kategori"))
	    		{
	    			sName = KategoriTool.getLongDescription ((String)oData.get(0));
				}     	
	    		else if (sKategori.equals("Item"))
	    		{
	    			Item oItem = ItemTool.getItemByID((String)oData.get(0));
	    			sName = oItem.getItemCode() +  " "  + oItem.getItemName();
				}
	    		else if (sKategori.equals("Customer"))
	    		{
	    			Customer oCust = CustomerTool.getCustomerByID((String)oData.get(0));
	    			sName = oCust.getCustomerName();
				}
	    		else if (sKategori.equals("Cashier"))
	    		{
	    			sName = (String)oData.get(0);
				}
	    		else if (sKategori.equals("Salesman"))
	    		{
	    			Employee oEmp = EmployeeTool.getEmployeeByID((String)oData.get(0));
	    			if (oEmp != null) sName = oEmp.getEmployeeName(); else sName = "";
				}
	    		else if (sKategori.equals("SalesDetail"))
	    		{
	    			Employee oEmp = EmployeeTool.getEmployeeByID((String)oData.get(0));
	    			if (oEmp != null) sName = oEmp.getEmployeeName(); else sName = "";
				}
	    		
	    		else if (sKategori.equals("InventoryMovement"))
	    		{
	    			sName = (String)oData.get(1) +  " "  + (String)oData.get(2);
				}
	    		else
	    		{
	    			sName = (String) oData.get(iName);
	    		}
	    		//log.debug("Name : " + sName + " Value : " + oValue);
        		dataset.addValue(oValue, sName, "");
			}
		}
		return dataset;
	}

    private void create3DPieChart (List _vData)
    	throws Exception
    {
		
        chart = ChartFactory.createPieChart3D(sTitle,  // chart title
            								  createPieDS (_vData),   // data
            								  bLegend,    // include legend
            								  true,
            								  false);
            								  
        chart.setBackgroundPaint(new GradientPaint(0, 0, getColorByCode(sBGColor1), 1000, 0, getColorByCode(sBGColor2)));
        Pie3DPlot plot = (Pie3DPlot) chart.getPlot();
        plot.setSectionLabelType(PiePlot.NAME_AND_PERCENT_LABELS);
        plot.setStartAngle(270);
        plot.setDirection(Rotation.ANTICLOCKWISE);
        plot.setForegroundAlpha(0.60f);
        plot.setInteriorGap(0.33);
        plot.setNoDataMessage("No data available");    
    }

    private void createPieChart (List _vData)
		throws Exception
    {
        chart = ChartFactory.createPieChart(sTitle,  // chart title
            								createPieDS (_vData),    // data
            								bLegend,    // include legend
            								true,
            								false);
            								  
        chart.setBackgroundPaint(new GradientPaint(0, 0, getColorByCode(sBGColor1), 1000, 0, getColorByCode(sBGColor2)));
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setSectionLabelType(PiePlot.NAME_AND_PERCENT_LABELS);
        plot.setStartAngle(270);
        plot.setDirection(Rotation.ANTICLOCKWISE);
        plot.setForegroundAlpha(0.60f);
        plot.setInteriorGap(0.33);
        plot.setNoDataMessage("No data available");    
    }

    private void createVerticalBarChart (List _vData, String _sKategori)
		throws Exception
    {

        chart = ChartFactory.createBarChart(
            sTitle,                   // chart title
            _sKategori,               // domain axis label
            "Amount",                 // range axis label
            createCategoryDS(_vData), // data
            PlotOrientation.VERTICAL, // orientation
            bLegend,           		  // include legend
            true,
            false
        );
            								  
        chart.setBackgroundPaint(new GradientPaint(0, 0, getColorByCode(sBGColor1), 1000, 0, getColorByCode(sBGColor2)));
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setMaxCategoryLabelWidthRatio(10.0f);
        //NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        //rangeAxis.setRange(0.0, 100.0);
        //rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    } 
 
    private void create3DVerticalBarChart (List _vData, String _sKategori)
		throws Exception
    {
        chart = ChartFactory.createBarChart3D(
            sTitle,          		   // chart title
            _sKategori,                // domain axis label
            "Amount",                  // range axis label
            createCategoryDS(_vData),  // data
            PlotOrientation.VERTICAL,  // orientation
            bLegend,                      // include legend
            true,                      // tooltips
            false                      // urls
        );

		chart.setBackgroundPaint(new GradientPaint(0, 0, getColorByCode(sBGColor1), 1000, 0, getColorByCode(sBGColor2)));
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setForegroundAlpha(1.0f);
        CategoryAxis axis = plot.getDomainAxis();
        CategoryLabelPositions p = axis.getCategoryLabelPositions();
        CategoryLabelPosition left = new CategoryLabelPosition( 
            RectangleAnchor.LEFT, TextBlockAnchor.CENTER_LEFT, TextAnchor.CENTER_LEFT, 0.0
        );
        axis.setCategoryLabelPositions(CategoryLabelPositions.replaceLeftPosition(p, left));
        axis.setMaxCategoryLabelWidthRatio(3.0f);
    } 
}
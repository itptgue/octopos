package com.ssti.enterprise.pos.tools.pref;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/pref/PrintingPreference.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PrintingPreference.java,v 1.1 2008/06/29 07:17:52 albert Exp $
 *
 * $Log: PrintingPreference.java,v $
 * Revision 1.1  2008/06/29 07:17:52  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/03/05 16:10:10  albert
 *
 */

public class PrintingPreference implements Serializable
{
	private static final long serialVersionUID = 2L;
	
	//Print Detail Line
	String salesOrder;
	String deliveryOrder;
	String salesInvoice;
	String salesReturn;
	String purchaseRequest;
	String purchaseOrder;
	String purchaseReceipt;
	String purchaseInvoice;
	String issueReceipt;
	String itemTransfer;
	String cycleCount;
	String debitMemo;
	String creditMemo;
	String arPayment;
	String apPayment;
	String cashFlow;
	String pettyCash;
	String journalVoucher;
	String fixedAsset;
	String jobCosting;
	String packingList;
	String openingBalance;
	String pointReward;
	String taxInvoice; 
	String costAdjustment;
	String taxSerial;
	public String getApPayment() {
		return apPayment;
	}
	public void setApPayment(String apPayment) {
		this.apPayment = apPayment;
	}
	public String getArPayment() {
		return arPayment;
	}
	public void setArPayment(String arPayment) {
		this.arPayment = arPayment;
	}
	public String getCashFlow() {
		return cashFlow;
	}
	public void setCashFlow(String cashFlow) {
		this.cashFlow = cashFlow;
	}
	public String getCostAdjustment() {
		return costAdjustment;
	}
	public void setCostAdjustment(String costAdjustment) {
		this.costAdjustment = costAdjustment;
	}
	public String getCreditMemo() {
		return creditMemo;
	}
	public void setCreditMemo(String creditMemo) {
		this.creditMemo = creditMemo;
	}
	public String getCycleCount() {
		return cycleCount;
	}
	public void setCycleCount(String cycleCount) {
		this.cycleCount = cycleCount;
	}
	public String getDebitMemo() {
		return debitMemo;
	}
	public void setDebitMemo(String debitMemo) {
		this.debitMemo = debitMemo;
	}
	public String getDeliveryOrder() {
		return deliveryOrder;
	}
	public void setDeliveryOrder(String deliveryOrder) {
		this.deliveryOrder = deliveryOrder;
	}
	public String getFixedAsset() {
		return fixedAsset;
	}
	public void setFixedAsset(String fixedAsset) {
		this.fixedAsset = fixedAsset;
	}
	public String getIssueReceipt() {
		return issueReceipt;
	}
	public void setIssueReceipt(String issueReceipt) {
		this.issueReceipt = issueReceipt;
	}
	public String getItemTransfer() {
		return itemTransfer;
	}
	public void setItemTransfer(String itemTransfer) {
		this.itemTransfer = itemTransfer;
	}
	public String getJobCosting() {
		return jobCosting;
	}
	public void setJobCosting(String jobCosting) {
		this.jobCosting = jobCosting;
	}
	public String getJournalVoucher() {
		return journalVoucher;
	}
	public void setJournalVoucher(String journalVoucher) {
		this.journalVoucher = journalVoucher;
	}
	public String getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(String openingBalance) {
		this.openingBalance = openingBalance;
	}
	public String getPackingList() {
		return packingList;
	}
	public void setPackingList(String packingList) {
		this.packingList = packingList;
	}
	public String getPettyCash() {
		return pettyCash;
	}
	public void setPettyCash(String pettyCash) {
		this.pettyCash = pettyCash;
	}
	public String getPointReward() {
		return pointReward;
	}
	public void setPointReward(String pointReward) {
		this.pointReward = pointReward;
	}
	public String getPurchaseInvoice() {
		return purchaseInvoice;
	}
	public void setPurchaseInvoice(String purchaseInvoice) {
		this.purchaseInvoice = purchaseInvoice;
	}
	public String getPurchaseOrder() {
		return purchaseOrder;
	}
	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	public String getPurchaseReceipt() {
		return purchaseReceipt;
	}
	public void setPurchaseReceipt(String purchaseReceipt) {
		this.purchaseReceipt = purchaseReceipt;
	}
	public String getPurchaseRequest() {
		return purchaseRequest;
	}
	public void setPurchaseRequest(String purchaseRequest) {
		this.purchaseRequest = purchaseRequest;
	}
	public String getSalesInvoice() {
		return salesInvoice;
	}
	public void setSalesInvoice(String salesInvoice) {
		this.salesInvoice = salesInvoice;
	}
	public String getSalesOrder() {
		return salesOrder;
	}
	public void setSalesOrder(String salesOrder) {
		this.salesOrder = salesOrder;
	}
	public String getSalesReturn() {
		return salesReturn;
	}
	public void setSalesReturn(String salesReturn) {
		this.salesReturn = salesReturn;
	}
	public String getTaxInvoice() {
		return taxInvoice;
	}
	public void setTaxInvoice(String taxInvoice) {
		this.taxInvoice = taxInvoice;
	}
	public String getTaxSerial() {
		return taxSerial;
	}
	public void setTaxSerial(String taxSerial) {
		this.taxSerial = taxSerial;
	}
	
	public static List getFieldNames() 
	{
		List vData = new ArrayList();
		try
		{
			Class cData = PrintingPreference.class;
			Field[] aFields = cData.getDeclaredFields();

			for (int i = 0; i < aFields.length; i++)
			{
				Field oField = aFields[i];
				if (!oField.getName().equals("serialVersionUID"))
				{
					vData.add(oField.getName());
				}
		    }
		}
		catch (Exception _oEx)
		{
		}
		return vData;
	}	
}

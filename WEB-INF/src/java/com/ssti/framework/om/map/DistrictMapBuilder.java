package com.ssti.framework.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DistrictMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.DistrictMapBuilder";

    /**
     * Item
     * @deprecated use DistrictPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "district";
    }

  
    /**
     * district.DISTRICT_ID
     * @return the column name for the DISTRICT_ID field
     * @deprecated use DistrictPeer.district.DISTRICT_ID constant
     */
    public static String getDistrict_DistrictId()
    {
        return "district.DISTRICT_ID";
    }
  
    /**
     * district.REGENCY_ID
     * @return the column name for the REGENCY_ID field
     * @deprecated use DistrictPeer.district.REGENCY_ID constant
     */
    public static String getDistrict_RegencyId()
    {
        return "district.REGENCY_ID";
    }
  
    /**
     * district.DISTRICT
     * @return the column name for the DISTRICT field
     * @deprecated use DistrictPeer.district.DISTRICT constant
     */
    public static String getDistrict_District()
    {
        return "district.DISTRICT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("district");
        TableMap tMap = dbMap.getTable("district");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("district.DISTRICT_ID", "");
                          tMap.addForeignKey(
                "district.REGENCY_ID", "" , "regency" ,
                "regency_id");
                          tMap.addColumn("district.DISTRICT", "");
          }
}

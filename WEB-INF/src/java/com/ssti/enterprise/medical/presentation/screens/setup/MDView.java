package com.ssti.enterprise.medical.presentation.screens.setup;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.DoctorTool;

public class MDView extends Default 
{
	protected static final Log log = LogFactory.getLog(MDView.class);
	
	@Override
	public void doBuildTemplate(RunData data, Context context) 
	{		
		try 
		{
			super.doBuildTemplate(data, context);
			context.put("Doctors", DoctorTool.getAllDoctor());			
		} 
		catch (Exception e) 
		{
		}
	}
}

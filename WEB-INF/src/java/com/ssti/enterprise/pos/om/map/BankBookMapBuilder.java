package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BankBookMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BankBookMapBuilder";

    /**
     * Item
     * @deprecated use BankBookPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "bank_book";
    }

  
    /**
     * bank_book.BANK_BOOK_ID
     * @return the column name for the BANK_BOOK_ID field
     * @deprecated use BankBookPeer.bank_book.BANK_BOOK_ID constant
     */
    public static String getBankBook_BankBookId()
    {
        return "bank_book.BANK_BOOK_ID";
    }
  
    /**
     * bank_book.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use BankBookPeer.bank_book.BANK_ID constant
     */
    public static String getBankBook_BankId()
    {
        return "bank_book.BANK_ID";
    }
  
    /**
     * bank_book.BANK_NAME
     * @return the column name for the BANK_NAME field
     * @deprecated use BankBookPeer.bank_book.BANK_NAME constant
     */
    public static String getBankBook_BankName()
    {
        return "bank_book.BANK_NAME";
    }
  
    /**
     * bank_book.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use BankBookPeer.bank_book.CURRENCY_ID constant
     */
    public static String getBankBook_CurrencyId()
    {
        return "bank_book.CURRENCY_ID";
    }
  
    /**
     * bank_book.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use BankBookPeer.bank_book.OPENING_BALANCE constant
     */
    public static String getBankBook_OpeningBalance()
    {
        return "bank_book.OPENING_BALANCE";
    }
  
    /**
     * bank_book.ACCOUNT_BALANCE
     * @return the column name for the ACCOUNT_BALANCE field
     * @deprecated use BankBookPeer.bank_book.ACCOUNT_BALANCE constant
     */
    public static String getBankBook_AccountBalance()
    {
        return "bank_book.ACCOUNT_BALANCE";
    }
  
    /**
     * bank_book.BASE_BALANCE
     * @return the column name for the BASE_BALANCE field
     * @deprecated use BankBookPeer.bank_book.BASE_BALANCE constant
     */
    public static String getBankBook_BaseBalance()
    {
        return "bank_book.BASE_BALANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("bank_book");
        TableMap tMap = dbMap.getTable("bank_book");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("bank_book.BANK_BOOK_ID", "");
                          tMap.addColumn("bank_book.BANK_ID", "");
                          tMap.addColumn("bank_book.BANK_NAME", "");
                          tMap.addColumn("bank_book.CURRENCY_ID", "");
                            tMap.addColumn("bank_book.OPENING_BALANCE", bd_ZERO);
                            tMap.addColumn("bank_book.ACCOUNT_BALANCE", bd_ZERO);
                            tMap.addColumn("bank_book.BASE_BALANCE", bd_ZERO);
          }
}

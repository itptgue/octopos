package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class GlTransactionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.GlTransactionMapBuilder";

    /**
     * Item
     * @deprecated use GlTransactionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "gl_transaction";
    }

  
    /**
     * gl_transaction.GL_TRANSACTION_ID
     * @return the column name for the GL_TRANSACTION_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.GL_TRANSACTION_ID constant
     */
    public static String getGlTransaction_GlTransactionId()
    {
        return "gl_transaction.GL_TRANSACTION_ID";
    }
  
    /**
     * gl_transaction.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use GlTransactionPeer.gl_transaction.TRANSACTION_TYPE constant
     */
    public static String getGlTransaction_TransactionType()
    {
        return "gl_transaction.TRANSACTION_TYPE";
    }
  
    /**
     * gl_transaction.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.TRANSACTION_ID constant
     */
    public static String getGlTransaction_TransactionId()
    {
        return "gl_transaction.TRANSACTION_ID";
    }
  
    /**
     * gl_transaction.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use GlTransactionPeer.gl_transaction.TRANSACTION_NO constant
     */
    public static String getGlTransaction_TransactionNo()
    {
        return "gl_transaction.TRANSACTION_NO";
    }
  
    /**
     * gl_transaction.GL_TRANSACTION_NO
     * @return the column name for the GL_TRANSACTION_NO field
     * @deprecated use GlTransactionPeer.gl_transaction.GL_TRANSACTION_NO constant
     */
    public static String getGlTransaction_GlTransactionNo()
    {
        return "gl_transaction.GL_TRANSACTION_NO";
    }
  
    /**
     * gl_transaction.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use GlTransactionPeer.gl_transaction.TRANSACTION_DATE constant
     */
    public static String getGlTransaction_TransactionDate()
    {
        return "gl_transaction.TRANSACTION_DATE";
    }
  
    /**
     * gl_transaction.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.PROJECT_ID constant
     */
    public static String getGlTransaction_ProjectId()
    {
        return "gl_transaction.PROJECT_ID";
    }
  
    /**
     * gl_transaction.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.DEPARTMENT_ID constant
     */
    public static String getGlTransaction_DepartmentId()
    {
        return "gl_transaction.DEPARTMENT_ID";
    }
  
    /**
     * gl_transaction.PERIOD_ID
     * @return the column name for the PERIOD_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.PERIOD_ID constant
     */
    public static String getGlTransaction_PeriodId()
    {
        return "gl_transaction.PERIOD_ID";
    }
  
    /**
     * gl_transaction.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.ACCOUNT_ID constant
     */
    public static String getGlTransaction_AccountId()
    {
        return "gl_transaction.ACCOUNT_ID";
    }
  
    /**
     * gl_transaction.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.CURRENCY_ID constant
     */
    public static String getGlTransaction_CurrencyId()
    {
        return "gl_transaction.CURRENCY_ID";
    }
  
    /**
     * gl_transaction.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use GlTransactionPeer.gl_transaction.CURRENCY_RATE constant
     */
    public static String getGlTransaction_CurrencyRate()
    {
        return "gl_transaction.CURRENCY_RATE";
    }
  
    /**
     * gl_transaction.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use GlTransactionPeer.gl_transaction.AMOUNT constant
     */
    public static String getGlTransaction_Amount()
    {
        return "gl_transaction.AMOUNT";
    }
  
    /**
     * gl_transaction.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use GlTransactionPeer.gl_transaction.AMOUNT_BASE constant
     */
    public static String getGlTransaction_AmountBase()
    {
        return "gl_transaction.AMOUNT_BASE";
    }
  
    /**
     * gl_transaction.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use GlTransactionPeer.gl_transaction.DESCRIPTION constant
     */
    public static String getGlTransaction_Description()
    {
        return "gl_transaction.DESCRIPTION";
    }
  
    /**
     * gl_transaction.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use GlTransactionPeer.gl_transaction.USER_NAME constant
     */
    public static String getGlTransaction_UserName()
    {
        return "gl_transaction.USER_NAME";
    }
  
    /**
     * gl_transaction.DEBIT_CREDIT
     * @return the column name for the DEBIT_CREDIT field
     * @deprecated use GlTransactionPeer.gl_transaction.DEBIT_CREDIT constant
     */
    public static String getGlTransaction_DebitCredit()
    {
        return "gl_transaction.DEBIT_CREDIT";
    }
  
    /**
     * gl_transaction.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use GlTransactionPeer.gl_transaction.CREATE_DATE constant
     */
    public static String getGlTransaction_CreateDate()
    {
        return "gl_transaction.CREATE_DATE";
    }
  
    /**
     * gl_transaction.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.LOCATION_ID constant
     */
    public static String getGlTransaction_LocationId()
    {
        return "gl_transaction.LOCATION_ID";
    }
  
    /**
     * gl_transaction.SUB_LEDGER_TYPE
     * @return the column name for the SUB_LEDGER_TYPE field
     * @deprecated use GlTransactionPeer.gl_transaction.SUB_LEDGER_TYPE constant
     */
    public static String getGlTransaction_SubLedgerType()
    {
        return "gl_transaction.SUB_LEDGER_TYPE";
    }
  
    /**
     * gl_transaction.SUB_LEDGER_ID
     * @return the column name for the SUB_LEDGER_ID field
     * @deprecated use GlTransactionPeer.gl_transaction.SUB_LEDGER_ID constant
     */
    public static String getGlTransaction_SubLedgerId()
    {
        return "gl_transaction.SUB_LEDGER_ID";
    }
  
    /**
     * gl_transaction.SUB_LEDGER_DESC
     * @return the column name for the SUB_LEDGER_DESC field
     * @deprecated use GlTransactionPeer.gl_transaction.SUB_LEDGER_DESC constant
     */
    public static String getGlTransaction_SubLedgerDesc()
    {
        return "gl_transaction.SUB_LEDGER_DESC";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("gl_transaction");
        TableMap tMap = dbMap.getTable("gl_transaction");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("gl_transaction.GL_TRANSACTION_ID", "");
                            tMap.addColumn("gl_transaction.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction.TRANSACTION_ID", "");
                          tMap.addColumn("gl_transaction.TRANSACTION_NO", "");
                          tMap.addColumn("gl_transaction.GL_TRANSACTION_NO", "");
                          tMap.addColumn("gl_transaction.TRANSACTION_DATE", new Date());
                          tMap.addColumn("gl_transaction.PROJECT_ID", "");
                          tMap.addColumn("gl_transaction.DEPARTMENT_ID", "");
                          tMap.addColumn("gl_transaction.PERIOD_ID", "");
                          tMap.addColumn("gl_transaction.ACCOUNT_ID", "");
                          tMap.addColumn("gl_transaction.CURRENCY_ID", "");
                            tMap.addColumn("gl_transaction.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("gl_transaction.AMOUNT", bd_ZERO);
                            tMap.addColumn("gl_transaction.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("gl_transaction.DESCRIPTION", "");
                          tMap.addColumn("gl_transaction.USER_NAME", "");
                            tMap.addColumn("gl_transaction.DEBIT_CREDIT", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction.CREATE_DATE", new Date());
                          tMap.addColumn("gl_transaction.LOCATION_ID", "");
                            tMap.addColumn("gl_transaction.SUB_LEDGER_TYPE", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction.SUB_LEDGER_ID", "");
                          tMap.addColumn("gl_transaction.SUB_LEDGER_DESC", "");
          }
}

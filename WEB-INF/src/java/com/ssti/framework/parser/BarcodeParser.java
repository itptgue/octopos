package com.ssti.framework.parser;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.SysConfig;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.Calculator;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Complex barcode parser
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BarcodeParser.java,v 1.11 2008/02/26 05:15:24 albert Exp $ <br>
 *
 * <pre>
 * $Log: BarcodeParser.java,v $
 * Revision 1.11  2008/02/26 05:15:24  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/05/09 06:52:46  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/05/03 14:36:38  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/02/23 14:13:02  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class BarcodeParser implements Attributes
{           
    private static Log log = LogFactory.getLog ( BarcodeParser.class );
    
    private static final int i_QTY   = 1;
    private static final int i_PRICE = 2;
            
    /**
     * parse string itu barcode data
     * 
     * @param _sBarcode
     * @param CONFIG
     * @return BarcodeData
     */
 	public static BarcodeData parseBarcode(String _sBarcode)
 	{
 	    BarcodeData oData = new BarcodeData();
        
        //set default initial value for barcode data
        oData.setComplex(false);
 	    oData.setItemCode(_sBarcode);
 	    oData.setQty(bd_ONE);
 	    oData.setPrice(bd_ZERO);
 	    
 	    SysConfig oConf = PreferenceTool.getSysConfig();
 	     	    
 	    if (_sBarcode != null && oConf != null && Boolean.valueOf(oConf.getComplexBarcodeUse())) 
 	    { 	    	 	    	 	    	
            int iLength = _sBarcode.length();
            int iComplexLength = Integer.valueOf(oConf.getComplexBarcodeTotalLength());            
            if (iLength == iComplexLength) // if barcode length is correct
            {  
                String sPrefix = oConf.getComplexBarcodePrefix();
                String sBarcodePrefix = _sBarcode.substring(0, sPrefix.length());
            	System.out.println("Barcode Pref: " + sPrefix);
                
                if (sBarcodePrefix.equals(sPrefix)) // prefix correct
                {
 	                
                	oData.setComplex  (true);

                    int iPLUStart  = Integer.valueOf(oConf.getComplexBarcodePluStart());
                    int iPLULength = Integer.valueOf(oConf.getComplexBarcodePluLength());
                    int iPLUEndIdx = iPLUStart + iPLULength;
                    
                    String sItemCode = _sBarcode.substring(iPLUStart,iPLUEndIdx); //parse item code                    
                    oData.setItemCode (sItemCode);
 	                                                        
                    int iQtyStart   = Integer.valueOf(oConf.getComplexBarcodeQtyStart());
                    int iQtyLength  = Integer.valueOf(oConf.getComplexBarcodeQtyLength());
                    int iQtyDecPos  = Integer.valueOf(oConf.getComplexBarcodeQtyDecPos());
                    int iQtyEnd 	= iQtyStart + iQtyLength;

                    StringBuilder sQty = new StringBuilder(_sBarcode.substring(iQtyStart, iQtyEnd)); //parse qty
                    sQty.insert(iQtyDecPos, ".");
                    BigDecimal dQty = new BigDecimal(sQty.toString());
                    oData.setQty(dQty);
                	
                    int iOtherType   = Integer.valueOf(oConf.getComplexBarcodeOtherType());
                    int iOtherStart  = Integer.valueOf(oConf.getComplexBarcodeOtherStart());
                    int iOtherLength = Integer.valueOf(oConf.getComplexBarcodeOtherLength());
                    int iOtherDecPos = Integer.valueOf(oConf.getComplexBarcodeOtherDecPos());
                    if (iOtherStart >= iQtyEnd && iOtherLength > 0)
                    {
                    	int iOtherEnd = iOtherStart + iOtherLength;
	                    StringBuilder sOther = new StringBuilder(_sBarcode.substring(iOtherStart, iOtherEnd)); //parse other
	                    if(iOtherDecPos > 0)
	                    {
	                    	sOther.insert(iOtherDecPos, ".");
	                    }
                    	BigDecimal dOther = new BigDecimal(sOther.toString());  //if the string 09.02 will parse to 9.02 , if the string 13.948 will parse to 13.948
                    	oData.setPrice(new BigDecimal(Calculator.div(dOther,dQty)));
                    }
                    log.debug("Barcode Parser Result : " + oData);
                }
            }            
        }
        log.debug (oData);
        return oData;
    }
}
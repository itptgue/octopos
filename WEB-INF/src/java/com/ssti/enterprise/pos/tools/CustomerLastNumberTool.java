package com.ssti.enterprise.pos.tools;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: LastNumberTool.java,v 1.26 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: LastNumberTool.java,v $
 * Revision 1.26  2009/05/04 02:03:51  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CustomerLastNumberTool extends LastNumberByEntityTool
{
	private final Log log = LogFactory.getLog (CustomerLastNumberTool.class);	
	public CustomerLastNumberTool(int _iType, String _sPrefix, String _sFormat, String _sLocCode, Connection _oConn)
		throws Exception
	{
		s_TABLE = "ctl_customer_trans";
		this.iType = _iType;
		this.sPrefix = _sPrefix;
		this.sFormat = _sFormat;
		this.sLocCode = _sLocCode;
		this.m_oConn = _oConn;
		validate(_oConn);
	}

}       
        
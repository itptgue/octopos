package org.apache.turbine.flux.modules.actions.role;

import org.apache.torque.om.BaseObject;
import org.apache.turbine.flux.modules.actions.FluxAction;
import org.apache.turbine.om.security.Permission;
import org.apache.turbine.om.security.Role;
import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.EntityExistsException;
import org.apache.turbine.util.security.PermissionSet;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/org/apache/turbine/flux/modules/actions/role/FluxRoleAction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: FluxRoleAction.java,v 1.7 2006/01/21 12:10:01 albert Exp $
 *
 * $Log: FluxRoleAction.java,v $
 * Revision 1.7  2006/01/21 12:10:01  albert
 * *** empty log message ***
 *
 * Revision 1.6  2005/04/08 06:56:48  Albert
 * *** empty log message ***
 *
 * Revision 1.5  2005/03/29 02:34:02  Albert
 * *** empty log message ***
 *
 */
public class FluxRoleAction extends FluxAction
{
	public void doInsert(RunData data, Context context)
		throws Exception
	{
		Role role = TurbineSecurity.getRoleInstance(null);
		data.getParameters().setProperties(role);
		String name = data.getParameters().getString("name");
		try
		{
			TurbineSecurity.addRole(role);
			data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
		}
		catch(EntityExistsException _oEx)
		{
			context.put("name", name);
			context.put("role", role);
			data.getParameters().add("mode", "insert");
			data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + " " + _oEx.getMessage());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + " " + e.getMessage());
		}
    }

    public void doPerform(RunData data, Context context)
        throws Exception
    {
        data.setMessage(LocaleTool.getString("action_notfound"));
    }

    public void doPermissions(RunData data, Context context)
        throws Exception
    {
    	try
		{
	        String name = data.getParameters().getString("name");
	        Role role = TurbineSecurity.getRoleByName(name);
	        PermissionSet rolePermissions = role.getPermissions();
	        Permission permissions[] = TurbineSecurity.getAllPermissions().getPermissionsArray();	        
	        String roleName = role.getName();
	        for(int i = 0; i < permissions.length; i++)
	        {
	            String permissionName = permissions[i].getName();
	            String srolePermissionId = ((BaseObject)permissions[i]).getPrimaryKey().getValue().toString();
	            String formRolePermission = data.getParameters().getString("Permission"+srolePermissionId);

	            Permission permission = permissions[i];
	            //org.apache.turbine.om.security.Permission permission = TurbineSecurity.getPermissionByName(permissionName);
	            if(formRolePermission != null && !rolePermissions.contains(permission))
	            {
	                log.debug("adding " + permissionName + " to " + roleName);
	                role.grant(permission);
	            } 
	            else if(formRolePermission == null && rolePermissions.contains(permission))
	            {
	                log.debug("removing " + permissionName + " from " + roleName);
	                role.revoke(permission);
	            }
	        }
	        data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
		}
	    catch(Exception _oEx) 
		{
			data.setMessage (LocaleTool.getString(s_UPDATE_FAILED + _oEx.getMessage()));        
		}
    }

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
        Role role = TurbineSecurity.getRoleByName(data.getParameters().getString("name"));
        data.getParameters().setProperties(role);
        try
        {
            TurbineSecurity.saveRole(role);
	        data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch(Exception _oEx) 
		{ 
	        data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());        
        }
    }

    public void doDelete(RunData data, Context context)
	    throws Exception
	{
	    Role role = TurbineSecurity.getRoleByName(data.getParameters().getString("name"));
	    try
	    {
	        TurbineSecurity.removeRole(role);
	        data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
	    }
	    catch(Exception _oEx) 
		{
	        data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());        
	    }
	}

}
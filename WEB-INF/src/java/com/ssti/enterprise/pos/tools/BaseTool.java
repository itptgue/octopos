package com.ssti.enterprise.pos.tools;

import java.io.StringWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.adapter.DB;
import org.apache.torque.om.Persistent;
import org.apache.torque.util.Criteria;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.turbine.util.RunData;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.model.TransactionMasterOM;
import com.ssti.enterprise.pos.om.AccountPeer;
import com.ssti.enterprise.pos.om.ApPaymentPeer;
import com.ssti.enterprise.pos.om.ArPaymentPeer;
import com.ssti.enterprise.pos.om.BankPeer;
import com.ssti.enterprise.pos.om.CashFlowJournalPeer;
import com.ssti.enterprise.pos.om.CashFlowPeer;
import com.ssti.enterprise.pos.om.CashFlowTypePeer;
import com.ssti.enterprise.pos.om.CourierPeer;
import com.ssti.enterprise.pos.om.CreditMemoPeer;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.om.CustomerTypePeer;
import com.ssti.enterprise.pos.om.DebitMemoPeer;
import com.ssti.enterprise.pos.om.DeliveryOrderPeer;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.EmployeePeer;
import com.ssti.enterprise.pos.om.FobPeer;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemTransferPeer;
import com.ssti.enterprise.pos.om.JournalVoucherPeer;
import com.ssti.enterprise.pos.om.KategoriPeer;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.PaymentTermPeer;
import com.ssti.enterprise.pos.om.PaymentTypePeer;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.om.PurchaseInvoicePeer;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceiptPeer;
import com.ssti.enterprise.pos.om.PurchaseRequestPeer;
import com.ssti.enterprise.pos.om.PurchaseReturnPeer;
import com.ssti.enterprise.pos.om.SalesOrderPeer;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.om.VendorPeer;
import com.ssti.framework.exception.DBException;
import com.ssti.framework.exception.DuplicateIDException;
import com.ssti.framework.exception.DuplicateNoException;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BaseTool.java,v 1.19 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: BaseTool.java,v $
 * 2017-03-27
 * -add method validateOverwrite to validate whether a transaction is trying to override
 *  another transaction mostly due to opening multiple window.
 * 
 * </pre><br>
 */
public class BaseTool implements AppAttributes, TransactionAttributes
{
	protected static final Log log = LogFactory.getLog(BaseTool.class);
	
	protected static DB oDB = null;
	
	static 
	{
		try
		{
			oDB = Torque.getDB(s_DB_NAME);
		}
		catch (TorqueException _oTEX)
		{
			log.error (_oTEX);
		}
	}
	
	public static final int i_TRANS_ISOLATION = Connection.TRANSACTION_READ_COMMITTED;

    public static Map m_TRANS = new HashMap();  
    public static Map m_TRNO  = new HashMap();
    public static Map m_TRNOF = new HashMap();
    public static Map m_TRID  = new HashMap();    
    public static Map m_PCLS  = new HashMap();            

    static 
    {
        try
        {
            m_TRANS.put(i_PO, LocaleTool.getString("purchase_order")); 
            m_TRANS.put(i_PR, LocaleTool.getString("purchase_receipt")); 
            m_TRANS.put(i_PI, LocaleTool.getString("purchase_invoice")); 
            m_TRANS.put(i_PT, LocaleTool.getString("purchase_return")); 
            m_TRANS.put(i_SO, LocaleTool.getString("sales_order")); 
            m_TRANS.put(i_DO, LocaleTool.getString("delivery_order")); 
            m_TRANS.put(i_SI, LocaleTool.getString("sales_invoice")); 
            m_TRANS.put(i_SR, LocaleTool.getString("sales_return")); 
            m_TRANS.put(i_RQ, LocaleTool.getString("item_request")); 
            m_TRANS.put(i_IT, LocaleTool.getString("item_transfer")); 
            m_TRANS.put(i_IR, LocaleTool.getString("issue_receipt")); 
            m_TRANS.put(i_JC, LocaleTool.getString("job_costing")); 
            m_TRANS.put(i_PL, LocaleTool.getString("packing_list")); 
            m_TRANS.put(i_CM, LocaleTool.getString("credit_memo")); 
            m_TRANS.put(i_DM, LocaleTool.getString("debit_memo")); 
            m_TRANS.put(i_RP, LocaleTool.getString("receivable_payment")); 
            m_TRANS.put(i_PP, LocaleTool.getString("payable_payment")); 
            m_TRANS.put(i_CF, LocaleTool.getString("cash_management")); 
            m_TRANS.put(i_JV, LocaleTool.getString("journal_voucher")); 
            m_TRANS.put(i_FA, LocaleTool.getString("fixed_asset")); 

            m_TRID.put(i_PO, PurchaseOrderPeer   .PURCHASE_ORDER_ID   );
            m_TRID.put(i_PR, PurchaseReceiptPeer .PURCHASE_RECEIPT_ID );
            m_TRID.put(i_PI, PurchaseInvoicePeer .PURCHASE_INVOICE_ID );
            m_TRID.put(i_PT, PurchaseReturnPeer  .PURCHASE_RETURN_ID  );
            m_TRID.put(i_SO, SalesOrderPeer      .SALES_ORDER_ID      );
            m_TRID.put(i_DO, DeliveryOrderPeer   .DELIVERY_ORDER_ID   );
            m_TRID.put(i_SI, SalesTransactionPeer.SALES_TRANSACTION_ID);
            m_TRID.put(i_SR, SalesReturnPeer     .SALES_RETURN_ID     );
            m_TRID.put(i_RQ, PurchaseRequestPeer .PURCHASE_REQUEST_ID );
            m_TRID.put(i_IT, ItemTransferPeer    .ITEM_TRANSFER_ID    );
            m_TRID.put(i_IR, IssueReceiptPeer    .ISSUE_RECEIPT_ID    );
            //m_TRID.put(i_JC, JobCostingPeer      .JOB_COSTING_ID      );
            //m_TRID.put(i_PL, PackingListPeer     .PACKING_LIST_ID     );
            m_TRID.put(i_CM, CreditMemoPeer      .CREDIT_MEMO_ID      );
            m_TRID.put(i_DM, DebitMemoPeer       .DEBIT_MEMO_ID       );
            m_TRID.put(i_RP, ArPaymentPeer       .AR_PAYMENT_ID       );
            m_TRID.put(i_PP, ApPaymentPeer       .AP_PAYMENT_ID       );
            m_TRID.put(i_CF, CashFlowPeer        .CASH_FLOW_ID        );
            m_TRID.put(i_JV, JournalVoucherPeer  .JOURNAL_VOUCHER_ID  );
            
            m_TRNO.put(i_PO, PurchaseOrderPeer   .PURCHASE_ORDER_NO  );
            m_TRNO.put(i_PR, PurchaseReceiptPeer .RECEIPT_NO         );
            m_TRNO.put(i_PI, PurchaseInvoicePeer .PURCHASE_INVOICE_NO);
            m_TRNO.put(i_PT, PurchaseReturnPeer  .RETURN_NO          );
            m_TRNO.put(i_SO, SalesOrderPeer      .SALES_ORDER_NO     );
            m_TRNO.put(i_DO, DeliveryOrderPeer   .DELIVERY_ORDER_NO  );
            m_TRNO.put(i_SI, SalesTransactionPeer.INVOICE_NO         );
            m_TRNO.put(i_SR, SalesReturnPeer     .RETURN_NO          );
            m_TRNO.put(i_RQ, PurchaseRequestPeer .TRANSACTION_NO     );
            m_TRNO.put(i_IT, ItemTransferPeer    .TRANSACTION_NO     );
            m_TRNO.put(i_IR, IssueReceiptPeer    .TRANSACTION_NO     );
            //m_TRNO.put(i_JC, JobCostingPeer      .JOB_COSTING_NO     );
            //m_TRNO.put(i_PL, PackingListPeer     .PACKING_LIST_NO    );
            m_TRNO.put(i_CM, CreditMemoPeer      .CREDIT_MEMO_NO     );
            m_TRNO.put(i_DM, DebitMemoPeer       .DEBIT_MEMO_NO      );
            m_TRNO.put(i_RP, ArPaymentPeer       .AR_PAYMENT_NO      );
            m_TRNO.put(i_PP, ApPaymentPeer       .AP_PAYMENT_NO      );
            m_TRNO.put(i_CF, CashFlowPeer        .CASH_FLOW_NO       );
            m_TRNO.put(i_JV, JournalVoucherPeer  .TRANSACTION_NO     );

            m_TRNOF.put(i_PO, "purchaseOrderNo");
            m_TRNOF.put(i_PR, "receiptNo");
            m_TRNOF.put(i_PI, "purchaseInvoiceNo");
            m_TRNOF.put(i_PT, "returnNo");
            m_TRNOF.put(i_SO, "salesOrderNo");
            m_TRNOF.put(i_DO, "deliveryOrderNo");
            m_TRNOF.put(i_SI, "invoiceNo");
            m_TRNOF.put(i_SR, "returnNo");
            m_TRNOF.put(i_RQ, "transactionNo");
            m_TRNOF.put(i_IT, "transactionNo");
            m_TRNOF.put(i_IR, "transactionNo");
            m_TRNOF.put(i_JC, "job_costingNo");
            m_TRNOF.put(i_PL, "packingListNo");
            m_TRNOF.put(i_CM, "creditMemoNo");
            m_TRNOF.put(i_DM, "debitMemoNo");
            m_TRNOF.put(i_RP, "arPaymentNo");
            m_TRNOF.put(i_PP, "apPaymentNo");
            m_TRNOF.put(i_CF, "cashFlowNo");
            m_TRNOF.put(i_JV, "transactionNo");
            m_TRNOF.put(i_FA, "fixed_asset_code");

            m_PCLS.put(i_PO, PurchaseOrderPeer   .class  );
            m_PCLS.put(i_PR, PurchaseReceiptPeer .class  );
            m_PCLS.put(i_PI, PurchaseInvoicePeer .class  );
            m_PCLS.put(i_PT, PurchaseReturnPeer  .class  );
            m_PCLS.put(i_SO, SalesOrderPeer      .class  );
            m_PCLS.put(i_DO, DeliveryOrderPeer   .class  );
            m_PCLS.put(i_SI, SalesTransactionPeer.class  );
            m_PCLS.put(i_SR, SalesReturnPeer     .class  );
            m_PCLS.put(i_RQ, PurchaseRequestPeer .class  );
            m_PCLS.put(i_IT, ItemTransferPeer    .class  );
            m_PCLS.put(i_IR, IssueReceiptPeer    .class  );
            //m_PCLS.put(i_JC, JobCostingPeer      .class  );
            //m_PCLS.put(i_PL, PackingListPeer     .class  );
            m_PCLS.put(i_CM, CreditMemoPeer      .class  );
            m_PCLS.put(i_DM, DebitMemoPeer       .class  );
            m_PCLS.put(i_RP, ArPaymentPeer       .class  );
            m_PCLS.put(i_PP, ApPaymentPeer       .class  );
            m_PCLS.put(i_CF, CashFlowPeer        .class  );
            m_PCLS.put(i_JV, JournalVoucherPeer  .class  );            
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
    }
    
	/**
	 * get First Or Last ID
	 * 
	 * @param _oPeerClass
	 * @param _sIDColumn
	 * @param _bIsFirst
	 * @return string of first / last ID
	 * @throws Exception
	 */
	public static String getFirstOrLastID(Class _oPeerClass, String _sIDColumn, boolean _bIsFirst)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (_bIsFirst) 
		{
			oCrit.addAscendingOrderByColumn(_sIDColumn);
		}
		else {
			oCrit.addDescendingOrderByColumn(_sIDColumn);		
		}
		oCrit.setLimit (1);

		List vData = doSelect(_oPeerClass, oCrit);
		if (vData.size() > 0) {
			return ((Persistent)vData.get(0)).getPrimaryKey().getValue().toString();
		}
		return "";
	}
	
	/**
	 * Get the previous or next ID of an OM
	 * 
	 * @param _oPeerClass
	 * @param _sIDColumn 
	 * @param _sID The Current ID
	 * @param _bIsNext
	 * @return string of prev / next ID
	 * @throws Exception
	 */
	public static String getPrevOrNextID(Class _oPeerClass, String _sIDColumn, String _sID, boolean _bIsNext)
		throws Exception
	{		
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sID)) 
		{
			if (_bIsNext) 
			{
				oCrit.add (_sIDColumn, (Object)_sID, Criteria.GREATER_THAN);
				oCrit.addAscendingOrderByColumn(_sIDColumn);
			}
			else 
			{
				oCrit.add (_sIDColumn, (Object)_sID, Criteria.LESS_THAN);
				oCrit.addDescendingOrderByColumn(_sIDColumn);		
			}
			oCrit.setLimit (1);
									
			List vData = doSelect(_oPeerClass, oCrit);
			if (vData.size() > 0) {
				return ((Persistent)vData.get(0)).getPrimaryKey().getValue().toString();
			}
			return "";
		}
		else 
		{
			if (_bIsNext) 
			{
				return getFirstOrLastID (_oPeerClass, _sIDColumn, true);
			}
			return getFirstOrLastID (_oPeerClass, _sIDColumn, false);
		}
	}
	
	/**
	 * Perform doSelect on PeerClass submitted as parameter 
	 * and supply Criteria as the PeerObject.doSelect parameter
	 * 
	 * @param _oPeerClass PeerObject Class i.e BankPeer.class
	 * @param _oCrit Criteria of the doSelect Query
	 * @return List 
	 * @throws Exception
	 */
	
	public static List doSelect(Class _oPeerClass, Criteria _oCrit)
		throws Exception
	{
		Class[] aMethodParamClass = {Criteria.class};
		Method oMethod = _oPeerClass.getMethod("doSelect", aMethodParamClass);
		//log.debug("Method " + oMethod);
		
		Object[] aObjectParamClass = {_oCrit};
		Object oPeer = _oPeerClass.newInstance();
		//log.debug("PeerObj " + oPeer);

		return (List) oMethod.invoke(oPeer, aObjectParamClass);	
	}
	
	public static double countQtyPerItem (List _vDetails, String _sItemID)
	{
		return countQtyPerItem (_vDetails, _sItemID, "qty");
	}	
	
	/**
	 * general method to count qty per item exist in a list 
	 * 
	 * @param _vDetails
	 * @param _sItemID
	 * @return result
	 */
	public static double countQtyPerItem (List _vDetails, String _sItemID, String _sQtyField)
	{
		double dQty = 0;
		if(_vDetails != null)
		{
		    for (int i = 0; i < _vDetails.size(); i++) 
		    {
		    	Object oDet = _vDetails.get(i);
		    	if(_sItemID != null && _sItemID.equals(BeanUtil.invokeGetter(oDet, "itemId")))
		    	{
		    		BigDecimal oQty = (BigDecimal) BeanUtil.invokeGetter(oDet, _sQtyField);
		    	    dQty += oQty.doubleValue();
		    	}
		    }
		}
		return dQty;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// direct add
	///////////////////////////////////////////////////////////////////////////
		
    public static boolean getDirectAdd (String sFrom)
	{	    
    	boolean bDA = false;
    	
    	if (sFrom.equals(s_SO))       {bDA = b_SO_DIRECT_ADD;}
    	else if (sFrom.equals(s_DO))  {bDA = b_DO_DIRECT_ADD;}    	
    	else if (sFrom.equals(s_TR))  {bDA = b_SI_DIRECT_ADD;}    	
    	else if (sFrom.equals("pos")) {bDA = b_POS_DIRECT_ADD;}    	
    	
    	else if (sFrom.equals(s_PO)) {bDA = b_PO_DIRECT_ADD;}
    	else if (sFrom.equals(s_PR)) {bDA = b_PR_DIRECT_ADD;}
    	else if (sFrom.equals(s_PI)) {bDA = b_PI_DIRECT_ADD;}
    	
    	else if (sFrom.equals(s_RQ)) {bDA = b_RQ_DIRECT_ADD;}
    	else if (sFrom.equals(s_IR)) {bDA = b_IR_DIRECT_ADD;}
    	else if (sFrom.equals(s_IT)) {bDA = b_IT_DIRECT_ADD;}
    	//else if (sFrom.equals(s_JC)) {bDA = b_JC_DIRECT_ADD;}
    	
    	return bDA;
	} 	
	
	///////////////////////////////////////////////////////////////////////////
	// find trans helper
	///////////////////////////////////////////////////////////////////////////
	
	public static Map m_FIND_TEXT = new HashMap();
	
	static
	{
	    m_FIND_TEXT.put(Integer.valueOf(i_TRANS_NO 	  ), LocaleTool.getString("trans_no"));
	    m_FIND_TEXT.put(Integer.valueOf(i_DESCRIPTION ), LocaleTool.getString("description"));
	    m_FIND_TEXT.put(Integer.valueOf(i_CREATE_BY   ), LocaleTool.getString("create_by"));
	    m_FIND_TEXT.put(Integer.valueOf(i_CONFIRM_BY  ), LocaleTool.getString("confirmed_by")); 
	    m_FIND_TEXT.put(Integer.valueOf(i_REF_NO   	  ), LocaleTool.getString("reference_no")); 
	    m_FIND_TEXT.put(Integer.valueOf(i_LOCATION    ), LocaleTool.getString("location"));
	    m_FIND_TEXT.put(Integer.valueOf(i_ITEM_CODE   ), LocaleTool.getString("item_code"));   
	    m_FIND_TEXT.put(Integer.valueOf(i_ITEM_NAME   ), LocaleTool.getString("item_name"));
	    m_FIND_TEXT.put(Integer.valueOf(i_ITEM_DESC   ), LocaleTool.getString("item_desc"));
	    m_FIND_TEXT.put(Integer.valueOf(i_PREF_VEND   ), LocaleTool.getString("prefered_vendor"));
	    m_FIND_TEXT.put(Integer.valueOf(i_KATEGORI    ), LocaleTool.getString("category"));    
	    m_FIND_TEXT.put(Integer.valueOf(i_PMT_TYPE    ), LocaleTool.getString("payment_type"));
	    m_FIND_TEXT.put(Integer.valueOf(i_PMT_TERM    ), LocaleTool.getString("payment_term"));
	    m_FIND_TEXT.put(Integer.valueOf(i_COURIER     ), LocaleTool.getString("courier"));
	    m_FIND_TEXT.put(Integer.valueOf(i_FOB 	      ), LocaleTool.getString("fob_data"));
	    m_FIND_TEXT.put(Integer.valueOf(i_CURRENCY    ), LocaleTool.getString("currency"));
	    m_FIND_TEXT.put(Integer.valueOf(i_BANK    	  ), LocaleTool.getString("bank"));
	    m_FIND_TEXT.put(Integer.valueOf(i_VENDOR      ), LocaleTool.getString("vendor"));
	    m_FIND_TEXT.put(Integer.valueOf(i_CUSTOMER    ), LocaleTool.getString("customer"));
	    m_FIND_TEXT.put(Integer.valueOf(i_SALESMAN    ), LocaleTool.getString("salesman"));	
	    m_FIND_TEXT.put(Integer.valueOf(i_ISSUER      ), LocaleTool.getString("issuer_dest"));	
	    m_FIND_TEXT.put(Integer.valueOf(i_ACCOUNT     ), LocaleTool.getString("account"));	 	    
	    m_FIND_TEXT.put(Integer.valueOf(i_REF_NO   	  ), LocaleTool.getString("reference_no")); 
	    m_FIND_TEXT.put(Integer.valueOf(i_VEND_DO     ), LocaleTool.getString("vendor_do_no")); 
	    m_FIND_TEXT.put(Integer.valueOf(i_VEND_SI     ), LocaleTool.getString("vendor_si_no")); 
	    m_FIND_TEXT.put(Integer.valueOf(i_DEPARTMENT  ), LocaleTool.getString("department")); 
	    m_FIND_TEXT.put(Integer.valueOf(i_PROJECT     ), LocaleTool.getString("project"));
	    m_FIND_TEXT.put(Integer.valueOf(i_CFT 		  ), LocaleTool.getString("cash_flow_type"));
	    m_FIND_TEXT.put(Integer.valueOf(i_INV_NO 	  ), LocaleTool.getString("invoice_no"));
	    m_FIND_TEXT.put(Integer.valueOf(i_CUST_TYPE   ), LocaleTool.getString("customer_type"));
	    m_FIND_TEXT.put(Integer.valueOf(i_PMT_NO      ), LocaleTool.getString("payment_trans"));	    
	    m_FIND_TEXT.put(Integer.valueOf(i_PO_NO       ), LocaleTool.getString("purchase_order"));	    
	    m_FIND_TEXT.put(Integer.valueOf(i_PR_NO       ), LocaleTool.getString("purchase_receipt"));	    
	    m_FIND_TEXT.put(Integer.valueOf(i_PI_NO       ), LocaleTool.getString("purchase_invoice"));	    
	    m_FIND_TEXT.put(Integer.valueOf(i_SO_NO       ), LocaleTool.getString("sales_order"));	    
	    m_FIND_TEXT.put(Integer.valueOf(i_DO_NO       ), LocaleTool.getString("delivery_order"));	    
	    m_FIND_TEXT.put(Integer.valueOf(i_SI_NO       ), LocaleTool.getString("sales_invoice"));	    
	}

	public static Map m_GROUP_TEXT = new HashMap();
	
	static
	{
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_NONE  	  ), LocaleTool.getString("none")); 		
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_CUST_VEND  ), LocaleTool.getString("customer")); 
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_CUST_TYPE  ), LocaleTool.getString("customer_type")); 		
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_STATUS     ), LocaleTool.getString("status"));             
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_LOCATION   ), LocaleTool.getString("location"));             
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_CREATE_BY  ), LocaleTool.getString("create_by"));                 
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_CONFIRM_BY ), LocaleTool.getString("confirmed_by"));               
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_PMT_TYPE   ), LocaleTool.getString("pmt_type_term"));
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_CURRENCY   ), LocaleTool.getString("currency"));
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_SALESMAN   ), LocaleTool.getString("salesman"));
		
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_TAX   	  ), LocaleTool.getString("tax"));
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_CATEGORY   ), LocaleTool.getString("category"));
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_ITEM	 	  ), LocaleTool.getString("item_data"));
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_CV_ITEM    ), LocaleTool.getString("customer") + " & " +  LocaleTool.getString("item_data")); 
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_LOC_ITEM   ), LocaleTool.getString("location") + " & " +  LocaleTool.getString("item_data")); 
		m_GROUP_TEXT.put (Integer.valueOf(i_GB_SALES_ITEM ), LocaleTool.getString("salesman") + " & " +  LocaleTool.getString("item_data")); 
	}
		
	
	/**
	 * 
	 * @param m_FIND_PEER
	 * @param s_ITEM String contains column name separated by ','
	 */
	public static void addInv(Map m_FIND_PEER, String s_ITEM)
	{
		m_FIND_PEER.put (Integer.valueOf(i_ITEM_CODE  ), s_ITEM);
		m_FIND_PEER.put (Integer.valueOf(i_ITEM_NAME  ), s_ITEM);
		m_FIND_PEER.put (Integer.valueOf(i_ITEM_DESC  ), s_ITEM);
		m_FIND_PEER.put (Integer.valueOf(i_KATEGORI   ), s_ITEM); 
        m_FIND_PEER.put (Integer.valueOf(i_PREF_VEND  ), s_ITEM);	
	}
	
	/**
	 * 
	 * @param s_FIND_PEER
	 * @param _iCond
	 * @param _sKeywords
	 * @return
	 */
	public static Criteria buildFindCriteria (Map m_FIND_PEER, int _iCond, String _sKeywords) throws Exception
	{
		return buildFindCriteria (m_FIND_PEER, _iCond, _sKeywords, null, null, null, null);
	}

	/**
	 * 
	 * @param s_FIND_PEER
	 * @param _iCond
	 * @param _sKeywords
	 * @return
	 */
	public static Criteria buildFindCriteria (String _sDatePeer, Date _dStart, Date _dEnd) throws Exception
	{
		return buildFindCriteria (null, -1, null, _sDatePeer, _dStart, _dEnd, null);
	}
	
	/**
	 * 
	 * @param s_FIND_PEER
	 * @param _iCond
	 * @param _sKeywords
	 * @param _sDatePeer
	 * @param _dStart
	 * @param _dEnd
	 * @return Criteria
	 */
	public static Criteria buildFindCriteria (Map m_FIND_PEER, 
											  int _iCond, 
											  String _sKeywords, 
											  String _sDatePeer,
											  Date _dStart, 
											  Date _dEnd,
											  String _sCurrencyID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		
		if (StringUtil.isNotEmpty(_sDatePeer))
		{
			if (_dStart != null) oCrit.add(_sDatePeer, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
			if (_dEnd != null) oCrit.and(_sDatePeer, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		}

		if (StringUtil.isNotEmpty(_sCurrencyID))
		{	
			String sCurrencyPeer = (String)m_FIND_PEER.get(Integer.valueOf(i_CURRENCY));
			if (StringUtil.isNotEmpty(sCurrencyPeer)) oCrit.add(sCurrencyPeer, _sCurrencyID);
		}

		if (m_FIND_PEER != null && StringUtil.isNotEmpty(_sKeywords))
		{
			String sColumn = (String) m_FIND_PEER.get(Integer.valueOf(_iCond));
			String aColumn[] = {};
			
			if (_iCond >= i_ITEM_CODE && _iCond <= i_PREF_VEND)
			{
				aColumn = StringUtil.split(sColumn, ",");
			}
			
			oCrit.setIgnoreCase(true);			
			if (StringUtil.isNotEmpty(sColumn))
			{
				if (_iCond < i_LOCATION && _iCond != i_CREATE_BY && _iCond != i_CONFIRM_BY)
				{
					oCrit.add(sColumn, (Object) _sKeywords, Criteria.ILIKE);				
				}
				else if (_iCond == i_CREATE_BY || _iCond == i_CONFIRM_BY)
				{
					List vEmp = EmployeeTool.findData(1, _sKeywords, "");
					if(vEmp.size() > 0)
					{
						for(int i = 0; i < vEmp.size(); i++)
						{
							Employee oEmp = (Employee) vEmp.get(i);	
							if(i == 0) oCrit.add(sColumn, oEmp.getUserName()); 
							else oCrit.or(sColumn, oEmp.getUserName()); 							
						}
					}
					else
					{
						oCrit.add(sColumn, (Object) _sKeywords, Criteria.ILIKE);
					}
				}
				else if (_iCond == i_LOCATION)
				{
					oCrit.addJoin(sColumn, LocationPeer.LOCATION_ID);
					oCrit.add(LocationPeer.LOCATION_NAME, (Object) _sKeywords, Criteria.ILIKE);		
				}
				else if (_iCond == i_ITEM_CODE)
				{
					oCrit.addJoin(aColumn[0], aColumn[1]);				
					oCrit.addJoin(aColumn[2], ItemPeer.ITEM_ID);
					oCrit.add(ItemPeer.ITEM_CODE, (Object) _sKeywords, Criteria.ILIKE);	
					oCrit.setDistinct();
				}
				else if (_iCond == i_ITEM_NAME)
				{
					oCrit.addJoin(aColumn[0], aColumn[1]);				
					oCrit.addJoin(aColumn[2], ItemPeer.ITEM_ID);
					oCrit.add(ItemPeer.ITEM_NAME, (Object) _sKeywords, Criteria.ILIKE);
					oCrit.setDistinct();
				}
				else if (_iCond == i_ITEM_DESC)
				{
					oCrit.addJoin(aColumn[0], aColumn[1]);				
					oCrit.addJoin(aColumn[2], ItemPeer.ITEM_ID);
					oCrit.add(ItemPeer.DESCRIPTION, (Object) _sKeywords, Criteria.ILIKE);
					oCrit.setDistinct();
				}
				else if (_iCond == i_KATEGORI)
				{
					String sID = KategoriTool.getIDByDescription(_sKeywords);
					List vID = KategoriTool.getChildIDList(sID);
					vID.add(sID);
					
					oCrit.addJoin(aColumn[0], aColumn[1]);				
					oCrit.addJoin(aColumn[2], ItemPeer.ITEM_ID);
					oCrit.addJoin(ItemPeer.KATEGORI_ID, KategoriPeer.KATEGORI_ID);
					oCrit.addIn(KategoriPeer.KATEGORI_ID, vID);		
					oCrit.setDistinct();
				}			
				else if (_iCond == i_PREF_VEND)
				{
					oCrit.addJoin(aColumn[0], aColumn[1]);				
					oCrit.addJoin(aColumn[2], ItemPeer.ITEM_ID);
					oCrit.addJoin(ItemPeer.PREFERED_VENDOR_ID, VendorPeer.VENDOR_ID);
					oCrit.add(VendorPeer.VENDOR_NAME, (Object) _sKeywords ,Criteria.ILIKE);	
					oCrit.setDistinct();
				}							
				else if (_iCond == i_PMT_TYPE)
				{
					oCrit.addJoin(sColumn, PaymentTypePeer.PAYMENT_TYPE_ID);				
					oCrit.add(PaymentTypePeer.DESCRIPTION, (Object) _sKeywords, Criteria.ILIKE);		
				}			
				else if (_iCond == i_PMT_TERM)
				{
					oCrit.addJoin(sColumn, PaymentTermPeer.PAYMENT_TERM_ID);				
					oCrit.add(PaymentTermPeer.DESCRIPTION, (Object) _sKeywords, Criteria.ILIKE);
				}			
				else if (_iCond == i_COURIER)
				{
					oCrit.addJoin(sColumn, CourierPeer.COURIER_ID);				
					oCrit.add(CourierPeer.COURIER_NAME, (Object) _sKeywords, Criteria.ILIKE);
				}			
				else if (_iCond == i_FOB)
				{
					oCrit.addJoin(sColumn, FobPeer.FOB_ID);				
					oCrit.add(FobPeer.DESCRIPTION, (Object) _sKeywords, Criteria.ILIKE);
				}			
				else if (_iCond == i_CURRENCY)
				{
					oCrit.addJoin(sColumn, CurrencyPeer.CURRENCY_ID);				
					oCrit.add(CurrencyPeer.DESCRIPTION, (Object) _sKeywords, Criteria.ILIKE);
				}
				else if (_iCond == i_BANK)
				{
					oCrit.addJoin(sColumn, BankPeer.BANK_ID);				
					oCrit.add(BankPeer.DESCRIPTION, (Object) _sKeywords, Criteria.ILIKE);		
				}

				else if (_iCond == i_VENDOR)
				{
					oCrit.addJoin(sColumn, VendorPeer.VENDOR_ID);				
					oCrit.add(VendorPeer.VENDOR_NAME, (Object) _sKeywords, Criteria.ILIKE);		
				}				
				else if (_iCond == i_CUSTOMER)
				{
					oCrit.addJoin(sColumn, CustomerPeer.CUSTOMER_ID);				
					oCrit.add(CustomerPeer.CUSTOMER_NAME, (Object) _sKeywords, Criteria.ILIKE);		
				}				
				else if (_iCond == i_SALESMAN)
				{
					oCrit.addJoin(sColumn, EmployeePeer.EMPLOYEE_ID);				
					oCrit.add(EmployeePeer.EMPLOYEE_NAME, (Object) _sKeywords, Criteria.ILIKE);		
				}				
				else if (_iCond == i_ISSUER)
				{
					oCrit.add(sColumn, (Object) _sKeywords, Criteria.ILIKE);		
				}				
				else if (_iCond == i_ACCOUNT)
				{
					aColumn = StringUtil.split(sColumn, ",");
					oCrit.addJoin(aColumn[0], aColumn[1]);				
					oCrit.addJoin(aColumn[2], AccountPeer.ACCOUNT_ID);
					oCrit.add(AccountPeer.ACCOUNT_NAME, (Object) _sKeywords ,Criteria.ILIKE);	
				}			
				else if (_iCond == i_VEND_DO)
				{
					oCrit.add(sColumn, (Object) _sKeywords, Criteria.ILIKE);		
				}							
				else if (_iCond == i_VEND_SI)
				{
					oCrit.add(sColumn, (Object) _sKeywords, Criteria.ILIKE);		
				}											
				else if (_iCond == i_CFT)
				{
					if (StringUtil.isNotEmpty(sColumn) && sColumn.contains(CashFlowJournalPeer.CASH_FLOW_TYPE_ID))
					{
						aColumn = StringUtil.split(sColumn, ",");
						oCrit.addJoin(aColumn[0], aColumn[1]);				
						oCrit.addJoin(aColumn[2], CashFlowTypePeer.CASH_FLOW_TYPE_ID);
						oCrit.add(CashFlowTypePeer.DESCRIPTION, (Object) _sKeywords ,Criteria.ILIKE);	
					}
					else
					{
						oCrit.addJoin(sColumn, CashFlowTypePeer.CASH_FLOW_TYPE_ID);
						oCrit.add(CashFlowTypePeer.DESCRIPTION, (Object) _sKeywords ,Criteria.ILIKE);	
					}
				}
				else if (_iCond == i_INV_NO)
				{
					aColumn = StringUtil.split(sColumn, ",");
					oCrit.addJoin(aColumn[0], aColumn[1]);				
					oCrit.add(aColumn[2], (Object) _sKeywords ,Criteria.ILIKE);
				}
				else if (_iCond == i_CUST_TYPE)
				{
					oCrit.addJoin(sColumn, CustomerPeer.CUSTOMER_ID);				
					oCrit.addJoin(CustomerPeer.CUSTOMER_TYPE_ID, CustomerTypePeer.CUSTOMER_TYPE_ID);								
					oCrit.add(CustomerTypePeer.DESCRIPTION, (Object) _sKeywords, Criteria.ILIKE);
				}
				else if (_iCond == i_PMT_NO)
				{
					oCrit.add(sColumn, (Object)_sKeywords, Criteria.ILIKE);		
				}				
			}			
		}
		return oCrit;
	}
	
	/**
	 * 
	 * @param _oCrit
	 * @param m_GROUP_PEER
	 * @param _iGroupBy
	 * @throws Exception
	 */
	public static void buildOrderFromGroupBy(Criteria _oCrit, Map m_GROUP_PEER, int _iGroupBy)
		throws Exception
	{		
		String sColumn = (String) m_GROUP_PEER.get(Integer.valueOf(_iGroupBy));
		String aColumn[] = {};
		
		if (StringUtil.isNotEmpty(sColumn) && sColumn.contains(","))
		{			
			aColumn = StringUtil.split(sColumn, ",");
			if (_iGroupBy == i_GB_ITEM && aColumn.length == 3)
			{
				_oCrit.addJoin(aColumn[0], aColumn[1]);				
				_oCrit.addJoin(aColumn[2], ItemPeer.ITEM_ID);
				_oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);							
			}			
			if (_iGroupBy == i_GB_CATEGORY && aColumn.length == 3)
			{
				_oCrit.addJoin(aColumn[0], aColumn[1]);				
				_oCrit.addJoin(aColumn[2], ItemPeer.ITEM_ID);
				_oCrit.addJoin(ItemPeer.KATEGORI_ID, KategoriPeer.KATEGORI_ID);
				_oCrit.addAscendingOrderByColumn(KategoriPeer.KATEGORI_CODE);							
			}	
		}
		else if (StringUtil.isNotEmpty(sColumn))
		{
			if (_iGroupBy == i_GB_CUST_VEND || _iGroupBy == i_GB_CV_ITEM)
			{
				if (StringUtil.isNotEmpty(sColumn) || sColumn.contains("customer_id")) 
				{
					_oCrit.addJoin(sColumn, CustomerPeer.CUSTOMER_ID);
					_oCrit.addAscendingOrderByColumn(CustomerPeer.CUSTOMER_NAME);
				}
				else if (StringUtil.isNotEmpty(sColumn) || sColumn.contains("vendor_id"))
				{
					_oCrit.addJoin(sColumn, VendorPeer.VENDOR_ID);
					_oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);				
				}
			}
			else if (_iGroupBy == i_GB_LOCATION || _iGroupBy == i_GB_LOC_ITEM)
			{ 
				_oCrit.addJoin(sColumn, LocationPeer.LOCATION_ID);
				_oCrit.addAscendingOrderByColumn(LocationPeer.LOCATION_NAME);
			}
			else if (_iGroupBy == i_GB_PMT_TYPE)
			{ 
				_oCrit.addJoin(sColumn, PaymentTypePeer.PAYMENT_TYPE_ID);
				_oCrit.addAscendingOrderByColumn(PaymentTypePeer.PAYMENT_TYPE_CODE);
			}
			else if (_iGroupBy == i_GB_CURRENCY)
			{ 
				_oCrit.addJoin(sColumn, CurrencyPeer.CURRENCY_ID);
				_oCrit.addDescendingOrderByColumn(CurrencyPeer.IS_DEFAULT);
				_oCrit.addAscendingOrderByColumn(CurrencyPeer.CURRENCY_CODE);
			}
			else if (_iGroupBy == i_GB_SALESMAN)
			{ 
				_oCrit.addJoin(sColumn, EmployeePeer.EMPLOYEE_ID);
				_oCrit.addAscendingOrderByColumn(EmployeePeer.EMPLOYEE_NAME);
			}			
			else
			{
				_oCrit.addAscendingOrderByColumn(sColumn);
			}
		}
		String sTransDateCol = (String) m_GROUP_PEER.get(Integer.valueOf(i_ORDER_BY_DATE));
		if (StringUtil.isNotEmpty(sTransDateCol))
		{
			_oCrit.addAscendingOrderByColumn(sTransDateCol);
		}	
	}
		
	
	/**
	 * build conditionSB
	 * 
	 * @param m_FIND_PEER
	 * @param _iSelected
	 * @return SelectBox String
	 */
	public static String conditionSB (Map m_FIND_PEER, int _iSelected)
	{
		StringBuilder oSB = new StringBuilder();
		oSB.append("<select name=\"Condition\" style=\"width:150px\">");		
		Set vKey = new TreeSet(m_FIND_PEER.keySet());
		Iterator iter  = vKey.iterator();
		while (iter.hasNext())
		{
			Integer iKey = (Integer) iter.next();
			String sPeer = (String) m_FIND_PEER.get(iKey);
			if (StringUtil.isNotEmpty(sPeer))
			{
				oSB.append(" <option value=\"")
				   .append(iKey).append("\"");
				if (iKey.intValue() == _iSelected) oSB.append(" SELECTED ");
				oSB.append(">");
				oSB.append(m_FIND_TEXT.get(iKey)).append("</option>");
			}
		}
		oSB.append("</select>");
		return oSB.toString();
	}	

	public static String groupSB (Map m_GROUP_PEER, int _iSelected)
	{
		return 	groupSB (m_GROUP_PEER, false, _iSelected);
	}
	
	/**
	 * build conditionSB
	 * 
	 * @param m_FIND_PEER
	 * @param _iSelected
	 * @return
	 */
	public static String groupSB (Map m_GROUP_PEER, boolean _bVendor, int _iSelected)
	{
		StringBuilder oSB = new StringBuilder();
		oSB.append("<select name=\"GroupBy\" style=\"width:200px\">");		
		Set vKey = new TreeSet(m_GROUP_PEER.keySet());
		Iterator iter  = vKey.iterator();
		while (iter.hasNext())
		{
			Integer iKey = (Integer) iter.next();
			if (iKey.intValue() > 0)
			{
				String sPeer = (String) m_GROUP_PEER.get(iKey);
				if (StringUtil.isNotEmpty(sPeer) || (StringUtil.isEmpty(sPeer) && iKey.intValue() == i_GB_NONE)) 
				{
					oSB.append(" <option value=\"")
					.append(iKey).append("\"");
					if (iKey.intValue() == _iSelected) oSB.append(" SELECTED ");
					oSB.append(">");
					if (_bVendor && iKey.intValue() == i_GB_CUST_VEND)
					{
						oSB.append(LocaleTool.getString("vendor"));					
					}
					else if (_bVendor && iKey.intValue() == i_GB_CV_ITEM)
					{
						oSB.append(LocaleTool.getString("vendor") + " & " +  LocaleTool.getString("item_data"));					
					}
					else
					{
						oSB.append(m_GROUP_TEXT.get(iKey));
					}
				}
				oSB.append("</option>");
			}
		}
		oSB.append("</select>");
		return oSB.toString();
	}		
	
	public static String groupTxt (int _iGroupBy)
	{		
		return (String) m_GROUP_TEXT.get(Integer.valueOf(_iGroupBy));		
	}	
	
	public static String condTxt (int _iCond)
	{		
		return (String) m_FIND_TEXT.get(Integer.valueOf(_iCond));		
	}		
	
	/**
	 * build custom from to for hand-written sql query
	 * 
	 * @param _oSQL
	 * @param _sDateCol
	 * @param _dStart
	 * @param _dEnd
	 * @return Date Query StringBuilder
	 */
	public static StringBuilder buildDateQuery (StringBuilder _oSQL, String _sDateCol, 
												Date _dStart, Date _dEnd)
	{
		if (_dStart != null)
		{
			_oSQL.append (" ").append(_sDateCol).append (" >= ");
			_oSQL.append (oDB.getDateString(DateUtil.getStartOfDayDate(_dStart))); 
		}
		if (_dEnd != null)
		{			
			_oSQL.append (" AND ").append(_sDateCol).append (" <= ");
			_oSQL.append (oDB.getDateString(DateUtil.getEndOfDayDate(_dEnd))); 			
			_oSQL.append (" "); 			
		}
		return _oSQL;
	}
	
	public static Criteria transDateCriteria (Criteria _oCrit, String _sCol, Date _dStart, Date _dEnd)
	{
        if (_oCrit == null) _oCrit = new Criteria();
        
        if (_dStart != null)
        	_oCrit.add(_sCol, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);

        if (_dEnd != null)
            _oCrit.and(_sCol, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        
        return _oCrit;
	}	
	
	/**
	 * create cancelled by remark
	 * 
	 * @param _sRemark
	 * @param _sCancelBy
	 * @return String
	 */
	public static String cancelledBy(String _sRemark, String _sCancelBy)
	{
		StringBuilder oSB = new StringBuilder();
		if (StringUtil.isNotEmpty(_sRemark))
		{
			oSB.append(_sRemark).append("\n");
		}
		String sCancelDate = CustomFormatter.formatDateTime(new Date());
		oSB.append(LocaleTool.getString("cancelled_by")).append(" ").append(_sCancelBy).append("\n");
		oSB.append(LocaleTool.getString("cancel_date")).append(" : ").append(sCancelDate);
		return oSB.toString();
	}
	
	/**
	 * 
	 * @param _vDetail
	 * @param _sItemID
	 * @return filtered List
	 * @throws Exception
	 */
	public static List filterByKategoriID (List _vDetail, String _sKategoriID)
		throws Exception
	{
		List vResult = (List)((ArrayList)_vDetail).clone();
		Iterator oIter = vResult.iterator();
		while (oIter.hasNext()) 
		{
			Object o =  oIter.next();
			String sItemID = (String) BeanUtil.invokeGetter(o, "itemId");
			Item oItem = ItemTool.getItemByID(sItemID);
			
			if (oItem != null && oItem.getKategoriId() != null && (!oItem.getKategoriId().equals(_sKategoriID)))
			{
				oIter.remove();
			}
		}
		return vResult;
	}
	
	/**
	 * validate transaction date towards closed period
	 * 
	 * @param _dTransDate
	 * @throws Exception
	 */
	public static void validateDate (Date _dTransDate, Connection _oConn)
		throws Exception
	{
		Period oPeriod = PeriodTool.getPeriodByDate(_dTransDate, _oConn);
        String sDate = CustomFormatter.formatDate(_dTransDate);
        if (DateUtil.isAfter(_dTransDate, new Date()))
        {
			StringBuilder oSB = new StringBuilder();
			oSB.append("Trans Date is in the Future : ").append(sDate);
			throw new Exception(oSB.toString());
        }
		if (oPeriod != null && oPeriod.getIsClosed())
		{
			StringBuilder oSB = new StringBuilder();
			oSB.append("Invalid Trans Date : ").append(sDate);
			oSB.append(" Period ").append(oPeriod.getDescription()).append(" is already closed ");			
			throw new Exception(oSB.toString());
		}
        if (PreferenceTool.syncDailyClosing())
        {
            
        }
	}
    
	/**
	 * simple one column based finder
	 * 
	 * @param _sPeerClass
	 * @param _sColumn
	 * @param _sValue
	 * @return List of object from peer query result
	 * @throws Exception
	 */
    public static List find (String _sPeerClass, String _sColumn, String _sValue)
        throws Exception
    {
        Class cPeer = Class.forName("com.ssti.enterprise.pos.om." + _sPeerClass);
        Criteria oCrit = new Criteria();
        oCrit.add(_sColumn, (Object) ("%" + _sValue + "%") ,Criteria.LIKE);
        oCrit.addAscendingOrderByColumn(_sColumn);        
        return doSelect(cPeer, oCrit);
    }
    
    /**
     * validate ID against existing ID in same DB table
     * 
     * @param _oExist existing object with same ID
     * @param _sNo no field
     * @throws Exception
     */
	public static void validateID(Object _oExist, String _sNo) 
		throws Exception
	{
		if (_oExist != null)
		{
			Object oNo = BeanUtil.invokeGetter(_oExist, _sNo);
			if (oNo != null && (oNo instanceof String)) 
			{
				String sNo = (String) oNo;
				if (StringUtil.isNotEmpty(sNo))
				{
					throw new DuplicateIDException (sNo);
				}
			}
		}
	}
	
	/**
	 * Get transaction by transaction no
	 * 
	 * @param _sNoColumn, column name in database
	 * @param _sNo, no to check
	 * @param _cPeer peer class
	 * @param _oConn
	 * @return transaction object
	 * @throws Exception
	 */
	public static Object getByTransNo(String _sNoColumn,
									  String _sNo, 
									  Class _cPeer, 
									  Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(_sNoColumn, _sNo);
	    
	    Class cParam[] = {Criteria.class, Connection.class};
	    Object oParam[] = {oCrit, _oConn}; 
	    
	    List vData = (List) _cPeer.getMethod("doSelect",cParam).invoke(null, oParam);
	    if (vData.size() > 0) 
	    {
			return vData.get(0);
		}
		return null;
	}

	/**
	 * Validate saving new transaction for overwriting an existing one.
	 * When user opening multiple window, there are posibility of one transaction 
	 * overwriting another, we should validate when trying to save for the first time  	 
	 *  
	 * @param _sIDColumn, primary key column name in database
	 * @param _sID, ID to check
	 * @param _cPeer peer class
	 * @param _oConn
	 * @throws Exception
	 */
	public static void validateOverwrite (String _sIDColumn,
			  					   		  String _sID, 
			  					   		  Class _cPeer, 
			  					   		  int _iPendingStatus,
			  					   		  Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		
		Criteria oCrit = new Criteria();
	    oCrit.add(_sIDColumn, _sID);	    
	    Class cParam[] = {Criteria.class, Connection.class};
	    Object oParam[] = {oCrit, _oConn}; 	    	    
	    List vData = (List) _cPeer.getMethod("doSelect",cParam).invoke(null, oParam);
	    if (vData.size() > 0) 
	    {	    	
	    	TransactionMasterOM oExist = (TransactionMasterOM) vData.get(0);
	    	String sNo = oExist.getTransactionNo();	    	
	    	if(oExist.getStatus() != _iPendingStatus)
	    	{	    		
	    		throw new NestableException("Invalid Data, Trying to Overwrite Other Existing Trans "  + sNo + "  , Please Create a New Transaction!");
	    	}
		}				
	}
	
	/**
	 * Validate newly generated Transaction No against existing
	 * trans no in database
	 * 
	 * @param _sNoColumn, column name in database
	 * @param _sNo, no to check
	 * @param _cPeer peer class
	 * @param _oConn
	 * @throws Exception
	 */
	public static void validateNo (String _sNoColumn,
			  					   String _sNo, 
			  					   Class _cPeer, 
			  					   Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		
		String sSWKey = "validateNo " + _sNo;
		StopWatch oSW = new StopWatch(); //identify possible index problem
		oSW.setLogger(log);
		oSW.start(sSWKey);
		Object o = getByTransNo (_sNoColumn, _sNo, _cPeer, _oConn);
		oSW.stop(sSWKey);
		if (o != null)
		{
			throw new DuplicateNoException (_sNo);
		}
	}
	
	/**
	 * Create connection object and set auto commit to false
	 * 
	 * @return Connection object
	 * @throws Exception
	 */
	public static Connection beginTrans()
		throws Exception
	{
		Connection oConn = Torque.getConnection();
		if (oConn != null)
		{
			oConn.setAutoCommit(false);
		}
		else
		{
			throw new DBException("Can not Acquire Connection to Database for Transaction");
		}
		if (log.isDebugEnabled()) log.debug(oConn);
		return oConn;
	}

	/**
	 * Commit connection object
	 * 
	 * @param _oConn Connection object to commit
	 * @throws Exception
	 */
    public static void commit(Connection _oConn) throws Exception
    {
    	validate(_oConn);
        try
        {
        	_oConn.commit();
            _oConn.setAutoCommit(true);
        }
        catch (SQLException _oSQLEx)
        {
            throw new DBException("Failed Committing Transaction : " + _oSQLEx.getMessage(), _oSQLEx);
        }
        finally
        {
            Torque.closeConnection(_oConn);
        }
    }
	
    /**
     * Rollback connection object
     * 
     * @param _oConn Connection object to rollback
     * @throws Exception
     */
    public static void rollback(Connection _oConn) throws Exception
    {
    	validate(_oConn);
        try
        {
        	_oConn.rollback();
        	_oConn.setAutoCommit(true);
        }
        catch (SQLException _oSQLEx)
        {
            throw new DBException("Failed to Rollback Transaction : " + _oSQLEx.getMessage(), _oSQLEx);
        }
        finally
        {
            Torque.closeConnection(_oConn);
        }
    }
    
    /**
     * Validate connection object used in transaction
     * check for auto commit valid state and not closed
     * 
     * @param _oConn Connection object to validate
     * @throws Exception
     */
    public static void validate(Connection _oConn)
		throws Exception
	{
		if (_oConn != null)
		{
			if (_oConn.isClosed())
			{				
				throw new DBException("Invalid Connection State (Closed)");
			}
			if (_oConn.getAutoCommit())
			{
				throw new DBException("Invalid Connection State (Auto Commit)");
			}
			if (log.isDebugEnabled()) log.debug(_oConn);
		}
		else
		{
			throw new DBException("Invalid Connection Object (Null)");
		}
	}
    
	/**
	 * validate transaction date towards closed period
	 * 
	 * @param _dTransDate
	 * @throws Exception
	 */
	public static Date checkBackDate (Date _dTransDate, int _iTransType)
		throws Exception
	{
		if (_dTransDate != null && DateUtil.isBackDate(_dTransDate))
		{				
			if (_iTransType == i_INV_OUT)
			{
				return DateUtil.getEndOfDayDate(_dTransDate);	
			}
			else if (_iTransType == i_INV_IN)
			{
				return DateUtil.getStartOfDayDate(_dTransDate);	
			}
			else if (_iTransType == i_INV_TRF)
			{
				if (!DateUtil.isTimeSet(_dTransDate))
				{	
					//only format to mid date if time is not set 
					String sHour = CONFIG.getString("itemtransfer.backdate.hour", "08:00");
					return DateUtil.setTimeToDate(_dTransDate, sHour);	
				}
			}
		}
        else if(_dTransDate == null || DateUtil.isToday(_dTransDate))
        {
            return new Date();
        }
		return _dTransDate;
	}
	
	public static String generateCode (String _sPrefix, 
									   String _sColumn, 
									   String _sField,
									   Class _cPeer,
									   int _iLength)
	{
		if (_sPrefix == null) _sPrefix = "";
		
		Criteria oCrit = new Criteria();
	    if (StringUtil.isNotEmpty(_sPrefix))
	    {
	    	oCrit.add(_sColumn, (Object)(_sPrefix + "%"), Criteria.LIKE);
	    }
	    oCrit.addDescendingOrderByColumn(_sColumn);
	    oCrit.setLimit(1);
	    
	    Class cParam[] = {Criteria.class};
	    Object oParam[] = {oCrit}; 
	    String sLastCode = "";
	    String sNewCode = "";
	    List vData;
		try 
		{
			vData = (List) _cPeer.getMethod("doSelect",cParam).invoke(null, oParam);
			if (vData.size() > 0) 
		    {
		    	sLastCode = (String) BeanUtil.invokeGetter(vData.get(0), _sField);
			}
			
			int iLastNumber = 0;	    
			if (StringUtil.isNotEmpty(sLastCode))
			{
				int iPrefLen = _sPrefix.length();
				String sNumber = sLastCode.substring(iPrefLen, sLastCode.length());
				iLastNumber = Integer.parseInt(sNumber)+ 1;
			}
			else
			{
				iLastNumber = 1;			
			}
			sNewCode = _sPrefix + StringUtil.formatNumberString(Integer.toString(iLastNumber), _iLength);
		} 
		catch (Exception _oEx) 
		{
			log.error("ERROR GENERATING CODE : " + _oEx.getMessage(), _oEx);
			_oEx.printStackTrace();
		}
		return sNewCode;
	}
	
	public static void renumber(List _vTD)
	{
		if (_vTD != null)
		{
			for (int i = 0; i < _vTD.size(); i++)
			{
				InventoryDetailOM oTD = (InventoryDetailOM) _vTD.get(i);
				int j = i + 1;
				oTD.setIndexNo(j);
			}
		}
	}
	
	public static String displayStackTrace(Throwable t, StringBuilder sb)
	{
		if (sb == null) sb = new StringBuilder();	
		if(t != null)
		{
			String cls = t.getClass().getName();
			log.debug(cls);
			if(StringUtil.contains(cls,"java.lang") || 
				t instanceof NullPointerException)
			{
				sb.append(cls).append(s_LINE_SEPARATOR);
				StackTraceElement[] el = t.getStackTrace();
				if (el != null)
				{
					for (int i = 0; i < el.length; i ++)
					{
						if (i <= 3)
						{
							sb.append("    ")
							  .append(displayStackEl(el[i]))
							  .append(s_LINE_SEPARATOR);
						}
					}
				}	
			}					
			if (t.getCause() != null)
			{
				displayStackTrace(t.getCause(), sb);			
			}	
		}
		return sb.toString();
	}
	
	public static String displayStackEl(StackTraceElement el)
	{
		StringBuilder sb = new StringBuilder();
		if (el != null && el.getClass() != null)
		{
			String cls = el.getClassName();
			int dot = cls.lastIndexOf(".") + 1;
			if (cls.length() > dot) cls = cls.substring(dot);
			sb.append(cls)
			  .append(".").append(el.getMethodName())
			  .append(":").append(el.getLineNumber());
		
		}
		return sb.toString();
	}
	
	public static String displayStackTpl(RunData data, String _sStack, Throwable t)
	{
		if (StringUtil.isEmpty(_sStack) && t != null)
		{
			_sStack = displayStackTrace(t, null);
		}
        VelocityContext oCtx = new VelocityContext();
        oCtx.put("stack", _sStack);        
        try
		{
	        Context oGlobal = TurbinePull.getGlobalContext();		
			for (int i = 0; i < oGlobal.getKeys().length; i++)
			{
				String sKey = (String) oGlobal.getKeys()[i];
				oCtx.put(sKey, oGlobal.get(sKey));
			}		
	        TurbinePull.populateContext(oCtx, data);
	        
		    StringWriter oWriter = new StringWriter ();
		    Velocity.mergeTemplate(s_ERR_STACK_TPL, s_DEFAULT_ENCODING, oCtx, oWriter );
	        return oWriter.toString();		  	    				
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error(e);
		}
		return "";
	}
}

package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.report.SalesReportTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.ssti.framework.tools.TemplateUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-09-18
 * -add method generateRQ to create SO to Item Request
 * 
 * </pre><br>
 */
public class SalesOrderToPO extends SalesOrderView
{
	String sUserName = "";
    String sLocID = "";
    String sRemark = "";
    String sStart = null;
    String sEnd = null;
	public void doBuildTemplate(RunData data, Context context)
    {
		super.doBuildTemplate(data, context);
		
		int iGenTx = data.getParameters().getInt("genPO");		
		sStart = data.getParameters().getString("StartDate");
		sEnd = data.getParameters().getString("EndDate");
		sLocID = data.getParameters().getString("LocationId");
		sUserName = data.getUser().getName();
		sRemark = "Outstanding SO";
		if(StringUtil.isNotEmpty(sStart) || StringUtil.isNotEmpty(sEnd))
		{
			sRemark +=  sStart + " - " + sEnd;
		}

		List vData = null;
		if(iGenTx > 0)
		{
			try 
			{					
				vData = (List) context.get(s_CTX_VAR);		
				if (iGenTx == 1 && vData != null) //generate PO
				{
					generatePO(vData);
					data.setMessage("PO Generated Successfully");
				}
				else if (iGenTx == 2 && vData != null) //generate Request
				{
					generateRQ(vData);
					data.setMessage("RQ Generated Successfully");
				}
				context.put("result",sResult);
			}
			catch (Exception _oEx) 
			{
				_oEx.printStackTrace();
				log.error(_oEx);
				data.setMessage("Generate PO Failed, ERROR: " + _oEx.getMessage());
			}
		}			
	}
    
	StringBuilder sResult = new StringBuilder();
	private void generatePO(List _vSO)
		throws Exception
	{		
		List vID = SqlUtil.toListOfID(_vSO);		
		//map / group by vendor
		Map mVendor = SalesReportTool.createSOVendorMap(vID);
		
		//create PO
		Iterator iter = mVendor.keySet().iterator();
		while(iter.hasNext())
		{
			String sVendorID = (String)iter.next();
			List vItem = (List)mVendor.get(sVendorID);
			String sMsg = PurchaseOrderTool.createPO(sVendorID, sLocID, vItem, sUserName, sRemark);
			sResult.append(sMsg);
		}		
	}    	 

	private void generateRQ(List _vSO)
		throws Exception
	{		
		List vLoc = BeanUtil.getDistinctListByField(_vSO, "locationId");
		for (int i = 0; i < vLoc.size(); i++)
		{
			SalesOrder oLoc = (SalesOrder) vLoc.get(i);
			List vByLoc = BeanUtil.filterListByFieldValue(_vSO, "locationId", oLoc.getLocationId());
			Map mItems = new HashMap();
			for (int j = 0; j < vByLoc.size(); j++)				
			{
				SalesOrder oSO = (SalesOrder) vByLoc.get(j);
				List vSOD = oSO.getSalesOrderDetails();
				for (int k = 0; k < vSOD.size(); k++)				
				{
					SalesOrderDetail oSOD = (SalesOrderDetail) vSOD.get(k);
					double dQty = oSOD.getQty().doubleValue() - oSOD.getDeliveredQty().doubleValue();

					String sUnitID = oSOD.getUnitId();					
					String sItemID = oSOD.getItemId();
					Item oItem = ItemTool.getItemByID(sItemID);
					if(oItem != null && StringUtil.isEqual(sUnitID, oItem.getPurchaseUnitId()))
					{
						dQty = dQty * oItem.getUnitConversion().doubleValue();
					}					
					if (dQty > 0)
					{
						TemplateUtil.addToMap(mItems, sItemID, dQty);
					}
				}
			}			
			String sMsg = PurchaseRequestTool.createRQ(oLoc.getLocationId(), sUserName, sRemark, mItems);
			sResult.append(sMsg);
		}	
	}    	 
}
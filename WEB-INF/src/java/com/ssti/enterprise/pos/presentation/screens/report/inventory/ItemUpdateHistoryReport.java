package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.ItemHistoryTool;

public class ItemUpdateHistoryReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		
	    	//String sStart       = data.getParameters().getString("StartDate"));
	    	//String sEnd         = data.getParameters().getString("EndDate"));
	    	//String sKeywords    = data.getParameters().getString("Keywords"));
	    	//String sItemID      = data.getParameters().getString("ItemId"));
	    	//String sUpdatePart  = data.getParameters().getString("UpdatePart"));
			//int _iCondition     = data.getParameters().getInt("Condition"));
			//int _iSortBy        = data.getParameters().getInt("SortBy"));
			//
			//List vData = 
			context.put ("itemhistory", ItemHistoryTool.getInstance());
			context.put ("vFieldNames", Item.getFieldNames());	    	
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Data LookUp Failed !");
    		log.error(_oEx);
    	}    
    }    
}

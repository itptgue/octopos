package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SalesTransactionDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SalesTransactionDetailMapBuilder";

    /**
     * Item
     * @deprecated use SalesTransactionDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "sales_transaction_detail";
    }

  
    /**
     * sales_transaction_detail.SALES_TRANSACTION_DETAIL_ID
     * @return the column name for the SALES_TRANSACTION_DETAIL_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.SALES_TRANSACTION_DETAIL_ID constant
     */
    public static String getSalesTransactionDetail_SalesTransactionDetailId()
    {
        return "sales_transaction_detail.SALES_TRANSACTION_DETAIL_ID";
    }
  
    /**
     * sales_transaction_detail.SALES_ORDER_ID
     * @return the column name for the SALES_ORDER_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.SALES_ORDER_ID constant
     */
    public static String getSalesTransactionDetail_SalesOrderId()
    {
        return "sales_transaction_detail.SALES_ORDER_ID";
    }
  
    /**
     * sales_transaction_detail.DELIVERY_ORDER_ID
     * @return the column name for the DELIVERY_ORDER_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.DELIVERY_ORDER_ID constant
     */
    public static String getSalesTransactionDetail_DeliveryOrderId()
    {
        return "sales_transaction_detail.DELIVERY_ORDER_ID";
    }
  
    /**
     * sales_transaction_detail.DELIVERY_ORDER_DETAIL_ID
     * @return the column name for the DELIVERY_ORDER_DETAIL_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.DELIVERY_ORDER_DETAIL_ID constant
     */
    public static String getSalesTransactionDetail_DeliveryOrderDetailId()
    {
        return "sales_transaction_detail.DELIVERY_ORDER_DETAIL_ID";
    }
  
    /**
     * sales_transaction_detail.SALES_TRANSACTION_ID
     * @return the column name for the SALES_TRANSACTION_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.SALES_TRANSACTION_ID constant
     */
    public static String getSalesTransactionDetail_SalesTransactionId()
    {
        return "sales_transaction_detail.SALES_TRANSACTION_ID";
    }
  
    /**
     * sales_transaction_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.INDEX_NO constant
     */
    public static String getSalesTransactionDetail_IndexNo()
    {
        return "sales_transaction_detail.INDEX_NO";
    }
  
    /**
     * sales_transaction_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.ITEM_ID constant
     */
    public static String getSalesTransactionDetail_ItemId()
    {
        return "sales_transaction_detail.ITEM_ID";
    }
  
    /**
     * sales_transaction_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.ITEM_CODE constant
     */
    public static String getSalesTransactionDetail_ItemCode()
    {
        return "sales_transaction_detail.ITEM_CODE";
    }
  
    /**
     * sales_transaction_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.ITEM_NAME constant
     */
    public static String getSalesTransactionDetail_ItemName()
    {
        return "sales_transaction_detail.ITEM_NAME";
    }
  
    /**
     * sales_transaction_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.DESCRIPTION constant
     */
    public static String getSalesTransactionDetail_Description()
    {
        return "sales_transaction_detail.DESCRIPTION";
    }
  
    /**
     * sales_transaction_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.UNIT_ID constant
     */
    public static String getSalesTransactionDetail_UnitId()
    {
        return "sales_transaction_detail.UNIT_ID";
    }
  
    /**
     * sales_transaction_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.UNIT_CODE constant
     */
    public static String getSalesTransactionDetail_UnitCode()
    {
        return "sales_transaction_detail.UNIT_CODE";
    }
  
    /**
     * sales_transaction_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.QTY constant
     */
    public static String getSalesTransactionDetail_Qty()
    {
        return "sales_transaction_detail.QTY";
    }
  
    /**
     * sales_transaction_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.QTY_BASE constant
     */
    public static String getSalesTransactionDetail_QtyBase()
    {
        return "sales_transaction_detail.QTY_BASE";
    }
  
    /**
     * sales_transaction_detail.RETURNED_QTY
     * @return the column name for the RETURNED_QTY field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.RETURNED_QTY constant
     */
    public static String getSalesTransactionDetail_ReturnedQty()
    {
        return "sales_transaction_detail.RETURNED_QTY";
    }
  
    /**
     * sales_transaction_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.ITEM_PRICE constant
     */
    public static String getSalesTransactionDetail_ItemPrice()
    {
        return "sales_transaction_detail.ITEM_PRICE";
    }
  
    /**
     * sales_transaction_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.TAX_ID constant
     */
    public static String getSalesTransactionDetail_TaxId()
    {
        return "sales_transaction_detail.TAX_ID";
    }
  
    /**
     * sales_transaction_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.TAX_AMOUNT constant
     */
    public static String getSalesTransactionDetail_TaxAmount()
    {
        return "sales_transaction_detail.TAX_AMOUNT";
    }
  
    /**
     * sales_transaction_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.SUB_TOTAL_TAX constant
     */
    public static String getSalesTransactionDetail_SubTotalTax()
    {
        return "sales_transaction_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * sales_transaction_detail.DISCOUNT_ID
     * @return the column name for the DISCOUNT_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.DISCOUNT_ID constant
     */
    public static String getSalesTransactionDetail_DiscountId()
    {
        return "sales_transaction_detail.DISCOUNT_ID";
    }
  
    /**
     * sales_transaction_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.DISCOUNT constant
     */
    public static String getSalesTransactionDetail_Discount()
    {
        return "sales_transaction_detail.DISCOUNT";
    }
  
    /**
     * sales_transaction_detail.SUB_TOTAL_DISC
     * @return the column name for the SUB_TOTAL_DISC field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.SUB_TOTAL_DISC constant
     */
    public static String getSalesTransactionDetail_SubTotalDisc()
    {
        return "sales_transaction_detail.SUB_TOTAL_DISC";
    }
  
    /**
     * sales_transaction_detail.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.ITEM_COST constant
     */
    public static String getSalesTransactionDetail_ItemCost()
    {
        return "sales_transaction_detail.ITEM_COST";
    }
  
    /**
     * sales_transaction_detail.SUB_TOTAL_COST
     * @return the column name for the SUB_TOTAL_COST field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.SUB_TOTAL_COST constant
     */
    public static String getSalesTransactionDetail_SubTotalCost()
    {
        return "sales_transaction_detail.SUB_TOTAL_COST";
    }
  
    /**
     * sales_transaction_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.SUB_TOTAL constant
     */
    public static String getSalesTransactionDetail_SubTotal()
    {
        return "sales_transaction_detail.SUB_TOTAL";
    }
  
    /**
     * sales_transaction_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.PROJECT_ID constant
     */
    public static String getSalesTransactionDetail_ProjectId()
    {
        return "sales_transaction_detail.PROJECT_ID";
    }
  
    /**
     * sales_transaction_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.DEPARTMENT_ID constant
     */
    public static String getSalesTransactionDetail_DepartmentId()
    {
        return "sales_transaction_detail.DEPARTMENT_ID";
    }
  
    /**
     * sales_transaction_detail.EMPLOYEE_ID
     * @return the column name for the EMPLOYEE_ID field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.EMPLOYEE_ID constant
     */
    public static String getSalesTransactionDetail_EmployeeId()
    {
        return "sales_transaction_detail.EMPLOYEE_ID";
    }
  
    /**
     * sales_transaction_detail.CHANGED_MANUAL
     * @return the column name for the CHANGED_MANUAL field
     * @deprecated use SalesTransactionDetailPeer.sales_transaction_detail.CHANGED_MANUAL constant
     */
    public static String getSalesTransactionDetail_ChangedManual()
    {
        return "sales_transaction_detail.CHANGED_MANUAL";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("sales_transaction_detail");
        TableMap tMap = dbMap.getTable("sales_transaction_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("sales_transaction_detail.SALES_TRANSACTION_DETAIL_ID", "");
                          tMap.addColumn("sales_transaction_detail.SALES_ORDER_ID", "");
                          tMap.addColumn("sales_transaction_detail.DELIVERY_ORDER_ID", "");
                          tMap.addColumn("sales_transaction_detail.DELIVERY_ORDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "sales_transaction_detail.SALES_TRANSACTION_ID", "" , "sales_transaction" ,
                "sales_transaction_id");
                            tMap.addColumn("sales_transaction_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("sales_transaction_detail.ITEM_ID", "");
                          tMap.addColumn("sales_transaction_detail.ITEM_CODE", "");
                          tMap.addColumn("sales_transaction_detail.ITEM_NAME", "");
                          tMap.addColumn("sales_transaction_detail.DESCRIPTION", "");
                          tMap.addColumn("sales_transaction_detail.UNIT_ID", "");
                          tMap.addColumn("sales_transaction_detail.UNIT_CODE", "");
                            tMap.addColumn("sales_transaction_detail.QTY", bd_ZERO);
                            tMap.addColumn("sales_transaction_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("sales_transaction_detail.RETURNED_QTY", bd_ZERO);
                            tMap.addColumn("sales_transaction_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("sales_transaction_detail.TAX_ID", "");
                            tMap.addColumn("sales_transaction_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_transaction_detail.SUB_TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("sales_transaction_detail.DISCOUNT_ID", "");
                          tMap.addColumn("sales_transaction_detail.DISCOUNT", "");
                            tMap.addColumn("sales_transaction_detail.SUB_TOTAL_DISC", bd_ZERO);
                            tMap.addColumn("sales_transaction_detail.ITEM_COST", bd_ZERO);
                            tMap.addColumn("sales_transaction_detail.SUB_TOTAL_COST", bd_ZERO);
                            tMap.addColumn("sales_transaction_detail.SUB_TOTAL", bd_ZERO);
                          tMap.addColumn("sales_transaction_detail.PROJECT_ID", "");
                          tMap.addColumn("sales_transaction_detail.DEPARTMENT_ID", "");
                          tMap.addColumn("sales_transaction_detail.EMPLOYEE_ID", "");
                          tMap.addColumn("sales_transaction_detail.CHANGED_MANUAL", Boolean.TRUE);
          }
}

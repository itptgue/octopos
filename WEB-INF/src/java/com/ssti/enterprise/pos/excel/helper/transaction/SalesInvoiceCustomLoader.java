package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.6  2009/05/04 01:38:37  albert
 * *** empty log message ***
 *
 * Revision 1.5  2008/08/17 02:17:44  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/04/18 06:46:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SalesInvoiceCustomLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( SalesInvoiceCustomLoader.class );

	private static final String s_START_HEADING = "SOP Number";	
    
    public SalesInvoiceCustomLoader ()
    {
    }
    
	public SalesInvoiceCustomLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
    HSSFSheet oSheetMST = null;
    HSSFSheet oSheetDET = null;
    SalesTransaction oTR = null;
    List vTD = null;
    List vPMT = null;
    
    String sTransNo = "";
    /**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	oSheetMST = oWB.getSheetAt(0);
        oSheetDET = oWB.getSheetAt(1);        
		m_iTotalRows = oSheetMST.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
				
		for (int iRow = 1; iRow < m_iTotalRows; iRow++)
		{
            HSSFRow oRow = oSheetMST.getRow(iRow);
    		if (oRow != null)
    		{
    			sTransNo = getString(oRow, i_START);
    			if (StringUtil.isNotEmpty(sTransNo))
    			{    				
                    log.debug("* TRANS NO: " + sTransNo);               

                    m_bTransError = false;
                    m_iTotalTrans++;

                    oTR = new SalesTransaction();
                    vTD = new ArrayList();
    				vPMT = new ArrayList();
    				mapTrans (oRow, oTR);
                    mapDetails();
    				
    				log.debug("* oTR " + oTR);
                    log.debug("* vTD " + vTD);
                    
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			TransactionTool.setHeaderProperties(oTR, vTD, null);
    		    			if (vPMT == null || vPMT.size() <= 0)
    		    			{
    		    				vPMT = createInvoicePayment(oTR);
    		    			}
    		    			TransactionTool.saveData(oTR, vTD, vPMT);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, SalesTransaction _oTR)
    	throws Exception
    {
        /*
        SOP Number  
        Location Code   
        Customer Number 
        Document Date   
        Salesperson ID  
        User To Enter   
        Credit Limit Type   
        Payment Terms ID    
        Note Index  
        Trade Discount 
        Amount   
        Payment Received    
        Document Amount 
        Tax Amount  
        Currency ID 
        Credit Limit 
        Period Exchange Rate   
        Credit Limit Period Amount  
        Cash Account Number
         */
        
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
        if (isExists()) logError("Trans Already Exist", sTransNo);
        
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = getLocByName(sLocationCode);
    	if (oLoc == null) {logError("Location", sLocationCode);}
    	else {_oTR.setLocationId(oLoc.getLocationId());}
    	
    	String sCustomerCode = getString(_oRow, iCol); iCol++;
    	Customer oCust = CustomerTool.getCustomerByCode(sCustomerCode);
    	if (oCust == null) {logError("Customer Code", sCustomerCode);}
    	else 
    	{
    		_oTR.setCustomerId(oCust.getCustomerId()); 
    		_oTR.setCustomerName(oCust.getCustomerName());
            
            PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(oCust.getDefaultTypeId());
            if (oPT == null) {logError("Payment Type", sCustomerCode);}
            else {_oTR.setPaymentTypeId(oPT.getPaymentTypeId());}
            
            PaymentTerm oPTerm = PaymentTermTool.getPaymentTermByID(oCust.getDefaultTermId());
            if (oPTerm == null) {logError("Payment Term", sCustomerCode);}
            else {_oTR.setPaymentTermId(oPTerm.getPaymentTermId());}
    	}
    	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setTransactionDate(dTrans);}

        String sSales = getString(_oRow, iCol); iCol++;
        Employee oSales = EmployeeTool.getEmployeeByName(sSales);
        if (oSales != null) 
        {
            _oTR.setSalesId(oSales.getEmployeeId());
        }
        else 
        {
            _oTR.setSalesId("");
        }

    	String sCashier = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sCashier)) {logError("Cashier", sCashier);}
    	else {_oTR.setCashierName(sCashier);}
		
        iCol++;
        iCol++;
    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setRemark(sRemark);
    	
    	//String sTotalDiscPct = getString(_oRow, iCol); iCol++;
    	BigDecimal dTotalDiscPct = getBD(_oRow, "Trade Disc", (short)iCol); iCol++; 
    	
    	//if (StringUtil.isEmpty(sTotalDiscPct)) sTotalDiscPct = "0";
        //else sTotalDiscPct = parseNumber("Trade Disc", sTotalDiscPct).toString();
    	_oTR.setTotalDiscountPct(dTotalDiscPct.toString());
    	
    	_oTR.setPaymentAmount(bd_ZERO);
        _oTR.setChangeAmount(bd_ZERO);
    	_oTR.setIsTaxable(true);
    	_oTR.setIsInclusiveTax(true);
        _oTR.setCurrencyId(CurrencyTool.getDefaultCurrency().getCurrencyId());
        _oTR.setCurrencyRate(bd_ONE);
        _oTR.setFiscalRate(bd_ONE);
            	
    	//OTHER FIELD
    	if (!m_bTransError)
    	{
    		_oTR.setInvoiceNo(sTransNo);
    		
	    	_oTR.setStatus(i_PROCESSED);
	    	_oTR.setCourierId("");
	    	_oTR.setShippingPrice(bd_ZERO);
	    	_oTR.setRemark(_oTR.getRemark());
	    	_oTR.setShipTo("");
	    	_oTR.setFobId("");
	    	_oTR.setFreightAccountId("");
	    	
	    	//updated by processPayment
	    	_oTR.setPaidAmount(bd_ZERO); 
	    	_oTR.setPaymentAmount(bd_ZERO); //updated when saved
	    	_oTR.setPaymentDate(_oTR.getTransactionDate()); //updated when saved
	    	_oTR.setDueDate(_oTR.getTransactionDate());
	    	
	    	//currency
	    	if (StringUtil.isEmpty(_oTR.getCurrencyId()))
	    	{
		    	_oTR.setCurrencyId(CurrencyTool.getDefaultCurrency(null).getCurrencyId());
		    	_oTR.setCurrencyRate(bd_ONE);
		    	_oTR.setFiscalRate(bd_ONE);
	    	}
    	}
    }
    
    protected void mapDetails()
		throws Exception
	{    	
        //SOP Number  Item Number QTY U Of M  Unit Price  Markdown Amount

        int iTotalDet = oSheetDET.getPhysicalNumberOfRows();        
        for (int iRow = 1; iRow < iTotalDet; iRow++)
        {
            HSSFRow oRow = oSheetDET.getRow(iRow);
            if (oRow != null)
            {
                String sTransNoDet = getString(oRow, i_START);
                if (StringUtil.isNotEmpty(sTransNoDet) && StringUtil.isEqual(sTransNoDet,sTransNo))
                {                   
                    SalesTransactionDetail oTD = new SalesTransactionDetail();
                    
                    int iCol = 1;
                    String sItemCode = getString (oRow, iCol); iCol++;
                    m_oResult.append(LocaleTool.getString("item_code")).append(": ").append(sItemCode).append(s_LINE_SEPARATOR);
                    
                    Item oItem = ItemTool.getItemByCode(sItemCode);
                    if (oItem == null) {logError("Item Code", sItemCode);}
                    else 
                    {
                        oTD.setItemId(oItem.getItemId());
                        oTD.setItemCode(oItem.getItemCode());
                        oTD.setItemName(oItem.getItemName());
                        oTD.setDescription(oItem.getDescription());
                        
                        Tax oTax = TaxTool.getTaxByID(oItem.getTaxId());
                        if (oTax != null)
                        {
                            oTD.setTaxId(oTax.getTaxId());
                            oTD.setTaxAmount(oTax.getAmount());
                        }                                           
                    }

                    //String sQty = getString(oRow, iCol); iCol++;
                    //BigDecimal dQty = parseNumber("Qty",sQty);
                    BigDecimal dQty = getBD(oRow, "Qty", (short)iCol); iCol++;
                    
                    if (dQty == null) {logError("Qty", dQty);}
                    else 
                    {
                        oTD.setQty(dQty);
                        oTD.setQtyBase(UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), dQty));
                        oTD.setReturnedQty(bd_ZERO);
                    }

                    String sUnitCode = getString (oRow, iCol); iCol++;
                    Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
                    if (oUnit == null) {logError("Unit Code", sUnitCode);}
                    else 
                    {
                        oTD.setUnitId(oUnit.getUnitId());
                        oTD.setUnitCode(oUnit.getUnitCode());
                    }

                    //String sPrice = getString (oRow, iCol); iCol++;
                    BigDecimal dPrice = getBD (oRow, "Item Price", (short)iCol); iCol++;
                    if (dPrice == null) {logError("Item Price", dPrice);}
                    else 
                    {
                        //BigDecimal dPrice = parseNumber("Price ", sPrice);
                        oTD.setItemPrice(dPrice);
                    }
                    
                    //String sDisc = getString(oRow, iCol); iCol++;
                    BigDecimal dDisc = getBD(oRow, "Markdown", (short)iCol); iCol++;
                    String sDisc = "0";
                    if (dDisc == null) 
                    {
                        //sDisc = "0";
                    	oTD.setDiscount(sDisc);
                        oTD.setDiscountId("");
                    }
                    else
                    {
                        //BigDecimal dDisc = parseNumber("Markdown ", sDisc);
                        dDisc = new BigDecimal(dDisc.doubleValue() * dQty.doubleValue());
                        sDisc = dDisc.toString();
                        oTD.setDiscount(sDisc);
                    }
                    oTD.setDepartmentId("");
                    oTD.setProjectId("");

                    //OTHER FIELD
                    oTD.setDeliveryOrderId("");
                    oTD.setDeliveryOrderDetailId("");
                    
                    //CALCULATED FIELD
                    if (!m_bTransError)
                    {
                        double dSubTotal = oTD.getItemPrice().doubleValue() * oTD.getQty().doubleValue();
                        double dSubTotalDisc = Calculator.calculateDiscount(oTD.getDiscount(), dSubTotal);
                        double dSubTotalTax = oTD.getTaxAmount().doubleValue() / 100 * (dSubTotal  - dSubTotalDisc);
                        oTD.setSubTotal(new BigDecimal(dSubTotal));
                        oTD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
                        oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
                        
                        //SKIP COST, will be recalculated when trans saved
                        oTD.setItemCost(bd_ZERO);
                        oTD.setSubTotalCost(bd_ZERO);
                        vTD.add(oTD);
                    }                                        
                }
            }
        }

	}   

//    private BigDecimal parseNumber(String _sField, String _sNumber) 
//        throws Exception
//    {
//        String sResult = "";
//        log.debug("** PARSE NUMBER " + _sField + " " + _sNumber);
//        if (StringUtil.isNotEmpty(_sNumber))
//        {
//            boolean bDecimal = false;
//            sResult = StringUtil.replace(_sNumber,",00","");
//            sResult = StringUtil.replace(sResult,"Rp. ","");
//            sResult = StringUtil.replace(sResult,".","");
//            if (StringUtil.contains(sResult,","))
//            {
//                sResult = StringUtil.replace(sResult,",",".");
//                bDecimal = true;
//            }
//            log.debug("** PARSE NUMBER " + _sField + " " + sResult);
//            try
//            {
//                BigDecimal bdResult = new BigDecimal(sResult);  
//                bdResult = bdResult.setScale(2,4);                    
//                return bdResult;
//            }
//            catch (Exception e)
//            {
//                logError(_sField,_sNumber);
//            }
//        }
//        return bd_ZERO;
//    }

    protected List createInvoicePayment (SalesTransaction _oTR)
		throws Exception
	{
    	List vPmt = new ArrayList (1);
        InvoicePayment oInv 	 = new InvoicePayment();
        oInv.setPaymentTypeId    (_oTR.getPaymentTypeId());
        oInv.setPaymentTermId    (_oTR.getPaymentTermId());
        oInv.setPaymentAmount    (_oTR.getTotalAmount());
        oInv.setTransTotalAmount (_oTR.getTotalAmount());
        oInv.setVoucherId        ("");
        oInv.setReferenceNo      ("");           
        vPmt.add (oInv);
        return vPmt;
	}
    
    public Location getLocByName(String _sName)
        throws Exception
    {        
        if (StringUtil.isNotEmpty(_sName))
        {
            Criteria oCrit = new Criteria();
            oCrit.add(LocationPeer.LOCATION_NAME, _sName);
            List vData = LocationPeer.doSelect(oCrit);
            if (vData.size() > 0) {
                return (Location) vData.get(0);
            }
        }
        return null;
    }
    
    public boolean isExists()
        throws Exception
    {        
        if (StringUtil.isNotEmpty(sTransNo))
        {
            Criteria oCrit = new Criteria();
            oCrit.add(SalesTransactionPeer.INVOICE_NO, sTransNo);
            oCrit.add(SalesTransactionPeer.STATUS, i_PROCESSED);            
            List vData = SalesTransactionPeer.doSelect(oCrit);
            if (vData.size() > 0) 
            {
                return true;
            }
        }
        return false;
    }
    
    protected BigDecimal getBD(HSSFRow _oRow, String _sField, short _iColumn)
    	throws Exception
    {                                
        if (_oRow != null)
        {                
    	    HSSFCell oCell = _oRow.getCell(_iColumn);
    	    if (oCell != null)
    	    {
    	        if (oCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
    	        {
    	        	return new BigDecimal (oCell.getNumericCellValue());
    	        }
    	        else if (oCell.getCellType() == HSSFCell.CELL_TYPE_STRING)
    	        {
    	        	String sValue = "";
    	        	try 
					{
    	        		sValue = oCell.getStringCellValue();
    	        		return new BigDecimal (StringUtil.trim(sValue));
    	        	}
    	        	catch(NumberFormatException _oNFEx)
    	        	{
    	        		String sError = "ERROR parsing NUMERIC CELL ROW: " +  _oRow.getRowNum() + 
    	        				        " COL:" + _iColumn + " VALUE:" + sValue;
    	        		log.error(sError , _oNFEx);
    	        	
    	        		logError(_sField, sValue);
    	        		//throw new Exception(sError);
    	        		return Attributes.bd_ZERO;                                    
    	        	}
    	        }
            }
        }
    	return Attributes.bd_ZERO;                                    
    } 
}

package com.ssti.enterprise.medical.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PrescriptionDetail
 */
public abstract class BasePrescriptionDetail extends BaseObject
{
    /** The Peer class */
    private static final PrescriptionDetailPeer peer =
        new PrescriptionDetailPeer();

        
    /** The value for the prescriptionDetailId field */
    private String prescriptionDetailId;
      
    /** The value for the prescriptionId field */
    private String prescriptionId;
      
    /** The value for the prescriptionType field */
    private int prescriptionType;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
                                                
          
    /** The value for the reqQty field */
    private BigDecimal reqQty= bd_ZERO;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
      
    /** The value for the itemPrice field */
    private BigDecimal itemPrice;
      
    /** The value for the taxId field */
    private String taxId;
      
    /** The value for the taxAmount field */
    private BigDecimal taxAmount;
      
    /** The value for the subTotal field */
    private BigDecimal subTotal;
      
    /** The value for the rfee field */
    private String rfee;
      
    /** The value for the packaging field */
    private String packaging;
      
    /** The value for the total field */
    private BigDecimal total;
                                                
    /** The value for the dosage field */
    private String dosage = "";
                                                
    /** The value for the beforeAfter field */
    private String beforeAfter = "";
                                                
    /** The value for the instruction field */
    private String instruction = "";
                                                
    /** The value for the powderItemId field */
    private String powderItemId = "";
                                                
          
    /** The value for the resultQty field */
    private BigDecimal resultQty= new BigDecimal(1);
                                                
    /** The value for the batchNo field */
    private String batchNo = "";
                                                
    /** The value for the presFile field */
    private String presFile = "";
  
    
    /**
     * Get the PrescriptionDetailId
     *
     * @return String
     */
    public String getPrescriptionDetailId()
    {
        return prescriptionDetailId;
    }

                                              
    /**
     * Set the value of PrescriptionDetailId
     *
     * @param v new value
     */
    public void setPrescriptionDetailId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.prescriptionDetailId, v))
              {
            this.prescriptionDetailId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PresPowderDetail
        if (collPresPowderDetails != null)
        {
            for (int i = 0; i < collPresPowderDetails.size(); i++)
            {
                ((PresPowderDetail) collPresPowderDetails.get(i))
                    .setPrescriptionDetailId(v);
            }
        }
                                }
  
    /**
     * Get the PrescriptionId
     *
     * @return String
     */
    public String getPrescriptionId()
    {
        return prescriptionId;
    }

                              
    /**
     * Set the value of PrescriptionId
     *
     * @param v new value
     */
    public void setPrescriptionId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.prescriptionId, v))
              {
            this.prescriptionId = v;
            setModified(true);
        }
    
                          
                if (aPrescription != null && !ObjectUtils.equals(aPrescription.getPrescriptionId(), v))
                {
            aPrescription = null;
        }
      
              }
  
    /**
     * Get the PrescriptionType
     *
     * @return int
     */
    public int getPrescriptionType()
    {
        return prescriptionType;
    }

                        
    /**
     * Set the value of PrescriptionType
     *
     * @param v new value
     */
    public void setPrescriptionType(int v) 
    {
    
                  if (this.prescriptionType != v)
              {
            this.prescriptionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReqQty
     *
     * @return BigDecimal
     */
    public BigDecimal getReqQty()
    {
        return reqQty;
    }

                        
    /**
     * Set the value of ReqQty
     *
     * @param v new value
     */
    public void setReqQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.reqQty, v))
              {
            this.reqQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

                        
    /**
     * Set the value of ItemPrice
     *
     * @param v new value
     */
    public void setItemPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPrice, v))
              {
            this.itemPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxId
     *
     * @return String
     */
    public String getTaxId()
    {
        return taxId;
    }

                        
    /**
     * Set the value of TaxId
     *
     * @param v new value
     */
    public void setTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxId, v))
              {
            this.taxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTaxAmount()
    {
        return taxAmount;
    }

                        
    /**
     * Set the value of TaxAmount
     *
     * @param v new value
     */
    public void setTaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.taxAmount, v))
              {
            this.taxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotal
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotal()
    {
        return subTotal;
    }

                        
    /**
     * Set the value of SubTotal
     *
     * @param v new value
     */
    public void setSubTotal(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotal, v))
              {
            this.subTotal = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Rfee
     *
     * @return String
     */
    public String getRfee()
    {
        return rfee;
    }

                        
    /**
     * Set the value of Rfee
     *
     * @param v new value
     */
    public void setRfee(String v) 
    {
    
                  if (!ObjectUtils.equals(this.rfee, v))
              {
            this.rfee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Packaging
     *
     * @return String
     */
    public String getPackaging()
    {
        return packaging;
    }

                        
    /**
     * Set the value of Packaging
     *
     * @param v new value
     */
    public void setPackaging(String v) 
    {
    
                  if (!ObjectUtils.equals(this.packaging, v))
              {
            this.packaging = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Total
     *
     * @return BigDecimal
     */
    public BigDecimal getTotal()
    {
        return total;
    }

                        
    /**
     * Set the value of Total
     *
     * @param v new value
     */
    public void setTotal(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.total, v))
              {
            this.total = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Dosage
     *
     * @return String
     */
    public String getDosage()
    {
        return dosage;
    }

                        
    /**
     * Set the value of Dosage
     *
     * @param v new value
     */
    public void setDosage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.dosage, v))
              {
            this.dosage = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BeforeAfter
     *
     * @return String
     */
    public String getBeforeAfter()
    {
        return beforeAfter;
    }

                        
    /**
     * Set the value of BeforeAfter
     *
     * @param v new value
     */
    public void setBeforeAfter(String v) 
    {
    
                  if (!ObjectUtils.equals(this.beforeAfter, v))
              {
            this.beforeAfter = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Instruction
     *
     * @return String
     */
    public String getInstruction()
    {
        return instruction;
    }

                        
    /**
     * Set the value of Instruction
     *
     * @param v new value
     */
    public void setInstruction(String v) 
    {
    
                  if (!ObjectUtils.equals(this.instruction, v))
              {
            this.instruction = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PowderItemId
     *
     * @return String
     */
    public String getPowderItemId()
    {
        return powderItemId;
    }

                        
    /**
     * Set the value of PowderItemId
     *
     * @param v new value
     */
    public void setPowderItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.powderItemId, v))
              {
            this.powderItemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ResultQty
     *
     * @return BigDecimal
     */
    public BigDecimal getResultQty()
    {
        return resultQty;
    }

                        
    /**
     * Set the value of ResultQty
     *
     * @param v new value
     */
    public void setResultQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.resultQty, v))
              {
            this.resultQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BatchNo
     *
     * @return String
     */
    public String getBatchNo()
    {
        return batchNo;
    }

                        
    /**
     * Set the value of BatchNo
     *
     * @param v new value
     */
    public void setBatchNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.batchNo, v))
              {
            this.batchNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PresFile
     *
     * @return String
     */
    public String getPresFile()
    {
        return presFile;
    }

                        
    /**
     * Set the value of PresFile
     *
     * @param v new value
     */
    public void setPresFile(String v) 
    {
    
                  if (!ObjectUtils.equals(this.presFile, v))
              {
            this.presFile = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Prescription aPrescription;

    /**
     * Declares an association between this object and a Prescription object
     *
     * @param v Prescription
     * @throws TorqueException
     */
    public void setPrescription(Prescription v) throws TorqueException
    {
            if (v == null)
        {
                  setPrescriptionId((String) null);
              }
        else
        {
            setPrescriptionId(v.getPrescriptionId());
        }
            aPrescription = v;
    }

                                            
    /**
     * Get the associated Prescription object
     *
     * @return the associated Prescription object
     * @throws TorqueException
     */
    public Prescription getPrescription() throws TorqueException
    {
        if (aPrescription == null && (!ObjectUtils.equals(this.prescriptionId, null)))
        {
                          aPrescription = PrescriptionPeer.retrieveByPK(SimpleKey.keyFor(this.prescriptionId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Prescription obj = PrescriptionPeer.retrieveByPK(this.prescriptionId);
               obj.addPrescriptionDetails(this);
            */
        }
        return aPrescription;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPrescriptionKey(ObjectKey key) throws TorqueException
    {
      
                        setPrescriptionId(key.toString());
                  }
       
                                
            
          /**
     * Collection to store aggregation of collPresPowderDetails
     */
    protected List collPresPowderDetails;

    /**
     * Temporary storage of collPresPowderDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPresPowderDetails()
    {
        if (collPresPowderDetails == null)
        {
            collPresPowderDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a PresPowderDetail object to this object
     * through the PresPowderDetail foreign key attribute
     *
     * @param l PresPowderDetail
     * @throws TorqueException
     */
    public void addPresPowderDetail(PresPowderDetail l) throws TorqueException
    {
        getPresPowderDetails().add(l);
        l.setPrescriptionDetail((PrescriptionDetail) this);
    }

    /**
     * The criteria used to select the current contents of collPresPowderDetails
     */
    private Criteria lastPresPowderDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPresPowderDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPresPowderDetails() throws TorqueException
    {
              if (collPresPowderDetails == null)
        {
            collPresPowderDetails = getPresPowderDetails(new Criteria(10));
        }
        return collPresPowderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PrescriptionDetail has previously
     * been saved, it will retrieve related PresPowderDetails from storage.
     * If this PrescriptionDetail is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPresPowderDetails(Criteria criteria) throws TorqueException
    {
              if (collPresPowderDetails == null)
        {
            if (isNew())
            {
               collPresPowderDetails = new ArrayList();
            }
            else
            {
                        criteria.add(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID, getPrescriptionDetailId() );
                        collPresPowderDetails = PresPowderDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID, getPrescriptionDetailId());
                            if (!lastPresPowderDetailsCriteria.equals(criteria))
                {
                    collPresPowderDetails = PresPowderDetailPeer.doSelect(criteria);
                }
            }
        }
        lastPresPowderDetailsCriteria = criteria;

        return collPresPowderDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPresPowderDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPresPowderDetails(Connection con) throws TorqueException
    {
              if (collPresPowderDetails == null)
        {
            collPresPowderDetails = getPresPowderDetails(new Criteria(10), con);
        }
        return collPresPowderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PrescriptionDetail has previously
     * been saved, it will retrieve related PresPowderDetails from storage.
     * If this PrescriptionDetail is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPresPowderDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPresPowderDetails == null)
        {
            if (isNew())
            {
               collPresPowderDetails = new ArrayList();
            }
            else
            {
                         criteria.add(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID, getPrescriptionDetailId());
                         collPresPowderDetails = PresPowderDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID, getPrescriptionDetailId());
                             if (!lastPresPowderDetailsCriteria.equals(criteria))
                 {
                     collPresPowderDetails = PresPowderDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPresPowderDetailsCriteria = criteria;

         return collPresPowderDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PrescriptionDetail is new, it will return
     * an empty collection; or if this PrescriptionDetail has previously
     * been saved, it will retrieve related PresPowderDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PrescriptionDetail.
     */
    protected List getPresPowderDetailsJoinPrescriptionDetail(Criteria criteria)
        throws TorqueException
    {
                    if (collPresPowderDetails == null)
        {
            if (isNew())
            {
               collPresPowderDetails = new ArrayList();
            }
            else
            {
                              criteria.add(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID, getPrescriptionDetailId());
                              collPresPowderDetails = PresPowderDetailPeer.doSelectJoinPrescriptionDetail(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID, getPrescriptionDetailId());
                                    if (!lastPresPowderDetailsCriteria.equals(criteria))
            {
                collPresPowderDetails = PresPowderDetailPeer.doSelectJoinPrescriptionDetail(criteria);
            }
        }
        lastPresPowderDetailsCriteria = criteria;

        return collPresPowderDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PrescriptionDetailId");
              fieldNames.add("PrescriptionId");
              fieldNames.add("PrescriptionType");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("Description");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("ReqQty");
              fieldNames.add("Qty");
              fieldNames.add("QtyBase");
              fieldNames.add("ItemPrice");
              fieldNames.add("TaxId");
              fieldNames.add("TaxAmount");
              fieldNames.add("SubTotal");
              fieldNames.add("Rfee");
              fieldNames.add("Packaging");
              fieldNames.add("Total");
              fieldNames.add("Dosage");
              fieldNames.add("BeforeAfter");
              fieldNames.add("Instruction");
              fieldNames.add("PowderItemId");
              fieldNames.add("ResultQty");
              fieldNames.add("BatchNo");
              fieldNames.add("PresFile");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PrescriptionDetailId"))
        {
                return getPrescriptionDetailId();
            }
          if (name.equals("PrescriptionId"))
        {
                return getPrescriptionId();
            }
          if (name.equals("PrescriptionType"))
        {
                return Integer.valueOf(getPrescriptionType());
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("ReqQty"))
        {
                return getReqQty();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("ItemPrice"))
        {
                return getItemPrice();
            }
          if (name.equals("TaxId"))
        {
                return getTaxId();
            }
          if (name.equals("TaxAmount"))
        {
                return getTaxAmount();
            }
          if (name.equals("SubTotal"))
        {
                return getSubTotal();
            }
          if (name.equals("Rfee"))
        {
                return getRfee();
            }
          if (name.equals("Packaging"))
        {
                return getPackaging();
            }
          if (name.equals("Total"))
        {
                return getTotal();
            }
          if (name.equals("Dosage"))
        {
                return getDosage();
            }
          if (name.equals("BeforeAfter"))
        {
                return getBeforeAfter();
            }
          if (name.equals("Instruction"))
        {
                return getInstruction();
            }
          if (name.equals("PowderItemId"))
        {
                return getPowderItemId();
            }
          if (name.equals("ResultQty"))
        {
                return getResultQty();
            }
          if (name.equals("BatchNo"))
        {
                return getBatchNo();
            }
          if (name.equals("PresFile"))
        {
                return getPresFile();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PrescriptionDetailPeer.PRESCRIPTION_DETAIL_ID))
        {
                return getPrescriptionDetailId();
            }
          if (name.equals(PrescriptionDetailPeer.PRESCRIPTION_ID))
        {
                return getPrescriptionId();
            }
          if (name.equals(PrescriptionDetailPeer.PRESCRIPTION_TYPE))
        {
                return Integer.valueOf(getPrescriptionType());
            }
          if (name.equals(PrescriptionDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(PrescriptionDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(PrescriptionDetailPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PrescriptionDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(PrescriptionDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(PrescriptionDetailPeer.REQ_QTY))
        {
                return getReqQty();
            }
          if (name.equals(PrescriptionDetailPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(PrescriptionDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(PrescriptionDetailPeer.ITEM_PRICE))
        {
                return getItemPrice();
            }
          if (name.equals(PrescriptionDetailPeer.TAX_ID))
        {
                return getTaxId();
            }
          if (name.equals(PrescriptionDetailPeer.TAX_AMOUNT))
        {
                return getTaxAmount();
            }
          if (name.equals(PrescriptionDetailPeer.SUB_TOTAL))
        {
                return getSubTotal();
            }
          if (name.equals(PrescriptionDetailPeer.RFEE))
        {
                return getRfee();
            }
          if (name.equals(PrescriptionDetailPeer.PACKAGING))
        {
                return getPackaging();
            }
          if (name.equals(PrescriptionDetailPeer.TOTAL))
        {
                return getTotal();
            }
          if (name.equals(PrescriptionDetailPeer.DOSAGE))
        {
                return getDosage();
            }
          if (name.equals(PrescriptionDetailPeer.BEFORE_AFTER))
        {
                return getBeforeAfter();
            }
          if (name.equals(PrescriptionDetailPeer.INSTRUCTION))
        {
                return getInstruction();
            }
          if (name.equals(PrescriptionDetailPeer.POWDER_ITEM_ID))
        {
                return getPowderItemId();
            }
          if (name.equals(PrescriptionDetailPeer.RESULT_QTY))
        {
                return getResultQty();
            }
          if (name.equals(PrescriptionDetailPeer.BATCH_NO))
        {
                return getBatchNo();
            }
          if (name.equals(PrescriptionDetailPeer.PRES_FILE))
        {
                return getPresFile();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPrescriptionDetailId();
            }
              if (pos == 1)
        {
                return getPrescriptionId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getPrescriptionType());
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getDescription();
            }
              if (pos == 6)
        {
                return getUnitId();
            }
              if (pos == 7)
        {
                return getUnitCode();
            }
              if (pos == 8)
        {
                return getReqQty();
            }
              if (pos == 9)
        {
                return getQty();
            }
              if (pos == 10)
        {
                return getQtyBase();
            }
              if (pos == 11)
        {
                return getItemPrice();
            }
              if (pos == 12)
        {
                return getTaxId();
            }
              if (pos == 13)
        {
                return getTaxAmount();
            }
              if (pos == 14)
        {
                return getSubTotal();
            }
              if (pos == 15)
        {
                return getRfee();
            }
              if (pos == 16)
        {
                return getPackaging();
            }
              if (pos == 17)
        {
                return getTotal();
            }
              if (pos == 18)
        {
                return getDosage();
            }
              if (pos == 19)
        {
                return getBeforeAfter();
            }
              if (pos == 20)
        {
                return getInstruction();
            }
              if (pos == 21)
        {
                return getPowderItemId();
            }
              if (pos == 22)
        {
                return getResultQty();
            }
              if (pos == 23)
        {
                return getBatchNo();
            }
              if (pos == 24)
        {
                return getPresFile();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PrescriptionDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PrescriptionDetailPeer.doInsert((PrescriptionDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PrescriptionDetailPeer.doUpdate((PrescriptionDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPresPowderDetails != null)
            {
                for (int i = 0; i < collPresPowderDetails.size(); i++)
                {
                    ((PresPowderDetail) collPresPowderDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key prescriptionDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPrescriptionDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPrescriptionDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPrescriptionDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PrescriptionDetail copy() throws TorqueException
    {
        return copyInto(new PrescriptionDetail());
    }
  
    protected PrescriptionDetail copyInto(PrescriptionDetail copyObj) throws TorqueException
    {
          copyObj.setPrescriptionDetailId(prescriptionDetailId);
          copyObj.setPrescriptionId(prescriptionId);
          copyObj.setPrescriptionType(prescriptionType);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setDescription(description);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setReqQty(reqQty);
          copyObj.setQty(qty);
          copyObj.setQtyBase(qtyBase);
          copyObj.setItemPrice(itemPrice);
          copyObj.setTaxId(taxId);
          copyObj.setTaxAmount(taxAmount);
          copyObj.setSubTotal(subTotal);
          copyObj.setRfee(rfee);
          copyObj.setPackaging(packaging);
          copyObj.setTotal(total);
          copyObj.setDosage(dosage);
          copyObj.setBeforeAfter(beforeAfter);
          copyObj.setInstruction(instruction);
          copyObj.setPowderItemId(powderItemId);
          copyObj.setResultQty(resultQty);
          copyObj.setBatchNo(batchNo);
          copyObj.setPresFile(presFile);
  
                    copyObj.setPrescriptionDetailId((String)null);
                                                                                                                                                            
                                      
                            
        List v = getPresPowderDetails();
        for (int i = 0; i < v.size(); i++)
        {
            PresPowderDetail obj = (PresPowderDetail) v.get(i);
            copyObj.addPresPowderDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PrescriptionDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PrescriptionDetail\n");
        str.append("------------------\n")
           .append("PrescriptionDetailId   : ")
           .append(getPrescriptionDetailId())
           .append("\n")
           .append("PrescriptionId       : ")
           .append(getPrescriptionId())
           .append("\n")
           .append("PrescriptionType     : ")
           .append(getPrescriptionType())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("ReqQty               : ")
           .append(getReqQty())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("ItemPrice            : ")
           .append(getItemPrice())
           .append("\n")
           .append("TaxId                : ")
           .append(getTaxId())
           .append("\n")
           .append("TaxAmount            : ")
           .append(getTaxAmount())
           .append("\n")
           .append("SubTotal             : ")
           .append(getSubTotal())
           .append("\n")
           .append("Rfee                 : ")
           .append(getRfee())
           .append("\n")
           .append("Packaging            : ")
           .append(getPackaging())
           .append("\n")
           .append("Total                : ")
           .append(getTotal())
           .append("\n")
           .append("Dosage               : ")
           .append(getDosage())
           .append("\n")
           .append("BeforeAfter          : ")
           .append(getBeforeAfter())
           .append("\n")
           .append("Instruction          : ")
           .append(getInstruction())
           .append("\n")
           .append("PowderItemId         : ")
           .append(getPowderItemId())
           .append("\n")
           .append("ResultQty            : ")
           .append(getResultQty())
           .append("\n")
           .append("BatchNo              : ")
           .append(getBatchNo())
           .append("\n")
           .append("PresFile             : ")
           .append(getPresFile())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.tools.sales;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.torque.util.Transaction;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.DeliveryOrderDetailPeer;
import com.ssti.enterprise.pos.om.DeliveryOrderPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.om.SalesOrderDetailPeer;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.enterprise.pos.tools.journal.SalesJournalTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.IDGenerator;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/sales/DeliveryOrderTool.java,v $
 * Purpose: Business object controller for DeliveryOrder OM
 *
 * @author  $Author: albert $
 * @version $Id: DeliveryOrderTool.java,v 1.10 2006/03/22 15:46:55 albert Exp $
 * @see DeliveryOrder
 *
 * $Log: DeliveryOrderTool.java,v $
 * Revision 1.10  2006/03/22 15:46:55  albert
 * *** empty log message ***
 *
 * Revision 1.9  2006/01/21 12:02:09  albert
 * *** empty log message ***
 *
 * Revision 1.8  2006/01/02 09:53:36  albert
 * *** empty log message ***
 *
 */

public class DeliveryOrderTool extends BaseTool 
{	
	private static Log log = LogFactory.getLog(DeliveryOrderTool.class);

	public static final String s_ITEM = new StringBuilder(DeliveryOrderPeer.DELIVERY_ORDER_ID).append(",")
	.append(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID).append(",")
	.append(DeliveryOrderDetailPeer.ITEM_ID).toString();
	
	public static final String s_TAX = new StringBuilder(DeliveryOrderPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.TAX_ID).toString();
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), DeliveryOrderPeer.DELIVERY_ORDER_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), DeliveryOrderPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER), 	 DeliveryOrderPeer.CUSTOMER_ID);                  
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), DeliveryOrderPeer.CREATE_BY);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), DeliveryOrderPeer.CONFIRM_BY);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), DeliveryOrderPeer.LOCATION_ID);             
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), "");
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), "");
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), DeliveryOrderPeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), DeliveryOrderPeer.FOB_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), DeliveryOrderPeer.CURRENCY_ID);
		
		addInv(m_FIND_PEER, s_ITEM);
	}   
	
	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), DeliveryOrderPeer.DELIVERY_ORDER_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 	
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), DeliveryOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), DeliveryOrderPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), DeliveryOrderPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), DeliveryOrderPeer.CREATE_BY);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), DeliveryOrderPeer.CONFIRM_BY);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), "");
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), DeliveryOrderPeer.CURRENCY_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALESMAN   ), DeliveryOrderPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), "");
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), DeliveryOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), DeliveryOrderPeer.LOCATION_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALES_ITEM ), DeliveryOrderPeer.SALES_ID); 
	}	
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, _iSelected);}
		
	public static DeliveryOrder getHeaderByID(String _sID) 
		throws Exception
	{
		return getHeaderByID (_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static DeliveryOrder getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderPeer.DELIVERY_ORDER_ID, _sID);
        List vData = DeliveryOrderPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (DeliveryOrder) vData.get(0);
		}
		return null;
	}
	
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
		return getDetailsByID(_sID, null);
	}

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, _sID);
	    return DeliveryOrderDetailPeer.doSelect(oCrit, _oConn);
	}

	public static DeliveryOrderDetail getDetailByDetailID(String _sID) 
		throws Exception
	{
	    return getDetailByDetailID(_sID, null);
	}		

	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static DeliveryOrderDetail getDetailByDetailID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_DETAIL_ID, _sID);
	    List vData = DeliveryOrderDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) return (DeliveryOrderDetail) vData.get(0);
	    return null;
	}		
	
	/**
	 * used by SO Report
	 * 
	 * @param _sSOID
	 * @return
	 * @throws Exception
	 */
	public static List getDetailsBySODetailID(String _sSOID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(DeliveryOrderDetailPeer.SALES_ORDER_DETAIL_ID,_sSOID);
		List vData = DeliveryOrderDetailPeer.doSelect (oCrit);
		return vData;
	}
	
	/**
	 * used by so to validate whether this SOID exist in any active DO
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getDOBySOID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderPeer.SALES_ORDER_ID, _sID);
        oCrit.add(DeliveryOrderPeer.STATUS, i_DO_CANCELLED, Criteria.NOT_EQUAL);
        return DeliveryOrderPeer.doSelect(oCrit,_oConn);
	}

	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_DO_NEW) return LocaleTool.getString("new");
		if (_iStatusID == i_DO_DELIVERED) return LocaleTool.getString("confirmed");
		if (_iStatusID == i_DO_PAID) return LocaleTool.getString("paid");
		return LocaleTool.getString("cancelled");
	}


	/** 
	 * method to check Paid Status on DO 
	 * foreach details in a DO, get all SI Detail by DO ID
	 * count number of item in SI whether >= Qty in DO
	 * if true then all item in DO already PAID
	 */
	private static boolean checkPaidStatus(String _sDOID, Connection _oConn)
        throws Exception
	{
		List vDODetail = getDetailsByID(_sDOID, _oConn);
		List vSIDetail = TransactionTool.getDetailsByDOID(_sDOID, _oConn);

		for(int i = 0; i < vDODetail.size(); i++)
		{
			double dTotal = 0;
			double dReturned = 0;
			DeliveryOrderDetail oDODet = (DeliveryOrderDetail)vDODetail.get(i);
			
			dReturned = oDODet.getReturnedQty().doubleValue();
			
			for(int j = 0; j < vSIDetail.size(); j++)
			{
				SalesTransactionDetail oSIDet = (SalesTransactionDetail)vSIDetail.get(j);
				
				if( oDODet.getItemId() != null && oDODet.getItemId().equals(oSIDet.getItemId()))
				{
                    dTotal = dTotal + dReturned + oSIDet.getQty().doubleValue();
			    }
			}
			if(dTotal < oDODet.getQty().doubleValue())
			{
				return false;			
			}
		}
		return true;
	}
	
	/**
	 * update DO status
	 * 
	 * @param _sDOID
	 * @param _bInvoiced
	 * @param _oConn
	 * @throws Exception
	 */
	private static void changeStatus(String _sDOID, boolean _bInvoiced, Connection _oConn)
	    throws Exception
	{
		DeliveryOrder oDeliveryOrder = new DeliveryOrder();
		oDeliveryOrder = getHeaderByID(_sDOID, _oConn);
		oDeliveryOrder.setIsBill(_bInvoiced);
		if(_bInvoiced)
		{
		    oDeliveryOrder.setStatus(i_DO_PAID);
		}
		else
		{
		    oDeliveryOrder.setStatus(i_DO_DELIVERED);
	    }
		oDeliveryOrder.save(_oConn);	
	}
	
	/**
 	 * this is the method to update DO from invoice
 	 * first it will iterate through Invoice Detail then, foreach detail, it will 
 	 * take the inv detail DOID and DODetailID
 	 * secondly, it will get the DO Detail from database then update the DO detail billed Qty
 	 * third, calls checkPaidStatus, if paid then set the DO Status to PAID
	 *
	 * @param List _vSIDet, List of Invoice Detail
	 * @param Connection _oConn, Connection Object
	 */	
	public static void updateDOFromInvoice (List _vSIDet, boolean _bIsCancel, Connection _oConn)
		throws Exception
	{
		for(int i = 0; i < _vSIDet.size(); i++)
		{
			SalesTransactionDetail oTD = (SalesTransactionDetail) _vSIDet.get(i);

			String sDOID = oTD.getDeliveryOrderId();
			String sDODetailID = oTD.getDeliveryOrderDetailId();
            
            //update Billed Qty
			if (StringUtil.isNotEmpty(sDODetailID))
			{
			    DeliveryOrderDetail oDODet = getDetailByDetailID(sDODetailID, _oConn);
			    if (oDODet != null)
			    {
			    	double dBilledQty = oDODet.getBilledQty().doubleValue();
			    	if(!_bIsCancel)
			    	{
			    	    dBilledQty = dBilledQty + oTD.getQty().doubleValue();
			    	}
			    	else
			        {
			            dBilledQty = dBilledQty - oTD.getQty().doubleValue();
			        }
			    	oDODet.setBilledQty (new BigDecimal(dBilledQty));
			    	oDODet.save(_oConn);               
			    }
			}
            
   			//to check balance item on SO and SI
			//if the item already balanced then the SO is Paid
            if (StringUtil.isNotEmpty(sDOID))
			{
			    //if is all items in DO already billed   
				if(checkPaidStatus(sDOID, _oConn) && !_bIsCancel) 
				{
                    changeStatus(sDOID, true, _oConn);
				}
				else
				{
				    changeStatus(sDOID, false, _oConn);
				}
			} 
		}
	}

	
	/**
	 * update DO returned qty 
	 * 
	 * @param vSRD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateReturnQty(List vSRD, boolean _bCancel, Connection _oConn)
		throws Exception
	{
		 try
		 {
			for(int i = 0; i < vSRD.size(); i++)
			{
				SalesReturnDetail oSRD = (SalesReturnDetail) vSRD.get(i);
				DeliveryOrderDetail oDODet = getDetailByDetailID(oSRD.getTransactionDetailId(), _oConn);
				if (oDODet != null)
				{
					double dReturnQty = oSRD.getQty().doubleValue();
					if (_bCancel) dReturnQty = dReturnQty * -1;
					double dLastReturnedQty = oDODet.getReturnedQty().doubleValue();
					oDODet.setReturnedQty(new BigDecimal(dLastReturnedQty + dReturnQty));
					oDODet.save(_oConn);
				}
			}
		 }
		 catch(Exception _oEx)
		 {
		 	_oEx.printStackTrace();
		 	throw new NestableException ("Update Returned Qty Failed, ERROR : ", _oEx);
		 }
	}

	/**
	 * setHeaderProperties
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param data
	 * @throws Exception
	 */
    public static void setHeaderProperties (DeliveryOrder _oDO, List _vDOD, RunData data)
    	throws Exception
    {
    	ValueParser formData = data.getParameters();
		try
		{
			formData.setProperties (_oDO);
			_oDO.setCustomerName(CustomerTool.getCustomerNameByID(_oDO.getCustomerId()) );
			_oDO.setDeliveryOrderDate( CustomParser.parseDate(formData.getString("DeliveryOrderDate")));
			_oDO.setCustomerPoDate( CustomParser.parseDate(formData.getString("CustomerPoDate")));
			
			if( _oDO.getStatus() != i_DO_NEW )
			{
				_oDO.setConfirmDate ( CustomParser.parseDate(formData.getString("ConfirmDate")) );
			}
			
			if( formData.getInt("Status") != i_DO_DELIVERED &&
				formData.getInt("Status") != i_DO_CANCELLED) 
			{
				_oDO.setCreateDate ( new Date () );
			}	
    		_oDO.setTotalQty ( new BigDecimal(countQty (_vDOD)) );	
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set DO Failed : " + _oEx.getMessage (), _oEx); 
		}
    }

    /**
     * count all qty in details
     * 
     * @param _vDetails
     * @return
     */
	public static double countQty (List _vDetails)
		throws Exception
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			DeliveryOrderDetail oDet = (DeliveryOrderDetail) _vDetails.get(i);
			if (oDet.getQtyBase() == null) //if from add detail
			{
				oDet.setQtyBase (UnitTool.getBaseQty(oDet.getUnitId(), oDet.getQty()));
			}
			dQty += oDet.getQtyBase().doubleValue();
		}
		return dQty;
	}
	
	//set DO detail properties
    public static void updateDetail (List _vDet, RunData data)
    	throws Exception
    {
    	ValueParser formData = data.getParameters();
		try
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				DeliveryOrderDetail oDetail = (DeliveryOrderDetail) _vDet.get(i);
				log.debug ("DO Update Detail BEFORE : "  + oDetail);
				
				oDetail.setQty 		(formData.getBigDecimal("Qty" + (i + 1)) );
				oDetail.setQtyBase 	(UnitTool.getBaseQty(oDetail.getUnitId(), oDetail.getQty()));				
				oDetail.setSubTotal (formData.getBigDecimal("SubTotal" + (i + 1)) );

				log.debug ("DO Update Detail AFTER : "  + oDetail);
				_vDet.set (i, oDetail);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Update DO Detail Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
	
	public static void mapSOHeaderToDOHeader (SalesOrder _oSO, DeliveryOrder _oDO)
		throws Exception
	{
		String sLocationID = _oSO.getLocationId();
		if (StringUtil.isEmpty(sLocationID)) sLocationID = PreferenceTool.getLocationID();
		Location oLocation = LocationTool.getLocationByID (sLocationID);
		
		_oDO.setSalesOrderId		(_oSO.getSalesOrderId	      ());	
		_oDO.setCustomerPoNo		(_oSO.getCustomerPoNo         ());
		_oDO.setCustomerPoDate		(_oSO.getCustomerPoDate		  ());
		
        _oDO.setLocationId			(oLocation.getLocationId   	  ());
        _oDO.setLocationName      	(oLocation.getLocationName 	  ());
	    _oDO.setDeliveryOrderDate	(new Date());
        _oDO.setCustomerId			(_oSO.getCustomerId 	     ());
        _oDO.setCustomerName		(_oSO.getCustomerName 	     ());
        _oDO.setTotalQty			(_oSO.getTotalQty		     ());
        _oDO.setCreateBy			(_oSO.getUserName            ());
        _oDO.setRemark				(_oSO.getRemark              ());
        _oDO.setTotalDiscountPct	(_oSO.getTotalDiscountPct    ());
        _oDO.setPaymentTypeId		(_oSO.getPaymentTypeId       ());
        _oDO.setPaymentTermId		(_oSO.getPaymentTermId       ());
        _oDO.setCurrencyId		    (_oSO.getCurrencyId          ());
        _oDO.setCurrencyRate		(_oSO.getCurrencyRate        ());
        _oDO.setShipTo      		(_oSO.getShipTo              ());
        _oDO.setSalesId      		(_oSO.getSalesId			 ());
        _oDO.setCourierId   		(_oSO.getCourierId           ());
        _oDO.setFobId          		(_oSO.getFobId               ());
        _oDO.setEstimatedFreight	(_oSO.getEstimatedFreight	 ());
        _oDO.setIsTaxable			(_oSO.getIsTaxable			 ());
        _oDO.setIsInclusiveTax		(_oSO.getIsInclusiveTax		 ());
        _oDO.setStatus 			    (i_DO_NEW);
	}

	public static void mapSODetailToDODetail ( SalesOrderDetail _oSODet, 
											   DeliveryOrderDetail _oDet )
		throws Exception
	{
		_oDet.setSalesOrderDetailId	    (_oSODet.getSalesOrderDetailId());	
        _oDet.setItemId       			(_oSODet.getItemId());
        _oDet.setItemCode       		(_oSODet.getItemCode());
        _oDet.setItemName       		(_oSODet.getItemName());
        _oDet.setUnitId					(_oSODet.getUnitId());
        _oDet.setUnitCode				(_oSODet.getUnitCode());
        
        double dNewQty = _oSODet.getQty().doubleValue()-_oSODet.getDeliveredQty().doubleValue();
        
        _oDet.setQty					(new BigDecimal(dNewQty));        
        _oDet.setItemPriceInSo			(_oSODet.getItemPrice());
        _oDet.setTaxId		            (_oSODet.getTaxId());
        _oDet.setTaxAmount      		(_oSODet.getTaxAmount());
        _oDet.setDiscount               (_oSODet.getDiscount());
        _oDet.setSubTotal				(_oSODet.getSubTotal());
        _oDet.setCostPerUnit			(_oSODet.getCostPerUnit());   
	}

	/**
	 * get the latest item cost for this DO
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void recalculateCost (DeliveryOrder _oTR, List _vTD, Map _mGroup, Connection _oConn) 
		throws Exception
	{		
		for ( int i = 0; i < _vTD.size(); i++ ) 
		{
			DeliveryOrderDetail oTD = (DeliveryOrderDetail) _vTD.get (i);
			Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
			if (oItem.getItemType() == i_INVENTORY_PART)
			{
				oTD.setCostPerUnit(InventoryLocationTool.getItemCost(oTD.getItemId(), _oTR.getLocationId(), 
													  			     _oTR.getDeliveryOrderDate(), _oConn));
			}
			else if (oItem.getItemType() == i_GROUPING)
			{
				List vGroup = ItemGroupTool.getItemGroupByGroupID(oTD.getItemId(), _oConn);
				double dGroupCost = 0;
				for (int j = 0; j < vGroup.size(); j++)
				{
					ItemGroup oGroupPart = (ItemGroup) vGroup.get(j);
					BigDecimal dCost = InventoryLocationTool.getItemCost(oGroupPart.getItemId(), 
							  											 _oTR.getLocationId(), 
							  											 _oTR.getDeliveryOrderDate(), _oConn);
					dGroupCost += dCost.doubleValue();
					oGroupPart.setCost(dCost);
				}
				_mGroup.put(oTD.getItemId(), vGroup);
				oTD.setCostPerUnit(new BigDecimal(dGroupCost));
			}
		}
	}	
	
	/**
	 * save Data
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param _oConn
	 * @param _bCloseSO
	 * @throws Exception
	 */
	public static void saveData (DeliveryOrder _oDO,
								 List _vDOD,
								 Connection _oConn,
								 boolean _bCloseSO)
		throws Exception
	{
		boolean bJoinTrans = true;
		if (_oConn == null) 
		{
			_oConn = Transaction.begin(s_DB_NAME);
			_oConn.setTransactionIsolation(i_TRANS_ISOLATION);
			bJoinTrans = false;
		}			
		try
		{
			Map mGroup = new HashMap();
			
			recalculateCost (_oDO, _vDOD, mGroup, _oConn);
			
			processSaveDOData (_oDO, _vDOD, _oConn);
			
			if(_oDO.getStatus() == i_DO_DELIVERED)
			{
				//Update Inventory Transaction
				InventoryTransactionTool.createFromDO (_oDO, _vDOD, mGroup, _oConn);				
                
				//Update SO From DO
    			SalesOrderTool.updateSOFromDO(_vDOD,_oDO.getSalesOrderId(),_bCloseSO, false, _oConn);
    		
                //Journal To GL
				if (b_USE_GL)
				{	
					SalesJournalTool oSalesJournal = new SalesJournalTool(_oDO);
					oSalesJournal.createDOJournal (_oDO, _vDOD, mGroup, _oConn);
				}
			}
            if (!bJoinTrans) 
			{
				Transaction.commit ( _oConn );
			}
		}
		catch (Exception _oEx)
		{
			if (!bJoinTrans) 
			{
				Transaction.safeRollback ( _oConn );
			}
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * process save do data
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveDOData (DeliveryOrder _oDO, List _vDOD, Connection _oConn) 
		throws Exception
	{
	    //get location where item deliver
		String sLocationID = _oDO.getLocationId();
		
		//whenever location not define, will get preference location
		if (StringUtil.isEmpty(sLocationID)) 
		{
		    sLocationID = PreferenceTool.getLocationID();
		}
		
		Location oLocation = LocationTool.getLocationByID (sLocationID);
		
		//set create date when null
		if(_oDO.getCreateDate() == null)_oDO.setCreateDate(new Date());
		
        //when approved will set the receipt number
		if(_oDO.getStatus() == i_DO_DELIVERED || _oDO.getStatus() == i_DO_CANCELLED)
        {
            if(_oDO.getStatus() == i_DO_DELIVERED && StringUtil.isEmpty(_oDO.getDeliveryOrderNo()))
            {
 	 	        _oDO.setDeliveryOrderNo (LastNumberTool.get(s_DO_FORMAT, LastNumberTool.i_DELIVERY_ORDER, _oConn));
			}
        }
        else
        {
            _oDO.setDeliveryOrderNo ("");
        }

        //set id when not define
        if(StringUtil.isEmpty(_oDO.getDeliveryOrderId()))
		{
	 	    _oDO.setDeliveryOrderId ( IDGenerator.generateSysID() );
	 	}
	 	
   	 	_oDO.setLocationName 	( oLocation.getLocationName()    );
   	 	
        if(_oDO.getStatus() == i_DO_NEW)
        {
	    	_oDO.setConfirmBy ("");
        }
		else
		{
		    _oDO.setConfirmDate( new Date());
	    }
		_oDO.save (_oConn);
	 			
		for ( int i = 0; i < _vDOD.size(); i++ )
		{
            DeliveryOrderDetail oTD = (DeliveryOrderDetail) _vDOD.get (i);
            if(StringUtil.isEmpty(oTD.getDeliveryOrderDetailId()))
            {
            	oTD.setDeliveryOrderDetailId ( IDGenerator.generateSysID() );
            }
            oTD.setReturnedQty(new BigDecimal(0));
            oTD.setBilledQty(new BigDecimal(0));
            oTD.setDeliveryOrderId ( _oDO.getDeliveryOrderId());
            
            //double dQtyBase = UnitTool.convertQtyToBaseUnit(oTD.getQty().doubleValue(),oTD.getUnitId());
			//oTD.setQtyBase (new BigDecimal(dQtyBase));            
            oTD.save (_oConn);
            
            if(_oDO.getStatus() == i_DO_DELIVERED || _oDO.getStatus() == i_DO_CANCELLED)
            {
            	//if DO qty <= 0 then delete the detail
                if(oTD.getQty().doubleValue() <= 0)
                {
                    _vDOD.get(i);
                    Criteria oCrit = new Criteria();
		            oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_DETAIL_ID, oTD.getDeliveryOrderDetailId());
		            DeliveryOrderDetailPeer.doDelete(oCrit,_oConn);
                }
            }
		}
	}

	/**
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelDO (DeliveryOrder _oDO,
				 				 List _vDOD,
								 String _sCancelBy,
								 Connection _oConn)
		throws Exception
	{
		boolean bJoinTrans = true;
		if (_oConn == null) 
		{
			_oConn = Transaction.begin(s_DB_NAME);
			_oConn.setTransactionIsolation(i_TRANS_ISOLATION);
			bJoinTrans = false;
		}
		try
		{
			if (!TransactionTool.isInvoiced(_oDO.getDeliveryOrderId(), _oConn))
			{
				if (!SalesReturnTool.isReturned(_oDO.getDeliveryOrderId(), _oConn))
				{
					//rollback inventory
					InventoryTransactionTool.delete(_oDO.getDeliveryOrderId(), _oConn);
					
					//update so 
					SalesOrderTool.updateSOFromDO(_vDOD,_oDO.getSalesOrderId(),false, true, _oConn);
					
					//rollback and delete gl journal
					if (b_USE_GL)
					{
						SalesJournalTool.deleteJournal(
						    GlAttributes.i_GL_TRANS_DELIVERY_ORDER,_oDO.getDeliveryOrderId(),_oConn
						);
					}
				}
				_oDO.setRemark(cancelledBy(_oDO.getRemark(), _sCancelBy));				
				processSaveDOData (_oDO, _vDOD, _oConn);
			}
			if (!bJoinTrans) 
			{
				Transaction.commit ( _oConn );
			}
		}
		catch (Exception _oEx)
		{
			if (!bJoinTrans) 
			{
				Transaction.safeRollback ( _oConn );
			}
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

    /**
     * 
     * @param _iCond
     * @param _sKeywords
     * @param _dStart
     * @param _dEnd
     * @param _sCustomerID
     * @param _sLocationID
     * @param _iStatus
     * @param _iLimit
     * @param _iGroupBy
     * @param _sCurrencyID
     * @return
     * @throws Exception
     */
    public static LargeSelect findData (int _iCond, 
    									String _sKeywords, 
										Date _dStart, 
										Date _dEnd, 
    							        String _sCustomerID, 
										String _sLocationID, 
										int _iStatus, 
										int _iLimit,
										int _iGroupBy,
										String _sCurrencyID) 
      	throws Exception
      {
    	Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
            	DeliveryOrderPeer.DELIVERY_ORDER_DATE, _dStart, _dEnd, _sCurrencyID);
    	
    	if (StringUtil.isNotEmpty(_sCustomerID)) 
    	{
    		oCrit.add(DeliveryOrderPeer.CUSTOMER_ID, _sCustomerID);	
    	}
    	if (_iStatus > 0) 
    	{
    		oCrit.add(DeliveryOrderPeer.STATUS, _iStatus);	
    	}		
    	if (StringUtil.isNotEmpty(_sLocationID)) 
    	{
    		oCrit.add(DeliveryOrderPeer.LOCATION_ID, _sLocationID);	
    	}		
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}
		log.debug(oCrit);
    	return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.DeliveryOrderPeer");
    }
	
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods
	/////////////////////////////////////////////////////////////////////////////////////
	
    public static List getTransactionInDateRange (Date _dStartDate, Date _dEndDate, int _iGroupBy, 
                                                  String _sKeywords, int _iOrderBy)
      	throws Exception
	{
    	Criteria oCrit = new Criteria();
    	oCrit.add(DeliveryOrderPeer.DELIVERY_ORDER_DATE, DateUtil.getStartOfDayDate( _dStartDate), Criteria.GREATER_EQUAL);
    	oCrit.and(DeliveryOrderPeer.DELIVERY_ORDER_DATE, DateUtil.getEndOfDayDate(_dEndDate), Criteria.LESS_EQUAL);
    	if(_iGroupBy == 2)
    	{
    		oCrit.add(DeliveryOrderPeer.CUSTOMER_NAME, (Object) SqlUtil.like 
    			(DeliveryOrderPeer.CUSTOMER_NAME, _sKeywords,false,true), Criteria.CUSTOM);
    	}
    	if(_iGroupBy == 4)
    	{
    		oCrit.add(LocationPeer.LOCATION_NAME, (Object) SqlUtil.like 
    			(LocationPeer.LOCATION_NAME, _sKeywords,false,true), Criteria.CUSTOM);
    		oCrit.addJoin(DeliveryOrderPeer.LOCATION_ID,LocationPeer.LOCATION_ID);
    	}
    	if(_iGroupBy == 6)
    	{
    		oCrit.add(DeliveryOrderPeer.CREATE_BY, (Object) SqlUtil.like 
    			(DeliveryOrderPeer.CREATE_BY, _sKeywords,false,true), Criteria.CUSTOM);
    	}
    	
    	if(_iOrderBy == 1)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.DELIVERY_ORDER_NO);
    	}
    	if(_iOrderBy == 2)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.CUSTOMER_NAME);
    	}
    	if(_iOrderBy == 3)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.CREATE_BY);
    	}
    	
    	if(_iOrderBy == 4)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.CONFIRM_BY);
    	}
    	
    	log.debug(oCrit);
    	return DeliveryOrderPeer.doSelect(oCrit);
	}

	public static List getDOByStatusAndCustomerID (String _sCustomerID, 
												   int _iStatus, 
												   boolean _bPaidStatus, 
												   String _sCurrencyID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderPeer.CUSTOMER_ID, _sCustomerID);
        oCrit.add(DeliveryOrderPeer.STATUS, _iStatus);
       	oCrit.add(DeliveryOrderPeer.IS_BILL, _bPaidStatus);
       	oCrit.add(DeliveryOrderPeer.CURRENCY_ID, _sCurrencyID);
       	oCrit.or(DeliveryOrderPeer.CURRENCY_ID, "");
        return DeliveryOrderPeer.doSelect(oCrit);
	}
	
	////////////////////////////////////////////////////////////
	// TODO: remove methods below
	////////////////////////////////////////////////////////////
	
	public static List getCustomerList (List _vData)
    	throws Exception
    {
		List vDO = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vData.get(i);
			if (!isCustomerInList (vDO, oDO))
			{
				vDO.add (oDO);
			}
		}
		return vDO;
	}

	private static boolean isCustomerInList (List _vDO, DeliveryOrder _oDO)
	{
		for (int i = 0; i < _vDO.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vDO.get(i);
			if (oDO.getCustomerId().equals(_oDO.getCustomerId()))
			{
				return true;
			}
		}
		return false;
	}

	public static List filterTransByCustomerID (List _vDO, String _sCustomerID)
    	throws Exception
    {
		List vDO = (List)((ArrayList)_vDO).clone();
		DeliveryOrder oDO = null;
		Iterator oIter = vDO.iterator();
		while (oIter.hasNext()) {
			oDO = (DeliveryOrder ) oIter.next();
			if ( !oDO.getCustomerId().equals(_sCustomerID))
			{
				oIter.remove();
			}
		}
		return vDO;
	}
	
	public static List filterTransByStatus (List _vDO, String _sStatus)
    	throws Exception
    {
		int iStatusID = (Integer.valueOf(_sStatus)).intValue();
		List vDO = (List)((ArrayList)_vDO).clone();
		Iterator oIter = vDO.iterator();
		while (oIter.hasNext())
		{
			DeliveryOrder oDO = (DeliveryOrder ) oIter.next();
			if (oDO.getStatus() != iStatusID)
			{
				oIter.remove();
			}
		}
		return vDO;
	}

	public static List getLocationList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			DeliveryOrder oTrans = (DeliveryOrder) _vData.get(i);			
			if (!isLocationInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isLocationInList (List _vTrans, DeliveryOrder _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			DeliveryOrder oTrans = (DeliveryOrder) _vTrans.get(i);			
			if (oTrans.getLocationId().equals(_oTrans.getLocationId())) {			
				return true;
			}
		}
		return false;
	}

	public static List filterTransByLocationID (List _vTrans, String _sLocationID) 
    	throws Exception
    { 

		List vTrans = new ArrayList(_vTrans); 
		DeliveryOrder oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (DeliveryOrder) oIter.next();
			if (!oTrans.getLocationId().equals(_sLocationID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//report method to group data by item
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static DeliveryOrder filterTransByDeliveryOrderID (List _vDO, String _sDOID)
    	throws Exception
    {

		DeliveryOrder oDO = null;
		for (int i = 0; i < _vDO.size(); i++)
		{
			oDO = (DeliveryOrder) _vDO.get(i);
			if ( oDO.getDeliveryOrderId().equals(_sDOID) )
			{
				return oDO;
			}
		}
		return oDO;
	}

    public static List getTransDetails (List _vDO)
    	throws Exception
    {
        return getTransDetails(_vDO,"");
    }
    
	public static List getTransDetails (List _vDO, String _sItemName)
    	throws Exception
    {
		if (_vDO == null) return new ArrayList(1);
		
		List vDOID = new ArrayList (_vDO.size());
		for (int i = 0; i < _vDO.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vDO.get(i);
			vDOID.add (oDO.getDeliveryOrderId());
		}
		Criteria oCrit = new Criteria();
		if(StringUtil.isNotEmpty(_sItemName))
		{
		    oCrit.add(DeliveryOrderDetailPeer.ITEM_NAME,
		        (Object) SqlUtil.like (DeliveryOrderDetailPeer.ITEM_NAME, _sItemName,false,true), Criteria.CUSTOM);
		}
        if(vDOID.size()>0)
        {
		    oCrit.addIn (DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, vDOID);
		}
		return DeliveryOrderDetailPeer.doSelect(oCrit);
	}

	public static List getItemList (List _vDetail)
    	throws Exception
    {
		List vDetail = new ArrayList (_vDetail.size());
		for (int i = 0; i < _vDetail.size(); i++)
		{
			DeliveryOrderDetail oDetail = (DeliveryOrderDetail) _vDetail.get(i);
			if (!isItemInList (vDetail, oDetail))
			{
				vDetail.add (oDetail);
			}
		}
		return vDetail;
	}

	private static boolean isItemInList (List _vDetail, DeliveryOrderDetail _oDet)
	{
		for (int i = 0; i < _vDetail.size(); i++)
		{
			DeliveryOrderDetail oDet = (DeliveryOrderDetail) _vDetail.get(i);
			if (oDet.getItemId().equals(_oDet.getItemId()))
			{
				return true;
			}
		}
		return false;
	}

	public static List filterTransDetailByItemID (List _vDODet, String _sItemID)
    	throws Exception
    {

		List vDODet = (List)((ArrayList)_vDODet).clone();
		Iterator oIter = vDODet.iterator();
		while (oIter.hasNext()) {
			DeliveryOrderDetail oDO = (DeliveryOrderDetail) oIter.next();
			if (!oDO.getItemId().equals(_sItemID))
			{
				oIter.remove();
			}
		}
		return vDODet;
	}
	
	public static List getCreateUserList (List _vData)
    	throws Exception
    {
		List vDO = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vData.get(i);
			if (!isCreateUserInList (vDO, oDO))
			{
				vDO.add (oDO);
			}
		}
		return vDO;
	}

	private static boolean isCreateUserInList (List _vDO, DeliveryOrder _oDO)
	{
		for (int i = 0; i < _vDO.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vDO.get(i);
			if (oDO.getCreateBy().equals(_oDO.getCreateBy()))
			{
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByCreateBy(List _vDO, String _sCreateBy)
    	throws Exception
    {

		List vDO = (List)((ArrayList)_vDO).clone();
		DeliveryOrder oDO = null;
		Iterator oIter = vDO.iterator();
		while (oIter.hasNext()) {
			oDO = (DeliveryOrder) oIter.next();
			if (!oDO.getCreateBy().equals(_sCreateBy))
			{
				oIter.remove();
			}
		}
		return vDO;
	}
	
	public static List getConfirmUserList (List _vData)
    	throws Exception
    {
		List vDO = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vData.get(i);
			if (!isConfirmUserInList (vDO, oDO))
			{
				vDO.add (oDO);
			}
		}
		return vDO;
	}

	private static boolean isConfirmUserInList (List _vDO, DeliveryOrder _oDO)
	{
		for (int i = 0; i < _vDO.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vDO.get(i);
			if (oDO.getConfirmBy().equals(_oDO.getConfirmBy()))
			{
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByConfirmBy(List _vDO, String _sConfirmBy)
    	throws Exception
    {

		List vDO = (List)((ArrayList)_vDO).clone();
		DeliveryOrder oDO = null;
		Iterator oIter = vDO.iterator();
		while (oIter.hasNext()) {
			oDO = (DeliveryOrder) oIter.next();
			if (!oDO.getConfirmBy().equals(_sConfirmBy))
			{
				oIter.remove();
			}
		}
		return vDO;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//end report methods
	/////////////////////////////////////////////////////////////////////////////////////

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(DeliveryOrderPeer.class, DeliveryOrderPeer.DELIVERY_ORDER_ID, _sID, _bIsNext);
	}	  
	//end next prev
}
package com.ssti.enterprise.pos.presentation.screens.periodic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StreamCorruptedException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.SerializationUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/periodic/ViewFileContent.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ViewFileContent.java,v 1.6 2009/05/04 01:58:34 albert Exp $
 *
 * $Log: ViewFileContent.java,v $
 * Revision 1.6  2009/05/04 01:58:34  albert
 * *** empty log message ***
 *
 * Revision 1.5  2006/03/22 15:49:33  albert
 * *** empty log message ***
 *
 * Revision 1.4  2006/02/04 12:19:59  albert
 * *** empty log message ***
 *
 */
public class ViewFileContent extends PeriodicSecureScreen
{
	private static Log log = LogFactory.getLog(ViewFileContent.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {   
        try
        {
        	String sFilePath = data.getParameters().getString("File");
        	if (StringUtil.isNotEmpty(sFilePath))
        	{
         		if (!sFilePath.endsWith(".log") && !sFilePath.endsWith(".ERROR"))
         		{
	        		Object oData = SerializationUtil.deserialize (sFilePath);
	         		if (oData != null) 
	         		{
	        			context.put ("filecontent", oData.toString());
	        		}
	        		else 
	        		{
	        			context.put ("filecontent", "File Content is empty or invalid");
	        		}
         		}
         		else //if log file
         		{
         			FileReader oReader = new FileReader(sFilePath);
         			BufferedReader oBR = new BufferedReader (oReader);
         			StringBuilder sContent = new StringBuilder();
					while(true)
					{
						String s = oBR.readLine();
						if (s != null)
						{
							sContent.append(s).append(Attributes.s_LINE_SEPARATOR);
						}
						else 
						{
							break;
						}
					}
					oBR.close();
					context.put ("filecontent", sContent.toString());
         		}
        	}	
        }
        
        catch (StreamCorruptedException _oSCEx)
        {
			log.error ( _oSCEx.getMessage(), _oSCEx);
			data.setMessage("File doesn't contain valid object or file is not system generated file");
        }                
        catch (ClassCastException _oCCEx)
        {
			log.error ( _oCCEx.getMessage(), _oCCEx);
			data.setMessage("File doesn't contain valid object or file is not system generated file");
        }                
        catch (Exception _oEx)
        {
			log.error (_oEx);
			_oEx.printStackTrace();
			data.setMessage("File is Invalid " + _oEx.getMessage());
        }        
    }  
}

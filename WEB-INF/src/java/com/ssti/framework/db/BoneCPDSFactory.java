package com.ssti.framework.db;

/*
 * Copyright 2001-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.sql.DataSource;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;
import org.apache.torque.dsfactory.DataSourceFactory;

import com.jolbox.bonecp.BoneCPDataSource;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: Albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class BoneCPDSFactory
    extends AbstractDSFactory
    implements DataSourceFactory
{

    /** The log. */
    private static Log log 
            = LogFactory.getLog(BoneCPDSFactory.class);

    /** The wrapped <code>DataSource</code>. */
    private DataSource ds;

    /**
     * @see org.apache.torque.dsfactory.DataSourceFactory#getDataSource
     */
    public DataSource getDataSource()
    {
        return ds;
    }

    /**
     * @see org.apache.torque.dsfactory.DataSourceFactory#initialize
     */
    public void initialize(Configuration configuration) throws TorqueException
    {
        if (configuration == null)
        {
            throw new TorqueException(
                "Torque cannot be initialized without a valid configuration. "
                + "Please check the log files for further details.");
        }
        
        init(configuration);

        try
        {
            BoneCPDataSource datasource = new BoneCPDataSource();
            datasource.setJdbcUrl(sDBURL);
            datasource.setUsername(sDBUSR);
            datasource.setPassword(sDBPWD);
            datasource.setDriverClass(sDBDRV);
            datasource.setPartitionCount(2);
            datasource.setMaxConnectionsPerPartition(iMaxActive);
            datasource.setMinConnectionsPerPartition(iMinIdle);        

            log.debug(datasource + " "  + datasource.getConnection());
            this.ds = datasource;            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}

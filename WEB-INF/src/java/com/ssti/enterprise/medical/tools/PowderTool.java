package com.ssti.enterprise.medical.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.medical.manager.MedConfigManager;
import com.ssti.enterprise.medical.om.PowderItem;
import com.ssti.enterprise.medical.om.PowderItemPeer;
import com.ssti.enterprise.medical.om.PresPowderDetail;
import com.ssti.enterprise.medical.om.PresPowderDetailPeer;
import com.ssti.enterprise.medical.om.PrescriptionDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PowderTool.java,v 1.1 2009/05/04 01:38:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: PowderTool.java,v $
 * Revision 1.1  2009/05/04 01:38:15  albert
 * *** empty log message ***
 *
 * 2018-06-28
 * - add req_qty in prescription_detail to keep original doctor request qty
 * - in powder resultQty => reqQty, add getQty to calculate
 * </pre><br>
 */
public class PowderTool extends BaseTool 
{	
	static final Log log = LogFactory.getLog(PowderTool.class);
	
	static PowderTool instance = null;
	
	public static synchronized PowderTool getInstance() 
	{
		if (instance == null) instance = new PowderTool();
		return instance;
	}		

	public static List getAllPowderItem() 
		throws Exception 
	{
		return MedConfigManager.getInstance().getAllPowderItem();
	}		

	/**
	 * check if item is powderItem (capsul / bottle / etc)
	 * 
	 * @param _sItemID
	 * @return
	 */
	public static boolean isPowderItem(String _sItemID)
	{
		try {
			List v = getAllPowderItem();
			for(int i = 0; i < v.size(); i++)
			{
				PowderItem oPwd = (PowderItem) v.get(i);
				if(StringUtil.equals(_sItemID, oPwd.getItemId()))
				{
					return true;
				}
			}	
		} 
		catch (Exception e) {
			log.error(e);
		}		
		return false;
	}
	

	/**
	 * check if item is powderItem (capsul / bottle / etc)
	 * 
	 * @param _sItemID
	 * @return
	 */
	public static PowderItem getPowderItem(String _sItemID)
	{
		try {
			List v = getAllPowderItem();
			for(int i = 0; i < v.size(); i++)
			{
				PowderItem oPwd = (PowderItem) v.get(i);
				if(StringUtil.equals(_sItemID, oPwd.getItemId()))
				{
					return oPwd;
				}
			}	
		} 
		catch (Exception e) {
			log.error(e);
		}		
		return null;
	}
	
	public static PowderItem getPowderItemByID(String _sID) 
		throws Exception 
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PowderItemPeer.POWDER_ITEM_ID, _sID);
		List vData = PowderItemPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (PowderItem) vData.get(0);
		}
		return null;
	}		

	public static PowderItem getByItemCode(String _sCode) 
		throws Exception 
	{
		List v = getAllPowderItem();
		for(int i = 0; i < v.size(); i++)
		{
			PowderItem oPwd = (PowderItem) v.get(i);
			if(StringUtil.equals(_sCode, oPwd.getItemCode()))
			{
				return oPwd;
			}
		}
		return null;
	}	
		
	//static methods
	public static List getByPresDetID(String _sID, Connection _oConn)
    	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID, _sID);
		oCrit.addAscendingOrderByColumn(PresPowderDetailPeer.PRES_POWDER_DETAIL_ID);
		return PresPowderDetailPeer.doSelect(oCrit, _oConn);
	}

	/**
	 * 
	 * @param data
	 * @param _vTD
	 * @throws Exception
	 */
	public static void updateDetail(RunData data, List<PresPowderDetail> _vTD) 
		throws Exception
	{		    	
		System.out.println("Update Detail");
		String sPowderCode = data.getParameters().getString("PowderItemCode","");
		double dResultQty = data.getParameters().getDouble("ResultQty");
		for (int i = 0; i < _vTD.size(); i++)
		{
			int iNo = i + 1;
			PresPowderDetail oTD = (PresPowderDetail) _vTD.get(i);
			System.out.println (oTD.getItemCode() + " " + sPowderCode);
			double dQty = data.getParameters().getDouble("Qty" + iNo);
			double dUsed = dQty * dResultQty;
			oTD.setQty(new BigDecimal(dQty));						
			oTD.setUsedQty(new BigDecimal(dUsed));
			oTD.setReqQty(data.getParameters().getBigDecimal("PwdReqQty" + iNo, bd_ONE));
			if(MedConfigTool.isWarningItems(oTD.getItem().getColor())){
				oTD.setBilledQty(new BigDecimal(dUsed)); // do not round NKT,OOT items with warning				
			}
			else
			{
				oTD.setBilledQty(new BigDecimal(dUsed).setScale(0, BigDecimal.ROUND_UP));
			}
			double dBilled = oTD.getBilledQty().doubleValue();
			double dSubTotal = dBilled * oTD.getItemPrice().doubleValue();				
			oTD.setSubTotal(new BigDecimal(dSubTotal).setScale(0, BigDecimal.ROUND_HALF_UP));
			_vTD.set(i, oTD);
		}
	}		
	
	/**
	 * 
	 * @param data
	 * @param _oPRSD
	 * @throws Exception
	 */
    public static void setHeader(RunData data, PrescriptionDetail _oPRSD, List<PresPowderDetail> _vTD) 
		throws Exception
	{
		_oPRSD.setDescription(data.getParameters().getString("Remark"));
		_oPRSD.setDosage(data.getParameters().getString("Dosage"));
		_oPRSD.setBeforeAfter(data.getParameters().getString("BeforeAfter"));
		_oPRSD.setInstruction(data.getParameters().getString("Instruction"));
		_oPRSD.setReqQty(data.getParameters().getBigDecimal("ReqQty"));
		_oPRSD.setResultQty(data.getParameters().getBigDecimal("ResultQty"));
		_oPRSD.setPowderItemId(data.getParameters().getString("PowderItemId"));

		double dPRSDRFee = 0;	
		double dPRSDPack = 0;
		double dResQty = _oPRSD.getResultQty().doubleValue();
		PowderItem oPWD = getPowderItemByID(_oPRSD.getPowderItemId());
		if(oPWD != null)
		{
			dPRSDRFee = PowderTool.calculateRFee(oPWD, dResQty);
			dPRSDPack = PowderTool.calculatePack(oPWD, dResQty);			
			if(MedConfigTool.getRfeeEditable())
			{
				if(dPRSDRFee == 0) dPRSDRFee = data.getParameters().getDouble("Rfee",0);
				if(dPRSDPack == 0) dPRSDPack = data.getParameters().getDouble("Packaging",0);
			}			
			_oPRSD.setRfee(new BigDecimal(dPRSDRFee).toString());
			_oPRSD.setPackaging(new BigDecimal(dPRSDPack).toString());
		}
	
		/*
		String bdPRSDF = _oPRSD.getRfee();
		String bdPRSDP = _oPRSD.getPackaging();
		
		if (bdPRSDF != null) dPRSDRFee = Double.parseDouble(bdPRSDF);
		if (bdPRSDP != null) dPRSDPack = Double.parseDouble(bdPRSDP);
		*/
		
		double dPRSDSub = 0;	
		double dPRSDTotal = 0;
		double dPRSDTax = 0;
		
		for (int i = 0; i < _vTD.size(); i++)
		{			
			PresPowderDetail oTD = (PresPowderDetail) _vTD.get(i);
			oTD.setQtyBase(oTD.getQty());
			oTD.setSubTotal(oTD.getSubTotal().setScale(0, BigDecimal.ROUND_HALF_UP));
			double dTax = oTD.getSubTotal().doubleValue() * (oTD.getTaxAmount().doubleValue()/100);
			double dSubTotal = oTD.getSubTotal().doubleValue();
			dPRSDSub += dSubTotal;
			dPRSDTax += dTax;			
		}
		
		Item oItem = ItemTool.getItemByCode(MedConfigTool.getPowderItemCode());
		if (oItem != null)
		{
			_oPRSD.setItemId (oItem.getItemId());
			_oPRSD.setItemCode (oItem.getItemCode());
		}		
		dPRSDTotal = dPRSDSub + dPRSDRFee + dPRSDPack;

		double dPrice = dPRSDSub;
		if(_oPRSD.getResultQty().doubleValue() > 0)
		{
			dPrice = dPRSDSub / _oPRSD.getResultQty().doubleValue();
		}
		
		//calculate getQty
		_oPRSD.setQty(_oPRSD.getResultQty());
		_oPRSD.setQtyBase(_oPRSD.getQty());
		_oPRSD.setItemPrice(new BigDecimal(dPrice).setScale(0, BigDecimal.ROUND_HALF_UP));
		_oPRSD.setSubTotal(new BigDecimal(dPRSDSub).setScale(0, BigDecimal.ROUND_HALF_UP));
		_oPRSD.setTotal(new BigDecimal(dPRSDTotal).setScale(0, BigDecimal.ROUND_HALF_UP));
		_oPRSD.setPrescriptionType(MedicalAttributes.i_PRES_POWDER);
		_oPRSD.setPowderDetail(_vTD);
		
        BeanUtil.setBDScale(MedConfigTool.getDecimalScale(), _oPRSD);
	}

	public static void deleteByPresID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PresPowderDetailPeer.PRESCRIPTION_ID, _sID);
		PresPowderDetailPeer.doDelete (oCrit, _oConn);
	}
	
	public static double calculateRfee(String _sItemID, double _dQty)
	{
		if(StringUtil.isNotEmpty(_sItemID) && isPowderItem(_sItemID))
		{
		
			try 
			{
				return calculateRFee(getPowderItem(_sItemID), _dQty);	
			} 
			catch (Exception e) {
				log.error(e);
			}			
		}
		return 0;
	}
	
	public static double calculatePack(String _sItemID, double _dQty)
	{
		if(StringUtil.isNotEmpty(_sItemID) && isPowderItem(_sItemID))
		{
		
			try 
			{
				return calculatePack(getPowderItem(_sItemID), _dQty);	
			} 
			catch (Exception e) {
				log.error(e);
			}			
		}
		return 0;
	}	
	
	public static double calculateRFee(PowderItem _oPI, double _dResQty)
	{
		double dValue = 0;
		if(_oPI != null && _dResQty > 0)
		{			
			double dMin = _oPI.getRfeeMinAmount().doubleValue();
			double dMax = _oPI.getRfeeMaxAmount().doubleValue();		
			if(StringUtil.isNotEmpty(_oPI.getDefaultRfee())) 
			{
				dValue = Double.valueOf(_oPI.getDefaultRfee());
			}
			if (_oPI.getMultiplied())
			{
				dValue = dValue * _dResQty;
				if (dMin != 0 && dValue < dMin)
				{
					return dMin;
				}
				else if (dMax != 0 && dValue > dMax)
				{
					return dMax;
				}
			}
		}
		return dValue;
	}
	
	public static double calculatePack(PowderItem _oPI, double _dResQty)
	{
		double dValue = 0;
		if(_oPI != null  && _dResQty > 0)
		{
			double dMin = _oPI.getPackMinAmount().doubleValue();
			double dMax = _oPI.getPackMaxAmount().doubleValue();		
			if(StringUtil.isNotEmpty(_oPI.getDefaultPackaging())) 
			{
				dValue = Double.valueOf(_oPI.getDefaultPackaging());
			}
			if (_oPI.getMultiplied())
			{
				dValue = dValue * _dResQty;
				if (dMin != 0 && dValue < dMin)
				{
					return dMin;
				}
				else if (dMax != 0 && dValue > dMax)
				{
					return dMax;
				}
			}		
		}
		return dValue;
	}	
}

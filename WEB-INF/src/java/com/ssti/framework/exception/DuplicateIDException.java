package com.ssti.framework.exception;

public class DuplicateIDException extends Exception 
{
	static final String s_PREFIX = "Invalid ID, already used by transaction no : ";
	public DuplicateIDException (String _sTransNo)
	{
		super(s_PREFIX + _sTransNo);
	}
}

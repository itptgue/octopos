package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.BankTransferMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseBankTransferPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "bank_transfer";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(BankTransferMapBuilder.CLASS_NAME);
    }

      /** the column name for the BANK_TRANSFER_ID field */
    public static final String BANK_TRANSFER_ID;
      /** the column name for the TRANS_NO field */
    public static final String TRANS_NO;
      /** the column name for the TRANS_DATE field */
    public static final String TRANS_DATE;
      /** the column name for the OUT_BANK_ID field */
    public static final String OUT_BANK_ID;
      /** the column name for the OUT_ACC_ID field */
    public static final String OUT_ACC_ID;
      /** the column name for the OUT_LOC_ID field */
    public static final String OUT_LOC_ID;
      /** the column name for the OUT_CFT_ID field */
    public static final String OUT_CFT_ID;
      /** the column name for the OUT_CF_ID field */
    public static final String OUT_CF_ID;
      /** the column name for the IN_BANK_ID field */
    public static final String IN_BANK_ID;
      /** the column name for the IN_ACC_ID field */
    public static final String IN_ACC_ID;
      /** the column name for the IN_LOC_ID field */
    public static final String IN_LOC_ID;
      /** the column name for the IN_CFT_ID field */
    public static final String IN_CFT_ID;
      /** the column name for the IN_CF_ID field */
    public static final String IN_CF_ID;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the AMOUNT field */
    public static final String AMOUNT;
      /** the column name for the AMOUNT_BASE field */
    public static final String AMOUNT_BASE;
      /** the column name for the USER_NAME field */
    public static final String USER_NAME;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
  
    static
    {
          BANK_TRANSFER_ID = "bank_transfer.BANK_TRANSFER_ID";
          TRANS_NO = "bank_transfer.TRANS_NO";
          TRANS_DATE = "bank_transfer.TRANS_DATE";
          OUT_BANK_ID = "bank_transfer.OUT_BANK_ID";
          OUT_ACC_ID = "bank_transfer.OUT_ACC_ID";
          OUT_LOC_ID = "bank_transfer.OUT_LOC_ID";
          OUT_CFT_ID = "bank_transfer.OUT_CFT_ID";
          OUT_CF_ID = "bank_transfer.OUT_CF_ID";
          IN_BANK_ID = "bank_transfer.IN_BANK_ID";
          IN_ACC_ID = "bank_transfer.IN_ACC_ID";
          IN_LOC_ID = "bank_transfer.IN_LOC_ID";
          IN_CFT_ID = "bank_transfer.IN_CFT_ID";
          IN_CF_ID = "bank_transfer.IN_CF_ID";
          CURRENCY_ID = "bank_transfer.CURRENCY_ID";
          CURRENCY_RATE = "bank_transfer.CURRENCY_RATE";
          AMOUNT = "bank_transfer.AMOUNT";
          AMOUNT_BASE = "bank_transfer.AMOUNT_BASE";
          USER_NAME = "bank_transfer.USER_NAME";
          STATUS = "bank_transfer.STATUS";
          DESCRIPTION = "bank_transfer.DESCRIPTION";
          CANCEL_BY = "bank_transfer.CANCEL_BY";
          CANCEL_DATE = "bank_transfer.CANCEL_DATE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(BankTransferMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(BankTransferMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  22;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.BankTransfer";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseBankTransferPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                      
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(BANK_TRANSFER_ID);
          criteria.addSelectColumn(TRANS_NO);
          criteria.addSelectColumn(TRANS_DATE);
          criteria.addSelectColumn(OUT_BANK_ID);
          criteria.addSelectColumn(OUT_ACC_ID);
          criteria.addSelectColumn(OUT_LOC_ID);
          criteria.addSelectColumn(OUT_CFT_ID);
          criteria.addSelectColumn(OUT_CF_ID);
          criteria.addSelectColumn(IN_BANK_ID);
          criteria.addSelectColumn(IN_ACC_ID);
          criteria.addSelectColumn(IN_LOC_ID);
          criteria.addSelectColumn(IN_CFT_ID);
          criteria.addSelectColumn(IN_CF_ID);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(AMOUNT);
          criteria.addSelectColumn(AMOUNT_BASE);
          criteria.addSelectColumn(USER_NAME);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static BankTransfer row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            BankTransfer obj = (BankTransfer) cls.newInstance();
            BankTransferPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      BankTransfer obj)
        throws TorqueException
    {
        try
        {
                obj.setBankTransferId(row.getValue(offset + 0).asString());
                  obj.setTransNo(row.getValue(offset + 1).asString());
                  obj.setTransDate(row.getValue(offset + 2).asUtilDate());
                  obj.setOutBankId(row.getValue(offset + 3).asString());
                  obj.setOutAccId(row.getValue(offset + 4).asString());
                  obj.setOutLocId(row.getValue(offset + 5).asString());
                  obj.setOutCftId(row.getValue(offset + 6).asString());
                  obj.setOutCfId(row.getValue(offset + 7).asString());
                  obj.setInBankId(row.getValue(offset + 8).asString());
                  obj.setInAccId(row.getValue(offset + 9).asString());
                  obj.setInLocId(row.getValue(offset + 10).asString());
                  obj.setInCftId(row.getValue(offset + 11).asString());
                  obj.setInCfId(row.getValue(offset + 12).asString());
                  obj.setCurrencyId(row.getValue(offset + 13).asString());
                  obj.setCurrencyRate(row.getValue(offset + 14).asBigDecimal());
                  obj.setAmount(row.getValue(offset + 15).asBigDecimal());
                  obj.setAmountBase(row.getValue(offset + 16).asBigDecimal());
                  obj.setUserName(row.getValue(offset + 17).asString());
                  obj.setStatus(row.getValue(offset + 18).asInt());
                  obj.setDescription(row.getValue(offset + 19).asString());
                  obj.setCancelBy(row.getValue(offset + 20).asString());
                  obj.setCancelDate(row.getValue(offset + 21).asUtilDate());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseBankTransferPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                      
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(BankTransferPeer.row2Object(row, 1,
                BankTransferPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseBankTransferPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(BANK_TRANSFER_ID, criteria.remove(BANK_TRANSFER_ID));
                                                                                                                                                                                                                        
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         BankTransferPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(BankTransfer obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(BankTransfer obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(BankTransfer obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(BankTransfer obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(BankTransfer) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(BankTransfer obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(BankTransfer) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(BankTransfer obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(BankTransfer) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(BankTransfer obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseBankTransferPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(BANK_TRANSFER_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( BankTransfer obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(BANK_TRANSFER_ID, obj.getBankTransferId());
              criteria.add(TRANS_NO, obj.getTransNo());
              criteria.add(TRANS_DATE, obj.getTransDate());
              criteria.add(OUT_BANK_ID, obj.getOutBankId());
              criteria.add(OUT_ACC_ID, obj.getOutAccId());
              criteria.add(OUT_LOC_ID, obj.getOutLocId());
              criteria.add(OUT_CFT_ID, obj.getOutCftId());
              criteria.add(OUT_CF_ID, obj.getOutCfId());
              criteria.add(IN_BANK_ID, obj.getInBankId());
              criteria.add(IN_ACC_ID, obj.getInAccId());
              criteria.add(IN_LOC_ID, obj.getInLocId());
              criteria.add(IN_CFT_ID, obj.getInCftId());
              criteria.add(IN_CF_ID, obj.getInCfId());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(AMOUNT, obj.getAmount());
              criteria.add(AMOUNT_BASE, obj.getAmountBase());
              criteria.add(USER_NAME, obj.getUserName());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( BankTransfer obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(BANK_TRANSFER_ID, obj.getBankTransferId());
                          criteria.add(TRANS_NO, obj.getTransNo());
                          criteria.add(TRANS_DATE, obj.getTransDate());
                          criteria.add(OUT_BANK_ID, obj.getOutBankId());
                          criteria.add(OUT_ACC_ID, obj.getOutAccId());
                          criteria.add(OUT_LOC_ID, obj.getOutLocId());
                          criteria.add(OUT_CFT_ID, obj.getOutCftId());
                          criteria.add(OUT_CF_ID, obj.getOutCfId());
                          criteria.add(IN_BANK_ID, obj.getInBankId());
                          criteria.add(IN_ACC_ID, obj.getInAccId());
                          criteria.add(IN_LOC_ID, obj.getInLocId());
                          criteria.add(IN_CFT_ID, obj.getInCftId());
                          criteria.add(IN_CF_ID, obj.getInCfId());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(AMOUNT, obj.getAmount());
                          criteria.add(AMOUNT_BASE, obj.getAmountBase());
                          criteria.add(USER_NAME, obj.getUserName());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static BankTransfer retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static BankTransfer retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static BankTransfer retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        BankTransfer retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static BankTransfer retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (BankTransfer)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( BANK_TRANSFER_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

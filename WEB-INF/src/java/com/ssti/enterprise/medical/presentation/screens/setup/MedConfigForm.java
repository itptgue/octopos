package com.ssti.enterprise.medical.presentation.screens.setup;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.manager.MedConfigManager;
import com.ssti.enterprise.medical.om.MedConfig;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class MedConfigForm extends Default 
{
	protected static final Log log = LogFactory.getLog(MedConfigForm.class);

	static final int i_OP_SAVE = 1;
	
	@Override
	public void doBuildTemplate(RunData data, Context context) 
	{		
		try 
		{
			int iOp = data.getParameters().getInt("op");
			if (iOp == i_OP_SAVE)
			{
				MedConfig oConf = MedConfigTool.getConfig();
				if (oConf == null)
				{
					oConf = new MedConfig();
					oConf.setMedConfigId(IDGenerator.generateSysID());
				}
				data.getParameters().setProperties(oConf);
				oConf.setRfeeEditable(data.getParameters().getBoolean("RfeeEditable"));
				oConf.setPosRfeeEditable(data.getParameters().getBoolean("PosRfeeEditable"));
				
				System.out.println(data.getParameters().getBoolean("PosRfeeEditable"));
				System.out.println(oConf);
				
				oConf.save();
				MedConfigManager.getInstance().refreshCache();
				data.setMessage(LocaleTool.getString("save_success"));								
			}			
			context.put("mc", MedConfigTool.getConfig());						
			MedConfigTool.setContextTool(context);			
			
			context.put("Roles", TurbineSecurity.getAllRoles().getRolesArray() );		
		} 
		catch (Exception e) 
		{
			data.setMessage("ERROR:" + e.getMessage());
		}
	}	
}

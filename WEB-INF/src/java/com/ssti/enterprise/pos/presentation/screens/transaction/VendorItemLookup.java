package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

public class VendorItemLookup extends ItemLookup
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
	        super.doBuildTemplate(data, context);
	        context.put(s_RELATED_EMPLOYEE, Boolean.valueOf(b_PURCHASE_RELATED_EMPLOYEE));
	        if(hasPermission(data, "Access All Vendor"))
    	    {
    	        context.put(s_RELATED_EMPLOYEE,Boolean.valueOf(false));
            }
			
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}    	
    }
}
package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.StringUtil;

public class SOLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		String sCustomerID = data.getParameters().getString ("CustomerId");
    		String sDisplayID = data.getParameters().getString ("DisplayId", "");
    		
    		if (StringUtil.isNotEmpty(sCustomerID))
    		{
    			context.put ("OpenSO", SalesOrderTool.getSOByStatusAndCustomerID(sCustomerID, i_SO_ORDERED));	
    		}
    		if (StringUtil.isNotEmpty(sDisplayID))
    		{
    			context.put ("Details", SalesOrderTool.getDetailsByID (sDisplayID));
    		}
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("SO Lookup Failed : " + _oEx.getMessage());
    	}
    }
}
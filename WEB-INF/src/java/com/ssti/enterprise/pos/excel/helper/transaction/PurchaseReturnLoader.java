// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 7/1/2010 6:29:56 PM
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PurchaseReturnLoader.java

package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

// Referenced classes of package com.ssti.enterprise.pos.excel.helper.transaction:
//            TransactionLoader

public class PurchaseReturnLoader extends TransactionLoader
{

    public PurchaseReturnLoader(String _sUserName)
    {
        m_sUserName = (new StringBuilder(String.valueOf(_sUserName))).append(" from (Excel)").toString();
    }

    public void loadData(InputStream _oInput)
        throws Exception
    {
        POIFSFileSystem oPOI = new POIFSFileSystem(_oInput);
        HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
        HSSFSheet oSheet = oWB.getSheetAt(0);
        HSSFRow oRow = null;
        m_iTotalRows = oSheet.getPhysicalNumberOfRows();
        boolean bTransHeader = false;
        boolean bTransProcessed = false;
        boolean bDetailProcess = false;
        PurchaseReturn oTR = null;
        List vTD = null;
        for(int iRow = 0; iRow < m_iTotalRows; iRow++)
        {
            oRow = oSheet.getRow(iRow);
            if(oRow != null)
            {
                String sValue = getString(oRow, 0);
                if(sValue.equals("Trans No") && !bTransHeader && !bTransProcessed && !bDetailProcess)
                {
                    m_bTransError = false;
                    m_iTotalTrans++;
                    bTransHeader = true;
                    log.debug((new StringBuilder("Found Trans HEADING :")).append(sValue).toString());
                } else
                if(StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
                {
                    oTR = new PurchaseReturn();
                    mapTrans(oRow, oTR);
                    bTransProcessed = true;
                    log.debug((new StringBuilder("* TR ")).append(oTR).toString());
                } else
                if(sValue.equals("Item Code") && bTransHeader && bTransProcessed && !bDetailProcess)
                {
                    vTD = new ArrayList();
                    bDetailProcess = true;
                    log.debug((new StringBuilder("Found Detail HEADING :")).append(sValue).toString());
                } else
                if(!sValue.equals("End Trans") && bTransHeader && bTransProcessed && bDetailProcess)
                {
                    PurchaseReturnDetail oTD = new PurchaseReturnDetail();
                    mapDetails(oRow, oTR, oTD);
                    if(!m_bTransError)
                        vTD.add(oTD);
                    log.debug((new StringBuilder("* TD ")).append(oTD).toString());
                } else
                if(sValue.equals("End Trans") && bTransHeader && bTransProcessed && bDetailProcess)
                {
                    if(!m_bTransError)
                        try
                        {
                            PurchaseReturnTool.setHeaderProperties(oTR, vTD, null);
                            PurchaseReturnTool.saveData(oTR, vTD);
                            logSuccess();
                        }
                        catch(Exception _oEx)
                        {
                            String sMsg = _oEx.getMessage();
                            _oEx.printStackTrace();
                            log.error(_oEx);
                            logSaveError(sMsg);
                        }
                    else
                        logReject();
                    m_bTransError = false;
                    bTransHeader = false;
                    bTransProcessed = false;
                    bDetailProcess = false;
                } else
                {
                    logInvalidFile();
                    return;
                }
            }
        }

    }

    protected void mapTrans(HSSFRow _oRow, PurchaseReturn _oTR)
        throws Exception
    {
        int iCol = 0;
        String sTransNo = getString(_oRow, iCol);
        iCol++;
        m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
        String sLocationCode = getString(_oRow, iCol);
        iCol++;
        Location oLoc = LocationTool.getLocationByCode(sLocationCode);
        if(oLoc == null)
            logError("Location", sLocationCode);
        else
            _oTR.setLocationId(oLoc.getLocationId());
        String sVendorCode = getString(_oRow, iCol);
        iCol++;
        Vendor oVendor = VendorTool.getVendorByCode(sVendorCode);
        if(oVendor == null)
        {
            logError("Vendor Code", sVendorCode);
        } else
        {
            _oTR.setVendorId(oVendor.getVendorId());
            _oTR.setVendorName(oVendor.getVendorName());
        }
        String sCurrencyCode = getString(_oRow, iCol);
        iCol++;
        Currency oCurr = CurrencyTool.getCurrencyByCode(sCurrencyCode);
        if(oCurr == null)
            logError("Currency Code", sCurrencyCode);
        else
            _oTR.setCurrencyId(oCurr.getCurrencyId());
        BigDecimal dRate = getBigDecimal(_oRow, iCol);
        iCol++;
        if(dRate == null || dRate.equals(bd_ZERO))
            logError("Currency Rate", dRate);
        else
            _oTR.setCurrencyRate(dRate);
        BigDecimal dFiscalRate = getBigDecimal(_oRow, iCol);
        iCol++;
        if(dRate == null || dRate.equals(bd_ZERO))
            logError("Fiscal Rate", dRate);
        else
            _oTR.setCurrencyRate(dFiscalRate);
        java.util.Date dTrans = CustomParser.parseDate(getString(_oRow, iCol));
        iCol++;
        if(dTrans == null)
            logError("Transaction Date", dTrans);
        else
            _oTR.setReturnDate(dTrans);
        String sCashier = getString(_oRow, iCol);
        iCol++;
        if(StringUtil.isEmpty(sCashier))
            logError("Create By", sCashier);
        else
            _oTR.setUserName(sCashier);
        String sRemark = getString(_oRow, iCol);
        iCol++;
        _oTR.setRemark(sRemark);
        String sInclusive = getString(_oRow, iCol);
        iCol++;
        if(StringUtil.isNotEmpty(sInclusive))
        {
            _oTR.setIsTaxable(Boolean.valueOf(sInclusive).booleanValue());
            _oTR.setIsInclusiveTax(Boolean.valueOf(sInclusive).booleanValue());
        }
        if(!m_bTransError)
        {
            _oTR.setTransactionId("");
            _oTR.setTransactionNo("");
            _oTR.setTransactionType(3);
            _oTR.setTransactionDate(_oTR.getReturnDate());
            _oTR.setStatus(2);
            _oTR.setCreateDebitMemo(true);
            _oTR.setRemark(setDesc(_oTR.getRemark() + sTransNo, m_sUserName));                    
        }
    }

    protected void mapDetails(HSSFRow _oRow, PurchaseReturn _oTR, PurchaseReturnDetail _oTD)
        throws Exception
    {
        int iCol = 0;
        String sItemCode = getString(_oRow, iCol);
        iCol++;
        m_oResult.append(LocaleTool.getString("item_code")).append(": ").append(sItemCode).append(s_LINE_SEPARATOR);
        Item oItem = ItemTool.getItemByCode(sItemCode);
        if(oItem == null)
        {
            logError("Item Code", sItemCode);
        } else
        {
            _oTD.setItemId(oItem.getItemId());
            _oTD.setItemCode(oItem.getItemCode());
            _oTD.setItemName(oItem.getItemName());
            _oTD.setDescription(oItem.getDescription());
        }
        BigDecimal dQty = getBigDecimal(_oRow, iCol);
        iCol++;
        if(dQty == null)
        {
            logError("Qty", dQty);
        } else
        {
            _oTD.setQty(dQty);
            _oTD.setQtyBase(UnitTool.getBaseQty(_oTD.getItemId(), _oTD.getUnitId(), dQty));
        }
        String sUnitCode = getString(_oRow, iCol);
        iCol++;
        Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
        if(oUnit == null)
        {
            logError("Unit Code", sUnitCode);
        } else
        {
            _oTD.setUnitId(oUnit.getUnitId());
            _oTD.setUnitCode(oUnit.getUnitCode());
        }
        BigDecimal dPrice = getBigDecimal(_oRow, iCol);
        iCol++;
        if(dPrice == null)
            logError("Item Price", dPrice);
        else
            _oTD.setItemPrice(dPrice);
        String sTaxCode = getString(_oRow, iCol);
        iCol++;
        Tax oTax = TaxTool.getTaxByID(TaxTool.getIDByCode(sTaxCode));
        if(oTax == null)
        {
            logError("Tax Code", sTaxCode);
        } else
        {
            _oTD.setTaxId(oTax.getTaxId());
            _oTD.setTaxAmount(oTax.getAmount());
        }
        _oTD.setTransactionDetailId("");
        _oTD.setProjectId("");
        _oTD.setDepartmentId("");
        if(!m_bTransError)
        {
            double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(), _oTD.getUnitId());
            double dQtyBase = _oTD.getQty().doubleValue() * dBaseValue;
            if(dQtyBase > 0.0D)
            {
                double dDiscount = 0.0D;
                double dPriceBase = _oTD.getItemPrice().doubleValue() * _oTR.getCurrencyRate().doubleValue();
                double dRate = _oTR.getCurrencyRate().doubleValue();
                double dCostPerUnit = dPriceBase - dDiscount;
                dCostPerUnit /= dBaseValue;
                double dRealCost = dCostPerUnit * 1.0D;
                _oTD.setItemCost(new BigDecimal(dRealCost));
                _oTD.setReturnAmount(_oTD.getItemCost());
            } else
            {
                _oTD.setReturnAmount(bd_ZERO);
            }
            double dSubTotal = _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();
            double dSubTotalTax = (_oTD.getTaxAmount().doubleValue() / 100D) * dSubTotal;
            _oTD.setSubTotalCost(new BigDecimal(dSubTotal));
            _oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
        }
    }

    private static Log log = LogFactory.getLog(PurchaseReturnLoader.class);
    private static final String s_START_HEADING = "Trans No";
    private static final String s_DETAIL_HEADING = "Item Code";
    private static final String s_END_TRANS = "End Trans";

}
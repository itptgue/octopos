package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class GlConfigMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.GlConfigMapBuilder";

    /**
     * Item
     * @deprecated use GlConfigPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "gl_config";
    }

  
    /**
     * gl_config.GL_CONFIG_ID
     * @return the column name for the GL_CONFIG_ID field
     * @deprecated use GlConfigPeer.gl_config.GL_CONFIG_ID constant
     */
    public static String getGlConfig_GlConfigId()
    {
        return "gl_config.GL_CONFIG_ID";
    }
  
    /**
     * gl_config.DIRECT_SALES_ACCOUNT
     * @return the column name for the DIRECT_SALES_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.DIRECT_SALES_ACCOUNT constant
     */
    public static String getGlConfig_DirectSalesAccount()
    {
        return "gl_config.DIRECT_SALES_ACCOUNT";
    }
  
    /**
     * gl_config.DEFAULT_FREIGHT_ACCOUNT
     * @return the column name for the DEFAULT_FREIGHT_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.DEFAULT_FREIGHT_ACCOUNT constant
     */
    public static String getGlConfig_DefaultFreightAccount()
    {
        return "gl_config.DEFAULT_FREIGHT_ACCOUNT";
    }
  
    /**
     * gl_config.INVENTORY_ACCOUNT
     * @return the column name for the INVENTORY_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.INVENTORY_ACCOUNT constant
     */
    public static String getGlConfig_InventoryAccount()
    {
        return "gl_config.INVENTORY_ACCOUNT";
    }
  
    /**
     * gl_config.SALES_ACCOUNT
     * @return the column name for the SALES_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.SALES_ACCOUNT constant
     */
    public static String getGlConfig_SalesAccount()
    {
        return "gl_config.SALES_ACCOUNT";
    }
  
    /**
     * gl_config.SALES_RETURN_ACCOUNT
     * @return the column name for the SALES_RETURN_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.SALES_RETURN_ACCOUNT constant
     */
    public static String getGlConfig_SalesReturnAccount()
    {
        return "gl_config.SALES_RETURN_ACCOUNT";
    }
  
    /**
     * gl_config.ITEM_DISCOUNT_ACCOUNT
     * @return the column name for the ITEM_DISCOUNT_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.ITEM_DISCOUNT_ACCOUNT constant
     */
    public static String getGlConfig_ItemDiscountAccount()
    {
        return "gl_config.ITEM_DISCOUNT_ACCOUNT";
    }
  
    /**
     * gl_config.COGS_ACCOUNT
     * @return the column name for the COGS_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.COGS_ACCOUNT constant
     */
    public static String getGlConfig_CogsAccount()
    {
        return "gl_config.COGS_ACCOUNT";
    }
  
    /**
     * gl_config.PURCHASE_RETURN_ACCOUNT
     * @return the column name for the PURCHASE_RETURN_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.PURCHASE_RETURN_ACCOUNT constant
     */
    public static String getGlConfig_PurchaseReturnAccount()
    {
        return "gl_config.PURCHASE_RETURN_ACCOUNT";
    }
  
    /**
     * gl_config.EXPENSE_ACCOUNT
     * @return the column name for the EXPENSE_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.EXPENSE_ACCOUNT constant
     */
    public static String getGlConfig_ExpenseAccount()
    {
        return "gl_config.EXPENSE_ACCOUNT";
    }
  
    /**
     * gl_config.UNBILLED_GOODS_ACCOUNT
     * @return the column name for the UNBILLED_GOODS_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.UNBILLED_GOODS_ACCOUNT constant
     */
    public static String getGlConfig_UnbilledGoodsAccount()
    {
        return "gl_config.UNBILLED_GOODS_ACCOUNT";
    }
  
    /**
     * gl_config.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use GlConfigPeer.gl_config.OPENING_BALANCE constant
     */
    public static String getGlConfig_OpeningBalance()
    {
        return "gl_config.OPENING_BALANCE";
    }
  
    /**
     * gl_config.RETAINED_EARNING
     * @return the column name for the RETAINED_EARNING field
     * @deprecated use GlConfigPeer.gl_config.RETAINED_EARNING constant
     */
    public static String getGlConfig_RetainedEarning()
    {
        return "gl_config.RETAINED_EARNING";
    }
  
    /**
     * gl_config.CREDIT_MEMO_ACCOUNT
     * @return the column name for the CREDIT_MEMO_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.CREDIT_MEMO_ACCOUNT constant
     */
    public static String getGlConfig_CreditMemoAccount()
    {
        return "gl_config.CREDIT_MEMO_ACCOUNT";
    }
  
    /**
     * gl_config.DEBIT_MEMO_ACCOUNT
     * @return the column name for the DEBIT_MEMO_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.DEBIT_MEMO_ACCOUNT constant
     */
    public static String getGlConfig_DebitMemoAccount()
    {
        return "gl_config.DEBIT_MEMO_ACCOUNT";
    }
  
    /**
     * gl_config.DELIVERY_ORDER_ACCOUNT
     * @return the column name for the DELIVERY_ORDER_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.DELIVERY_ORDER_ACCOUNT constant
     */
    public static String getGlConfig_DeliveryOrderAccount()
    {
        return "gl_config.DELIVERY_ORDER_ACCOUNT";
    }
  
    /**
     * gl_config.POS_ROUNDING_ACCOUNT
     * @return the column name for the POS_ROUNDING_ACCOUNT field
     * @deprecated use GlConfigPeer.gl_config.POS_ROUNDING_ACCOUNT constant
     */
    public static String getGlConfig_PosRoundingAccount()
    {
        return "gl_config.POS_ROUNDING_ACCOUNT";
    }
  
    /**
     * gl_config.AR_PAYMENT_TEMP
     * @return the column name for the AR_PAYMENT_TEMP field
     * @deprecated use GlConfigPeer.gl_config.AR_PAYMENT_TEMP constant
     */
    public static String getGlConfig_ArPaymentTemp()
    {
        return "gl_config.AR_PAYMENT_TEMP";
    }
  
    /**
     * gl_config.AP_PAYMENT_TEMP
     * @return the column name for the AP_PAYMENT_TEMP field
     * @deprecated use GlConfigPeer.gl_config.AP_PAYMENT_TEMP constant
     */
    public static String getGlConfig_ApPaymentTemp()
    {
        return "gl_config.AP_PAYMENT_TEMP";
    }
  
    /**
     * gl_config.PR_PI_VARIANCE
     * @return the column name for the PR_PI_VARIANCE field
     * @deprecated use GlConfigPeer.gl_config.PR_PI_VARIANCE constant
     */
    public static String getGlConfig_PrPiVariance()
    {
        return "gl_config.PR_PI_VARIANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("gl_config");
        TableMap tMap = dbMap.getTable("gl_config");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("gl_config.GL_CONFIG_ID", "");
                          tMap.addColumn("gl_config.DIRECT_SALES_ACCOUNT", "");
                          tMap.addColumn("gl_config.DEFAULT_FREIGHT_ACCOUNT", "");
                          tMap.addColumn("gl_config.INVENTORY_ACCOUNT", "");
                          tMap.addColumn("gl_config.SALES_ACCOUNT", "");
                          tMap.addColumn("gl_config.SALES_RETURN_ACCOUNT", "");
                          tMap.addColumn("gl_config.ITEM_DISCOUNT_ACCOUNT", "");
                          tMap.addColumn("gl_config.COGS_ACCOUNT", "");
                          tMap.addColumn("gl_config.PURCHASE_RETURN_ACCOUNT", "");
                          tMap.addColumn("gl_config.EXPENSE_ACCOUNT", "");
                          tMap.addColumn("gl_config.UNBILLED_GOODS_ACCOUNT", "");
                          tMap.addColumn("gl_config.OPENING_BALANCE", "");
                          tMap.addColumn("gl_config.RETAINED_EARNING", "");
                          tMap.addColumn("gl_config.CREDIT_MEMO_ACCOUNT", "");
                          tMap.addColumn("gl_config.DEBIT_MEMO_ACCOUNT", "");
                          tMap.addColumn("gl_config.DELIVERY_ORDER_ACCOUNT", "");
                          tMap.addColumn("gl_config.POS_ROUNDING_ACCOUNT", "");
                          tMap.addColumn("gl_config.AR_PAYMENT_TEMP", "");
                          tMap.addColumn("gl_config.AP_PAYMENT_TEMP", "");
                          tMap.addColumn("gl_config.PR_PI_VARIANCE", "");
          }
}

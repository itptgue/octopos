package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.List;

import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class PurchaseReturnTransLookup extends TransactionSecureScreen
{
	private static final String s_PR = "findPRResult";
	private static final String s_PI = "findPIResult";
	
    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		return true;
	}
    
    public void doBuildTemplate (RunData data, Context context)
    {	
    	try
		{
    		int iRetFrom = data.getParameters().getInt("ReturnFrom", -1);

			if (data.getParameters().getString("op", "").equals("first"))
			{												
	    		getTransData (data, context, iRetFrom);				
			}

    		if (data.getUser().getTemp(s_PR) != null)
    		{
    			LargeSelectHandler.handleLargeSelectParameter (data, context, s_PR, "vPR");    		
    		}
    		if (data.getUser().getTemp(s_PI) != null)
    		{
    			LargeSelectHandler.handleLargeSelectParameter (data, context, s_PI, "vPI");    		
    		}
		}
		catch (Exception _oEx) 
		{
			data.setMessage("Error : " + _oEx.getMessage());
		}
    }

    private void getTransData ( RunData data, Context context, int _iRetFrom  ) 
    	throws Exception
    {
    	String sTransNo  = data.getParameters().getString ("TransactionNo");
    	String sVendorID = data.getParameters().getString ("VendorId");
    	int iLimit = data.getParameters().getInt("viewLimit", 25);
    	
		System.out.println(data.getParameters() + " " + sTransNo);
    	try 
    	{	
    		LargeSelect oLS = null;
    		List vPRTrans = null;
    		
    		//FROM PR
    		if(_iRetFrom == AppAttributes.i_RET_FROM_PR_DO)
    		{	
    			oLS = PurchaseReceiptTool.findData(i_TRANS_NO, sTransNo, 
    				null, null, sVendorID, "", i_PR_APPROVED, iLimit, null);
    			
    			vPRTrans = oLS.getNextResults();
    			
    			if (vPRTrans != null && vPRTrans.size() == 1)
    			{
	    			PurchaseReceipt oPR = (PurchaseReceipt) vPRTrans.get(0);
    				context.put("sTransID", oPR.getPurchaseReceiptId());
    			}
    			else
    			{
    				context.put("vPR", vPRTrans);
    				data.getUser().setTemp(s_PR, oLS);
    			}
    		}
    		
    		//IMPORT FROM PI
        	if(_iRetFrom == AppAttributes.i_RET_FROM_PI_SI)
        	{
        		oLS = PurchaseInvoiceTool.findData(i_TRANS_NO, sTransNo, null, null, 
        		    sVendorID, "", -1, i_PI_PROCESSED, iLimit, null); 
        		
        		vPRTrans = oLS.getNextResults();

        		System.out.println(data.getParameters() + " " + oLS + " " + vPRTrans);
        		
        		if (vPRTrans!= null && vPRTrans.size() == 1)
        		{
        			PurchaseInvoice oPI = (PurchaseInvoice) vPRTrans.get(0);
    				context.put("sTransID", oPI.getPurchaseInvoiceId());
        		}
    			else 
    			{
    				context.put("vPI", vPRTrans);
    				data.getUser().setTemp(s_PI, oLS);
    			}
    		}
	  	}
    	catch (Exception _oEx) 
    	{
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    	}
    }
}

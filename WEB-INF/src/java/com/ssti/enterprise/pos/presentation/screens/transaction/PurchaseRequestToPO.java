package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class PurchaseRequestToPO extends PurchaseRequestView
{
	static final int i_VIEW = 1;
	static final int i_GEN_PO = 2;
	
    int op = 0;
	StringBuilder sResult = null;
	String sUserName = "";
	String sLocID = "";
    public void doBuildTemplate(RunData data, Context context)
    {
    	
    	op = data.getParameters().getInt("op");
    	if(op == i_VIEW)
    	{	    	
    		viewData(data, context);
    	}
    	if(op == i_GEN_PO)
    	{
			sUserName = data.getUser().getName();
			sLocID = data.getParameters().getString("LocationId");
			List vTD = (List) data.getUser().getTemp("RQ2PO");
			if (vTD != null)
			{
				try 
				{
					sResult = new StringBuilder();
					generatePO(vTD);
					data.setMessage("PO Generated Successfully");
					context.put("result",sResult.toString());
					System.out.println(sResult);
				} 
				catch (Exception _oEx) 
				{
					_oEx.printStackTrace();
					log.error(_oEx);
					data.setMessage("Generate PO Failed, ERROR: " + _oEx.getMessage());
				}
			}
			context.put("vTD", vTD);
    	}
	}
    
    void viewData(RunData data, Context context)
    {
		int iCond = data.getParameters().getInt("Condition");
		String sKeywords = data.getParameters().getString("Keywords", "");
		String sVTypeID = data.getParameters().getString("VendorTypeId", "");
		String sVendorID = data.getParameters().getString("VendorId", "");
		String sKatID = data.getParameters().getString("KategoriID", "");
		String sLocID = data.getParameters().getString("LocationId", "");
		String sReqBy = data.getParameters().getString("RequestedBY", "");		
		int iStatus = data.getParameters().getInt("Status");
		int iSortBy = data.getParameters().getInt("SortBy");
		boolean bExt = data.getParameters().getBoolean("IsExternal");
		Date dStart = data.getParameters().getDate("StartDate");
		Date dEnd = data.getParameters().getDate("EndDate");
		
		try 
		{
			List vTD = PurchaseRequestTool.findDetail(iCond, sKeywords, dStart, dEnd, 
					sVTypeID, sVendorID, sKatID, sLocID,  sReqBy, iStatus, iSortBy, bExt);
			
			context.put("vTD", vTD);
			data.getUser().setTemp("RQ2PO", vTD);
		} 
		catch (Exception e) 
		{
			log.error(e);
			e.printStackTrace();
		}
    }
    
    Map mVendor = null;
	void generatePO(List _vData)
		throws Exception
	{		
		//List vID = SqlUtil.toListOfID(_vData);		
		//map / group by vendor
		createVendorMap(_vData);
		
		//create PO
		Iterator iter = mVendor.keySet().iterator();
		while(iter.hasNext())
		{
			String sVendorID = (String)iter.next();
			List vItem = (List)mVendor.get(sVendorID);
			if(vItem.size() > 0)
			{
				String sMsg = PurchaseOrderTool.createPO(sVendorID, sLocID, vItem, sUserName, "RQ");
				System.out.println(sMsg);
				sResult.append(sMsg);
			}
			else
			{
				sResult.append("Empty PO");
			}
		}		
	}   
	
	void createVendorMap(List _vTD)
		throws Exception
	{
		mVendor = new HashMap(_vTD.size());						
		for (int i = 0; i < _vTD.size(); i++)
		{
			Record oRecord = (Record)_vTD.get(i);
			if(oRecord.getValue("po_qty").asInt() == 0)
			{
				String sVendID = oRecord.getValue("prefered_vendor_id").asString();
				if (StringUtil.isNotEmpty(sVendID))
				{
					List vItem = (List) mVendor.get(sVendID);
					if (vItem == null)
					{
						vItem = new ArrayList();
					}					
					vItem.add(oRecord);
					mVendor.put(sVendID, vItem);											
				}
			}
		}
	}
}
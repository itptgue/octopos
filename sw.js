var sCTX = '/RTL';
var CACHE_NAME = 'rtl-offline-statics';

var resourcesToCache = [
    sCTX + '/',    
    sCTX + '/css/font-awesome/css/font-awesome.min.css',
    sCTX + '/css/pikaday.css',
    sCTX + '/css/pos-touch.css',
    sCTX + '/css/stylesheet.css?160710',
    sCTX + '/javascript/modules/select2/select2.min.css',
    sCTX + '/javascript/common.js',
    sCTX + '/javascript/pikaday.js',
    sCTX + '/javascript/coolmenus4.js',
    sCTX + '/javascript/shortcut.js',
    sCTX + '/javascript/format_number.js',
    sCTX + '/javascript/time.js',
    sCTX + '/javascript/moment.min.js',
    sCTX + '/javascript/jquery.min.js',
    sCTX + '/javascript/jq/jquery.simplemodal.min.js',
    sCTX + '/javascript/notify.min.js',
    sCTX + '/javascript/jq/spin.min.js',
    sCTX + '/javascript/jq/jquery.spin.js',
    sCTX + '/javascript/modules/select2/select2.min.js',
    sCTX + '/javascript/common_trans.js',    
    sCTX + '/javascript/db/pouchdb.min.js',
    sCTX + '/javascript/db/pouchdb.find.min.js',
    sCTX + '/javascript/db/pouchdb.quick-search.min.js',
    sCTX + '/javascript/db/jsonQ.min.js',
    sCTX + '/javascript/handlebars.min.js',
    sCTX + '/rpos/index.html', 
    sCTX + '/rpos/POS.html',
    sCTX + '/rpos/POSView.html',
	sCTX + '/rpos/ItemView.html'
    //sCTX + '/sw.js'
];

self.addEventListener('install', function(event) {
    console.info('installing service worker');
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.info('cache opened');
                return cache.addAll(resourcesToCache);
            })
            .then(function() {
        		return self.skipWaiting();
      		})
    );
});

self.addEventListener('activate', function(event) {
    console.info('service worker activated');
});

self.addEventListener('fetch', function(event) {
	var hasQuery = event.request.url.indexOf('?') != -1;
	console.log(event.request);
    event.respondWith(
        caches.match(event.request, {ignoreSearch: hasQuery})
            .then(function(response) {                                
                if (response) {
                    console.info(event.request.url + ' cache hit');
                    return response;
                }
                console.info(event.request.url + ' fetching after cache matching');
                return fetch(event.request);
            })
    );
});

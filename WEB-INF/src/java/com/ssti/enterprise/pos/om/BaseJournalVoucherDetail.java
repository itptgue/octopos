package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to JournalVoucherDetail
 */
public abstract class BaseJournalVoucherDetail extends BaseObject
{
    /** The Peer class */
    private static final JournalVoucherDetailPeer peer =
        new JournalVoucherDetailPeer();

        
    /** The value for the journalVoucherDetailId field */
    private String journalVoucherDetailId;
      
    /** The value for the journalVoucherId field */
    private String journalVoucherId;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the accountCode field */
    private String accountCode;
      
    /** The value for the accountName field */
    private String accountName;
      
    /** The value for the debitAmount field */
    private BigDecimal debitAmount;
      
    /** The value for the creditAmount field */
    private BigDecimal creditAmount;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
      
    /** The value for the projectId field */
    private String projectId;
      
    /** The value for the departmentId field */
    private String departmentId;
                                                
    /** The value for the memo field */
    private String memo = "";
      
    /** The value for the debitCredit field */
    private int debitCredit;
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                                
    /** The value for the referenceNo field */
    private String referenceNo = "";
                                          
    /** The value for the subLedgerType field */
    private int subLedgerType = 0;
                                                
    /** The value for the subLedgerId field */
    private String subLedgerId = "";
                                                
    /** The value for the subLedgerDesc field */
    private String subLedgerDesc = "";
  
    
    /**
     * Get the JournalVoucherDetailId
     *
     * @return String
     */
    public String getJournalVoucherDetailId()
    {
        return journalVoucherDetailId;
    }

                        
    /**
     * Set the value of JournalVoucherDetailId
     *
     * @param v new value
     */
    public void setJournalVoucherDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.journalVoucherDetailId, v))
              {
            this.journalVoucherDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JournalVoucherId
     *
     * @return String
     */
    public String getJournalVoucherId()
    {
        return journalVoucherId;
    }

                              
    /**
     * Set the value of JournalVoucherId
     *
     * @param v new value
     */
    public void setJournalVoucherId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.journalVoucherId, v))
              {
            this.journalVoucherId = v;
            setModified(true);
        }
    
                          
                if (aJournalVoucher != null && !ObjectUtils.equals(aJournalVoucher.getJournalVoucherId(), v))
                {
            aJournalVoucher = null;
        }
      
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountCode
     *
     * @return String
     */
    public String getAccountCode()
    {
        return accountCode;
    }

                        
    /**
     * Set the value of AccountCode
     *
     * @param v new value
     */
    public void setAccountCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountCode, v))
              {
            this.accountCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountName
     *
     * @return String
     */
    public String getAccountName()
    {
        return accountName;
    }

                        
    /**
     * Set the value of AccountName
     *
     * @param v new value
     */
    public void setAccountName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountName, v))
              {
            this.accountName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DebitAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getDebitAmount()
    {
        return debitAmount;
    }

                        
    /**
     * Set the value of DebitAmount
     *
     * @param v new value
     */
    public void setDebitAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.debitAmount, v))
              {
            this.debitAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreditAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getCreditAmount()
    {
        return creditAmount;
    }

                        
    /**
     * Set the value of CreditAmount
     *
     * @param v new value
     */
    public void setCreditAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.creditAmount, v))
              {
            this.creditAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Memo
     *
     * @return String
     */
    public String getMemo()
    {
        return memo;
    }

                        
    /**
     * Set the value of Memo
     *
     * @param v new value
     */
    public void setMemo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memo, v))
              {
            this.memo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DebitCredit
     *
     * @return int
     */
    public int getDebitCredit()
    {
        return debitCredit;
    }

                        
    /**
     * Set the value of DebitCredit
     *
     * @param v new value
     */
    public void setDebitCredit(int v) 
    {
    
                  if (this.debitCredit != v)
              {
            this.debitCredit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerType
     *
     * @return int
     */
    public int getSubLedgerType()
    {
        return subLedgerType;
    }

                        
    /**
     * Set the value of SubLedgerType
     *
     * @param v new value
     */
    public void setSubLedgerType(int v) 
    {
    
                  if (this.subLedgerType != v)
              {
            this.subLedgerType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerId
     *
     * @return String
     */
    public String getSubLedgerId()
    {
        return subLedgerId;
    }

                        
    /**
     * Set the value of SubLedgerId
     *
     * @param v new value
     */
    public void setSubLedgerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.subLedgerId, v))
              {
            this.subLedgerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerDesc
     *
     * @return String
     */
    public String getSubLedgerDesc()
    {
        return subLedgerDesc;
    }

                        
    /**
     * Set the value of SubLedgerDesc
     *
     * @param v new value
     */
    public void setSubLedgerDesc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.subLedgerDesc, v))
              {
            this.subLedgerDesc = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private JournalVoucher aJournalVoucher;

    /**
     * Declares an association between this object and a JournalVoucher object
     *
     * @param v JournalVoucher
     * @throws TorqueException
     */
    public void setJournalVoucher(JournalVoucher v) throws TorqueException
    {
            if (v == null)
        {
                  setJournalVoucherId((String) null);
              }
        else
        {
            setJournalVoucherId(v.getJournalVoucherId());
        }
            aJournalVoucher = v;
    }

                                            
    /**
     * Get the associated JournalVoucher object
     *
     * @return the associated JournalVoucher object
     * @throws TorqueException
     */
    public JournalVoucher getJournalVoucher() throws TorqueException
    {
        if (aJournalVoucher == null && (!ObjectUtils.equals(this.journalVoucherId, null)))
        {
                          aJournalVoucher = JournalVoucherPeer.retrieveByPK(SimpleKey.keyFor(this.journalVoucherId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               JournalVoucher obj = JournalVoucherPeer.retrieveByPK(this.journalVoucherId);
               obj.addJournalVoucherDetails(this);
            */
        }
        return aJournalVoucher;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setJournalVoucherKey(ObjectKey key) throws TorqueException
    {
      
                        setJournalVoucherId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("JournalVoucherDetailId");
              fieldNames.add("JournalVoucherId");
              fieldNames.add("AccountId");
              fieldNames.add("AccountCode");
              fieldNames.add("AccountName");
              fieldNames.add("DebitAmount");
              fieldNames.add("CreditAmount");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("Amount");
              fieldNames.add("AmountBase");
              fieldNames.add("ProjectId");
              fieldNames.add("DepartmentId");
              fieldNames.add("Memo");
              fieldNames.add("DebitCredit");
              fieldNames.add("LocationId");
              fieldNames.add("ReferenceNo");
              fieldNames.add("SubLedgerType");
              fieldNames.add("SubLedgerId");
              fieldNames.add("SubLedgerDesc");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("JournalVoucherDetailId"))
        {
                return getJournalVoucherDetailId();
            }
          if (name.equals("JournalVoucherId"))
        {
                return getJournalVoucherId();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("AccountCode"))
        {
                return getAccountCode();
            }
          if (name.equals("AccountName"))
        {
                return getAccountName();
            }
          if (name.equals("DebitAmount"))
        {
                return getDebitAmount();
            }
          if (name.equals("CreditAmount"))
        {
                return getCreditAmount();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("Memo"))
        {
                return getMemo();
            }
          if (name.equals("DebitCredit"))
        {
                return Integer.valueOf(getDebitCredit());
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("SubLedgerType"))
        {
                return Integer.valueOf(getSubLedgerType());
            }
          if (name.equals("SubLedgerId"))
        {
                return getSubLedgerId();
            }
          if (name.equals("SubLedgerDesc"))
        {
                return getSubLedgerDesc();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(JournalVoucherDetailPeer.JOURNAL_VOUCHER_DETAIL_ID))
        {
                return getJournalVoucherDetailId();
            }
          if (name.equals(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID))
        {
                return getJournalVoucherId();
            }
          if (name.equals(JournalVoucherDetailPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(JournalVoucherDetailPeer.ACCOUNT_CODE))
        {
                return getAccountCode();
            }
          if (name.equals(JournalVoucherDetailPeer.ACCOUNT_NAME))
        {
                return getAccountName();
            }
          if (name.equals(JournalVoucherDetailPeer.DEBIT_AMOUNT))
        {
                return getDebitAmount();
            }
          if (name.equals(JournalVoucherDetailPeer.CREDIT_AMOUNT))
        {
                return getCreditAmount();
            }
          if (name.equals(JournalVoucherDetailPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(JournalVoucherDetailPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(JournalVoucherDetailPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(JournalVoucherDetailPeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(JournalVoucherDetailPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(JournalVoucherDetailPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(JournalVoucherDetailPeer.MEMO))
        {
                return getMemo();
            }
          if (name.equals(JournalVoucherDetailPeer.DEBIT_CREDIT))
        {
                return Integer.valueOf(getDebitCredit());
            }
          if (name.equals(JournalVoucherDetailPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(JournalVoucherDetailPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(JournalVoucherDetailPeer.SUB_LEDGER_TYPE))
        {
                return Integer.valueOf(getSubLedgerType());
            }
          if (name.equals(JournalVoucherDetailPeer.SUB_LEDGER_ID))
        {
                return getSubLedgerId();
            }
          if (name.equals(JournalVoucherDetailPeer.SUB_LEDGER_DESC))
        {
                return getSubLedgerDesc();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getJournalVoucherDetailId();
            }
              if (pos == 1)
        {
                return getJournalVoucherId();
            }
              if (pos == 2)
        {
                return getAccountId();
            }
              if (pos == 3)
        {
                return getAccountCode();
            }
              if (pos == 4)
        {
                return getAccountName();
            }
              if (pos == 5)
        {
                return getDebitAmount();
            }
              if (pos == 6)
        {
                return getCreditAmount();
            }
              if (pos == 7)
        {
                return getCurrencyId();
            }
              if (pos == 8)
        {
                return getCurrencyRate();
            }
              if (pos == 9)
        {
                return getAmount();
            }
              if (pos == 10)
        {
                return getAmountBase();
            }
              if (pos == 11)
        {
                return getProjectId();
            }
              if (pos == 12)
        {
                return getDepartmentId();
            }
              if (pos == 13)
        {
                return getMemo();
            }
              if (pos == 14)
        {
                return Integer.valueOf(getDebitCredit());
            }
              if (pos == 15)
        {
                return getLocationId();
            }
              if (pos == 16)
        {
                return getReferenceNo();
            }
              if (pos == 17)
        {
                return Integer.valueOf(getSubLedgerType());
            }
              if (pos == 18)
        {
                return getSubLedgerId();
            }
              if (pos == 19)
        {
                return getSubLedgerDesc();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(JournalVoucherDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        JournalVoucherDetailPeer.doInsert((JournalVoucherDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        JournalVoucherDetailPeer.doUpdate((JournalVoucherDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key journalVoucherDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setJournalVoucherDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setJournalVoucherDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getJournalVoucherDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public JournalVoucherDetail copy() throws TorqueException
    {
        return copyInto(new JournalVoucherDetail());
    }
  
    protected JournalVoucherDetail copyInto(JournalVoucherDetail copyObj) throws TorqueException
    {
          copyObj.setJournalVoucherDetailId(journalVoucherDetailId);
          copyObj.setJournalVoucherId(journalVoucherId);
          copyObj.setAccountId(accountId);
          copyObj.setAccountCode(accountCode);
          copyObj.setAccountName(accountName);
          copyObj.setDebitAmount(debitAmount);
          copyObj.setCreditAmount(creditAmount);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setAmount(amount);
          copyObj.setAmountBase(amountBase);
          copyObj.setProjectId(projectId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setMemo(memo);
          copyObj.setDebitCredit(debitCredit);
          copyObj.setLocationId(locationId);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setSubLedgerType(subLedgerType);
          copyObj.setSubLedgerId(subLedgerId);
          copyObj.setSubLedgerDesc(subLedgerDesc);
  
                    copyObj.setJournalVoucherDetailId((String)null);
                                                                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public JournalVoucherDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("JournalVoucherDetail\n");
        str.append("--------------------\n")
            .append("JournalVoucherDetailId   : ")
           .append(getJournalVoucherDetailId())
           .append("\n")
           .append("JournalVoucherId     : ")
           .append(getJournalVoucherId())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("AccountCode          : ")
           .append(getAccountCode())
           .append("\n")
           .append("AccountName          : ")
           .append(getAccountName())
           .append("\n")
           .append("DebitAmount          : ")
           .append(getDebitAmount())
           .append("\n")
           .append("CreditAmount         : ")
           .append(getCreditAmount())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("Memo                 : ")
           .append(getMemo())
           .append("\n")
           .append("DebitCredit          : ")
           .append(getDebitCredit())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("SubLedgerType        : ")
           .append(getSubLedgerType())
           .append("\n")
           .append("SubLedgerId          : ")
           .append(getSubLedgerId())
           .append("\n")
           .append("SubLedgerDesc        : ")
           .append(getSubLedgerDesc())
           .append("\n")
        ;
        return(str.toString());
    }
}

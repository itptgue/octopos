package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */

public class ItemListReport extends TransactionSecureScreen
{
    //start screen methods
	protected static final int i_ADD_DETAIL  = 1;
	protected static final int i_DEL_DETAIL  = 2;
	protected static final int i_NEW_TRANS   = 3;
	protected static final int i_UPDATE_QTY  = 4;
	protected static final int i_ADD_ITEMS   = 5;
    
    protected List vITDet = null;
    protected HttpSession oSes;
    protected String s_SESKEY = s_ITMD;
    protected int iOp = 0;
    protected boolean bMerge = true;
    
    public void doBuildTemplate ( RunData data, Context context )
    {	
    	super.doBuildTemplate(data, context);    	
		iOp = data.getParameters().getInt("op");
		if(StringUtil.isNotEmpty(data.getParameters().getString("merge")))
		{
			bMerge = data.getParameters().getBoolean("merge");
		}
		initSession (data);
		try 
		{
			if ( iOp == i_ADD_DETAIL ) {
				addDetailData (data);
			}	
			else if ( iOp == i_DEL_DETAIL ) {
				delDetailData (data);
			}	
			else if ( iOp == i_NEW_TRANS ) {
				createNewTrans (data);
			}
			else if ( iOp == i_ADD_ITEMS ) {
				addItems(data);
			}
			else if ( iOp == i_UPDATE_QTY ) {
				updateQty(data);
			}			
		}
		catch (Exception _oEx) 
		{
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_SESKEY, oSes.getAttribute (s_SESKEY));
    }

	/**
	 * @param data
	 */
	protected void updateQty(RunData data) 
	{
		int iNo = data.getParameters().getInt("no");
		if (vITDet != null)
		{
			if (iNo == -1)
			{
				for (int i = 0; i < vITDet.size(); i++)
				{
					int idx = i + 1;
					if (idx <= vITDet.size())
					{
						ItemList oItem = (ItemList) vITDet.get(i);
						
						double dQty = data.getParameters().getDouble("qty" + idx);
						if (oItem != null) oItem.setQty(new BigDecimal(dQty));
					}
				}
			}
			else if (iNo > 0)
			{
				double dQty = data.getParameters().getDouble("qty" + iNo);
				if ((iNo - 1) <= vITDet.size())
				{
					ItemList oItem = (ItemList) vITDet.get(iNo - 1);
					if (oItem != null) oItem.setQty(new BigDecimal(dQty));
				}
			}
		}
	}

	protected void initSession(RunData data)
	{	
		oSes = data.getSession();
		if ( oSes.getAttribute (s_SESKEY) == null ) 
		{
			oSes.setAttribute (s_SESKEY, new ArrayList());
		}
		vITDet = (List) oSes.getAttribute (s_SESKEY);
	}
    
    protected void addDetailData(RunData data) 
    	throws Exception
    {
    	String sItemID = data.getParameters().getString("ItemId"); 
        Item oItem = ItemTool.getItemByID(sItemID);		
        if (oItem != null)
        {
	        ItemList oITDet = new ItemList(oItem);
			data.getParameters().setProperties (oITDet);				
			if (StringUtil.isNotEmpty(oITDet.getItemCode()) && !isExist (oITDet)) 
			{
				vITDet.add (0, oITDet);
			}
        }
		oSes.setAttribute (s_SESKEY, vITDet); 
    }
    
    /**
	 * add multiple items as once. item id saved in query string
	 * parse the id one by one then process it
	 * 
	 * @param data
	 * @throws Exception
	 */
    protected void addItems(RunData data)
    	throws Exception
    {
		String sItemID = data.getParameters().getString ("items");		
		log.debug (sItemID);
		if (sItemID != null && !sItemID.equals(""))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddItem(sItemID);
					
				}
			}
			else {
				processAddItem (sItemID);
			}
		}
	}
    
    protected void delDetailData ( RunData data )
    	throws Exception
    {
        
		if (vITDet.size() > (data.getParameters().getInt("No") - 1)) {
			vITDet.remove ( data.getParameters().getInt("No") - 1 );
			oSes.setAttribute (s_SESKEY, vITDet);
		}    	
    }
    
    protected void processAddItem(String _sItemID)
        throws Exception
    {
        Item oItem = ItemTool.getItemByID(_sItemID);
		ItemList oITDet = new ItemList(oItem);
		oITDet.setQty(bd_ONE);
		
		if (!isExist (oITDet)) 
		{
			vITDet.add (0, oITDet);
		}
		oSes.setAttribute (s_SESKEY, vITDet); 
    }
    
    protected void createNewTrans ( RunData data ) 
    {
    	oSes.setAttribute (s_SESKEY, new ArrayList());
    }
        
    protected boolean isExist (ItemList _oITDet)
    {
    	for (int i = 0; i < vITDet.size(); i++)
    	{
    		ItemList oExist = (ItemList) vITDet.get(i);
    		if(bMerge && (oExist.getItemId().equals(_oITDet.getItemId())))
    		{
    			double dNewQty = oExist.getQty().doubleValue() + _oITDet.getQty().doubleValue();
    			oExist.setQty(new BigDecimal(dNewQty));
    			oExist.setSource(_oITDet.getSource());
    			oExist.setInputBy(_oITDet.getInputBy());    			
				return true;
    		}
    	}
    	return false;
    }
}

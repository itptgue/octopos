package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class GroupItemLookup extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");    		
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}    	
    }
}
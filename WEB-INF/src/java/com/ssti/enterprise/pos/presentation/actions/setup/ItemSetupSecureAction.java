package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.framework.presentation.SecureAction;

public class ItemSetupSecureAction extends SecureAction implements AppAttributes
{
	private static final String[] a_PERM = {"View Item Data"};
	private static final String[] a_UPDATE_PERM = {"View Item Data", "Update Item Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doFind",null) != null)
        {
            return isAuthorized (data, a_PERM);
    	}
        else if(data.getParameters().getString("eventSubmit_doInsert",null) != null ||
                data.getParameters().getString("eventSubmit_doUpdate",null) != null ||
                data.getParameters().getString("eventSubmit_doDelete",null) != null ||
                data.getParameters().getString("eventSubmit_doLoadfile",null) != null ||
                data.getParameters().getString("perform",null) != null)
        {
        	return isAuthorized (data, a_UPDATE_PERM);
    	}
        return true;
    }
}

package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerCfield
 */
public abstract class BaseCustomerCfield extends BaseObject
{
    /** The Peer class */
    private static final CustomerCfieldPeer peer =
        new CustomerCfieldPeer();

        
    /** The value for the cfieldId field */
    private String cfieldId;
                                          
    /** The value for the fno field */
    private int fno = 1;
                                                
    /** The value for the fieldGroup field */
    private String fieldGroup = "";
                                                
    /** The value for the fieldName field */
    private String fieldName = "";
                                          
    /** The value for the fieldType field */
    private int fieldType = 1;
                                                
    /** The value for the fieldValue field */
    private String fieldValue = "";
      
    /** The value for the updateDate field */
    private Date updateDate;
                                                
    /** The value for the lastUpdateBy field */
    private String lastUpdateBy = "";
                                                
    /** The value for the customerTypeId field */
    private String customerTypeId = "";
  
    
    /**
     * Get the CfieldId
     *
     * @return String
     */
    public String getCfieldId()
    {
        return cfieldId;
    }

                        
    /**
     * Set the value of CfieldId
     *
     * @param v new value
     */
    public void setCfieldId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cfieldId, v))
              {
            this.cfieldId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Fno
     *
     * @return int
     */
    public int getFno()
    {
        return fno;
    }

                        
    /**
     * Set the value of Fno
     *
     * @param v new value
     */
    public void setFno(int v) 
    {
    
                  if (this.fno != v)
              {
            this.fno = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldGroup
     *
     * @return String
     */
    public String getFieldGroup()
    {
        return fieldGroup;
    }

                        
    /**
     * Set the value of FieldGroup
     *
     * @param v new value
     */
    public void setFieldGroup(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldGroup, v))
              {
            this.fieldGroup = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldName
     *
     * @return String
     */
    public String getFieldName()
    {
        return fieldName;
    }

                        
    /**
     * Set the value of FieldName
     *
     * @param v new value
     */
    public void setFieldName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldName, v))
              {
            this.fieldName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldType
     *
     * @return int
     */
    public int getFieldType()
    {
        return fieldType;
    }

                        
    /**
     * Set the value of FieldType
     *
     * @param v new value
     */
    public void setFieldType(int v) 
    {
    
                  if (this.fieldType != v)
              {
            this.fieldType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldValue
     *
     * @return String
     */
    public String getFieldValue()
    {
        return fieldValue;
    }

                        
    /**
     * Set the value of FieldValue
     *
     * @param v new value
     */
    public void setFieldValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldValue, v))
              {
            this.fieldValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateBy
     *
     * @return String
     */
    public String getLastUpdateBy()
    {
        return lastUpdateBy;
    }

                        
    /**
     * Set the value of LastUpdateBy
     *
     * @param v new value
     */
    public void setLastUpdateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateBy, v))
              {
            this.lastUpdateBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerTypeId
     *
     * @return String
     */
    public String getCustomerTypeId()
    {
        return customerTypeId;
    }

                        
    /**
     * Set the value of CustomerTypeId
     *
     * @param v new value
     */
    public void setCustomerTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerTypeId, v))
              {
            this.customerTypeId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CfieldId");
              fieldNames.add("Fno");
              fieldNames.add("FieldGroup");
              fieldNames.add("FieldName");
              fieldNames.add("FieldType");
              fieldNames.add("FieldValue");
              fieldNames.add("UpdateDate");
              fieldNames.add("LastUpdateBy");
              fieldNames.add("CustomerTypeId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CfieldId"))
        {
                return getCfieldId();
            }
          if (name.equals("Fno"))
        {
                return Integer.valueOf(getFno());
            }
          if (name.equals("FieldGroup"))
        {
                return getFieldGroup();
            }
          if (name.equals("FieldName"))
        {
                return getFieldName();
            }
          if (name.equals("FieldType"))
        {
                return Integer.valueOf(getFieldType());
            }
          if (name.equals("FieldValue"))
        {
                return getFieldValue();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("LastUpdateBy"))
        {
                return getLastUpdateBy();
            }
          if (name.equals("CustomerTypeId"))
        {
                return getCustomerTypeId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerCfieldPeer.CFIELD_ID))
        {
                return getCfieldId();
            }
          if (name.equals(CustomerCfieldPeer.FNO))
        {
                return Integer.valueOf(getFno());
            }
          if (name.equals(CustomerCfieldPeer.FIELD_GROUP))
        {
                return getFieldGroup();
            }
          if (name.equals(CustomerCfieldPeer.FIELD_NAME))
        {
                return getFieldName();
            }
          if (name.equals(CustomerCfieldPeer.FIELD_TYPE))
        {
                return Integer.valueOf(getFieldType());
            }
          if (name.equals(CustomerCfieldPeer.FIELD_VALUE))
        {
                return getFieldValue();
            }
          if (name.equals(CustomerCfieldPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(CustomerCfieldPeer.LAST_UPDATE_BY))
        {
                return getLastUpdateBy();
            }
          if (name.equals(CustomerCfieldPeer.CUSTOMER_TYPE_ID))
        {
                return getCustomerTypeId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCfieldId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getFno());
            }
              if (pos == 2)
        {
                return getFieldGroup();
            }
              if (pos == 3)
        {
                return getFieldName();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getFieldType());
            }
              if (pos == 5)
        {
                return getFieldValue();
            }
              if (pos == 6)
        {
                return getUpdateDate();
            }
              if (pos == 7)
        {
                return getLastUpdateBy();
            }
              if (pos == 8)
        {
                return getCustomerTypeId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerCfieldPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerCfieldPeer.doInsert((CustomerCfield) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerCfieldPeer.doUpdate((CustomerCfield) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cfieldId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCfieldId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCfieldId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCfieldId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerCfield copy() throws TorqueException
    {
        return copyInto(new CustomerCfield());
    }
  
    protected CustomerCfield copyInto(CustomerCfield copyObj) throws TorqueException
    {
          copyObj.setCfieldId(cfieldId);
          copyObj.setFno(fno);
          copyObj.setFieldGroup(fieldGroup);
          copyObj.setFieldName(fieldName);
          copyObj.setFieldType(fieldType);
          copyObj.setFieldValue(fieldValue);
          copyObj.setUpdateDate(updateDate);
          copyObj.setLastUpdateBy(lastUpdateBy);
          copyObj.setCustomerTypeId(customerTypeId);
  
                    copyObj.setCfieldId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerCfieldPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerCfield\n");
        str.append("--------------\n")
           .append("CfieldId             : ")
           .append(getCfieldId())
           .append("\n")
           .append("Fno                  : ")
           .append(getFno())
           .append("\n")
           .append("FieldGroup           : ")
           .append(getFieldGroup())
           .append("\n")
           .append("FieldName            : ")
           .append(getFieldName())
           .append("\n")
           .append("FieldType            : ")
           .append(getFieldType())
           .append("\n")
           .append("FieldValue           : ")
           .append(getFieldValue())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("LastUpdateBy         : ")
           .append(getLastUpdateBy())
           .append("\n")
           .append("CustomerTypeId       : ")
           .append(getCustomerTypeId())
           .append("\n")
        ;
        return(str.toString());
    }
}

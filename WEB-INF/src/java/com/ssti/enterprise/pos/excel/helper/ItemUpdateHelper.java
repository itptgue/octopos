package com.ssti.enterprise.pos.excel.helper;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.CustomField;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.CustomFieldTool;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.ItemStatusCodeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;


/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-08-08
 * - add field brand in EXCEL FILE after manufacturer column
 * - @see {@link ItemFieldTool} getManufacturer & getBrand
 * </pre><br>
 */
public class ItemUpdateHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public static String[] a_HEADER = 
    {
        "Item Code"             ,
        "Barcode"               ,
        "Item Name"             ,
        "Description"           ,
        "SKU"                   ,
        "SKU Name" 		        ,
        "Item Type"             ,
        "Sales Price"           ,
        "Minimum Price"			,
        "Last Purchase"         ,
        "Last Purchase Curr"    ,
        "Last Purchase Rate"    ,        
        "Unit Code"             ,
        "Purchase Unit Code"    ,
        "Unit Conversion" 		,
        "Kategori Code"         ,
        "Sales Tax Code"        ,
        "Purchase Tax Code"     ,
        "Vendor Code"           ,
        "Vendor Item Code"      ,
        "Vendor Item Name"      ,
        "Item Status Code"      ,
        "Warehouse Code"        ,
        "Rack Location"         ,
        "Max Stock"             ,
        "Min Stock"             ,
        "Reorder Point"         ,
        "Margin Setup"          ,
        "Inventory Acc."        ,
        "Sales Acc."            ,
        "Sales Return Acc."     ,
        "Item Discount Acc."    ,
        "COGS Acc."        		,
        "Purchase Return Acc."  ,
        "Expense Acc."          ,
        "Unbilled Goods Acc."   ,
        "Manufacturer"   		,
        "Brand"   				,
        "Color"                 ,
        "Size"                  ,
        "Size Unit"             ,
        "Fixed Price"  		    ,
        "Track Batch No"        ,            
        "Track Serial No"       ,
        "Status"				,
        "Discountinue"		    ,        
        "Consignment"			,
        "Picture File"          ,
        "Tags"					,
        "Discount Amount"		        
    };
    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Item Update Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        
        for (int i = 0; i < a_HEADER.length; i++)
        {
		    createHeaderCell(oWB, oRow, (short)iCol, a_HEADER[i]); iCol++;
	    }
		
		//CUSTOM FIELD HEADER
		List vCField = CustomFieldTool.getAllCustomField();
		Iterator oIter = vCField.iterator();
		while (oIter.hasNext())
		{
			CustomField oCField = (CustomField) oIter.next();
			createHeaderCell(oWB, oRow, (short)iCol,  oCField.getFieldName()); iCol++;			
		}

        List vData = (List) _oSession.getAttribute("Items");
        
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Item oItem = (Item) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oItem.getItemCode   ());                              iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getBarcode    ());                              iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getItemName   ());                              iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getDescription());                              iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getItemSku    ());                              iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getItemSkuName());                              iCol++;                 
                createCell(oWB, oRow2, (short)iCol, ItemTool.getItemType(oItem.getItemType ()));          iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getItemPrice  ().toString());                   iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getMinPrice  ().toString());                    iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getLastPurchasePrice().toString());             iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getLastPurchCurrCode());              		  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getLastPurchaseRate().toString());              iCol++; 
                createCell(oWB, oRow2, (short)iCol, UnitTool.getCodeByID(oItem.getUnitId()));             iCol++; 
                createCell(oWB, oRow2, (short)iCol, UnitTool.getCodeByID(oItem.getPurchaseUnitId()));     iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getUnitConversion());                           iCol++; 
                createCell(oWB, oRow2, (short)iCol, KategoriTool.getParentToChildCodeByID(oItem.getKategoriId()));     iCol++; 
                createCell(oWB, oRow2, (short)iCol, TaxTool.getCodeByID(oItem.getTaxId()));               iCol++; 
                createCell(oWB, oRow2, (short)iCol, TaxTool.getCodeByID(oItem.getPurchaseTaxId()));       iCol++; 
                createCell(oWB, oRow2, (short)iCol, VendorTool.getCodeByID(oItem.getPreferedVendorId())); iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getVendorItemCode());                    		  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getVendorItemName());                    		  iCol++; 
                createCell(oWB, oRow2, (short)iCol, ItemStatusCodeTool.getCodeByID(oItem.getItemStatusCodeId()));      iCol++; 
                createCell(oWB, oRow2, (short)iCol, LocationTool.getCodeByID(oItem.getWarehouseId()));    iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getRackLocation());                    		  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getMaximumStock().toString());                  iCol++;  
                createCell(oWB, oRow2, (short)iCol, oItem.getMinimumStock().toString());                  iCol++;   
                createCell(oWB, oRow2, (short)iCol, oItem.getReorderPoint().toString());                  iCol++;   
                createCell(oWB, oRow2, (short)iCol, oItem.getProfitMargin());                             iCol++;   
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getInventoryAccount()) );			iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getSalesAccount()));    			iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getSalesReturnAccount()));        iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getItemDiscountAccount()));       iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getCogsAccount()));               iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getPurchaseReturnAccount()));     iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getExpenseAccount()));            iCol++;
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oItem.getUnbilledGoodsAccount()));      iCol++;
                createCell(oWB, oRow2, (short)iCol, ItemFieldTool.getManufacturer(oItem)); 	iCol++; 
                createCell(oWB, oRow2, (short)iCol, ItemFieldTool.getBrand(oItem)); 		iCol++;
                createCell(oWB, oRow2, (short)iCol, oItem.getColor()); 	      iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getSize());         iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getSizeUnit());     iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oItem.getIsFixedPrice()));  iCol++;                 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oItem.getTrackBatchNo()));  iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oItem.getTrackSerialNo())); iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oItem.getItemStatus())); 	 iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oItem.getIsDiscontinue())); iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oItem.getIsConsignment())); iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oItem.getItemPicture());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getTags()); iCol++;
                createCell(oWB, oRow2, (short)iCol, oItem.getDiscountAmount());  iCol++; 
                
                //CUSTOM FIELD VALUE
        		oIter = vCField.iterator();
        		while (oIter.hasNext())
        		{
        			CustomField oCField = (CustomField) oIter.next();
        			createCell(oWB, oRow2, (short)iCol, 
        				ItemFieldTool.getValue(oItem.getItemId(), oCField.getFieldName())); iCol++;			
        		}
            }        
        }
        return oWB;
    }
}

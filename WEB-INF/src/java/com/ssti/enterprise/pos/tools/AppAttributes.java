package com.ssti.enterprise.pos.tools;

import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AppAttributes.java,v 1.82 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: AppAttributes.java,v $
 * 
 * 2017-05-14
 * - add Attribute i_ASSET add item type Asset
 * 
 * </pre><br>
 */
public interface AppAttributes extends Attributes
{	
	public static final String s_DB_TYPE = CONFIG.getString("retailsoft.db.server");

	public static final boolean b_IS_MYSQL = "mysql".equals(s_DB_TYPE);
	public static final boolean b_IS_PGSQL = "postgresql".equals(s_DB_TYPE);
	public static final boolean b_IS_ORACLE = "oracle".equals(s_DB_TYPE);
			
	//LOCATION FUNCTION / TYPE
	public static final int i_HEAD_OFFICE = 1;
	public static final int i_STORE 	  = 2;
	public static final int i_WAREHOUSE	  = 3;
	public static final int i_CONSIGNMENT = 4;

	//item merge setting
	public static final int i_MERGE_NOT = 0;
	public static final int i_MERGE_ITM = 1;
	public static final int i_MERGE_ERR = 2;
	
    //multi currency transaction constants
    //every trans return double[] 0 = base, 1 = prime
    public static final int i_BASE  = 0;
    public static final int i_PRIME = 1;	
	
	//TRANSACTION FORMAT
	public static final String s_FORMAT_MO  = "mm";
	public static final String s_FORMAT_YR  = "yy";
	public static final String s_FORMAT_LC  = "lc";	
	public static final String s_SEPARATOR  = CONFIG.getString("trans.no.separator");	
	public static final String s_FORMAT_NO  = CONFIG.getString("trans.no.variable");
	
    public static final String s_RQ_FORMAT = CONFIG.getString("rq.format");
    public static final String s_PO_FORMAT = CONFIG.getString("po.format");
    public static final String s_PR_FORMAT = CONFIG.getString("pr.format");
    public static final String s_PI_FORMAT = CONFIG.getString("pi.format");
    public static final String s_PT_FORMAT = CONFIG.getString("pt.format");

    public static final String s_QT_FORMAT = CONFIG.getString("qt.format");
    public static final String s_SO_FORMAT = CONFIG.getString("so.format");
    public static final String s_DO_FORMAT = CONFIG.getString("do.format");
    public static final String s_SI_FORMAT = CONFIG.getString("si.format");
    public static final String s_SR_FORMAT = CONFIG.getString("sr.format");
    public static final String s_TX_FORMAT = CONFIG.getString("tx.format");
    public static final String s_TD_FORMAT = CONFIG.getString("td.format");
    public static final String s_TS_FORMAT = CONFIG.getString("ts.format");
    
    public static final String s_PW_FORMAT = CONFIG.getString("pw.format"); //POINT REWARD
    
    public static final String s_IR_FORMAT = CONFIG.getString("ir.format");
    public static final String s_IT_FORMAT = CONFIG.getString("it.format");
    public static final String s_PL_FORMAT = CONFIG.getString("pl.format");
    public static final String s_JC_FORMAT = CONFIG.getString("jc.format");
    public static final String s_CC_FORMAT = CONFIG.getString("cc.format");
    
    public static final String s_CM_FORMAT = CONFIG.getString("cm.format");
    public static final String s_DM_FORMAT = CONFIG.getString("dm.format");
    public static final String s_RP_FORMAT = CONFIG.getString("rp.format");
    public static final String s_PP_FORMAT = CONFIG.getString("pp.format");
    public static final String s_PC_FORMAT = CONFIG.getString("pc.format");
    public static final String s_CF_FORMAT = CONFIG.getString("cf.format");
    public static final String s_BT_FORMAT = CONFIG.getString("bt.format");

    public static final String s_FA_FORMAT = CONFIG.getString("fa.format");
    public static final String s_JV_FORMAT = CONFIG.getString("jv.format");
    public static final String s_OB_FORMAT = CONFIG.getString("ob.format"); //OPENING BALANCE
    public static final String s_CA_FORMAT = CONFIG.getString("ca.format"); //COST ADJUSTMENT
    
	//LAST PURCHASE COUNTING METHOD
	public static final int i_ITEM_PRICE_METHOD   = 1;
	public static final int i_DISCOUNT_METHOD     = 2;
	public static final int i_DISCOUNT_ALL_METHOD = 3;
	public static final int i_TAX_INC_METHOD      = 4;
	
	//COSTING METHOD
	public static final int i_WEIGHTED_AVERAGE = 1;
	public static final int i_SIMPLE_AVERAGE   = 2;
	public static final int i_LAST_COST		   = 3;

	//INVENTORY TYPE
	public static final int i_INV_NORMAL   = 1;
	public static final int i_INV_MINUS    = 2;
	public static final int i_INV_DROPSHIP = 3;
		
    //SO STATUS
	public static final int i_SO_ORDERED   = 1;	
	public static final int i_SO_DELIVERED = 2;	
	public static final int i_SO_CLOSED    = 3;
	public static final int i_SO_NEW       = 4;	
	public static final int i_SO_CANCELLED = 5;
	
	//DO STATUS
	public static final int i_DO_NEW      	= 1;	
	public static final int i_DO_DELIVERED 	= 2;
	public static final int i_DO_CANCELLED	= 3;
	public static final int i_DO_INVOICED   = 4;
	
	//TRANS STATUS
	public static final int i_TRANS_PENDING   = 1;
	public static final int i_TRANS_PROCESSED = 2;
	public static final int i_TRANS_CANCELLED = 3;
	public static final int i_TRANS_CLOSED	  = 4;

	//RQ STATUS
	public static final int i_RQ_NEW        = 1;	
	public static final int i_RQ_APPROVED   = 2;
	public static final int i_RQ_CONFIRMED  = 3;
	public static final int i_RQ_TRANSFERED = 4;
	public static final int i_RQ_CLOSED 	= 5;

	//PO STATUS
	public static final int i_PO_ORDERED   = 1;	
	public static final int i_PO_RECEIVED  = 2;	
	public static final int i_PO_CLOSED    = 3;
	public static final int i_PO_NEW       = 4;	
	public static final int i_PO_CANCELLED = 5;
	
	//INVOICE PAYMENT STATUS (SI / PI)
	public static final int i_PAYMENT_OPEN  = 1;	
	public static final int i_PAYMENT_PAID  = 2;

	//PI STATUS
	public static final int i_PI_PENDING   = 1;
	public static final int i_PI_PROCESSED = 2;
	public static final int i_PI_CANCELLED = 3;

	//PR STATUS
	public static final int i_PR_NEW      	= 1;	
	public static final int i_PR_APPROVED 	= 2;
	public static final int i_PR_REJECTED 	= 3;
	public static final int i_PR_INVOICED   = 4;
	public static final int i_PR_CANCELLED 	= 5;
	
	//PRet STATUS
	public static final int i_RET_FROM_PR_DO    = 1;	
	public static final int i_RET_FROM_PI_SI    = 2;	
	public static final int i_RET_FROM_NONE     = 3;	
		
	//CREDIT & DEBIT MEMO STATUS
	public static final int i_MEMO_OPEN			= 1;	
	public static final int i_MEMO_CLOSED 		= 2;
	public static final int i_MEMO_CANCELLED 	= 3;
	public static final int i_MEMO_NEW 			= 4;
	

	//AR PAYMENT, AP PAYMENT, CASH FLOW STATUS
	public static final int i_PAYMENT_RECEIVED  = 1;	
	public static final int i_PAYMENT_PROCESSED = 2;
	public static final int i_PAYMENT_CANCELLED = 3;
	
	//ITEM STATUS
	public static final boolean b_ACTIVE_ITEM      = true;	
	public static final boolean b_NONACTIVE_ITEM   = false;

	//INVENTORY IN / OUT
    public static final int i_INV_IN  = 1;
    public static final int i_INV_OUT = 2;
    public static final int i_INV_TRF = 3;    
	
	//CASH FLOW AND PETTY CASH
    public static final int i_DEPOSIT    = 1;
    public static final int i_WITHDRAWAL = 2;

	//INVENTORY -> USED BY ITEM TRANSFER, ISSUE RECEIPT, JOURNAL VOUCHER
	public static final int i_PENDING   = 1;
	public static final int i_PROCESSED = 2;
	public static final int i_CANCELLED = 3;
    
    //ITEM TRANSFER, INTERBRANCH TRANSFER
    public static final int i_RECEIVED  = 4;
    
    //ITEM TRANSFER (PROCESSED / RECEIVED), PO/SO (ORDERED,CLOSED), DO/PR (DELIVERED/INVOICED)
    public static final int i_MULTISTAT = 9;
    	
	//INVENTORY TRANSACTION
	public static final int i_INV_TRANS_RECEIPT_UNPLANNED = 1;
	public static final int i_INV_TRANS_ISSUE_UNPLANNED   = 2;
	public static final int i_INV_TRANS_PURCHASE_RECEIPT  = 3;
	public static final int i_INV_TRANS_PURCHASE_RETURN   = 4;
	//public static final int i_INV_TRANS_ITEM_TRANSFER 	  = 5;
	public static final int i_INV_TRANS_SALES_INVOICE 	  = 6;
	public static final int i_INV_TRANS_SALES_RETURN 	  = 7;
	public static final int i_INV_TRANS_PURCHASE_INVOICE  = 10;
	public static final int i_INV_TRANS_DELIVERY_ORDER    = 11;
	public static final int i_INV_TRANS_JOB_COSTING       = 12;
	public static final int i_INV_TRANS_JC_ROLLOVER 	  = 13;	
	public static final int i_INV_TRANS_TRANSFER_OUT 	  = 14;
	public static final int i_INV_TRANS_TRANSFER_IN 	  = 15;	
	
	//AR TRANSACTION
	public static final int i_AR_TRANS_SALES            = 1;
	public static final int i_AR_TRANS_CASH_RECEIPT     = 2;
	public static final int i_AR_TRANS_CREDIT_MEMO      = 3;
	public static final int i_AR_TRANS_OPENING_BALANCE  = 5;
	public static final int i_AR_TRANS_SO_DOWNPAYMENT   = 6;
	public static final int i_AR_TRANS_SALES_RETURN     = 7;	
	
	//AP TRANSACTION	
	public static final int i_AP_TRANS_PURCHASE_RECEIPT = 1;
	public static final int i_AP_TRANS_CASH_PAYMENT 	= 2;
	public static final int i_AP_TRANS_DEBIT_MEMO  		= 3;
	public static final int i_AP_TRANS_PURCHASE_INVOICE	= 4;
    public static final int i_AP_TRANS_OPENING_BALANCE  = 5;
	public static final int i_AP_TRANS_PO_DOWNPAYMENT   = 6;
	public static final int i_AP_TRANS_PURCHASE_RETURN  = 7;
	public static final int i_AP_TRANS_PI_TAX  			= 8;
	public static final int i_AP_TRANS_PI_FREIGHT  		= 9;	
	
	//public static final int i_AP_TRANS_PURCHASE_INVOICE_CANCEL	= 8;
	
	//CASH FLOW TRANSACTION	TYPE                                    
	public static final int i_CF_AR_PAYMENT      = 1;
	public static final int i_CF_AP_PAYMENT      = 2;
	public static final int i_CF_NORMAL          = 3;	
	public static final int i_CF_OPENING_BALANCE = 4;
	public static final int i_CF_DIRECT_SALES    = 5;
	public static final int i_CF_CREDIT_MEMO     = 6;
	public static final int i_CF_DEBIT_MEMO      = 7;
    public static final int i_CF_BANK_TRANSFER   = 8;
    	
	//JV & CF SUB LEDGER TYPE
    public static int i_SUB_LEDGER_AP = 1;
    public static int i_SUB_LEDGER_AR = 2;
    public static int i_SUB_LEDGER_CASH = 3;
	
	//JOB COSTING
	public static final int i_JC_NEW       = 1;	
	public static final int i_JC_CREATED   = 2;	
	public static final int i_JC_FINISHED  = 3;	
	public static final int i_JC_CANCELLED    = 4;
	
	//ITEM TYPE
	public static final int i_INVENTORY_PART 	 = 1;
	public static final int i_NON_INVENTORY_PART = 2;
	public static final int i_GROUPING 			 = 3;
	public static final int i_SERVICE 			 = 4;
	public static final int i_ASSET			     = 5;	
	
	//TRANSACTION GROUP
	//use to define item type for this group of trans
	public static final int i_PURCHASE_TRANSACTION 	= 1;
	public static final int i_SALES_TRANSACTION 	= 2;
	public static final int i_INVENTORY_TRANSACTION = 3;

	//TRANSACTION RELATED
    public static final String s_PO   = "po";		//PurchaseOrder
    public static final String s_POD  = "poDet";    //PurchaseOrder
    public static final String s_PI   = "pi";		//PurchaseInvoice
    public static final String s_PID  = "piDet";    //PurchaseInvoice
    public static final String s_PIF  = "piFrt";    //PurchaseInvoice
    public static final String s_PIE  = "piExp";    //PurchaseInvoice    
    public static final String s_PR   = "pr";		//PurchaseReceipt
    public static final String s_PRD  = "prDet";    //PurchaseReceipt
    public static final String s_PRT  = "pret";	    //PurchaseReturn
    public static final String s_PRTD = "pretDet";  //PurchaseReturn

	public static final String s_QT  = "qt";		//Quotation
    public static final String s_QTD = "qtDet";     //Quotation
	public static final String s_SO  = "so";		//SalesOrder
    public static final String s_SOD = "soDet";     //SalesOrder
    public static final String s_DO  = "do";		//DeliveryOrder
    public static final String s_DOD = "doDet";     //DeliveryOrder
	public static final String s_TR  = "trans";    	//SalesTransaction 
    public static final String s_TD  = "transDet"; 	//SalesTransactionDetail
    public static final String s_VD  = "voidDet"; 	//VoidDetail
    public static final String s_PMT = "vPmt";    	//InvoicePayment List
    public static final String s_SR  = "sret";	    //SalesReturn
    public static final String s_SRD = "sretDet";  	//SalesReturn

    public static final String s_PMT_VCH = "vPmtVoucher";   //InvoicePayment List Voucher 

    public static final String s_RQ  = "rq";		//PurchaseRequest
    public static final String s_RQD = "rqDet";     //PurchaseRequest
	public static final String s_IT   = "it";    	//Item Transfer Master 
	public static final String s_ITD  = "itDet";    //Item Transfer Detail 
	public static final String s_IR   = "ir";       //Issue Receipt Master 
	public static final String s_IRD  = "irDet";    //Issue Receipt Detail 
    public static final String s_ITMD = "itemDet";  //Item List
    public static final String s_PL   = "pl";    	//Packing List
    public static final String s_PLD  = "plDet"; 	//Packing List

    public static final String s_JC  = "jc";     	 //Job Costing
    public static final String s_JCD = "jcDet";    	 //Job Costing Detail
    public static final String s_JCE = "jcExp";    	 //Job Costing Expense Detail
    public static final String s_JCF  = "jcf";     	 //Job Costing Finished
    public static final String s_JCFD = "jcfDet";    //Job Costing Finished Detail
    public static final String s_JCFACC = "jcfAcc";  //Job Costing Finished Account Detail
    
    public static final String s_AR   = "ar";     		//AccountReceivable
    public static final String s_ARD  = "arDet";    	//AccountReceivable
    public static final String s_ARDP = "arDP";			//AccountReceivable
    public static final String s_ARDPTMP = "arDPTemp";	//AccountReceivable
    
    public static final String s_AP   = "ap";     		//AccountPayable
    public static final String s_APD  = "apDet";    	//AccountPayable
    public static final String s_APDP = "apDP";			//AccountPayable
    public static final String s_APDPTMP = "apDPTemp";	//AccountPayable
    
    public static final String s_JV  = "jv";     	//Journal Voucher
    public static final String s_JVD = "jvDet";    	//Journal Voucher
    public static final String s_CF  = "cf";     	//Cash Flow
    public static final String s_CFD = "cfDet";    	//Cash Flow
    
    public static final String s_FA  = "fa";     	//Fixed Asset 
    public static final String s_FAD = "faDet";    	//Fixed Asset 
    public static final String s_FAM = "faMnt";     //Fixed Asset 
    public static final String s_FAN = "faNot";    	//Fixed Asset     
    	    
	public static final String s_SERIAL = "vSerial"; //serial list Session & Context
		
    //SYNC RELATED
	public static final int i_STORE_TO_HO  = 1;
	public static final int i_HO_TO_STORE  = 2;
	public static final int i_STORE_SETUP  = 3;   
	
	//ERROR MESSAGE
	public static final String s_DUPLICATE_MSG = "Duplicate entry";	
	
	//SEARCH RELATED
	public static final String s_COND = "Condition";
	public static final String s_KEY = "Keywords";
	public static final String s_START_DATE = "StartDate";
	public static final String s_END_DATE = "EndDate";
	public static final String s_VIEW_LIMIT = "ViewLimit";
	public static final String s_GROUP_BY = "GroupBy";	
	public static final String s_CURRENCY_ID = "CurrencyId";
	public static final String s_LOCATION_ID = "LocationId";

    //TRANS_TYPE
    public static final String s_TRANS_OPENING_BALANCE   = "Opening Balance";
	public static final String s_TRANS_RECEIPT_UNPLANNED = "Issue Receipt";
	public static final String s_TRANS_ISSUE_UNPLANNED   = "Issue Receipt";
	public static final String s_TRANS_PURCHASE_ORDER    = "Purchase Order";	
	public static final String s_TRANS_PURCHASE_RECEIPT  = "Purchase Receipt";
	public static final String s_TRANS_PURCHASE_INVOICE  = "Purchase Invoice";
	public static final String s_TRANS_PURCHASE_RETURN   = "Purchase Return";
	public static final String s_TRANS_ITEM_TRANSFER 	 = "Item Transfer";
	public static final String s_TRANS_SALES_ORDER       = "Sales Order";	
	public static final String s_TRANS_DELIVERY_ORDER    = "Delivery Order";
	public static final String s_TRANS_SALES_INVOICE     = "Sales Invoice";
	public static final String s_TRANS_SALES_RETURN 	 = "Sales Return";
	public static final String s_TRANS_FIXED_ASSET 	     = "Fixed Asset";
	public static final String s_TRANS_JOURNAL_VOUCHER   = "Journal Voucher";
	public static final String s_TRANS_AR_PAYMENT        = "Receivable Payment";
	public static final String s_TRANS_AP_PAYMENT        = "Payable Payment";
	public static final String s_TRANS_AR_MEMO           = "Credit Memo";
	public static final String s_TRANS_AP_MEMO           = "Debit Memo";
	public static final String s_TRANS_CASH_MANAGEMENT   = "Cash Management";
    public static final String s_TRANS_BANK_TRANSFER     = "Bank Transfer";    
	public static final String s_TRANS_JOB_COSTING       = "Job Costing";
	public static final String s_TRANS_JC_ROLLOVER       = "Job Roll Over";	
	
	//TRANSACTION SCREEN
	public static final String s_SCREEN_ISSUE_RECEIPT     = "transaction,IssueReceiptTransaction.vm";
	public static final String s_SCREEN_ITEM_TRANSFER 	  = "transaction,ItemTransferTransaction.vm";
	
	public static final String s_SCREEN_PURCHASE_ORDER    = "transaction,PurchaseOrderTransaction.vm";
	public static final String s_SCREEN_PURCHASE_RECEIPT  = "transaction,PurchaseReceiptTransaction.vm";
	public static final String s_SCREEN_PURCHASE_INVOICE  = "transaction,PurchaseInvoiceTransaction.vm";
	public static final String s_SCREEN_PURCHASE_RETURN   = "transaction,PurchaseReturnTransaction.vm";
	
	public static final String s_SCREEN_SALES_ORDER    	  = "transaction,SalesOrderTransaction.vm";	
	public static final String s_SCREEN_DELIVERY_ORDER    = "transaction,DeliveryOrderTransaction.vm";
	public static final String s_SCREEN_SALES_INVOICE	  = "transaction,SalesInvoiceMulti.vm";                   
	public static final String s_SCREEN_SALES_RETURN 	  = "transaction,SalesReturnTransaction.vm";
	
	public static final String s_SCREEN_AR_PAYMENT        = "transaction,ReceivablePayment.vm";
	public static final String s_SCREEN_AP_PAYMENT        = "transaction,PayablePayment.vm";
	public static final String s_SCREEN_CREDIT_MEMO       = "transaction,CreditMemoTransaction.vm";
	public static final String s_SCREEN_DEBIT_MEMO        = "transaction,DebitMemoTransaction.vm";
	public static final String s_SCREEN_CASH_MANAGEMENT   = "transaction,CashManagement.vm";    
    public static final String s_SCREEN_BANK_TRANSFER     = "transaction,BankTransferTrans.vm";    
    
	public static final String s_SCREEN_JOURNAL_VOUCHER   = "transaction,JournalVoucherTransaction.vm";

	public static final String s_SCREEN_FIXED_ASSET 	  = "transaction,fixedasset,FixedAssetTransaction.vm"; 
	public static final String s_SCREEN_JOB_COSTING       = "transaction,jobcost,JobCostingTransaction.vm";
	public static final String s_SCREEN_JC_ROLLOVER       = "transaction,jobcost,JobCostRollOver.vm";		
	
    public static final double d_NUMBER_TOLERANCE = CONFIG.getDouble("application.number.tolerance");
    
    public static final String SYSLOC = "SYSLOC"; //system install location
    public static final String USRLOC = "USRLOC"; //user set location
    public static final String ACCLOC = "ACCLOC"; //access all location
    public static final String ACCSITE = "ACCSITE"; //access all site
    
    public static final String s_CASHIER_ROLE = "Cashier"; //cashier role
    
    public static final String s_ERR_STACK_TPL = "/util/ErrorStack.vm";
}
package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemVendor;
import com.ssti.enterprise.pos.om.ItemVendorPeer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemVendorTool.java,v 1.11 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemVendorTool.java,v $
 * </pre><br>
 */
public class ItemVendorTool extends BaseTool
{
	private static ItemVendorTool instance = null;
	
	public static synchronized ItemVendorTool getInstance()
	{
		if (instance == null) instance = new ItemVendorTool();
		return instance;
	}
	
	public static List getAll()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return ItemVendorPeer.doSelect(oCrit);
	}
	
	public static List getByItem(String _sItemID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemVendorPeer.ITEM_ID, _sItemID);
		return ItemVendorPeer.doSelect(oCrit);
	}

	public static List getByVendor(String _sVendID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemVendorPeer.VENDOR_ID, _sVendID);
		return ItemVendorPeer.doSelect(oCrit);
	}

	public static List getItemIDByVendor(List _vVendor)
		throws Exception
	{
		List vItemID = new ArrayList();
		for (int i = 0; i < _vVendor.size(); i++)
		{
			Vendor oVendor = (Vendor) _vVendor.get(i);
			vItemID.addAll(getItemIDByVendor(oVendor.getVendorId()));
		}
		return vItemID;
	}
	
	public static List getItemIDByVendor(String _sVendID)
		throws Exception
	{
		List vData = getByVendor(_sVendID);
		List vItemID = new ArrayList(vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			ItemVendor oData = (ItemVendor) vData.get(i);
			vItemID.add(oData.getItemId());
		}
		return vItemID;
	}
	
	public static ItemVendor getByID(String _sItemVendorID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemVendorPeer.ITEM_VENDOR_ID, _sItemVendorID);
		List vData = ItemVendorPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (ItemVendor) vData.get(0);
		}
		return null;
	}
	
	public static void delete(String _sItemVendorID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemVendorPeer.ITEM_VENDOR_ID, _sItemVendorID);
		ItemVendorPeer.doDelete(oCrit, _oConn);
	}

	public static ItemVendor getItemVendor(String _sItemID, String _sVendorID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sItemID)) oCrit.add(ItemVendorPeer.ITEM_ID, _sItemID);
		if (StringUtil.isNotEmpty(_sVendorID)) oCrit.add(ItemVendorPeer.VENDOR_ID, _sVendorID);		
		List vData = ItemVendorPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (ItemVendor) vData.get(0);
		}
		return null;
	}
	
	/**
	 * 
	 * @param _sItemID
	 * @param _sVendorID
	 * @param _sValue
	 * @throws Exception
	 */
	public static void save(String _sItemID, String _sVendorID, boolean _bPref) 
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sItemID) && StringUtil.isNotEmpty(_sVendorID))
		{
			ItemVendor oData = getItemVendor (_sItemID, _sVendorID);
			if (oData == null)
			{
				oData = new ItemVendor ();
				oData.setItemId(_sItemID);
				oData.setVendorId(_sVendorID);
				oData.setItemVendorId(IDGenerator.generateSysID());
			}
			else
			{
				oData.setItemId(_sItemID);
				oData.setVendorId(_sVendorID);
			}
			oData.setUpdateDate(new Date());
			oData.save();
			
			if(_bPref)
			{
				Item oItem = ItemTool.getItemByID(_sItemID);
				if (oItem != null)
				{
					oItem.setPreferedVendorId(_sVendorID);
					oItem.save();
					ItemManager.getInstance().refreshCache(oItem);
				}
			}
		}
	}

	public static List getUpdated (boolean _bActiveOnly)
		throws Exception
	{
		return getUpdated(_bActiveOnly, null);
	}		
	
	public static List getUpdated (boolean _bActiveOnly, Date _dLast)
		throws Exception
	{
		if (_dLast == null)
		{
			_dLast = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE);
		}
		Criteria oCrit = new Criteria();
		if (_dLast  != null) 
		{
	    	oCrit.add(ItemVendorPeer.UPDATE_DATE, _dLast, Criteria.GREATER_EQUAL);   
	    	if ( _bActiveOnly ) 
	    	{
	    		oCrit.addJoin(ItemPeer.ITEM_ID, ItemVendorPeer.ITEM_ID);
	    		oCrit.add(ItemPeer.ITEM_STATUS, b_ACTIVE_ITEM);
			}
		}
		return ItemVendorPeer.doSelect(oCrit);
	}		
	
	public static List getNew(List _vItems)
		throws Exception
	{
		if (_vItems != null && _vItems.size() > 0)
		{
			List vIDs = new ArrayList(_vItems.size());
			for (int i = 0; i < _vItems.size(); i++)
			{
				Item oItem = (Item) _vItems.get(i);
				vIDs.add(oItem.getItemId());
			}
			if (vIDs.size() > 0) 
			{
				Criteria oCrit = new Criteria();
				oCrit.addIn(ItemVendorPeer.ITEM_ID, vIDs);
				return ItemVendorPeer.doSelect(oCrit);
			}
		}		
		return new ArrayList(1);		
	}	
}

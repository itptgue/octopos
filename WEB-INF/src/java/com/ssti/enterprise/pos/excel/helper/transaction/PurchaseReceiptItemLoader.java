package com.ssti.enterprise.pos.excel.helper.transaction;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.excel.helper.ItemListLoader;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/PurchaseOrderLoader.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseOrderLoader.java,v 1.12 2008/08/17 02:17:21 albert Exp $
 *
 * $Log: PurchaseOrderLoader.java,v $
 * Revision 1.12  2008/08/17 02:17:21  albert
 * *** empty log message ***
 */
public class PurchaseReceiptItemLoader extends BaseExcelLoader implements ItemListLoader 
{    
    private static Log log = LogFactory.getLog (PurchaseReceiptItemLoader.class);
    private String sLocationID;
    private List vPRD = null;
    
    public PurchaseReceiptItemLoader(String _sLocationID)
        throws Exception    
    {
    	sLocationID = _sLocationID;
    }        
    
    public void setPRD(List _vPRD) 
    {
		this.vPRD = _vPRD;
	}

	protected void updateList (HSSFRow _oRow, short _iIdx) 
        throws Exception
    {
    	int iRow = _oRow.getRowNum();
   		short i = 0;
		   		
		String sCode = getString(_oRow,_iIdx + i);
		String sDesc = getString(_oRow,_iIdx + 2);
        Item oItem = ItemTool.getItemByCode(sCode);
        BigDecimal bdQty = getBigDecimal(_oRow,_iIdx + 5);
        double dQty = bdQty.doubleValue();
        double dQtyBase = dQty;
                        
        if(oItem != null && vPRD != null && vPRD.size() > 0)
        {
    		for (int  j = 0; j < vPRD.size(); j++)
    		{        			
        		PurchaseReceiptDetail oPRD = (PurchaseReceiptDetail) vPRD.get(j);
        		if(StringUtil.isEqual(oItem.getItemId(), oPRD.getItemId()))
        		{        				
	        		PurchaseOrderDetail oPOD = oPRD.getPurchaseOrderDetail();        		
	        		double dPO = 0;
	        		double dReceived = 0;
	        		if(oPOD != null) 
	        		{
	        			dPO = oPOD.getQty().doubleValue();
	        			dReceived = oPOD.getReceivedQty().doubleValue();
	        			if(!PreferenceTool.getPurchPrQtyGtPo() && (dQty > (dPO - dReceived)))
	        			{
	        				setError (iRow, sCode, "Receipt Qty " + dQty + " May not more than PO " + dPO  + " - Received " + dReceived);
	        			}   
	        			else
	        			{
	        				oPRD.setQty(bdQty);
	        				oPRD.setQtyBase(UnitTool.getBaseQty(oPRD.getItemId(), oPRD.getUnitId(), bdQty));
	        			}
	        		}
	        		vPRD.set(j, oPRD);
        		}
    		}
        }
        else
        {
        	if(oItem == null) setError (iRow, sCode, "Item Not Found");
        	if(vPRD == null || vPRD.size() == 0) setError(iRow, sCode, "Empty Received List ");
        }
        m_vDataList = vPRD;
    }        
    
    void setError (int _iRow, String _sCode, String _sMsg)
    {
		m_iError++;
		
		StringBuilder oMsg = new StringBuilder();
		oMsg.append (m_iError);					
		oMsg.append (". ERROR Loading ROW : ");
		oMsg.append (_iRow + 1);
		oMsg.append (" ITEM CODE " + _sCode + ", " + _sMsg );
		oMsg.append ("\n");	

		m_oError.append (oMsg);
		log.error(oMsg);
    }
}

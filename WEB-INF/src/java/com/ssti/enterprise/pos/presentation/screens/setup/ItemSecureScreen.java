package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.framework.presentation.SecureScreen;

public class ItemSecureScreen extends SecureScreen implements AppAttributes
{
	private static final String[] a_PERM = {"View Item Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
}

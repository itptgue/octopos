package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureAction;

public class PaymentSetupSecureAction extends SecureAction
{
	private static final String[] a_PERM = {"View Payment Setup"};
	private static final String[] a_UPDATE_PERM = {"View Payment Setup", "Update Payment Setup"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doFind",null) != null)
        {
            return isAuthorized (data, a_PERM);
    	}
        else if(data.getParameters().getString("eventSubmit_doInsert",null) != null ||
                data.getParameters().getString("eventSubmit_doUpdate",null) != null ||
                data.getParameters().getString("eventSubmit_doDelete",null) != null ||
                data.getParameters().getString("eventSubmit_doLoadfile",null) != null ||
                data.getParameters().getString("perform",null) != null)
        {
        	return isAuthorized (data, a_UPDATE_PERM);
    	}
        return true;
    }	
}

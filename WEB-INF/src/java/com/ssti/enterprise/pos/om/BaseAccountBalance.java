package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AccountBalance
 */
public abstract class BaseAccountBalance extends BaseObject
{
    /** The Peer class */
    private static final AccountBalancePeer peer =
        new AccountBalancePeer();

        
    /** The value for the accountBalanceId field */
    private String accountBalanceId;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the balance field */
    private BigDecimal balance;
      
    /** The value for the primeBalance field */
    private BigDecimal primeBalance;
  
    
    /**
     * Get the AccountBalanceId
     *
     * @return String
     */
    public String getAccountBalanceId()
    {
        return accountBalanceId;
    }

                        
    /**
     * Set the value of AccountBalanceId
     *
     * @param v new value
     */
    public void setAccountBalanceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountBalanceId, v))
              {
            this.accountBalanceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Balance
     *
     * @return BigDecimal
     */
    public BigDecimal getBalance()
    {
        return balance;
    }

                        
    /**
     * Set the value of Balance
     *
     * @param v new value
     */
    public void setBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.balance, v))
              {
            this.balance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrimeBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getPrimeBalance()
    {
        return primeBalance;
    }

                        
    /**
     * Set the value of PrimeBalance
     *
     * @param v new value
     */
    public void setPrimeBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.primeBalance, v))
              {
            this.primeBalance = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AccountBalanceId");
              fieldNames.add("AccountId");
              fieldNames.add("Balance");
              fieldNames.add("PrimeBalance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AccountBalanceId"))
        {
                return getAccountBalanceId();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("Balance"))
        {
                return getBalance();
            }
          if (name.equals("PrimeBalance"))
        {
                return getPrimeBalance();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AccountBalancePeer.ACCOUNT_BALANCE_ID))
        {
                return getAccountBalanceId();
            }
          if (name.equals(AccountBalancePeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(AccountBalancePeer.BALANCE))
        {
                return getBalance();
            }
          if (name.equals(AccountBalancePeer.PRIME_BALANCE))
        {
                return getPrimeBalance();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAccountBalanceId();
            }
              if (pos == 1)
        {
                return getAccountId();
            }
              if (pos == 2)
        {
                return getBalance();
            }
              if (pos == 3)
        {
                return getPrimeBalance();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AccountBalancePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AccountBalancePeer.doInsert((AccountBalance) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AccountBalancePeer.doUpdate((AccountBalance) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key accountBalanceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAccountBalanceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAccountBalanceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAccountBalanceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AccountBalance copy() throws TorqueException
    {
        return copyInto(new AccountBalance());
    }
  
    protected AccountBalance copyInto(AccountBalance copyObj) throws TorqueException
    {
          copyObj.setAccountBalanceId(accountBalanceId);
          copyObj.setAccountId(accountId);
          copyObj.setBalance(balance);
          copyObj.setPrimeBalance(primeBalance);
  
                    copyObj.setAccountBalanceId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AccountBalancePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AccountBalance\n");
        str.append("--------------\n")
           .append("AccountBalanceId     : ")
           .append(getAccountBalanceId())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("Balance              : ")
           .append(getBalance())
           .append("\n")
           .append("PrimeBalance         : ")
           .append(getPrimeBalance())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerPoint
 */
public abstract class BaseCustomerPoint extends BaseObject
{
    /** The Peer class */
    private static final CustomerPointPeer peer =
        new CustomerPointPeer();

        
    /** The value for the customerPointId field */
    private String customerPointId;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the totalPoint field */
    private BigDecimal totalPoint;
      
    /** The value for the lastUpdate field */
    private Date lastUpdate;
  
    
    /**
     * Get the CustomerPointId
     *
     * @return String
     */
    public String getCustomerPointId()
    {
        return customerPointId;
    }

                        
    /**
     * Set the value of CustomerPointId
     *
     * @param v new value
     */
    public void setCustomerPointId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerPointId, v))
              {
            this.customerPointId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalPoint
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalPoint()
    {
        return totalPoint;
    }

                        
    /**
     * Set the value of TotalPoint
     *
     * @param v new value
     */
    public void setTotalPoint(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalPoint, v))
              {
            this.totalPoint = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdate
     *
     * @return Date
     */
    public Date getLastUpdate()
    {
        return lastUpdate;
    }

                        
    /**
     * Set the value of LastUpdate
     *
     * @param v new value
     */
    public void setLastUpdate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdate, v))
              {
            this.lastUpdate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerPointId");
              fieldNames.add("CustomerId");
              fieldNames.add("TotalPoint");
              fieldNames.add("LastUpdate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerPointId"))
        {
                return getCustomerPointId();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("TotalPoint"))
        {
                return getTotalPoint();
            }
          if (name.equals("LastUpdate"))
        {
                return getLastUpdate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerPointPeer.CUSTOMER_POINT_ID))
        {
                return getCustomerPointId();
            }
          if (name.equals(CustomerPointPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerPointPeer.TOTAL_POINT))
        {
                return getTotalPoint();
            }
          if (name.equals(CustomerPointPeer.LAST_UPDATE))
        {
                return getLastUpdate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerPointId();
            }
              if (pos == 1)
        {
                return getCustomerId();
            }
              if (pos == 2)
        {
                return getTotalPoint();
            }
              if (pos == 3)
        {
                return getLastUpdate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerPointPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerPointPeer.doInsert((CustomerPoint) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerPointPeer.doUpdate((CustomerPoint) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerPointId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomerPointId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomerPointId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerPointId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerPoint copy() throws TorqueException
    {
        return copyInto(new CustomerPoint());
    }
  
    protected CustomerPoint copyInto(CustomerPoint copyObj) throws TorqueException
    {
          copyObj.setCustomerPointId(customerPointId);
          copyObj.setCustomerId(customerId);
          copyObj.setTotalPoint(totalPoint);
          copyObj.setLastUpdate(lastUpdate);
  
                    copyObj.setCustomerPointId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerPointPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerPoint\n");
        str.append("-------------\n")
           .append("CustomerPointId      : ")
           .append(getCustomerPointId())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("TotalPoint           : ")
           .append(getTotalPoint())
           .append("\n")
           .append("LastUpdate           : ")
           .append(getLastUpdate())
           .append("\n")
        ;
        return(str.toString());
    }
}

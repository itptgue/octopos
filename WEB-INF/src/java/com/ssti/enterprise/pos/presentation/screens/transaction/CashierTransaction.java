package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CashierBalance;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.ShiftTool;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.DateUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2015-09-01
 * - get default amount from preferences
 * </pre><br>
 */
public class CashierTransaction extends TransactionSecureScreen
{
    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
    	AccessControlList oACL = data.getACL();
    	if (oACL != null)
    	{
	    	int iOp = data.getParameters().getInt("op",1);
	    	if (iOp == 1)
	    	{
		    	if (oACL != null && oACL.getRoles().containsName("Cashier"))
		    	{
		    		return true;
		    	}	    	
	    	}
	    	else if (iOp == 4)
	    	{
	    		return true;
	    	}
    	}
    	return false;
	}
	
	public void doBuildTemplate(RunData data, Context context)
    {
        try
        {
        	super.doBuildTemplate(data, context);
        	String sCashier = data.getUser().getName();
        	String sLocID = PreferenceTool.getLocationID(data.getUser().getName());
        	//get cashier trans status 
        	int iType = data.getParameters().getInt("Type");
        	int iOp = data.getParameters().getInt("op",1);
        	
        	if (iOp == 1)
        	{            	        	
            	//get last OPEN balance
            	CashierBalance oBalance = 
            		CashierBalanceTool.getBalance(sCashier,sLocID,"","",CashierBalanceTool.i_OPEN, i_DESC);
        		
            	boolean bClosed = false;
        		if (oBalance != null) bClosed = CashierBalanceTool.isClosed(oBalance);            	
            	if (iType == CashierBalanceTool.i_OPEN)
            	{
            		context.put("defaultAmount", PreferenceTool.getSysConfig().getPosDefaultOpenCashierAmt());
            		if (oBalance != null && !bClosed) 
            		{
            			context.put("oBalance", oBalance);
            		}
            	}
            	else if (iType == CashierBalanceTool.i_CLOSE)
            	{
    	            if (oBalance != null && !bClosed)
    	            {    	            	
    	            	context.put("vPending", TransactionTool.findTrans(oBalance.getTransactionDate(), DateUtil.getTodayDate(), i_TRANS_PENDING, "", "", oBalance.getCashierName()));
    	            	context.put("oOpening", oBalance);
    	            	context.put("isOpened", Boolean.valueOf(true));
    	            }
            	}	
        	}
        	if (iOp == 4)
        	{
        		String sID = data.getParameters().getString("id");        		
        		CashierBalance oBalance = CashierBalanceTool.getByID(sID);
        		context.put("oCloseTrans", oBalance);
        	}
        	context.put("type", Integer.valueOf(iType));
        	context.put("cashierbalance", CashierBalanceTool.getInstance());
        	context.put("shifttool", ShiftTool.getInstance());   	        
        }
    	catch(Exception _oEx)
    	{
    		_oEx.printStackTrace();
			data.setMessage( _oEx.getMessage());
    	}
    }
}

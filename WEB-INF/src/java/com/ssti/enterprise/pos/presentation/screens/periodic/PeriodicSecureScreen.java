package com.ssti.enterprise.pos.presentation.screens.periodic;

import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.presentation.SecureScreen;

public class PeriodicSecureScreen extends SecureScreen implements AppAttributes
{
	public static final boolean b_ALLOW_PENDING = Boolean.valueOf(PreferenceTool.getSysConfig().getSyncAllowPendingTrans());

	protected static final String[] a_PERM = {"View Periodic Transaction"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return super.isAuthorized(data, a_PERM);
    }
}

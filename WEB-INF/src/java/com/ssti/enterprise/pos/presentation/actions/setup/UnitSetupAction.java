package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.UnitLoader;
import com.ssti.enterprise.pos.manager.UnitManager;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.UnitPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;
/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class UnitSetupAction extends POSSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Unit oUnit = new Unit();
			setProperties (oUnit, data);
			oUnit.setUnitId (IDGenerator.generateSysID());
	        oUnit.save();
	        UnitManager.getInstance().refreshCache(oUnit);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doUpdate(RunData data, Context context)
        throws Exception
    {
    	try
    	{
	    	Unit oUnit = UnitTool.getUnitByID(data.getParameters().getString("ID"));
        	Unit oOld = oUnit.copy();        	
	    	setProperties (oUnit, data);
        	oUnit.setBaseUnit(data.getParameters().getBoolean("BaseUnit"));
        	oUnit.save();
	        UnitManager.getInstance().refreshCache(oUnit);
	        UpdateHistoryTool.createHistory(oOld, oUnit, data.getUser().getName(), null);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("id");
    		if(SqlUtil.validateFKRef("item","unit_id",sID)
    			&& SqlUtil.validateFKRef("item_group","unit_id",sID)
    			&& SqlUtil.validateFKRef("sales_transaction_detail","unit_id",sID)
    			&& SqlUtil.validateFKRef("sales_return_detail","unit_id",sID)
    			&& SqlUtil.validateFKRef("purchase_request_detail","unit_id",sID)
    			&& SqlUtil.validateFKRef("purchase_order_detail","unit_id",sID)
    			&& SqlUtil.validateFKRef("purchase_receipt_detail","unit_id",sID)
    			&& SqlUtil.validateFKRef("purchase_return_detail","unit_id",sID))
    		{
    			Unit oUnit = UnitTool.getUnitByID(sID);
    			
	    		Criteria oCrit = new Criteria();
	        	String sCode = UnitTool.getCodeByID(sID);
	        	oCrit.add(UnitPeer.UNIT_ID, sID);
	        	UnitPeer.doDelete(oCrit);
	        	UnitManager.getInstance().refreshCache(sID, sCode);
	        	UpdateHistoryTool.createDelHistory(oUnit, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

	public void setProperties (Unit _oUnit, RunData data)
    {
    	try
    	{
	    	data.getParameters().setProperties(_oUnit);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
	
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			UnitLoader oLoader = new UnitLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
}

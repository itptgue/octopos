package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;

public class PurchaseReturnTransaction extends PurchaseSecureScreen
{
    private static final String[] a_PERM = {"Purchase Transaction", "View Purchase Return"};

    private static final Log log = LogFactory.getLog(PurchaseReturnTransaction.class);

    private static final int i_OP_NEW  		  	  = 1;
    private static final int i_OP_GET_TRANS   	  = 2;
	private static final int i_OP_VIEW_RETURN 	  = 3;
	private static final int i_OP_VIEW_RETURN_ALT = 4;
	private static final int i_OP_ADD_NEW_ITEM    = 5;
	private static final int i_OP_DEL_ITEM  	  = 6;

    protected HttpSession oSes;
    protected PurchaseReturn oPRT = null;    
    protected List vPRTD = null;
    
    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		return isAuthorized (data, a_PERM);
	}
    
    public void doBuildTemplate ( RunData data, Context context )
    {	
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_NEW ) 
			{
		    	newTransaction(data);		
            }	
			else if ( iOp == i_OP_GET_TRANS && !b_REFRESH) 
			{
				getTransData (data, context);
			}	
			else if (iOp == i_OP_VIEW_RETURN || (iOp == i_OP_VIEW_RETURN_ALT)) 
			{
				getData (data, context);
			}
			else if (iOp == i_OP_ADD_NEW_ITEM && !b_REFRESH) 
			{
				addDetailData (data);
			}	
			else if (iOp == i_OP_DEL_ITEM && !b_REFRESH) 
			{
				delDetailData (data);
			}	
			
     		context.put(s_RET_DET, oSes.getAttribute (s_PRTD));
     		context.put(s_RET, oSes.getAttribute (s_PRT));
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			data.setMessage("Error : " + _oEx.getMessage());
		}
    }

    private void getTransData ( RunData data, Context context  ) 
    	throws Exception
    {
    	validateVendor();
    	String sTransID = data.getParameters().getString("TransID");    	
    	try 
    	{	
    		if(data.getParameters().getInt("ReturnFrom") == i_RET_FROM_PR_DO)
    		{
	    		PurchaseReceipt oPR = PurchaseReceiptTool.getHeaderByID(sTransID);
	    		if (oPR != null)
	    		{
		    		List vPRDet = PurchaseReceiptTool.getDetailsByID(oPR.getPurchaseReceiptId());
			    	PurchaseReturnTool.mapPRToPRet(oPR,oPRT);
			    	vPRTD = new ArrayList (vPRDet.size());
	
			    	for (int i = 0; i < vPRDet.size(); i++)
			    	{
			    		PurchaseReceiptDetail oPRD = (PurchaseReceiptDetail) vPRDet.get(i);
			    		PurchaseReturnDetail oPRetD = new PurchaseReturnDetail();
			    		PurchaseReturnTool.mapPRDetailToPRetDetail (oPRD, oPRT, oPRetD);
			    		vPRTD.add (oPRetD);
			    	}
	    		}
	    		oPRT.setTransactionType(i_RET_FROM_PR_DO);
	    		oPRT.setIsInclusiveTax(oPR.getIsInclusiveTax());
    		}

    		if(data.getParameters().getInt("ReturnFrom") == i_RET_FROM_PI_SI)
    		{
	    		PurchaseInvoice oPI = PurchaseInvoiceTool.getHeaderByID(sTransID);
	    		if (oPI != null)
	    		{
		    		List vPRDet = PurchaseInvoiceTool.getDetailsByID(oPI.getPurchaseInvoiceId());
			    	PurchaseReturnTool.mapPIToPRet(oPI,oPRT);
			    	vPRTD = new ArrayList (vPRDet.size());
	
			    	for (int i = 0; i < vPRDet.size(); i++)
			    	{
			    		PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail) vPRDet.get(i);
			    		PurchaseReturnDetail oPRetD = new PurchaseReturnDetail();
			    		PurchaseReturnTool.mapPIDetailToPRetDetail (oPID, oPRT, oPRetD);
			    		vPRTD.add (oPRetD);
			    	}
	    		}
	    		oPRT.setTransactionType(i_RET_FROM_PI_SI);
	    		oPRT.setIsInclusiveTax(oPI.getIsInclusiveTax());
    		}    		
		    oSes.setAttribute (s_PRT, oPRT);
	        oSes.setAttribute (s_PRTD, vPRTD);
	  	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    	}
    }
    
    private void getData ( RunData data, Context context ) 
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (sID != null && (!sID.equals(""))) {
    		try 
    		{
    		    oSes.setAttribute (s_PRT, PurchaseReturnTool.getHeaderByID (sID));
		        oSes.setAttribute (s_PRTD, PurchaseReturnTool.getDetailsByID (sID));
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }
    
    private boolean isExist (PurchaseReturnDetail _oPRet, RunData data) 
    	throws Exception
    {
    	for (int i = 0; i < vPRTD.size(); i++) 
    	{
    		PurchaseReturnDetail oExistTD = (PurchaseReturnDetail) vPRTD.get (i);
    		if(oExistTD.getItemCode().equals(_oPRet.getItemCode()) && 
    		    (oExistTD.getItemId().equals(_oPRet.getItemId())) && 
    		    (oExistTD.getUnitId().equals(_oPRet.getUnitId())) && 
    		    (oExistTD.getReturnAmount().intValue() == _oPRet.getReturnAmount().intValue()) 
    		    ) 
    		{
				mergeData (oExistTD, _oPRet);
				return true;    			
    		}		
    	}
    	return false;
    }
    
    private void mergeData (PurchaseReturnDetail _oOldTD, PurchaseReturnDetail _oNewTD) 
    {		
    	double dQty 	  = _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
    	double dTotalPurc = dQty * _oNewTD.getItemPrice().doubleValue();
    	double dSubTotal  = dTotalPurc ;

    	_oOldTD.setQty 	        (new BigDecimal(dQty));    	
    	_oOldTD.setReturnAmount (new BigDecimal(dSubTotal));
    	_oNewTD = null;
    }
    
    private void addDetailData ( RunData data ) 
    	throws Exception
    {
    	validateVendor();
        PurchaseReturnTool.updateDetail(vPRTD,data);
		PurchaseReturnDetail oPRetDet = new PurchaseReturnDetail();
		data.getParameters().setProperties (oPRetDet);	
		oPRetDet.setTransactionDetailId("");
		if (!isExist (oPRetDet, data)) 
		{
		    if (b_PURCHASE_FIRST_LAST)
		    {
			    vPRTD.add(0, oPRetDet);
			}
			else{
			    vPRTD.add (oPRetDet);
			}
		}
        PurchaseReturnTool.setHeaderProperties(oPRT,vPRTD,data);
		oSes.setAttribute (s_PRTD, vPRTD); 
		oSes.setAttribute (s_PRT, oPRT);     
    }
    
    private void delDetailData ( RunData data )
    	throws Exception
    {
    	int iIDX = data.getParameters().getInt("no") - 1;    
        PurchaseReturnTool.updateDetail(vPRTD,data);
		if (vPRTD.size() > iIDX) 
		{
			vPRTD.remove(iIDX);
		}    	
        PurchaseReturnTool.setHeaderProperties(oPRT,vPRTD,data);
		oSes.setAttribute (s_PRTD, vPRTD);
		oSes.setAttribute (s_PRT, oPRT); 
    }
    
    private void initSession ( RunData data )
	{	
    	synchronized (this)
    	{
    		oSes = data.getSession();
    		if ( oSes.getAttribute (s_PRT) == null ) 
    		{
    			oSes.setAttribute (s_PRT, new PurchaseReturn());
    			oSes.setAttribute (s_PRTD, new ArrayList());
    		}
    		oPRT = (PurchaseReturn) oSes.getAttribute (s_PRT);
    		vPRTD = (List) oSes.getAttribute (s_PRTD);
    	}
	}
    
    private void newTransaction ( RunData data )
    {
    	synchronized (this)
    	{
    		int iRetFrom = data.getParameters().getInt("ReturnFrom",i_RET_FROM_PR_DO);
    		String sVendorID = data.getParameters().getString("VendorId","");
    		
    		oPRT = new PurchaseReturn();
    		vPRTD = new ArrayList();
    		oPRT.setReturnFrom(iRetFrom);
    		oPRT.setVendorId(sVendorID);
    		oSes.setAttribute (s_PRT, oPRT);
    		oSes.setAttribute (s_PRTD, vPRTD);	    		
    	}
    }
    
    private void validateVendor()
    	throws Exception
    {
    	if(oPRT != null && oPRT.getVendor() != null && oPRT.getVendor().getAllowReturn() == 2)
    	{
    		throw new Exception(LocaleTool.getString("vendor") + " Non Returnable");
    	}
    }
}

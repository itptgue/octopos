package com.gpos.views.actions;

import java.util.Enumeration;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.TurbineConstants;
import org.apache.turbine.modules.Action;
import org.apache.turbine.om.security.User;
import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.TurbineSecurityException;

import com.ssti.framework.mvc.session.SessionHandler;
import com.ssti.framework.presentation.SecureAttributes;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Logout Action 
 * This action removes a user from the session. It makes sure to save
 * the User object in the session.
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: Logout.java,v 1.11 2009/05/04 02:00:25 albert Exp $ <br>
 *
 * <pre>
 * $Log: Logout.java,v $
 * 2015-10-01
 * - Remove Attributes at Logout from getAttributeNames Enum
 * 
 * </pre><br>
 */
public class Logout extends Action implements SecureAttributes
{
    /** Logging */
    private static Log log = LogFactory.getLog(Logout.class);
	
    /**
     * Clears the RunData user object back to an anonymous status not
     * logged in, and with a null ACL.  If the tr.props ACTION_LOGIN
     * is anthing except "LogoutUser", flow is transfered to the
     * SCREEN_HOMEPAGE
     *
     * If this action name is the value of action.logout then we are
     * being run before the session validator, so we don't need to
     * set the screen (we assume that the session validator will handle
     * that). This is basically still here simply to preserve old behaviour
     * - it is recommended that action.logout is set to "LogoutUser" and
     * that the session validator does handle setting the screen/template
     * for a logged out (read not-logged-in) user.
     *
     * @param data Turbine information.
     * @exception TurbineSecurityException a problem occured in the security
     *            service.
     */
    public void doPerform(RunData data)
            throws TurbineSecurityException
    {
    	logout(data);
        data.setScreenTemplate(s_LOGIN_TPL);
    }
    
    public static void logout(RunData data)
    	throws TurbineSecurityException
    {
        User user = data.getUser();

        if (!TurbineSecurity.isAnonymousUser(user))
        {
            // Make sure that the user has really logged in...
            if (!user.hasLoggedIn())
            {
                return;
            }
            user.setHasLoggedIn(Boolean.FALSE);
            try 
            {
            	SessionHandler.deleteActiveUser(user.getName(),"");
            	TurbineSecurity.saveUser(user);			
            } 
            catch (Exception e) {
				log.error(e);
			}                        
        }

        Configuration conf = Attributes.CONFIG;

        data.setMessage(conf.getString(TurbineConstants.LOGOUT_MESSAGE, ""));

        // This will cause the acl to be removed from the session in
        // the Turbine servlet code.
        data.setACL(null);

        // Retrieve an anonymous user.
        data.setUser(TurbineSecurity.getAnonymousUser());
        data.save();

        Enumeration<String> names = data.getSession().getAttributeNames();
        while(names.hasMoreElements())
        {
        	String sKey = names.nextElement();
        	log.debug("Clear Session Key:" + sKey);        	
        	data.getSession().removeAttribute(sKey);
        }
        data.getSession().invalidate();
    }   
}       

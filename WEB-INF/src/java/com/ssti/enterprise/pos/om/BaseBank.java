package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Bank
 */
public abstract class BaseBank extends BaseObject
{
    /** The Peer class */
    private static final BankPeer peer =
        new BankPeer();

        
    /** The value for the bankId field */
    private String bankId;
      
    /** The value for the bankCode field */
    private String bankCode;
      
    /** The value for the accountNo field */
    private String accountNo;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the description field */
    private String description;
                                                
    /** The value for the accountId field */
    private String accountId = "";
                                                
          
    /** The value for the openingBalance field */
    private BigDecimal openingBalance= bd_ZERO;
      
    /** The value for the asDate field */
    private Date asDate;
                                                
    /** The value for the obTransId field */
    private String obTransId = "";
                                                
          
    /** The value for the obRate field */
    private BigDecimal obRate= new BigDecimal(1);
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                                                
    /** The value for the isCash field */
    private boolean isCash = false;
  
    
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankCode
     *
     * @return String
     */
    public String getBankCode()
    {
        return bankCode;
    }

                        
    /**
     * Set the value of BankCode
     *
     * @param v new value
     */
    public void setBankCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankCode, v))
              {
            this.bankCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountNo
     *
     * @return String
     */
    public String getAccountNo()
    {
        return accountNo;
    }

                        
    /**
     * Set the value of AccountNo
     *
     * @param v new value
     */
    public void setAccountNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountNo, v))
              {
            this.accountNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AsDate
     *
     * @return Date
     */
    public Date getAsDate()
    {
        return asDate;
    }

                        
    /**
     * Set the value of AsDate
     *
     * @param v new value
     */
    public void setAsDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.asDate, v))
              {
            this.asDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObTransId
     *
     * @return String
     */
    public String getObTransId()
    {
        return obTransId;
    }

                        
    /**
     * Set the value of ObTransId
     *
     * @param v new value
     */
    public void setObTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.obTransId, v))
              {
            this.obTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObRate
     *
     * @return BigDecimal
     */
    public BigDecimal getObRate()
    {
        return obRate;
    }

                        
    /**
     * Set the value of ObRate
     *
     * @param v new value
     */
    public void setObRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.obRate, v))
              {
            this.obRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsCash
     *
     * @return boolean
     */
    public boolean getIsCash()
    {
        return isCash;
    }

                        
    /**
     * Set the value of IsCash
     *
     * @param v new value
     */
    public void setIsCash(boolean v) 
    {
    
                  if (this.isCash != v)
              {
            this.isCash = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BankId");
              fieldNames.add("BankCode");
              fieldNames.add("AccountNo");
              fieldNames.add("CurrencyId");
              fieldNames.add("Description");
              fieldNames.add("AccountId");
              fieldNames.add("OpeningBalance");
              fieldNames.add("AsDate");
              fieldNames.add("ObTransId");
              fieldNames.add("ObRate");
              fieldNames.add("LocationId");
              fieldNames.add("IsCash");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankCode"))
        {
                return getBankCode();
            }
          if (name.equals("AccountNo"))
        {
                return getAccountNo();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("AsDate"))
        {
                return getAsDate();
            }
          if (name.equals("ObTransId"))
        {
                return getObTransId();
            }
          if (name.equals("ObRate"))
        {
                return getObRate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("IsCash"))
        {
                return Boolean.valueOf(getIsCash());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BankPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(BankPeer.BANK_CODE))
        {
                return getBankCode();
            }
          if (name.equals(BankPeer.ACCOUNT_NO))
        {
                return getAccountNo();
            }
          if (name.equals(BankPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(BankPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(BankPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(BankPeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(BankPeer.AS_DATE))
        {
                return getAsDate();
            }
          if (name.equals(BankPeer.OB_TRANS_ID))
        {
                return getObTransId();
            }
          if (name.equals(BankPeer.OB_RATE))
        {
                return getObRate();
            }
          if (name.equals(BankPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(BankPeer.IS_CASH))
        {
                return Boolean.valueOf(getIsCash());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBankId();
            }
              if (pos == 1)
        {
                return getBankCode();
            }
              if (pos == 2)
        {
                return getAccountNo();
            }
              if (pos == 3)
        {
                return getCurrencyId();
            }
              if (pos == 4)
        {
                return getDescription();
            }
              if (pos == 5)
        {
                return getAccountId();
            }
              if (pos == 6)
        {
                return getOpeningBalance();
            }
              if (pos == 7)
        {
                return getAsDate();
            }
              if (pos == 8)
        {
                return getObTransId();
            }
              if (pos == 9)
        {
                return getObRate();
            }
              if (pos == 10)
        {
                return getLocationId();
            }
              if (pos == 11)
        {
                return Boolean.valueOf(getIsCash());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BankPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BankPeer.doInsert((Bank) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BankPeer.doUpdate((Bank) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key bankId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBankId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBankId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBankId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Bank copy() throws TorqueException
    {
        return copyInto(new Bank());
    }
  
    protected Bank copyInto(Bank copyObj) throws TorqueException
    {
          copyObj.setBankId(bankId);
          copyObj.setBankCode(bankCode);
          copyObj.setAccountNo(accountNo);
          copyObj.setCurrencyId(currencyId);
          copyObj.setDescription(description);
          copyObj.setAccountId(accountId);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setAsDate(asDate);
          copyObj.setObTransId(obTransId);
          copyObj.setObRate(obRate);
          copyObj.setLocationId(locationId);
          copyObj.setIsCash(isCash);
  
                    copyObj.setBankId((String)null);
                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BankPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Bank\n");
        str.append("----\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankCode             : ")
           .append(getBankCode())
           .append("\n")
           .append("AccountNo            : ")
           .append(getAccountNo())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("AsDate               : ")
           .append(getAsDate())
           .append("\n")
           .append("ObTransId            : ")
           .append(getObTransId())
           .append("\n")
           .append("ObRate               : ")
           .append(getObRate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("IsCash               : ")
           .append(getIsCash())
           .append("\n")
        ;
        return(str.toString());
    }
}

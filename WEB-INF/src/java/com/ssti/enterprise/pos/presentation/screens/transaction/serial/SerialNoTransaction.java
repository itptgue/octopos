package com.ssti.enterprise.pos.presentation.screens.transaction.serial;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.SerialTransaction;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.AppAttributes;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/serial/SerialNoTransaction.java,v $
 * Purpose: Serial Transaction Screen Class
 *
 * @author  $Author: albert $
 * @version $Id: SerialNoTransaction.java,v 1.4 2008/02/26 00:39:37 albert Exp $
 * @see SerialTransaction
 *
 * $Log: SerialNoTransaction.java,v $
 * Revision 1.4  2008/02/26 00:39:37  albert
 * *** empty log message ***
 *
 * Revision 1.3  2005/08/21 14:35:54  albert
 * *** empty log message ***
 *
 * Revision 1.2  2005/03/28 09:18:23  Albert
 * *** empty log message ***
 *
 * Revision 1.1  2004/11/25 13:14:11  albert
 * prototype
 *
 */

public class SerialNoTransaction extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
            HttpSession oSes = data.getSession();
            List vSerial = (List) oSes.getAttribute(AppAttributes.s_SERIAL);
            String sItemID = data.getParameters().getString("ItemId");
            String sLocationID = data.getParameters().getString("LocationId");
            int iType = data.getParameters().getInt("Type");
            
            if (oSes != null)
            {                                                  
                int iOp = data.getParameters().getInt ("op", 0);
                
                //if op == 1, submit from form
                if (iOp == 1)
                {
                    int iNo = data.getParameters().getInt("TotalNo");

                    List vSerialTrans = new ArrayList(iNo);

                    for (int i = 0; i < iNo; i++)
                    {
                        String sSerialNo = data.getParameters().getString("SerialNo" + i);
                        SerialTransaction oTrans = new SerialTransaction();
                        oTrans.setSerialNo   	 (sSerialNo);
                        oTrans.setItemId         (sItemID);
                        oTrans.setLocationId     (sLocationID);
                        oTrans.setTransactionType(iType);                        
                        vSerialTrans.add (oTrans);
                    }
                    vSerial.add(vSerialTrans);
                    oSes.setAttribute(AppAttributes.s_SERIAL, vSerial);                    
                }    
    	    }
    	}
    	catch (Exception _oEx)
    	{
    		_oEx.printStackTrace();
    		data.setMessage("Serial Transaction Save Failed : " + _oEx.getMessage());
    	}
    }
}
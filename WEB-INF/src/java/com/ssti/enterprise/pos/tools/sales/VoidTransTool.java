package com.ssti.enterprise.pos.tools.sales;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.VoidDetail;
import com.ssti.enterprise.pos.om.VoidDetailPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for SalesTransaction and SalesTransactionDetail OM
 * @see SalesOrderTool, DeliveryOrderTool, SalesTransaction, SalesTransactionDetail
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: TransactionTool.java,v 1.33 2009/05/04 02:05:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: TransactionTool.java,v $
 * Revision 1.33  2009/05/04 02:05:18  albert
 * *** empty log message ***

 * </pre><br>
 */
public class VoidTransTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( VoidTransTool.class );
	
	private static VoidTransTool instance = null;
	
	public static synchronized VoidTransTool getInstance()
	{
		if (instance == null) instance = new VoidTransTool();
		return instance;
	}
	
	public static List getVoidTrans (String _sTransID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(VoidDetailPeer.SALES_TRANSACTION_ID, _sTransID);
		return VoidDetailPeer.doSelect(oCrit, _oConn);
	}
	
	public static void setVoid(SalesTransaction _oTR, 
							   SalesTransactionDetail _oTD, 
							   List _vVD)
	{
		VoidDetail oVD = new VoidDetail();		
		boolean isPOS = true;
		if (_oTR != null)
		{
			isPOS = _oTR.getIsPosTrans();
			oVD.setVoidBy(_oTR.getCashierName());
			oVD.setVoidRemark(_oTR.getRemark());
		}
		if (isPOS)
		{
			if (_oTD != null)
			{
				oVD.setVoidDate(new Date());
				oVD.setItemId(_oTD.getItemId());
				oVD.setItemCode(_oTD.getItemCode());
				oVD.setItemName(_oTD.getItemName());
				oVD.setDescription(_oTD.getDescription());
				oVD.setQty(_oTD.getQty());
				oVD.setQtyBase(_oTD.getQtyBase());
				oVD.setUnitId(_oTD.getUnitId());
				oVD.setUnitCode(_oTD.getUnitCode());
				oVD.setItemPrice(_oTD.getItemPrice());
			}
			
			if (_vVD == null) _vVD = new ArrayList();
			_vVD.add(oVD);
		}
	}
	
	public static List newTransVoid(SalesTransaction _oTR, List _vVD)
	{
		//only save new trans from unsaved trans
		if (_oTR != null && StringUtil.isEmpty(_oTR.getSalesTransactionId()))
		{
			if (_vVD != null && _vVD.size() > 0)
			{
				saveVoid(_oTR, _vVD);
				_vVD = new ArrayList();
				return _vVD;
			}
		}
		return new ArrayList();
	}
	
	public static void saveVoid(SalesTransaction _oTR, List _vVD) 
	{
		saveVoid(_oTR.getSalesTransactionId(), _oTR.getTransactionNo(), _oTR.getCashierName(), _vVD, null);
	}
	
	public static void saveVoid(String _sTransID, 
								String _sTransNo,
								String _sCreateBy,
								List _vVD, 
								Connection _oConn) 
	{
		if (_vVD != null && _vVD.size() > 0)
		{

			String sTransID = "";
			String sTransNo = "";
			String sCashierName = "";
			String sRemark = "";
			boolean bIsPOS = true;
			
			//empty sales transaction ID means cancelled trans (from NEW TRANS)
			if (StringUtil.isEmpty(_sTransID))
			{
				sRemark = " Void From UNSAVED Trans!" ;
			}			
			//called from saveTrans
			else if(StringUtil.isNotEmpty(_sTransID))
			{
				sTransID = _sTransID;
				sTransNo = _sTransNo;
				sCashierName = _sCreateBy;
				sRemark = " Void From SAVED Trans: " + sTransNo + " by: " + sCashierName ;
			}
			
			try
			{
				for (int i = 0; i < _vVD.size(); i++)
				{
					VoidDetail oVD = (VoidDetail) _vVD.get(i);
					//might not be empty if void trans from previously pending trans
					if (StringUtil.isEmpty(oVD.getVoidDetailId()))
					{
						oVD.setVoidDetailId(IDGenerator.generateSysID());
					}
					oVD.setCashierName(sCashierName);
					oVD.setSalesTransactionId(sTransID);
					oVD.setTransNo(sTransNo);
					oVD.setVoidRemark(sRemark + oVD.getVoidRemark());
					oVD.save(_oConn);
				}				
			}
			catch (Exception _oEx) 
			{
				log.error("ERROR SAVING VOID TRANS: " + _oEx.getMessage(), _oEx);
				_oEx.printStackTrace();
			}
		}
	}
	
	public static List findVoid (Date _dStart, 
								 Date _dEnd, 
								 String _sCreateBy) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (_dStart != null)oCrit.add(VoidDetailPeer.VOID_DATE, _dStart, Criteria.GREATER_EQUAL);
		if (_dEnd != null)oCrit.and(VoidDetailPeer.VOID_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		if (StringUtil.isNotEmpty(_sCreateBy)) oCrit.add(VoidDetailPeer.CASHIER_NAME, _sCreateBy);			
		return VoidDetailPeer.doSelect(oCrit);
	}
}
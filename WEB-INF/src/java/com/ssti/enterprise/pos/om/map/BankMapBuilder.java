package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BankMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BankMapBuilder";

    /**
     * Item
     * @deprecated use BankPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "bank";
    }

  
    /**
     * bank.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use BankPeer.bank.BANK_ID constant
     */
    public static String getBank_BankId()
    {
        return "bank.BANK_ID";
    }
  
    /**
     * bank.BANK_CODE
     * @return the column name for the BANK_CODE field
     * @deprecated use BankPeer.bank.BANK_CODE constant
     */
    public static String getBank_BankCode()
    {
        return "bank.BANK_CODE";
    }
  
    /**
     * bank.ACCOUNT_NO
     * @return the column name for the ACCOUNT_NO field
     * @deprecated use BankPeer.bank.ACCOUNT_NO constant
     */
    public static String getBank_AccountNo()
    {
        return "bank.ACCOUNT_NO";
    }
  
    /**
     * bank.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use BankPeer.bank.CURRENCY_ID constant
     */
    public static String getBank_CurrencyId()
    {
        return "bank.CURRENCY_ID";
    }
  
    /**
     * bank.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use BankPeer.bank.DESCRIPTION constant
     */
    public static String getBank_Description()
    {
        return "bank.DESCRIPTION";
    }
  
    /**
     * bank.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use BankPeer.bank.ACCOUNT_ID constant
     */
    public static String getBank_AccountId()
    {
        return "bank.ACCOUNT_ID";
    }
  
    /**
     * bank.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use BankPeer.bank.OPENING_BALANCE constant
     */
    public static String getBank_OpeningBalance()
    {
        return "bank.OPENING_BALANCE";
    }
  
    /**
     * bank.AS_DATE
     * @return the column name for the AS_DATE field
     * @deprecated use BankPeer.bank.AS_DATE constant
     */
    public static String getBank_AsDate()
    {
        return "bank.AS_DATE";
    }
  
    /**
     * bank.OB_TRANS_ID
     * @return the column name for the OB_TRANS_ID field
     * @deprecated use BankPeer.bank.OB_TRANS_ID constant
     */
    public static String getBank_ObTransId()
    {
        return "bank.OB_TRANS_ID";
    }
  
    /**
     * bank.OB_RATE
     * @return the column name for the OB_RATE field
     * @deprecated use BankPeer.bank.OB_RATE constant
     */
    public static String getBank_ObRate()
    {
        return "bank.OB_RATE";
    }
  
    /**
     * bank.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use BankPeer.bank.LOCATION_ID constant
     */
    public static String getBank_LocationId()
    {
        return "bank.LOCATION_ID";
    }
  
    /**
     * bank.IS_CASH
     * @return the column name for the IS_CASH field
     * @deprecated use BankPeer.bank.IS_CASH constant
     */
    public static String getBank_IsCash()
    {
        return "bank.IS_CASH";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("bank");
        TableMap tMap = dbMap.getTable("bank");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("bank.BANK_ID", "");
                          tMap.addColumn("bank.BANK_CODE", "");
                          tMap.addColumn("bank.ACCOUNT_NO", "");
                          tMap.addColumn("bank.CURRENCY_ID", "");
                          tMap.addColumn("bank.DESCRIPTION", "");
                          tMap.addColumn("bank.ACCOUNT_ID", "");
                            tMap.addColumn("bank.OPENING_BALANCE", bd_ZERO);
                          tMap.addColumn("bank.AS_DATE", new Date());
                          tMap.addColumn("bank.OB_TRANS_ID", "");
                            tMap.addColumn("bank.OB_RATE", bd_ZERO);
                          tMap.addColumn("bank.LOCATION_ID", "");
                          tMap.addColumn("bank.IS_CASH", Boolean.TRUE);
          }
}

package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PriceListDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PriceListDetailMapBuilder";

    /**
     * Item
     * @deprecated use PriceListDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "price_list_detail";
    }

  
    /**
     * price_list_detail.PRICE_LIST_DETAIL_ID
     * @return the column name for the PRICE_LIST_DETAIL_ID field
     * @deprecated use PriceListDetailPeer.price_list_detail.PRICE_LIST_DETAIL_ID constant
     */
    public static String getPriceListDetail_PriceListDetailId()
    {
        return "price_list_detail.PRICE_LIST_DETAIL_ID";
    }
  
    /**
     * price_list_detail.PRICE_LIST_ID
     * @return the column name for the PRICE_LIST_ID field
     * @deprecated use PriceListDetailPeer.price_list_detail.PRICE_LIST_ID constant
     */
    public static String getPriceListDetail_PriceListId()
    {
        return "price_list_detail.PRICE_LIST_ID";
    }
  
    /**
     * price_list_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PriceListDetailPeer.price_list_detail.ITEM_ID constant
     */
    public static String getPriceListDetail_ItemId()
    {
        return "price_list_detail.ITEM_ID";
    }
  
    /**
     * price_list_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PriceListDetailPeer.price_list_detail.ITEM_CODE constant
     */
    public static String getPriceListDetail_ItemCode()
    {
        return "price_list_detail.ITEM_CODE";
    }
  
    /**
     * price_list_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use PriceListDetailPeer.price_list_detail.ITEM_NAME constant
     */
    public static String getPriceListDetail_ItemName()
    {
        return "price_list_detail.ITEM_NAME";
    }
  
    /**
     * price_list_detail.OLD_PRICE
     * @return the column name for the OLD_PRICE field
     * @deprecated use PriceListDetailPeer.price_list_detail.OLD_PRICE constant
     */
    public static String getPriceListDetail_OldPrice()
    {
        return "price_list_detail.OLD_PRICE";
    }
  
    /**
     * price_list_detail.NEW_PRICE
     * @return the column name for the NEW_PRICE field
     * @deprecated use PriceListDetailPeer.price_list_detail.NEW_PRICE constant
     */
    public static String getPriceListDetail_NewPrice()
    {
        return "price_list_detail.NEW_PRICE";
    }
  
    /**
     * price_list_detail.DISCOUNT_AMOUNT
     * @return the column name for the DISCOUNT_AMOUNT field
     * @deprecated use PriceListDetailPeer.price_list_detail.DISCOUNT_AMOUNT constant
     */
    public static String getPriceListDetail_DiscountAmount()
    {
        return "price_list_detail.DISCOUNT_AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("price_list_detail");
        TableMap tMap = dbMap.getTable("price_list_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("price_list_detail.PRICE_LIST_DETAIL_ID", "");
                          tMap.addForeignKey(
                "price_list_detail.PRICE_LIST_ID", "" , "price_list" ,
                "price_list_id");
                          tMap.addColumn("price_list_detail.ITEM_ID", "");
                          tMap.addColumn("price_list_detail.ITEM_CODE", "");
                          tMap.addColumn("price_list_detail.ITEM_NAME", "");
                            tMap.addColumn("price_list_detail.OLD_PRICE", bd_ZERO);
                            tMap.addColumn("price_list_detail.NEW_PRICE", bd_ZERO);
                          tMap.addColumn("price_list_detail.DISCOUNT_AMOUNT", "");
          }
}

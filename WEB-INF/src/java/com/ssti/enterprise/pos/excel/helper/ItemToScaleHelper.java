package com.ssti.enterprise.pos.excel.helper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

public class ItemToScaleHelper
	extends BaseExcelHelper 
	implements ExcelHelper, TransactionAttributes
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] aIsItemList  = (String[]) oParam.get("IsFromItemList"); 
        String[] aLocationID  = (String[]) oParam.get("LocationId");  
        
        int iIsFromItemList = 0; if (aIsItemList != null) iIsFromItemList = Integer.valueOf(aIsItemList[0]).intValue();
        boolean bFromList = false;
        if (iIsFromItemList == 1) bFromList = true;
        
        String sLocationID = ""; if (aLocationID != null) sLocationID = aLocationID[0];         
        Date dAsOf = null;
        String[] aAsOf = (String[]) oParam.get("AsOf");
        if (aAsOf != null && aAsOf.length > 0)
        {
        	dAsOf = DateUtil.getEndOfDayDate(CustomParser.parseDate(aAsOf[0]));
        }

        boolean bNoCost = false;  
        String sNoCost = (String)getFromParam(oParam, "NoCost", String.class);
        if (StringUtil.isNotEmpty(sNoCost)){bNoCost = Boolean.valueOf(sNoCost);}
        
        List vData = (List) _oSession.getAttribute(AppAttributes.s_ITMD);
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Scale Item Data");
                
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        double dQoH = 0;
        double dCost = 0;
        int iScale = PreferenceTool.getInvenCommaScale();

        createHeaderCell(oWB, oRow, (short)iCol, "No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Name"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Price"); iCol++;
		
        if (vData != null)
        {
             for (int i = 0; i < vData.size(); i++)
             {                 
                 ItemList oItem = (ItemList) vData.get(i);
                
                 HSSFRow oRow2 = oSheet.createRow(1 + i);                 
                 Item oItemMs = ItemTool.getItemByID(oItem.getItemId());
                 
                 
                 
                 iCol = 0;
                 createCell(oWB,oRow2,(short)iCol, Integer.toString(i + 1)); iCol++;                  
                 createCell(oWB,oRow2,(short)iCol, oItem.getItemCode   ()); iCol++; 
                 createCell(oWB,oRow2,(short)iCol, oItem.getItemName   ()); iCol++; 
                 createCell(oWB,oRow2,(short)iCol, oItem.getDescription()); iCol++;                                  
                 createCell(oWB,oRow2,(short)iCol, oItem.getUnitCode   ()); iCol++; 
                 createCell(oWB,oRow2,(short)iCol, oItemMs.getPrice(sLocationID, "")); iCol++; 
             }        
        }
        return oWB;
    }
}

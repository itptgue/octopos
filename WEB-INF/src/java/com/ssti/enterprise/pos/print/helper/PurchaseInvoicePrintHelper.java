package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseInvoicePrintHelper.java,v 1.16 2009/05/04 02:03:33 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseInvoicePrintHelper.java,v $
 * Revision 1.16  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.15  2007/12/20 13:48:43  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.13  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PurchaseInvoicePrintHelper 
	extends BasePrintHelper 
	implements PrintHelper
{           
	private PurchaseInvoice oTR = null;
	private List vTD = null;
	private List vEX = null;
	
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
		
		oTR = PurchaseInvoiceTool.getHeaderByID (sID);
		vTD = PurchaseInvoiceTool.getDetailsByID (sID);
		vEX = PurchaseInvoiceTool.getExpensesByID(sID, null);
		
        oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        oCtx.put(AppAttributes.s_PIE, vEX);
        
        prepareContext();
        checkTemplate (oParam);
        
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }        
        return oWriter.toString();
    }
   	
   	/**
	 * get Printed Text (use detail per page)
	 */
	 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);
    	
		if (_oSession != null)
		{
			oTR = (PurchaseInvoice) _oSession.getAttribute(AppAttributes.s_PI);
			vTD = (List) _oSession.getAttribute(AppAttributes.s_PID);
			vEX = (List) _oSession.getAttribute(AppAttributes.s_PIE);
		}
		//get transaction data by id		
		else if (_oSession == null || oTR == null) 
		{
			oTR = PurchaseInvoiceTool.getHeaderByID (sID);
			vTD = PurchaseInvoiceTool.getDetailsByID (sID);
			vEX = PurchaseInvoiceTool.getExpensesByID(sID, null);
		}
		
		oCtx.put(AppAttributes.s_PIE, vEX);
		
        prepareContext();
        checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }
    
    private void prepareContext()
    {
        s_PDF_TPL_NAME = "/print/PurchaseInvoicePDF.vm";
        s_TXT_TPL_NAME = "/print/PurchaseInvoiceTXT.vm";
        s_HTML_TPL_NAME = "/print/PurchaseInvoiceHTML.vm";  
    }
}

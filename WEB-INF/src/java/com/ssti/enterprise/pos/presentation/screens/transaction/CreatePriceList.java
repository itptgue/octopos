package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.framework.presentation.SecureScreen;

public class CreatePriceList extends SecureScreen
{    
	private static final String[] a_PERM = {"Price List Setup", "View Customer Price List"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }	
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	String sMode = data.getParameters().getString("mode", "");
		String sID = "";
		try
    	{
			if (sMode.equals("update") || sMode.equals("delete") || sMode.equals("adjPrice"))
			{
				sID = data.getParameters().getString ("id");
				context.put("PriceList", PriceListTool.getPriceListByID(sID));
    		}
    		
    		context.put("PriceLists", PriceListTool.getAllPriceList());
    	}
    	catch(Exception _oEx)
    	{
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}

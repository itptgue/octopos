package com.ssti.enterprise.pos.tools.report;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.InventoryLocationPeer;
import com.ssti.enterprise.pos.om.InventoryTransactionPeer;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventoryPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryAnalysisTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InventoryReportTool.java,v 1.10 2009/05/04 02:05:09 albert Exp $ <br>
 *
 * <pre>
 * $Log: InventoryReportTool.java,v $
 * 
 * 2018-02-01
 * - override getBalances add stock status to filter qty in get back dated balance
 * 
 * 2016-08-17
 * - add method getBalances, getChanges to get balances of multiple item, a much more efficient method 
 *   than query each item to DB
 * - add method getBalancesByKatID
 * - add method to filter total In, total Out
 * - change transType remove IU, RU / IU Code to IR
 * 
 * </pre><br>
 */
public class InventoryReportTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( InventoryReportTool.class );
	
	static InventoryReportTool instance = null;
	
	public static synchronized InventoryReportTool getInstance() 
	{
		if (instance == null) instance = new InventoryReportTool();
		return instance;
	}	
	
	static List vInvTransType = null;
	static
	{
		vInvTransType = new LinkedList();
        vInvTransType.add(i_INV_TRANS_RECEIPT_UNPLANNED);
		//vInvTransType.add(i_INV_TRANS_ISSUE_UNPLANNED  );
		vInvTransType.add(i_INV_TRANS_PURCHASE_RECEIPT );
		vInvTransType.add(i_INV_TRANS_PURCHASE_INVOICE );
		vInvTransType.add(i_INV_TRANS_PURCHASE_RETURN  );
		vInvTransType.add(i_INV_TRANS_TRANSFER_OUT     );
		vInvTransType.add(i_INV_TRANS_TRANSFER_IN      );		
		vInvTransType.add(i_INV_TRANS_DELIVERY_ORDER   );
		vInvTransType.add(i_INV_TRANS_SALES_INVOICE    );
		vInvTransType.add(i_INV_TRANS_SALES_RETURN 	   );
		vInvTransType.add(i_INV_TRANS_JOB_COSTING      );
		vInvTransType.add(i_INV_TRANS_JC_ROLLOVER      );
	}
	
	static Map mTypeCode = null;
	static
	{
		mTypeCode = new LinkedHashMap(10);
		mTypeCode.put(i_INV_TRANS_RECEIPT_UNPLANNED,"IR");
		//mTypeCode.put(i_INV_TRANS_ISSUE_UNPLANNED  ,"IU");
		mTypeCode.put(i_INV_TRANS_PURCHASE_RECEIPT ,"PR");
		mTypeCode.put(i_INV_TRANS_PURCHASE_INVOICE ,"PI");
		mTypeCode.put(i_INV_TRANS_PURCHASE_RETURN  ,"PT");
		mTypeCode.put(i_INV_TRANS_TRANSFER_OUT     ,"TO");
		mTypeCode.put(i_INV_TRANS_TRANSFER_IN      ,"TI");		
		mTypeCode.put(i_INV_TRANS_DELIVERY_ORDER   ,"DO");
		mTypeCode.put(i_INV_TRANS_SALES_INVOICE    ,"SI");
		mTypeCode.put(i_INV_TRANS_SALES_RETURN 	   ,"SR");
		mTypeCode.put(i_INV_TRANS_JOB_COSTING      ,"JC");
		mTypeCode.put(i_INV_TRANS_JC_ROLLOVER      ,"JR");
	}

	static Map mSimpleCode = null;
	static
	{
		mSimpleCode = new LinkedHashMap(10);
		mSimpleCode.put(i_INV_TRANS_RECEIPT_UNPLANNED,"IR");
		//mSimpleCode.put(i_INV_TRANS_ISSUE_UNPLANNED  ,"IU");
		mSimpleCode.put(i_INV_TRANS_PURCHASE_RECEIPT ,"PR");
		mSimpleCode.put(i_INV_TRANS_PURCHASE_INVOICE ,"PI");	
		mSimpleCode.put(i_INV_TRANS_PURCHASE_RETURN  ,"PT");
		mSimpleCode.put(i_INV_TRANS_TRANSFER_OUT     ,"TO");
		mSimpleCode.put(i_INV_TRANS_TRANSFER_IN      ,"TI");		
		mSimpleCode.put(i_INV_TRANS_DELIVERY_ORDER   ,"DO");
		mSimpleCode.put(i_INV_TRANS_SALES_INVOICE    ,"SI");
		mSimpleCode.put(i_INV_TRANS_SALES_RETURN 	 ,"SR");

		//mSimpleCode.put(i_INV_TRANS_PURCHASE_INVOICE ,"PI");
		//mSimpleCode.put(i_INV_TRANS_JOB_COSTING      ,"JC");
		//mSimpleCode.put(i_INV_TRANS_JC_ROLLOVER      ,"JR");
	}
	
	public static List getInvTransType()
	{
		return vInvTransType;
	}

	public static Map getTypeCodeMap()
	{
		return mTypeCode;
	}

	public static Map getSimpleCodeMap()
	{
		return mSimpleCode;
	}
	
	public static Map getSimpleCodeMapWithJC()
	{
		mSimpleCode.put(i_INV_TRANS_JOB_COSTING,"JC");
		mSimpleCode.put(i_INV_TRANS_JC_ROLLOVER,"JR");
		return mSimpleCode;
	}
    
	public static List getInventoryInLocations (List _vItems, List _vLocationIDs)
		throws Exception
	{
		if (_vItems != null && _vLocationIDs != null)
		{
			if (_vItems.size() > 0 && _vLocationIDs.size() > 0)
			{
				List vItemIDs = new ArrayList (_vItems.size());
				for (int i = 0; i < _vItems.size(); i++)
				{
					Item oItem = (Item)_vItems.get(i);
					vItemIDs.add (oItem.getItemId());
				}
				Criteria oCrit = new Criteria();
				oCrit.addIn (InventoryLocationPeer.ITEM_ID, vItemIDs);
				oCrit.addIn (InventoryLocationPeer.LOCATION_ID, _vLocationIDs);		
				return  InventoryLocationPeer.doSelect(oCrit);
			}
		}	
		return null;
	}

	/**
	 * get total qty changes for an Item SKU in a location or 
	 * all location since certain date
	 * 
	 * @param _sItemID
	 * @param _sSKU
	 * @param _sKategoriID
	 * @param _sLocationID
	 * @param _dFrom
	 * @param _dTo
	 * @param _iMode
	 * @param _bValue
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getChangesPerTrans (String _sItemID, 
										   String _sKategoriID,
										   String _sLocationID, 
										   Date _dFrom, 
										   Date _dTo) 
		throws Exception
	{ 
		StringBuilder oSB = new StringBuilder();
		oSB.append(" SELECT itrans.item_id, itrans.transaction_type,");
		oSB.append(" SUM(itrans.qty_changes), SUM(itrans.qty_changes * itrans.cost) ");		
		oSB.append(" FROM item itm, inventory_transaction itrans ");
		oSB.append(" WHERE itm.item_id = itrans.item_id ");		
		if (StringUtil.isNotEmpty(_sItemID))
		{
			oSB.append(" AND itrans.item_id = '").append(_sItemID).append("' ");
		}
		else if (StringUtil.isNotEmpty(_sKategoriID))		
		{		
			List vChild = KategoriTool.getChildIDList(_sKategoriID);
			vChild.add(_sKategoriID);
			String sKat = SqlUtil.convertToINMode(vChild);
			if (StringUtil.isNotEmpty(sKat) && !sKat.equals("()"))
			{
				oSB.append(" AND itm.kategori_id IN ").append(sKat).append(" ");
			}
		}		
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oSB.append(" AND itrans.location_id = '").append(_sLocationID).append("' ");
		}		
		if (_dFrom != null)
		{
			oSB.append(" AND itrans.transaction_date >= ");
			oSB.append(oDB.getDateString(_dFrom));
		}
		if (_dTo != null)
		{
			oSB.append(" AND itrans.transaction_date <= ");
			oSB.append(oDB.getDateString(_dTo));
		}
		oSB.append(" GROUP BY itrans.item_id, itrans.transaction_type");		
		
		if (log.isDebugEnabled())
		{
			log.debug (oSB.toString());
		}
		return InventoryTransactionPeer.executeQuery(oSB.toString());
	}	

	public static Record filterByType(List _vRecord, String _sItemID, int _iType)
	{
		try 
		{
			for (int i = 0; i < _vRecord.size(); i++)
			{
				Record oRec = (Record) _vRecord.get(i);
				if (oRec.getValue("transaction_type").asInt() == _iType && 
					StringUtil.equals(oRec.getValue("item_id").asString(), _sItemID))
				{
					return oRec;
				}
			}	
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
		}
		return null;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// Inventory Location Filters
	//////////////////////////////////////////////////////////////////////////////////	

    //group by SKU
	public static BigDecimal getCurrentQty (String _sItemID, String _sLocationID, List _vInvLocs, String _sSKU)
		throws Exception
	{
		if (_vInvLocs != null)
		{
			for (int i = 0; i < _vInvLocs.size(); i++)
			{
				InventoryLocation oInv = (InventoryLocation)_vInvLocs.get(i);
				if (oInv != null)
				{
					if (oInv.getItemId().equals(_sItemID) && oInv.getLocationId().equals(_sLocationID))
					{
						Record oRec = InventoryLocationTool.getDataBySKUAndLocationID (_sSKU, _sLocationID);
						if (oRec != null)
						{
						    return oRec.getValue(2).asBigDecimal();
						}
					}
				}
			}
		}
		return bd_ZERO;
	}

    //item code
	public static InventoryLocation filterInvLoc (String _sItemID, String _sLocationID, List _vInvLocs)
		throws Exception
	{
		if (_vInvLocs != null)
		{
			for (int i = 0; i < _vInvLocs.size(); i++)
			{
				InventoryLocation oInv = (InventoryLocation)_vInvLocs.get(i);
				if (oInv != null)
				{
					if (oInv.getItemId().equals(_sItemID) && oInv.getLocationId().equals(_sLocationID))
					{
						return oInv;
					}
				}
			}
		}
		return null;
	}
	
    //item code
	public static BigDecimal getCurrentQty (String _sItemID, String _sLocationID, List _vInvLocs)
		throws Exception
	{
		if (_vInvLocs != null)
		{
			for (int i = 0; i < _vInvLocs.size(); i++)
			{
				InventoryLocation oInv = (InventoryLocation)_vInvLocs.get(i);
				if (oInv != null)
				{
					if (oInv.getItemId().equals(_sItemID) && oInv.getLocationId().equals(_sLocationID))
					{
						return oInv.getCurrentQty();
					}
				}
			}
		}
		return bd_ZERO;
	}
	
    //group by SKU 
	public static BigDecimal getItemCost (String _sItemID, String _sLocationID, List _vInvLocs, String _sSKU)
		throws Exception
	{
		if (_vInvLocs != null)
		{
			for (int i = 0; i < _vInvLocs.size(); i++)
			{
				InventoryLocation oInv = (InventoryLocation)_vInvLocs.get(i);
				if (oInv != null)
				{
					if (oInv.getItemId().equals(_sItemID) && oInv.getLocationId().equals(_sLocationID))
					{
						Record oRec = InventoryLocationTool.getDataBySKUAndLocationID (_sSKU, _sLocationID);
						if (oRec != null)
						{
						    return oRec.getValue(3).asBigDecimal();
						}
					}
				}
			}
		}
		return bd_ZERO;
	}

    //item code
	public static BigDecimal getItemCost (String _sItemID, String _sLocationID, List _vInvLocs)
		throws Exception
	{
		if (_vInvLocs != null)
		{
			for (int i = 0; i < _vInvLocs.size(); i++)
			{
				InventoryLocation oInv = (InventoryLocation)_vInvLocs.get(i);
				if (oInv != null)
				{
					if (oInv.getItemId().equals(_sItemID) && oInv.getLocationId().equals(_sLocationID))
					{
						return oInv.getItemCost();
					}
				}
			}
		}
		return bd_ZERO;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// Analysis Report part
	//////////////////////////////////////////////////////////////////////////////////
	
	public static List getTopMovementRanking (Date _dStart, Date _dEnd, int _iLimit, int _iType, int _iOrderBy)
		throws Exception
	{
	    return getTopMovementRanking (_dStart, _dEnd, _iLimit, _iType, _iOrderBy, "");
    }
	public static List getTopMovementRanking (Date _dStart, Date _dEnd, int _iLimit, int _iType, int _iOrderBy ,String _sLocationID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.item_id, tr.item_code, it.item_name, SUM(tr.qty_changes) AS move_total, ");
		oSQL.append (" COUNT(tr.inventory_transaction_id) AS move_num ");
		oSQL.append ("FROM inventory_transaction tr, item it WHERE ");

		InventoryAnalysisTool.appendDate(oSQL, _dStart, _dEnd);
		
		oSQL.append (" AND tr.item_id = it.item_id ");
		if (_iType == 1) oSQL.append (" AND tr.qty_changes > 0 "); //Amount 
		if (_iType == 2) oSQL.append (" AND tr.qty_changes < 0 "); 
		
		if(_sLocationID != null && !_sLocationID.equals(""))
		{
		    oSQL.append (" AND tr.location_id = '");
		    oSQL.append (_sLocationID);
		    oSQL.append ("'");
		}
		oSQL.append ("  GROUP BY tr.item_id, tr.item_code, it.item_name ORDER BY ");

		if (_iType == 3) oSQL.append (" move_num DESC "); 
		else if (_iType == 1 && _iOrderBy == 2) oSQL.append (" move_total DESC "); 
		else if (_iType == 2 && _iOrderBy == 2) oSQL.append (" move_total ASC "); 
		else if (_iType == 1 && _iOrderBy == 1) oSQL.append (" move_num DESC "); 
		else if (_iType == 2 && _iOrderBy == 1) oSQL.append (" move_num DESC "); 
		else oSQL.append ("move_total DESC "); 

		oSQL.append (" LIMIT ");
		oSQL.append (_iLimit);
				
		log.debug (oSQL.toString());

		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (5);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(oData.getValue(2).asString());
			vRecordData.add(oData.getValue(3).asString());
			vRecordData.add(oData.getValue(4).asBigDecimal());			
			vRecordData.add(oData.getValue(5).asBigDecimal());
			vResult.add (vRecordData); 
		}
		return vResult;	
	}	


	public static List getTopTurnoverRanking (Date _dStart, Date _dEnd, int _iLimit, int _iType, int _iOrderBy)
		throws Exception
	{		
	    return getTopTurnoverRanking (_dStart, _dEnd, _iLimit, _iType, _iOrderBy ,"");
    }

	public static List getTopTurnoverRanking (Date _dStart, Date _dEnd, int _iLimit, int _iType, int _iOrderBy ,String _sLocationID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT tr.item_id, tr.item_code, it.item_name, SUM(tr.qty_changes) AS move_total, COUNT(tr.inventory_transaction_id) AS move_num ");
		oSQL.append ("FROM inventory_transaction AS tr, item AS it WHERE ");
		
		InventoryAnalysisTool.appendDate(oSQL, _dStart, _dEnd);
		
		oSQL.append (" AND tr.item_id = it.item_id ");
		if (_iType == 1) oSQL.append (" AND tr.qty_changes > 0 "); //Amount 
		if (_iType == 2) oSQL.append (" AND tr.qty_changes < 0 "); 
		
		if(_sLocationID != null && !_sLocationID.equals(""))
		{
		    oSQL.append (" AND tr.location_id = '");
		    oSQL.append (_sLocationID);
		    oSQL.append ("'");
		}
		oSQL.append ("  GROUP BY tr.item_id, tr.item_code, it.item_name ORDER BY ");

		if (_iType == 3) oSQL.append (" move_num DESC "); 
		else if (_iType == 1 && _iOrderBy == 2) oSQL.append (" move_total DESC "); 
		else if (_iType == 2 && _iOrderBy == 2) oSQL.append (" move_total ASC "); 
		else if (_iType == 1 && _iOrderBy == 1) oSQL.append (" move_num DESC "); 
		else if (_iType == 2 && _iOrderBy == 1) oSQL.append (" move_num DESC "); 
		else oSQL.append ("move_total DESC "); 

		oSQL.append (" LIMIT ");
		oSQL.append (_iLimit);
				
		log.debug (oSQL.toString());

		List vData = InventoryTransactionPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (5);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(oData.getValue(2).asString());
			vRecordData.add(oData.getValue(3).asString());
			vRecordData.add(oData.getValue(4).asBigDecimal());			
			vRecordData.add(oData.getValue(5).asBigDecimal());
			vResult.add (vRecordData); 
		}
		return vResult;	
	}	
	
	public static Record getQtyBySite (String _sItemID, String _sSiteID)
		throws Exception
	{
		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT il.item_id, SUM(il.current_qty) ");
		oSQL.append (" FROM inventory_location il, location lc, site st WHERE ");
		oSQL.append (" il.item_id = '").append(_sItemID).append("' ");
		oSQL.append (" AND il.location_id = lc.location_id ");
		oSQL.append (" AND lc.site_id = st.site_id ");
		oSQL.append (" AND st.site_id = '").append(_sSiteID).append("' ");
		oSQL.append (" GROUP BY il.item_id");
		
		log.debug(oSQL.toString());
		
		List vData = InventoryLocationPeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			return (Record) vData.get(0);
		}
		return null;
	}
	
	public static List findIRItem (Date _dStart, 
								   Date _dEnd, 
								   int _iSearchBy,
								   int _iSortBy,
								   int _iType,
								   String _sKeywords,
								   String _sColor,
								   String _sKategoriID,
								   String _sLocationID, 
								   String _sAdjTypeID)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT ir.issue_receipt_id, ir.transaction_no, ir.transaction_date, ir.transaction_type, ")
			.append (" ir.adjustment_type_id, adj.description, ir.user_name, ir.confirm_by, ")		
			.append (" ird.item_id, ird.item_code, i.item_name, ird.unit_id, ird.unit_code, ")
			.append (" ird.qty_changes, ird.qty_base, ird.cost ")
			.append (" FROM issue_receipt ir, issue_receipt_detail ird, kategori k, item i, adjustment_type adj WHERE ");
		
		buildIRStdQuery (oSQL, _dStart, _dEnd, i_PROCESSED, _sLocationID, "", "");
		
		oSQL.append (" AND ir.issue_receipt_id = ird.issue_receipt_id ");
		oSQL.append (" AND ird.item_id = i.item_id ");
		oSQL.append (" AND i.kategori_id = k.kategori_id ");
		oSQL.append (" AND ir.adjustment_type_id = adj.adjustment_type_id ");
		
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			vKategori.add(_sKategoriID);
			oSQL.append (" AND i.kategori_id IN ")
			.append (SqlUtil.convertToINMode(vKategori));		
		}
		if (StringUtil.isNotEmpty(_sAdjTypeID))
		{
			oSQL.append(" AND ir.adjustment_type_id = '").append(_sAdjTypeID).append("' ");
		}
		if (StringUtil.isNotEmpty(_sColor))
		{
			oSQL.append(" AND i.color LIKE '").append(_sColor).append("' ");
		}
		if (StringUtil.isNotEmpty(_sKeywords))
		{
			if (_iSearchBy == 1) //item code
			{
				oSQL.append(" AND i.item_code ilike '").append(_sKeywords).append("' ");
			}
			else if (_iSearchBy == 2) //item name
			{
				oSQL.append(" AND i.item_name ilike '").append(_sKeywords).append("' ");
			}
			else if (_iSearchBy == 3) //trans no
			{
				oSQL.append(" AND ir.transaction_no ilike '").append(_sKeywords).append("' ");
			}
			else if (_iSearchBy == 4) //user name
			{
				oSQL.append(" AND ir.user_name like '").append(_sKeywords).append("' ");
			}
			else if (_iSearchBy == 5) //confirm by
			{
				oSQL.append(" AND ir.confirm_by like '").append(_sKeywords).append("' ");
			}
			else if (_iSearchBy == 6) //description
			{
				oSQL.append(" AND ir.description like '").append(_sKeywords).append("' ");
			}
		}


		if (_iSortBy == 1) //item code
		{
			oSQL.append(" ORDER BY i.item_code ");
		}
		else if (_iSortBy == 2) //item name
		{
			oSQL.append(" ORDER BY i.item_name ");
		}
		else if (_iSortBy == 3) //trans no
		{
			oSQL.append(" ORDER BY ir.transaction_no, i.item_name ");
		}
		
		log.debug (oSQL.toString());
		List vData = IssueReceiptPeer.executeQuery(oSQL.toString());	
		return vData;
	}
	
	
	public static StringBuilder buildIRStdQuery (StringBuilder oSQL, 
												 Date _dStart, 
												 Date _dEnd, 
												 int _iStatus,
												 String _sLocationID,
												 String _sDepartmentID,
												 String _sProjectID)
	{
		BaseTool.buildDateQuery(oSQL, "ir.transaction_date", _dStart, _dEnd);
		
		if(_iStatus <= 0) _iStatus = i_TRANS_PROCESSED;
		oSQL.append (" AND ir.status = ").append (_iStatus).append(" ");		
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oSQL.append (" AND ir.location_id = '").append (_sLocationID).append ("'");
		}
		if (StringUtil.isNotEmpty(_sDepartmentID))
		{
			oSQL.append (" AND ird.department_id = '").append (_sDepartmentID).append ("'");
		}
		if (StringUtil.isNotEmpty(_sProjectID))
		{
			oSQL.append (" AND ird.project_id = '").append (_sProjectID).append ("'");
		}
		return oSQL;
	}
	
	public static List findItemNotInIR (Date _dStart, 
								        Date _dEnd, 
								        int _iSearchBy,
								        int _iSortBy,
								        int _iType,
								        String _sKeywords,
								        String _sColor,
								        String _sKategoriID)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append("SELECT i.item_id, i.item_code, i.item_name, i.last_adjustment FROM item i, kategori k WHERE i.item_id NOT IN ");
		buildIRSubSelect(oSQL, _dStart, _dEnd);
		
		oSQL.append (" AND i.item_type = ").append(i_INVENTORY_PART);		
		oSQL.append (" AND i.kategori_id = k.kategori_id ");
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			vKategori.add(_sKategoriID);
			oSQL.append (" AND i.kategori_id IN ")
			.append (SqlUtil.convertToINMode(vKategori));		
		}
		if (StringUtil.isNotEmpty(_sColor))
		{
			oSQL.append(" AND i.color LIKE '").append(_sColor).append("' ");
		}
		if (StringUtil.isNotEmpty(_sKeywords))
		{
			if (_iSearchBy == 1) //item code
			{
				oSQL.append(" AND i.item_code ilike '").append(_sKeywords).append("' ");
			}
			else if (_iSearchBy == 2) //item name
			{
				oSQL.append(" AND i.item_name ilike '").append(_sKeywords).append("' ");
			}
		}
		
		if (_iSortBy == 1) //item code
		{
			oSQL.append(" ORDER BY i.item_code ");
		}
		else
		{
			oSQL.append(" ORDER BY i.item_name ");
		}
		
		log.debug (oSQL.toString());
		List vData = IssueReceiptPeer.executeQuery(oSQL.toString());	
		return vData;
	}
	
	public static BigDecimal countItem(int _iType)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append("SELECT count(item_id) FROM item WHERE item_type = ").append(i_INVENTORY_PART);
		List vData = BasePeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			Record oData =(Record)vData.get(0);
			return oData.getValue(1).asBigDecimal();
		}
		return bd_ZERO;
	}
	
	public static BigDecimal countItemNotInIR (Date _dStart, Date _dEnd)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append("SELECT count(i.item_id) FROM item i WHERE i.item_id NOT IN ");
		buildIRSubSelect(oSQL, _dStart, _dEnd);
		oSQL.append(" AND i.item_type = ").append(i_INVENTORY_PART);		
		log.debug (oSQL.toString());
		List vData = BasePeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			Record oData =(Record)vData.get(0);
			return oData.getValue(1).asBigDecimal();
		}
		return bd_ZERO;
	}
	
	private static void buildIRSubSelect (StringBuilder oSQL, Date _dStart, Date _dEnd)
	{
		oSQL.append(" (SELECT ird.item_id from issue_receipt_detail ird, issue_receipt ir WHERE ");
		oSQL.append(" ir.issue_receipt_id = ird.issue_receipt_id AND ");
		BaseTool.buildDateQuery(oSQL, "ir.transaction_date", _dStart, _dEnd);
		oSQL.append(") ");
	}
	
	public static List getInactiveItem (Date _dDate, String _sKategoriID, int _iSortBy) 
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append("SELECT i.item_id, i.item_code, i.item_name, i.last_adjustment FROM item i ");
		buildInactiveItemQuery(_dDate, _sKategoriID, oSQL, _iSortBy);
		
		log.debug (oSQL.toString());
		List vData = IssueReceiptPeer.executeQuery(oSQL.toString());	
		return vData;
	}
	
	private static void buildInactiveItemQuery(Date _dDate, String _sKategoriID, StringBuilder oSQL, int _iSortBy) 
		throws Exception
	{
		if (_dDate == null)
		{
			_dDate = new Date();
		}
		Date dDate = DateUtil.addMonths(_dDate, -3);
		String sDate = CustomFormatter.formatSqlDate(dDate);		
	
		oSQL.append(" WHERE i.item_type = ").append(i_INVENTORY_PART);
		
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			vKategori.add(_sKategoriID);
			oSQL.append (" AND i.kategori_id IN ").append (SqlUtil.convertToINMode(vKategori));		
		}
		
		oSQL.append(" AND i.item_id NOT IN (SELECT il.item_id FROM inventory_location il WHERE il.item_id = i.item_id) ");
		oSQL.append(" OR ((SELECT SUM(il.current_qty) FROM inventory_location il WHERE il.item_id = i.item_id) = 0 ");
		oSQL.append("      AND i.item_id NOT IN (SELECT it.item_id FROM inventory_transaction it WHERE it.item_id = i.item_id ");
		oSQL.append("      AND it.transaction_date >= '").append(sDate).append("')) ");		
		
		if (_iSortBy == 1) oSQL.append(" ORDER BY i.item_code ");
		if (_iSortBy == 2) oSQL.append(" ORDER BY i.item_name ");
	}
	
	public static BigDecimal countInactiveItem (Date _dDate)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append("SELECT count(i.item_id) FROM item i ");
		buildInactiveItemQuery(_dDate, "", oSQL, -1);

		log.debug (oSQL.toString());
		List vData = BasePeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			Record oData =(Record)vData.get(0);
			return oData.getValue(1).asBigDecimal();
		}
		return bd_ZERO;
	}
	
	public static List filterItemNotInIRExclInactive(List _vNIR, List _vInactive) 
		throws Exception
	{
		List vResult = new ArrayList(_vNIR.size() - _vInactive.size());
		for (int i = 0; i < _vNIR.size(); i++)
		{
			Record oRec = (Record) _vNIR.get(i);
			String sItemID = oRec.getValue("item_id").asString();
			if (!isInactive(sItemID, _vInactive))
			{
				vResult.add(oRec);
			}
		}
		return vResult;
	}
	
	private static boolean isInactive(String _sItemID, List _vInactive) 
		throws Exception
	{
		List vResult = new ArrayList(_vInactive.size() - _vInactive.size());
		for (int i = 0; i < _vInactive.size(); i++)
		{
			Record oRec = (Record) _vInactive.get(i);
			String sItemID = oRec.getValue("item_id").asString();
			if (StringUtil.isEqual(sItemID, _sItemID))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param _sItemID
	 * @return
	 * @throws Exception
	 */
	public static Record getTotalQtyCost (String _sItemID) 
		throws Exception
	{		
	    StringBuilder oSQL = new StringBuilder();
	    oSQL.append (" SELECT SUM(current_qty), SUM(current_qty * item_cost)")
	    	.append (" FROM inventory_location ")
	        .append (" WHERE item_id = '").append(_sItemID).append("' ");
	    
	    //log.debug(oSQL);
		List vData = InventoryLocationPeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
	        return (Record) vData.get(0);
		}
		return null;      
	}
	
	public static List getNoTransItem(int _iCondition, 
									  String _sKeywords, 
									  String _sKatID, 
									  int _iSortBy,
									  int _iViewLimit,
									  String _sLocationID,
									  String _sVendorID,
									  Date _dStart,
									  Date _dEnd,
									  boolean _bExclAdj,
									  boolean _bExclTrf,
									  boolean _bExclPur)
		throws Exception
	{
	
		Criteria oCrit = ItemTool.createFindCriteria(_iCondition,_sKeywords,_iSortBy,_sKatID,_sVendorID,_sLocationID);		
		if (_dStart != null && _dEnd != null)
		{
			StringBuilder oSQL = new StringBuilder();
			_dEnd = DateUtil.getEndOfDayDate(_dEnd);
			oSQL.append("SELECT i.item_id FROM item i ")
				.append(" WHERE i.item_id NOT IN (SELECT it.item_id FROM inventory_transaction it WHERE ")
				.append(" it.transaction_date >= '").append(CustomFormatter.formatSqlDate(_dStart)).append("'")
				.append(" AND it.transaction_date <= '").append(CustomFormatter.formatSqlDate(_dEnd)).append("'");			
			if (StringUtil.isNotEmpty(_sLocationID))
			{
				oSQL.append(" AND it.location_id = '").append(_sLocationID).append("' ");			
			}
			if (_bExclAdj)
			{
				oSQL.append(" AND it.transaction_type NOT IN (1, 2, 12, 13) ");						
			}
			if (_bExclTrf)
			{
				oSQL.append(" AND it.transaction_type NOT IN (14, 15) ");						
			}
			if (_bExclPur)
			{
				oSQL.append(" AND it.transaction_type NOT IN (3, 4, 10) ");						
			}			
			oSQL.append(")");
			log.debug(oSQL.toString());
			List vItemID = BasePeer.executeQuery(oSQL.toString());
			List vIn = new ArrayList(vItemID.size());
			for (int i = 0; i < vItemID.size(); i++)
			{
				Record oRec = (Record) vItemID.get(i);
				vIn.add(oRec.getValue("item_id").asString());
			}
			if (vIn.size() > 0)
			{
				oCrit.addIn(ItemPeer.ITEM_ID, vIn);
			}
		}
		return ItemPeer.doSelect(oCrit);	
	}
	
	public static LargeSelect getActiveItem(int _iCondition, 
											String _sKeywords, 
											String _sKatID, 
											int _iSortBy,
											int _iViewLimit,
											String _sLocationID,
											String _sVendorID,
											Date _dStart,
											Date _dEnd)
		throws Exception
	{

		Criteria oCrit = ItemTool.createFindCriteria(_iCondition,_sKeywords,_iSortBy,_sKatID,_sVendorID,_sLocationID);		
		if (_dStart != null && _dEnd != null)
		{
			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT DISTINCT item_id FROM inventory_transaction ")
				.append(" WHERE transaction_date >= '").append(CustomFormatter.formatSqlDate(_dStart)).append("'")
				.append(" AND transaction_date <= '").append(CustomFormatter.formatSqlDate(_dEnd)).append("'");
			
			if (StringUtil.isNotEmpty(_sLocationID))
			{
				oSQL.append(" AND location_id = '").append(_sLocationID).append("'");			
			}
			
			List vItemID = BasePeer.executeQuery(oSQL.toString());
			List vIn = new ArrayList(vItemID.size());
			for (int i = 0; i < vItemID.size(); i++)
			{
				Record oRec = (Record) vItemID.get(i);
				vIn.add(oRec.getValue("item_id").asString());
			}
			if (vIn.size() > 0)
			{
				oCrit.addIn(ItemPeer.ITEM_ID, vIn);
			}
		}
		log.debug (oCrit);
		return new LargeSelect(oCrit, _iViewLimit, "com.ssti.enterprise.pos.om.ItemPeer");
	}
	
	//-------------------------------------------------------------------------
	// ITEM LIST
	//-------------------------------------------------------------------------
	
	public static List findToItemList(int _iCondition, 
									  int _iSortBy, 
									  int _iStockStatus,
									  boolean _bAll,
									  String _sKeywords, 
									  String _sKatID, 
									  String _sLocationID,
									  String _sVendorID,
									  Date _dAsOf)
		throws Exception
	{		
		Criteria oCrit = ItemTool.createFindCriteria(_iCondition,_sKeywords,_iSortBy,_sKatID,_sVendorID, _sLocationID);
		oCrit.add(ItemPeer.ITEM_TYPE,i_INVENTORY_PART);
		oCrit.add(ItemPeer.ITEM_STATUS, b_ACTIVE_ITEM); //find Active Item ONLY
		System.out.println(oCrit);
		if (!_bAll) //exists in inventory location only
		{
			oCrit.addJoin(ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);		
			oCrit.add(InventoryLocationPeer.LOCATION_ID, _sLocationID);					
		}		
	    //stock status
		if (_iStockStatus > 0 && (DateUtil.isToday(_dAsOf) || _iStockStatus == 11))
		{
			oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
			oCrit.add(InventoryLocationPeer.LOCATION_ID, _sLocationID);
		    if (_iStockStatus == 1) {
		        oCrit.add (ItemInventoryPeer.LOCATION_ID, _sLocationID);
		        oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
				oCrit.addJoin (InventoryLocationPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.addJoin (ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery (InventoryLocationPeer.CURRENT_QTY, 
				ItemInventoryPeer.MINIMUM_QTY, "<=") , Criteria.CUSTOM);
		    }
		    if (_iStockStatus == 2) {
	   			oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
				oCrit.add (ItemInventoryPeer.LOCATION_ID, _sLocationID);
				oCrit.addJoin (InventoryLocationPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.addJoin (ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery (InventoryLocationPeer.CURRENT_QTY, 
				ItemInventoryPeer.REORDER_POINT, "<=") , Criteria.CUSTOM);
		    }
		    if (_iStockStatus == 3) {
				oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
				oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);			
				oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0), Criteria.LESS_THAN);  			
		    }
		    if (_iStockStatus == 4) {
				oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
				oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
				oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0), Criteria.GREATER_THAN);  			
		    }
	   	    if (_iStockStatus == 5) {
	   	    	oCrit.add (ItemInventoryPeer.LOCATION_ID, _sLocationID);
		        oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
				oCrit.addJoin (InventoryLocationPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.addJoin (ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
				oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery (InventoryLocationPeer.CURRENT_QTY, 
				ItemInventoryPeer.MAXIMUM_QTY, ">=") , Criteria.CUSTOM);
		    }
	   	    
	  	    if (_iStockStatus == 11) { //item exist in inventory location
				oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
				oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
		    }
	        if (_iStockStatus == 12) {
	            oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
	            oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
	            oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0));           
	        }
	        if (_iStockStatus == 13) {
	            oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
	            oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
	            oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0), Criteria.GREATER_EQUAL);           
	        }	
	        
//			if (_iStockStatus == 1) {
//				oCrit.add (InventoryLocationPeer.CURRENT_QTY, (Object)ItemPeer.MINIMUM_STOCK, Criteria.LESS_EQUAL);  			
//		    }
//		    if (_iStockStatus == 2) {				
//				oCrit.add (InventoryLocationPeer.CURRENT_QTY, (Object)ItemPeer.REORDER_POINT, Criteria.LESS_EQUAL);  			
//		    }
//		    if (_iStockStatus == 3) {
//				oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0), Criteria.LESS_THAN);  			
//		    }
//		    if (_iStockStatus == 4) {
//				oCrit.add (InventoryLocationPeer.CURRENT_QTY, Integer.valueOf(0), Criteria.GREATER_THAN);  			
//		    }
//		    if (_iStockStatus == 5) {
//				oCrit.add (InventoryLocationPeer.CURRENT_QTY, (Object)ItemPeer.MAXIMUM_STOCK, Criteria.GREATER_EQUAL);  			
//		    }
		}
		log.debug(oCrit);
		StopWatch sw = new StopWatch();
		List vResult = new ArrayList();
		List vItem = ItemPeer.doSelect(oCrit);
		List vItemID = SqlUtil.toListOfID(vItem);
		List vBalances = getBalances(vItemID, _sLocationID, _dAsOf, _iStockStatus);				
		
		for (int i = 0; i < vItem.size(); i++)
		{
			boolean bInclude = true;
			Item oItem = (Item) vItem.get(i);			
			ItemList oData = new ItemList(oItem);			
			BigDecimal bdQty = bd_ZERO;
			BigDecimal bdCost = bd_ZERO;			

			List vFBal = SqlUtil.filterRecord(vBalances, "item_id", oItem.getItemId());
			if(vFBal.size() > 0)
			{
				Record oRecord = (Record) vFBal.get(0);
				double dQty = oRecord.getValue("qty").asDouble();
				double dVal = oRecord.getValue("cost").asDouble();
				bdQty = new BigDecimal(dQty);
				if(dQty != 0)
				{
					double dCost = Calculator.div(dVal, dQty);				
					bdCost = new BigDecimal(dCost);
				}																
			}			
			oData.setQty(bdQty);
			oData.setCost(bdCost);

			if (_iStockStatus > 0 && !DateUtil.isToday(_dAsOf))
			{
				if(_iStockStatus == 3  && bdQty.doubleValue() >= 0) bInclude = false; //< 0
				if(_iStockStatus == 4  && bdQty.doubleValue() <= 0) bInclude = false; //> 0
				if(_iStockStatus == 12 && bdQty.doubleValue() != 0) bInclude = false; //= 0			
				if(_iStockStatus == 13 && bdQty.doubleValue() < 0)  bInclude = false; //>= 0							
			}
			if(bInclude)
			{
				vResult.add(oData);
			}			
		}
		return vResult;
	}
	
	//-------------------------------------------------------------------------
	// PURCHASE REQUEST ORDER DETAIL 
	//-------------------------------------------------------------------------	
		
		
	//-------------------------------------------------------------------------
	// Inventory Cost / Item / Kategori / Trans Type
	//-------------------------------------------------------------------------	

	public static List sumByType(String _sItemID, 
								 String _sKatID,
								 String _sLocationID,
								 String _sVendorID,
								 Date _dStart,
								 Date _dEnd,
								 int _iGroup,
								 int _iType)
		throws Exception
	{
		if (_dStart != null && _dEnd != null)
		{
			String sStart = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStart));
			String sEnd = CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd));
			
			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT i.item_id, i.item_code, i.item_name, i.description, i.last_purchase_price, i.item_price, ")
			    .append(" SUM(it.qty_changes) AS qty, SUM(it.qty_changes * it.cost) AS cost, i.kategori_id, ")
			    .append(" SUM(it.qty_changes * it.item_price) AS price ")
			    .append(" FROM item i, inventory_transaction it  WHERE i.item_id = it.item_id ")			
				.append(" AND it.transaction_date >= '").append(sStart).append("'")
				.append(" AND it.transaction_date <= '").append(sEnd).append("'");			
			if (StringUtil.isNotEmpty(_sLocationID))
			{
				oSQL.append(" AND it.location_id = '").append(_sLocationID).append("' ");			
			}
			if (StringUtil.isNotEmpty(_sItemID))
			{
				oSQL.append(" AND i.item_id = '").append(_sItemID).append("'");
			}
			else 
			{
				if (StringUtil.isNotEmpty(_sKatID))						
				{		
					List vChild = KategoriTool.getChildIDList(_sKatID);
					vChild.add(_sKatID);
					String sKat = SqlUtil.convertToINMode(vChild);
					if (StringUtil.isNotEmpty(sKat) && !sKat.equals("()"))
					{
						oSQL.append(" AND i.kategori_id IN ").append(sKat).append(" ");
					}	
				}
			}
			if (_iType > 0)
			{
				oSQL.append(" AND it.transaction_type =  ").append(_iType);											
			}
			else
			{
				if (_iGroup == 1) oSQL.append(" AND it.transaction_type IN (1, 2, 12, 13) ");	//adjustment								
				if (_iGroup == 2) oSQL.append(" AND it.transaction_type IN (3, 4, 10) ");		//PR,PI & PT												
				if (_iGroup == 3) oSQL.append(" AND it.transaction_type IN (11, 6, 7) ");		//DO,SI & SR				
				if (_iGroup == 4) oSQL.append(" AND it.transaction_type = 14 ");				//TO		
				if (_iGroup == 5) oSQL.append(" AND it.transaction_type = 15 ");				//TI			
				if (_iGroup == 6) oSQL.append(" AND it.transaction_type IN (14,15) ");			//IT	
			}			
			oSQL.append(" GROUP BY i.item_id ");
			log.debug("sumByType: " + oSQL.toString());
			return BasePeer.executeQuery(oSQL.toString());			
		}
		return null;
	}

	public static double sumCostPerGroupByKat(List _vGroup, String _sKatID, boolean _bCost) throws Exception
	{
		double dTotal = 0;
		if (_vGroup != null && StringUtil.isNotEmpty(_sKatID))
		{
			List vChild = KategoriTool.getChildIDList(_sKatID);
			vChild.add(_sKatID);
			String sKat = SqlUtil.convertToINMode(vChild);
			
			for (int i = 0; i < _vGroup.size(); i++)
			{
				Record oRecord = (Record)_vGroup.get(i);
				String sKatID = oRecord.getValue("kategori_id").asString();
				if(StringUtil.contains(sKat, sKatID) || StringUtil.isEqual(sKatID, _sKatID))
				{
					if (_bCost)
					{
						dTotal += oRecord.getValue("cost").asDouble();
					}
					else
					{
						dTotal += oRecord.getValue("qty").asDouble(); 
					}				
				}
			}
		}
		return dTotal;
	}

	public static double sumCostPerGroupByItem(List _vGroup, String _sItemID, boolean _bCost) throws Exception
	{
		double dTotal = 0;
		if (_vGroup != null && StringUtil.isNotEmpty(_sItemID))
		{			
			for (int i = 0; i < _vGroup.size(); i++)
			{
				Record oRecord = (Record)_vGroup.get(i);
				String sItemID = oRecord.getValue("item_id").asString();
				if(StringUtil.isEqual(sItemID, _sItemID))
				{
					if (_bCost)
					{
						dTotal += oRecord.getValue("cost").asDouble();
					}
					else
					{
						dTotal += oRecord.getValue("qty").asDouble(); 
					}				
				}
			}
		}
		return dTotal;
	}

	public static double sumCostPerGroup(List _vGroup, String _sKatID, String _sItemID, boolean _bCost) throws Exception
	{
		if(StringUtil.isNotEmpty(_sItemID))
		{
			return sumCostPerGroupByItem(_vGroup, _sItemID, _bCost);			
		}
		else
		{
			return sumCostPerGroupByKat(_vGroup, _sKatID, _bCost);						
		}
	}
	
	public static double sumPricePerGroup(List _vGroup, String _sKatID, String _sItemID) throws Exception
	{
		double dTotal = 0;
		if (_vGroup != null)
		{			
			for (int i = 0; i < _vGroup.size(); i++)
			{
				Record oRecord = (Record)_vGroup.get(i);
				String sVal = _sKatID;
				String sCol = "kategori_id";
				String sVals = _sKatID;
				if(StringUtil.isNotEmpty(_sItemID))
				{
					sCol = "item_id";
					sVal = _sItemID;
					sVals = _sItemID;
				}
				else
				{
					if (StringUtil.isNotEmpty(_sKatID))
					{
						List vChild = KategoriTool.getChildIDList(_sKatID);
						vChild.add(_sKatID);
						sVals = SqlUtil.convertToINMode(vChild);
						//System.out.println("Kat: " + KategoriTool.getLongDescription(_sKatID) + " sVals " + sVals);
					}
				}				
				String sColVal = oRecord.getValue(sCol).asString();
				
				if(StringUtil.isEqual(sVal,sColVal) || StringUtil.contains(sVals, sColVal))
				{				
					System.out.println( sColVal + " PLU: " + oRecord.getValue("item_code").asString() + ": " +  oRecord.getValue("price").asDouble());
					
					dTotal += oRecord.getValue("price").asDouble();									
				}				
			}
		}
		return dTotal;
	}
	
	/**
	 * get List of Last Inventory transaction as Of Certain Date
	 * Need inv_trans_kategori view
	 * @param _sKatID
	 * @param _sItemID
	 * @param _sLocationID
	 * @param _dEnd
	 * @return
	 * @throws Exception
	 */
	public static List getLastInvTrans(String _sKatID, String _sItemID, String _sLocationID, Date _dEnd) 
		throws Exception
	{
		if (_dEnd != null)
		{
			StringBuilder oSQL = new StringBuilder();
			String sEnd =  CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd));
			oSQL.append("SELECT DISTINCT ON(item_id,location_id) ")
				.append(" last_value(transaction_date) OVER wnd AS last_date,  ")
				.append(" last_value(inventory_transaction_id) OVER wnd AS id, ")
				.append(" kategori_id, location_id, item_id, item_code, ")				
				.append(" qty_balance, ")				
				.append(" value_balance ")
				.append(" FROM inv_trans_kategori itg ")
				.append(" WHERE itg.transaction_date <= '").append(sEnd).append("'");
			if (StringUtil.isNotEmpty(_sLocationID))
			{
				oSQL.append(" AND itg.location_id = '").append(_sLocationID).append("' ");			
			}		
			if (StringUtil.isNotEmpty(_sItemID))
			{
				oSQL.append(" AND itg.item_id = '").append(_sItemID).append("' ");			
			}				
			else 
			{
				if (StringUtil.isNotEmpty(_sKatID))						
				{		
					List vChild = KategoriTool.getChildIDList(_sKatID);
					vChild.add(_sKatID);
					String sKat = SqlUtil.convertToINMode(vChild);
					if (StringUtil.isNotEmpty(sKat) && !sKat.equals("()"))
					{
						oSQL.append(" AND itg.kategori_id IN ").append(sKat).append(" ");
					}	
				}
			}

			 oSQL.append (" WINDOW wnd AS ( ")
			     .append (" PARTITION BY location_id,item_id ORDER BY transaction_date DESC, inventory_transaction_id DESC ")
			     .append (" ) ")
			; 
			
			//oSQL.append(" GROUP BY kategori_id, location_id, item_id, inventory_transaction_id ");
			 System.out.println("getLastInvTrans: "  + oSQL.toString());
			 log.debug("getLastInvTrans: "  + oSQL.toString());
			return BasePeer.executeQuery(oSQL.toString());	
		}
		return null;
	}
	
	public static double sumLastInvCostByKategori(List _vLast, String _sKatID, boolean _bCost) throws Exception
	{
		double dTotal = 0;
		if (_vLast != null && StringUtil.isNotEmpty(_sKatID))
		{
			List vChild = KategoriTool.getChildIDList(_sKatID);
			vChild.add(_sKatID);
			String sKat = SqlUtil.convertToINMode(vChild);
			
			for (int i = 0; i < _vLast.size(); i++)
			{
				Record oRecord = (Record)_vLast.get(i);
				String sID = oRecord.getValue("id").asString();
				String sKatID = oRecord.getValue("kategori_id").asString();
				if(StringUtil.contains(sKat, sKatID) || StringUtil.isEqual(sKatID, _sKatID))
				{
					
					//log.debug(" Kategori : " + _sKatID + " Kats: " + sKat + " RECORD: " + oRecord);										
					if (_bCost)
					{
						dTotal += oRecord.getValue("value_balance").asDouble();							
					}
					else
					{
						dTotal += oRecord.getValue("qty_balance").asDouble();
					}
				}
			}
		}
		return dTotal;
	}

	public static double sumLastInvCostByItem(List _vLast, String _sItemID, boolean _bCost) throws Exception
	{
		double dTotal = 0;
		if (_vLast != null && StringUtil.isNotEmpty(_sItemID))
		{			
			for (int i = 0; i < _vLast.size(); i++)
			{
				Record oRecord = (Record)_vLast.get(i);
				String sID = oRecord.getValue("id").asString();
				String sItemID = oRecord.getValue("item_id").asString();
				if(StringUtil.isEqual(sItemID, _sItemID))
				{				
					//log.debug(" Item : " + oRecord.getValue("item_code").asString() + " RECORD: " + oRecord);
					if (_bCost)
					{
						dTotal += oRecord.getValue("value_balance").asDouble();							
						//dTotal += oInvTrans.getValueBalance().doubleValue();
					}
					else
					{
						dTotal += oRecord.getValue("qty_balance").asDouble();
						//dTotal += oInvTrans.getQtyBalance().doubleValue();							
					}					
				}
			}
		}
		return dTotal;
	}

	/**
	 * sum last inv cost by item / kat
	 * 
	 * @param _vLast
	 * @param _sKatID
	 * @param _sItemID
	 * @param _bCost
	 * @return
	 * @throws Exception
	 */
	public static double sumLastInvCost(List _vLast, String _sKatID, String _sItemID, boolean _bCost) throws Exception
	{
		if(StringUtil.isNotEmpty(_sItemID))
		{
			return sumLastInvCostByItem(_vLast, _sItemID, _bCost);
		}
		if(StringUtil.isNotEmpty(_sKatID))
		{
			return sumLastInvCostByKategori(_vLast, _sKatID, _bCost);			
		}
		return sumLastInvCost(_vLast, _bCost);					
	}

	
	/**
	 * sum all last inv cost
	 * 
	 * @param _vLast
	 * @param _bCost
	 * @return
	 * @throws Exception
	 */
	public static double sumLastInvCost(List _vLast, boolean _bCost) throws Exception
	{
		double dTotal = 0;
		if (_vLast != null)
		{			
			for (int i = 0; i < _vLast.size(); i++)
			{
				Record oRecord = (Record)_vLast.get(i);
				if(_bCost)
				{
					dTotal += oRecord.getValue("value_balance").asDouble();							
					//dTotal += oInvTrans.getValueBalance().doubleValue();
				}
				else
				{
					dTotal += oRecord.getValue("qty_balance").asDouble();
					//dTotal += oInvTrans.getQtyBalance().doubleValue();							
				}
			}
		}
		return dTotal;
	}
	
	//-------------------------------------------------------------------------
	// BY ADJUSTMENT TYPE
	//-------------------------------------------------------------------------

	public static List sumByAdj(String _sItemID, 
								String _sKatID,
								String _sLocationID,
								String _sVendorID,
								Date _dStart,
								Date _dEnd)
		throws Exception
	{
		if (_dStart != null && _dEnd != null)
		{
			String sStart = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStart));
			String sEnd = CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd));

			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT it.item_id, it.item_code, it.kategori_id, it.adjustment_type_id, ")
				.append(" SUM(it.qty_changes) AS qty, SUM(subcost) AS subcost ")
				.append(" FROM inv_trans_adj_type it ")			
				.append(" WHERE it.transaction_date >= '").append(sStart).append("'")
				.append(" AND it.transaction_date <= '").append(sEnd).append("'");			
			if (StringUtil.isNotEmpty(_sLocationID))
			{
				oSQL.append(" AND it.location_id = '").append(_sLocationID).append("' ");			
			}
			if (StringUtil.isNotEmpty(_sItemID))
			{
				oSQL.append(" AND it.item_id = '").append(_sItemID).append("'");
			}
			else 
			{
				if (StringUtil.isNotEmpty(_sKatID))						
				{		
					List vChild = KategoriTool.getChildIDList(_sKatID);
					vChild.add(_sKatID);
					String sKat = SqlUtil.convertToINMode(vChild);
					if (StringUtil.isNotEmpty(sKat) && !sKat.equals("()"))
					{
						oSQL.append(" AND it.kategori_id IN ").append(sKat).append(" ");
					}	
				}
			}
			oSQL.append(" GROUP BY it.item_id, it.item_code, it.kategori_id, it.adjustment_type_id ");
			log.debug(oSQL.toString());
			return BasePeer.executeQuery(oSQL.toString());			
		}
		return null;
	}	
	
	public static double sumCostByAdj(List _vAdj, String _sKatID, String _sItemID, String _sAdjID, boolean _bCost) throws Exception
	{
		String sKat = ""; //kategori with child id
		String sCol = "kategori_id";
		String sID = _sKatID;
		boolean bKat = true;
		if(StringUtil.isNotEmpty(_sItemID)) 
		{
			bKat = false;
			sID = _sItemID;
			sCol = "item_id"; 
		}
		
		double dTotal = 0;
		if (_vAdj != null && StringUtil.isNotEmpty(sID))
		{
			if (bKat)
			{
				List vChild = KategoriTool.getChildIDList(sID);
				vChild.add(sID);
				sKat = SqlUtil.convertToINMode(vChild);
			}
			for (int i = 0; i < _vAdj.size(); i++)
			{
				Record oRecord = (Record)_vAdj.get(i);
				String sColID = oRecord.getValue(sCol).asString();
				String sAdjID = oRecord.getValue("adjustment_type_id").asString();
				if(((bKat && StringUtil.contains(sKat, sColID)) || StringUtil.isEqual(sColID, sID)) 
						  && StringUtil.isEqual(_sAdjID, sAdjID))
				{
					if (_bCost)
					{
						dTotal += oRecord.getValue("subcost").asDouble();
					}
					else
					{
						dTotal += oRecord.getValue("qty").asDouble(); 
					}				
				}
			}
		}
		return dTotal;
	}
	
	//-------------------------------------------------------------------------
	// New Stock Valuation Report
	//-------------------------------------------------------------------------
	public static List getBalances(List _vItemID, 			 					 
			   					   String _sLocID,			 					 
			   					   Date _dAsOf)
	throws Exception
	{
		return getBalances(_vItemID, _sLocID, _dAsOf, -1);
	}
	
	public static List getBalances(List _vItemID, 			 					 
			 					   String _sLocID,			 					 
			 					   Date _dAsOf, 
			 					   int _iStockStatus)
		throws Exception
	{
		if (_dAsOf != null)
		{
			String sStr = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(PreferenceTool.getStartDate()));
			String sEnd = CustomFormatter.formatSqlDate(_dAsOf);

			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT i.item_id, ")
				.append(" SUM(it.qty_changes) AS qty, SUM(it.qty_changes * it.cost) AS cost, ")
				.append(" SUM(it.qty_changes * it.item_price) AS price ")
				.append(" FROM item i, inventory_transaction it  WHERE i.item_id = it.item_id ")			
				.append(" AND it.transaction_date >= '").append(sStr).append("'")
				.append(" AND it.transaction_date <= '").append(sEnd).append("'");			
			if (StringUtil.isNotEmpty(_sLocID))
			{
				oSQL.append(" AND it.location_id = '").append(_sLocID).append("' ");			
			}
			if (_vItemID != null && _vItemID.size() > 0)
			{
				String sID = SqlUtil.convertToINMode(_vItemID);
				oSQL.append(" AND i.item_id IN ").append(sID).append(" ");
			}
			oSQL.append(" GROUP BY i.item_id ");
			if(_iStockStatus > 0)
			{
				if(_iStockStatus == 3)  oSQL.append(" HAVING SUM(it.qty_changes) < 0 ");
				if(_iStockStatus == 4)  oSQL.append(" HAVING SUM(it.qty_changes) > 0 ");
				if(_iStockStatus == 12) oSQL.append(" HAVING SUM(it.qty_changes) = 0 ");
				if(_iStockStatus == 13) oSQL.append(" HAVING SUM(it.qty_changes) >= 0 ");
			}
			System.out.println("getBalances: " + oSQL.toString());
			return BasePeer.executeQuery(oSQL.toString());			
		}
		return null;
	}

	public static List getChanges(List _vItemID, 			 					 
			   					  String _sLocID,			 					 
			   					  Date _dStart,
			   					  Date _dEnd)
		throws Exception
	{
		if (_dEnd != null)
		{
			String sStr = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStart));
			String sEnd = CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd));

			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT i.item_id, it.transaction_type, ")
				.append(" SUM(it.qty_changes) AS qty, SUM(it.qty_changes * it.cost) AS cost, i.kategori_id, ")
				.append(" SUM(it.qty_changes * it.item_price) AS price ")
				.append(" FROM item i, inventory_transaction it  WHERE i.item_id = it.item_id ")			
				.append(" AND it.transaction_date >= '").append(sStr).append("'")
				.append(" AND it.transaction_date <= '").append(sEnd).append("'");			
			if (StringUtil.isNotEmpty(_sLocID))
			{
				oSQL.append(" AND it.location_id = '").append(_sLocID).append("' ");			
			}
			if (_vItemID != null && _vItemID.size() > 0)
			{
				String sID = SqlUtil.convertToINMode(_vItemID);
				oSQL.append(" AND i.item_id IN ").append(sID).append(" ");
			}
			oSQL.append(" GROUP BY i.item_id, it.transaction_type ");
			System.out.println("getChanges: " + oSQL.toString());
			return BasePeer.executeQuery(oSQL.toString());			
		}
		return null;
	}
	
	public static List getBalancesByKatID(List _vKatID, String _sLocID, Date _dAsOf)
		throws Exception
	{
		if (_dAsOf != null)
		{
			String sStr = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(PreferenceTool.getStartDate()));
			String sEnd = CustomFormatter.formatSqlDate(_dAsOf);

			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT i.kategori_id, ")
			.append(" SUM(it.qty_changes) AS qty, SUM(it.qty_changes * it.cost) AS cost, ")
			.append(" SUM(it.qty_changes * it.item_price) AS price ")
			.append(" FROM item i, inventory_transaction it  WHERE i.item_id = it.item_id ")			
			.append(" AND it.transaction_date >= '").append(sStr).append("'")
			.append(" AND it.transaction_date <= '").append(sEnd).append("'");			
			if (StringUtil.isNotEmpty(_sLocID))
			{
				oSQL.append(" AND it.location_id = '").append(_sLocID).append("' ");			
			}
			if (_vKatID != null && _vKatID.size() > 0)
			{
				String sID = SqlUtil.convertToINMode(_vKatID);
				oSQL.append(" AND i.kategori_id IN ").append(sID).append(" ");
			}
			oSQL.append(" GROUP BY i.kategori_id ");
			System.out.println("getBalancesByKatID: " + oSQL.toString());
			return BasePeer.executeQuery(oSQL.toString());			
		}
		return null;
	}

	public static List getChangesByKatID(List _vKatID, 			 					 
				  				  		 String _sLocID,			 					 
				  				  		 Date _dStart,
				  				  		 Date _dEnd)
         throws Exception
	{
		if (_dEnd != null)
		{
			String sStr = CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStart));
			String sEnd = CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd));

			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT i.kategori_id, it.transaction_type, ")
			.append(" SUM(it.qty_changes) AS qty, SUM(it.qty_changes * it.cost) AS cost, ")
			.append(" SUM(it.qty_changes * it.item_price) AS price ")
			.append(" FROM item i, inventory_transaction it  WHERE i.item_id = it.item_id ")			
			.append(" AND it.transaction_date >= '").append(sStr).append("'")
			.append(" AND it.transaction_date <= '").append(sEnd).append("'");			
			if (StringUtil.isNotEmpty(_sLocID))
			{
				oSQL.append(" AND it.location_id = '").append(_sLocID).append("' ");			
			}
			if (_vKatID != null && _vKatID.size() > 0)
			{
				String sID = SqlUtil.convertToINMode(_vKatID);
				oSQL.append(" AND i.kategori_id IN ").append(sID).append(" ");
			}
			oSQL.append(" GROUP BY i.kategori_id, it.transaction_type ");
			System.out.println("getChangesByKatID: " + oSQL.toString());
			return BasePeer.executeQuery(oSQL.toString());			
		}
		return null;
	}

	/**
	 * sum record by kategoriId
	 * 
	 * @param _vRecord List of Record
	 * @param _sKatID kategoriId 
	 * @param _iType    1. Balance, 2. In / Out, 3. Tx Type
	 * @param _iSubType 1. Balance {1. Qty, 2. Value}  2. In/Out {1. InQty, 2. OutQty 3. InVal, 4. OutVal} 3. Tx Type
	 * @return
	 */
	public static double sumByKatID(List _vRecord, String _sKatID, int _iType, int _iSubType)
	{
		
		double dTotal = 0;
		try 
		{			
			List vKatID = KategoriTool.getChildIDList(_sKatID);
			vKatID.add(_sKatID);
			for (int k = 0; k < vKatID.size(); k++)
			{
				String sKatID = (String) vKatID.get(k);
				for (int i = 0; i < _vRecord.size(); i++)
				{
					Record oRec = (Record) _vRecord.get(i);
					double dQty = oRec.getValue("qty").asDouble();
					double dVal = oRec.getValue("cost").asDouble();
					
					if (_iType == 1) //balance
					{
						if(StringUtil.equals(oRec.getValue("kategori_id").asString(), sKatID))
						{
							if(_iSubType == 1) dTotal += dQty;
							if(_iSubType == 2) dTotal += dVal;							
						}
					}
					if (_iType == 2) //in / out
					{
						if(StringUtil.equals(oRec.getValue("kategori_id").asString(), sKatID))
						{
							if(_iSubType == 1 && dQty >= 0) dTotal += dQty;
							if(_iSubType == 2 && dQty < 0) dTotal += dQty;

							if(_iSubType == 3 && dQty >= 0) dTotal += dVal;
							if(_iSubType == 4 && dQty < 0) dTotal += dVal;
						}
					}
					
				}
			}
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
		}
		return dTotal;
	
	}
	
	/**
	 * 
	 * @param _vRecord
	 * @param _sRecordCol Column Name
	 * @param _sValue Column value to compare
	 * @param _bIn is in 
	 * @param _bQty is qty / value
	 * @return
	 */
	public static double sumInOut(List _vRecord, String _sRecordCol, String _sValue, boolean _bIn, boolean _bQty)
	{
		double dTotal = 0;
		try 
		{			
			for (int i = 0; i < _vRecord.size(); i++)
			{
				Record oRec = (Record) _vRecord.get(i);
				double dQty = oRec.getValue("qty").asDouble();
				if (((_bIn && dQty >= 0) || (!_bIn && dQty < 0)) && 
					StringUtil.equals(oRec.getValue(_sRecordCol).asString(), _sValue))
				{
					if(_bQty) dTotal += dQty;
					else dTotal += oRec.getValue("cost").asDouble();
				}
			}	
		} 
		catch (Exception _oEx) 
		{
			log.error(_oEx);
		}
		return dTotal;
	}
}
package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.VendorTool;

public class VendorForm extends VendorSetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (sMode.equals("delete") || (iOp == 4 || sMode.equals("update")) || sMode.equals("view") )
			{
				sID = data.getParameters().getString("id");
				context.put("Vendor", VendorTool.getVendorByID(sID));
    		}
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx);
    	}
    }
}

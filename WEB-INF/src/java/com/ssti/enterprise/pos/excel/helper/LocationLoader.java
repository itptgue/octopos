package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.LocationManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.SiteTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

public class LocationLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( LocationLoader.class );
	private static final String s_START_HEADING = "Location Code";
	
	public LocationLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx) 
    	throws Exception
    {
    	StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started(_oRow, _iIdx))	
		{
			boolean bNew = true;
			boolean bValid = true;
			
			short i = 0;
			
			String sLocationCode = getString(_oRow, _iIdx + i); i++;
			String sLocationName = getString(_oRow, _iIdx + i); i++;
			
			if (StringUtil.isNotEmpty(sLocationCode))
			{				
				Location oLocation = LocationTool.getLocationByCode(sLocationCode);
				
				log.debug ("Column " + _iIdx + " Location Code : " + sLocationCode);
				
				if (oLocation == null) 
				{	
					log.debug ("Code Not Exist, create new Code");

					oLocation = new Location();
					oLocation.setLocationId (IDGenerator.generateSysID());
					oLocation.setLocationCode(sLocationCode);
					bNew = true;
				}
				else 
				{
					bNew = false;
				}
				oLocation.setLocationName(sLocationName); 
				bValid = validateString("Location Name", oLocation.getLocationName(), 100, oEmpty, oLength);
				
				oLocation.setLocationAddress (getString(_oRow, _iIdx + i)); i++;
				
                //email
                oLocation.setLocationEmail(getString(_oRow, _iIdx + i)); i++;

                //url
                oLocation.setLocationUrl(getString(_oRow, _iIdx + i)); i++;
				
                //site
                String sSiteName = getString(_oRow, _iIdx + i); i++;
                oLocation.setSiteId(SiteTool.getIDByName(sSiteName));

                //inventory type
                String sInventoryType = getString(_oRow, _iIdx + i); i++;
                int iInventoryType = 1;
                if(sInventoryType.equals("MINUS"))iInventoryType=2;
                if(sInventoryType.equals("DROPSHIP"))iInventoryType=3;
                oLocation.setInventoryType(iInventoryType);
                
                //Location Type
                String sLocType = getString(_oRow, _iIdx + i); i++;
                oLocation.setLocationType(Integer.parseInt(sLocType));

                //Ho2store lead time
                String sHO2StoreLeadTime = getString(_oRow, _iIdx + i); i++;
                oLocation.setHoStoreLeadtime(Integer.parseInt(sHO2StoreLeadTime));
                
                //description
                oLocation.setDescription(getString(_oRow, _iIdx + i)); i++;
                
                //parent code
                String sParent = getString(_oRow, _iIdx + i); i++;
                if (StringUtil.isNotEmpty(sParent))
                {
                	oLocation.setParentLocationId(LocationTool.getIDByCode(sParent));
                }

                //transfer acc
                String sTransAcc = getString(_oRow, _iIdx + i); i++;
                if (StringUtil.isNotEmpty(sTransAcc))
                {
                	Account oAcc = AccountTool.getAccountByCode(sTransAcc);
                	if (oAcc != null && !oAcc.getHasChild()) // not parent
                	{
                		oLocation.setTransferAccount(oAcc.getAccountId());
                	}
                	else
                	{
    					bValid = validateString("Transfer Account '" + sTransAcc + "'", oLocation.getTransferAccount(), oEmpty);                		
                	}
                }
                
                BigDecimal dLon = getBigDecimal(_oRow, _iIdx + i); i++;
                BigDecimal dLat = getBigDecimal(_oRow, _iIdx + i); i++;
                if(dLon != null) oLocation.setLongitudes(dLon);
                if(dLat != null) oLocation.setLatitudes(dLat);
                
                oLocation.save();
                LocationManager.getInstance().refreshCache(oLocation);
                
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sLocationCode,30));
						m_oNewData.append (StringUtil.left(sLocationName,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sLocationCode,30));
						m_oUpdated.append (StringUtil.left(sLocationName,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sLocationCode,30));
				    m_oRejected.append (StringUtil.left(sLocationName,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}      	
    }
}
package com.ssti.enterprise.pos.om;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Fri Apr 27 17:24:28 ICT 2012]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class BankTransferPeer
    extends com.ssti.enterprise.pos.om.BaseBankTransferPeer
{
}

package com.ssti.enterprise.medical.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.om.map.PresPowderDetailMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


  
/**
 */
public abstract class BasePresPowderDetailPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "pres_powder_detail";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(PresPowderDetailMapBuilder.CLASS_NAME);
    }

      /** the column name for the PRES_POWDER_DETAIL_ID field */
    public static final String PRES_POWDER_DETAIL_ID;
      /** the column name for the PRESCRIPTION_DETAIL_ID field */
    public static final String PRESCRIPTION_DETAIL_ID;
      /** the column name for the PRESCRIPTION_ID field */
    public static final String PRESCRIPTION_ID;
      /** the column name for the ITEM_ID field */
    public static final String ITEM_ID;
      /** the column name for the ITEM_CODE field */
    public static final String ITEM_CODE;
      /** the column name for the ITEM_NAME field */
    public static final String ITEM_NAME;
      /** the column name for the ITEM_DESC field */
    public static final String ITEM_DESC;
      /** the column name for the UNIT_ID field */
    public static final String UNIT_ID;
      /** the column name for the UNIT_CODE field */
    public static final String UNIT_CODE;
      /** the column name for the ITEM_DOSAGE field */
    public static final String ITEM_DOSAGE;
      /** the column name for the USE_DOSAGE field */
    public static final String USE_DOSAGE;
      /** the column name for the REQ_QTY field */
    public static final String REQ_QTY;
      /** the column name for the USED_QTY field */
    public static final String USED_QTY;
      /** the column name for the QTY field */
    public static final String QTY;
      /** the column name for the QTY_BASE field */
    public static final String QTY_BASE;
      /** the column name for the BILLED_QTY field */
    public static final String BILLED_QTY;
      /** the column name for the ITEM_PRICE field */
    public static final String ITEM_PRICE;
      /** the column name for the TAX_ID field */
    public static final String TAX_ID;
      /** the column name for the TAX_AMOUNT field */
    public static final String TAX_AMOUNT;
      /** the column name for the SUB_TOTAL field */
    public static final String SUB_TOTAL;
      /** the column name for the BATCH_NO field */
    public static final String BATCH_NO;
  
    static
    {
          PRES_POWDER_DETAIL_ID = "pres_powder_detail.PRES_POWDER_DETAIL_ID";
          PRESCRIPTION_DETAIL_ID = "pres_powder_detail.PRESCRIPTION_DETAIL_ID";
          PRESCRIPTION_ID = "pres_powder_detail.PRESCRIPTION_ID";
          ITEM_ID = "pres_powder_detail.ITEM_ID";
          ITEM_CODE = "pres_powder_detail.ITEM_CODE";
          ITEM_NAME = "pres_powder_detail.ITEM_NAME";
          ITEM_DESC = "pres_powder_detail.ITEM_DESC";
          UNIT_ID = "pres_powder_detail.UNIT_ID";
          UNIT_CODE = "pres_powder_detail.UNIT_CODE";
          ITEM_DOSAGE = "pres_powder_detail.ITEM_DOSAGE";
          USE_DOSAGE = "pres_powder_detail.USE_DOSAGE";
          REQ_QTY = "pres_powder_detail.REQ_QTY";
          USED_QTY = "pres_powder_detail.USED_QTY";
          QTY = "pres_powder_detail.QTY";
          QTY_BASE = "pres_powder_detail.QTY_BASE";
          BILLED_QTY = "pres_powder_detail.BILLED_QTY";
          ITEM_PRICE = "pres_powder_detail.ITEM_PRICE";
          TAX_ID = "pres_powder_detail.TAX_ID";
          TAX_AMOUNT = "pres_powder_detail.TAX_AMOUNT";
          SUB_TOTAL = "pres_powder_detail.SUB_TOTAL";
          BATCH_NO = "pres_powder_detail.BATCH_NO";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(PresPowderDetailMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(PresPowderDetailMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  21;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.medical.om.PresPowderDetail";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BasePresPowderDetailPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(PRES_POWDER_DETAIL_ID);
          criteria.addSelectColumn(PRESCRIPTION_DETAIL_ID);
          criteria.addSelectColumn(PRESCRIPTION_ID);
          criteria.addSelectColumn(ITEM_ID);
          criteria.addSelectColumn(ITEM_CODE);
          criteria.addSelectColumn(ITEM_NAME);
          criteria.addSelectColumn(ITEM_DESC);
          criteria.addSelectColumn(UNIT_ID);
          criteria.addSelectColumn(UNIT_CODE);
          criteria.addSelectColumn(ITEM_DOSAGE);
          criteria.addSelectColumn(USE_DOSAGE);
          criteria.addSelectColumn(REQ_QTY);
          criteria.addSelectColumn(USED_QTY);
          criteria.addSelectColumn(QTY);
          criteria.addSelectColumn(QTY_BASE);
          criteria.addSelectColumn(BILLED_QTY);
          criteria.addSelectColumn(ITEM_PRICE);
          criteria.addSelectColumn(TAX_ID);
          criteria.addSelectColumn(TAX_AMOUNT);
          criteria.addSelectColumn(SUB_TOTAL);
          criteria.addSelectColumn(BATCH_NO);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static PresPowderDetail row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            PresPowderDetail obj = (PresPowderDetail) cls.newInstance();
            PresPowderDetailPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      PresPowderDetail obj)
        throws TorqueException
    {
        try
        {
                obj.setPresPowderDetailId(row.getValue(offset + 0).asString());
                  obj.setPrescriptionDetailId(row.getValue(offset + 1).asString());
                  obj.setPrescriptionId(row.getValue(offset + 2).asString());
                  obj.setItemId(row.getValue(offset + 3).asString());
                  obj.setItemCode(row.getValue(offset + 4).asString());
                  obj.setItemName(row.getValue(offset + 5).asString());
                  obj.setItemDesc(row.getValue(offset + 6).asString());
                  obj.setUnitId(row.getValue(offset + 7).asString());
                  obj.setUnitCode(row.getValue(offset + 8).asString());
                  obj.setItemDosage(row.getValue(offset + 9).asBigDecimal());
                  obj.setUseDosage(row.getValue(offset + 10).asBigDecimal());
                  obj.setReqQty(row.getValue(offset + 11).asBigDecimal());
                  obj.setUsedQty(row.getValue(offset + 12).asBigDecimal());
                  obj.setQty(row.getValue(offset + 13).asBigDecimal());
                  obj.setQtyBase(row.getValue(offset + 14).asBigDecimal());
                  obj.setBilledQty(row.getValue(offset + 15).asBigDecimal());
                  obj.setItemPrice(row.getValue(offset + 16).asBigDecimal());
                  obj.setTaxId(row.getValue(offset + 17).asString());
                  obj.setTaxAmount(row.getValue(offset + 18).asBigDecimal());
                  obj.setSubTotal(row.getValue(offset + 19).asBigDecimal());
                  obj.setBatchNo(row.getValue(offset + 20).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BasePresPowderDetailPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(PresPowderDetailPeer.row2Object(row, 1,
                PresPowderDetailPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BasePresPowderDetailPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(PRES_POWDER_DETAIL_ID, criteria.remove(PRES_POWDER_DETAIL_ID));
                                                                                                                                                                                                              
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         PresPowderDetailPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(PresPowderDetail obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PresPowderDetail obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PresPowderDetail obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PresPowderDetail obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(PresPowderDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PresPowderDetail obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(PresPowderDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PresPowderDetail obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(PresPowderDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PresPowderDetail obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BasePresPowderDetailPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(PRES_POWDER_DETAIL_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( PresPowderDetail obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(PRES_POWDER_DETAIL_ID, obj.getPresPowderDetailId());
              criteria.add(PRESCRIPTION_DETAIL_ID, obj.getPrescriptionDetailId());
              criteria.add(PRESCRIPTION_ID, obj.getPrescriptionId());
              criteria.add(ITEM_ID, obj.getItemId());
              criteria.add(ITEM_CODE, obj.getItemCode());
              criteria.add(ITEM_NAME, obj.getItemName());
              criteria.add(ITEM_DESC, obj.getItemDesc());
              criteria.add(UNIT_ID, obj.getUnitId());
              criteria.add(UNIT_CODE, obj.getUnitCode());
              criteria.add(ITEM_DOSAGE, obj.getItemDosage());
              criteria.add(USE_DOSAGE, obj.getUseDosage());
              criteria.add(REQ_QTY, obj.getReqQty());
              criteria.add(USED_QTY, obj.getUsedQty());
              criteria.add(QTY, obj.getQty());
              criteria.add(QTY_BASE, obj.getQtyBase());
              criteria.add(BILLED_QTY, obj.getBilledQty());
              criteria.add(ITEM_PRICE, obj.getItemPrice());
              criteria.add(TAX_ID, obj.getTaxId());
              criteria.add(TAX_AMOUNT, obj.getTaxAmount());
              criteria.add(SUB_TOTAL, obj.getSubTotal());
              criteria.add(BATCH_NO, obj.getBatchNo());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( PresPowderDetail obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(PRES_POWDER_DETAIL_ID, obj.getPresPowderDetailId());
                          criteria.add(PRESCRIPTION_DETAIL_ID, obj.getPrescriptionDetailId());
                          criteria.add(PRESCRIPTION_ID, obj.getPrescriptionId());
                          criteria.add(ITEM_ID, obj.getItemId());
                          criteria.add(ITEM_CODE, obj.getItemCode());
                          criteria.add(ITEM_NAME, obj.getItemName());
                          criteria.add(ITEM_DESC, obj.getItemDesc());
                          criteria.add(UNIT_ID, obj.getUnitId());
                          criteria.add(UNIT_CODE, obj.getUnitCode());
                          criteria.add(ITEM_DOSAGE, obj.getItemDosage());
                          criteria.add(USE_DOSAGE, obj.getUseDosage());
                          criteria.add(REQ_QTY, obj.getReqQty());
                          criteria.add(USED_QTY, obj.getUsedQty());
                          criteria.add(QTY, obj.getQty());
                          criteria.add(QTY_BASE, obj.getQtyBase());
                          criteria.add(BILLED_QTY, obj.getBilledQty());
                          criteria.add(ITEM_PRICE, obj.getItemPrice());
                          criteria.add(TAX_ID, obj.getTaxId());
                          criteria.add(TAX_AMOUNT, obj.getTaxAmount());
                          criteria.add(SUB_TOTAL, obj.getSubTotal());
                          criteria.add(BATCH_NO, obj.getBatchNo());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PresPowderDetail retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PresPowderDetail retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PresPowderDetail retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        PresPowderDetail retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PresPowderDetail retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (PresPowderDetail)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( PRES_POWDER_DETAIL_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



          
                                              
                
                

    /**
     * selects a collection of PresPowderDetail objects pre-filled with their
     * PrescriptionDetail objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PresPowderDetailPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinPrescriptionDetail(Criteria criteria)
        throws TorqueException
    {
        setDbName(criteria);

        PresPowderDetailPeer.addSelectColumns(criteria);
        int offset = numColumns + 1;
        PrescriptionDetailPeer.addSelectColumns(criteria);


                        criteria.addJoin(PresPowderDetailPeer.PRESCRIPTION_DETAIL_ID,
            PrescriptionDetailPeer.PRESCRIPTION_DETAIL_ID);
        

                                                                                                                                                                                                                                                                                                                                                                                                  
        List rows = BasePeer.doSelect(criteria);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = PresPowderDetailPeer.getOMClass();
                    PresPowderDetail obj1 = PresPowderDetailPeer
                .row2Object(row, 1, omClass);
                     omClass = PrescriptionDetailPeer.getOMClass();
                    PrescriptionDetail obj2 = PrescriptionDetailPeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                PresPowderDetail temp_obj1 = (PresPowderDetail)results.get(j);
                PrescriptionDetail temp_obj2 = temp_obj1.getPrescriptionDetail();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                              temp_obj2.addPresPowderDetail(obj1);
                              break;
                }
            }
                      if (newObject)
            {
                obj2.initPresPowderDetails();
                obj2.addPresPowderDetail(obj1);
            }
                      results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

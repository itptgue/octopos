package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ReturnReason
 */
public abstract class BaseReturnReason extends BaseObject
{
    /** The Peer class */
    private static final ReturnReasonPeer peer =
        new ReturnReasonPeer();

        
    /** The value for the returnReasonId field */
    private String returnReasonId;
      
    /** The value for the code field */
    private String code;
      
    /** The value for the description field */
    private String description;
                                          
    /** The value for the transType field */
    private int transType = 0;
      
    /** The value for the lastUpdateBy field */
    private String lastUpdateBy;
      
    /** The value for the lastUpdate field */
    private Date lastUpdate;
  
    
    /**
     * Get the ReturnReasonId
     *
     * @return String
     */
    public String getReturnReasonId()
    {
        return returnReasonId;
    }

                        
    /**
     * Set the value of ReturnReasonId
     *
     * @param v new value
     */
    public void setReturnReasonId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.returnReasonId, v))
              {
            this.returnReasonId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Code
     *
     * @return String
     */
    public String getCode()
    {
        return code;
    }

                        
    /**
     * Set the value of Code
     *
     * @param v new value
     */
    public void setCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.code, v))
              {
            this.code = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransType
     *
     * @return int
     */
    public int getTransType()
    {
        return transType;
    }

                        
    /**
     * Set the value of TransType
     *
     * @param v new value
     */
    public void setTransType(int v) 
    {
    
                  if (this.transType != v)
              {
            this.transType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateBy
     *
     * @return String
     */
    public String getLastUpdateBy()
    {
        return lastUpdateBy;
    }

                        
    /**
     * Set the value of LastUpdateBy
     *
     * @param v new value
     */
    public void setLastUpdateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateBy, v))
              {
            this.lastUpdateBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdate
     *
     * @return Date
     */
    public Date getLastUpdate()
    {
        return lastUpdate;
    }

                        
    /**
     * Set the value of LastUpdate
     *
     * @param v new value
     */
    public void setLastUpdate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdate, v))
              {
            this.lastUpdate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ReturnReasonId");
              fieldNames.add("Code");
              fieldNames.add("Description");
              fieldNames.add("TransType");
              fieldNames.add("LastUpdateBy");
              fieldNames.add("LastUpdate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ReturnReasonId"))
        {
                return getReturnReasonId();
            }
          if (name.equals("Code"))
        {
                return getCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("TransType"))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals("LastUpdateBy"))
        {
                return getLastUpdateBy();
            }
          if (name.equals("LastUpdate"))
        {
                return getLastUpdate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ReturnReasonPeer.RETURN_REASON_ID))
        {
                return getReturnReasonId();
            }
          if (name.equals(ReturnReasonPeer.CODE))
        {
                return getCode();
            }
          if (name.equals(ReturnReasonPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(ReturnReasonPeer.TRANS_TYPE))
        {
                return Integer.valueOf(getTransType());
            }
          if (name.equals(ReturnReasonPeer.LAST_UPDATE_BY))
        {
                return getLastUpdateBy();
            }
          if (name.equals(ReturnReasonPeer.LAST_UPDATE))
        {
                return getLastUpdate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getReturnReasonId();
            }
              if (pos == 1)
        {
                return getCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getTransType());
            }
              if (pos == 4)
        {
                return getLastUpdateBy();
            }
              if (pos == 5)
        {
                return getLastUpdate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ReturnReasonPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ReturnReasonPeer.doInsert((ReturnReason) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ReturnReasonPeer.doUpdate((ReturnReason) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key returnReasonId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setReturnReasonId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setReturnReasonId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getReturnReasonId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ReturnReason copy() throws TorqueException
    {
        return copyInto(new ReturnReason());
    }
  
    protected ReturnReason copyInto(ReturnReason copyObj) throws TorqueException
    {
          copyObj.setReturnReasonId(returnReasonId);
          copyObj.setCode(code);
          copyObj.setDescription(description);
          copyObj.setTransType(transType);
          copyObj.setLastUpdateBy(lastUpdateBy);
          copyObj.setLastUpdate(lastUpdate);
  
                    copyObj.setReturnReasonId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ReturnReasonPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ReturnReason\n");
        str.append("------------\n")
           .append("ReturnReasonId       : ")
           .append(getReturnReasonId())
           .append("\n")
           .append("Code                 : ")
           .append(getCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("TransType            : ")
           .append(getTransType())
           .append("\n")
           .append("LastUpdateBy         : ")
           .append(getLastUpdateBy())
           .append("\n")
           .append("LastUpdate           : ")
           .append(getLastUpdate())
           .append("\n")
        ;
        return(str.toString());
    }
}

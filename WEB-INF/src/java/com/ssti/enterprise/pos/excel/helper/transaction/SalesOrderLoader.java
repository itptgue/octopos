package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesOrderLoader.java,v 1.1 2009/05/04 01:38:37 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesOrderLoader.java,v $
 * Revision 1.1  2009/05/04 01:38:37  albert
 * *** empty log message ***
 *
 * 
 * </pre><br>
 */
public class SalesOrderLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( SalesOrderLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Item Code";
	private static final String s_PAYMENT_HEADING = "Payment Type";
	private static final String s_END_TRANS = "End Trans";	
	
	public SalesOrderLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		boolean bPaymentProcess = false;
		
		SalesOrder oTR = null;
		List vTD = null;
		List vPMT = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new SalesOrder();
    				vPMT = new ArrayList();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (sValue.equals(s_PAYMENT_HEADING) && bTransHeader && bTransProcessed && !bPaymentProcess)
    			{
    				vPMT = new ArrayList();
    				bPaymentProcess = true;
    				
    				log.debug("Found Payment HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess && !bPaymentProcess)
    			{    				
    				SalesOrderDetail oTD = new SalesOrderDetail();
    				mapDetails (oRow, oTR, oTD);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			SalesOrderTool.setHeaderProperties(oTR, vTD, null);
    		    			SalesOrderTool.saveData(oTR, vTD);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			_oEx.printStackTrace();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;		
    				bPaymentProcess = false;
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, SalesOrder _oTR)
    	throws Exception
    {
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("Location", sLocationCode);}
    	else {_oTR.setLocationId(oLoc.getLocationId());}
    	
    	String sCustomerCode = getString(_oRow, iCol); iCol++;
    	Customer oCust = CustomerTool.getCustomerByCode(sCustomerCode);
    	if (oCust == null) {logError("Customer Code", sCustomerCode);}
    	else 
    	{
    		_oTR.setCustomerId(oCust.getCustomerId()); 
    		_oTR.setCustomerName(oCust.getCustomerName());
    	}
    	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setTransactionDate(dTrans);}

    	String sSales = getString(_oRow, iCol); iCol++;
    	Employee oSales = EmployeeTool.getEmployeeByName(sSales);
    	if (oSales != null) 
    	{
    		_oTR.setSalesId(oSales.getEmployeeId());
    	}
    	else 
    	{
    		_oTR.setSalesId("");
    	}

    	String sUserName = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sUserName)) {logError("User", sUserName);}
    	else {
    		_oTR.setUserName(sUserName);
    		_oTR.setConfirmBy(sUserName);
    	}
		
    	String sTypeCode = getString(_oRow, iCol); iCol++;
    	PaymentType oPT = PaymentTypeTool.getPaymentTypeByCode(sTypeCode);
    	if (oPT == null) {logError("Payment Type", sTypeCode);}
    	else {_oTR.setPaymentTypeId(oPT.getPaymentTypeId());}
    	
    	String sTermCode = getString(_oRow, iCol); iCol++;
    	PaymentTerm oPTerm = PaymentTermTool.getPaymentTermByCode(sTermCode);
    	if (oPTerm == null) {logError("Payment Term", sTermCode);}
    	else {_oTR.setPaymentTermId(oPTerm.getPaymentTermId());}

    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setRemark(sRemark);
    	
    	String sTotalDiscPct = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sTotalDiscPct)) sTotalDiscPct = "0";
    	_oTR.setTotalDiscountPct(sTotalDiscPct);
    	
    	String sInclusive = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sInclusive)) 
    	{
    		_oTR.setIsTaxable(Boolean.valueOf(sInclusive));
    		_oTR.setIsInclusiveTax(Boolean.valueOf(sInclusive));
    	}

    	//freight
    	BigDecimal oFreight = getBigDecimal(_oRow, iCol); iCol++;
    	_oTR.setEstimatedFreight(bd_ZERO);
    	if (oFreight != null)
    	{
    		_oTR.setEstimatedFreight(oFreight);
    	}
    	
    	//freight account
    	String sFRAcc = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sFRAcc))
    	{
    		Account oAcc = AccountTool.getAccountByCode(sFRAcc);
    		if (oAcc == null) {logError("FreightAcc", sFRAcc);}
    		else {_oTR.setFreightAccountId(oAcc.getAccountId());}
    	}    	
    	
    	//dp
    	BigDecimal oDP = getBigDecimal(_oRow, iCol); iCol++;
    	_oTR.setDownPayment(bd_ZERO);
    	if (oDP != null)
    	{
    		_oTR.setDownPayment(oDP);
    	}
    	
    	String sBank = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sBank))
    	{
    		Bank oBank = BankTool.getBankByCode(sBank);
    		if (oBank == null) {logError("Bank", sBank);}
    		else {_oTR.setBankId(oBank.getBankId());}
    	}
    	
    	String sDPAcc = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sDPAcc))
    	{
    		Account oAcc = AccountTool.getAccountByCode(sDPAcc);
    		if (oAcc == null) {logError("DPAcc", sDPAcc);}
    		else {_oTR.setDpAccountId(oAcc.getAccountId());}
    	}
    	
    	String sCFT = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sCFT))
    	{
    		CashFlowType oCFT = CashFlowTypeTool.getTypeByCode(sCFT,null);
    		if (oCFT == null) {logError("Cash Flow Type",sCFT);}
    		else {_oTR.setCashFlowTypeId(oCFT.getCashFlowTypeId());}
    	}
    	
    	String sDest = getString(_oRow, iCol); iCol++;
    	_oTR.setBankIssuer(sDest);
    	
    	String sRef = getString(_oRow, iCol); iCol++;
    	_oTR.setReferenceNo(sRef);
    	
    	String sDueDate = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sDueDate))
    	{
    		Date oDue = CustomParser.parseDate(sDueDate);
    		if (oDue != null) {_oTR.setDpDueDate(oDue);}
    		else {logError("DP Due Date",sDueDate);}
    	}
    	
    	//OTHER FIELD
    	if (!m_bTransError)
    	{
    		//_oTR.setInvoiceNo(sTransNo);
    		
	    	_oTR.setTransactionStatus(i_SO_ORDERED);
	    	_oTR.setCourierId("");
	    	_oTR.setRemark(_oTR.getRemark() + setDesc(sTransNo,m_sUserName));
	    	_oTR.setShipTo("");
	    	_oTR.setFobId("");
	    	
	    	//updated by processPayment
	    	_oTR.setDueDate(_oTR.getTransactionDate());
	    	
	    	//currency
	    	_oTR.setCurrencyId(CurrencyTool.getDefaultCurrency(null).getCurrencyId());
	    	_oTR.setCurrencyRate(bd_ONE);
	    	_oTR.setFiscalRate(bd_ONE);
	    	_oTR.setTotalFee(bd_ZERO);
    	}
    }
    
    protected void mapDetails (HSSFRow _oRow, SalesOrder _oTR, SalesOrderDetail _oTD)
		throws Exception
	{    	
    	int iCol = 0;
    	String sItemCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("item_code")).append(": ").append(sItemCode).append(s_LINE_SEPARATOR);
    	
    	Item oItem = ItemTool.getItemByCode(sItemCode);
    	if (oItem == null) {logError("Item Code", sItemCode);}
    	else 
    	{
    		_oTD.setItemId(oItem.getItemId());
    		_oTD.setItemCode(oItem.getItemCode());
    		_oTD.setItemName(oItem.getItemName());
    	}

    	BigDecimal dQty = getBigDecimal (_oRow, iCol); iCol++;
    	if (dQty == null) {logError("Qty", dQty);}
    	else 
    	{
    		_oTD.setQty(dQty);
    		_oTD.setQtyBase(UnitTool.getBaseQty(_oTD.getItemId(), _oTD.getUnitId(), dQty));
    	}

    	String sUnitCode = getString (_oRow, iCol); iCol++;
    	Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
    	if (oUnit == null) {logError("Unit Code", sUnitCode);}
    	else 
    	{
    		_oTD.setUnitId(oUnit.getUnitId());
    		_oTD.setUnitCode(oUnit.getUnitCode());
    	}

    	BigDecimal dPrice = getBigDecimal (_oRow, iCol); iCol++;
    	if (dPrice == null) {logError("Item Price", dPrice);}
    	else 
    	{
    		_oTD.setItemPrice(dPrice);
    	}

    	String sTaxCode = getString(_oRow, iCol); iCol++;
    	Tax oTax = TaxTool.getTaxByID(TaxTool.getIDByCode(sTaxCode));
    	if (oTax == null) {logError("Tax Code", sTaxCode);}
    	else
    	{
    		_oTD.setTaxId(oTax.getTaxId());
    		_oTD.setTaxAmount(oTax.getAmount());
    	}
    	
    	String sDisc = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sDisc)) {sDisc = "0";}
    	_oTD.setDiscount(sDisc);
    	
    	//OTHER FIELD
    	_oTD.setProjectId("");
    	_oTD.setDepartmentId("");
    	
		//CALCULATED FIELD
    	if (!m_bTransError)
    	{
    		double dSubTotal = _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();
    		double dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), dSubTotal);
    		double dSubTotalTax = _oTD.getTaxAmount().doubleValue() / 100 * (dSubTotal  - dSubTotalDisc);
    		
			dSubTotal = dSubTotal - dSubTotalDisc;
    		_oTD.setSubTotal(new BigDecimal(dSubTotal));
    		_oTD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
    		_oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
    		
    		//SKIP COST, will be recalculated when trans saved
    		_oTD.setCostPerUnit(bd_ZERO);
    	}
	}   



}

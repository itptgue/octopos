package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.sales.SalesAnalysisTool;
import com.ssti.framework.tools.StringUtil;

public class CustomerRankingReport extends RankingReport
{
	public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	if (StringUtil.isNotEmpty(sStart) && StringUtil.isNotEmpty(sEnd))
    	{
    		try
			{
    			List vData =
	    			SalesAnalysisTool.getRanking(false, 
	    					 					 "s.customer_id",
	    										 dStart, 
												 dEnd, 
												 iLimit, 
												 iRankBy, 
												 iRankType,
												 sLocationID,
												 sKategoriID,
												 sCustomerID,
												 sCustTypeID,
												 sCashier,
												 sSalesID,
												 bIncTax);
    			context.put("vData", vData);
			}
    		catch (Exception _oEx)
			{
    			log.error (_oEx);
			}
    	}
    }   
}

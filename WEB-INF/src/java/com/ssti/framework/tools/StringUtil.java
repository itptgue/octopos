package com.ssti.framework.tools;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Various String related utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: StringUtil.java,v 1.30 2008/06/29 07:11:30 albert Exp $ <br>
 * 
 * <pre>
 * 2018-05-28
 * -add method toCSV, convert from List of object to CSV 
 * 
 * 2016-09-08
 * -add method asciiValue to convert char to int 
 * 
 * 2016-05-15
 * -change StringUtil toList to use default separator
 * -Add ASCII to HEX
 * 
 * </pre><br>
 *
 */
public class StringUtil extends StringUtils implements Attributes
{    		
	private static final String s_SPACE  = " ";
	private static final int i_LEFT   = 1;
	private static final int i_RIGHT  = 2;
	private static final int i_CENTER = 3;
	private static final String s_NAME_VALUE_SEP = ":";
	
	static StringUtil instance = null;	
	public static synchronized StringUtil getInstance() 
	{
		if (instance == null) instance = new StringUtil();
		return instance;
	}	
	
	/**
	 * Flip string from a\nb to b\na
	 * 
	 * @param _sStr 
	 * @return flipped string
	 */
	public static String flip(String _sStr)
	{
		StringTokenizer oToken = new StringTokenizer(_sStr, s_LINE_SEPARATOR);
		StringBuilder oSB = new StringBuilder();
		while (oToken.hasMoreTokens())
		{
			oSB.insert(0, oToken.nextToken());
		}
		return oSB.toString();
	}
	
	/**
	 * check whether string is empty 
	 * 
	 * @param _sStr
	 * @return whether string is empty (== null or equals to "")
	 */
	public static boolean empty(String _sStr)
	{
		if (_sStr == null || _sStr.equals(""))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * set number format from e.g : 12 to 00012 
	 * 
	 * @param _sStr
	 * @param _iLength
	 * @return string of formatted number 
	 */
	public static String formatNumberString (String _sStr, int _iLength) 
	{
		StringBuilder sbVal = new StringBuilder ( _sStr );
		int iStrLength = _sStr.length();
		int iDiff = _iLength - iStrLength;
		if ( iDiff > 0 ) {
			for ( int i = 0; i < iDiff; i++ ) {
				sbVal.insert (0, 0);
			}
		}
		return sbVal.toString();
	}
	
	public static void setNull (Object _oObj) 
	{
		_oObj = null;
	}

	/**
	 * Align a string by adding white spaces used by text templates
	 * 
	 * @param _sStr
	 * @param _iLength
	 * @param _iAlign
	 * @return aligned string
	 */
	public static String align (String _sStr, int _iLength, int _iAlign)
	{
		if (_iAlign == i_RIGHT)  return right (_sStr, _iLength);
		if (_iAlign == i_CENTER) return center (_sStr, _iLength);
		return left (_sStr, _iLength);
	}

	
	/**
	 * left aligned string, String "ABC", expected length 10 
	 * will be formatted to "ABC       " used in text templates
	 * 
	 * @param _iLength
	 * @return left aligned String
	 */	
	public static String left (String _sStr, int _iLength)
	{
		return addToString (_sStr, _iLength, s_SPACE);
	}

	/**
	 * left aligned string, String "ABC", expected length 10 
	 * will be formatted to "ABC       " used in text templates
	 * 
	 * @param _iLength
	 * @return center aligned String
	 */	
	public static String center (String _sStr, int _iLength)
	{
		int iHalfExpected = _iLength / 2;
		int iStrHalf = _sStr.length() / 2;
		
		String sStr1 = _sStr.substring(0, iStrHalf);
		String sStr2 = _sStr.substring(iStrHalf, _sStr.length());
		
		String sHalf = addToFirstString (sStr1, iHalfExpected, s_SPACE);
		return sHalf + addToString(sStr2, iHalfExpected, s_SPACE);
	}	
	
	public static String centerMulti (String _sStr, int _iLength)
	{
    	if (_sStr != null)
    	{
    		if (_sStr.contains(s_UNIX_LINE_SEPARATOR))
    		{
    			_sStr = _sStr.replace(s_UNIX_LINE_SEPARATOR, s_WIN_LINE_SEPARATOR);
    		}
    		if (_sStr.contains(s_WIN_LINE_SEPARATOR))
    		{
				StringTokenizer oToken = new StringTokenizer(_sStr, s_WIN_LINE_SEPARATOR);
				StringBuilder oResult = new StringBuilder("");
				
				int i = 1;
				int iExistRow = oToken.countTokens();
						
				while (oToken.hasMoreTokens())
				{				
					String sRow = oToken.nextToken();
					oResult.append(center(sRow,_iLength)).append(s_WIN_LINE_SEPARATOR);
				}
				return oResult.toString();    	
    		}
    		return center(_sStr, _iLength); //not multiline
    	}
    	return "";
	}	
	
	
	
	/**
	 * right aligned string, String "ABC", expected length 10 
	 * will be formatted to "       ABC" used in text templates
	 * 
	 * @param _iLength
	 * @return right aligned String
	 */		
	public static String right (String _sStr, int _iLength)
	{
		return addToFirstString (_sStr, _iLength, s_SPACE);
	}	

    /**
     * cut a string into certain limit and add replacement character
     * to the end of the string
     * 
     * @param _sStr
     * @param _iLimit
     * @param _sReplace
     * @return cut string
     */
	public static String cut(String _sStr, int _iLimit, String _sReplace)
    {
        if (_sStr != null && _sReplace != null)
        {
        	int iLength = _sStr.length();
        	int iLimit = _iLimit - _sReplace.length();
        	if (iLength > _iLimit)
        	{
        		_sStr = _sStr.substring(0, iLimit);
        		return _sStr + _sReplace;
        	}
        }
        return _sStr;
    }
	
	/**
	 * @deprecated
	 * @param _sStr
	 * @param _iLength
	 * @param _sChar
	 * @return addToString
	 */
	public static String addCharToFillString (String _sStr, int _iLength, String _sChar)
	{
		return addToString (_sStr, _iLength, _sChar);
	}
	
	/**
	 * add certain string to another string or cut string if length exceeded
	 * "abc" to "abc   " 
	 * 
	 * @param _sStr
	 * @param _iLength
	 * @param _sChar
	 * @return processed string
	 */
	public static String addToString (String _sStr, int _iLength, String _sChar) 
	{
		if (_sStr != null)
		{
			StringBuilder sbVal = new StringBuilder (_sStr);
			int iStrLength = _sStr.length();
			int iDiff = _iLength - iStrLength;
			if (iDiff > 0) 
			{
				for (int i = 0; i < iDiff; i++) 
				{
					sbVal.append (_sChar);
				}
			}
			else if (iDiff < 0) 
			{
			    return sbVal.substring (0, _iLength);
			}
			return sbVal.toString();
		}
		return "";
	}

	/**
	 * @deprecated
	 * @param _sStr
	 * @param _iLength
	 * @param _sChar
	 * @return add to first string
	 */
	public static String addCharToFillFirstString (String _sStr, int _iLength, String _sChar)
	{
		return addToFirstString (_sStr, _iLength, _sChar);
	}
	
	/**
	 * add certain string to index 0 of another string 
	 * 
	 * @param _sStr
	 * @param _iLength
	 * @param _sChar
	 * @return processed 
	 */	
	public static String addToFirstString (String _sStr, int _iLength, String _sChar) 
	{
		StringBuilder sbVal = new StringBuilder ();
		int iStrLength = _sStr.length();
		int iDiff = _iLength - iStrLength;
		if (iDiff > 0) 
		{
			for (int i = 0; i < iDiff; i++) 
			{
				sbVal.append (_sChar);
			}
		}
		sbVal.append(_sStr);
		return sbVal.toString();
	}	
	
	/**
	 * create empty lines
	 * 
	 * @param _iNumOfLines
	 * @return string of lines "\n" 
	 */
	public static String line (int _iNumOfLines) 
	{
		StringBuilder sbVal = new StringBuilder ();
    	for ( int i = 0; i < _iNumOfLines; i++ ) 
    	{
    		sbVal.append ('\n');
		}
		return sbVal.toString();
	}

	/**
	 * 
	 * @param _vTxt
	 * @param _iColLength
	 * @return print column with default left align
	 */
	public static String printColumn (List _vTxt, int _iColLength) 
	{
		return printColumn (_vTxt, _iColLength, i_LEFT);
	}

	/**
	 * print multi line text into column according to number of string passed
	 * in the array
	 * 
	 * @param _vTxt Array of multi line text
	 * @param _iColLength length of each column
	 * @param _iAlign column alignment
	 * @return formatted string
	 */
	public static String printColumn (List _vTxt, int _iColLength, int _iAlign) 
	{
    	int iMaxRow = 0;
    	
    	//get maximum row exist in array
		for (int i = 0; i < _vTxt.size(); i++ )
    	{
    		StringTokenizer oToken = new StringTokenizer((String)_vTxt.get(i),s_LINE_SEPARATOR);
    		int iRow = oToken.countTokens();
    		if (iRow > iMaxRow) iMaxRow = iRow;
    	}
		
		StringBuilder oSB = new StringBuilder();
		for (int i = 1; i <= iMaxRow; i++ )
    	{
			for (int j = 0; j < _vTxt.size(); j++ )
			{
				String sTxt = getRow((String)_vTxt.get(j),i);
				if (_iAlign == i_RIGHT)
					oSB.append(right(sTxt, _iColLength));
				if (_iAlign == i_CENTER)
					oSB.append(center(sTxt, _iColLength));
				else
					oSB.append(left(sTxt, _iColLength));
			}
			oSB.append("\n");
    	}
		return oSB.toString();
	}	
	
	/**
	 * parse multi line string then get the expected row
	 * 
	 * @param _sText multi line text to parse
	 * @param _iRow
	 * @return processed string
	 */
	public static String getRow(String _sText, int _iRow)
	{
		StringTokenizer oToken = new StringTokenizer(_sText, s_LINE_SEPARATOR);
		int i = 1;
		int iExistRow = oToken.countTokens();
				
		if (_iRow <= iExistRow)
		{
			while (oToken.hasMoreTokens())
			{				
				String sRow = oToken.nextToken();
				if (i == _iRow) return sRow;
				i++;
			}
		}
		return "";
	}
	
	/**
	 * check whether two objects are equals
	 * 
	 * @param _o1
	 * @param _o2
	 * @return is the 2 objects are equals
	 */
	public static boolean isEqual (Object _o1, Object _o2)
	{
	    if (_o1 != null)
	    {
	        if (_o2 != null)
	        {
	            if (_o1.equals (_o2)) 
	            {   
	                return true;
	            }
	        }
	        else 
	        {
	            return false;
	        }
	    }
        else
        {
            if (_o2 == null)
            {
	            return true;
	        }
        }	    
        return false;
	}

	/**
	 * repeat string for a number of times
	 * 
	 * @param _sStr
	 * @param _iTimes
	 * @return repeated string
	 */
    public static String repeatString(String _sStr, int _iTimes)
    {
        StringBuilder oBuffer = new StringBuilder ("");
        for (int i = 0; i < _iTimes; i++)
        {
            oBuffer.append (_sStr);
        }
        return oBuffer.toString();
    }
    
    /**
     * escape special XML character
     * 
     * @param _sStr
     * @return formatted string
     */
    public static String escapeXML(String _sStr)
    {
    	if (_sStr != null)
    	{
	        _sStr = _sStr.replaceAll("&","&amp;")
	        	 		 .replaceAll("<","&lt;")
	        	 		 .replaceAll(">","&gt;")
	        	 		 .replaceAll("'","&apos;")
	        	 		 .replaceAll("\"","&quot;");
    	}
        return _sStr;
    }  
    
    /**
     * format a string to html
     * 
     * @param _sStr
     * @return formatted string
     */
    public static String formatStrToHTML(String _sStr)
    {
    	if (_sStr != null)
    	{
    		_sStr = _sStr.replaceAll("\r\n", "<br>");
	        _sStr = _sStr.replaceAll("\n", "<br>");
	        _sStr = _sStr.replaceAll(" ", "&nbsp;");
	        //TODO: complete some some other replacement
    	}    	
        return _sStr;
    }
    
    /**
     * format a string to html
     * 
     * @param _sStr
     * @return formatted string
     */
    public static String formatStrToFO(String _sStr)
    {
    	if (_sStr != null)
    	{
	        _sStr = escapeXML (_sStr); 
	        _sStr = _sStr.replaceAll("\n","<fo:block></fo:block>");
    	}
        return _sStr;
    }    
    
    /**
     * format a html to plain string
     * 
     * @param _sStr
     * @return formatted string
     */
    public static String formatHTMLToStr (String _sStr)
    {
    	if (_sStr != null)
    	{
	        _sStr = _sStr.replaceAll("<br>", "\n" );
	        _sStr = _sStr.replaceAll("&nbsp;", " ");
    	}
        //TODO: complete some some other replacement
        return _sStr;
    }
    
    /**
     * clean string from special JS char
     * @param _sStr
     * @return cleaned string
     */
    public static String cleanForJS (String _sStr)
    {
    	if (_sStr != null)
    	{
    		_sStr = _sStr.replace("'", "");
    		_sStr = _sStr.replace("\r", "");
    		_sStr = _sStr.replace("\t", "");
    		_sStr = _sStr.replace("\"", "");
    		_sStr = _sStr.replace("\\", "");
    	}
    	return _sStr;
    }
    
    public static String tooltip (String _sStr)
    {
    	if (_sStr != null)
    	{
    		_sStr = cleanForJS(_sStr);
    		_sStr = formatStrToHTML(_sStr);
    	}
        return _sStr;
    }    
    
    /**
     * parse a string to display in a JS
     * a String God's Love -> into 'God\'s Love'
     * 
     * @param _sStr
     * @return formatted string
     */
    public static String parseForJS (String _sStr)
    {
    	if (_sStr != null)
    	{
    		StringBuilder oSB = new StringBuilder (_sStr);
    		int iAddedChar = 0;
    		for (int i = 0; i < _sStr.length(); i++)
    		{
    			if (_sStr.charAt(i) == '\'' || _sStr.charAt(i) == '\\')
    			{
    	    		oSB.insert(i + iAddedChar, '\\');    				
    	    		iAddedChar++;
    			}
    		}
			_sStr = oSB.toString();
    	}
    	//add ' for string start;
    	if (_sStr == null) _sStr = "";
    	_sStr = '\'' + _sStr + '\'';
    	return _sStr;
    }
    
    /**
     * parse a string to display in a JS
     * a String God's Love -> into 'God\'s Love'
     * 
     * @param _sStr
     * @return formatted string
     */
    public static String parseForJS (String _sStr, boolean _bWithQuote)
    {
    	if (_sStr != null)
    	{
    		StringBuilder oSB = new StringBuilder (_sStr);
    		int iAddedChar = 0;
    		for (int i = 0; i < _sStr.length(); i++)
    		{
    			if (_sStr.charAt(i) == '\'' || _sStr.charAt(i) == '\\')
    			{
    	    		oSB.insert(i + iAddedChar, '\\');    				
    	    		iAddedChar++;
    			}
    		}
			_sStr = oSB.toString();
    	}
    	//add ' for string start;
    	if (_sStr == null) _sStr = "";
    	if (_bWithQuote)
    	{
    		_sStr = '\'' + _sStr + '\'';
    	}
    	return _sStr;
    }
    
    /**
     * parse a string to display in a JS
     * a String God's Love -> into 'God\'s Love'
     * 
     * @param _sStr
     * @return formatted string
     */
    public static String parseMultiLineForJS (String _sStr)
    {
    	if (_sStr != null)
    	{
    		if (_sStr.contains(s_UNIX_LINE_SEPARATOR))
    		{
    			_sStr = _sStr.replace(s_UNIX_LINE_SEPARATOR, s_WIN_LINE_SEPARATOR);
    		}
			StringTokenizer oToken = new StringTokenizer(_sStr, s_WIN_LINE_SEPARATOR);
			StringBuilder oResult = new StringBuilder("");
			
			int i = 1;
			int iExistRow = oToken.countTokens();
					
			while (oToken.hasMoreTokens())
			{				
				String sRow = oToken.nextToken();
				oResult.append("'").append(parseForJS(sRow,false)).append("\\n'");
				if (oToken.hasMoreTokens()) oResult.append(" + ");
			}
			if (isNotEmpty(oResult.toString()))
			{
				return oResult.toString();    	
			}
    	}
		return "''";
    }    
    
    /**
     * encode URL string
     * 
     * @param _sStr
     * @return encoded string
     */
    public static String encode (String _sStr)
    {
    	if (isNotEmpty(_sStr))
    	{
	    	try
			{
	    		_sStr = URLEncoder.encode(_sStr, s_DEFAULT_ENCODING);
			}
	    	catch(Exception _oEx)
			{
	    		_oEx.printStackTrace();
			}
    	}
    	return _sStr;
	}
    
    /**
     * decode URL string
     * 
     * @param _sStr
     * @return decoded string
     */
    public static String decode (String _sStr)
    {
        if (isNotEmpty(_sStr))
        {
            try
            {
                _sStr = URLDecoder.decode(_sStr, s_DEFAULT_ENCODING);
            }
            catch(Exception _oEx)
            {
                _oEx.printStackTrace();
            }
        }
        return _sStr;
    }
    
    /**
     * utility to get value from servlet parameter map
     * 
     * @param params
     * @param key
     * @return string from parameter map
     */
    public static String getString(Map params, String key)
    {
    	String result = "";
    	String[] aResult =  (String[]) params.get(key);
    	if (aResult != null && aResult.length > 0)
    	{
    		result = aResult[0];
    	}
    	return result;
    }
    
    /**
     * utility to get int value from servlet parameter map
     * 
     * @param params
     * @param key
     * @return int from parameter map
     */
    public static int getInt(Map params, String key)
    {
    	String sValue = getString(params, key);
    	if (StringUtil.isNotEmpty(sValue))
    	{
    		try 
    		{
    			return Integer.parseInt(sValue);	
			} 
    		catch (Exception e) 
    		{
			}    		
    	}
		return -1;
    }
    
    /**
     * utility to get boolean value from servlet parameter map
     * 
     * @param params
     * @param key
     * @return int from parameter map
     */
    public static boolean getBoolean(Map _oParam, String _sKey)
    {
    	String sValue = getString(_oParam, _sKey);
    	if (StringUtil.isNotEmpty(sValue))
    	{
    		try 
    		{
    			return Boolean.parseBoolean(sValue);	
			} 
    		catch (Exception e) 
    		{
			}    		
    	}
		return false;
    }
    

    /**
     * change "27,112,0,25,250" to {27,112,0,25,(byte)250}
     * invalid parameter not numeric will be converted to 0
     * 
     * @param _sStr
     * @return byte array
     */
    public static byte[] toByteArray(String _sStr)
    {
    	if (isNotEmpty(_sStr))
    	{
    		String[] as = split(_sStr, ",");
    		return toByteArray(as);
    	}
    	return null;
    }
    
    /**
     * convert byte array to string array 
     * 
     * @param as
     * @return byte array
     */
    public static byte[] toByteArray(String[] as)
    {
    	if (as != null)
    	{
	    	byte[] ab = new byte[as.length];
	    	for (int i = 0; i < as.length; i++)
	    	{	    	
	    		if (isNumeric(as[i]))
	    		{
	    			ab[i] = (byte) Integer.parseInt(as[i]);
	    		}
	    		else
	    		{
	    			ab[i] = 0;
	    		}
	    	}
	    	return ab;	   
    	}
    	return null;
    }    
    
    /**
     * convert byte array to string 
     * 
     * @param b
     * @return array in string divided by ","
     */
    public static String byteToStr(byte[] b)
    {
    	if (b != null)
    	{
    		StringBuilder sb = new StringBuilder();
	    	for (int i = 0; i < b.length; i++)
	    	{	    	
	    		sb.append(b[i]);
	    		if (i != (b.length -1)) sb.append(",");
	    	}
	    	return sb.toString();
    	}
    	return "";
    }
    
    /**
     * return {"name", "value"} from "name:value"
     * 
     * @param s
     * @return
     */
    public static String[] parseNameValue (String s)
    { 
    	String[] aResult = {"", ""};
    	if (isNotEmpty(s))
    	{
    		if (s.contains(s_NAME_VALUE_SEP))
    		{
    			try
    			{
    				int iSep = s.lastIndexOf(s_NAME_VALUE_SEP);
    				if (iSep < s.length())
    				{
	    				aResult[0] = s.substring(0, iSep);
	    				aResult[1] = s.substring(iSep + 1, s.length());
    				}
    			}
    			catch (Exception _oEx)
    			{
    				_oEx.printStackTrace();
    			}
    		}
    	}
    	return aResult;
    }
    
    /**
     * Create List from CSV String i.e "a,b,c" to ArrayList contains "a","b","c" 
     * 
     * @param s
     * @param _sSeparator
     * @return
     */
    public static List toList (String s, String _sSeparator)
    {
    	List vResult = new ArrayList();
    	if(s != null)
    	{
    		if(StringUtil.isEmpty(_sSeparator))
    		{
    			if(isMultiLine(s)) _sSeparator = s_LINE_SEPARATOR;
    			if(contains(s, ",")) _sSeparator = ",";
    			if(contains(s, ":")) _sSeparator = ":";    			
    		}
	    	StringTokenizer oToken = new StringTokenizer(s, _sSeparator);
	    	while (oToken.hasMoreTokens())
	    	{
	    		vResult.add(oToken.nextToken());
	    	}
    	}
    	return vResult;
    }

    public static boolean isMultiLine (String s)
    {
        if (s != null)
        {           
            if (s.contains(s_UNIX_LINE_SEPARATOR) || s.contains(s_WIN_LINE_SEPARATOR))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * change multi line string to one line
     * used in reporting where multi line desc need to become one line
     * 
     * @param s string to format
     * @return formatted string
     */
    public static String toOneLine (String s)
    {
    	if (s != null)
    	{    		
    		if (s.contains(s_UNIX_LINE_SEPARATOR))
			{
				s = s.replace(s_UNIX_LINE_SEPARATOR, s_WIN_LINE_SEPARATOR);
			}
			s = s.replace(s_WIN_LINE_SEPARATOR, " ");
    	}
    	return s;
    }
    
    public static String createString (int a, int b, int c, int d, int e, int f)
    {
    	byte[] ch = new byte[6];
    	if (a > 0) ch[0] = (byte)a;
    	if (b > 0) ch[1] = (byte)b;
    	if (c > 0) ch[2] = (byte)c;
    	if (d > 0) ch[3] = (byte)d;
    	if (e > 0) ch[4] = (byte)e;
    	if (e > 0) ch[5] = (byte)f;    	
    	return new String (ch);
    }  

    public static String createString (int a)
    {
    	byte[] ch = new byte[1];
    	if (a > 0) ch[0] = (byte)a;
    	return new String (ch);
    }

    public static String createString(byte[] b)
    {    	
    	if(b != null) return new String (b);
    	return "";
    }
    
    public static String splitStrToCSV (String s,  int len)
    {
    	if (StringUtil.isNotEmpty(s))
    	{    		
    		StringBuilder sb = new StringBuilder();
    		int i = 0;
    		while (i < s.length())
    		{
    			int to = i + len;
    			if (to > s.length()) to = s.length();
    			sb.append(s.substring(i, to));
    			i = to;
    			if (i < s.length())
    			{
    				sb.append(",");
    			}
    			
    		}
//    		for (int i =0; i < s.length(); i++)
//    		{
//    			sb.append(s.charAt(i));
//    			if (i < (s.length() - 1))
//    			{
//    				sb.append(",");
//    			}
//    		}
    		return sb.toString();
    	}
    	return "";    	
    }
    
    public static String field2Table(String s)
    {
        if (StringUtil.isNotEmpty(s))
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                String[] r = s.split("(?=\\p{Upper})");
                for (int i = 0; i < r.length; i++)
                {
                    sb.append(r[i]);
                    if(i < r.length - 1)
                    {
                        sb.append("_");
                    }
                }      
                return sb.toString();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return s;
    }
    
    public static boolean containsIgnoreCase(String s, List v)
    {
    	if (v != null)
    	{
    		for (int i = 0; i < v.size(); i++)
    		{
    			String s1 = (String) v.get(i).toString();
    			if (containsIgnoreCase(s, s1))
    			{
    				return true;
    			}
    		}
    	}
    	return false;
    }
    
    public static String getStackTrace(Throwable t) 
    {
        if (t != null)
        {
        	StringWriter sw = new StringWriter();
        	t.printStackTrace(new PrintWriter(sw));
        	return sw.toString();
        }
        return "";
    }
    
    /**
     * use to display error message contains with javascript
     * @param _sErr
     * @return
     */
    public static String parseErrMsgJS(String _sErr) 
    {
    	if (StringUtil.isNotEmpty(_sErr))
    	{    		
    		int iScriptPos = _sErr.indexOf("<script>");
    		if (iScriptPos > 0)
    		{
    			_sErr = _sErr.substring(0, iScriptPos);
    			return parseForJS(_sErr);
    		}
    		return parseForJS(_sErr);
    	}
        return _sErr;
    }


    /**
     * convert get ascii value from a string (1) char
     * 
     * @param str
     * @return
     */
    public static int asciiValue(String str)
    {
       if(str.length() > 0 && str.length() == 1)
       {
    	   return (int) str.charAt(0);
       }
       return 0;
    }

    /**
     * convert ascii string to hex
     * 
     * @param asciiValue
     * @return
     */
    public static String asciiToHex(String asciiValue)
    {
       char[] chars = asciiValue.toCharArray();
       StringBuffer hex = new StringBuffer();
       for (int i = 0; i < chars.length; i++)
       {
          hex.append(Integer.toHexString((int) chars[i]));
       }
       return hex.toString();
    }
  
    /**
     * convert hex to ASCII
     * 
     * @param hexValue
     * @return
     */
    public static String hexToASCII(String hexValue)
    {
       StringBuilder output = new StringBuilder("");
       for (int i = 0; i < hexValue.length(); i += 2)
       {
          String str = hexValue.substring(i, i + 2);
          output.append((char) Integer.parseInt(str, 16));
       }
       return output.toString();
    }
    
    static final String s_ALPHANUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();
    
    /**
     * generate random string
     * 
     * @param len
     * @return
     */
    public static String randomString(int len)
    {
       StringBuilder sb = new StringBuilder(len);
       for( int i = 0; i < len; i++)
       {
          sb.append( s_ALPHANUM.charAt(rnd.nextInt(s_ALPHANUM.length())) );
       }
       return sb.toString();
    }
    
    /**
     * 
     * @param _v
     * @param _sField
     * @return
     * @throws Exception
     */
    public static String toCSV(List _v, String _sField)
    	throws Exception
    {
    	StringBuilder oSB = new StringBuilder();
    	if(_v != null)
    	{
	    	for (int i = 0; i < _v.size(); i++)
	    	{
	    		String s = "";
	    		Object o = (Object) _v.get(i);    		
	    		if(StringUtil.isNotEmpty(_sField))
	    		{
	    			s = (String) BeanUtil.invokeGetter(o, _sField);
	    		}
	    		else
	    		{
	    			s = o.toString();
	    		}
	    		
	            if (s.equals(""))
	            {
	                oSB.append("");
	                break;
	            }
	            else 
	            {
	            	oSB.append(s);
	                if (i != (_v.size() - 1))
	                {
	                	oSB.append(",");
	                }
	            }            
	    	}
    	}
    	return oSB.toString();
    }
    
    public static String toCSV(String[] _a)
    	throws Exception
    {
    	StringBuilder oSB = new StringBuilder();
    	if(_a != null)
    	{
	    	for (int i = 0; i < _a.length; i++)
	    	{
	    		String s = _a[i];    		
	            if (s.equals(""))
	            {
	                oSB.append("");
	                break;
	            }
	            else 
	            {
	            	oSB.append(s);
	                if (i != (_a.length - 1))
	                {
	                	oSB.append(",");
	                }
	            }            
	    	}
    	}
    	return oSB.toString();
    }        
}

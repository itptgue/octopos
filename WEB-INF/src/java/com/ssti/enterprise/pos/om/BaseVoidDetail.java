package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to VoidDetail
 */
public abstract class BaseVoidDetail extends BaseObject
{
    /** The Peer class */
    private static final VoidDetailPeer peer =
        new VoidDetailPeer();

        
    /** The value for the voidDetailId field */
    private String voidDetailId;
      
    /** The value for the salesTransactionId field */
    private String salesTransactionId;
      
    /** The value for the transNo field */
    private String transNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
                                                
    /** The value for the description field */
    private String description = "";
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
      
    /** The value for the itemPrice field */
    private BigDecimal itemPrice;
      
    /** The value for the cashierName field */
    private String cashierName;
      
    /** The value for the voidDate field */
    private Date voidDate;
      
    /** The value for the voidBy field */
    private String voidBy;
                                                
    /** The value for the voidRemark field */
    private String voidRemark = "";
  
    
    /**
     * Get the VoidDetailId
     *
     * @return String
     */
    public String getVoidDetailId()
    {
        return voidDetailId;
    }

                        
    /**
     * Set the value of VoidDetailId
     *
     * @param v new value
     */
    public void setVoidDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.voidDetailId, v))
              {
            this.voidDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesTransactionId
     *
     * @return String
     */
    public String getSalesTransactionId()
    {
        return salesTransactionId;
    }

                        
    /**
     * Set the value of SalesTransactionId
     *
     * @param v new value
     */
    public void setSalesTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesTransactionId, v))
              {
            this.salesTransactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransNo
     *
     * @return String
     */
    public String getTransNo()
    {
        return transNo;
    }

                        
    /**
     * Set the value of TransNo
     *
     * @param v new value
     */
    public void setTransNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transNo, v))
              {
            this.transNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

                        
    /**
     * Set the value of ItemPrice
     *
     * @param v new value
     */
    public void setItemPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPrice, v))
              {
            this.itemPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashierName
     *
     * @return String
     */
    public String getCashierName()
    {
        return cashierName;
    }

                        
    /**
     * Set the value of CashierName
     *
     * @param v new value
     */
    public void setCashierName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashierName, v))
              {
            this.cashierName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VoidDate
     *
     * @return Date
     */
    public Date getVoidDate()
    {
        return voidDate;
    }

                        
    /**
     * Set the value of VoidDate
     *
     * @param v new value
     */
    public void setVoidDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.voidDate, v))
              {
            this.voidDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VoidBy
     *
     * @return String
     */
    public String getVoidBy()
    {
        return voidBy;
    }

                        
    /**
     * Set the value of VoidBy
     *
     * @param v new value
     */
    public void setVoidBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.voidBy, v))
              {
            this.voidBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VoidRemark
     *
     * @return String
     */
    public String getVoidRemark()
    {
        return voidRemark;
    }

                        
    /**
     * Set the value of VoidRemark
     *
     * @param v new value
     */
    public void setVoidRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.voidRemark, v))
              {
            this.voidRemark = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VoidDetailId");
              fieldNames.add("SalesTransactionId");
              fieldNames.add("TransNo");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("Description");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("Qty");
              fieldNames.add("QtyBase");
              fieldNames.add("ItemPrice");
              fieldNames.add("CashierName");
              fieldNames.add("VoidDate");
              fieldNames.add("VoidBy");
              fieldNames.add("VoidRemark");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VoidDetailId"))
        {
                return getVoidDetailId();
            }
          if (name.equals("SalesTransactionId"))
        {
                return getSalesTransactionId();
            }
          if (name.equals("TransNo"))
        {
                return getTransNo();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("ItemPrice"))
        {
                return getItemPrice();
            }
          if (name.equals("CashierName"))
        {
                return getCashierName();
            }
          if (name.equals("VoidDate"))
        {
                return getVoidDate();
            }
          if (name.equals("VoidBy"))
        {
                return getVoidBy();
            }
          if (name.equals("VoidRemark"))
        {
                return getVoidRemark();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VoidDetailPeer.VOID_DETAIL_ID))
        {
                return getVoidDetailId();
            }
          if (name.equals(VoidDetailPeer.SALES_TRANSACTION_ID))
        {
                return getSalesTransactionId();
            }
          if (name.equals(VoidDetailPeer.TRANS_NO))
        {
                return getTransNo();
            }
          if (name.equals(VoidDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(VoidDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(VoidDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(VoidDetailPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(VoidDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(VoidDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(VoidDetailPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(VoidDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(VoidDetailPeer.ITEM_PRICE))
        {
                return getItemPrice();
            }
          if (name.equals(VoidDetailPeer.CASHIER_NAME))
        {
                return getCashierName();
            }
          if (name.equals(VoidDetailPeer.VOID_DATE))
        {
                return getVoidDate();
            }
          if (name.equals(VoidDetailPeer.VOID_BY))
        {
                return getVoidBy();
            }
          if (name.equals(VoidDetailPeer.VOID_REMARK))
        {
                return getVoidRemark();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVoidDetailId();
            }
              if (pos == 1)
        {
                return getSalesTransactionId();
            }
              if (pos == 2)
        {
                return getTransNo();
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getItemName();
            }
              if (pos == 6)
        {
                return getDescription();
            }
              if (pos == 7)
        {
                return getUnitId();
            }
              if (pos == 8)
        {
                return getUnitCode();
            }
              if (pos == 9)
        {
                return getQty();
            }
              if (pos == 10)
        {
                return getQtyBase();
            }
              if (pos == 11)
        {
                return getItemPrice();
            }
              if (pos == 12)
        {
                return getCashierName();
            }
              if (pos == 13)
        {
                return getVoidDate();
            }
              if (pos == 14)
        {
                return getVoidBy();
            }
              if (pos == 15)
        {
                return getVoidRemark();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VoidDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VoidDetailPeer.doInsert((VoidDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VoidDetailPeer.doUpdate((VoidDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key voidDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setVoidDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setVoidDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVoidDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public VoidDetail copy() throws TorqueException
    {
        return copyInto(new VoidDetail());
    }
  
    protected VoidDetail copyInto(VoidDetail copyObj) throws TorqueException
    {
          copyObj.setVoidDetailId(voidDetailId);
          copyObj.setSalesTransactionId(salesTransactionId);
          copyObj.setTransNo(transNo);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setDescription(description);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setQty(qty);
          copyObj.setQtyBase(qtyBase);
          copyObj.setItemPrice(itemPrice);
          copyObj.setCashierName(cashierName);
          copyObj.setVoidDate(voidDate);
          copyObj.setVoidBy(voidBy);
          copyObj.setVoidRemark(voidRemark);
  
                    copyObj.setVoidDetailId((String)null);
                                                                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VoidDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("VoidDetail\n");
        str.append("----------\n")
           .append("VoidDetailId         : ")
           .append(getVoidDetailId())
           .append("\n")
           .append("SalesTransactionId   : ")
           .append(getSalesTransactionId())
           .append("\n")
           .append("TransNo              : ")
           .append(getTransNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("ItemPrice            : ")
           .append(getItemPrice())
           .append("\n")
           .append("CashierName          : ")
           .append(getCashierName())
           .append("\n")
           .append("VoidDate             : ")
           .append(getVoidDate())
           .append("\n")
           .append("VoidBy               : ")
           .append(getVoidBy())
           .append("\n")
           .append("VoidRemark           : ")
           .append(getVoidRemark())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.framework.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Iterator;
import java.util.LinkedList;

import javax.servlet.jsp.JspWriter;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * a more efficient ByteArrayOutputStream
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: FastByteArrayOutputStream.java,v 1.2 2007/02/23 14:12:09 albert Exp $ <br>
 *
 * <pre>
 * $Log: FastByteArrayOutputStream.java,v $
 * Revision 1.2  2007/02/23 14:12:09  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class FastByteArrayOutputStream
   extends OutputStream
{
   // Static --------------------------------------------------------

   private static final int DEFAULT_BLOCK_SIZE = 8192;

   // Attributes ----------------------------------------------------

   // internal buffer

   private byte[] buffer;
   private LinkedList buffers;
   private int index;
   private int size;
   private int blockSize;
   // is the stream closed?

   private boolean closed;

   // Constructors --------------------------------------------------

   public FastByteArrayOutputStream()
   {
      this(DEFAULT_BLOCK_SIZE);
   }

   public FastByteArrayOutputStream(int aSize)
   {
      blockSize = aSize;
      buffer = new byte[blockSize];
   }

   // Public
   public void writeTo(OutputStream out) throws IOException
   {
      // Check if we have a list of buffers
      if (buffers != null)
      {
         Iterator iterator = buffers.iterator();
         while (iterator.hasNext())
         {
            byte[] bytes = (byte[]) iterator.next();
            out.write(bytes, 0, blockSize);
         }
      }

      // write the internal buffer directly
      out.write(buffer, 0, index);
   }

   public void writeTo(RandomAccessFile out) throws IOException
   {
      // Check if we have a list of buffers

      if (buffers != null)
      {
         Iterator iterator = buffers.iterator();
         while (iterator.hasNext())
         {
            byte[] bytes = (byte[]) iterator.next();
            out.write(bytes, 0, blockSize);
         }
      }

      // write the internal buffer directly

      out.write(buffer, 0, index);
   }

   public void writeTo(JspWriter out, String encoding) throws IOException
   {
      // Check if we have a list of buffers

      if (buffers != null)
      {
         Iterator iterator = buffers.iterator();
         while (iterator.hasNext())
         {
            byte[] bytes = (byte[]) iterator.next();

            if (encoding != null)
               out.write(new String(bytes, encoding));
            else
               out.write(new String(bytes));
         }
      }

      // write the internal buffer directly

      if (encoding != null)
         out.write(new String(buffer, 0, index, encoding));
      else
         out.write(new String(buffer, 0, index));
   }

   public int getSize()
   {
      return size+index;
   }

   public byte[] toByteArray()
   {
      byte[] data = new byte[getSize()];

      // Check if we have a list of buffers

      int pos = 0;
      if (buffers != null)
      {
         Iterator iterator = buffers.iterator();
         while (iterator.hasNext())
         {
            byte[] bytes = (byte[]) iterator.next();
            System.arraycopy(bytes,0,data,pos,blockSize);
            pos+=blockSize;
         }
      }

      // write the internal buffer directly

      System.arraycopy(buffer, 0, data, pos, index);

      return data;
   }

   public String toString()
   {
      return new String(toByteArray());
   }

   /**
   * Create a new buffer and store the
   * current one in linked list
   */
   protected void addBuffer()
   {
      if (buffers == null)
         buffers = new LinkedList();

      buffers.addLast(buffer);

      buffer = new byte[blockSize];
      size += index;
      index = 0;
   }

   // OutputStream overrides ----------------------------------------

   public void write(int datum) throws IOException
   {
      if (closed)
      {
         throw new IOException("Stream closed");
      } else
      {
         if (index == blockSize)
            addBuffer();

         // store the byte

         buffer[index++] = (byte) datum;
      }
   }

   public void write(byte[] data, int offset, int length)
      throws IOException
   {
      if (data == null)
      {
         throw new NullPointerException();
      } else if ((offset < 0) || (offset + length > data.length)
         || (length < 0))
      {
         throw new IndexOutOfBoundsException();
      } else if (closed)
      {
         throw new IOException("Stream closed");
      } else
      {
         if (index + length > blockSize)
         {
            int copyLength;
            do
            {
               if (index==blockSize)
                  addBuffer();

               copyLength = blockSize-index;
               if (length < copyLength)
                  copyLength = length;
               System.arraycopy(data, offset, buffer, index, copyLength);
               offset += copyLength;
               index += copyLength;
               length -= copyLength;
            } while (length > 0);
         } else
         {
            // Copy in the subarray

            System.arraycopy(data, offset, buffer, index, length);
            index += length;
         }
      }
   }

   public void close()
   {
      closed = true;
   }
}


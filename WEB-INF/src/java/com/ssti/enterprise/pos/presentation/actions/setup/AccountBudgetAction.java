package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.AccountBudgetLoader;
import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/setup/AccountBudgetAction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: AccountBudgetAction.java,v 1.7 2007/02/15 03:28:49 albert Exp $
 *
 * $Log: AccountBudgetAction.java,v $
 * Revision 1.7  2007/02/15 03:28:49  albert
 * *** empty log message ***
 *
 * Revision 1.6  2006/03/22 15:52:38  albert
 * *** empty log message ***
 *
 */
public class AccountBudgetAction extends AccountSetupAction
{
    private static Log log = LogFactory.getLog ( AccountBudgetAction.class );

    public void doLoadfile (RunData data, Context context)
        throws Exception
    {
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
         	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			AccountBudgetLoader oLoader = new AccountBudgetLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
        	context.put ("LoadResult", oLoader.getResult());
        }
        catch (Exception _oEx) 
        {
        	String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
        	log.error ( sError, _oEx );
        	data.setMessage (sError);
        }
    }
}
package com.ssti.framework.excel.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base Excel helper 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BaseExcelHelper.java,v 1.13 2008/04/18 06:51:00 albert Exp $ <br>
 *
 * <pre>
 * $Log: BaseExcelHelper.java,v $
 *
 * 2016-12-05
 * - Change getString, format numeric to 2 digit comma, to prevent too long value 
 * </pre><br>
 *
 */
public abstract class BaseExcelHelper 
{                  
	private static Log log = LogFactory.getLog ( BaseExcelHelper.class );

    public void createHeaderCell(HSSFWorkbook _oWB, HSSFRow _oRow, short _iColumn, String _sValue)
    {                                                                                      
        createHeaderCell(_oWB, _oRow, _iColumn, HSSFCellStyle.ALIGN_LEFT, _sValue);                                                
    }
    
	/**
	 * create default type header cell
	 * 
	 * @param _oWB
	 * @param _oRow
	 * @param _iColumn
	 * @param _iAlign
	 * @param _sValue
	 */
    public void createHeaderCell(HSSFWorkbook _oWB, 
    							 HSSFRow _oRow, 
    							 short _iColumn, 
                                 short _iAlign, 
                                 String _sValue)
    {                                                                                      
        HSSFCell oCell = _oRow.createCell(_iColumn);                                            
        oCell.setCellValue(_sValue);  
             
        HSSFCellStyle oStyle = _oWB.createCellStyle();                                    
        oStyle.setAlignment(_iAlign);                                                     
        oStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        oStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);    
        oStyle.setWrapText(false);
        oCell.setCellStyle(oStyle);                                                      
    }           
    
    public void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short _iColumn, String _sValue)
    {                                                                                      
        createCell(_oWB, _oRow, _iColumn, HSSFCellStyle.ALIGN_LEFT, _sValue);                                                
    }
    
    public void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short _iColumn, BigDecimal _bdValue)
    {                                                                                  
    	String sValue = "";
    	if (_bdValue != null) sValue = _bdValue.toString();
        createCell(_oWB, _oRow, _iColumn, HSSFCellStyle.ALIGN_LEFT, sValue);                                                
    }
    
    /**
     * create cell 
     * 
     * @param _oWB
     * @param _oRow
     * @param _iColumn
     * @param _iAlign
     * @param _sValue
     */
    public void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short _iColumn, short _iAlign, String _sValue)
    {                                                                                      
        HSSFCell oCell = _oRow.createCell(_iColumn);                                            
        oCell.setCellValue(_sValue);  
        
        if (_iAlign == HSSFCellStyle.ALIGN_RIGHT)
        {
            HSSFCellStyle oStyle = _oWB.createCellStyle();                                    
            oStyle.setAlignment(_iAlign);                                           
            oStyle.setWrapText(false);
            oCell.setCellStyle(oStyle);        	
        }	                                                   
    }

    /**
     * create cell 
     * 
     * @param _oWB
     * @param _oRow
     * @param _iColumn
     * @param _iAlign
     * @param _sValue
     */
    public void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short iCol, Object _oValue)
    {                                                                                      
    	if (_oValue != null)
    	{
        	if (_oValue instanceof Date)
        	{
        		createCell(_oWB, _oRow, (short)iCol, (Date)_oValue);   	
        	}
        	else if (_oValue instanceof Number)
        	{
        		Number nVal = (Number)_oValue;
        		createCell(_oWB, _oRow, (short)iCol, nVal.doubleValue()); 
        	}
        	else
        	{
            	String sVal = _oValue.toString();
        		createCell(_oWB, _oRow, (short)iCol, sVal);          	
        	}
    	}
    	else
    	{
    		createCell(_oWB, _oRow, (short)iCol, ""); 
    	}                  
    }
    
    /**
     * create cell 
     * 
     * @param _oWB
     * @param _oRow
     * @param _iColumn
     * @param _iAlign
     * @param _sValue
     */
    public void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short _iColumn, double _dValue)
    {                                                                                      
        HSSFCell oCell = _oRow.createCell(_iColumn);                                            
        oCell.setCellValue(_dValue);  
        HSSFCellStyle oStyle = _oWB.createCellStyle();                                    
        oStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);                                           
        oStyle.setWrapText(false);
        oCell.setCellStyle(oStyle);        	                         
    }

    /**
     * create cell 
     * 
     * @param _oWB
     * @param _oRow
     * @param _iColumn
     * @param _iAlign
     * @param _sValue
     */
    public void createCell(HSSFWorkbook _oWB, HSSFRow _oRow, short _iColumn, Date _dValue)
    {                                                                                      
        HSSFCell oCell = _oRow.createCell(_iColumn);               
        oCell.setCellValue(CustomFormatter.formatDate(_dValue));  
        
        HSSFCellStyle oStyle = _oWB.createCellStyle();                                    
        oStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);                                           
        oStyle.setWrapText(false);        
        oStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
        oCell.setCellStyle(oStyle);        	                         
    }
    
    /**
     * 
     * @param _oRow
     * @param _iColumn
     * @return column value big decimal 
     */
    public BigDecimal getBigDecimal(HSSFRow _oRow, int _iColumn) 
    {                                                
        return getBigDecimal(_oRow, (short) _iColumn);
    }   
    
    /**
     * 
     * @param _oRow
     * @param _iColumn
     * @return column value in big decimal
     * @throws Exception 
     */
    public BigDecimal getBigDecimal(HSSFRow _oRow, short _iColumn)
    {                                
        if (_oRow != null)
        {                
    	    HSSFCell oCell = _oRow.getCell(_iColumn);
    	    if (oCell != null)
    	    {
    	        if (oCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
    	        {
    	        	return new BigDecimal (oCell.getNumericCellValue());
    	        }
    	        else if (oCell.getCellType() == HSSFCell.CELL_TYPE_STRING)
    	        {
    	        	String sValue = "";
    	        	try 
					{
    	        		sValue = oCell.getStringCellValue();
    	        		//sValue = sValue.replace(",","");
    	        		return new BigDecimal (StringUtil.trim(sValue));
    	        	}
    	        	catch(NumberFormatException _oNFEx)
    	        	{
    	        		log.error("ERROR parsing NUMERIC CELL ROW: " +  _oRow.getRowNum() + 
    	        				  " COL:" + _iColumn + " VALUE:" + sValue, _oNFEx);
    	        		
    	        		return Attributes.bd_ZERO;                                    
    	        	}
    	        }
            }
        }
    	return Attributes.bd_ZERO;                                    
    }        
    
    /**
     * 
     * @param _oRow
     * @param _iColumn
     * @return column value in string
     */
    public String getString(HSSFRow _oRow, int _iColumn)
    {                                                
        return getString(_oRow, (short) _iColumn);
    }    
    
    /**
     * 
     * @param _oRow
     * @param _iColumn
     * @return column value in string
     */
    public String getString(HSSFRow _oRow, short _iColumn)
    {                                                
    	if (_oRow != null)
    	{
    	    HSSFCell oCell = _oRow.getCell(_iColumn);

    	    if (oCell != null)
    	    {    	    
    	        if (oCell.getCellType() == HSSFCell.CELL_TYPE_STRING)
    	        {
    	        	return StringUtil.trim(oCell.getStringCellValue());
    	        }
    	        else if (oCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
    	        {    
    	            StringBuilder oSB = new StringBuilder();
    	            BigDecimal dVal = new BigDecimal (oCell.getNumericCellValue());
    	            dVal = dVal.setScale(2, BigDecimal.ROUND_HALF_UP);
    	        	return StringUtil.trim(oSB.append(dVal).toString());
    	        }
            }
        }
        return "";
    }             
    
    /**
     * get HTTP Parameter value stored in Parameter Map
     * 
     * @param oParam
     * @param _sKey
     * @param clazz
     * @return
     */
    public static Object getFromParam(Map oParam, String _sKey, Class clazz)
    {
    	String[] aParam = (String[]) oParam.get(_sKey);
    	 if (aParam != null && aParam.length > 0) 
    	 {
    		 if (clazz != null)
    		 {
	    		 if (clazz.equals(Date.class))
	    		 {
	    			 return CustomParser.parseDate(aParam[0]);
	    		 }
	    		 else if (clazz.equals(Integer.class))
	    		 {
	    			 if (StringUtil.isNotEmpty(aParam[0]))
	    			 {
	    				 return Integer.valueOf(aParam[0]);
	    			 }
	    			 else
	    			 {
	    				 return Integer.valueOf(-1);
	    			 }
	    		 }
    		 }
    		return aParam[0];
    	 }
    	 return "";
    }
}                                                                                           
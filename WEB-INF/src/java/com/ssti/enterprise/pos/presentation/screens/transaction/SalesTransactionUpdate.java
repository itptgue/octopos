package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

public class SalesTransactionUpdate extends SalesTransactionView
{
	protected static final String[] a_PERM = {"View Sales Invoice"};
    
	StringBuilder sResult = new StringBuilder();
	String sField = "";
    public void doBuildTemplate(RunData data, Context context)
    {
		super.doBuildTemplate(data, context);
		
		int iUpdate = data.getParameters().getInt("update");
		if (iUpdate == 1)
		{
			sField = data.getParameters().getString("field");
			log.debug("Updating Field " + sField);
			
			if (StringUtil.isNotEmpty(sField))
			{			
				List vData = (List) context.get(s_CTX_VAR);
				if (vData != null)
				{
					try 
					{					
						updateSI(data, vData);
						data.setMessage("SI Updated Successfully");
						context.put("result",sResult);
					} 
					catch (Exception _oEx) 
					{
						_oEx.printStackTrace();
						log.error(_oEx);
						data.setMessage("Update SI Failed, ERROR: " + _oEx.getMessage());
					}
					//gen PO 
				}
			}
		}
	}
    
	private void updateSI(RunData data, List _vSI)
		throws Exception
	{			
		for (int i = 0; i < _vSI.size(); i++)
		{
			int iNo = i + 1;
			SalesTransaction oSI = (SalesTransaction) _vSI.get(i);			
			log.debug("Updating SI " + oSI.getTransactionNo());

			String sValue = data.getParameters().getString("field" + iNo);
			if (oSI != null && StringUtil.isNotEmpty(sValue))
			{
				log.debug("Value " + sValue);
				BeanUtil.setProperty (oSI, sField, sValue);
				oSI.save();
			}
		}
	}    		
}
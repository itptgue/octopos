package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.om.BankTransfer;
import com.ssti.enterprise.pos.tools.financial.BankTransferTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SortingTool;

public class BankTransferHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Cash Management Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {                
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        String sKeywords = (String) getFromParam(oParam, "Keywords", null);        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        String sBankID = (String) getFromParam(oParam, "BankId", null);        
        String sLocationID = (String) getFromParam(oParam, "LocationId", null);
        int iStatus = (Integer) getFromParam(oParam, "status", Integer.class);  
        
        LargeSelect oData = BankTransferTool.findData(iCond, sKeywords, sBankID, sLocationID, iStatus, dStart, dEnd, 1000);
        List vData = new ArrayList();
        		
        if (oData != null)
        {
        	vData = oData.getNextResults();
        	vData = SortingTool.sort(vData,"cashFlowNo");
        
        	int iRow = 0;

        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
    		createMasterHeader(oRow);

        	for (int i = 0; i < vData.size(); i++)
        	{
        		BankTransfer oTR = (BankTransfer) vData.get(i);
        		oRow = oSheet.createRow(iRow); iRow++;
        		createMasterContent(oRow, oTR);        		
        	}
        }
        return oWB;
        
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Amount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "From Bank"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "From Loc"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "From Acc"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "From CFT"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "To Bank"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "To Loc"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "To Acc"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "To CFT"); iCol++;

    }
    
	private void createMasterContent(HSSFRow oRow, BankTransfer oTR) 
		throws Exception
	{
    	int iCol = 0;
		createCell(oWB, oRow, (short)iCol, oTR.getTransactionNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getTransactionDate()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getDescription()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getAmount()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getOutBank().getCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getOutLoc().getCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getOutAcc().getCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getOutCftCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getInBank().getCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getInLoc().getCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getInAcc().getCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getInCftCode()); iCol++;		
	}
}

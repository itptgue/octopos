package com.ssti.framework.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DbHistoryMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.DbHistoryMapBuilder";

    /**
     * Item
     * @deprecated use DbHistoryPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "db_history";
    }

  
    /**
     * db_history.DB_HISTORY_ID
     * @return the column name for the DB_HISTORY_ID field
     * @deprecated use DbHistoryPeer.db_history.DB_HISTORY_ID constant
     */
    public static String getDbHistory_DbHistoryId()
    {
        return "db_history.DB_HISTORY_ID";
    }
  
    /**
     * db_history.DB_VERSION
     * @return the column name for the DB_VERSION field
     * @deprecated use DbHistoryPeer.db_history.DB_VERSION constant
     */
    public static String getDbHistory_DbVersion()
    {
        return "db_history.DB_VERSION";
    }
  
    /**
     * db_history.VERSION_DATE
     * @return the column name for the VERSION_DATE field
     * @deprecated use DbHistoryPeer.db_history.VERSION_DATE constant
     */
    public static String getDbHistory_VersionDate()
    {
        return "db_history.VERSION_DATE";
    }
  
    /**
     * db_history.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use DbHistoryPeer.db_history.UPDATE_DATE constant
     */
    public static String getDbHistory_UpdateDate()
    {
        return "db_history.UPDATE_DATE";
    }
  
    /**
     * db_history.UPDATE_SCRIPT
     * @return the column name for the UPDATE_SCRIPT field
     * @deprecated use DbHistoryPeer.db_history.UPDATE_SCRIPT constant
     */
    public static String getDbHistory_UpdateScript()
    {
        return "db_history.UPDATE_SCRIPT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("db_history");
        TableMap tMap = dbMap.getTable("db_history");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("db_history.DB_HISTORY_ID", "");
                          tMap.addColumn("db_history.DB_VERSION", "");
                          tMap.addColumn("db_history.VERSION_DATE", "");
                          tMap.addColumn("db_history.UPDATE_DATE", new Date());
                          tMap.addColumn("db_history.UPDATE_SCRIPT", "");
          }
}

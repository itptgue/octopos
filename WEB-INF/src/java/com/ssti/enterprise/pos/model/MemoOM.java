package com.ssti.enterprise.pos.model;

public interface MemoOM extends SubLedgerOM
{
	public String getMemoId();
	public String getMemoNo();
	public Object getReturnTrans();
	public String getStatusStr();
}

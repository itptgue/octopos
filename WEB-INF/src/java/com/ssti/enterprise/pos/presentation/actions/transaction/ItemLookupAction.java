package com.ssti.enterprise.pos.presentation.actions.transaction;

import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-04-07
 * - change in ItemTool.findData add Parameter PrincipalID 
 * 
 * 2016-07-21
 * -add BrandID parameters
 * 
 * </pre><br>
 */
public class ItemLookupAction extends TransactionSecureAction
{
    public void doPerform(RunData data, Context context)
        throws Exception
    {
    	doFind (data,context);
    }

	public void doFind(RunData data, Context context)
        throws Exception
    {
		try 
		{
			int iCondition 	      = data.getParameters().getInt  	("Condition");
			String sKeyword       = data.getParameters().getString  ("Keywords");
			String sKatID 	      = data.getParameters().getString  ("KategoriId");		
			int iSortBy 	      = data.getParameters().getInt  	("SortBy", 2);
			int iViewLimit 	      = data.getParameters().getInt  	("ViewLimit", 25);
			String sGrpID         = data.getParameters().getString  ("groupid");
			String sVendorID      = data.getParameters().getString  ("VendorId","");
			String sLocationID    = data.getParameters().getString  ("LocationId","");
			String sBrandID       = data.getParameters().getString  ("BrandId","");	
			String sPrincID       = data.getParameters().getString  ("PrincipalId","");
			int iStockStatus	  = data.getParameters().getInt     ("StockStatus",-1);
			int iItemStatus 	  = data.getParameters().getInt     ("ItemStatus",1);
			boolean bGroupBySKU   = data.getParameters().getBoolean ("GroupBySKU", false);
			int iStockToView      = data.getParameters().getInt     ("StockToView",0);
			int iStockToViewEnd   = data.getParameters().getInt     ("StockToViewEnd",0);
			int iUsedBarcode   	  = data.getParameters().getInt     ("UsedBarcode",-1);
			int iTransactionGroup = data.getParameters().getInt 	("TransactionGroup",-1);
			int iItemType  		  = data.getParameters().getInt 	("ItemType",-1);
			
			LargeSelect vItem     = ItemTool.findData ( iCondition, 
														sKeyword, sKatID, 
														iSortBy, 
														iViewLimit, 
														sGrpID, 
													    sVendorID, 
														sLocationID, 
														sBrandID,
														sPrincID,
														iStockStatus, 
														iItemStatus, 
														-1,
													    bGroupBySKU,
														iStockToView,
														iStockToViewEnd,
														iUsedBarcode,
														iTransactionGroup,
														iItemType);
            
			data.getUser().setTemp("findItemResult", vItem);	
        }
        catch (Exception _oEx) 
        {
        	String sError = LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage();
        	data.setMessage (sError);
        }
    }
}
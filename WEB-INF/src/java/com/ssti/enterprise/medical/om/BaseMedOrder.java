package com.ssti.enterprise.medical.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to MedOrder
 */
public abstract class BaseMedOrder extends BaseObject
{
    /** The Peer class */
    private static final MedOrderPeer peer =
        new MedOrderPeer();

        
    /** The value for the medOrderId field */
    private String medOrderId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the status field */
    private int status;
      
    /** The value for the orderNo field */
    private String orderNo;
      
    /** The value for the invoiceNo field */
    private String invoiceNo;
                                                
    /** The value for the deliveryNo field */
    private String deliveryNo = "";
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the dueDate field */
    private Date dueDate;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount;
      
    /** The value for the totalTax field */
    private BigDecimal totalTax;
                                                
    /** The value for the courierId field */
    private String courierId = "";
                                                
          
    /** The value for the shippingPrice field */
    private BigDecimal shippingPrice= bd_ZERO;
                                                
          
    /** The value for the totalExpense field */
    private BigDecimal totalExpense= bd_ZERO;
      
    /** The value for the totalCost field */
    private BigDecimal totalCost;
      
    /** The value for the salesId field */
    private String salesId;
      
    /** The value for the cashierName field */
    private String cashierName;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
                                          
    /** The value for the printTimes field */
    private int printTimes = 0;
                                                
          
    /** The value for the paidAmount field */
    private BigDecimal paidAmount= bd_ZERO;
                                                
          
    /** The value for the paymentAmount field */
    private BigDecimal paymentAmount= bd_ZERO;
                                                
          
    /** The value for the changeAmount field */
    private BigDecimal changeAmount= bd_ZERO;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
                                                
    /** The value for the shipTo field */
    private String shipTo = "";
                                                
    /** The value for the fobId field */
    private String fobId = "";
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
      
    /** The value for the paymentDate field */
    private Date paymentDate;
      
    /** The value for the paymentStatus field */
    private int paymentStatus;
      
    /** The value for the isInclusiveFreight field */
    private boolean isInclusiveFreight;
                                                
    /** The value for the freightAccountId field */
    private String freightAccountId = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
                                                
    /** The value for the salesTransactionId field */
    private String salesTransactionId = "";
                                                
          
    /** The value for the totalFee field */
    private BigDecimal totalFee= bd_ZERO;
                                                
          
    /** The value for the roundingAmount field */
    private BigDecimal roundingAmount= bd_ZERO;
                                                
    /** The value for the doctorName field */
    private String doctorName = "";
                                                
    /** The value for the patientName field */
    private String patientName = "";
                                                
    /** The value for the patientAge field */
    private String patientAge = "";
                                                
    /** The value for the patientPhone field */
    private String patientPhone = "";
                                                
    /** The value for the patientAddress field */
    private String patientAddress = "";
                                                
    /** The value for the registrationId field */
    private String registrationId = "";
                                                
    /** The value for the insuranceId field */
    private String insuranceId = "";
                                                
    /** The value for the policyNo field */
    private String policyNo = "";
                                                
    /** The value for the policyName field */
    private String policyName = "";
      
    /** The value for the isInsurance field */
    private boolean isInsurance;
  
    
    /**
     * Get the MedOrderId
     *
     * @return String
     */
    public String getMedOrderId()
    {
        return medOrderId;
    }

                                              
    /**
     * Set the value of MedOrderId
     *
     * @param v new value
     */
    public void setMedOrderId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.medOrderId, v))
              {
            this.medOrderId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated MedOrderDetail
        if (collMedOrderDetails != null)
        {
            for (int i = 0; i < collMedOrderDetails.size(); i++)
            {
                ((MedOrderDetail) collMedOrderDetails.get(i))
                    .setMedOrderId(v);
            }
        }
                                }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OrderNo
     *
     * @return String
     */
    public String getOrderNo()
    {
        return orderNo;
    }

                        
    /**
     * Set the value of OrderNo
     *
     * @param v new value
     */
    public void setOrderNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.orderNo, v))
              {
            this.orderNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceNo
     *
     * @return String
     */
    public String getInvoiceNo()
    {
        return invoiceNo;
    }

                        
    /**
     * Set the value of InvoiceNo
     *
     * @param v new value
     */
    public void setInvoiceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceNo, v))
              {
            this.invoiceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryNo
     *
     * @return String
     */
    public String getDeliveryNo()
    {
        return deliveryNo;
    }

                        
    /**
     * Set the value of DeliveryNo
     *
     * @param v new value
     */
    public void setDeliveryNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryNo, v))
              {
            this.deliveryNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalTax()
    {
        return totalTax;
    }

                        
    /**
     * Set the value of TotalTax
     *
     * @param v new value
     */
    public void setTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalTax, v))
              {
            this.totalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShippingPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getShippingPrice()
    {
        return shippingPrice;
    }

                        
    /**
     * Set the value of ShippingPrice
     *
     * @param v new value
     */
    public void setShippingPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.shippingPrice, v))
              {
            this.shippingPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalExpense
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalExpense()
    {
        return totalExpense;
    }

                        
    /**
     * Set the value of TotalExpense
     *
     * @param v new value
     */
    public void setTotalExpense(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalExpense, v))
              {
            this.totalExpense = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalCost
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalCost()
    {
        return totalCost;
    }

                        
    /**
     * Set the value of TotalCost
     *
     * @param v new value
     */
    public void setTotalCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalCost, v))
              {
            this.totalCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesId
     *
     * @return String
     */
    public String getSalesId()
    {
        return salesId;
    }

                        
    /**
     * Set the value of SalesId
     *
     * @param v new value
     */
    public void setSalesId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesId, v))
              {
            this.salesId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashierName
     *
     * @return String
     */
    public String getCashierName()
    {
        return cashierName;
    }

                        
    /**
     * Set the value of CashierName
     *
     * @param v new value
     */
    public void setCashierName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashierName, v))
              {
            this.cashierName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrintTimes
     *
     * @return int
     */
    public int getPrintTimes()
    {
        return printTimes;
    }

                        
    /**
     * Set the value of PrintTimes
     *
     * @param v new value
     */
    public void setPrintTimes(int v) 
    {
    
                  if (this.printTimes != v)
              {
            this.printTimes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaidAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaidAmount()
    {
        return paidAmount;
    }

                        
    /**
     * Set the value of PaidAmount
     *
     * @param v new value
     */
    public void setPaidAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paidAmount, v))
              {
            this.paidAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaymentAmount()
    {
        return paymentAmount;
    }

                        
    /**
     * Set the value of PaymentAmount
     *
     * @param v new value
     */
    public void setPaymentAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAmount, v))
              {
            this.paymentAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ChangeAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getChangeAmount()
    {
        return changeAmount;
    }

                        
    /**
     * Set the value of ChangeAmount
     *
     * @param v new value
     */
    public void setChangeAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.changeAmount, v))
              {
            this.changeAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShipTo
     *
     * @return String
     */
    public String getShipTo()
    {
        return shipTo;
    }

                        
    /**
     * Set the value of ShipTo
     *
     * @param v new value
     */
    public void setShipTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shipTo, v))
              {
            this.shipTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FobId
     *
     * @return String
     */
    public String getFobId()
    {
        return fobId;
    }

                        
    /**
     * Set the value of FobId
     *
     * @param v new value
     */
    public void setFobId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fobId, v))
              {
            this.fobId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentDate
     *
     * @return Date
     */
    public Date getPaymentDate()
    {
        return paymentDate;
    }

                        
    /**
     * Set the value of PaymentDate
     *
     * @param v new value
     */
    public void setPaymentDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentDate, v))
              {
            this.paymentDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentStatus
     *
     * @return int
     */
    public int getPaymentStatus()
    {
        return paymentStatus;
    }

                        
    /**
     * Set the value of PaymentStatus
     *
     * @param v new value
     */
    public void setPaymentStatus(int v) 
    {
    
                  if (this.paymentStatus != v)
              {
            this.paymentStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveFreight
     *
     * @return boolean
     */
    public boolean getIsInclusiveFreight()
    {
        return isInclusiveFreight;
    }

                        
    /**
     * Set the value of IsInclusiveFreight
     *
     * @param v new value
     */
    public void setIsInclusiveFreight(boolean v) 
    {
    
                  if (this.isInclusiveFreight != v)
              {
            this.isInclusiveFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightAccountId
     *
     * @return String
     */
    public String getFreightAccountId()
    {
        return freightAccountId;
    }

                        
    /**
     * Set the value of FreightAccountId
     *
     * @param v new value
     */
    public void setFreightAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightAccountId, v))
              {
            this.freightAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesTransactionId
     *
     * @return String
     */
    public String getSalesTransactionId()
    {
        return salesTransactionId;
    }

                        
    /**
     * Set the value of SalesTransactionId
     *
     * @param v new value
     */
    public void setSalesTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesTransactionId, v))
              {
            this.salesTransactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalFee
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalFee()
    {
        return totalFee;
    }

                        
    /**
     * Set the value of TotalFee
     *
     * @param v new value
     */
    public void setTotalFee(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalFee, v))
              {
            this.totalFee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RoundingAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getRoundingAmount()
    {
        return roundingAmount;
    }

                        
    /**
     * Set the value of RoundingAmount
     *
     * @param v new value
     */
    public void setRoundingAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.roundingAmount, v))
              {
            this.roundingAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DoctorName
     *
     * @return String
     */
    public String getDoctorName()
    {
        return doctorName;
    }

                        
    /**
     * Set the value of DoctorName
     *
     * @param v new value
     */
    public void setDoctorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.doctorName, v))
              {
            this.doctorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientName
     *
     * @return String
     */
    public String getPatientName()
    {
        return patientName;
    }

                        
    /**
     * Set the value of PatientName
     *
     * @param v new value
     */
    public void setPatientName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientName, v))
              {
            this.patientName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientAge
     *
     * @return String
     */
    public String getPatientAge()
    {
        return patientAge;
    }

                        
    /**
     * Set the value of PatientAge
     *
     * @param v new value
     */
    public void setPatientAge(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientAge, v))
              {
            this.patientAge = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientPhone
     *
     * @return String
     */
    public String getPatientPhone()
    {
        return patientPhone;
    }

                        
    /**
     * Set the value of PatientPhone
     *
     * @param v new value
     */
    public void setPatientPhone(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientPhone, v))
              {
            this.patientPhone = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PatientAddress
     *
     * @return String
     */
    public String getPatientAddress()
    {
        return patientAddress;
    }

                        
    /**
     * Set the value of PatientAddress
     *
     * @param v new value
     */
    public void setPatientAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.patientAddress, v))
              {
            this.patientAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RegistrationId
     *
     * @return String
     */
    public String getRegistrationId()
    {
        return registrationId;
    }

                        
    /**
     * Set the value of RegistrationId
     *
     * @param v new value
     */
    public void setRegistrationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.registrationId, v))
              {
            this.registrationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InsuranceId
     *
     * @return String
     */
    public String getInsuranceId()
    {
        return insuranceId;
    }

                        
    /**
     * Set the value of InsuranceId
     *
     * @param v new value
     */
    public void setInsuranceId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.insuranceId, v))
              {
            this.insuranceId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PolicyNo
     *
     * @return String
     */
    public String getPolicyNo()
    {
        return policyNo;
    }

                        
    /**
     * Set the value of PolicyNo
     *
     * @param v new value
     */
    public void setPolicyNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.policyNo, v))
              {
            this.policyNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PolicyName
     *
     * @return String
     */
    public String getPolicyName()
    {
        return policyName;
    }

                        
    /**
     * Set the value of PolicyName
     *
     * @param v new value
     */
    public void setPolicyName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.policyName, v))
              {
            this.policyName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInsurance
     *
     * @return boolean
     */
    public boolean getIsInsurance()
    {
        return isInsurance;
    }

                        
    /**
     * Set the value of IsInsurance
     *
     * @param v new value
     */
    public void setIsInsurance(boolean v) 
    {
    
                  if (this.isInsurance != v)
              {
            this.isInsurance = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collMedOrderDetails
     */
    protected List collMedOrderDetails;

    /**
     * Temporary storage of collMedOrderDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initMedOrderDetails()
    {
        if (collMedOrderDetails == null)
        {
            collMedOrderDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a MedOrderDetail object to this object
     * through the MedOrderDetail foreign key attribute
     *
     * @param l MedOrderDetail
     * @throws TorqueException
     */
    public void addMedOrderDetail(MedOrderDetail l) throws TorqueException
    {
        getMedOrderDetails().add(l);
        l.setMedOrder((MedOrder) this);
    }

    /**
     * The criteria used to select the current contents of collMedOrderDetails
     */
    private Criteria lastMedOrderDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getMedOrderDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getMedOrderDetails() throws TorqueException
    {
              if (collMedOrderDetails == null)
        {
            collMedOrderDetails = getMedOrderDetails(new Criteria(10));
        }
        return collMedOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MedOrder has previously
     * been saved, it will retrieve related MedOrderDetails from storage.
     * If this MedOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getMedOrderDetails(Criteria criteria) throws TorqueException
    {
              if (collMedOrderDetails == null)
        {
            if (isNew())
            {
               collMedOrderDetails = new ArrayList();
            }
            else
            {
                        criteria.add(MedOrderDetailPeer.MED_ORDER_ID, getMedOrderId() );
                        collMedOrderDetails = MedOrderDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(MedOrderDetailPeer.MED_ORDER_ID, getMedOrderId());
                            if (!lastMedOrderDetailsCriteria.equals(criteria))
                {
                    collMedOrderDetails = MedOrderDetailPeer.doSelect(criteria);
                }
            }
        }
        lastMedOrderDetailsCriteria = criteria;

        return collMedOrderDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getMedOrderDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getMedOrderDetails(Connection con) throws TorqueException
    {
              if (collMedOrderDetails == null)
        {
            collMedOrderDetails = getMedOrderDetails(new Criteria(10), con);
        }
        return collMedOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MedOrder has previously
     * been saved, it will retrieve related MedOrderDetails from storage.
     * If this MedOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getMedOrderDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collMedOrderDetails == null)
        {
            if (isNew())
            {
               collMedOrderDetails = new ArrayList();
            }
            else
            {
                         criteria.add(MedOrderDetailPeer.MED_ORDER_ID, getMedOrderId());
                         collMedOrderDetails = MedOrderDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(MedOrderDetailPeer.MED_ORDER_ID, getMedOrderId());
                             if (!lastMedOrderDetailsCriteria.equals(criteria))
                 {
                     collMedOrderDetails = MedOrderDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastMedOrderDetailsCriteria = criteria;

         return collMedOrderDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this MedOrder is new, it will return
     * an empty collection; or if this MedOrder has previously
     * been saved, it will retrieve related MedOrderDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in MedOrder.
     */
    protected List getMedOrderDetailsJoinMedOrder(Criteria criteria)
        throws TorqueException
    {
                    if (collMedOrderDetails == null)
        {
            if (isNew())
            {
               collMedOrderDetails = new ArrayList();
            }
            else
            {
                              criteria.add(MedOrderDetailPeer.MED_ORDER_ID, getMedOrderId());
                              collMedOrderDetails = MedOrderDetailPeer.doSelectJoinMedOrder(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(MedOrderDetailPeer.MED_ORDER_ID, getMedOrderId());
                                    if (!lastMedOrderDetailsCriteria.equals(criteria))
            {
                collMedOrderDetails = MedOrderDetailPeer.doSelectJoinMedOrder(criteria);
            }
        }
        lastMedOrderDetailsCriteria = criteria;

        return collMedOrderDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("MedOrderId");
              fieldNames.add("LocationId");
              fieldNames.add("Status");
              fieldNames.add("OrderNo");
              fieldNames.add("InvoiceNo");
              fieldNames.add("DeliveryNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("DueDate");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("TotalQty");
              fieldNames.add("TotalAmount");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("TotalDiscount");
              fieldNames.add("TotalTax");
              fieldNames.add("CourierId");
              fieldNames.add("ShippingPrice");
              fieldNames.add("TotalExpense");
              fieldNames.add("TotalCost");
              fieldNames.add("SalesId");
              fieldNames.add("CashierName");
              fieldNames.add("Remark");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("PrintTimes");
              fieldNames.add("PaidAmount");
              fieldNames.add("PaymentAmount");
              fieldNames.add("ChangeAmount");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("ShipTo");
              fieldNames.add("FobId");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("PaymentDate");
              fieldNames.add("PaymentStatus");
              fieldNames.add("IsInclusiveFreight");
              fieldNames.add("FreightAccountId");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("SalesTransactionId");
              fieldNames.add("TotalFee");
              fieldNames.add("RoundingAmount");
              fieldNames.add("DoctorName");
              fieldNames.add("PatientName");
              fieldNames.add("PatientAge");
              fieldNames.add("PatientPhone");
              fieldNames.add("PatientAddress");
              fieldNames.add("RegistrationId");
              fieldNames.add("InsuranceId");
              fieldNames.add("PolicyNo");
              fieldNames.add("PolicyName");
              fieldNames.add("IsInsurance");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("MedOrderId"))
        {
                return getMedOrderId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("OrderNo"))
        {
                return getOrderNo();
            }
          if (name.equals("InvoiceNo"))
        {
                return getInvoiceNo();
            }
          if (name.equals("DeliveryNo"))
        {
                return getDeliveryNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("TotalTax"))
        {
                return getTotalTax();
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("ShippingPrice"))
        {
                return getShippingPrice();
            }
          if (name.equals("TotalExpense"))
        {
                return getTotalExpense();
            }
          if (name.equals("TotalCost"))
        {
                return getTotalCost();
            }
          if (name.equals("SalesId"))
        {
                return getSalesId();
            }
          if (name.equals("CashierName"))
        {
                return getCashierName();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("PrintTimes"))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals("PaidAmount"))
        {
                return getPaidAmount();
            }
          if (name.equals("PaymentAmount"))
        {
                return getPaymentAmount();
            }
          if (name.equals("ChangeAmount"))
        {
                return getChangeAmount();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("ShipTo"))
        {
                return getShipTo();
            }
          if (name.equals("FobId"))
        {
                return getFobId();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("PaymentDate"))
        {
                return getPaymentDate();
            }
          if (name.equals("PaymentStatus"))
        {
                return Integer.valueOf(getPaymentStatus());
            }
          if (name.equals("IsInclusiveFreight"))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals("FreightAccountId"))
        {
                return getFreightAccountId();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("SalesTransactionId"))
        {
                return getSalesTransactionId();
            }
          if (name.equals("TotalFee"))
        {
                return getTotalFee();
            }
          if (name.equals("RoundingAmount"))
        {
                return getRoundingAmount();
            }
          if (name.equals("DoctorName"))
        {
                return getDoctorName();
            }
          if (name.equals("PatientName"))
        {
                return getPatientName();
            }
          if (name.equals("PatientAge"))
        {
                return getPatientAge();
            }
          if (name.equals("PatientPhone"))
        {
                return getPatientPhone();
            }
          if (name.equals("PatientAddress"))
        {
                return getPatientAddress();
            }
          if (name.equals("RegistrationId"))
        {
                return getRegistrationId();
            }
          if (name.equals("InsuranceId"))
        {
                return getInsuranceId();
            }
          if (name.equals("PolicyNo"))
        {
                return getPolicyNo();
            }
          if (name.equals("PolicyName"))
        {
                return getPolicyName();
            }
          if (name.equals("IsInsurance"))
        {
                return Boolean.valueOf(getIsInsurance());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(MedOrderPeer.MED_ORDER_ID))
        {
                return getMedOrderId();
            }
          if (name.equals(MedOrderPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(MedOrderPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(MedOrderPeer.ORDER_NO))
        {
                return getOrderNo();
            }
          if (name.equals(MedOrderPeer.INVOICE_NO))
        {
                return getInvoiceNo();
            }
          if (name.equals(MedOrderPeer.DELIVERY_NO))
        {
                return getDeliveryNo();
            }
          if (name.equals(MedOrderPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(MedOrderPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(MedOrderPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(MedOrderPeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(MedOrderPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(MedOrderPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(MedOrderPeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(MedOrderPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(MedOrderPeer.TOTAL_TAX))
        {
                return getTotalTax();
            }
          if (name.equals(MedOrderPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(MedOrderPeer.SHIPPING_PRICE))
        {
                return getShippingPrice();
            }
          if (name.equals(MedOrderPeer.TOTAL_EXPENSE))
        {
                return getTotalExpense();
            }
          if (name.equals(MedOrderPeer.TOTAL_COST))
        {
                return getTotalCost();
            }
          if (name.equals(MedOrderPeer.SALES_ID))
        {
                return getSalesId();
            }
          if (name.equals(MedOrderPeer.CASHIER_NAME))
        {
                return getCashierName();
            }
          if (name.equals(MedOrderPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(MedOrderPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(MedOrderPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(MedOrderPeer.PRINT_TIMES))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals(MedOrderPeer.PAID_AMOUNT))
        {
                return getPaidAmount();
            }
          if (name.equals(MedOrderPeer.PAYMENT_AMOUNT))
        {
                return getPaymentAmount();
            }
          if (name.equals(MedOrderPeer.CHANGE_AMOUNT))
        {
                return getChangeAmount();
            }
          if (name.equals(MedOrderPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(MedOrderPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(MedOrderPeer.SHIP_TO))
        {
                return getShipTo();
            }
          if (name.equals(MedOrderPeer.FOB_ID))
        {
                return getFobId();
            }
          if (name.equals(MedOrderPeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(MedOrderPeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(MedOrderPeer.PAYMENT_DATE))
        {
                return getPaymentDate();
            }
          if (name.equals(MedOrderPeer.PAYMENT_STATUS))
        {
                return Integer.valueOf(getPaymentStatus());
            }
          if (name.equals(MedOrderPeer.IS_INCLUSIVE_FREIGHT))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals(MedOrderPeer.FREIGHT_ACCOUNT_ID))
        {
                return getFreightAccountId();
            }
          if (name.equals(MedOrderPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(MedOrderPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(MedOrderPeer.SALES_TRANSACTION_ID))
        {
                return getSalesTransactionId();
            }
          if (name.equals(MedOrderPeer.TOTAL_FEE))
        {
                return getTotalFee();
            }
          if (name.equals(MedOrderPeer.ROUNDING_AMOUNT))
        {
                return getRoundingAmount();
            }
          if (name.equals(MedOrderPeer.DOCTOR_NAME))
        {
                return getDoctorName();
            }
          if (name.equals(MedOrderPeer.PATIENT_NAME))
        {
                return getPatientName();
            }
          if (name.equals(MedOrderPeer.PATIENT_AGE))
        {
                return getPatientAge();
            }
          if (name.equals(MedOrderPeer.PATIENT_PHONE))
        {
                return getPatientPhone();
            }
          if (name.equals(MedOrderPeer.PATIENT_ADDRESS))
        {
                return getPatientAddress();
            }
          if (name.equals(MedOrderPeer.REGISTRATION_ID))
        {
                return getRegistrationId();
            }
          if (name.equals(MedOrderPeer.INSURANCE_ID))
        {
                return getInsuranceId();
            }
          if (name.equals(MedOrderPeer.POLICY_NO))
        {
                return getPolicyNo();
            }
          if (name.equals(MedOrderPeer.POLICY_NAME))
        {
                return getPolicyName();
            }
          if (name.equals(MedOrderPeer.IS_INSURANCE))
        {
                return Boolean.valueOf(getIsInsurance());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getMedOrderId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 3)
        {
                return getOrderNo();
            }
              if (pos == 4)
        {
                return getInvoiceNo();
            }
              if (pos == 5)
        {
                return getDeliveryNo();
            }
              if (pos == 6)
        {
                return getTransactionDate();
            }
              if (pos == 7)
        {
                return getDueDate();
            }
              if (pos == 8)
        {
                return getCustomerId();
            }
              if (pos == 9)
        {
                return getCustomerName();
            }
              if (pos == 10)
        {
                return getTotalQty();
            }
              if (pos == 11)
        {
                return getTotalAmount();
            }
              if (pos == 12)
        {
                return getTotalDiscountPct();
            }
              if (pos == 13)
        {
                return getTotalDiscount();
            }
              if (pos == 14)
        {
                return getTotalTax();
            }
              if (pos == 15)
        {
                return getCourierId();
            }
              if (pos == 16)
        {
                return getShippingPrice();
            }
              if (pos == 17)
        {
                return getTotalExpense();
            }
              if (pos == 18)
        {
                return getTotalCost();
            }
              if (pos == 19)
        {
                return getSalesId();
            }
              if (pos == 20)
        {
                return getCashierName();
            }
              if (pos == 21)
        {
                return getRemark();
            }
              if (pos == 22)
        {
                return getPaymentTypeId();
            }
              if (pos == 23)
        {
                return getPaymentTermId();
            }
              if (pos == 24)
        {
                return Integer.valueOf(getPrintTimes());
            }
              if (pos == 25)
        {
                return getPaidAmount();
            }
              if (pos == 26)
        {
                return getPaymentAmount();
            }
              if (pos == 27)
        {
                return getChangeAmount();
            }
              if (pos == 28)
        {
                return getCurrencyId();
            }
              if (pos == 29)
        {
                return getCurrencyRate();
            }
              if (pos == 30)
        {
                return getShipTo();
            }
              if (pos == 31)
        {
                return getFobId();
            }
              if (pos == 32)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 33)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 34)
        {
                return getPaymentDate();
            }
              if (pos == 35)
        {
                return Integer.valueOf(getPaymentStatus());
            }
              if (pos == 36)
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
              if (pos == 37)
        {
                return getFreightAccountId();
            }
              if (pos == 38)
        {
                return getCancelBy();
            }
              if (pos == 39)
        {
                return getCancelDate();
            }
              if (pos == 40)
        {
                return getSalesTransactionId();
            }
              if (pos == 41)
        {
                return getTotalFee();
            }
              if (pos == 42)
        {
                return getRoundingAmount();
            }
              if (pos == 43)
        {
                return getDoctorName();
            }
              if (pos == 44)
        {
                return getPatientName();
            }
              if (pos == 45)
        {
                return getPatientAge();
            }
              if (pos == 46)
        {
                return getPatientPhone();
            }
              if (pos == 47)
        {
                return getPatientAddress();
            }
              if (pos == 48)
        {
                return getRegistrationId();
            }
              if (pos == 49)
        {
                return getInsuranceId();
            }
              if (pos == 50)
        {
                return getPolicyNo();
            }
              if (pos == 51)
        {
                return getPolicyName();
            }
              if (pos == 52)
        {
                return Boolean.valueOf(getIsInsurance());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(MedOrderPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        MedOrderPeer.doInsert((MedOrder) this, con);
                        setNew(false);
                    }
                    else
                    {
                        MedOrderPeer.doUpdate((MedOrder) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collMedOrderDetails != null)
            {
                for (int i = 0; i < collMedOrderDetails.size(); i++)
                {
                    ((MedOrderDetail) collMedOrderDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key medOrderId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setMedOrderId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setMedOrderId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getMedOrderId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public MedOrder copy() throws TorqueException
    {
        return copyInto(new MedOrder());
    }
  
    protected MedOrder copyInto(MedOrder copyObj) throws TorqueException
    {
          copyObj.setMedOrderId(medOrderId);
          copyObj.setLocationId(locationId);
          copyObj.setStatus(status);
          copyObj.setOrderNo(orderNo);
          copyObj.setInvoiceNo(invoiceNo);
          copyObj.setDeliveryNo(deliveryNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setDueDate(dueDate);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setTotalQty(totalQty);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setTotalTax(totalTax);
          copyObj.setCourierId(courierId);
          copyObj.setShippingPrice(shippingPrice);
          copyObj.setTotalExpense(totalExpense);
          copyObj.setTotalCost(totalCost);
          copyObj.setSalesId(salesId);
          copyObj.setCashierName(cashierName);
          copyObj.setRemark(remark);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setPrintTimes(printTimes);
          copyObj.setPaidAmount(paidAmount);
          copyObj.setPaymentAmount(paymentAmount);
          copyObj.setChangeAmount(changeAmount);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setShipTo(shipTo);
          copyObj.setFobId(fobId);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setPaymentDate(paymentDate);
          copyObj.setPaymentStatus(paymentStatus);
          copyObj.setIsInclusiveFreight(isInclusiveFreight);
          copyObj.setFreightAccountId(freightAccountId);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setSalesTransactionId(salesTransactionId);
          copyObj.setTotalFee(totalFee);
          copyObj.setRoundingAmount(roundingAmount);
          copyObj.setDoctorName(doctorName);
          copyObj.setPatientName(patientName);
          copyObj.setPatientAge(patientAge);
          copyObj.setPatientPhone(patientPhone);
          copyObj.setPatientAddress(patientAddress);
          copyObj.setRegistrationId(registrationId);
          copyObj.setInsuranceId(insuranceId);
          copyObj.setPolicyNo(policyNo);
          copyObj.setPolicyName(policyName);
          copyObj.setIsInsurance(isInsurance);
  
                    copyObj.setMedOrderId((String)null);
                                                                                                                                                                                                                                                                                                                                    
                                      
                            
        List v = getMedOrderDetails();
        for (int i = 0; i < v.size(); i++)
        {
            MedOrderDetail obj = (MedOrderDetail) v.get(i);
            copyObj.addMedOrderDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public MedOrderPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("MedOrder\n");
        str.append("--------\n")
           .append("MedOrderId           : ")
           .append(getMedOrderId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("OrderNo              : ")
           .append(getOrderNo())
           .append("\n")
           .append("InvoiceNo            : ")
           .append(getInvoiceNo())
           .append("\n")
           .append("DeliveryNo           : ")
           .append(getDeliveryNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("TotalTax             : ")
           .append(getTotalTax())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("ShippingPrice        : ")
           .append(getShippingPrice())
           .append("\n")
           .append("TotalExpense         : ")
           .append(getTotalExpense())
           .append("\n")
           .append("TotalCost            : ")
           .append(getTotalCost())
           .append("\n")
           .append("SalesId              : ")
           .append(getSalesId())
           .append("\n")
           .append("CashierName          : ")
           .append(getCashierName())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("PrintTimes           : ")
           .append(getPrintTimes())
           .append("\n")
           .append("PaidAmount           : ")
           .append(getPaidAmount())
           .append("\n")
           .append("PaymentAmount        : ")
           .append(getPaymentAmount())
           .append("\n")
           .append("ChangeAmount         : ")
           .append(getChangeAmount())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("ShipTo               : ")
           .append(getShipTo())
           .append("\n")
           .append("FobId                : ")
           .append(getFobId())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("PaymentDate          : ")
           .append(getPaymentDate())
           .append("\n")
           .append("PaymentStatus        : ")
           .append(getPaymentStatus())
           .append("\n")
           .append("IsInclusiveFreight   : ")
           .append(getIsInclusiveFreight())
           .append("\n")
           .append("FreightAccountId     : ")
           .append(getFreightAccountId())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("SalesTransactionId   : ")
           .append(getSalesTransactionId())
           .append("\n")
           .append("TotalFee             : ")
           .append(getTotalFee())
           .append("\n")
           .append("RoundingAmount       : ")
           .append(getRoundingAmount())
           .append("\n")
           .append("DoctorName           : ")
           .append(getDoctorName())
           .append("\n")
           .append("PatientName          : ")
           .append(getPatientName())
           .append("\n")
           .append("PatientAge           : ")
           .append(getPatientAge())
           .append("\n")
           .append("PatientPhone         : ")
           .append(getPatientPhone())
           .append("\n")
           .append("PatientAddress       : ")
           .append(getPatientAddress())
           .append("\n")
           .append("RegistrationId       : ")
           .append(getRegistrationId())
           .append("\n")
           .append("InsuranceId          : ")
           .append(getInsuranceId())
           .append("\n")
           .append("PolicyNo             : ")
           .append(getPolicyNo())
           .append("\n")
           .append("PolicyName           : ")
           .append(getPolicyName())
           .append("\n")
           .append("IsInsurance          : ")
           .append(getIsInsurance())
           .append("\n")
        ;
        return(str.toString());
    }
}

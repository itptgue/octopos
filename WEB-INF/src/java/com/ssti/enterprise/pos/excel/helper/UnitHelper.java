package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class UnitHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = UnitTool.getAllUnit();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Unit Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        
		createHeaderCell(oWB, oRow, (short)iCol,"Unit Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Base Value"); iCol++;				
		createHeaderCell(oWB, oRow, (short)iCol,"Convertion"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol,"Base Unit"); iCol++;
	
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Unit oUnit = (Unit) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oUnit.getUnitCode());   iCol++; 
                createCell(oWB, oRow2, (short)iCol, oUnit.getDescription());iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oUnit.getBaseUnit()));	iCol++;
                createCell(oWB, oRow2, (short)iCol, oUnit.getValueToBase());	iCol++;
                createCell(oWB, oRow2, (short)iCol, oUnit.getAlternateBase());	iCol++;
            }        
        }
        return oWB;
    }
}

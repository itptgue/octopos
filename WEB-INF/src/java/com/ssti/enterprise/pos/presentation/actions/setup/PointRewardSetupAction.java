package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PointReward;
import com.ssti.enterprise.pos.om.PointRewardPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PointRewardTool;

public class PointRewardSetupAction extends SetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			PointReward oPointReward = new PointReward();
			setProperties (oPointReward, data);
			oPointReward.setPointRewardId (IDGenerator.generateSysID());
	        oPointReward.save();
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch(Exception _oEx)
        {
	       	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			PointReward oPointReward = PointRewardTool.getPointRewardByID(data.getParameters().getString("ID"));
			setProperties (oPointReward, data);
	        oPointReward.save();
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");	
	    	Criteria oCrit = new Criteria();
	        oCrit.add(PointRewardPeer.POINT_REWARD_ID, sID);
	        PointRewardPeer.doDelete(oCrit);
        	data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

    private void setProperties (PointReward _oPointReward, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oPointReward);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.EmployeeLoader;
import com.ssti.enterprise.pos.manager.EmployeeManager;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.EmployeePeer;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.presentation.SecureAction;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class EmployeeFormAction extends SecureAction
{
	private static Log log = LogFactory.getLog(EmployeeFormAction.class);
	
	private static final String[] a_PERM = {"Setup Master Data", "Update Employee Data", "View Employee Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }

    public void doPerform(RunData data, Context context)
    {
    }

	public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			int iCondition = data.getParameters().getInt ("Condition");
			String sKeyword = data.getParameters().getString ("Keywords");
			String sLocID = data.getParameters().getString ("LocationId");
			String sAreaID = data.getParameters().getString("SalesAreaId","");
			String sTerrID = data.getParameters().getString("TerritoryId","");
			String sBossID = data.getParameters().getString("BossId","");	
			boolean bNonAct = data.getParameters().getBoolean("NonActive");
			context.put ("Employees", EmployeeTool.findData (iCondition, sKeyword, sLocID,sAreaID,sTerrID,sBossID,bNonAct));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		Employee oEmployee = null;
		try
		{
			oEmployee = new Employee();
			setProperties ( oEmployee, data);
			
			oEmployee.setUserName (data.getParameters().getString("UserName"));
			oEmployee.setJobTitle (data.getParameters().getString("JobTitle"));
			oEmployee.setEmployeeId (IDGenerator.generateSysID());
	        oEmployee.setAddDate(new Date());
	        oEmployee.setUpdateDate(new Date());
			oEmployee.save();
	        
			EmployeeTool.updateParentHasChild(oEmployee.getParentId());			
	        EmployeeTool.createUserFromEmployee(oEmployee);
	        
	        EmployeeManager.getInstance().refreshCache(oEmployee);
	        data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	context.put("Employee", oEmployee);
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

 	public void doUpdate(RunData data, Context context)
        throws Exception
    {
    	try
    	{
	    	Employee oEmployee = EmployeeTool.getEmployeeByID(data.getParameters().getString("ID"));
        	if (oEmployee != null)
        	{
        		Employee oOld = oEmployee.copy();
        		
		    	setProperties (oEmployee, data);
	        	oEmployee.setUpdateDate(new Date());
        		oEmployee.setIsActive(true);
	        	oEmployee.save();
	        	
	        	UpdateHistoryTool.createHistory(oOld, oEmployee, data.getUser().getName(), null);
	        	
	        	boolean bNonActive = data.getParameters().getBoolean("NonActive");	        	
	        	if(bNonActive)
	        	{
	        		oEmployee.setIsActive(false);
	        		oEmployee.save();
	        		EmployeeTool.deleteUser(oEmployee);
	        	}
	        	else
	        	{
	        		EmployeeTool.updateParentHasChild(oEmployee.getParentId());
	        		EmployeeTool.createUserFromEmployee(oEmployee);
	        	}
		        EmployeeManager.getInstance().refreshCache(oEmployee);        	
	        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        	}
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }


 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("id");
    		if(SqlUtil.validateFKRef("customer","default_employee_id",sID))
    		{	
    			Employee oEmp = EmployeeTool.getEmployeeByID(sID);
    			
    			Criteria oCrit = new Criteria();
	        	oCrit.add(EmployeePeer.EMPLOYEE_ID, sID);
	        	EmployeePeer.doDelete(oCrit);
        		
	        	UpdateHistoryTool.createDelHistory(oEmp, data.getUser().getName(), null);
	        	data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
	        	EmployeeManager.getInstance().refreshCache(sID);
        	}
        	else
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

	private void setProperties (Employee _oEmployee, RunData data)
		throws Exception
    {
    	try
    	{
    		data.getParameters().setProperties(_oEmployee);
    		_oEmployee.setSalesCommisionPct ((data.getParameters().getBigDecimal("SalesCommisionPct")));
			_oEmployee.setMarginCommisionPct((data.getParameters().getBigDecimal("MarginCommisionPct")));
	    	_oEmployee.setStartDate   (CustomParser.parseDate (data.getParameters().getString ("StartDate","")));
			_oEmployee.setQuitDate    (CustomParser.parseDate (data.getParameters().getString ("QuitDate","")));
			_oEmployee.setBirthDate   (CustomParser.parseDate (data.getParameters().getString ("BirthDate","")));
	        _oEmployee.setEmployeeLevel(EmployeeTool.setChildLevel(_oEmployee.getParentId()));
	        _oEmployee.setIsSalesman  (data.getParameters().getBoolean("IsSalesman",false));
			if(_oEmployee.getQuitDate() != null)
			{
				_oEmployee.setIsActive(false);
				//TODO: validate child
			}
    	}
    	catch (Exception _oEx)
    	{
    		log.error(_oEx);
    		_oEx.printStackTrace();
    		throw new Exception(LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
	
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			EmployeeLoader oLoader = new EmployeeLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}   
}
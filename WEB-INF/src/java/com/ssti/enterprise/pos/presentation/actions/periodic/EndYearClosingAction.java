package com.ssti.enterprise.pos.presentation.actions.periodic;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.actions.setup.GLSecureAction;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PeriodTool;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-08-14
 * -fix parameter location & first jan passing to PeriodTool.closeYear
 * </pre><br>
 */
public class EndYearClosingAction extends GLSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	//-------------------------------------------------------------------------
	// PERIOD CLOSING
	//-------------------------------------------------------------------------
	public void doClose(RunData data, Context context)
	    throws Exception
	{		
		try
		{
			int iYear = data.getParameters().getInt("year");
			boolean bLocation = data.getParameters().getBoolean("perLocation");
			boolean bFirstJan = data.getParameters().getBoolean("firstJan");
			PeriodTool.closeYear(iYear, data.getUser().getName(), bLocation, bFirstJan);
			data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
		}
		catch (Exception _oEx)
		{
			data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
		}
	}    
	
	public void doReopen(RunData data, Context context)
	    throws Exception
	{
		try
		{
			int iYear = data.getParameters().getInt("year");
			PeriodTool.reopenYear(iYear, data.getUser().getName());
	    	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
		}
	    catch (Exception _oEx)
	    {
	    	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	    }
	}    		
}

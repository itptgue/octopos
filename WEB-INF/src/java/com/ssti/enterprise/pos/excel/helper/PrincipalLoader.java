package com.ssti.enterprise.pos.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Principal;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.StringUtil;

public class PrincipalLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( PrincipalLoader.class );
	
	private static final String s_START_HEADING = "Principal Code";	
	
	public static String[] m_aHeaders =  {       
       "Principal Code",
       "Principal Name"
    };
	
	public PrincipalLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	m_bValidate = true;    	
    	sHeading = s_START_HEADING;
	}

    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {		
    	StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCode = getString(_oRow, _iIdx + i); i++;
			String sName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sCode))
			{	
				Principal oData = ItemFieldTool.getPrincipalByCode(sCode);
				Principal oOld = null;
				
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Principal Code : " + sCode);
				
				if (oData == null) 
				{	
					log.debug ("Code Not Exist, create new Code");

					oData = new Principal();
					oData.setPrincipalId (IDGenerator.generateSysID());
					oData.setPrincipalCode(sCode); 					
					bNew = true;
				}
				else 
				{
					bNew = false;
					oOld = oData.copy();
				}
				oData.setPrincipalName(sName); 
				bValid = validateString("Principal Name", oData.getPrincipalName(), 100, oEmpty, oLength);				
                oData.save();
                
                if(oOld != null)
                {
                	UpdateHistoryTool.createHistory(oOld, oData, m_sUserName, null);
                }
                if(bValid)
                {
                	if (bNew) 
                	{	
                		m_iNewData++;
                		m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
                		m_oNewData.append (StringUtil.left(sCode,30));
                		m_oNewData.append (StringUtil.left(sName,100));
                		m_oNewData.append ("\n");
                	}
                	else 
                	{
                		m_iUpdated++;
                		m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
                		m_oUpdated.append (StringUtil.left(sCode,30));
                		m_oUpdated.append (StringUtil.left(sName,100));
                		m_oUpdated.append ("\n");
                	}
                	ItemManager.getInstance().refreshPrincipal(oData.getId());
                }
                else
                {
                	m_iRejected++;
                	m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
                	m_oRejected.append (StringUtil.left(sCode,30));
                	m_oRejected.append (StringUtil.left(sName,50));
                	if (oEmpty.length() > s_EMPTY_REJECT.length())
                	{				    
                		m_oRejected.append (oEmpty);
                	}
                	if (oLength.length() > s_LENGTH_REJECT.length())
                	{
                		m_oRejected.append ("; ");				    
                		m_oRejected.append (oLength);
                	}
                	m_oRejected.append ("\n");
                }
			}
		}    
    }
}

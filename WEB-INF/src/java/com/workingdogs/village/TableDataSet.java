package com.workingdogs.village;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * This class is used for doing select/insert/delete/update on the database. A TableDataSet cannot be used to join multiple tables
 * for an update, if you need join functionality on a select, you should use a <a href="QueryDataSet.html">QueryDataSet</a>.
 *
 * <P>
 * Here is an example usage for this code that gets the first 10 records where column "a" = 1:
 * <PRE>
 *  KeyDef kd = new KeyDef().setAttrib("column");
 *  TableDataSet tds = new TableDataSet(connection, "table_name", kd );
 *  tds.where ("a=1" ); // WHERE a = 1
 *  tds.fetchRecords(10); // fetch first 10 records where column a=1
 *  for ( int i=0;i< tds.size(); i++ )
 *  {
 *  Record rec = tds.getRecord(i); // zero based
 *  String columnA = rec.getValue("a");
 *  if ( columnA.equals ("1") )
 *  System.out.print ("We got a column!");
 *  }
 *  tds.close();
 *  </PRE>
 * </p>
 *
 * <P>
 * It is important to remember to always close() the TableDataSet when you are finished with it.
 * </p>
 *
 * <P>
 * As you can see, using a TableDataSet makes doing selects from the database trivial. You do not need to write any SQL and it
 * makes it easy to cache a TableDataSet for future use within your application.
 * </p>
 *
 * @author <a href="mailto:jon@latchkey.com">Jon S. Stevens</a>
 * @version $Revision: 1.1 $
 */
public class TableDataSet
        extends DataSet
{
    /** the optimistic locking column value */
    private String optimisticLockingCol;

    /** the value for the sql where clause */
    private String where = null;

    /** the value for the sql order by clause */
    private String order = null;

    /** the value for the sql other clause */
    private String other = null;

    // by default this is false;

    /** TODO: DOCUMENT ME! */
    private boolean refreshOnSave = false;

    /**
     * Default constructor.
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public TableDataSet()
            throws SQLException, DataSetException
    {
        super();
    }

    /**
     * Creates a new TableDataSet object.
     *
     * @param conn TODO: DOCUMENT ME!
     * @param tableName TODO: DOCUMENT ME!
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet(Connection conn, String tableName)
            throws SQLException, DataSetException
    {
        super(conn, tableName);
    }

    /**
     * Creates a new TableDataSet object.
     *
     * @param conn TODO: DOCUMENT ME!
     * @param schema TODO: DOCUMENT ME!
     * @param keydef TODO: DOCUMENT ME!
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet(Connection conn, Schema schema, KeyDef keydef)
            throws SQLException, DataSetException
    {
        super(conn, schema, keydef);
    }

    /**
     * Creates a new TableDataSet object.
     *
     * @param conn TODO: DOCUMENT ME!
     * @param tableName TODO: DOCUMENT ME!
     * @param keydef TODO: DOCUMENT ME!
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet(Connection conn, String tableName, KeyDef keydef)
            throws SQLException, DataSetException
    {
        super(conn, tableName, keydef);
    }

    /**
     * Creates a new TableDataSet object.
     *
     * @param conn TODO: DOCUMENT ME!
     * @param tableName TODO: DOCUMENT ME!
     * @param columns TODO: DOCUMENT ME!
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet(Connection conn, String tableName, String columns)
            throws SQLException, DataSetException
    {
        super(conn, tableName, columns);
    }

    /**
     * Creates a new TableDataSet object.
     *
     * @param conn TODO: DOCUMENT ME!
     * @param tableName TODO: DOCUMENT ME!
     * @param columns TODO: DOCUMENT ME!
     * @param keydef TODO: DOCUMENT ME!
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet(Connection conn, String tableName, String columns, KeyDef keydef)
            throws SQLException, DataSetException
    {
        super(conn, tableName, columns, keydef);
    }

    /**
     * Use the TDS fetchRecords instead of the DataSet.fetchRecords
     *
     * @return an instance of myself
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public DataSet fetchRecords()
            throws SQLException, DataSetException
    {
        return fetchRecords(-1);
    }

    /**
     * Use the TDS fetchRecords instead of the DataSet.fetchRecords
     *
     * @param max
     *
     * @return an instance of myself
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public DataSet fetchRecords(int max)
            throws SQLException, DataSetException
    {
        return fetchRecords(0, max);
    }

    /**
     * Fetch start to max records. start is at Record 0
     *
     * @param start
     * @param max
     *
     * @return an instance of myself
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public DataSet fetchRecords(int start, int max)
            throws SQLException, DataSetException
    {
        buildSelectString();

        return super.fetchRecords(start, max);
    }

    /**
     * this is a string that contains the columns for the table that this TableDataSet represents.
     *
     * @return columns separated by ","
     */
    public String attributes()
    {
        return super.getColumns();
    }

    /**
     * Returns the KeyDef for the DataSet
     *
     * @return a keydef
     */
    public KeyDef keydef()
    {
        return super.keydef();
    }

    /**
     * Returns the ResultSet for the DataSet
     *
     * @return a ResultSet
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public ResultSet resultSet()
            throws SQLException, DataSetException
    {
        return super.resultSet();
    }

    /**
     * Returns the Schema for the DataSet
     *
     * @return a Schema
     */
    public Schema schema()
    {
        return super.schema();
    }

    /**
     * Saves all the records in the DataSet.
     *
     * @return total number of records updated/inserted/deleted
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public int save()
            throws SQLException, DataSetException
    {
        return save(connection(), false);
    }

    /**
     * Saves all the records in the DataSet with the intransaction boolean value.
     *
     * @param intransaction TODO: DOCUMENT ME!
     *
     * @return total number of records updated/inserted/deleted
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public int save(boolean intransaction)
            throws SQLException, DataSetException
    {
        return save(connection(), intransaction);
    }

    /**
     * Saves all the records in the DataSet with the given connection and intransaction boolean value.
     *
     * @param conn TODO: DOCUMENT ME!
     * @param intransaction TODO: DOCUMENT ME!
     *
     * @return total number of records updated/inserted/deleted
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public int save(Connection conn, boolean intransaction)
            throws SQLException, DataSetException
    {
        int j = 0;

        for (Enumeration e = records.elements(); e.hasMoreElements();)
        {
            Record rec = (Record) e.nextElement();
            rec.save(conn);
            j++;
        }

        // now go through and remove any records
        // that were previously marked as a zombie by the
        // delete process
        removeDeletedRecords();

        return j;
    }

    /**
     * Not yet implemented
     *
     * @param conn TODO: DOCUMENT ME!
     *
     * @return TODO: DOCUMENT ME!
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public int saveWithoutStatusUpdate(Connection conn)
            throws SQLException, DataSetException
    {
        throw new DataSetException("Not yet implemented!");
    }

    /**
     * Hell if I know what this does.
     *
     * @return TODO: DOCUMENT ME!
     */
    public String debugInfo()
    {
        return "Not yet implemented!";
    }

    /**
     * Removes any records that are marked as a zombie.
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public void removeDeletedRecords()
            throws DataSetException
    {
        for (Enumeration e = records.elements(); e.hasMoreElements();)
        {
            Record rec = (Record) e.nextElement();

            if (rec.isAZombie())
            {
                removeRecord(rec);
            }
        }
    }

    /**
     * Sets the table column used for optomistic locking.
     *
     * @param olc TODO: DOCUMENT ME!
     */
    public void setOptimisticLockingColumn(String olc)
    {
        this.optimisticLockingCol = olc;
    }

    /**
     * Gets the table column used for optomistic locking.
     *
     * @return string
     */
    public String optimisticLockingCol()
    {
        return this.optimisticLockingCol;
    }

    /**
     * Sets the value for the SQL portion of the WHERE statement
     *
     * @param where TODO: DOCUMENT ME!
     *
     * @return instance of self
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet where(String where)
            throws DataSetException
    {
        if (where == null)
        {
            throw new DataSetException("null not allowed for where clause");
        }

        this.where = where;

        return this;
    }

    /**
     * Gets the value of the SQL portion of WHERE.
     *
     * @return string
     */
    String getWhere()
    {
        return this.where;
    }

    /**
     * Sets the value for the SQL portion of the ORDER statement
     *
     * @param order TODO: DOCUMENT ME!
     *
     * @return instance of self
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet order(String order)
            throws DataSetException
    {
        if (order == null)
        {
            throw new DataSetException("null not allowed for order clause");
        }

        this.order = order;

        return this;
    }

    /**
     * Gets the value of the SQL portion of ORDER.
     *
     * @return string
     */
    String getOrder()
    {
        return this.order;
    }

    /**
     * Sets the value for the SQL portion of the OTHER statement
     *
     * @param other TODO: DOCUMENT ME!
     *
     * @return instance of self
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public TableDataSet other(String other)
            throws DataSetException
    {
        if (other == null)
        {
            throw new DataSetException("null not allowed for other clause");
        }

        this.other = other;

        return this;
    }

    /**
     * Gets the value of the SQL portion of OTHER.
     *
     * @return string
     */
    String getOther()
    {
        return this.other;
    }

    /**
     * This method refreshes all of the Records stored in this TableDataSet.
     *
     * @param conn TODO: DOCUMENT ME!
     *
     * @throws SQLException TODO: DOCUMENT ME!
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public void refresh(Connection conn)
            throws SQLException, DataSetException
    {
        for (Enumeration e = records.elements(); e.hasMoreElements();)
        {
            Record rec = (Record) e.nextElement();
            rec.refresh(conn);
        }
    }

    /**
     * Setting this causes each Record to refresh itself when a save() is performed on it.
     *
     * <P>
     * Default value is false.
     * </p>
     *
     * @param val TODO: DOCUMENT ME!
     */
    public void setRefreshOnSave(boolean val)
    {
        this.refreshOnSave = val;
    }

    /**
     * Setting this causes each Record to refresh itself when a save() is performed on it.
     *
     * <P>
     * Default value is false.
     * </p>
     *
     * @return true if it is on; false otherwise
     */
    public boolean refreshOnSave()
    {
        return this.refreshOnSave;
    }

    /**
     * This sets additional SQL for the table name. The string appears after the table name. Sybase users would set this to
     * "HOLDLOCK" to get repeatable reads.
     *
     * <P>
     * FIXME: Is this right? I don't use Sybase.
     * </p>
     *
     * @param tq TODO: DOCUMENT ME!
     *
     * @return an instance of self
     */
    public TableDataSet tableQualifier(String tq)
    {
        // go directly to schema() cause it is where tableName is stored
        schema().appendTableName(tq);

        return this;
    }

    /**
     * The name of the table for which this TableDataSet was created.
     *
     * @return string
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public String tableName()
            throws DataSetException
    {
        return super.tableName();
    }

    /**
     * Not yet implemented
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public void updateStatus()
            throws SQLException, DataSetException
    {
        throw new DataSetException("Not yet implemented!");
    }

    /**
     * Builds the select string that was used to populate this TableDataSet.
     *
     * @return SQL select string
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public String getSelectString()
            throws DataSetException
    {
        buildSelectString();

        return this.selectString.toString();
    }

    /**
     * Used by getSelectString to build the select string that was used to populate this TableDataSet.
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    private void buildSelectString()
            throws DataSetException
    {
        if (selectString == null)
        {
            selectString = new StringBuffer(256);
        }
        else
        {
            selectString.setLength(0);
        }

        selectString.append("SELECT ");
        selectString.append(schema().attributes());
        selectString.append(" FROM ");
        selectString.append(schema().tableName());

        if ((this.where != null) && (this.where.length() > 0))
        {
            selectString.append(" WHERE " + this.where);
        }

        if ((this.order != null) && (this.order.length() > 0))
        {
            selectString.append(" ORDER BY " + this.order);
        }

        if ((this.other != null) && (this.other.length() > 0))
        {
            selectString.append(this.other);
        }
    }
}

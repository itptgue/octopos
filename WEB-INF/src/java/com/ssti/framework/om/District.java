
package com.ssti.framework.om;


import java.util.ArrayList;
import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class District
    extends com.ssti.framework.om.BaseDistrict
    implements Persistent
{
	static List empty = new ArrayList(1);
	
	public String getId() {
		return getDistrictId();
	}

	public String getCode() {
		return getDistrict();
	}

	public String getName() {
		return getDistrict();
	}
	
	public List getVillages()
	{
		if(StringUtil.isNotEmpty(getDistrictId()))
		{
			try {
				return AddressTool.getInstance().getVillage(getDistrictId());	
			} catch (Exception e) {
			}
		}
		return empty;
	}
}

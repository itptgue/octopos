package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import com.ssti.enterprise.pos.manager.UnitManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.framework.tools.SortingTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: UnitTool.java,v 1.31 2008/03/03 03:13:10 albert Exp $ <br>
 *
 * <pre>
 * $Log: UnitTool.java,v $
 * Revision 1.31  2008/03/03 03:13:10  albert
 * *** empty log message ***
 *
 * Revision 1.30  2008/03/03 02:07:25  albert
 * *** empty log message ***
 *
 * Revision 1.29  2007/11/09 02:08:54  albert
 * *** empty log message ***
 *
 * Revision 1.28  2007/11/09 01:59:40  albert
 * *** empty log message ***
 *
 * Revision 1.27  2007/07/10 04:51:48  albert
 * *** empty log message ***
 *
 * Revision 1.26  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class UnitTool extends BaseTool 
{
	public static List getAllUnit()
		throws Exception
	{
		return UnitManager.getInstance().getAllUnit();
	}
	
	public static List getAllUnit(int _iOrderBy)
		throws Exception
	{
		List vUnit = getAllUnit();
	    if(_iOrderBy == 1)
	    {
	    	vUnit = SortingTool.sort(vUnit, "baseUnit");
	    }
	    else if(_iOrderBy == 2)
	    {
	    	vUnit = SortingTool.sort(vUnit, "unitCode");
	    }
   	    else if(_iOrderBy == 3)
	    {
   	    	vUnit = SortingTool.sort(vUnit, "description");
	    }
		return vUnit;
	}
	
	public static List getAllBaseUnit()
		throws Exception
	{
		return UnitManager.getInstance().getAllBaseUnit();
	}

	public static List getAllNonBaseUnit()
		throws Exception
	{
		return UnitManager.getInstance().getAllNonBaseUnit();
	}
	
	public static Unit getUnitByID(String _sID)
    	throws Exception
    {
		return UnitManager.getInstance().getUnitByID(_sID, null);
	}

	public static Unit getUnitByID(String _sID, Connection _oConn)
		throws Exception
	{
		return UnitManager.getInstance().getUnitByID(_sID, _oConn);
	}

	public static Unit getUnitByCode(String _sCode)
		throws Exception
	{
		return UnitManager.getInstance().getUnitByCode(_sCode, null);
	}
	
	public static Unit getAlternateUnitByID(String _sID)
    	throws Exception
    {
		Unit oUnit = getUnitByID(_sID);
		if(oUnit != null)
		{
		    if (!oUnit.getBaseUnit())
		    {
		        Unit oBaseUnit = getUnitByID(oUnit.getAlternateBase());
		        return oBaseUnit;
		    }
		    else 
		    {
		        return oUnit;		    
		    }
		}
		else
		{
		    return null;
		}	
	}
	
	public static String getCodeByID(String _sID)
    	throws Exception
    {
		Unit oUnit = getUnitByID (_sID);		
        if (oUnit != null) {
        	return oUnit.getUnitCode();
		}
		return "";
	}
	
	public static String getDescriptionByID(String _sID)
		throws Exception
	{
		Unit oUnit = getUnitByID (_sID);		
	    if (oUnit != null) {
	    	return oUnit.getDescription();
		}
		return "";
	}

	public static String getIDByCode(String _sCode)
    	throws Exception
    {
		Unit oUnit = UnitManager.getInstance().getUnitByCode(_sCode, null);
        if (oUnit != null) 
        {
        	return oUnit.getUnitId();
		}
		return "";
	}	

	
	/**
	 * @@deprecated
	 */
	public static double getBaseValue(String _sUnitID, Connection _oConn)
		throws Exception
	{
		return getBaseValue(null, _sUnitID, _oConn);
	}
	
	public static double getBaseValue(String _sItemID, String _sUnitID)
		throws Exception
	{
		return getBaseValue(_sItemID, _sUnitID, null);
	}
	
	public static double getBaseValue(String _sItemID, String _sUnitID, Connection _oConn)
    	throws Exception
    {
		Item oItem = ItemTool.getItemByID(_sItemID, _oConn);
		return getBaseValue(oItem, _sUnitID);
	}
	
	public static double getBaseValue(Item _oItem, String _sUnitID)
		throws Exception
	{
		if (_oItem != null && StringUtil.isEqual(_oItem.getPurchaseUnitId(),_sUnitID))
		{
			return _oItem.getUnitConversion().doubleValue();
		}
		/*
		Unit oUnit = getUnitByID (_sUnitID, _oConn);	
		if (oUnit != null)
		{	
	   		return oUnit.getValueToBase().doubleValue();
		}*/
		return 1;
	}
	
	/**
	 * @@deprecated
	 */
	public static BigDecimal getBaseQty(String _sUnitID, BigDecimal _dQty)
		throws Exception
	{
		return getBaseQty(null, _sUnitID, _dQty);
	}
	
	public static BigDecimal getBaseQty(String _sItemID, String _sUnitID, BigDecimal _dQty)
		throws Exception
	{
		return new BigDecimal (getBaseQty(_sItemID, _sUnitID, _dQty.doubleValue()));
	}	

	public static double getBaseQty(String _sItemID, String _sUnitID, double _dQty)
		throws Exception
	{
		double dBaseValue = getBaseValue(_sItemID,_sUnitID);
	   	return (_dQty * dBaseValue);
	}
	/*
	public static double convertQtyToBaseUnit(String _sItemID, String _sUnitID, double _dQty, Connection _oConn)
		throws Exception
	{
		if(!isBaseUnit(_sUnitID, _oConn))
		{
			Unit oUnit = getUnitByID (_sUnitID, _oConn);
			double dNewQty = _dQty * oUnit.getValueToBase().doubleValue();
			return dNewQty;
		}
		else
		{
		 	return _dQty;
		}
	}*/
	
	/*
	public static BigDecimal getBaseQty(String _sUnitID, BigDecimal _dQty)
		throws Exception
	{
		Unit oUnit = getUnitByID (_sUnitID);	
		if (oUnit != null)
		{	
			double dQty = 0;
			if (_dQty != null) dQty = _dQty.doubleValue();
	   		return new BigDecimal (oUnit.getValueToBase().doubleValue() * dQty);
		}
		return _dQty;
	}	
	
	public static double convertQtyToBaseUnit(double _dQty, String _sUnitID, Connection _oConn)
		throws Exception
	{
		if(!isBaseUnit(_sUnitID, _oConn))
		{
			Unit oUnit = getUnitByID (_sUnitID, _oConn);
			double dNewQty = _dQty * oUnit.getValueToBase().doubleValue();
			return dNewQty;
		}
		else
		{
		 	return _dQty;
		}
	}*/

	public static boolean isBaseUnit(String _sUnitID)
		throws Exception
	{
		return isBaseUnit(_sUnitID, null);
	}
	
	public static boolean isBaseUnit(String _sUnitID, Connection _oConn)
		throws Exception
	{
		Unit oUnit = getUnitByID (_sUnitID, _oConn);		
		if (oUnit != null)
		{
			return oUnit.getBaseUnit();
		}
		log.warn("Unit " + _sUnitID + " Not found ");
		return false;
	}
	
	//select box
	public static String createJSArray ()
		throws Exception
	{
		return createJSArray (-1);
	}
	
	public static String createJSArray (int _iOrderBy)
		throws Exception
	{	
	 	List vData = getAllUnit(_iOrderBy);

	 	StringBuilder oSB = new StringBuilder ();
		oSB.append ("var aOF = new Array (");
		oSB.append (vData.size());
		oSB.append (");\n");
	
	 	for (int i = 0; i < vData.size(); i++)
	 	{
		 	Unit oField = (Unit) vData.get(i);
			oSB.append ("aOF[");
			oSB.append (i);
			oSB.append ("] = new Array ('");
			oSB.append (oField.getValueToBase());
			oSB.append ("'); //").append(oField.getUnitCode()).append("\n");
	 	}
	 	return oSB.toString();
	}
}

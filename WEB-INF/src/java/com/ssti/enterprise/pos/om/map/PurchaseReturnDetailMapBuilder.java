package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseReturnDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseReturnDetailMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseReturnDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_return_detail";
    }

  
    /**
     * purchase_return_detail.PURCHASE_RETURN_DETAIL_ID
     * @return the column name for the PURCHASE_RETURN_DETAIL_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.PURCHASE_RETURN_DETAIL_ID constant
     */
    public static String getPurchaseReturnDetail_PurchaseReturnDetailId()
    {
        return "purchase_return_detail.PURCHASE_RETURN_DETAIL_ID";
    }
  
    /**
     * purchase_return_detail.PURCHASE_RETURN_ID
     * @return the column name for the PURCHASE_RETURN_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.PURCHASE_RETURN_ID constant
     */
    public static String getPurchaseReturnDetail_PurchaseReturnId()
    {
        return "purchase_return_detail.PURCHASE_RETURN_ID";
    }
  
    /**
     * purchase_return_detail.TRANSACTION_DETAIL_ID
     * @return the column name for the TRANSACTION_DETAIL_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.TRANSACTION_DETAIL_ID constant
     */
    public static String getPurchaseReturnDetail_TransactionDetailId()
    {
        return "purchase_return_detail.TRANSACTION_DETAIL_ID";
    }
  
    /**
     * purchase_return_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.INDEX_NO constant
     */
    public static String getPurchaseReturnDetail_IndexNo()
    {
        return "purchase_return_detail.INDEX_NO";
    }
  
    /**
     * purchase_return_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.ITEM_ID constant
     */
    public static String getPurchaseReturnDetail_ItemId()
    {
        return "purchase_return_detail.ITEM_ID";
    }
  
    /**
     * purchase_return_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.ITEM_CODE constant
     */
    public static String getPurchaseReturnDetail_ItemCode()
    {
        return "purchase_return_detail.ITEM_CODE";
    }
  
    /**
     * purchase_return_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.ITEM_NAME constant
     */
    public static String getPurchaseReturnDetail_ItemName()
    {
        return "purchase_return_detail.ITEM_NAME";
    }
  
    /**
     * purchase_return_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.DESCRIPTION constant
     */
    public static String getPurchaseReturnDetail_Description()
    {
        return "purchase_return_detail.DESCRIPTION";
    }
  
    /**
     * purchase_return_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.UNIT_ID constant
     */
    public static String getPurchaseReturnDetail_UnitId()
    {
        return "purchase_return_detail.UNIT_ID";
    }
  
    /**
     * purchase_return_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.UNIT_CODE constant
     */
    public static String getPurchaseReturnDetail_UnitCode()
    {
        return "purchase_return_detail.UNIT_CODE";
    }
  
    /**
     * purchase_return_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.QTY constant
     */
    public static String getPurchaseReturnDetail_Qty()
    {
        return "purchase_return_detail.QTY";
    }
  
    /**
     * purchase_return_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.QTY_BASE constant
     */
    public static String getPurchaseReturnDetail_QtyBase()
    {
        return "purchase_return_detail.QTY_BASE";
    }
  
    /**
     * purchase_return_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.ITEM_PRICE constant
     */
    public static String getPurchaseReturnDetail_ItemPrice()
    {
        return "purchase_return_detail.ITEM_PRICE";
    }
  
    /**
     * purchase_return_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.TAX_ID constant
     */
    public static String getPurchaseReturnDetail_TaxId()
    {
        return "purchase_return_detail.TAX_ID";
    }
  
    /**
     * purchase_return_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.TAX_AMOUNT constant
     */
    public static String getPurchaseReturnDetail_TaxAmount()
    {
        return "purchase_return_detail.TAX_AMOUNT";
    }
  
    /**
     * purchase_return_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.SUB_TOTAL_TAX constant
     */
    public static String getPurchaseReturnDetail_SubTotalTax()
    {
        return "purchase_return_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * purchase_return_detail.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.ITEM_COST constant
     */
    public static String getPurchaseReturnDetail_ItemCost()
    {
        return "purchase_return_detail.ITEM_COST";
    }
  
    /**
     * purchase_return_detail.SUB_TOTAL_COST
     * @return the column name for the SUB_TOTAL_COST field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.SUB_TOTAL_COST constant
     */
    public static String getPurchaseReturnDetail_SubTotalCost()
    {
        return "purchase_return_detail.SUB_TOTAL_COST";
    }
  
    /**
     * purchase_return_detail.RETURN_AMOUNT
     * @return the column name for the RETURN_AMOUNT field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.RETURN_AMOUNT constant
     */
    public static String getPurchaseReturnDetail_ReturnAmount()
    {
        return "purchase_return_detail.RETURN_AMOUNT";
    }
  
    /**
     * purchase_return_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.PROJECT_ID constant
     */
    public static String getPurchaseReturnDetail_ProjectId()
    {
        return "purchase_return_detail.PROJECT_ID";
    }
  
    /**
     * purchase_return_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use PurchaseReturnDetailPeer.purchase_return_detail.DEPARTMENT_ID constant
     */
    public static String getPurchaseReturnDetail_DepartmentId()
    {
        return "purchase_return_detail.DEPARTMENT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_return_detail");
        TableMap tMap = dbMap.getTable("purchase_return_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_return_detail.PURCHASE_RETURN_DETAIL_ID", "");
                          tMap.addForeignKey(
                "purchase_return_detail.PURCHASE_RETURN_ID", "" , "purchase_return" ,
                "purchase_return_id");
                          tMap.addColumn("purchase_return_detail.TRANSACTION_DETAIL_ID", "");
                            tMap.addColumn("purchase_return_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("purchase_return_detail.ITEM_ID", "");
                          tMap.addColumn("purchase_return_detail.ITEM_CODE", "");
                          tMap.addColumn("purchase_return_detail.ITEM_NAME", "");
                          tMap.addColumn("purchase_return_detail.DESCRIPTION", "");
                          tMap.addColumn("purchase_return_detail.UNIT_ID", "");
                          tMap.addColumn("purchase_return_detail.UNIT_CODE", "");
                            tMap.addColumn("purchase_return_detail.QTY", bd_ZERO);
                            tMap.addColumn("purchase_return_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("purchase_return_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("purchase_return_detail.TAX_ID", "");
                            tMap.addColumn("purchase_return_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("purchase_return_detail.SUB_TOTAL_TAX", bd_ZERO);
                            tMap.addColumn("purchase_return_detail.ITEM_COST", bd_ZERO);
                            tMap.addColumn("purchase_return_detail.SUB_TOTAL_COST", bd_ZERO);
                            tMap.addColumn("purchase_return_detail.RETURN_AMOUNT", bd_ZERO);
                          tMap.addColumn("purchase_return_detail.PROJECT_ID", "");
                          tMap.addColumn("purchase_return_detail.DEPARTMENT_ID", "");
          }
}

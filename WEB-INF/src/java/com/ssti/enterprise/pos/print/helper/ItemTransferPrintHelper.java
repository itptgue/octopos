package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemTransferPrintHelper.java,v 1.6 2007/12/20 13:48:43 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemTransferPrintHelper.java,v $
 * Revision 1.6  2007/12/20 13:48:43  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ItemTransferPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{    
	private static Log log = LogFactory.getLog(ItemTransferPrintHelper.class);
		
	private ItemTransfer oTR = null;
	private List vTD = null;
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
		
		oTR = ItemTransferTool.getHeaderByID (sID);
		vTD = ItemTransferTool.getDetailsByID (sID);
		
		oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        
        prepareContext();
        checkTemplate (oParam);
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) 
        {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF)
        {
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML)
        {
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }        
        return oWriter.toString();
    }
 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);

		//get transaction data by id		
		if (_oSession == null) 
		{
			oTR = ItemTransferTool.getHeaderByID (sID);
			vTD = ItemTransferTool.getDetailsByID (sID);
		}
		else
		{
			oTR = (ItemTransfer) _oSession.getAttribute(AppAttributes.s_IT);
			vTD = (List) _oSession.getAttribute(AppAttributes.s_ITD);
		}
        prepareContext();
        checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }    
    
    private void prepareContext()
    {
        s_PDF_TPL_NAME = "/print/ItemTransferPDF.vm";
        s_TXT_TPL_NAME = "/print/ItemTransferTXT.vm";
        s_HTML_TPL_NAME = "/print/ItemTransferHTML.vm";
    }

}

package com.ssti.enterprise.medical.presentation.screens.report.medical;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;

public class Default extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		MedConfigTool.setContextTool(context);
    		setParams(data);
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
		}    
    }    
}

package com.ssti.enterprise.pos.print.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.PrintingLogTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesOrderPrintHelper.java,v 1.12 2009/05/04 02:03:33 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesOrderPrintHelper.java,v $
 * Revision 1.12  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/12/20 13:48:43  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SalesOrderPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper 
{       
	private SalesOrder oTR = null;
	private List vTD = null;
	
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
    	String sUserName = getString(oParam, s_PARAM_USER);
		
		oTR = SalesOrderTool.getHeaderByID (sID);
		vTD = SalesOrderTool.getDetailsByID (sID);

		logPrintingProcess (oTR,  sUserName);
		
        oCtx.put(s_CTX_TRANS, oTR);
        oCtx.put(s_CTX_TRANSDET, vTD);
        prepareContext();
        checkTemplate (oParam);
        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        
        return oWriter.toString();
    }
   	
   	/**
	 * get Printed Text (use detail per page)
	 */
	 
    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
		//get & process required parameter
    	String sID = getString(oParam, s_PARAM_ID);
    	String sUserName = getString(oParam, s_PARAM_USER);
    	
		if (_oSession != null)
		{
			oTR = (SalesOrder) _oSession.getAttribute(AppAttributes.s_SO);
			vTD = (List) _oSession.getAttribute(AppAttributes.s_SOD);
		}
		//get transaction data by id		
		else if (_oSession == null || oTR == null) 
		{
			oTR = SalesOrderTool.getHeaderByID (sID);
			vTD = SalesOrderTool.getDetailsByID (sID);
		}
		
		logPrintingProcess (oTR,  sUserName);
        prepareContext();
        checkTemplate (oParam);
		return getPage (oTR, vTD, _iDetailPerPage, s_TXT_TPL_NAME);
    }
    
    private void prepareContext()
    {
        s_PDF_TPL_NAME = "/print/SalesOrderPDF.vm";
        s_TXT_TPL_NAME = "/print/SalesOrderTXT.vm";
        s_HTML_TPL_NAME = "/print/SalesOrderHTML.vm";    
    }

    /**
     * 
     * @param _oTR
     * @param _sUserName
     * @throws Exception
     */
    private void logPrintingProcess (SalesOrder _oTR, String _sUserName)	
    	throws Exception
    {
    	if (_oTR != null)
		{
    		SalesOrderTool.updatePrintTimes (_oTR);
			PrintingLogTool.logTrans (_oTR, _sUserName);
		}
    }
}

package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.JournalVoucherLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.tools.StringUtil;
/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/transaction/JournalVoucherAction.java,v $
 * Purpose: Journal Voucher Action Class
 *
 * @author  $Author: albert $
 * @version $Id: JournalVoucherAction.java,v 1.12 2008/02/12 01:26:14 albert Exp $
 * @see JournalVoucherTool
 *
 * $Log: JournalVoucherAction.java,v $
 * Revision 1.12  2008/02/12 01:26:14  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/12/20 13:49:55  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/11/09 01:12:15  albert
 * *** empty log message ***
 *
 * Revision 1.9  2006/02/04 05:17:52  albert
 * *** empty log message ***
 *
 * Revision 1.8  2005/12/27 03:33:10  albert
 * *** empty log message ***
 *
 * Revision 1.7  2005/08/29 10:00:59  albert
 * *** empty log message ***
 *
 */
public class JournalVoucherAction extends TransactionSecureAction
{  	
    private static final String s_VIEW_PERM    = "View Journal Voucher";
	private static final String s_CREATE_PERM  = "Create Journal Voucher";
	private static final String s_CANCEL_PERM  = "Cancel Journal Voucher";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	if(data.getParameters().getString("eventSubmit_doInsert", null) != null)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
    	else if(data.getParameters().getString("eventSubmit_doCancel", null) != null)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	if (data.getParameters().getInt("save") == 1) {
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("save") == 2) {
    		doCancel (data,context);
    	}    	
    }

    public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);			
			int iStatus = data.getParameters().getInt("Status");
			LargeSelect vTR = JournalVoucherTool.findData (iCond, sKeywords, dStart, dEnd, iStatus, iLimit);
			data.getUser().setTemp("findJournalVoucherResult",vTR);
        }                 
        catch (Exception _oEx)
        {                 
        	log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }                 
    }

	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{
			synchronized (this) 
			{				
				HttpSession oSes = data.getSession();
				JournalVoucher oJV =  (JournalVoucher) oSes.getAttribute (s_JV);			
				List vJVD = (List) oSes.getAttribute (s_JVD);
				JournalVoucherTool.setHeaderProperties (oJV, vJVD, data, true);			
				JournalVoucherTool.saveData (oJV, vJVD);			
				data.setMessage(LocaleTool.getString("jv_save_success"));
			}
        }
        catch (Exception _oEx) 
        {
        	handleError (data, LocaleTool.getString("jv_save_failed"), _oEx);
        }
    }

	public void doCancel ( RunData data, Context context )
	    throws Exception
	{
		try
		{
			synchronized (this) 
			{
				HttpSession oSes = data.getSession();
				JournalVoucher oJV =  (JournalVoucher) oSes.getAttribute (s_JV);		
				if (oJV != null)
				{
					if (oJV.getTransType() == JournalVoucherTool.i_JV_NORMAL) //normal JV
					{
						JournalVoucherTool.cancelJV (oJV, data.getUser().getName());
						data.setMessage(LocaleTool.getString("jv_cancel_success"));        	
					}
					else
					{
						data.setMessage(LocaleTool.getString("trans_cancel_src") + oJV.getTransactionNo());
					}
				}
			}
	    }
	    catch (Exception _oEx) 
	    {
        	handleError (data, LocaleTool.getString("jv_cancel_failed"), _oEx);
	    }
	}
	
    public void doLoadfile (RunData data, Context context)
    	throws Exception
    {
    	try 
    	{
    		FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
    		InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
            String sLoader = data.getParameters().getString("loader");
            TransactionLoader oLoader = null;
            if (StringUtil.isNotEmpty(sLoader))
            {
                oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
            }
            else
            {
                oLoader = new JournalVoucherLoader(data.getUser().getName());
            }
    		oLoader.loadData (oBufferStream);
    		data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
    		context.put ("LoadResult", oLoader.getResult());
    	}
    	catch (Exception _oEx) 
    	{
    		String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
    		log.error ( sError, _oEx );
    		data.setMessage (sError);
    	}
    } 
}
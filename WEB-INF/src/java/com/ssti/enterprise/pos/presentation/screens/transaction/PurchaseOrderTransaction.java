package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.VendorPriceListDetail;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/PurchaseOrderTransaction.java,v $
 * Purpose: this class used as purchase order screen handler 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseOrderTransaction.java,v 1.14 2009/05/04 02:02:36 albert Exp $
 * @see PurchaseOrderTransaction
 *
 * $Log: PurchaseOrderTransaction.java,v $
 *
 * 2018-08-29
 * - add method processAddGroup now support grouping in purchase
 * - when add grouping item break down to parts item
 *
 * 2015-03-10
 * Change method processAddItem change request Qty to PO Qty in Purchase Unit (round up)
 * 
 */

public class PurchaseOrderTransaction extends PurchaseSecureScreen
{
    private static final String[] a_PERM = {"Purchase Transaction", "View Purchase Order"};

    private static final Log log = LogFactory.getLog(PurchaseOrderTransaction.class);

	private static final int i_OP_ADD_DETAIL	= 1;
    private static final int i_OP_DEL_DETAIL	= 2;
	private static final int i_OP_NEW_TRANS		= 3;
	private static final int i_OP_VIEW_TRANS	= 4; //after saved or view details
    private static final int i_OP_ADD_ITEMS		= 5;
    private static final int i_OP_ADD_REQUESTS	= 6;
    private static final int i_OP_COPY_TRANS	= 7;
    private static final int i_OP_GENERATE_FILE	= 8;
    private static final int i_OP_LOAD_EXCEL	= 9;
    private static final int i_OP_LOAD_SO		= 10;
    
    private static final int i_USER_INPUT		= 1;
	private static final int i_ADD_ITEMS		= 2;
	
    private PurchaseOrder oPO = null;
    private List vPOD = null;
    private HttpSession oPOSes;
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH)
			{
				addDetailData (data);
			}
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH)
			{
				delDetailData (data);
			}
			else if ( iOp == i_OP_NEW_TRANS )
			{
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS )
			{
				getData (data);
			}
			else if (iOp == i_OP_ADD_ITEMS && !b_REFRESH)
			{
				addItems (data);
			}		
			else if (iOp == i_OP_ADD_REQUESTS && !b_REFRESH)
			{
				addRequests (data);
			}
			else if (iOp == i_OP_COPY_TRANS)
			{
				copyTrans (data);
			}			
			else if (iOp == i_OP_LOAD_EXCEL)
			{
				PurchaseOrderTool.setHeaderProperties (oPO, vPOD,data);
			}
			else if (iOp == i_OP_LOAD_SO && !b_REFRESH)
			{
				loadSO (data);
			}

		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_POD, oPOSes.getAttribute (s_POD));
    	context.put(s_PO, oPOSes.getAttribute (s_PO));    	
    }

	private void initSession(RunData data)
	{
		synchronized (this)
		{
			oPOSes = data.getSession();
			if ( oPOSes.getAttribute (s_PO) == null )
			{
				createNewTrans(data);
			}
			oPO = (PurchaseOrder) oPOSes.getAttribute (s_PO);
			vPOD = (List) oPOSes.getAttribute (s_POD);
		}
	}

    /**
     * add single detail
     * 
     * @param data
     * @throws Exception
     */
    private void addDetailData ( RunData data )
    	throws Exception
    {
    	//save current updated qty first
    	PurchaseOrderTool.updateDetail (vPOD,data);

        String sItemID = data.getParameters().getString("ItemId");
        //double dQty = data.getParameters().getDouble("Qty");
        
        processAddItem (sItemID,i_USER_INPUT, -1, null, data);
    }
	
	/**
	 * add multiple items as once. item id saved in query string
	 * parse the id one by one then process it
	 * 
	 * @param data
	 * @throws Exception
	 */
    private void addItems(RunData data)
    	throws Exception
    {
    	//save current updated qty first
		PurchaseOrderTool.updateDetail (vPOD,data);

		String sItemID = data.getParameters().getString ("items");		
		log.debug (sItemID);
		if (sItemID != null && !sItemID.equals(""))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddItem (sItemID,i_ADD_ITEMS, -1, null,data);
				}
			}
			else {
				processAddItem (sItemID,i_ADD_ITEMS,-1, null,data);
			}
		}
	}
    
    /**
     * process add single item from multiple item
     * 
     * @param _sItemID
     * @param data
     * @param _dQty if -1 then count using max - min 
     * if Qty -1 also mean call from addItems
     * @throws Exception
     */
    private void processAddItem (String _sItemID, int _iAddFrom, double _dQty, InventoryDetailOM _oTD, RunData data)
    	throws Exception
    {
		String sLocationID = data.getParameters().getString("LocationId");
		String sVendorID = data.getParameters().getString("VendorId","");
		
		Unit oUnit = null;
		double dUnitBaseValue = 1;
		PurchaseOrderDetail oPOD = new PurchaseOrderDetail ();
		
		//if from user input set Detail from user's input
		if(_iAddFrom == i_USER_INPUT && data != null)
		{
		    data.getParameters().setProperties(oPOD);
		}
		
		Item oItem = ItemTool.getItemByID (_sItemID);		
		if(oItem != null)
		{
			if(oItem.getItemType() == i_GROUPING)
			{								
				processAddGroup(oItem, data);
			}
			else
			{					
				//unit
				if(_iAddFrom == i_ADD_ITEMS)
				{
				    oUnit = UnitTool.getUnitByID(oItem.getPurchaseUnitId());
				    dUnitBaseValue = UnitTool.getBaseValue(oItem.getItemId(), oUnit.getUnitId());
	
	    			oPOD.setItemId   (_sItemID);
		    		oPOD.setItemCode (oItem.getItemCode());
			    	oPOD.setItemName (oItem.getItemName());
				    oPOD.setUnitId   (oUnit.getUnitId());
				    oPOD.setUnitCode (oUnit.getUnitCode());
				}
				else
				{
				    oUnit = UnitTool.getUnitByID(oPOD.getUnitId());
				    dUnitBaseValue = UnitTool.getBaseValue(oItem.getItemId(), oUnit.getUnitId());	    		
				    oPOD.setItemCode (oItem.getItemCode());
			    	oPOD.setItemName (oItem.getItemName());
				}			
				
				//qty
				double dQty = 1;
				if(_dQty > 0) //qty set from function param 
				{ 
					dQty = _dQty;
					oPOD.setQty(new BigDecimal(dQty));
				}
				if (_oTD == null) 
				{
					if (_iAddFrom == i_ADD_ITEMS)
					{	
						dQty = ItemInventoryTool.getOrderQty(_sItemID, sLocationID);
						dQty = (int) (dQty / dUnitBaseValue); //convert to purchase unit
						if (dQty == 0) dQty = 1;
					}
					else 
					{
						dQty = oPOD.getQty().doubleValue(); //qty in purchase unit 
					}
				}
				else //from request / SO
				{		
					dQty = _oTD.getQty().doubleValue();				
					if(_oTD instanceof PurchaseRequestDetail)
					{	
						PurchaseRequestDetail oRQD = (PurchaseRequestDetail) _oTD;
						double iSent = oRQD.getSentQty().doubleValue();				
						if (dQty >= iSent)
						{
							dQty = dQty - iSent;
						}
						//request qty is always in base unit, convert it to purchase unit
						dQty = Calculator.roundUp(dQty / dUnitBaseValue, BigDecimal.ROUND_UP).doubleValue();
						oPOD.setDescription(_oTD.getDescription());
						oPOD.setRequestType(1);					
					}
					if(_oTD instanceof SalesOrderDetail)
					{	
						SalesOrderDetail oSOD = (SalesOrderDetail) _oTD;					
						double dSOinPO = oSOD.getQtyInDirectPO();	
						System.out.println("dSOinPO " + dSOinPO);
						double iDelivered = oSOD.getDeliveredQty().doubleValue();	
						if(dSOinPO > 0)
						{
							oPOD.setChangedManual(true);
						}
						if (dQty >= (iDelivered + dSOinPO))
						{
							//count SO Qty already in PO 												
							dQty = dQty - iDelivered - dSOinPO;
						}
						//if SO qty in sales unit, convert it to purchase unit
						if(oSOD.getUnitId().equals(oItem.getUnitId()) && !StringUtil.isEqual(oItem.getPurchaseUnitId(), oItem.getUnitId()))
						{
							dQty = Calculator.roundUp(dQty / dUnitBaseValue, BigDecimal.ROUND_UP).doubleValue();
						}
						oPOD.setDescription(_oTD.getDescription());
						oPOD.setRequestType(2);
					}							
					
					oPOD.setRequestDetailId(_oTD.getTransDetId());
					oPOD.setRequestId(_oTD.getTransId());
				}
				
				//set Qty and QtyBase
				oPOD.setQty(new BigDecimal (dQty));
				double dQtyBase = dQty * dUnitBaseValue; 
				oPOD.setQtyBase(new BigDecimal(dQtyBase));
				
				//price and discount
				if(_iAddFrom == i_ADD_ITEMS)
				{
			        oPOD.setDiscount ( "0" );
			        //TODO: check again
			        double dPurcPrice = 
			        	oItem.getLastPurchasePrice().doubleValue() * oItem.getUnitConversion().doubleValue();
			        oPOD.setItemPrice(new BigDecimal(dPurcPrice));
	
			        //check price & discount in vendor price list
				    VendorPriceListDetail oVPL = 
				    	VendorPriceListTool.getPriceListDetailByVendorID(sVendorID, _sItemID, oPO.getTransactionDate());			   
				    			    			    
				    //price list is first priority
				    if(oVPL != null)
				    {
				    	oPOD.setDiscount  (oVPL.getDiscountAmount());			    	
				    	if(oVPL.getPurchasePrice().doubleValue() >= 0)
				    	{    		        
				    		oPOD.setItemPrice (oVPL.getPurchasePrice());
				    	}
				    }
				    else
				    {
				    	//check item discount in vendor
				    	if(oPO.getVendor() != null && StringUtil.isNotEmpty(oPO.getVendor().getDefaultItemDisc()))
				    	{
				    		oPOD.setDiscount(oPO.getVendor().getDefaultItemDisc());
				    	}			    	
				    }
				} //end from add items
				
				//calculate sub total, discount
	    		double dTotal = oPOD.getItemPrice().doubleValue() * dQty;
	    		double dDiscount = Calculator.calculateDiscount(oPOD.getDiscount(), dTotal);
	    		oPOD.setSubTotalDisc (new BigDecimal(dDiscount));
	    		dTotal = dTotal - oPOD.getSubTotalDisc().doubleValue();
	    					
				//calculate last purchase & cost
				//PurchaseOrderTool.calculateLastPurchase(oPOD,i_LAST_PURCHASE_METHOD);
				
				double dRate = data.getParameters().getDouble("CurrencyRate", 1);
				double dCost = 0;
				if (oPOD.getQtyBase().doubleValue() > 0)
				{
					dCost = dTotal / oPOD.getQtyBase().doubleValue() * dRate;    		
				}
				oPOD.setCostPerUnit (new BigDecimal(dCost));
				oPOD.setSubTotal (new BigDecimal(dTotal));
	
				//tax
				if(_iAddFrom == i_ADD_ITEMS)
				{
	                oPOD.setTaxId (oItem.getPurchaseTaxId());
				    oPOD.setTaxAmount (TaxTool.getAmountByID(oItem.getPurchaseTaxId()));
				}
				
	    		double dTax = dTotal * oPOD.getTaxAmount().doubleValue() / 100;
	    		oPOD.setSubTotalTax (new BigDecimal(dTax));
	    		
				if (!isExist (oPOD))
				{
	   			    if (b_PURCHASE_FIRST_LAST)
	   			    {
				        vPOD.add (0, oPOD);
				    }
				    else
				    {
	    				vPOD.add (oPOD);
					}
				}
				PurchaseOrderTool.setHeaderProperties (oPO, vPOD,data);
				oPOSes.setAttribute (s_POD, vPOD);
				oPOSes.setAttribute (s_PO, oPO);
	    	}
		}
    }	
    
    private void processAddGroup(Item _oGroupItem, RunData data) 
    	throws Exception
    {
    	List vGroup = ItemGroupTool.getItemGroupByGroupID(_oGroupItem.getItemId());
    	double dGroupQty = 1;
    	if(data != null) { dGroupQty = data.getParameters().getDouble("Qty"); }
    	for(int i = 0; i < vGroup.size(); i++)
    	{
    		ItemGroup oIG = (ItemGroup) vGroup.get(i);
    		Item oItem = oIG.getItem();
    		if(oItem != null)
    		{
	    		double dIGQty = oIG.getQty().doubleValue() * dGroupQty;	    		
	    		data.getParameters().setString("qty", new BigDecimal(dIGQty).toString());
	    		data.getParameters().setString("unitid", oIG.getUnitId());
	    		data.getParameters().setString("itemid", oItem.getItemId());
	    		if(oItem.getLastPurchasePrice() != null)
	    		{
	    			data.getParameters().setString("itemprice", oItem.getLastPurchasePrice().toString());
	    		}
	    		processAddItem(oItem.getItemId(), i_USER_INPUT, dIGQty, null, data);
    		}
    	}    				
	}

	/**
     * add purchase request to po
     * 
     * @param data
     * @throws Exception
     */
    private void addRequests(RunData data)
    	throws Exception
    {
    	//save current updated qty first
		PurchaseOrderTool.updateDetail (vPOD,data);
		String sRequestID = data.getParameters().getString ("ids");		
		data.getParameters().setProperties(oPO);
		
		if (StringUtil.isNotEmpty(sRequestID))
		{
			if (sRequestID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sRequestID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sRequestID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddRequest (sRequestID, data);
				}
			}
			else 
			{
				processAddRequest (sRequestID, data);
			}
			PurchaseOrderTool.setHeaderProperties (oPO, vPOD,data);
			oPOSes.setAttribute (s_POD, vPOD);
			oPOSes.setAttribute (s_PO, oPO);
		}
	}

    private void processAddRequest ( String _sRequestID, RunData data )
    	throws Exception
    {
		List vData = PurchaseRequestTool.getDetailsByID (_sRequestID);
		for (int i = 0; i < vData.size(); i++)
		{
			InventoryDetailOM oRQD = (InventoryDetailOM) vData.get(i);
			processAddItem (oRQD.getItemId(), i_ADD_ITEMS, -1, oRQD, data);
    	} 
    }	
    
    /**
     * Delete PO detail data defined by parameter No in RunData
     * 
     * @param data
     * @throws Exception
     */
    private void delDetailData ( RunData data )
    	throws Exception
    {
    	//save current updated qty first
		PurchaseOrderTool.updateDetail (vPOD,data);

		if (vPOD.size() > (data.getParameters().getInt("No") - 1))
		{
			vPOD.remove ( data.getParameters().getInt("No") - 1 );
			oPOSes.setAttribute  ( s_POD, vPOD );
			PurchaseOrderTool.setHeaderProperties ( oPO, vPOD, data );
			oPOSes.setAttribute ( s_PO, oPO );
		}
    }

    /**
     * Create new Purchase Order
     * 
     * @param data
     */
    private void createNewTrans ( RunData data )
    {
    	synchronized (this)
    	{
    		oPO = new PurchaseOrder();
    		vPOD = new ArrayList();
    		
    		//initialize inclusive tax
    		oPO.setIsInclusiveTax(b_PURCHASE_TAX_INCLUSIVE);
    		
    		oPOSes.setAttribute (s_PO,  oPO);
    		oPOSes.setAttribute (s_POD, vPOD);
    	}
    }

    /**
     * Check wheter current detail exist by comparing item_id, item_code, unit_id
     * 
     * @param _oPOD
     * @return
     */
    private boolean isExist (PurchaseOrderDetail _oPOD)
    	throws Exception
    {
    	for (int i = 0; i < vPOD.size(); i++)
    	{
    		PurchaseOrderDetail oExistPOD = (PurchaseOrderDetail) vPOD.get (i);
    		if (StringUtil.isEqual(oExistPOD.getItemCode(), _oPOD.getItemCode())  &&
    			StringUtil.isEqual(oExistPOD.getItemId(), _oPOD.getItemId()) &&
    		    StringUtil.isEqual(oExistPOD.getDescription(), _oPOD.getDescription()) &&    		    
    			StringUtil.isEqual(oExistPOD.getUnitId(), _oPOD.getUnitId()) &&    			
    		    StringUtil.isEqual(oExistPOD.getTaxId(), _oPOD.getTaxId())   &&
    			StringUtil.isEqual(oExistPOD.getRequestId(), _oPOD.getRequestId()) &&
    			StringUtil.isEqual(oExistPOD.getDiscount(), _oPOD.getDiscount()) &&
    			(oExistPOD.getItemPrice().intValue() == _oPOD.getItemPrice().intValue()) )
    		{    			
    		    if(i_MERGE_DET == i_MERGE_ITM)
    		    {
				    mergeData (oExistPOD, _oPOD);
					return true;					 
    		    }
    		    else if(i_MERGE_DET == i_MERGE_ERR)
    		    {
    		    	throw new Exception("Item Already Exist in Detail");
    		    }
    		}
    	}
    	return false;
    }

    /**
     * merge po if same item
     * 
     * @param _oOldTD
     * @param _oNewTD
     */
    //TODO: cost per unit not yet calculated here. find out if this causing problem 
    private void mergeData (PurchaseOrderDetail _oOldTD, PurchaseOrderDetail _oNewTD)
    {
    	double dQty 		= _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
    	double dQtyBase 	= _oOldTD.getQtyBase().doubleValue() + _oNewTD.getQtyBase().doubleValue();
    	
    	double dTotalPurc   = dQty * _oNewTD.getItemPrice().doubleValue();
    	String sDiscount	= _oNewTD.getDiscount();
    	double dTotalDisc   = Calculator.calculateDiscount(sDiscount,dTotalPurc);
    	dTotalPurc 			= dTotalPurc - dTotalDisc;
    	    	
    	double dTaxAmount   = _oNewTD.getTaxAmount().doubleValue()/100;
    	double dTotalTax 	= dTotalPurc * dTaxAmount;
    	double dSubTotal 	= dTotalPurc + dTotalTax;

    	_oOldTD.setQty 			( new BigDecimal(dQty));
    	_oOldTD.setQtyBase		( new BigDecimal(dQtyBase));
		_oOldTD.setDiscount 	( sDiscount );    	
		_oOldTD.setTaxAmount	( new BigDecimal (dTaxAmount * 100) );    
    	_oOldTD.setSubTotalDisc ( new BigDecimal (dTotalDisc));
    	_oOldTD.setSubTotalTax 	( new BigDecimal (dTotalTax) );
    	_oOldTD.setSubTotal     ( new BigDecimal (dSubTotal) );
    	_oNewTD = null;
   	}
    
    /**
     * get data from DB and set in session
     * 
     * @param data
     * @throws Exception
     */
    private void getData ( RunData data )
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID))
    	{
    		try
    		{
    			oPOSes.setAttribute (s_PO, PurchaseOrderTool.getHeaderByID (sID));
				oPOSes.setAttribute (s_POD, PurchaseOrderTool.getDetailsByID (sID));
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }
    
    /**
     * copy Trans
     * 
     * @param data
     * @throws Exception
     */
    private void copyTrans ( RunData data )
    	throws Exception
    {
    	String sID = data.getParameters().getString("TransId");
    	PurchaseOrder oTRC = PurchaseOrderTool.getHeaderByID(sID);
		
		if (oTRC != null && StringUtil.isNotEmpty (sID))
		{			
			List vTrans = PurchaseOrderTool.getDetailsByID(sID);
			vPOD = new ArrayList(vTrans.size());
			for (int i = 0; i < vTrans.size(); i++)
			{
				PurchaseOrderDetail oDet = (PurchaseOrderDetail) vTrans.get(i);
				PurchaseOrderDetail oNewDet = oDet.copy();
				oNewDet.setReceivedQty(bd_ZERO);
				vPOD.add(oNewDet);
			}
		}
		PurchaseOrderTool.setHeaderProperties(oPO, vPOD, data);
		
		oPOSes.setAttribute (s_POD, vPOD);
		oPOSes.setAttribute (s_PO, oPO);
    }        
           
    /**
     * add purchase request to po
     * 
     * @param data
     * @throws Exception
     */
    private void loadSO(RunData data)
    	throws Exception
    {
    	//save current updated qty first
		PurchaseOrderTool.updateDetail (vPOD,data);
		String sSOID = data.getParameters().getString ("SoId");		
		data.getParameters().setProperties(oPO);
		
		if (StringUtil.isNotEmpty(sSOID))
		{			
			List vData = SalesOrderTool.getDetailsByID (sSOID);
			for (int i = 0; i < vData.size(); i++)
			{
				SalesOrderDetail oSOD = (SalesOrderDetail) vData.get(i);
				if(oSOD.getQty().doubleValue() > oSOD.getDeliveredQty().doubleValue())
				{
					processAddItem (oSOD.getItemId(), i_ADD_ITEMS, -1, oSOD, data);
				}
	    	} 
			
			PurchaseOrderTool.setHeaderProperties (oPO, vPOD,data);
			oPOSes.setAttribute (s_POD, vPOD);
			oPOSes.setAttribute (s_PO, oPO);
		}
	}
}

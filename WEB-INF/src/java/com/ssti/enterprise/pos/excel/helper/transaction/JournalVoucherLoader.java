package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.om.JournalVoucherDetail;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.enterprise.pos.tools.gl.SubLedgerTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: JournalVoucherLoader.java,v 1.1 2008/02/12 01:25:41 albert Exp $ <br>
 *
 * <pre>
 * $Log: JournalVoucherLoader.java,v $
 * Revision 1.1  2008/02/12 01:25:41  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/07/02 15:37:32  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class JournalVoucherLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( JournalVoucherLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Account Code";
	private static final String s_END_TRANS = "End Trans";	
	
	public JournalVoucherLoader ()
	{
	}
	
	public JournalVoucherLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		
		JournalVoucher oTR = null;
		List vTD = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new JournalVoucher();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{    				
    				JournalVoucherDetail oTD = new JournalVoucherDetail();
    				mapDetails (oRow, oTR, oTD);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			JournalVoucherTool.setHeaderProperties(oTR, vTD, null, false);
    		    			JournalVoucherTool.saveData(oTR, vTD);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			_oEx.printStackTrace();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;		
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, JournalVoucher _oTR)
    	throws Exception
    {
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	   	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setTransactionDate(dTrans);}

    	String sUserName = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sUserName)) {logError("Create By", sUserName);}
    	else {_oTR.setUserName(sUserName);}
	
    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setDescription(sRemark);
    	    	
    	//OTHER FIELD
    	if (!m_bTransError)
    	{
	    	_oTR.setStatus(i_PROCESSED);
	    	_oTR.setTransType(JournalVoucherTool.i_JV_NORMAL);	    	
	    	_oTR.setDescription(_oTR.getDescription());
	    	
	    	//set total debit & credit
	    	_oTR.setTotalCredit(bd_ZERO);
	    	_oTR.setTotalDebit(bd_ZERO);	    	
    	}
    }

    /**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @throws Exception
	 */
	protected void mapDetails (HSSFRow _oRow, JournalVoucher _oTR, JournalVoucherDetail _oTD)
		throws Exception
	{    	
    	mapDetails(_oRow,_oTR,_oTD, 0);
	}
	
	/**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @param _iStartCol
	 * @throws Exception
	 */
    
    protected void mapDetails (HSSFRow _oRow, JournalVoucher _oTR, JournalVoucherDetail _oTD, int _iStartCol)
		throws Exception
	{    	
    	int iCol = _iStartCol;
    	String sAccCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("account_code")).append(": ").append(sAccCode).append(s_LINE_SEPARATOR);
    	
    	Account oAcc = AccountTool.getAccountByCode(sAccCode);
    	if (oAcc == null) {logError("Account Code", sAccCode);}
    	else 
    	{
    		if (oAcc.getHasChild()) logInvalidAcc(sAccCode);
    		_oTD.setAccountId(oAcc.getAccountId());
    		_oTD.setAccountCode(oAcc.getAccountCode());
    		_oTD.setAccountName(oAcc.getAccountName());
    	}
    	
    	String sCurrCode = getString (_oRow, iCol); iCol++;
    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurrCode);
    	if (oCurr == null) {logError("Currency Code", sCurrCode);}
    	else 
    	{
    		_oTD.setCurrencyId(oCurr.getCurrencyId());
    	}
    	    	
    	BigDecimal dRate = getBigDecimal (_oRow, iCol); iCol++;
    	if (dRate == null) {logError("Currency Rate", dRate);}
    	else 
    	{
    		_oTD.setCurrencyRate(dRate);
    	}
    	
    	if (oCurr.getIsDefault() && dRate.doubleValue() != 1) 
    	{
    		logError("Default currency and rate not match ", oCurr.getCurrencyCode() + " : " + dRate);
    	}

    	BigDecimal dDebit = getBigDecimal (_oRow, iCol); iCol++;
    	_oTD.setDebitAmount(dDebit);

    	BigDecimal dCredit = getBigDecimal (_oRow, iCol); iCol++;
    	_oTD.setCreditAmount(dCredit);
    	
    	double dAmount = 0;
    	if (dCredit.doubleValue() != 0 && dDebit.doubleValue() != 0)
    	{
    		logError("Debit Credit", dCredit.doubleValue() + dDebit.doubleValue());
    	}
    	else
    	{
    		dAmount = dDebit.doubleValue() + dCredit.doubleValue();
    	}    	

    	if (dDebit.doubleValue() != 0) _oTD.setDebitCredit(GlAttributes.i_DEBIT);
    	else if (dCredit.doubleValue() != 0) _oTD.setDebitCredit(GlAttributes.i_CREDIT);
    	
    	String sMemo= getString(_oRow, iCol); iCol++;
    	_oTD.setMemo(sMemo);
    	
    	String sLocationCode = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sLocationCode))
    	{
	    	Location oLocation = LocationTool.getLocationByCode(sLocationCode);
	    	if (oLocation == null) {logError("Location Code", sLocationCode);}
	    	else
	    	{
	    		_oTD.setLocationId(oLocation.getLocationId());
	    	}
    	}
    	else
    	{
    		_oTD.setLocationId("");
    	}
    	
    	String sProjectCode = getString(_oRow, iCol); iCol++;    	
    		_oTD.setProjectId("");
    	    	
    	String sDepartmentCode = getString(_oRow, iCol); iCol++;    	
    	_oTD.setDepartmentId("");
    	
    	
    	String sSLType  = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sSLType))
    	{
    		int iSLType = Integer.valueOf(sSLType);
    		if (iSLType < 1 && iSLType > 5) logError("SubLedgerType", sSLType);
    		else _oTD.setSubLedgerType(iSLType);
    		
	    	String sSLCode  = getString(_oRow, iCol); iCol++;
	    	String sSLID = SubLedgerTool.getIDByCode(iSLType,sSLCode);
	    	_oTD.setSubLedgerId(sSLID);
	    	
	    	String sSLDesc  = getString(_oRow, iCol); iCol++;
	    	_oTD.setSubLedgerDesc(sSLDesc);
	    	
	    	String sRefNo  = getString(_oRow, iCol); iCol++;
	    	_oTD.setReferenceNo(sRefNo);
    	}
		//CALCULATED FIELD
    	if (!m_bTransError)
    	{
    		double dAmountBase = dAmount * _oTD.getCurrencyRate().doubleValue();
    		_oTD.setAmount(new BigDecimal(dAmount));
    		_oTD.setAmountBase(new BigDecimal(dAmountBase));
    	}
	}
}

package com.ssti.framework.jasper;

import javax.servlet.ServletContext;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BaseJasper.java,v 1.3 2007/02/23 14:12:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: BaseJasper.java,v $
 * Revision 1.3  2007/02/23 14:12:18  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class BaseJasper 
{

	public static void setProperties (ServletContext context)
	{
		System.setProperty(
			"jasper.reports.compile.class.path", 
			context.getRealPath("/WEB-INF/lib/jasperreports-1.0.2.jar") +
			System.getProperty("path.separator") + 
			context.getRealPath("/WEB-INF/classes/") +
			System.getProperty("path.separator") + 
			context.getRealPath("/report/")
			
			);
		System.setProperty("jasper.reports.compile.temp", context.getRealPath("/report/"));
		System.setProperty("org.xml.sax.driver", "org.apache.xerces.parsers.SAXParser");
	}
	
}

package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.VendorPriceList;
import com.ssti.enterprise.pos.om.VendorPriceListDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class ManageVendorPLHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    private static final String s_PARAM_ID = "id";
     
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {   
        String[] aID = (String[]) oParam.get(s_PARAM_ID);
        String sID = aID [0];
        
        List vData = VendorPriceListTool.getDetailByVendorPriceListID(sID);
        VendorPriceList oPL = VendorPriceListTool.getVendorPriceListByID(sID);      
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Item View");

        HSSFRow oRow = oSheet.createRow(0);
        int iRow = 0;

        createHeaderCell(oWB, oRow, (short)iRow, "Price Code"); iRow++;        
        createHeaderCell(oWB, oRow, (short)iRow, "Item Code	"); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Item Name "); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Last Purc."); iRow++;
        createHeaderCell(oWB, oRow, (short)iRow, "Discount   "); iRow++;        
        createHeaderCell(oWB, oRow, (short)iRow, "New Price "); iRow++;
        
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                VendorPriceListDetail oDetail = (VendorPriceListDetail) vData.get(i);
                
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                Item oItem = ItemTool.getItemByID (oDetail.getItemId());

                iRow = 0;
                createCell(oWB, oRow2, (short)iRow, oPL.getVendorPlCode());                   iRow++; 
                createCell(oWB, oRow2, (short)iRow, oItem.getItemCode());                     iRow++; 
                createCell(oWB, oRow2, (short)iRow, oItem.getVendorItemName());               iRow++; 
                createCell(oWB, oRow2, (short)iRow, oItem.getLastPurchasePrice().toString()); iRow++;   
                createCell(oWB, oRow2, (short)iRow, oDetail.getDiscountAmount()); 			  iRow++;   
                createCell(oWB, oRow2, (short)iRow, oDetail.getPurchasePrice()); 			  iRow++;   
            }        
        }
        return oWB;
    }
}

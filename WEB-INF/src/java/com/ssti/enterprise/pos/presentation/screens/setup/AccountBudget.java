package com.ssti.enterprise.pos.presentation.screens.setup;

import java.math.BigDecimal;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GeneralLedgerTool;
import com.ssti.framework.tools.StringUtil;

public class AccountBudget extends SetupSecureScreen
{
   	private static final String s_VIEW_PERM    = "View Chart Of Account";
	private static final String s_CREATE_PERM  = "Update Chart Of Account";
    
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_CREATE_PERM);
    }
    
	static final int i_SAVE_OB = 1;
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	if (data.getParameters().getInt("op", -1) == i_SAVE_OB)
    	{	
    		save (data, context);
    	}
    	context.put("generalledger", GeneralLedgerTool.getInstance());
    }    
    
    private void save(RunData data, Context context)
    {
		int iTotalRow = data.getParameters().getInt("TotalRow");
        int iTotalCol = data.getParameters().getInt("TotalCol");
        
        String sSubType = data.getParameters().getString("src");
        String sSubID  = data.getParameters().getString("id");
        
        log.debug ("row " + iTotalRow + " col " + iTotalCol);

        try
		{            
            for (int i = 0; i < iTotalRow; i++)
            {
                String sAccountID = data.getParameters().getString("AccountId" + i);
                
                for (int j = 0; j < iTotalCol; j++)
                {
                    String sPeriodID = data.getParameters().getString("PeriodIdRow" + i + "Col" + j);
                    double dBudget = data.getParameters().getDouble("BudgetRow" + i + "Col" + j);

                    log.debug ("Row : " + i + " Col : " + j);                    
                    log.debug (" Account : " + AccountTool.getCodeByID(sAccountID) + 
                    		   " Period  : " + PeriodTool.getPeriodByID(sPeriodID));
                    
                    if (StringUtil.isNotEmpty(sAccountID) && StringUtil.isNotEmpty(sPeriodID))
                    {
                        log.debug("Budget : " + dBudget);
                        if (dBudget != 0)
                        {
	                    	GeneralLedgerTool.setBudget (sPeriodID, 
	                        							 sAccountID, 
	                        							 sSubType,
														 sSubID,
														 new BigDecimal(dBudget));                
                        }
                    }
                }            
            }            
            data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            _oEx.printStackTrace();
            log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }  
}

package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.VendorPriceListDetailPeer;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.StringUtil;

public class ManageVendorPriceList extends SecureScreen
{
	private static Log log = LogFactory.getLog(ManageVendorPriceList.class);
	
	private static final String[] a_PERM = {"Price List Setup", "View Vendor Price List"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
    		String sID = data.getParameters().getString("id");
    		int iOp = data.getParameters().getInt("op", -1);
 			String sDetID= data.getParameters().getString ("DetId");
    		double dPrice = data.getParameters().getDouble("PurchasePrice");
    		String sDiscAmount = data.getParameters().getString("DiscountAmount");
			if (StringUtil.isNotEmpty(sID))
			{
				if(iOp == 1)
				{
					updateVendorPriceListDetail(data);
					iOp = 5;
				}
				else if(iOp == 2)
				{
					updatePurchasePrice(sDetID,dPrice,sDiscAmount,data);
					iOp = 5;
				}
				else if(iOp == 3)
				{
					deleteItem(sDetID, data);
					iOp = 5;
				}
				else if(iOp == 4)
				{
					addVendorPriceListDetail (sID, data);
					iOp = 5;
				}
				displayVendorPriceList (iOp, sID,data, context);
    		}
    	}
    	catch (Exception _oEx) 
    	{
    		log.error ( _oEx );
    		data.setMessage (_oEx.getMessage());
    	}
    }

	private void displayVendorPriceList(int _iOp, String _sID, RunData data, Context context)
		throws Exception
	{
		context.put ("VendorPriceList", VendorPriceListTool.getVendorPriceListByID (_sID));
		
		int iCondition = data.getParameters().getInt("Condition");
		int iSortBy = data.getParameters().getInt("SortBy",2);
		int iViewLimit = data.getParameters().getInt("ViewLimit", 50);
		String sKeywords = data.getParameters().getString("Keywords","");	
		String sKategoriID = data.getParameters().getString("KategoriID","");	
		int iPage = data.getParameters().getInt("page",1);				
		
		LargeSelect oDetails = null;
		
		Object oTemp = data.getUser().getTemp ("priceListResult") ;
		
		if (_iOp == 5) //refresh data filter
		{
			oDetails = VendorPriceListTool.findData (_sID, iCondition, iSortBy, iViewLimit, sKategoriID, sKeywords);
			data.getUser().setTemp ("priceListResult",oDetails);
		}
		else 
		{
			if (oTemp != null)
			{
				oDetails = (LargeSelect) oTemp;
			}
			else {
				oDetails = VendorPriceListTool.findData (_sID, iCondition, iSortBy, iViewLimit, sKategoriID, sKeywords);
				data.getUser().setTemp ("priceListResult",oDetails);		
			}
		}
		context.put ("VendorPriceListDetail", oDetails.getPage(iPage));
		context.put ("LargeData", oDetails);
	}

    private void addVendorPriceListDetail(String _sID, RunData data)
    	throws Exception
    {
		String sItemID = data.getParameters().getString ("itemid");		
		if (sItemID != null && !sItemID.equals(""))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddDetail(_sID, sItemID, data);
				}
			}
			else {
				processAddDetail(_sID, sItemID, data);
			}
		}
	}

    private void processAddDetail(String _sID, String _sItemID, RunData data)
    	throws Exception
    {
		if (!VendorPriceListTool.isItemExist(_sItemID, _sID))
		{
			VendorPriceListTool.addItemToVendorPriceList (_sItemID, _sID);
		}
		else
		{
			data.setMessage("Item Already Exist in Catalog");
		}    
    }	

    private void updateVendorPriceListDetail(RunData data)
    	throws Exception
    {
    	try
    	{
    		int iSize = data.getParameters().getInt("DataSize");
    		for ( int i = 0; i < iSize; i++ )
    		{
				String sDiscountAmount = data.getParameters().getString("DiscountAmount"+(i+1));
				double dPrice = data.getParameters().getDouble("PurchasePrice"+(i+1));
				String sDetID= data.getParameters().getString("VendorPriceListDetailId"+(i+1));
				VendorPriceListTool.updateDetailByID(sDetID, dPrice, sDiscountAmount);
			}
    		data.setMessage("Price List Detail Updated Successfully");
    	}
    	catch(Exception _oEx)
    	{
    		log.error (_oEx);
    		throw new Exception("Price List Detail Update Failed : " + _oEx.getMessage());
    	}
	}

	private void updatePurchasePrice(String _sDetID, double _dPrice, String _sDiscountAmount, RunData data)
		throws Exception
	{
		try
    	{
    		String sDiscount = _sDiscountAmount.replace(' ', '+');
    		VendorPriceListTool.updateDetailByID(_sDetID, _dPrice, sDiscount);
    		data.setMessage("New Price Updated Successfully");
    	}
    	catch(Exception _oEx)
    	{
    		log.error (_oEx);
    		throw new Exception("New Price Update Failed : " + _oEx.getMessage());
    	}
	}

	private void deleteItem(String _sID, RunData data)
		throws Exception
	{
		try
    	{
    		log.debug("delete");
			Criteria oCrit = new Criteria();
	        oCrit.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_DETAIL_ID, _sID);
	        VendorPriceListDetailPeer.doDelete(oCrit);
    		data.setMessage("Item Deleted Successfully");
    	}
    	catch(Exception _oEx)
    	{
    		log.error (_oEx);
    		throw new Exception("Item Deletion Failed : " + _oEx.getMessage());
    	}
	}
}

package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

public class IssueReceiptUnplannedHelper
	extends BaseExcelHelper 
	implements ExcelHelper, TransactionAttributes
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] aIsItemList  = (String[]) oParam.get("IsFromItemList"); 
        String[] aLocationID  = (String[]) oParam.get("LocationId");  
        
        int iIsFromItemList = 0; if (aIsItemList != null) iIsFromItemList = Integer.valueOf(aIsItemList[0]).intValue();
        boolean bFromList = false;
        if (iIsFromItemList == 1) bFromList = true;
        
        String sLocationID = ""; if (aLocationID != null) sLocationID = aLocationID[0];         
        Date dAsOf = null;
        String[] aAsOf = (String[]) oParam.get("AsOf");
        if (aAsOf != null && aAsOf.length > 0)
        {
        	dAsOf = DateUtil.getEndOfDayDate(CustomParser.parseDate(aAsOf[0]));
        }

        boolean bNoCost = false;  
        String sNoCost = (String)getFromParam(oParam, "NoCost", String.class);
        if (StringUtil.isNotEmpty(sNoCost)){bNoCost = Boolean.valueOf(sNoCost);}
        
        List vData = (List) _oSession.getAttribute(AppAttributes.s_ITMD);
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Stock Opname Worksheet");
                
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        double dQoH = 0;
        double dCost = 0;
        int iScale = PreferenceTool.getInvenCommaScale();

		createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Name"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "New Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "+/-"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cost"); iCol++;		
		if (PreferenceTool.medicalInstalled())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Batch Number"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Expired Date"); iCol++;
		}
		
        if (vData != null)
        {
             for (int i = 0; i < vData.size(); i++)
             {                 
                 ItemList oItem = (ItemList) vData.get(i);
                 if(bFromList)
                 {
                	 if(StringUtil.isEmpty(sLocationID))
                	 {
                		 sLocationID = PreferenceTool.getLocationID();
                	 }
                	 InventoryLocation oIL = InventoryLocationTool.getDataByItemAndLocationID(oItem.getItemId(),sLocationID);
                	 if(oIL != null)
                	 {
                		 dQoH = oIL.getCurrentQty().setScale(iScale,4).doubleValue();
                		 dCost = oIL.getItemCost().setScale(iScale,4).doubleValue();
                	 }
                	 if (dAsOf != null && !DateUtil.isToday(dAsOf))
                	 {
                		 InventoryTransaction oInv = InventoryTransactionTool.getLastTrans(oItem.getItemId(), sLocationID, dAsOf, null);
                		 if (oInv != null)
                		 {
                			 dQoH = oInv.getQtyBalance().doubleValue();
                			 if (!b_GLOBAL_COSTING && dQoH > 0)
                			 {
                				 dCost = oInv.getValueBalance().doubleValue() / dQoH;
                			 }
                			 else
                			 {
                				 dCost = oInv.getCost().doubleValue();
                			 }
                		 }
                	 }                       
                 }                 
                 else //from Stock Card
                 {
                	 if(oItem.getQty() != null) dQoH = oItem.getQty().doubleValue();
	        		 if(oItem.getCost() != null) dCost = oItem.getCost().doubleValue();
                 }
                 
                 HSSFRow oRow2 = oSheet.createRow(1 + i); 
                 
                 Item oItemMs = ItemTool.getItemByID(oItem.getItemId());
                 
                 iCol = 0;
                 createCell(oWB,oRow2,(short)iCol, oItem.getItemCode   ()); iCol++; 
                 createCell(oWB,oRow2,(short)iCol, oItem.getItemName   ()); iCol++; 
                 createCell(oWB,oRow2,(short)iCol, oItem.getDescription() + " " + oItemMs.getColor()); iCol++; 
                 
                 if(bFromList)
                 {
                	 double dChangeQty = oItem.getQty().setScale(iScale,4).doubleValue() - dQoH;                	 
                     createCell(oWB,oRow2,(short)iCol, new BigDecimal(dQoH).setScale(iScale,4).toString()); iCol++; 
                	 createCell(oWB,oRow2,(short)iCol, oItem.getQty().setScale(iScale,4).toString()); iCol++;                      
                     createCell(oWB,oRow2,(short)iCol, new BigDecimal(dChangeQty).setScale(iScale,4).toString()); iCol++; 
                 }
                 else
                 {
                	 createCell(oWB,oRow2,(short)iCol, oItem.getQty().setScale(iScale,4).toString()); iCol++;                 	 
                     createCell(oWB,oRow2,(short)iCol, ""); iCol++; 
                	 createCell(oWB,oRow2,(short)iCol, ""); iCol++; 
                 }
                 createCell(oWB,oRow2,(short)iCol, oItem.getUnitCode()); iCol++; 
                 createCell(oWB,oRow2,(short)iCol, oItemMs.getRackLocation()); iCol++; 
                 
                 if (bNoCost)
                 {
                	 createCell(oWB,oRow2,(short)iCol, bd_ZERO); iCol++;                 	 
                 }
                 else
                 {
                	 createCell(oWB,oRow2,(short)iCol, new BigDecimal(dCost).setScale(iScale,4).toString());  iCol++; 
                 }
             }        
        }
        return oWB;
    }
}

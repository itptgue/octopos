package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorBalance;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.AccountPayableTool;
import com.ssti.enterprise.pos.tools.financial.VendorBalanceTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class APValidator extends BaseValidator implements AppAttributes
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Vendor", "Debit", "Credit", "Activity Balance", "Current Balance"};
	static final int[] l1 = {5, 25, 20, 20, 20, 20};
	static final int[] a1 = {0, 0, 1, 1, 1, 1};

	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void validateBalance()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate AR Result : ", s1, l1, a1);			
		
		try
		{							
			List vData = VendorTool.getAllVendor();
			
			for (int i = 0; i < vData.size(); i++)
			{
				Vendor oData = (Vendor) vData.get(i);
				String sID = oData.getVendorId();
			
				Date dAsOf = oData.getAsDate();
				
				VendorBalance oBalance = VendorBalanceTool.getVendorBalance(sID);
				if (oBalance != null)
				{
					double dBaseBalance = oBalance.getApBalance().setScale(2,4).doubleValue();
					double dPrimeBalance = oBalance.getPrimeBalance().setScale(2,4).doubleValue();
					
					//get all AP transaction since beginning balance until now then recalculate 
				    List vTrans = AccountPayableTool.getTransactionHistory(dAsOf, dNow, sID, "", -1);
			        double[] dDebit = AccountPayableTool.filterTotalAmountByType (vTrans, 1);
				    double[] dCredit = AccountPayableTool.filterTotalAmountByType (vTrans, 2);
					double dTrans = dDebit[i_BASE] - dCredit[i_BASE];
					
					dTrans = (new BigDecimal(dTrans).setScale(2,4)).doubleValue();
					
					if (dBaseBalance != dTrans)
					{
						m_iInvalidResult ++;
						m_oResult.append("\n");
						
						append (null, l1[0]);
						append (StringUtil.cut(oData.getVendorName(),22,".."), l1[1]);
						append (new Double(dDebit[i_BASE]), l1[2]);
						append (new Double(dCredit[i_BASE]), l1[3]);
						
						append (new Double(dTrans), l1[4]);
						append (new Double(dBaseBalance), l1[5]);
					}	
				}
			}
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
}

package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.Date;
import java.util.List;

import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2015-09-17
 * - change findData in TransactionTool & DeliveryOrderTool add parameter SalesID to filter by SalesID
 * 
 * </pre><br>
 */
public class SalesReturnTransLookup extends TransactionSecureScreen
{   
	private static final String s_DO = "findDOResult";
	private static final String s_SI = "findSIResult";
	
    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		return true;
	}
    
    public void doBuildTemplate (RunData data, Context context)
    {	
    	try
		{
    		int iRetFrom = data.getParameters().getInt("ReturnFrom", -1);

			if (data.getParameters().getString("op", "").equals("first"))
			{
	    		getTransData (data, context, iRetFrom);				
			}
    		if (data.getUser().getTemp(s_DO) != null)
    		{
    			LargeSelectHandler.handleLargeSelectParameter (data, context, s_DO, "vDO");    		
    		}
    		if (data.getUser().getTemp(s_SI) != null)
    		{
    			LargeSelectHandler.handleLargeSelectParameter (data, context, s_SI, "vSI");    		
    		}
		}
		catch (Exception _oEx) 
		{
			data.setMessage("Error : " + _oEx.getMessage());
		}
    }

    private void getTransData (RunData data, Context context, int _iRetFrom ) 
    	throws Exception
    {
    	String sTransNo  = data.getParameters().getString ("TransactionNo","");
    	if(StringUtil.isNotEmpty(sTransNo))
    	{
    		sTransNo = sTransNo.toUpperCase();
    	}
    	Date dStart = CustomParser.parseDate(data.getParameters().getString ("TransactionDate"));
    	Date dEnd = null; if (dStart != null) dEnd = DateUtil.getEndOfDayDate(dStart);
    	String sCustomerID = data.getParameters().getString ("CustomerId");
    	String sLocID = data.getParameters().getString ("LocationId");    	
    	int iLimit = data.getParameters().getInt("viewLimit", 25);
    	
    	try 
    	{	
    		LargeSelect oLS = null;
    		List vTrans = null;
    		
    		//FROM DO
    		if(_iRetFrom == i_RET_FROM_PR_DO)
    		{	
    			oLS = DeliveryOrderTool.findData(i_TRANS_NO, sTransNo, dStart, dEnd, 
    				sCustomerID, sLocID, "", i_DO_DELIVERED, iLimit, -1, null);
    			
    			vTrans = oLS.getNextResults();
    			
    			if (vTrans != null && vTrans.size() == 1)
    			{
	    			DeliveryOrder oPR = (DeliveryOrder) vTrans.get(0);
    				context.put("sTransID", oPR.getDeliveryOrderId());
    			}
    			else
    			{
    				context.put("vDO", vTrans);
    				data.getUser().setTemp(s_DO, oLS);
    			}
    		}
    		
    		//IMPORT FROM SI
        	if(_iRetFrom == i_RET_FROM_PI_SI)
        	{
        		oLS = TransactionTool.findData(i_TRANS_NO, sTransNo, dStart, dEnd,
        			sCustomerID, sLocID, i_TRANS_PROCESSED, -1, iLimit, -1, "", "", ""); 

        		vTrans = oLS.getNextResults();

        		if (vTrans!= null && vTrans.size() == 1)
        		{
        			SalesTransaction oSI = (SalesTransaction) vTrans.get(0);
    				context.put("sTransID", oSI.getSalesTransactionId());
        		}
    			else 
    			{
    				context.put("vSI", vTrans);
    				data.getUser().setTemp(s_SI, oLS);
    			}
    		}
	  	}
    	catch (Exception _oEx) 
    	{
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    	}
    }
}

package com.ssti.enterprise.pos.tools.sales;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.DeliveryOrderDetailPeer;
import com.ssti.enterprise.pos.om.DeliveryOrderPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.om.SalesOrderDetailPeer;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionDetailPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.InventoryCostingTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.enterprise.pos.tools.journal.SalesJournalTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for DeliveryOrder and DeliveryOrderDetail OM
 * @see SalesOrderTool, DeliveryOrder, DeliveryOrderDetail, TransactionTool
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DeliveryOrderTool.java,v 1.24 2009/05/04 02:05:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: DeliveryOrderTool.java,v $
 * 
 * 2015-09-17
 * - change findData (called by DeliveryOrderAction and SalesReturnTransLookup) add parameter SalesID to filter by SalesID
 * 
 * 2015-04-29
 * - add method isDOInActiveSI (String _sDOID) to validate whether DO exists in another SI
 *   used in DefaultSalesTransaction
 * 
 * </pre><br>
 */
public class DeliveryOrderTool extends BaseTool 
{	
	private static Log log = LogFactory.getLog(DeliveryOrderTool.class);

	public static final String s_ITEM = new StringBuilder(DeliveryOrderPeer.DELIVERY_ORDER_ID).append(",")
	.append(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID).append(",")
	.append(DeliveryOrderDetailPeer.ITEM_ID).toString();
	
	public static final String s_TAX = new StringBuilder(DeliveryOrderPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.TAX_ID).toString();
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), DeliveryOrderPeer.DELIVERY_ORDER_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), DeliveryOrderPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER), 	 DeliveryOrderPeer.CUSTOMER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CUST_TYPE), 	 DeliveryOrderPeer.CUSTOMER_ID);		
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), DeliveryOrderPeer.CREATE_BY);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), DeliveryOrderPeer.CONFIRM_BY);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), DeliveryOrderPeer.LOCATION_ID);             
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), "");
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), "");
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), DeliveryOrderPeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), DeliveryOrderPeer.FOB_ID);		
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), DeliveryOrderPeer.CURRENCY_ID);
		
		addInv(m_FIND_PEER, s_ITEM);
	}   
	
	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), DeliveryOrderPeer.DELIVERY_ORDER_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 	
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), DeliveryOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), DeliveryOrderPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), DeliveryOrderPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), DeliveryOrderPeer.CREATE_BY);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), DeliveryOrderPeer.CONFIRM_BY);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), "");
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), DeliveryOrderPeer.CURRENCY_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALESMAN   ), DeliveryOrderPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), "");
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), DeliveryOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), DeliveryOrderPeer.LOCATION_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALES_ITEM ), DeliveryOrderPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_FOB 		  ), DeliveryOrderPeer.SALES_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_COURIER    ), DeliveryOrderPeer.SALES_ID);
	}	
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, _iSelected);}
		
	public static DeliveryOrder getHeaderByID(String _sID) 
		throws Exception
	{
		return getHeaderByID (_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return DeliveryOrder object
	 * @throws Exception
	 */
	public static DeliveryOrder getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderPeer.DELIVERY_ORDER_ID, _sID);
        List vData = DeliveryOrderPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (DeliveryOrder) vData.get(0);
		}
		return null;
	}
	
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
		return getDetailsByID(_sID, null);
	}

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of DeliveryOrderDetail
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, _sID);
        oCrit.addAscendingOrderByColumn(DeliveryOrderDetailPeer.INDEX_NO);
	    return DeliveryOrderDetailPeer.doSelect(oCrit, _oConn);
	}

	/**
	 * getByNo
	 * 
	 * @param _sNo
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static DeliveryOrder getByNo(String _sNo, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderPeer.DELIVERY_ORDER_NO, _sNo);
        List vData = DeliveryOrderPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
            return (DeliveryOrder) vData.get(0);
        }
        return null;    
    }
	
	/**
	 * delete transaction details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, _sID);
		DeliveryOrderDetailPeer.doDelete (oCrit, _oConn);
	}
		
	/**
	 * 
	 * @param _sID
	 * @return DeliveryOrderDetail
	 * @throws Exception
	 */
	public static DeliveryOrderDetail getDetailByDetailID(String _sID) 
		throws Exception
	{
	    return getDetailByDetailID(_sID, null);
	}		

	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return DeliveryOrderDetail
	 * @throws Exception
	 */
	public static DeliveryOrderDetail getDetailByDetailID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_DETAIL_ID, _sID);
	    List vData = DeliveryOrderDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) return (DeliveryOrderDetail) vData.get(0);
	    return null;
	}		
	
	/**
	 * used by SO Report
	 * 
	 * @param _sSOID
	 * @return List of DeliveryOrderDetail
	 * @throws Exception
	 */
	public static List getDetailsBySODetailID(String _sSOID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(DeliveryOrderDetailPeer.SALES_ORDER_DETAIL_ID,_sSOID);
		List vData = DeliveryOrderDetailPeer.doSelect (oCrit);
		return vData;
	}
	
	/**
	 * used by so to validate whether this SOID exist in any active DO
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return DeliveryOrder
	 * @throws Exception
	 */
	public static List getDOBySOID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderPeer.SALES_ORDER_ID, _sID);
        oCrit.add(DeliveryOrderPeer.STATUS, i_DO_CANCELLED, Criteria.NOT_EQUAL);
        return DeliveryOrderPeer.doSelect(oCrit,_oConn);
	}

	public static String getStatusString (int _iStatusID)
	{
		if (_iStatusID == i_DO_NEW) return LocaleTool.getString("new");
		if (_iStatusID == i_DO_DELIVERED) return LocaleTool.getString("confirmed");
		if (_iStatusID == i_DO_INVOICED) return LocaleTool.getString("invoiced");
		return LocaleTool.getString("cancelled");
	}	

	/** 
	 * method to check Paid Status on DO 
	 * foreach details in a DO, get all SI Detail by DO ID
	 * count number of item in SI whether >= Qty in DO
	 * if true then all item in DO already PAID
	 * 
	 * @param _sDOID
	 * @param _oConn
	 * @return is DO ivnoiced
	 */
	private static boolean checkPaidStatus(String _sDOID, Connection _oConn)
        throws Exception
	{
		List vDODetail = getDetailsByID(_sDOID, _oConn);
		List vSIDetail = TransactionTool.getDetailsByDOID(_sDOID, _oConn);

		for(int i = 0; i < vDODetail.size(); i++)
		{
			double dTotal = 0;
			double dReturned = 0;
			DeliveryOrderDetail oDODet = (DeliveryOrderDetail)vDODetail.get(i);
			
			dReturned = oDODet.getReturnedQty().doubleValue();
			
			for(int j = 0; j < vSIDetail.size(); j++)
			{
				SalesTransactionDetail oSIDet = (SalesTransactionDetail)vSIDetail.get(j);
				
				if( oDODet.getItemId() != null && oDODet.getItemId().equals(oSIDet.getItemId()))
				{
                    dTotal = dTotal + dReturned + oSIDet.getQty().doubleValue();
			    }
			}
			if(dTotal < oDODet.getQty().doubleValue())
			{
				return false;			
			}
		}
		return true;
	}
	
	/**
	 * update DO status
	 * 
	 * @param _sDOID
	 * @param _bInvoiced
	 * @param _oConn
	 * @throws Exception
	 */
	private static void changeStatus(String _sDOID, boolean _bInvoiced, Connection _oConn)
	    throws Exception
	{
		DeliveryOrder oDO = getHeaderByID(_sDOID, _oConn);		
		if(oDO != null)
		{
			oDO.setIsBill(_bInvoiced);
			if(_bInvoiced)
			{
			    oDO.setStatus(i_DO_INVOICED);
			}
			else
			{
			    oDO.setStatus(i_DO_DELIVERED);
		    }
			oDO.save(_oConn);
		}
	}
	
	/**
 	 * this is the method to update DO from invoice
 	 * first it will iterate through Invoice Detail then, foreach detail, it will 
 	 * take the inv detail DOID and DODetailID
 	 * secondly, it will get the DO Detail from database then update the DO detail billed Qty
 	 * third, calls checkPaidStatus, if paid then set the DO Status to PAID
	 *
	 * @param List _vSIDet, List of Invoice Detail
	 * @param Connection _oConn, Connection Object
	 */	
	public static void updateDOFromInvoice (List _vSIDet, boolean _bIsCancel, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		for(int i = 0; i < _vSIDet.size(); i++)
		{
			SalesTransactionDetail oTD = (SalesTransactionDetail) _vSIDet.get(i);

			String sSOID = oTD.getSalesOrderId();
			String sDOID = oTD.getDeliveryOrderId();
			String sDODetailID = oTD.getDeliveryOrderDetailId();
			
            //update Billed Qty if SI is not directly from SO
			if (StringUtil.isNotEmpty(sDODetailID) && !StringUtil.isEqual(sSOID,sDOID))
			{
			    DeliveryOrderDetail oDODet = getDetailByDetailID(sDODetailID, _oConn);
			    if (oDODet != null)
			    {
			    	double dBilledQty = oDODet.getBilledQty().doubleValue();
			    	if(!_bIsCancel)
			    	{
			    	    dBilledQty = dBilledQty + oTD.getQty().doubleValue();
			    	}
			    	else
			        {
			            dBilledQty = dBilledQty - oTD.getQty().doubleValue();
			        }
			    	oDODet.setBilledQty (new BigDecimal(dBilledQty));
			    	oDODet.save(_oConn);               
			    }
			}
            
   			//to check balance item on SO and SI
			//if the item already balanced then the SO is Paid
            if (StringUtil.isNotEmpty(sDOID) && !StringUtil.isEqual(sSOID,sDOID))
			{
			    //if is all items in DO already billed   
				if(checkPaidStatus(sDOID, _oConn) && !_bIsCancel) 
				{
                    changeStatus(sDOID, true, _oConn);
				}
				else
				{
				    changeStatus(sDOID, false, _oConn);
				}
			}             
		}
	}

	
	/**
	 * update DO returned qty 
	 * 
	 * @param vSRD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateReturnQty(List vSRD, boolean _bCancel, Connection _oConn)
		throws Exception
	{
		 try
		 {
			for(int i = 0; i < vSRD.size(); i++)
			{
				SalesReturnDetail oSRD = (SalesReturnDetail) vSRD.get(i);
				DeliveryOrderDetail oDODet = getDetailByDetailID(oSRD.getTransactionDetailId(), _oConn);
				if (oDODet != null)
				{
					double dReturnQty = oSRD.getQty().doubleValue();
					if (_bCancel) dReturnQty = dReturnQty * -1;
					double dLastReturnedQty = oDODet.getReturnedQty().doubleValue();
					oDODet.setReturnedQty(new BigDecimal(dLastReturnedQty + dReturnQty));
					oDODet.save(_oConn);
				}
			}
		 }
		 catch(Exception _oEx)
		 {
		 	_oEx.printStackTrace();
		 	throw new NestableException ("Update Returned Qty Failed, ERROR : ", _oEx);
		 }
	}

	/**
	 * setHeaderProperties
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param data
	 * @throws Exception
	 */
    public static void setHeaderProperties (DeliveryOrder _oDO, List _vDOD, RunData data)
    	throws Exception
    {
    	
		try
		{
			_oDO.setCustomerName(CustomerTool.getCustomerNameByID(_oDO.getCustomerId()) );			
			if(data != null)
			{
				ValueParser formData = data.getParameters();
				formData.setProperties (_oDO);
				_oDO.setDeliveryOrderDate( CustomParser.parseDate(formData.getString("DeliveryOrderDate")));
				_oDO.setCustomerPoDate( CustomParser.parseDate(formData.getString("CustomerPoDate")));
				
				if( _oDO.getStatus() != i_DO_NEW )
				{
					_oDO.setConfirmDate ( CustomParser.parseDate(formData.getString("ConfirmDate")) );
				}
				
				if( formData.getInt("Status") != i_DO_DELIVERED &&
					formData.getInt("Status") != i_DO_CANCELLED) 
				{
					_oDO.setCreateDate ( new Date () );
				}	
			}
    		_oDO.setTotalQty ( new BigDecimal(countQty (_vDOD)) );	
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set DO Failed : " + _oEx.getMessage (), _oEx); 
		}
    }

    /**
     * count all qty in details
     * 
     * @param _vDetails
     * @return total qty
     */
	public static double countQty (List _vDetails)
		throws Exception
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			DeliveryOrderDetail oDet = (DeliveryOrderDetail) _vDetails.get(i);
			if (oDet.getQtyBase() == null) //if from add detail
			{
				oDet.setQtyBase (UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty()));
			}
			dQty += oDet.getQtyBase().doubleValue();
		}
		return dQty;
	}
	
	/**
	 * update detail 
	 * 
	 * @param _vDet
	 * @param data
	 * @throws Exception
	 */
    public static void updateDetail (List _vDet, RunData data)
    	throws Exception
    {
    	ValueParser formData = data.getParameters();
		try
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				DeliveryOrderDetail oDetail = (DeliveryOrderDetail) _vDet.get(i);
				log.debug ("DO Update Detail BEFORE : "  + oDetail);
				
				oDetail.setQty 		(formData.getBigDecimal("Qty" + (i + 1)) );
				oDetail.setQtyBase 	(UnitTool.getBaseQty(oDetail.getItemId(), oDetail.getUnitId(), oDetail.getQty()));				
				oDetail.setSubTotal (formData.getBigDecimal("SubTotal" + (i + 1)) );

				log.debug ("DO Update Detail AFTER : "  + oDetail);
				_vDet.set (i, oDetail);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Update DO Detail Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
	
    /**
     * map SalesOrder to DeliveyOrder
     * 
     * @param _oSO
     * @param _oDO
     * @throws Exception
     */
	public static void mapSOHeaderToDOHeader (SalesOrder _oSO, DeliveryOrder _oDO)
		throws Exception
	{
		String sLocationID = _oSO.getLocationId();
		if (StringUtil.isEmpty(sLocationID)) sLocationID = PreferenceTool.getLocationID();
		Location oLocation = LocationTool.getLocationByID (sLocationID);
		
		_oDO.setSalesOrderId		(_oSO.getSalesOrderId	      ());	
		_oDO.setCustomerPoNo		(_oSO.getCustomerPoNo         ());
		_oDO.setCustomerPoDate		(_oSO.getCustomerPoDate		  ());
		
        _oDO.setLocationId			(oLocation.getLocationId   	  ());
        _oDO.setLocationName      	(oLocation.getLocationName 	  ());
	    _oDO.setDeliveryOrderDate	(new Date());
        _oDO.setCustomerId			(_oSO.getCustomerId 	     ());
        _oDO.setCustomerName		(_oSO.getCustomerName 	     ());
        _oDO.setTotalQty			(_oSO.getTotalQty		     ());
        _oDO.setCreateBy			(_oSO.getUserName            ());
        _oDO.setRemark				(_oSO.getRemark              ());
        _oDO.setTotalDiscountPct	(_oSO.getTotalDiscountPct    ());
        _oDO.setPaymentTypeId		(_oSO.getPaymentTypeId       ());
        _oDO.setPaymentTermId		(_oSO.getPaymentTermId       ());
        _oDO.setCurrencyId		    (_oSO.getCurrencyId          ());
        _oDO.setCurrencyRate		(_oSO.getCurrencyRate        ());
        _oDO.setShipTo      		(_oSO.getShipTo              ());
        _oDO.setSalesId      		(_oSO.getSalesId			 ());
        _oDO.setCourierId   		(_oSO.getCourierId           ());
        _oDO.setFobId          		(_oSO.getFobId               ());
        _oDO.setEstimatedFreight	(_oSO.getEstimatedFreight	 ());
        _oDO.setIsTaxable			(_oSO.getIsTaxable			 ());
        _oDO.setIsInclusiveTax		(_oSO.getIsInclusiveTax		 ());
        _oDO.setFreightAccountId	(_oSO.getFreightAccountId    ());
        _oDO.setIsInclusiveFreight	(_oDO.getIsInclusiveFreight  ());
        
        _oDO.setStatus 			    (i_DO_NEW);
	}
	
	/**
	 * map SalesOrderDetail to DeliveyOrderDetail
	 * 
	 * @param _oSODet
	 * @param _oDet
	 * @throws Exception
	 */
	public static void mapSODetailToDODetail (SalesOrderDetail _oSODet, 
											  DeliveryOrderDetail _oDet,
											  String _sLocID)
		throws Exception
	{
		_oDet.setSalesOrderDetailId	 (_oSODet.getSalesOrderDetailId());	
		_oDet.setIndexNo     (_oSODet.getIndexNo());
        _oDet.setItemId      (_oSODet.getItemId());
        _oDet.setItemCode    (_oSODet.getItemCode());
        _oDet.setItemName    (_oSODet.getItemName());
        _oDet.setDescription (_oSODet.getDescription());        
        _oDet.setUnitId	     (_oSODet.getUnitId());
        _oDet.setUnitCode    (_oSODet.getUnitCode());
        
        double dNewQty = _oSODet.getQty().doubleValue()-_oSODet.getDeliveredQty().doubleValue();
        BigDecimal dQtyOH = InventoryLocationTool.getInventoryOnHand(_oDet.getItemId(), _sLocID);
        _oDet.setQtyOH(dQtyOH);
        if (dQtyOH.doubleValue() >= 0 && dQtyOH.doubleValue() < dNewQty)
        {
        	dNewQty = dQtyOH.doubleValue();
        }

        _oDet.setQty			(new BigDecimal(dNewQty));        
        _oDet.setItemPriceInSo	(_oSODet.getItemPrice());
        _oDet.setTaxId		    (_oSODet.getTaxId());
        _oDet.setTaxAmount      (_oSODet.getTaxAmount());
        _oDet.setDiscount       (_oSODet.getDiscount());
        _oDet.setSubTotal		(_oSODet.getSubTotal());
        _oDet.setCostPerUnit	(_oSODet.getCostPerUnit());   
        _oDet.setDepartmentId	(_oSODet.getDepartmentId());		
	}

	/**
	 * get the latest item cost for this DO
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void recalculateCost (DeliveryOrder _oTR, List _vTD, Map _mGroup, Connection _oConn) 
		throws Exception
	{		
		for ( int i = 0; i < _vTD.size(); i++ ) 
		{
			DeliveryOrderDetail oTD = (DeliveryOrderDetail) _vTD.get (i);
			Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
			if (oItem.getItemType() == i_INVENTORY_PART)
			{
				oTD.setCostPerUnit(InventoryLocationTool.getItemCost(oTD.getItemId(), _oTR.getLocationId(), 
													  			     _oTR.getDeliveryOrderDate(), _oConn));
			}
			else if (oItem.getItemType() == i_GROUPING)
			{
				List vGroup = ItemGroupTool.getItemGroupByGroupID(oTD.getItemId(), _oConn);
				double dGroupCost = 0;
				for (int j = 0; j < vGroup.size(); j++)
				{
					ItemGroup oGroupPart = (ItemGroup) vGroup.get(j);
					BigDecimal dCost = InventoryLocationTool.getItemCost(oGroupPart.getItemId(), 
							  											 _oTR.getLocationId(), 
							  											 _oTR.getDeliveryOrderDate(), _oConn);
					dGroupCost += dCost.doubleValue();
					oGroupPart.setCost(dCost);
				}
				_mGroup.put(oTD.getItemId(), vGroup);
				oTD.setCostPerUnit(new BigDecimal(dGroupCost));
			}
		}
	}	
	
	/**
	 * generate transaction no
	 * 
	 * @param _oDO
	 * @param _oConn
	 * @return transaction no
	 * @throws Exception
	 */
	private static String generateTransactionNo (DeliveryOrder _oDO, Connection _oConn) 
		throws Exception
	{
		String sTransNo = "";
		String sLocCode = "";
		if (!PreferenceTool.syncInstalled())
		{
			sLocCode = LocationTool.getLocationCodeByID(_oDO.getLocationId(), _oConn);
		}

		int iType = LastNumberTool.i_DELIVERY_ORDER;
		String sFormat = s_DO_FORMAT;		
		
		SalesOrder oSO = SalesOrderTool.getHeaderByID(_oDO.getSalesOrderId(), _oConn);
		if (TransactionAttributes.b_SALES_SEPARATE_TAX_INV)
		{
			if (oSO != null && oSO.getTotalTax().doubleValue() > 0)
			{
				iType = LastNumberTool.i_TAX_DO;
				sFormat = s_TD_FORMAT;
			}
		}
		sTransNo = LastNumberTool.generateByCustomer(_oDO.getCustomerId(),_oDO.getLocationId(),sLocCode,sFormat,iType,_oConn);
		return sTransNo;
	}
	
	/**
	 * save Data
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param _oConn
	 * @param _bCloseSO
	 * @throws Exception
	 */
	public static void saveData (DeliveryOrder _oDO,
								 List _vDOD,
								 Connection _oConn,
								 boolean _bCloseSO)
		throws Exception
	{
		boolean bNew = false;
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}			
		try
		{
			if(StringUtil.isEmpty(_oDO.getDeliveryOrderId())) bNew = true;		

			Map mGroup = new HashMap();
			
			_oDO.setDeliveryOrderDate(checkBackDate(_oDO.getDeliveryOrderDate(), i_INV_OUT));
			
			recalculateCost (_oDO, _vDOD, mGroup, _oConn);
			validateDate (_oDO.getDeliveryOrderDate(), _oConn);			
			processSaveDOData (_oDO, _vDOD, _oConn);
			
			if(_oDO.getStatus() == i_DO_DELIVERED)
			{
				//Update Inventory Transaction
                InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
                oCT.createFromDO(_oDO, _vDOD, mGroup);
				
				//Update SO From DO
    			SalesOrderTool.updateSOFromDO(_vDOD,_oDO.getSalesOrderId(),_bCloseSO, false, _oConn);
    		
                //Journal To GL
				if (b_USE_GL && PreferenceTool.getLocationFunction() != i_STORE)
				{	
					SalesJournalTool oSalesJournal = new SalesJournalTool(_oDO, _oConn);
					oSalesJournal.createDOJournal (_oDO, _vDOD, mGroup);
				}
			}
            if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx)
		{
			_oDO.setStatus(i_TRANS_PENDING);
			_oDO.setDeliveryOrderNo("");						
			if (bNew)
			{
				_oDO.setNew(true);
				_oDO.setDeliveryOrderId("");
			}			
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * process save do data
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveDOData (DeliveryOrder _oDO, List _vDOD, Connection _oConn) 
		throws Exception
	{
        if(StringUtil.isEmpty(_oDO.getDeliveryOrderId()))
		{
	 	    _oDO.setDeliveryOrderId (IDGenerator.generateSysID());
	 	    _oDO.setNew (true);
	 	}
        
        validateID (getHeaderByID(_oDO.getDeliveryOrderId(), _oConn), "deliveryOrderNo");

		//set create date when null
		if(_oDO.getCreateDate() == null)_oDO.setCreateDate(new Date());

	    //get location where item deliver
		String sLocID = _oDO.getLocationId();		
		if (StringUtil.isEmpty(sLocID)) sLocID = PreferenceTool.getLocationID();
		Location oLocation = LocationTool.getLocationByID (sLocID);		
   	 	_oDO.setLocationName (oLocation.getLocationName());
   	 	String sLocCode = oLocation.getLocationCode();
   	 	
   	 	if (_oDO.getDeliveryOrderNo() == null) _oDO.setDeliveryOrderNo ("");
        if(_oDO.getStatus() == i_DO_DELIVERED && StringUtil.isEmpty(_oDO.getDeliveryOrderNo()))
        {
        	_oDO.setDeliveryOrderNo (generateTransactionNo(_oDO, _oConn));
        	validateNo(DeliveryOrderPeer.DELIVERY_ORDER_NO,_oDO.getDeliveryOrderNo(),
        			   DeliveryOrderPeer.class, _oConn);
		}
        if(_oDO.getStatus() == i_DO_NEW)
        {
	    	_oDO.setConfirmBy ("");
        }
		else
		{
        	_oDO.setConfirmDate( new Date());		    
	    }
		_oDO.save (_oConn);

		//delete all existing detail from pending transaction
		if (StringUtil.isNotEmpty(_oDO.getDeliveryOrderId()))
		{
			deleteDetailsByID (_oDO.getDeliveryOrderId(), _oConn);
		}			
		
		renumber(_vDOD);	
		for ( int i = 0; i < _vDOD.size(); i++ )
		{
            DeliveryOrderDetail oTD = (DeliveryOrderDetail) _vDOD.get (i);
			oTD.setModified (true);
			oTD.setNew (true);
            
			if(StringUtil.isEmpty(oTD.getDeliveryOrderDetailId()))
            {
            	oTD.setDeliveryOrderDetailId (IDGenerator.generateSysID());
            }
			oTD.setQtyOH(InventoryLocationTool.getInventoryOnHand(oTD.getItemId(), _oDO.getLocationId(), _oConn));
            oTD.setReturnedQty(bd_ZERO);
            oTD.setBilledQty(bd_ZERO);
            oTD.setDeliveryOrderId (_oDO.getDeliveryOrderId());
            
            //double dQtyBase = UnitTool.convertQtyToBaseUnit(oTD.getQty().doubleValue(),oTD.getUnitId());
			//oTD.setQtyBase (new BigDecimal(dQtyBase));            
            oTD.save (_oConn);
            
			if (_oDO.getStatus() == i_DO_DELIVERED && oTD.getQty().doubleValue() > 0)
			{
				//validate serial no
				ItemSerialTool.validateSerial(oTD.getItemId(), oTD.getQty().doubleValue(), oTD.getSerialTrans(), _oConn);
			}

            if(_oDO.getStatus() == i_DO_DELIVERED || _oDO.getStatus() == i_DO_CANCELLED)
            {
            	//if DO qty <= 0 then delete the detail
                if(oTD.getQty().doubleValue() == 0)
                {
                    _vDOD.get(i);
                    Criteria oCrit = new Criteria();
		            oCrit.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_DETAIL_ID, oTD.getDeliveryOrderDetailId());
		            DeliveryOrderDetailPeer.doDelete(oCrit,_oConn);
                }
            }
		}
	}

	/**
	 * cancel do
	 * 
	 * @param _oDO
	 * @param _vDOD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelDO (DeliveryOrder _oDO,
				 				 List _vDOD,
								 String _sCancelBy,
								 Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try
		{
			validateDate (_oDO.getDeliveryOrderDate(), _oConn);
			
			if (!TransactionTool.isInvoiced(_oDO.getDeliveryOrderId(), _oConn))
			{
				if (!SalesReturnTool.isReturned(_oDO.getDeliveryOrderId(), _oConn))
				{
					//rollback inventory
					InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
					oCT.delete(_oDO, _oDO.getDeliveryOrderId(), i_INV_TRANS_DELIVERY_ORDER);
					
					//update so 
					SalesOrderTool.updateSOFromDO(_vDOD,_oDO.getSalesOrderId(),false, true, _oConn);
					
					//rollback and delete gl journal
					if (b_USE_GL)
					{
						SalesJournalTool.deleteJournal(
						    GlAttributes.i_GL_TRANS_DELIVERY_ORDER,_oDO.getDeliveryOrderId(),_oConn
						);
					}
					_oDO.setCancelBy(_sCancelBy);
					_oDO.setCancelDate(new Date());
					_oDO.setStatus(i_DO_CANCELLED);
					_oDO.setRemark(cancelledBy(_oDO.getRemark(), _sCancelBy));				
					_oDO.save(_oConn);
				}
				else
				{
					throw new Exception ("DO is already Returned ");
				}
			}
			else
			{
				throw new Exception ("DO is already Invoiced ");
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * update print times indicate how many times this invoice printed
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
    public static void updatePrintTimes (DeliveryOrder _oTR) 
		throws Exception
	{
		_oTR.setPrintTimes (_oTR.getPrintTimes() + 1);
		_oTR.save();
	}
	
    /**
     * 
     * @param _iCond
     * @param _sKeywords
     * @param _dStart
     * @param _dEnd
     * @param _sCustomerID
     * @param _sLocationID
     * @param _iStatus
     * @param _iLimit
     * @param _iGroupBy
     * @param _sCurrencyID
     * @return query result in LargeSelect
     * @throws Exception
     */
    public static LargeSelect findData (int _iCond, 
    									String _sKeywords, 
										Date _dStart, 
										Date _dEnd, 
    							        String _sCustomerID, 
										String _sLocationID, 
										String _sSalesID,
										int _iStatus, 
										int _iLimit,
										int _iGroupBy,
										String _sCurrencyID) 
      	throws Exception
      {
    	Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
            	DeliveryOrderPeer.DELIVERY_ORDER_DATE, _dStart, _dEnd, _sCurrencyID);
    	
    	if (StringUtil.isNotEmpty(_sCustomerID)) 
    	{
    		oCrit.add(DeliveryOrderPeer.CUSTOMER_ID, _sCustomerID);	
    	}
    	if (_iStatus > 0) 
    	{
    		oCrit.add(DeliveryOrderPeer.STATUS, _iStatus);	
    	}		
    	if (StringUtil.isNotEmpty(_sLocationID)) 
    	{
    		oCrit.add(DeliveryOrderPeer.LOCATION_ID, _sLocationID);	
    	}		
		if (StringUtil.isNotEmpty(_sSalesID)) 
		{
			oCrit.add(DeliveryOrderPeer.SALES_ID, _sSalesID);	
		}    	
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}
		else
		{
			oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.DELIVERY_ORDER_DATE);
		}
		log.debug(oCrit);
    	return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.DeliveryOrderPeer");
    }
	
    public static boolean isDOInActiveSI(String _sDOID)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(SalesTransactionDetailPeer.DELIVERY_ORDER_ID, _sDOID);
    	oCrit.addJoin(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, SalesTransactionPeer.SALES_TRANSACTION_ID);
    	oCrit.add(SalesTransactionPeer.STATUS, i_PENDING);
    	oCrit.or(SalesTransactionPeer.STATUS, i_PROCESSED);
    	List vData = SalesTransactionDetailPeer.doSelect(oCrit);
    	if(vData.size() > 0)
    	{
    		return true;
    	}
    	return false;    	
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods
	/////////////////////////////////////////////////////////////////////////////////////
 
    /**
     * 
     * @param _iCond
     * @param _sKeywords
     * @param _dStart
     * @param _dEnd
     * @param _sCustomerID
     * @param _sLocationID
     * @param _iStatus
     * @param _iLimit
     * @param _iGroupBy
     * @param _sCurrencyID
     * @return query result in LargeSelect
     * @throws Exception
     */
    public static List findTrans(int _iCond, 
								 String _sKeywords, 
								 Date _dStart, 
								 Date _dEnd, 
						         String _sCustomerID, 
								 String _sLocationID, 
								 int _iStatus, 
								 int _iLimit,
								 int _iGroupBy,
								 String _sCurrencyID) 
      	throws Exception
      {
    	Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
            	DeliveryOrderPeer.DELIVERY_ORDER_DATE, _dStart, _dEnd, _sCurrencyID);
    	
    	if (StringUtil.isNotEmpty(_sCustomerID)) 
    	{
    		oCrit.add(DeliveryOrderPeer.CUSTOMER_ID, _sCustomerID);	
    	}
    	if (_iStatus > 0) 
    	{
    		oCrit.add(DeliveryOrderPeer.STATUS, _iStatus);	
    		if(_iStatus == 99)
    		{
        		oCrit.add(DeliveryOrderPeer.STATUS, i_DO_DELIVERED);	
        		oCrit.or(DeliveryOrderPeer.STATUS, i_DO_INVOICED);    		    			
    		}
    	}		
    	else
    	{
    		oCrit.add(DeliveryOrderPeer.STATUS, i_DO_DELIVERED);	
    		oCrit.or(DeliveryOrderPeer.STATUS, i_DO_INVOICED);    		
    	}
    	if (StringUtil.isNotEmpty(_sLocationID)) 
    	{
    		oCrit.add(DeliveryOrderPeer.LOCATION_ID, _sLocationID);	
    	}		
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}
		else
		{
			oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.DELIVERY_ORDER_DATE);
		}
		log.debug(oCrit);
		return DeliveryOrderPeer.doSelect(oCrit);
    }

    
    public static List getTransactionInDateRange (Date _dStart, Date _dEnd, int _iGroupBy, 
                                                  String _sKeywords, int _iOrderBy)
      	throws Exception
	{
    	Criteria oCrit = new Criteria();
    	oCrit.add(DeliveryOrderPeer.DELIVERY_ORDER_DATE, DateUtil.getStartOfDayDate( _dStart), Criteria.GREATER_EQUAL);
    	oCrit.and(DeliveryOrderPeer.DELIVERY_ORDER_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
    	if(_iGroupBy == 2)
    	{
    		oCrit.add(DeliveryOrderPeer.CUSTOMER_NAME, (Object) SqlUtil.like 
    			(DeliveryOrderPeer.CUSTOMER_NAME, _sKeywords,false,true), Criteria.CUSTOM);
    	}
    	if(_iGroupBy == 4)
    	{
    		oCrit.add(LocationPeer.LOCATION_NAME, (Object) SqlUtil.like 
    			(LocationPeer.LOCATION_NAME, _sKeywords,false,true), Criteria.CUSTOM);
    		oCrit.addJoin(DeliveryOrderPeer.LOCATION_ID,LocationPeer.LOCATION_ID);
    	}
    	if(_iGroupBy == 6)
    	{
    		oCrit.add(DeliveryOrderPeer.CREATE_BY, (Object) SqlUtil.like 
    			(DeliveryOrderPeer.CREATE_BY, _sKeywords,false,true), Criteria.CUSTOM);
    	}
    	
    	if(_iOrderBy == 1)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.DELIVERY_ORDER_NO);
    	}
    	if(_iOrderBy == 2)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.CUSTOMER_NAME);
    	}
    	if(_iOrderBy == 3)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.CREATE_BY);
    	}
    	
    	if(_iOrderBy == 4)
    	{
    		oCrit.addAscendingOrderByColumn(DeliveryOrderPeer.CONFIRM_BY);
    	}
    	
    	log.debug(oCrit);
    	return DeliveryOrderPeer.doSelect(oCrit);
	}

	public static List getDOByStatusAndCustomerID (String _sCustomerID, 
												   int _iStatus, 
												   boolean _bPaidStatus, 
												   String _sCurrencyID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DeliveryOrderPeer.CUSTOMER_ID, _sCustomerID);
        oCrit.add(DeliveryOrderPeer.STATUS, _iStatus);
       	oCrit.add(DeliveryOrderPeer.IS_BILL, _bPaidStatus);
       	oCrit.add(DeliveryOrderPeer.CURRENCY_ID, _sCurrencyID);
       	oCrit.or(DeliveryOrderPeer.CURRENCY_ID, "");
        return DeliveryOrderPeer.doSelect(oCrit);
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//report method to group data by details
	/////////////////////////////////////////////////////////////////////////////////////
	
    public static List getTransDetails (List _vDO)
    	throws Exception
    {
        return getTransDetails(_vDO,"");
    }
    
	public static List getTransDetails (List _vDO, String _sItemName)
    	throws Exception
    {
		if (_vDO == null) return new ArrayList(1);
		
		List vDOID = new ArrayList (_vDO.size());
		for (int i = 0; i < _vDO.size(); i++)
		{
			DeliveryOrder oDO = (DeliveryOrder) _vDO.get(i);
			vDOID.add (oDO.getDeliveryOrderId());
		}
		Criteria oCrit = new Criteria();
		if(StringUtil.isNotEmpty(_sItemName))
		{
		    oCrit.add(DeliveryOrderDetailPeer.ITEM_NAME,
		        (Object) SqlUtil.like (DeliveryOrderDetailPeer.ITEM_NAME, _sItemName,false,true), Criteria.CUSTOM);
		}
        if(vDOID.size()>0)
        {
		    oCrit.addIn (DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, vDOID);
		}
		return DeliveryOrderDetailPeer.doSelect(oCrit);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//end report methods
	/////////////////////////////////////////////////////////////////////////////////////

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(DeliveryOrderPeer.class, DeliveryOrderPeer.DELIVERY_ORDER_ID, _sID, _bIsNext);
	}	  
	//end next prev
	
	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
		
	/**
	 * get List of DeliveryOrder created since last store 2 ho
	 * 
	 * @return List of DeliveryOrder created since last store 2 ho
	 * @throws Exception
	 */
	public static List getStoreDeliveryOrder ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate != null) 
		{
        	oCrit.add(DeliveryOrderPeer.CONFIRM_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
    	oCrit.add(DeliveryOrderPeer.STATUS, i_DO_DELIVERED);
		return DeliveryOrderPeer.doSelect(oCrit);
	}
}
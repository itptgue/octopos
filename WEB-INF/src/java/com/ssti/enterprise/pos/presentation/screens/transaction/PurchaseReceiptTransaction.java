package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.VendorPriceListDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/PurchaseReceiptTransaction.java,v $
 * Purpose: this class used as purchase receipt screen handler 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseReceiptTransaction.java,v 1.38 2009/05/04 02:02:36 albert Exp $
 * @see PurchaseReceipt
 *
 * $Log: PurchaseReceiptTransaction.java,v $
 * Revision 1.38  2009/05/04 02:02:36  albert
 * *** empty log message ***
 *
 * Revision 1.37  2008/03/13 03:07:08  albert
 * *** empty log message ***
 *
 * Revision 1.36  2007/11/09 01:13:28  albert
 * *** empty log message ***
 *
 * Revision 1.35  2007/07/10 04:49:17  albert
 * *** empty log message ***
 *
 * Revision 1.34  2007/05/30 05:10:23  albert
 * *** empty log message ***
 */

public class PurchaseReceiptTransaction extends PurchaseSecureScreen
{
    protected static final String[] a_PERM = {"Purchase Transaction", "View Purchase Receipt"};

    protected static final Log log = LogFactory.getLog(PurchaseReceiptTransaction.class);

    protected static final int i_OP_ADD_DETAIL  = 1;
    protected static final int i_OP_DEL_DETAIL  = 2;
	protected static final int i_OP_NEW_TRANS   = 3;
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details
	protected static final int i_OP_ADD_ITEMS   = 5;
	protected static final int i_OP_RELOAD      = 6;
	protected static final int i_OP_IMPORT_PO   = 7;
	protected static final int i_OP_COPY_DETAIL = 8; //to split row for batch purpose	
	
	protected static final String s_ALLOW_QTY_GTPO = "bAllowGTPO";
	
    protected PurchaseReceipt oPR = null;
    protected List vPRD = null;
    protected HttpSession oPRSes;

    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		return isAuthorized (data, a_PERM);
	}

    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH)
			{
				addDetailData (data);
			}
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH)
			{
				delDetailData (data);
			}
			else if ( iOp == i_OP_NEW_TRANS)
			{
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS)
			{
				getData (data);
			}
			else if (iOp == i_OP_IMPORT_PO && !b_REFRESH)
			{
				importPOData (data);
			}
    		else if (iOp == i_OP_ADD_ITEMS && !b_REFRESH)
			{
				addItems (data);
			}		
    		else if (iOp == i_OP_RELOAD)
			{
			}
    		else if (iOp == i_OP_COPY_DETAIL && !b_REFRESH)
			{
				if(PreferenceTool.useBatchNo())
				{
					copyDetail(data);
				}
			}
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_ALLOW_QTY_GTPO, PreferenceTool.getPurchPrQtyGtPo());
		context.put(s_PRD, oPRSes.getAttribute (s_PRD));
    	context.put(s_PR, oPRSes.getAttribute (s_PR));
    }

	protected void initSession ( RunData data )
	{
		synchronized (this)
		{
			oPRSes = data.getSession();
			if ( oPRSes.getAttribute (s_PR) == null )
			{
				oPRSes.setAttribute (s_PR, new PurchaseReceipt());
				oPRSes.setAttribute (s_PRD, new ArrayList());
			}
			oPR = (PurchaseReceipt) oPRSes.getAttribute (s_PR);
			vPRD = (List) oPRSes.getAttribute (s_PRD);
		}
	}

    protected void addDetailData ( RunData data )
    	throws Exception
    {
    	PurchaseReceiptTool.updateDetail(vPRD,data);
    	String sItemID = data.getParameters().getString("ItemId");
    	double dQty = data.getParameters().getDouble("Qty");
    	processAddItem (sItemID, dQty, data);
    }

    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	int iDelNo = data.getParameters().getInt("No") - 1;
    	PurchaseReceiptTool.updateDetail(vPRD,data);	
		if (vPRD.size() > iDelNo)
		{
			vPRD.remove (iDelNo);
			oPRSes.setAttribute  ( s_PRD, vPRD );
			PurchaseReceiptTool.setHeaderProperties ( oPR, vPRD, data );
			oPRSes.setAttribute ( s_PR, oPR );
		}
    }

    protected void createNewTrans ( RunData data )
    {
    	synchronized(this)
    	{
    		oPR = new PurchaseReceipt();
    		vPRD = new ArrayList();
    		oPRSes.setAttribute (s_PR,  oPR);
    		oPRSes.setAttribute (s_PRD, vPRD);
    	}
    }

    protected boolean isExist (PurchaseReceiptDetail _oPRD)
    	throws Exception
    {
    	for (int i = 0; i < vPRD.size(); i++)
    	{
    		PurchaseReceiptDetail oExist = (PurchaseReceiptDetail) vPRD.get (i);
    		if ( oExist.getItemCode().equals(_oPRD.getItemCode()) && 
    			(oExist.getItemId().equals(_oPRD.getItemId())) && 
    			(oExist.getDescription().equals(_oPRD.getDescription())) && 
				(oExist.getPurchaseOrderDetailId().equals("")) && 
				(oExist.getUnitId().equals(_oPRD.getUnitId())) ) 
    		{
       		    if(b_PURCHASE_MERGE_DETAIL)
    		    {
				    mergeData (oExist, _oPRD);
				    return true;
    		    }				
    		}
    	}
    	return false;
    }

    protected void mergeData (PurchaseReceiptDetail _oOldTD, PurchaseReceiptDetail _oNewTD)
    	throws Exception
    {    	
    	double dQty 	= _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
    	double dQtyBase = _oOldTD.getQtyBase().doubleValue() + _oNewTD.getQtyBase().doubleValue();

    	_oOldTD.setQty 	   ( new BigDecimal (dQty) );    
    	_oOldTD.setQtyBase ( new BigDecimal (dQtyBase) );    
    	
		double dTotal = _oOldTD.getItemPriceInPo().doubleValue() * dQty;
		double dLastPurchase = dTotal / dQty;
		double dTax = dTotal * _oOldTD.getTaxAmount().doubleValue() / 100;
		dTotal = dTotal + dTax;
		
	    _oOldTD.setLastPurchase (new BigDecimal (dLastPurchase));
	    _oOldTD.setSubTotal  	(new BigDecimal (dTotal));
    	_oNewTD = null;
    }

    protected void getData ( RunData data )
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID))
    	{
    		try
    		{
    			synchronized (this) 
    			{
        			oPR = PurchaseReceiptTool.getHeaderByID(sID);
        			vPRD = PurchaseReceiptTool.getDetailsByID (sID);
        			
        			//load tmp batch
        			if (oPR != null && oPR.getStatus() == i_PENDING)
        			{
        				BatchTransactionTool.setTmpBatch(sID, i_INV_TRANS_PURCHASE_RECEIPT, vPRD, null);
        			}
        			
        			oPRSes.setAttribute (s_PR, oPR);
    				oPRSes.setAttribute (s_PRD, vPRD);
				}
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }
    
	protected void importPOData ( RunData data )
    	throws Exception
    {
        String sPONo = data.getParameters().getString ("PurchaseOrderNo");
        String sPOID = "";
        if (StringUtil.isNotEmpty(sPONo))
        {
            PurchaseOrder oPO = PurchaseOrderTool.getByNoAndStatus(sPONo,i_PO_ORDERED);
            if (oPO != null) sPOID = oPO.getPurchaseOrderId();
            else data.setMessage("Purchase Order " + sPONo + " Not Found");
        }
        if (StringUtil.isEmpty(sPOID))
        {
            sPOID = data.getParameters().getString ("PoId");        
        }
    	if (StringUtil.isNotEmpty(sPOID))
    	{
    		try
    		{
    			PurchaseOrder oPO = PurchaseOrderTool.getHeaderByID (sPOID);
    			List vPOD = PurchaseOrderTool.getDetailsByID (sPOID, i_PURCHASE_SORT_BY, null);
    			oPR = new PurchaseReceipt();
    			data.getParameters().setProperties(oPR);
    			
    			if (DateUtil.isBefore(oPR.getReceiptDate(), oPO.getTransactionDate()))
    			{
    	    		data.setMessage(LocaleTool.getString("warn_import_date"));
    			}
    			PurchaseReceiptTool.mapPOHeaderToPRHeader (oPO, oPR);
    			oPR.setCreateBy(data.getUser().getName());
    			vPRD = new ArrayList (vPOD.size());
    			double dTotalQty = 0;
    			for (int i = 0; i < vPOD.size(); i++)
    			{
    				
    				PurchaseOrderDetail  oPOD = (PurchaseOrderDetail) vPOD.get(i);
    				PurchaseReceiptDetail oPRD = new PurchaseReceiptDetail();
    				PurchaseReceiptTool.mapPODetailToPRDetail (oPO, oPR, oPOD, oPRD);
    				dTotalQty = dTotalQty + oPRD.getQty().doubleValue();
       				vPRD.add (oPRD);
    			}
    			oPR.setTotalQty(new BigDecimal(dTotalQty));
    			oPRSes.setAttribute (s_PR, oPR);
				oPRSes.setAttribute (s_PRD, vPRD);
    		}
    		catch (Exception _oEx)
    		{
    			_oEx.printStackTrace();
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }
    
    protected void addItems(RunData data)
    	throws Exception
    {
    	//save current updated qty first
    	PurchaseReceiptTool.updateDetail(vPRD,data);
		
		String sItemID = data.getParameters().getString ("items");		
		log.debug (sItemID);
		if (StringUtil.isNotEmpty(sItemID))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while ( oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddItem (sItemID, -1, data);
				}
			}
			else {
				processAddItem (sItemID, -1, data);
			}
		}
	}
    
	protected void processAddItem (String _sItemID, double _dQty, RunData data)
    	throws Exception
    {
		String sLocationID = data.getParameters().getString("LocationId");
		String sVendorID = data.getParameters().getString("VendorId","");
		String sDescription = data.getParameters().getString("Description","");
		PurchaseReceiptDetail oPRD = new PurchaseReceiptDetail ();
		Item oItem = ItemTool.getItemByID (_sItemID);

		if (oItem != null)
		{
			Unit oUnit = UnitTool.getUnitByID (oItem.getPurchaseUnitId());
			double dUnitBaseValue = oUnit.getValueToBase().doubleValue();
			oPRD.setPurchaseOrderDetailId  ("");
			oPRD.setItemId   	  (_sItemID);
			oPRD.setItemCode 	  (oItem.getItemCode());
			oPRD.setItemName 	  (oItem.getItemName());
			oPRD.setDescription   (sDescription);

			if(_dQty < 0) //from multiple item add
			{
				oPRD.setDescription(oItem.getDescription());
				oPRD.setUnitId   (oUnit.getUnitId());
			    oPRD.setUnitCode (oUnit.getUnitCode());
			}
			else
			{
   			    oPRD.setUnitId   (data.getParameters().getString("UnitId"));
			    oPRD.setUnitCode (data.getParameters().getString("UnitCode"));
			    oUnit = UnitTool.getUnitByID (data.getParameters().getString("UnitId"));
			    dUnitBaseValue = oUnit.getValueToBase().doubleValue();
			}
			
			double dQty = 1;		
			if (_dQty < 0) //if from multiple item add
			{
				double dQtyOnHand = InventoryLocationTool.getInventoryOnHand(_sItemID, sLocationID).doubleValue();
				ItemInventory oItemInv = ItemInventoryTool.getItemInventory(_sItemID, sLocationID);				
				if( oItemInv != null && (dQtyOnHand <= oItemInv.getReorderPoint().doubleValue()) )
				{
					//reorder at max - min
				    dQty = oItemInv.getMaximumQty().doubleValue() - oItemInv.getMinimumQty().doubleValue();
				}
				
				//double dQtyOnHand = InventoryLocationTool.getInventoryOnHand(_sItemID, sLocationID).doubleValue();			
				//dQty = ItemTool.getInventoryAmountToPurchase(_sItemID, dQtyOnHand);
			}
			else
			{
				dQty = _dQty;
			}
			
			oPRD.setQty 		  (new BigDecimal(dQty));
			double dQtyBase = dQty * dUnitBaseValue;
			oPRD.setQtyBase 	  (new BigDecimal(dQtyBase));
			
			oPRD.setReturnedQty   (bd_ZERO);
			oPRD.setBilledQty     (bd_ZERO);		    
			oPRD.setTaxId   	  (oItem.getPurchaseTaxId());
			oPRD.setTaxAmount 	  (TaxTool.getAmountByID(oPRD.getTaxId()));
			
			//price and discount
		    oPRD.setDiscount	  ("0");
		    oPRD.setItemPriceInPo (oItem.getLastPurchasePrice());
			
		    //check price & discount in vendor price list
			VendorPriceListDetail oVendPrice = 
				VendorPriceListTool.getPriceListDetailByVendorID(sVendorID,_sItemID,oPR.getReceiptDate());
			
			if(oVendPrice != null)
			{
    		    oPRD.setDiscount	  (oVendPrice.getDiscountAmount());
			    oPRD.setItemPriceInPo (oVendPrice.getPurchasePrice());
			}
			
			//calculate sub total, discount
			double dTotal = oPRD.getItemPriceInPo().doubleValue() * dQty;
			double dRate = data.getParameters().getDouble("CurrencyRate", 1);
			double dLastPurchase = dTotal / dQty;
			double dCost = dLastPurchase / dUnitBaseValue * dRate;

			//tax
            oPRD.setTaxId (oItem.getPurchaseTaxId());
			oPRD.setTaxAmount (TaxTool.getAmountByID(oItem.getPurchaseTaxId()));
    		double dTax = dTotal * oPRD.getTaxAmount().doubleValue() / 100;
    		dTotal = dTotal + dTax;
    		
    		oPRD.setCostPerUnit   ( new BigDecimal (dCost)); 
		    oPRD.setLastPurchase  ( new BigDecimal (dLastPurchase));
		    oPRD.setSubTotal  	  ( new BigDecimal (dTotal));
			
		    if (!isExist(oPRD))
			{
   			    if (b_PURCHASE_FIRST_LAST)
   			    {
			        vPRD.add (0,oPRD);
			    }
			    else
			    {
    				vPRD.add (oPRD);
				}
			}
			PurchaseReceiptTool.setHeaderProperties ( oPR, vPRD, data );
			oPRSes.setAttribute (s_PRD, vPRD);
			oPRSes.setAttribute (s_PR, oPR);
    	}
    }	
	
    protected void copyDetail ( RunData data )
    	throws Exception
    {
    	int iCopyNo = data.getParameters().getInt("No") - 1;
    	PurchaseReceiptTool.updateDetail(vPRD,data);	
		if(vPRD.size() > iCopyNo)
		{
			PurchaseReceiptDetail oPRD = (PurchaseReceiptDetail) vPRD.get(iCopyNo);
			PurchaseReceiptDetail oPRD2 = oPRD.copy();
			vPRD.add(iCopyNo + 1, oPRD2);
			
			oPRSes.setAttribute(s_PRD, vPRD);
			PurchaseReceiptTool.setHeaderProperties ( oPR, vPRD, data );
			oPRSes.setAttribute( s_PR, oPR );
		}
    }

}

package com.ssti.enterprise.pos.tools.purchase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PurchaseInvoicePeer;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceiptPeer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Helper tool for Purchase Analysis
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseAnalysisTool.java,v 1.10 2009/05/04 02:04:54 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseAnalysisTool.java,v $
 * 
 * 2015-09-25
 * - Change in getPurchaseLeadTime, Now return List of Record instead of List of List linked to ItemInventoryTool
 * - Used template LeadTimeAnalysis.vm
 * 
 * </pre><br>
 */
public class PurchaseAnalysisTool extends BaseTool
{
	private static Log log = LogFactory.getLog(PurchaseAnalysisTool.class);

	private static PurchaseAnalysisTool instance = null;
	
	public static synchronized PurchaseAnalysisTool getInstance()
	{
		if (instance == null) instance = new PurchaseAnalysisTool();
		return instance;
	}
	
	public static StringBuilder buildStdQuery (StringBuilder oSQL, 
											   Date _dStart, 
											   Date _dEnd, 
											   int _iStatus,
											   String _sLocationID)
	{
		buildDateQuery(oSQL, "pi.purchase_invoice_date", _dStart, _dEnd);
		
		if(_iStatus <= 0) _iStatus = i_TRANS_PROCESSED;
		oSQL.append (" AND pi.status = ").append (_iStatus).append(" ");		
		
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oSQL.append (" AND pi.location_id = '").append (_sLocationID).append ("'");
		}				
		return oSQL;
	}

    public static StringBuilder buildStdQuery (StringBuilder oSQL, 
                                               Date _dStart, 
                                               Date _dEnd, 
                                               int _iStatus,
                                               String _sLocationID,
                                               String _sDateCol)
    {
        buildDateQuery(oSQL, _sDateCol, _dStart, _dEnd);
        
        if(_iStatus <= 0) _iStatus = i_TRANS_PROCESSED;
        oSQL.append (" AND pi.status = ").append (_iStatus).append(" ");        
        
        if (StringUtil.isNotEmpty(_sLocationID))
        {
        oSQL.append (" AND pi.location_id = '").append (_sLocationID).append ("'");
        }               
        return oSQL;
    }
    
	/**
     * Method to get Item that most purchased based on quantity filter or total amount filter
     * 
     * @param _dStart date that show begin
     * @param _dEnd date that show end
     * @param _iLimit limit ranking
     * @param _iType order by
     * @return List of List
     */
	public static List getItemRankingAnalysis (Date _dStart, Date _dEnd, int _iLimit, int _iType)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT pd.item_id, SUM(pd.sub_total) AS total_amt, SUM(pd.qty_base) AS total_qty FROM purchase_order AS p, ");
		oSQL.append (" purchase_order_detail AS pd WHERE ");
		oSQL.append (" p.transaction_date >= '");
		oSQL.append (CustomFormatter.formatCustomDate(_dStart,"yyyy-MM-dd")); 
		oSQL.append ("' AND p.transaction_date <= '");
		oSQL.append (CustomFormatter.formatCustomDate(_dEnd,"yyyy-MM-dd")); 
		oSQL.append ("' AND p.purchase_order_id = pd.purchase_order_id ");
		oSQL.append ("  GROUP BY pd.item_id ORDER BY ");
		if (_iType == 1) oSQL.append (" total_amt DESC "); //Amount 
		if (_iType == 2) oSQL.append (" total_qty DESC "); //Amount 
		oSQL.append (" LIMIT ");
		oSQL.append (_iLimit);
				
		log.debug (oSQL.toString());

		List vData = PurchaseOrderPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			Item oItem = ItemTool.getItemByID(oData.getValue(1).asString());

			List vRecordData = new ArrayList (5);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(oItem.getItemCode());
			vRecordData.add(oItem.getItemName());
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			vRecordData.add(oData.getValue(3).asBigDecimal());
			vRecordData.add(oItem.getBarcode());
			vResult.add (vRecordData); 
		}
		return vResult;	
	}

	/**
     * Method to get Item that most Received 
     * 
     * @param _dStart date that show begin
     * @param _dEnd date that show end
     * @param _iLimit limit ranking
     * @return List of List
     */
	public static List getItemReceivedAnalysis (Date _dStart, Date _dEnd, int _iLimit)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT prdet.item_id, SUM(prdet.qty_base) AS total_receipt_qty FROM purchase_receipt AS pr, ");
		oSQL.append (" purchase_receipt_detail AS prdet WHERE ");
		oSQL.append (" pr.receipt_date >= '");
		oSQL.append (CustomFormatter.formatCustomDate(_dStart,"yyyy-MM-dd")); 
		oSQL.append ("' AND pr.receipt_date <= '");
		oSQL.append (CustomFormatter.formatCustomDate(_dEnd,"yyyy-MM-dd")); 
		oSQL.append ("' AND pr.purchase_receipt_id = prdet.purchase_receipt_id ");
		oSQL.append ("  AND ( pr.status = "+AppAttributes.i_PR_APPROVED);
		oSQL.append ("  OR pr.status = "+AppAttributes.i_PR_INVOICED+")");
		oSQL.append ("  GROUP BY prdet.item_id ORDER BY total_receipt_qty DESC "); //Amount 
		oSQL.append (" LIMIT ");
		oSQL.append (_iLimit);
				
		log.debug (oSQL.toString());

		List vData = PurchaseReceiptPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			Item oItem = ItemTool.getItemByID(oData.getValue(1).asString());

			List vRecordData = new ArrayList (4);
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(oItem.getItemCode());
			vRecordData.add(oItem.getItemName());
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			vRecordData.add(oItem.getBarcode());
			vResult.add (vRecordData); 
		}
		return vResult;	
	}
	
	/**
     * Method to get Vendor that most ever Purchased based on quantity filter or total amount filter
     * 
     *
     * @param _dStart date that show begin
     * @param _dEnd date that show end
     * @param _iLimit limit ranking
     * @param _iType order by
     * @return List of List
     */
	public static List getVendorRankingAnalysis (Date _dStart, Date _dEnd, int _iLimit, int _iType)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT vnd.vendor_id, SUM(pi.total_amount) AS total_amt, SUM(pi.total_qty) AS total_qty FROM purchase_invoice AS pi, ");
		oSQL.append (" vendor AS vnd WHERE ");
		oSQL.append (" pi.purchase_invoice_date >= '");
		oSQL.append (CustomFormatter.formatCustomDate(_dStart,"yyyy-MM-dd")); 
		oSQL.append ("' AND pi.purchase_invoice_date <= '");
		oSQL.append (CustomFormatter.formatCustomDate(_dEnd,"yyyy-MM-dd")); 
		oSQL.append ("' AND pi.vendor_id = vnd.vendor_id ");
		oSQL.append (" AND pi.status = 2 ");		
		oSQL.append ("  GROUP BY vnd.vendor_id ORDER BY ");
		if (_iType == 1) oSQL.append (" total_amt DESC "); //Amount 
		if (_iType == 2) oSQL.append (" total_qty DESC "); //Amount 
		oSQL.append (" LIMIT ");
		oSQL.append (_iLimit);
				
		log.debug (oSQL.toString());

		List vData = PurchaseOrderPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			List vRecordData = new ArrayList (5);
			Vendor oVend = VendorTool.getVendorByID(oData.getValue(1).asString());
			vRecordData.add(oData.getValue(1).asString());
			vRecordData.add(oVend.getVendorCode());
			vRecordData.add(oVend.getVendorName());
			vRecordData.add(oData.getValue(2).asBigDecimal());			
			vRecordData.add(oData.getValue(3).asBigDecimal());
			vResult.add (vRecordData); 
		}
		return vResult;	
	}

	/**
     * Method to get Total Purchase per month
     * 
     * @param _dStart date that show begin
     * @param _dEnd date that show end
     * @param _bPaymentType select what type of payment
     * @return List of Record
     */
	public static List getPurchaseByDateAndPaymentType(String _sTrans,
                                                       String _sVendorID,
                                                       String _sLocID,
                                                       Date _dStart, 
                                                       Date _dEnd, 
                                                       boolean _bPaymentType)
		throws Exception
	{		
        List vPTID = PaymentTypeTool.getPaymentTypeIDByStatus (_bPaymentType);
        String sIDs = SqlUtil.convertToINMode(vPTID);
        String sDateCol = "purchase_invoice_date";
        String sTbl = "purchase_invoice";
        
        if (StringUtil.equals(_sTrans,"po"))
        {
            sDateCol = "transaction_date";
            sTbl = "purchase_order";            
        }
        else if (StringUtil.equals(_sTrans,"pr"))
        {
            sDateCol = "receipt_date";
            sTbl = "purchase_receipt";  
        }
        else if (StringUtil.equals(_sTrans,"pt"))
        {
            sDateCol = "return_date";
            sTbl = "purchase_return";  
        }
        
		StringBuilder oSQL = new StringBuilder ();
		String sDay  = SqlUtil.dayCol(sDateCol);

		oSQL.append ("SELECT ").append(sDay).append(", SUM(pi.total_amount) ");
		oSQL.append ("FROM ").append(sTbl).append(" pi WHERE ");
		buildStdQuery (oSQL, _dStart, _dEnd, i_PROCESSED, _sLocID, sDateCol);		
		oSQL.append (" AND pi.payment_type_id IN ");
		oSQL.append (sIDs);
		if(StringUtil.isNotEmpty(_sVendorID))
        {
		    oSQL.append (" AND vendor_id = '").append(_sVendorID).append("' ");
        }
        
        oSQL.append (" GROUP BY ").append(sDay);
		oSQL.append (" ORDER BY ").append(sDay);
		
		log.debug (oSQL.toString());
		List vData = PurchaseInvoicePeer.executeQuery(oSQL.toString());	
		return vData;	
	}	

	/**
     * Method to get Total Purchase per Year
     * 
     * @param _dStart date that show begin
     * @param _dEnd date that show end
     * @param _bPaymentType select what type of payment
     * @return List of Record
     */
	public static List getPurchaseByMonthAndPaymentType(String _sTrans,
                                                        String _sVendorID, 
                                                        String _sLocID,
                                                        Date _dStart, 
                                                        Date _dEnd, 
                                                        boolean _bPaymentType)
		throws Exception
	{		
		List vPTID = PaymentTypeTool.getPaymentTypeIDByStatus(_bPaymentType);
		String sIDs = SqlUtil.convertToINMode(vPTID);
        
        String sDateCol = "purchase_invoice_date";
        String sTbl = "purchase_invoice";        
        if (StringUtil.equals(_sTrans,"po"))
        {
            sDateCol = "transaction_date";
            sTbl = "purchase_order";            
        }
        else if (StringUtil.equals(_sTrans,"pr"))
        {
            sDateCol = "receipt_date";
            sTbl = "purchase_receipt";  
        }
        else if (StringUtil.equals(_sTrans,"pt"))
        {
            sDateCol = "return_date";
            sTbl = "purchase_return";  
        }
        
		StringBuilder oSQL = new StringBuilder ();
		String sMonth  = SqlUtil.monthCol(sDateCol);

		oSQL.append ("SELECT ").append(sMonth).append(", SUM(pi.total_amount) ");
		oSQL.append ("FROM ").append(sTbl).append(" pi WHERE ");
		buildStdQuery (oSQL, _dStart, _dEnd, i_PROCESSED, _sLocID, sDateCol);		
		oSQL.append (" AND pi.payment_type_id IN ");
		oSQL.append (sIDs);
        if(StringUtil.isNotEmpty(_sVendorID))
        {
            oSQL.append (" AND vendor_id = '").append(_sVendorID).append("' ");
        }
		oSQL.append (" GROUP BY ").append(sMonth);
		oSQL.append (" ORDER BY ").append(sMonth);
		
		log.debug (oSQL.toString());
		List vData = PurchaseInvoicePeer.executeQuery(oSQL.toString());	
		return vData;	
	}	

	/**
     * Method to get Total Purchase per day
     * 
     * @param _sDate date that will choose
     * @param _vData list of purchase per month
     * @return total purchase 
     */
	public static BigDecimal getTotalPurchase (String _sDate, List _vData)
		throws Exception
	{					
        if (_vData != null)
        {
    		for (int i = 0; i < _vData.size(); i++)
    		{
    			Record oData = (Record) _vData.get(i);
    			if (_sDate.equals(oData.getValue(1).asString()) )
    			{
    				return oData.getValue(2).asBigDecimal();
    			}
    		}
        }
		return bd_ZERO;
	}	
	
	/**
     * Method to get Total Purchase per day
     * 
     * @param _iMonth Month that will choose
     * @param _vData list of purchase per Year
     * @return total purchase per year
     */
	public static BigDecimal getTotalPurchase (int _iMonth, List _vData)
		throws Exception
	{			
        if (_vData != null)
        {
    		for (int i = 0; i < _vData.size(); i++)
    		{
    			Record oData = (Record) _vData.get(i);
    			if (_iMonth == oData.getValue(1).asInt()) 
    			{
    				return oData.getValue(2).asBigDecimal();
    			}
    		}
        }
		return bd_ZERO;
	}	


	/**
	 * method to get lead time of an item from vendor since ordered to received
	 * 
	 * @param _dStart 
	 * @param _dEnd
	 * @param _sItemKey
	 * @param _sVendorKey
	 * @param _bIsKeyID whether
	 * @return List of List
	 * @throws Exception
	 */
	//TODO: refactor method
	public static List getPurchaseLeadTime(Date _dStart, 
										   Date _dEnd, 
										   String _sItemKey, 
										   String _sVendorKey, 
										   boolean _bIsKeyID)
		throws Exception
	{		
	    StringBuilder oSQL = new StringBuilder ();
	    oSQL.append ("SELECT po.vendor_id, po.vendor_name,po.transaction_date, ");
	    oSQL.append ("pod.item_id, pod.item_name, pod.item_code, pr.receipt_date, ");
	    oSQL.append ("po.purchase_order_id, pr.purchase_receipt_id ");
	    oSQL.append ("FROM purchase_order AS po, purchase_order_detail AS pod, purchase_receipt AS pr, ");
	    if(!_bIsKeyID)
	    {
	        if(StringUtil.isNotEmpty(_sItemKey))
	        {
	            oSQL.append (" item i, ");
	        }
	        if(StringUtil.isNotEmpty(_sVendorKey))
	        {
	            oSQL.append (" vendor v, ");
	        }
	    }
	    oSQL.append (" purchase_receipt_detail AS prd WHERE ");
	    oSQL.append (" po.purchase_order_id = pod.purchase_order_id AND ");
	    oSQL.append (" pod.purchase_order_detail_id = prd.purchase_order_detail_id AND ");
	    oSQL.append (" prd.purchase_receipt_id = pr.purchase_receipt_id AND ");
	    oSQL.append (" po.transaction_date >= '");
	    oSQL.append (CustomFormatter.formatCustomDate(_dStart,"yyyy-MM-dd")); 
	    oSQL.append ("' AND po.transaction_date <= '");
	    oSQL.append (CustomFormatter.formatCustomDate(_dEnd,"yyyy-MM-dd")); 
	    oSQL.append ("'");
	    if(_bIsKeyID)
	    {
	        if(StringUtil.isNotEmpty(_sItemKey))
	        {
	            oSQL.append (" AND pod.item_id='"); 
	            oSQL.append (_sItemKey);
	            oSQL.append ("'");
	        }
	        if(StringUtil.isNotEmpty(_sVendorKey))
	        {
	            oSQL.append (" AND po.vendor_id='"); 
	            oSQL.append (_sVendorKey);
	            oSQL.append ("'");
	        }
	    }
	    else
	    {
	        if(StringUtil.isNotEmpty(_sItemKey))
	        {
	            oSQL.append(" AND i.item_name LIKE '");
	            oSQL.append(_sItemKey);
	            oSQL.append ("%' AND i.item_id = pod.item_id"); 
	        }
	        if(StringUtil.isNotEmpty(_sVendorKey))
	        {
	            oSQL.append(" AND v.vendor_name LIKE '");
	            oSQL.append(_sVendorKey);
	            oSQL.append ("%' AND v.vendor_id = po.vendor_id"); 
	        }
	    }
	    oSQL.append (" ORDER BY po.vendor_name,pod.item_name,po.transaction_date");
	    log.debug (oSQL.toString());
	    
	    List vData = PurchaseOrderPeer.executeQuery(oSQL.toString());	
//	    List vResult = new ArrayList (vData.size());
//	    for (int i = 0; i < vData.size(); i++)
//	    {
//	        Record oData = (Record) vData.get(i);
//	        List vRecordData = new ArrayList (9);
//	        vRecordData.add(oData.getValue(1).asString());
//	        vRecordData.add(oData.getValue(2).asString());
//	        vRecordData.add(oData.getValue(3).asDate());
//	        vRecordData.add(oData.getValue(4).asString());
//	        vRecordData.add(oData.getValue(5).asString());
//	        vRecordData.add(oData.getValue(6).asString());
//	        vRecordData.add(oData.getValue(7).asDate());
//	        vRecordData.add(oData.getValue(8).asString());
//	        vRecordData.add(oData.getValue(9).asString());
//	        vResult.add (vRecordData); 
//	    }
	    return vData;	
	}

	/**
     * Method to get Item that most purchased based on quantity filter or total amount filter
     * 
     *
     * @param _dStart date that show begin
     * @param _dEnd date that show end
     * @param _iLimit limit ranking
     * @param iType order by
     * @return List of List
     */
	public static List getPurchasePriceHistory(int _iCond, 
											   String _sKey, 
											   Date _dStart, 
											   Date _dEnd,
											   String _sKatID, 
											   String _sVendorID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT pi.vendor_id, pi.vendor_name, pi.purchase_invoice_date, ");
		oSQL.append (" pid.item_id, pid.item_name, pid.item_code, pi.purchase_invoice_no, pid.qty, pid.unit_code, ");
		oSQL.append (" pid.item_price, pid.discount, pid.sub_total, pid.sub_total_tax, pi.location_id ");
		oSQL.append ("FROM purchase_invoice pi, purchase_invoice_detail pid, item i");

		oSQL.append ("  WHERE pi.purchase_invoice_id = pid.purchase_invoice_id AND i.item_id = pid.item_id ");
		if (_dStart != null) oSQL.append(" AND ");
		
		buildStdQuery(oSQL, _dStart, _dEnd, i_TRANS_PROCESSED, "");
			        
	    if(StringUtil.isNotEmpty(_sKey))
	    {
	        oSQL.append (" AND "); 	        
	        if (_iCond == 1) oSQL.append(SqlUtil.like("i.item_code", _sKey));
	        if (_iCond == 2) oSQL.append(SqlUtil.like("i.item_name", _sKey));
	        if (_iCond == 3) oSQL.append(SqlUtil.like("i.description", _sKey));
	        if (_iCond == 4) oSQL.append(SqlUtil.like("i.item_sku", _sKey));
	        if (_iCond == 5) oSQL.append(SqlUtil.like("i.item_sku_name", _sKey));   	        
	    }	    
	    if (StringUtil.isNotEmpty(_sVendorID))
	    {
	    	oSQL.append(" AND pi.vendor_id = '").append(_sVendorID).append("'");
	    }	    
		if (StringUtil.isNotEmpty(_sKatID))
		{
			List vKategori = KategoriTool.getChildIDList(_sKatID);
			vKategori.add(_sKatID);
		    oSQL.append (" AND i.kategori_id IN ")
	    		.append (SqlUtil.convertToINMode(vKategori));		
		}
	    
		oSQL.append (" ORDER BY pid.purchase_invoice_detail_id, pi.purchase_invoice_date, pid.item_code ASC");
		log.debug (oSQL.toString());

		List vData = PurchaseOrderPeer.executeQuery(oSQL.toString());	
		
		List vResult = new ArrayList (vData.size());
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			Map mRecord = new HashMap(12);
			
			mRecord.put("VendorId", oData.getValue(1).asString());
			mRecord.put("VendorName", oData.getValue(2).asString());
			mRecord.put("PurchaseInvoiceDate", oData.getValue(3).asDate());
			mRecord.put("ItemId", oData.getValue(4).asString());
			mRecord.put("ItemName", oData.getValue(5).asString());
			mRecord.put("ItemCode", oData.getValue(6).asString());
			mRecord.put("PurchaseInvoiceNo", oData.getValue(7).asString());
			mRecord.put("Qty", oData.getValue(8).asBigDecimal());
			mRecord.put("UnitCode", oData.getValue(9).asString());
			mRecord.put("ItemPrice", oData.getValue(10).asBigDecimal());
			mRecord.put("Discount", oData.getValue(11).asString());			
			mRecord.put("SubTotal", oData.getValue(12).asBigDecimal());
			mRecord.put("SubTotalTax", oData.getValue(13).asBigDecimal());
			mRecord.put("LocationId", oData.getValue(14).asString());
			
			vResult.add(mRecord); 
		}
		return vResult;	
	}

	public static List filterTrans (List _vSource, String _sValue, String _sKey)
    	throws Exception
    {

		List vResult = (List)((ArrayList)_vSource).clone();
		Iterator oIter = vResult.iterator();
		while (oIter.hasNext()) 
		{
			Map mData = (Map) oIter.next();
			if (!mData.get(_sKey).equals(_sValue))
			{
				oIter.remove();
			}
		}
		return vResult;
	}
	
	private static boolean isInList(List _vResult, Map _mRecord, String _sKey)
	throws Exception
	{
		for(int i = 0; i < _vResult.size(); i++)
		{
			Map mData = (Map)_vResult.get(i);
			if(mData.get(_sKey).equals(_mRecord.get(_sKey)))
				return true;
		}
		
		return false;
	}
	
	public static List getDistinctList(List _vData, String _sKey)
	    throws Exception
	{
	    List vResult = new ArrayList(_vData.size());
	    for(int i = 0; i < _vData.size(); i++)
	    {
	        Map mRecord = (Map)_vData.get(i);
	        if(!isInList(vResult, mRecord, _sKey))
	            vResult.add(mRecord);
	    }
	
	    return vResult;
	}
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocID
	 * @param _sKategoriID
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static List getPurchaseItem (Date _dStart, 
									 	Date _dEnd, 
									 	String _sLocID,
									 	String _sKategoriID, 
									 	String _sItemID,
									 	String _sVendorID,
									 	String _sColor,
									 	String _sField,
									 	String _sValue,	
									 	String _sOrderBy,
									 	int _iStatus)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT pd.item_id, pd.item_code, pd.item_name, pd.qty, pd.qty_base, pd.unit_code, pd.item_price, ");
		oSQL.append (" pd.discount, pd.sub_total, pd.cost_per_unit, i.last_purchase_price, i.description AS item_desc, ");
		oSQL.append (" i.manufacturer, i.brand, i.tags, ");
		oSQL.append (" pi.purchase_invoice_id, pi.purchase_invoice_no, ");
		oSQL.append (" pi.purchase_invoice_date, pi.vendor_id, v.vendor_code, v.vendor_name ");
		oSQL.append (" FROM purchase_invoice pi, ");
		oSQL.append (" purchase_invoice_detail pd, ");
		oSQL.append (" kategori k, item i, vendor v  WHERE ");
		
		buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocID);
		
		oSQL.append ("  AND pi.purchase_invoice_id = pd.purchase_invoice_id ");
		oSQL.append ("  AND pi.vendor_id = v.vendor_id ");
		oSQL.append ("  AND pd.item_id = i.item_id ");
        oSQL.append ("  AND i.kategori_id = k.kategori_id ");        
		
        if (StringUtil.isNotEmpty(_sItemID))
        {
		    oSQL.append ("  AND i.item_id = '");
		    oSQL.append (_sItemID);
		    oSQL.append ("'");
        }
        else
        {
			if (StringUtil.isNotEmpty(_sKategoriID))
			{		
	    		List vChildID = KategoriTool.getChildIDList(_sKategoriID);	    		
	    		if (vChildID.size() > 0)
	    		{
	    		    vChildID.add (_sKategoriID);
	    		    oSQL.append ("  AND k.kategori_id IN ");
	                oSQL.append (SqlUtil.convertToINMode(vChildID));
	    		}
	    		else 
	    		{
	    		    oSQL.append ("  AND k.kategori_id = '");
	    		    oSQL.append (_sKategoriID);
	    		    oSQL.append ("'");
		        }
		    }  
        }
        if (StringUtil.isNotEmpty(_sVendorID))
        {
		    oSQL.append ("  AND pi.vendor_id = '");
		    oSQL.append (_sVendorID);
		    oSQL.append ("'");
        }  
        if (StringUtil.isNotEmpty(_sField) && StringUtil.isNotEmpty(_sValue))
        {
		    oSQL.append ("  AND i." + _sField + " = '");
		    oSQL.append (_sValue);
		    oSQL.append ("'");
        }          
        if (StringUtil.isNotEmpty(_sColor))
        {
		    oSQL.append ("  AND i.color = '");
		    oSQL.append (_sColor);
		    oSQL.append ("'");
        }
		log.debug (oSQL.toString());
		return PurchaseInvoicePeer.executeQuery(oSQL.toString());	
	}	
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocID
	 * @param _sKategoriID
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static List getTotalPOPerItem (Date _dStart, 
								          Date _dEnd, 
								          String _sLocID,
								          String _sKategoriID, 
								          String _sItemID,
								          String _sVendorID,
								          String _sColor,
								          int _iStatus)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT pd.item_id, SUM(pd.qty_base), SUM(pd.sub_total), SUM(pd.sub_total_disc),");
		oSQL.append (" SUM(pd.sub_total_tax), SUM(pd.sub_total - pd.sub_total_disc) ");
		oSQL.append (" FROM purchase_order p, ");
		oSQL.append (" purchase_order_detail pd, ");
		oSQL.append (" kategori k, item i  WHERE ");
		
		buildDateQuery(oSQL, "po.purchase_order_date", _dStart, _dEnd);
		
		oSQL.append (" AND p.purchase_order_id = pd.purchase_order_id ");
		oSQL.append (" AND pd.item_id = i.item_id ");
        oSQL.append (" AND i.kategori_id = k.kategori_id ");
		oSQL.append (" AND po.status != 4 AND po.status != 5 ");
        if (StringUtil.isNotEmpty(_sItemID))
        {
		    oSQL.append ("  AND i.item_id = '");
		    oSQL.append (_sItemID);
		    oSQL.append ("'");
        }
        else
        {
			if (StringUtil.isNotEmpty(_sKategoriID))
			{		
	    		List vChildID = KategoriTool.getChildIDList(_sKategoriID);
	    		
	    		if (vChildID.size() > 0)
	    		{
	    		    vChildID.add (_sKategoriID);
	    		    oSQL.append ("  AND k.kategori_id IN ");
	                oSQL.append (SqlUtil.convertToINMode(vChildID));
	    		}
	    		else 
	    		{
	    		    oSQL.append ("  AND k.kategori_id = '");
	    		    oSQL.append (_sKategoriID);
	    		    oSQL.append ("'");
		        }
		    }  
        }
        if (StringUtil.isNotEmpty(_sVendorID))
        {
		    oSQL.append ("  AND pi.vendor_id = '");
		    oSQL.append (_sVendorID);
		    oSQL.append ("'");
        }        
        if (StringUtil.isNotEmpty(_sColor))
        {
		    oSQL.append ("  AND i.color = '");
		    oSQL.append (_sColor);
		    oSQL.append ("'");
        }
        oSQL.append(" GROUP BY item_id");
		log.debug (oSQL.toString());
		return PurchaseInvoicePeer.executeQuery(oSQL.toString());	
	}	
	
	
	/**
	 * 
	 * @param _dStr
	 * @param _dEnd
	 * @param _sLocID
	 * @param _sKategoriID
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static List getBestPurchase (String _sItemID, 
										List _vItemID,
									    String _sLocID,
									    String _sOrderCol,
									    Date _dStr,
									    Date _dEnd,
									    int _iLimit)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		if(StringUtil.isEmpty(_sOrderCol)) _sOrderCol = " discount DESC, cost_per_unit ASC";
		
		oSQL.append(" SELECT                                   ")
		    .append("  pd.purchase_invoice_id AS piid,         ")
		    .append("  pd.purchase_invoice_detail_id AS pidid, ")
		    .append("  pi.purchase_invoice_no AS pino,         ")
		    .append("  pi.purchase_invoice_date AS pidate,     ")
		    .append("  pi.vendor_id,   ")
		    .append("  pi.vendor_name, ")
		    .append("  item_id,        ")
		    .append("  item_code,      ")
		    .append("  item_price,     ")
		    .append("  qty,            ")
		    .append("  unit_code,      ")
		    .append("  qty_base,       ")
		    .append("  discount,       ")
		    .append("  tax_amount,     ")
		    .append("  cost_per_unit,  ")
		    .append("  rank() OVER (PARTITION BY item_id ORDER BY ").append(_sOrderCol).append(" ) AS rank ")
		    .append(" FROM purchase_invoice_detail pd, purchase_invoice pi ")
		    .append(" WHERE pd.purchase_invoice_id = pi.purchase_invoice_id AND pi.status = 2 ");		  
		if(StringUtil.isNotEmpty(_sItemID))
		{
			oSQL.append(" AND item_id = '").append(_sItemID).append("' ");
		}
		else
		{
			if(_vItemID != null)
			{
				oSQL.append(" AND item_id IN ").append(SqlUtil.convertToINMode(_vItemID));
			}
		}
		if(StringUtil.isNotEmpty(_sLocID))
		{
			oSQL.append(" AND location_id = '").append(_sLocID).append("'");
		}
		if(_dStr != null || _dEnd != null)
		{
			if(_dStr != null) oSQL.append(" AND pi.purchase_invoice_date >= ").append(CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStr)));
			if(_dEnd != null) oSQL.append(" AND pi.purchase_invoice_date <= ").append(CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd)));
		}
		else
		{
			oSQL.append(" AND pi.purchase_invoice_date >= date('now') - interval '1 year' ");
		}		
		if(StringUtil.isNotEmpty(_sItemID) && _iLimit > 0)
		{
			oSQL.append(" LIMIT ").append(_iLimit);
		}			
		
		log.debug (oSQL.toString());
		return PurchaseInvoicePeer.executeQuery(oSQL.toString());	
	}
}
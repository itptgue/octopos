package com.ssti.enterprise.pos.presentation.screens.report.purchase;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;

public class OrderReceivedHistory extends PurchaseReportScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
		{
	    	super.doBuildTemplate(data, context);
	    	super.setParams(data);

	    	context.put("PurchaseOrders", 
	    		PurchaseOrderTool.findData(iCondition, sKeywords, dStart, dEnd, sVendorID, iStatus)
	    	);
	    }   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
		}    
    	
    }    
}

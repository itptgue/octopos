package com.ssti.framework.excel.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Excel Helper Interface
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ExcelHelper.java,v 1.6 2008/06/29 07:10:53 albert Exp $ <br>
 *
 * <pre>
 * $Log: ExcelHelper.java,v $
 * Revision 1.6  2008/06/29 07:10:53  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/09/14 02:16:51  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/05/03 14:36:12  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/02/23 14:11:55  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public interface ExcelHelper extends Attributes
{    
	/**
	 * create excel workbook object
	 * 
	 * @param _mParams
	 * @param _oSession
	 * @return HSSFWorkBook
	 * @throws Exception
	 */
	public HSSFWorkbook getWorkbook (Map _mParams, HttpSession _oSession) throws Exception;
}
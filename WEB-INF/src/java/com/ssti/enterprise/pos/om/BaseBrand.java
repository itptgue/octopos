package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Brand
 */
public abstract class BaseBrand extends BaseObject
{
    /** The Peer class */
    private static final BrandPeer peer =
        new BrandPeer();

        
    /** The value for the brandId field */
    private String brandId;
      
    /** The value for the brandCode field */
    private String brandCode;
                                                
    /** The value for the brandName field */
    private String brandName = "";
                                                
    /** The value for the description field */
    private String description = "";
                                                
    /** The value for the brandLogo field */
    private String brandLogo = "";
                                                
    /** The value for the principalId field */
    private String principalId = "";
  
    
    /**
     * Get the BrandId
     *
     * @return String
     */
    public String getBrandId()
    {
        return brandId;
    }

                        
    /**
     * Set the value of BrandId
     *
     * @param v new value
     */
    public void setBrandId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.brandId, v))
              {
            this.brandId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BrandCode
     *
     * @return String
     */
    public String getBrandCode()
    {
        return brandCode;
    }

                        
    /**
     * Set the value of BrandCode
     *
     * @param v new value
     */
    public void setBrandCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.brandCode, v))
              {
            this.brandCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BrandName
     *
     * @return String
     */
    public String getBrandName()
    {
        return brandName;
    }

                        
    /**
     * Set the value of BrandName
     *
     * @param v new value
     */
    public void setBrandName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.brandName, v))
              {
            this.brandName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BrandLogo
     *
     * @return String
     */
    public String getBrandLogo()
    {
        return brandLogo;
    }

                        
    /**
     * Set the value of BrandLogo
     *
     * @param v new value
     */
    public void setBrandLogo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.brandLogo, v))
              {
            this.brandLogo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrincipalId
     *
     * @return String
     */
    public String getPrincipalId()
    {
        return principalId;
    }

                        
    /**
     * Set the value of PrincipalId
     *
     * @param v new value
     */
    public void setPrincipalId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.principalId, v))
              {
            this.principalId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BrandId");
              fieldNames.add("BrandCode");
              fieldNames.add("BrandName");
              fieldNames.add("Description");
              fieldNames.add("BrandLogo");
              fieldNames.add("PrincipalId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BrandId"))
        {
                return getBrandId();
            }
          if (name.equals("BrandCode"))
        {
                return getBrandCode();
            }
          if (name.equals("BrandName"))
        {
                return getBrandName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("BrandLogo"))
        {
                return getBrandLogo();
            }
          if (name.equals("PrincipalId"))
        {
                return getPrincipalId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BrandPeer.BRAND_ID))
        {
                return getBrandId();
            }
          if (name.equals(BrandPeer.BRAND_CODE))
        {
                return getBrandCode();
            }
          if (name.equals(BrandPeer.BRAND_NAME))
        {
                return getBrandName();
            }
          if (name.equals(BrandPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(BrandPeer.BRAND_LOGO))
        {
                return getBrandLogo();
            }
          if (name.equals(BrandPeer.PRINCIPAL_ID))
        {
                return getPrincipalId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBrandId();
            }
              if (pos == 1)
        {
                return getBrandCode();
            }
              if (pos == 2)
        {
                return getBrandName();
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return getBrandLogo();
            }
              if (pos == 5)
        {
                return getPrincipalId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BrandPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BrandPeer.doInsert((Brand) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BrandPeer.doUpdate((Brand) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key brandId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBrandId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBrandId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBrandId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Brand copy() throws TorqueException
    {
        return copyInto(new Brand());
    }
  
    protected Brand copyInto(Brand copyObj) throws TorqueException
    {
          copyObj.setBrandId(brandId);
          copyObj.setBrandCode(brandCode);
          copyObj.setBrandName(brandName);
          copyObj.setDescription(description);
          copyObj.setBrandLogo(brandLogo);
          copyObj.setPrincipalId(principalId);
  
                    copyObj.setBrandId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BrandPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Brand\n");
        str.append("-----\n")
           .append("BrandId              : ")
           .append(getBrandId())
           .append("\n")
           .append("BrandCode            : ")
           .append(getBrandCode())
           .append("\n")
           .append("BrandName            : ")
           .append(getBrandName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("BrandLogo            : ")
           .append(getBrandLogo())
           .append("\n")
           .append("PrincipalId          : ")
           .append(getPrincipalId())
           .append("\n")
        ;
        return(str.toString());
    }
}

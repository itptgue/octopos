package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class ReceivablePaymentView extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Receivable Payment"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
        try 
    	{
        	context.put("arpayment", ReceivablePaymentTool.getInstance());
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findReceivablePaymentResult", "ArPayments");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}

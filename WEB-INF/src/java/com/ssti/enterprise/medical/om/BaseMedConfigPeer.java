package com.ssti.enterprise.medical.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.om.map.MedConfigMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseMedConfigPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "med_config";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(MedConfigMapBuilder.CLASS_NAME);
    }

      /** the column name for the MED_CONFIG_ID field */
    public static final String MED_CONFIG_ID;
      /** the column name for the PHARMACIST_NAME field */
    public static final String PHARMACIST_NAME;
      /** the column name for the LICENSE_NO field */
    public static final String LICENSE_NO;
      /** the column name for the WORK_LICENSE_NO field */
    public static final String WORK_LICENSE_NO;
      /** the column name for the PRES_NO_FORMAT field */
    public static final String PRES_NO_FORMAT;
      /** the column name for the DEFAULT_RFEE field */
    public static final String DEFAULT_RFEE;
      /** the column name for the DEFAULT_PACKAGING field */
    public static final String DEFAULT_PACKAGING;
      /** the column name for the RFEE_EDITABLE field */
    public static final String RFEE_EDITABLE;
      /** the column name for the RFEE_MULTIPLIED field */
    public static final String RFEE_MULTIPLIED;
      /** the column name for the RFEE_MIN_AMOUNT field */
    public static final String RFEE_MIN_AMOUNT;
      /** the column name for the RFEE_MAX_AMOUNT field */
    public static final String RFEE_MAX_AMOUNT;
      /** the column name for the PACK_MIN_AMOUNT field */
    public static final String PACK_MIN_AMOUNT;
      /** the column name for the PACK_MAX_AMOUNT field */
    public static final String PACK_MAX_AMOUNT;
      /** the column name for the POS_DEFAULT_RFEE field */
    public static final String POS_DEFAULT_RFEE;
      /** the column name for the POS_DEFAULT_PACK field */
    public static final String POS_DEFAULT_PACK;
      /** the column name for the POS_RFEE_EDITABLE field */
    public static final String POS_RFEE_EDITABLE;
      /** the column name for the POS_RFEE_MULTIPLIED field */
    public static final String POS_RFEE_MULTIPLIED;
      /** the column name for the POS_RFEE_MIN_AMOUNT field */
    public static final String POS_RFEE_MIN_AMOUNT;
      /** the column name for the POS_RFEE_MAX_AMOUNT field */
    public static final String POS_RFEE_MAX_AMOUNT;
      /** the column name for the POS_PACK_MIN_AMOUNT field */
    public static final String POS_PACK_MIN_AMOUNT;
      /** the column name for the POS_PACK_MAX_AMOUNT field */
    public static final String POS_PACK_MAX_AMOUNT;
      /** the column name for the RFEE_ITEM_CODE field */
    public static final String RFEE_ITEM_CODE;
      /** the column name for the PACKAGING_ITEM_CODE field */
    public static final String PACKAGING_ITEM_CODE;
      /** the column name for the PRES_ROOT_KATEGORI field */
    public static final String PRES_ROOT_KATEGORI;
      /** the column name for the PRES_ITEMS_GROUP field */
    public static final String PRES_ITEMS_GROUP;
      /** the column name for the GEN_ITEMS_GROUP field */
    public static final String GEN_ITEMS_GROUP;
      /** the column name for the PRES_ITEMS_MARGIN field */
    public static final String PRES_ITEMS_MARGIN;
      /** the column name for the GEN_ITEMS_MARGIN field */
    public static final String GEN_ITEMS_MARGIN;
      /** the column name for the GEN_USE_PRES_MARGIN field */
    public static final String GEN_USE_PRES_MARGIN;
      /** the column name for the POS_COMMA_SCALE field */
    public static final String POS_COMMA_SCALE;
      /** the column name for the ROUNDING_SCALE field */
    public static final String ROUNDING_SCALE;
      /** the column name for the ROUNDING_MODE field */
    public static final String ROUNDING_MODE;
      /** the column name for the ROUNDING_ITEM_CODE field */
    public static final String ROUNDING_ITEM_CODE;
      /** the column name for the POWDER_ITEM_CODE field */
    public static final String POWDER_ITEM_CODE;
      /** the column name for the WARNING_ITEMS field */
    public static final String WARNING_ITEMS;
      /** the column name for the INSURANCE_CTYPE field */
    public static final String INSURANCE_CTYPE;
      /** the column name for the INSURANCE_PTYPE field */
    public static final String INSURANCE_PTYPE;
      /** the column name for the ALERT_PRESCRIPTION field */
    public static final String ALERT_PRESCRIPTION;
      /** the column name for the ALERT_HOSTS field */
    public static final String ALERT_HOSTS;
      /** the column name for the REG_NO_FORMAT field */
    public static final String REG_NO_FORMAT;
      /** the column name for the REC_NO_FORMAT field */
    public static final String REC_NO_FORMAT;
      /** the column name for the LAB_NO_FORMAT field */
    public static final String LAB_NO_FORMAT;
      /** the column name for the CL_CONFIG_FILE field */
    public static final String CL_CONFIG_FILE;
      /** the column name for the CL_MENU_FILE field */
    public static final String CL_MENU_FILE;
  
    static
    {
          MED_CONFIG_ID = "med_config.MED_CONFIG_ID";
          PHARMACIST_NAME = "med_config.PHARMACIST_NAME";
          LICENSE_NO = "med_config.LICENSE_NO";
          WORK_LICENSE_NO = "med_config.WORK_LICENSE_NO";
          PRES_NO_FORMAT = "med_config.PRES_NO_FORMAT";
          DEFAULT_RFEE = "med_config.DEFAULT_RFEE";
          DEFAULT_PACKAGING = "med_config.DEFAULT_PACKAGING";
          RFEE_EDITABLE = "med_config.RFEE_EDITABLE";
          RFEE_MULTIPLIED = "med_config.RFEE_MULTIPLIED";
          RFEE_MIN_AMOUNT = "med_config.RFEE_MIN_AMOUNT";
          RFEE_MAX_AMOUNT = "med_config.RFEE_MAX_AMOUNT";
          PACK_MIN_AMOUNT = "med_config.PACK_MIN_AMOUNT";
          PACK_MAX_AMOUNT = "med_config.PACK_MAX_AMOUNT";
          POS_DEFAULT_RFEE = "med_config.POS_DEFAULT_RFEE";
          POS_DEFAULT_PACK = "med_config.POS_DEFAULT_PACK";
          POS_RFEE_EDITABLE = "med_config.POS_RFEE_EDITABLE";
          POS_RFEE_MULTIPLIED = "med_config.POS_RFEE_MULTIPLIED";
          POS_RFEE_MIN_AMOUNT = "med_config.POS_RFEE_MIN_AMOUNT";
          POS_RFEE_MAX_AMOUNT = "med_config.POS_RFEE_MAX_AMOUNT";
          POS_PACK_MIN_AMOUNT = "med_config.POS_PACK_MIN_AMOUNT";
          POS_PACK_MAX_AMOUNT = "med_config.POS_PACK_MAX_AMOUNT";
          RFEE_ITEM_CODE = "med_config.RFEE_ITEM_CODE";
          PACKAGING_ITEM_CODE = "med_config.PACKAGING_ITEM_CODE";
          PRES_ROOT_KATEGORI = "med_config.PRES_ROOT_KATEGORI";
          PRES_ITEMS_GROUP = "med_config.PRES_ITEMS_GROUP";
          GEN_ITEMS_GROUP = "med_config.GEN_ITEMS_GROUP";
          PRES_ITEMS_MARGIN = "med_config.PRES_ITEMS_MARGIN";
          GEN_ITEMS_MARGIN = "med_config.GEN_ITEMS_MARGIN";
          GEN_USE_PRES_MARGIN = "med_config.GEN_USE_PRES_MARGIN";
          POS_COMMA_SCALE = "med_config.POS_COMMA_SCALE";
          ROUNDING_SCALE = "med_config.ROUNDING_SCALE";
          ROUNDING_MODE = "med_config.ROUNDING_MODE";
          ROUNDING_ITEM_CODE = "med_config.ROUNDING_ITEM_CODE";
          POWDER_ITEM_CODE = "med_config.POWDER_ITEM_CODE";
          WARNING_ITEMS = "med_config.WARNING_ITEMS";
          INSURANCE_CTYPE = "med_config.INSURANCE_CTYPE";
          INSURANCE_PTYPE = "med_config.INSURANCE_PTYPE";
          ALERT_PRESCRIPTION = "med_config.ALERT_PRESCRIPTION";
          ALERT_HOSTS = "med_config.ALERT_HOSTS";
          REG_NO_FORMAT = "med_config.REG_NO_FORMAT";
          REC_NO_FORMAT = "med_config.REC_NO_FORMAT";
          LAB_NO_FORMAT = "med_config.LAB_NO_FORMAT";
          CL_CONFIG_FILE = "med_config.CL_CONFIG_FILE";
          CL_MENU_FILE = "med_config.CL_MENU_FILE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(MedConfigMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(MedConfigMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  44;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.medical.om.MedConfig";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseMedConfigPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(GEN_USE_PRES_MARGIN))
        {
            Object possibleBoolean = criteria.get(GEN_USE_PRES_MARGIN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(GEN_USE_PRES_MARGIN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ALERT_PRESCRIPTION))
        {
            Object possibleBoolean = criteria.get(ALERT_PRESCRIPTION);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ALERT_PRESCRIPTION, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(MED_CONFIG_ID);
          criteria.addSelectColumn(PHARMACIST_NAME);
          criteria.addSelectColumn(LICENSE_NO);
          criteria.addSelectColumn(WORK_LICENSE_NO);
          criteria.addSelectColumn(PRES_NO_FORMAT);
          criteria.addSelectColumn(DEFAULT_RFEE);
          criteria.addSelectColumn(DEFAULT_PACKAGING);
          criteria.addSelectColumn(RFEE_EDITABLE);
          criteria.addSelectColumn(RFEE_MULTIPLIED);
          criteria.addSelectColumn(RFEE_MIN_AMOUNT);
          criteria.addSelectColumn(RFEE_MAX_AMOUNT);
          criteria.addSelectColumn(PACK_MIN_AMOUNT);
          criteria.addSelectColumn(PACK_MAX_AMOUNT);
          criteria.addSelectColumn(POS_DEFAULT_RFEE);
          criteria.addSelectColumn(POS_DEFAULT_PACK);
          criteria.addSelectColumn(POS_RFEE_EDITABLE);
          criteria.addSelectColumn(POS_RFEE_MULTIPLIED);
          criteria.addSelectColumn(POS_RFEE_MIN_AMOUNT);
          criteria.addSelectColumn(POS_RFEE_MAX_AMOUNT);
          criteria.addSelectColumn(POS_PACK_MIN_AMOUNT);
          criteria.addSelectColumn(POS_PACK_MAX_AMOUNT);
          criteria.addSelectColumn(RFEE_ITEM_CODE);
          criteria.addSelectColumn(PACKAGING_ITEM_CODE);
          criteria.addSelectColumn(PRES_ROOT_KATEGORI);
          criteria.addSelectColumn(PRES_ITEMS_GROUP);
          criteria.addSelectColumn(GEN_ITEMS_GROUP);
          criteria.addSelectColumn(PRES_ITEMS_MARGIN);
          criteria.addSelectColumn(GEN_ITEMS_MARGIN);
          criteria.addSelectColumn(GEN_USE_PRES_MARGIN);
          criteria.addSelectColumn(POS_COMMA_SCALE);
          criteria.addSelectColumn(ROUNDING_SCALE);
          criteria.addSelectColumn(ROUNDING_MODE);
          criteria.addSelectColumn(ROUNDING_ITEM_CODE);
          criteria.addSelectColumn(POWDER_ITEM_CODE);
          criteria.addSelectColumn(WARNING_ITEMS);
          criteria.addSelectColumn(INSURANCE_CTYPE);
          criteria.addSelectColumn(INSURANCE_PTYPE);
          criteria.addSelectColumn(ALERT_PRESCRIPTION);
          criteria.addSelectColumn(ALERT_HOSTS);
          criteria.addSelectColumn(REG_NO_FORMAT);
          criteria.addSelectColumn(REC_NO_FORMAT);
          criteria.addSelectColumn(LAB_NO_FORMAT);
          criteria.addSelectColumn(CL_CONFIG_FILE);
          criteria.addSelectColumn(CL_MENU_FILE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MedConfig row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            MedConfig obj = (MedConfig) cls.newInstance();
            MedConfigPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      MedConfig obj)
        throws TorqueException
    {
        try
        {
                obj.setMedConfigId(row.getValue(offset + 0).asString());
                  obj.setPharmacistName(row.getValue(offset + 1).asString());
                  obj.setLicenseNo(row.getValue(offset + 2).asString());
                  obj.setWorkLicenseNo(row.getValue(offset + 3).asString());
                  obj.setPresNoFormat(row.getValue(offset + 4).asString());
                  obj.setDefaultRfee(row.getValue(offset + 5).asString());
                  obj.setDefaultPackaging(row.getValue(offset + 6).asString());
                  obj.setRfeeEditable(row.getValue(offset + 7).asBoolean());
                  obj.setRfeeMultiplied(row.getValue(offset + 8).asBoolean());
                  obj.setRfeeMinAmount(row.getValue(offset + 9).asBigDecimal());
                  obj.setRfeeMaxAmount(row.getValue(offset + 10).asBigDecimal());
                  obj.setPackMinAmount(row.getValue(offset + 11).asBigDecimal());
                  obj.setPackMaxAmount(row.getValue(offset + 12).asBigDecimal());
                  obj.setPosDefaultRfee(row.getValue(offset + 13).asString());
                  obj.setPosDefaultPack(row.getValue(offset + 14).asString());
                  obj.setPosRfeeEditable(row.getValue(offset + 15).asBoolean());
                  obj.setPosRfeeMultiplied(row.getValue(offset + 16).asBoolean());
                  obj.setPosRfeeMinAmount(row.getValue(offset + 17).asBigDecimal());
                  obj.setPosRfeeMaxAmount(row.getValue(offset + 18).asBigDecimal());
                  obj.setPosPackMinAmount(row.getValue(offset + 19).asBigDecimal());
                  obj.setPosPackMaxAmount(row.getValue(offset + 20).asBigDecimal());
                  obj.setRfeeItemCode(row.getValue(offset + 21).asString());
                  obj.setPackagingItemCode(row.getValue(offset + 22).asString());
                  obj.setPresRootKategori(row.getValue(offset + 23).asString());
                  obj.setPresItemsGroup(row.getValue(offset + 24).asString());
                  obj.setGenItemsGroup(row.getValue(offset + 25).asString());
                  obj.setPresItemsMargin(row.getValue(offset + 26).asString());
                  obj.setGenItemsMargin(row.getValue(offset + 27).asString());
                  obj.setGenUsePresMargin(row.getValue(offset + 28).asBoolean());
                  obj.setPosCommaScale(row.getValue(offset + 29).asInt());
                  obj.setRoundingScale(row.getValue(offset + 30).asInt());
                  obj.setRoundingMode(row.getValue(offset + 31).asInt());
                  obj.setRoundingItemCode(row.getValue(offset + 32).asString());
                  obj.setPowderItemCode(row.getValue(offset + 33).asString());
                  obj.setWarningItems(row.getValue(offset + 34).asString());
                  obj.setInsuranceCtype(row.getValue(offset + 35).asString());
                  obj.setInsurancePtype(row.getValue(offset + 36).asString());
                  obj.setAlertPrescription(row.getValue(offset + 37).asBoolean());
                  obj.setAlertHosts(row.getValue(offset + 38).asString());
                  obj.setRegNoFormat(row.getValue(offset + 39).asString());
                  obj.setRecNoFormat(row.getValue(offset + 40).asString());
                  obj.setLabNoFormat(row.getValue(offset + 41).asString());
                  obj.setClConfigFile(row.getValue(offset + 42).asString());
                  obj.setClMenuFile(row.getValue(offset + 43).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseMedConfigPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(GEN_USE_PRES_MARGIN))
        {
            Object possibleBoolean = criteria.get(GEN_USE_PRES_MARGIN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(GEN_USE_PRES_MARGIN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ALERT_PRESCRIPTION))
        {
            Object possibleBoolean = criteria.get(ALERT_PRESCRIPTION);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ALERT_PRESCRIPTION, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(MedConfigPeer.row2Object(row, 1,
                MedConfigPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseMedConfigPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(MED_CONFIG_ID, criteria.remove(MED_CONFIG_ID));
                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(GEN_USE_PRES_MARGIN))
        {
            Object possibleBoolean = criteria.get(GEN_USE_PRES_MARGIN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(GEN_USE_PRES_MARGIN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ALERT_PRESCRIPTION))
        {
            Object possibleBoolean = criteria.get(ALERT_PRESCRIPTION);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ALERT_PRESCRIPTION, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         MedConfigPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_EDITABLE))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_EDITABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_EDITABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(POS_RFEE_MULTIPLIED))
        {
            Object possibleBoolean = criteria.get(POS_RFEE_MULTIPLIED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(POS_RFEE_MULTIPLIED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(GEN_USE_PRES_MARGIN))
        {
            Object possibleBoolean = criteria.get(GEN_USE_PRES_MARGIN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(GEN_USE_PRES_MARGIN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(ALERT_PRESCRIPTION))
        {
            Object possibleBoolean = criteria.get(ALERT_PRESCRIPTION);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(ALERT_PRESCRIPTION, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(MedConfig obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(MedConfig obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(MedConfig obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(MedConfig obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(MedConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(MedConfig obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(MedConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(MedConfig obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(MedConfig) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(MedConfig obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseMedConfigPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(MED_CONFIG_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( MedConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(MED_CONFIG_ID, obj.getMedConfigId());
              criteria.add(PHARMACIST_NAME, obj.getPharmacistName());
              criteria.add(LICENSE_NO, obj.getLicenseNo());
              criteria.add(WORK_LICENSE_NO, obj.getWorkLicenseNo());
              criteria.add(PRES_NO_FORMAT, obj.getPresNoFormat());
              criteria.add(DEFAULT_RFEE, obj.getDefaultRfee());
              criteria.add(DEFAULT_PACKAGING, obj.getDefaultPackaging());
              criteria.add(RFEE_EDITABLE, obj.getRfeeEditable());
              criteria.add(RFEE_MULTIPLIED, obj.getRfeeMultiplied());
              criteria.add(RFEE_MIN_AMOUNT, obj.getRfeeMinAmount());
              criteria.add(RFEE_MAX_AMOUNT, obj.getRfeeMaxAmount());
              criteria.add(PACK_MIN_AMOUNT, obj.getPackMinAmount());
              criteria.add(PACK_MAX_AMOUNT, obj.getPackMaxAmount());
              criteria.add(POS_DEFAULT_RFEE, obj.getPosDefaultRfee());
              criteria.add(POS_DEFAULT_PACK, obj.getPosDefaultPack());
              criteria.add(POS_RFEE_EDITABLE, obj.getPosRfeeEditable());
              criteria.add(POS_RFEE_MULTIPLIED, obj.getPosRfeeMultiplied());
              criteria.add(POS_RFEE_MIN_AMOUNT, obj.getPosRfeeMinAmount());
              criteria.add(POS_RFEE_MAX_AMOUNT, obj.getPosRfeeMaxAmount());
              criteria.add(POS_PACK_MIN_AMOUNT, obj.getPosPackMinAmount());
              criteria.add(POS_PACK_MAX_AMOUNT, obj.getPosPackMaxAmount());
              criteria.add(RFEE_ITEM_CODE, obj.getRfeeItemCode());
              criteria.add(PACKAGING_ITEM_CODE, obj.getPackagingItemCode());
              criteria.add(PRES_ROOT_KATEGORI, obj.getPresRootKategori());
              criteria.add(PRES_ITEMS_GROUP, obj.getPresItemsGroup());
              criteria.add(GEN_ITEMS_GROUP, obj.getGenItemsGroup());
              criteria.add(PRES_ITEMS_MARGIN, obj.getPresItemsMargin());
              criteria.add(GEN_ITEMS_MARGIN, obj.getGenItemsMargin());
              criteria.add(GEN_USE_PRES_MARGIN, obj.getGenUsePresMargin());
              criteria.add(POS_COMMA_SCALE, obj.getPosCommaScale());
              criteria.add(ROUNDING_SCALE, obj.getRoundingScale());
              criteria.add(ROUNDING_MODE, obj.getRoundingMode());
              criteria.add(ROUNDING_ITEM_CODE, obj.getRoundingItemCode());
              criteria.add(POWDER_ITEM_CODE, obj.getPowderItemCode());
              criteria.add(WARNING_ITEMS, obj.getWarningItems());
              criteria.add(INSURANCE_CTYPE, obj.getInsuranceCtype());
              criteria.add(INSURANCE_PTYPE, obj.getInsurancePtype());
              criteria.add(ALERT_PRESCRIPTION, obj.getAlertPrescription());
              criteria.add(ALERT_HOSTS, obj.getAlertHosts());
              criteria.add(REG_NO_FORMAT, obj.getRegNoFormat());
              criteria.add(REC_NO_FORMAT, obj.getRecNoFormat());
              criteria.add(LAB_NO_FORMAT, obj.getLabNoFormat());
              criteria.add(CL_CONFIG_FILE, obj.getClConfigFile());
              criteria.add(CL_MENU_FILE, obj.getClMenuFile());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( MedConfig obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(MED_CONFIG_ID, obj.getMedConfigId());
                          criteria.add(PHARMACIST_NAME, obj.getPharmacistName());
                          criteria.add(LICENSE_NO, obj.getLicenseNo());
                          criteria.add(WORK_LICENSE_NO, obj.getWorkLicenseNo());
                          criteria.add(PRES_NO_FORMAT, obj.getPresNoFormat());
                          criteria.add(DEFAULT_RFEE, obj.getDefaultRfee());
                          criteria.add(DEFAULT_PACKAGING, obj.getDefaultPackaging());
                          criteria.add(RFEE_EDITABLE, obj.getRfeeEditable());
                          criteria.add(RFEE_MULTIPLIED, obj.getRfeeMultiplied());
                          criteria.add(RFEE_MIN_AMOUNT, obj.getRfeeMinAmount());
                          criteria.add(RFEE_MAX_AMOUNT, obj.getRfeeMaxAmount());
                          criteria.add(PACK_MIN_AMOUNT, obj.getPackMinAmount());
                          criteria.add(PACK_MAX_AMOUNT, obj.getPackMaxAmount());
                          criteria.add(POS_DEFAULT_RFEE, obj.getPosDefaultRfee());
                          criteria.add(POS_DEFAULT_PACK, obj.getPosDefaultPack());
                          criteria.add(POS_RFEE_EDITABLE, obj.getPosRfeeEditable());
                          criteria.add(POS_RFEE_MULTIPLIED, obj.getPosRfeeMultiplied());
                          criteria.add(POS_RFEE_MIN_AMOUNT, obj.getPosRfeeMinAmount());
                          criteria.add(POS_RFEE_MAX_AMOUNT, obj.getPosRfeeMaxAmount());
                          criteria.add(POS_PACK_MIN_AMOUNT, obj.getPosPackMinAmount());
                          criteria.add(POS_PACK_MAX_AMOUNT, obj.getPosPackMaxAmount());
                          criteria.add(RFEE_ITEM_CODE, obj.getRfeeItemCode());
                          criteria.add(PACKAGING_ITEM_CODE, obj.getPackagingItemCode());
                          criteria.add(PRES_ROOT_KATEGORI, obj.getPresRootKategori());
                          criteria.add(PRES_ITEMS_GROUP, obj.getPresItemsGroup());
                          criteria.add(GEN_ITEMS_GROUP, obj.getGenItemsGroup());
                          criteria.add(PRES_ITEMS_MARGIN, obj.getPresItemsMargin());
                          criteria.add(GEN_ITEMS_MARGIN, obj.getGenItemsMargin());
                          criteria.add(GEN_USE_PRES_MARGIN, obj.getGenUsePresMargin());
                          criteria.add(POS_COMMA_SCALE, obj.getPosCommaScale());
                          criteria.add(ROUNDING_SCALE, obj.getRoundingScale());
                          criteria.add(ROUNDING_MODE, obj.getRoundingMode());
                          criteria.add(ROUNDING_ITEM_CODE, obj.getRoundingItemCode());
                          criteria.add(POWDER_ITEM_CODE, obj.getPowderItemCode());
                          criteria.add(WARNING_ITEMS, obj.getWarningItems());
                          criteria.add(INSURANCE_CTYPE, obj.getInsuranceCtype());
                          criteria.add(INSURANCE_PTYPE, obj.getInsurancePtype());
                          criteria.add(ALERT_PRESCRIPTION, obj.getAlertPrescription());
                          criteria.add(ALERT_HOSTS, obj.getAlertHosts());
                          criteria.add(REG_NO_FORMAT, obj.getRegNoFormat());
                          criteria.add(REC_NO_FORMAT, obj.getRecNoFormat());
                          criteria.add(LAB_NO_FORMAT, obj.getLabNoFormat());
                          criteria.add(CL_CONFIG_FILE, obj.getClConfigFile());
                          criteria.add(CL_MENU_FILE, obj.getClMenuFile());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedConfig retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedConfig retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedConfig retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        MedConfig retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedConfig retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (MedConfig)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( MED_CONFIG_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

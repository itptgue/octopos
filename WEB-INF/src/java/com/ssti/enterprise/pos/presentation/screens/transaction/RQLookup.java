package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.framework.tools.StringUtil;

public class RQLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{		
    		String sVendorID = data.getParameters().getString ("VendorId");
    		String sLocationID = data.getParameters().getString ("LocationId");
    		String sRequestNo = data.getParameters().getString ("TransactionNo");
    		String sDisplayID = data.getParameters().getString ("DisplayId", "");
    		context.put ("PurchaseRequests", PurchaseRequestTool.findRequest(sVendorID, sLocationID, sRequestNo,""));    			
    		if (StringUtil.isNotEmpty(sDisplayID))
    		{
    			context.put ("Details", PurchaseRequestTool.getDetailsByID (sDisplayID));
    		}
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("PR Lookup Failed : " + _oEx.getMessage());
    	}
    }
}
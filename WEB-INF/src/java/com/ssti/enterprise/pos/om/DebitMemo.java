
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MemoOM;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2017-10-20
 * - add bank, cashFlowType, crossAccount
 * </pre><br>
 */
public  class DebitMemo
    extends com.ssti.enterprise.pos.om.BaseDebitMemo
    implements Persistent, MemoOM
{
	public String getEntityId() 
	{
		return getVendorId();
	}

	public String getEntityName() 
	{
		return getVendorName();
	}
	
	public String getMemoId() 
	{
		return getDebitMemoId();
	}
	
	public String getMemoNo() 
	{
		return getDebitMemoNo();
	}

	Bank bank = null;
	public Bank getBank()
	{
		try
		{
			if(bank == null)
			{
				bank = BankTool.getBankByID(getBankId());	
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return bank;
	}
	
	CashFlowType cashFlowType = null;
	public CashFlowType getCashFlowType()
	{
		try
		{
			if(cashFlowType == null && StringUtil.isNotEmpty(getCashFlowTypeId()))
			{
				cashFlowType = CashFlowTypeTool.getTypeByID(getCashFlowTypeId(),null);	
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return cashFlowType;
	}	
	
	Account account;
	public Account getAccount()
	{
		if(account == null && StringUtil.isNotEmpty(getAccountId()))
		{
			try 
			{
				account = AccountTool.getAccountByID(getAccountId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return account;
	}
	
	Account crossAccount;
	public Account getCrossAccount()
	{
		if(crossAccount == null && StringUtil.isNotEmpty(getCrossAccountId()))
		{
			try 
			{
				crossAccount = AccountTool.getAccountByID(getCrossAccountId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return crossAccount;
	}		
	
	PurchaseReturn returnTrans;
	public PurchaseReturn getReturnTrans()	
	{
		if(returnTrans == null && StringUtil.isNotEmpty(getTransactionId()))
		{
			try 
			{
				returnTrans = PurchaseReturnTool.getHeaderByID(getTransactionId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnTrans;
	}	

	ApPayment paymentTrans;
	public ApPayment gePaymentTrans()	
	{
		if(getStatus() != AppAttributes.i_MEMO_CANCELLED)
		{
			try 
			{
				paymentTrans = PayablePaymentTool.getHeaderByID(getPaymentTransId());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return paymentTrans;
	}
	
	public String getStatusStr()
	{
		return DebitMemoTool.getStatusString(getStatus());
	}
	
	boolean updateOrder = false;
	public boolean getUpdateOrder() {
		return updateOrder;
	}

	public void setUpdateOrder(boolean updateOrder) {
		this.updateOrder = updateOrder;
	}			
}

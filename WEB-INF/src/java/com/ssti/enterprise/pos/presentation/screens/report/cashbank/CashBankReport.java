package com.ssti.enterprise.pos.presentation.screens.report.cashbank;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;

public class CashBankReport extends ReportSecureScreen
{
	static String[] a_PERMS = {"View Cash Bank Reports"};
	String sBankID = "";
	
	@Override
	protected boolean isAuthorized(RunData data, String[] _aPerms) 
		throws Exception 
	{
		return super.isAuthorized(data, a_PERMS);
	}
	
	@Override
	public void setParams(RunData data) 
	{
		super.setParams(data);
		sBankID = data.getParameters().getString("BankId");		
	}
	
	public void doBuildTemplate(RunData data, Context context) 
	{
		super.doBuildTemplate(data, context);
    	bPrintable = data.getParameters().getBoolean("printable");
    	bExcel = data.getParameters().getBoolean("excel");
    	if (bPrintable) 
    	{
    		data.setLayoutTemplate("/Printable.vm");
     	    context.put("printable", Boolean.valueOf(bPrintable));	
     	    context.put("excel", Boolean.valueOf(bExcel));	
    	}
		context.put ("cashflow", CashFlowTool.getInstance());
	}
}

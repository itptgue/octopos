package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BankReconcileMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BankReconcileMapBuilder";

    /**
     * Item
     * @deprecated use BankReconcilePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "bank_reconcile";
    }

  
    /**
     * bank_reconcile.BANK_RECONCILE_ID
     * @return the column name for the BANK_RECONCILE_ID field
     * @deprecated use BankReconcilePeer.bank_reconcile.BANK_RECONCILE_ID constant
     */
    public static String getBankReconcile_BankReconcileId()
    {
        return "bank_reconcile.BANK_RECONCILE_ID";
    }
  
    /**
     * bank_reconcile.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use BankReconcilePeer.bank_reconcile.BANK_ID constant
     */
    public static String getBankReconcile_BankId()
    {
        return "bank_reconcile.BANK_ID";
    }
  
    /**
     * bank_reconcile.BANK_AMOUNT
     * @return the column name for the BANK_AMOUNT field
     * @deprecated use BankReconcilePeer.bank_reconcile.BANK_AMOUNT constant
     */
    public static String getBankReconcile_BankAmount()
    {
        return "bank_reconcile.BANK_AMOUNT";
    }
  
    /**
     * bank_reconcile.CALCULATED_AMOUNT
     * @return the column name for the CALCULATED_AMOUNT field
     * @deprecated use BankReconcilePeer.bank_reconcile.CALCULATED_AMOUNT constant
     */
    public static String getBankReconcile_CalculatedAmount()
    {
        return "bank_reconcile.CALCULATED_AMOUNT";
    }
  
    /**
     * bank_reconcile.RECONCILE_DATE
     * @return the column name for the RECONCILE_DATE field
     * @deprecated use BankReconcilePeer.bank_reconcile.RECONCILE_DATE constant
     */
    public static String getBankReconcile_ReconcileDate()
    {
        return "bank_reconcile.RECONCILE_DATE";
    }
  
    /**
     * bank_reconcile.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use BankReconcilePeer.bank_reconcile.USER_NAME constant
     */
    public static String getBankReconcile_UserName()
    {
        return "bank_reconcile.USER_NAME";
    }
  
    /**
     * bank_reconcile.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use BankReconcilePeer.bank_reconcile.DESCRIPTION constant
     */
    public static String getBankReconcile_Description()
    {
        return "bank_reconcile.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("bank_reconcile");
        TableMap tMap = dbMap.getTable("bank_reconcile");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("bank_reconcile.BANK_RECONCILE_ID", "");
                          tMap.addColumn("bank_reconcile.BANK_ID", "");
                            tMap.addColumn("bank_reconcile.BANK_AMOUNT", bd_ZERO);
                            tMap.addColumn("bank_reconcile.CALCULATED_AMOUNT", bd_ZERO);
                          tMap.addColumn("bank_reconcile.RECONCILE_DATE", new Date());
                          tMap.addColumn("bank_reconcile.USER_NAME", "");
                          tMap.addColumn("bank_reconcile.DESCRIPTION", "");
          }
}

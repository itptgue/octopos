package com.ssti.framework.db;

import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DBTool.java,v 1.2 2007/02/23 14:11:39 albert Exp $ <br>
 *
 * <pre>
 * $Log: DBTool.java,v $
 * Revision 1.2  2007/02/23 14:11:39  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public interface DBTool extends Attributes
{	
	//added here so we could use this tool w/o Turbine
	static final String s_FILE_SEPARATOR = System.getProperty("file.separator");
	static final String s_LINE_SEPARATOR = System.getProperty("line.separator");
	
	public void backup() throws Exception;	
	public void restore() throws Exception;
	public void newDB() throws Exception;
	
	public String getResult();
}

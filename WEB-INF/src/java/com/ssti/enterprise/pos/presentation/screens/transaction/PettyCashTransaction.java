package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.PettyCashTool;

public class PettyCashTransaction extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Petty Cash"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == 4)
			{
				sID = data.getParameters().getString ("id");
				context.put("oPettyCash", PettyCashTool.getPettyCashByID(sID, null));
    		}    		
    	}
    	catch(Exception _oEx)
    	{
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}

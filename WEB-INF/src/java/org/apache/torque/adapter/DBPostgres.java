package org.apache.torque.adapter;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DBPostgres extends DB
{
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    private SimpleDateFormat sdf;
    
    protected DBPostgres()
    {
        sdf = null;
        sdf = new SimpleDateFormat(DATE_FORMAT);
    }

    public String toUpperCase(String in)
    {
        String s = "UPPER(" + in + ")";
        return s;
    }

    public String ignoreCase(String in)
    {
        String s = "UPPER(" + in + ")";
        return s;
    }

    public String getIDMethodType()
    {
        return "sequence";
    }

    public String getIDMethodSQL(Object name)
    {
        return "select nextval('" + name + "')";
    }

    public void lockTable(Connection connection, String s)
        throws SQLException
    {
    }

    public void unlockTable(Connection connection, String s)
        throws SQLException
    {
    }

    public boolean supportsNativeLimit()
    {
        return true;
    }

    public boolean supportsNativeOffset()
    {
        return true;
    }

    public int getLimitStyle()
    {
        return 1;
    }

    public String getBooleanString(Boolean b)
    {
        return b != null ? Boolean.TRUE.equals(b) ? "TRUE" : "FALSE" : "FALSE";
    }

    public String getDateString(Date date)
    {
        StringBuilder dateBuf = new StringBuilder();
        char delim = getStringDelimiter();
        dateBuf.append(delim);
        dateBuf.append(sdf.format(date));
        dateBuf.append(delim);
        return dateBuf.toString();
    }
}
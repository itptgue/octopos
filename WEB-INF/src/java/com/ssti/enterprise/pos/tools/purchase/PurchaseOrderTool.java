package com.ssti.enterprise.pos.tools.purchase;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Message.RecipientType;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseOrderDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorPriceListDetail;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.ItemVendorTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.MailTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.framework.tools.Attributes;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for PurchaseOrder OM
 * @see PurchaseOrder, PurchaseOrderPeer, PurchaseOrderDetail, PurchaseOrderDetailPeer
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseOrderTool.java,v 1.27 2009/05/04 02:04:54 albert Exp $ <br>
 *
 * <pre>
 * 
 * 2017-09-01
 * add method getPOByRQID get distinct PO list by RQ ID
 * 
 * 2017-07-10
 * Change method createPO, now ItemList variable can contains Record / PurchaseRequestDetail 

 * 2017-05-26
 * Override method getOnOrderQty (String _sItemID, String _sLocID) add parameter LocationID
 * 
 * 2017-03-01
 * Add method sendPOEmail - send PO to Vendor Email @see templates/app/print/modules/POEmail.vm
 * 
 * 2015-11-25 
 * Change method findData - add status 99 for status query ordered / received, templates: PurchaseOrderView.vm
 * 
 * 2015-03-10
 * Change method createPO fix PO Purchase Price, because unit is using purchase unit 
 * 
 * </pre><br>
 */
public class PurchaseOrderTool extends BaseTool
{
	private static Log log = LogFactory.getLog(PurchaseOrderTool.class);
	
    public static final String s_ITEM = new StringBuilder(PurchaseOrderPeer.PURCHASE_ORDER_ID).append(",")
    .append(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID).append(",")
	.append(PurchaseOrderDetailPeer.ITEM_ID).toString();

	public static final String s_TAX = new StringBuilder(PurchaseOrderPeer.PURCHASE_ORDER_ID).append(",")
	.append(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID).append(",")
	.append(PurchaseOrderDetailPeer.TAX_ID).toString();
    
    protected static Map m_FIND_PEER = new HashMap();
    
    static
	{   
	    m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), PurchaseOrderPeer.PURCHASE_ORDER_NO);      	  
        m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), PurchaseOrderPeer.REMARK);                  
        m_FIND_PEER.put (Integer.valueOf(i_VENDOR), 	 PurchaseOrderPeer.VENDOR_ID);                  
        m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), PurchaseOrderPeer.CREATE_BY);                 
        m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), PurchaseOrderPeer.CONFIRM_BY);               
        m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), PurchaseOrderPeer.REFERENCE_NO);             
        m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), PurchaseOrderPeer.LOCATION_ID);             
        
        m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), PurchaseOrderPeer.PAYMENT_TYPE_ID);
        m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), PurchaseOrderPeer.PAYMENT_TERM_ID);
        m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), PurchaseOrderPeer.COURIER_ID);
        m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), PurchaseOrderPeer.FOB_ID);
        m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), PurchaseOrderPeer.CURRENCY_ID);
        m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), PurchaseOrderPeer.BANK_ID);

        addInv(m_FIND_PEER, s_ITEM);
	}   

	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), PurchaseOrderPeer.TRANSACTION_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), PurchaseOrderPeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), PurchaseOrderPeer.TRANSACTION_STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), PurchaseOrderPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), PurchaseOrderPeer.CREATE_BY);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), PurchaseOrderPeer.CONFIRM_BY);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), PurchaseOrderPeer.PAYMENT_TYPE_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), PurchaseOrderPeer.CURRENCY_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), PurchaseOrderPeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), PurchaseOrderPeer.LOCATION_ID); 
	}
    
    public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, true, _iSelected);}
	
    public static PurchaseOrder getHeaderByID(String _sID)
	    throws Exception
	{
	    return getHeaderByID(_sID, null);
	}
    
    /**
     * get PurchaseOrder object
     * @param _sID
     * @param _oConn
     * @return PurchaseOrder object
     * @throws Exception
     */
    public static PurchaseOrder getHeaderByID(String _sID, Connection _oConn)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(PurchaseOrderPeer.PURCHASE_ORDER_ID, _sID);
        List vData = null;
        vData = PurchaseOrderPeer.doSelect(oCrit, _oConn);
        if (vData != null && vData.size() > 0) 
        {
            return (PurchaseOrder) vData.get(0);
        }
        return null;
    }

    public static PurchaseOrder getHeaderByDetailID(String _sID)
        throws Exception
    {
        PurchaseOrderDetail oPODetail = getDetailsByPODetailID(_sID);
        Criteria oCrit = new Criteria();
        oCrit.add(PurchaseOrderPeer.PURCHASE_ORDER_ID, oPODetail.getPurchaseOrderId());

        List vData = PurchaseOrderPeer.doSelect(oCrit);
        if (vData.size() > 0) {
            return (PurchaseOrder) vData.get(0);
        }
        return null;
    }
    
    public static List getDetailsByID(String _sID)
        throws Exception
    {
        return getDetailsByID(_sID, null);
    }
    
    public static List getDetailsByID(String _sID, Connection _oConn)
        throws Exception
    {
        return getDetailsByID(_sID, i_PURCHASE_SORT_BY, _oConn);
    }
    
    /**
     * get List of Purchase Order Details
     * @param _sID
     * @param _iOrderBy
     * @param _oConn
     * @return List of PurchaseOrderDetail
     * @throws Exception
     */
    public static List getDetailsByID(String _sID, int _iOrderBy, Connection _oConn)
        throws Exception
    {        
        Criteria oCrit = new Criteria();
        oCrit.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, _sID);

        if(_iOrderBy == 1) // item plu
        {
        	oCrit.addAscendingOrderByColumn(PurchaseOrderDetailPeer.ITEM_CODE);
        }
        else if(_iOrderBy == 2) // item name 
        {

        	oCrit.addAscendingOrderByColumn(PurchaseOrderDetailPeer.ITEM_NAME);
        }
        else if(_iOrderBy == 3) // item name desc
        {
        	oCrit.addDescendingOrderByColumn(PurchaseOrderDetailPeer.DESCRIPTION);
        } 
        else
        {
        	oCrit.addDescendingOrderByColumn(PurchaseOrderDetailPeer.INDEX_NO);
        }
        
        List vData = PurchaseOrderDetailPeer.doSelect(oCrit, _oConn);
        return vData;
    }

    public static PurchaseOrderDetail getDetailsByPODetailID(String _sID)
	 	throws Exception
	{
	    return getDetailsByPODetailID(_sID, null);
	}
    
    public static PurchaseOrderDetail getDetailsByPODetailID(String _sID, Connection _oConn)
	 	throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_DETAIL_ID, _sID);
	    List vData = PurchaseOrderDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) {
	        return ((PurchaseOrderDetail)vData.get(0));
	    }
	    return null;
	}
    
    public static List getPOByRQID(String _sRQID, boolean _bActiveOnly, Connection _oConn)
	 	throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseOrderDetailPeer.REQUEST_ID, _sRQID);
	    oCrit.addJoin(PurchaseOrderPeer.PURCHASE_ORDER_ID, PurchaseOrderDetailPeer.PURCHASE_ORDER_ID);
	    if(_bActiveOnly)
	    {
	    	oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, i_PO_CANCELLED, Criteria.NOT_EQUAL);
	    }
	    List v = PurchaseOrderPeer.doSelect(oCrit, _oConn);
	    return BeanUtil.getDistinctListByField(v, "purchaseOrderId");
	}
        
    private static void deleteDetailsByID (String _sID,Connection _oConn) 
	    throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, _sID);
	    PurchaseOrderDetailPeer.doDelete (oCrit,_oConn);
	}
    
    public static PurchaseOrder getByNoAndStatus(String _sNo, int _iStatus)
	    throws Exception
	{
        Criteria oCrit = new Criteria();
        oCrit.add(PurchaseOrderPeer.PURCHASE_ORDER_NO, _sNo);
        oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, _iStatus);        
        List vData = PurchaseOrderPeer.doSelect(oCrit);
        if (vData != null && vData.size() > 0) 
        {
            return (PurchaseOrder) vData.get(0);
        }
        return null;
	}
    
    public static String getPONoByID(String _sID)
        throws Exception
    {
        PurchaseOrder oPO = getHeaderByID (_sID);
        if (oPO != null) return oPO.getPurchaseOrderNo();
        return "";
    }

    public static List getPOByStatusAndVendorID (String _sVendorID, String _sLocationID, int _iStatus)
	    throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseOrderPeer.VENDOR_ID, _sVendorID);
	    oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, _iStatus);
	    if (StringUtil.isNotEmpty(_sLocationID))
	    {
	    	oCrit.add(PurchaseOrderPeer.LOCATION_ID, _sLocationID);
	    }
	    return PurchaseOrderPeer.doSelect(oCrit);
	}
    
    public static String getStatusString (int _iStatusID)
    {
        if (_iStatusID == i_PO_NEW) return LocaleTool.getString("new");
        if (_iStatusID == i_PO_ORDERED) return LocaleTool.getString("ordered");
        if (_iStatusID == i_PO_RECEIVED) return LocaleTool.getString("received");
        if (_iStatusID == i_PO_CANCELLED) return LocaleTool.getString("cancelled");
        return LocaleTool.getString("closed");    
    }   
    
    /**
     * Update PO Detail Received Qty From Qty Received in Purchase Receipt
     * 
     * @param _sPOID
     * @param _vPRDet
     * @param _bClosePO
     * @param _bIsCancel
     * @param _oConn
     * @throws Exception
     */
    public static void updatePOFromPR(String _sPOID, 
    								  List _vPRDet, 
    								  boolean _bClosePO, 
									  boolean _bIsCancel, 
									  Connection _oConn)
        throws Exception
    {
    	validate(_oConn);
        try
        {
            for(int i = 0; i < _vPRDet.size(); i++)
            {      
                PurchaseReceiptDetail oPRDet = (PurchaseReceiptDetail) _vPRDet.get(i);
                if(StringUtil.isNotEmpty(oPRDet.getPurchaseOrderDetailId())) //if PO Detail 
                {
                    Criteria oCrit = new Criteria();
                    oCrit.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_DETAIL_ID, oPRDet.getPurchaseOrderDetailId());
                    //get PO Detail By PO Detail ID
                    List vPODet = PurchaseOrderDetailPeer.doSelect(oCrit,_oConn);
                    if (vPODet.size() > 0) 
                    {
                        //cek PO Qty and Update Received Qty on PO Detail
                        PurchaseOrderDetail oPODet = (PurchaseOrderDetail)vPODet.get(0);
                        double dPOQty = oPODet.getQty().doubleValue();
                        double dReceived = oPODet.getReceivedQty().doubleValue();
                        double dPRQty = oPRDet.getQty().doubleValue();
                        
                        log.debug("dPOQty : " + dPOQty);                        
                        log.debug("dReceived : " + dReceived);
                        log.debug("dPRQty : " + dPRQty);
                        
                        //if cancel then minus qty
                        if (_bIsCancel) dPRQty = dPRQty * -1;

                        double dAfterReceipt = dReceived + dPRQty;
                        
                        oPODet.setReceivedQty(new BigDecimal(dAfterReceipt));
                        oPODet.save(_oConn);
                    }// end if po detail size > 0
                }// end if pr is from po
            }//end for
            
            if(StringUtil.isNotEmpty(_sPOID))
            {
                //to check balance item on PO and PR
       			//if the item was balance then the PO is close
            	updateStatusFromReceivedItem(_sPOID, _oConn);
                if(_bClosePO) 
                {
                	changeStatusByPOID(_sPOID, i_PO_CLOSED, _oConn);
                }
            }
        }
        catch(Exception _oEx)
        {
            String sMessage = "Update PO From PR Error : " + _oEx.getMessage();
            log.error(_oEx);
            throw new NestableException(sMessage, _oEx);
        }
    }
    
    /**
     * Update PO Detail Received Qty From Qty Received in Purchase Invoice
     * 
     * @param _sPOID
     * @param _vPID
     * @param _bClosePO
     * @param _bIsCancel
     * @param _oConn
     * @throws Exception
     */
    public static void updatePOFromPI(List _vPID, 
    								  boolean _bClosePO, 
									  boolean _bIsCancel, 
									  Connection _oConn)
        throws Exception
    {
    	validate(_oConn);
        try
        {
        	String sPOID = "";
            for(int i = 0; i < _vPID.size(); i++)
            {      
                PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail) _vPID.get(i);
		    	sPOID = oPID.getPurchaseOrderId();
		    	String sPRID = oPID.getPurchaseReceiptId();
		    	String sPRDID = oPID.getPurchaseReceiptDetailId();
                if((StringUtil.isNotEmpty(sPOID) && StringUtil.isEqual(sPRID, sPOID)) && 
                	StringUtil.isNotEmpty(sPRDID))//PO == PR, PI import PO directly    			    
    	    	{
                	PurchaseOrderDetail oPOD = getDetailsByPODetailID(oPID.getPurchaseReceiptDetailId(),_oConn);
                    if(oPOD != null) 
                    {
                        //cek PO Qty and Update Received Qty on PO Detail
                        double dPOQty = oPOD.getQty().doubleValue();
                        double dReceived = oPOD.getReceivedQty().doubleValue();
                        double dPIQty = oPID.getQty().doubleValue();
                        
                        log.debug("dPOQty : " + dPOQty);                        
                        log.debug("dReceived : " + dReceived);
                        log.debug("dPRQty : " + dPIQty);
                        
                        //if cancel then minus qty
                        if (_bIsCancel) dPIQty = dPIQty * -1;

                        double dAfterReceipt = dReceived + dPIQty;
                        
                        oPOD.setReceivedQty(new BigDecimal(dAfterReceipt));
                        oPOD.save(_oConn);
                    }// end if po detail size > 0
                }// end if pr is from po
            }//end for
            
            if(StringUtil.isNotEmpty(sPOID))
            {
                //to check balance item on PO and PR
       			//if the item was balance then the PO is close
            	updateStatusFromReceivedItem(sPOID, _oConn);
                if(_bClosePO) 
                {
                	changeStatusByPOID(sPOID, i_PO_CLOSED, _oConn);
                }
            }
        }
        catch(Exception _oEx)
        {
            String sMessage = "Update PO From PI Error : " + _oEx.getMessage();
            log.error(_oEx);
            throw new NestableException(sMessage, _oEx);
        }
    }
    
    //-------------------------------------------------------------------------
    // Transaction Processing
    //-------------------------------------------------------------------------
    /**
     * Set PO object properties with data from Transaction Screen
     * @param _oPO
     * @param _vPOD
     * @param data
     * @throws Exception
     */
    public static void setHeaderProperties (PurchaseOrder _oPO, List _vPOD, RunData data)
        throws Exception
    {
    	ValueParser formData = null;
        try
        {
        	if (data != null)
        	{
        		formData = data.getParameters();
        		formData.setProperties (_oPO);
                _oPO.setTransactionDate(CustomParser.parseDate(formData.getString("TransactionDate")));
                _oPO.setDpDueDate 	   (CustomParser.parseDate(formData.getString("DpDueDate")));
                _oPO.setIsInclusiveTax (formData.getBoolean("IsInclusiveTax", false));
                _oPO.setIsTaxable	   (formData.getBoolean("IsTaxable", false));
                
        	}          
            _oPO.setVendorName      (VendorTool.getVendorNameByID (_oPO.getVendorId()));
            _oPO.setDueDate         (new Date () );
            _oPO.setTotalQty        (new BigDecimal(countQty(_vPOD)));
            _oPO.setTotalDiscount   (countDiscount (_vPOD));
            _oPO.setTotalAmount     (countSubTotal (_vPOD));
            
            //count total amount after total discount percentage
            String sTotalInvDisc = "0";
    		if(StringUtil.isEmpty(_oPO.getTotalDiscountPct()) && formData != null)
    		{
			    sTotalInvDisc = formData.getString("TotalDiscountPct","0").trim();
			}
			else
			{
			    sTotalInvDisc = _oPO.getTotalDiscountPct().trim();
		    }
    		
    		BigDecimal bdTotalAmount = _oPO.getTotalAmount();    		
			if (StringUtil.isNotEmpty(sTotalInvDisc) && !StringUtil.equals(sTotalInvDisc, "0")) 
            { 
				//count total discount from total amount
				BigDecimal bdTotalItemDisc = _oPO.getTotalDiscount();
				BigDecimal bdTotalInvDisc = new BigDecimal(Calculator.calculateDiscount(sTotalInvDisc, bdTotalAmount))
												.setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
			 	_oPO.setTotalDiscount(bdTotalItemDisc.add(bdTotalInvDisc));
			 	bdTotalAmount = _oPO.getTotalAmount().subtract(bdTotalInvDisc); //because total amount already minus item discount
			 	updateCostPerUnit(sTotalInvDisc, _vPOD, _oPO);
            }
            _oPO.setTotalTax (countTax(_oPO, _vPOD));
            
			bdTotalAmount = countTotal(_oPO, _vPOD); //count total again after applying invoice discount
			
			bdTotalAmount = bdTotalAmount.add(_oPO.getTotalTax()); //add tax because subtotal now excl tax			
            
            calculateLastPurchase(_vPOD, _oPO);            
            _oPO.setTotalAmount (bdTotalAmount); 
        }
        catch (Exception _oEx)
        {
            _oEx.printStackTrace();
            log.error (_oEx);
            throw new NestableException ("Set PO Header Properties Failed ", _oEx); 
        }
    }

    /**
     * Since using discount is 2 level then CostPerUnit in each 
     * PurchaseTransactionDetail should be updated
     * 
     * @param _sDisc
     * @param _vDetails
     * @param _oPO
     * @throws Exception
     */
    public static void updateCostPerUnit(String _sDisc,  List _vDetails, PurchaseOrder _oPO)
        throws Exception
    {
        try 
		{
			double dTotalCost = countCost(_vDetails);
			for (int i = 0; i < _vDetails.size(); i++)
			{
			    PurchaseOrderDetail oDet = (PurchaseOrderDetail) _vDetails.get(i);
			    double dCost = oDet.getCostPerUnit().doubleValue();
			    double dDisc = Calculator.calculateDiscount(_sDisc, dCost);
			    
			    if(!_sDisc.contains(Calculator.s_PCT)) //if discount not in percentage
			    {
			    	if (dTotalCost > 0)
			    	{
			    		//complete step was : dCost = dCost - (dDisc * (dSubTotalCost / dTotalCost) / dQty);
			    		dCost = dCost - (dDisc * (dCost / dTotalCost));
			    	}
			    }
			    else
			    {
			    	dCost = dCost - dDisc;
			    }
	        			    
//			    if (_oPO.getIsInclusiveTax())
//			    {	
//			    	dCost = dCost  / ((oDet.getTaxAmount().doubleValue() + 100) / 100);
//			    }
			    oDet.setCostPerUnit (new BigDecimal(dCost).setScale(4,BigDecimal.ROUND_HALF_UP));			    
			}
		} catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new NestableException ("updateCostPerUnit Failed ", _oEx);
		}
    }

    /**
     * count total cost per unit of List of PO Details
	 * @param details
	 * @return total cost in details
	 */
	private static double countCost(List _vDetails) 
	{		
        double dTotalCost = 0;
        for (int i = 0; i < _vDetails.size(); i++)
        {
            PurchaseOrderDetail oDet = (PurchaseOrderDetail) _vDetails.get(i);
            dTotalCost += oDet.getCostPerUnit().doubleValue() * oDet.getQtyBase().doubleValue();
        }
        return dTotalCost;
	}
	
	/**
	 * 
	 * @param _vDetails
	 * @return sum of qty_base in po details
	 * @throws Exception
	 */
    public static double countQty (List _vDetails)
    	throws Exception
    {
        double dQty = 0;
        for (int i = 0; i < _vDetails.size(); i++)
        {
            PurchaseOrderDetail oDet = (PurchaseOrderDetail) _vDetails.get(i);
			if (oDet.getQtyBase() == null) //if from add detail
			{
				oDet.setQtyBase (UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty()));
			}
            dQty += oDet.getQtyBase().doubleValue();
        }
        return dQty;
    }

    private static BigDecimal countTax (PurchaseOrder _oPO, List _vDetails)
    {
        BigDecimal bdAmt = bd_ZERO;
        for (int i = 0; i < _vDetails.size(); i++)
        {
        	PurchaseOrderDetail oDet = (PurchaseOrderDetail) _vDetails.get(i);
            if(StringUtil.isNotEmpty(_oPO.getTotalDiscountPct()) && !StringUtil.equals(_oPO.getTotalDiscountPct(),"0"))
            {
            	BigDecimal bdCost = oDet.getCostPerUnit().divide(_oPO.getCurrencyRate(),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP).multiply(oDet.getQtyBase()); 				
            	BigDecimal bdTax = oDet.getTaxAmount().divide(new BigDecimal(100),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP);
            	bdTax = bdTax.multiply(bdCost).setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
	            oDet.setSubTotalTax	(bdTax);
            }
            bdAmt = bdAmt.add(oDet.getSubTotalTax());
        }
        return bdAmt;
    }
    
    private static void calculateLastPurchase(List _vPOD, PurchaseOrder _oPO)
    {
    	for(int i = 0; i<_vPOD.size(); i++)
        {
            PurchaseOrderDetail oPOD = (PurchaseOrderDetail)_vPOD.get(i);
            double dLastPur = oPOD.getCostPerUnit().doubleValue();   //default use cost per unit            
            double dPOQty = oPOD.getQty().doubleValue();
            double dUnitConv = 1;                                		
    		if (StringUtil.equals(oPOD.getUnitId(),oPOD.getItem().getPurchaseUnitId())) //purchase is in base unit
    		{
    			dUnitConv = oPOD.getItem().getUnitConversion().doubleValue();
    		}
    		
    		if(PreferenceTool.getPiLastPurchMethod() == i_ITEM_PRICE_METHOD) 
    		{        		                 
    			dLastPur = oPOD.getItemPrice().doubleValue() / dUnitConv; 
    			if(!b_PURCHASE_TAX_INCLUSIVE) 
    			{
    				//if exclusive tax, but current PI is Inclusive then calculate the Price Before Tax else no need
    				//because last purchase will be same with item price
    				dLastPur = TaxTool.calculateBeforeTax(oPOD.getTaxId(), _oPO.getIsInclusiveTax(), dLastPur);
    			}    			
    		}
    		else if(PreferenceTool.getPiLastPurchMethod() == i_DISCOUNT_METHOD && dPOQty > 0)
    		{
    			dLastPur = oPOD.getItemPrice().doubleValue() * oPOD.getQty().doubleValue();
    			dLastPur = dLastPur - oPOD.getSubTotalDisc().doubleValue();
    			dLastPur = dLastPur / oPOD.getQty().doubleValue();
    			dLastPur = dLastPur / dUnitConv;   
    		}
    		else if(PreferenceTool.getPiLastPurchMethod() == i_DISCOUNT_ALL_METHOD)
    		{
    			dLastPur = oPOD.getCostPerUnit().doubleValue();	            
    		}
    		else if(PreferenceTool.getPiLastPurchMethod() == i_TAX_INC_METHOD && dPOQty > 0)
    		{
    			dLastPur = (oPOD.getCostPerUnit().doubleValue() * oPOD.getQtyBase().doubleValue());
    			dLastPur = dLastPur + (dLastPur  * oPOD.getTaxAmount().doubleValue() / 100);
    			dLastPur = dLastPur / oPOD.getQtyBase().doubleValue();
    		}   
            oPOD.setLastPurchase (new BigDecimal(dLastPur));
        }
    }

    private static BigDecimal countDiscount (List _vDetails)
    {
    	BigDecimal bdAmt = bd_ZERO;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseOrderDetail oDet = (PurchaseOrderDetail) _vDetails.get(i);			
			bdAmt = bdAmt.add(oDet.getSubTotalDisc());
		}
		return bdAmt;
    }
    
    public static BigDecimal getCostPerUnitByID (String _sID)
    	throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(PurchaseOrderDetailPeer.PURCHASE_ORDER_DETAIL_ID, _sID);
        List vData = PurchaseOrderDetailPeer.doSelect(oCrit);
        if (vData.size() > 0) 
        {
            return ((PurchaseOrderDetail) vData.get(0)).getCostPerUnit();
        }
        return bd_ZERO;
    }

    private static BigDecimal countSubTotal (List _vDetails)
    {
    	BigDecimal bdAmt = bd_ZERO;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseOrderDetail oDet = (PurchaseOrderDetail) _vDetails.get(i);									
			bdAmt = bdAmt.add(oDet.getSubTotal());
		}
		return bdAmt;
    }
    
    /**
	 * count total for the last time so total amount will be the same with GL amount
	 * @param _oPI
	 * @param _vDetails
	 * @return
	 */
	public static BigDecimal countTotal (PurchaseOrder _oPI, List _vDetails)
	{
		BigDecimal bdAmt = bd_ZERO;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseOrderDetail oDet = (PurchaseOrderDetail) _vDetails.get(i);	
			bdAmt = bdAmt.add(oDet.getCostPerUnit().multiply(oDet.getQtyBase()).divide(_oPI.getCurrencyRate(),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP));
		}
		return bdAmt;
	} 

	
    /**
     * set purchase order detail properties
     *  
     * @param _vPOD List of PO Detail
     * @param data
     * @throws Exception
     */
    public static void updateDetail (List _vPOD, RunData data)
        throws Exception
    {
    	ValueParser formData = data.getParameters();
        String sItemCode = "";
        try
        {
            //double dCurrencyRate = formData.getDouble("CurrencyRate", 1);
            BigDecimal bdRate = formData.getBigDecimal("CurrencyRate",bd_ONE);
            boolean bIsIncTax = formData.getBoolean("IsInclusiveTax",false);
            
            for (int i = 0; i < _vPOD.size(); i++)
            {
                PurchaseOrderDetail oPOD = (PurchaseOrderDetail) _vPOD.get(i);
                int iIdx = i + 1;

                sItemCode = oPOD.getItemCode();
                
                //prevent screen refresh after add item
                if (formData.getString("UnitId" + iIdx) != null)
                {
	                //log.debug (" PO Detail " + iIdx + " Before update : "  + oPOD);
	                
	                //get unit id in detail form. if the unit updated then the rest must also be updated
	                oPOD.setUnitId    (formData.getString("UnitId" + iIdx));
	                oPOD.setUnitCode  (formData.getString("UnitCode" + iIdx));
	                oPOD.setQty 	  (formData.getBigDecimal("Qty" + iIdx));
	                
	                double dBaseValue = UnitTool.getBaseValue(oPOD.getItemId(), oPOD.getUnitId());
	                double dQtyBase = oPOD.getQty().doubleValue() * dBaseValue; 
                    
	       			oPOD.setQtyBase 	 (new BigDecimal(dQtyBase));
	                oPOD.setItemPrice    (formData.getBigDecimal("ItemPrice"    + iIdx));
	                oPOD.setTaxId        (formData.getString    ("TaxId"   	    + iIdx));	                                
	                oPOD.setDiscount  	 (formData.getString	("Discount"     + iIdx));
	                oPOD.setChangedManual(formData.getBoolean	("ChangedManual"+ iIdx));
	                oPOD.setTaxAmount    (oPOD.getTax().getAmount());
	                
	                BigDecimal bdSubTax = bd_ZERO;
					BigDecimal bdSubTotal = oPOD.getItemPrice().multiply(oPOD.getQty());
					BigDecimal bdSubDisc = new BigDecimal(Calculator.calculateDiscount(oPOD.getDiscount(), bdSubTotal));
					bdSubTotal = bdSubTotal.subtract(bdSubDisc);
					if(oPOD.getTax().getAmount().doubleValue() > 0)
					{			
						if(bIsIncTax)
						{							
							bdSubTax = bdSubTotal.subtract(new BigDecimal(bdSubTotal.doubleValue() / ((100 + oPOD.getTaxAmount().doubleValue()) / 100)));
							bdSubTotal = bdSubTotal.subtract(bdSubTax);
						}
						else
						{
							bdSubTax = new BigDecimal(bdSubTotal.doubleValue() * (oPOD.getTaxAmount().doubleValue() / 100));
						}												
						bdSubTax = bdSubTax.setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
						bdSubTotal = bdSubTotal.setScale(i_PURCHASE_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
					}
					
					oPOD.setSubTotalTax(bdSubTax);
					oPOD.setSubTotalDisc(bdSubDisc);
					oPOD.setSubTotal(bdSubTotal);
					if(oPOD.getQtyBase().doubleValue() > 0)
					{
						BigDecimal bdCostPerUnit = 
							bdSubTotal.divide(oPOD.getQtyBase(),i_PURCHASE_COMMA_SCALE,BigDecimal.ROUND_HALF_UP).multiply(bdRate);
						oPOD.setCostPerUnit(bdCostPerUnit);
					}
					System.out.println ("PO Detail " + iIdx + " After update : "  + oPOD);
					//log.debug ("PO Detail " + iIdx + " After update : "  + oPOD);
                }
                
            }
        }
        catch (Exception _oEx)
        {
            _oEx.printStackTrace();
            log.error(_oEx);
            throw new NestableException ("Update Detail Item " + sItemCode + " Failed, Message:" + _oEx.getMessage(), _oEx); 
        }
    }

    /**
     * this method is used to set due date on the Purchase Order
     *
     * @param _oPO - Purchase Order Object 
     */

    private static void setDueDate (PurchaseOrder _oPO, Connection _oConn) 
        throws Exception 
    {
        boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oPO.getPaymentTypeId(), _oConn);
        if (bIsCredit) 
        {
            //check default payment term for this customer
            String sDefaultTerm = VendorTool.getDefaultTermByID(_oPO.getVendorId());
            int iDueDays = 0;
            if (StringUtil.isNotEmpty(sDefaultTerm) && PaymentTermTool.isCashPaymentTerm(_oPO.getPaymentTermId())) 
            {
                iDueDays = PaymentTermTool.getNetPaymentDaysByID (sDefaultTerm);
                _oPO.setPaymentTermId(sDefaultTerm);
            }
            else 
            {
                iDueDays = PaymentTermTool.getNetPaymentDaysByID (_oPO.getPaymentTermId());
            }
            Calendar oCal = new GregorianCalendar ();
            oCal.setTime (_oPO.getTransactionDate());
            oCal.add (Calendar.DATE, iDueDays);
            _oPO.setDueDate (oCal.getTime());
        }
    }
    
    /**
     * Save Purchase Order Data
     * 
     * @param _oPO Purchase Order Object
     * @param _vPOD List of PO Detail
     * @param _oConn Connection object, if null we will create new transaction
     * @throws Exception
     */
    public static void saveData (PurchaseOrder _oPO, List _vPOD, Connection _oConn)
        throws Exception
    {
        boolean bStartTrans = false;
        if (_oConn == null) 
        {
            _oConn = beginTrans();
            bStartTrans = true;
        }
        try
        {
			//validate date
			validateDate(_oPO.getTransactionDate(), _oConn);

            processSavePOData (_oPO, _vPOD, _oConn);
            if (bStartTrans) 
            {
                commit (_oConn);
            }
        }
        catch (Exception _oEx)
        {
            //restore object state to unsaved
        	_oPO.setPurchaseOrderId("");
            _oPO.setPurchaseOrderNo("");
            _oPO.setTransactionStatus(i_PO_NEW);

        	if (bStartTrans) 
            {
                rollback (_oConn);
            }            
            log.error(_oEx);
            throw new NestableException (_oEx.getMessage(), _oEx);
        }
    }

    /**
     * Process save data
     * 1. generate id and trans no
     * 2. delete old details 
     * 3. save new details
     * 
     * @param _oPO
     * @param _vPOD
     * @param _oConn
     * @throws Exception
     */
    private static void processSavePOData (PurchaseOrder _oPO, List _vPOD, Connection _oConn) 
        throws Exception
    {        
        if(StringUtil.isEmpty(_oPO.getPurchaseOrderId()))
        {
        	_oPO.setPurchaseOrderId( IDGenerator.generateSysID() );
        	_oPO.setNew(true);
            validateID (getHeaderByID(_oPO.getPurchaseOrderId(),_oConn), "purchaseOrderNo");
        }        

        //set payment due date 
        setDueDate (_oPO , _oConn);
        
    	if (_oPO.getPurchaseOrderNo() == null) _oPO.setPurchaseOrderNo("");
        if ( _oPO.getTransactionStatus () == i_PO_ORDERED && StringUtil.isEmpty(_oPO.getPurchaseOrderNo())) 
        {
        	String sLocCode = LocationTool.getLocationCodeByID(_oPO.getLocationId(), _oConn);
        	
            //generate Purchase Number no first
            _oPO.setPurchaseOrderNo(
            	LastNumberTool.generateByVendor(_oPO.getVendorId(),_oPO.getLocationId(),sLocCode,s_PO_FORMAT,LastNumberTool.i_PURCHASE_ORDER, _oConn)	
            );
            validateNo(PurchaseOrderPeer.PURCHASE_ORDER_NO,_oPO.getPurchaseOrderNo(),
            		   PurchaseOrderPeer.class, _oConn);
                        
            //set confirm date
            _oPO.setConfirmDate(new Date());
            
            //check if down payment > 0 then create debit memo to note that the vendor has payable amount to us
            if (_oPO.getDownPayment().doubleValue() > 0)
            {
                DebitMemoTool.createFromPODownPayment (_oPO, _oConn);
            }
        }
    
        //delete all existing detail from new purchase
        if (StringUtil.isNotEmpty(_oPO.getPurchaseOrderId()))
        {
            deleteDetailsByID (_oPO.getPurchaseOrderId(),_oConn);
        }           
        _oPO.save (_oConn);
        
        renumber(_vPOD);
        for ( int i = 0; i < _vPOD.size(); i++ )
        {
            PurchaseOrderDetail oTD = (PurchaseOrderDetail) _vPOD.get (i);
            oTD.setModified (true);
            oTD.setNew (true);
            
            if (oTD.getRequestId() == null) oTD.setRequestId("");
            if (oTD.getRequestDetailId() == null) oTD.setRequestDetailId("");            
            
            //Generate Sys Data
            if (StringUtil.isEmpty(oTD.getPurchaseOrderDetailId())) 
            {   
            	oTD.setPurchaseOrderDetailId (IDGenerator.generateSysID());
            	oTD.setPurchaseOrderId (_oPO.getPurchaseOrderId());
            }
            oTD.save (_oConn);
            
            //update item vendor 
            if ( _oPO.getTransactionStatus () == i_PO_ORDERED) 
            {
            	ItemVendorTool.save(oTD.getItemId(), _oPO.getVendorId(), false);
            }
        }   
        if ( _oPO.getTransactionStatus () == i_PO_ORDERED) 
        {
        	PurchaseRequestTool.updateFromPOD(_oPO, _vPOD, _oConn);
        }
    }
    
	/**
	 * update SO Status to cancelled
	 * @param _SoID
	 * @param m_oConn
	 * @throws Exception
	 */
	public static void cancelPO (PurchaseOrder _oPO, String _sCancelBy)
		throws Exception
	{
		Connection oConn = null;
	    try
	    {
	        if (_oPO != null)
	        {	        
	        	oConn = beginTrans();
		    	validateDate (_oPO.getTransactionDate(), oConn);
		        _oPO.setRemark(cancelledBy(_oPO.getRemark(), _sCancelBy));
	    		_oPO.setTransactionStatus(i_PO_CANCELLED);    		
	    		_oPO.save(oConn);
		        commit(oConn);		        		        
	        }
	    }
	    catch (Exception _oEx)
	    {
	    	_oEx.printStackTrace();
	    	if(oConn != null) rollback(oConn);
	        throw new NestableException (_oEx.getMessage(), _oEx);
	    }
	}	    
	
	/**
	 * Close By PO
	 * @param _oPO
	 * @param _sCloseBy
	 * @throws Exception
	 */
	public static void closePO (PurchaseOrder _oPO, String _sCloseBy)
		throws Exception
	{
		Connection oConn = null;
	    try
	    {
	        if (_oPO != null)
	        {
	        	oConn = beginTrans();
		    	
                //no need to validte close PO DATE
                //validateDate (_oPO.getTransactionDate(), oConn);
		    	
				StringBuilder oSB = new StringBuilder();
				if (StringUtil.isNotEmpty(_oPO.getRemark()))
				{
					oSB.append(_oPO.getRemark()).append("\n");
				}
				String sCloseDate = CustomFormatter.formatDateTime(new Date());
				oSB.append("Closed By ").append(" ").append(_sCloseBy).append("\n");
				oSB.append("Closed Date ").append(" : ").append(sCloseDate);
		    					
		        _oPO.setRemark(oSB.toString());
	    		_oPO.setTransactionStatus(i_PO_CLOSED);    		
	    		_oPO.setClosedDate(new Date());
	    		_oPO.save(oConn);
		        commit(oConn);
	        }
	    }
	    catch (Exception _oEx)
	    {
	    	rollback(oConn);
	        throw new NestableException (_oEx.getMessage(), _oEx);
	    }
	}	
		
	/**
	 * Unconfirm PO
	 * @param _oPO
	 * @param _sCloseBy
	 * @throws Exception
	 */
	public static void unconfirmPO (PurchaseOrder _oPO, String _sUnconfirmBy)
		throws Exception
	{
		Connection oConn = null;
	    try
	    {
	        if (_oPO != null)
	        {
	        	oConn = beginTrans();
		    	//validateDate (_oPO.getTransactionDate(), oConn);		  
		    	String sUnconfirmDate = CustomFormatter.formatDateTime(new Date());
		    	_oPO.setRemark(_oPO.getRemark() + "\nUnconfirm by : " + _sUnconfirmBy + " Date: " + sUnconfirmDate);
	    		_oPO.setTransactionStatus(i_PO_NEW);    		
	    		_oPO.save(oConn);
		        commit(oConn);
	        }
	    }
	    catch (Exception _oEx)
	    {
	    	rollback(oConn);
	        throw new NestableException (_oEx.getMessage(), _oEx);
	    }
	}	

	/**
	 * update print times indicate how many times this invoice printed
	 * 
	 * @param _oPO
	 * @throws Exception
	 */
    public static void updatePrintTimes (PurchaseOrder _oPO) 
		throws Exception
	{
    	if (_oPO != null)
    	{
			_oPO.setPrintTimes (_oPO.getPrintTimes() + 1);
			_oPO.save();
    	}
	}
	
    //-------------------------------------------------------------------------
    //finder
    //-------------------------------------------------------------------------

    /**
     * find PO
     * @param _iCond
     * @param _sKeywords
     * @param _dStart
     * @param _dEnd
     * @param _sVendorID
     * @param _iStatus
     * @param _iLimit
     * @param _sUserName
     * @return query result in LargeSelect 
     * @throws Exception
     */
    public static LargeSelect findData (int _iCond, 
    									String _sKeywords, 
										Date _dStart, 
										Date _dEnd, 
                                        String _sVendorID, 
										String _sLocationID,
                                        int _iStatus, 
										int _iLimit, 
										String _sUserName,
										String _sCurrencyID) 
        throws Exception
    {
        Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
        	PurchaseOrderPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID
		);
        
        if (StringUtil.isNotEmpty(_sVendorID)) 
        {
            oCrit.add(PurchaseOrderPeer.VENDOR_ID, _sVendorID); 
        }
        if (StringUtil.isNotEmpty(_sLocationID)) 
        {
            oCrit.add(PurchaseOrderPeer.LOCATION_ID, _sLocationID); 
        }        
        if (StringUtil.isNotEmpty(_sUserName)) 
        {
            oCrit.add(PurchaseOrderPeer.CREATE_BY, _sUserName); 
        }
        if (_iStatus > 0) 
        {
			if(_iStatus == 99)
			{
				oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, i_PO_ORDERED);
				oCrit.or(PurchaseOrderPeer.TRANSACTION_STATUS, i_PO_RECEIVED);				
			}
			else
			{
				oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, _iStatus);
			}            
        }       
        oCrit.addAscendingOrderByColumn(PurchaseOrderPeer.TRANSACTION_DATE);
        log.debug(oCrit);
        return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.PurchaseOrderPeer");
    }

    /**
     * Method to get List of Purchase Order based on item filter
     * join table PurchaseOrder,PurchaseOrderDetail,and Item
     * @param _dStartDate start transaction date,int 
     * @param _dEndDate end transaction date
     * @param _iStatus purchase order status
     * @param _sVendorID vendor ID
     * @param _sKatID item kategori ID
     * @param _sKeywords item keyword that search
     * @param _iCondition filter condition
     * @return List of Purchase Order
     */
    public static List findData (int _iCond,
    							 String _sKeywords,
    							 Date _dStart,
    							 Date _dEnd,
    							 String _sVendorID,
								 int _iStatus)
        throws Exception
    {
    	return findData(_iCond, _sKeywords, _dStart, _dEnd, _sVendorID, "", _iStatus);
    }

    /**
     * Method to get List of Purchase Order based on item filter
     * join table PurchaseOrder,PurchaseOrderDetail,and Item
     * @param _dStartDate start transaction date,int 
     * @param _dEndDate end transaction date
     * @param _iStatus purchase order status
     * @param _sVendorID vendor ID
     * @param _sLocationID location ID
     * @param _sKatID item kategori ID
     * @param _sKeywords item keyword that search
     * @param _iCondition filter condition
     * @return List of Purchase Order
     */
    public static List findData (int _iCond,
    							 String _sKeywords,
    							 Date _dStart,
    							 Date _dEnd,
    							 String _sVendorID,
    							 String _sLocationID,
								 int _iStatus)
        throws Exception
    {
        Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
        	PurchaseOrderPeer.TRANSACTION_DATE, _dStart, _dEnd, ""
    	);        
        if (StringUtil.isNotEmpty(_sVendorID)) 
        {
            oCrit.add(PurchaseOrderPeer.VENDOR_ID, _sVendorID); 
        }
        if (StringUtil.isNotEmpty(_sLocationID)) 
        {
            oCrit.add(PurchaseOrderPeer.LOCATION_ID, _sLocationID); 
        }
        if (_iStatus > 0) 
        {
            oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, _iStatus);  
        }
        else if(_iStatus == 0 || _iStatus < -1)
        {
            oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, 1); 
            oCrit.or(PurchaseOrderPeer.TRANSACTION_STATUS, 3);  
        }
        log.debug(oCrit);
        return PurchaseOrderPeer.doSelect(oCrit);
    }
    
    public static List findExpired (int _iCond,
						    	 	String _sKeywords,
						    	 	Date _dStart,
						    	 	Date _dEnd,
						    	 	Date _dExpStart,
						    	 	Date _dExpEnd,
						    	 	String _sVendorID,
						    	 	int _iStatus)
    throws Exception
    {
    	Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
    			PurchaseOrderPeer.TRANSACTION_DATE, _dStart, _dEnd, ""
    	);
    	
		if (_dExpStart != null) 
		{
			oCrit.add(PurchaseOrderPeer.EXPIRED_DATE, DateUtil.getStartOfDayDate(_dExpStart), Criteria.GREATER_EQUAL);
		}
		if (_dExpEnd != null) 
		{
			oCrit.and(PurchaseOrderPeer.EXPIRED_DATE, DateUtil.getEndOfDayDate(_dExpEnd), Criteria.LESS_EQUAL);
		}    	
    	if (StringUtil.isNotEmpty(_sVendorID)) 
    	{
    		oCrit.add(PurchaseOrderPeer.VENDOR_ID, _sVendorID); 
    	}
    	if (_iStatus > 0) 
    	{
    		oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, _iStatus);  
    	}
    	else if(_iStatus == 0)
    	{
    		oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, 1); 
    		oCrit.or(PurchaseOrderPeer.TRANSACTION_STATUS, 3);  
    	}
    	log.debug(oCrit);
    	return PurchaseOrderPeer.doSelect(oCrit);
    }
    
    //-------------------------------------------------------------------------
    // update PO from PR 
    //-------------------------------------------------------------------------
    
    /**
     * update PO status to closed if all details is already received
     * 
     * @param _sPOID
     * @param _oConn
     */
    private static void updateStatusFromReceivedItem (String _sPOID, Connection _oConn)
    	throws Exception
    {        
    	boolean bAllReceived = true;
    	List vPOD = getDetailsByID(_sPOID, _oConn);
        for(int i = 0; i < vPOD.size(); i++)
        {
            PurchaseOrderDetail oPODet = (PurchaseOrderDetail)vPOD.get(i);
            double dReceived = oPODet.getReceivedQty().doubleValue();
            double dOrdered = oPODet.getQty().doubleValue();
            if ( dReceived >= dOrdered )
            {
                oPODet.setClosed(true);
                oPODet.save(_oConn);
            }
            else
            {
            	bAllReceived = false;
            }
        }
        PurchaseOrder oPO = getHeaderByID(_sPOID, _oConn);
        if (bAllReceived)
        {
        	oPO.setTransactionStatus(i_PO_RECEIVED);
        }
        else
        {
        	oPO.setTransactionStatus(i_PO_ORDERED);
        }
    	oPO.save(_oConn);
    }
    
    private static void changeStatusByPOID(String _sPOID, int _iStatus , Connection _oConn)
      	throws Exception
    {
        PurchaseOrder oPurchaseOrder = getHeaderByID(_sPOID, _oConn);
        oPurchaseOrder.setTransactionStatus(_iStatus);
        oPurchaseOrder.save(_oConn);    
    }
    
    public static double getOnOrderQty (String _sItemID)
       throws Exception
    {
    	return getOnOrderQty(_sItemID,"");
    }
    
    public static double getOnOrderQty (String _sItemID, String _sLocID)
       throws Exception
    {
        StringBuilder oSQL = new StringBuilder();
        oSQL.append(" SELECT SUM(pod.qty - pod.received_qty), pod.unit_id "); 
        oSQL.append(" FROM purchase_order po, purchase_order_detail pod");
        oSQL.append(" WHERE pod.purchase_order_id = po.purchase_order_id ");        
        oSQL.append(" AND po.transaction_status  = 1");
        oSQL.append(" AND pod.item_id  = '").append(_sItemID).append("'");
        if(StringUtil.isNotEmpty(_sLocID))
        {
        	oSQL.append(" AND po.location_id  = '").append(_sLocID).append("'");
        }
        oSQL.append(" GROUP BY (pod.unit_id) ");
        
        log.debug (oSQL);
        
        List vData = PurchaseOrderDetailPeer.executeQuery (oSQL.toString());
        double dTotalOnOrder = 0;
        for (int i = 0; i < vData.size(); i++)
        {
            Record oData = (Record) vData.get(i);
            double dOnOrder = oData.getValue(1).asDouble();              
            double dBase = UnitTool.getBaseValue(_sItemID, oData.getValue(2).asString());            
            dTotalOnOrder += (dOnOrder * dBase);
        }
        return dTotalOnOrder;       
    }
    
    //-------------------------------------------------------------------------
    //report methods
    //-------------------------------------------------------------------------

    public static List getTransactionInDateRange (Date _dStartDate, Date _dEndDate, int _iGroupBy, String _sKeywords, int _iOrderBy, int _iStatus)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(PurchaseOrderPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStartDate), Criteria.GREATER_EQUAL);
        oCrit.and(PurchaseOrderPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEndDate), Criteria.LESS_EQUAL);
        if(_iGroupBy == 2)
        {
            oCrit.add(PurchaseOrderPeer.VENDOR_NAME, 
                (Object) SqlUtil.like (PurchaseOrderPeer.VENDOR_NAME, _sKeywords,false,true), Criteria.CUSTOM);
        }
        if(_iGroupBy == 4)
        {
            oCrit.add(LocationPeer.LOCATION_NAME, 
                (Object) SqlUtil.like (LocationPeer.LOCATION_NAME, _sKeywords,false,true), Criteria.CUSTOM);
            oCrit.addJoin(PurchaseOrderPeer.LOCATION_ID,LocationPeer.LOCATION_ID);
        }
        if(_iGroupBy == 6)
        {
            oCrit.add(PurchaseOrderPeer.CREATE_BY, 
                (Object) SqlUtil.like (PurchaseOrderPeer.CREATE_BY, _sKeywords,false,true), Criteria.CUSTOM);
        }

        if(_iOrderBy == 1)
        {
            oCrit.addAscendingOrderByColumn(PurchaseOrderPeer.PURCHASE_ORDER_NO);
        }
        if(_iOrderBy == 2)
        {
            oCrit.addAscendingOrderByColumn(PurchaseOrderPeer.VENDOR_NAME);
        }
        if(_iOrderBy == 3)
        {
            oCrit.addAscendingOrderByColumn(PurchaseOrderPeer.CREATE_BY);
        }
        if(_iStatus != -1)
        {
            oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS,_iStatus);
        }

        return PurchaseOrderPeer.doSelect(oCrit);
    }
    
    //-------------------------------------------------------------------------
    //report method to group data by item
    //-------------------------------------------------------------------------
    
    public static PurchaseOrder filterTransByPurchaseOrderID (List _vPO, String _sID)
        throws Exception
    {
        PurchaseOrder oPO = null;
        for (int i = 0; i < _vPO.size(); i++)
        {
            oPO = (PurchaseOrder) _vPO.get(i);
            if (oPO.getPurchaseOrderId().equals(_sID))
            {
                return oPO;
            }
        }
        return oPO;
    }

    public static List getTransDetails (List _vPO)
        throws Exception
    {
        return getTransDetails (_vPO,"");
    }

    public static List getTransDetails (List _vPO, String _sItemName)
        throws Exception
    {
        List vPOID = new ArrayList(_vPO.size());
        for (int i = 0; i < _vPO.size(); i++)
        {
            PurchaseOrder oPO = (PurchaseOrder) _vPO.get(i);
            vPOID.add (oPO.getPurchaseOrderId());
        }
        Criteria oCrit = new Criteria();
        if(StringUtil.isNotEmpty(_sItemName))
        {
            oCrit.add(PurchaseOrderDetailPeer.ITEM_NAME,
                (Object) SqlUtil.like (PurchaseOrderDetailPeer.ITEM_NAME, _sItemName,false,true), Criteria.CUSTOM);
        }
        if(vPOID.size()>0)
        {
            oCrit.addIn (PurchaseOrderDetailPeer.PURCHASE_ORDER_ID, vPOID);
        }
        return PurchaseOrderDetailPeer.doSelect(oCrit);
    }

	/**
	 * get DP available for this invoice
	 * @param _sID
	 * @return DP amount
	 */
	public static BigDecimal getDPByInvoiceID (String _sID)
	{
		Criteria oCrit = new Criteria();
		oCrit.addJoin(PurchaseOrderPeer.PURCHASE_ORDER_ID, PurchaseInvoiceDetailPeer.PURCHASE_ORDER_ID);
		oCrit.add(PurchaseInvoiceDetailPeer.PURCHASE_INVOICE_ID, _sID);
		oCrit.setDistinct();
		try 
		{
			List vSO = PurchaseOrderPeer.doSelect(oCrit);
			String sID = "";
			double dDP = 0;
			for (int i = 0; i < vSO.size(); i++)
			{
				PurchaseOrder oPO = (PurchaseOrder) vSO.get(i);
				if ((!sID.equals("") && !oPO.getPurchaseOrderId().equals(sID)) || sID.equals(""))
				{
					dDP = dDP + oPO.getDownPayment().doubleValue();
					sID = oPO.getPurchaseOrderId();					
				}
			}
			return new BigDecimal(dDP);
		} 
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}
		return bd_ZERO;
	}	
	
    
    //-------------------------------------------------------------------------
    //end report methods
    //-------------------------------------------------------------------------

	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
		
	/**
	 * get all PO confirmed since last ho 2 store
	 * Store will not sent PO back so PO goes one way from ho 2 store	 
	 * 
	 * @return List of PurchaseOrder
	 * @throws Exception
	 */
	public static List getForStore ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		Criteria oCrit = new Criteria();
		
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(PurchaseOrderPeer.CONFIRM_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
        	oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, i_PO_ORDERED);
        	//only get ordered PO, PO closed by purchase receipt or manually in store 
		}
		log.debug("get PO FOR STORE : " + oCrit);
		return PurchaseOrderPeer.doSelect(oCrit);
	}
    
	/**
	 * get all PO confirmed since last ho 2 store
	 * Store will not sent PO back so PO goes one way from ho 2 store	 
	 * 
	 * @return List of PurchaseOrder
	 * @throws Exception
	 */
	public static List getStoreClosedPO ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		Criteria oCrit = new Criteria();
		
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(PurchaseOrderPeer.CLOSED_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
        	oCrit.add(PurchaseOrderPeer.TRANSACTION_STATUS, i_PO_CLOSED);
		}
		log.debug("get CLOSED PO FOR STORE : " + oCrit);
		return PurchaseOrderPeer.doSelect(oCrit);
	}
	
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PurchaseOrderPeer.class, PurchaseOrderPeer.PURCHASE_ORDER_ID, _sID, _bIsNext);
	}	   
    //end next prev
	
	//Automatic PO from SO / RQ	
	/**
	 * 
	 * @param sVendorID vendor ID
	 * @param vItem Record of Item in List 
	 * @param _sUserName
	 * @return
	 * @throws Exception
	 */
	public static String createPO(String sVendorID, String _sLocID, List vItem, String _sUserName, String _sFromTrans)
		throws Exception
	{
		Vendor oVendor = VendorTool.getVendorByID(sVendorID);
		StringBuilder sResult= new StringBuilder();
		sResult.append("PO to ").append(oVendor.getVendorCode()).append(" - ")
			   .append(oVendor.getVendorName()).append(s_LINE_SEPARATOR);
	
		if(StringUtil.isEmpty(_sLocID)) _sLocID = PreferenceTool.getLocationID();
		
		PurchaseOrder oPO = new PurchaseOrder(); 
		oPO.setLocationId(_sLocID); 
		oPO.setVendorId(oVendor.getVendorId());
		oPO.setVendorName(oVendor.getVendorName());
		oPO.setCurrencyId(oVendor.getDefaultCurrencyId());
		oPO.setCurrencyRate(bd_ONE);
		oPO.setTransactionDate(new Date());
		oPO.setCreateBy(_sUserName);
		oPO.setPaymentTypeId(oVendor.getDefaultTypeId());
		oPO.setPaymentTermId(oVendor.getDefaultTermId());
		oPO.setRemark("Auto PO From  " + _sFromTrans);
		oPO.setTotalDiscountPct("0");
		oPO.setIsTaxable(true);
		oPO.setIsInclusiveTax(PreferenceTool.getPurchInclusiveTax());
		oPO.setTransactionStatus(i_PO_NEW);
		oPO.setCourierId("");
		oPO.setFobId("");
		oPO.setTotalFee(bd_ZERO);
		oPO.setConfirmBy("");
		
		//updated by set header
		oPO.setTotalAmount(bd_ZERO); 
		oPO.setDueDate(oPO.getTransactionDate());				
	
		List vPOD = new ArrayList(vItem.size());
		for (int i = 0; i < vItem.size(); i++)
		{
			String sRQID = "";
			String sRQDID = "";
			String sItemID = "";
			String sUnitID = "";
			BigDecimal bdPOQty = bd_ZERO;
			BigDecimal bdPrice = bd_ZERO;
			
			Object o = vItem.get(i);
			if (o instanceof Record)
			{
				Record oRec = (Record) o;
				sItemID = oRec.getValue("item_id").asString();
				sUnitID = oRec.getValue("unit_id").asString();
				bdPOQty = oRec.getValue("left").asBigDecimal();												
				if(StringUtil.containsIgnoreCase(_sFromTrans, "RQ"))
				{
					sRQID  = oRec.getValue("purchase_request_id").asString();
					sRQDID = oRec.getValue("purchase_request_detail_id").asString();
				}
			}
			else if(o instanceof PurchaseRequestDetail)
			{
				PurchaseRequestDetail oRQD = (PurchaseRequestDetail) o;
				sItemID = oRQD.getItemId();
				sUnitID = oRQD.getUnitId();				
				sRQID = oRQD.getPurchaseRequestId();
				sRQDID = oRQD.getPurchaseRequestDetailId();
				bdPOQty = oRQD.getRequestQty();
				bdPrice = oRQD.getRqPrice();
			}
			
			Item oItem = ItemTool.getItemByID(sItemID);
			if(bdPrice == bd_ZERO)
			{
				bdPrice = oItem.getLastPurchasePrice();
			}
			
			String sPurchUnit = oItem.getPurchaseUnitId();
			double dUnitConv = oItem.getUnitConversion().doubleValue();
			if (!StringUtil.isEqual(sUnitID,sPurchUnit)) //if not in purchase unit
			{								
				if (dUnitConv != 1) //convert to purchase_unit
				{
					double dPOQty = bdPOQty.doubleValue() / dUnitConv;
					bdPOQty = Calculator.roundUp(dPOQty,BigDecimal.ROUND_UP);					
				}				
				sUnitID = sPurchUnit;
				bdPrice = new BigDecimal(Calculator.mul(dUnitConv,bdPrice));
			}				
			
			sResult.append(" - Item ").append(oItem.getItemCode()).append(" - ")
			   	   .append(oItem.getItemName()).append(" Qty: ").append(bdPOQty)
			   	   .append(s_LINE_SEPARATOR);
			
			PurchaseOrderDetail oPOD = new PurchaseOrderDetail();
			oPOD.setItemId(oItem.getItemId());
			oPOD.setItemCode(oItem.getItemCode());
			oPOD.setItemName(oItem.getItemName());
			oPOD.setQty(bdPOQty);
			oPOD.setUnitId(sUnitID);
			oPOD.setUnitCode(UnitTool.getCodeByID(sUnitID));
			oPOD.setQtyBase(UnitTool.getBaseQty(oItem.getItemId(), sUnitID, bdPOQty));
			oPOD.setTaxId(oVendor.getDefaultTaxId());
			oPOD.setTaxAmount(TaxTool.getAmountByID(oPOD.getTaxId()));
			
	        //check price & discount in vendor price list
		    VendorPriceListDetail oVPL = 
		    	VendorPriceListTool.getPriceListDetailByVendorID(sVendorID, oItem.getItemId(), oPO.getTransactionDate());

		    //PRICE & DISCOUNT		    
		    if(oVPL != null) //price list exists
		    {
				oPOD.setItemPrice(oVPL.getPurchasePrice());
				oPOD.setDiscount(oVPL.getDiscountAmount()); 
		    }
		    else
		    {
				oPOD.setItemPrice(bdPrice);
				if(StringUtil.isNotEmpty(oItem.getDiscountAmount()))
                {
					oPOD.setDiscount(oItem.getDiscountAmount()); 
                }
				else //check item discount in vendor
				{		    						
					if(oPO.getVendor() != null && StringUtil.isNotEmpty(oPO.getVendor().getDefaultItemDisc()))
			    	{
			    		oPOD.setDiscount(oPO.getVendor().getDefaultItemDisc());
			    	}
				}
		    }
			oPOD.setProjectId("");
			oPOD.setDepartmentId("");
			oPOD.setRequestId(sRQID);
			oPOD.setRequestDetailId(sRQDID);
			
			if(StringUtil.isNotEmpty(sRQID) && StringUtil.isNotEmpty(sRQDID))
			{
				PurchaseRequestDetail oRQD = PurchaseRequestTool.getDetailByDetailID(sRQDID, null);
				if(oRQD != null & StringUtil.isNotEmpty(oRQD.getDescription())) oPOD.setDescription(oRQD.getDescription());
			}

			double dQty = oPOD.getQty().doubleValue();
	        double dPrice = oPOD.getItemPrice().doubleValue();
			double dSubTotal = dPrice * dQty;
			double dSubTotalDisc = Calculator.calculateDiscount(oPOD.getDiscount(), dSubTotal);
			double dSubTotalTax = oPOD.getTaxAmount().doubleValue() / 100 * (dSubTotal  - dSubTotalDisc);
			oPOD.setSubTotal(new BigDecimal(dSubTotal));
			oPOD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
			oPOD.setSubTotalTax(new BigDecimal(dSubTotalTax));
			
			double dDiscount = 0;
	        
	        //if discount was not % then divide discount with purchase qty 
	        if(oPOD.getDiscount().contains(Calculator.s_PCT))
	        {
	            dDiscount = Calculator.calculateDiscount(oPOD.getDiscount(),dPrice);
	        }
	        else
	        {
	        	if(dQty > 0)
	        	{
	        		dDiscount = Calculator.calculateDiscount(oPOD.getDiscount(),dPrice) / dQty;
	        	}
	        }
	        
	        double dCostPerUnit = oPOD.getItemPrice().doubleValue() - dDiscount;	
	        dCostPerUnit = dCostPerUnit / dUnitConv;
	        
	        double dRealCost = dCostPerUnit;
	        System.out.println(dCostPerUnit + " "  + dRealCost);
	        oPOD.setCostPerUnit(new BigDecimal(dRealCost));	    	
	        vPOD.add(oPOD);
		}    	
		setHeaderProperties(oPO, vPOD, null);
		saveData(oPO, vPOD, null);
		return sResult.toString();
	}  
	
	//-------------------------------------------------------------------------
	//PO Approval methods
	//------------------------------------------------------------------------- 
	
	public static List getApprovals(String _sPOID, Connection _oConn)
		throws Exception
	{
		return new ArrayList(1);
	}	

	//-------------------------------------------------------------------------
	//Email methods
	//------------------------------------------------------------------------- 
	public static String sendPOEmail(PurchaseOrder _oPO)
		throws Exception
	{
		String sMessage = "No Email Sent";
		//Send Email
		Vendor oVendor = _oPO.getVendor();
		if(oVendor != null && StringUtil.isNotEmpty(oVendor.getEmail()))
		{
			String sEmail = oVendor.getEmail();
			try 
			{
				String aTo[] = {sEmail};
				String sSub = "New Purchase Order: " + _oPO.getPurchaseOrderNo();				
				StringWriter oSW = new StringWriter();
				VelocityContext oCtx = new VelocityContext();
				Context oGlobal = TurbinePull.getGlobalContext();		
				for (int i = 0; i < oGlobal.getKeys().length; i++)
				{
					String sKey = (String) oGlobal.getKeys()[i];
					oCtx.put(sKey, oGlobal.get(sKey));
				}						
				oCtx.put("po",_oPO);
				Velocity.mergeTemplate("/print/modules/POEmail.vm", Attributes.s_DEFAULT_ENCODING, oCtx, oSW);								
				MailTool.sendHOEmail("", aTo, sSub, oSW.toString(), "", RecipientType.TO);
				sMessage = "Email sent to: "  + sEmail;
			} 
			catch (Exception e) 
			{
				sMessage = "ERROR Sending Email to:" + sEmail + " Message:" + e.getMessage();
				log.error(sMessage, e);
			}			
		}
		return sMessage;
	}	
}


package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.VendorTypeTool;
import com.ssti.enterprise.pos.tools.financial.VendorBalanceTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.CustomFormatter;

public class VendorViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    

    public static String[] a_HEADER = 
    {       
       "Vendor Code"     ,
       "Vendor Name"     ,
       "Vendor Type"     ,
       "Item Type"     	 ,
       "Tax No"          ,
       "Vendor Status"   ,
       "Contact Name1"   ,
       "Contact Phone1"  ,
       "Job Title1"      ,
       "Contact Name2"   ,
       "Contact Phone2"  ,
       "Job Title2"      ,
       "Contact Name3"   ,
       "Contact Phone3"  ,
       "Job Title3"      ,
       "Address"         ,
       "Zip"             ,
       "Country"         ,
       "Province"        ,
       "City"            ,
       "District"        ,
       "Village"         ,
       "Phone1"          ,
       "Phone2"          ,
       "Fax"             ,
       "Email"           ,
       "WebSite"         ,
       "Default Tax"     ,
       "Payment Type"    ,
       "Payment Term"    ,
       "Related Employee",
       "Currency"        ,
       "Default Item Disc",
       "Default Discount",
       "Credit Limit"    ,
       "Memo"            ,
       "Field1"          ,
       "Value1"          ,
       "Field2"          ,
       "Value2"          ,
       "AP Account"              
    };
    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = (List) _oSession.getAttribute("Vendors");
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Vendor View Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;        
        for (int i = 0; i < a_HEADER.length; i++)
        {
        	createHeaderCell(oWB, oRow, (short)iCol, a_HEADER[i]); iCol++;        	        
        }             
		if (PreferenceTool.useVendorTrPrefix())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Trans Prefix"); iCol++;	
		}
		createHeaderCell(oWB, oRow, (short)iCol, "Current Balance"); iCol++; 
				
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Vendor oVendor = (Vendor) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                String sTypeCode = VendorTypeTool.getCodeByID(oVendor.getVendorTypeId());

                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oVendor.getVendorCode());      iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getVendorName());      iCol++; 
                createCell(oWB, oRow2, (short)iCol, sTypeCode);      			   iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getItemTypeDesc());    iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getTaxNo     ());      iCol++; 
                if(oVendor.getVendorStatus() == 1){
                    createCell(oWB, oRow2, (short)iCol, "Active");                 iCol++; 
                }
                else{
                    createCell(oWB, oRow2, (short)iCol, "Non Active");             iCol++; 
                }
                createCell(oWB, oRow2, (short)iCol, oVendor.getContactName1  ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getContactPhone1 ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getJobTitle1     ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getContactName2  ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getContactPhone2 ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getJobTitle2     ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getContactName3  ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getContactPhone3 ());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getJobTitle3());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getAddress  ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getZip      ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getCountry  ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, AddressTool.getProvStr(oVendor.getProvince    ())); iCol++; 
                createCell(oWB, oRow2, (short)iCol, AddressTool.getCityStr(oVendor.getCity        ())); iCol++;                 
                createCell(oWB, oRow2, (short)iCol, AddressTool.getDistrictStr(oVendor.getDistrict())); iCol++;                 
                createCell(oWB, oRow2, (short)iCol, AddressTool.getVillageStr(oVendor.getVillage  ())); iCol++;                 
                
                createCell(oWB, oRow2, (short)iCol, oVendor.getPhone1   ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getPhone2   ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getFax      ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getEmail    ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getWebSite  ());       iCol++; 
                createCell(oWB, oRow2, (short)iCol, TaxTool.getCodeByID(oVendor.getDefaultTaxId()));                        iCol++; 
                createCell(oWB, oRow2, (short)iCol, PaymentTypeTool.getPaymentTypeCodeByID(oVendor.getDefaultTypeId()));    iCol++; 
                createCell(oWB, oRow2, (short)iCol, PaymentTermTool.getPaymentTermCodeByID(oVendor.getDefaultTermId()));    iCol++; 
                createCell(oWB, oRow2, (short)iCol, EmployeeTool.getEmployeeNameByID(oVendor.getDefaultEmployeeId()));      iCol++; 
                createCell(oWB, oRow2, (short)iCol, CurrencyTool.getCodeByID(oVendor.getDefaultCurrencyId()));              iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getDefaultDiscountAmount());    								iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getDefaultItemDisc());    										iCol++;
                createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oVendor.getCreditLimit()));                iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getMemo());            iCol++; 
                createCell(oWB, oRow2, (short)iCol, oVendor.getField1());          iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oVendor.getValue1());          iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oVendor.getField2());          iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oVendor.getValue2());          iCol++;                 
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oVendor.getApAccount())); iCol++;         
                if (PreferenceTool.useVendorTrPrefix())
        		{
                    createCell(oWB, oRow2, (short)iCol, oVendor.getTransPrefix()); iCol++;        
        		}

                //non master field
                BigDecimal dApBalance = VendorBalanceTool.getBalance(oVendor.getVendorId());
                double dLastCreditLimit = oVendor.getCreditLimit().doubleValue() - dApBalance.doubleValue();
                
                createCell(oWB, oRow2, (short)iCol, 
                		CustomFormatter.formatNumber(new BigDecimal(dLastCreditLimit))); iCol++;                 		                	
            }        
        }
        return oWB;
    }
}

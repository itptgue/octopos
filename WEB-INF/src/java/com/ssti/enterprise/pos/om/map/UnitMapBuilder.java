package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class UnitMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.UnitMapBuilder";

    /**
     * Item
     * @deprecated use UnitPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "unit";
    }

  
    /**
     * unit.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use UnitPeer.unit.UNIT_ID constant
     */
    public static String getUnit_UnitId()
    {
        return "unit.UNIT_ID";
    }
  
    /**
     * unit.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use UnitPeer.unit.UNIT_CODE constant
     */
    public static String getUnit_UnitCode()
    {
        return "unit.UNIT_CODE";
    }
  
    /**
     * unit.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use UnitPeer.unit.DESCRIPTION constant
     */
    public static String getUnit_Description()
    {
        return "unit.DESCRIPTION";
    }
  
    /**
     * unit.BASE_UNIT
     * @return the column name for the BASE_UNIT field
     * @deprecated use UnitPeer.unit.BASE_UNIT constant
     */
    public static String getUnit_BaseUnit()
    {
        return "unit.BASE_UNIT";
    }
  
    /**
     * unit.VALUE_TO_BASE
     * @return the column name for the VALUE_TO_BASE field
     * @deprecated use UnitPeer.unit.VALUE_TO_BASE constant
     */
    public static String getUnit_ValueToBase()
    {
        return "unit.VALUE_TO_BASE";
    }
  
    /**
     * unit.ALTERNATE_BASE
     * @return the column name for the ALTERNATE_BASE field
     * @deprecated use UnitPeer.unit.ALTERNATE_BASE constant
     */
    public static String getUnit_AlternateBase()
    {
        return "unit.ALTERNATE_BASE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("unit");
        TableMap tMap = dbMap.getTable("unit");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("unit.UNIT_ID", "");
                          tMap.addColumn("unit.UNIT_CODE", "");
                          tMap.addColumn("unit.DESCRIPTION", "");
                          tMap.addColumn("unit.BASE_UNIT", Boolean.TRUE);
                            tMap.addColumn("unit.VALUE_TO_BASE", bd_ZERO);
                          tMap.addColumn("unit.ALTERNATE_BASE", "");
          }
}

package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ApPaymentOtherMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ApPaymentOtherMapBuilder";

    /**
     * Item
     * @deprecated use ApPaymentOtherPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "ap_payment_other";
    }

  
    /**
     * ap_payment_other.AP_PAYMENT_OTHER_ID
     * @return the column name for the AP_PAYMENT_OTHER_ID field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.AP_PAYMENT_OTHER_ID constant
     */
    public static String getApPaymentOther_ApPaymentOtherId()
    {
        return "ap_payment_other.AP_PAYMENT_OTHER_ID";
    }
  
    /**
     * ap_payment_other.AP_PAYMENT_DETAIL_ID
     * @return the column name for the AP_PAYMENT_DETAIL_ID field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.AP_PAYMENT_DETAIL_ID constant
     */
    public static String getApPaymentOther_ApPaymentDetailId()
    {
        return "ap_payment_other.AP_PAYMENT_DETAIL_ID";
    }
  
    /**
     * ap_payment_other.AP_PAYMENT_ID
     * @return the column name for the AP_PAYMENT_ID field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.AP_PAYMENT_ID constant
     */
    public static String getApPaymentOther_ApPaymentId()
    {
        return "ap_payment_other.AP_PAYMENT_ID";
    }
  
    /**
     * ap_payment_other.OTHER_AMOUNT
     * @return the column name for the OTHER_AMOUNT field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.OTHER_AMOUNT constant
     */
    public static String getApPaymentOther_OtherAmount()
    {
        return "ap_payment_other.OTHER_AMOUNT";
    }
  
    /**
     * ap_payment_other.OTER_ACCOUNT_ID
     * @return the column name for the OTER_ACCOUNT_ID field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.OTER_ACCOUNT_ID constant
     */
    public static String getApPaymentOther_OterAccountId()
    {
        return "ap_payment_other.OTER_ACCOUNT_ID";
    }
  
    /**
     * ap_payment_other.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.DEPARTMENT_ID constant
     */
    public static String getApPaymentOther_DepartmentId()
    {
        return "ap_payment_other.DEPARTMENT_ID";
    }
  
    /**
     * ap_payment_other.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.PROJECT_ID constant
     */
    public static String getApPaymentOther_ProjectId()
    {
        return "ap_payment_other.PROJECT_ID";
    }
  
    /**
     * ap_payment_other.MEMO
     * @return the column name for the MEMO field
     * @deprecated use ApPaymentOtherPeer.ap_payment_other.MEMO constant
     */
    public static String getApPaymentOther_Memo()
    {
        return "ap_payment_other.MEMO";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("ap_payment_other");
        TableMap tMap = dbMap.getTable("ap_payment_other");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("ap_payment_other.AP_PAYMENT_OTHER_ID", "");
                          tMap.addForeignKey(
                "ap_payment_other.AP_PAYMENT_DETAIL_ID", "" , "ap_payment_detail" ,
                "ap_payment_detail_id");
                          tMap.addColumn("ap_payment_other.AP_PAYMENT_ID", "");
                            tMap.addColumn("ap_payment_other.OTHER_AMOUNT", bd_ZERO);
                          tMap.addColumn("ap_payment_other.OTER_ACCOUNT_ID", "");
                          tMap.addColumn("ap_payment_other.DEPARTMENT_ID", "");
                          tMap.addColumn("ap_payment_other.PROJECT_ID", "");
                          tMap.addColumn("ap_payment_other.MEMO", "");
          }
}

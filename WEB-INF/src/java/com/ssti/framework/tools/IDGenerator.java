package com.ssti.framework.tools;

import java.util.Date;
import java.util.Random;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Generate Unique ID 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: IDGenerator.java,v 1.1 2008/06/29 07:11:57 albert Exp $ <br>
 * 
 * <pre>
 * $Log: IDGenerator.java,v $
 * Revision 1.1  2008/06/29 07:11:57  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/11/08 09:32:10  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class IDGenerator
{
	private static final Random oSeeder = new Random();
	private static int iSequence = 0;

	public static synchronized String generateSysID () 
		throws Exception
	{
        return generateSysID (true);
	}
	
	/**
	 * Generate Unique ID 
	 * Location Code + currentTimeMillis + sequence(4) + random(4)
	 * 
	 * @param _bUseLocation
	 * @return generated sys ID
	 * @throws Exception
	 */
	public static synchronized String generateSysID (boolean _bUseLocation) 
		throws Exception
	{		
	    String sCode = "";

		String sSequence = (Integer.valueOf(iSequence)).toString();
		String sRandom = (Integer.valueOf(oSeeder.nextInt(9999))).toString();
		
		StringBuilder oSB =  new StringBuilder(sCode);
		oSB.append (System.currentTimeMillis());
		oSB.append (StringUtil.formatNumberString (sSequence,  4));
		oSB.append (StringUtil.formatNumberString (sRandom, 4));
		
		iSequence++; //max 9999
		if (iSequence == 10000) { iSequence = 0; Thread.sleep(50); } //reset sequence
		
		return oSB.toString();
	}
	
	public static Date getDateFromID (String _sID, boolean _bUseLocation) 
		throws Exception
	{		
	    String sCode = "";
	    int iStart = 0;
	    if (_bUseLocation)
	    {
		    iStart = sCode.length();
		}
	    if (StringUtil.isNotEmpty(_sID))
	    {
	    	String sID = StringUtil.substring(_sID, iStart);
		    sID = sID.substring(0, sID.length() - 4);   
		    Date d = new Date();
		    d.setTime(Long.parseLong(sID));
		    return d;
	    }
	    return null;
	}		
}  
package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CurrencyManager;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyDetailPeer;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Currency action class
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: MultiCurrencyAction.java,v 1.13 2008/06/29 07:03:22 albert Exp $ <br>
 *
 * <pre>
 * $Log: MultiCurrencyAction.java,v $
 * 
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class MultiCurrencyAction extends GLSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Currency oCurr = new Currency();
			setProperties (oCurr, data);
			oCurr.setIsActive(data.getParameters().getBoolean("IsActive"));

			if(data.getParameters().getBoolean("IsDefault") == false)
			{
				if(CurrencyTool.checkDefaultStatus())
				{
					oCurr.setIsDefault(false);		
			    }
			}
			else
			{
				CurrencyTool.setAllStatus(false);
				oCurr.setIsDefault(true);		
			}
	        oCurr.setCurrencyId (IDGenerator.generateSysID());
	        oCurr.save();
        	
        	if (oCurr.getIsDefault())
        	{
        	    CurrencyTool.createDefaultDetailRate(oCurr);
        	}
        	CurrencyManager.getInstance().refreshCache(oCurr);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
           if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Currency oCurr = CurrencyTool.getCurrencyByID(data.getParameters().getString("ID"));
	        Currency oOld = oCurr.copy();
	        
			setProperties (oCurr, data);
   			oCurr.setIsActive(data.getParameters().getBoolean("IsActive"));
			oCurr.setIsDefault(data.getParameters().getBoolean("IsDefault"));
	        oCurr.save();	        
	        CurrencyManager.getInstance().refreshCache(oCurr);
	        UpdateHistoryTool.createHistory(oOld, oCurr, data.getUser().getName(), null);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    { 		
    	try
    	{
    		StringBuilder oMsg = new StringBuilder();
    		String sColumn = "currency_id";
    		String sID = data.getParameters().getString("ID");
    		if (SqlUtil.validateFKRef("bank",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("customer","default_currency_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("vendor","default_currency_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("journal_voucher_detail",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("purchase_order",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("purchase_receipt",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("purchase_invoice",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("bank_book",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("cash_flow",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("gl_transaction",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("sales_order",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("delivery_order",sColumn,sID,oMsg)
    			&& SqlUtil.validateFKRef("sales_transaction",sColumn,sID,oMsg))
    		{
    			Currency oCurr = CurrencyTool.getCurrencyByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(CurrencyDetailPeer.CURRENCY_ID, sID);
	        	CurrencyDetailPeer.doDelete(oCrit);
            	
	        	oCrit = new Criteria();
	        	oCrit.add(CurrencyPeer.CURRENCY_ID, sID);
	        	CurrencyPeer.doDelete(oCrit);
	        	
	        	UpdateHistoryTool.createDelHistory(oCurr, data.getUser().getName(), null);
	        	CurrencyManager.getInstance().refreshCache(sID);
	        	data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
	    	}
	    	else
	    	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE) + " \n" + oMsg);
            }
        }
        catch (Exception _oEx)
        {
            log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

    public void setProperties (Currency _oCurr, RunData data)
        throws Exception
    {
		data.getParameters().setProperties(_oCurr);
    }
}

package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.EmployeeMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseEmployeePeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "employee";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(EmployeeMapBuilder.CLASS_NAME);
    }

      /** the column name for the EMPLOYEE_ID field */
    public static final String EMPLOYEE_ID;
      /** the column name for the EMPLOYEE_CODE field */
    public static final String EMPLOYEE_CODE;
      /** the column name for the EMPLOYEE_NAME field */
    public static final String EMPLOYEE_NAME;
      /** the column name for the USER_NAME field */
    public static final String USER_NAME;
      /** the column name for the JOB_TITLE field */
    public static final String JOB_TITLE;
      /** the column name for the GENDER field */
    public static final String GENDER;
      /** the column name for the BIRTH_PLACE field */
    public static final String BIRTH_PLACE;
      /** the column name for the BIRTH_DATE field */
    public static final String BIRTH_DATE;
      /** the column name for the START_DATE field */
    public static final String START_DATE;
      /** the column name for the QUIT_DATE field */
    public static final String QUIT_DATE;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the ADDRESS field */
    public static final String ADDRESS;
      /** the column name for the ZIP field */
    public static final String ZIP;
      /** the column name for the COUNTRY field */
    public static final String COUNTRY;
      /** the column name for the PROVINCE field */
    public static final String PROVINCE;
      /** the column name for the CITY field */
    public static final String CITY;
      /** the column name for the DISTRICT field */
    public static final String DISTRICT;
      /** the column name for the VILLAGE field */
    public static final String VILLAGE;
      /** the column name for the HOME_PHONE field */
    public static final String HOME_PHONE;
      /** the column name for the MOBILE_PHONE field */
    public static final String MOBILE_PHONE;
      /** the column name for the EMAIL field */
    public static final String EMAIL;
      /** the column name for the SALES_COMMISION_PCT field */
    public static final String SALES_COMMISION_PCT;
      /** the column name for the MARGIN_COMMISION_PCT field */
    public static final String MARGIN_COMMISION_PCT;
      /** the column name for the MONTHLY_SALARY field */
    public static final String MONTHLY_SALARY;
      /** the column name for the OTHER_INCOME field */
    public static final String OTHER_INCOME;
      /** the column name for the FREQUENCY field */
    public static final String FREQUENCY;
      /** the column name for the MEMO field */
    public static final String MEMO;
      /** the column name for the ADD_DATE field */
    public static final String ADD_DATE;
      /** the column name for the UPDATE_DATE field */
    public static final String UPDATE_DATE;
      /** the column name for the ROLE_ID field */
    public static final String ROLE_ID;
      /** the column name for the FIELD1 field */
    public static final String FIELD1;
      /** the column name for the FIELD2 field */
    public static final String FIELD2;
      /** the column name for the VALUE1 field */
    public static final String VALUE1;
      /** the column name for the VALUE2 field */
    public static final String VALUE2;
      /** the column name for the IS_ACTIVE field */
    public static final String IS_ACTIVE;
      /** the column name for the IS_SALESMAN field */
    public static final String IS_SALESMAN;
      /** the column name for the DEPARTMENT_ID field */
    public static final String DEPARTMENT_ID;
      /** the column name for the PARENT_ID field */
    public static final String PARENT_ID;
      /** the column name for the HAS_CHILD field */
    public static final String HAS_CHILD;
      /** the column name for the EMPLOYEE_LEVEL field */
    public static final String EMPLOYEE_LEVEL;
      /** the column name for the SALES_AREA_ID field */
    public static final String SALES_AREA_ID;
      /** the column name for the SALES_AREA1_ID field */
    public static final String SALES_AREA1_ID;
      /** the column name for the SALES_AREA2_ID field */
    public static final String SALES_AREA2_ID;
      /** the column name for the SALES_AREA3_ID field */
    public static final String SALES_AREA3_ID;
      /** the column name for the TERRITORY_ID field */
    public static final String TERRITORY_ID;
      /** the column name for the TERRITORY1_ID field */
    public static final String TERRITORY1_ID;
      /** the column name for the TERRITORY2_ID field */
    public static final String TERRITORY2_ID;
      /** the column name for the TERRITORY3_ID field */
    public static final String TERRITORY3_ID;
  
    static
    {
          EMPLOYEE_ID = "employee.EMPLOYEE_ID";
          EMPLOYEE_CODE = "employee.EMPLOYEE_CODE";
          EMPLOYEE_NAME = "employee.EMPLOYEE_NAME";
          USER_NAME = "employee.USER_NAME";
          JOB_TITLE = "employee.JOB_TITLE";
          GENDER = "employee.GENDER";
          BIRTH_PLACE = "employee.BIRTH_PLACE";
          BIRTH_DATE = "employee.BIRTH_DATE";
          START_DATE = "employee.START_DATE";
          QUIT_DATE = "employee.QUIT_DATE";
          LOCATION_ID = "employee.LOCATION_ID";
          ADDRESS = "employee.ADDRESS";
          ZIP = "employee.ZIP";
          COUNTRY = "employee.COUNTRY";
          PROVINCE = "employee.PROVINCE";
          CITY = "employee.CITY";
          DISTRICT = "employee.DISTRICT";
          VILLAGE = "employee.VILLAGE";
          HOME_PHONE = "employee.HOME_PHONE";
          MOBILE_PHONE = "employee.MOBILE_PHONE";
          EMAIL = "employee.EMAIL";
          SALES_COMMISION_PCT = "employee.SALES_COMMISION_PCT";
          MARGIN_COMMISION_PCT = "employee.MARGIN_COMMISION_PCT";
          MONTHLY_SALARY = "employee.MONTHLY_SALARY";
          OTHER_INCOME = "employee.OTHER_INCOME";
          FREQUENCY = "employee.FREQUENCY";
          MEMO = "employee.MEMO";
          ADD_DATE = "employee.ADD_DATE";
          UPDATE_DATE = "employee.UPDATE_DATE";
          ROLE_ID = "employee.ROLE_ID";
          FIELD1 = "employee.FIELD1";
          FIELD2 = "employee.FIELD2";
          VALUE1 = "employee.VALUE1";
          VALUE2 = "employee.VALUE2";
          IS_ACTIVE = "employee.IS_ACTIVE";
          IS_SALESMAN = "employee.IS_SALESMAN";
          DEPARTMENT_ID = "employee.DEPARTMENT_ID";
          PARENT_ID = "employee.PARENT_ID";
          HAS_CHILD = "employee.HAS_CHILD";
          EMPLOYEE_LEVEL = "employee.EMPLOYEE_LEVEL";
          SALES_AREA_ID = "employee.SALES_AREA_ID";
          SALES_AREA1_ID = "employee.SALES_AREA1_ID";
          SALES_AREA2_ID = "employee.SALES_AREA2_ID";
          SALES_AREA3_ID = "employee.SALES_AREA3_ID";
          TERRITORY_ID = "employee.TERRITORY_ID";
          TERRITORY1_ID = "employee.TERRITORY1_ID";
          TERRITORY2_ID = "employee.TERRITORY2_ID";
          TERRITORY3_ID = "employee.TERRITORY3_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(EmployeeMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(EmployeeMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  48;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Employee";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseEmployeePeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_SALESMAN))
        {
            Object possibleBoolean = criteria.get(IS_SALESMAN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_SALESMAN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(EMPLOYEE_ID);
          criteria.addSelectColumn(EMPLOYEE_CODE);
          criteria.addSelectColumn(EMPLOYEE_NAME);
          criteria.addSelectColumn(USER_NAME);
          criteria.addSelectColumn(JOB_TITLE);
          criteria.addSelectColumn(GENDER);
          criteria.addSelectColumn(BIRTH_PLACE);
          criteria.addSelectColumn(BIRTH_DATE);
          criteria.addSelectColumn(START_DATE);
          criteria.addSelectColumn(QUIT_DATE);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(ADDRESS);
          criteria.addSelectColumn(ZIP);
          criteria.addSelectColumn(COUNTRY);
          criteria.addSelectColumn(PROVINCE);
          criteria.addSelectColumn(CITY);
          criteria.addSelectColumn(DISTRICT);
          criteria.addSelectColumn(VILLAGE);
          criteria.addSelectColumn(HOME_PHONE);
          criteria.addSelectColumn(MOBILE_PHONE);
          criteria.addSelectColumn(EMAIL);
          criteria.addSelectColumn(SALES_COMMISION_PCT);
          criteria.addSelectColumn(MARGIN_COMMISION_PCT);
          criteria.addSelectColumn(MONTHLY_SALARY);
          criteria.addSelectColumn(OTHER_INCOME);
          criteria.addSelectColumn(FREQUENCY);
          criteria.addSelectColumn(MEMO);
          criteria.addSelectColumn(ADD_DATE);
          criteria.addSelectColumn(UPDATE_DATE);
          criteria.addSelectColumn(ROLE_ID);
          criteria.addSelectColumn(FIELD1);
          criteria.addSelectColumn(FIELD2);
          criteria.addSelectColumn(VALUE1);
          criteria.addSelectColumn(VALUE2);
          criteria.addSelectColumn(IS_ACTIVE);
          criteria.addSelectColumn(IS_SALESMAN);
          criteria.addSelectColumn(DEPARTMENT_ID);
          criteria.addSelectColumn(PARENT_ID);
          criteria.addSelectColumn(HAS_CHILD);
          criteria.addSelectColumn(EMPLOYEE_LEVEL);
          criteria.addSelectColumn(SALES_AREA_ID);
          criteria.addSelectColumn(SALES_AREA1_ID);
          criteria.addSelectColumn(SALES_AREA2_ID);
          criteria.addSelectColumn(SALES_AREA3_ID);
          criteria.addSelectColumn(TERRITORY_ID);
          criteria.addSelectColumn(TERRITORY1_ID);
          criteria.addSelectColumn(TERRITORY2_ID);
          criteria.addSelectColumn(TERRITORY3_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Employee row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Employee obj = (Employee) cls.newInstance();
            EmployeePeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Employee obj)
        throws TorqueException
    {
        try
        {
                obj.setEmployeeId(row.getValue(offset + 0).asString());
                  obj.setEmployeeCode(row.getValue(offset + 1).asString());
                  obj.setEmployeeName(row.getValue(offset + 2).asString());
                  obj.setUserName(row.getValue(offset + 3).asString());
                  obj.setJobTitle(row.getValue(offset + 4).asString());
                  obj.setGender(row.getValue(offset + 5).asInt());
                  obj.setBirthPlace(row.getValue(offset + 6).asString());
                  obj.setBirthDate(row.getValue(offset + 7).asUtilDate());
                  obj.setStartDate(row.getValue(offset + 8).asUtilDate());
                  obj.setQuitDate(row.getValue(offset + 9).asUtilDate());
                  obj.setLocationId(row.getValue(offset + 10).asString());
                  obj.setAddress(row.getValue(offset + 11).asString());
                  obj.setZip(row.getValue(offset + 12).asString());
                  obj.setCountry(row.getValue(offset + 13).asString());
                  obj.setProvince(row.getValue(offset + 14).asString());
                  obj.setCity(row.getValue(offset + 15).asString());
                  obj.setDistrict(row.getValue(offset + 16).asString());
                  obj.setVillage(row.getValue(offset + 17).asString());
                  obj.setHomePhone(row.getValue(offset + 18).asString());
                  obj.setMobilePhone(row.getValue(offset + 19).asString());
                  obj.setEmail(row.getValue(offset + 20).asString());
                  obj.setSalesCommisionPct(row.getValue(offset + 21).asBigDecimal());
                  obj.setMarginCommisionPct(row.getValue(offset + 22).asBigDecimal());
                  obj.setMonthlySalary(row.getValue(offset + 23).asBigDecimal());
                  obj.setOtherIncome(row.getValue(offset + 24).asBigDecimal());
                  obj.setFrequency(row.getValue(offset + 25).asInt());
                  obj.setMemo(row.getValue(offset + 26).asString());
                  obj.setAddDate(row.getValue(offset + 27).asUtilDate());
                  obj.setUpdateDate(row.getValue(offset + 28).asUtilDate());
                  obj.setRoleId(row.getValue(offset + 29).asInt());
                  obj.setField1(row.getValue(offset + 30).asString());
                  obj.setField2(row.getValue(offset + 31).asString());
                  obj.setValue1(row.getValue(offset + 32).asString());
                  obj.setValue2(row.getValue(offset + 33).asString());
                  obj.setIsActive(row.getValue(offset + 34).asBoolean());
                  obj.setIsSalesman(row.getValue(offset + 35).asBoolean());
                  obj.setDepartmentId(row.getValue(offset + 36).asString());
                  obj.setParentId(row.getValue(offset + 37).asString());
                  obj.setHasChild(row.getValue(offset + 38).asBoolean());
                  obj.setEmployeeLevel(row.getValue(offset + 39).asInt());
                  obj.setSalesAreaId(row.getValue(offset + 40).asString());
                  obj.setSalesArea1Id(row.getValue(offset + 41).asString());
                  obj.setSalesArea2Id(row.getValue(offset + 42).asString());
                  obj.setSalesArea3Id(row.getValue(offset + 43).asString());
                  obj.setTerritoryId(row.getValue(offset + 44).asString());
                  obj.setTerritory1Id(row.getValue(offset + 45).asString());
                  obj.setTerritory2Id(row.getValue(offset + 46).asString());
                  obj.setTerritory3Id(row.getValue(offset + 47).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseEmployeePeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_SALESMAN))
        {
            Object possibleBoolean = criteria.get(IS_SALESMAN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_SALESMAN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(EmployeePeer.row2Object(row, 1,
                EmployeePeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseEmployeePeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(EMPLOYEE_ID, criteria.remove(EMPLOYEE_ID));
                                                                                                                                                                                                                                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_SALESMAN))
        {
            Object possibleBoolean = criteria.get(IS_SALESMAN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_SALESMAN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         EmployeePeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_ACTIVE))
        {
            Object possibleBoolean = criteria.get(IS_ACTIVE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_ACTIVE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_SALESMAN))
        {
            Object possibleBoolean = criteria.get(IS_SALESMAN);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_SALESMAN, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAS_CHILD))
        {
            Object possibleBoolean = criteria.get(HAS_CHILD);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAS_CHILD, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Employee obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Employee obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Employee obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Employee obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Employee) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Employee obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Employee) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Employee obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Employee) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Employee obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseEmployeePeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(EMPLOYEE_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Employee obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(EMPLOYEE_ID, obj.getEmployeeId());
              criteria.add(EMPLOYEE_CODE, obj.getEmployeeCode());
              criteria.add(EMPLOYEE_NAME, obj.getEmployeeName());
              criteria.add(USER_NAME, obj.getUserName());
              criteria.add(JOB_TITLE, obj.getJobTitle());
              criteria.add(GENDER, obj.getGender());
              criteria.add(BIRTH_PLACE, obj.getBirthPlace());
              criteria.add(BIRTH_DATE, obj.getBirthDate());
              criteria.add(START_DATE, obj.getStartDate());
              criteria.add(QUIT_DATE, obj.getQuitDate());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(ADDRESS, obj.getAddress());
              criteria.add(ZIP, obj.getZip());
              criteria.add(COUNTRY, obj.getCountry());
              criteria.add(PROVINCE, obj.getProvince());
              criteria.add(CITY, obj.getCity());
              criteria.add(DISTRICT, obj.getDistrict());
              criteria.add(VILLAGE, obj.getVillage());
              criteria.add(HOME_PHONE, obj.getHomePhone());
              criteria.add(MOBILE_PHONE, obj.getMobilePhone());
              criteria.add(EMAIL, obj.getEmail());
              criteria.add(SALES_COMMISION_PCT, obj.getSalesCommisionPct());
              criteria.add(MARGIN_COMMISION_PCT, obj.getMarginCommisionPct());
              criteria.add(MONTHLY_SALARY, obj.getMonthlySalary());
              criteria.add(OTHER_INCOME, obj.getOtherIncome());
              criteria.add(FREQUENCY, obj.getFrequency());
              criteria.add(MEMO, obj.getMemo());
              criteria.add(ADD_DATE, obj.getAddDate());
              criteria.add(UPDATE_DATE, obj.getUpdateDate());
              criteria.add(ROLE_ID, obj.getRoleId());
              criteria.add(FIELD1, obj.getField1());
              criteria.add(FIELD2, obj.getField2());
              criteria.add(VALUE1, obj.getValue1());
              criteria.add(VALUE2, obj.getValue2());
              criteria.add(IS_ACTIVE, obj.getIsActive());
              criteria.add(IS_SALESMAN, obj.getIsSalesman());
              criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
              criteria.add(PARENT_ID, obj.getParentId());
              criteria.add(HAS_CHILD, obj.getHasChild());
              criteria.add(EMPLOYEE_LEVEL, obj.getEmployeeLevel());
              criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
              criteria.add(SALES_AREA1_ID, obj.getSalesArea1Id());
              criteria.add(SALES_AREA2_ID, obj.getSalesArea2Id());
              criteria.add(SALES_AREA3_ID, obj.getSalesArea3Id());
              criteria.add(TERRITORY_ID, obj.getTerritoryId());
              criteria.add(TERRITORY1_ID, obj.getTerritory1Id());
              criteria.add(TERRITORY2_ID, obj.getTerritory2Id());
              criteria.add(TERRITORY3_ID, obj.getTerritory3Id());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Employee obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(EMPLOYEE_ID, obj.getEmployeeId());
                          criteria.add(EMPLOYEE_CODE, obj.getEmployeeCode());
                          criteria.add(EMPLOYEE_NAME, obj.getEmployeeName());
                          criteria.add(USER_NAME, obj.getUserName());
                          criteria.add(JOB_TITLE, obj.getJobTitle());
                          criteria.add(GENDER, obj.getGender());
                          criteria.add(BIRTH_PLACE, obj.getBirthPlace());
                          criteria.add(BIRTH_DATE, obj.getBirthDate());
                          criteria.add(START_DATE, obj.getStartDate());
                          criteria.add(QUIT_DATE, obj.getQuitDate());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(ADDRESS, obj.getAddress());
                          criteria.add(ZIP, obj.getZip());
                          criteria.add(COUNTRY, obj.getCountry());
                          criteria.add(PROVINCE, obj.getProvince());
                          criteria.add(CITY, obj.getCity());
                          criteria.add(DISTRICT, obj.getDistrict());
                          criteria.add(VILLAGE, obj.getVillage());
                          criteria.add(HOME_PHONE, obj.getHomePhone());
                          criteria.add(MOBILE_PHONE, obj.getMobilePhone());
                          criteria.add(EMAIL, obj.getEmail());
                          criteria.add(SALES_COMMISION_PCT, obj.getSalesCommisionPct());
                          criteria.add(MARGIN_COMMISION_PCT, obj.getMarginCommisionPct());
                          criteria.add(MONTHLY_SALARY, obj.getMonthlySalary());
                          criteria.add(OTHER_INCOME, obj.getOtherIncome());
                          criteria.add(FREQUENCY, obj.getFrequency());
                          criteria.add(MEMO, obj.getMemo());
                          criteria.add(ADD_DATE, obj.getAddDate());
                          criteria.add(UPDATE_DATE, obj.getUpdateDate());
                          criteria.add(ROLE_ID, obj.getRoleId());
                          criteria.add(FIELD1, obj.getField1());
                          criteria.add(FIELD2, obj.getField2());
                          criteria.add(VALUE1, obj.getValue1());
                          criteria.add(VALUE2, obj.getValue2());
                          criteria.add(IS_ACTIVE, obj.getIsActive());
                          criteria.add(IS_SALESMAN, obj.getIsSalesman());
                          criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
                          criteria.add(PARENT_ID, obj.getParentId());
                          criteria.add(HAS_CHILD, obj.getHasChild());
                          criteria.add(EMPLOYEE_LEVEL, obj.getEmployeeLevel());
                          criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
                          criteria.add(SALES_AREA1_ID, obj.getSalesArea1Id());
                          criteria.add(SALES_AREA2_ID, obj.getSalesArea2Id());
                          criteria.add(SALES_AREA3_ID, obj.getSalesArea3Id());
                          criteria.add(TERRITORY_ID, obj.getTerritoryId());
                          criteria.add(TERRITORY1_ID, obj.getTerritory1Id());
                          criteria.add(TERRITORY2_ID, obj.getTerritory2Id());
                          criteria.add(TERRITORY3_ID, obj.getTerritory3Id());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Employee retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Employee retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Employee retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Employee retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Employee retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Employee)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( EMPLOYEE_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

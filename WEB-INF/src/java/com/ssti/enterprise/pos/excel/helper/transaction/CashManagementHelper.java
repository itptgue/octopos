package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SortingTool;

public class CashManagementHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Cash Management Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {                
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        String sKeywords = (String) getFromParam(oParam, "Keywords", null);        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        String sBankID = (String) getFromParam(oParam, "BankId", null);        
        String sLocationID = (String) getFromParam(oParam, "LocationId", null);
        int iType = (Integer) getFromParam(oParam, "CashFlowType", Integer.class);
        int iStatus = (Integer) getFromParam(oParam, "status", Integer.class);        
        String sCurrencyID = (String) getFromParam(oParam, "CurrencyId", null);
        Date dStartDue = CustomParser.parseDate((String)getFromParam(oParam, "StartDue", null));
        Date dEndDue = CustomParser.parseDate((String)getFromParam(oParam, "EndDue", null));
        
        List vData = CashFlowTool.findTrans(iCond, sKeywords, sBankID, sLocationID, iType, 
        									iStatus,  AppAttributes.i_CF_NORMAL, dStart, dEnd, dStartDue, dEndDue);
        vData = SortingTool.sort(vData,"cashFlowNo");

        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	CashFlow oTR = (CashFlow) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = CashFlowTool.getDetailsByID(oTR.getCashFlowId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		CashFlowJournal oTD = (CashFlowJournal) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}

        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Bank Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans Type"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Due Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Reference No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Issuer Dest"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Rate"); iCol++;
    }
    
	private void createMasterContent(HSSFRow oRow, CashFlow oTR) 
		throws Exception
	{
    	int iCol = 0;
    	String sType = "Deposit";
    	if (oTR.getCashFlowType() == GlAttributes.i_WITHDRAWAL) sType = "Withdrawal";
    	
    	createCell(oWB, oRow, (short)iCol, oTR.getCashFlowNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, BankTool.getBankByID(oTR.getBankId()).getBankCode()); iCol++;		
		createCell(oWB, oRow, (short)iCol, sType); iCol++;		
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getDueDate())); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getReferenceNo()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getBankIssuer()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTR.getDescription()); iCol++;
    	createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTR.getCurrencyId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getCurrencyRate().toString()); iCol++;	
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Account Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Rate"); iCol++;
    	createHeaderCell(oWB, oRow, (short)iCol, "Amount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Memo"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cash Flow Type"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Location"); iCol++;
		if (PreferenceTool.getMultiDept())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Department"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Project"); iCol++;
		}
    }
    
    private void createDetailContent(HSSFRow oRow, CashFlowJournal oTD) 
    	throws Exception 
    {    	
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getAccountCode()); iCol++;
    	createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTD.getCurrencyId())); iCol++;
    	createCell(oWB, oRow, (short)iCol, oTD.getCurrencyRate().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getAmount().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getMemo()); iCol++;		
		createCell(oWB, oRow, (short)iCol, CashFlowTypeTool.getTypeCodeByID(oTD.getCashFlowTypeId(),null)); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTD.getLocationId())); iCol++;
		
		createCell(oWB, oRow, (short)iCol, ""); iCol++;		
        //createCell(oWB, oRow, (short)iCol, oTD.getAccountName()); iCol++;
    }
}

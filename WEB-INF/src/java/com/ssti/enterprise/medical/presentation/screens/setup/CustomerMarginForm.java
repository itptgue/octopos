package com.ssti.enterprise.medical.presentation.screens.setup;

import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.manager.MedConfigManager;
import com.ssti.enterprise.medical.om.CustomerMargin;
import com.ssti.enterprise.medical.om.CustomerMarginPeer;
import com.ssti.enterprise.medical.tools.CustomerMarginTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

public class CustomerMarginForm extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
		String sMode = data.getParameters().getString("mode", "");
		String sID = "";
		int iOp = data.getParameters().getInt("op");
		log.debug("iOP" + iOp);
		try
		{
			if (iOp == 1)
			{
				doInsert(data, context);
			}
			else if (iOp == 2)
			{
				doUpdate(data, context);
			}
			else if (iOp == 3)
			{
				doDelete(data, context);
			}
			else
			{
				if (sMode.equals("update") || sMode.equals("delete"))
				{
					sID = data.getParameters().getString("CustomerMarginId");
					context.put("cm", CustomerMarginTool.getByID(sID));
	    		}
			}
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
    
	public void doInsert(RunData data, Context context)
    	throws Exception
    {
		try
		{						
			CustomerMargin oCM = new CustomerMargin();
			oCM.setCustomerMarginId(IDGenerator.generateSysID());
			setProp(oCM,data,context);
			validate(oCM);
			oCM.save();
			MedConfigManager.getInstance().refreshCache();
			data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
			
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
		}
    }
	
	public void doUpdate(RunData data, Context context)
		throws Exception
	{
		try
		{
			
			String sCMID = data.getParameters().getString("id");
			CustomerMargin oCM = CustomerMarginTool.getByID(sCMID);			
			if (oCM != null)
			{
				setProp(oCM,data,context);
				validate(oCM);
				oCM.save();
				MedConfigManager.getInstance().refreshCache();
				data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
		}
	}
	
	public void doDelete(RunData data, Context context)
		throws Exception
	{
		try
		{
			String sID = data.getParameters().getString("id");			
			Criteria oCrit = new Criteria();
			oCrit.add(CustomerMarginPeer.CUSTOMER_MARGIN_ID, sID);
			CustomerMarginPeer.doDelete(oCrit);
			MedConfigManager.getInstance().refreshCache();
			data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
		}
	}
	
	void setProp(CustomerMargin oCM, RunData data, Context context)
		throws Exception
	{
		data.getParameters().setProperties(oCM);
		
		StringBuilder oClr = new StringBuilder();
		String[] aClr = data.getParameters().getStrings("Color");		
		if(aClr != null)
		{
			for (int i = 0; i < aClr.length; i++)
	        {
	            String sClr = aClr[i];
	            if (sClr.equals(""))
	            {
	                oClr.append("");
	                break;
	            }
	            else 
	            {
	                oClr.append(sClr);
	                if (i != (aClr.length - 1))
	                {
	                    oClr.append(",");
	                }
	            }
	        }
		}
		
		StringBuilder oLoc = new StringBuilder();
		String[] aLoc = data.getParameters().getStrings("locationId");
        for (int i = 0; i < aLoc.length; i++)
        {
            String sLoc = aLoc[i];
            if (sLoc.equals(""))
            {
                oLoc.append("");
                break;
            }
            else 
            {
                oLoc.append(sLoc);
                if (i != (aLoc.length - 1))
                {
                    oLoc.append(",");
                }
            }
        }
        
        oCM.setColor(oClr.toString());
        oCM.setLocationIds(oLoc.toString());		
		oCM.setRfeeApplied(data.getParameters().getBoolean("RfeeApplied"));
		oCM.setPackApplied(data.getParameters().getBoolean("PackApplied"));		
	}
	
	private void validate(CustomerMargin _oCM)
	    throws Exception
    {
        //validate customer code
	    if (StringUtil.isNotEmpty(_oCM.getCustomerId()))
        {
	        List vCode = StringUtil.toList(_oCM.getCustomerId(),",");
            for (int i = 0; i < vCode.size(); i++)
            {
                String sCode = (String)vCode.get(i);
                if (CustomerTool.getCustomerByCode(sCode) == null) throw new Exception("Customer " + sCode + " Not Found");
            }
        }
        //validate item code
        if (StringUtil.isNotEmpty(_oCM.getItemCodes()))
        {
            List vCode = StringUtil.toList(_oCM.getItemCodes(),",");
            for (int i = 0; i < vCode.size(); i++)
            {
                String sCode = (String)vCode.get(i);
                if (ItemTool.getItemByCode(sCode) == null) throw new Exception("Item " + sCode + " Not Found");
            }
        }
        //validate SKU
        
        //validate type && other rule   
        
        //validate exists
//        Discount oExist = DiscountTool.getExists(_oCM);
//        if(oExist != null)
//        {
//        	throw new Exception("Discount Rule Conflict With : " + oExist.getDiscountCode());
//        }
    } 
}
package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Transaction;

import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BaseValidator.java,v 1.3 2007/04/17 13:05:40 albert Exp $ <br>
 *
 * <pre>
 * $Log: BaseValidator.java,v $
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public abstract class BaseValidator implements DataValidator 
{
	Log log = LogFactory.getLog(getClass());
	
	protected Connection oConn = null;
	
	protected static final String s_ALL_INV_LOC = 
		"SELECT * FROM inventory_location order by location_id ";
	
	protected StringBuilder m_oResult = new StringBuilder();
	protected int m_iInvalidResult  = 0;
	protected long m_lStart = 0;
	
	public String getResult()
	{
		return m_oResult.toString();
	}

	private int _total_length = 0;

	protected void prepareTitle(String _sTitle, String[] s, int[] l)
	{
		prepareTitle(_sTitle, s, l, null);
	}
	
	protected void prepareTitle(String _sTitle, String[] s, int[] l, int[] a)
	{
		
		m_oResult.append("\n").append(_sTitle).append("\n");
		for (int i = 0; i < _sTitle.length(); i++) m_oResult.append("*");
		m_oResult.append("\n\n");

		for(int i = 0; i < l.length; i++ ) for (int j = 0; j < l[i]; j++) _total_length++;
		for(int i = 0; i < _total_length; i++ ) m_oResult.append("-");
		
		m_oResult.append("\n");

		for(int i = 0; i < s.length; i++ )
		{
			if (l[i] != 10 && l[i] != 15)
			{
				if (a == null)
				{
					m_oResult.append(StringUtil.left(s[i], l[i]));		
				}
				else if (i < a.length)
				{					
					if (a[i] == 0) //left aligned
					{
						m_oResult.append(StringUtil.left(s[i], l[i]));						
					}
					else if (a[i] == 1)
					{
						m_oResult.append(StringUtil.right(s[i], l[i]));											
					}
				}
			}
			else //preserve 10 & 15 for number
			{
				m_oResult.append(StringUtil.right(s[i], l[i]));					
			}
		}
		m_oResult.append("\n");
		for(int i = 0; i < _total_length; i++ ) m_oResult.append("-");
		m_oResult.append("\n");

		m_lStart = System.currentTimeMillis();
	}

	protected void append(Object o, int l)
	{
		
		if (o != null)
		{	
			if(o instanceof String)
			{
				m_oResult.append(StringUtil.left(o.toString(), l));
			}
			else if (o instanceof Number || o instanceof BigDecimal)
			{
				m_oResult.append(StringUtil.right(CustomFormatter.formatAligned(o), l));
			}
		}
		else
		{
			m_oResult.append(StringUtil.left(m_iInvalidResult + ".", l));			
		}
	}

	protected void footer ()
	{
		m_oResult.append("\n");
		for(int i = 0; i < _total_length; i++ ) m_oResult.append("-");
		
		long lEnd = System.currentTimeMillis() - m_lStart;
		m_oResult.append("\n\nElapsed Time : ").append(lEnd).append(" ms");
		if (m_iInvalidResult == 0)
		{
			m_oResult.append("\n\nAll Result is valid");
		}
		else
		{
			m_oResult.append("\n\nThere are ")
			         .append(m_iInvalidResult)
					 .append(" invalid data, please check from details above");				
		}			
	}
	
	protected void handleError (Connection _oConn, Exception _oEx)
	{
		Transaction.safeRollback(_oConn);
		log.error (_oEx);
		_oEx.printStackTrace();
		m_oResult.append("\nERROR : " + _oEx.getMessage());	
	}
	
	protected void validateField (Object o, String[] fields, StringBuilder sb)
	{
		for (int i = 0; i < fields.length; i++)
		{
			String field = fields[i];
			try
			{
				Object val = BeanUtil.invokeGetter(o, field);
				if (val != null)
				{
					if (val instanceof String)
					{
						if (StringUtil.isEmpty(val.toString()))
						{
							if (StringUtil.isNotEmpty(sb.toString())) sb.append(",");
							sb.append(field);
						}
					}
					else if (val instanceof Number)
					{
						if (val instanceof Integer)
						{
							Integer nval = (Integer) val;
							if (nval.intValue() == 0 )
							{
								if (StringUtil.isNotEmpty(sb.toString())) sb.append(",");
								sb.append(field);							
							}
						}
						if (val instanceof BigDecimal)
						{
							BigDecimal nval = (BigDecimal) val;
							if (nval.intValue() == 0 )
							{
								if (StringUtil.isNotEmpty(sb.toString())) sb.append(",");
								sb.append(field);							
							}
						}
					}
				}
				else
				{
					if (StringUtil.isNotEmpty(sb.toString())) sb.append(",");
					sb.append(field);				
				}
			}
			catch (Exception _oEx)
			{
				_oEx.printStackTrace();
			}
		}
	}
}

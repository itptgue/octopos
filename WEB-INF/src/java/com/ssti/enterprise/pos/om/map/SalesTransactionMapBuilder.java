package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SalesTransactionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SalesTransactionMapBuilder";

    /**
     * Item
     * @deprecated use SalesTransactionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "sales_transaction";
    }

  
    /**
     * sales_transaction.SALES_TRANSACTION_ID
     * @return the column name for the SALES_TRANSACTION_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.SALES_TRANSACTION_ID constant
     */
    public static String getSalesTransaction_SalesTransactionId()
    {
        return "sales_transaction.SALES_TRANSACTION_ID";
    }
  
    /**
     * sales_transaction.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.LOCATION_ID constant
     */
    public static String getSalesTransaction_LocationId()
    {
        return "sales_transaction.LOCATION_ID";
    }
  
    /**
     * sales_transaction.STATUS
     * @return the column name for the STATUS field
     * @deprecated use SalesTransactionPeer.sales_transaction.STATUS constant
     */
    public static String getSalesTransaction_Status()
    {
        return "sales_transaction.STATUS";
    }
  
    /**
     * sales_transaction.INVOICE_NO
     * @return the column name for the INVOICE_NO field
     * @deprecated use SalesTransactionPeer.sales_transaction.INVOICE_NO constant
     */
    public static String getSalesTransaction_InvoiceNo()
    {
        return "sales_transaction.INVOICE_NO";
    }
  
    /**
     * sales_transaction.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use SalesTransactionPeer.sales_transaction.TRANSACTION_DATE constant
     */
    public static String getSalesTransaction_TransactionDate()
    {
        return "sales_transaction.TRANSACTION_DATE";
    }
  
    /**
     * sales_transaction.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use SalesTransactionPeer.sales_transaction.DUE_DATE constant
     */
    public static String getSalesTransaction_DueDate()
    {
        return "sales_transaction.DUE_DATE";
    }
  
    /**
     * sales_transaction.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.CUSTOMER_ID constant
     */
    public static String getSalesTransaction_CustomerId()
    {
        return "sales_transaction.CUSTOMER_ID";
    }
  
    /**
     * sales_transaction.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use SalesTransactionPeer.sales_transaction.CUSTOMER_NAME constant
     */
    public static String getSalesTransaction_CustomerName()
    {
        return "sales_transaction.CUSTOMER_NAME";
    }
  
    /**
     * sales_transaction.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_QTY constant
     */
    public static String getSalesTransaction_TotalQty()
    {
        return "sales_transaction.TOTAL_QTY";
    }
  
    /**
     * sales_transaction.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_AMOUNT constant
     */
    public static String getSalesTransaction_TotalAmount()
    {
        return "sales_transaction.TOTAL_AMOUNT";
    }
  
    /**
     * sales_transaction.TOTAL_DISCOUNT_ID
     * @return the column name for the TOTAL_DISCOUNT_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_DISCOUNT_ID constant
     */
    public static String getSalesTransaction_TotalDiscountId()
    {
        return "sales_transaction.TOTAL_DISCOUNT_ID";
    }
  
    /**
     * sales_transaction.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_DISCOUNT_PCT constant
     */
    public static String getSalesTransaction_TotalDiscountPct()
    {
        return "sales_transaction.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * sales_transaction.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_DISCOUNT constant
     */
    public static String getSalesTransaction_TotalDiscount()
    {
        return "sales_transaction.TOTAL_DISCOUNT";
    }
  
    /**
     * sales_transaction.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_TAX constant
     */
    public static String getSalesTransaction_TotalTax()
    {
        return "sales_transaction.TOTAL_TAX";
    }
  
    /**
     * sales_transaction.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.COURIER_ID constant
     */
    public static String getSalesTransaction_CourierId()
    {
        return "sales_transaction.COURIER_ID";
    }
  
    /**
     * sales_transaction.FREIGHT_NO
     * @return the column name for the FREIGHT_NO field
     * @deprecated use SalesTransactionPeer.sales_transaction.FREIGHT_NO constant
     */
    public static String getSalesTransaction_FreightNo()
    {
        return "sales_transaction.FREIGHT_NO";
    }
  
    /**
     * sales_transaction.SHIPPING_PRICE
     * @return the column name for the SHIPPING_PRICE field
     * @deprecated use SalesTransactionPeer.sales_transaction.SHIPPING_PRICE constant
     */
    public static String getSalesTransaction_ShippingPrice()
    {
        return "sales_transaction.SHIPPING_PRICE";
    }
  
    /**
     * sales_transaction.TOTAL_EXPENSE
     * @return the column name for the TOTAL_EXPENSE field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_EXPENSE constant
     */
    public static String getSalesTransaction_TotalExpense()
    {
        return "sales_transaction.TOTAL_EXPENSE";
    }
  
    /**
     * sales_transaction.TOTAL_COST
     * @return the column name for the TOTAL_COST field
     * @deprecated use SalesTransactionPeer.sales_transaction.TOTAL_COST constant
     */
    public static String getSalesTransaction_TotalCost()
    {
        return "sales_transaction.TOTAL_COST";
    }
  
    /**
     * sales_transaction.SALES_ID
     * @return the column name for the SALES_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.SALES_ID constant
     */
    public static String getSalesTransaction_SalesId()
    {
        return "sales_transaction.SALES_ID";
    }
  
    /**
     * sales_transaction.CASHIER_NAME
     * @return the column name for the CASHIER_NAME field
     * @deprecated use SalesTransactionPeer.sales_transaction.CASHIER_NAME constant
     */
    public static String getSalesTransaction_CashierName()
    {
        return "sales_transaction.CASHIER_NAME";
    }
  
    /**
     * sales_transaction.REMARK
     * @return the column name for the REMARK field
     * @deprecated use SalesTransactionPeer.sales_transaction.REMARK constant
     */
    public static String getSalesTransaction_Remark()
    {
        return "sales_transaction.REMARK";
    }
  
    /**
     * sales_transaction.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.PAYMENT_TYPE_ID constant
     */
    public static String getSalesTransaction_PaymentTypeId()
    {
        return "sales_transaction.PAYMENT_TYPE_ID";
    }
  
    /**
     * sales_transaction.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.PAYMENT_TERM_ID constant
     */
    public static String getSalesTransaction_PaymentTermId()
    {
        return "sales_transaction.PAYMENT_TERM_ID";
    }
  
    /**
     * sales_transaction.PRINT_TIMES
     * @return the column name for the PRINT_TIMES field
     * @deprecated use SalesTransactionPeer.sales_transaction.PRINT_TIMES constant
     */
    public static String getSalesTransaction_PrintTimes()
    {
        return "sales_transaction.PRINT_TIMES";
    }
  
    /**
     * sales_transaction.PAID_AMOUNT
     * @return the column name for the PAID_AMOUNT field
     * @deprecated use SalesTransactionPeer.sales_transaction.PAID_AMOUNT constant
     */
    public static String getSalesTransaction_PaidAmount()
    {
        return "sales_transaction.PAID_AMOUNT";
    }
  
    /**
     * sales_transaction.ROUNDING_AMOUNT
     * @return the column name for the ROUNDING_AMOUNT field
     * @deprecated use SalesTransactionPeer.sales_transaction.ROUNDING_AMOUNT constant
     */
    public static String getSalesTransaction_RoundingAmount()
    {
        return "sales_transaction.ROUNDING_AMOUNT";
    }
  
    /**
     * sales_transaction.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use SalesTransactionPeer.sales_transaction.PAYMENT_AMOUNT constant
     */
    public static String getSalesTransaction_PaymentAmount()
    {
        return "sales_transaction.PAYMENT_AMOUNT";
    }
  
    /**
     * sales_transaction.CHANGE_AMOUNT
     * @return the column name for the CHANGE_AMOUNT field
     * @deprecated use SalesTransactionPeer.sales_transaction.CHANGE_AMOUNT constant
     */
    public static String getSalesTransaction_ChangeAmount()
    {
        return "sales_transaction.CHANGE_AMOUNT";
    }
  
    /**
     * sales_transaction.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.CURRENCY_ID constant
     */
    public static String getSalesTransaction_CurrencyId()
    {
        return "sales_transaction.CURRENCY_ID";
    }
  
    /**
     * sales_transaction.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use SalesTransactionPeer.sales_transaction.CURRENCY_RATE constant
     */
    public static String getSalesTransaction_CurrencyRate()
    {
        return "sales_transaction.CURRENCY_RATE";
    }
  
    /**
     * sales_transaction.SHIP_TO
     * @return the column name for the SHIP_TO field
     * @deprecated use SalesTransactionPeer.sales_transaction.SHIP_TO constant
     */
    public static String getSalesTransaction_ShipTo()
    {
        return "sales_transaction.SHIP_TO";
    }
  
    /**
     * sales_transaction.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.FOB_ID constant
     */
    public static String getSalesTransaction_FobId()
    {
        return "sales_transaction.FOB_ID";
    }
  
    /**
     * sales_transaction.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use SalesTransactionPeer.sales_transaction.IS_TAXABLE constant
     */
    public static String getSalesTransaction_IsTaxable()
    {
        return "sales_transaction.IS_TAXABLE";
    }
  
    /**
     * sales_transaction.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use SalesTransactionPeer.sales_transaction.IS_INCLUSIVE_TAX constant
     */
    public static String getSalesTransaction_IsInclusiveTax()
    {
        return "sales_transaction.IS_INCLUSIVE_TAX";
    }
  
    /**
     * sales_transaction.PAYMENT_DATE
     * @return the column name for the PAYMENT_DATE field
     * @deprecated use SalesTransactionPeer.sales_transaction.PAYMENT_DATE constant
     */
    public static String getSalesTransaction_PaymentDate()
    {
        return "sales_transaction.PAYMENT_DATE";
    }
  
    /**
     * sales_transaction.PAYMENT_STATUS
     * @return the column name for the PAYMENT_STATUS field
     * @deprecated use SalesTransactionPeer.sales_transaction.PAYMENT_STATUS constant
     */
    public static String getSalesTransaction_PaymentStatus()
    {
        return "sales_transaction.PAYMENT_STATUS";
    }
  
    /**
     * sales_transaction.FREIGHT_ACCOUNT_ID
     * @return the column name for the FREIGHT_ACCOUNT_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.FREIGHT_ACCOUNT_ID constant
     */
    public static String getSalesTransaction_FreightAccountId()
    {
        return "sales_transaction.FREIGHT_ACCOUNT_ID";
    }
  
    /**
     * sales_transaction.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use SalesTransactionPeer.sales_transaction.FISCAL_RATE constant
     */
    public static String getSalesTransaction_FiscalRate()
    {
        return "sales_transaction.FISCAL_RATE";
    }
  
    /**
     * sales_transaction.SOURCE_TRANS_ID
     * @return the column name for the SOURCE_TRANS_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.SOURCE_TRANS_ID constant
     */
    public static String getSalesTransaction_SourceTransId()
    {
        return "sales_transaction.SOURCE_TRANS_ID";
    }
  
    /**
     * sales_transaction.INVOICE_DISC_ACC_ID
     * @return the column name for the INVOICE_DISC_ACC_ID field
     * @deprecated use SalesTransactionPeer.sales_transaction.INVOICE_DISC_ACC_ID constant
     */
    public static String getSalesTransaction_InvoiceDiscAccId()
    {
        return "sales_transaction.INVOICE_DISC_ACC_ID";
    }
  
    /**
     * sales_transaction.IS_INCLUSIVE_FREIGHT
     * @return the column name for the IS_INCLUSIVE_FREIGHT field
     * @deprecated use SalesTransactionPeer.sales_transaction.IS_INCLUSIVE_FREIGHT constant
     */
    public static String getSalesTransaction_IsInclusiveFreight()
    {
        return "sales_transaction.IS_INCLUSIVE_FREIGHT";
    }
  
    /**
     * sales_transaction.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use SalesTransactionPeer.sales_transaction.CANCEL_BY constant
     */
    public static String getSalesTransaction_CancelBy()
    {
        return "sales_transaction.CANCEL_BY";
    }
  
    /**
     * sales_transaction.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use SalesTransactionPeer.sales_transaction.CANCEL_DATE constant
     */
    public static String getSalesTransaction_CancelDate()
    {
        return "sales_transaction.CANCEL_DATE";
    }
  
    /**
     * sales_transaction.IS_POS_TRANS
     * @return the column name for the IS_POS_TRANS field
     * @deprecated use SalesTransactionPeer.sales_transaction.IS_POS_TRANS constant
     */
    public static String getSalesTransaction_IsPosTrans()
    {
        return "sales_transaction.IS_POS_TRANS";
    }
  
    /**
     * sales_transaction.DELIVERY_DATE
     * @return the column name for the DELIVERY_DATE field
     * @deprecated use SalesTransactionPeer.sales_transaction.DELIVERY_DATE constant
     */
    public static String getSalesTransaction_DeliveryDate()
    {
        return "sales_transaction.DELIVERY_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("sales_transaction");
        TableMap tMap = dbMap.getTable("sales_transaction");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("sales_transaction.SALES_TRANSACTION_ID", "");
                          tMap.addColumn("sales_transaction.LOCATION_ID", "");
                            tMap.addColumn("sales_transaction.STATUS", Integer.valueOf(0));
                          tMap.addColumn("sales_transaction.INVOICE_NO", "");
                          tMap.addColumn("sales_transaction.TRANSACTION_DATE", new Date());
                          tMap.addColumn("sales_transaction.DUE_DATE", new Date());
                          tMap.addColumn("sales_transaction.CUSTOMER_ID", "");
                          tMap.addColumn("sales_transaction.CUSTOMER_NAME", "");
                            tMap.addColumn("sales_transaction.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("sales_transaction.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("sales_transaction.TOTAL_DISCOUNT_ID", "");
                          tMap.addColumn("sales_transaction.TOTAL_DISCOUNT_PCT", "");
                            tMap.addColumn("sales_transaction.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("sales_transaction.TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("sales_transaction.COURIER_ID", "");
                          tMap.addColumn("sales_transaction.FREIGHT_NO", "");
                            tMap.addColumn("sales_transaction.SHIPPING_PRICE", bd_ZERO);
                            tMap.addColumn("sales_transaction.TOTAL_EXPENSE", bd_ZERO);
                            tMap.addColumn("sales_transaction.TOTAL_COST", bd_ZERO);
                          tMap.addColumn("sales_transaction.SALES_ID", "");
                          tMap.addColumn("sales_transaction.CASHIER_NAME", "");
                          tMap.addColumn("sales_transaction.REMARK", "");
                          tMap.addColumn("sales_transaction.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("sales_transaction.PAYMENT_TERM_ID", "");
                            tMap.addColumn("sales_transaction.PRINT_TIMES", Integer.valueOf(0));
                            tMap.addColumn("sales_transaction.PAID_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_transaction.ROUNDING_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_transaction.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_transaction.CHANGE_AMOUNT", bd_ZERO);
                          tMap.addColumn("sales_transaction.CURRENCY_ID", "");
                            tMap.addColumn("sales_transaction.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("sales_transaction.SHIP_TO", "");
                          tMap.addColumn("sales_transaction.FOB_ID", "");
                          tMap.addColumn("sales_transaction.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("sales_transaction.IS_INCLUSIVE_TAX", Boolean.TRUE);
                          tMap.addColumn("sales_transaction.PAYMENT_DATE", new Date());
                            tMap.addColumn("sales_transaction.PAYMENT_STATUS", Integer.valueOf(0));
                          tMap.addColumn("sales_transaction.FREIGHT_ACCOUNT_ID", "");
                            tMap.addColumn("sales_transaction.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("sales_transaction.SOURCE_TRANS_ID", "");
                          tMap.addColumn("sales_transaction.INVOICE_DISC_ACC_ID", "");
                          tMap.addColumn("sales_transaction.IS_INCLUSIVE_FREIGHT", Boolean.TRUE);
                          tMap.addColumn("sales_transaction.CANCEL_BY", "");
                          tMap.addColumn("sales_transaction.CANCEL_DATE", new Date());
                          tMap.addColumn("sales_transaction.IS_POS_TRANS", Boolean.TRUE);
                          tMap.addColumn("sales_transaction.DELIVERY_DATE", new Date());
          }
}

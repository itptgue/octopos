package com.ssti.enterprise.pos.tools.financial;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.om.BankTransfer;
import com.ssti.enterprise.pos.om.BankTransferPeer;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for Cash Flow and Cash Flow Journal OM 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BankTransferTool.java,v 1.33 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: BankTransferTool.java,v $
 * Revision 1.33  2009/05/04 02:04:13  albert
 * *** empty log message ***
 *
 * Revision 1.32  2008/10/22 05:49:42  albert
 * *** empty log message ***
 *
 * Revision 1.31  2008/06/29 07:12:27  albert
 * *** empty log message ***
 *
 * </pre><br>
 */
public class BankTransferTool extends BaseTool
{
    private static Log log = LogFactory.getLog(BankTransferTool.class);
    
	protected static Map m_FIND_PEER = new HashMap();
	
    public static final String s_CTX= "banktrf";
    
	static BankTransferTool instance = null;	
	public static synchronized BankTransferTool getInstance() 
	{
		if (instance == null) instance = new BankTransferTool();
		return instance;
	}

    static
    {   
        m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), BankTransferPeer.TRANS_NO);          
        m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), BankTransferPeer.DESCRIPTION);
        m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), BankTransferPeer.USER_NAME); 
        m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), BankTransferPeer.CURRENCY_ID);
    }   

    public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}    

	//static methods
	public static BankTransfer getHeaderByID(String _sID)
    	throws Exception
    {
		return getHeaderByID (_sID, null);
	}

	public static BankTransfer getHeaderByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(BankTransferPeer.BANK_TRANSFER_ID, _sID);
	    List vData = BankTransferPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) 
	    {
			return (BankTransfer) vData.get(0);
		}
		return null;
	}

    /**
     * @param _oBT
     * @throws Exception
     */
    public static void saveData (BankTransfer _oBT, Connection _oConn)
        throws Exception
    {
        boolean bNew = false;
        boolean bStartTrans = false;
        if (_oConn == null) 
        {
            _oConn = beginTrans();
            bStartTrans = true;
        }
        try 
        {
            if(StringUtil.isEmpty(_oBT.getBankTransferId())) bNew = true;     
            
            //validate date
            validateDate(_oBT.getTransDate(), _oConn);
                        
            //process save Data
            if (StringUtil.isEmpty(_oBT.getBankTransferId()))
            {
                _oBT.setBankTransferId (IDGenerator.generateSysID());
                _oBT.setNew (true);
                validateID(getHeaderByID(_oBT.getBankTransferId(), _oConn), "transNo");
            }
            if(StringUtil.isEmpty(_oBT.getTransNo())) _oBT.setTransNo("");                
            if(StringUtil.isEmpty(_oBT.getOutCfId())) _oBT.setOutCfId("");                
            if(StringUtil.isEmpty(_oBT.getInCfId()))  _oBT.setInCfId("");                                
            
            if (_oBT.getStatus() == i_PROCESSED)
            {
                String sLocCode = LocationTool.getLocationCodeByID(_oBT.getLocationId(), _oConn);
                _oBT.setTransNo (LastNumberTool.get(s_BT_FORMAT, LastNumberTool.i_BANK_TRANSFER, sLocCode, _oConn));
                validateNo(BankTransferPeer.TRANS_NO,_oBT.getTransNo(),BankTransferPeer.class, _oConn);
            }
            _oBT.save (_oConn);
            
            if (_oBT.getStatus() == i_PROCESSED)
            {
                //create cash flow entry            
                CashFlow oOutCF = CashFlowTool.createFromTransfer(_oBT, i_WITHDRAWAL, _oConn);

                //create cash flow entry            
                CashFlow oInCF = CashFlowTool.createFromTransfer(_oBT, i_DEPOSIT, _oConn);

                //update ARPayment
                _oBT.setOutCfId(oOutCF.getCashFlowId());
                _oBT.setInCfId(oInCF.getCashFlowId());
                _oBT.save(_oConn);
            }
            if (bStartTrans) 
            {
                commit (_oConn);
            }
        }
        catch (Exception _oEx) 
        {
            if (_oBT.getStatus() == i_PROCESSED)
            {
                _oBT.setStatus(i_PENDING);
                _oBT.setTransNo("");
            }
            if (bNew)
            {
                _oBT.setBankTransferId("");               
            }
            if (bStartTrans) 
            {
                rollback (_oConn);
            }
            log.error(_oEx);
            _oEx.printStackTrace();
            throw new NestableException (_oEx.getMessage(), _oEx);
        }
    }

    /**
     * @param _oBT
     * @throws Exception
     */
    public static void cancelTrans (BankTransfer _oBT, String _sUserName)
        throws Exception
    {
        boolean bNew = false;
        boolean bStartTrans = true;
        Connection oConn = null;
        try 
        {
            oConn = beginTrans();
            
            //validate date
            validateDate(_oBT.getTransDate(), oConn);
            
            _oBT.setCancelBy(_sUserName);
            _oBT.setCancelDate(new Date());
            _oBT.setStatus(i_CANCELLED);
            _oBT.setDescription(cancelledBy(_oBT.getDescription(), _sUserName));
            _oBT.save(oConn);

            //cancel cash flow entry            
            CashFlow oOut = CashFlowTool.getHeaderByID(_oBT.getOutCfId(), oConn);
            CashFlowTool.cancelCashFlow(oOut, _sUserName, oConn);
            
            //create cash flow entry            
            CashFlow oIn = CashFlowTool.getHeaderByID(_oBT.getInCfId(), oConn);
            CashFlowTool.cancelCashFlow(oIn, _sUserName, oConn);
            
            commit(oConn);
        }
        catch (Exception _oEx) 
        {
            rollback(oConn);
            log.error(_oEx);
            _oEx.printStackTrace();
            throw new NestableException (_oEx.getMessage(), _oEx);
        }
    }
    
    //-------------------------------------------------------------------------
    //finder
    //-------------------------------------------------------------------------
    private static Criteria buildCriteria(int _iCond, 
                                          String _sKeywords, 
                                          String _sBankID,
                                          String _sLocationID,
                                          int _iStatus,
                                          Date _dStart, 
                                          Date _dEnd, 
                                          String _sCurrencyID) 
    throws Exception
    {
        Criteria oCrit =  buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
                BankTransferPeer.TRANS_DATE, _dStart, _dEnd, _sCurrencyID);
        
        if (StringUtil.isNotEmpty(_sBankID))
        {
            oCrit.add(BankTransferPeer.OUT_BANK_ID, _sBankID);
        }
        if (StringUtil.isNotEmpty(_sLocationID))
        {
            oCrit.add(BankTransferPeer.OUT_LOC_ID, _sLocationID);
        }       
        if (_iStatus != 0)
        {
            oCrit.add(BankTransferPeer.STATUS, _iStatus);
        }       
        oCrit.addAscendingOrderByColumn (BankTransferPeer.TRANS_DATE);
        return oCrit;
    }
    
    public static LargeSelect findData(int _iCond, 
                                       String _sKeywords, 
                                       String _sBankID, 
                                       String _sLocID,
                                       int _iStatus,
                                       Date _dStart, 
                                       Date _dEnd,
                                       int _iLimit)
        throws Exception
    {
        Criteria oCrit = buildCriteria(_iCond, _sKeywords, _sBankID, _sLocID, _iStatus, _dStart, _dEnd, "");
        return new LargeSelect(oCrit,_iLimit,"com.ssti.enterprise.pos.om.BankTransferPeer");
    }
    
    //next prev button util
    public static String getPrevOrNextID(String _sID, boolean _bIsNext)
        throws Exception
    {
        return getPrevOrNextID(BankTransferPeer.class, BankTransferPeer.BANK_TRANSFER_ID, _sID, _bIsNext);
    }   
}
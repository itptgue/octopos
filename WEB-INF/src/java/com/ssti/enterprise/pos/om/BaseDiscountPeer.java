package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.DiscountMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseDiscountPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "discount";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(DiscountMapBuilder.CLASS_NAME);
    }

      /** the column name for the DISCOUNT_ID field */
    public static final String DISCOUNT_ID;
      /** the column name for the PROMO_CODE field */
    public static final String PROMO_CODE;
      /** the column name for the DISCOUNT_CODE field */
    public static final String DISCOUNT_CODE;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the ITEM_ID field */
    public static final String ITEM_ID;
      /** the column name for the ITEM_SKU field */
    public static final String ITEM_SKU;
      /** the column name for the KATEGORI_ID field */
    public static final String KATEGORI_ID;
      /** the column name for the CUSTOMER_TYPE_ID field */
    public static final String CUSTOMER_TYPE_ID;
      /** the column name for the CUSTOMERS field */
    public static final String CUSTOMERS;
      /** the column name for the ITEMS field */
    public static final String ITEMS;
      /** the column name for the BRANDS field */
    public static final String BRANDS;
      /** the column name for the MANUFACTURERS field */
    public static final String MANUFACTURERS;
      /** the column name for the TAGS field */
    public static final String TAGS;
      /** the column name for the PAYMENT_TYPE_ID field */
    public static final String PAYMENT_TYPE_ID;
      /** the column name for the CARD_PREFIX field */
    public static final String CARD_PREFIX;
      /** the column name for the DISCOUNT_DAYS field */
    public static final String DISCOUNT_DAYS;
      /** the column name for the TOTAL_DISC_SCRIPT field */
    public static final String TOTAL_DISC_SCRIPT;
      /** the column name for the IS_TOTAL_DISC field */
    public static final String IS_TOTAL_DISC;
      /** the column name for the HAPPY_HOUR field */
    public static final String HAPPY_HOUR;
      /** the column name for the START_HOUR field */
    public static final String START_HOUR;
      /** the column name for the END_HOUR field */
    public static final String END_HOUR;
      /** the column name for the DISCOUNT_TYPE field */
    public static final String DISCOUNT_TYPE;
      /** the column name for the QTY_AMOUNT_1 field */
    public static final String QTY_AMOUNT_1;
      /** the column name for the QTY_AMOUNT_2 field */
    public static final String QTY_AMOUNT_2;
      /** the column name for the QTY_AMOUNT_3 field */
    public static final String QTY_AMOUNT_3;
      /** the column name for the QTY_AMOUNT_4 field */
    public static final String QTY_AMOUNT_4;
      /** the column name for the QTY_AMOUNT_5 field */
    public static final String QTY_AMOUNT_5;
      /** the column name for the DISCOUNT_VALUE_1 field */
    public static final String DISCOUNT_VALUE_1;
      /** the column name for the DISCOUNT_VALUE_2 field */
    public static final String DISCOUNT_VALUE_2;
      /** the column name for the DISCOUNT_VALUE_3 field */
    public static final String DISCOUNT_VALUE_3;
      /** the column name for the DISCOUNT_VALUE_4 field */
    public static final String DISCOUNT_VALUE_4;
      /** the column name for the DISCOUNT_VALUE_5 field */
    public static final String DISCOUNT_VALUE_5;
      /** the column name for the EVENT_BEGIN_DATE field */
    public static final String EVENT_BEGIN_DATE;
      /** the column name for the EVENT_END_DATE field */
    public static final String EVENT_END_DATE;
      /** the column name for the EVENT_DISCOUNT field */
    public static final String EVENT_DISCOUNT;
      /** the column name for the PER_ITEM field */
    public static final String PER_ITEM;
      /** the column name for the MULTIPLY field */
    public static final String MULTIPLY;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the UPDATE_DATE field */
    public static final String UPDATE_DATE;
      /** the column name for the LAST_UPDATE_BY field */
    public static final String LAST_UPDATE_BY;
      /** the column name for the BUY_QTY field */
    public static final String BUY_QTY;
      /** the column name for the GET_QTY field */
    public static final String GET_QTY;
      /** the column name for the IS_CC_DISC field */
    public static final String IS_CC_DISC;
      /** the column name for the CC_DISC_COMP field */
    public static final String CC_DISC_COMP;
      /** the column name for the CC_DISC_BANK field */
    public static final String CC_DISC_BANK;
      /** the column name for the CC_MIN_PURCH field */
    public static final String CC_MIN_PURCH;
      /** the column name for the COUPON_TYPE field */
    public static final String COUPON_TYPE;
      /** the column name for the COUPON_DESC field */
    public static final String COUPON_DESC;
      /** the column name for the COUPON_PURCH_AMT field */
    public static final String COUPON_PURCH_AMT;
      /** the column name for the COUPON_VALUE field */
    public static final String COUPON_VALUE;
      /** the column name for the COUPON_QTY field */
    public static final String COUPON_QTY;
      /** the column name for the COUPON_MESSAGE field */
    public static final String COUPON_MESSAGE;
      /** the column name for the COUPON_VALID_DAYS field */
    public static final String COUPON_VALID_DAYS;
      /** the column name for the COUPON_VALID_FROM field */
    public static final String COUPON_VALID_FROM;
      /** the column name for the COUPON_VALID_TO field */
    public static final String COUPON_VALID_TO;
      /** the column name for the DISC_SCRIPT field */
    public static final String DISC_SCRIPT;
  
    static
    {
          DISCOUNT_ID = "discount.DISCOUNT_ID";
          PROMO_CODE = "discount.PROMO_CODE";
          DISCOUNT_CODE = "discount.DISCOUNT_CODE";
          LOCATION_ID = "discount.LOCATION_ID";
          ITEM_ID = "discount.ITEM_ID";
          ITEM_SKU = "discount.ITEM_SKU";
          KATEGORI_ID = "discount.KATEGORI_ID";
          CUSTOMER_TYPE_ID = "discount.CUSTOMER_TYPE_ID";
          CUSTOMERS = "discount.CUSTOMERS";
          ITEMS = "discount.ITEMS";
          BRANDS = "discount.BRANDS";
          MANUFACTURERS = "discount.MANUFACTURERS";
          TAGS = "discount.TAGS";
          PAYMENT_TYPE_ID = "discount.PAYMENT_TYPE_ID";
          CARD_PREFIX = "discount.CARD_PREFIX";
          DISCOUNT_DAYS = "discount.DISCOUNT_DAYS";
          TOTAL_DISC_SCRIPT = "discount.TOTAL_DISC_SCRIPT";
          IS_TOTAL_DISC = "discount.IS_TOTAL_DISC";
          HAPPY_HOUR = "discount.HAPPY_HOUR";
          START_HOUR = "discount.START_HOUR";
          END_HOUR = "discount.END_HOUR";
          DISCOUNT_TYPE = "discount.DISCOUNT_TYPE";
          QTY_AMOUNT_1 = "discount.QTY_AMOUNT_1";
          QTY_AMOUNT_2 = "discount.QTY_AMOUNT_2";
          QTY_AMOUNT_3 = "discount.QTY_AMOUNT_3";
          QTY_AMOUNT_4 = "discount.QTY_AMOUNT_4";
          QTY_AMOUNT_5 = "discount.QTY_AMOUNT_5";
          DISCOUNT_VALUE_1 = "discount.DISCOUNT_VALUE_1";
          DISCOUNT_VALUE_2 = "discount.DISCOUNT_VALUE_2";
          DISCOUNT_VALUE_3 = "discount.DISCOUNT_VALUE_3";
          DISCOUNT_VALUE_4 = "discount.DISCOUNT_VALUE_4";
          DISCOUNT_VALUE_5 = "discount.DISCOUNT_VALUE_5";
          EVENT_BEGIN_DATE = "discount.EVENT_BEGIN_DATE";
          EVENT_END_DATE = "discount.EVENT_END_DATE";
          EVENT_DISCOUNT = "discount.EVENT_DISCOUNT";
          PER_ITEM = "discount.PER_ITEM";
          MULTIPLY = "discount.MULTIPLY";
          DESCRIPTION = "discount.DESCRIPTION";
          UPDATE_DATE = "discount.UPDATE_DATE";
          LAST_UPDATE_BY = "discount.LAST_UPDATE_BY";
          BUY_QTY = "discount.BUY_QTY";
          GET_QTY = "discount.GET_QTY";
          IS_CC_DISC = "discount.IS_CC_DISC";
          CC_DISC_COMP = "discount.CC_DISC_COMP";
          CC_DISC_BANK = "discount.CC_DISC_BANK";
          CC_MIN_PURCH = "discount.CC_MIN_PURCH";
          COUPON_TYPE = "discount.COUPON_TYPE";
          COUPON_DESC = "discount.COUPON_DESC";
          COUPON_PURCH_AMT = "discount.COUPON_PURCH_AMT";
          COUPON_VALUE = "discount.COUPON_VALUE";
          COUPON_QTY = "discount.COUPON_QTY";
          COUPON_MESSAGE = "discount.COUPON_MESSAGE";
          COUPON_VALID_DAYS = "discount.COUPON_VALID_DAYS";
          COUPON_VALID_FROM = "discount.COUPON_VALID_FROM";
          COUPON_VALID_TO = "discount.COUPON_VALID_TO";
          DISC_SCRIPT = "discount.DISC_SCRIPT";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(DiscountMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(DiscountMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  56;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Discount";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseDiscountPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TOTAL_DISC))
        {
            Object possibleBoolean = criteria.get(IS_TOTAL_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TOTAL_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAPPY_HOUR))
        {
            Object possibleBoolean = criteria.get(HAPPY_HOUR);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAPPY_HOUR, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(PER_ITEM))
        {
            Object possibleBoolean = criteria.get(PER_ITEM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(PER_ITEM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(MULTIPLY))
        {
            Object possibleBoolean = criteria.get(MULTIPLY);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(MULTIPLY, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CC_DISC))
        {
            Object possibleBoolean = criteria.get(IS_CC_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CC_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(DISCOUNT_ID);
          criteria.addSelectColumn(PROMO_CODE);
          criteria.addSelectColumn(DISCOUNT_CODE);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(ITEM_ID);
          criteria.addSelectColumn(ITEM_SKU);
          criteria.addSelectColumn(KATEGORI_ID);
          criteria.addSelectColumn(CUSTOMER_TYPE_ID);
          criteria.addSelectColumn(CUSTOMERS);
          criteria.addSelectColumn(ITEMS);
          criteria.addSelectColumn(BRANDS);
          criteria.addSelectColumn(MANUFACTURERS);
          criteria.addSelectColumn(TAGS);
          criteria.addSelectColumn(PAYMENT_TYPE_ID);
          criteria.addSelectColumn(CARD_PREFIX);
          criteria.addSelectColumn(DISCOUNT_DAYS);
          criteria.addSelectColumn(TOTAL_DISC_SCRIPT);
          criteria.addSelectColumn(IS_TOTAL_DISC);
          criteria.addSelectColumn(HAPPY_HOUR);
          criteria.addSelectColumn(START_HOUR);
          criteria.addSelectColumn(END_HOUR);
          criteria.addSelectColumn(DISCOUNT_TYPE);
          criteria.addSelectColumn(QTY_AMOUNT_1);
          criteria.addSelectColumn(QTY_AMOUNT_2);
          criteria.addSelectColumn(QTY_AMOUNT_3);
          criteria.addSelectColumn(QTY_AMOUNT_4);
          criteria.addSelectColumn(QTY_AMOUNT_5);
          criteria.addSelectColumn(DISCOUNT_VALUE_1);
          criteria.addSelectColumn(DISCOUNT_VALUE_2);
          criteria.addSelectColumn(DISCOUNT_VALUE_3);
          criteria.addSelectColumn(DISCOUNT_VALUE_4);
          criteria.addSelectColumn(DISCOUNT_VALUE_5);
          criteria.addSelectColumn(EVENT_BEGIN_DATE);
          criteria.addSelectColumn(EVENT_END_DATE);
          criteria.addSelectColumn(EVENT_DISCOUNT);
          criteria.addSelectColumn(PER_ITEM);
          criteria.addSelectColumn(MULTIPLY);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(UPDATE_DATE);
          criteria.addSelectColumn(LAST_UPDATE_BY);
          criteria.addSelectColumn(BUY_QTY);
          criteria.addSelectColumn(GET_QTY);
          criteria.addSelectColumn(IS_CC_DISC);
          criteria.addSelectColumn(CC_DISC_COMP);
          criteria.addSelectColumn(CC_DISC_BANK);
          criteria.addSelectColumn(CC_MIN_PURCH);
          criteria.addSelectColumn(COUPON_TYPE);
          criteria.addSelectColumn(COUPON_DESC);
          criteria.addSelectColumn(COUPON_PURCH_AMT);
          criteria.addSelectColumn(COUPON_VALUE);
          criteria.addSelectColumn(COUPON_QTY);
          criteria.addSelectColumn(COUPON_MESSAGE);
          criteria.addSelectColumn(COUPON_VALID_DAYS);
          criteria.addSelectColumn(COUPON_VALID_FROM);
          criteria.addSelectColumn(COUPON_VALID_TO);
          criteria.addSelectColumn(DISC_SCRIPT);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Discount row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Discount obj = (Discount) cls.newInstance();
            DiscountPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Discount obj)
        throws TorqueException
    {
        try
        {
                obj.setDiscountId(row.getValue(offset + 0).asString());
                  obj.setPromoCode(row.getValue(offset + 1).asString());
                  obj.setDiscountCode(row.getValue(offset + 2).asString());
                  obj.setLocationId(row.getValue(offset + 3).asString());
                  obj.setItemId(row.getValue(offset + 4).asString());
                  obj.setItemSku(row.getValue(offset + 5).asString());
                  obj.setKategoriId(row.getValue(offset + 6).asString());
                  obj.setCustomerTypeId(row.getValue(offset + 7).asString());
                  obj.setCustomers(row.getValue(offset + 8).asString());
                  obj.setItems(row.getValue(offset + 9).asString());
                  obj.setBrands(row.getValue(offset + 10).asString());
                  obj.setManufacturers(row.getValue(offset + 11).asString());
                  obj.setTags(row.getValue(offset + 12).asString());
                  obj.setPaymentTypeId(row.getValue(offset + 13).asString());
                  obj.setCardPrefix(row.getValue(offset + 14).asString());
                  obj.setDiscountDays(row.getValue(offset + 15).asString());
                  obj.setTotalDiscScript(row.getValue(offset + 16).asString());
                  obj.setIsTotalDisc(row.getValue(offset + 17).asBoolean());
                  obj.setHappyHour(row.getValue(offset + 18).asBoolean());
                  obj.setStartHour(row.getValue(offset + 19).asString());
                  obj.setEndHour(row.getValue(offset + 20).asString());
                  obj.setDiscountType(row.getValue(offset + 21).asInt());
                  obj.setQtyAmount1(row.getValue(offset + 22).asBigDecimal());
                  obj.setQtyAmount2(row.getValue(offset + 23).asBigDecimal());
                  obj.setQtyAmount3(row.getValue(offset + 24).asBigDecimal());
                  obj.setQtyAmount4(row.getValue(offset + 25).asBigDecimal());
                  obj.setQtyAmount5(row.getValue(offset + 26).asBigDecimal());
                  obj.setDiscountValue1(row.getValue(offset + 27).asString());
                  obj.setDiscountValue2(row.getValue(offset + 28).asString());
                  obj.setDiscountValue3(row.getValue(offset + 29).asString());
                  obj.setDiscountValue4(row.getValue(offset + 30).asString());
                  obj.setDiscountValue5(row.getValue(offset + 31).asString());
                  obj.setEventBeginDate(row.getValue(offset + 32).asUtilDate());
                  obj.setEventEndDate(row.getValue(offset + 33).asUtilDate());
                  obj.setEventDiscount(row.getValue(offset + 34).asString());
                  obj.setPerItem(row.getValue(offset + 35).asBoolean());
                  obj.setMultiply(row.getValue(offset + 36).asBoolean());
                  obj.setDescription(row.getValue(offset + 37).asString());
                  obj.setUpdateDate(row.getValue(offset + 38).asUtilDate());
                  obj.setLastUpdateBy(row.getValue(offset + 39).asString());
                  obj.setBuyQty(row.getValue(offset + 40).asBigDecimal());
                  obj.setGetQty(row.getValue(offset + 41).asBigDecimal());
                  obj.setIsCcDisc(row.getValue(offset + 42).asBoolean());
                  obj.setCcDiscComp(row.getValue(offset + 43).asBigDecimal());
                  obj.setCcDiscBank(row.getValue(offset + 44).asBigDecimal());
                  obj.setCcMinPurch(row.getValue(offset + 45).asBigDecimal());
                  obj.setCouponType(row.getValue(offset + 46).asInt());
                  obj.setCouponDesc(row.getValue(offset + 47).asString());
                  obj.setCouponPurchAmt(row.getValue(offset + 48).asBigDecimal());
                  obj.setCouponValue(row.getValue(offset + 49).asBigDecimal());
                  obj.setCouponQty(row.getValue(offset + 50).asInt());
                  obj.setCouponMessage(row.getValue(offset + 51).asString());
                  obj.setCouponValidDays(row.getValue(offset + 52).asInt());
                  obj.setCouponValidFrom(row.getValue(offset + 53).asUtilDate());
                  obj.setCouponValidTo(row.getValue(offset + 54).asUtilDate());
                  obj.setDiscScript(row.getValue(offset + 55).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseDiscountPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TOTAL_DISC))
        {
            Object possibleBoolean = criteria.get(IS_TOTAL_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TOTAL_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAPPY_HOUR))
        {
            Object possibleBoolean = criteria.get(HAPPY_HOUR);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAPPY_HOUR, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(PER_ITEM))
        {
            Object possibleBoolean = criteria.get(PER_ITEM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(PER_ITEM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(MULTIPLY))
        {
            Object possibleBoolean = criteria.get(MULTIPLY);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(MULTIPLY, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CC_DISC))
        {
            Object possibleBoolean = criteria.get(IS_CC_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CC_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                    
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(DiscountPeer.row2Object(row, 1,
                DiscountPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseDiscountPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(DISCOUNT_ID, criteria.remove(DISCOUNT_ID));
                                                                                                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TOTAL_DISC))
        {
            Object possibleBoolean = criteria.get(IS_TOTAL_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TOTAL_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAPPY_HOUR))
        {
            Object possibleBoolean = criteria.get(HAPPY_HOUR);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAPPY_HOUR, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(PER_ITEM))
        {
            Object possibleBoolean = criteria.get(PER_ITEM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(PER_ITEM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(MULTIPLY))
        {
            Object possibleBoolean = criteria.get(MULTIPLY);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(MULTIPLY, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CC_DISC))
        {
            Object possibleBoolean = criteria.get(IS_CC_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CC_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         DiscountPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TOTAL_DISC))
        {
            Object possibleBoolean = criteria.get(IS_TOTAL_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TOTAL_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(HAPPY_HOUR))
        {
            Object possibleBoolean = criteria.get(HAPPY_HOUR);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(HAPPY_HOUR, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(PER_ITEM))
        {
            Object possibleBoolean = criteria.get(PER_ITEM);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(PER_ITEM, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(MULTIPLY))
        {
            Object possibleBoolean = criteria.get(MULTIPLY);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(MULTIPLY, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_CC_DISC))
        {
            Object possibleBoolean = criteria.get(IS_CC_DISC);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_CC_DISC, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Discount obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Discount obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Discount obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Discount obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Discount) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Discount obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Discount) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Discount obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Discount) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Discount obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseDiscountPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(DISCOUNT_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Discount obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(DISCOUNT_ID, obj.getDiscountId());
              criteria.add(PROMO_CODE, obj.getPromoCode());
              criteria.add(DISCOUNT_CODE, obj.getDiscountCode());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(ITEM_ID, obj.getItemId());
              criteria.add(ITEM_SKU, obj.getItemSku());
              criteria.add(KATEGORI_ID, obj.getKategoriId());
              criteria.add(CUSTOMER_TYPE_ID, obj.getCustomerTypeId());
              criteria.add(CUSTOMERS, obj.getCustomers());
              criteria.add(ITEMS, obj.getItems());
              criteria.add(BRANDS, obj.getBrands());
              criteria.add(MANUFACTURERS, obj.getManufacturers());
              criteria.add(TAGS, obj.getTags());
              criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
              criteria.add(CARD_PREFIX, obj.getCardPrefix());
              criteria.add(DISCOUNT_DAYS, obj.getDiscountDays());
              criteria.add(TOTAL_DISC_SCRIPT, obj.getTotalDiscScript());
              criteria.add(IS_TOTAL_DISC, obj.getIsTotalDisc());
              criteria.add(HAPPY_HOUR, obj.getHappyHour());
              criteria.add(START_HOUR, obj.getStartHour());
              criteria.add(END_HOUR, obj.getEndHour());
              criteria.add(DISCOUNT_TYPE, obj.getDiscountType());
              criteria.add(QTY_AMOUNT_1, obj.getQtyAmount1());
              criteria.add(QTY_AMOUNT_2, obj.getQtyAmount2());
              criteria.add(QTY_AMOUNT_3, obj.getQtyAmount3());
              criteria.add(QTY_AMOUNT_4, obj.getQtyAmount4());
              criteria.add(QTY_AMOUNT_5, obj.getQtyAmount5());
              criteria.add(DISCOUNT_VALUE_1, obj.getDiscountValue1());
              criteria.add(DISCOUNT_VALUE_2, obj.getDiscountValue2());
              criteria.add(DISCOUNT_VALUE_3, obj.getDiscountValue3());
              criteria.add(DISCOUNT_VALUE_4, obj.getDiscountValue4());
              criteria.add(DISCOUNT_VALUE_5, obj.getDiscountValue5());
              criteria.add(EVENT_BEGIN_DATE, obj.getEventBeginDate());
              criteria.add(EVENT_END_DATE, obj.getEventEndDate());
              criteria.add(EVENT_DISCOUNT, obj.getEventDiscount());
              criteria.add(PER_ITEM, obj.getPerItem());
              criteria.add(MULTIPLY, obj.getMultiply());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(UPDATE_DATE, obj.getUpdateDate());
              criteria.add(LAST_UPDATE_BY, obj.getLastUpdateBy());
              criteria.add(BUY_QTY, obj.getBuyQty());
              criteria.add(GET_QTY, obj.getGetQty());
              criteria.add(IS_CC_DISC, obj.getIsCcDisc());
              criteria.add(CC_DISC_COMP, obj.getCcDiscComp());
              criteria.add(CC_DISC_BANK, obj.getCcDiscBank());
              criteria.add(CC_MIN_PURCH, obj.getCcMinPurch());
              criteria.add(COUPON_TYPE, obj.getCouponType());
              criteria.add(COUPON_DESC, obj.getCouponDesc());
              criteria.add(COUPON_PURCH_AMT, obj.getCouponPurchAmt());
              criteria.add(COUPON_VALUE, obj.getCouponValue());
              criteria.add(COUPON_QTY, obj.getCouponQty());
              criteria.add(COUPON_MESSAGE, obj.getCouponMessage());
              criteria.add(COUPON_VALID_DAYS, obj.getCouponValidDays());
              criteria.add(COUPON_VALID_FROM, obj.getCouponValidFrom());
              criteria.add(COUPON_VALID_TO, obj.getCouponValidTo());
              criteria.add(DISC_SCRIPT, obj.getDiscScript());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Discount obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(DISCOUNT_ID, obj.getDiscountId());
                          criteria.add(PROMO_CODE, obj.getPromoCode());
                          criteria.add(DISCOUNT_CODE, obj.getDiscountCode());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(ITEM_ID, obj.getItemId());
                          criteria.add(ITEM_SKU, obj.getItemSku());
                          criteria.add(KATEGORI_ID, obj.getKategoriId());
                          criteria.add(CUSTOMER_TYPE_ID, obj.getCustomerTypeId());
                          criteria.add(CUSTOMERS, obj.getCustomers());
                          criteria.add(ITEMS, obj.getItems());
                          criteria.add(BRANDS, obj.getBrands());
                          criteria.add(MANUFACTURERS, obj.getManufacturers());
                          criteria.add(TAGS, obj.getTags());
                          criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
                          criteria.add(CARD_PREFIX, obj.getCardPrefix());
                          criteria.add(DISCOUNT_DAYS, obj.getDiscountDays());
                          criteria.add(TOTAL_DISC_SCRIPT, obj.getTotalDiscScript());
                          criteria.add(IS_TOTAL_DISC, obj.getIsTotalDisc());
                          criteria.add(HAPPY_HOUR, obj.getHappyHour());
                          criteria.add(START_HOUR, obj.getStartHour());
                          criteria.add(END_HOUR, obj.getEndHour());
                          criteria.add(DISCOUNT_TYPE, obj.getDiscountType());
                          criteria.add(QTY_AMOUNT_1, obj.getQtyAmount1());
                          criteria.add(QTY_AMOUNT_2, obj.getQtyAmount2());
                          criteria.add(QTY_AMOUNT_3, obj.getQtyAmount3());
                          criteria.add(QTY_AMOUNT_4, obj.getQtyAmount4());
                          criteria.add(QTY_AMOUNT_5, obj.getQtyAmount5());
                          criteria.add(DISCOUNT_VALUE_1, obj.getDiscountValue1());
                          criteria.add(DISCOUNT_VALUE_2, obj.getDiscountValue2());
                          criteria.add(DISCOUNT_VALUE_3, obj.getDiscountValue3());
                          criteria.add(DISCOUNT_VALUE_4, obj.getDiscountValue4());
                          criteria.add(DISCOUNT_VALUE_5, obj.getDiscountValue5());
                          criteria.add(EVENT_BEGIN_DATE, obj.getEventBeginDate());
                          criteria.add(EVENT_END_DATE, obj.getEventEndDate());
                          criteria.add(EVENT_DISCOUNT, obj.getEventDiscount());
                          criteria.add(PER_ITEM, obj.getPerItem());
                          criteria.add(MULTIPLY, obj.getMultiply());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(UPDATE_DATE, obj.getUpdateDate());
                          criteria.add(LAST_UPDATE_BY, obj.getLastUpdateBy());
                          criteria.add(BUY_QTY, obj.getBuyQty());
                          criteria.add(GET_QTY, obj.getGetQty());
                          criteria.add(IS_CC_DISC, obj.getIsCcDisc());
                          criteria.add(CC_DISC_COMP, obj.getCcDiscComp());
                          criteria.add(CC_DISC_BANK, obj.getCcDiscBank());
                          criteria.add(CC_MIN_PURCH, obj.getCcMinPurch());
                          criteria.add(COUPON_TYPE, obj.getCouponType());
                          criteria.add(COUPON_DESC, obj.getCouponDesc());
                          criteria.add(COUPON_PURCH_AMT, obj.getCouponPurchAmt());
                          criteria.add(COUPON_VALUE, obj.getCouponValue());
                          criteria.add(COUPON_QTY, obj.getCouponQty());
                          criteria.add(COUPON_MESSAGE, obj.getCouponMessage());
                          criteria.add(COUPON_VALID_DAYS, obj.getCouponValidDays());
                          criteria.add(COUPON_VALID_FROM, obj.getCouponValidFrom());
                          criteria.add(COUPON_VALID_TO, obj.getCouponValidTo());
                          criteria.add(DISC_SCRIPT, obj.getDiscScript());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Discount retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Discount retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Discount retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Discount retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Discount retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Discount)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( DISCOUNT_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Tag
 */
public abstract class BaseTag extends BaseObject
{
    /** The Peer class */
    private static final TagPeer peer =
        new TagPeer();

        
    /** The value for the tagId field */
    private String tagId;
                                                
    /** The value for the tagName field */
    private String tagName = "";
                                                
    /** The value for the description field */
    private String description = "";
                                                
    /** The value for the tagIcon field */
    private String tagIcon = "";
                                                
    /** The value for the tagLogo field */
    private String tagLogo = "";
  
    
    /**
     * Get the TagId
     *
     * @return String
     */
    public String getTagId()
    {
        return tagId;
    }

                        
    /**
     * Set the value of TagId
     *
     * @param v new value
     */
    public void setTagId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.tagId, v))
              {
            this.tagId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TagName
     *
     * @return String
     */
    public String getTagName()
    {
        return tagName;
    }

                        
    /**
     * Set the value of TagName
     *
     * @param v new value
     */
    public void setTagName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.tagName, v))
              {
            this.tagName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TagIcon
     *
     * @return String
     */
    public String getTagIcon()
    {
        return tagIcon;
    }

                        
    /**
     * Set the value of TagIcon
     *
     * @param v new value
     */
    public void setTagIcon(String v) 
    {
    
                  if (!ObjectUtils.equals(this.tagIcon, v))
              {
            this.tagIcon = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TagLogo
     *
     * @return String
     */
    public String getTagLogo()
    {
        return tagLogo;
    }

                        
    /**
     * Set the value of TagLogo
     *
     * @param v new value
     */
    public void setTagLogo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.tagLogo, v))
              {
            this.tagLogo = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("TagId");
              fieldNames.add("TagName");
              fieldNames.add("Description");
              fieldNames.add("TagIcon");
              fieldNames.add("TagLogo");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("TagId"))
        {
                return getTagId();
            }
          if (name.equals("TagName"))
        {
                return getTagName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("TagIcon"))
        {
                return getTagIcon();
            }
          if (name.equals("TagLogo"))
        {
                return getTagLogo();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(TagPeer.TAG_ID))
        {
                return getTagId();
            }
          if (name.equals(TagPeer.TAG_NAME))
        {
                return getTagName();
            }
          if (name.equals(TagPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(TagPeer.TAG_ICON))
        {
                return getTagIcon();
            }
          if (name.equals(TagPeer.TAG_LOGO))
        {
                return getTagLogo();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getTagId();
            }
              if (pos == 1)
        {
                return getTagName();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return getTagIcon();
            }
              if (pos == 4)
        {
                return getTagLogo();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(TagPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        TagPeer.doInsert((Tag) this, con);
                        setNew(false);
                    }
                    else
                    {
                        TagPeer.doUpdate((Tag) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key tagId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setTagId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setTagId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getTagId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Tag copy() throws TorqueException
    {
        return copyInto(new Tag());
    }
  
    protected Tag copyInto(Tag copyObj) throws TorqueException
    {
          copyObj.setTagId(tagId);
          copyObj.setTagName(tagName);
          copyObj.setDescription(description);
          copyObj.setTagIcon(tagIcon);
          copyObj.setTagLogo(tagLogo);
  
                    copyObj.setTagId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public TagPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Tag\n");
        str.append("---\n")
           .append("TagId                : ")
           .append(getTagId())
           .append("\n")
           .append("TagName              : ")
           .append(getTagName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("TagIcon              : ")
           .append(getTagIcon())
           .append("\n")
           .append("TagLogo              : ")
           .append(getTagLogo())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.tools.financial;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.BankReconcile;
import com.ssti.enterprise.pos.om.BankReconcilePeer;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for Cash Flow and Cash Flow Journal OM 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BankReconcileTool.java,v 1.1 2008/06/29 07:12:27 albert Exp $ <br>
 *
 * <pre>
 * $Log: BankReconcileTool.java,v $
 * Revision 1.1  2008/06/29 07:12:27  albert
 * *** empty log message ***
 *
 *
 * </pre><br>
 */
public class BankReconcileTool extends BaseTool
{
    private static Log log = LogFactory.getLog(BankReconcileTool.class);
    
	
	private static BankReconcileTool instance = null;
	
	public static synchronized BankReconcileTool getinstance()
	{
		if (instance == null) instance = new BankReconcileTool();
		return instance;
	}
    
    public static BankReconcile getLastReconcile(String _sBankID, Date _dPrev)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add (BankReconcilePeer.BANK_ID, _sBankID);
    	oCrit.addDescendingOrderByColumn(BankReconcilePeer.RECONCILE_DATE);
    	if (_dPrev != null)
    	{
    		oCrit.add (BankReconcilePeer.RECONCILE_DATE, _dPrev, Criteria.LESS_THAN);
    	}
    	oCrit.setLimit(1);
    	
    	List vData = BankReconcilePeer.doSelect(oCrit);
    	if (vData.size() > 0)
    	{
    		return (BankReconcile) vData.get(0);
    	}
    	return null;
    }
    
    public static Date getLastReconDate(String _sBankID, Date _dPrev)
		throws Exception
	{
		BankReconcile oBR = getLastReconcile(_sBankID, _dPrev);
		if (oBR != null)
		{
			return oBR.getReconcileDate();
		}
		return null;
	}    
    
    public static BankReconcile getReconcileAtDate(String _sBankID, Date _dRecDate)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (BankReconcilePeer.BANK_ID, _sBankID);		
		oCrit.add (BankReconcilePeer.RECONCILE_DATE, _dRecDate);
		oCrit.setLimit(1);
		List vData = BankReconcilePeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (BankReconcile) vData.get(0);
		}
		return null;
	}
    
    public static List getBankReconcile(String _sBankID, Date _dStart, Date _dEnd)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sBankID))
		{
			oCrit.add (BankReconcilePeer.BANK_ID, _sBankID);
		}
		if (_dStart != null)
		{
			oCrit.add (BankReconcilePeer.RECONCILE_DATE, _dStart, Criteria.GREATER_EQUAL);
			oCrit.and (BankReconcilePeer.RECONCILE_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);		
		}
		oCrit.addAscendingOrderByColumn(BankReconcilePeer.RECONCILE_DATE);
		return BankReconcilePeer.doSelect(oCrit);
	}    
    
    private static void deleteOldReconcile (BankReconcile _oBR, Connection _oConn)
		throws Exception
	{
      	Criteria oCrit = new Criteria();
    	oCrit.add (BankReconcilePeer.BANK_ID, _oBR.getBankId());
    	oCrit.add (BankReconcilePeer.RECONCILE_DATE, _oBR.getReconcileDate());
    	BankReconcilePeer.doDelete(oCrit, _oConn);
	}    

	public static void saveData(BankReconcile _oBR, List _vCF)
		throws Exception
	{
		Connection oConn = null;
		try
		{
			oConn = beginTrans();		
			_oBR.setReconcileDate(DateUtil.getEndOfDayDate(_oBR.getReconcileDate()));
			deleteOldReconcile (_oBR, oConn);
			
			log.debug ("saving bank reconcile: " + _oBR);
			
			_oBR.setBankReconcileId(IDGenerator.generateSysID());
			_oBR.setNew(true);
			_oBR.setModified(true);
			_oBR.save(oConn);
			
			for (int i = 0; i < _vCF.size(); i++)
			{
				CashFlow oCF = (CashFlow) _vCF.get(i);
				oCF.save(oConn);
			}
			commit(oConn);
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			rollback(oConn);			
			throw new Exception (LocaleTool.getString("save_failed") + _oEx.getMessage());
		}
	}

	public static void cancelBR(BankReconcile _oBR, List _vCF)
		throws Exception
	{
		Connection oConn = null;
		try
		{
			oConn = beginTrans();		
			Criteria oCrit = new Criteria();
			oCrit.add(BankReconcilePeer.BANK_RECONCILE_ID, _oBR.getBankReconcileId());
			BankReconcilePeer.doDelete(oCrit, oConn);
			
			for (int i = 0; i < _vCF.size(); i++)
			{
				CashFlow oCF = (CashFlow) _vCF.get(i);
				oCF.setReconciled(false);
				oCF.setReconcileDate(null);
				oCF.save(oConn);
			}
			commit(oConn);
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			rollback(oConn);			
			throw new Exception (LocaleTool.getString("delete_failed") + _oEx.getMessage());
		}
	}
}
package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PriceList;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.om.PriceListPeer;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class PriceListDetailManager
{
	private static Log log = LogFactory.getLog(PriceListDetailManager.class);
    
    private static PriceListDetailManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "discount";    

    private PriceListDetailManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static PriceListDetailManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (PriceListDetailManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new PriceListDetailManager();
                }
            }
        }
        return m_oInstance;
    }
        
    /**
     * getPriceListByID
     * 
     * @param _sID
     * @param _oConn
     * @return
     * @throws Exception
     */
    public PriceList getPLByID(String _sID, Connection _oConn)
		throws Exception
	{
        String sKey = "PL-" + _sID;        
        Cache oCache = m_oManager.getCache(s_CACHE);
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {           	
			Criteria oCrit = new Criteria();
		    oCrit.add(PriceListPeer.PRICE_LIST_ID, _sID);
		    List vData = PriceListPeer.doSelect(oCrit, _oConn);
		    if (vData.size() > 0)
		    {
				PriceList oPL = (PriceList) vData.get(0);
				oCache.put(new Element (sKey, (Serializable) oPL)); 
				return oPL;
			}
        }
        else
        {
        	return (PriceList) oElement.getValue();
        }
		return null;
	}
    
    /**
     * check if PL for specific customer (with not empty customer ID) exists
     * 
     * @param _sLocID
     * @return
     * @throws Exception
     */
    public boolean isByCustExist(String _sLocID) 
    	throws Exception
    {		
    	String sKey = "PLBYCUST-" + _sLocID;
    	Cache oCache = m_oManager.getCache(s_CACHE);
    	Element oElement = oCache.get(sKey);
    	if (oElement == null)
    	{       
    		boolean bExist = PriceListTool.isByCustomerExist(_sLocID);
    		oCache.put(new Element (sKey, Boolean.valueOf(bExist))); 
    		return bExist;
    	}
    	else 
    	{
    		return (Boolean) oElement.getValue();
    	}
    }
    
    /**
     * get PriceListDetail in Location for certain customer
     * check if PL by customer exists then put customerID in key 
     * 
     * @param _sCustID
     * @param _sCustTypeID
     * @param _sLocID
     * @param _sItemID
     * @param _dTransDate
     * @return
     * @throws Exception
     */
    public PriceListDetail getDetail (String _sCustID, 
    								  String _sCustTypeID, 
    							  	  String _sLocID, 
    							  	  String _sItemID,
    							  	  Date _dTransDate) 
    	throws Exception
    {		
    	String sDate = CustomFormatter.formatCustomDateTime(_dTransDate,"yyMMdd");
    	String sKey = "PLD-" + _sLocID + "-" + _sItemID + "-" + sDate + "-" + _sCustTypeID;
    	if(isByCustExist(_sLocID)) sKey += "-" + _sCustID;
    	
        Cache oCache = m_oManager.getCache(s_CACHE);
        Element oElement = oCache.get(sKey);
        if (oElement == null)
        {       
            log.info("PLD From DB " + sKey);
            PriceListDetail oPLD = PriceListTool.getPLDetail(_sCustID, _sCustTypeID, _sLocID, _sItemID, _dTransDate);
            oCache.put(new Element (sKey, (Serializable) oPLD)); 
            return oPLD;
        }
        else 
        {
            log.debug("PLD From CACHE " + sKey);
            return (PriceListDetail) oElement.getValue();
        }
    }	
    
    /**
     * 
     * called by PriceListAction doInsert doUpdate 
     *    
     * @param _oData
     * @throws Exception
     */
  	public void refreshCache (PriceList _oData) 
  		throws Exception
  	{	
  		Cache oCache = m_oManager.getCache(s_CACHE);         
  		List vKeys = oCache.getKeys();
  		if(vKeys != null && vKeys.size() > 0)
  		{
  			Iterator iter = vKeys.iterator();
  			while(iter.hasNext())
  			{
  				String sKey = (String) iter.next();
  				if(StringUtil.containsIgnoreCase(sKey, "PLD-") || 
  				   StringUtil.containsIgnoreCase(sKey, "PLBYCUST-"))
  				{
  					oCache.remove(sKey);
  				}
  			}
  		}                
  		if (_oData != null && StringUtil.isNotEmpty(_oData.getPriceListId())) 
  		{
  			oCache.put(new Element ("PL-" + _oData.getPriceListId(), _oData));
  		}
  	}
  	
  	/**
  	 * called by PriceListAction doDelete doLoadExcel
  	 * 
  	 * @throws Exception
  	 */
  	public void refreshCache() 
  		throws Exception
  	{	
  		Cache oCache = m_oManager.getCache(s_CACHE);         
  		List vKeys = oCache.getKeys();
  		if(vKeys != null && vKeys.size() > 0)
  		{
  			Iterator iter = vKeys.iterator();
  			while(iter.hasNext())
  			{
  				String sKey = (String) iter.next();
  				if(StringUtil.containsIgnoreCase(sKey, "PL-") || 
  				   StringUtil.containsIgnoreCase(sKey, "PLD-") ||
  				   StringUtil.containsIgnoreCase(sKey, "PLBYCUST-"))
  				{
  					oCache.remove(sKey);
  				}
  			}
  		}                
  	}  
}

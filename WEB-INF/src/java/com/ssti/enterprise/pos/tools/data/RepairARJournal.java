package com.ssti.enterprise.pos.tools.data;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.financial.AccountReceivableTool;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class RepairARJournal extends BaseValidator implements AppAttributes
{
	Log log = LogFactory.getLog(getClass());

	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void repairInvoiceARJournal(Date _dStart, Date _dEnd)
	{		
		if (_dStart != null && _dEnd != null)
		{
			Connection oConn = null;
			try
			{							
				oConn = BaseTool.beginTrans();
				List vTR = TransactionTool.findTrans(_dStart, _dEnd, i_PROCESSED, "", "", "");
				int iUpdated = 0;
				for (int i = 0; i < vTR.size(); i++)
				{
					SalesTransaction oTR = (SalesTransaction) vTR.get(i);
					List vPMT = InvoicePaymentTool.getTransPayment(oTR.getSalesTransactionId(), oConn);
					if (vPMT == null || vPMT.size() == 0)
					{
						InvoicePayment oPMT 	 = new InvoicePayment();
						oPMT.setInvoicePaymentId (IDGenerator.generateSysID());
				        oPMT.setPaymentTypeId    (oTR.getPaymentTypeId());
				        oPMT.setPaymentTermId    (oTR.getPaymentTermId());
				        oPMT.setPaymentAmount    (oTR.getTotalAmount());
				        oPMT.setTransTotalAmount (oTR.getTotalAmount());
				        oPMT.setVoucherId        ("");
				        oPMT.setTransactionDate  (oTR.getTransactionDate());
				        oPMT.setDueDate  		 (oTR.getDueDate());
				        oPMT.setBankId			 ("");
				        oPMT.setBankIssuer		 ("");
				        oPMT.setReferenceNo		 ("");
				        oPMT.setTransactionId    (oTR.getSalesTransactionId());
				        oPMT.setTransactionType  (1); //sales
				        oPMT.setModified 		 (true);
				        oPMT.setNew 			 (true);
				        oPMT.save(oConn);
				        
						PaymentType oPT = PaymentTypeTool.getPaymentTypeByID(oTR.getPaymentTypeId(), oConn);
						if (oPT.getIsCredit())
						{
							List vAR = AccountReceivableTool.getDataByTransID(oTR.getSalesTransactionId(), oConn);
							if (vAR.size() == 0)
							{
								AccountReceivableTool.createAREntryFromSales (oTR, oPMT, oConn);							
							}
						}
						iUpdated++;
						m_oResult.append(" Transaction " + oTR.getInvoiceNo() + " UPDATED \n");
					}
					else
					{
						m_oResult.append(" Transaction " + oTR.getInvoiceNo() + " NOT UPDATED (OK) \n");
					}
				}
				BaseTool.commit(oConn);
				m_oResult.append(" Total " + iUpdated + " Transaction UPDATED ");				
			}
			catch (Exception _oEx)
			{
				try 
				{
					BaseTool.rollback(oConn);
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				handleError (oConn, _oEx);
			}
		}
	}
}

package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.CashManagementLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

public class CashManagementAction extends TransactionSecureAction
{
   	private static final String s_VIEW_PERM    = "View Cash Management";
	private static final String s_CREATE_PERM  = "Create Cash Management";
	private static final String s_CANCEL_PERM  = "Cancel Cash Management";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doInsert",null) != null)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
        if(data.getParameters().getString("eventSubmit_doCancel",null) != null)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform(RunData data, Context context)
    {
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1) 
    	{
    		doSave(data,context);
    	}
    	if (iSave == 2) 
    	{
    		doCancel(data,context);
    	}
    }

	public void doFind(RunData data, Context context)
    {
		try
		{
			super.doFind(data);			
			String sBankID = data.getParameters().getString ("BankId");
			int iCashFlowType = data.getParameters().getInt ("CashFlowType");
			int iStatus = data.getParameters().getInt ("Status");
			int iTransType = data.getParameters().getInt ("TransType", -1);			
			Date dStartDue = CustomParser.parseDate(data.getParameters().getString("StartDue"));
			Date dEndDue = CustomParser.parseDate(data.getParameters().getString("EndDue"));
			
			LargeSelect vTR = CashFlowTool.findData (iCond, sKeywords, sBankID, sLocationID,
													 iCashFlowType, iStatus, iTransType,
													 dStart, dEnd, dStartDue, dEndDue, iLimit);
			
			data.getUser().setTemp("findCashFlowResult", vTR);
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();        	
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
	
	public void doSave(RunData data, Context context)
    {
		try
		{
			synchronized(this)
			{
				HttpSession oSes = data.getSession();
				CashFlow oCF =  (CashFlow) oSes.getAttribute (s_CF);			
				List vCFD = (List) oSes.getAttribute (s_CFD);			
				CashFlowTool.setHeaderProperties (oCF, vCFD, data, true);			
				CashFlowTool.saveData (oCF, vCFD);			
				data.setMessage(LocaleTool.getString("cf_save_success"));     
			}
        }
        catch(Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("cf_save_failed"), _oEx);
        }
    }

	public void doCancel (RunData data, Context context)
    {
		try
		{
			synchronized(this)
			{
				HttpSession oSes = data.getSession();
				CashFlow oCF =  (CashFlow) oSes.getAttribute (s_CF);			
				if (oCF != null) 
				{
					CashFlowTool.cancelCashFlow (oCF, data.getUser().getName(), null);			
					data.setMessage(LocaleTool.getString("cf_cancel_success"));

					if (oCF.getTransactionType() == i_CF_NORMAL)
					{
						CashFlowTool.cancelCashFlow (oCF, data.getUser().getName(), null);			
						data.setMessage(LocaleTool.getString("cf_cancel_success"));     
					}
					else
					{
						data.setMessage(LocaleTool.getString("trans_cancel_src") + oCF.getTransactionNo());
					}
				}
			}
        }
        catch(Exception _oEx)
		{
        	handleError (data, LocaleTool.getString("cf_cancel_failed"), _oEx);
        }
    }	
	
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
            String sLoader = data.getParameters().getString("loader");
            TransactionLoader oLoader = null;
            if (StringUtil.isNotEmpty(sLoader))
            {
                oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
                oLoader.setUserName(data.getUser().getName());
            }
            else
            {
                oLoader = new CashManagementLoader(data.getUser().getName());
            }            
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
			context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
			log.error ( sError, _oEx );
			data.setMessage (sError);
		}
	} 
}
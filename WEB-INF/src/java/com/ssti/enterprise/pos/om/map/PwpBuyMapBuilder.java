package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PwpBuyMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PwpBuyMapBuilder";

    /**
     * Item
     * @deprecated use PwpBuyPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "pwp_buy";
    }

  
    /**
     * pwp_buy.PWP_BUY_ID
     * @return the column name for the PWP_BUY_ID field
     * @deprecated use PwpBuyPeer.pwp_buy.PWP_BUY_ID constant
     */
    public static String getPwpBuy_PwpBuyId()
    {
        return "pwp_buy.PWP_BUY_ID";
    }
  
    /**
     * pwp_buy.PWP_ID
     * @return the column name for the PWP_ID field
     * @deprecated use PwpBuyPeer.pwp_buy.PWP_ID constant
     */
    public static String getPwpBuy_PwpId()
    {
        return "pwp_buy.PWP_ID";
    }
  
    /**
     * pwp_buy.BUY_ITEM_ID
     * @return the column name for the BUY_ITEM_ID field
     * @deprecated use PwpBuyPeer.pwp_buy.BUY_ITEM_ID constant
     */
    public static String getPwpBuy_BuyItemId()
    {
        return "pwp_buy.BUY_ITEM_ID";
    }
  
    /**
     * pwp_buy.BUY_QTY
     * @return the column name for the BUY_QTY field
     * @deprecated use PwpBuyPeer.pwp_buy.BUY_QTY constant
     */
    public static String getPwpBuy_BuyQty()
    {
        return "pwp_buy.BUY_QTY";
    }
  
    /**
     * pwp_buy.BUY_PRICE
     * @return the column name for the BUY_PRICE field
     * @deprecated use PwpBuyPeer.pwp_buy.BUY_PRICE constant
     */
    public static String getPwpBuy_BuyPrice()
    {
        return "pwp_buy.BUY_PRICE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("pwp_buy");
        TableMap tMap = dbMap.getTable("pwp_buy");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("pwp_buy.PWP_BUY_ID", "");
                          tMap.addForeignKey(
                "pwp_buy.PWP_ID", "" , "pwp" ,
                "pwp_id");
                          tMap.addColumn("pwp_buy.BUY_ITEM_ID", "");
                            tMap.addColumn("pwp_buy.BUY_QTY", bd_ZERO);
                            tMap.addColumn("pwp_buy.BUY_PRICE", bd_ZERO);
          }
}

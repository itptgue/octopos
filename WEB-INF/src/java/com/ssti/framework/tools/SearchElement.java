package com.ssti.framework.tools;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SearchElement.java,v 1.3 2007/02/23 14:14:04 albert Exp $ <br>
 * 
 * <pre>
 * $Log: SearchElement.java,v $
 * Revision 1.3  2007/02/23 14:14:04  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SearchElement
{    		
	private int Condition;
	private String Keyword;
	private int Comparator;
	private int Connector;

	public SearchElement () {}
	
	public SearchElement (int iCondition, String sKeyword, int iComparator, int iConnector) 
	{
		this.Condition  = iCondition;
		this.Keyword    = sKeyword;
		this.Comparator = iComparator;
		this.Connector 	= iConnector;
	}
	
	public void setCondition(int Condition) {
		this.Condition = Condition; 
	}

	public void setKeyword(String Keyword) {
		this.Keyword = Keyword; 
	}

	public void setComparator(int Comparator) {
		this.Comparator = Comparator; 
	}

	public void setConnector(int Connector) {
		this.Connector = Connector; 
	}
	public int getCondition() {
		return (this.Condition); 
	}

	public String getKeyword() {
		return (this.Keyword); 
	}

	public int getComparator() {
		return (this.Comparator); 
	}

	public int getConnector() {
		return (this.Connector); 
	}
	
	public String toString ()
	{ 
		StringBuilder oSB = new StringBuilder ();
		oSB.append ("\n\nCondition : ");
		oSB.append ( Condition );
		oSB.append ("\n\nKeyword : ");
		oSB.append ( Keyword );
		oSB.append ("\n\nComparator : ");
		oSB.append ( Comparator );
		oSB.append ("\n\nConnector : ");
		oSB.append ( Connector );
		return oSB.toString();
	}
}

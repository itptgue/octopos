package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;

public class RankingReport extends ReportSecureScreen
{
	String sStart;
	String sEnd;
	
	Date dStart;
	Date dEnd;
	
	int iLimit; 
	int iRankBy;
	int iRankType;
	String sLocationID;
	String sKategoriID;
	String sCustomerID;
	String sCustTypeID;
	String sCashier;
	String sSalesID;
	
	boolean bIncTax;
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
		{
    		sStart = data.getParameters().getString("StartDate");
    		sEnd = data.getParameters().getString("EndDate");
			
	    	dStart = CustomParser.parseDate(sStart);
	    	dEnd = CustomParser.parseDate(sEnd);

	        dStart  = DateUtil.getStartOfDayDate(dStart);
    	    dEnd = DateUtil.getEndOfDayDate(dEnd);
    	    
    	    iLimit = data.getParameters().getInt("Ranking");
    	    iRankBy = data.getParameters().getInt("RankBy");
    	    iRankType = data.getParameters().getInt("RankType");
    	    
    	    sLocationID = data.getParameters().getString("LocationId");
    	    sKategoriID = data.getParameters().getString("KategoriId");

    	    sCustomerID = data.getParameters().getString("CustomerId");
    	    sCustTypeID = data.getParameters().getString("CustomerTypeId");

    	    sCashier = data.getParameters().getString("Cashier");
    	    sSalesID = data.getParameters().getString("SalesId");

    	    bIncTax = data.getParameters().getBoolean("IncludeTax");
    	    
	    	context.put ("start", sStart);
    		context.put ("end", sEnd);    

	    	context.put ("iRank", Integer.valueOf(iLimit));
    		context.put ("iRankBy", Integer.valueOf(iRankBy));    
    		context.put ("iRankType", Integer.valueOf(iRankType));    
    		context.put ("bIncTax", bIncTax);    
    		
	    	context.put ("dStart", dStart);
    		context.put ("dEnd", dEnd);    		
		}   
    	catch (Exception _oEx) {
    		data.setMessage("Sales Summary Data Lookup Failed : " + _oEx.getMessage());
    	}    
    }    
}

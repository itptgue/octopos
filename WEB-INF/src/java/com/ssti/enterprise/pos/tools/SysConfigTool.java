package com.ssti.enterprise.pos.tools;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.PreferenceManager;
import com.ssti.enterprise.pos.om.SysConfig;
import com.ssti.enterprise.pos.om.SysConfigPeer;
import com.ssti.framework.om.CardioConfig;
import com.ssti.framework.tools.AppConfigTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

public class SysConfigTool extends AppConfigTool
{
	static final Log log = LogFactory.getLog(SysConfigTool.class);
	static SysConfigTool instance = null;
	
	public static synchronized SysConfigTool getInstance() 
	{
		if (instance == null) instance = new SysConfigTool();
		return instance;
	}	
	
	public static SysConfig getSysConfig() 
		throws Exception
	{
		SysConfig oSC = PreferenceManager.getInstance().getSysConfig();
		if (oSC != null)
		{
			return oSC;
		}
		else
		{
			return createDefaultConfig();
		}
	}

	private static SysConfig createDefaultConfig() 
		throws Exception
	{
		List v = SysConfigPeer.doSelect(new Criteria());
		if (v.size() == 0)
		{		
			SysConfig oConf = new SysConfig();
			oConf.setSysConfigId("1");
			oConf.setNew(true);
			oConf.save();
			
			PreferenceManager.getInstance().refreshCache(oConf);
			return oConf;
		}
		return getSysConfig();
	}

	static List vSys = null;
	static List vDev = null;
	static List vPOS = null;
	static List vKey = null;	
	static final List vFields = SysConfig.getFieldNames();

	public static List getSysFields() 
		throws Exception
	{
		return filterFields("BackupOutputDir", "DefaultSearchCond", vSys);
	}
	
	public static List getDevFields() 
		throws Exception
	{
		vDev = CardioConfig.getFieldNames();
		return vDev;
		//return filterFields("PrinterHost", "BarcodePrinterUse", vDev);
	}

	public static List getPOSFields() 
		throws Exception
	{
		return filterFields("PosDefaultScreen", "ComplexBarcodeTotalLength", vPOS);
	}
	
	public static List getKeyFields() 
		throws Exception
	{
		return filterFields("KeyNewTrans", "KeySelectPayment", vKey);
	}
	
	public static List filterFields(String _sStart, String _sEnd, List _vResult) 
		throws Exception
	{
		if (_vResult == null)
		{
			_vResult = new ArrayList(vFields.size());
			boolean bStart = false;
			boolean bEnd = false;
			for (int i = 0; i < vFields.size(); i++)
			{
				String sField = (String) vFields.get(i);
				if (StringUtil.equalsIgnoreCase(sField, _sStart)) bStart = true;
				if (bStart && !bEnd) _vResult.add(sField);
				if (StringUtil.equalsIgnoreCase(sField, _sEnd)) bEnd = true;
			}
		}
		return _vResult;
	}
		
	public static Object getValue(String _sField) 
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sField))
		{
			String sFirst = _sField.substring(0,1).toLowerCase();
			String sField = sFirst + _sField.substring(1);
			try 
			{
				return BeanUtil.invokeGetter(getSysConfig(), sField);							
			} 
			catch (Exception _oEx) 
			{
				log.error("Error Getting Sysconfig "+ sField + ": " + _oEx.getMessage(), _oEx);
			}
		}
		return "";
	}
}

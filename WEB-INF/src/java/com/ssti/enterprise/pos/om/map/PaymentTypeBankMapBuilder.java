package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PaymentTypeBankMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PaymentTypeBankMapBuilder";

    /**
     * Item
     * @deprecated use PaymentTypeBankPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "payment_type_bank";
    }

  
    /**
     * payment_type_bank.PAYMENT_TYPE_BANK_ID
     * @return the column name for the PAYMENT_TYPE_BANK_ID field
     * @deprecated use PaymentTypeBankPeer.payment_type_bank.PAYMENT_TYPE_BANK_ID constant
     */
    public static String getPaymentTypeBank_PaymentTypeBankId()
    {
        return "payment_type_bank.PAYMENT_TYPE_BANK_ID";
    }
  
    /**
     * payment_type_bank.BANK_CODE
     * @return the column name for the BANK_CODE field
     * @deprecated use PaymentTypeBankPeer.payment_type_bank.BANK_CODE constant
     */
    public static String getPaymentTypeBank_BankCode()
    {
        return "payment_type_bank.BANK_CODE";
    }
  
    /**
     * payment_type_bank.BANK_NAME
     * @return the column name for the BANK_NAME field
     * @deprecated use PaymentTypeBankPeer.payment_type_bank.BANK_NAME constant
     */
    public static String getPaymentTypeBank_BankName()
    {
        return "payment_type_bank.BANK_NAME";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("payment_type_bank");
        TableMap tMap = dbMap.getTable("payment_type_bank");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("payment_type_bank.PAYMENT_TYPE_BANK_ID", "");
                          tMap.addColumn("payment_type_bank.BANK_CODE", "");
                          tMap.addColumn("payment_type_bank.BANK_NAME", "");
          }
}

package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemFieldMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemFieldMapBuilder";

    /**
     * Item
     * @deprecated use ItemFieldPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_field";
    }

  
    /**
     * item_field.ITEM_FIELD_ID
     * @return the column name for the ITEM_FIELD_ID field
     * @deprecated use ItemFieldPeer.item_field.ITEM_FIELD_ID constant
     */
    public static String getItemField_ItemFieldId()
    {
        return "item_field.ITEM_FIELD_ID";
    }
  
    /**
     * item_field.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemFieldPeer.item_field.ITEM_ID constant
     */
    public static String getItemField_ItemId()
    {
        return "item_field.ITEM_ID";
    }
  
    /**
     * item_field.FIELD_NAME
     * @return the column name for the FIELD_NAME field
     * @deprecated use ItemFieldPeer.item_field.FIELD_NAME constant
     */
    public static String getItemField_FieldName()
    {
        return "item_field.FIELD_NAME";
    }
  
    /**
     * item_field.FIELD_VALUE
     * @return the column name for the FIELD_VALUE field
     * @deprecated use ItemFieldPeer.item_field.FIELD_VALUE constant
     */
    public static String getItemField_FieldValue()
    {
        return "item_field.FIELD_VALUE";
    }
  
    /**
     * item_field.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use ItemFieldPeer.item_field.UPDATE_DATE constant
     */
    public static String getItemField_UpdateDate()
    {
        return "item_field.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_field");
        TableMap tMap = dbMap.getTable("item_field");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_field.ITEM_FIELD_ID", "");
                          tMap.addColumn("item_field.ITEM_ID", "");
                          tMap.addColumn("item_field.FIELD_NAME", "");
                          tMap.addColumn("item_field.FIELD_VALUE", "");
                          tMap.addColumn("item_field.UPDATE_DATE", new Date());
          }
}

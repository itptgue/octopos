package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class FifoOutMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.FifoOutMapBuilder";

    /**
     * Item
     * @deprecated use FifoOutPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "fifo_out";
    }

  
    /**
     * fifo_out.FIFO_OUT_ID
     * @return the column name for the FIFO_OUT_ID field
     * @deprecated use FifoOutPeer.fifo_out.FIFO_OUT_ID constant
     */
    public static String getFifoOut_FifoOutId()
    {
        return "fifo_out.FIFO_OUT_ID";
    }
  
    /**
     * fifo_out.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use FifoOutPeer.fifo_out.LOCATION_ID constant
     */
    public static String getFifoOut_LocationId()
    {
        return "fifo_out.LOCATION_ID";
    }
  
    /**
     * fifo_out.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use FifoOutPeer.fifo_out.LOCATION_NAME constant
     */
    public static String getFifoOut_LocationName()
    {
        return "fifo_out.LOCATION_NAME";
    }
  
    /**
     * fifo_out.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use FifoOutPeer.fifo_out.ITEM_ID constant
     */
    public static String getFifoOut_ItemId()
    {
        return "fifo_out.ITEM_ID";
    }
  
    /**
     * fifo_out.QTY
     * @return the column name for the QTY field
     * @deprecated use FifoOutPeer.fifo_out.QTY constant
     */
    public static String getFifoOut_Qty()
    {
        return "fifo_out.QTY";
    }
  
    /**
     * fifo_out.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use FifoOutPeer.fifo_out.ITEM_COST constant
     */
    public static String getFifoOut_ItemCost()
    {
        return "fifo_out.ITEM_COST";
    }
  
    /**
     * fifo_out.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use FifoOutPeer.fifo_out.TRANSACTION_DATE constant
     */
    public static String getFifoOut_TransactionDate()
    {
        return "fifo_out.TRANSACTION_DATE";
    }
  
    /**
     * fifo_out.LAST_UPDATE
     * @return the column name for the LAST_UPDATE field
     * @deprecated use FifoOutPeer.fifo_out.LAST_UPDATE constant
     */
    public static String getFifoOut_LastUpdate()
    {
        return "fifo_out.LAST_UPDATE";
    }
  
    /**
     * fifo_out.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use FifoOutPeer.fifo_out.TRANSACTION_ID constant
     */
    public static String getFifoOut_TransactionId()
    {
        return "fifo_out.TRANSACTION_ID";
    }
  
    /**
     * fifo_out.INVENTORY_TRANSACTION_ID
     * @return the column name for the INVENTORY_TRANSACTION_ID field
     * @deprecated use FifoOutPeer.fifo_out.INVENTORY_TRANSACTION_ID constant
     */
    public static String getFifoOut_InventoryTransactionId()
    {
        return "fifo_out.INVENTORY_TRANSACTION_ID";
    }
  
    /**
     * fifo_out.FIFO_IN_ID
     * @return the column name for the FIFO_IN_ID field
     * @deprecated use FifoOutPeer.fifo_out.FIFO_IN_ID constant
     */
    public static String getFifoOut_FifoInId()
    {
        return "fifo_out.FIFO_IN_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("fifo_out");
        TableMap tMap = dbMap.getTable("fifo_out");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("fifo_out.FIFO_OUT_ID", "");
                          tMap.addColumn("fifo_out.LOCATION_ID", "");
                          tMap.addColumn("fifo_out.LOCATION_NAME", "");
                          tMap.addColumn("fifo_out.ITEM_ID", "");
                            tMap.addColumn("fifo_out.QTY", bd_ZERO);
                            tMap.addColumn("fifo_out.ITEM_COST", bd_ZERO);
                          tMap.addColumn("fifo_out.TRANSACTION_DATE", new Date());
                          tMap.addColumn("fifo_out.LAST_UPDATE", new Date());
                          tMap.addColumn("fifo_out.TRANSACTION_ID", "");
                          tMap.addForeignKey(
                "fifo_out.INVENTORY_TRANSACTION_ID", "" , "inventory_transaction" ,
                "inventory_transaction_id");
                          tMap.addColumn("fifo_out.FIFO_IN_ID", "");
          }
}

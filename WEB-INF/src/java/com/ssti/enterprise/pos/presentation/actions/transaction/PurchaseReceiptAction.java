package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.PurchaseReceiptItemLoader;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/transaction/PurchaseReceiptAction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseReceiptAction.java,v 1.32 2009/05/04 01:58:24 albert Exp $
 *
 * $Log: PurchaseReceiptAction.java,v $
 * Revision 1.32  2009/05/04 01:58:24  albert
 * *** empty log message ***
 *
 * Revision 1.31  2007/11/09 01:12:15  albert
 * *** empty log message ***
 *
 * Revision 1.30  2006/02/04 05:17:52  albert
 * *** empty log message ***
 *
 * Revision 1.29  2006/01/02 09:54:33  albert
 * *** empty log message ***
 *
 * Revision 1.28  2005/12/27 03:33:10  albert
 * *** empty log message ***
 *
 * Revision 1.27  2005/10/10 03:36:21  albert
 * *** empty log message ***
 *
 */
public class PurchaseReceiptAction extends TransactionSecureAction
{
	private static final String s_VIEW_PERM    = "View Purchase Receipt";
	private static final String s_CREATE_PERM  = "Create Purchase Receipt";
	private static final String s_CONFIRM_PERM = "Confirm Purchase Receipt";
	private static final String s_CANCEL_PERM  = "Cancel Purchase Receipt";
	
    private static final String[] a_CREATE_PERM  = {s_PURCHASE_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CONFIRM_PERM = {s_PURCHASE_PERM, s_VIEW_PERM, s_CONFIRM_PERM};
    private static final String[] a_CANCEL_PERM  = {s_PURCHASE_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_PURCHASE_PERM, s_VIEW_PERM};
    
	private static Log log = LogFactory.getLog ( PurchaseReceiptAction.class );
	
    private HttpSession oPRcptSes = null;
    private PurchaseReceipt oPR;
    private List vPRD;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	int iStatus = data.getParameters().getInt("Status");
        if (data.getParameters().getInt("save") == 1 &&  iStatus == i_PR_NEW) 
        {
            return isAuthorized (data, a_CREATE_PERM);
    	}
        else if (data.getParameters().getInt("save") == 1 && 
        			(iStatus == i_PR_APPROVED || iStatus == i_PR_REJECTED))
        {
            return isAuthorized (data, a_CONFIRM_PERM);        
        }   
       	else if	(data.getParameters().getInt("save") == 2)
    	{
            return isAuthorized (data, a_CANCEL_PERM);
    	}
    	else
    	{
            return isAuthorized (data, a_VIEW_PERM);
    	}
    }
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	int iStatus = data.getParameters().getInt("Status");
    	 if (data.getParameters().getInt("save") == 1 &&  iStatus == i_PR_NEW) 
    	{
    		doSave (data,context);
    	}
         else if (data.getParameters().getInt("save") == 1 && 
    			(iStatus == i_PR_APPROVED || iStatus == i_PR_REJECTED))
    	{
    		doConfirm (data,context);
    	}
        else if	(data.getParameters().getInt("save") == 2)
        {
    		doCancel (data,context);
    	}
        else if	(data.getParameters().getInt("save") == 3)
        {
    		doClosePO (data,context);
    	}
    	else
    	{
    		doFind (data,context);
    	}
    }

    public void doFind ( RunData data, Context context )
        throws Exception
    {
    	super.doFind(data);

    	String sVendorID = data.getParameters().getString("VendorId", "");
    	String sLocationID = data.getParameters().getString("LocationId", "");
		int iStatus = data.getParameters().getInt("Status");

		LargeSelect vTR = PurchaseReceiptTool.findData (
			iCond, sKeywords, dStart, dEnd, sVendorID,sLocationID,iStatus, iLimit, sCurrencyID);
		
		data.getUser().setTemp("findPurchaseReceiptResult", vTR);
	}

	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{
			initSession ( data );
			PurchaseReceiptTool.updateDetail(vPRD,data);
			PurchaseReceiptTool.setHeaderProperties (oPR, vPRD, data);
			prepareTransData ( data );
			
			PurchaseReceiptTool.saveData (oPR, vPRD, null,i_PR_NEW,false);
			data.setMessage(LocaleTool.getString("pr_save_success"));
	       	resetSession ( data );
        }
        catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("pr_save_failed"), _oEx);
        }
    }
    
    public void doConfirm ( RunData data, Context context )
        throws Exception
    {
		try
		{
			initSession ( data );
			PurchaseReceiptTool.updateDetail(vPRD,data);
			PurchaseReceiptTool.setHeaderProperties (oPR, vPRD, data);
			prepareTransData ( data );
			boolean bClosePO = data.getParameters().getBoolean("ClosePO");
			
			//if transaction happened in STORE then always close PO
			if (PreferenceTool.getLocationFunction() == i_STORE)
			{
				//bClosePO = true;
			}
			if(data.getParameters().getInt("Status") == i_PR_APPROVED)
			{
				oPR.setConfirmBy(data.getUser().getName());
				PurchaseReceiptTool.saveData (oPR, vPRD, null, i_PR_APPROVED, bClosePO);
			}
			if(data.getParameters().getInt("Status") == i_PR_REJECTED)
			{
				oPR.setConfirmBy(data.getUser().getName());
				PurchaseReceiptTool.saveData (oPR, vPRD, null, i_PR_REJECTED, bClosePO);
			}
        	data.setMessage(LocaleTool.getString("pr_confirm_success"));
	       	resetSession ( data );
        }
        catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("pr_confirm_failed"), _oEx);
        }
    }
    
    public void doCancel ( RunData data, Context context )
		throws Exception
	{
		try
		{
			initSession(data);
			PurchaseReceiptTool.cancelPR(oPR, vPRD, data.getUser().getName(), null);
			data.setMessage(LocaleTool.getString("pr_cancel_success"));			
		}
		catch (Exception _oEx)
	    {
			handleError (data, LocaleTool.getString("pr_cancel_failed"), _oEx);
	    }	
	}

    public void doClosePO ( RunData data, Context context )
		throws Exception
	{
		try
		{
			initSession(data);
			if (oPR != null)
			{
				PurchaseOrder oPO = PurchaseOrderTool.getHeaderByID(oPR.getPurchaseOrderId());
				if (oPO != null)
				{
					oPO.setTransactionStatus(i_PO_CLOSED);
					oPO.save();
				}
			}
			data.setMessage(LocaleTool.getString("po_close_success"));	
		}
		catch (Exception _oEx)
	    {
			handleError (data, LocaleTool.getString("po_close_failed"), _oEx);
	    }	
	}
    
    private void initSession ( RunData data )
    	throws Exception
    {
    	synchronized (this)
    	{
    		oPRcptSes = data.getSession();
    		if ( oPRcptSes.getAttribute (s_PR) == null ||
    				oPRcptSes.getAttribute (s_PRD) == null )
    		{
    			throw new Exception ("Purchase Receipt Data Invalid");
    		}
    		oPR = (PurchaseReceipt) oPRcptSes.getAttribute (s_PR);
    		vPRD = (List) oPRcptSes.getAttribute (s_PRD);
    	}
    }

    private void prepareTransData ( RunData data )
    	throws Exception
    {
    	//check transaction data
    	if ( oPR.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 0"); } //-- should never happen
		if ( vPRD.size () < 1) {throw new Exception ("Total Item Purchased < 1"); } //-- should never happen
    }

    private void resetSession ( RunData data )
    	throws Exception
    {
    	synchronized (this)
    	{
			oPRcptSes.setAttribute (s_PR, oPR );
			oPRcptSes.setAttribute (s_PRD, vPRD );
    	}
    }
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			initSession(data);
			int iType = data.getParameters().getInt("ExcelType");			
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			if (iType == 1) //load PO item only
			{
				PurchaseReceiptItemLoader oLoader = new PurchaseReceiptItemLoader("");
				oLoader.setPRD(vPRD);
				oLoader.loadData (oBufferStream);						
								
				oPRcptSes = data.getSession();				
				oPRcptSes.setAttribute(s_PRD, oLoader.getListResult());					
				
				if (oLoader.getTotalError() > 0)
				{
					data.setMessage(oLoader.getErrorMessage());
				}
				else 
				{
					context.put("bClose", Boolean.valueOf(true));
				}		
				
				PurchaseReceiptTool.setHeaderProperties(oPR, vPRD, data);
			}
		}
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	} 
}
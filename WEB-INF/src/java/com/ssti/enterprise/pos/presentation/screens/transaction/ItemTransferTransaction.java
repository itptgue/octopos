package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.PurchaseRequest;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;
import com.ssti.enterprise.pos.tools.report.InventoryReportTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - add setQty to setQtyOH in pending trans
 * 
 * </pre><br>
 */
public class ItemTransferTransaction extends TransactionSecureScreen
{
    protected static final String[] a_PERM = {"View Item Transfer"};
	
    //start screen methods
	protected static final int i_OP_ADD_DETAIL  = 1;                                 
	protected static final int i_OP_DEL_DETAIL  = 2;                                 
	protected static final int i_OP_NEW_TRANS   = 3;                                 
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
  	protected static final int i_OP_ADD_ITEMS   = 5; 
  	protected static final int i_OP_COPY_TRANS  = 6;
  	protected static final int i_OP_ADD_REQUEST = 7; 
  	
  	protected static final int i_OP_GENERATE_FILE  = 8;  
  	protected static final int i_OP_IMPORT_FROM_EXCEL  = 9;	
  	
  	protected ItemTransfer oIT = null;
    protected List vITD = null;
    protected HttpSession oSes;  
  
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
  	public void doBuildTemplate(RunData data, Context context)
  	{
  		super.doBuildTemplate(data, context);
  		
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH) {
				addDetailData (data);
			}	
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH) {
				delDetailData (data);
			}	
			else if ( iOp == i_OP_NEW_TRANS ) {
				createNewTrans ();
			}
			else if (iOp == i_OP_VIEW_TRANS ) {
				getData (data, context);
			}
			else if (iOp == i_OP_ADD_ITEMS && !b_REFRESH)
			{
				addItems (data);
			}									
			else if (iOp == i_OP_ADD_REQUEST && !b_REFRESH) {
				importRequestData (data);
			}
			else if (iOp == i_OP_COPY_TRANS && !b_REFRESH) {
				copyTrans (data);
			}
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			data.setMessage("ERROR : " + _oEx.getMessage());
		}
		context.put(s_IT, oSes.getAttribute (s_IT));
    	context.put(s_ITD, oSes.getAttribute (s_ITD));
    }

	protected void initSession ( RunData data )
	{	
		synchronized (this)
		{
			oSes = data.getSession();
			if ( oSes.getAttribute (s_IT) == null ) 
			{
				oSes.setAttribute (s_IT, new ItemTransfer());
				oSes.setAttribute (s_ITD, new ArrayList());
			}
			oIT  = (ItemTransfer) oSes.getAttribute (s_IT);
			vITD = (List) oSes.getAttribute (s_ITD);
		}
	}
		
    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
    	ItemTransferTool.updateDetail(data, vITD);
    	
		ItemTransferDetail oDetail = new ItemTransferDetail();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			data.getParameters().setProperties (oDetail);	
			oDetail.setItemCode (oItem.getItemCode());
		}
		else
		{
			return;
		}						
		oDetail.setQtyOH(data.getParameters().getBigDecimal("CurrentQty", bd_ZERO));
		oDetail.setCostPerUnit(data.getParameters().getBigDecimal("ItemCost", bd_ZERO));
		if (!isExist (oDetail)) 
		{
			if (b_INVENTORY_FIRST_LAST)
			{
				vITD.add (0, oDetail);
			}
			else
			{
				vITD.add (oDetail);				
			}
		}

		ItemTransferTool.setHeader(data, oIT);
		oSes.setAttribute (s_IT, oIT); 
		oSes.setAttribute (s_ITD, vITD);     
    }	
	    
    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	ItemTransferTool.updateDetail(data, vITD);
		int iDelNo = data.getParameters().getInt("No") - 1;
		if (vITD.size() > iDelNo) 
		{
			vITD.remove (iDelNo);
			oSes.setAttribute (s_ITD, vITD);
		}    	
		ItemTransferTool.setHeader(data, oIT);    
	}
    
    protected void createNewTrans () 
    {
    	synchronized (this)
    	{
    		oIT = new ItemTransfer();
    		vITD = new ArrayList();
    		
    		oSes.setAttribute (s_IT,  oIT);
    		oSes.setAttribute (s_ITD, vITD);
    	}
    }
    
    protected boolean isExist (ItemTransferDetail _oData) 
    {
    	for (int i = 0; i < vITD.size(); i++) 
    	{
    		ItemTransferDetail oExistData = (ItemTransferDetail) vITD.get (i);
    		if (oExistData.getItemCode().equals(_oData.getItemCode()) && 
    			oExistData.getItemId().equals(_oData.getItemId()) &&     				
				oExistData.getUnitId().equals(_oData.getUnitId()) && !PreferenceTool.useBatchNo()
    		   ) 
    		{
				mergeData (oExistData, _oData);
				return true;    			
    		}		
    	}
    	return false;
    } 
    
    protected void mergeData (ItemTransferDetail _oOldData, ItemTransferDetail _oNewData) 
    {		
    	double dQty = _oOldData.getQtyChanges().doubleValue() + _oNewData.getQtyChanges().doubleValue();
    	_oOldData.setQtyChanges (new BigDecimal(dQty));    	
    	_oNewData = null;
    }
    
 	protected void getData ( RunData data, Context context ) 
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{
    			oIT = ItemTransferTool.getHeaderByID (sID);
    			vITD = ItemTransferTool.getDetailsByID (sID);
    			if(oIT.getStatus() == i_PENDING) setQty();
    			oSes.setAttribute (s_IT, oIT);
				oSes.setAttribute (s_ITD, vITD);
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }
 	 	
 	protected void setQty()
 	{
 		if(oIT != null && vITD != null && StringUtil.isNotEmpty(oIT.getFromLocationId()))
 		{
 			try 
 			{
 	 			List vItemID = SqlUtil.toListOfKeys(vITD,"itemId");
 	 			List vBals = InventoryReportTool.getBalances(vItemID, oIT.getFromLocationId(), oIT.getTransactionDate());
 	 			Iterator iter = vBals.iterator();
 	 			while(iter.hasNext())
 	 			{
 	 				Record r = (Record) iter.next();
 	 				for(int i = 0; i < vITD.size(); i++) 	 				
 	 				{
 	 					ItemTransferDetail oITD = (ItemTransferDetail) vITD.get(i);
 	 					if(StringUtil.equals(oITD.getItemId(), r.getValue("item_id").asString()))
 	 					{
 	 						oITD.setQtyOH(r.getValue("qty").asBigDecimal());
 	 						oITD.setCostPerUnit(r.getValue("cost").asBigDecimal());
 	 					} 	 							
 	 				}
 	 			}
			} 
 			catch (Exception e) 
 			{
 				e.printStackTrace();
 				log.error(e.getMessage(), e);
			} 			
 		}
 	}
    
    protected void importRequestData ( RunData data )
    	throws Exception
    {
    	if(StringUtil.isNotEmpty(oIT.getTransactionNo()))
    	{
    		createNewTrans();
    	}
    	String sRQID = data.getParameters().getString ("PurchaseRequestId");
    	boolean bExclZero = data.getParameters().getBoolean("ExcludeZero");
        if (StringUtil.isEmpty(sRQID)) sRQID = data.getParameters().getString("RequestId");
    	if (StringUtil.isNotEmpty(sRQID))
    	{
    		try
    		{	
    			PurchaseRequest oRQ = PurchaseRequestTool.getHeaderByID (sRQID);
    			List vRQD = PurchaseRequestTool.getDetailsByID (sRQID);
                data.getParameters().setProperties (oIT);    			
                if (oRQ != null)
                {
        		    oIT.setDescription(LocaleTool.getString("tr_from_rq") + oRQ.getTransactionNo());
        		    oIT.setRequestId(oRQ.getPurchaseRequestId());
        		    oIT.setRequestNo(oRQ.getTransactionNo());
        		    
        		    String sFromID = PreferenceTool.getLocationID(data.getUser().getName()); 
        		    if(StringUtil.isEmpty(oIT.getFromLocationId()) && StringUtil.isNotEmpty(sFromID))
        		    {
        		    	oIT.setFromLocationId(sFromID);        		    	
        		    }
        		    oIT.setFromLocationName(LocationTool.getLocationNameByID(oIT.getFromLocationId()));
        		    oIT.setToLocationId(oRQ.getLocationId());
        		    oIT.setToLocationName(oRQ.getLocationName());
        		    
        		    if (DateUtil.isBefore(oIT.getTransactionDate(), oRQ.getTransactionDate()))
        		    {
        		    	data.setMessage(LocaleTool.getString("warn_import_date"));
        		    }
        		    
        			vITD = new ArrayList (vRQD.size());
        			for (int i = 0; i < vRQD.size(); i++)
        			{
        				boolean bAdd = true;
        				PurchaseRequestDetail oRQD = (PurchaseRequestDetail) vRQD.get(i);
        				if(oRQD.getRequestQty().doubleValue() > 0 && oRQD.getOutstanding() > 0)
        				{
	        				ItemTransferDetail oITD = new ItemTransferDetail();        				
	        				PurchaseRequestTool.mapRQDetailToITDetail (oRQD, oITD);
	        				
	        				oITD.setIndexNo(oRQD.getIndexNo());
	    					oITD.setQtyOH(bd_ZERO);
	    					oITD.setCostPerUnit(bd_ZERO);
	
	        				if(StringUtil.isNotEmpty(oIT.getFromLocationId()))
	        				{
			    				InventoryLocation oIL = InventoryLocationTool.getDataByItemAndLocationID(oITD.getItemId(), oIT.getFromLocationId());
			    				if(oIL != null)
			    				{
			    					oITD.setQtyOH(oIL.getCurrentQty());
			    					oITD.setCostPerUnit(oIL.getItemCost());
			    					if(oIL.getCurrentQty().doubleValue() <= 0) //if qty in from location not enough 
			    					{
			    						oITD.setQtyChanges(bd_ZERO);
			    						oITD.setQtyBase(bd_ZERO);
			    						if(bExclZero) bAdd = false;
			    					}		    					
			    				}
			    				else //no inv loc 
			    				{
			    					if(bExclZero) bAdd = false;
			    				}		    					
	        				}
	        				if(bAdd) vITD.add(oITD);    		        	
	        			}
        			}
                }
    			oSes.setAttribute (s_IT, oIT);
				oSes.setAttribute (s_ITD, vITD);
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }
    
	/**
	 * @param data
	 */
	protected void copyTrans(RunData data) 
		throws Exception
	{
		try
		{
			String sTransID = data.getParameters().getString("TransId");
	    	ItemTransferTool.updateDetail(data, vITD);
			
			if (StringUtil.isNotEmpty (sTransID))
			{			
				List vTrans = ItemTransferTool.getDetailsByID(sTransID);
				vITD = new ArrayList(vTrans.size());
				for (int i = 0; i < vTrans.size(); i++)
				{
					ItemTransferDetail oDet = (ItemTransferDetail) vTrans.get(i);
					ItemTransferDetail oNewDet = oDet.copy();
					vITD.add(oNewDet);
				}
				oSes.setAttribute (s_IT, oIT);
				oSes.setAttribute (s_ITD, vITD);
			}
			ItemTransferTool.setHeader(data, oIT);		
		}
		catch (Exception _oEx)
		{
			throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
		}
	}
	
    /**
     * generate Transfer data to binary or XML
     * 
     * @param data
     * @throws Exception
     */
    
	/**
	 * Add items from query string, we sent item id in 00123,00124 
	 * 
	 * @param data
	 * @throws Exception
	 */
	protected void addItems(RunData data)
		throws Exception
	{
		//save current updated qty first
		ItemTransferTool.updateDetail(data, vITD);
		
		String sItemID = data.getParameters().getString ("items");		
		log.debug (sItemID);
		if (sItemID != null && !sItemID.equals(""))
		{
			if (sItemID.indexOf(',') > 0 )
			{
				StringBuilder oTemp = new StringBuilder(sItemID);
				while (oTemp.indexOf(",") > 0 ) 
				{	
					int iLastCommaPosition = oTemp.indexOf(",");
					sItemID = oTemp.substring (0, iLastCommaPosition);
					oTemp.delete (0,iLastCommaPosition + 1);
					processAddItem (sItemID, data);
				}
			}
			else 
			{
				processAddItem (sItemID, data);
			}
		}
		ItemTransferTool.setHeader(data, oIT);
	}
	
	protected void processAddItem ( String _sItemID, RunData data)
		throws Exception
	{
		ItemTransferDetail oDetail = new ItemTransferDetail();
		Item oItem = ItemTool.getItemByID(_sItemID);
		if (oItem != null)
		{
			String sLocID = data.getParameters().getString("LocationId");
			InventoryLocation oIL = InventoryLocationTool.getDataByItemAndLocationID(_sItemID, sLocID);
			double dCost = 0;
			if(oIL != null)
			{
				oDetail.setQtyOH(oIL.getCurrentQty());
				oDetail.setCostPerUnit(oIL.getItemCost());	
				dCost = oIL.getItemCost().doubleValue();
				if (dCost == 0)
				{
					dCost = oItem.getLastPurchasePrice().doubleValue(); //* oItem.getLastPurchaseRate().doubleValue();
					if(oItem.getLastPurchaseRate().doubleValue() != 0)
					{
						dCost = dCost * oItem.getLastPurchaseRate().doubleValue();
					}
				}
			}				
			oDetail.setItemId(oItem.getItemId());
			oDetail.setItemCode (oItem.getItemCode());
			oDetail.setQtyBase(bd_ONE);
			oDetail.setQtyChanges(bd_ONE);			
			oDetail.setCostPerUnit(new BigDecimal(dCost));
			oDetail.setUnitId(oItem.getUnitId());
			oDetail.setUnitCode(oItem.getUnit().getUnitCode());
		}
		else
		{
			return;
		}				
		
		if (!PreferenceTool.useBatchNo() && 
			!PreferenceTool.useSerialNo() && !isExist (oDetail)) 
		{
	    	if (b_INVENTORY_FIRST_LAST)
	    	{
				vITD.add (0, oDetail);
			}
			else
			{
				vITD.add (oDetail);
			}
		}
	}
}

package com.ssti.framework.tools;

import com.ssti.framework.om.CardioConfig;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2016-12-11
 * - add new variable value remote-local
 * - new method isPrinterRemoteLocal
 * - when using remote-local then we will not use RemotePrintHelper to send via 
 *   HttpClient to RemotePrintServlet, but we will use jQuery from browser direct
 *   to POSClient locahost:8080   
 * 
 * </pre><br>
 */
public class DeviceConfigTool 
{
    //REMOTE PROPERTIES
    public static final String s_LOCAL  = "local";
    public static final String s_REMOTE = "remote";
    public static final String s_REMOTE_LOCAL = "remote-local";
    
	private static CardioConfig getCardioConfig()
	{
		return CardioConfigTool.getCardioConfig();			
	}
	
	public static String getPrinterPort()
	{
		return getCardioConfig().getPrinterPort();
	}
	
	public static String getPrinterHost()
	{
		return getCardioConfig().getPrinterHost();
	}

	public static byte getPrinterPrintMode()
	{
		return Byte.valueOf(getCardioConfig().getPrinterPrintMode());
	}
	
	public static byte getPrinterPageLength()
	{
		return Byte.valueOf(getCardioConfig().getPrinterPageLength());
	}
	
	public static boolean isPrinterRemote()
	{
		String sHost = getPrinterHost();
		if (StringUtil.isEqual(sHost, s_REMOTE))
		{
			return true;
		}
		return false;
	}
	
	public static boolean isPrinterRemoteLocal()
	{
		String sHost = getPrinterHost();
		if (StringUtil.isEqual(sHost, s_REMOTE_LOCAL))
		{
			return true;
		}
		return false;
	}
		
	public static boolean printerCutPaper()
	{
		return Boolean.valueOf(getCardioConfig().getPrinterCutpaper());
	}

	public static String printerCutPaperCommandStr()
	{
		return getCardioConfig().getPrinterCutpaperCmd();
	}
	
	public static byte[] printerCutPaperCommand()
	{
		return StringUtil.toByteArray(printerCutPaperCommandStr());
	}	
	
	//drawer
	public static boolean drawerUse()
	{
		return Boolean.valueOf(getCardioConfig().getDrawerUse());
	}

	public static String drawerPort()
	{
		return getCardioConfig().getDrawerPort();
	}

	public static String drawerType()
	{
		return getCardioConfig().getDrawerType();
	}
	
	public static String drawerOpenCommand()
	{
		return getCardioConfig().getDrawerOpenCommand();
	}

	//displayer
	public static boolean displayerUse()
	{
		return Boolean.valueOf(getCardioConfig().getDisplayerUse());
	}
	
	public static String getDisplayerPort()
	{
		return getCardioConfig().getDisplayerPort();
	}
	
	public static String getDisplayerHost()
	{
		return getCardioConfig().getDisplayerHost();
	}

	public static boolean isDisplayerRemote()
	{
		String sHost = getDisplayerHost();
		if(StringUtil.isEqual(sHost, s_REMOTE) || StringUtil.isEqual(sHost, s_REMOTE_LOCAL) )
		{
			return true;
		}
		return false;
	}
	
	public static String getDisplayerLine1()
	{
		return getCardioConfig().getDisplayerLine1Text();
	}
	
	public static String getDisplayerLine2()
	{
		return getCardioConfig().getDisplayerLine2Text();
	}
	
	public static String getDisplayerClearCmd()
	{
		return getCardioConfig().getDisplayerClearCmd();
	}

	public static String getDisplayerFlPref()
	{
		return getCardioConfig().getDisplayerFlPref();
	}

	public static String getDisplayerSlPref()
	{
		return getCardioConfig().getDisplayerSlPref();
	}
}

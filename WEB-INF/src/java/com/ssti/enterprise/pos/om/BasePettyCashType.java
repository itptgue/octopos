package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PettyCashType
 */
public abstract class BasePettyCashType extends BaseObject
{
    /** The Peer class */
    private static final PettyCashTypePeer peer =
        new PettyCashTypePeer();

        
    /** The value for the pettyCashTypeId field */
    private String pettyCashTypeId;
      
    /** The value for the incomingOutgoing field */
    private int incomingOutgoing;
      
    /** The value for the pettyCashCode field */
    private String pettyCashCode;
      
    /** The value for the description field */
    private String description;
                                                
    /** The value for the accountId field */
    private String accountId = "";
  
    
    /**
     * Get the PettyCashTypeId
     *
     * @return String
     */
    public String getPettyCashTypeId()
    {
        return pettyCashTypeId;
    }

                        
    /**
     * Set the value of PettyCashTypeId
     *
     * @param v new value
     */
    public void setPettyCashTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashTypeId, v))
              {
            this.pettyCashTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IncomingOutgoing
     *
     * @return int
     */
    public int getIncomingOutgoing()
    {
        return incomingOutgoing;
    }

                        
    /**
     * Set the value of IncomingOutgoing
     *
     * @param v new value
     */
    public void setIncomingOutgoing(int v) 
    {
    
                  if (this.incomingOutgoing != v)
              {
            this.incomingOutgoing = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PettyCashCode
     *
     * @return String
     */
    public String getPettyCashCode()
    {
        return pettyCashCode;
    }

                        
    /**
     * Set the value of PettyCashCode
     *
     * @param v new value
     */
    public void setPettyCashCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.pettyCashCode, v))
              {
            this.pettyCashCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PettyCashTypeId");
              fieldNames.add("IncomingOutgoing");
              fieldNames.add("PettyCashCode");
              fieldNames.add("Description");
              fieldNames.add("AccountId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PettyCashTypeId"))
        {
                return getPettyCashTypeId();
            }
          if (name.equals("IncomingOutgoing"))
        {
                return Integer.valueOf(getIncomingOutgoing());
            }
          if (name.equals("PettyCashCode"))
        {
                return getPettyCashCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PettyCashTypePeer.PETTY_CASH_TYPE_ID))
        {
                return getPettyCashTypeId();
            }
          if (name.equals(PettyCashTypePeer.INCOMING_OUTGOING))
        {
                return Integer.valueOf(getIncomingOutgoing());
            }
          if (name.equals(PettyCashTypePeer.PETTY_CASH_CODE))
        {
                return getPettyCashCode();
            }
          if (name.equals(PettyCashTypePeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PettyCashTypePeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPettyCashTypeId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getIncomingOutgoing());
            }
              if (pos == 2)
        {
                return getPettyCashCode();
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return getAccountId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PettyCashTypePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PettyCashTypePeer.doInsert((PettyCashType) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PettyCashTypePeer.doUpdate((PettyCashType) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key pettyCashTypeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPettyCashTypeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPettyCashTypeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPettyCashTypeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PettyCashType copy() throws TorqueException
    {
        return copyInto(new PettyCashType());
    }
  
    protected PettyCashType copyInto(PettyCashType copyObj) throws TorqueException
    {
          copyObj.setPettyCashTypeId(pettyCashTypeId);
          copyObj.setIncomingOutgoing(incomingOutgoing);
          copyObj.setPettyCashCode(pettyCashCode);
          copyObj.setDescription(description);
          copyObj.setAccountId(accountId);
  
                    copyObj.setPettyCashTypeId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PettyCashTypePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PettyCashType\n");
        str.append("-------------\n")
           .append("PettyCashTypeId      : ")
           .append(getPettyCashTypeId())
           .append("\n")
           .append("IncomingOutgoing     : ")
           .append(getIncomingOutgoing())
           .append("\n")
           .append("PettyCashCode        : ")
           .append(getPettyCashCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
        ;
        return(str.toString());
    }
}

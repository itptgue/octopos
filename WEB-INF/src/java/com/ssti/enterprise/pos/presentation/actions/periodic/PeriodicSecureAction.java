package com.ssti.enterprise.pos.presentation.actions.periodic;

import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.framework.presentation.SecureAction;

public class PeriodicSecureAction extends SecureAction 
	implements AppAttributes
{
	private static final String[] a_PERM = {"Create Periodic Transaction"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }	
}

package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PaymentEdcConfigMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PaymentEdcConfigMapBuilder";

    /**
     * Item
     * @deprecated use PaymentEdcConfigPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "payment_edc_config";
    }

  
    /**
     * payment_edc_config.CONFIG_ID
     * @return the column name for the CONFIG_ID field
     * @deprecated use PaymentEdcConfigPeer.payment_edc_config.CONFIG_ID constant
     */
    public static String getPaymentEdcConfig_ConfigId()
    {
        return "payment_edc_config.CONFIG_ID";
    }
  
    /**
     * payment_edc_config.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use PaymentEdcConfigPeer.payment_edc_config.PAYMENT_TERM_ID constant
     */
    public static String getPaymentEdcConfig_PaymentTermId()
    {
        return "payment_edc_config.PAYMENT_TERM_ID";
    }
  
    /**
     * payment_edc_config.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PaymentEdcConfigPeer.payment_edc_config.LOCATION_ID constant
     */
    public static String getPaymentEdcConfig_LocationId()
    {
        return "payment_edc_config.LOCATION_ID";
    }
  
    /**
     * payment_edc_config.MERCHANT_ID
     * @return the column name for the MERCHANT_ID field
     * @deprecated use PaymentEdcConfigPeer.payment_edc_config.MERCHANT_ID constant
     */
    public static String getPaymentEdcConfig_MerchantId()
    {
        return "payment_edc_config.MERCHANT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("payment_edc_config");
        TableMap tMap = dbMap.getTable("payment_edc_config");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("payment_edc_config.CONFIG_ID", "");
                          tMap.addColumn("payment_edc_config.PAYMENT_TERM_ID", "");
                          tMap.addColumn("payment_edc_config.LOCATION_ID", "");
                          tMap.addColumn("payment_edc_config.MERCHANT_ID", "");
          }
}

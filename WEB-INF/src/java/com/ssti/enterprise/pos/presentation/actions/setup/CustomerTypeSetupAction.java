package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.CustomerType;
import com.ssti.enterprise.pos.om.CustomerTypePeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class CustomerTypeSetupAction extends CustomerSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			CustomerType oCT = new CustomerType();
			setProperties(oCT, data);
			oCT.setCustomerTypeId (IDGenerator.generateSysID());			
	        oCT.save();
	        CustomerManager.getInstance().refreshCache(oCT);
        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
       	{
       	    if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			CustomerType oCT = CustomerTypeTool.getCustomerTypeByID(data.getParameters().getString("ID"));
			CustomerType oOld = oCT.copy();
			setProperties (oCT, data);
	        oCT.save();
	        CustomerManager.getInstance().refreshCache(oCT);
	        UpdateHistoryTool.createHistory(oOld, oCT, data.getUser().getName(), null);
	        data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("customer","customer_type_id",sID) && 
    		   SqlUtil.validateFKRef("price_list","customer_type_id",sID))
    		{
	    		CustomerType oCT = CustomerTypeTool.getCustomerTypeByID(sID);    			
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(CustomerTypePeer.CUSTOMER_TYPE_ID, sID);
	        	CustomerTypePeer.doDelete(oCrit);	        	
	        	CustomerManager.getInstance().refreshCache(oCT);
	        	UpdateHistoryTool.createDelHistory(oCT, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	 data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

    private void setProperties (CustomerType _oCustomerType, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oCustomerType);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
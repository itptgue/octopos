package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.PurchaseRequestMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BasePurchaseRequestPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "purchase_request";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(PurchaseRequestMapBuilder.CLASS_NAME);
    }

      /** the column name for the PURCHASE_REQUEST_ID field */
    public static final String PURCHASE_REQUEST_ID;
      /** the column name for the TRANSACTION_STATUS field */
    public static final String TRANSACTION_STATUS;
      /** the column name for the TRANSACTION_NO field */
    public static final String TRANSACTION_NO;
      /** the column name for the TRANSACTION_DATE field */
    public static final String TRANSACTION_DATE;
      /** the column name for the LAST_UPDATE_DATE field */
    public static final String LAST_UPDATE_DATE;
      /** the column name for the TOTAL_QTY field */
    public static final String TOTAL_QTY;
      /** the column name for the VENDOR_ID field */
    public static final String VENDOR_ID;
      /** the column name for the VENDOR_NAME field */
    public static final String VENDOR_NAME;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the REQUESTED_BY field */
    public static final String REQUESTED_BY;
      /** the column name for the APPROVED_BY field */
    public static final String APPROVED_BY;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
      /** the column name for the CONFIRM_DATE field */
    public static final String CONFIRM_DATE;
      /** the column name for the TRANSFER_TRANS_ID field */
    public static final String TRANSFER_TRANS_ID;
      /** the column name for the TRANSFER_BY field */
    public static final String TRANSFER_BY;
      /** the column name for the TRANSFER_DATE field */
    public static final String TRANSFER_DATE;
      /** the column name for the IS_PURCHASE field */
    public static final String IS_PURCHASE;
      /** the column name for the IS_REIMBURSE field */
    public static final String IS_REIMBURSE;
      /** the column name for the IS_EXTERNAL field */
    public static final String IS_EXTERNAL;
      /** the column name for the CROSS_ENTITY_ID field */
    public static final String CROSS_ENTITY_ID;
  
    static
    {
          PURCHASE_REQUEST_ID = "purchase_request.PURCHASE_REQUEST_ID";
          TRANSACTION_STATUS = "purchase_request.TRANSACTION_STATUS";
          TRANSACTION_NO = "purchase_request.TRANSACTION_NO";
          TRANSACTION_DATE = "purchase_request.TRANSACTION_DATE";
          LAST_UPDATE_DATE = "purchase_request.LAST_UPDATE_DATE";
          TOTAL_QTY = "purchase_request.TOTAL_QTY";
          VENDOR_ID = "purchase_request.VENDOR_ID";
          VENDOR_NAME = "purchase_request.VENDOR_NAME";
          LOCATION_ID = "purchase_request.LOCATION_ID";
          REQUESTED_BY = "purchase_request.REQUESTED_BY";
          APPROVED_BY = "purchase_request.APPROVED_BY";
          REMARK = "purchase_request.REMARK";
          CANCEL_BY = "purchase_request.CANCEL_BY";
          CANCEL_DATE = "purchase_request.CANCEL_DATE";
          CONFIRM_DATE = "purchase_request.CONFIRM_DATE";
          TRANSFER_TRANS_ID = "purchase_request.TRANSFER_TRANS_ID";
          TRANSFER_BY = "purchase_request.TRANSFER_BY";
          TRANSFER_DATE = "purchase_request.TRANSFER_DATE";
          IS_PURCHASE = "purchase_request.IS_PURCHASE";
          IS_REIMBURSE = "purchase_request.IS_REIMBURSE";
          IS_EXTERNAL = "purchase_request.IS_EXTERNAL";
          CROSS_ENTITY_ID = "purchase_request.CROSS_ENTITY_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(PurchaseRequestMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(PurchaseRequestMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  22;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.PurchaseRequest";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseRequestPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_PURCHASE))
        {
            Object possibleBoolean = criteria.get(IS_PURCHASE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_PURCHASE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_REIMBURSE))
        {
            Object possibleBoolean = criteria.get(IS_REIMBURSE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_REIMBURSE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_EXTERNAL))
        {
            Object possibleBoolean = criteria.get(IS_EXTERNAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_EXTERNAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
            
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(PURCHASE_REQUEST_ID);
          criteria.addSelectColumn(TRANSACTION_STATUS);
          criteria.addSelectColumn(TRANSACTION_NO);
          criteria.addSelectColumn(TRANSACTION_DATE);
          criteria.addSelectColumn(LAST_UPDATE_DATE);
          criteria.addSelectColumn(TOTAL_QTY);
          criteria.addSelectColumn(VENDOR_ID);
          criteria.addSelectColumn(VENDOR_NAME);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(REQUESTED_BY);
          criteria.addSelectColumn(APPROVED_BY);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
          criteria.addSelectColumn(CONFIRM_DATE);
          criteria.addSelectColumn(TRANSFER_TRANS_ID);
          criteria.addSelectColumn(TRANSFER_BY);
          criteria.addSelectColumn(TRANSFER_DATE);
          criteria.addSelectColumn(IS_PURCHASE);
          criteria.addSelectColumn(IS_REIMBURSE);
          criteria.addSelectColumn(IS_EXTERNAL);
          criteria.addSelectColumn(CROSS_ENTITY_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static PurchaseRequest row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            PurchaseRequest obj = (PurchaseRequest) cls.newInstance();
            PurchaseRequestPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      PurchaseRequest obj)
        throws TorqueException
    {
        try
        {
                obj.setPurchaseRequestId(row.getValue(offset + 0).asString());
                  obj.setTransactionStatus(row.getValue(offset + 1).asInt());
                  obj.setTransactionNo(row.getValue(offset + 2).asString());
                  obj.setTransactionDate(row.getValue(offset + 3).asUtilDate());
                  obj.setLastUpdateDate(row.getValue(offset + 4).asUtilDate());
                  obj.setTotalQty(row.getValue(offset + 5).asBigDecimal());
                  obj.setVendorId(row.getValue(offset + 6).asString());
                  obj.setVendorName(row.getValue(offset + 7).asString());
                  obj.setLocationId(row.getValue(offset + 8).asString());
                  obj.setRequestedBy(row.getValue(offset + 9).asString());
                  obj.setApprovedBy(row.getValue(offset + 10).asString());
                  obj.setRemark(row.getValue(offset + 11).asString());
                  obj.setCancelBy(row.getValue(offset + 12).asString());
                  obj.setCancelDate(row.getValue(offset + 13).asUtilDate());
                  obj.setConfirmDate(row.getValue(offset + 14).asUtilDate());
                  obj.setTransferTransId(row.getValue(offset + 15).asString());
                  obj.setTransferBy(row.getValue(offset + 16).asString());
                  obj.setTransferDate(row.getValue(offset + 17).asUtilDate());
                  obj.setIsPurchase(row.getValue(offset + 18).asBoolean());
                  obj.setIsReimburse(row.getValue(offset + 19).asBoolean());
                  obj.setIsExternal(row.getValue(offset + 20).asBoolean());
                  obj.setCrossEntityId(row.getValue(offset + 21).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseRequestPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_PURCHASE))
        {
            Object possibleBoolean = criteria.get(IS_PURCHASE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_PURCHASE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_REIMBURSE))
        {
            Object possibleBoolean = criteria.get(IS_REIMBURSE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_REIMBURSE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_EXTERNAL))
        {
            Object possibleBoolean = criteria.get(IS_EXTERNAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_EXTERNAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
            
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(PurchaseRequestPeer.row2Object(row, 1,
                PurchaseRequestPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BasePurchaseRequestPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(PURCHASE_REQUEST_ID, criteria.remove(PURCHASE_REQUEST_ID));
                                                                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_PURCHASE))
        {
            Object possibleBoolean = criteria.get(IS_PURCHASE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_PURCHASE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_REIMBURSE))
        {
            Object possibleBoolean = criteria.get(IS_REIMBURSE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_REIMBURSE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_EXTERNAL))
        {
            Object possibleBoolean = criteria.get(IS_EXTERNAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_EXTERNAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         PurchaseRequestPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_PURCHASE))
        {
            Object possibleBoolean = criteria.get(IS_PURCHASE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_PURCHASE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_REIMBURSE))
        {
            Object possibleBoolean = criteria.get(IS_REIMBURSE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_REIMBURSE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_EXTERNAL))
        {
            Object possibleBoolean = criteria.get(IS_EXTERNAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_EXTERNAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(PurchaseRequest obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseRequest obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseRequest obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseRequest obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(PurchaseRequest) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseRequest obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(PurchaseRequest) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseRequest obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(PurchaseRequest) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseRequest obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BasePurchaseRequestPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(PURCHASE_REQUEST_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( PurchaseRequest obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(PURCHASE_REQUEST_ID, obj.getPurchaseRequestId());
              criteria.add(TRANSACTION_STATUS, obj.getTransactionStatus());
              criteria.add(TRANSACTION_NO, obj.getTransactionNo());
              criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
              criteria.add(LAST_UPDATE_DATE, obj.getLastUpdateDate());
              criteria.add(TOTAL_QTY, obj.getTotalQty());
              criteria.add(VENDOR_ID, obj.getVendorId());
              criteria.add(VENDOR_NAME, obj.getVendorName());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(REQUESTED_BY, obj.getRequestedBy());
              criteria.add(APPROVED_BY, obj.getApprovedBy());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
              criteria.add(CONFIRM_DATE, obj.getConfirmDate());
              criteria.add(TRANSFER_TRANS_ID, obj.getTransferTransId());
              criteria.add(TRANSFER_BY, obj.getTransferBy());
              criteria.add(TRANSFER_DATE, obj.getTransferDate());
              criteria.add(IS_PURCHASE, obj.getIsPurchase());
              criteria.add(IS_REIMBURSE, obj.getIsReimburse());
              criteria.add(IS_EXTERNAL, obj.getIsExternal());
              criteria.add(CROSS_ENTITY_ID, obj.getCrossEntityId());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( PurchaseRequest obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(PURCHASE_REQUEST_ID, obj.getPurchaseRequestId());
                          criteria.add(TRANSACTION_STATUS, obj.getTransactionStatus());
                          criteria.add(TRANSACTION_NO, obj.getTransactionNo());
                          criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
                          criteria.add(LAST_UPDATE_DATE, obj.getLastUpdateDate());
                          criteria.add(TOTAL_QTY, obj.getTotalQty());
                          criteria.add(VENDOR_ID, obj.getVendorId());
                          criteria.add(VENDOR_NAME, obj.getVendorName());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(REQUESTED_BY, obj.getRequestedBy());
                          criteria.add(APPROVED_BY, obj.getApprovedBy());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
                          criteria.add(CONFIRM_DATE, obj.getConfirmDate());
                          criteria.add(TRANSFER_TRANS_ID, obj.getTransferTransId());
                          criteria.add(TRANSFER_BY, obj.getTransferBy());
                          criteria.add(TRANSFER_DATE, obj.getTransferDate());
                          criteria.add(IS_PURCHASE, obj.getIsPurchase());
                          criteria.add(IS_REIMBURSE, obj.getIsReimburse());
                          criteria.add(IS_EXTERNAL, obj.getIsExternal());
                          criteria.add(CROSS_ENTITY_ID, obj.getCrossEntityId());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseRequest retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseRequest retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseRequest retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        PurchaseRequest retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseRequest retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (PurchaseRequest)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( PURCHASE_REQUEST_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to BankTransfer
 */
public abstract class BaseBankTransfer extends BaseObject
{
    /** The Peer class */
    private static final BankTransferPeer peer =
        new BankTransferPeer();

        
    /** The value for the bankTransferId field */
    private String bankTransferId;
      
    /** The value for the transNo field */
    private String transNo;
      
    /** The value for the transDate field */
    private Date transDate;
      
    /** The value for the outBankId field */
    private String outBankId;
      
    /** The value for the outAccId field */
    private String outAccId;
      
    /** The value for the outLocId field */
    private String outLocId;
      
    /** The value for the outCftId field */
    private String outCftId;
      
    /** The value for the outCfId field */
    private String outCfId;
      
    /** The value for the inBankId field */
    private String inBankId;
      
    /** The value for the inAccId field */
    private String inAccId;
      
    /** The value for the inLocId field */
    private String inLocId;
      
    /** The value for the inCftId field */
    private String inCftId;
      
    /** The value for the inCfId field */
    private String inCfId;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the status field */
    private int status;
                                                
    /** The value for the description field */
    private String description = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the BankTransferId
     *
     * @return String
     */
    public String getBankTransferId()
    {
        return bankTransferId;
    }

                        
    /**
     * Set the value of BankTransferId
     *
     * @param v new value
     */
    public void setBankTransferId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankTransferId, v))
              {
            this.bankTransferId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransNo
     *
     * @return String
     */
    public String getTransNo()
    {
        return transNo;
    }

                        
    /**
     * Set the value of TransNo
     *
     * @param v new value
     */
    public void setTransNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transNo, v))
              {
            this.transNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransDate
     *
     * @return Date
     */
    public Date getTransDate()
    {
        return transDate;
    }

                        
    /**
     * Set the value of TransDate
     *
     * @param v new value
     */
    public void setTransDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transDate, v))
              {
            this.transDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OutBankId
     *
     * @return String
     */
    public String getOutBankId()
    {
        return outBankId;
    }

                        
    /**
     * Set the value of OutBankId
     *
     * @param v new value
     */
    public void setOutBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.outBankId, v))
              {
            this.outBankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OutAccId
     *
     * @return String
     */
    public String getOutAccId()
    {
        return outAccId;
    }

                        
    /**
     * Set the value of OutAccId
     *
     * @param v new value
     */
    public void setOutAccId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.outAccId, v))
              {
            this.outAccId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OutLocId
     *
     * @return String
     */
    public String getOutLocId()
    {
        return outLocId;
    }

                        
    /**
     * Set the value of OutLocId
     *
     * @param v new value
     */
    public void setOutLocId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.outLocId, v))
              {
            this.outLocId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OutCftId
     *
     * @return String
     */
    public String getOutCftId()
    {
        return outCftId;
    }

                        
    /**
     * Set the value of OutCftId
     *
     * @param v new value
     */
    public void setOutCftId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.outCftId, v))
              {
            this.outCftId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OutCfId
     *
     * @return String
     */
    public String getOutCfId()
    {
        return outCfId;
    }

                        
    /**
     * Set the value of OutCfId
     *
     * @param v new value
     */
    public void setOutCfId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.outCfId, v))
              {
            this.outCfId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InBankId
     *
     * @return String
     */
    public String getInBankId()
    {
        return inBankId;
    }

                        
    /**
     * Set the value of InBankId
     *
     * @param v new value
     */
    public void setInBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inBankId, v))
              {
            this.inBankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InAccId
     *
     * @return String
     */
    public String getInAccId()
    {
        return inAccId;
    }

                        
    /**
     * Set the value of InAccId
     *
     * @param v new value
     */
    public void setInAccId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inAccId, v))
              {
            this.inAccId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InLocId
     *
     * @return String
     */
    public String getInLocId()
    {
        return inLocId;
    }

                        
    /**
     * Set the value of InLocId
     *
     * @param v new value
     */
    public void setInLocId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inLocId, v))
              {
            this.inLocId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InCftId
     *
     * @return String
     */
    public String getInCftId()
    {
        return inCftId;
    }

                        
    /**
     * Set the value of InCftId
     *
     * @param v new value
     */
    public void setInCftId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inCftId, v))
              {
            this.inCftId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InCfId
     *
     * @return String
     */
    public String getInCfId()
    {
        return inCfId;
    }

                        
    /**
     * Set the value of InCfId
     *
     * @param v new value
     */
    public void setInCfId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inCfId, v))
              {
            this.inCfId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BankTransferId");
              fieldNames.add("TransNo");
              fieldNames.add("TransDate");
              fieldNames.add("OutBankId");
              fieldNames.add("OutAccId");
              fieldNames.add("OutLocId");
              fieldNames.add("OutCftId");
              fieldNames.add("OutCfId");
              fieldNames.add("InBankId");
              fieldNames.add("InAccId");
              fieldNames.add("InLocId");
              fieldNames.add("InCftId");
              fieldNames.add("InCfId");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("Amount");
              fieldNames.add("AmountBase");
              fieldNames.add("UserName");
              fieldNames.add("Status");
              fieldNames.add("Description");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BankTransferId"))
        {
                return getBankTransferId();
            }
          if (name.equals("TransNo"))
        {
                return getTransNo();
            }
          if (name.equals("TransDate"))
        {
                return getTransDate();
            }
          if (name.equals("OutBankId"))
        {
                return getOutBankId();
            }
          if (name.equals("OutAccId"))
        {
                return getOutAccId();
            }
          if (name.equals("OutLocId"))
        {
                return getOutLocId();
            }
          if (name.equals("OutCftId"))
        {
                return getOutCftId();
            }
          if (name.equals("OutCfId"))
        {
                return getOutCfId();
            }
          if (name.equals("InBankId"))
        {
                return getInBankId();
            }
          if (name.equals("InAccId"))
        {
                return getInAccId();
            }
          if (name.equals("InLocId"))
        {
                return getInLocId();
            }
          if (name.equals("InCftId"))
        {
                return getInCftId();
            }
          if (name.equals("InCfId"))
        {
                return getInCfId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BankTransferPeer.BANK_TRANSFER_ID))
        {
                return getBankTransferId();
            }
          if (name.equals(BankTransferPeer.TRANS_NO))
        {
                return getTransNo();
            }
          if (name.equals(BankTransferPeer.TRANS_DATE))
        {
                return getTransDate();
            }
          if (name.equals(BankTransferPeer.OUT_BANK_ID))
        {
                return getOutBankId();
            }
          if (name.equals(BankTransferPeer.OUT_ACC_ID))
        {
                return getOutAccId();
            }
          if (name.equals(BankTransferPeer.OUT_LOC_ID))
        {
                return getOutLocId();
            }
          if (name.equals(BankTransferPeer.OUT_CFT_ID))
        {
                return getOutCftId();
            }
          if (name.equals(BankTransferPeer.OUT_CF_ID))
        {
                return getOutCfId();
            }
          if (name.equals(BankTransferPeer.IN_BANK_ID))
        {
                return getInBankId();
            }
          if (name.equals(BankTransferPeer.IN_ACC_ID))
        {
                return getInAccId();
            }
          if (name.equals(BankTransferPeer.IN_LOC_ID))
        {
                return getInLocId();
            }
          if (name.equals(BankTransferPeer.IN_CFT_ID))
        {
                return getInCftId();
            }
          if (name.equals(BankTransferPeer.IN_CF_ID))
        {
                return getInCfId();
            }
          if (name.equals(BankTransferPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(BankTransferPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(BankTransferPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(BankTransferPeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(BankTransferPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(BankTransferPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(BankTransferPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(BankTransferPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(BankTransferPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBankTransferId();
            }
              if (pos == 1)
        {
                return getTransNo();
            }
              if (pos == 2)
        {
                return getTransDate();
            }
              if (pos == 3)
        {
                return getOutBankId();
            }
              if (pos == 4)
        {
                return getOutAccId();
            }
              if (pos == 5)
        {
                return getOutLocId();
            }
              if (pos == 6)
        {
                return getOutCftId();
            }
              if (pos == 7)
        {
                return getOutCfId();
            }
              if (pos == 8)
        {
                return getInBankId();
            }
              if (pos == 9)
        {
                return getInAccId();
            }
              if (pos == 10)
        {
                return getInLocId();
            }
              if (pos == 11)
        {
                return getInCftId();
            }
              if (pos == 12)
        {
                return getInCfId();
            }
              if (pos == 13)
        {
                return getCurrencyId();
            }
              if (pos == 14)
        {
                return getCurrencyRate();
            }
              if (pos == 15)
        {
                return getAmount();
            }
              if (pos == 16)
        {
                return getAmountBase();
            }
              if (pos == 17)
        {
                return getUserName();
            }
              if (pos == 18)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 19)
        {
                return getDescription();
            }
              if (pos == 20)
        {
                return getCancelBy();
            }
              if (pos == 21)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BankTransferPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BankTransferPeer.doInsert((BankTransfer) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BankTransferPeer.doUpdate((BankTransfer) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key bankTransferId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBankTransferId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBankTransferId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBankTransferId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public BankTransfer copy() throws TorqueException
    {
        return copyInto(new BankTransfer());
    }
  
    protected BankTransfer copyInto(BankTransfer copyObj) throws TorqueException
    {
          copyObj.setBankTransferId(bankTransferId);
          copyObj.setTransNo(transNo);
          copyObj.setTransDate(transDate);
          copyObj.setOutBankId(outBankId);
          copyObj.setOutAccId(outAccId);
          copyObj.setOutLocId(outLocId);
          copyObj.setOutCftId(outCftId);
          copyObj.setOutCfId(outCfId);
          copyObj.setInBankId(inBankId);
          copyObj.setInAccId(inAccId);
          copyObj.setInLocId(inLocId);
          copyObj.setInCftId(inCftId);
          copyObj.setInCfId(inCfId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setAmount(amount);
          copyObj.setAmountBase(amountBase);
          copyObj.setUserName(userName);
          copyObj.setStatus(status);
          copyObj.setDescription(description);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setBankTransferId((String)null);
                                                                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BankTransferPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("BankTransfer\n");
        str.append("------------\n")
           .append("BankTransferId       : ")
           .append(getBankTransferId())
           .append("\n")
           .append("TransNo              : ")
           .append(getTransNo())
           .append("\n")
           .append("TransDate            : ")
           .append(getTransDate())
           .append("\n")
           .append("OutBankId            : ")
           .append(getOutBankId())
           .append("\n")
           .append("OutAccId             : ")
           .append(getOutAccId())
           .append("\n")
           .append("OutLocId             : ")
           .append(getOutLocId())
           .append("\n")
           .append("OutCftId             : ")
           .append(getOutCftId())
           .append("\n")
           .append("OutCfId              : ")
           .append(getOutCfId())
           .append("\n")
           .append("InBankId             : ")
           .append(getInBankId())
           .append("\n")
           .append("InAccId              : ")
           .append(getInAccId())
           .append("\n")
           .append("InLocId              : ")
           .append(getInLocId())
           .append("\n")
           .append("InCftId              : ")
           .append(getInCftId())
           .append("\n")
           .append("InCfId               : ")
           .append(getInCfId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

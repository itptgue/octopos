package com.ssti.framework.tools;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.google.gson.Gson;
import com.ssti.enterprise.pos.manager.AccountManager;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.framework.om.District;
import com.ssti.framework.om.DistrictPeer;
import com.ssti.framework.om.Province;
import com.ssti.framework.om.ProvincePeer;
import com.ssti.framework.om.Regency;
import com.ssti.framework.om.RegencyPeer;
import com.ssti.framework.om.Village;
import com.ssti.framework.om.VillagePeer;
import com.workingdogs.village.Record;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class AddressTool extends BaseTool
{
	static final Log log = LogFactory.getLog(AddressTool.class);
	static AddressTool instance = null;
	
    static CacheManager m_oManager;
    static Gson gson;
    private static final String s_CACHE = "master";    

    static final String s_ALL_PROV = "AllProvince";
    static final String s_REG = "Regency";
    static final String s_DIS = "District";
    static final String s_VIL = "Village";    
        
    private AddressTool() 
    {
        try 
        {
            m_oManager = CacheManager.create();
            gson = new Gson();
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }
    
	public static  AddressTool getInstance() 
	{
		if (instance == null)
		{
			synchronized (AccountManager.class) 
            {
				instance = new AddressTool();
            }
		}
		return instance;
	}	

	public List getAllProvince()
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get(s_ALL_PROV);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			List vData = ProvincePeer.doSelect(oCrit);
			oCache.put(new Element (s_ALL_PROV, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}

	public List getRegency(String provId)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get(s_REG + provId);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			if(StringUtil.isNotEmpty(provId))
			{
				oCrit.add(RegencyPeer.PROVINCE_ID, provId);
			}
			List vData = RegencyPeer.doSelect(oCrit);				
			oCache.put(new Element (s_REG + provId, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}

	public List getDistrict(String regId)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get(s_DIS + regId);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			if(StringUtil.isNotEmpty(regId))
			{
				oCrit.add(DistrictPeer.REGENCY_ID, regId);
			}
			List vData = DistrictPeer.doSelect(oCrit);				
			oCache.put(new Element (s_DIS + regId, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}

	public List getVillage(String disId)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get(s_VIL + disId);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(VillagePeer.DISTRICT_ID, disId);
			List vData = VillagePeer.doSelect(oCrit);				
			oCache.put(new Element (s_VIL + disId, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}
	
	public void clearCache()
	{
		Cache oCache = m_oManager.getCache(s_CACHE);
		List vKeys = oCache.getKeys();
		for(int i = 0; i < vKeys.size(); i++)
		{
			String sKey = (String) vKeys.get(i);
			if(StringUtil.isNotEmpty(sKey) && 
				(sKey.equals(s_ALL_PROV) || sKey.startsWith(s_REG) || sKey.startsWith(s_DIS) || sKey.startsWith(s_VIL)) ) 
			{
				oCache.remove(sKey);
			}
		}
	}
	
	public static Province getProvinceByID(String _sID)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(ProvincePeer.PROVINCE_ID, (Object)_sID, Criteria.ILIKE);
		System.out.println(c);
		List v = ProvincePeer.doSelect(c);
		if (v.size() > 0)
		{
			return (Province) v.get(0);
		}
		return null;		
	}

	public static Province getByProvince(String _sProvince)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(ProvincePeer.PROVINCE, (Object)(_sProvince+"%"), Criteria.ILIKE);
		System.out.println(c);
		List v = ProvincePeer.doSelect(c);
		if (v.size() > 0)
		{
			return (Province) v.get(0);
		}
		return null;		
	}
	
	public static Regency getRegencyByID(String _sID)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(RegencyPeer.REGENCY_ID, (Object)_sID, Criteria.ILIKE);
		List v = RegencyPeer.doSelect(c);
		if (v.size() > 0)
		{
			return (Regency) v.get(0);
		}
		return null;		
	}

	public static Regency getByRegency(String _sRegency)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(RegencyPeer.REGENCY, (Object)(_sRegency+"%"), Criteria.ILIKE);
		List v = RegencyPeer.doSelect(c);
		if (v.size() > 0)
		{
			return (Regency) v.get(0);
		}
		return null;		
	}

	public static District getDistrictByID(String _sID)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(DistrictPeer.DISTRICT_ID, (Object)_sID, Criteria.ILIKE);
		List v = DistrictPeer.doSelect(c);
		if (v.size() > 0)
		{
			return (District) v.get(0);
		}
		return null;		
	}

	public static District getByDistrict(String _sDistrict)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(DistrictPeer.DISTRICT, (Object)(_sDistrict+"%"), Criteria.ILIKE);
		List v = DistrictPeer.doSelect(c);
		if (v.size() > 0)
		{
			return (District) v.get(0);
		}
		return null;		
	}

	public static Village getVillageByID(String _sID)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(VillagePeer.VILLAGE_ID, (Object)_sID, Criteria.ILIKE);
		List v = VillagePeer.doSelect(c);
		if (v.size() > 0)
		{
			return (Village) v.get(0);
		}
		return null;		
	}

	public static Village getByVillage(String _sVillage)
		throws Exception 
	{
		Criteria c = new Criteria();
		c.add(VillagePeer.VILLAGE, (Object)(_sVillage+"%"), Criteria.ILIKE);
		List v = VillagePeer.doSelect(c);
		if (v.size() > 0)
		{
			return (Village) v.get(0);
		}
		return null;		
	}	

	public String getProvinceJSON()
		throws Exception
	{
		List v = getAllProvince();
		return gson.toJson(v);
	}

	public String getRegencyJSON(String provId)
		throws Exception
	{
		List v = getRegency(provId);
		return gson.toJson(v);
	}

	public String getDistrictJSON(String regId)
		throws Exception
	{
		List v = getDistrict(regId);
		return gson.toJson(v);
	}
	
	public String getVillageJSON(String disId)
		throws Exception
	{
		List v = getVillage(disId);
		return gson.toJson(v);
	}

	//-------------------------------------------------------------------------
	// Object getter & setter
	//-------------------------------------------------------------------------

	public static String getProvStr(String s)
	{	
		try 
		{			
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				Province d = getProvinceByID(s);
				if(d != null) return d.getProvince();
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return s;	
	}

	public static String getCityStr(String s)
	{	
		try 
		{			
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				Regency d = AddressTool.getRegencyByID(s);
				if(d != null) return d.getRegency();
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return s;	
	}
	
	public static String getDistrictStr(String s)
	{	
		try 
		{			
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				District d = AddressTool.getDistrictByID(s);
				if(d != null) return d.getDistrict();
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return s;	
	}

	public static String getVillageStr(String s)
	{	
		try 
		{			
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				Village d = AddressTool.getVillageByID(s);
				if(d != null) return d.getVillage();
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return s;	
	}
	
	public static void setProvinceStr(Object o, String s)		
	{
		try 
		{
			String id = s;
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				Province data = AddressTool.getByProvince(s);
				if(data != null) id = data.getProvinceId();				
			}	
			BeanUtil.setProperty(o, "province", id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
	}

	public static void setRegencyStr(Object o, String s)
	{
		try 
		{
			String id = s;
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				Regency data = AddressTool.getByRegency(s);
				if(data != null) id = data.getRegencyId();				
			}	
			BeanUtil.setProperty(o, "city", id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
	}

	public static void setDistrictStr(Object o, String s)
	{
		try 
		{			
			String id = s;
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				District data = AddressTool.getByDistrict(s);
				if(data != null) id = data.getDistrictId();				
			}	
			BeanUtil.setProperty(o, "district", id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
	}
	
	public static void setVillageStr(Object o, String s)
	{
		try 
		{			
			String id = s;
			if(getInstance().getAllProvince().size() > 0 && StringUtil.isNotEmpty(s))
			{
				Village data = AddressTool.getByVillage(s);
				if(data != null) id = data.getVillageId();				
			}	
			BeanUtil.setProperty(o, "village", id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
	}

	//-------------------------------------------------------------------------
	// Raja Ongkir
	//-------------------------------------------------------------------------
	
	public static int getCityID(String _sRegencyID)
		throws Exception 
	{
		String sKey = s_REG + "_RO_"+ _sRegencyID;
		Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			String sSQL = "SELECT city_id from regency_ro WHERE regency_id = '" + _sRegencyID + "'";
			List v = SqlUtil.executeQuery(sSQL);
			if(v.size() > 0)
			{
				Record r = (Record) v.get(0);
				int iCityID = r.getValue("city_id").asInt();
				oCache.put(new Element (sKey, (Serializable) iCityID)); 
				return iCityID;
			}			
		}
		else 
		{
			return ((Integer) oElement.getValue()).intValue();
		}
		return 0;		
	}	
}

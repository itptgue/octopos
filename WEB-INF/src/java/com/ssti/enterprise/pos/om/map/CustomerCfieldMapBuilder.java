package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerCfieldMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerCfieldMapBuilder";

    /**
     * Item
     * @deprecated use CustomerCfieldPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_cfield";
    }

  
    /**
     * customer_cfield.CFIELD_ID
     * @return the column name for the CFIELD_ID field
     * @deprecated use CustomerCfieldPeer.customer_cfield.CFIELD_ID constant
     */
    public static String getCustomerCfield_CfieldId()
    {
        return "customer_cfield.CFIELD_ID";
    }
  
    /**
     * customer_cfield.FNO
     * @return the column name for the FNO field
     * @deprecated use CustomerCfieldPeer.customer_cfield.FNO constant
     */
    public static String getCustomerCfield_Fno()
    {
        return "customer_cfield.FNO";
    }
  
    /**
     * customer_cfield.FIELD_GROUP
     * @return the column name for the FIELD_GROUP field
     * @deprecated use CustomerCfieldPeer.customer_cfield.FIELD_GROUP constant
     */
    public static String getCustomerCfield_FieldGroup()
    {
        return "customer_cfield.FIELD_GROUP";
    }
  
    /**
     * customer_cfield.FIELD_NAME
     * @return the column name for the FIELD_NAME field
     * @deprecated use CustomerCfieldPeer.customer_cfield.FIELD_NAME constant
     */
    public static String getCustomerCfield_FieldName()
    {
        return "customer_cfield.FIELD_NAME";
    }
  
    /**
     * customer_cfield.FIELD_TYPE
     * @return the column name for the FIELD_TYPE field
     * @deprecated use CustomerCfieldPeer.customer_cfield.FIELD_TYPE constant
     */
    public static String getCustomerCfield_FieldType()
    {
        return "customer_cfield.FIELD_TYPE";
    }
  
    /**
     * customer_cfield.FIELD_VALUE
     * @return the column name for the FIELD_VALUE field
     * @deprecated use CustomerCfieldPeer.customer_cfield.FIELD_VALUE constant
     */
    public static String getCustomerCfield_FieldValue()
    {
        return "customer_cfield.FIELD_VALUE";
    }
  
    /**
     * customer_cfield.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use CustomerCfieldPeer.customer_cfield.UPDATE_DATE constant
     */
    public static String getCustomerCfield_UpdateDate()
    {
        return "customer_cfield.UPDATE_DATE";
    }
  
    /**
     * customer_cfield.LAST_UPDATE_BY
     * @return the column name for the LAST_UPDATE_BY field
     * @deprecated use CustomerCfieldPeer.customer_cfield.LAST_UPDATE_BY constant
     */
    public static String getCustomerCfield_LastUpdateBy()
    {
        return "customer_cfield.LAST_UPDATE_BY";
    }
  
    /**
     * customer_cfield.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use CustomerCfieldPeer.customer_cfield.CUSTOMER_TYPE_ID constant
     */
    public static String getCustomerCfield_CustomerTypeId()
    {
        return "customer_cfield.CUSTOMER_TYPE_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_cfield");
        TableMap tMap = dbMap.getTable("customer_cfield");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_cfield.CFIELD_ID", "");
                            tMap.addColumn("customer_cfield.FNO", Integer.valueOf(0));
                          tMap.addColumn("customer_cfield.FIELD_GROUP", "");
                          tMap.addColumn("customer_cfield.FIELD_NAME", "");
                            tMap.addColumn("customer_cfield.FIELD_TYPE", Integer.valueOf(0));
                          tMap.addColumn("customer_cfield.FIELD_VALUE", "");
                          tMap.addColumn("customer_cfield.UPDATE_DATE", new Date());
                          tMap.addColumn("customer_cfield.LAST_UPDATE_BY", "");
                          tMap.addColumn("customer_cfield.CUSTOMER_TYPE_ID", "");
          }
}

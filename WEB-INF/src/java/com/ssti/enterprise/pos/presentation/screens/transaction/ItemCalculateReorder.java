package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.ItemInventoryManager;
import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.framework.parser.FormulaParser;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

public class ItemCalculateReorder extends TransactionSecureScreen
{
	static final Integer i_SAFETY_DAY = Integer.valueOf(CONFIG.getInt("inventory.safetyday"));
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	String sStart = data.getParameters().getString("StartDate");
    	String sEnd = data.getParameters().getString("EndDate");
		Date dStart = CustomParser.parseDate(sStart);
		Date dEnd = CustomParser.parseDate(sEnd);
    	String sLocID = data.getParameters().getString("LocationId");
    	int iOp = data.getParameters().getInt("Op");
    	int iPage = data.getParameters().getInt("Page");
    	try 
    	{
    		if (iOp == 1 && dStart != null && dEnd != null)
    		{
    			//recalculate 
    			int iMinDays = data.getParameters().getInt("MinimumDays");
    			int iOrdDays = data.getParameters().getInt("ReorderDays");
    			int iMaxDays = data.getParameters().getInt("MaximumDays");
    			int iDays = DateUtil.getDateInRange(dStart, dEnd).size();
    			boolean bAll = data.getParameters().getBoolean("AllItems");
    			
                log.debug("Calculate: " + iMinDays + " " + iOrdDays + " " + iMaxDays + " " + iDays + " ALL: " + bAll);
                
                List vItems = null;
                
                if(bAll)
                {
                	vItems = getInventoryItem();
                }
                else
                {
                	LargeSelect oLargeData = (LargeSelect) data.getUser().getTemp("findItemResult");
    				vItems = oLargeData.getPage(iPage);
                }
                log.debug("vItems: " + vItems.size());

                for (int i = 0; i < vItems.size(); i++)
    			{
    				Item oItem = (Item) vItems.get(i);
    				ItemInventory oInv = ItemInventoryTool.getItemInventory(oItem.getItemId(), sLocID);

                    log.debug("oInv: " + oInv);

                    double dAvgSales = ItemManager.getInstance().getAvgSales(oItem.getItemId(), sLocID, iDays, dEnd);
    				log.debug("Item " + oItem.getItemName() + " Sales Last " + iDays + 
    						  " Days To " + CustomFormatter.formatDate(dEnd) + " : " + dAvgSales);
    				
    				if (dAvgSales > 0)
    				{
                        log.debug("dAvgSales: " + dAvgSales);
                        
                        double dMin = iMinDays * dAvgSales;
    					double dOrd = iOrdDays * dAvgSales;
    					double dMax = iMaxDays * dAvgSales;
    					
                        if (dMin != 0 || dOrd != 0 || dMax != 0)
                        {
                            if (oInv == null)
                            {
                                oInv = new ItemInventory();
                                oInv.setItemInventoryId(IDGenerator.generateSysID());
                                oInv.setLocationId(sLocID);
                                oInv.setLocationName(LocationTool.getLocationNameByID(sLocID));
                                oInv.setItemId(oItem.getItemId());
                                oInv.setItemCode(oItem.getItemCode());
                                oInv.setMaximumQty(new BigDecimal(dMax));
                                oInv.setMinimumQty(new BigDecimal(dMin));
                                oInv.setReorderPoint(new BigDecimal(dOrd));
                                oInv.setRackLocation("");
                                oInv.setActive(true);
                                oInv.setUpdateDate(new Date());
                            }
                        }
                        
    					oInv.setMinimumQty(Calculator.roundUp(dMin,BigDecimal.ROUND_UP));
    					oInv.setReorderPoint(Calculator.roundUp(dOrd,BigDecimal.ROUND_UP));
    					oInv.setMaximumQty(Calculator.roundUp(dMax,BigDecimal.ROUND_UP));
    					oInv.save();

        				log.debug("Item Inv "  + oInv);

    					ItemInventoryManager.getInstance().refreshCache(oInv);
    				}
    			}
    		}
    		else
    		{
	    		if (StringUtil.isEmpty(sStart) && StringUtil.isEmpty(sEnd))
	    		{
		            String sParserCount = CONFIG.getString("inventory.safetystockformula");
		            FormulaParser oParser = new FormulaParser(sParserCount);
		            Date[] aDate = oParser.getResult();
		            sStart = CustomFormatter.formatDate(aDate [FormulaParser.i_START]);
		            sEnd = CustomFormatter.formatDate(aDate [FormulaParser.i_END]);	            
	    		}
				context.put("start", sStart);
				context.put("end", sEnd);
				
				context.put("dStart", CustomParser.parseDate(sStart));
				context.put("dEnd", CustomParser.parseDate(sEnd));
	
				context.put("iSafetyDay", i_SAFETY_DAY);
    		}
		}
    	catch (Exception _oEx) 
    	{
            log.error(_oEx);
    		data.setMessage("Calculate Reorder Failed : " + _oEx.getMessage());
    	}
    }
    
    public static List getInventoryItem()
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(ItemPeer.ITEM_TYPE,i_INVENTORY_PART);
    	oCrit.add(ItemPeer.ITEM_STATUS, true);
    	return ItemPeer.doSelect(oCrit);
    }
}
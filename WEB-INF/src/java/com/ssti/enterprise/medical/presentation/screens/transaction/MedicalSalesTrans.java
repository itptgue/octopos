package com.ssti.enterprise.medical.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.MedicineSales;
import com.ssti.enterprise.medical.om.MedicineSalesDetail;
import com.ssti.enterprise.medical.om.PresPowderDetail;
import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.medical.om.PrescriptionDetail;
import com.ssti.enterprise.medical.tools.DoctorTool;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalAttributes;
import com.ssti.enterprise.medical.tools.MedicalSalesTool;
import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.medical.tools.PrescriptionTool;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.presentation.screens.transaction.SalesSecureScreen;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.enterprise.pos.tools.pwp.PWPTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.IDGenerator;
import com.ssti.framework.tools.SortingTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-06-16
 * - add txType & parentId field in MedicineSalesDetail to enable input pres & powder 
 *   directly from POS
 * - change method addItem, generate PrescriptionDetailId for i_PRES_NORMAL & i_PRES_POWDER (powder item only)  
 * - chamge method addPrescription, setTxType, setParentId for powder item
 * - add method getClosestPowderItem to get prescriptionDetailId to be used as parentId in 
 *   powder from POS  
 * 
 * 2017-02-27
 * - move addMedRecord to MedicalSalesTool
 * 
 * 2016-12-08
 * - change name from MedicineSalesTransaction
 * 
 * 2016-09-21
 * - change PWP logic using discount amount 
 *   1. Change method applyPWPDiscount to call PWPTool.updateDiscAmount
 *   
 * </pre><br>
 */
public class MedicalSalesTrans extends SalesSecureScreen implements MedicalAttributes
{
	protected static final Log log = LogFactory.getLog(MedicalSalesTrans.class);
	
	protected static final String[] a_PERM = {"Sales Transaction", "View Sales Invoice"};
	
    //start screen method
    protected static final int i_OP_ADD_DETAIL  = 1;
    protected static final int i_OP_DEL_DETAIL  = 2;
	protected static final int i_OP_NEW_TRANS   = 3;
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
	protected static final int i_OP_IMPORT_FROM_DO = 5;
	
	protected static final int i_OP_FROM_REG = 9;
	protected static final int i_OP_REFRESH_DETAIL = 10;
	protected static final int i_OP_REFRESH_MASTER = 11; //only when no promo exists if promo then should refresh detail
	
	protected String m_sClientIP = "";    
    protected MedicineSales oTR = null;    
    protected List vTD = null;
    protected List vPMT = null;
    
    protected HttpSession oSes;
    protected boolean b_UPDATEABLE_DETAIL = false; 
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	m_sClientIP = data.getRemoteAddr();
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {	    	
    	super.doBuildTemplate(data, context);
    	
    	int iOp = data.getParameters().getInt("op");
		b_UPDATEABLE_DETAIL = data.getParameters().getBoolean("bUpdateDetail", false);
		initSession (data);
		
		try 
		{
	    	//validate open cashier
	    	CashierBalanceTool.validateOpenCashier(data, context);
	    	
			if (iOp == i_OP_ADD_DETAIL && !b_REFRESH) {
				addDetailData (data);
			}	
			else if (iOp == i_OP_DEL_DETAIL && !b_REFRESH) {
				delDetailData (data);
			}	
			else if ( iOp == i_OP_NEW_TRANS ) {
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS ) {
				getData (data);
			}
			else if (iOp == i_OP_FROM_REG ) {
				
			}
			else if (iOp == i_OP_REFRESH_DETAIL && !b_REFRESH) {
				refreshDetail (data);
			}	
			else if (iOp == i_OP_REFRESH_MASTER && !b_REFRESH) {
				refreshMaster (data);
			}		
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage("ERROR : " + _oEx.getMessage());
		}

    	context.put(s_MTD, oSes.getAttribute (s_MTD));
    	context.put(s_MTR, oSes.getAttribute (s_MTR));
    	context.put(s_PMT, oSes.getAttribute (s_PMT));
    	
    	//set Invoice related variable in context
    	setInvoiceContext(data, context);
    	
    	//set med related variable & tools in context
    	MedConfigTool.setContextTool(context);
    }

	protected void initSession ( RunData data )
	{	
		oSes = data.getSession();
		if (oSes.getAttribute (s_MTR) == null) 
		{
			createNewTrans(data);
		}
		oTR = (MedicineSales) oSes.getAttribute (s_MTR);
		vTD = (List) oSes.getAttribute (s_MTD);	
		vPMT = (List) oSes.getAttribute (s_PMT);
	}

	/**
     * refresh detail and clear discount to reflect payment type change
     * 
     * @param data
     * @throws Exception
     */
	private void refreshMaster(RunData data)
		throws Exception
	{
        if(b_UPDATEABLE_DETAIL) MedicalSalesTool.updateDetail(vTD, data);
        data.getParameters().setProperties(oTR);	
						
        MedicalSalesTool.setHeaderProperties(oTR, vTD, data);
		oSes.setAttribute (s_TD, vTD); 
		oSes.setAttribute (s_TR, oTR);     
	}
	
    protected void addDetailData ( RunData data ) 
    	throws Exception
    {
        data.getParameters().setProperties (oTR);	
        
        int iItemType = data.getParameters().getInt("ItemType");
        if (iItemType == MedicalSalesTool.i_PRS_TYPE)
        {
        	//set header with null data so if pres discount exist not overriden
			String sPRSID = data.getParameters().getString("ItemId");
        	addPrescription(sPRSID); 
        	MedicalSalesTool.setHeaderProperties (oTR, vTD, null);
        }
        else
        {
        	addItem(data);
        	MedicalSalesTool.setHeaderProperties (oTR, vTD, data);
        }
		oSes.setAttribute (s_MTD, vTD); 
		oSes.setAttribute (s_MTR, oTR);     
    }
    
    void addItem(RunData data)
    	throws Exception
    {
    	String sItemID = data.getParameters().getString("ItemId");
    	int iTxType = data.getParameters().getInt("TxType", i_NON_PRES);
    	if (StringUtil.isNotEmpty(sItemID))
    	{
			Item oItem = ItemTool.getItemByID(sItemID);
			if (oItem != null)
			{
				MedicineSalesDetail oTD = new MedicineSalesDetail();
				data.getParameters().setProperties (oTD);	
				String sItemName = data.getParameters().getString("ItemName");
				if (!StringUtil.isEqual(sItemName,oItem.getItemName()))
				{
					oTD.setDescription(sItemName);
				}
				
				oTD.setItemCode (oItem.getItemCode());
				oTD.setItemName(oItem.getItemName());				
				//oTD.setDescription(oItem.getDescription());
				oTD.setTxType(iTxType);
				oTD.setQoh(data.getParameters().getBigDecimal("currentQty",bd_ZERO));
				
				if (oTD.getQtyBase() == null || oTD.getQtyBase().equals(bd_ZERO)) 
				{
					oTD.setQtyBase (UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), oTD.getQty()));
				}
				
				if(iTxType == i_PRES_NORMAL) //add prescription directly from POS not via Prescription Module
				{
					oTD.setPrescriptionDetailId(IDGenerator.generateSysID()); //generate prescription_detail_id					
				}
				if(iTxType == i_PRES_POWDER) //add powder directly from POS not via Prescription Module
				{					
					if(PowderTool.isPowderItem(oTD.getItemId()))
					{
						oTD.setPrescriptionDetailId(IDGenerator.generateSysID()); //generate presc_detail_id 
						oTD.setParentId(""); //powder item has empty parent
						oTD.setRfee( Double.toString(PowderTool.calculateRfee(oTD.getItemId(), oTD.getQty().doubleValue())) );
						oTD.setPackaging ( Double.toString(PowderTool.calculatePack(oTD.getItemId(), oTD.getQty().doubleValue())) );
						oTD.setIndexNo(countPowder() + 1);
					}
					else
					{
						//get nearest Powder Item
						oTD.setParentId(getClosestPowderItem());
						if(StringUtil.empty(oTD.getParentId()))
						{
							throw new Exception(LocaleTool.getString("powder_pack_item") + " " + LocaleTool.getString("may_not_empty"));
						}
						oTD.setPrescriptionDetailId(oTD.getParentId()); //use closest powder_item presc_detail_id
						oTD.setIndexNo(countPowder());
					}
				}
				
				//validate price first before add into detail
				TransactionTool.validatePrice(oTD.getItemId(), oTD.getItemPrice().doubleValue(), 
					oTD.getDiscount(), oTR.getLocationId(), oTR.getCustomerId());
				
				//System.out.println(oTD);
				
				if (!isExist(oTD, data)) 
				{
				    if (b_SALES_FIRST_LAST)
				    {
					    vTD.add (0, oTD);
					}
					else
					{
					    vTD.add (oTD);
					}
				}   
			}
    	}
    }
    
    void addPrescription(String _sID)
		throws Exception
	{		 
		if (StringUtil.isNotEmpty(_sID))
		{
	    	Prescription oPRS = PrescriptionTool.getHeaderByID(_sID, null);
			if (oPRS != null)
			{
				oTR.setPatientName(oPRS.getPatientName());
				oTR.setPatientAge(oPRS.getPatientAge());
				oTR.setPatientPhone(oPRS.getPatientPhone());
				oTR.setPatientAddress(oPRS.getPatientAddress());
				oTR.setPatientWeight(oPRS.getPatientWeight());
				oTR.setSalesId(oPRS.getDoctorId());
				oTR.setDoctorName(DoctorTool.getDoctorNameByID(oPRS.getDoctorId()));
				List vPRSD = PrescriptionTool.getDetailsByID(_sID, null);
				for (int i = 0; i < vPRSD.size(); i++)
				{
					PrescriptionDetail oPRSD = (PrescriptionDetail) vPRSD.get(i);				
					List vPWDD = oPRSD.getPresPowderDetails();
					vPWDD = SortingTool.sort(vPWDD,"presPowderDetailId");
					if (vPWDD != null && vPWDD.size() > 0)
					{
						//calculate between request qty and buy qty						 
						for (int j = 0; j < vPWDD.size(); j++)
						{
							MedicineSalesDetail oTD = new MedicineSalesDetail();
							PresPowderDetail oPWDD = (PresPowderDetail) vPWDD.get(j);
							
							BeanUtil.setBean2BeanProperties(oPWDD, oTD);							
							BigDecimal bdBilled = oPWDD.getBilledQty();
							if(PowderTool.isPowderItem(oTD.getItemId()))
							{
								bdBilled = oPRSD.getQty();
								oTD.setParentId("");
								oTD.setRfee(oPRSD.getRfee());
								oTD.setPackaging(oPRSD.getPackaging());		
								oTD.setDosage(oPRSD.getDosage());
								oTD.setBeforeAfter(oPRSD.getBeforeAfter());
								oTD.setInstruction(oPRSD.getInstruction());								
							}
							else //set parent id = prescription_detail_id for powder component
							{
								oTD.setParentId(oTD.getPrescriptionDetailId());								
							}							
							 
							oTD.setDescription(oPRSD.getDescription());
							oTD.setItemType(MedicalSalesTool.i_PRS_TYPE);
							oTD.setQty(bdBilled);
							oTD.setQtyBase(bdBilled);
							oTD.setItemCost (bd_ZERO); //calculate later
							oTD.setTxType(i_PRES_POWDER);														
							
							//TAX
							Tax oTax = TaxTool.getDefaultSalesTax();
							oTD.setTaxId(oTax.getTaxId());
							oTD.setTaxAmount(oTax.getAmount());
									
							System.out.println("PWDD TYPE : " + oTD);
							vTD.add (oTD);							
						}
					}
					else
					{
						MedicineSalesDetail oTD = new MedicineSalesDetail();
	
						BeanUtil.setBean2BeanProperties(oPRSD, oTD);
						
						oTD.setItemType(MedicalSalesTool.i_PRS_TYPE);						
						oTD.setItemName (oPRSD.getItemName()); //previously oPRSD.getDesc
						oTD.setItemCost (bd_ZERO); //calculate later
						oTD.setTxType(i_PRES_NORMAL);
						
						//TAX
						Tax oTax = TaxTool.getDefaultSalesTax();
						oTD.setTaxId(oTax.getTaxId());
						oTD.setTaxAmount(oTax.getAmount());
						
						System.out.println("PRS TYPE : " + oTD);
						vTD.add (oTD);
					}
				}
				//set discount
				setDiscountFromPres(oPRS, true);
			}
		}
	}
    
    protected void delDetailData ( RunData data )
    	throws Exception
    {
    	int iNo = data.getParameters().getInt("No") - 1;
		if (iNo >= 0 && iNo < vTD.size()) 
		{
			MedicineSalesDetail oTD = (MedicineSalesDetail) vTD.get(iNo);
			String sPresID = oTD.getPrescriptionId();
			oTR.setTotalExpense(bd_ZERO);
			if(StringUtil.isNotEmpty(sPresID))
			{
				removePres(sPresID);
				MedicalSalesTool.setHeaderProperties (oTR, vTD, null);
			}
			else
			{
				if(PowderTool.isPowderItem(oTD.getItemId()) && StringUtil.isNotEmpty(oTD.getPrescriptionDetailId()))
				{
					removePowder(oTD); //remove MedPOS powder input via POS
				}
				else
				{
					vTD.remove (iNo);
				}
				MedicalSalesTool.setHeaderProperties (oTR, vTD, data);
			}
			oSes.setAttribute(s_MTD, vTD);
			oSes.setAttribute(s_MTR, oTR); 
		}    	
    }
    
    protected void removePres(String sPresID) throws Exception
    {
		Iterator iter = vTD.iterator();
		while(iter.hasNext())
		{
			MedicineSalesDetail oMTD = (MedicineSalesDetail)iter.next();										
			if (StringUtil.isEqual(sPresID,oMTD.getPrescriptionId()))
			{
				iter.remove();
			}
		}
		Prescription oPRS = PrescriptionTool.getHeaderByID(sPresID, null);
		setDiscountFromPres(oPRS, false);
		oTR.setPatientName("");
		oTR.setDoctorName("");
    }
    
    /**
     * powder from MedPOS prescription input
     * 
     * @param sPresID
     * @throws Exception
     */
    protected void removePowder(MedicineSalesDetail _oTD) throws Exception
    {
		Iterator iter = vTD.iterator();
		while(iter.hasNext())
		{
			MedicineSalesDetail oMTD = (MedicineSalesDetail)iter.next();										
			if (StringUtil.isEqual(oMTD.getParentId(),_oTD.getPrescriptionDetailId()) || 
				StringUtil.equals(oMTD.getPrescriptionDetailId(), _oTD.getPrescriptionDetailId()))
			{
				iter.remove();
			}			
		}
    }           
    
    protected void setDiscountFromPres(Prescription _oPRS, boolean _bAdd)
    {
		//set discount
		double dPresDisc = _oPRS.getTotalDiscount().doubleValue();
		if (dPresDisc > 0)
		{
			if (!_bAdd) dPresDisc = dPresDisc * -1;
			String sDiscPct = oTR.getTotalDiscountPct();	
			if (!StringUtil.contains(sDiscPct, "%"))
			{
				double dDiscPct= Double.parseDouble(sDiscPct);
				dDiscPct = dDiscPct + dPresDisc;
				oTR.setTotalDiscountPct(Double.toString(dDiscPct));
				oTR.setTotalDiscount(new BigDecimal(dDiscPct));
			}
		}
    }
    
    protected void createNewTrans ( RunData data ) 
    {
    	synchronized (this) 
    	{
        	oTR = new MedicineSales();
        	vTD = new ArrayList();
        	vPMT = new ArrayList();
        	
        	//initialize inclusive tax
        	oTR.setIsInclusiveTax(b_SALES_TAX_INCLUSIVE);
        	
        	oSes.setAttribute (s_MTR,  oTR);
    		oSes.setAttribute (s_MTD,  vTD);
    		oSes.setAttribute (s_PMT, vPMT);    		
		}
    }
    
    protected boolean isExist (MedicineSalesDetail _oTD, RunData data) 
    	throws Exception
    {
    	boolean bIsExist = true; 
    	String sCustomerID = data.getParameters().getString("CustomerId","");
    	for (int i = 0; i < vTD.size(); i++) 
    	{
    		MedicineSalesDetail oExistTD = (MedicineSalesDetail) vTD.get (i);
    		if ( oExistTD.getTxType() == _oTD.getTxType() &&
    			 oExistTD.getItemId().equals(_oTD.getItemId()) && 
    		     oExistTD.getUnitId().equals(_oTD.getUnitId()) &&
    		     oExistTD.getTaxId().equals(_oTD.getTaxId()) &&
     		    (oExistTD.getDiscount().equals(_oTD.getDiscount()) || oExistTD.isQtyDiscount() || oExistTD.isPwpDiscount() || oExistTD.isMultiUnit()) &&     		    
     		    !oExistTD.isSpecialDiscByAmount() &&
 				(oExistTD.getItemPrice().intValue() == _oTD.getItemPrice().intValue()) &&    		     
    		     oExistTD.getPrescriptionId().equals(_oTD.getPrescriptionId()) &&
    		     oExistTD.getPrescriptionDetailId().equals(_oTD.getPrescriptionDetailId()) &&
				(oExistTD.getItemPrice().doubleValue() == _oTD.getItemPrice().doubleValue()) &&
   		     	 StringUtil.isEmpty(oExistTD.getDeliveryOrderDetailId()) ) 
    		{
       		    if(b_SALES_MERGE_DETAIL || oExistTD.isPwpDiscount() || oExistTD.isMultiUnit())
    		    {
				    mergeData (oExistTD, _oTD);
				    //apply discount setup to the existing detail data
				    //if discount on sales trans was 0 then apply Disc from discount setup
				    if(oExistTD.getDiscount().equals("0"))
				    {
    			        applyDiscount (oTR, oExistTD, true); //apply discount after merge
    			    }
				}
				else
				{
	    	        //apply discount to the new detail data
	    	        //if discount on sales trans was 0 then apply Disc from discount setup
	    	        if(_oTD.getDiscount().equals("0"))
	    	        {
	    	            applyDiscount (oTR, _oTD, false);
	    	        }				
					bIsExist = false;
				}
				return bIsExist;    			
    		}		
    	}
    	
    	//if there is no same item on list or  no item in list exist
    	bIsExist = false;
    	//apply discount to the new detail data
    	//if discount on sales trans was 0 then apply Disc from discount setup
    	if(StringUtil.equals(_oTD.getDiscount(), "0") || 
    	   StringUtil.equals(_oTD.getDiscount(), "0.00"))
    	{
    	    applyDiscount (oTR, _oTD, false);
    	}
    	return bIsExist;
    } 

    /**
     * 
     * @param _oTR
     * @param _oTD
     * @throws Exception
     */
    protected void applyDiscount(MedicineSales _oTR, MedicineSalesDetail _oTD, boolean _bMerge)
        throws Exception
    {
        String sLocID = _oTR.getLocationId();
        String sItemID = _oTD.getItemId();
        String sCustID = _oTR.getCustomerId();
        String sPTypeID = _oTR.getPaymentTypeId();
        Date dDate = _oTR.getTransactionDate();     
        
        log.debug("****** FIND DISCOUNT **********");
        log.debug("Loc   : " + LocationTool.getLocationCodeByID(sLocID));
        log.debug("Cust  : " + CustomerTool.getCodeByID(sCustID));
        log.debug("Item  : " + ItemTool.getItemCodeByID(sItemID));
        
        Discount oDisc =  DiscountTool.findDiscount(sLocID, sItemID, sCustID, sPTypeID, dDate);
        boolean bPWP = false;
        boolean bSKU = false;
        boolean bMUL = false;
        
        if (oDisc != null)
        {
            if (StringUtil.isNotEmpty(oDisc.getItemSku()))
            {
                bSKU = true;
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_PWP)
            {
                bPWP = true; 
            }
            if (oDisc.getDiscountType() == DiscountTool.i_BY_MULTI)
            {
            	bMUL = true; 
            }            
            if (bSKU)
            {
                applySKUDiscount(_oTD,oDisc,_bMerge);   
            }
            else if(bPWP)
            {
                applyPWPDiscount(_oTD,oDisc);
            }
            else if(bMUL)
            {
                applyMULDiscount(_oTD,oDisc);
            }            
            else
            {
                applySTDDiscount(_oTD,oDisc);
            }
            _oTD.setDiscountId(oDisc.getDiscountId());
        }
    }    
    
    private void applySKUDiscount(MedicineSalesDetail _oTD,
                                  Discount _oDisc,
                                  boolean _bMerge)
        throws Exception
    {
        log.debug ("****** SKU DISCOUNT **********");
        List vSKU = new ArrayList(vTD.size());
        double dTotalSKUQty = 0;
        for (int i = 0; i < vTD.size(); i++) 
        {
            MedicineSalesDetail oExistTD = (MedicineSalesDetail) vTD.get (i);
            Item oItem = ItemTool.getItemByID(oExistTD.getItemId());
            if (oItem.getItemSku().equals(_oDisc.getItemSku()))
            {
                vSKU.add(oExistTD);
                dTotalSKUQty += oExistTD.getQtyBase().doubleValue();
            }
        }
        if (!_bMerge)
        {
            vSKU.add(_oTD);
            dTotalSKUQty += _oTD.getQty().doubleValue();
        }
        for (int i = 0; i < vSKU.size(); i++) 
        {
            MedicineSalesDetail oExistTD = (MedicineSalesDetail) vSKU.get (i);
            oExistTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, dTotalSKUQty));
            recalculateTD(oExistTD);
        }
    }
    
    private void applyPWPDiscount(MedicineSalesDetail _oTD,Discount _oDisc)
        throws Exception
    {
        log.debug ("****** PWP DISCOUNT **********");
        PWPTool.updateGetDiscAmount(_oDisc, vTD, _oTD);
        recalculateTD(_oTD);
    }

    private void applySTDDiscount(MedicineSalesDetail _oTD,Discount _oDisc)
        throws Exception
    {
        log.debug ("****** NORMAL DISCOUNT **********");        
        if(_oTD.getQtyBase() == null) { _oTD.setQtyBase(UnitTool.getBaseQty(_oTD.getItemId(), _oTD.getUnitId(), _oTD.getQty()));}
        _oTD.setDiscount (DiscountTool.getDiscountAmount(_oDisc, _oTD.getQtyBase().doubleValue()));
        recalculateTD(_oTD);
    }

    /**
     * apply multi unit discount validate with existing buy qty
     * 
     * @param _oTD
     * @throws Exception
     */
    private void applyMULDiscount(MedicineSalesDetail _oTD,Discount _oDisc)
    	throws Exception
    {    	    	
    	log.debug ("****** MULTI UNIT DISCOUNT **********");
		if(_oDisc != null)		
		{			
			int iBuy = _oDisc.getBuyQty().intValue();
			int iGet = _oDisc.getGetQty().intValue();
			int iPack = iBuy + iGet;
			int iQty = _oTD.getQty().intValue(); 
			
			int iAvail = (int) (iQty / iPack);
			int iDisc = iAvail * iGet;
			double dAmt = Calculator.mul(_oTD.getItemPrice(),iDisc);			
			
			_oTD.setDiscount(new BigDecimal(dAmt).toString());
			_oTD.setDiscountId(_oDisc.getDiscountId());	
			recalculateTD(_oTD);
		}
	}
	
    private void recalculateTD(MedicineSalesDetail _oTD)
        throws Exception
    {        
        double dSubTotal =  _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();            
        double dSubTotalDisc = 0;
        dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), new Double(dSubTotal));            
        dSubTotal = dSubTotal - dSubTotalDisc;
        double dSubTotalTax = (_oTD.getTaxAmount().doubleValue() / 100) * dSubTotal;
        
        //dSubTotal = dSubTotal;//+ dSubTotalTax;
        _oTD.setSubTotalDisc (new BigDecimal (dSubTotalDisc));
        _oTD.setSubTotalTax  (new BigDecimal (dSubTotalTax));
        _oTD.setSubTotal     (new BigDecimal (dSubTotal));
        
        log.debug("Discount : " + _oTD.getDiscount());
        log.debug("SubTotal : " + _oTD.getSubTotal());
    }      
    
    protected void mergeData (MedicineSalesDetail _oOldTD, MedicineSalesDetail _oNewTD) 
    	throws Exception
    {		
    	double dQty 		= _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
    	
    	String sDiscount 	= _oNewTD.getDiscount();
    	double dTotalSales  = dQty * _oNewTD.getItemPrice().doubleValue();
    	double dTotalDisc   = Calculator.calculateDiscount(sDiscount,dTotalSales);
    	double dTaxAmount   = _oNewTD.getTaxAmount().doubleValue()/100;
    	
    	dTotalSales 		 = dTotalSales - dTotalDisc;
    	double dTotalTax 	 = dTotalSales * dTaxAmount;
    	double dSubTotal 	 = dTotalSales; //+ dTotalTax;
    	double dSubTotalCost = dQty * _oNewTD.getItemCost().doubleValue();
    	
    	//return RFEE to percentage or default mode so it will be recalculated 
    	//by set header properties
    	_oOldTD.setRfee			( _oNewTD.getRfee());
    	_oOldTD.setPackaging	( _oNewTD.getPackaging());   
    	
    	_oOldTD.setQty 			( new BigDecimal (dQty));    	
    	_oOldTD.setQtyBase		( UnitTool.getBaseQty(_oOldTD.getItemId(), _oOldTD.getUnitId(), _oOldTD.getQty()));
		_oOldTD.setDiscount 	( sDiscount  );    	
		_oOldTD.setTaxAmount 	( new BigDecimal (dTaxAmount * 100)  );    	
		
		_oOldTD.setSubTotalCost ( new BigDecimal (dSubTotalCost));
    	_oOldTD.setSubTotalDisc ( new BigDecimal (dTotalDisc)   );
    	_oOldTD.setSubTotalTax 	( new BigDecimal (dTotalTax)    );
    	_oOldTD.setSubTotal     ( new BigDecimal (dSubTotal)    );
    	_oNewTD = null;
    }
    
    private boolean isExist (String _sDOID)
    {
    	for (int i = 0; i < vTD.size(); i++)
    	{
    		MedicineSalesDetail oExistPID = (MedicineSalesDetail) vTD.get (i);
    		if ( (oExistPID.getDeliveryOrderId().equals(_sDOID)) )
    		{
				return true;
    		}
    	}
    	return false;
    }
    
    protected void getData ( RunData data ) 
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{
    		    if(data.getParameters().getInt("save",-1) < 0)
    		    {
    			    oSes.setAttribute (s_MTR, MedicalSalesTool.getHeaderByID (sID));
				    oSes.setAttribute (s_MTD, MedicalSalesTool.getDetailsByID (sID));
					//payment session must also be set to new 
				    oSes.setAttribute (s_PMT, new ArrayList());
    		    }
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }

	/**
     * refresh detail and clear discount to reflect payment type change
     * 
     * @param data
     * @throws Exception
     */
	private void refreshDetail(RunData data)
		throws Exception
	{
        if(b_UPDATEABLE_DETAIL) MedicalSalesTool.updateDetail (vTD, data);
        data.getParameters().setProperties (oTR);	
						
		MedicalSalesTool.setHeaderProperties (oTR, vTD,data);
		oSes.setAttribute (s_MTD, vTD); 
		oSes.setAttribute (s_MTR, oTR);     
	}
	
	/**
	 * this method is used to set parent id for powder, by finding closest powder item
	 * 
	 * @return prescriptionDetailId of closest powder item 
	 */
	private String getClosestPowderItem()
	{
		if(vTD != null)
		{
			if(b_SALES_FIRST_LAST)
			{
				for (int i = 0; i < vTD.size(); i++)
				{
					MedicineSalesDetail oMSD = (MedicineSalesDetail) vTD.get(i);
					System.out.println(i + " PowderItem: " +  PowderTool.isPowderItem(oMSD.getItemId()) +  "  "+oMSD.getTxType() +" Item: " +oMSD.getItemName());
					if(oMSD.getTxType() == i_PRES_POWDER && 
					   StringUtil.empty(oMSD.getParentId()) && 
					   PowderTool.isPowderItem(oMSD.getItemId()))
					{
						return oMSD.getPrescriptionDetailId();
					}
				}
			}
			else
			{				
				for (int i = (vTD.size() - 1); i >= 0; i--)
				{
					MedicineSalesDetail oMSD = (MedicineSalesDetail) vTD.get(i);
					if(oMSD.getTxType() == i_PRES_POWDER && 
					   StringUtil.empty(oMSD.getParentId()) && 
					   PowderTool.isPowderItem(oMSD.getItemId()))
					{
						return oMSD.getPrescriptionDetailId();
					}
				}
			}
		}
		return "";
	}
	
	private int countPowder()
	{
		int no = 0;
		if(vTD != null)
		{			
			for (int i = 0; i < vTD.size(); i++)
			{
				MedicineSalesDetail oMSD = (MedicineSalesDetail) vTD.get(i);
				if(oMSD.getTxType() == i_PRES_POWDER && 
				   StringUtil.empty(oMSD.getParentId()) && 
				   PowderTool.isPowderItem(oMSD.getItemId()))
				{
					no = no + 1;
				}
			}			
		}
		return no;
	}
	
}

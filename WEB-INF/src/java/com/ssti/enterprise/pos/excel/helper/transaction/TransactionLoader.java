package com.ssti.enterprise.pos.excel.helper.transaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.tools.AppAttributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.2  2007/07/02 15:37:32  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/06/30 13:32:08  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public abstract class TransactionLoader extends BaseExcelLoader  implements AppAttributes
{   
	static Log log = LogFactory.getLog ( TransactionLoader.class );

	protected static final int i_START = 0;	
	
	protected int m_iTotalTrans = 0;
	protected int m_iTransSuccess = 0;
	protected int m_iTransRejected = 0;
	protected int m_iTransError = 0;
	
	protected boolean m_bTransError = false;
	
	protected void updateList (HSSFRow _oRow, short _iIdx) throws Exception
    {    	
    }
    
	public String getSummary()
    {
    	StringBuilder oSummary = new StringBuilder();
    	oSummary.append(" (TOTAL TRANS : ").append(m_iTotalTrans)
				.append(", SAVED : ").append(m_iTransSuccess)
				.append(", REJECTED : ").append(m_iTransRejected)
				.append(", ERROR : ").append(m_iTransError).append(")");
    	
    	return oSummary.toString();
    }    

	public void setUserName(String _sUserName)
	{
		m_sUserName = _sUserName;
	}
	
	public String getResult()
	{
		return m_oResult.append(s_LINE_SEPARATOR).append("LOAD RESULT").append(getSummary()).toString();
	}	

    protected void logError (String _sField, Object _sValue) throws Exception
    {    	
    	m_bTransError = true;
    	m_oResult.append(" - Invalid/Empty Field : ").append(_sField)
    			 .append(" Excel Value : ").append(_sValue).append(s_LINE_SEPARATOR);
    }    

    protected void logInvalidAcc(Object _sValue) throws Exception
    {    	
    	m_bTransError = true;
    	m_oResult.append(" - Invalid Account Is Parent : ").append(_sValue).append(s_LINE_SEPARATOR);
    }  
    
    protected void logSuccess () throws Exception
    {    	
		m_oResult.append("SAVE TRANS SUCCESS ").append(s_LINE_SEPARATOR);
		m_oResult.append("------------------ ").append(s_LINE_SEPARATOR);
		m_iTransSuccess++;
    }  

    protected void logSaveError (String _sMessage) throws Exception
    {    	
		m_oResult.append("SAVE TRANS ERROR ").append(_sMessage).append(s_LINE_SEPARATOR);
		m_oResult.append("---------------- ").append(s_LINE_SEPARATOR);
		m_iTransError++;
    }  

    protected void logReject () throws Exception
    {  
		m_oResult.append("INVALID TRANS DATA, NOT SAVED ").append(s_LINE_SEPARATOR);
		m_oResult.append("----------------------------- ").append(s_LINE_SEPARATOR);
		m_iTransRejected++;
    }

    protected void logInvalidFile () throws Exception
    {  
		String sMsg = "INVALID EXCEL FILE STRUCTURE, LOADING STOPPED !";
		//excel structure error / invalid condition
		m_oResult.append(s_LINE_SEPARATOR).append(sMsg).append(s_LINE_SEPARATOR);
    }
    
    protected String setDesc(String _sTransNo, String _sUser)
    {
        return "\n TRN:" + _sTransNo + " USR:" + _sUser;
    }
}                                                                                           
package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BarcodePaperMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BarcodePaperMapBuilder";

    /**
     * Item
     * @deprecated use BarcodePaperPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "barcode_paper";
    }

  
    /**
     * barcode_paper.BARCODE_PAPER_ID
     * @return the column name for the BARCODE_PAPER_ID field
     * @deprecated use BarcodePaperPeer.barcode_paper.BARCODE_PAPER_ID constant
     */
    public static String getBarcodePaper_BarcodePaperId()
    {
        return "barcode_paper.BARCODE_PAPER_ID";
    }
  
    /**
     * barcode_paper.SETTING_NAME
     * @return the column name for the SETTING_NAME field
     * @deprecated use BarcodePaperPeer.barcode_paper.SETTING_NAME constant
     */
    public static String getBarcodePaper_SettingName()
    {
        return "barcode_paper.SETTING_NAME";
    }
  
    /**
     * barcode_paper.PAPER_TYPE
     * @return the column name for the PAPER_TYPE field
     * @deprecated use BarcodePaperPeer.barcode_paper.PAPER_TYPE constant
     */
    public static String getBarcodePaper_PaperType()
    {
        return "barcode_paper.PAPER_TYPE";
    }
  
    /**
     * barcode_paper.PAPER_COLUMN
     * @return the column name for the PAPER_COLUMN field
     * @deprecated use BarcodePaperPeer.barcode_paper.PAPER_COLUMN constant
     */
    public static String getBarcodePaper_PaperColumn()
    {
        return "barcode_paper.PAPER_COLUMN";
    }
  
    /**
     * barcode_paper.PAPER_ROW
     * @return the column name for the PAPER_ROW field
     * @deprecated use BarcodePaperPeer.barcode_paper.PAPER_ROW constant
     */
    public static String getBarcodePaper_PaperRow()
    {
        return "barcode_paper.PAPER_ROW";
    }
  
    /**
     * barcode_paper.COLUMN_WIDTH
     * @return the column name for the COLUMN_WIDTH field
     * @deprecated use BarcodePaperPeer.barcode_paper.COLUMN_WIDTH constant
     */
    public static String getBarcodePaper_ColumnWidth()
    {
        return "barcode_paper.COLUMN_WIDTH";
    }
  
    /**
     * barcode_paper.COLUMN_HEIGHT
     * @return the column name for the COLUMN_HEIGHT field
     * @deprecated use BarcodePaperPeer.barcode_paper.COLUMN_HEIGHT constant
     */
    public static String getBarcodePaper_ColumnHeight()
    {
        return "barcode_paper.COLUMN_HEIGHT";
    }
  
    /**
     * barcode_paper.COLUMN_SPACING
     * @return the column name for the COLUMN_SPACING field
     * @deprecated use BarcodePaperPeer.barcode_paper.COLUMN_SPACING constant
     */
    public static String getBarcodePaper_ColumnSpacing()
    {
        return "barcode_paper.COLUMN_SPACING";
    }
  
    /**
     * barcode_paper.PAPER_WIDTH
     * @return the column name for the PAPER_WIDTH field
     * @deprecated use BarcodePaperPeer.barcode_paper.PAPER_WIDTH constant
     */
    public static String getBarcodePaper_PaperWidth()
    {
        return "barcode_paper.PAPER_WIDTH";
    }
  
    /**
     * barcode_paper.PAPER_HEIGHT
     * @return the column name for the PAPER_HEIGHT field
     * @deprecated use BarcodePaperPeer.barcode_paper.PAPER_HEIGHT constant
     */
    public static String getBarcodePaper_PaperHeight()
    {
        return "barcode_paper.PAPER_HEIGHT";
    }
  
    /**
     * barcode_paper.BARCODE_TYPE
     * @return the column name for the BARCODE_TYPE field
     * @deprecated use BarcodePaperPeer.barcode_paper.BARCODE_TYPE constant
     */
    public static String getBarcodePaper_BarcodeType()
    {
        return "barcode_paper.BARCODE_TYPE";
    }
  
    /**
     * barcode_paper.BARCODE_WIDTH
     * @return the column name for the BARCODE_WIDTH field
     * @deprecated use BarcodePaperPeer.barcode_paper.BARCODE_WIDTH constant
     */
    public static String getBarcodePaper_BarcodeWidth()
    {
        return "barcode_paper.BARCODE_WIDTH";
    }
  
    /**
     * barcode_paper.BARCODE_HEIGHT
     * @return the column name for the BARCODE_HEIGHT field
     * @deprecated use BarcodePaperPeer.barcode_paper.BARCODE_HEIGHT constant
     */
    public static String getBarcodePaper_BarcodeHeight()
    {
        return "barcode_paper.BARCODE_HEIGHT";
    }
  
    /**
     * barcode_paper.RESOLUTION
     * @return the column name for the RESOLUTION field
     * @deprecated use BarcodePaperPeer.barcode_paper.RESOLUTION constant
     */
    public static String getBarcodePaper_Resolution()
    {
        return "barcode_paper.RESOLUTION";
    }
  
    /**
     * barcode_paper.LEFT_MARGIN
     * @return the column name for the LEFT_MARGIN field
     * @deprecated use BarcodePaperPeer.barcode_paper.LEFT_MARGIN constant
     */
    public static String getBarcodePaper_LeftMargin()
    {
        return "barcode_paper.LEFT_MARGIN";
    }
  
    /**
     * barcode_paper.TOP_MARGIN
     * @return the column name for the TOP_MARGIN field
     * @deprecated use BarcodePaperPeer.barcode_paper.TOP_MARGIN constant
     */
    public static String getBarcodePaper_TopMargin()
    {
        return "barcode_paper.TOP_MARGIN";
    }
  
    /**
     * barcode_paper.BOTTOM_MARGIN
     * @return the column name for the BOTTOM_MARGIN field
     * @deprecated use BarcodePaperPeer.barcode_paper.BOTTOM_MARGIN constant
     */
    public static String getBarcodePaper_BottomMargin()
    {
        return "barcode_paper.BOTTOM_MARGIN";
    }
  
    /**
     * barcode_paper.BORDER
     * @return the column name for the BORDER field
     * @deprecated use BarcodePaperPeer.barcode_paper.BORDER constant
     */
    public static String getBarcodePaper_Border()
    {
        return "barcode_paper.BORDER";
    }
  
    /**
     * barcode_paper.IS_DEFAULT
     * @return the column name for the IS_DEFAULT field
     * @deprecated use BarcodePaperPeer.barcode_paper.IS_DEFAULT constant
     */
    public static String getBarcodePaper_IsDefault()
    {
        return "barcode_paper.IS_DEFAULT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("barcode_paper");
        TableMap tMap = dbMap.getTable("barcode_paper");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("barcode_paper.BARCODE_PAPER_ID", "");
                          tMap.addColumn("barcode_paper.SETTING_NAME", "");
                            tMap.addColumn("barcode_paper.PAPER_TYPE", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.PAPER_COLUMN", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.PAPER_ROW", Integer.valueOf(0));
                          tMap.addColumn("barcode_paper.COLUMN_WIDTH", "");
                            tMap.addColumn("barcode_paper.COLUMN_HEIGHT", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.COLUMN_SPACING", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.PAPER_WIDTH", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.PAPER_HEIGHT", Integer.valueOf(0));
                          tMap.addColumn("barcode_paper.BARCODE_TYPE", "");
                            tMap.addColumn("barcode_paper.BARCODE_WIDTH", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.BARCODE_HEIGHT", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.RESOLUTION", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.LEFT_MARGIN", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.TOP_MARGIN", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.BOTTOM_MARGIN", Integer.valueOf(0));
                            tMap.addColumn("barcode_paper.BORDER", Integer.valueOf(0));
                          tMap.addColumn("barcode_paper.IS_DEFAULT", Boolean.TRUE);
          }
}

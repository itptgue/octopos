package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to IssueReceipt
 */
public abstract class BaseIssueReceipt extends BaseObject
{
    /** The Peer class */
    private static final IssueReceiptPeer peer =
        new IssueReceiptPeer();

        
    /** The value for the issueReceiptId field */
    private String issueReceiptId;
      
    /** The value for the adjustmentTypeId field */
    private String adjustmentTypeId;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the transactionType field */
    private int transactionType;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the description field */
    private String description;
                                          
    /** The value for the status field */
    private int status = 1;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the costAdjustment field */
    private boolean costAdjustment;
                                                
    /** The value for the confirmBy field */
    private String confirmBy = "";
      
    /** The value for the confirmDate field */
    private Date confirmDate;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
      
    /** The value for the internalTransfer field */
    private boolean internalTransfer;
                                                
    /** The value for the requestId field */
    private String requestId = "";
                                                
    /** The value for the crossEntityId field */
    private String crossEntityId = "";
                                                
    /** The value for the crossEntityCode field */
    private String crossEntityCode = "";
                                                
    /** The value for the crossTransId field */
    private String crossTransId = "";
                                                
    /** The value for the crossTransNo field */
    private String crossTransNo = "";
                                                
    /** The value for the crossConfirmBy field */
    private String crossConfirmBy = "";
      
    /** The value for the crossConfirmDate field */
    private Date crossConfirmDate;
                                                
    /** The value for the fileName field */
    private String fileName = "";
  
    
    /**
     * Get the IssueReceiptId
     *
     * @return String
     */
    public String getIssueReceiptId()
    {
        return issueReceiptId;
    }

                                              
    /**
     * Set the value of IssueReceiptId
     *
     * @param v new value
     */
    public void setIssueReceiptId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.issueReceiptId, v))
              {
            this.issueReceiptId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated IssueReceiptDetail
        if (collIssueReceiptDetails != null)
        {
            for (int i = 0; i < collIssueReceiptDetails.size(); i++)
            {
                ((IssueReceiptDetail) collIssueReceiptDetails.get(i))
                    .setIssueReceiptId(v);
            }
        }
                                }
  
    /**
     * Get the AdjustmentTypeId
     *
     * @return String
     */
    public String getAdjustmentTypeId()
    {
        return adjustmentTypeId;
    }

                        
    /**
     * Set the value of AdjustmentTypeId
     *
     * @param v new value
     */
    public void setAdjustmentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.adjustmentTypeId, v))
              {
            this.adjustmentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CostAdjustment
     *
     * @return boolean
     */
    public boolean getCostAdjustment()
    {
        return costAdjustment;
    }

                        
    /**
     * Set the value of CostAdjustment
     *
     * @param v new value
     */
    public void setCostAdjustment(boolean v) 
    {
    
                  if (this.costAdjustment != v)
              {
            this.costAdjustment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmBy
     *
     * @return String
     */
    public String getConfirmBy()
    {
        return confirmBy;
    }

                        
    /**
     * Set the value of ConfirmBy
     *
     * @param v new value
     */
    public void setConfirmBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmBy, v))
              {
            this.confirmBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmDate
     *
     * @return Date
     */
    public Date getConfirmDate()
    {
        return confirmDate;
    }

                        
    /**
     * Set the value of ConfirmDate
     *
     * @param v new value
     */
    public void setConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmDate, v))
              {
            this.confirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InternalTransfer
     *
     * @return boolean
     */
    public boolean getInternalTransfer()
    {
        return internalTransfer;
    }

                        
    /**
     * Set the value of InternalTransfer
     *
     * @param v new value
     */
    public void setInternalTransfer(boolean v) 
    {
    
                  if (this.internalTransfer != v)
              {
            this.internalTransfer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RequestId
     *
     * @return String
     */
    public String getRequestId()
    {
        return requestId;
    }

                        
    /**
     * Set the value of RequestId
     *
     * @param v new value
     */
    public void setRequestId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.requestId, v))
              {
            this.requestId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossEntityId
     *
     * @return String
     */
    public String getCrossEntityId()
    {
        return crossEntityId;
    }

                        
    /**
     * Set the value of CrossEntityId
     *
     * @param v new value
     */
    public void setCrossEntityId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.crossEntityId, v))
              {
            this.crossEntityId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossEntityCode
     *
     * @return String
     */
    public String getCrossEntityCode()
    {
        return crossEntityCode;
    }

                        
    /**
     * Set the value of CrossEntityCode
     *
     * @param v new value
     */
    public void setCrossEntityCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.crossEntityCode, v))
              {
            this.crossEntityCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossTransId
     *
     * @return String
     */
    public String getCrossTransId()
    {
        return crossTransId;
    }

                        
    /**
     * Set the value of CrossTransId
     *
     * @param v new value
     */
    public void setCrossTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.crossTransId, v))
              {
            this.crossTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossTransNo
     *
     * @return String
     */
    public String getCrossTransNo()
    {
        return crossTransNo;
    }

                        
    /**
     * Set the value of CrossTransNo
     *
     * @param v new value
     */
    public void setCrossTransNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.crossTransNo, v))
              {
            this.crossTransNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossConfirmBy
     *
     * @return String
     */
    public String getCrossConfirmBy()
    {
        return crossConfirmBy;
    }

                        
    /**
     * Set the value of CrossConfirmBy
     *
     * @param v new value
     */
    public void setCrossConfirmBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.crossConfirmBy, v))
              {
            this.crossConfirmBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossConfirmDate
     *
     * @return Date
     */
    public Date getCrossConfirmDate()
    {
        return crossConfirmDate;
    }

                        
    /**
     * Set the value of CrossConfirmDate
     *
     * @param v new value
     */
    public void setCrossConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.crossConfirmDate, v))
              {
            this.crossConfirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FileName
     *
     * @return String
     */
    public String getFileName()
    {
        return fileName;
    }

                        
    /**
     * Set the value of FileName
     *
     * @param v new value
     */
    public void setFileName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fileName, v))
              {
            this.fileName = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collIssueReceiptDetails
     */
    protected List collIssueReceiptDetails;

    /**
     * Temporary storage of collIssueReceiptDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initIssueReceiptDetails()
    {
        if (collIssueReceiptDetails == null)
        {
            collIssueReceiptDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a IssueReceiptDetail object to this object
     * through the IssueReceiptDetail foreign key attribute
     *
     * @param l IssueReceiptDetail
     * @throws TorqueException
     */
    public void addIssueReceiptDetail(IssueReceiptDetail l) throws TorqueException
    {
        getIssueReceiptDetails().add(l);
        l.setIssueReceipt((IssueReceipt) this);
    }

    /**
     * The criteria used to select the current contents of collIssueReceiptDetails
     */
    private Criteria lastIssueReceiptDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getIssueReceiptDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getIssueReceiptDetails() throws TorqueException
    {
              if (collIssueReceiptDetails == null)
        {
            collIssueReceiptDetails = getIssueReceiptDetails(new Criteria(10));
        }
        return collIssueReceiptDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this IssueReceipt has previously
     * been saved, it will retrieve related IssueReceiptDetails from storage.
     * If this IssueReceipt is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getIssueReceiptDetails(Criteria criteria) throws TorqueException
    {
              if (collIssueReceiptDetails == null)
        {
            if (isNew())
            {
               collIssueReceiptDetails = new ArrayList();
            }
            else
            {
                        criteria.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, getIssueReceiptId() );
                        collIssueReceiptDetails = IssueReceiptDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, getIssueReceiptId());
                            if (!lastIssueReceiptDetailsCriteria.equals(criteria))
                {
                    collIssueReceiptDetails = IssueReceiptDetailPeer.doSelect(criteria);
                }
            }
        }
        lastIssueReceiptDetailsCriteria = criteria;

        return collIssueReceiptDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getIssueReceiptDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getIssueReceiptDetails(Connection con) throws TorqueException
    {
              if (collIssueReceiptDetails == null)
        {
            collIssueReceiptDetails = getIssueReceiptDetails(new Criteria(10), con);
        }
        return collIssueReceiptDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this IssueReceipt has previously
     * been saved, it will retrieve related IssueReceiptDetails from storage.
     * If this IssueReceipt is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getIssueReceiptDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collIssueReceiptDetails == null)
        {
            if (isNew())
            {
               collIssueReceiptDetails = new ArrayList();
            }
            else
            {
                         criteria.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, getIssueReceiptId());
                         collIssueReceiptDetails = IssueReceiptDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, getIssueReceiptId());
                             if (!lastIssueReceiptDetailsCriteria.equals(criteria))
                 {
                     collIssueReceiptDetails = IssueReceiptDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastIssueReceiptDetailsCriteria = criteria;

         return collIssueReceiptDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this IssueReceipt is new, it will return
     * an empty collection; or if this IssueReceipt has previously
     * been saved, it will retrieve related IssueReceiptDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in IssueReceipt.
     */
    protected List getIssueReceiptDetailsJoinIssueReceipt(Criteria criteria)
        throws TorqueException
    {
                    if (collIssueReceiptDetails == null)
        {
            if (isNew())
            {
               collIssueReceiptDetails = new ArrayList();
            }
            else
            {
                              criteria.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, getIssueReceiptId());
                              collIssueReceiptDetails = IssueReceiptDetailPeer.doSelectJoinIssueReceipt(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(IssueReceiptDetailPeer.ISSUE_RECEIPT_ID, getIssueReceiptId());
                                    if (!lastIssueReceiptDetailsCriteria.equals(criteria))
            {
                collIssueReceiptDetails = IssueReceiptDetailPeer.doSelectJoinIssueReceipt(criteria);
            }
        }
        lastIssueReceiptDetailsCriteria = criteria;

        return collIssueReceiptDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("IssueReceiptId");
              fieldNames.add("AdjustmentTypeId");
              fieldNames.add("TransactionDate");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionType");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("UserName");
              fieldNames.add("Description");
              fieldNames.add("Status");
              fieldNames.add("AccountId");
              fieldNames.add("CostAdjustment");
              fieldNames.add("ConfirmBy");
              fieldNames.add("ConfirmDate");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("InternalTransfer");
              fieldNames.add("RequestId");
              fieldNames.add("CrossEntityId");
              fieldNames.add("CrossEntityCode");
              fieldNames.add("CrossTransId");
              fieldNames.add("CrossTransNo");
              fieldNames.add("CrossConfirmBy");
              fieldNames.add("CrossConfirmDate");
              fieldNames.add("FileName");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("IssueReceiptId"))
        {
                return getIssueReceiptId();
            }
          if (name.equals("AdjustmentTypeId"))
        {
                return getAdjustmentTypeId();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("CostAdjustment"))
        {
                return Boolean.valueOf(getCostAdjustment());
            }
          if (name.equals("ConfirmBy"))
        {
                return getConfirmBy();
            }
          if (name.equals("ConfirmDate"))
        {
                return getConfirmDate();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("InternalTransfer"))
        {
                return Boolean.valueOf(getInternalTransfer());
            }
          if (name.equals("RequestId"))
        {
                return getRequestId();
            }
          if (name.equals("CrossEntityId"))
        {
                return getCrossEntityId();
            }
          if (name.equals("CrossEntityCode"))
        {
                return getCrossEntityCode();
            }
          if (name.equals("CrossTransId"))
        {
                return getCrossTransId();
            }
          if (name.equals("CrossTransNo"))
        {
                return getCrossTransNo();
            }
          if (name.equals("CrossConfirmBy"))
        {
                return getCrossConfirmBy();
            }
          if (name.equals("CrossConfirmDate"))
        {
                return getCrossConfirmDate();
            }
          if (name.equals("FileName"))
        {
                return getFileName();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(IssueReceiptPeer.ISSUE_RECEIPT_ID))
        {
                return getIssueReceiptId();
            }
          if (name.equals(IssueReceiptPeer.ADJUSTMENT_TYPE_ID))
        {
                return getAdjustmentTypeId();
            }
          if (name.equals(IssueReceiptPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(IssueReceiptPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(IssueReceiptPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(IssueReceiptPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(IssueReceiptPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(IssueReceiptPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(IssueReceiptPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(IssueReceiptPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(IssueReceiptPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(IssueReceiptPeer.COST_ADJUSTMENT))
        {
                return Boolean.valueOf(getCostAdjustment());
            }
          if (name.equals(IssueReceiptPeer.CONFIRM_BY))
        {
                return getConfirmBy();
            }
          if (name.equals(IssueReceiptPeer.CONFIRM_DATE))
        {
                return getConfirmDate();
            }
          if (name.equals(IssueReceiptPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(IssueReceiptPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(IssueReceiptPeer.INTERNAL_TRANSFER))
        {
                return Boolean.valueOf(getInternalTransfer());
            }
          if (name.equals(IssueReceiptPeer.REQUEST_ID))
        {
                return getRequestId();
            }
          if (name.equals(IssueReceiptPeer.CROSS_ENTITY_ID))
        {
                return getCrossEntityId();
            }
          if (name.equals(IssueReceiptPeer.CROSS_ENTITY_CODE))
        {
                return getCrossEntityCode();
            }
          if (name.equals(IssueReceiptPeer.CROSS_TRANS_ID))
        {
                return getCrossTransId();
            }
          if (name.equals(IssueReceiptPeer.CROSS_TRANS_NO))
        {
                return getCrossTransNo();
            }
          if (name.equals(IssueReceiptPeer.CROSS_CONFIRM_BY))
        {
                return getCrossConfirmBy();
            }
          if (name.equals(IssueReceiptPeer.CROSS_CONFIRM_DATE))
        {
                return getCrossConfirmDate();
            }
          if (name.equals(IssueReceiptPeer.FILE_NAME))
        {
                return getFileName();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getIssueReceiptId();
            }
              if (pos == 1)
        {
                return getAdjustmentTypeId();
            }
              if (pos == 2)
        {
                return getTransactionDate();
            }
              if (pos == 3)
        {
                return getTransactionNo();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 5)
        {
                return getLocationId();
            }
              if (pos == 6)
        {
                return getLocationName();
            }
              if (pos == 7)
        {
                return getUserName();
            }
              if (pos == 8)
        {
                return getDescription();
            }
              if (pos == 9)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 10)
        {
                return getAccountId();
            }
              if (pos == 11)
        {
                return Boolean.valueOf(getCostAdjustment());
            }
              if (pos == 12)
        {
                return getConfirmBy();
            }
              if (pos == 13)
        {
                return getConfirmDate();
            }
              if (pos == 14)
        {
                return getCancelBy();
            }
              if (pos == 15)
        {
                return getCancelDate();
            }
              if (pos == 16)
        {
                return Boolean.valueOf(getInternalTransfer());
            }
              if (pos == 17)
        {
                return getRequestId();
            }
              if (pos == 18)
        {
                return getCrossEntityId();
            }
              if (pos == 19)
        {
                return getCrossEntityCode();
            }
              if (pos == 20)
        {
                return getCrossTransId();
            }
              if (pos == 21)
        {
                return getCrossTransNo();
            }
              if (pos == 22)
        {
                return getCrossConfirmBy();
            }
              if (pos == 23)
        {
                return getCrossConfirmDate();
            }
              if (pos == 24)
        {
                return getFileName();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(IssueReceiptPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        IssueReceiptPeer.doInsert((IssueReceipt) this, con);
                        setNew(false);
                    }
                    else
                    {
                        IssueReceiptPeer.doUpdate((IssueReceipt) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collIssueReceiptDetails != null)
            {
                for (int i = 0; i < collIssueReceiptDetails.size(); i++)
                {
                    ((IssueReceiptDetail) collIssueReceiptDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key issueReceiptId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setIssueReceiptId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setIssueReceiptId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getIssueReceiptId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public IssueReceipt copy() throws TorqueException
    {
        return copyInto(new IssueReceipt());
    }
  
    protected IssueReceipt copyInto(IssueReceipt copyObj) throws TorqueException
    {
          copyObj.setIssueReceiptId(issueReceiptId);
          copyObj.setAdjustmentTypeId(adjustmentTypeId);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionType(transactionType);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setUserName(userName);
          copyObj.setDescription(description);
          copyObj.setStatus(status);
          copyObj.setAccountId(accountId);
          copyObj.setCostAdjustment(costAdjustment);
          copyObj.setConfirmBy(confirmBy);
          copyObj.setConfirmDate(confirmDate);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setInternalTransfer(internalTransfer);
          copyObj.setRequestId(requestId);
          copyObj.setCrossEntityId(crossEntityId);
          copyObj.setCrossEntityCode(crossEntityCode);
          copyObj.setCrossTransId(crossTransId);
          copyObj.setCrossTransNo(crossTransNo);
          copyObj.setCrossConfirmBy(crossConfirmBy);
          copyObj.setCrossConfirmDate(crossConfirmDate);
          copyObj.setFileName(fileName);
  
                    copyObj.setIssueReceiptId((String)null);
                                                                                                                                                            
                                      
                            
        List v = getIssueReceiptDetails();
        for (int i = 0; i < v.size(); i++)
        {
            IssueReceiptDetail obj = (IssueReceiptDetail) v.get(i);
            copyObj.addIssueReceiptDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public IssueReceiptPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("IssueReceipt\n");
        str.append("------------\n")
           .append("IssueReceiptId       : ")
           .append(getIssueReceiptId())
           .append("\n")
           .append("AdjustmentTypeId     : ")
           .append(getAdjustmentTypeId())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("CostAdjustment       : ")
           .append(getCostAdjustment())
           .append("\n")
           .append("ConfirmBy            : ")
           .append(getConfirmBy())
           .append("\n")
           .append("ConfirmDate          : ")
           .append(getConfirmDate())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("InternalTransfer     : ")
           .append(getInternalTransfer())
           .append("\n")
           .append("RequestId            : ")
           .append(getRequestId())
           .append("\n")
           .append("CrossEntityId        : ")
           .append(getCrossEntityId())
           .append("\n")
           .append("CrossEntityCode      : ")
           .append(getCrossEntityCode())
           .append("\n")
           .append("CrossTransId         : ")
           .append(getCrossTransId())
           .append("\n")
           .append("CrossTransNo         : ")
           .append(getCrossTransNo())
           .append("\n")
           .append("CrossConfirmBy       : ")
           .append(getCrossConfirmBy())
           .append("\n")
           .append("CrossConfirmDate     : ")
           .append(getCrossConfirmDate())
           .append("\n")
           .append("FileName             : ")
           .append(getFileName())
           .append("\n")
        ;
        return(str.toString());
    }
}

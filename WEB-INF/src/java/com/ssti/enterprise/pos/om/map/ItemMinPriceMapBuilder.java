package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemMinPriceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemMinPriceMapBuilder";

    /**
     * Item
     * @deprecated use ItemMinPricePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_min_price";
    }

  
    /**
     * item_min_price.ITEM_MIN_PRICE_ID
     * @return the column name for the ITEM_MIN_PRICE_ID field
     * @deprecated use ItemMinPricePeer.item_min_price.ITEM_MIN_PRICE_ID constant
     */
    public static String getItemMinPrice_ItemMinPriceId()
    {
        return "item_min_price.ITEM_MIN_PRICE_ID";
    }
  
    /**
     * item_min_price.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemMinPricePeer.item_min_price.ITEM_ID constant
     */
    public static String getItemMinPrice_ItemId()
    {
        return "item_min_price.ITEM_ID";
    }
  
    /**
     * item_min_price.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use ItemMinPricePeer.item_min_price.LOCATION_ID constant
     */
    public static String getItemMinPrice_LocationId()
    {
        return "item_min_price.LOCATION_ID";
    }
  
    /**
     * item_min_price.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use ItemMinPricePeer.item_min_price.CUSTOMER_TYPE_ID constant
     */
    public static String getItemMinPrice_CustomerTypeId()
    {
        return "item_min_price.CUSTOMER_TYPE_ID";
    }
  
    /**
     * item_min_price.MIN_PRICE
     * @return the column name for the MIN_PRICE field
     * @deprecated use ItemMinPricePeer.item_min_price.MIN_PRICE constant
     */
    public static String getItemMinPrice_MinPrice()
    {
        return "item_min_price.MIN_PRICE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_min_price");
        TableMap tMap = dbMap.getTable("item_min_price");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_min_price.ITEM_MIN_PRICE_ID", "");
                          tMap.addColumn("item_min_price.ITEM_ID", "");
                          tMap.addColumn("item_min_price.LOCATION_ID", "");
                          tMap.addColumn("item_min_price.CUSTOMER_TYPE_ID", "");
                            tMap.addColumn("item_min_price.MIN_PRICE", bd_ZERO);
          }
}

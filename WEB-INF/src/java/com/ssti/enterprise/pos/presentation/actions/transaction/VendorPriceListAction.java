package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.VendorPriceListLoader;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorPriceList;
import com.ssti.enterprise.pos.om.VendorPriceListDetailPeer;
import com.ssti.enterprise.pos.om.VendorPriceListPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.VendorPriceListTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.framework.presentation.SecureAction;
import com.ssti.framework.tools.CustomParser;

public class VendorPriceListAction extends SecureAction
{
   	private static final String s_VIEW_PERM    = "View Vendor Price List";
	private static final String s_UPDATE_PERM  = "Update Vendor Price List";
	
    private static final String[] a_UPDATE_PERM  = {s_VIEW_PERM, s_UPDATE_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if(data.getParameters().getString("eventSubmit_doInsert",null) != null ||
           data.getParameters().getString("eventSubmit_doUpdate",null) != null ||
           data.getParameters().getString("eventSubmit_doDelete",null) != null )
        {
        	return isAuthorized (data, a_UPDATE_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
	private static Log log = LogFactory.getLog(VendorPriceListAction.class);
		
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			VendorPriceList oPL = new VendorPriceList();
			setProperties (oPL, data);
			//Generate Sys ID                                   
			String sVendorID = oPL.getVendorId();
			Vendor oVendor = VendorTool.getVendorByID(sVendorID);
			oPL.setCurrencyId(oVendor.getDefaultCurrencyId());
			oPL.setVendorPriceListId ( IDGenerator.generateSysID() );			
			oPL.setUpdateDate (new Date());
		    oPL.save();
		    if(data.getParameters().getBoolean("ImportVendorItem"))
			{
				VendorPriceListTool.importVendorItem(oPL.getVendorPriceListId(), 
													 oPL.getVendorId());
			}
		    data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	log.error (_oEx);
        	data.setMessage(LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			VendorPriceList oVendorPriceList = 
				VendorPriceListTool.getVendorPriceListByID(data.getParameters().getString ("ID"));
		    setProperties (oVendorPriceList, data);
		    oVendorPriceList.setUpdateDate (new Date());
		    oVendorPriceList.save();
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

    public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
	    	Criteria oCrit = new Criteria();
	        oCrit.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, data.getParameters().getString("ID"));
	        VendorPriceListDetailPeer.doDelete(oCrit);
	        oCrit.add(VendorPriceListPeer.VENDOR_PRICE_LIST_ID, data.getParameters().getString("ID"));
	        VendorPriceListPeer.doDelete(oCrit);
        	data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	log.error (_oEx);        	
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
	     	VendorPriceListLoader oLoader = new VendorPriceListLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}	
    
    private void setProperties (VendorPriceList  _oVendorPriceList,  RunData data)
    {
		try
		{
			data.getParameters().setProperties(_oVendorPriceList);
			_oVendorPriceList.setCreateDate(CustomParser.parseDate (data.getParameters().getString ("CreateDate")));
			_oVendorPriceList.setExpiredDate(CustomParser.parseDate (data.getParameters().getString ("ExpiredDate")));
    	}
    	catch (Exception _oEx)
    	{
    		log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) + _oEx.getMessage());
    	}
    }
}
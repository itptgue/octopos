package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.CashFlowMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseCashFlowPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "cash_flow";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CashFlowMapBuilder.CLASS_NAME);
    }

      /** the column name for the CASH_FLOW_ID field */
    public static final String CASH_FLOW_ID;
      /** the column name for the CASH_FLOW_NO field */
    public static final String CASH_FLOW_NO;
      /** the column name for the CASH_FLOW_TYPE field */
    public static final String CASH_FLOW_TYPE;
      /** the column name for the BANK_ID field */
    public static final String BANK_ID;
      /** the column name for the BANK_ISSUER field */
    public static final String BANK_ISSUER;
      /** the column name for the REFERENCE_NO field */
    public static final String REFERENCE_NO;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the EXPECTED_DATE field */
    public static final String EXPECTED_DATE;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the DUE_DATE field */
    public static final String DUE_DATE;
      /** the column name for the CASH_FLOW_AMOUNT field */
    public static final String CASH_FLOW_AMOUNT;
      /** the column name for the AMOUNT_BASE field */
    public static final String AMOUNT_BASE;
      /** the column name for the USER_NAME field */
    public static final String USER_NAME;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the TRANSACTION_ID field */
    public static final String TRANSACTION_ID;
      /** the column name for the TRANSACTION_NO field */
    public static final String TRANSACTION_NO;
      /** the column name for the TRANSACTION_TYPE field */
    public static final String TRANSACTION_TYPE;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the SAY_AMOUNT field */
    public static final String SAY_AMOUNT;
      /** the column name for the JOURNAL_AMOUNT field */
    public static final String JOURNAL_AMOUNT;
      /** the column name for the RECONCILED field */
    public static final String RECONCILED;
      /** the column name for the RECONCILE_DATE field */
    public static final String RECONCILE_DATE;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
  
    static
    {
          CASH_FLOW_ID = "cash_flow.CASH_FLOW_ID";
          CASH_FLOW_NO = "cash_flow.CASH_FLOW_NO";
          CASH_FLOW_TYPE = "cash_flow.CASH_FLOW_TYPE";
          BANK_ID = "cash_flow.BANK_ID";
          BANK_ISSUER = "cash_flow.BANK_ISSUER";
          REFERENCE_NO = "cash_flow.REFERENCE_NO";
          CURRENCY_ID = "cash_flow.CURRENCY_ID";
          CURRENCY_RATE = "cash_flow.CURRENCY_RATE";
          EXPECTED_DATE = "cash_flow.EXPECTED_DATE";
          LOCATION_ID = "cash_flow.LOCATION_ID";
          DUE_DATE = "cash_flow.DUE_DATE";
          CASH_FLOW_AMOUNT = "cash_flow.CASH_FLOW_AMOUNT";
          AMOUNT_BASE = "cash_flow.AMOUNT_BASE";
          USER_NAME = "cash_flow.USER_NAME";
          DESCRIPTION = "cash_flow.DESCRIPTION";
          TRANSACTION_ID = "cash_flow.TRANSACTION_ID";
          TRANSACTION_NO = "cash_flow.TRANSACTION_NO";
          TRANSACTION_TYPE = "cash_flow.TRANSACTION_TYPE";
          STATUS = "cash_flow.STATUS";
          SAY_AMOUNT = "cash_flow.SAY_AMOUNT";
          JOURNAL_AMOUNT = "cash_flow.JOURNAL_AMOUNT";
          RECONCILED = "cash_flow.RECONCILED";
          RECONCILE_DATE = "cash_flow.RECONCILE_DATE";
          CANCEL_BY = "cash_flow.CANCEL_BY";
          CANCEL_DATE = "cash_flow.CANCEL_DATE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CashFlowMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CashFlowMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  25;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.CashFlow";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCashFlowPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RECONCILED))
        {
            Object possibleBoolean = criteria.get(RECONCILED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RECONCILED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CASH_FLOW_ID);
          criteria.addSelectColumn(CASH_FLOW_NO);
          criteria.addSelectColumn(CASH_FLOW_TYPE);
          criteria.addSelectColumn(BANK_ID);
          criteria.addSelectColumn(BANK_ISSUER);
          criteria.addSelectColumn(REFERENCE_NO);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(EXPECTED_DATE);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(DUE_DATE);
          criteria.addSelectColumn(CASH_FLOW_AMOUNT);
          criteria.addSelectColumn(AMOUNT_BASE);
          criteria.addSelectColumn(USER_NAME);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(TRANSACTION_ID);
          criteria.addSelectColumn(TRANSACTION_NO);
          criteria.addSelectColumn(TRANSACTION_TYPE);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(SAY_AMOUNT);
          criteria.addSelectColumn(JOURNAL_AMOUNT);
          criteria.addSelectColumn(RECONCILED);
          criteria.addSelectColumn(RECONCILE_DATE);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static CashFlow row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            CashFlow obj = (CashFlow) cls.newInstance();
            CashFlowPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      CashFlow obj)
        throws TorqueException
    {
        try
        {
                obj.setCashFlowId(row.getValue(offset + 0).asString());
                  obj.setCashFlowNo(row.getValue(offset + 1).asString());
                  obj.setCashFlowType(row.getValue(offset + 2).asInt());
                  obj.setBankId(row.getValue(offset + 3).asString());
                  obj.setBankIssuer(row.getValue(offset + 4).asString());
                  obj.setReferenceNo(row.getValue(offset + 5).asString());
                  obj.setCurrencyId(row.getValue(offset + 6).asString());
                  obj.setCurrencyRate(row.getValue(offset + 7).asBigDecimal());
                  obj.setExpectedDate(row.getValue(offset + 8).asUtilDate());
                  obj.setLocationId(row.getValue(offset + 9).asString());
                  obj.setDueDate(row.getValue(offset + 10).asUtilDate());
                  obj.setCashFlowAmount(row.getValue(offset + 11).asBigDecimal());
                  obj.setAmountBase(row.getValue(offset + 12).asBigDecimal());
                  obj.setUserName(row.getValue(offset + 13).asString());
                  obj.setDescription(row.getValue(offset + 14).asString());
                  obj.setTransactionId(row.getValue(offset + 15).asString());
                  obj.setTransactionNo(row.getValue(offset + 16).asString());
                  obj.setTransactionType(row.getValue(offset + 17).asInt());
                  obj.setStatus(row.getValue(offset + 18).asInt());
                  obj.setSayAmount(row.getValue(offset + 19).asString());
                  obj.setJournalAmount(row.getValue(offset + 20).asBigDecimal());
                  obj.setReconciled(row.getValue(offset + 21).asBoolean());
                  obj.setReconcileDate(row.getValue(offset + 22).asUtilDate());
                  obj.setCancelBy(row.getValue(offset + 23).asString());
                  obj.setCancelDate(row.getValue(offset + 24).asUtilDate());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCashFlowPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RECONCILED))
        {
            Object possibleBoolean = criteria.get(RECONCILED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RECONCILED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CashFlowPeer.row2Object(row, 1,
                CashFlowPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCashFlowPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CASH_FLOW_ID, criteria.remove(CASH_FLOW_ID));
                                                                                                                                                                                                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RECONCILED))
        {
            Object possibleBoolean = criteria.get(RECONCILED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RECONCILED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                        
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CashFlowPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(RECONCILED))
        {
            Object possibleBoolean = criteria.get(RECONCILED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(RECONCILED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                        
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(CashFlow obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CashFlow obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CashFlow obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CashFlow obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(CashFlow) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CashFlow obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(CashFlow) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CashFlow obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(CashFlow) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CashFlow obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCashFlowPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CASH_FLOW_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( CashFlow obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CASH_FLOW_ID, obj.getCashFlowId());
              criteria.add(CASH_FLOW_NO, obj.getCashFlowNo());
              criteria.add(CASH_FLOW_TYPE, obj.getCashFlowType());
              criteria.add(BANK_ID, obj.getBankId());
              criteria.add(BANK_ISSUER, obj.getBankIssuer());
              criteria.add(REFERENCE_NO, obj.getReferenceNo());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(EXPECTED_DATE, obj.getExpectedDate());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(DUE_DATE, obj.getDueDate());
              criteria.add(CASH_FLOW_AMOUNT, obj.getCashFlowAmount());
              criteria.add(AMOUNT_BASE, obj.getAmountBase());
              criteria.add(USER_NAME, obj.getUserName());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(TRANSACTION_ID, obj.getTransactionId());
              criteria.add(TRANSACTION_NO, obj.getTransactionNo());
              criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(SAY_AMOUNT, obj.getSayAmount());
              criteria.add(JOURNAL_AMOUNT, obj.getJournalAmount());
              criteria.add(RECONCILED, obj.getReconciled());
              criteria.add(RECONCILE_DATE, obj.getReconcileDate());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( CashFlow obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CASH_FLOW_ID, obj.getCashFlowId());
                          criteria.add(CASH_FLOW_NO, obj.getCashFlowNo());
                          criteria.add(CASH_FLOW_TYPE, obj.getCashFlowType());
                          criteria.add(BANK_ID, obj.getBankId());
                          criteria.add(BANK_ISSUER, obj.getBankIssuer());
                          criteria.add(REFERENCE_NO, obj.getReferenceNo());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(EXPECTED_DATE, obj.getExpectedDate());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(DUE_DATE, obj.getDueDate());
                          criteria.add(CASH_FLOW_AMOUNT, obj.getCashFlowAmount());
                          criteria.add(AMOUNT_BASE, obj.getAmountBase());
                          criteria.add(USER_NAME, obj.getUserName());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(TRANSACTION_ID, obj.getTransactionId());
                          criteria.add(TRANSACTION_NO, obj.getTransactionNo());
                          criteria.add(TRANSACTION_TYPE, obj.getTransactionType());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(SAY_AMOUNT, obj.getSayAmount());
                          criteria.add(JOURNAL_AMOUNT, obj.getJournalAmount());
                          criteria.add(RECONCILED, obj.getReconciled());
                          criteria.add(RECONCILE_DATE, obj.getReconcileDate());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlow retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlow retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlow retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        CashFlow retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlow retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (CashFlow)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CASH_FLOW_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.ReceivablePaymentLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ReceivablePaymentAction.java,v 1.12 2009/05/04 01:58:24 albert Exp $ <br>
 *
 * <pre>
 * $Log: ReceivablePaymentAction.java,v $
 * Revision 1.12  2009/05/04 01:58:24  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/11/09 01:12:15  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/07/02 15:38:12  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/06/07 16:47:26  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/03/12 08:54:49  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ReceivablePaymentAction extends TransactionSecureAction
{
   	private static final String s_VIEW_PERM    = "View Receivable Payment";
	private static final String s_CREATE_PERM  = "Create Receivable Payment";
	private static final String s_CANCEL_PERM  = "Cancel Receivable Payment";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
    
    public boolean isAuthorized ( RunData data )
    	throws Exception
    {
    	if (data.getParameters().getInt("save") == 1) {
    		return isAuthorized (data, a_CREATE_PERM);
    	}
    	if (data.getParameters().getInt("save") == 2) {
    		return isAuthorized (data, a_CANCEL_PERM);
    	}
    	else 
    	{
    		return isAuthorized (data, a_VIEW_PERM);
    	}    
    }
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	if (data.getParameters().getInt("save") == 1) {
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("save") == 2) 
    	{
    		doCancel (data,context);
    	}
    	else 
    	{
    		doFind (data,context);
    	}
    }

    public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);
			
			String sCustID = data.getParameters().getString ("CustomerId");
			Date dStartDue = CustomParser.parseDate(data.getParameters().getString("StartDue"));
			Date dEndDue = CustomParser.parseDate(data.getParameters().getString("EndDue"));
			int iStatus = data.getParameters().getInt("Status");
			int iCFStatus = data.getParameters().getInt("CfStatus");

            LargeSelect vTR = ReceivablePaymentTool.findData(
                iCond, sKeywords, sCustID, sLocationID, dStart, dEnd, 
                dStartDue, dEndDue, iLimit, iStatus, iCFStatus, sCurrencyID);
            
            data.getUser().setTemp("findReceivablePaymentResult", vTR);
         }
         catch (Exception _oEx)
         {
        	log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
         }
    }

	public void doUpdate ( RunData data, Context context )
        throws Exception
    {
		doSave (data, context);
    }

	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try
		{
			HttpSession oSes = data.getSession();
			
			ArPayment oAR =  (ArPayment) oSes.getAttribute (s_AR);			
			List vARD = (List) oSes.getAttribute (s_ARD);
			List vCreditMemo = (List) oSes.getAttribute (s_ARDP);

			ReceivablePaymentTool.setHeaderProperties (oAR, vARD, data);
			ReceivablePaymentTool.saveData (oAR, vARD, vCreditMemo);
			
        	data.setMessage(LocaleTool.getString("arp_save_success"));        	
        }
        catch (Exception _oEx) 
		{
        	handleError (data, LocaleTool.getString("arp_save_failed"), _oEx);
        }
    }

	public void doCancel ( RunData data, Context context )
	    throws Exception
	{
		String sID = data.getParameters().getString("ArPaymentId");
		ArPayment oAR = (ArPayment) data.getSession().getAttribute(s_AR);
		List vARD = (List) data.getSession().getAttribute(s_ARD);
		
		try
		{				
			if (oAR == null || vARD == null) 
			{
				oAR = ReceivablePaymentTool.getHeaderByID(sID);
				vARD = ReceivablePaymentTool.getDetailsByID(sID);
			}
			if (oAR != null && oAR.getStatus() != i_PAYMENT_CANCELLED &&  vARD != null)
			{			
				ReceivablePaymentTool.cancelTransaction(oAR, vARD, data.getUser().getName());
				data.getSession().setAttribute(s_AR, oAR);
				data.getSession().setAttribute(s_ARD, vARD);
				
		    	data.setMessage(LocaleTool.getString("arp_cancel_success"));        	
			}
			else
			{
				throw new Exception ("AR Payment / details is not found ");
			}
	    }
	    catch (Exception _oEx) 
		{
        	handleError (data, LocaleTool.getString("arp_cancel_failed"), _oEx);
	    }
	}
	
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
	     	String sLoader = data.getParameters().getString("loader");
	     	TransactionLoader oLoader = null;	     
	     	if (StringUtil.isNotEmpty(sLoader))
            {
                oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
                oLoader.setUserName(data.getUser().getName());
            }
            else
            {
    			oLoader = new ReceivablePaymentLoader(data.getUser().getName());
            }
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}  
}
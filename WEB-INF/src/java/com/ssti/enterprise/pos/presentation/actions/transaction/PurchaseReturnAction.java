package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.PurchaseReturnLoader;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.framework.tools.CustomParser;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/transaction/PurchaseReturnAction.java,v $
 * Purpose: Purchase return action handler
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseReturnAction.java,v 1.24 2009/05/04 01:58:24 albert Exp $
 *
 * $Log: PurchaseReturnAction.java,v $
 * Revision 1.24  2009/05/04 01:58:24  albert
 * *** empty log message ***
 *
 * Revision 1.23  2007/03/05 16:04:24  albert
 * *** empty log message ***
 *
 * Revision 1.22  2006/02/04 05:17:52  albert
 * *** empty log message ***
 *
 * Revision 1.21  2006/01/02 09:54:33  albert
 * *** empty log message ***
 *
 */
public class PurchaseReturnAction extends TransactionSecureAction
{    
    private static final String[] a_CREATE_PERM = {"Purchase Transaction", "View Purchase Return", "Create Purchase Return"};
    private static final String[] a_CANCEL_PERM = {"Purchase Transaction", "View Purchase Return", "Cancel Purchase Return"};
    private static final String[] a_VIEW_PERM = {"Purchase Transaction", "View Purchase Return"};
    
    private static final Log log = LogFactory.getLog(PurchaseReturnAction.class);
	HttpSession oSes = null;
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	
        if (data.getParameters().getInt("save") == 1 ||
            data.getParameters().getString("eventSubmit_doUpdate", null) != null)
        {
        	return isAuthorized(data, a_CREATE_PERM);
        }
        else if (data.getParameters().getInt("save") == 2)
        {
        	return isAuthorized(data, a_CANCEL_PERM);
        }
    	else
    	{
        	return isAuthorized(data, a_VIEW_PERM);
        }
    }

    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	oSes = data.getSession();
    	if (data.getParameters().getInt("save") == 1) 
    	{
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("save") == 2) 
    	{
    		doCancel (data,context);
    	}
    	else 
    	{
    		doFind (data,context);
    	}
    }
    
    public void doFind ( RunData data, Context context )
        throws Exception
    {
    	super.doFind(data);
    	
    	Date dStart = CustomParser.parseDate ( data.getParameters().getString("StartDate") );
    	Date dEnd = CustomParser.parseDate ( data.getParameters().getString("EndDate") );    	
    	String sVendorID = data.getParameters().getString("VendorId");
    	String sReasonID = data.getParameters().getString("ReturnReasonId","");
    	int iStatus = data.getParameters().getInt("Status");
    	
    	LargeSelect vTR = PurchaseReturnTool.findData (
    		iCond, sKeywords, dStart, dEnd, sVendorID, sLocationID,
    		iStatus, iLimit, sCurrencyID, sReasonID
		);
		data.getUser().setTemp("findPurchaseReturnResult", vTR);	
	}

	public void doUpdate ( RunData data, Context context )
        throws Exception
    {	
		doSave (data, context);
    }    
    
	public void doSave ( RunData data, Context context )
        throws Exception
    {
		try 
		{
			oSes = data.getSession();
		    PurchaseReturn oPR = (PurchaseReturn) oSes.getAttribute (s_PRT);
		    List vPRD = (List) oSes.getAttribute (s_PRTD);

		    PurchaseReturnTool.updateDetail(vPRD,data);
		    PurchaseReturnTool.setHeaderProperties(oPR, vPRD, data);		    
			PurchaseReturnTool.saveData (oPR, vPRD);	
			
		    oSes.setAttribute (s_PRT, oPR);
		    oSes.setAttribute (s_PRTD, vPRD);
			
			context.put (s_RET, oPR);
			context.put (s_RET_DET, vPRD);		
					
        	data.setMessage(LocaleTool.getString("pret_save_success"));
        }
        catch (Exception _oEx) 
        {
        	handleError (data, LocaleTool.getString("pret_save_failed"), _oEx);
        }
    }

	public void doCancel ( RunData data, Context context )
	    throws Exception
	{
		try 
		{
			oSes = data.getSession();
		    PurchaseReturn oPR = (PurchaseReturn) oSes.getAttribute (s_PRT);
		    List vPRD = (List) oSes.getAttribute (s_PRTD);
		    PurchaseReturnTool.cancelReturn(oPR, vPRD, data.getUser().getName());		    
	    	data.setMessage(LocaleTool.getString("pret_cancel_success"));
		}
	    catch (Exception _oEx) 
	    {
        	handleError (data, LocaleTool.getString("pret_cancel_failed"), _oEx);
	    }
	}
	
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			PurchaseReturnLoader oLoader = new PurchaseReturnLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	} 
    
}
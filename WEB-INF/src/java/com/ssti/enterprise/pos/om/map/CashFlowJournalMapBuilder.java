package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CashFlowJournalMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CashFlowJournalMapBuilder";

    /**
     * Item
     * @deprecated use CashFlowJournalPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "cash_flow_journal";
    }

  
    /**
     * cash_flow_journal.CASH_FLOW_JOURNAL_ID
     * @return the column name for the CASH_FLOW_JOURNAL_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.CASH_FLOW_JOURNAL_ID constant
     */
    public static String getCashFlowJournal_CashFlowJournalId()
    {
        return "cash_flow_journal.CASH_FLOW_JOURNAL_ID";
    }
  
    /**
     * cash_flow_journal.CASH_FLOW_ID
     * @return the column name for the CASH_FLOW_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.CASH_FLOW_ID constant
     */
    public static String getCashFlowJournal_CashFlowId()
    {
        return "cash_flow_journal.CASH_FLOW_ID";
    }
  
    /**
     * cash_flow_journal.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.ACCOUNT_ID constant
     */
    public static String getCashFlowJournal_AccountId()
    {
        return "cash_flow_journal.ACCOUNT_ID";
    }
  
    /**
     * cash_flow_journal.ACCOUNT_CODE
     * @return the column name for the ACCOUNT_CODE field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.ACCOUNT_CODE constant
     */
    public static String getCashFlowJournal_AccountCode()
    {
        return "cash_flow_journal.ACCOUNT_CODE";
    }
  
    /**
     * cash_flow_journal.ACCOUNT_NAME
     * @return the column name for the ACCOUNT_NAME field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.ACCOUNT_NAME constant
     */
    public static String getCashFlowJournal_AccountName()
    {
        return "cash_flow_journal.ACCOUNT_NAME";
    }
  
    /**
     * cash_flow_journal.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.CURRENCY_ID constant
     */
    public static String getCashFlowJournal_CurrencyId()
    {
        return "cash_flow_journal.CURRENCY_ID";
    }
  
    /**
     * cash_flow_journal.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.CURRENCY_RATE constant
     */
    public static String getCashFlowJournal_CurrencyRate()
    {
        return "cash_flow_journal.CURRENCY_RATE";
    }
  
    /**
     * cash_flow_journal.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.AMOUNT constant
     */
    public static String getCashFlowJournal_Amount()
    {
        return "cash_flow_journal.AMOUNT";
    }
  
    /**
     * cash_flow_journal.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.AMOUNT_BASE constant
     */
    public static String getCashFlowJournal_AmountBase()
    {
        return "cash_flow_journal.AMOUNT_BASE";
    }
  
    /**
     * cash_flow_journal.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.PROJECT_ID constant
     */
    public static String getCashFlowJournal_ProjectId()
    {
        return "cash_flow_journal.PROJECT_ID";
    }
  
    /**
     * cash_flow_journal.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.DEPARTMENT_ID constant
     */
    public static String getCashFlowJournal_DepartmentId()
    {
        return "cash_flow_journal.DEPARTMENT_ID";
    }
  
    /**
     * cash_flow_journal.MEMO
     * @return the column name for the MEMO field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.MEMO constant
     */
    public static String getCashFlowJournal_Memo()
    {
        return "cash_flow_journal.MEMO";
    }
  
    /**
     * cash_flow_journal.DEBIT_CREDIT
     * @return the column name for the DEBIT_CREDIT field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.DEBIT_CREDIT constant
     */
    public static String getCashFlowJournal_DebitCredit()
    {
        return "cash_flow_journal.DEBIT_CREDIT";
    }
  
    /**
     * cash_flow_journal.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.LOCATION_ID constant
     */
    public static String getCashFlowJournal_LocationId()
    {
        return "cash_flow_journal.LOCATION_ID";
    }
  
    /**
     * cash_flow_journal.SUB_LEDGER_TYPE
     * @return the column name for the SUB_LEDGER_TYPE field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.SUB_LEDGER_TYPE constant
     */
    public static String getCashFlowJournal_SubLedgerType()
    {
        return "cash_flow_journal.SUB_LEDGER_TYPE";
    }
  
    /**
     * cash_flow_journal.SUB_LEDGER_ID
     * @return the column name for the SUB_LEDGER_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.SUB_LEDGER_ID constant
     */
    public static String getCashFlowJournal_SubLedgerId()
    {
        return "cash_flow_journal.SUB_LEDGER_ID";
    }
  
    /**
     * cash_flow_journal.SUB_LEDGER_DESC
     * @return the column name for the SUB_LEDGER_DESC field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.SUB_LEDGER_DESC constant
     */
    public static String getCashFlowJournal_SubLedgerDesc()
    {
        return "cash_flow_journal.SUB_LEDGER_DESC";
    }
  
    /**
     * cash_flow_journal.SUB_LEDGER_TRANS_ID
     * @return the column name for the SUB_LEDGER_TRANS_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.SUB_LEDGER_TRANS_ID constant
     */
    public static String getCashFlowJournal_SubLedgerTransId()
    {
        return "cash_flow_journal.SUB_LEDGER_TRANS_ID";
    }
  
    /**
     * cash_flow_journal.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use CashFlowJournalPeer.cash_flow_journal.CASH_FLOW_TYPE_ID constant
     */
    public static String getCashFlowJournal_CashFlowTypeId()
    {
        return "cash_flow_journal.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("cash_flow_journal");
        TableMap tMap = dbMap.getTable("cash_flow_journal");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("cash_flow_journal.CASH_FLOW_JOURNAL_ID", "");
                          tMap.addForeignKey(
                "cash_flow_journal.CASH_FLOW_ID", "" , "cash_flow" ,
                "cash_flow_id");
                          tMap.addColumn("cash_flow_journal.ACCOUNT_ID", "");
                          tMap.addColumn("cash_flow_journal.ACCOUNT_CODE", "");
                          tMap.addColumn("cash_flow_journal.ACCOUNT_NAME", "");
                          tMap.addColumn("cash_flow_journal.CURRENCY_ID", "");
                            tMap.addColumn("cash_flow_journal.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("cash_flow_journal.AMOUNT", bd_ZERO);
                            tMap.addColumn("cash_flow_journal.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("cash_flow_journal.PROJECT_ID", "");
                          tMap.addColumn("cash_flow_journal.DEPARTMENT_ID", "");
                          tMap.addColumn("cash_flow_journal.MEMO", "");
                            tMap.addColumn("cash_flow_journal.DEBIT_CREDIT", Integer.valueOf(0));
                          tMap.addColumn("cash_flow_journal.LOCATION_ID", "");
                            tMap.addColumn("cash_flow_journal.SUB_LEDGER_TYPE", Integer.valueOf(0));
                          tMap.addColumn("cash_flow_journal.SUB_LEDGER_ID", "");
                          tMap.addColumn("cash_flow_journal.SUB_LEDGER_DESC", "");
                          tMap.addColumn("cash_flow_journal.SUB_LEDGER_TRANS_ID", "");
                          tMap.addColumn("cash_flow_journal.CASH_FLOW_TYPE_ID", "");
          }
}

package com.ssti.enterprise.pos.print.helper;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.velocity.app.Velocity;

import com.ssti.enterprise.pos.om.PettyCash;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.financial.PettyCashTool;
import com.ssti.framework.print.helper.BasePrintHelper;
import com.ssti.framework.print.helper.PrintAttributes;
import com.ssti.framework.print.helper.PrintHelper;
import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PettyCashPrintHelper.java,v 1.9 2009/05/04 02:03:33 albert Exp $ <br>
 *
 * <pre>
 * $Log: PettyCashPrintHelper.java,v $
 * Revision 1.9  2009/05/04 02:03:33  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/12/20 13:48:42  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/09/17 07:36:10  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/06/07 16:48:45  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/04/17 13:04:57  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PettyCashPrintHelper 
	extends BasePrintHelper 
	implements PrintHelper, TransactionAttributes 
{    
    String s_PDF_TPL_NAME  = "/print/PettyCashPDF.vm";
    String s_TXT_TPL_NAME  = "/print/PettyCashTXT.vm";
    String s_HTML_TPL_NAME = "/print/PettyCashHTML.vm";
    
	private PettyCash oTR = null;
	
	/**
	 * get Printed Text (normal)
	 */
	
    public String getPrintedText (Map oParam, int _iPrintType, HttpSession _oSession)	
    	throws Exception
    {
    	String sID = getString(oParam, s_PARAM_ID);
		
		oTR = PettyCashTool.getPettyCashByID(sID, null);
				
        oCtx.put(s_CTX_TRANS, oTR);
        checkTemplate (oParam);

        if (_iPrintType == PrintAttributes.i_DOC_TYPE_TEXT) {
        	Velocity.mergeTemplate(s_TXT_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_PDF){
        	Velocity.mergeTemplate(s_PDF_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }
        else if (_iPrintType == PrintAttributes.i_DOC_TYPE_HTML){
        	Velocity.mergeTemplate(s_HTML_TPL_NAME, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );        
        }               
        return oWriter.toString();
    }

    public String getPrintedText (Map oParam, int _iPrintType, int _iDetailPerPage, HttpSession _oSession)	
    	throws Exception
    {
        return getPrintedText (oParam, _iPrintType, _oSession);
    }    
}

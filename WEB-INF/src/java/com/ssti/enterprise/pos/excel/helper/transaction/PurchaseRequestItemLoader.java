package com.ssti.enterprise.pos.excel.helper.transaction;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.excel.helper.BaseExcelLoader;
import com.ssti.enterprise.pos.excel.helper.ItemListLoader;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/PurchaseRequestLoader.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseRequestLoader.java,v 1.12 2008/08/17 02:17:21 albert Exp $
 *
 * $Log: PurchaseRequestLoader.java,v $
 * Revision 1.12  2008/08/17 02:17:21  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/06/30 13:31:56  albert
 * *** empty log message ***
 *
 * Revision 1.10  2006/01/21 12:08:41  albert
 * *** empty log message ***
 *
 */
public class PurchaseRequestItemLoader extends BaseExcelLoader implements ItemListLoader 
{    
    private static Log log = LogFactory.getLog (PurchaseRequestItemLoader.class);
    private String sLocationID;
    private boolean bForceZero = false;
    
    
    public PurchaseRequestItemLoader(String _sLocationID)
        throws Exception    
    {
    	sLocationID = _sLocationID;
    }

    protected void updateList (HSSFRow _oRow, short _iIdx) 
        throws Exception
    {
    	int iRow = _oRow.getRowNum();
   		short i = 0;
		
		String sCode = getString(_oRow,_iIdx + i); i++;				
		String sName = getString(_oRow,_iIdx + i);		
		double dRequestQty = getBigDecimal(_oRow,_iIdx + 5).doubleValue();
		
		Item oItem = ItemTool.getItemByCode(sCode);        
        if(oItem != null && dRequestQty > 0)
        {
        	PurchaseRequestDetail oRQD = new PurchaseRequestDetail();
            oRQD.setItemId(oItem.getItemId());
            oRQD.setItemCode(oItem.getItemCode());
            oRQD.setItemName(oItem.getItemName());
            oRQD.setDescription(oItem.getDescription());
            if(oRQD.getDescription() == null) {
            	oRQD.setDescription(oItem.getItemName());
            }
            String sUnitID = oItem.getUnitId();
        	Unit oUnit = UnitTool.getUnitByID(sUnitID);
        	boolean bBase = true;
        	
            if(oUnit != null)
            {            
            	if (StringUtil.isEqual(oUnit.getUnitId(),oItem.getPurchaseUnitId()))
            	{
            		bBase = false;
            		oUnit = UnitTool.getUnitByID(oItem.getUnitId());
            		dRequestQty = dRequestQty * oItem.getUnitConversion().doubleValue();
            	}            	
                oRQD.setUnitId(oUnit.getUnitId());
                oRQD.setUnitCode(oUnit.getUnitCode());
            }
            else
            {
            	setError (iRow, sCode, sName, "Item contains invalid Unit ");
            }    
			oRQD.setMinimumStock  (bd_ZERO);
			oRQD.setMaximumStock  (bd_ZERO);
			oRQD.setReorderPoint  (bd_ZERO);
			oRQD.setRecommendedQty(bd_ZERO);			
            oRQD.setRequestQty(new BigDecimal(dRequestQty)); 
			oRQD.setSentQty (bd_ZERO);

            InventoryLocation oInvLoc = 
                	InventoryLocationTool.getDataByItemAndLocationID(oItem.getItemId(), sLocationID);
            
            double dQtyOH = 0;
			if(oInvLoc != null)
			{
				dQtyOH = oInvLoc.getCurrentQty().doubleValue();
				oRQD.setCurrentQty (oInvLoc.getCurrentQty());
				oRQD.setCostPerUnit(oInvLoc.getItemCost());
			}
			else
			{
				oRQD.setCurrentQty  (bd_ZERO);
				oRQD.setCostPerUnit (bd_ZERO);
			}
            
            if(dRequestQty == 0) 
            {
				ItemInventory oItemInv = ItemInventoryTool.getItemInventory(oRQD.getItemId(), sLocationID);
				if(oItemInv != null)
				{
					oRQD.setMinimumStock  (oItemInv.getMinimumQty());
					oRQD.setMaximumStock  (oItemInv.getMaximumQty());
					oRQD.setReorderPoint  (oItemInv.getReorderPoint());
				}				
				double dMax = oRQD.getMaximumStock().doubleValue(); 
				if (dMax > 0 && dQtyOH < dMax) 
				{
					dRequestQty = oRQD.getMaximumStock().doubleValue() - dQtyOH;
				}
				oRQD.setRecommendedQty (new BigDecimal(dRequestQty));
				//if current qty < reorder point 
				if(oRQD.getCurrentQty().doubleValue() <= oRQD.getReorderPoint().doubleValue() && dRequestQty > 0)
				{					
					oRQD.setRequestQty (new BigDecimal(dRequestQty));
				}				   
            }
            m_vDataList.add(oRQD);
        }
        else
        {
        	setError (iRow, sCode, sName, "Item Not Found");
        }
    }        
    
    void setError (int _iRow, String _sCode, String _sName, String _sMsg)
    {
		m_iError++;
		
		StringBuilder oMsg = new StringBuilder();
		oMsg.append (m_iError);					
		oMsg.append (". ERROR Loading ROW : ");
		oMsg.append (_iRow + 1);
		oMsg.append (",ITEM CODE, " + _sCode );
		oMsg.append (",ITEM NAME, " + _sName );
		oMsg.append (_sMsg);
		oMsg.append ("\n");	

		m_oError.append (oMsg);
		log.error(oMsg);
    }
}

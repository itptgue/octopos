package com.ssti.enterprise.pos.model;

import java.math.BigDecimal;
import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.om.BatchTransaction;
import com.ssti.enterprise.pos.om.Item;

public interface InventoryDetailOM extends Persistent
{
	public String getTransDetId();
	public String getTransId();
	
	public String getItemId();
	public String getItemCode();
	public String getItemName();
	public String getDescription();

	public Item getItem();

	public String getUnitCode();
	
	public BigDecimal getQty();
	public BigDecimal getQtyBase();
	public BigDecimal getItemPrice();
	
	public void setBatchTransaction (BatchTransaction trans);
	public BatchTransaction getBatchTransaction ();
	
	public void setSerialTrans (List v);
	public List getSerialTrans ();
	
	public void setIndexNo(int i);	
}

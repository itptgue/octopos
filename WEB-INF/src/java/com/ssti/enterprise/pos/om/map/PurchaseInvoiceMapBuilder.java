package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseInvoiceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseInvoiceMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseInvoicePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_invoice";
    }

  
    /**
     * purchase_invoice.PURCHASE_INVOICE_ID
     * @return the column name for the PURCHASE_INVOICE_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PURCHASE_INVOICE_ID constant
     */
    public static String getPurchaseInvoice_PurchaseInvoiceId()
    {
        return "purchase_invoice.PURCHASE_INVOICE_ID";
    }
  
    /**
     * purchase_invoice.PAYMENT_STATUS
     * @return the column name for the PAYMENT_STATUS field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PAYMENT_STATUS constant
     */
    public static String getPurchaseInvoice_PaymentStatus()
    {
        return "purchase_invoice.PAYMENT_STATUS";
    }
  
    /**
     * purchase_invoice.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.LOCATION_ID constant
     */
    public static String getPurchaseInvoice_LocationId()
    {
        return "purchase_invoice.LOCATION_ID";
    }
  
    /**
     * purchase_invoice.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.LOCATION_NAME constant
     */
    public static String getPurchaseInvoice_LocationName()
    {
        return "purchase_invoice.LOCATION_NAME";
    }
  
    /**
     * purchase_invoice.VENDOR_INVOICE_NO
     * @return the column name for the VENDOR_INVOICE_NO field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.VENDOR_INVOICE_NO constant
     */
    public static String getPurchaseInvoice_VendorInvoiceNo()
    {
        return "purchase_invoice.VENDOR_INVOICE_NO";
    }
  
    /**
     * purchase_invoice.PURCHASE_INVOICE_NO
     * @return the column name for the PURCHASE_INVOICE_NO field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PURCHASE_INVOICE_NO constant
     */
    public static String getPurchaseInvoice_PurchaseInvoiceNo()
    {
        return "purchase_invoice.PURCHASE_INVOICE_NO";
    }
  
    /**
     * purchase_invoice.PURCHASE_INVOICE_DATE
     * @return the column name for the PURCHASE_INVOICE_DATE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PURCHASE_INVOICE_DATE constant
     */
    public static String getPurchaseInvoice_PurchaseInvoiceDate()
    {
        return "purchase_invoice.PURCHASE_INVOICE_DATE";
    }
  
    /**
     * purchase_invoice.PAYMENT_DUE_DATE
     * @return the column name for the PAYMENT_DUE_DATE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PAYMENT_DUE_DATE constant
     */
    public static String getPurchaseInvoice_PaymentDueDate()
    {
        return "purchase_invoice.PAYMENT_DUE_DATE";
    }
  
    /**
     * purchase_invoice.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.VENDOR_ID constant
     */
    public static String getPurchaseInvoice_VendorId()
    {
        return "purchase_invoice.VENDOR_ID";
    }
  
    /**
     * purchase_invoice.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.VENDOR_NAME constant
     */
    public static String getPurchaseInvoice_VendorName()
    {
        return "purchase_invoice.VENDOR_NAME";
    }
  
    /**
     * purchase_invoice.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.TOTAL_QTY constant
     */
    public static String getPurchaseInvoice_TotalQty()
    {
        return "purchase_invoice.TOTAL_QTY";
    }
  
    /**
     * purchase_invoice.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.TOTAL_AMOUNT constant
     */
    public static String getPurchaseInvoice_TotalAmount()
    {
        return "purchase_invoice.TOTAL_AMOUNT";
    }
  
    /**
     * purchase_invoice.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.TOTAL_DISCOUNT_PCT constant
     */
    public static String getPurchaseInvoice_TotalDiscountPct()
    {
        return "purchase_invoice.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * purchase_invoice.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.TOTAL_DISCOUNT constant
     */
    public static String getPurchaseInvoice_TotalDiscount()
    {
        return "purchase_invoice.TOTAL_DISCOUNT";
    }
  
    /**
     * purchase_invoice.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.TOTAL_TAX constant
     */
    public static String getPurchaseInvoice_TotalTax()
    {
        return "purchase_invoice.TOTAL_TAX";
    }
  
    /**
     * purchase_invoice.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.COURIER_ID constant
     */
    public static String getPurchaseInvoice_CourierId()
    {
        return "purchase_invoice.COURIER_ID";
    }
  
    /**
     * purchase_invoice.SHIPPING_PRICE
     * @return the column name for the SHIPPING_PRICE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.SHIPPING_PRICE constant
     */
    public static String getPurchaseInvoice_ShippingPrice()
    {
        return "purchase_invoice.SHIPPING_PRICE";
    }
  
    /**
     * purchase_invoice.TOTAL_EXPENSE
     * @return the column name for the TOTAL_EXPENSE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.TOTAL_EXPENSE constant
     */
    public static String getPurchaseInvoice_TotalExpense()
    {
        return "purchase_invoice.TOTAL_EXPENSE";
    }
  
    /**
     * purchase_invoice.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.CREATE_BY constant
     */
    public static String getPurchaseInvoice_CreateBy()
    {
        return "purchase_invoice.CREATE_BY";
    }
  
    /**
     * purchase_invoice.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.REMARK constant
     */
    public static String getPurchaseInvoice_Remark()
    {
        return "purchase_invoice.REMARK";
    }
  
    /**
     * purchase_invoice.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PAYMENT_TYPE_ID constant
     */
    public static String getPurchaseInvoice_PaymentTypeId()
    {
        return "purchase_invoice.PAYMENT_TYPE_ID";
    }
  
    /**
     * purchase_invoice.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PAYMENT_TERM_ID constant
     */
    public static String getPurchaseInvoice_PaymentTermId()
    {
        return "purchase_invoice.PAYMENT_TERM_ID";
    }
  
    /**
     * purchase_invoice.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.CURRENCY_ID constant
     */
    public static String getPurchaseInvoice_CurrencyId()
    {
        return "purchase_invoice.CURRENCY_ID";
    }
  
    /**
     * purchase_invoice.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.CURRENCY_RATE constant
     */
    public static String getPurchaseInvoice_CurrencyRate()
    {
        return "purchase_invoice.CURRENCY_RATE";
    }
  
    /**
     * purchase_invoice.PAID_AMOUNT
     * @return the column name for the PAID_AMOUNT field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.PAID_AMOUNT constant
     */
    public static String getPurchaseInvoice_PaidAmount()
    {
        return "purchase_invoice.PAID_AMOUNT";
    }
  
    /**
     * purchase_invoice.STATUS
     * @return the column name for the STATUS field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.STATUS constant
     */
    public static String getPurchaseInvoice_Status()
    {
        return "purchase_invoice.STATUS";
    }
  
    /**
     * purchase_invoice.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.BANK_ID constant
     */
    public static String getPurchaseInvoice_BankId()
    {
        return "purchase_invoice.BANK_ID";
    }
  
    /**
     * purchase_invoice.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.BANK_ISSUER constant
     */
    public static String getPurchaseInvoice_BankIssuer()
    {
        return "purchase_invoice.BANK_ISSUER";
    }
  
    /**
     * purchase_invoice.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.DUE_DATE constant
     */
    public static String getPurchaseInvoice_DueDate()
    {
        return "purchase_invoice.DUE_DATE";
    }
  
    /**
     * purchase_invoice.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.REFERENCE_NO constant
     */
    public static String getPurchaseInvoice_ReferenceNo()
    {
        return "purchase_invoice.REFERENCE_NO";
    }
  
    /**
     * purchase_invoice.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.CASH_FLOW_TYPE_ID constant
     */
    public static String getPurchaseInvoice_CashFlowTypeId()
    {
        return "purchase_invoice.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * purchase_invoice.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.FOB_ID constant
     */
    public static String getPurchaseInvoice_FobId()
    {
        return "purchase_invoice.FOB_ID";
    }
  
    /**
     * purchase_invoice.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.IS_TAXABLE constant
     */
    public static String getPurchaseInvoice_IsTaxable()
    {
        return "purchase_invoice.IS_TAXABLE";
    }
  
    /**
     * purchase_invoice.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.IS_INCLUSIVE_TAX constant
     */
    public static String getPurchaseInvoice_IsInclusiveTax()
    {
        return "purchase_invoice.IS_INCLUSIVE_TAX";
    }
  
    /**
     * purchase_invoice.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.FISCAL_RATE constant
     */
    public static String getPurchaseInvoice_FiscalRate()
    {
        return "purchase_invoice.FISCAL_RATE";
    }
  
    /**
     * purchase_invoice.FREIGHT_AS_COST
     * @return the column name for the FREIGHT_AS_COST field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.FREIGHT_AS_COST constant
     */
    public static String getPurchaseInvoice_FreightAsCost()
    {
        return "purchase_invoice.FREIGHT_AS_COST";
    }
  
    /**
     * purchase_invoice.FREIGHT_ACCOUNT_ID
     * @return the column name for the FREIGHT_ACCOUNT_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.FREIGHT_ACCOUNT_ID constant
     */
    public static String getPurchaseInvoice_FreightAccountId()
    {
        return "purchase_invoice.FREIGHT_ACCOUNT_ID";
    }
  
    /**
     * purchase_invoice.FREIGHT_VENDOR_ID
     * @return the column name for the FREIGHT_VENDOR_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.FREIGHT_VENDOR_ID constant
     */
    public static String getPurchaseInvoice_FreightVendorId()
    {
        return "purchase_invoice.FREIGHT_VENDOR_ID";
    }
  
    /**
     * purchase_invoice.FREIGHT_CURRENCY_ID
     * @return the column name for the FREIGHT_CURRENCY_ID field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.FREIGHT_CURRENCY_ID constant
     */
    public static String getPurchaseInvoice_FreightCurrencyId()
    {
        return "purchase_invoice.FREIGHT_CURRENCY_ID";
    }
  
    /**
     * purchase_invoice.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.CANCEL_BY constant
     */
    public static String getPurchaseInvoice_CancelBy()
    {
        return "purchase_invoice.CANCEL_BY";
    }
  
    /**
     * purchase_invoice.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use PurchaseInvoicePeer.purchase_invoice.CANCEL_DATE constant
     */
    public static String getPurchaseInvoice_CancelDate()
    {
        return "purchase_invoice.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_invoice");
        TableMap tMap = dbMap.getTable("purchase_invoice");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_invoice.PURCHASE_INVOICE_ID", "");
                            tMap.addColumn("purchase_invoice.PAYMENT_STATUS", Integer.valueOf(0));
                          tMap.addColumn("purchase_invoice.LOCATION_ID", "");
                          tMap.addColumn("purchase_invoice.LOCATION_NAME", "");
                          tMap.addColumn("purchase_invoice.VENDOR_INVOICE_NO", "");
                          tMap.addColumn("purchase_invoice.PURCHASE_INVOICE_NO", "");
                          tMap.addColumn("purchase_invoice.PURCHASE_INVOICE_DATE", new Date());
                          tMap.addColumn("purchase_invoice.PAYMENT_DUE_DATE", new Date());
                          tMap.addColumn("purchase_invoice.VENDOR_ID", "");
                          tMap.addColumn("purchase_invoice.VENDOR_NAME", "");
                            tMap.addColumn("purchase_invoice.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("purchase_invoice.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("purchase_invoice.TOTAL_DISCOUNT_PCT", "");
                            tMap.addColumn("purchase_invoice.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("purchase_invoice.TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("purchase_invoice.COURIER_ID", "");
                            tMap.addColumn("purchase_invoice.SHIPPING_PRICE", bd_ZERO);
                            tMap.addColumn("purchase_invoice.TOTAL_EXPENSE", bd_ZERO);
                          tMap.addColumn("purchase_invoice.CREATE_BY", "");
                          tMap.addColumn("purchase_invoice.REMARK", "");
                          tMap.addColumn("purchase_invoice.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("purchase_invoice.PAYMENT_TERM_ID", "");
                          tMap.addColumn("purchase_invoice.CURRENCY_ID", "");
                            tMap.addColumn("purchase_invoice.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("purchase_invoice.PAID_AMOUNT", bd_ZERO);
                            tMap.addColumn("purchase_invoice.STATUS", Integer.valueOf(0));
                          tMap.addColumn("purchase_invoice.BANK_ID", "");
                          tMap.addColumn("purchase_invoice.BANK_ISSUER", "");
                          tMap.addColumn("purchase_invoice.DUE_DATE", new Date());
                          tMap.addColumn("purchase_invoice.REFERENCE_NO", "");
                          tMap.addColumn("purchase_invoice.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("purchase_invoice.FOB_ID", "");
                          tMap.addColumn("purchase_invoice.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("purchase_invoice.IS_INCLUSIVE_TAX", Boolean.TRUE);
                            tMap.addColumn("purchase_invoice.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("purchase_invoice.FREIGHT_AS_COST", Boolean.TRUE);
                          tMap.addColumn("purchase_invoice.FREIGHT_ACCOUNT_ID", "");
                          tMap.addColumn("purchase_invoice.FREIGHT_VENDOR_ID", "");
                          tMap.addColumn("purchase_invoice.FREIGHT_CURRENCY_ID", "");
                          tMap.addColumn("purchase_invoice.CANCEL_BY", "");
                          tMap.addColumn("purchase_invoice.CANCEL_DATE", new Date());
          }
}

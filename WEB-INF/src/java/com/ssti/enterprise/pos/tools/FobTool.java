package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import com.ssti.enterprise.pos.manager.FOBCourierManager;
import com.ssti.enterprise.pos.om.Fob;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: FobTool.java,v 1.6 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: FobTool.java,v $
 * Revision 1.6  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/07/10 04:51:48  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class FobTool extends BaseTool 
{
	public static List getAllFob()
		throws Exception
	{
		return FOBCourierManager.getInstance().getAllFob();
	}
	
	public static Fob getFobByID(String _sID)
    	throws Exception
    {
		return FOBCourierManager.getInstance().getFobByID(_sID, null);
	}

	public static Fob getFobByID(String _sID, Connection _oConn)
		throws Exception
	{
		return FOBCourierManager.getInstance().getFobByID(_sID, _oConn);
	}
	
	public static String getFobCodeByID(String _sID)
		throws Exception
	{
		Fob oFob = getFobByID (_sID);
		if (oFob != null)
		{
			return oFob.getFobCode();
		}
		return "";
	}
}
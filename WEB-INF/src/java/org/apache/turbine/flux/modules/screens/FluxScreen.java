package org.apache.turbine.flux.modules.screens;

import org.apache.turbine.flux.tools.FluxTool;
import org.apache.turbine.modules.screens.VelocityScreen;
import org.apache.turbine.services.velocity.TurbineVelocity;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.velocity.context.Context;

public abstract class FluxScreen extends VelocityScreen
{

    public FluxScreen()
    {
    }

    protected void doBuildTemplate(RunData data)
        throws Exception
    {
        Context context = TurbineVelocity.getContext(data);
        if(isAuthorized(data))
        {
        	doBuildTemplate(data, context);
        }
    }

    public void doBuildTemplate(RunData rundata, Context context1)
    {
    }

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        boolean isAuthorized = false;
        String fluxAdminRole = FluxTool.s_ADMIN;
        AccessControlList acl = data.getACL();
        
        if(acl == null)
        {
            data.setScreenTemplate(FluxTool.CONFIG.getString("template.login"));
            return false;
        }
        if(acl != null && !acl.hasRole(fluxAdminRole))
        {
            data.setScreenTemplate(FluxTool.CONFIG.getString("template.nopermission"));
            isAuthorized = false;
        } 
        else if(acl.hasRole(fluxAdminRole))
        {
			isAuthorized = true;
        }
        return isAuthorized;
    }

    private static final Boolean TRUE = Boolean.valueOf(true);

}
package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseInvoiceDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseInvoiceDetailMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseInvoiceDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_invoice_detail";
    }

  
    /**
     * purchase_invoice_detail.PURCHASE_INVOICE_DETAIL_ID
     * @return the column name for the PURCHASE_INVOICE_DETAIL_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.PURCHASE_INVOICE_DETAIL_ID constant
     */
    public static String getPurchaseInvoiceDetail_PurchaseInvoiceDetailId()
    {
        return "purchase_invoice_detail.PURCHASE_INVOICE_DETAIL_ID";
    }
  
    /**
     * purchase_invoice_detail.PURCHASE_ORDER_ID
     * @return the column name for the PURCHASE_ORDER_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.PURCHASE_ORDER_ID constant
     */
    public static String getPurchaseInvoiceDetail_PurchaseOrderId()
    {
        return "purchase_invoice_detail.PURCHASE_ORDER_ID";
    }
  
    /**
     * purchase_invoice_detail.PURCHASE_RECEIPT_ID
     * @return the column name for the PURCHASE_RECEIPT_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.PURCHASE_RECEIPT_ID constant
     */
    public static String getPurchaseInvoiceDetail_PurchaseReceiptId()
    {
        return "purchase_invoice_detail.PURCHASE_RECEIPT_ID";
    }
  
    /**
     * purchase_invoice_detail.PURCHASE_RECEIPT_DETAIL_ID
     * @return the column name for the PURCHASE_RECEIPT_DETAIL_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.PURCHASE_RECEIPT_DETAIL_ID constant
     */
    public static String getPurchaseInvoiceDetail_PurchaseReceiptDetailId()
    {
        return "purchase_invoice_detail.PURCHASE_RECEIPT_DETAIL_ID";
    }
  
    /**
     * purchase_invoice_detail.PURCHASE_INVOICE_ID
     * @return the column name for the PURCHASE_INVOICE_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.PURCHASE_INVOICE_ID constant
     */
    public static String getPurchaseInvoiceDetail_PurchaseInvoiceId()
    {
        return "purchase_invoice_detail.PURCHASE_INVOICE_ID";
    }
  
    /**
     * purchase_invoice_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.INDEX_NO constant
     */
    public static String getPurchaseInvoiceDetail_IndexNo()
    {
        return "purchase_invoice_detail.INDEX_NO";
    }
  
    /**
     * purchase_invoice_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.ITEM_ID constant
     */
    public static String getPurchaseInvoiceDetail_ItemId()
    {
        return "purchase_invoice_detail.ITEM_ID";
    }
  
    /**
     * purchase_invoice_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.ITEM_CODE constant
     */
    public static String getPurchaseInvoiceDetail_ItemCode()
    {
        return "purchase_invoice_detail.ITEM_CODE";
    }
  
    /**
     * purchase_invoice_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.ITEM_NAME constant
     */
    public static String getPurchaseInvoiceDetail_ItemName()
    {
        return "purchase_invoice_detail.ITEM_NAME";
    }
  
    /**
     * purchase_invoice_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.DESCRIPTION constant
     */
    public static String getPurchaseInvoiceDetail_Description()
    {
        return "purchase_invoice_detail.DESCRIPTION";
    }
  
    /**
     * purchase_invoice_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.UNIT_ID constant
     */
    public static String getPurchaseInvoiceDetail_UnitId()
    {
        return "purchase_invoice_detail.UNIT_ID";
    }
  
    /**
     * purchase_invoice_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.UNIT_CODE constant
     */
    public static String getPurchaseInvoiceDetail_UnitCode()
    {
        return "purchase_invoice_detail.UNIT_CODE";
    }
  
    /**
     * purchase_invoice_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.TAX_ID constant
     */
    public static String getPurchaseInvoiceDetail_TaxId()
    {
        return "purchase_invoice_detail.TAX_ID";
    }
  
    /**
     * purchase_invoice_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.TAX_AMOUNT constant
     */
    public static String getPurchaseInvoiceDetail_TaxAmount()
    {
        return "purchase_invoice_detail.TAX_AMOUNT";
    }
  
    /**
     * purchase_invoice_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.SUB_TOTAL_TAX constant
     */
    public static String getPurchaseInvoiceDetail_SubTotalTax()
    {
        return "purchase_invoice_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * purchase_invoice_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.DISCOUNT constant
     */
    public static String getPurchaseInvoiceDetail_Discount()
    {
        return "purchase_invoice_detail.DISCOUNT";
    }
  
    /**
     * purchase_invoice_detail.SUB_TOTAL_DISC
     * @return the column name for the SUB_TOTAL_DISC field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.SUB_TOTAL_DISC constant
     */
    public static String getPurchaseInvoiceDetail_SubTotalDisc()
    {
        return "purchase_invoice_detail.SUB_TOTAL_DISC";
    }
  
    /**
     * purchase_invoice_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.QTY constant
     */
    public static String getPurchaseInvoiceDetail_Qty()
    {
        return "purchase_invoice_detail.QTY";
    }
  
    /**
     * purchase_invoice_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.QTY_BASE constant
     */
    public static String getPurchaseInvoiceDetail_QtyBase()
    {
        return "purchase_invoice_detail.QTY_BASE";
    }
  
    /**
     * purchase_invoice_detail.RETURNED_QTY
     * @return the column name for the RETURNED_QTY field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.RETURNED_QTY constant
     */
    public static String getPurchaseInvoiceDetail_ReturnedQty()
    {
        return "purchase_invoice_detail.RETURNED_QTY";
    }
  
    /**
     * purchase_invoice_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.ITEM_PRICE constant
     */
    public static String getPurchaseInvoiceDetail_ItemPrice()
    {
        return "purchase_invoice_detail.ITEM_PRICE";
    }
  
    /**
     * purchase_invoice_detail.WEIGHT
     * @return the column name for the WEIGHT field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.WEIGHT constant
     */
    public static String getPurchaseInvoiceDetail_Weight()
    {
        return "purchase_invoice_detail.WEIGHT";
    }
  
    /**
     * purchase_invoice_detail.SUB_TOTAL_EXP
     * @return the column name for the SUB_TOTAL_EXP field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.SUB_TOTAL_EXP constant
     */
    public static String getPurchaseInvoiceDetail_SubTotalExp()
    {
        return "purchase_invoice_detail.SUB_TOTAL_EXP";
    }
  
    /**
     * purchase_invoice_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.SUB_TOTAL constant
     */
    public static String getPurchaseInvoiceDetail_SubTotal()
    {
        return "purchase_invoice_detail.SUB_TOTAL";
    }
  
    /**
     * purchase_invoice_detail.LAST_PURCHASE
     * @return the column name for the LAST_PURCHASE field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.LAST_PURCHASE constant
     */
    public static String getPurchaseInvoiceDetail_LastPurchase()
    {
        return "purchase_invoice_detail.LAST_PURCHASE";
    }
  
    /**
     * purchase_invoice_detail.COST_PER_UNIT
     * @return the column name for the COST_PER_UNIT field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.COST_PER_UNIT constant
     */
    public static String getPurchaseInvoiceDetail_CostPerUnit()
    {
        return "purchase_invoice_detail.COST_PER_UNIT";
    }
  
    /**
     * purchase_invoice_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.PROJECT_ID constant
     */
    public static String getPurchaseInvoiceDetail_ProjectId()
    {
        return "purchase_invoice_detail.PROJECT_ID";
    }
  
    /**
     * purchase_invoice_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use PurchaseInvoiceDetailPeer.purchase_invoice_detail.DEPARTMENT_ID constant
     */
    public static String getPurchaseInvoiceDetail_DepartmentId()
    {
        return "purchase_invoice_detail.DEPARTMENT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_invoice_detail");
        TableMap tMap = dbMap.getTable("purchase_invoice_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_invoice_detail.PURCHASE_INVOICE_DETAIL_ID", "");
                          tMap.addColumn("purchase_invoice_detail.PURCHASE_ORDER_ID", "");
                          tMap.addColumn("purchase_invoice_detail.PURCHASE_RECEIPT_ID", "");
                          tMap.addColumn("purchase_invoice_detail.PURCHASE_RECEIPT_DETAIL_ID", "");
                          tMap.addForeignKey(
                "purchase_invoice_detail.PURCHASE_INVOICE_ID", "" , "purchase_invoice" ,
                "purchase_invoice_id");
                            tMap.addColumn("purchase_invoice_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("purchase_invoice_detail.ITEM_ID", "");
                          tMap.addColumn("purchase_invoice_detail.ITEM_CODE", "");
                          tMap.addColumn("purchase_invoice_detail.ITEM_NAME", "");
                          tMap.addColumn("purchase_invoice_detail.DESCRIPTION", "");
                          tMap.addColumn("purchase_invoice_detail.UNIT_ID", "");
                          tMap.addColumn("purchase_invoice_detail.UNIT_CODE", "");
                          tMap.addColumn("purchase_invoice_detail.TAX_ID", "");
                            tMap.addColumn("purchase_invoice_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.SUB_TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("purchase_invoice_detail.DISCOUNT", "");
                            tMap.addColumn("purchase_invoice_detail.SUB_TOTAL_DISC", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.QTY", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.RETURNED_QTY", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.ITEM_PRICE", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.WEIGHT", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.SUB_TOTAL_EXP", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.SUB_TOTAL", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.LAST_PURCHASE", bd_ZERO);
                            tMap.addColumn("purchase_invoice_detail.COST_PER_UNIT", bd_ZERO);
                          tMap.addColumn("purchase_invoice_detail.PROJECT_ID", "");
                          tMap.addColumn("purchase_invoice_detail.DEPARTMENT_ID", "");
          }
}

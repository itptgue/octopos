package com.ssti.enterprise.pos.excel.helper;

import java.io.InputStream;
import java.util.List;

public interface ItemListLoader
{    		
	public List getListResult();

	public int getTotalRows();
	
	public void loadData (InputStream _oInput)	throws Exception;	
}
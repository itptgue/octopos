package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Courier;
import com.ssti.enterprise.pos.om.CourierPeer;
import com.ssti.enterprise.pos.om.Fob;
import com.ssti.enterprise.pos.om.FobPeer;
import com.ssti.framework.tools.StringUtil;

public class FOBCourierManager
{   
	private static Log log = LogFactory.getLog(FOBCourierManager.class);
	 
    private static FOBCourierManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private FOBCourierManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static FOBCourierManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (FOBCourierManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new FOBCourierManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllFob ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllFob");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (FobPeer.DESCRIPTION);	
			List vData = FobPeer.doSelect(oCrit);
			oCache.put(new Element ("AllFob", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public Fob getFobByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Fob" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(FobPeer.FOB_ID, _sID);
				List vData = FobPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Fob" + _sID, (Serializable)vData.get(0))); 
					return (Fob) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Fob) oElement.getValue();
		}
	}
	
	public List getAllCourier ()
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllCourier");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (CourierPeer.DESCRIPTION);	
			List vData = CourierPeer.doSelect(oCrit);
			oCache.put(new Element ("AllCourier", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	
	
	public Courier getCourierByID(String _sID, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Courier" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CourierPeer.COURIER_ID, _sID);
				List vData = CourierPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Courier" + _sID, (Serializable)vData.get(0))); 
					return (Courier) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Courier) oElement.getValue();
		}
	}	
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Fob _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getFobId())) 
        {
            oCache.remove("Fob" + _oData.getFobId());
        }
        oCache.put(new Element ("Fob" + _oData.getFobId(), _oData));
        oCache.remove("AllFob");
    }
	
	public void refreshCache (Courier _oData) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);         
	    if ( _oData != null && StringUtil.isNotEmpty(_oData.getCourierId())) 
	    {
	        oCache.remove("Courier" + _oData.getCourierId());
	    }
	    oCache.put(new Element ("Courier" + _oData.getCourierId(), _oData));
	    oCache.remove("AllCourier");
	}	
	
	//for delete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("Fob" + _sID);     
        oCache.remove("Courier" + _sID);             
        oCache.remove("AllFob");
        oCache.remove("AllCourier");
    } 
}
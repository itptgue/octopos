package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.CustomerManager;
import com.ssti.enterprise.pos.om.AccountReceivable;
import com.ssti.enterprise.pos.om.CustomerBalance;
import com.ssti.enterprise.pos.om.CustomerBalancePeer;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CustomerBalanceTool.java,v 1.12 2008/10/22 05:49:42 albert Exp $ <br>
 *
 * <pre>
 * $Log: CustomerBalanceTool.java,v $
 * 
 * 2015-12-18
 * - add method getBalances with parameter List of id
 * - add getBalanceAsOf with passing List so prevent query foreach customer
 * </pre><br>
 */
public class CustomerBalanceTool extends BaseTool 
{    
	public static CustomerBalance getCustomerBalance(String _sID)
		throws Exception
	{
		return getCustomerBalance(_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static CustomerBalance getCustomerBalance(String _sID, Connection _oConn)
		throws Exception
	{
		if (_oConn != null) //if conn is not null then do not get from cache
		{
			Criteria oCrit = new Criteria();
			oCrit.add(CustomerBalancePeer.CUSTOMER_ID, _sID);
			List vData = CustomerBalancePeer.doSelect(oCrit, _oConn);
			if(vData.size() > 0)
			{
				CustomerBalance oBalance = (CustomerBalance) vData.get(0);
				return oBalance;
			}
			return null;
		}
		else
		{
			return CustomerManager.getInstance().getCustomerBalance(_sID, null);
		}
	}	

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getBalances(List _vID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addIn(CustomerBalancePeer.CUSTOMER_ID,  _vID);
		oCrit.addAscendingOrderByColumn(CustomerBalancePeer.CUSTOMER_NAME);
		List vData = CustomerBalancePeer.doSelect(oCrit, _oConn);
		return vData;
	}	
	
	/**
	 * get CustomerBalance for update
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return CustomerBalance
	 * @throws Exception
	 */
	private static CustomerBalance getForUpdate(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerBalancePeer.CUSTOMER_ID, _sID);
		oCrit.setForUpdate(true);
		List vData = CustomerBalancePeer.doSelect(oCrit, _oConn);
		if(vData.size() > 0)
		{
			CustomerBalance oBalance = (CustomerBalance) vData.get(0);
			return oBalance;
		}
		return null;
	}	
	
	/**
	 * get balance if null then return 0
	 * 
	 * @param _sCustID
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal getBalance(String _sCustID)
		throws Exception
	{
		CustomerBalance oCustBalance = getCustomerBalance (_sCustID);
		if (oCustBalance != null) 
		{
			return oCustBalance.getArBalance();
		}
		return bd_ZERO;
	}
	
	/**
	 * get Customer Balance whose AR Balance != 0
	 * 
	 * @return List of Owing Customer
	 * @throws Exception
	 */
	public static List getOwingCustomer()
		throws Exception
	{
		return getOwingCustomer("");
	}

	/**
	 * get Customer Balance whose AR Balance != 0
	 * 
	 * @param Customer Type ID
	 * @return List of Owing Customer
	 * @throws Exception
	 */
	public static List getOwingCustomer(String _sTypeID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerBalancePeer.AR_BALANCE, 0, Criteria.NOT_EQUAL);
		if (StringUtil.isNotEmpty(_sTypeID))
		{
			oCrit.addJoin(CustomerBalancePeer.CUSTOMER_ID, CustomerPeer.CUSTOMER_ID);
			oCrit.add(CustomerPeer.CUSTOMER_TYPE_ID, _sTypeID);
		}
		oCrit.addAscendingOrderByColumn(CustomerBalancePeer.CUSTOMER_NAME);
		return CustomerBalancePeer.doSelect(oCrit);
	}
	
	/**
	 * update Customer balance
	 * 
	 * @param _oAP
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateBalance (AccountReceivable _oOldAR, AccountReceivable _oAR, Connection _oConn)
    	throws Exception
    {
		if (_oOldAR != null) rollbackBalance (_oOldAR, _oConn);
		updateBalance (_oAR, _oConn);
	}		

	/**
	 * update Customer balance
	 * 
	 * @param _oAR
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateBalance (AccountReceivable _oAR, Connection _oConn)
    	throws Exception
    {		
		double dAmount = 0;
		double dAmountBase = 0;
		
		int iType = _oAR.getTransactionType();
		
		if (iType == i_AR_TRANS_OPENING_BALANCE || 
		    iType == i_AR_TRANS_SALES )
        {
			dAmount = _oAR.getAmount().doubleValue();
            dAmountBase = _oAR.getAmountBase().doubleValue();
        }
        else 
        {
        	dAmount = _oAR.getAmount().doubleValue() * -1;
            dAmountBase = _oAR.getAmountBase().doubleValue() * -1;        
        }
		setBalance (_oAR, dAmount, dAmountBase, _oConn);
	}	
	
	/**
	 * rollback Customer balance
	 * 
	 * @param _oAR
	 * @param _oConn
	 * @throws Exception
	 */
	public static void rollbackBalance (AccountReceivable _oAR, Connection _oConn)
    	throws Exception
    {		
		double dAmount = 0;
		double dAmountBase = 0;
		
		int iType = _oAR.getTransactionType();
		
		if (iType == i_AR_TRANS_OPENING_BALANCE || 
		    iType == i_AR_TRANS_SALES)
        {
			dAmount = _oAR.getAmount().doubleValue() * -1;
            dAmountBase = _oAR.getAmountBase().doubleValue() * -1;
        }
        else 
        {
        	dAmount = _oAR.getAmount().doubleValue();
            dAmountBase = _oAR.getAmountBase().doubleValue();        
        }
		setBalance (_oAR, dAmount, dAmountBase, _oConn);
	}	
	
	private static void setBalance (AccountReceivable _oAR, 
									double _dAmount, 
									double _dAmountBase, 
									Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		
		double dPrimeBalance = 0;
		double dBalance = 0;
		
		CustomerBalance oBalance = getForUpdate(_oAR.getCustomerId(), _oConn);
		if(oBalance != null)
		{
			dPrimeBalance = oBalance.getPrimeBalance().doubleValue() + _dAmount;
			dBalance = oBalance.getArBalance().doubleValue() + _dAmountBase;
			oBalance.setPrimeBalance(new BigDecimal (dPrimeBalance));	
			oBalance.setArBalance(new BigDecimal (dBalance));
        	oBalance.save(_oConn);
		}
		else
		{
			oBalance = new CustomerBalance();
			oBalance.setCustomerBalanceId (IDGenerator.generateSysID());
			oBalance.setCustomerId		  (_oAR.getCustomerId());
	    	oBalance.setCustomerName	  (_oAR.getCustomerName());
	    	oBalance.setPrimeBalance	  (new BigDecimal (_dAmount));	
	    	oBalance.setArBalance	      (new BigDecimal (_dAmountBase));
	    	oBalance.save(_oConn);
	    }
		CustomerManager.getInstance().refreshCache(oBalance);
	}	
	
	/**
	 * get balance as of
	 * 
	 * @param _sCustomerID
	 * @param _sCurrencyID
	 * @param _dAsOf
	 * @return
	 * @throws Exception
	 */
	public static double[] getBalanceAsOf(String _sCustomerID, Date _dAsOf)
		throws Exception
	{
	    List vData = AccountReceivableTool.getTransactionHistory (_dAsOf, null, _sCustomerID, -1);
	    double[] dBalance = new double[2];
	    CustomerBalance oBalance = getCustomerBalance (_sCustomerID);
	    if (oBalance != null)
	    {
	        dBalance[i_BASE]  = oBalance.getArBalance().doubleValue();
	        dBalance[i_PRIME] = oBalance.getPrimeBalance().doubleValue();
	    
	    }
        double[] dDebit  = AccountReceivableTool.filterTotalAmountByType (vData, 1);
	    double[] dCredit = AccountReceivableTool.filterTotalAmountByType (vData, 2);
	    
	    dBalance[i_BASE]  = dBalance[i_BASE]  - dDebit[i_BASE]  + dCredit[i_BASE];
	    dBalance[i_PRIME] = dBalance[i_PRIME] - dDebit[i_PRIME] + dCredit[i_PRIME];
	    
		return dBalance;
	}	

	/**
	 * get balance as of
	 * 
	 * @param _sCustomerID
	 * @param _sCurrencyID
	 * 
	 * @return
	 * @throws Exception
	 */
	public static double[] getBalanceAsOf(List _vData, String _sCustomerID)
		throws Exception
	{
		double[] dBalance = new double[2];
		if(_vData != null)
		{
		    List vData = BeanUtil.filterListByFieldValue(_vData, "customerId", _sCustomerID);
		    CustomerBalance oBalance = getCustomerBalance (_sCustomerID);
		    if (oBalance != null)
		    {
		        dBalance[i_BASE]  = oBalance.getArBalance().doubleValue();
		        dBalance[i_PRIME] = oBalance.getPrimeBalance().doubleValue();
		    
		    }
	        double[] dDebit  = AccountReceivableTool.filterTotalAmountByType (vData, 1);
		    double[] dCredit = AccountReceivableTool.filterTotalAmountByType (vData, 2);
		    
		    dBalance[i_BASE]  = dBalance[i_BASE]  - dDebit[i_BASE]  + dCredit[i_BASE];
		    dBalance[i_PRIME] = dBalance[i_PRIME] - dDebit[i_PRIME] + dCredit[i_PRIME];
		}
		return dBalance;
	}	
}


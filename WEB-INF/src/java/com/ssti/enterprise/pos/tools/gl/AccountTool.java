package com.ssti.enterprise.pos.tools.gl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.AccountManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountPeer;
import com.ssti.enterprise.pos.om.AccountTypePeer;
import com.ssti.enterprise.pos.om.BankPeer;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.GlConfig;
import com.ssti.enterprise.pos.om.GlTransactionPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.journal.BaseJournalTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AccountTool.java,v 1.20 2009/05/04 02:04:20 albert Exp $ <br>
 *
 * <pre>
 * $Log: AccountTool.java,v $
 * 
 *  2016-11-03
 *  - change getDirectSalesAccount return default direct sales account for any other payment type
 * </pre><br>
 */
public class AccountTool extends BaseTool implements GlAttributes
{
	private static Log log = LogFactory.getLog(AccountTool.class);
    	
	public static List getAllAccount()
    	throws Exception
    {
    	return AccountManager.getInstance().getAllAccount();
	}
	
	public static List getAllChildAccount()
		throws Exception
	{
		return AccountManager.getInstance().getAllChildAccount();
	}	
	
	public static Account getAccountByID(String _sID)
    	throws Exception
    {
		return getAccountByID(_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return Account
	 * @throws Exception
	 */
	public static Account getAccountByID(String _sID, Connection _oConn)
		throws Exception
	{
		return AccountManager.getInstance().getAccountByID(_sID, _oConn);
	}
	
	public static Account getAccountByCode(String _sCode)
    	throws Exception
    {
		return AccountManager.getInstance().getAccountByCode(_sCode, null);
	}

	public static List getByParentID(String _sParentID)
    	throws Exception
    {
		return AccountManager.getInstance().getByParentID(_sParentID, null);		
	}
	
	public static String getAccountNameByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Account oAcc = getAccountByID(_sID, _oConn);
	    if (oAcc != null)
	    {
	        return oAcc.getAccountName();
	    }
	    return "";
	}

	public static Account getAccountByCode(String _sCode, boolean _bUseCache)
    	throws Exception
    {
        if (_bUseCache)
        {
            return (getAccountByCode(_sCode));
        }
        Criteria oCrit = new Criteria();
        oCrit.add (AccountPeer.ACCOUNT_CODE, _sCode);
        List vData = AccountPeer.doSelect(oCrit);
        if (vData.size() > 0)
        {
            return ((Account)vData.get(0));
        }
        return null;
	}

	public static List getAccountByType(int _iType, boolean _bIncludeParent, boolean _bUseCache)
    	throws Exception
    {
        if (_bUseCache)
        {
            return (AccountManager.getInstance().getAccountByType(_iType, _bIncludeParent, null));
        }
        Criteria oCrit = new Criteria();
        oCrit.add (AccountPeer.ACCOUNT_TYPE, _iType);
        if (!_bIncludeParent) oCrit.add (AccountPeer.HAS_CHILD, false);
        oCrit.addAscendingOrderByColumn (AccountPeer.ACCOUNT_CODE);
        return AccountPeer.doSelect(oCrit);
	}	

	public static List getMultiCurrencyAcc(String _sCurrencyID)
		throws Exception
	{
		int[] aADJType = {i_CASH_BANK, i_ACCOUNT_RECEIVABLE, i_OTHER_ASSET, 
						  i_ACCOUNT_PAYABLE, i_OTHER_LIABILITY, i_LONG_TERM_LIABILITY};
	    
		Criteria oCrit = new Criteria();
	    oCrit.addIn (AccountPeer.ACCOUNT_TYPE, aADJType);
	    oCrit.add (AccountPeer.HAS_CHILD, false);
	    oCrit.add (AccountPeer.CURRENCY_ID, _sCurrencyID);
	    oCrit.addAscendingOrderByColumn (AccountPeer.ACCOUNT_TYPE);
	    oCrit.addAscendingOrderByColumn (AccountPeer.ACCOUNT_CODE);
	    
	    log.debug("getMultiCurrencyAcc " + oCrit);
	    return AccountPeer.doSelect(oCrit);
	}	
	
	public static List getAccountByGLType(int _iGLType, boolean _bIncludeParent, boolean _bUseCache)
    	throws Exception
    {
        if (_bUseCache)
        {
            return (AccountManager.getInstance().getAccountByGLType(_iGLType, _bIncludeParent, null));
        }
        Criteria oCrit = new Criteria();
        if (_iGLType > 0)
        {
            oCrit.addJoin (AccountPeer.ACCOUNT_TYPE, AccountTypePeer.ACCOUNT_TYPE);
            oCrit.add (AccountTypePeer.GL_TYPE, _iGLType);
        }
        if (!_bIncludeParent) oCrit.add (AccountPeer.HAS_CHILD, false);
        oCrit.addAscendingOrderByColumn (AccountPeer.ACCOUNT_CODE);
        return AccountPeer.doSelect(oCrit);
	}	

	public static List findChildAccount(String _sCode, String _sName, int _iGLType, int _iAccType)
		throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add(AccountPeer.HAS_CHILD, false);
	    if(StringUtil.isNotEmpty(_sCode)) oCrit.add(AccountPeer.ACCOUNT_CODE, (Object)_sCode, Criteria.ILIKE);
	    if(StringUtil.isNotEmpty(_sName)) oCrit.add(AccountPeer.ACCOUNT_NAME, (Object)_sName, Criteria.ILIKE);
        if (_iGLType > 0)
        {
            oCrit.addJoin (AccountPeer.ACCOUNT_TYPE, AccountTypePeer.ACCOUNT_TYPE);
            oCrit.add (AccountTypePeer.GL_TYPE, _iGLType);
        }
        if (_iAccType > 0)
        {
        	oCrit.add (AccountPeer.ACCOUNT_TYPE, _iAccType);
        }
	    oCrit.addAscendingOrderByColumn (AccountPeer.ACCOUNT_CODE);
	    return AccountPeer.doSelect(oCrit);
	}
	
	public static String getDescriptionByID(String _sID)
    	throws Exception
    {
		Account oAccount = getAccountByID (_sID);
		if (oAccount != null) return oAccount.getDescription();
		return "";
	}

	public static boolean hasChild (String _sID)
    	throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add (AccountPeer.PARENT_ID, _sID);
        List vData = AccountPeer.doSelect(oCrit);
        if (vData.size() > 0) return true;
        return false;
	}
	
    public static List getChildIDList (String _sID)
    	throws Exception
    {
        return getChildIDList(new ArrayList(), _sID);
    }

	public static List getChildIDList (List _vID, String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(AccountPeer.PARENT_ID, _sID);
		List vData = AccountPeer.doSelect(oCrit);
		for (int i=0; i < vData.size(); i++)
		{
			Account oAccount = (Account)vData.get(i);
			getChildIDList (_vID, oAccount.getAccountId());
			_vID.add( oAccount.getAccountId() );
		}
		return _vID;
	}  	

	public static String getCodeByID(String _sID)
    	throws Exception
    {
		Account oAccount = getAccountByID(_sID);
		if (oAccount != null) 
		{
			return oAccount.getAccountCode();
		}
		return "";
	}

	public static String getIDByCode(String _sCode)
		throws Exception
	{
		return getIDByCode(_sCode, true);
	}
	
	public static String getIDByCode(String _sCode, boolean _bChildOnly)
    	throws Exception
    {
		Account oAccount = getAccountByCode(_sCode);
		if (oAccount != null && 
			((_bChildOnly && !oAccount.getHasChild()) || !_bChildOnly) )  
		{
			return oAccount.getAccountId();
		}
		return "";
	}
	
	public static int setChildLevel(String _sParentID)
		throws Exception
	{
		int iLevel = 1;
		if (StringUtil.isNotEmpty(_sParentID))
		{
			Account oParent = getAccountByID(_sParentID);
			iLevel = oParent.getAccountLevel() + 1;
		}
		return iLevel;
	}
	
	/**
	 * delete account, only delete if
	 * account is not used in any transaction
	 * account is not parent account
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void deleteAccountByID(String _sID)
    	throws Exception
    {	
		Account oAccount = getAccountByID (_sID);
		//validate account for deletion check if exists in any transaction
		
		if (oAccount != null)
		{
			if (!GlTransactionTool.isExistInTransaction(_sID, -1, false))
			{
				//validate whether this account has child
				Criteria oCrit = new Criteria();
				oCrit.add(AccountPeer.PARENT_ID, _sID);
				List vChild = AccountPeer.doSelect(oCrit);
				if (vChild.size() > 0) throw new Exception ("Account Has Children. Remove Child Account First");
				
				//clear gl transaction
				GlTransactionTool.deleteDataByAccountID(_sID);
				//clear general ledger
				GeneralLedgerTool.deleteDataByAccountID(_sID);
				//clear account_balance
				AccountBalanceTool.deleteDataByAccountID(_sID);
				
				oCrit = new Criteria();
				oCrit.add(AccountPeer.ACCOUNT_ID, _sID);
				AccountPeer.doDelete(oCrit);
				
				//update parent has child status
				String sParentID = oAccount.getParentId();
				if (StringUtil.isNotEmpty(sParentID))
				{
					if (!hasChild(sParentID))
					{
						Account oParent = getAccountByID(sParentID);
						if (oParent != null)
						{
							oParent.setHasChild(false);
							oParent.save();
							AccountManager.getInstance().refreshCache(oParent);
						}
					}
				}
				
				//refresh cache
				AccountManager.getInstance().refreshCache(oAccount);
			}
			else 
			{
				throw new Exception ("Account May Not Be Deleted, It's Already Used In Transaction(s)");
			}
		}
    }
	
	///////////////////////////////////////////////
	//generate javascript tree
	///////////////////////////////////////////////
	private static List getAccountAtLevel(List _aData, int _iLevel)
	{
		List vCategories = new ArrayList();
		for (int i = 0; i < _aData.size(); i++)
		{
			Account oAccount = (Account) _aData.get(i);
			if (oAccount.getAccountLevel() == _iLevel) vCategories.add(oAccount);
		}
		return vCategories;
	}

	private static List getChildren(List _aData, String _sParentID)
	{
		List vChildren = new ArrayList();
		for (int i = 0; i < _aData.size(); i++)
		{
			Account oAccount = (Account) _aData.get(i);
			if (oAccount.getParentId().equals(_sParentID)) vChildren.add(oAccount);
		}
		return vChildren;
	}

	private static String createVarStr(Account _oAccount, String _sParentNode, String _sNode)
	{
		StringBuilder oSpace = new StringBuilder("");
		if (log.isDebugEnabled()) for (int i = 1; i < _oAccount.getAccountLevel(); i++) oSpace.append("    ");
		
		StringBuilder oStr = new StringBuilder();
		for (int i = 1; i < _oAccount.getAccountLevel(); i++) oStr.append("\t");
		oStr.append (oSpace);
		oStr.append ("var node");
		oStr.append (_sNode);
		oStr.append (" = new TreeNode (");
		oStr.append (_sNode);
		oStr.append (", \"");
		oStr.append (_oAccount.getAccountCode());
		oStr.append (" ");		
		oStr.append (_oAccount.getAccountName());
		oStr.append ("\", new Array(closedGif,openGif), '");
		oStr.append (_oAccount.getAccountId());
		oStr.append ("');\n");
		
		if (StringUtil.isNotEmpty(_sParentNode))
		{
			oStr.append (oSpace);
			oStr.append ("node");
			oStr.append (_sParentNode);
			oStr.append (".addChild(node");
			oStr.append (_sNode);
			oStr.append (");\n");
		}
		else 
		{
			oStr.append ("rootNode.addChild(node");
			oStr.append (_sNode);
			oStr.append (");\n");
		}
		return oStr.toString();
	}


	private static void buildTree(List _aAllData, List _aLevel, String _sParentNode, int _iLevel, StringBuilder _oJS)
	{
		for (int i = 0; i < _aLevel.size(); i++)
		{
			int iDigit = 3; if (_iLevel >= 2) iDigit = 2;
			String sNode = _sParentNode + Integer.valueOf(_iLevel).toString() + 
				StringUtil.formatNumberString(Integer.valueOf(i).toString(), iDigit);
			
			Account oAccount = (Account) _aLevel.get(i);
			_oJS.append (createVarStr (oAccount, _sParentNode, sNode));
			List aChildren = getChildren (_aAllData, oAccount.getAccountId());
			if (aChildren.size() > 0)
			{
				int iChildLevel = _iLevel + 1;
				buildTree(_aAllData, aChildren, sNode, iChildLevel, _oJS);
			}
		}
	}

	public static String createJSTree(int _iGLType)
		throws Exception
	{
		List aData = getAccountByGLType(_iGLType, true, false);
		StringBuilder oJS = new StringBuilder();
		oJS.append("rootNode = new TreeNode(-1,'Account', userIcon,'-1');\n\n");
		List aAccountLevel1 = getAccountAtLevel(aData, 1);
		buildTree(aData, aAccountLevel1, "", 1, oJS);
		return oJS.toString();
	}

	public static String getAccountTree(int _iGLType)
		throws Exception
	{
		return AccountManager.getInstance().getAccountTree(_iGLType);
	}
	//end JSTree	
    
    //////////////////////////////////////////////////
	//generate SelectBox TREE
    //////////////////////////////////////////////////
    
	private static String createSBOptionStr(Account _oAccount, String _sSelectedID)
	{
		StringBuilder oStr = new StringBuilder();
		for (int i = 1; i < _oAccount.getAccountLevel(); i++) oStr.append("\t");
		oStr.append ("<option value=\"");
		oStr.append (_oAccount.getAccountId());
		oStr.append ("\"");
		if ( _sSelectedID != null && _sSelectedID.equals(_oAccount.getAccountId()) )
		{
			oStr.append (" SELECTED ");
		}
		oStr.append (">");
		for (int i = 1; i < _oAccount.getAccountLevel(); i++) 
		{
			oStr.append("&nbsp;&nbsp;");
		}
		oStr.append (_oAccount.getAccountCode());
		oStr.append (" [");
		oStr.append (_oAccount.getDescription());
		oStr.append ("]</option>");
		return oStr.toString();
	}
	
	private static void buildSBTree(List _aAllData,  List _aLevel, StringBuilder _oSB, String _sSelectedID)
	{
		for (int i = 0; i < _aLevel.size(); i++)
		{
			Account oAccount = (Account) _aLevel.get(i);
			_oSB.append (createSBOptionStr (oAccount, _sSelectedID));
			List aChildren = getChildren (_aAllData, oAccount.getAccountId());
			if (aChildren.size() > 0)
			{
				buildSBTree(_aAllData, aChildren, _oSB, _sSelectedID);
			}
		}
	}
	
	public static String createSBTree(String _sName, String _sSelectedID)
		throws Exception
	{
		List aData = getAllAccount();
		StringBuilder oSB = new StringBuilder();
		oSB.append ("<select name=\"");
		oSB.append (_sName);
		oSB.append ("\"><option value=\"\"> -- ACCOUNT --</option>");
		List aAccountLevel1 = getAccountAtLevel(aData, 1);
		buildSBTree(aData, aAccountLevel1 ,oSB, _sSelectedID);
		oSB.append ("</select>");
		return oSB.toString();
	}	

	public static String getAccountSBTree(String _sName, String _sSelectedID)
		throws Exception
	{
	    return AccountManager.getInstance().getAccountSBTree(_sName, _sSelectedID);
	}	

	//end SB TREE

    //AccountTree	
	public static List getAccountTreeList(List _vData, String _sID)
		throws Exception
	{
		return AccountManager.getInstance().getAccountTreeList(_vData, _sID);
	}
    
	public static List getAccountTreeList(List _vData, String _sID, int _iGLType)
		throws Exception
	{
	    if (_vData == null || _vData.size() < 1)
	    {
	        _vData = new ArrayList();
	        if (StringUtil.isNotEmpty(_sID)) 
	        {
	            _vData.add(getAccountByID(_sID));
	        }
	    }
	    Criteria oCrit = new Criteria();
        oCrit.add(AccountPeer.PARENT_ID, _sID);
        oCrit.addAscendingOrderByColumn(AccountPeer.ACCOUNT_CODE);
        
        if (_iGLType >= 1 && _iGLType <= 8)
        {
        	oCrit.addJoin(AccountPeer.ACCOUNT_TYPE, AccountTypePeer.ACCOUNT_TYPE);
        	oCrit.add(AccountTypePeer.GL_TYPE, _iGLType);
        }
        else if (_iGLType == 9) //BS ONLY
        {
        	oCrit.add(AccountPeer.ACCOUNT_TYPE, 10, Criteria.LESS_EQUAL );
        }
        else if (_iGLType == 10) //PL ONLY
        {
        	oCrit.add(AccountPeer.ACCOUNT_TYPE, 11, Criteria.GREATER_EQUAL );        
        }
        
		List vData = AccountPeer.doSelect(oCrit);
		for (int i=0; i < vData.size(); i++)
		{
			Account oAccount = (Account)vData.get(i);
			_vData.add( oAccount );
            if (hasChild(oAccount.getAccountId())) 
            {
			    getAccountTreeList (_vData, oAccount.getAccountId(), _iGLType);
		    }
		}
		return _vData;
	}	

	public static List getPLAccountTreeList(List _vData, String _sID)
		throws Exception
	{
	    if (_vData == null || _vData.size() < 1)
	    {
	        _vData = new ArrayList();
	        if (StringUtil.isNotEmpty(_sID)) 
	        {
	            _vData.add(getAccountByID(_sID));
	        }
	    }
		Criteria oCrit = new Criteria();
	    oCrit.add(AccountPeer.PARENT_ID, _sID);
	    oCrit.add(AccountPeer.ACCOUNT_TYPE, 11, Criteria.GREATER_EQUAL);
		List vData = AccountPeer.doSelect(oCrit);
		for (int i=0; i < vData.size(); i++)
		{
			Account oAccount = (Account)vData.get(i);
			_vData.add( oAccount );
	        if (hasChild(oAccount.getAccountId())) 
	        {
			    getPLAccountTreeList (_vData, oAccount.getAccountId());
		    }
		}
		return _vData;
	}	

    public static List getTreeListByAccType(List _vData, String _sID, int _iAccType)
        throws Exception
    {
        if (_vData == null || _vData.size() < 1)
        {
            _vData = new ArrayList();
            if (StringUtil.isNotEmpty(_sID)) 
            {
                _vData.add(getAccountByID(_sID));
            }
        }
        Criteria oCrit = new Criteria();
        oCrit.add(AccountPeer.PARENT_ID, _sID);
        oCrit.addAscendingOrderByColumn(AccountPeer.ACCOUNT_CODE);

        if(_iAccType > 0)
        {
            oCrit.add(AccountPeer.ACCOUNT_TYPE, _iAccType);
        }        
        List vData = AccountPeer.doSelect(oCrit);
        for (int i=0; i < vData.size(); i++)
        {
            Account oAccount = (Account)vData.get(i);
            _vData.add( oAccount );
            if (hasChild(oAccount.getAccountId())) 
            {
                getTreeListByAccType (_vData, oAccount.getAccountId(), _iAccType);
            }
        }
        return _vData;
    }   
        
	public static List getAccountIDList(List _vData)
		throws Exception
	{
	    List vID = new ArrayList(_vData.size());
	    for (int i = 0; i < _vData.size(); i++)
	    {
	        Account oAccount = (Account) _vData.get(i);
	        if (oAccount != null)
	        {
                vID.add (oAccount.getAccountId());
	        }
	    }
	    return vID;
	}

	/**
	 * get opening balance equity account, used in opening balance gl transaction	 
     * this method will throw exception to action class with corresponding message
     * about things that causing error
     * 
     * @return OB Account
     */	
	public static Account getOpeningBalanceEquity (Connection _oConn)
	    throws Exception
	{
        GlConfig oConfig = GLConfigTool.getGLConfig(_oConn);
    	if (oConfig != null)
    	{
    	    String sEquityID = oConfig.getOpeningBalance();
    	    if (StringUtil.isNotEmpty(sEquityID))
    	    {
    	        Account oEquity = AccountTool.getAccountByID (sEquityID, _oConn);
    	        if (oEquity != null) 
    	        {
                    return oEquity;
                }
                else 
                {
                    throw new Exception ("Opening Balance Equity Account Not Found, Please Check Chart Of Account");
                }
            }
            else 
            {
                throw new Exception ("Opening Balance Equity Account Not Configured Properly");
            }
        }
        else 
        {
            throw new Exception ("General Ledger Account Not Configured Properly");
        }
    }

	/**
	 * get retained earning account, used in year end process
     * this method will throw exception to action class with corresponding message
     * about things that causing error
     * 
     * @return RE Account
     */	
	public static Account getRetainedEarning (Connection _oConn)
	    throws Exception
	{
        GlConfig oConfig = GLConfigTool.getGLConfig(_oConn);
    	if (oConfig != null)
    	{
    	    String sREID = oConfig.getRetainedEarning();
    	    if (StringUtil.isNotEmpty(sREID))
    	    {
    	        Account oEarning = AccountTool.getAccountByID (sREID, _oConn);
    	        if (oEarning != null) 
    	        {
                    return oEarning;
                }
                else 
                {
                    throw new Exception ("Retained Earning Account Not Found, Please Check Chart Of Account");
                }
            }
            else 
            {
                throw new Exception ("Retained Earning Account Not Configured Properly");
            }
        }
        else 
        {
            throw new Exception ("General Ledger Account Not Configured Properly");
        }
    }

	/**
	 * get opening balance equity account, used in opening balance gl transaction	 
     * this method will throw exception to action class with corresponding message
     * about things that causing error
     * 
     * @param _sCurrencyID
     * @return AP Account
     */	
	public static Account getAccountPayable (String _sCurrencyID, String _sVendorID, Connection _oConn)
	    throws Exception
	{
		String sAP = "";
		//get AR Account from customer first
	    Vendor oVendor = VendorTool.getVendorByID(_sVendorID, _oConn);
		if (oVendor != null)
		{
			sAP = oVendor.getApAccount();
			if (StringUtil.isNotEmpty(sAP))
		    {
	            Account oAP = AccountTool.getAccountByID (sAP, _oConn);
	            if (oAP != null) 
	            {
	                return oAP;
	            }
	            else 
	            {
	                String sMsg = "Account Payable Account for Vendor " + oVendor.getVendorName() + 
	                              " Not Found, Please Check Vendor Data";
	                throw new Exception (sMsg);
	            }
		    }			
		}	    
		
		//get AR Account from currency if not exist in customer data		
	    Currency oCurrency = CurrencyTool.getCurrencyByID(_sCurrencyID, _oConn);
        if (oCurrency != null)
        {
        	sAP = oCurrency.getApAccount();
        	if (StringUtil.isNotEmpty(sAP))
        	{
        		Account oAP = AccountTool.getAccountByID (sAP, _oConn);
        		if (oAP != null) 
        		{
        			return oAP;
        		}
        		else 
        		{
        			String sMsg = "Account Payable Account for Currency " + oCurrency.getDescription() + 
        			" Not Found, Please Check Chart Of Account";
        			throw new Exception (sMsg);
        		}
        	}
        	else 
        	{
        		String sMsg = "Account Payable Account for Currency " + oCurrency.getDescription() + 
        		" Not Configured Properly";
        		throw new Exception (sMsg);
        	}
        }
        else
        {
            throw new Exception ("Invalid Currency ");
        }
    }

	/**
	 * get opening balance equity account, used in opening balance gl transaction	 
     * this method will throw exception to action class with corresponding message
     * about things that causing error
     * 
     * @param _sCurrencyID
     * @return AR Account
     */	
	public static Account getAccountReceivable (String _sCurrencyID, String _sCustomerID, Connection _oConn)
	    throws Exception
	{
		String sAR = "";
		//get AR Account from customer first
	    Customer oCustomer = CustomerTool.getCustomerByID(_sCustomerID, _oConn);
		if (oCustomer != null)
		{
			sAR = oCustomer.getArAccount();
			if (StringUtil.isNotEmpty(sAR))
		    {
	            Account oAR = AccountTool.getAccountByID (sAR, _oConn);
	            if (oAR != null) 
	            {
	                return oAR;
	            }
	            else 
	            {
	                String sMsg = "Account Receivable Account for Customer " + oCustomer.getCustomerName() + 
	                              " Not Found, Please Check Customer Data";
	                throw new Exception (sMsg);
	            }
		    }			
		}	    
		
		//get AR Account from currency if not exist in customer data
		Currency oCurrency = CurrencyTool.getCurrencyByID(_sCurrencyID, _oConn);
        if (oCurrency != null)
        {
		    sAR = oCurrency.getArAccount();
	        if (StringUtil.isNotEmpty(sAR))
	        {
	            Account oAR = AccountTool.getAccountByID (sAR, _oConn);
	            if (oAR != null) 
	            {
	                return oAR;
	            }
	            else 
	            {
	                String sMsg = "Account Receivable Account for Currency " + oCurrency.getDescription() + 
	                              " Not Found, Please Check Chart Of Account";
	                throw new Exception (sMsg);
	            }
	        }
	        else 
	        {
	            String sMsg = "Account Receivable Account for Currency " + oCurrency.getDescription() + 
	                          " Not Configured Properly";
	            throw new Exception (sMsg);
	        }
        }
        else
        {
            throw new Exception ("Invalid Currency ");
        }
    }
	
	/**
	 * get currency realized gain/loss account, used in closing period
     * this method will throw exception to action class with corresponding message
     * about things that causing error
     * 
	 * @param _sCurrencyID
	 * @param bRealized
	 * @return Account
	 * @throws Exception
	 */
	public static Account getRealUnrealGainLoss (String _sCurrencyID, boolean bRealized, Connection _oConn)
	    throws Exception
	{
	    Currency oCurrency = CurrencyTool.getCurrencyByID(_sCurrencyID, _oConn);
        if (oCurrency != null)
        {
		    String sAccID = oCurrency.getRgAccount();
		    if (!bRealized) sAccID = oCurrency.getUgAccount();
		    
	        if (StringUtil.isNotEmpty(sAccID))
	        {
	            Account oACC = AccountTool.getAccountByID (sAccID, _oConn);
	            if (oACC != null) 
	            {
	                return oACC;
	            }
	            else 
	            {
	                String sMsg = "Realized/Unrealized Gain/Loss Account for Currency " + oCurrency.getDescription() + 
	                              " Not Found, Please Check Chart Of Account";
	                throw new Exception (sMsg);
	            }
	        }
	        else 
	        {
	            String sMsg = "Realized/Unrealized Gain/Loss Account for Currency " + oCurrency.getDescription() + 
	                          " Not Configured Properly";
	            throw new Exception (sMsg);
	        }
        }
        else
        {
            throw new Exception ("Invalid Currency ");
        }
    }	
	
	/**
	 * get direct sales cash account
     * 
     * @return Direct Sales Account
     */	
	public static Account getDirectSalesAccount (Connection _oConn)
	    throws Exception
	{
		return getDirectSalesAccount(null, _oConn);
    }
	
	/**
	 * get direct sales cash account
     * 
     * @return Direct Sales Account
     */	
	public static Account getDirectSalesAccount (PaymentType _oPT, Connection _oConn)
	    throws Exception
	{		
        GlConfig oConfig = GLConfigTool.getGLConfig(_oConn);
    	if (oConfig != null)
    	{
    		String sDSID = oConfig.getDirectSalesAccount();
    		
    		//Sales return will always return null PaymentType
    		if (_oPT != null && !_oPT.getIsCredit()  && StringUtil.isNotEmpty(_oPT.getDirectSalesAccount()))
    	    {
    	    	sDSID = _oPT.getDirectSalesAccount(); //direct sales config from payment type
    	    }
    		
    		if (StringUtil.isNotEmpty(sDSID))
    	    {
    	        Account oDS = AccountTool.getAccountByID(sDSID, _oConn);
    	        if (oDS != null) 
    	        {
                    return oDS;
                }
                else 
                {
                    throw new Exception ("Direct Sales Account Not Found, Please Check Chart Of Account");
                }
            }
            else 
            {
                throw new Exception ("Direct Sales Account Not Configured Properly");
            }
        }
        else 
        {
            throw new Exception ("General Ledger Account Not Configured Properly");
        }
    }
	
	/**
     * get DeliveryOrder Account set in GL Config or Item COGS Account
     * if DeliveryOrder Account not set
     * 
     * @return DO / COGS Account
     */	
	public static Account getDeliveryOrderAccount (String _sCOGSAccID, Connection _oConn)
	    throws Exception
	{
        GlConfig oConfig = GLConfigTool.getGLConfig(_oConn);
    	if (oConfig != null)
    	{
    	    String sDOID = oConfig.getDeliveryOrderAccount();
    	    if (StringUtil.isNotEmpty(sDOID))
    	    {
    	        Account oDO = AccountTool.getAccountByID (sDOID, _oConn);
    	        if (oDO != null) 
    	        {
                    return oDO;
                }
    	    }
    	    else
    	    {
    	    	Account oCOGS = getAccountByID(_sCOGSAccID, _oConn);
    	    	if (oCOGS != null)
    	    	{
    	    		return oCOGS;
    	    	}
    	    }
    	}
    	return null;
    }
	
	/**
	 * when adding or substracting balance we need to match debit credit entry
	 * if the account normal balance is credit and the transaction entry is debit
	 * then we certainly need to minus the balance amount vice versa.
     *
     * @param _sAccountID Account ID which NormalBalance to validate
     * @param _iTransDebitCredit GlTransaction's DebitCredit that show the amount column position
     * @param _dAmountBase GlTransaction's DebitCredit that show the amount column position
     * @return balance in plus / minus form
	 */
	public static double checkBalancePlusMinus (String _sAccountID, 
	                                            int _iTransDebitCredit, 
	                                            double _dAmount)
	    throws Exception
	{
        Account oAcc = getAccountByID (_sAccountID);        
        if (oAcc != null)
        {
            if (_iTransDebitCredit != oAcc.getNormalBalance())
            {
            	return (_dAmount * -1);
            }
        }
        return _dAmount;
	}
	
	/**
	 * update parent position when an account is currently having value and balance but 
	 * another account is added into the account then we set the account into a new parent
	 *
	 * @param _oParent
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateParentPosition (Account _oParent, Connection _oConn)
	    throws Exception
	{
		if (_oParent != null)
		{
		    //validate parent process

		    //if !parent haschild 
            if (!_oParent.getHasChild())
            {
		        
		        //if parent existing transaction exist 
                if (GlTransactionTool.isExistInTransaction(_oParent.getAccountId(), -1, false))
                {
                    //clone parent into other obj
                    Account oParentClone = _oParent.copy();
                    oParentClone.setAccountId(_oParent.getAccountId());
                    
                    //change ID of the original object -> THE ORIGINAL OBJECT WILL BE SAVED AS NEW OBJECT
                    _oParent.setAccountId(IDGenerator.generateSysID(false));
                    _oParent.setNew (true);
                    _oParent.setModified (true);
                    
                    //change PARENT_ID of the clone object into the original object ID
                    oParentClone.setParentId(_oParent.getAccountId());
                    
                    //modify the code, add .1
                    oParentClone.setAccountCode (oParentClone.getAccountCode() + ".1");
                    oParentClone.setAccountLevel(oParentClone.getAccountLevel() + 1);
                    oParentClone.setNew (false);
                    oParentClone.setModified (true);
                    oParentClone.setHasChild(false);
                    //SAVE The Clone 
                    if (_oConn == null) oParentClone.save(); else oParentClone.save(_oConn); 
		            AccountManager.getInstance().refreshCache(oParentClone);    
		        }
                _oParent.setHasChild(true);
                
                //SAVE Original Parent
                if (_oConn == null) _oParent.save(); else _oParent.save(_oConn);
		        AccountManager.getInstance().refreshCache(_oParent);                
            }
        }
    }
     	
	/**
	 * validate account in JV / CF whether certain account used by item
	 * 
	 * @param _sAccountID
	 * @return
	 * @throws Exception
	 */
	public static boolean isAccountUsedByInventory (String _sAccountID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemPeer.INVENTORY_ACCOUNT, _sAccountID);
		List vData = ItemPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * validate account in JV / CF whether certain account used by cash/bank
	 * 
	 * @param _sAccountID
	 * @return is account used by bank
	 * @throws Exception
	 */
	public static boolean isAccountUsedByCashBank (String _sAccountID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(BankPeer.ACCOUNT_ID, _sAccountID);
		List vData = BankPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return true;
		}
		return false;
	}	
	
 	/**
 	 * set account opening balance
 	 * 
 	 * @param _oAccount
 	 * @param _sUserName
 	 * @throws Exception
 	 */
 	public static void setOpeningBalance(Account _oAccount, boolean _bUpdate, String _sUserName)
        throws Exception
    {
 		Connection oConn = null;
    	try
    	{
    	    if (_oAccount.getOpeningBalance() != null  &&  !_oAccount.getHasChild())
    	    {
    	    	boolean bSetOB = true;
    	    	//if insert && opening balance == 0 no need to set ob
    	    	if (!_bUpdate && _oAccount.getOpeningBalance().doubleValue() == 0)
    	    	{
    	    		bSetOB = false;
    	    	}
    	    	
    	    	if (bSetOB)
    	    	{
	    	    	oConn = BaseTool.beginTrans();
	    	    	//handle old ob trans, delete old opening balance type journal if exist 
	                if (StringUtil.isNotEmpty(_oAccount.getObTransId()))
	                {
	    			    BaseJournalTool.deleteJournal(
	    			    	i_GL_TRANS_OPENING_BALANCE, _oAccount.getObTransId(), oConn);
	                }
	    	    	Account oEquity = getOpeningBalanceEquity(oConn);
	    	    	BaseTool.commit(oConn);
	    	    	
	                JournalVoucherTool.createAccountOB(_oAccount, oEquity, _sUserName);
    	    	}
    	    }
        }
        catch (Exception _oEx)
        {        	
            if (oConn != null) 
            {
            	BaseTool.rollback(oConn);
            }
        	log.error (_oEx);
            throw new NestableException (_oEx.getMessage(),_oEx);
        }	    
    }	
    
    //-------------------------------------------------------------------------
    // display
    //-------------------------------------------------------------------------
    public static String display(Account _oAcc, String _sField)
    {
        return display(_oAcc, _sField, "", true);
    }
    
    public static String display(Account _oAcc, String _sField, String _sSPC, boolean _bBold)
    {
        if (StringUtil.isEmpty(_sSPC)) _sSPC = "&nbsp;&nbsp;&nbsp;&nbsp;";
        StringBuilder oSB = new StringBuilder();
        if (_oAcc != null)
        {
            int iRepeat = _oAcc.getAccountLevel() - 1;            
            oSB.append(StringUtil.repeatString(_sSPC,iRepeat));
            if(_bBold && _oAcc.getHasChild())
            {
                oSB.append("<b>");
            }
            if (StringUtil.isEqual(_sField,"code"))
            {
                oSB.append(_oAcc.getAccountCode());                
            }
            else if (StringUtil.isEqual(_sField,"altcode"))
            {
                oSB.append(_oAcc.getAltCode());                
            }            
            else
            {
                oSB.append(_oAcc.getDescription());
            }
        }
        return oSB.toString();
    }

    //-------------------------------------------------------------------------
    // ALT Code
    //-------------------------------------------------------------------------    
    /**
     * 
     * @param _sAltCode
     * @return
     * @throws Exception
     */
    public static List getByAltCode(String _sAltCode)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(AccountPeer.ALT_CODE, _sAltCode);
        oCrit.add(AccountPeer.HAS_CHILD, false);
        return AccountPeer.doSelect(oCrit);        
    }
    
    public static boolean isAsDateValid(Date _dAsDate, String _sAccID)
    	throws Exception
    {
    	if(_dAsDate != null)
    	{
	    	Criteria oCrit = new Criteria();
	    	oCrit.add(GlTransactionPeer.ACCOUNT_ID, _sAccID);
	    	oCrit.add(GlTransactionPeer.TRANSACTION_DATE, _dAsDate, Criteria.LESS_EQUAL);
	    	oCrit.setLimit(1);
	    	List vGL = GlTransactionPeer.doSelect(oCrit);
	    	if (vGL.size() > 0)
	    	{
	    		return false;    		
	    	}
    	}
    	return true;
    }
}
package com.ssti.enterprise.pos.tools.gl;

import com.ssti.enterprise.pos.tools.AppAttributes;

public interface GlAttributes extends AppAttributes
{
    public static final int i_DEBIT  = 1;
    public static final int i_CREDIT = 2;
    
	//GL Trans type
	public static final int i_GL_TRANS_OPENING_BALANCE    = 1;
	public static final int i_GL_TRANS_RECEIPT_UNPLANNED  = 2;
	public static final int i_GL_TRANS_ISSUE_UNPLANNED    = 3;
	public static final int i_GL_TRANS_PURCHASE_RECEIPT   = 4;
	public static final int i_GL_TRANS_PURCHASE_INVOICE   = 5;
	public static final int i_GL_TRANS_PURCHASE_RETURN    = 6;
	public static final int i_GL_TRANS_ITEM_TRANSFER 	  = 7;
	public static final int i_GL_TRANS_DELIVERY_ORDER     = 8;
	public static final int i_GL_TRANS_SALES_INVOICE      = 9;
	public static final int i_GL_TRANS_SALES_RETURN 	  = 11;
	public static final int i_GL_TRANS_FIXED_ASSET 	      = 12;
	public static final int i_GL_TRANS_JOURNAL_VOUCHER    = 13;
	public static final int i_GL_TRANS_AR_PAYMENT         = 14;
	public static final int i_GL_TRANS_AP_PAYMENT         = 15;
	public static final int i_GL_TRANS_CREDIT_MEMO        = 16;
	public static final int i_GL_TRANS_DEBIT_MEMO         = 17;
	public static final int i_GL_TRANS_CASH_MANAGEMENT    = 18;
    public static final int i_GL_TRANS_JOB_COSTING        = 19;
    public static final int i_GL_TRANS_JC_ROLLOVER        = 20;
    
    //ACCOUNT TYPE
    public static final int i_CASH_BANK             = 1 ;
    public static final int i_ACCOUNT_RECEIVABLE    = 2 ;
    public static final int i_INVENTORY_ASSET       = 3 ; 
    public static final int i_OTHER_ASSET           = 4 ;
    public static final int i_FIXED_ASSET           = 5 ;
    public static final int i_FA_DEPRECIATION       = 6 ;
    public static final int i_ACCOUNT_PAYABLE       = 7 ;  
    public static final int i_OTHER_LIABILITY       = 8 ;  
    public static final int i_LONG_TERM_LIABILITY   = 9 ;      
    public static final int i_EQUITY                = 10;
    public static final int i_REVENUE               = 11;
    public static final int i_COGS                  = 12;
    public static final int i_EXPENSE               = 13;
    public static final int i_OTHER_INCOME          = 14;
    public static final int i_OTHER_EXPENSE         = 15;
    public static final int i_INCOME_TAX         	= 16;

    public static final double d_UNBALANCE_TOLERANCE = CONFIG.getDouble("gl.unbalance.tolerance");
}
















package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class CustomerView extends CustomerSecureScreen
{    
	public void doBuildTemplate(RunData data, Context context)
    {
		try
		{				
    		LargeSelectHandler.handleLargeSelectParameter (data, context, "findCustomerResult", "Customers");
		}
        catch (Exception _oEx)
        {
        	log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
}

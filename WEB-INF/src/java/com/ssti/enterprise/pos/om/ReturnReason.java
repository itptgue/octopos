
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class ReturnReason
    extends com.ssti.enterprise.pos.om.BaseReturnReason
    implements Persistent,MasterOM
{
	//trans type constants
	public static final int i_BOTH = 0;
	public static final int i_SALES = 1;
	public static final int i_PURCHASE = 2;
	
	public String getId()
	{
		return getReturnReasonId();
	}	
	public String getTypeDesc()
	{		
		if(getTransType() == i_BOTH) return LocaleTool.getString("all");
		if(getTransType() == i_SALES) return LocaleTool.getString("sales");
		if(getTransType() == i_PURCHASE) return LocaleTool.getString("purchase");
		return "";
	}	
}

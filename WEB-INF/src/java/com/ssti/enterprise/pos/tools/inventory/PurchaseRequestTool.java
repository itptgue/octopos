package com.ssti.enterprise.pos.tools.inventory;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;

import com.google.gson.Gson;
import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.model.TransactionMasterOM;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseRequest;
import com.ssti.enterprise.pos.om.PurchaseRequestDetail;
import com.ssti.enterprise.pos.om.PurchaseRequestDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseRequestPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/inventory/PurchaseRequestTool.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseRequestTool.java,v 1.20 2009/05/04 02:04:27 albert Exp $
 *
 * $Log: PurchaseRequestTool.java,v $
 * 
 * 2018-03-13
 * -add Field isPurchase & isReimburse in @PurchaseRequest 
 * -change in @LastNumberTool
 * -change method process save, if isPurchase / isReimburse, use different CTL tables
 * -change method findData add parameter _bPur & _bRmb from @PurchaseRequestAction 
 * 
 * 2017-09-30
 * -change method findDetail add kategoriId, and sortBy
 * 
 * 2017-05-29
 * -change process save only del and save detail when new / processed
 * 
 * 2017-01-03
 * -add method getUnprocessedRQ get RQ where status is confirmed and not transferred yet
 *
 * 2015-09-18
 * -add method createRQ used by createRQ from SO
 */
public class PurchaseRequestTool extends BaseTool
{
	private static Log log = LogFactory.getLog(PurchaseRequestTool.class);

	public static final String s_ITEM = new StringBuilder(PurchaseRequestPeer.PURCHASE_REQUEST_ID).append(",")
		.append(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID).append(",")
		.append(PurchaseRequestDetailPeer.ITEM_ID).toString();

	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), PurchaseRequestPeer.TRANSACTION_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), PurchaseRequestPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), PurchaseRequestPeer.REQUESTED_BY);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), PurchaseRequestPeer.APPROVED_BY);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), PurchaseRequestPeer.LOCATION_ID);             
		
		addInv(m_FIND_PEER, s_ITEM);		
	}   
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}

	//static methods
	public static PurchaseRequest getHeaderByID(String _sID)
    	throws Exception
    {
		return getHeaderByID(_sID, null);
	}

	public static PurchaseRequest getHeaderByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseRequestPeer.PURCHASE_REQUEST_ID, _sID);
		List vData = PurchaseRequestPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) {
			return (PurchaseRequest) vData.get(0);
		}
		return null;
	}
	
	public static PurchaseRequestDetail getDetailByDetailID(String _sDetID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_DETAIL_ID, _sDetID);
		List vData = PurchaseRequestDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) {
			return (PurchaseRequestDetail) vData.get(0);
		}
		return null;

	}
	
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
		return getDetailsByID(_sID, null);
	}
	
	public static List getDetailsByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, _sID);
        oCrit.addAscendingOrderByColumn(PurchaseRequestDetailPeer.INDEX_NO);
		return PurchaseRequestDetailPeer.doSelect(oCrit, _oConn);
	}
	
	public static void updateDetailQty (List _vDet, RunData data)
    	throws Exception
    {
        updateDetailQty (_vDet, data, 0);
    }
	
	//set purchase request header properties
    public static void updateDetailQty (List _vDet, RunData data, int iConfirm)
    	throws Exception
    {
		try
		{
            for (int i = 0; i < _vDet.size(); i++)
			{
			    PurchaseRequestDetail oDetail = (PurchaseRequestDetail) _vDet.get(i);
                double dQty = 0;
                //log.debug (oDetail);
                if(iConfirm == 0)
				{
                    dQty = data.getParameters().getDouble("RequestQty" + (i + 1));
				    oDetail.setRequestQty(new BigDecimal(dQty));				    	
			    }
			    else if(iConfirm == 1)
			    {
				    dQty = data.getParameters().getDouble("SentQty" + (i + 1));
				    oDetail.setSentQty(new BigDecimal(dQty));				    	
			    }
				_vDet.set (i, oDetail);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("UPDATE Detail QTY Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
    public static void setHeaderProperties (PurchaseRequest _oRequest, List _vDet, RunData data)
    	throws Exception
    {
		try
		{
			Date dToday = new Date();
            data.getParameters().setProperties(_oRequest);
			_oRequest.setVendorName(VendorTool.getVendorNameByID(_oRequest.getVendorId()));
    		if(_oRequest.getTransactionDate() == null)
    		{
    			_oRequest.setTransactionDate(dToday);
    		}
    		//test
    		_oRequest.setLastUpdateDate(dToday);
    		_oRequest.setTotalQty (new BigDecimal(countQty(_vDet)));
    		//log.debug (_oRequest);
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }

	public static double countQty (List _vDetails)
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseRequestDetail oDet = (PurchaseRequestDetail) _vDetails.get(i);
			dQty += oDet.getRequestQty().doubleValue();
		}
		return dQty;
	}

	public static String getStatusString (int _iStatus)
	{
		if (_iStatus == i_RQ_NEW) return LocaleTool.getString("new");
		if (_iStatus == i_RQ_APPROVED) return LocaleTool.getString("requested");
		if (_iStatus == i_RQ_CONFIRMED) return LocaleTool.getString("confirmed");
		if (_iStatus == i_RQ_TRANSFERED) return LocaleTool.getString("transfered");
		if (_iStatus == i_RQ_CLOSED) return LocaleTool.getString("closed");		
		return LocaleTool.getString("new");		
	}	

	/**
	 * 
	 * @param _oTR
	 * @param _vDet
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (PurchaseRequest _oTR, List _vDet, Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		boolean bNew = false;
		try
		{
			if (_oConn == null)
			{
				_oConn = beginTrans();
				bStartTrans = true;
			}
			
			if(_oTR.getStatus() != i_RQ_CLOSED) //allow close even after close period
			{
				//validate date
				validateDate(_oTR.getTransactionDate(), _oConn);
			}
			
			if(StringUtil.isEmpty(_oTR.getPurchaseRequestId())) bNew = true;
			
			processSaveRequestData(_oTR, _vDet, _oConn);
			if (bStartTrans)
			{
				commit(_oConn);
			}
		}
		catch (Exception _oEx)
		{
			_oTR.setTransactionStatus(i_RQ_NEW);
			_oTR.setTransactionNo("");
			if(bNew)
			{
				_oTR.setPurchaseRequestId("");
			}
			if (bStartTrans)
			{
				rollback (_oConn);
			}
						
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * 
	 * @param _oTR
	 * @param _vDet
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveRequestData (PurchaseRequest _oTR, List _vDet, Connection _oConn) 
		throws Exception
	{
		if(StringUtil.isEmpty(_oTR.getPurchaseRequestId()))
        {
            _oTR.setPurchaseRequestId( IDGenerator.generateSysID() );
            _oTR.setNew (true);
        }    
		
		if (StringUtil.isEmpty(_oTR.getTransactionNo())) //special case validation
		{
			validateID(getHeaderByID(_oTR.getPurchaseRequestId(), _oConn), "transactionNo");
		}
		
	    //if trans no == null then set the transaction no to empty str to prevent null pointer
	    if (_oTR.getTransactionNo() == null) _oTR.setTransactionNo("");
	    
	    if(_oTR.getTransactionStatus() == i_RQ_NEW) _oTR.setApprovedBy("");
	    
	    //after trans approved in store for the first time		
		if ( _oTR.getTransactionStatus () == i_RQ_APPROVED &&
		     _oTR.getTransactionNo().equals("")) 
		{
			String sLocCode = PreferenceTool.getLocationCode();
			if (!PreferenceTool.syncInstalled())
			{
				Location oLoc = LocationTool.getLocationByID(_oTR.getLocationId(), _oConn);
				sLocCode = oLoc.getLocationCode();
			}
            _oTR.setConfirmDate(new Date());
			//generate Trans Number no first
			if(!_oTR.getIsPurchase() && !_oTR.getIsReimburse())
			{
	            _oTR.setTransactionNo(
					LastNumberTool.get(s_RQ_FORMAT, LastNumberTool.i_PURCHASE_REQUEST, sLocCode, _oConn)
				);
			}
			else
			{
				if(_oTR.getIsPurchase()) {
					String sFmt = "P" + s_RQ_FORMAT;
					_oTR.setTransactionNo(LastNumberTool.get(sFmt, LastNumberTool.i_PROC_REQUEST, sLocCode, _oConn));
				}
				if(_oTR.getIsReimburse()) {
					String sFmt = "R" + s_RQ_FORMAT;
					_oTR.setTransactionNo(LastNumberTool.get(sFmt, LastNumberTool.i_REIMBURSE_RQ, sLocCode, _oConn));
				}				
			}
			validateNo(PurchaseRequestPeer.TRANSACTION_NO,_oTR.getTransactionNo(), PurchaseRequestPeer.class, _oConn);     
		}	
   		
        _oTR.save (_oConn);
		
        if (_oTR.getTransactionStatus () == i_RQ_NEW || 
        	_oTR.getTransactionStatus () == i_RQ_APPROVED) 
   		{
			//delete all existing detail whenever we saved the rq trans
			if (StringUtil.isNotEmpty(_oTR.getPurchaseRequestId()))
			{
	            deleteDetailsByID(_oTR.getPurchaseRequestId(), _oConn);
			}
			
        	//save details	
	        renumber(_vDet);
			for ( int i = 0; i < _vDet.size(); i++ )
			{
				PurchaseRequestDetail oTD = (PurchaseRequestDetail) _vDet.get (i);
				oTD.setModified(true);
				oTD.setNew(true);
	            oTD.setPurchaseRequestDetailId (IDGenerator.generateSysID());
	            oTD.setPurchaseRequestId(_oTR.getPurchaseRequestId());		
				oTD.save(_oConn);
			}
   		}
	}

    private static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, _sID);
		PurchaseRequestDetailPeer.doDelete (oCrit, _oConn);
	}
    
	/**
	 * 
	 * @param _sVendorID
	 * @param _sLocationID
	 * @param _sTransNo
	 * @return
	 * @throws Exception
	 */
	public static List findRequest (String _sVendorID, String _sLocationID, String _sTransNo, String _sCreateBy) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sTransNo)) 
		{
			oCrit.add(PurchaseRequestPeer.TRANSACTION_NO, 
				(Object) SqlUtil.like (PurchaseRequestPeer.TRANSACTION_NO, _sTransNo), Criteria.CUSTOM);
		}
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseRequestPeer.VENDOR_ID, _sVendorID);	
			oCrit.or(PurchaseRequestPeer.VENDOR_ID, "");	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(PurchaseRequestPeer.LOCATION_ID, _sLocationID);	
		}
		if (StringUtil.isNotEmpty(_sCreateBy)) 
		{
			oCrit.add(PurchaseRequestPeer.ORDER_BY, _sCreateBy);	
		}
		
		return PurchaseRequestPeer.doSelect(oCrit);
	}
	
	/**
	 * 
	 * @param _sTransNo
	 * @param _dStart
	 * @param _dEnd
	 * @param _sVendorID
	 * @param _iStatus
	 * @param _iLimit
	 * @param _sLocationID
	 * @return
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond,
										String _sKeywords, 
										Date _dStart, 
										Date _dEnd, 
								        String _sVendorID, 
										int _iStatus, 
										int _iLimit,
										String _sLocationID,
										String _sRequestBy,
										boolean _bExt,
										boolean _bPur,
										boolean _bRmb) 
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			PurchaseRequestPeer.TRANSACTION_DATE, _dStart, _dEnd, null );
		
		if (_bExt) //filter only if external
		{
			oCrit.add(PurchaseRequestPeer.IS_EXTERNAL, _bExt);			
		}
		oCrit.add(PurchaseRequestPeer.IS_PURCHASE, _bPur);			
		oCrit.add(PurchaseRequestPeer.IS_REIMBURSE, _bRmb);					
		
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseRequestPeer.VENDOR_ID, _sVendorID);	
		}
		if (_iStatus > 0) 
		{
			oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS, _iStatus);	
		}		
		if(StringUtil.isNotEmpty(_sLocationID))
		{
		    oCrit.add(PurchaseRequestPeer.LOCATION_ID, _sLocationID);
		}
		if(StringUtil.isNotEmpty(_sRequestBy))
		{
			List vEmp = EmployeeTool.findData(1, _sRequestBy, _sLocationID);
			if(vEmp.size() > 0)
			{
				for(int i = 0; i < vEmp.size(); i++)
				{
					Employee oEmp = (Employee) vEmp.get(i);							
					if(i == 0) { oCrit.add(PurchaseRequestPeer.REQUESTED_BY, oEmp.getUserName()); }
					else { oCrit.or(PurchaseRequestPeer.REQUESTED_BY, oEmp.getUserName()); }
				}					
			}
			else
			{
				oCrit.add(PurchaseRequestPeer.REQUESTED_BY, (Object)_sRequestBy, Criteria.ILIKE);
			}
		}
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.PurchaseRequestPeer");
	}

	public static List findDetail (int _iCond,
								   String _sKeywords, 
								   Date _dStart, 
								   Date _dEnd, 
								   String _sVenTypeID,
								   String _sVendorID, 
								   String _sKatID,
								   String _sLocID,								   
								   String _sRequestBy,								   
								   int _iStatus,
								   int _iSortBy,
								   boolean _bExt) 
		throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				PurchaseRequestPeer.TRANSACTION_DATE, _dStart, _dEnd, null );
		
		oCrit.addJoin(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, PurchaseRequestPeer.PURCHASE_REQUEST_ID);
		oCrit.addJoin(PurchaseRequestDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
		
		if (_bExt) //filter only if external
		{
			oCrit.add(PurchaseRequestPeer.IS_EXTERNAL, _bExt);			
		}
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(ItemPeer.PREFERED_VENDOR_ID, _sVendorID);	
		}
		if (StringUtil.isNotEmpty(_sKatID)) 
		{
			List vKat = KategoriTool.getChildIDList(_sKatID);
			vKat.add(_sKatID);
			oCrit.addIn(ItemPeer.KATEGORI_ID, vKat);	
		}		
		else
		{
			if (StringUtil.isNotEmpty(_sVenTypeID)) 
			{
				List vVendor = VendorTool.getVendorByType(_sVenTypeID);
				List vVID = SqlUtil.toListOfID(vVendor);
				oCrit.addIn(ItemPeer.PREFERED_VENDOR_ID, vVID);
			}
		}
		if (_iStatus > 0) 
		{
			oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS, _iStatus);	
		}		
		if(StringUtil.isNotEmpty(_sLocID))
		{
			oCrit.add(PurchaseRequestPeer.LOCATION_ID, _sLocID);
		}
		if(StringUtil.isNotEmpty(_sRequestBy))
		{
			List vEmp = EmployeeTool.findData(1, _sRequestBy, _sLocID);
			if(vEmp.size() > 0)
			{
				for(int i = 0; i < vEmp.size(); i++)
				{
					Employee oEmp = (Employee) vEmp.get(i);							
					if(i == 0) { oCrit.add(PurchaseRequestPeer.REQUESTED_BY, oEmp.getUserName()); }
					else { oCrit.or(PurchaseRequestPeer.REQUESTED_BY, oEmp.getUserName()); }
				}					
			}
			else
			{
				oCrit.add(PurchaseRequestPeer.REQUESTED_BY, (Object)_sRequestBy, Criteria.ILIKE);
			}						
		}		
		StringBuilder oCol = new StringBuilder();
		oCol.append(" purchase_request.purchase_request_id, purchase_request.transaction_no, purchase_request.location_id, ")
			.append(" purchase_request.transaction_date, purchase_request.transaction_status, purchase_request.requested_by, purchase_request.approved_by, ")
			.append(" purchase_request_detail.purchase_request_detail_id, purchase_request_detail.item_id, purchase_request_detail.unit_id,  ")
			.append(" purchase_request_detail.request_qty, purchase_request_detail.sent_qty, (request_qty - sent_qty) as left, ")				
			.append(" item.item_code, item.item_name, item.prefered_vendor_id, item.kategori_id, ")
			.append(" pod.qty_base AS po_qty,COALESCE(po.purchase_order_id,'') AS po_id, COALESCE(po.purchase_order_no,'') AS po_no ");
		
		String sSQL = BasePeer.createQueryString(oCrit);	
		int iWhere = sSQL.indexOf("WHERE");
		
		StringBuilder oSQL = new StringBuilder();
		oSQL.append("SELECT ");
		if(sSQL.contains("DISTINCT")) oSQL.append("DISTINCT");		
		oSQL.append(oCol);
		oSQL.append(" FROM purchase_request, ");
		oSQL.append(" item LEFT JOIN vendor ven ON (item.prefered_vendor_id = ven.vendor_id), ");
		oSQL.append(" purchase_request_detail LEFT JOIN purchase_order_detail pod ON (purchase_request_detail_id = pod.request_detail_id) ");
		oSQL.append("   LEFT JOIN purchase_order po ON (pod.purchase_order_id = po.purchase_order_id)  ");
		oSQL.append(sSQL.substring(iWhere));
		
		if(_iSortBy > 0 && _iSortBy <= 4)
		{
			oSQL.append(" ORDER BY ");
			if(_iSortBy == 1) oSQL.append(" item.item_code ");
			if(_iSortBy == 2) oSQL.append(" item.item_name ");
			if(_iSortBy == 3) oSQL.append(" ven.vendor_code, item.item_code ");
			if(_iSortBy == 4) oSQL.append(" ven.vendor_name, item.item_name ");		
		}
					
		System.out.println("********** RQ Query: " + oSQL);
		return SqlUtil.executeQuery(oSQL.toString());
	}

	
	//-------------------------------------------------------------------------
	//report methods
	//-------------------------------------------------------------------------
	
	public static List findData (Date _dStart, 
								 Date _dEnd, 
								 String _sLocationID, 
								 int _iStatus)
    	throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseRequestPeer.TRANSACTION_DATE, 
        	DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        oCrit.and(PurchaseRequestPeer.TRANSACTION_DATE, 
        	DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        if(StringUtil.isNotEmpty(_sLocationID))
        {
            oCrit.add(PurchaseRequestPeer.LOCATION_ID, _sLocationID);
        }
        if(_iStatus > 0)
        {
            oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS, _iStatus);
        }
		return PurchaseRequestPeer.doSelect(oCrit);
	}

	public static List findForLookup(String _sLocationID, 
									 String _sVendorID,
									 String _sCreateBy, 
									 int _iStatus,
									 boolean _bExternal)
	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseRequestPeer.IS_EXTERNAL, _bExternal);
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(PurchaseRequestPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sVendorID))
		{
			oCrit.add(PurchaseRequestPeer.VENDOR_ID, _sVendorID);
		}		
		if (_iStatus > 0)
		{
			oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS, _iStatus);
		}
		else
		{
			oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS, i_RQ_APPROVED, Criteria.GREATER_EQUAL);
            oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS, i_RQ_CONFIRMED, Criteria.LESS_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sCreateBy))
		{
			oCrit.add(PurchaseRequestPeer.REQUESTED_BY, _sCreateBy);	
		}
		return PurchaseRequestPeer.doSelect(oCrit);
	}

	public static List getUnprocessedRQ(String _sLocID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS,i_RQ_APPROVED);
		oCrit.add(PurchaseRequestPeer.TRANSFER_BY, "");
		if(StringUtil.isNotEmpty(_sLocID))
		{
			oCrit.add(PurchaseRequestPeer.LOCATION_ID, _sLocID);
		}
		System.out.println(oCrit);
		return PurchaseRequestPeer.doSelect(oCrit);
	}
	
	//-------------------------------------------------------------------------
	//report method to group data by item
	//-------------------------------------------------------------------------

	public static List getTransDetails (List _vRequest)
    	throws Exception
    {
		List vRequestID = new ArrayList (_vRequest.size());
		for (int i = 0; i < _vRequest.size(); i++)
		{
			PurchaseRequest oRequest = (PurchaseRequest) _vRequest.get(i);
			vRequestID.add (oRequest.getPurchaseRequestId());
		}
		Criteria oCrit = new Criteria();
		if(_vRequest.size()>0)
		{
		    oCrit.addIn (PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, vRequestID);
		}
		return PurchaseRequestDetailPeer.doSelect(oCrit);
	}
    
	//-------------------------------------------------------------------------
	//end report methods
	//-------------------------------------------------------------------------

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PurchaseRequestPeer.class, PurchaseRequestPeer.PURCHASE_REQUEST_ID, _sID, _bIsNext);
	}	  		
	
	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
	/**
	 * get all purchase request created by store and confirmed or sent by HO
	 * this list of request goes one way from HO 2 Store
	 * 
	 * @return list of 
	 * @throws Exception
	 */
	public static List getConfirmedAndSentRequest ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE);
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) {
	    	oCrit.add(PurchaseRequestPeer.LAST_UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);
		}
		oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS,i_RQ_CONFIRMED);
		oCrit.or(PurchaseRequestPeer.TRANSACTION_STATUS,i_RQ_TRANSFERED);
				
		//log.debug(oCrit);
		return PurchaseRequestPeer.doSelect(oCrit);
	}
	
	/**
	 * get all purchase request made in store and has not been sent to HO
	 * 
	 * @return approved request made in store for HO
	 * @throws Exception
	 */
	public static List getStoreRequest ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) {
        	oCrit.add(PurchaseRequestPeer.TRANSACTION_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);
		}
		oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS,i_RQ_APPROVED);
		return PurchaseRequestPeer.doSelect(oCrit);
	}
	
	//method to check balanced item on REQUEST and PR
	public static List getByStatusAndVendorID (String _sVendorID, int _iStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseRequestPeer.VENDOR_ID, _sVendorID);
        oCrit.add(PurchaseRequestPeer.TRANSACTION_STATUS, _iStatus);
        return PurchaseRequestPeer.doSelect(oCrit);
	}
	
	public static void changeStatusByID(String _sRQID, int _iStatus)
		throws Exception
	{
		PurchaseRequest oPurchaseRequest = new PurchaseRequest();
		oPurchaseRequest = getHeaderByID(_sRQID);
		oPurchaseRequest.setTransactionStatus(_iStatus);
		oPurchaseRequest.setLastUpdateDate(new Date());
		oPurchaseRequest.save();	
	}
	//end next prev
		
	//method to import request to item transfer
	public static void mapRQDetailToITDetail(PurchaseRequestDetail _oRQDet, ItemTransferDetail _oDet)
		throws Exception
	{
        _oDet.setItemId      (_oRQDet.getItemId());
        _oDet.setItemCode    (_oRQDet.getItemCode());
        _oDet.setUnitId		 (_oRQDet.getUnitId());
        _oDet.setUnitCode	 (_oRQDet.getUnitCode());
        _oDet.setQtyChanges	 (new BigDecimal(_oRQDet.getOutstanding()));
        _oDet.setCostPerUnit (_oRQDet.getCostPerUnit());
	}
	
	public static void mapRQDetailToIRDetail(PurchaseRequestDetail _oRQDet, IssueReceiptDetail _oDet)
		throws Exception
	{
	    _oDet.setItemId      (_oRQDet.getItemId());
	    _oDet.setItemCode    (_oRQDet.getItemCode());
	    _oDet.setUnitId		 (_oRQDet.getUnitId());
	    _oDet.setUnitCode	 (_oRQDet.getUnitCode());
	    _oDet.setQtyChanges	 (_oRQDet.getRequestQty());
	    _oDet.setCost		 (bd_ZERO); //check while saving trans
	}
	
	public static void updateRQ(TransactionMasterOM _oTR, List _vTD, String _sRQID, boolean _bCancel, Connection _oConn)
		throws Exception
	{
		if (_oTR != null && _vTD != null)
		{
			for (int i = 0; i < _vTD.size(); i++)
			{
				InventoryDetailOM oTD = (InventoryDetailOM) _vTD.get(i);
				updateSentQty(_sRQID,oTD.getItemId(),oTD.getQtyBase().doubleValue(),_bCancel,_oConn);
			}
			updateSentStatus(_sRQID,_oTR.getTransactionId(),_oTR.getCreateBy(),_bCancel,_oConn);
		}
	}	
	
	private static void updateSentQty (String _sRQID, String _sItemID, double _dQty, boolean _bCancel, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, _sRQID);
        oCrit.add(PurchaseRequestDetailPeer.ITEM_ID, _sItemID);
        List vDet = PurchaseRequestDetailPeer.doSelect(oCrit,_oConn);
        if (vDet.size() > 0)
        {
        	if(_bCancel) _dQty = _dQty * -1;
        	PurchaseRequestDetail oDet = (PurchaseRequestDetail) vDet.get(0);
        	oDet.setSentQty(new BigDecimal(oDet.getSentQty().doubleValue() + _dQty));
        	oDet.save(_oConn);
        }
	}
	
	private static void updateSentStatus (String _sRQID, String _sTRFID, String _sUserName, boolean _bCancel, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseRequestPeer.PURCHASE_REQUEST_ID, _sRQID);
	    List vTR = PurchaseRequestPeer.doSelect(oCrit,_oConn);
	    if (vTR.size() > 0)
	    {
	    	PurchaseRequest oTR = (PurchaseRequest) vTR.get(0);
	    	if (!_bCancel)
	    	{
	    		boolean bAllSent = true;
	    		List vTD = getDetailsByID(oTR.getTransactionId(), _oConn);
	    		for (int i = 0; i < vTD.size(); i++)
	    		{
	    			PurchaseRequestDetail oTD = (PurchaseRequestDetail)vTD.get(i);
	    			if (oTD.getSentQty().doubleValue() < oTD.getRequestQty().doubleValue())
	    			{
	    				bAllSent = false;
	    			}
	    		}
	    		if (bAllSent)
	    		{
	    			oTR.setTransactionStatus(i_RQ_TRANSFERED);
	    		}
		    	oTR.setTransferTransId(_sTRFID);
		    	oTR.setTransferBy(_sUserName);
		    	oTR.setTransferDate(new Date());
	    	}
	    	else
	    	{
		    	oTR.setTransactionStatus(i_RQ_APPROVED);
		    	oTR.setTransferTransId("");
		    	oTR.setTransferBy("");
		    	oTR.setTransferDate(new Date());	    		
	    	}
	    	oTR.save(_oConn);
	    }
	}
	
	public static List getDetailForPO(List _vRQID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.purchase_request_id, s.transaction_no, s.transaction_date, s.location_id, s.transaction_status, ")
			.append (" sd.purchase_Request_detail_id, sd.item_id, sd.item_code, i.item_name, ")
			.append (" sd.request_qty, sd.sent_qty, (sd.request_qty - sd.sent_qty) AS left,  ")
			.append (" sd.unit_id, v.vendor_id, v.vendor_code, v.vendor_name ")
			.append (" FROM purchase_request s  ")
			.append (" LEFT JOIN purchase_request_detail sd ON s.purchase_request_id = sd.purchase_request_id ")
			.append (" LEFT JOIN item i ON sd.item_id = i.item_id ")		
			.append (" LEFT JOIN vendor v ON i.prefered_vendor_id = v.vendor_id ")		
			.append (" WHERE ")            
			.append (" s.purchase_request_id IN ").append(SqlUtil.convertToINMode(_vRQID))
            .append (" ORDER BY v.vendor_code ");
			
		//log.debug(oSQL.toString());
		return BasePeer.executeQuery(oSQL.toString());			
	}
    
    public static double getTotalSent(String _sRQID, List _vTD)
    {
        double dSent = 0;
        if (StringUtil.isNotEmpty(_sRQID))
        {
            try 
            {
                List vTD = _vTD;
                if (_vTD == null) vTD = PurchaseRequestTool.getDetailsByID(_sRQID);
                for (int i = 0; i < vTD.size(); i++)
                {
                    PurchaseRequestDetail oRQD = (PurchaseRequestDetail)vTD.get(i);
                    dSent += oRQD.getSentQty().doubleValue();
                }
            } 
            catch (Exception e) 
            {
                e.printStackTrace();
            }
        }
        return dSent;
    }
    
    /**
     * added 2/4/2014
     * 
     * @param _oPO
     * @param _vPOD
     * @param _oConn
     * @throws Exception
     */
    public static void updateFromPOD(PurchaseOrder _oPO, List _vPOD, Connection _oConn)
    	throws Exception
    {
    	if (_oPO != null)
    	{
	    	Set vRQ = new HashSet(_vPOD.size());
	    	for (int i = 0; i < _vPOD.size(); i++)
	    	{
	    		PurchaseOrderDetail oPOD = (PurchaseOrderDetail) _vPOD.get(i);
	    		if (StringUtil.isNotEmpty(oPOD.getRequestDetailId()) && StringUtil.isNotEmpty(oPOD.getRequestId()))
	    		{
	    			vRQ.add(oPOD.getRequestId());
	    		}
	    	}
	    	if (vRQ.size() > 0)
	    	{
		    	Iterator iter = vRQ.iterator();
		    	while(iter.hasNext())
		    	{
		    		String sID = (String)iter.next();
		    		PurchaseRequest oRQ = getHeaderByID(sID, _oConn);
		    		if (oRQ != null)
		    		{
		    			oRQ.setRemark(oRQ.getRemark() + s_LINE_SEPARATOR + " PO:" + _oPO.getPurchaseOrderNo());
		    			oRQ.setLastUpdateDate(new Date());
		    			oRQ.save(_oConn);
		    		}
		    	}
	    	}
    	}
    }

    /**
     * 
     * @param _sLocID
     * @param _sUserName
     * @param _mItems
     * @return
     * @throws Exception
     */
	public static String createRQ(String _sLocID, String _sUserName, String _sRemark, Map _mItems) 
		throws Exception
	{
		Location oLoc = LocationTool.getLocationByID(_sLocID);
		StringBuilder sResult= new StringBuilder();
		sResult.append("RQ From ")
			   .append(oLoc.getLocationCode()).append(" - ")
			   .append(oLoc.getLocationCode()).append(s_LINE_SEPARATOR);
			
		PurchaseRequest oRQ = new PurchaseRequest(); 
		oRQ.setLocationId(_sLocID); 
		oRQ.setVendorId("");
		oRQ.setVendorName("");
		oRQ.setTransactionDate(new Date());
		oRQ.setRequestedBy(_sUserName);
		oRQ.setRemark(_sRemark);		
		oRQ.setTransactionStatus(i_RQ_NEW);
		oRQ.setTotalQty(bd_ZERO);
		oRQ.setApprovedBy("");
		oRQ.setLastUpdateDate(oRQ.getTransactionDate());
		
		List vRQD = new ArrayList(_mItems.size());
		Iterator iter = _mItems.keySet().iterator();
		while(iter.hasNext())
		{
			String sItemID = (String) iter.next();
			Item oItem = ItemTool.getItemByID(sItemID);
			double dQty = (Double) _mItems.get(sItemID);
			
			sResult.append("Item: ").append(oItem.getItemCode()).append(" - ").append(oItem.getItemName())
				  .append(" Qty:").append(dQty).append(s_LINE_SEPARATOR);		
			
			PurchaseRequestDetail oRQD = new PurchaseRequestDetail();
			oRQD.setItemId(oItem.getItemId());
			oRQD.setItemCode(oItem.getItemCode());
			oRQD.setItemName(oItem.getItemName());
			oRQD.setDescription(oItem.getDescription());
			oRQD.setRequestQty(new BigDecimal(dQty));
			oRQD.setUnitId(oItem.getUnitId());
			oRQD.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));	
			
			InventoryLocation oInvLoc = InventoryLocationTool.getDataByItemAndLocationID (oRQD.getItemId(), _sLocID);				
			ItemInventory oItemInv = ItemInventoryTool.getItemInventory(oRQD.getItemId(), _sLocID);
			if(oItemInv != null)
			{
				oRQD.setMinimumStock  (oItemInv.getMinimumQty());
				oRQD.setMaximumStock  (oItemInv.getMaximumQty());
				oRQD.setReorderPoint  (oItemInv.getReorderPoint());
			}
			else
			{
				oRQD.setMinimumStock  (bd_ZERO);
				oRQD.setMaximumStock  (bd_ZERO);
				oRQD.setReorderPoint  (bd_ZERO);
			}
			
			double dQtyOH = 0;
			if(oInvLoc != null)
			{
				dQtyOH = oInvLoc.getCurrentQty().doubleValue();
				oRQD.setCurrentQty    (oInvLoc.getCurrentQty());
				oRQD.setCostPerUnit   (oInvLoc.getItemCost());
			}
			else
			{
				oRQD.setCurrentQty    (bd_ZERO);
				oRQD.setCostPerUnit   (bd_ZERO);
			}
							
			double dRequestQty = dQty;
			double dRecommended = 0;
			if (oRQD.getMaximumStock().doubleValue() != 0) 
			{
				dRecommended = oRQD.getMaximumStock().doubleValue() - dQtyOH;
			}
			oRQD.setRecommendedQty(new BigDecimal(dRecommended));
			oRQD.setSentQty(bd_ZERO);	
			vRQD.add(oRQD);
		}
		oRQ.setTotalQty(new BigDecimal(countQty(vRQD)));   	
		saveData(oRQ, vRQD, null);
		return sResult.toString();
	}
	
	/**
     * 
     * @param _sLocID
     * @param _sUserName
     * @param _mItems
     * @return
     * @throws Exception
     */
	public static String sendRQToHO(String _sRQID) 
		throws Exception
	{
		PurchaseRequest oRQ = getHeaderByID(_sRQID);
		List vRQD = getDetailsByID(_sRQID);
		String sResult = "RQ: " + oRQ.getTransactionNo();
		if(oRQ != null && vRQD != null && vRQD.size() > 0 && oRQ.getStatus() == i_RQ_CONFIRMED || oRQ.getStatus() == i_RQ_APPROVED)
		{
			Gson gson = new Gson();
			String sRQ = gson.toJson(oRQ);
			String sRQD = gson.toJson(vRQD);
						
			String sURL = PreferenceTool.getSysConfig().getSyncHoUploadUrl();
			if(StringUtil.isNotEmpty(sURL))
			{
				sURL = sURL.replace("upload", "weblayer/template/api,RQ.vm");
				
				PostMethod oPost =  new PostMethod(sURL);								
				oPost.addParameter("cmd", "2");
				oPost.addParameter("rq", sRQ);    	
				oPost.addParameter("rqd", sRQD);
			
				log.debug("URI:" + oPost.getURI() + " PARAMS: cmd:" + oPost.getParameter("cmd") + 
						  " rq:" + oPost.getParameter("rq") + " rqd:" + oPost.getParameter("rqd"));
				
				HttpClient oHTTPClient = new HttpClient();    	
				try 
				{
					int iResult = oHTTPClient.executeMethod(oPost);
					sResult = oPost.getResponseBodyAsString();
					if (iResult != -1)
					{
						oPost.releaseConnection();
					}
					log.debug("RESULT: " + iResult + " RESPONSE: " + sResult);
				} 
				catch (HttpException _oEx) 
				{
					sResult  += "HTTP Error: " + _oEx.getMessage();
					_oEx.printStackTrace();
					log.error(_oEx.getMessage(), _oEx);
				} 
				catch (IOException _oEx) 
				{
					sResult  += "I/O Error: " + _oEx.getMessage();
					_oEx.printStackTrace();
					log.error(_oEx.getMessage(), _oEx);
				}						
			}
			else{
				sResult += " Empty HO Upload URL in Preference";			
			}
		}
		return sResult;	
	}
	
	/**
     * 
     * @param _sLocID
     * @param _sUserName
     * @param _mItems
     * @return
     * @throws Exception
     */
	public static String loadJSON(String _sRQ, String _sRQD) 
		throws Exception
	{
		String sResult = "Server Result: ";
		try {
			Gson gson = new Gson();
			PurchaseRequest oRQ = (PurchaseRequest) gson.fromJson(_sRQ, PurchaseRequest.class);
			oRQ.setNew(true);
			oRQ.setModified(true);
			
			System.out.println(oRQ);
				
			sResult += "\nRQ: " + oRQ.getTransactionNo();
			
			PurchaseRequestDetail[] vRQ = gson.fromJson(_sRQD, PurchaseRequestDetail[].class);		
			List vRQD = new ArrayList();			
			for (int i = 0; i < vRQ.length; i++)
			{
				vRQD.add(vRQ[i]);			
			}
			sResult += "\nRQD: " + vRQD.size()  + " Items";
			
			if(oRQ != null && vRQD.size() > 0)
			{	
				PurchaseRequest oExist = getHeaderByID(oRQ.getPurchaseRequestId());
				if (oExist == null)
				{
					saveData(oRQ, vRQD, null);
					sResult += "\nSaved Successfully";
				}
				else
				{
					sResult += "\nERROR: RQ already exists!";
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			sResult += " ERROR:" + e.getMessage();
		}
		return sResult;
	}	
}


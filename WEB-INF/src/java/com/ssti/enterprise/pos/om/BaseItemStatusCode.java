package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemStatusCode
 */
public abstract class BaseItemStatusCode extends BaseObject
{
    /** The Peer class */
    private static final ItemStatusCodePeer peer =
        new ItemStatusCodePeer();

        
    /** The value for the itemStatusCodeId field */
    private String itemStatusCodeId;
      
    /** The value for the statusCode field */
    private String statusCode;
      
    /** The value for the variable field */
    private int variable;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the isDefault field */
    private boolean isDefault;
  
    
    /**
     * Get the ItemStatusCodeId
     *
     * @return String
     */
    public String getItemStatusCodeId()
    {
        return itemStatusCodeId;
    }

                        
    /**
     * Set the value of ItemStatusCodeId
     *
     * @param v new value
     */
    public void setItemStatusCodeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemStatusCodeId, v))
              {
            this.itemStatusCodeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the StatusCode
     *
     * @return String
     */
    public String getStatusCode()
    {
        return statusCode;
    }

                        
    /**
     * Set the value of StatusCode
     *
     * @param v new value
     */
    public void setStatusCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.statusCode, v))
              {
            this.statusCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Variable
     *
     * @return int
     */
    public int getVariable()
    {
        return variable;
    }

                        
    /**
     * Set the value of Variable
     *
     * @param v new value
     */
    public void setVariable(int v) 
    {
    
                  if (this.variable != v)
              {
            this.variable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsDefault
     *
     * @return boolean
     */
    public boolean getIsDefault()
    {
        return isDefault;
    }

                        
    /**
     * Set the value of IsDefault
     *
     * @param v new value
     */
    public void setIsDefault(boolean v) 
    {
    
                  if (this.isDefault != v)
              {
            this.isDefault = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemStatusCodeId");
              fieldNames.add("StatusCode");
              fieldNames.add("Variable");
              fieldNames.add("Description");
              fieldNames.add("IsDefault");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemStatusCodeId"))
        {
                return getItemStatusCodeId();
            }
          if (name.equals("StatusCode"))
        {
                return getStatusCode();
            }
          if (name.equals("Variable"))
        {
                return Integer.valueOf(getVariable());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("IsDefault"))
        {
                return Boolean.valueOf(getIsDefault());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemStatusCodePeer.ITEM_STATUS_CODE_ID))
        {
                return getItemStatusCodeId();
            }
          if (name.equals(ItemStatusCodePeer.STATUS_CODE))
        {
                return getStatusCode();
            }
          if (name.equals(ItemStatusCodePeer.VARIABLE))
        {
                return Integer.valueOf(getVariable());
            }
          if (name.equals(ItemStatusCodePeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(ItemStatusCodePeer.IS_DEFAULT))
        {
                return Boolean.valueOf(getIsDefault());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemStatusCodeId();
            }
              if (pos == 1)
        {
                return getStatusCode();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getVariable());
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return Boolean.valueOf(getIsDefault());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemStatusCodePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemStatusCodePeer.doInsert((ItemStatusCode) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemStatusCodePeer.doUpdate((ItemStatusCode) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemStatusCodeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemStatusCodeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemStatusCodeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemStatusCodeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemStatusCode copy() throws TorqueException
    {
        return copyInto(new ItemStatusCode());
    }
  
    protected ItemStatusCode copyInto(ItemStatusCode copyObj) throws TorqueException
    {
          copyObj.setItemStatusCodeId(itemStatusCodeId);
          copyObj.setStatusCode(statusCode);
          copyObj.setVariable(variable);
          copyObj.setDescription(description);
          copyObj.setIsDefault(isDefault);
  
                    copyObj.setItemStatusCodeId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemStatusCodePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemStatusCode\n");
        str.append("--------------\n")
           .append("ItemStatusCodeId     : ")
           .append(getItemStatusCodeId())
           .append("\n")
           .append("StatusCode           : ")
           .append(getStatusCode())
           .append("\n")
           .append("Variable             : ")
           .append(getVariable())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("IsDefault            : ")
           .append(getIsDefault())
           .append("\n")
        ;
        return(str.toString());
    }
}

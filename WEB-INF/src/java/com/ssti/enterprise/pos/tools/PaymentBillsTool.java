package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PaymentBills;
import com.ssti.enterprise.pos.om.PaymentBillsPeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PaymentBillsTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: PaymentBillsTool.java,v $
 * Revision 1.8  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/10 07:44:49  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class PaymentBillsTool extends BaseTool 
{
	private static PaymentBillsTool instance = null;
	
	public static synchronized PaymentBillsTool getInstance()
	{
		if (instance == null) instance = new PaymentBillsTool();
		return instance;
	}
	
	public static List getAll()
		throws Exception
	{
		Criteria oCrit = new Criteria();	
		oCrit.addDescendingOrderByColumn(PaymentBillsPeer.AMOUNT);
		return PaymentBillsPeer.doSelect(oCrit);
	}
	
	public static PaymentBills getByID(String _sID)
		throws Exception
	{		
		return getByID(_sID, null);
	}

	public static PaymentBills getByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(PaymentBillsPeer.PAYMENT_BILLS_ID, _sID);
		List vData = PaymentBillsPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (PaymentBills)vData.get(0);
		}
		return null;
	}

}

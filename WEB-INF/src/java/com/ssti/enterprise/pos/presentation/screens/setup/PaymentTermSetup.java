package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;

public class PaymentTermSetup extends PaymentSetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
    		boolean bEDC = data.getParameters().getBoolean("edc", false);
    		if(bEDC)
    		{
    			context.put("PaymentTerms", PaymentTermTool.getEDC());
    		}
    		else
    		{
    			context.put("PaymentTerms", PaymentTermTool.getAllPaymentTerm());
    		}
    		context.put("edc", bEDC);
        }
        catch(Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
}

package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.PaymentTermManager;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentTermPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class PaymentTermSetupAction extends PaymentSetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			PaymentTerm oPaymentTerm = new PaymentTerm();
			setProperties (oPaymentTerm, data);
			oPaymentTerm.setPaymentTermId (IDGenerator.generateSysID());
	        oPaymentTerm.save();
	        PaymentTermManager.getInstance().refreshCache(oPaymentTerm);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			PaymentTerm oPTM = PaymentTermTool.getPaymentTermByID(data.getParameters().getString("ID"));
			PaymentTerm oOld = oPTM.copy();
			setProperties (oPTM, data);
			oPTM.setCashPayment(data.getParameters().getBoolean("CashPayment"));
			oPTM.setIsDefault(data.getParameters().getBoolean("IsDefault"));
			oPTM.setIsMultiplePayment(data.getParameters().getBoolean("IsMultiplePayment"));
	        oPTM.save();
			PaymentTermManager.getInstance().refreshCache(oPTM);
			UpdateHistoryTool.createHistory(oOld, oPTM, data.getUser().getName(), null);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{	
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("sales_transaction","payment_term_id",sID)
    			&& SqlUtil.validateFKRef("purchase_order","payment_term_id",sID)
    			&& SqlUtil.validateFKRef("purchase_invoice","payment_term_id",sID))
    		{
    			PaymentTerm oPTM = PaymentTermTool.getPaymentTermByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(PaymentTermPeer.PAYMENT_TERM_ID, data.getParameters().getString("ID"));
	        	PaymentTermPeer.doDelete(oCrit);
				PaymentTermManager.getInstance().refreshCache(sID);
				UpdateHistoryTool.createDelHistory(oPTM, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	 data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED)+_oEx.getMessage());
        }
    }

    private void setProperties (PaymentTerm _oPaymentTerm, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oPaymentTerm);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}

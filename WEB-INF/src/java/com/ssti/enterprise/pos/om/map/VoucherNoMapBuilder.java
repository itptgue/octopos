package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class VoucherNoMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.VoucherNoMapBuilder";

    /**
     * Item
     * @deprecated use VoucherNoPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "voucher_no";
    }

  
    /**
     * voucher_no.VOUCHER_NO_ID
     * @return the column name for the VOUCHER_NO_ID field
     * @deprecated use VoucherNoPeer.voucher_no.VOUCHER_NO_ID constant
     */
    public static String getVoucherNo_VoucherNoId()
    {
        return "voucher_no.VOUCHER_NO_ID";
    }
  
    /**
     * voucher_no.VOUCHER_ID
     * @return the column name for the VOUCHER_ID field
     * @deprecated use VoucherNoPeer.voucher_no.VOUCHER_ID constant
     */
    public static String getVoucherNo_VoucherId()
    {
        return "voucher_no.VOUCHER_ID";
    }
  
    /**
     * voucher_no.VOUCHER_NO
     * @return the column name for the VOUCHER_NO field
     * @deprecated use VoucherNoPeer.voucher_no.VOUCHER_NO constant
     */
    public static String getVoucherNo_VoucherNo()
    {
        return "voucher_no.VOUCHER_NO";
    }
  
    /**
     * voucher_no.VALID_FROM
     * @return the column name for the VALID_FROM field
     * @deprecated use VoucherNoPeer.voucher_no.VALID_FROM constant
     */
    public static String getVoucherNo_ValidFrom()
    {
        return "voucher_no.VALID_FROM";
    }
  
    /**
     * voucher_no.VALID_TO
     * @return the column name for the VALID_TO field
     * @deprecated use VoucherNoPeer.voucher_no.VALID_TO constant
     */
    public static String getVoucherNo_ValidTo()
    {
        return "voucher_no.VALID_TO";
    }
  
    /**
     * voucher_no.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use VoucherNoPeer.voucher_no.AMOUNT constant
     */
    public static String getVoucherNo_Amount()
    {
        return "voucher_no.AMOUNT";
    }
  
    /**
     * voucher_no.CREATE_ID
     * @return the column name for the CREATE_ID field
     * @deprecated use VoucherNoPeer.voucher_no.CREATE_ID constant
     */
    public static String getVoucherNo_CreateId()
    {
        return "voucher_no.CREATE_ID";
    }
  
    /**
     * voucher_no.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use VoucherNoPeer.voucher_no.CREATE_BY constant
     */
    public static String getVoucherNo_CreateBy()
    {
        return "voucher_no.CREATE_BY";
    }
  
    /**
     * voucher_no.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use VoucherNoPeer.voucher_no.CREATE_DATE constant
     */
    public static String getVoucherNo_CreateDate()
    {
        return "voucher_no.CREATE_DATE";
    }
  
    /**
     * voucher_no.USE_PTYPE_ID
     * @return the column name for the USE_PTYPE_ID field
     * @deprecated use VoucherNoPeer.voucher_no.USE_PTYPE_ID constant
     */
    public static String getVoucherNo_UsePtypeId()
    {
        return "voucher_no.USE_PTYPE_ID";
    }
  
    /**
     * voucher_no.USE_LOC_ID
     * @return the column name for the USE_LOC_ID field
     * @deprecated use VoucherNoPeer.voucher_no.USE_LOC_ID constant
     */
    public static String getVoucherNo_UseLocId()
    {
        return "voucher_no.USE_LOC_ID";
    }
  
    /**
     * voucher_no.USE_TRANS_ID
     * @return the column name for the USE_TRANS_ID field
     * @deprecated use VoucherNoPeer.voucher_no.USE_TRANS_ID constant
     */
    public static String getVoucherNo_UseTransId()
    {
        return "voucher_no.USE_TRANS_ID";
    }
  
    /**
     * voucher_no.USE_CASHIER
     * @return the column name for the USE_CASHIER field
     * @deprecated use VoucherNoPeer.voucher_no.USE_CASHIER constant
     */
    public static String getVoucherNo_UseCashier()
    {
        return "voucher_no.USE_CASHIER";
    }
  
    /**
     * voucher_no.USE_DATE
     * @return the column name for the USE_DATE field
     * @deprecated use VoucherNoPeer.voucher_no.USE_DATE constant
     */
    public static String getVoucherNo_UseDate()
    {
        return "voucher_no.USE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("voucher_no");
        TableMap tMap = dbMap.getTable("voucher_no");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("voucher_no.VOUCHER_NO_ID", "");
                          tMap.addForeignKey(
                "voucher_no.VOUCHER_ID", "" , "voucher" ,
                "voucher_id");
                          tMap.addColumn("voucher_no.VOUCHER_NO", "");
                          tMap.addColumn("voucher_no.VALID_FROM", new Date());
                          tMap.addColumn("voucher_no.VALID_TO", new Date());
                            tMap.addColumn("voucher_no.AMOUNT", bd_ZERO);
                          tMap.addColumn("voucher_no.CREATE_ID", "");
                          tMap.addColumn("voucher_no.CREATE_BY", "");
                          tMap.addColumn("voucher_no.CREATE_DATE", new Date());
                          tMap.addColumn("voucher_no.USE_PTYPE_ID", "");
                          tMap.addColumn("voucher_no.USE_LOC_ID", "");
                          tMap.addColumn("voucher_no.USE_TRANS_ID", "");
                          tMap.addColumn("voucher_no.USE_CASHIER", "");
                          tMap.addColumn("voucher_no.USE_DATE", new Date());
          }
}

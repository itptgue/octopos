package com.ssti.enterprise.pos.tools.gl;

import java.util.List;

import com.ssti.enterprise.pos.manager.AccountTypeManager;
import com.ssti.enterprise.pos.om.AccountType;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AccountTypeTool.java,v 1.5 2007/05/09 06:45:06 albert Exp $ <br>
 *
 * <pre>
 * $Log: AccountTypeTool.java,v $
 * Revision 1.5  2007/05/09 06:45:06  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class AccountTypeTool extends BaseTool implements GlAttributes
{

	//static methods
	public static List getAllAccountType()
		throws Exception
	{
		return AccountTypeManager.getInstance().getAllAccountType();
	}

	public static AccountType getAccountType(int _iType)
    	throws Exception
    {
        return AccountTypeManager.getInstance().getAccountType(_iType, null);
	}
	
	public static AccountType getAccountTypeByID(String _sID)
    	throws Exception
    {
        return AccountTypeManager.getInstance().getAccountTypeByID(_sID, null);
	}

	public static String getCodeByID(String _sID)
    	throws Exception
    {
		AccountType oAccountType = getAccountTypeByID (_sID);
		if (oAccountType != null) {
			return oAccountType.getAccountTypeCode();
		}
		return "";
	}

	public static String getNameByID(String _sID)
    	throws Exception
    {
		AccountType oAccountType = getAccountTypeByID (_sID);
		if (oAccountType != null) {
			return oAccountType.getAccountTypeName();
		}
		return "";
	}

	public static String getNameByType(int _iType)
		throws Exception
	{
		if (_iType == i_CASH_BANK           ) return LocaleTool.getString ("bank");            
		if (_iType == i_ACCOUNT_RECEIVABLE  ) return LocaleTool.getString ("account_receivable");   
		if (_iType == i_INVENTORY_ASSET     ) return LocaleTool.getString ("inventory");            
		if (_iType == i_OTHER_ASSET         ) return LocaleTool.getString ("other_asset");          
		if (_iType == i_FIXED_ASSET         ) return LocaleTool.getString ("fixed_asset");          
		if (_iType == i_FA_DEPRECIATION     ) return LocaleTool.getString ("fa_depreciation");      
		if (_iType == i_ACCOUNT_PAYABLE     ) return LocaleTool.getString ("account_payable");      
		if (_iType == i_OTHER_LIABILITY     ) return LocaleTool.getString ("other_liability");      
		if (_iType == i_LONG_TERM_LIABILITY ) return LocaleTool.getString ("long_term_liability");  
		if (_iType == i_EQUITY              ) return LocaleTool.getString ("equity");               
		if (_iType == i_REVENUE             ) return LocaleTool.getString ("revenue");              
		if (_iType == i_COGS                ) return LocaleTool.getString ("cogs");                 
		if (_iType == i_EXPENSE             ) return LocaleTool.getString ("expense");              
		if (_iType == i_OTHER_INCOME        ) return LocaleTool.getString ("other_income");         
		if (_iType == i_OTHER_EXPENSE       ) return LocaleTool.getString ("other_expense");  	
		if (_iType == i_INCOME_TAX          ) return LocaleTool.getString ("income_tax");  			
		return "";
	}

	
	public static int getGLTypeByID(String _sID)
    	throws Exception
    {
		AccountType oAccountType = getAccountTypeByID (_sID);
		if (oAccountType != null) {
			return oAccountType.getGlType();
		}
		return -1;
	}

	public static AccountType getAccountTypeByName(String _sName)
    	throws Exception
    {
		return AccountTypeManager.getInstance().getAccountTypeByName(_sName);
	}

    public static List getAccountTypeByGLType(int _iGLType)
    	throws Exception
    {
        return AccountTypeManager.getInstance().getAccountTypeByGLType(_iGLType, null);
    }
    
	public static String getSimilarAccountType(int _iType)
    	throws Exception
    {
		AccountType oAccountType = getAccountType (_iType);		
        StringBuilder oSB = new StringBuilder();
		if (oAccountType != null)
		{
            List vData = getAccountTypeByGLType(oAccountType.getGlType());
            for (int i = 0; i < vData.size(); i++)
            {
                AccountType oType = (AccountType) vData.get(i);
                oSB.append(oType.getAccountType());
                if (i != (vData.size() - 1)) 
                oSB.append(",");
            }   
        }
        return oSB.toString();     
	}
}

package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class SalesReturnMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.SalesReturnMapBuilder";

    /**
     * Item
     * @deprecated use SalesReturnPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "sales_return";
    }

  
    /**
     * sales_return.SALES_RETURN_ID
     * @return the column name for the SALES_RETURN_ID field
     * @deprecated use SalesReturnPeer.sales_return.SALES_RETURN_ID constant
     */
    public static String getSalesReturn_SalesReturnId()
    {
        return "sales_return.SALES_RETURN_ID";
    }
  
    /**
     * sales_return.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use SalesReturnPeer.sales_return.TRANSACTION_TYPE constant
     */
    public static String getSalesReturn_TransactionType()
    {
        return "sales_return.TRANSACTION_TYPE";
    }
  
    /**
     * sales_return.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use SalesReturnPeer.sales_return.TRANSACTION_ID constant
     */
    public static String getSalesReturn_TransactionId()
    {
        return "sales_return.TRANSACTION_ID";
    }
  
    /**
     * sales_return.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use SalesReturnPeer.sales_return.LOCATION_ID constant
     */
    public static String getSalesReturn_LocationId()
    {
        return "sales_return.LOCATION_ID";
    }
  
    /**
     * sales_return.STATUS
     * @return the column name for the STATUS field
     * @deprecated use SalesReturnPeer.sales_return.STATUS constant
     */
    public static String getSalesReturn_Status()
    {
        return "sales_return.STATUS";
    }
  
    /**
     * sales_return.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use SalesReturnPeer.sales_return.TRANSACTION_NO constant
     */
    public static String getSalesReturn_TransactionNo()
    {
        return "sales_return.TRANSACTION_NO";
    }
  
    /**
     * sales_return.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use SalesReturnPeer.sales_return.TRANSACTION_DATE constant
     */
    public static String getSalesReturn_TransactionDate()
    {
        return "sales_return.TRANSACTION_DATE";
    }
  
    /**
     * sales_return.RETURN_NO
     * @return the column name for the RETURN_NO field
     * @deprecated use SalesReturnPeer.sales_return.RETURN_NO constant
     */
    public static String getSalesReturn_ReturnNo()
    {
        return "sales_return.RETURN_NO";
    }
  
    /**
     * sales_return.RETURN_DATE
     * @return the column name for the RETURN_DATE field
     * @deprecated use SalesReturnPeer.sales_return.RETURN_DATE constant
     */
    public static String getSalesReturn_ReturnDate()
    {
        return "sales_return.RETURN_DATE";
    }
  
    /**
     * sales_return.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use SalesReturnPeer.sales_return.CUSTOMER_ID constant
     */
    public static String getSalesReturn_CustomerId()
    {
        return "sales_return.CUSTOMER_ID";
    }
  
    /**
     * sales_return.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use SalesReturnPeer.sales_return.CUSTOMER_NAME constant
     */
    public static String getSalesReturn_CustomerName()
    {
        return "sales_return.CUSTOMER_NAME";
    }
  
    /**
     * sales_return.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use SalesReturnPeer.sales_return.TOTAL_QTY constant
     */
    public static String getSalesReturn_TotalQty()
    {
        return "sales_return.TOTAL_QTY";
    }
  
    /**
     * sales_return.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use SalesReturnPeer.sales_return.TOTAL_TAX constant
     */
    public static String getSalesReturn_TotalTax()
    {
        return "sales_return.TOTAL_TAX";
    }
  
    /**
     * sales_return.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use SalesReturnPeer.sales_return.TOTAL_DISCOUNT_PCT constant
     */
    public static String getSalesReturn_TotalDiscountPct()
    {
        return "sales_return.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * sales_return.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use SalesReturnPeer.sales_return.TOTAL_DISCOUNT constant
     */
    public static String getSalesReturn_TotalDiscount()
    {
        return "sales_return.TOTAL_DISCOUNT";
    }
  
    /**
     * sales_return.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use SalesReturnPeer.sales_return.TOTAL_AMOUNT constant
     */
    public static String getSalesReturn_TotalAmount()
    {
        return "sales_return.TOTAL_AMOUNT";
    }
  
    /**
     * sales_return.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use SalesReturnPeer.sales_return.USER_NAME constant
     */
    public static String getSalesReturn_UserName()
    {
        return "sales_return.USER_NAME";
    }
  
    /**
     * sales_return.REMARK
     * @return the column name for the REMARK field
     * @deprecated use SalesReturnPeer.sales_return.REMARK constant
     */
    public static String getSalesReturn_Remark()
    {
        return "sales_return.REMARK";
    }
  
    /**
     * sales_return.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use SalesReturnPeer.sales_return.PAYMENT_TYPE_ID constant
     */
    public static String getSalesReturn_PaymentTypeId()
    {
        return "sales_return.PAYMENT_TYPE_ID";
    }
  
    /**
     * sales_return.CREATE_CREDIT_MEMO
     * @return the column name for the CREATE_CREDIT_MEMO field
     * @deprecated use SalesReturnPeer.sales_return.CREATE_CREDIT_MEMO constant
     */
    public static String getSalesReturn_CreateCreditMemo()
    {
        return "sales_return.CREATE_CREDIT_MEMO";
    }
  
    /**
     * sales_return.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use SalesReturnPeer.sales_return.CURRENCY_ID constant
     */
    public static String getSalesReturn_CurrencyId()
    {
        return "sales_return.CURRENCY_ID";
    }
  
    /**
     * sales_return.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use SalesReturnPeer.sales_return.CURRENCY_RATE constant
     */
    public static String getSalesReturn_CurrencyRate()
    {
        return "sales_return.CURRENCY_RATE";
    }
  
    /**
     * sales_return.SALES_ID
     * @return the column name for the SALES_ID field
     * @deprecated use SalesReturnPeer.sales_return.SALES_ID constant
     */
    public static String getSalesReturn_SalesId()
    {
        return "sales_return.SALES_ID";
    }
  
    /**
     * sales_return.RETURN_REASON_ID
     * @return the column name for the RETURN_REASON_ID field
     * @deprecated use SalesReturnPeer.sales_return.RETURN_REASON_ID constant
     */
    public static String getSalesReturn_ReturnReasonId()
    {
        return "sales_return.RETURN_REASON_ID";
    }
  
    /**
     * sales_return.CASH_RETURN
     * @return the column name for the CASH_RETURN field
     * @deprecated use SalesReturnPeer.sales_return.CASH_RETURN constant
     */
    public static String getSalesReturn_CashReturn()
    {
        return "sales_return.CASH_RETURN";
    }
  
    /**
     * sales_return.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use SalesReturnPeer.sales_return.BANK_ID constant
     */
    public static String getSalesReturn_BankId()
    {
        return "sales_return.BANK_ID";
    }
  
    /**
     * sales_return.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use SalesReturnPeer.sales_return.BANK_ISSUER constant
     */
    public static String getSalesReturn_BankIssuer()
    {
        return "sales_return.BANK_ISSUER";
    }
  
    /**
     * sales_return.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use SalesReturnPeer.sales_return.DUE_DATE constant
     */
    public static String getSalesReturn_DueDate()
    {
        return "sales_return.DUE_DATE";
    }
  
    /**
     * sales_return.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use SalesReturnPeer.sales_return.REFERENCE_NO constant
     */
    public static String getSalesReturn_ReferenceNo()
    {
        return "sales_return.REFERENCE_NO";
    }
  
    /**
     * sales_return.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use SalesReturnPeer.sales_return.CASH_FLOW_TYPE_ID constant
     */
    public static String getSalesReturn_CashFlowTypeId()
    {
        return "sales_return.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * sales_return.ROUNDING_AMOUNT
     * @return the column name for the ROUNDING_AMOUNT field
     * @deprecated use SalesReturnPeer.sales_return.ROUNDING_AMOUNT constant
     */
    public static String getSalesReturn_RoundingAmount()
    {
        return "sales_return.ROUNDING_AMOUNT";
    }
  
    /**
     * sales_return.RETURNED_AMOUNT
     * @return the column name for the RETURNED_AMOUNT field
     * @deprecated use SalesReturnPeer.sales_return.RETURNED_AMOUNT constant
     */
    public static String getSalesReturn_ReturnedAmount()
    {
        return "sales_return.RETURNED_AMOUNT";
    }
  
    /**
     * sales_return.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use SalesReturnPeer.sales_return.FISCAL_RATE constant
     */
    public static String getSalesReturn_FiscalRate()
    {
        return "sales_return.FISCAL_RATE";
    }
  
    /**
     * sales_return.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use SalesReturnPeer.sales_return.IS_TAXABLE constant
     */
    public static String getSalesReturn_IsTaxable()
    {
        return "sales_return.IS_TAXABLE";
    }
  
    /**
     * sales_return.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use SalesReturnPeer.sales_return.IS_INCLUSIVE_TAX constant
     */
    public static String getSalesReturn_IsInclusiveTax()
    {
        return "sales_return.IS_INCLUSIVE_TAX";
    }
  
    /**
     * sales_return.RETURN_DISC_ACC_ID
     * @return the column name for the RETURN_DISC_ACC_ID field
     * @deprecated use SalesReturnPeer.sales_return.RETURN_DISC_ACC_ID constant
     */
    public static String getSalesReturn_ReturnDiscAccId()
    {
        return "sales_return.RETURN_DISC_ACC_ID";
    }
  
    /**
     * sales_return.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use SalesReturnPeer.sales_return.CANCEL_BY constant
     */
    public static String getSalesReturn_CancelBy()
    {
        return "sales_return.CANCEL_BY";
    }
  
    /**
     * sales_return.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use SalesReturnPeer.sales_return.CANCEL_DATE constant
     */
    public static String getSalesReturn_CancelDate()
    {
        return "sales_return.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("sales_return");
        TableMap tMap = dbMap.getTable("sales_return");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("sales_return.SALES_RETURN_ID", "");
                            tMap.addColumn("sales_return.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("sales_return.TRANSACTION_ID", "");
                          tMap.addColumn("sales_return.LOCATION_ID", "");
                            tMap.addColumn("sales_return.STATUS", Integer.valueOf(0));
                          tMap.addColumn("sales_return.TRANSACTION_NO", "");
                          tMap.addColumn("sales_return.TRANSACTION_DATE", new Date());
                          tMap.addColumn("sales_return.RETURN_NO", "");
                          tMap.addColumn("sales_return.RETURN_DATE", new Date());
                          tMap.addColumn("sales_return.CUSTOMER_ID", "");
                          tMap.addColumn("sales_return.CUSTOMER_NAME", "");
                            tMap.addColumn("sales_return.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("sales_return.TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("sales_return.TOTAL_DISCOUNT_PCT", "");
                            tMap.addColumn("sales_return.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("sales_return.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("sales_return.USER_NAME", "");
                          tMap.addColumn("sales_return.REMARK", "");
                          tMap.addColumn("sales_return.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("sales_return.CREATE_CREDIT_MEMO", Boolean.TRUE);
                          tMap.addColumn("sales_return.CURRENCY_ID", "");
                            tMap.addColumn("sales_return.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("sales_return.SALES_ID", "");
                          tMap.addColumn("sales_return.RETURN_REASON_ID", "");
                          tMap.addColumn("sales_return.CASH_RETURN", Boolean.TRUE);
                          tMap.addColumn("sales_return.BANK_ID", "");
                          tMap.addColumn("sales_return.BANK_ISSUER", "");
                          tMap.addColumn("sales_return.DUE_DATE", new Date());
                          tMap.addColumn("sales_return.REFERENCE_NO", "");
                          tMap.addColumn("sales_return.CASH_FLOW_TYPE_ID", "");
                            tMap.addColumn("sales_return.ROUNDING_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_return.RETURNED_AMOUNT", bd_ZERO);
                            tMap.addColumn("sales_return.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("sales_return.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("sales_return.IS_INCLUSIVE_TAX", Boolean.TRUE);
                          tMap.addColumn("sales_return.RETURN_DISC_ACC_ID", "");
                          tMap.addColumn("sales_return.CANCEL_BY", "");
                          tMap.addColumn("sales_return.CANCEL_DATE", new Date());
          }
}

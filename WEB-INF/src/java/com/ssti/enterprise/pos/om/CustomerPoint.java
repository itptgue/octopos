
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Tue Mar 08 19:17:05 ICT 2005]
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * 
 * 2016-03-10
 * - change CustomerPoint totalPoint to BigDecimal
 */
public  class CustomerPoint
    extends com.ssti.enterprise.pos.om.BaseCustomerPoint
    implements Persistent
{
}

package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PrintingLogMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PrintingLogMapBuilder";

    /**
     * Item
     * @deprecated use PrintingLogPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "printing_log";
    }

  
    /**
     * printing_log.PRINTING_LOG_ID
     * @return the column name for the PRINTING_LOG_ID field
     * @deprecated use PrintingLogPeer.printing_log.PRINTING_LOG_ID constant
     */
    public static String getPrintingLog_PrintingLogId()
    {
        return "printing_log.PRINTING_LOG_ID";
    }
  
    /**
     * printing_log.PRINTING_DATE
     * @return the column name for the PRINTING_DATE field
     * @deprecated use PrintingLogPeer.printing_log.PRINTING_DATE constant
     */
    public static String getPrintingLog_PrintingDate()
    {
        return "printing_log.PRINTING_DATE";
    }
  
    /**
     * printing_log.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use PrintingLogPeer.printing_log.TRANSACTION_ID constant
     */
    public static String getPrintingLog_TransactionId()
    {
        return "printing_log.TRANSACTION_ID";
    }
  
    /**
     * printing_log.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use PrintingLogPeer.printing_log.TRANSACTION_NO constant
     */
    public static String getPrintingLog_TransactionNo()
    {
        return "printing_log.TRANSACTION_NO";
    }
  
    /**
     * printing_log.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use PrintingLogPeer.printing_log.USER_NAME constant
     */
    public static String getPrintingLog_UserName()
    {
        return "printing_log.USER_NAME";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("printing_log");
        TableMap tMap = dbMap.getTable("printing_log");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("printing_log.PRINTING_LOG_ID", "");
                          tMap.addColumn("printing_log.PRINTING_DATE", new Date());
                          tMap.addColumn("printing_log.TRANSACTION_ID", "");
                          tMap.addColumn("printing_log.TRANSACTION_NO", "");
                          tMap.addColumn("printing_log.USER_NAME", "");
          }
}

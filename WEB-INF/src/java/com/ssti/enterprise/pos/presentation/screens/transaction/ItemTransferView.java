package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class ItemTransferView extends ItemTransferTransaction
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemTransferResult", "ItemTransfers");    		
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}
package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PettyCashBalanceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PettyCashBalanceMapBuilder";

    /**
     * Item
     * @deprecated use PettyCashBalancePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "petty_cash_balance";
    }

  
    /**
     * petty_cash_balance.PETTY_CASH_BALANCE_ID
     * @return the column name for the PETTY_CASH_BALANCE_ID field
     * @deprecated use PettyCashBalancePeer.petty_cash_balance.PETTY_CASH_BALANCE_ID constant
     */
    public static String getPettyCashBalance_PettyCashBalanceId()
    {
        return "petty_cash_balance.PETTY_CASH_BALANCE_ID";
    }
  
    /**
     * petty_cash_balance.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PettyCashBalancePeer.petty_cash_balance.LOCATION_ID constant
     */
    public static String getPettyCashBalance_LocationId()
    {
        return "petty_cash_balance.LOCATION_ID";
    }
  
    /**
     * petty_cash_balance.OPENING_BALANCE
     * @return the column name for the OPENING_BALANCE field
     * @deprecated use PettyCashBalancePeer.petty_cash_balance.OPENING_BALANCE constant
     */
    public static String getPettyCashBalance_OpeningBalance()
    {
        return "petty_cash_balance.OPENING_BALANCE";
    }
  
    /**
     * petty_cash_balance.PETTY_CASH_BALANCE
     * @return the column name for the PETTY_CASH_BALANCE field
     * @deprecated use PettyCashBalancePeer.petty_cash_balance.PETTY_CASH_BALANCE constant
     */
    public static String getPettyCashBalance_PettyCashBalance()
    {
        return "petty_cash_balance.PETTY_CASH_BALANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("petty_cash_balance");
        TableMap tMap = dbMap.getTable("petty_cash_balance");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("petty_cash_balance.PETTY_CASH_BALANCE_ID", "");
                          tMap.addColumn("petty_cash_balance.LOCATION_ID", "");
                            tMap.addColumn("petty_cash_balance.OPENING_BALANCE", bd_ZERO);
                            tMap.addColumn("petty_cash_balance.PETTY_CASH_BALANCE", bd_ZERO);
          }
}

package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.model.InventoryMasterOM;
import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.InvTransUpdate;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.InventoryTransactionPeer;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.journal.InventoryJournalTool;
import com.ssti.enterprise.pos.tools.journal.PurchaseReturnJournalTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.framework.perf.StopWatch;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.ssti.framework.tools.TemplateUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 *
 * 2018-09-08
 * - add member m_bUpdatePRCost, to specify whether PI will update PR Cost based on Inventory Journal
 * - m_bUpdatePRCost will be defined in setUpdatePRCost from mPR variables, will check each PR Items 
 *   whether already Transfered or not 
 * - @see {@link InventoryTransactionTool} for new method to check whether PR cost should be updated
 * 
 * 2018-08-15
 * - change updateTrans method use BigDecimal for calculation
 * 
 * 2018-07-10
 * - FIX CRITICAL BUG in updateGL function, previously not working because Calculator.isInTolerance
 * - Change delete(InvTrans) to public, to support partial delete
 * - add setter to m_oTrans to support del(InvTrans)
 * 
 * 2017-09-30
 * - change method updateValue, fix possible java.lang.double calculation problem,  
 *   set scale on QtyChanges, cost, totalCost, qtyBalance, valueBalance, don't forget to check
 *   InvenCommaScale in SysConfig 
 * 
 * 2016-12-27
 * - see ItemTransferTool
 * - change method createFromIT, add parameter iType. -1 Create all, 1 Create TO only, 2 Create TI Only
 * - change method mapITDetail
 * 
 * 2016-12-08
 * - change method createFromIT previously save InvTrans IT IN then IT OUT
 *   now changed to save InvTrans IT OUT then IT IN
 *   to enable FEFO batchTransaction processing  
 *   
 * </pre><br>
 */
public class InventoryCostingTool extends InventoryTransactionTool
{
	private static Log log = LogFactory.getLog(InventoryCostingTool.class);

	Connection m_oConn = null;
	InventoryMasterOM m_oTrans;
	List m_UpdatedTrans;	
		
	public InventoryCostingTool(Connection _oConn)
		throws Exception
	{
		m_UpdatedTrans = new ArrayList();
		m_oConn = _oConn;
		validate(_oConn);
	}
	
	public void setTrans(InventoryMasterOM _oTrans)
	{
		this.m_oTrans = _oTrans;
	}

	public void createFromIR (IssueReceipt _oTR, List _vTD)
		throws Exception
	{
		m_oTrans = _oTR;
		try 
		{
			for (int i = 0; i < _vTD.size(); i++)
			{
				IssueReceiptDetail oTD = (IssueReceiptDetail) _vTD.get(i);
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);	 			
				if (oItem.getItemType() == i_INVENTORY_PART) 
				{
					InventoryTransaction oIT = mapIRDetail (_oTR, oTD);
					save(oIT, oTD);
					
					oTD.setCost(oIT.getCost());
				}
			}
			updateGL();
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}	
	private InventoryTransaction mapIRDetail (IssueReceipt _oTR, IssueReceiptDetail _oTD)
		throws Exception
	{
		String sAdjDesc = "";
		if(_oTR.getAdjType() != null) sAdjDesc = _oTR.getAdjType().getDescription();
		
		StringBuilder oDesc = new StringBuilder ();
		oDesc.append (sAdjDesc).append(" (");
		oDesc.append (_oTR.getTypeDesc()).append(") ").append(_oTR.getDescription()); 
		if (_oTR.getInternalTransfer())
		{
			oDesc.append(" ").append(_oTR.getCrossEntityCode());
		}
		
		InventoryTransaction oInv = new InventoryTransaction();
		oInv.setTransactionId	    (_oTR.getIssueReceiptId());	
		oInv.setTransactionNo	    (_oTR.getTransactionNo());	
		oInv.setTransactionDetailId (_oTD.getIssueReceiptDetailId());			    
		oInv.setTransactionType     (i_INV_TRANS_RECEIPT_UNPLANNED); 
		oInv.setTransactionDate     (_oTR.getTransactionDate());
		oInv.setLocationId          (_oTR.getLocationId());
		oInv.setLocationName        (_oTR.getLocationName());
		oInv.setItemId			    (_oTD.getItemId());
		oInv.setItemCode		    (_oTD.getItemCode());

		double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(), _oTD.getUnitId(), m_oConn);
		double dQtyChange = _oTD.getQtyChanges().doubleValue();
		double dQtyBase   = dQtyChange * dBaseValue;
		double dCost 	  = _oTD.getCost().doubleValue() / dBaseValue;
		if(_oTR.getTransactionType() == i_INV_TRANS_ISSUE_UNPLANNED)
		{
			dQtyBase = dQtyBase * -1;
		}

		oInv.setQtyChanges	 (new BigDecimal(dQtyBase));
		oInv.setCost		 (new BigDecimal(dCost));     
		oInv.setDescription	 (oDesc.toString());
		oInv.setItemPrice	 (ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
		oInv.setPrice		 (oInv.getItemPrice());
		return oInv;
	}	

	public void createFromIT (ItemTransfer _oTR, List _vTD)
		throws Exception
	{
		createFromIT(_oTR, _vTD, -1);
	}	
	
	public void createFromIT (ItemTransfer _oTR, List _vTD, int _iType)
		throws Exception
	{
		m_oTrans = _oTR;
		try 
		{
			for (int i = 0; i < _vTD.size(); i++)
			{
				ItemTransferDetail oTD = (ItemTransferDetail) _vTD.get(i);
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);	 			
				if (oItem.getItemType() == i_INVENTORY_PART) 
				{
					if(_iType <= 0 || _iType == 1)
					{
						InventoryTransaction oOut= mapITDetail (_oTR, oTD, false, _iType);
						save(oOut, oTD);
						oTD.setCostPerUnit(oOut.getCost());
						oTD.save(m_oConn);
					}
					if(_iType <= 0 || _iType == 2)
					{
						InventoryTransaction oIn = mapITDetail (_oTR, oTD, true, _iType);
						save(oIn, oTD);
					}
				}
			}
			updateGL();
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private InventoryTransaction mapITDetail (ItemTransfer _oTR, ItemTransferDetail _oTD, boolean _bInTrans, int _iType)
		throws Exception
	{
		double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(), _oTD.getUnitId(), m_oConn);
		double dQtyChange = _oTD.getQtyChanges().doubleValue();
		double dQtyBase   = dQtyChange * dBaseValue;
		double dCost      = _oTD.getCostPerUnit().doubleValue(); //Cost already in base unit		

		int iTransType = i_INV_TRANS_TRANSFER_OUT;
		String sLocID = _oTR.getFromLocationId();
		String sLocNm = _oTR.getFromLocationName();	
		String sDesc  = "Transfer Out To: " + _oTR.getToLocationName();;
		Date dTransDate = _oTR.getTransactionDate();
		
		if(_bInTrans)
		{
			iTransType = i_INV_TRANS_TRANSFER_IN;
			sLocID = _oTR.getToLocationId();
			sLocNm = _oTR.getToLocationName();
			sDesc  = "Transfer In From: " + _oTR.getFromLocationName();
			if(_iType == ItemTransferTool.i_TI) {dTransDate = _oTR.getReceiveDate();}
		}
		else
		{
			dQtyBase = dQtyBase * -1;
		}
		
		StringBuilder oDesc = new StringBuilder ();
		oDesc.append(sDesc).append(" ");			       
		
		InventoryTransaction oInv = new InventoryTransaction ();		
		oInv.setTransactionId	    (_oTR.getItemTransferId());	
		oInv.setTransactionNo	    (_oTR.getTransactionNo());			    
		oInv.setTransactionDetailId (_oTD.getItemTransferDetailId());		       		    
		oInv.setTransactionType     (iTransType); 
		oInv.setTransactionDate     (dTransDate);
		oInv.setLocationId          (sLocID);
		oInv.setLocationName        (sLocNm);
		oInv.setItemId			    (_oTD.getItemId());
		oInv.setItemCode		    (_oTD.getItemCode());
		oInv.setQtyChanges		    (new BigDecimal (dQtyBase));
		oInv.setCost 			    (new BigDecimal (dCost));		
		oInv.setDescription	     	(oDesc.toString () + _oTR.getDescription());
		oInv.setItemPrice	 		(ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
		oInv.setPrice               (oInv.getItemPrice());
		return oInv;
	}	
	
	public void createFromPR (PurchaseReceipt _oTR, List _vTD)
		throws Exception
	{
		m_oTrans = _oTR;
		try 
		{
			for (int i = 0; i < _vTD.size(); i++)
			{
				PurchaseReceiptDetail oTD = (PurchaseReceiptDetail)_vTD.get(i);
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);	 			
				if (oItem.getItemType() == i_INVENTORY_PART && oTD.getQty().doubleValue() > 0) 
				{
					InventoryTransaction oIT = mapPRDetail (_oTR, oTD);	
					save(oIT, oTD);
					
					//update cost per unit in trans det
					oTD.setCostPerUnit(oIT.getCost());
					oTD.setSubTotal(oIT.getTotalCost());
					oTD.save(m_oConn);
				}
			}
			updateGL();
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private InventoryTransaction mapPRDetail (PurchaseReceipt _oTR, PurchaseReceiptDetail _oTD)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ("Receive From ");
		oDesc.append(_oTR.getVendorName());
		
		InventoryTransaction oInv = new InventoryTransaction();
		oInv.setTransactionId	   (_oTR.getPurchaseReceiptId());	
		oInv.setTransactionNo	   (_oTR.getReceiptNo());		
		oInv.setTransactionDetailId(_oTD.getPurchaseReceiptDetailId());		
		oInv.setTransactionType    (i_INV_TRANS_PURCHASE_RECEIPT); 
		oInv.setTransactionDate    (_oTR.getReceiptDate());
		oInv.setLocationId         (_oTR.getLocationId());
		oInv.setLocationName       (_oTR.getLocationName());			
		oInv.setItemId			   (_oTD.getItemId());
		oInv.setItemCode		   (_oTD.getItemCode());

		double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(),_oTD.getUnitId(), m_oConn);
		double dQtyChange = _oTD.getQty().doubleValue();
		double dQtyBase   = dQtyChange * dBaseValue;
		double dCost      = _oTD.getCostPerUnit().doubleValue(); //check cost is already in base unit

		//if receipt without PO then cost will be 0, so we'll get the current 
		//cost so the cost data in inventory location not changed
		if (StringUtil.isEmpty(_oTD.getPurchaseOrderDetailId()) && dCost == 0) 
		{			
			dCost = InventoryLocationTool
					.getItemCost(_oTD.getItemId(), _oTR.getLocationId(), _oTR.getReceiptDate(), m_oConn)
					.doubleValue();
			
			if (dCost == 0) //if still zero get best guess from last purchase
			{
				Item oItem = ItemTool.getItemByID(_oTD.getItemId(), m_oConn);
				dCost = Calculator.mul(oItem.getLastPurchasePrice(), oItem.getLastPurchaseRate()); 
			}
			_oTD.setCostPerUnit(new BigDecimal(dCost));
		}

		oInv.setPrice		(_oTD.getItemPriceInPo());
		oInv.setItemPrice	(ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
		oInv.setQtyChanges	(new BigDecimal(dQtyBase));
		oInv.setCost		(new BigDecimal(dCost));
		oInv.setDescription	(oDesc.toString());
		return oInv;
	}

	//-------------------------------------------------------------------------
	// purchase invoice
	//-------------------------------------------------------------------------
	boolean m_bUpdatePRCost = true;
	public boolean getUpdatePRCost()
	{
		return m_bUpdatePRCost;
	}
	
	public void setUpdatePRCost(Map _mPR)
	{
		if(_mPR != null && _mPR.size() > 0)
		{
			Iterator iter = _mPR.keySet().iterator();
			while(iter.hasNext())
			{
				String sPRID = (String) iter.next();
				boolean bUpdatePR = InventoryTransactionTool.updatePRCost(sPRID, m_oConn);
				if(!bUpdatePR) m_bUpdatePRCost = false;
			}
		}		
	}
	
	public void createFromPI (PurchaseInvoice _oTR, List _vTD)
		throws Exception
	{
		m_oTrans = _oTR;	
		try 
		{
			for (int i = 0; i < _vTD.size(); i++)
			{
				PurchaseInvoiceDetail oTD = (PurchaseInvoiceDetail)_vTD.get(i);
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);
				if (oItem.getItemType() == i_INVENTORY_PART) 
				{
					InventoryTransaction oIT = mapPIDetail (_oTR, oTD);
					if(oIT != null)
					{
						save(oIT, oTD);
						
						//update cost per unit in trans det
						oTD.setCostPerUnit(oIT.getCost());
						//TODO: cause problem
						//oTD.setSubTotal(oIT.getTotalCost());
						oTD.save(m_oConn);
					}
				}
			}
			updateGL();
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}	
	private InventoryTransaction mapPIDetail(PurchaseInvoice _oTR, PurchaseInvoiceDetail _oTD)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ("Purchase Invoice From ");
		oDesc.append ( _oTR.getVendorName() );

		//if direct purchase
		if (StringUtil.isEmpty(_oTD.getPurchaseReceiptDetailId()))
		{			
			InventoryTransaction oInv = new InventoryTransaction();
			oInv.setTransactionId	     (_oTR.getPurchaseInvoiceId());	
			oInv.setTransactionNo	     (_oTR.getPurchaseInvoiceNo());				
			oInv.setTransactionDetailId  (_oTD.getPurchaseInvoiceDetailId());		
			oInv.setTransactionType      (i_INV_TRANS_PURCHASE_INVOICE); 
			oInv.setTransactionDate      (_oTR.getPurchaseInvoiceDate());
			oInv.setLocationId           (_oTR.getLocationId());
			oInv.setLocationName         (_oTR.getLocationName());
			oInv.setItemId			     (_oTD.getItemId());
			oInv.setItemCode		     (_oTD.getItemCode());
			oInv.setCost		 		 (_oTD.getCostPerUnit());
			oInv.setQtyChanges	 		 (_oTD.getQtyBase());
			oInv.setPrice		 		 (_oTD.getItemPrice());	    
			oInv.setItemPrice	 		 (ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
			oInv.setDescription 		 (oDesc.toString());			

			if (!StringUtil.isEqual(_oTD.getItemName(),_oTD.getDescription()))
			{
				oInv.setDescription(oInv.getDescription() + "  " + _oTD.getDescription());
			}

			return oInv;
		}
		else //if from receipt 
		{
			InventoryTransaction oPRInv = 
					getByTransDetailID (_oTD.getPurchaseReceiptDetailId(), 
							i_INV_TRANS_PURCHASE_RECEIPT, m_oConn);

			BigDecimal bdTotalCost = _oTD.getCostPerUnit().multiply(_oTD.getQtyBase());
			
			//TODO: test & validate function
			if(log.isDebugEnabled())
			{
				log.debug(" PR Cost Before Update : " + oPRInv.getCost());
				log.debug(" PiUpdatePrCost(TRF): " + m_bUpdatePRCost +
						  " IS PERIOD OPEN:  " + !PeriodTool.isClosed(oPRInv.getTransactionDate(), m_oConn) + 
						  " IS IN TOLERANCE: " + Calculator.isInTolerance(oPRInv.getTotalCost(), bdTotalCost, d_NUMBER_TOLERANCE));
			}

			if (oPRInv != null && m_bUpdatePRCost) 
			{
				//if cost / price changed in PI
				if (!Calculator.isInTolerance(oPRInv.getTotalCost(), bdTotalCost, d_NUMBER_TOLERANCE))
				{
					StringBuilder oRemark = new StringBuilder(oPRInv.getDescription());
					oRemark.append(s_LINE_SEPARATOR).append("PI: ").append(_oTR.getPurchaseInvoiceNo());
					oPRInv.setDescription(oRemark.toString());
					oPRInv.setCost(_oTD.getCostPerUnit());
					return oPRInv;
				}
			}			
		}
		return null;
	}

	//-------------------------------------------------------------------------
	// purchase return
	//-------------------------------------------------------------------------
		
	public void createFromPT (PurchaseReturn _oTR, List _vPTD)
		throws Exception
	{
		m_oTrans = _oTR;
		try 
		{
			for (int i = 0; i < _vPTD.size(); i++)
			{
				PurchaseReturnDetail oTD = (PurchaseReturnDetail) _vPTD.get(i);
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);	 			
				if (oItem.getItemType() == i_INVENTORY_PART && oTD.getQty().doubleValue() > 0) 
				{
					InventoryTransaction oIT = mapPTDetail (_oTR, oTD);
					save(oIT,oTD);
					
					oTD.setItemCost(oIT.getCost());
					oTD.setSubTotalCost(oIT.getCost().multiply(oTD.getQtyBase()));
					oTD.save(m_oConn);
				}
			}
			updateGL();
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private InventoryTransaction mapPTDetail (PurchaseReturn _oTR, PurchaseReturnDetail _oTD)
		throws Exception
	{		
		StringBuilder oDesc = new StringBuilder ("Returned To ");
		oDesc.append (_oTR.getVendorName());
		oDesc.append ("  ");
		if (StringUtil.isNotEmpty(_oTR.getTransactionNo())) 
		{
			oDesc.append(" Trans No : ");
			oDesc.append(_oTR.getTransactionNo());
		}

		InventoryTransaction oInv = new InventoryTransaction ();
		oInv.setTransactionId	    (_oTR.getPurchaseReturnId());	
		oInv.setTransactionNo	    (_oTR.getReturnNo());			
		oInv.setTransactionDetailId (_oTD.getPurchaseReturnDetailId());	
		oInv.setTransactionType     (i_INV_TRANS_PURCHASE_RETURN); 
		oInv.setTransactionDate     (_oTR.getReturnDate());
		oInv.setLocationId          (_oTR.getLocationId());
		oInv.setLocationName        (LocationTool.getLocationNameByID(_oTR.getLocationId()));
		oInv.setItemId			    (_oTD.getItemId());
		oInv.setItemCode		    (_oTD.getItemCode());

		double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(),_oTD.getUnitId(), m_oConn);
		double dQtyChange = _oTD.getQty().doubleValue();
		double dQtyBase = dQtyChange * dBaseValue;

		double dCost = _oTD.getItemCost().doubleValue(); //Cost in PR not in Base Unit		
		dCost = dCost * _oTR.getCurrencyRate().doubleValue(); 
		dCost = InventoryLocationTool.getItemCost(_oTD.getItemId(), _oTR.getLocationId(), _oTR.getReturnDate(), m_oConn).doubleValue();
		log.debug("dItemCost = Current COST " + dCost);

		oInv.setQtyChanges	  (new BigDecimal(dQtyBase * -1));
		oInv.setCost		  (new BigDecimal(dCost));
		oInv.setPrice		  (_oTD.getItemPrice());
		oInv.setItemPrice	  (ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
		oInv.setDescription	  (oDesc.toString() + _oTR.getRemark());
		return oInv;
	}	
	
	//-------------------------------------------------------------------------
	// delivery order
	//-------------------------------------------------------------------------
		
	public void createFromDO (DeliveryOrder _oTR, List _vTD, Map _mGroup)
		throws Exception
	{
		m_oTrans = _oTR;
		try 
		{    
			for (int i = 0; i < _vTD.size(); i++)
			{
				DeliveryOrderDetail oTD = (DeliveryOrderDetail)_vTD.get(i);
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);
				 			
				if (oItem.getItemType() == i_INVENTORY_PART) 
				{
					InventoryTransaction oIT = mapDODetail (_oTR, oTD, null);						
					save(oIT, oTD);
					
					oTD.setCostPerUnit(oIT.getCost());
					oTD.save(m_oConn);
				}
				//if item in sales is grouping
				else if (oItem.getItemType() == i_GROUPING) 
				{
					List vGroup = (List) _mGroup.get(oTD.getItemId());
					BigDecimal bdGroupCost = bd_ZERO;
					for (int j = 0; j < vGroup.size(); j++) 
					{
						ItemGroup oGroupPart = (ItemGroup) vGroup.get(j);
						Item oGroupItem = ItemTool.getItemByID(oGroupPart.getItemId(), m_oConn);
						if(oGroupItem.getItemType() == i_INVENTORY_PART)
						{
							InventoryTransaction oIT = mapDODetail (_oTR, oTD, oGroupPart);
							save(oIT, oTD);
							bdGroupCost = bdGroupCost.add(oIT.getCost());
						}
					}
					oTD.setCostPerUnit(bdGroupCost);
					oTD.save(m_oConn);
				}
			}
			updateGL();
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}
	
	private InventoryTransaction mapDODetail (DeliveryOrder _oTR, DeliveryOrderDetail _oTD, ItemGroup _oGroupPart)
		throws Exception
	{				
		StringBuilder oDesc = new StringBuilder ("Delivery Order To ");
		oDesc.append (_oTR.getCustomerName());

		String sItemID    = _oTD.getItemId();
		String sItemCode  = _oTD.getItemCode();		
		double dBaseUnit  = UnitTool.getBaseValue(_oTD.getItemId(),_oTD.getUnitId(), m_oConn);
		double dQtyChange = _oTD.getQty().doubleValue();
		double dQtyBase   = dQtyChange * dBaseUnit;
		double dCost      = _oTD.getCostPerUnit().doubleValue(); //Cost already in base unit

		if(_oGroupPart != null)
		{
			oDesc.append ( " as Part of Group Item : " );
			oDesc.append ( _oTD.getItemCode() );

			sItemID    = _oGroupPart.getItemId();
			sItemCode  = _oGroupPart.getItemCode();		
			dBaseUnit  = UnitTool.getBaseValue(_oGroupPart.getItemId(), _oGroupPart.getUnitId(), m_oConn);
			dQtyChange = _oGroupPart.getQty().doubleValue();
			dQtyBase   = _oTD.getQty().doubleValue()* dQtyChange * dBaseUnit;	
			dCost 	   = _oGroupPart.getCost().doubleValue();
		}

		InventoryTransaction oInv = new InventoryTransaction ();
		oInv.setTransactionId		 (_oTR.getDeliveryOrderId());	
		oInv.setTransactionNo	     (_oTR.getDeliveryOrderNo());	
		oInv.setTransactionDetailId  (_oTD.getDeliveryOrderDetailId());		       			
		oInv.setTransactionType	 	 (i_INV_TRANS_DELIVERY_ORDER); 
		oInv.setTransactionDate      (_oTR.getDeliveryOrderDate());
		oInv.setLocationId       	 (_oTR.getLocationId());
		oInv.setLocationName     	 (_oTR.getLocationName());
		
		oInv.setItemId				 (sItemID);
		oInv.setItemCode			 (sItemCode);						
		oInv.setQtyChanges			 (new BigDecimal((dQtyBase * -1)));
		oInv.setCost 				 (new BigDecimal(dCost));
		oInv.setPrice				 (_oTD.getItemPriceInSo());
		oInv.setDescription			 (oDesc.toString());
		oInv.setItemPrice	 		 (ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
		return oInv;
	}	

	//-------------------------------------------------------------------------
	// sales invoice
	//-------------------------------------------------------------------------
	
	public void createFromSI (SalesTransaction _oTR, List _vTD, Map _mGroup)
		throws Exception
	{
		m_oTrans = _oTR;
		try 
		{    
			for (int i = 0; i < _vTD.size(); i++)
			{
				SalesTransactionDetail oTD = (SalesTransactionDetail)_vTD.get(i);
				//if not from DO || direct import from SO
				if(StringUtil.isEmpty(oTD.getDeliveryOrderDetailId()) || 
					(StringUtil.isNotEmpty(oTD.getDeliveryOrderDetailId()) && 
						StringUtil.isEqual(oTD.getDeliveryOrderId(),oTD.getSalesOrderId())) )
				{
					Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);				 		
					if (oItem.getItemType() == i_INVENTORY_PART) 
					{
						InventoryTransaction oIT = mapSIDetail (_oTR, oTD, null);							
						save(oIT,oTD);
						oTD.setItemCost(oIT.getCost());
						oTD.setSubTotalCost(oIT.getCost().multiply(oTD.getQtyBase()));
						oTD.save(m_oConn);
					}
					//if item in sales is grouping
					else if (oItem.getItemType() == i_GROUPING) 
					{
						BigDecimal bdGroupCost = bd_ZERO;
						List vGroup = (List) _mGroup.get(oTD.getItemId());
						for (int j = 0; j < vGroup.size(); j++) 
						{
							ItemGroup oGroupPart = (ItemGroup) vGroup.get (j);
							Item oGroupItem = ItemTool.getItemByID(oGroupPart.getItemId(), m_oConn);
							if(oGroupItem.getItemType() == i_INVENTORY_PART)
							{
								InventoryTransaction oIT = mapSIDetail (_oTR, oTD, oGroupPart);
								save(oIT,oTD);
								bdGroupCost = bdGroupCost.add(oIT.getCost());
							}
						}
						oTD.setItemCost(bdGroupCost);
						oTD.setSubTotalCost(bdGroupCost.multiply(oTD.getQtyBase()));
						oTD.save(m_oConn);
					}
				}
				updateGL();
			}
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}		
	}

	private InventoryTransaction mapSIDetail (SalesTransaction _oTR, SalesTransactionDetail _oTD, ItemGroup _oGroupPart)
		throws Exception
	{
		String sItemID    = _oTD.getItemId();
		String sItemCode  = _oTD.getItemCode();		
		double dBaseUnit  = UnitTool.getBaseValue(_oTD.getItemId(),_oTD.getUnitId(), m_oConn);
		double dQtyChange = _oTD.getQty().doubleValue();
		double dQtyBase   = dQtyChange * dBaseUnit;
		double dCost      = _oTD.getItemCost().doubleValue(); //Cost already in base unit
		
		String sDesc = "Sold To ";
		//support return by fill in with -qty in POS
		if (dQtyBase < 0)
		{
			sDesc = "Return From ";
		}
		StringBuilder oDesc = new StringBuilder(sDesc);
		oDesc.append(_oTR.getCustomerName());
		if(dQtyBase < 0)
		{
			oDesc.append(" ").append(_oTR.getRemark());
		}
		
		if(_oGroupPart != null)
		{
			oDesc.append ( " as Part of Group Item : " );
			oDesc.append ( _oTD.getItemCode() );

			sItemID    = _oGroupPart.getItemId();
			sItemCode  = _oGroupPart.getItemCode();		
			dBaseUnit  = UnitTool.getBaseValue(_oGroupPart.getItemId(), _oGroupPart.getUnitId(), m_oConn);
			dQtyChange = _oGroupPart.getQty().doubleValue();
			dQtyBase   = _oTD.getQty().doubleValue()* dQtyChange * dBaseUnit;	
			dCost 	   = _oGroupPart.getCost().doubleValue();
		}

		InventoryTransaction oInv = new InventoryTransaction ();
		oInv.setTransactionId		 (_oTR.getSalesTransactionId());	
		oInv.setTransactionNo	     (_oTR.getInvoiceNo());	
		oInv.setTransactionDetailId  (_oTD.getSalesTransactionDetailId());		       			
		oInv.setTransactionType	 	 (i_INV_TRANS_SALES_INVOICE); 
		oInv.setTransactionDate      (_oTR.getTransactionDate());
		oInv.setLocationId       	 (_oTR.getLocationId());
		oInv.setLocationName     	 (_oTR.getLocationName());
		
		oInv.setItemId				 (sItemID);
		oInv.setItemCode			 (sItemCode);						
		oInv.setQtyChanges			 (new BigDecimal((dQtyBase * - 1)));
		oInv.setCost 				 (new BigDecimal(dCost));
		oInv.setPrice				 (_oTD.getItemPrice());
		oInv.setDescription			 (oDesc.toString());
		oInv.setItemPrice	 		 (ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
		return oInv;		
	}		

	//-------------------------------------------------------------------------
	// sales return
	//-------------------------------------------------------------------------
	
	public void createFromSR (SalesReturn _oTR, List _vTD, Map _mGroup)
		throws Exception
	{
		m_oTrans = _oTR;
		try
		{
			for (int i = 0; i < _vTD.size(); i++)
			{
				SalesReturnDetail oTD = (SalesReturnDetail)_vTD.get(i);
				Item oItem = ItemTool.getItemByID(oTD.getItemId(), m_oConn);
				
				if (oItem.getItemType() == i_INVENTORY_PART) 
				{
					InventoryTransaction oIT = mapSRDetail (_oTR, oTD, null);
					save(oIT, oTD);
					oTD.setItemCost(oIT.getCost());
					oTD.setSubTotalCost(oIT.getCost().multiply(oTD.getQtyBase()));
					oTD.save(m_oConn);
				}
				//if item in sales is grouping
				else if (oItem.getItemType() == i_GROUPING) 
				{
					List vGroup = (List) _mGroup.get(oTD.getItemId());
					for (int j = 0; j < vGroup.size(); j++) 
					{
						ItemGroup oGroupPart = (ItemGroup) vGroup.get (j);
						Item oGroupItem = ItemTool.getItemByID(oGroupPart.getItemId(), m_oConn);
						if(oGroupItem.getItemType() == i_INVENTORY_PART)
						{
							InventoryTransaction oIT = mapSRDetail (_oTR, oTD, oGroupPart);
							save(oIT, oTD);
						}
					}
				}
			}
			updateGL();
		}
		catch (Exception _oEx) 
		{
			throw new NestableException (s_SAVE_FAILED + _oEx.getMessage(), _oEx);
		}
	}	
	
	private InventoryTransaction mapSRDetail (SalesReturn _oTR, SalesReturnDetail _oTD, ItemGroup _oGroupPart)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ("Return From ");
		oDesc.append (_oTR.getCustomerName());

		String sItemID    = _oTD.getItemId();
		String sItemCode  = _oTD.getItemCode();		
		double dBaseUnit  = UnitTool.getBaseValue(_oTD.getItemId(),_oTD.getUnitId(), m_oConn);
		double dQtyChange = _oTD.getQty().doubleValue();
		double dQtyBase   = dQtyChange * dBaseUnit;		
		double dCost      = _oTD.getItemCost().doubleValue(); //Cost already in base unit
		
		if(_oGroupPart != null)
		{
			oDesc.append ( " as Part of Group Item : " );
			oDesc.append ( _oTD.getItemCode() );

			sItemID    = _oGroupPart.getItemId();
			sItemCode  = _oGroupPart.getItemCode();		
			dBaseUnit  = UnitTool.getBaseValue(_oGroupPart.getItemId(), _oGroupPart.getUnitId(), m_oConn);
			dQtyChange = _oGroupPart.getQty().doubleValue();
			dQtyBase   = _oTD.getQty().doubleValue()* dQtyChange * dBaseUnit;	
			dCost 	   = _oGroupPart.getCost().doubleValue();
		}

		//if this item never been in transaction before, get from current cost / last purchase
		if(StringUtil.isEmpty(_oTR.getTransactionId()) && dCost <= 0) 
		{
			//cost is already cost per unit
			dCost  = InventoryLocationTool.getItemCost(sItemID, _oTR.getLocationId(), _oTR.getReturnDate(), m_oConn).doubleValue();				
			if (dCost == 0) //if still zero 
			{
				Item oItem = ItemTool.getItemByID(sItemID, m_oConn);
				dCost = Calculator.mul(oItem.getLastPurchasePrice(), oItem.getLastPurchaseRate()); 
			}
		}

		InventoryTransaction oInv = new InventoryTransaction ();
		oInv.setTransactionId		 (_oTR.getSalesReturnId());	
		oInv.setTransactionNo	     (_oTR.getReturnNo());	
		oInv.setTransactionDetailId  (_oTD.getSalesReturnDetailId());		       			
		oInv.setTransactionType	 	 (i_INV_TRANS_SALES_RETURN); 
		oInv.setTransactionDate      (_oTR.getReturnDate());
		oInv.setLocationId       	 (_oTR.getLocationId());
		oInv.setLocationName     	 (_oTR.getLocationName());
		
		oInv.setItemId				 (sItemID);
		oInv.setItemCode			 (sItemCode);						
		oInv.setQtyChanges			 (new BigDecimal(dQtyBase));
		oInv.setCost 				 (new BigDecimal(dCost));
		oInv.setPrice				 (_oTD.getItemPrice());
		oInv.setDescription			 (oDesc.toString());
		oInv.setItemPrice	 		 (ItemTool.getPriceExclTax(_oTD.getItemId(), m_oConn));
		return oInv;
	}			
		
	/**
	 * validate and save invTrans created by trans save
	 * 
	 * @param _oInv
	 * @param _oBT
	 * @throws Exception
	 */
	private void save (InventoryTransaction _oInv, InventoryDetailOM _oBT)
		throws Exception
	{		
		Date dNow = new Date();
		//Generate Sys ID
		boolean bNewInvTrans = false; 
		if (StringUtil.isEmpty(_oInv.getInventoryTransactionId()))
		{
			_oInv.setInventoryTransactionId ( IDGenerator.generateSysID() );
			_oInv.setCreateDate(dNow);
			bNewInvTrans = true; 
		}
		//validate exists ID to prevent duplicate transaction
		validateExists(_oInv, m_oConn);
		
		_oInv.setUpdateDate(dNow);
		_oInv.save(m_oConn);
		
		StopWatch oSW = null;		
		String sSWKey = null;
		if (log.isInfoEnabled())
		{
			sSWKey = "Recalculate " + _oInv.getTransactionNo() + ", Item : " + _oInv.getItemCode(); 
			oSW = new StopWatch();
			oSW.start(sSWKey);
		}
		
		try
		{
			recalculate(_oInv, bNewInvTrans);
		}
		catch (Exception _oEx) 
		{
			throw new Exception("ERROR recalculating inventory for Item : " + _oInv.getItemCode() + " " + _oEx.getMessage(), _oEx);
		}
		
		if (log.isInfoEnabled())
		{
			oSW.stop(sSWKey);
			log.debug(oSW.result(sSWKey));
		}
		
		//batch number process
		if (PreferenceTool.useBatchNo() && _oBT != null)
		{
			BatchTransactionTool.saveBatch(_oBT.getBatchTransaction(), _oInv, m_oConn);
		}
		
		//serial number process
		if (PreferenceTool.useSerialNo() && _oBT != null)
		{
			ItemSerialTool.saveSerial(_oBT.getSerialTrans(), _oInv, m_oConn);
		}
	}
	
	
	
	/**
	 * Query for any next invTrans after current invTrans and recalculate qty and value balance 
	 * if next transactions exist
	 * Update inventory location for last cost data 
	 * 
	 * @param _oInv
	 * @param _bNewInvTrans
	 * @throws Exception
	 */
	private void recalculate(InventoryTransaction _oInv, boolean _bNewInvTrans)
		throws Exception
	{
		if (log.isInfoEnabled()) 
		{
			log.info("** Processing Item " + _oInv.getItemCode() + " Trans : " + _oInv.getTransactionNo()); 
		}
		
		//get 1 previous inv trans which the date is just before this trans
		InventoryTransaction oLast = getLastTrans(_oInv, m_oConn);

		if (oLast != null)
		{
			//check the prev trans whether the value balance if correct not = 0
			//if not then start from the beginning for this item_id & location_id
			//if (!isValid(oLast))
			//{
			//	updatePrevTrans (_oInv, oLast, _oConn);
			//}
			updateValue (_oInv, oLast);
		}
		else
		{			
			//no last trans this should be the first trans
			updateValue (_oInv, null);			
		}
		
		//get list of the next invTrans after this one sort by date -> be careful of out of memory
		//update each next invTrans total_cost qty_balance value_balance accordingly
		//use greater than if new, greater equal if update
		oLast = _oInv;
		List vNextTrans = getAllNextTrans(_oInv, _bNewInvTrans, m_oConn); 
		if (log.isDebugEnabled()) 
		{
			log.debug("\n** START Recalculating " + vNextTrans.size() + " next trans ");
		}		
		
		for (int i = 0; i < vNextTrans.size(); i++)
		{
			InventoryTransaction oNext = (InventoryTransaction) vNextTrans.get(i);			
			updateValue (oNext, oLast);
			oLast = oNext;
		}
		
		if (log.isDebugEnabled()) 
		{
			log.debug("\n** END Recalculate, Last trans update InvLOC : \n" + oLast);
		}
		
		//clear to prevent decimal problem
		//oLast.setValueBalance(oLast.getValueBalance().setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));
		//oLast.setQtyBalance(oLast.getQtyBalance().setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));
		
		if (b_GLOBAL_COSTING)
		{
			//before update inventory location, get last trans for this certain location
			InventoryLocationTool.updateGlobal(_oInv, oLast, _bNewInvTrans, false, m_oConn);
		}
		else
		{			
			InventoryLocationTool.update(oLast, m_oConn);
		}
	}
	
	
	/**
	 * Update Qty Balance and Value Balance in Current Inventory Transaction Record
	 * Check if current transaction type need cost update from previous transaction
	 * DO, SI, SR, PT and Non Cost Adjustment IR cost will be updated from prev trans
	 * 
	 * @param _oInv
	 * @param _oPrev
	 * @throws Exception
	 */
	private void updateValue(InventoryTransaction _oInv, InventoryTransaction _oPrev)
		throws Exception
	{		
		if (_oPrev == null) //if no previous value just set totalCost, qtyBalance, valueBalance using its own value
		{			
			double dQtyChanges = _oInv.getQtyChanges().doubleValue();
			double dCost = _oInv.getCost().doubleValue();
			double dValueBalance = _oInv.getTotalCost().doubleValue();
            
			//validate temporary qty balance to not allow minus at all 
			validateQtyBalance(dQtyChanges, dCost, dValueBalance, _oInv, _oPrev, m_oConn);
			
			_oInv.setQtyChanges(_oInv.getQtyChanges().setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));
			_oInv.setTotalCost(_oInv.getQtyChanges().multiply(_oInv.getCost()));
			_oInv.setQtyBalance(_oInv.getQtyChanges());
			_oInv.setValueBalance(_oInv.getTotalCost());
			_oInv.setUpdateDate(new Date());
			_oInv.save(m_oConn);					
			
			if (log.isDebugEnabled())
			{
				log.debug("\n No Prev, InvTrans After updateValue : " + _oInv);
			}
		}
		else
		{
			boolean bIRCostAdj = false;
			int iType = _oInv.getTransactionType();
			
			//update sales, purchase return and non cost adjustment IR in HO from previous InvTrans cost
			//if(PreferenceTool.getLocationFunction() == i_HEAD_OFFICE)
			//{				
				//update sales trans cost
				if (iType == i_INV_TRANS_SALES_INVOICE || iType == i_INV_TRANS_DELIVERY_ORDER  ||
					iType == i_INV_TRANS_SALES_RETURN  || iType == i_INV_TRANS_PURCHASE_RETURN)
				{
					updateTrans (_oInv, _oPrev);				
				} 
				
				//update non cost adj IR 
				if (iType == i_INV_TRANS_RECEIPT_UNPLANNED || 
					iType == i_INV_TRANS_ISSUE_UNPLANNED) 
				{
					IssueReceipt oIR = IssueReceiptTool.getHeaderByID(_oInv.getTransactionId(), m_oConn);
					if(oIR != null && !oIR.getCostAdjustment())
					{
						_oInv.setTrans(oIR);
						updateTrans (_oInv, _oPrev);
					}
				}
			//}
			
			//update value process based on prev invTrans
			double dPrevQtyBalance = _oPrev.getQtyBalance().doubleValue();
			double dPrevValueBalance = _oPrev.getValueBalance().doubleValue();
			
			//for out transaction updated in trans above, current _oInv.getCost() value			
			//has been altered by the prev value balance / qty balance value

			_oInv.setCost(_oInv.getCost().setScale(4, BigDecimal.ROUND_HALF_UP));
			_oInv.setQtyChanges(_oInv.getQtyChanges().setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));
			_oInv.setTotalCost(_oInv.getQtyChanges().multiply(_oInv.getCost()));
			
			double dQtyChanges = _oInv.getQtyChanges().doubleValue();			
			double dTotalCost = _oInv.getTotalCost().doubleValue();
			double dQtyBalance = dPrevQtyBalance + dQtyChanges;
			double dValueBalance = dPrevValueBalance + dTotalCost;
			
			//TODO: Find out how to handle minus qty
			//if prev qty balance is minus make some adjustment to correct current value balance
			if ((iType == i_INV_TRANS_PURCHASE_RECEIPT || 
				 iType == i_INV_TRANS_PURCHASE_INVOICE ||
				 iType == i_INV_TRANS_TRANSFER_IN      || bIRCostAdj) && dPrevQtyBalance < 0)
			{
				if (DateUtil.isBackDate(_oInv.getTransactionDate()))
				{
					if (dQtyChanges > (dPrevQtyBalance * -1)) //only re-avg cost if qty changes > previous qty balance
					{
						//TODO: CHECK THIS METHOD, THIS METHOD ASSUME PREVIOUS COST IS CORRECT
						if (dPrevValueBalance < 0) dPrevValueBalance = dPrevValueBalance * -1; //set prev value bal to +
						if (dPrevQtyBalance < 0) dPrevQtyBalance = dPrevQtyBalance * -1; //set prev qty to +
						double dTmpQty = (dPrevQtyBalance + dQtyChanges) ;
						if (dTmpQty != 0)
						{						
							dValueBalance = ((dPrevValueBalance + dTotalCost) / dTmpQty) * dQtyBalance;
						}
					}
				}
			}
			
			//validate temporary qty balance to not allow minus at all 
			validateQtyBalance(dQtyBalance, _oInv.getCost().doubleValue(), dValueBalance, _oInv, _oPrev, m_oConn);
			
			//validate against nan and infinity
			if (dValueBalance == Double.NaN || 
				dValueBalance == Double.NEGATIVE_INFINITY || 
				dValueBalance == Double.POSITIVE_INFINITY)
			{
				log.warn("WARNING: Value Balance is NaN / Infinity \nInvTrans : " + 
						 _oInv + "\nPrev : " + _oPrev);
				dValueBalance = 0;
			}
			
			_oInv.setTotalCost   (new BigDecimal(dTotalCost).setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));
			_oInv.setQtyBalance  (new BigDecimal(dQtyBalance).setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));
			_oInv.setValueBalance(new BigDecimal(dValueBalance).setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP));						
			_oInv.setUpdateDate  (new Date());
			_oInv.save(m_oConn);
			
			if (log.isDebugEnabled())
			{
				log.debug("\n Prev Exist, InvTrans after updateValue : " + _oInv);
			}
		}
	}		

	
	/**
	 * Update Cost in Current Inventory Transaction Record, from previous trans
	 * Check if cost value is updated and record transaction as need GL rejournal / updates
	 * DO, SI, SR, PT and Non Cost Adjustment IR cost will be updated from prev trans
	 * 
	 * @param _oInv
	 * @param _oPrev
	 * @throws Exception
	 */	
	private void updateTrans (InventoryTransaction _oInv, InventoryTransaction _oPrev)
		throws Exception
	{
		//double dCost = 0;		
		BigDecimal bdCost = bd_ZERO;
		
		//TODO: Check this  getCost from previous invTrans
		//prevent NaN if prev qty balance 0 then get prev cost directly
		BigDecimal bdQtyBalance = _oPrev.getQtyBalance().setScale(i_INVENTORY_COMMA_SCALE, BigDecimal.ROUND_HALF_UP);
		if (bdQtyBalance.compareTo(bd_ZERO) == 0) 
		{
			bdCost = _oPrev.getCost();
		}
		else //on normal condition the cost will be calculated using prev value balance/qty balance
		{						
			if(bdQtyBalance.doubleValue() < 0.05) {
				log.warn("Qty Balance < safe treshold (0.05): " + _oPrev.getItemCode() + 
						 " Loc " + _oPrev.getLocationName() + " Trans: " + _oPrev.getTransactionNo());
			}
			System.out.println("Qty Balance " + bdQtyBalance);
			bdCost = _oPrev.getValueBalance().divide(bdQtyBalance, 4, BigDecimal.ROUND_HALF_UP);
		}
				
		//calculate cost difference between old cost and new cost for GL journal update 
		//TODO: not working on minus qty
		BigDecimal bdOldCost = _oInv.getCost();
		BigDecimal bdQty = _oInv.getQtyChanges();
		if (bdQty.doubleValue() < 0) bdQty = bdQty.multiply(new BigDecimal(-1));
				
		BigDecimal bdSubTotal = bdCost.multiply(bdQty);
		BigDecimal bdOldSubTotal = bdOldCost.multiply(bdQty);
		
		//double dDelta =  bdCost.min(bdOldCost).doubleValue();
		//double dSTDelta = bdSubTotal.min(bdOldSubTotal).doubleValue();
		
		//check if cost updating really needed
		boolean bUpdateCost = false;
		if (bdCost.compareTo(bdOldCost) != 0 &&
		    bdSubTotal.compareTo(bdOldSubTotal) != 0 &&				
		    !PeriodTool.isClosed(_oInv.getTransactionDate(), m_oConn)) 
		{
			bUpdateCost = true;
		}
		
		if (bUpdateCost) 
		{
			//detail table cost will be invoked by trigger in DB
			InvTransUpdate oITU = new InvTransUpdate(_oInv, bdOldSubTotal.doubleValue(), bdSubTotal.doubleValue(), m_oTrans, m_oConn);
			m_UpdatedTrans.add(oITU);
			
			//for DO if already invoiced will have SI linked trans to update GL
			if (oITU.getLinkedTrans() !=  null) 
			{
				m_UpdatedTrans.add(oITU.getLinkedTrans());
			}

			if (log.isDebugEnabled())
			{
				log.debug("\nTrans " + _oInv.getTransactionNo() + " Item " + _oInv.getItemCode() +
						  " Old Cost: " + bdOldCost + " New Cost: " + bdCost);
			}						
			
			//update this inv trans cost
			_oInv.setCost(bdCost);			
		}//end if cost updating needed		
	}

	/**
	 * delete all inv trans related to this trans
	 * 
	 * @param _sTransID
	 * @param _iTransType
	 * @throws Exception
	 */
	public void delete(InventoryMasterOM _oTx, String _sTransID, int _iTransType)
		throws Exception
	{
		m_oTrans = _oTx;
		List vInvTrans = getByTransID(_sTransID, _iTransType, m_oConn);
		for (int i = 0; i < vInvTrans.size(); i++)
		{
			InventoryTransaction oInv = (InventoryTransaction) vInvTrans.get(i);
			delete (oInv);		
		}	
	}
	    
    /**
     * delete all inv trans related to this trans
     * 
     * @param _sTransID
     * @param _sTransNo
     * @param _iTransType
     * @throws Exception
     */
    public void delete(String _sTransID, String _sTransNo, int _iTransType)
        throws Exception
    {
        List vInvTrans = getByTransID(_sTransID, _sTransNo, _iTransType, m_oConn);        
        for (int i = 0; i < vInvTrans.size(); i++)
        {
            InventoryTransaction oInv = (InventoryTransaction) vInvTrans.get(i);
            delete (oInv);
        }
    }       

	/**
	 * delete an inventory transaction entry
	 * WARNING: delete should be private, opened as public to support partial cancel
	 * 
	 * @param _oInv invTrans to delete
	 * @param _oConn
	 * @throws Exception
	 */
	public void delete(InventoryTransaction _oInv)
		throws Exception
	{
		if (log.isDebugEnabled()) log.debug("\n** START Delete Trans " + _oInv);
				
		//get 1 previous inv trans which the date is just before this trans
		InventoryTransaction oLast = getLastTrans(_oInv, m_oConn);
		
		//get next trans which greater equal because the trans deleted might be the first and
		//there are next trans which date is equal to the trans deleted
		List vNextTrans =  getAllNextTrans(_oInv, false, m_oConn); 
		
		//if no previous trans, then set the first of the next trans to be the last trans
		if (oLast == null)
		{
			if (vNextTrans.size() > 0)
			{
				oLast = (InventoryTransaction) vNextTrans.get(0);
				vNextTrans.remove(0);

				//update this first next trans also, because the cost might be differs from the prev
				updateValue (oLast, null); 

				if (log.isDebugEnabled())  log.debug("\nDelete, no last, first next trans (after update value) : " + oLast);
			}
		}

		log.debug("Delete, next trans to be updated : " + vNextTrans);

		//recalculate all next trans based on the last trans
		for (int i = 0; i < vNextTrans.size(); i++)
		{
			InventoryTransaction oNext = (InventoryTransaction) vNextTrans.get(i);
			log.debug("Processing : " + oNext);

			updateValue (oNext, oLast);
			oLast = oNext;
		}

		log.debug("** END Delete, Last trans that will update Inventory Location : \n : " + oLast);

		if (oLast != null)
		{
			if (b_GLOBAL_COSTING)
			{
				//before update inventory location, get last trans for this certain location
				InventoryLocationTool.updateGlobal(_oInv, oLast, false, true, m_oConn);
			}
			else
			{
				InventoryLocationTool.update(oLast, m_oConn);
			}		
		}
		else
		{
			//this is the first inv trans and no next trans so just delete the inv loc
			InventoryLocationTool.delete(_oInv, m_oConn);			
		}
		
		//delete the intended inv trans
		InventoryTransactionPeer.doDelete(
			new Criteria().add(InventoryTransactionPeer.INVENTORY_TRANSACTION_ID, 
				_oInv.getInventoryTransactionId()), m_oConn);
		
		updateGL();
		
		//delete batch
		if (PreferenceTool.useBatchNo())
		{
			BatchTransactionTool.deleteBatch(_oInv, m_oConn);
		}
		
		if (PreferenceTool.useSerialNo())
		{
			ItemSerialTool.deleteSerial(_oInv, m_oConn);
		}
	}	

	/**
	 * temporary method only used for fixing if (Calculator.isInTolerance(dDiff, 0, d_NUMBER_TOLERANCE))
	 * in updateGL, next mUpdatedTrans should always come from inventory trans recalculation
	 * 
	 * @throws Exception
	 */
	public void setUpdatedTrans(List _vUpdated)
		throws Exception
	{
		m_UpdatedTrans = _vUpdated;
	}
	
	/**
	 * Update GL trans per transId based on InvTransUpdate, 
	 * should be private only called from InvCostingTool 
	 * 
	 * @throws Exception
	 */
	public void updateGL()
		throws Exception
	{
		if(m_UpdatedTrans != null && m_UpdatedTrans.size() > 0)
		{
			log.info("Update GL, Total Updated Trans:" + m_UpdatedTrans.size());
			List vDisByType = BeanUtil.getDistinctListByField(m_UpdatedTrans, "txType");
			for (int j = 0; j < vDisByType.size(); j++)
			{
				InvTransUpdate oITUDisType = (InvTransUpdate) vDisByType.get(j);
				List vFilByType = BeanUtil.filterListByFieldValue(m_UpdatedTrans, "txType", oITUDisType.getTxType());
				List vDisByID = BeanUtil.getDistinctListByField(vFilByType, "transId");
				for (int k = 0; k < vDisByID.size(); k++)
				{
					InvTransUpdate oITUDisID = (InvTransUpdate) vDisByID.get(k);
					if(oITUDisID.getTxType() == i_INV_TRANS_ISSUE_UNPLANNED || 
					   oITUDisID.getTxType() == i_INV_TRANS_RECEIPT_UNPLANNED)
					{
						IssueReceipt oIR = IssueReceiptTool.getHeaderByID(oITUDisID.getTransId(), m_oConn);
						InventoryJournalTool oJT = new InventoryJournalTool(oIR, m_oConn);
						oJT.updateIRJournal(oIR);
					}
					else if(oITUDisID.getTxType() == i_INV_TRANS_PURCHASE_RETURN)
					{
						PurchaseReturn oPT = PurchaseReturnTool.getHeaderByID(oITUDisID.getTransId(), m_oConn);
						PurchaseReturnJournalTool oJT = new PurchaseReturnJournalTool(oPT, m_oConn);
						oJT.updatePTJournal(oPT);
					}					
					else if(oITUDisID.getTxType() == i_INV_TRANS_DELIVERY_ORDER || 
							oITUDisID.getTxType() == i_INV_TRANS_SALES_INVOICE  ||
							oITUDisID.getTxType() == i_INV_TRANS_SALES_RETURN)
					{
						List vFilByID = BeanUtil.filterListByFieldValue(vFilByType, "transId", oITUDisID.getTransId());
						Map mTxID = new HashMap();
						for (int l = 0; l < vFilByID.size(); l++)							
						{
							InvTransUpdate oITUTxID = (InvTransUpdate) vFilByID.get(l);
							String sKey = oITUTxID.getInvAccount() + "," + oITUTxID.getOthAccount();
							TemplateUtil.addToMap(mTxID, sKey,oITUTxID.getDifference());
						}
						
						Set vKey = mTxID.keySet();					
						Iterator iter = vKey.iterator();
						while(iter.hasNext())
						{
							String sKey = (String)iter.next();
							String[] sKeys = StringUtil.split(sKey,",");
							if(sKeys != null && sKeys.length == 2)
							{
								String sInvAcc = sKeys[0];
								String sOthAcc = sKeys[1];
								double dDiff = (Double) mTxID.get(sKey);	
								if (dDiff != 0 && !Calculator.isInTolerance(dDiff, 0, d_NUMBER_TOLERANCE))
								{
									GlTransactionTool.updateSalesGLTrans(
										oITUDisID.getGlType(), oITUDisID.getTransId(), 
											oITUDisID.getTransNo(), sInvAcc, sOthAcc, dDiff, m_oConn);
								}
							}
						}
					}
				}
			}
		}
	}
}

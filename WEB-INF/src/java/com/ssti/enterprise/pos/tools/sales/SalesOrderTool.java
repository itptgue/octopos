package com.ssti.enterprise.pos.tools.sales;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.om.SalesOrderDetailPeer;
import com.ssti.enterprise.pos.om.SalesOrderPeer;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionDetailPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for SalesOrder OM
 * @see SalesOrder, SalesOrderPeer, SalesOrderDetail, SalesOrderDetailPeer
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesOrderTool.java,v 1.23 2009/05/04 02:05:18 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesOrderTool.java,v $
 * 
 * 2015-11-25 findData - add status 99 for status query ordered / delivered, templates: SalesOrderView.vm
 * 2015-07-27 countSubTotal - now recalculate user discount input
 * 2015-02-24 processSave - set DepartmentID from Salesman
 * 2015-06-15 findTrans - Add override sSalesID
 * </pre><br>
 */
public class SalesOrderTool extends BaseTool
{
	private static Log log = LogFactory.getLog(SalesOrderTool.class);

	public static final String s_ITEM = new StringBuilder(SalesOrderPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.ITEM_ID).toString();
	
	public static final String s_TAX = new StringBuilder(SalesOrderPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.SALES_ORDER_ID).append(",")
	.append(SalesOrderDetailPeer.TAX_ID).toString();
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), SalesOrderPeer.SALES_ORDER_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), SalesOrderPeer.REMARK);                  
		m_FIND_PEER.put (Integer.valueOf(i_CUSTOMER), 	 SalesOrderPeer.CUSTOMER_ID);   
		m_FIND_PEER.put (Integer.valueOf(i_CUST_TYPE  ), SalesOrderPeer.CUSTOMER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), SalesOrderPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), SalesOrderPeer.USER_NAME);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), SalesOrderPeer.REFERENCE_NO);             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), SalesOrderPeer.LOCATION_ID);             
		m_FIND_PEER.put (Integer.valueOf(i_SALESMAN   ), SalesOrderPeer.SALES_ID);             
				                        
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), SalesOrderPeer.PAYMENT_TYPE_ID);
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), SalesOrderPeer.PAYMENT_TERM_ID);
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), SalesOrderPeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), SalesOrderPeer.FOB_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), SalesOrderPeer.CURRENCY_ID);
		m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), SalesOrderPeer.BANK_ID);
		
		addInv(m_FIND_PEER, s_ITEM);
	}   

	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), SalesOrderPeer.TRANSACTION_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), SalesOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), SalesOrderPeer.TRANSACTION_STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), SalesOrderPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), SalesOrderPeer.USER_NAME);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), SalesOrderPeer.USER_NAME);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), SalesOrderPeer.PAYMENT_TYPE_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), SalesOrderPeer.CURRENCY_ID);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALESMAN   ), SalesOrderPeer.SALES_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), SalesOrderPeer.CUSTOMER_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), SalesOrderPeer.LOCATION_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_SALES_ITEM ), SalesOrderPeer.SALES_ID); 
	}
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, _iSelected);}
	
	public static SalesOrder getHeaderByID(String _sID)
		throws Exception
	{
		return getHeaderByID (_sID, null);
	}

	/**
	 * get header by id
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return SalesOrder object
	 * @throws Exception
	 */
	public static SalesOrder getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesOrderPeer.SALES_ORDER_ID, _sID);
        List vData = SalesOrderPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (SalesOrder) vData.get(0);
		}
		return null;
	}
	
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
        return getDetailsByID(_sID,null);
    }

	/**
	 * get details by id
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of SalesOrderDetail
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID,Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesOrderDetailPeer.SALES_ORDER_ID, _sID);
        oCrit.addAscendingOrderByColumn(SalesOrderDetailPeer.INDEX_NO);	    
        return SalesOrderDetailPeer.doSelect(oCrit,_oConn);
	}
	
    protected static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesOrderDetailPeer.SALES_ORDER_ID, _sID);
		SalesOrderDetailPeer.doDelete (oCrit, _oConn);
	}
	
	public static String getSONoByID(String _sID)
    	throws Exception
    {
		SalesOrder oSO = getHeaderByID (_sID);
		if (oSO != null) return oSO.getSalesOrderNo();
		return "";
	}

	public static SalesOrderDetail getDetailByDetailID(String _sID)
		throws Exception
	{
		return getDetailByDetailID(_sID, null);
	}
	
	/**
	 * get SODetail by Detail ID
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return SalesOrderDetailObject
	 * @throws Exception
	 */
	public static SalesOrderDetail getDetailByDetailID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(SalesOrderDetailPeer.SALES_ORDER_DETAIL_ID, _sID);
		List vData = SalesOrderDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) {
			return ((SalesOrderDetail)vData.get(0));
    	}
    	return null;
	}

	/**
	 * find SO by customer and status
	 * 
	 * @param _sCustomerID
	 * @param _iStatus
	 * @return List of SalesOrder 
	 * @throws Exception
	 */
	public static List getSOByStatusAndCustomerID (String _sCustomerID, int _iStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SalesOrderPeer.CUSTOMER_ID, _sCustomerID);
        oCrit.add(SalesOrderPeer.TRANSACTION_STATUS, _iStatus);
        return SalesOrderPeer.doSelect(oCrit);
	}	
	
	//DO RELATED
	/**
	 * update so detail delivered qty value from delivery order detail
	 * method used both for save and cancel
	 * 
	 * @param _vDODet
	 * @param _sSOID
	 * @param _bCloseSO
	 * @param _bCancelDO
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateSOFromDO(List _vDODet, 
									 String _sSOID, 
									 boolean _bCloseSO,
									 boolean _bCancelDO, 
									 Connection _oConn)
        throws Exception
    {
        try
        {
            for(int i = 0; i < _vDODet.size(); i++)
		    {
		    	DeliveryOrderDetail oDODet = (DeliveryOrderDetail) _vDODet.get(i);
		    	if(StringUtil.isNotEmpty(oDODet.getSalesOrderDetailId()))
		    	{
		    		SalesOrderDetail oSODet = getDetailByDetailID(oDODet.getSalesOrderDetailId(), _oConn);
		    		if (oSODet != null)
		    		{
    		    		double dSOQty = oSODet.getQty().doubleValue();
    		    		double dSODeliveredQty = oSODet.getDeliveredQty().doubleValue();
    		    		double dDOQty = oDODet.getQty().doubleValue();
    		    		
    		    		//if cancel DO
    		    		if(_bCancelDO)dDOQty = dDOQty * -1;
    		    		
    		    		double dAfterDelivered = dSODeliveredQty + dDOQty;
    		    		if(!_bCancelDO)
    		    		{
    			    		if(dSOQty > (dAfterDelivered) ){
    			    			oSODet.setDeliveredQty(new BigDecimal(dAfterDelivered));
    			    		}
    			    		else {
    			    			oSODet.setDeliveredQty(new BigDecimal(dSOQty));
    			    		}//end if
    		    		}
    		    		else
    		    		{
    		    			if(dAfterDelivered < 0){
       		    				oSODet.setDeliveredQty(bd_ZERO);
    		    			}
    		    			else {
       		    				oSODet.setDeliveredQty(new BigDecimal(dAfterDelivered));
    		    			}
    		    		}
    		    		oSODet.save(_oConn);
		    	    }
		    	}//end if

            }//end for
            
            if(StringUtil.isNotEmpty(_sSOID))
            {
            	updateStatusFromDeliveredItem(_sSOID,_oConn);   
                if(_bCloseSO)
                {
                	closeSO(_sSOID,_oConn);
                }
            }
        }
        catch(Exception _oEx)
        {
            String sMessage = "Update SO from DO Failed ERROR : " + _oEx.getMessage();
            log.error(_oEx);
            _oEx.printStackTrace();
            throw new NestableException(sMessage, _oEx);
        }
    }

	//SI RELATED
	/**
	 * update so detail delivered qty value from SalesTtransaction detail
	 * method used both for save and cancel
	 * 
	 * @param _vSIDet
	 * @param _sSOID
	 * @param _bCloseSO
	 * @param _bCancelSI
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateSOFromInvoice(List _vSIDet, 
									  	   boolean _bCloseSO,
									  	   boolean _bCancelSI, 
									  	   Connection _oConn)
        throws Exception
    {
        try
        {
        	String sSOID = "";
            for(int i = 0; i < _vSIDet.size(); i++)
		    {
		    	SalesTransactionDetail oSIDet = (SalesTransactionDetail) _vSIDet.get(i);
		    	sSOID = oSIDet.getSalesOrderId();
		    	String sDOID = oSIDet.getDeliveryOrderId();
		    	String sDODetID = oSIDet.getDeliveryOrderDetailId();
		    	if((StringUtil.isNotEmpty(sSOID) && StringUtil.isEqual(sDOID, sSOID)) && //SO == DO, SI import SO directly
		    	   StringUtil.isNotEmpty(sDODetID))
		    	{
		    		SalesOrderDetail oSODet = getDetailByDetailID(sDODetID,_oConn);
		    		if (oSODet != null)
		    		{
    		    		double dSOQty = oSODet.getQty().doubleValue();
    		    		double dSODeliveredQty = oSODet.getDeliveredQty().doubleValue();
    		    		double dDOQty = oSIDet.getQty().doubleValue();
    		    		
    		    		//if cancel SI
    		    		if(_bCancelSI)dDOQty = dDOQty * -1;
    		    		
    		    		double dAfterDelivered = dSODeliveredQty + dDOQty;
    		    		if(!_bCancelSI)
    		    		{
    			    		if(dSOQty > (dAfterDelivered)){oSODet.setDeliveredQty(new BigDecimal(dAfterDelivered));}
    			    		else {oSODet.setDeliveredQty(new BigDecimal(dSOQty));}//end if
    		    		}
    		    		else
    		    		{
    		    			if(dAfterDelivered < 0){oSODet.setDeliveredQty(bd_ZERO);}
    		    			else {oSODet.setDeliveredQty(new BigDecimal(dAfterDelivered));}
    		    		}
    		    		oSODet.save(_oConn);
		    	    }
		    	}//end if

            }//end for
            
            if(StringUtil.isNotEmpty(sSOID))
            {
            	updateStatusFromDeliveredItem(sSOID,_oConn);   
                if(_bCloseSO)
                {
                	closeSO(sSOID,_oConn);
                }
            }
        }
        catch(Exception _oEx)
        {
            String sMessage = "Update SO from DO Failed ERROR : " + _oEx.getMessage();
            log.error(_oEx);
            _oEx.printStackTrace();
            throw new NestableException(sMessage, _oEx);
        }
    }

	
	/**
     * update SO status to closed if all details is already delivered
     * 
     * @param _sSOID
     * @param _oConn
     */
    private static void updateStatusFromDeliveredItem (String _sSOID, Connection _oConn)
    	throws Exception
    {        
    	boolean bAllDelivered = true;
    	List vSOD = getDetailsByID(_sSOID, _oConn);
        for(int i = 0; i < vSOD.size(); i++)
        {
            SalesOrderDetail oSODet = (SalesOrderDetail)vSOD.get(i);
            double dDelivered = oSODet.getDeliveredQty().doubleValue();
            double dOrdered = oSODet.getQty().doubleValue();
            if ( dDelivered >= dOrdered )
            {
                oSODet.setClosed(true);
                oSODet.save(_oConn);
            }
            else
            {
            	bAllDelivered = false;
            }
        }
        SalesOrder oSO = getHeaderByID(_sSOID, _oConn);
        if (oSO != null)
        {
	        if (bAllDelivered)
	        {
	        	oSO.setTransactionStatus(i_SO_DELIVERED);
	        }
	        else
	        {
	        	oSO.setTransactionStatus(i_SO_ORDERED);
	        }
	    	oSO.save(_oConn);
        }
    }	
	
    /**
     * set header properties from form
     * 
     * @param _oSO
     * @param _vSOD
     * @param data
     * @throws Exception
     */
    public static void setHeaderProperties (SalesOrder _oSO, List _vSOD, RunData data)
    	throws Exception
    {
    	ValueParser formData = null;
    	if (data != null) 
    	{
    		formData = data.getParameters();
    	}
		try
		{
			String sTotalDiscountPct = "";
			if (formData != null)
			{
				formData.setProperties  (_oSO);
				String sEmpName = formData.getString("EmployeeName");
				String sSalesID = formData.getString("SalesId");
				if (StringUtil.isNotEmpty(sEmpName) && StringUtil.isEmpty(sSalesID))
				{
					_oSO.setSalesId(EmployeeTool.getIDByName(sEmpName));
				}
				
				_oSO.setCustomerName  	( CustomerTool.getCustomerNameByID (_oSO.getCustomerId()) );
	    		_oSO.setTransactionDate ( CustomParser.parseDate(formData.getString("TransactionDate")) );
	    		_oSO.setDueDate 		( new Date () );
	    		_oSO.setCustomerPoDate	( CustomParser.parseDate(formData.getString("CustomerPoDate")));
	    		_oSO.setShipDate	    ( CustomParser.parseDate(formData.getString("ShipDate")));
	    		_oSO.setDpDueDate	    ( CustomParser.parseDate(formData.getString("DpDueDate")));
	    		_oSO.setIsTaxable  		( formData.getBoolean("IsTaxable",false));
	    		_oSO.setIsInclusiveTax  ( formData.getBoolean("IsInclusiveTax",false));
	            
	    		sTotalDiscountPct = formData.getString("TotalDiscountPct","");

			}
    		_oSO.setTotalQty 	    ( countQty(_vSOD));    		
			_oSO.setTotalAmount     ( countSubTotal(_vSOD));
			_oSO.setTotalDiscount   ( countDiscount(_vSOD));
			
			log.debug("SO Total Amount Before Tax : " + _oSO.getTotalAmount());
            
			//count total amount after total discount percentage
            double dTotalDiscountAmount = 0;
            double dTotalAmount = _oSO.getTotalAmount().doubleValue();
            
			_oSO.setTotalTax (countTax(sTotalDiscountPct,_vSOD,_oSO));
			
			log.debug("SO Total Amount After Tax : " + _oSO.getTotalAmount());
			
			if (StringUtil.isNotEmpty(sTotalDiscountPct)) 
			{
				//get discount for all 
				//count total discount from total amount
				dTotalDiscountAmount = Calculator.calculateDiscount(sTotalDiscountPct, dTotalAmount);
				double dSubTotalDiscount = _oSO.getTotalDiscount().doubleValue();
				_oSO.setTotalDiscount (new BigDecimal(dTotalDiscountAmount + dSubTotalDiscount));
				_oSO.setTotalAmount (new BigDecimal(dTotalAmount - dTotalDiscountAmount));							
				
				log.debug("SO Total Amount After Total Disc : " + _oSO.getTotalAmount());
			}

            //count Total Amount with tax
            if(!_oSO.getIsInclusiveTax())
            {
                dTotalAmount = _oSO.getTotalAmount().doubleValue() + _oSO.getTotalTax().doubleValue();
                _oSO.setTotalAmount (new BigDecimal(dTotalAmount));
            }	
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			throw new NestableException ("Set SO Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }

    /**
     * 
     * @param _sDiscountPct
     * @param _vDetails
     * @param _oSO
     * @return total tax
     */
	private static BigDecimal countTax (String _sDiscountPct, List _vDetails, SalesOrder _oSO)
	{
	    double dTotalAmount = _oSO.getTotalAmount().doubleValue();
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			SalesOrderDetail oDet = (SalesOrderDetail) _vDetails.get(i);
			double dSubTotal = oDet.getSubTotal().doubleValue();
			double dDisc = Calculator.calculateDiscount(_sDiscountPct, dSubTotal);
			double dSubTotalTax = 0;
			
		    if(!_sDiscountPct.contains(Calculator.s_PCT)) //if _sDiscountPCT ! contains '%'
		    {
		    	if (dTotalAmount > 0) //if dTotalAmount > 0, prevent divide by zero
		    	{
		    		dSubTotal = dSubTotal - (dDisc * (dSubTotal / dTotalAmount));
		    	}
		    }
		    else
		    {
		    	dSubTotal = dSubTotal - dDisc;
		    }
		    
			if(!_oSO.getIsInclusiveTax()) //if not inclusive
			{
			    dSubTotalTax = dSubTotal * oDet.getTaxAmount().doubleValue() / 100;
			}
			else
			{
			    //calculate inclusive Tax
			    dSubTotalTax = (dSubTotal * 100) / (oDet.getTaxAmount().doubleValue() + 100);
			    dSubTotalTax = dSubTotal - dSubTotalTax;  
			}
			oDet.setSubTotalTax(new BigDecimal(dSubTotalTax));
			dAmt += oDet.getSubTotalTax().doubleValue();
		}
		return new BigDecimal(dAmt);
	}

	/**
	 * 
	 * @param _vDetails
	 * @return total qty
	 * @throws Exception
	 */
	private static BigDecimal countQty (List _vDetails)
		throws Exception
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			SalesOrderDetail oDet = (SalesOrderDetail) _vDetails.get(i);
			if (oDet.getQtyBase() == null) //if from add detail
			{
				oDet.setQtyBase (UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty()));
			}
			dAmt += oDet.getQtyBase().doubleValue();
		}
		return new BigDecimal(dAmt);
	}	
	
	/**
	 * details per item discount
	 * 
	 * @param _vDetails
	 * @return total discount
	 */
	private static BigDecimal countDiscount (List _vDetails)
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			SalesOrderDetail oDet = (SalesOrderDetail) _vDetails.get(i);
			if(oDet.getSubTotalDisc() == null)
			{
				double dDisc = Calculator.calculateDiscount(oDet.getDiscount(), oDet.getSubTotal(), oDet.getQty());
				oDet.setSubTotalDisc(new BigDecimal(dDisc));
			}
			dAmt += oDet.getSubTotalDisc().doubleValue();
			
		}
		return new BigDecimal(dAmt);
	}

	public static BigDecimal countSubTotal (List _vDetails)
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			SalesOrderDetail oDet = (SalesOrderDetail) _vDetails.get(i);

			double dPrice = oDet.getItemPrice().doubleValue();
			String sDisc = oDet.getDiscount();
			double dSubTotal = oDet.getQty().doubleValue() * dPrice;
			double dSubTotalDisc = Calculator.calculateDiscount(sDisc, dSubTotal, oDet.getQty());
			dSubTotal = dSubTotal - dSubTotalDisc;
			oDet.setSubTotalDisc(new BigDecimal(dSubTotalDisc));			
			oDet.setSubTotal(new BigDecimal(dSubTotal));				
			dAmt += dSubTotal;
		}
		return new BigDecimal(dAmt);
	}
	
	/**
	 * update SO Detail
	 * 
	 * @param _vDet
	 * @param data
	 * @throws Exception
	 */
    public static void updateDetail (List _vDet, RunData data)
    	throws Exception
    {
    	ValueParser formData = data.getParameters();
		try
		{		    
			for (int i = 0; i < _vDet.size(); i++)
			{
				SalesOrderDetail oTD = (SalesOrderDetail) _vDet.get(i);
				log.debug ("Before update : "  + oTD);
				
				//TODO: correct this thing
				if (formData.getString("UnitId" + (i + 1)) != null)
				{
					oTD.setUnitId   (formData.getString("UnitId" + (i + 1)) );
					oTD.setUnitCode (formData.getString("UnitCode" + (i + 1)) );
					oTD.setQty      (formData.getBigDecimal("Qty" + (i + 1)) );
										
					oTD.setQtyBase 		( UnitTool.getBaseQty(oTD.getItemId(), oTD.getUnitId(), oTD.getQty()));
					oTD.setItemPrice    ( formData.getBigDecimal("ItemPrice" + (i + 1)) );
					oTD.setTaxAmount    ( formData.getBigDecimal("TaxAmount" + (i + 1)) );
					oTD.setTaxId        ( formData.getString    ("TaxId" + (i + 1)) );
					oTD.setSubTotalTax  ( formData.getBigDecimal("SubTotalTax" + (i + 1)) );
					oTD.setDiscount     ( formData.getString    ("Discount" + (i + 1)) );
					oTD.setSubTotalDisc ( formData.getBigDecimal("SubTotalDisc" + (i + 1)) );
					oTD.setSubTotal     ( formData.getBigDecimal("SubTotal" + (i + 1)) );
					oTD.setCostPerUnit  ( formData.getBigDecimal("CostPerUnit" + (i + 1)) );
				
					log.debug ("After update : "  + oTD);
				}
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("Update SO Detail Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
	/**
	 * generate transaction no
	 * 
	 * @param _oSO
	 * @param _oConn
	 * @return transaction no
	 * @throws Exception
	 */
	private static String generateTransactionNo (SalesOrder _oSO, Connection _oConn) 
		throws Exception
	{
		String sTransNo = "";
		String sLocCode = "";
		if (!PreferenceTool.syncInstalled())
		{
			sLocCode = LocationTool.getLocationCodeByID(_oSO.getLocationId(), _oConn);
		}

		int iType = LastNumberTool.i_SALES_ORDER;
		String sFormat = s_SO_FORMAT;		
		
		SalesOrder oSO = SalesOrderTool.getHeaderByID(_oSO.getSalesOrderId(), _oConn);
		if (TransactionAttributes.b_SALES_SEPARATE_TAX_INV)
		{
			if (oSO != null && oSO.getTotalTax().doubleValue() > 0)
			{
				iType = LastNumberTool.i_TAX_SO;
				sFormat = s_TS_FORMAT;
			}
		}
		sTransNo = LastNumberTool.generateByCustomer(_oSO.getCustomerId(),_oSO.getLocationId(),sLocCode,sFormat,iType,_oConn);
		return sTransNo;
	}
	
	
    /**
     * save SO
     * 
     * @param _oSO
     * @param _vSOD
     * @throws Exception
     */
	public static void saveData (SalesOrder _oSO, List _vSOD)
		throws Exception
	{
		Connection oConn = null;
		try
		{
			oConn = beginTrans();
			validateDate (_oSO.getTransactionDate(), oConn);
			processSaveSOData (_oSO, _vSOD, oConn);			
			
			//check if down payment > 0 then create credit memo to note that the customer has payable amount to us
			if (_oSO.getTransactionStatus() == i_SO_ORDERED && _oSO.getDownPayment().doubleValue() > 0)
		    {
			    CreditMemoTool.createFromSODownPayment (_oSO, oConn);
		    }
			
			commit (oConn);
		}
		catch (Exception _oEx)
		{
			rollback (oConn);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * 
	 * @param _oSO
	 * @param _vSOD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveSOData (SalesOrder _oSO, List _vSOD, Connection _oConn) 
		throws Exception
	{
        if(StringUtil.isEmpty(_oSO.getSalesOrderId()))
        {
        	_oSO.setSalesOrderId(IDGenerator.generateSysID());
        	_oSO.setNew(true);
        }
        validateID(getHeaderByID(_oSO.getSalesOrderId(),_oConn), "salesOrderNo");
        
	    if (_oSO.getSalesOrderNo() == null) _oSO.setSalesOrderNo("");
		
		if (_oSO.getTransactionStatus () == i_SO_ORDERED) 
		{	
			//validate credit limit
			CreditLimitValidator.validateSO(_oSO);	
			
			String sLocCode = LocationTool.getLocationCodeByID(_oSO.getLocationId(), _oConn);
			
			if (StringUtil.isEmpty(_oSO.getSalesOrderNo()))
			{
				//generate sales Number no first
				_oSO.setSalesOrderNo(generateTransactionNo(_oSO, _oConn));
			}
			validateNo(SalesOrderPeer.SALES_ORDER_NO,_oSO.getSalesOrderNo(),SalesOrderPeer.class, _oConn);
			
			//set confirm date
			_oSO.setConfirmDate (new Date());
			
			//set payment due date 
			setDueDate (_oSO , _oConn);
		}

		//delete all existing detail from new purchase
		if (StringUtil.isNotEmpty(_oSO.getSalesOrderId()))
		{
			deleteDetailsByID (_oSO.getSalesOrderId(), _oConn);
		}	
		
		_oSO.save (_oConn);
		
		renumber(_vSOD);
		for ( int i = 0; i < _vSOD.size(); i++ )
		{
			SalesOrderDetail oTD = (SalesOrderDetail) _vSOD.get (i);
			oTD.setModified (true);
			oTD.setNew (true);
			
			//validate Discount
			
			//Generate Sys Data
			if (StringUtil.isEmpty(oTD.getSalesOrderDetailId())) 
			{
				oTD.setSalesOrderDetailId (IDGenerator.generateSysID());
			}

			//set department id automatically from salesman
			if(PreferenceTool.getMultiDept())
			{
				Employee oSales = _oSO.getSalesman();
				if(oSales != null 
				   && StringUtil.isNotEmpty(oSales.getDepartmentId()) 
				   && StringUtil.isEmpty(oTD.getDepartmentId()))
				{
					oTD.setDepartmentId(oSales.getDepartmentId());
				}
			}
			
			oTD.setSalesOrderId (_oSO.getSalesOrderId());
			oTD.save (_oConn);
		}	
	}
	
	/**
     * this method is used to set due date on the sales Order
     *
     * @param _oSO - Sales Order Object 
     */
	public static void setDueDate (SalesOrder _oSO, Connection _oConn) 
		throws Exception 
	{
		boolean bIsCredit = PaymentTypeTool.isCreditPaymentType(_oSO.getPaymentTypeId(), _oConn);
		if (bIsCredit) 
		{
			//check default payment term for this customer
			String sDefaultTerm = CustomerTool.getDefaultTermByID(_oSO.getCustomerId(), _oConn);
			int iDueDays = 0;
			if (StringUtil.isNotEmpty(sDefaultTerm) && 
				PaymentTermTool.isCashPaymentTerm(_oSO.getPaymentTermId())) 
			{
				iDueDays = PaymentTermTool.getNetPaymentDaysByID (sDefaultTerm);
				_oSO.setPaymentTermId(sDefaultTerm);
			}
			else 
			{
				iDueDays = PaymentTermTool.getNetPaymentDaysByID (_oSO.getPaymentTermId());
			}
			Calendar oCal = new GregorianCalendar ();
			oCal.setTime (_oSO.getTransactionDate());
			oCal.add (Calendar.DATE, iDueDays);
			_oSO.setDueDate (oCal.getTime());
		}
	}

	public static String getStatusString (int _iStatus)
	{
   		if (_iStatus == i_SO_NEW) return LocaleTool.getString("new");
		if (_iStatus == i_SO_ORDERED) return LocaleTool.getString("ordered");
		if (_iStatus == i_SO_DELIVERED) return LocaleTool.getString("delivered");
		if (_iStatus == i_SO_CANCELLED) return LocaleTool.getString("cancelled");
		if (_iStatus == i_SO_CLOSED) return LocaleTool.getString("closed");
		return "";
	}	

	/**
	 * update print times indicate how many times this invoice printed
	 * 
	 * @param _oTR
	 * @throws Exception
	 */
    public static void updatePrintTimes (SalesOrder _oTR) 
		throws Exception
	{
		_oTR.setPrintTimes (_oTR.getPrintTimes() + 1);
		_oTR.save();
	}
	
	/**
	 * Finder 
	 * @param _sTransNo
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _iStatus
	 * @param _iLimit
	 * @return query result in LargeSelect
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond, 
									    String _sKeywords,  
										Date _dStart, 
										Date _dEnd, 
								        String _sCustomerID,								        
								        String _sLocationID,
								        String _sSalesID,
										int _iStatus, 
										int _iLimit,
										int _iGroupBy,
										String _sCurrencyID) 
    	throws Exception
    {
        Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
        	SalesOrderPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);
        
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(SalesOrderPeer.CUSTOMER_ID, _sCustomerID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(SalesOrderPeer.LOCATION_ID, _sLocationID);	
		}
		if (StringUtil.isNotEmpty(_sSalesID)) 
		{
			oCrit.add(SalesOrderPeer.SALES_ID, _sSalesID);	
		}		
		if (_iStatus > 0) 
		{
			if(_iStatus == 99)
			{
				oCrit.add(SalesOrderPeer.TRANSACTION_STATUS, i_SO_ORDERED);
				oCrit.or(SalesOrderPeer.TRANSACTION_STATUS, i_SO_DELIVERED);
				oCrit.or(SalesOrderPeer.TRANSACTION_STATUS, i_SO_CLOSED);
			}
			else
			{
				oCrit.add(SalesOrderPeer.TRANSACTION_STATUS, _iStatus);
			}
		}		
		if (_iGroupBy > 0)
		{
			buildOrderFromGroupBy (oCrit, m_GROUP_PEER, _iGroupBy);
		}
		else
		{
			oCrit.addAscendingOrderByColumn(SalesOrderPeer.TRANSACTION_DATE);
		}
		oCrit.setDistinct();
		log.debug(oCrit);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.SalesOrderPeer");
	}

	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _sVendorID
	 * @param _sUserName
	 * @param _sCurrencyID
	 * @return
	 * @throws Exception
	 */
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sCustomerID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sVendorID,
								  String _sUserName,
								  String _sCurrencyID) 
		throws Exception
	{
		return findTrans(_iCond, _sKeywords, _dStart, _dEnd, _sCustomerID, _sLocationID, _iStatus, _sVendorID, _sUserName, _sCurrencyID, "");
	}
	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sCustomerID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _sVendorID
	 * @param _sUserName
	 * @param _sCurrencyID
	 * @param _sSalesID
	 * @return
	 * @throws Exception
	 */
	public static List findTrans (int _iCond,
								  String _sKeywords,
								  Date _dStart, 
								  Date _dEnd,  
								  String _sCustomerID,
								  String _sLocationID,
								  int _iStatus, 
								  String _sVendorID,
								  String _sUserName,
								  String _sCurrencyID,
								  String _sSalesID) 
		throws Exception
	{
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				SalesOrderPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID);   
		
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(SalesOrderPeer.CUSTOMER_ID, _sCustomerID);	
		}
		if(StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(SalesOrderPeer.LOCATION_ID, _sLocationID);
		}
		if(StringUtil.isNotEmpty(_sSalesID))
		{
			List vEMP = EmployeeTool.getChildIDList(new ArrayList(),_sSalesID);
			vEMP.add(_sSalesID);
			oCrit.addIn(SalesOrderPeer.SALES_ID, vEMP);
		}		
		if (StringUtil.isNotEmpty(_sUserName))
		{
			oCrit.add(SalesOrderPeer.USER_NAME, _sUserName);
		}        
		if(_iStatus > 0)
		{
			oCrit.add(SalesOrderPeer.TRANSACTION_STATUS, _iStatus);
			if(_iStatus == 99)
			{
				oCrit.add(SalesOrderPeer.TRANSACTION_STATUS, i_SO_ORDERED);
				oCrit.or(SalesOrderPeer.TRANSACTION_STATUS, i_SO_DELIVERED);				
			}
			else
			{
				oCrit.add(SalesOrderPeer.TRANSACTION_STATUS, _iStatus);
			}		
		}
		if (StringUtil.isNotEmpty(_sVendorID))
		{
			oCrit.addJoin(SalesOrderPeer.SALES_ORDER_ID,SalesOrderDetailPeer.SALES_ORDER_ID);
			oCrit.addJoin(SalesOrderDetailPeer.ITEM_ID,ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.PREFERED_VENDOR_ID,_sVendorID);
		}	   
		log.debug(oCrit);
		return SalesOrderPeer.doSelect(oCrit);
	}	
	
	/**
	 * update SO Status to closed
	 * @param _sSOID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void closeSO (String _sSOID, Connection _oConn)
		throws Exception
	{
        try
        {
            SalesOrder oSO = getHeaderByID(_sSOID, _oConn);
            closeSO(oSO, _oConn);
        }
        catch (Exception _oEx)
        {
            throw new NestableException (_oEx.getMessage(), _oEx);
        }
	}

	/**
	 * update SO Status to closed
	 * @param _oSO
	 * @param _oConn
	 * @throws Exception
	 */
	public static void closeSO (SalesOrder _oSO, Connection _oConn)
		throws Exception
	{
        try
        {
        	if(_oSO != null)
        	{            
	            _oSO.setTransactionStatus(i_SO_CLOSED);
	            _oSO.save(_oConn);
        	}
        }
        catch (Exception _oEx)
        {
            throw new NestableException (_oEx.getMessage(), _oEx);
        }
	}
	
	
	/**
	 * update SO Status to cancelled
	 * @param _SoID
	 * @param m_oConn
	 * @throws Exception
	 */
	public static void cancelSO (SalesOrder _oSO, List _vSOD, String _sCancelBy)
		throws Exception
	{
		Connection oConn = null;
	    try
	    {
	        if (_oSO != null)
	        {
	        	oConn = beginTrans();
		    	validateDate (_oSO.getTransactionDate(), oConn);
		        _oSO.setRemark(cancelledBy(_oSO.getRemark(), _sCancelBy));
		        _oSO.setTransactionStatus(i_SO_CANCELLED);
		        _oSO.save(oConn);

				//if dp exists try cancel CM
		        if (_oSO.getDownPayment().doubleValue() > 0)
			    {
				    CreditMemoTool.cancelFromSalesOrderDP(_oSO, _sCancelBy, oConn);
			    }
		        		        		        
		        commit(oConn);
	        }
	    }
	    catch (Exception _oEx)
	    {
	    	rollback(oConn);
	        throw new NestableException (_oEx.getMessage(), _oEx);
	    }
	}	

	/**
	 * get Qty exist in all (NEW) SO 
	 * 
	 * @param _sItemID
	 * @return on SalesOrder qty 
	 * @throws Exception
	 */
	
	public static double getOnOrderQty (String _sItemID)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder();
		oSQL.append(" SELECT SUM(sod.qty - sod.delivered_qty), sod.unit_id ")
		.append(" FROM sales_order so, sales_order_detail sod ")
		.append(" WHERE sod.sales_order_id = so.sales_order_id ")
		.append(" AND so.transaction_status  = 1")
		.append(" AND sod.item_id  = '")
		.append(_sItemID)
		.append("' GROUP BY sod.unit_id");
		log.debug(oSQL);
		List vData = SalesOrderDetailPeer.executeQuery (oSQL.toString());

		double dTotalOnOrder = 0;
		for (int i = 0; i < vData.size(); i++)
		{
			Record oData = (Record) vData.get(i);
			double dOnOrder = oData.getValue(1).asDouble();  
			double dBase = UnitTool.getBaseValue(_sItemID, oData.getValue(2).asString());            
			dTotalOnOrder += (dOnOrder * dBase);
		}
		return dTotalOnOrder;       
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static List getTransactionInDateRange (Date _dStartDate, Date _dEndDate)
    	throws Exception
	{
	    return getTransactionInDateRange (_dStartDate,_dEndDate, 0, "", 0);
	}

	public static List getTransactionInDateRange (Date _dStart, 
												  Date _dEnd, 
												  int _iGroupBy, 
												  String _sKeywords, 
												  int _iOrderBy)
    	throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(SalesOrderPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        oCrit.and(SalesOrderPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        if(_iGroupBy == 2)
        {
            oCrit.add(SalesOrderPeer.CUSTOMER_NAME, 
                (Object) SqlUtil.like (SalesOrderPeer.CUSTOMER_NAME, _sKeywords,false,true), Criteria.CUSTOM);
        }
        if(_iGroupBy == 4)
        {
            oCrit.add(LocationPeer.LOCATION_NAME, 
                (Object) SqlUtil.like (LocationPeer.LOCATION_NAME, _sKeywords,false,true), Criteria.CUSTOM);
            oCrit.addJoin(SalesOrderPeer.LOCATION_ID,LocationPeer.LOCATION_ID);
        }
        if(_iGroupBy == 6)
        {
            oCrit.add(SalesOrderPeer.USER_NAME, 
                (Object) SqlUtil.like (SalesOrderPeer.USER_NAME, _sKeywords,false,true), Criteria.CUSTOM);
        }

        if(_iOrderBy == 1)
        {
            oCrit.addAscendingOrderByColumn(SalesOrderPeer.SALES_ORDER_NO);
        }
        if(_iOrderBy == 2)
        {
            oCrit.addAscendingOrderByColumn(SalesOrderPeer.CUSTOMER_NAME);
        }
        if(_iOrderBy == 3)
        {
            oCrit.addAscendingOrderByColumn(SalesOrderPeer.USER_NAME);
        }
		return SalesOrderPeer.doSelect(oCrit);
	}
	
	/**
	 * get all details of existing so in list
	 * 
	 * @param _vSO
	 * @return trans details
	 * @throws Exception
	 */
	public static List getTransDetails (List _vSO)
    	throws Exception
    {
		List vSOID = new ArrayList (_vSO.size());
		for (int i = 0; i < _vSO.size(); i++)
		{
			SalesOrder oSO = (SalesOrder) _vSO.get(i);
			vSOID.add (oSO.getSalesOrderId());
		}
		Criteria oCrit = new Criteria();
		oCrit.addIn (SalesOrderDetailPeer.SALES_ORDER_ID, vSOID);
		return SalesOrderDetailPeer.doSelect(oCrit);
	}
	
	/**
	 * get DP available for this invoice
	 * @param _sID
	 * @return DP amount
	 */
	public static BigDecimal getDPByInvoiceID (String _sID)
	{
		Criteria oCrit = new Criteria();
		oCrit.addJoin(SalesOrderPeer.SALES_ORDER_ID, SalesTransactionDetailPeer.SALES_ORDER_ID);
		oCrit.add(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, _sID);
		oCrit.setDistinct();
		try 
		{
			List vSO = SalesOrderPeer.doSelect(oCrit);
			String sID = "";
			double dDP = 0;
			for (int i = 0; i < vSO.size(); i++)
			{
				SalesOrder oSO = (SalesOrder) vSO.get(i);
				if ((!sID.equals("") && !oSO.getSalesOrderId().equals(sID)) || sID.equals(""))
				{
					dDP = dDP + oSO.getDownPayment().doubleValue();
					sID = oSO.getSalesOrderId();					
				}
			}
			return new BigDecimal(dDP);
		} 
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}
		return bd_ZERO;
	}		
	
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(SalesOrderPeer.class, SalesOrderPeer.SALES_ORDER_ID, _sID, _bIsNext);
	}
	
	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
		
	/**
	 * get List of SalesOrder created since last store 2 ho
	 * 
	 * @return List of SalesOrder created since last store 2 ho
	 * @throws Exception
	 */
	public static List getStoreSalesOrder ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();
		
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(SalesOrderPeer.CONFIRM_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
		oCrit.add(SalesOrderPeer.TRANSACTION_STATUS, i_SO_ORDERED);
		return SalesOrderPeer.doSelect(oCrit);
	}
}


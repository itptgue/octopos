package com.ssti.enterprise.pos.tools.report;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Financial Report Tool. Used for filtering, sum-up, calculate balance amount in financial Report
 * <br>
 *
 * @author  $Author: seph $ <br>
 * @version $Id: FinancialReportTool.java,v 1.3 2007/04/10 11:21:46 seph Exp $ <br>
 *
 * <pre>
 * $Log: FinancialReportTool.java,v $
 * Revision 1.3  2007/04/10 11:21:46  seph
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class FinancialReportTool
{
	private static Log log = LogFactory.getLog(FinancialReportTool.class);
    

}
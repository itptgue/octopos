package com.workingdogs.village;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 * The DataSet represents a table in the database. It is extended by <a href="QueryDataSet.html">QueryDataSet</a> and <a
 * href="TableDataSet.html">TableDataSet</a> and should not be used directly. A DataSet contains a <a
 * href="Schema.html">Schema</a> and potentially a collection of <a href="Record.html">Records</a>.
 *
 * @author <a href="mailto:jon@latchkey.com">Jon S. Stevens</a>
 * @version $Revision: 1.1 $
 */
public abstract class DataSet
{
    /** indicates that all records should be retrieved during a fetch */
    protected static final int ALL_RECORDS = -1;

    /** this DataSet's schema object */
    protected Schema schema;

    /** this DataSet's collection of Record objects */
    protected Vector records = null;

    /** this DataSet's connection object */
    protected Connection conn;

    /** have all records been retrieved with the fetchRecords? */
    private boolean allRecordsRetrieved = false;

    /** number of records retrieved */
    private int recordRetrievedCount = 0;

    /** number of records that were last fetched */
    private int lastFetchSize = 0;

    /** the columns in the SELECT statement for this DataSet */
    private String columns;

    /** the select string that was used to build this DataSet */
    protected StringBuffer selectString;

    /** the KeyDef for this DataSet */
    private KeyDef keyDefValue;

    /** the result set for this DataSet */
    protected ResultSet resultSet;

    /** the Statement for this DataSet */
    protected Statement stmt;

    /**
     * Private, not used
     *
     * @exception DataSetException
     * @exception SQLException
     */
    public DataSet()
            throws DataSetException, SQLException
    {
    }

    /**
     * Create a new DataSet with a connection and a Table name
     *
     * @param conn
     * @param tableName
     *
     * @exception DataSetException
     * @exception SQLException
     */
    DataSet(Connection conn, String tableName)
            throws DataSetException, SQLException
    {
        this.conn = conn;
        this.columns = "*";
        this.schema = new Schema().schema(conn, tableName);
    }

    /**
     * Create a new DataSet with a connection, schema and KeyDef
     *
     * @param conn
     * @param schema
     * @param keydef
     *
     * @exception DataSetException
     * @exception SQLException
     */
    DataSet(Connection conn, Schema schema, KeyDef keydef)
            throws DataSetException, SQLException
    {
        if (conn == null)
        {
            throw new SQLException("Database connection could not be established!");
        }
        else if (schema == null)
        {
            throw new DataSetException("You need to specify a valid schema!");
        }
        else if (keydef == null)
        {
            throw new DataSetException("You need to specify a valid KeyDef!");
        }

        this.conn = conn;
        this.schema = schema;
        this.columns = "*";

        this.keyDefValue = keydef;
    }

    /**
     * Create a new DataSet with a connection, tablename and KeyDef
     *
     * @param conn
     * @param tableName
     * @param keydef
     *
     * @exception SQLException
     * @exception DataSetException
     */
    DataSet(Connection conn, String tableName, KeyDef keydef)
            throws SQLException, DataSetException
    {
        this.conn = conn;
        this.keyDefValue = keydef;
        this.columns = "*";
        this.schema = new Schema().schema(conn, tableName);
    }

    /**
     * Create a new DataSet with a connection, tablename and list of columns
     *
     * @param conn
     * @param tableName
     * @param columns
     *
     * @exception SQLException
     * @exception DataSetException
     */
    DataSet(Connection conn, String tableName, String columns)
            throws SQLException, DataSetException
    {
        this.conn = conn;
        this.columns = columns;
        this.schema = new Schema().schema(conn, tableName, columns);
    }

    /**
     * Create a new DataSet with a connection, tableName, columns and a KeyDef
     *
     * @param conn
     * @param tableName
     * @param columns
     * @param keyDef
     *
     * @exception SQLException
     * @exception DataSetException
     */
    DataSet(Connection conn, String tableName, String columns, KeyDef keyDef)
            throws SQLException, DataSetException
    {
        this.conn = conn;
        this.columns = columns;
        this.keyDefValue = keyDef;
        this.schema = new Schema().schema(conn, tableName, columns);
    }

    /**
     * Gets the ResultSet for this DataSet
     *
     * @return the result set for this DataSet
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public ResultSet resultSet()
            throws SQLException, DataSetException
    {
        if (this.resultSet == null)
        {
            throw new DataSetException("ResultSet is null.");
        }

        return this.resultSet;
    }

    /**
     * Calls addRecord(DataSet)
     *
     * @return the added record
     *
     * @exception DataSetException
     * @exception SQLException
     */
    public Record addRecord()
            throws DataSetException, SQLException
    {
        return addRecord(this);
    }

    /**
     * Creates a new Record within this DataSet
     *
     * @param ds
     *
     * @return the added record
     *
     * @exception DataSetException
     * @exception SQLException
     */
    public Record addRecord(DataSet ds)
            throws DataSetException, SQLException
    {
        if (ds instanceof QueryDataSet)
        {
            throw new DataSetException("You cannot add records to a QueryDataSet.");
        }

        if (records == null)
        {
            records = new Vector(10);
        }

        Record rec = new Record(ds, true);
        rec.markForInsert();
        records.addElement(rec);

        return rec;
    }

    /**
     * Check if all the records have been retrieve
     *
     * @return true if all records have been retrieved
     */
    public boolean allRecordsRetrieved()
    {
        return this.allRecordsRetrieved;
    }

    /**
     * Set all records retrieved
     *
     * @param set TODO: DOCUMENT ME!
     */
    void setAllRecordsRetrieved(boolean set)
    {
        this.allRecordsRetrieved = set;
    }

    /**
     * Remove a record from the DataSet's internal storage
     *
     * @param rec
     *
     * @return the record removed
     *
     * @exception DataSetException
     */
    public Record removeRecord(Record rec)
            throws DataSetException
    {
        Record removeRec = null;

        try
        {
            int loc = this.records.indexOf(rec);
            removeRec = (Record) this.records.elementAt(loc);
            this.records.removeElementAt(loc);
        }
        catch (Exception e)
        {
            throw new DataSetException("Record could not be removed!");
        }

        return removeRec;
    }

    /**
     * Remove all records from the DataSet and nulls those records out and close() the DataSet.
     *
     * @return an instance of myself
     */
    public DataSet clearRecords()
    {
        this.records.removeAllElements();
        this.records = null;

        return this;
    }

    /**
     * Removes the records from the DataSet, but does not null the records out
     *
     * @return an instance of myself
     */
    public DataSet releaseRecords()
    {
        this.records = null;
        this.recordRetrievedCount = 0;
        this.lastFetchSize = 0;
        setAllRecordsRetrieved(false);

        return this;
    }

    /**
     * Releases the records, closes the ResultSet and the Statement, and nulls the Schema and Connection references.
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public void close()
            throws SQLException, DataSetException
    {
        releaseRecords();
        this.schema = null;

        if ((this.resultSet != null) && !(this instanceof QueryDataSet))
        {
            resultSet().close();
        }

        this.resultSet = null;

        if (this.stmt != null)
        {
            this.stmt.close();
        }

        this.conn = null;
    }

    /**
     * Essentially the same as releaseRecords, but it won't work on a QueryDataSet that has been created with a ResultSet
     *
     * @return an instance of myself
     *
     * @exception DataSetException
     * @exception SQLException
     */
    public DataSet reset()
            throws DataSetException, SQLException
    {
        if (!((resultSet() != null) && (this instanceof QueryDataSet)))
        {
            return releaseRecords();
        }
        else
        {
            throw new DataSetException("You cannot call reset() on a QueryDataSet.");
        }
    }

    /**
     * Gets the current database connection
     *
     * @return a database connection
     *
     * @exception SQLException
     */
    public Connection connection()
            throws SQLException
    {
        return this.conn;
    }

    /**
     * Gets the Schema for this DataSet
     *
     * @return the Schema for this DataSet
     */
    public Schema schema()
    {
        return this.schema;
    }

    /**
     * Get Record at 0 based index position
     *
     * @param pos
     *
     * @return an instance of the found Record
     *
     * @exception DataSetException
     */
    public Record getRecord(int pos)
            throws DataSetException
    {
        if (containsRecord(pos))
        {
            Record rec = (Record) this.records.elementAt(pos);

            if (this instanceof TableDataSet)
            {
                rec.markForUpdate();
            }

            recordRetrievedCount++;

            return rec;
        }

        throw new DataSetException("Record not found at index: " + pos);
    }

    /**
     * Find Record at 0 based index position. This is an internal alternative to getRecord which tries to be smart about the type
     * of record it is.
     *
     * @param pos
     *
     * @return an instance of the found Record
     *
     * @exception DataSetException
     */
    Record findRecord(int pos)
            throws DataSetException
    {
        if (containsRecord(pos))
        {
            return (Record) this.records.elementAt(pos);
        }

        throw new DataSetException("Record not found at index: " + pos);
    }

    /**
     * Check to see if the DataSet contains a Record at 0 based position
     *
     * @param pos
     *
     * @return true if record exists
     */
    public boolean containsRecord(int pos)
    {
        try
        {
            if (this.records.elementAt(pos) != null)
            {
                return true;
            }
        }
        catch (Exception e)
        {
            return false;
        }

        return false;
    }

    /**
     * Causes the DataSet to hit the database and fetch all the records.
     *
     * @return an instance of myself
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public DataSet fetchRecords()
            throws SQLException, DataSetException
    {
        return fetchRecords(ALL_RECORDS);
    }

    /**
     * Causes the DataSet to hit the database and fetch max records.
     *
     * @param max
     *
     * @return an instance of myself
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public DataSet fetchRecords(int max)
            throws SQLException, DataSetException
    {
        return fetchRecords(0, max);
    }

    /**
     * Causes the DataSet to hit the database and fetch max records, starting at start. Record count begins at 0.
     *
     * @param start
     * @param max
     *
     * @return an instance of myself
     *
     * @exception SQLException
     * @exception DataSetException
     */
    public DataSet fetchRecords(int start, int max)
            throws SQLException, DataSetException
    {
        if (max == 0)
        {
            throw new DataSetException("Max is 1 based and must be greater than 0!");
        }
        else if ((lastFetchSize() > 0) && (this.records != null))
        {
            throw new DataSetException("You must call DataSet.clearRecords() before executing DataSet.fetchRecords() again!");
        }

        if (selectString == null)
        {
            selectString = new StringBuffer(256);
            selectString.append("SELECT ");
            selectString.append(schema().attributes());
            selectString.append(" FROM ");
            selectString.append(schema().tableName());
        }

        try
        {
            if ((stmt == null) && (this.resultSet == null))
            {
                stmt = connection().createStatement();
                this.resultSet = stmt.executeQuery(selectString.toString());
            }

            if (this.resultSet != null)
            {
                if ((this.records == null) && (max > 0))
                {
                    this.records = new Vector(max);
                }
                else
                {
                    this.records = new Vector();
                }

                int startCounter = 0;
                int fetchCount = 0;

                while (!allRecordsRetrieved())
                {
                    if (fetchCount == max)
                    {
                        break;
                    }

                    if (this.resultSet.next())
                    {
                        if (startCounter >= start)
                        {
                            Record rec = new Record(this);
                            records.addElement(rec);
                            fetchCount++;
                        }
                        else
                        {
                            startCounter++;
                        }
                    }
                    else
                    {
                        setAllRecordsRetrieved(true);

                        break;
                    }
                }

                lastFetchSize = fetchCount;
            }
        }
        catch (SQLException e)
        {
            if (stmt != null)
            {
                stmt.close();
            }

            throw new SQLException(e.getMessage());
        }

        return this;
    }

    /**
     * The number of records that were fetched with the last fetchRecords.
     *
     * @return int
     */
    public int lastFetchSize()
    {
        return lastFetchSize;
    }

    /**
     * gets the KeyDef object for this DataSet
     *
     * @return the keydef for this DataSet, this value can be null
     */
    public KeyDef keydef()
    {
        return this.keyDefValue;
    }

    /**
     * This returns a represention of this DataSet
     *
     * @return TODO: DOCUMENT ME!
     */
    public String toString()
    {
        try
        {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            PrintWriter out = new PrintWriter(bout);

            if (schema != null)
            {
                out.println(schema.toString());
            }

            for (int i = 0; i < size(); i++)
            {
                out.println(findRecord(i));
            }

            out.flush();

            return bout.toString();
        }
        catch (DataSetException e)
        {
            return "{}";
        }
    }

    /**
     * Gets the tableName defined in the schema
     *
     * @return string
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public String tableName()
            throws DataSetException
    {
        return schema().tableName();
    }

    /**
     * Calculates the maxColumnWidths for the column in a DataSet I really don't know what this is used for so it isn't
     * implemented.
     *
     * @param with_heading
     *
     * @return int
     *
     * @exception DataSetException
     * @exception SQLException
     */
    public int [] maxColumnWidths(boolean with_heading)
            throws DataSetException, SQLException
    {
        if (schema() == null)
        {
            throw new DataSetException("Schema is null!");
        }

        throw new DataSetException("Not yet implemented!");
    }

    /**
     * Classes extending this class must implement this method.
     *
     * @return the select string
     *
     * @throws DataSetException TODO: DOCUMENT ME!
     */
    public abstract String getSelectString()
            throws DataSetException;

    /**
     * Returns the columns attribute for the DataSet
     *
     * @return the columns attribute for the DataSet
     */
    String getColumns()
    {
        return this.columns;
    }

    /**
     * Gets the number of Records in this DataSet. It is 0 based.
     *
     * @return number of Records in this DataSet
     */
    public int size()
    {
        if (this.records == null)
        {
            return 0;
        }

        return this.records.size();
    }
}

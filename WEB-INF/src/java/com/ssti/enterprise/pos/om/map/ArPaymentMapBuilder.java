package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ArPaymentMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ArPaymentMapBuilder";

    /**
     * Item
     * @deprecated use ArPaymentPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "ar_payment";
    }

  
    /**
     * ar_payment.AR_PAYMENT_ID
     * @return the column name for the AR_PAYMENT_ID field
     * @deprecated use ArPaymentPeer.ar_payment.AR_PAYMENT_ID constant
     */
    public static String getArPayment_ArPaymentId()
    {
        return "ar_payment.AR_PAYMENT_ID";
    }
  
    /**
     * ar_payment.AR_PAYMENT_NO
     * @return the column name for the AR_PAYMENT_NO field
     * @deprecated use ArPaymentPeer.ar_payment.AR_PAYMENT_NO constant
     */
    public static String getArPayment_ArPaymentNo()
    {
        return "ar_payment.AR_PAYMENT_NO";
    }
  
    /**
     * ar_payment.AR_PAYMENT_DATE
     * @return the column name for the AR_PAYMENT_DATE field
     * @deprecated use ArPaymentPeer.ar_payment.AR_PAYMENT_DATE constant
     */
    public static String getArPayment_ArPaymentDate()
    {
        return "ar_payment.AR_PAYMENT_DATE";
    }
  
    /**
     * ar_payment.AR_PAYMENT_DUE_DATE
     * @return the column name for the AR_PAYMENT_DUE_DATE field
     * @deprecated use ArPaymentPeer.ar_payment.AR_PAYMENT_DUE_DATE constant
     */
    public static String getArPayment_ArPaymentDueDate()
    {
        return "ar_payment.AR_PAYMENT_DUE_DATE";
    }
  
    /**
     * ar_payment.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use ArPaymentPeer.ar_payment.BANK_ID constant
     */
    public static String getArPayment_BankId()
    {
        return "ar_payment.BANK_ID";
    }
  
    /**
     * ar_payment.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use ArPaymentPeer.ar_payment.BANK_ISSUER constant
     */
    public static String getArPayment_BankIssuer()
    {
        return "ar_payment.BANK_ISSUER";
    }
  
    /**
     * ar_payment.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use ArPaymentPeer.ar_payment.CUSTOMER_ID constant
     */
    public static String getArPayment_CustomerId()
    {
        return "ar_payment.CUSTOMER_ID";
    }
  
    /**
     * ar_payment.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use ArPaymentPeer.ar_payment.CUSTOMER_NAME constant
     */
    public static String getArPayment_CustomerName()
    {
        return "ar_payment.CUSTOMER_NAME";
    }
  
    /**
     * ar_payment.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use ArPaymentPeer.ar_payment.REFERENCE_NO constant
     */
    public static String getArPayment_ReferenceNo()
    {
        return "ar_payment.REFERENCE_NO";
    }
  
    /**
     * ar_payment.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use ArPaymentPeer.ar_payment.PAYMENT_AMOUNT constant
     */
    public static String getArPayment_PaymentAmount()
    {
        return "ar_payment.PAYMENT_AMOUNT";
    }
  
    /**
     * ar_payment.TOTAL_DOWN_PAYMENT
     * @return the column name for the TOTAL_DOWN_PAYMENT field
     * @deprecated use ArPaymentPeer.ar_payment.TOTAL_DOWN_PAYMENT constant
     */
    public static String getArPayment_TotalDownPayment()
    {
        return "ar_payment.TOTAL_DOWN_PAYMENT";
    }
  
    /**
     * ar_payment.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use ArPaymentPeer.ar_payment.TOTAL_AMOUNT constant
     */
    public static String getArPayment_TotalAmount()
    {
        return "ar_payment.TOTAL_AMOUNT";
    }
  
    /**
     * ar_payment.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use ArPaymentPeer.ar_payment.USER_NAME constant
     */
    public static String getArPayment_UserName()
    {
        return "ar_payment.USER_NAME";
    }
  
    /**
     * ar_payment.REMARK
     * @return the column name for the REMARK field
     * @deprecated use ArPaymentPeer.ar_payment.REMARK constant
     */
    public static String getArPayment_Remark()
    {
        return "ar_payment.REMARK";
    }
  
    /**
     * ar_payment.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use ArPaymentPeer.ar_payment.CURRENCY_ID constant
     */
    public static String getArPayment_CurrencyId()
    {
        return "ar_payment.CURRENCY_ID";
    }
  
    /**
     * ar_payment.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use ArPaymentPeer.ar_payment.CURRENCY_RATE constant
     */
    public static String getArPayment_CurrencyRate()
    {
        return "ar_payment.CURRENCY_RATE";
    }
  
    /**
     * ar_payment.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use ArPaymentPeer.ar_payment.FISCAL_RATE constant
     */
    public static String getArPayment_FiscalRate()
    {
        return "ar_payment.FISCAL_RATE";
    }
  
    /**
     * ar_payment.STATUS
     * @return the column name for the STATUS field
     * @deprecated use ArPaymentPeer.ar_payment.STATUS constant
     */
    public static String getArPayment_Status()
    {
        return "ar_payment.STATUS";
    }
  
    /**
     * ar_payment.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use ArPaymentPeer.ar_payment.TOTAL_DISCOUNT constant
     */
    public static String getArPayment_TotalDiscount()
    {
        return "ar_payment.TOTAL_DISCOUNT";
    }
  
    /**
     * ar_payment.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use ArPaymentPeer.ar_payment.CASH_FLOW_TYPE_ID constant
     */
    public static String getArPayment_CashFlowTypeId()
    {
        return "ar_payment.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * ar_payment.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use ArPaymentPeer.ar_payment.LOCATION_ID constant
     */
    public static String getArPayment_LocationId()
    {
        return "ar_payment.LOCATION_ID";
    }
  
    /**
     * ar_payment.CF_STATUS
     * @return the column name for the CF_STATUS field
     * @deprecated use ArPaymentPeer.ar_payment.CF_STATUS constant
     */
    public static String getArPayment_CfStatus()
    {
        return "ar_payment.CF_STATUS";
    }
  
    /**
     * ar_payment.CF_DATE
     * @return the column name for the CF_DATE field
     * @deprecated use ArPaymentPeer.ar_payment.CF_DATE constant
     */
    public static String getArPayment_CfDate()
    {
        return "ar_payment.CF_DATE";
    }
  
    /**
     * ar_payment.CASH_FLOW_ID
     * @return the column name for the CASH_FLOW_ID field
     * @deprecated use ArPaymentPeer.ar_payment.CASH_FLOW_ID constant
     */
    public static String getArPayment_CashFlowId()
    {
        return "ar_payment.CASH_FLOW_ID";
    }
  
    /**
     * ar_payment.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use ArPaymentPeer.ar_payment.CANCEL_BY constant
     */
    public static String getArPayment_CancelBy()
    {
        return "ar_payment.CANCEL_BY";
    }
  
    /**
     * ar_payment.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use ArPaymentPeer.ar_payment.CANCEL_DATE constant
     */
    public static String getArPayment_CancelDate()
    {
        return "ar_payment.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("ar_payment");
        TableMap tMap = dbMap.getTable("ar_payment");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("ar_payment.AR_PAYMENT_ID", "");
                          tMap.addColumn("ar_payment.AR_PAYMENT_NO", "");
                          tMap.addColumn("ar_payment.AR_PAYMENT_DATE", new Date());
                          tMap.addColumn("ar_payment.AR_PAYMENT_DUE_DATE", new Date());
                          tMap.addColumn("ar_payment.BANK_ID", "");
                          tMap.addColumn("ar_payment.BANK_ISSUER", "");
                          tMap.addColumn("ar_payment.CUSTOMER_ID", "");
                          tMap.addColumn("ar_payment.CUSTOMER_NAME", "");
                          tMap.addColumn("ar_payment.REFERENCE_NO", "");
                            tMap.addColumn("ar_payment.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("ar_payment.TOTAL_DOWN_PAYMENT", bd_ZERO);
                            tMap.addColumn("ar_payment.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("ar_payment.USER_NAME", "");
                          tMap.addColumn("ar_payment.REMARK", "");
                          tMap.addColumn("ar_payment.CURRENCY_ID", "");
                            tMap.addColumn("ar_payment.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("ar_payment.FISCAL_RATE", bd_ZERO);
                            tMap.addColumn("ar_payment.STATUS", Integer.valueOf(0));
                            tMap.addColumn("ar_payment.TOTAL_DISCOUNT", bd_ZERO);
                          tMap.addColumn("ar_payment.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("ar_payment.LOCATION_ID", "");
                            tMap.addColumn("ar_payment.CF_STATUS", Integer.valueOf(0));
                          tMap.addColumn("ar_payment.CF_DATE", new Date());
                          tMap.addColumn("ar_payment.CASH_FLOW_ID", "");
                          tMap.addColumn("ar_payment.CANCEL_BY", "");
                          tMap.addColumn("ar_payment.CANCEL_DATE", new Date());
          }
}

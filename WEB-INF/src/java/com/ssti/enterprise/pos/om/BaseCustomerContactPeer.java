package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.CustomerContactMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


  
/**
 */
public abstract class BaseCustomerContactPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "customer_contact";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CustomerContactMapBuilder.CLASS_NAME);
    }

      /** the column name for the CUSTOMER_CONTACT_ID field */
    public static final String CUSTOMER_CONTACT_ID;
      /** the column name for the CUSTOMER_ID field */
    public static final String CUSTOMER_ID;
      /** the column name for the CONTACT_NAME field */
    public static final String CONTACT_NAME;
      /** the column name for the CONTACT_PHONE field */
    public static final String CONTACT_PHONE;
      /** the column name for the CONTACT_MOBILE field */
    public static final String CONTACT_MOBILE;
      /** the column name for the CONTACT_EMAIL field */
    public static final String CONTACT_EMAIL;
      /** the column name for the CONTACT_JOB_TITLE field */
    public static final String CONTACT_JOB_TITLE;
      /** the column name for the CONTACT_DEPT field */
    public static final String CONTACT_DEPT;
      /** the column name for the CONTACT_BIRTH_PLACE field */
    public static final String CONTACT_BIRTH_PLACE;
      /** the column name for the CONTACT_BIRTH_DATE field */
    public static final String CONTACT_BIRTH_DATE;
      /** the column name for the CONTACT_RELIGION field */
    public static final String CONTACT_RELIGION;
      /** the column name for the CONTACT_REMARK field */
    public static final String CONTACT_REMARK;
      /** the column name for the CONTACT_SM1 field */
    public static final String CONTACT_SM1;
      /** the column name for the CONTACT_SM2 field */
    public static final String CONTACT_SM2;
      /** the column name for the CONTACT_SM3 field */
    public static final String CONTACT_SM3;
      /** the column name for the CONTACT_JSON field */
    public static final String CONTACT_JSON;
  
    static
    {
          CUSTOMER_CONTACT_ID = "customer_contact.CUSTOMER_CONTACT_ID";
          CUSTOMER_ID = "customer_contact.CUSTOMER_ID";
          CONTACT_NAME = "customer_contact.CONTACT_NAME";
          CONTACT_PHONE = "customer_contact.CONTACT_PHONE";
          CONTACT_MOBILE = "customer_contact.CONTACT_MOBILE";
          CONTACT_EMAIL = "customer_contact.CONTACT_EMAIL";
          CONTACT_JOB_TITLE = "customer_contact.CONTACT_JOB_TITLE";
          CONTACT_DEPT = "customer_contact.CONTACT_DEPT";
          CONTACT_BIRTH_PLACE = "customer_contact.CONTACT_BIRTH_PLACE";
          CONTACT_BIRTH_DATE = "customer_contact.CONTACT_BIRTH_DATE";
          CONTACT_RELIGION = "customer_contact.CONTACT_RELIGION";
          CONTACT_REMARK = "customer_contact.CONTACT_REMARK";
          CONTACT_SM1 = "customer_contact.CONTACT_SM1";
          CONTACT_SM2 = "customer_contact.CONTACT_SM2";
          CONTACT_SM3 = "customer_contact.CONTACT_SM3";
          CONTACT_JSON = "customer_contact.CONTACT_JSON";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CustomerContactMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CustomerContactMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  16;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.CustomerContact";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCustomerContactPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                  
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CUSTOMER_CONTACT_ID);
          criteria.addSelectColumn(CUSTOMER_ID);
          criteria.addSelectColumn(CONTACT_NAME);
          criteria.addSelectColumn(CONTACT_PHONE);
          criteria.addSelectColumn(CONTACT_MOBILE);
          criteria.addSelectColumn(CONTACT_EMAIL);
          criteria.addSelectColumn(CONTACT_JOB_TITLE);
          criteria.addSelectColumn(CONTACT_DEPT);
          criteria.addSelectColumn(CONTACT_BIRTH_PLACE);
          criteria.addSelectColumn(CONTACT_BIRTH_DATE);
          criteria.addSelectColumn(CONTACT_RELIGION);
          criteria.addSelectColumn(CONTACT_REMARK);
          criteria.addSelectColumn(CONTACT_SM1);
          criteria.addSelectColumn(CONTACT_SM2);
          criteria.addSelectColumn(CONTACT_SM3);
          criteria.addSelectColumn(CONTACT_JSON);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static CustomerContact row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            CustomerContact obj = (CustomerContact) cls.newInstance();
            CustomerContactPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      CustomerContact obj)
        throws TorqueException
    {
        try
        {
                obj.setCustomerContactId(row.getValue(offset + 0).asString());
                  obj.setCustomerId(row.getValue(offset + 1).asString());
                  obj.setContactName(row.getValue(offset + 2).asString());
                  obj.setContactPhone(row.getValue(offset + 3).asString());
                  obj.setContactMobile(row.getValue(offset + 4).asString());
                  obj.setContactEmail(row.getValue(offset + 5).asString());
                  obj.setContactJobTitle(row.getValue(offset + 6).asString());
                  obj.setContactDept(row.getValue(offset + 7).asString());
                  obj.setContactBirthPlace(row.getValue(offset + 8).asString());
                  obj.setContactBirthDate(row.getValue(offset + 9).asUtilDate());
                  obj.setContactReligion(row.getValue(offset + 10).asString());
                  obj.setContactRemark(row.getValue(offset + 11).asString());
                  obj.setContactSm1(row.getValue(offset + 12).asString());
                  obj.setContactSm2(row.getValue(offset + 13).asString());
                  obj.setContactSm3(row.getValue(offset + 14).asString());
                  obj.setContactJson(row.getValue(offset + 15).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCustomerContactPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                  
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CustomerContactPeer.row2Object(row, 1,
                CustomerContactPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCustomerContactPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CUSTOMER_CONTACT_ID, criteria.remove(CUSTOMER_CONTACT_ID));
                                                                                                                                                            
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CustomerContactPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                  
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(CustomerContact obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CustomerContact obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CustomerContact obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CustomerContact obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(CustomerContact) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CustomerContact obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(CustomerContact) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CustomerContact obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(CustomerContact) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CustomerContact obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCustomerContactPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CUSTOMER_CONTACT_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( CustomerContact obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CUSTOMER_CONTACT_ID, obj.getCustomerContactId());
              criteria.add(CUSTOMER_ID, obj.getCustomerId());
              criteria.add(CONTACT_NAME, obj.getContactName());
              criteria.add(CONTACT_PHONE, obj.getContactPhone());
              criteria.add(CONTACT_MOBILE, obj.getContactMobile());
              criteria.add(CONTACT_EMAIL, obj.getContactEmail());
              criteria.add(CONTACT_JOB_TITLE, obj.getContactJobTitle());
              criteria.add(CONTACT_DEPT, obj.getContactDept());
              criteria.add(CONTACT_BIRTH_PLACE, obj.getContactBirthPlace());
              criteria.add(CONTACT_BIRTH_DATE, obj.getContactBirthDate());
              criteria.add(CONTACT_RELIGION, obj.getContactReligion());
              criteria.add(CONTACT_REMARK, obj.getContactRemark());
              criteria.add(CONTACT_SM1, obj.getContactSm1());
              criteria.add(CONTACT_SM2, obj.getContactSm2());
              criteria.add(CONTACT_SM3, obj.getContactSm3());
              criteria.add(CONTACT_JSON, obj.getContactJson());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( CustomerContact obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CUSTOMER_CONTACT_ID, obj.getCustomerContactId());
                          criteria.add(CUSTOMER_ID, obj.getCustomerId());
                          criteria.add(CONTACT_NAME, obj.getContactName());
                          criteria.add(CONTACT_PHONE, obj.getContactPhone());
                          criteria.add(CONTACT_MOBILE, obj.getContactMobile());
                          criteria.add(CONTACT_EMAIL, obj.getContactEmail());
                          criteria.add(CONTACT_JOB_TITLE, obj.getContactJobTitle());
                          criteria.add(CONTACT_DEPT, obj.getContactDept());
                          criteria.add(CONTACT_BIRTH_PLACE, obj.getContactBirthPlace());
                          criteria.add(CONTACT_BIRTH_DATE, obj.getContactBirthDate());
                          criteria.add(CONTACT_RELIGION, obj.getContactReligion());
                          criteria.add(CONTACT_REMARK, obj.getContactRemark());
                          criteria.add(CONTACT_SM1, obj.getContactSm1());
                          criteria.add(CONTACT_SM2, obj.getContactSm2());
                          criteria.add(CONTACT_SM3, obj.getContactSm3());
                          criteria.add(CONTACT_JSON, obj.getContactJson());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerContact retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerContact retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerContact retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        CustomerContact retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CustomerContact retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (CustomerContact)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CUSTOMER_CONTACT_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



          
                                              
                
                

    /**
     * selects a collection of CustomerContact objects pre-filled with their
     * Customer objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CustomerContactPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinCustomer(Criteria criteria)
        throws TorqueException
    {
        setDbName(criteria);

        CustomerContactPeer.addSelectColumns(criteria);
        int offset = numColumns + 1;
        CustomerPeer.addSelectColumns(criteria);


                        criteria.addJoin(CustomerContactPeer.CUSTOMER_ID,
            CustomerPeer.CUSTOMER_ID);
        

                                                                                                                                                                                                                                                                                                        
        List rows = BasePeer.doSelect(criteria);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = CustomerContactPeer.getOMClass();
                    CustomerContact obj1 = CustomerContactPeer
                .row2Object(row, 1, omClass);
                     omClass = CustomerPeer.getOMClass();
                    Customer obj2 = CustomerPeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                CustomerContact temp_obj1 = (CustomerContact)results.get(j);
                Customer temp_obj2 = temp_obj1.getCustomer();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                              temp_obj2.addCustomerContact(obj1);
                              break;
                }
            }
                      if (newObject)
            {
                obj2.initCustomerContacts();
                obj2.addCustomerContact(obj1);
            }
                      results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ShiftMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ShiftMapBuilder";

    /**
     * Item
     * @deprecated use ShiftPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "shift";
    }

  
    /**
     * shift.SHIFT_ID
     * @return the column name for the SHIFT_ID field
     * @deprecated use ShiftPeer.shift.SHIFT_ID constant
     */
    public static String getShift_ShiftId()
    {
        return "shift.SHIFT_ID";
    }
  
    /**
     * shift.SHIFT_CODE
     * @return the column name for the SHIFT_CODE field
     * @deprecated use ShiftPeer.shift.SHIFT_CODE constant
     */
    public static String getShift_ShiftCode()
    {
        return "shift.SHIFT_CODE";
    }
  
    /**
     * shift.SHIFT_NAME
     * @return the column name for the SHIFT_NAME field
     * @deprecated use ShiftPeer.shift.SHIFT_NAME constant
     */
    public static String getShift_ShiftName()
    {
        return "shift.SHIFT_NAME";
    }
  
    /**
     * shift.START_HOUR
     * @return the column name for the START_HOUR field
     * @deprecated use ShiftPeer.shift.START_HOUR constant
     */
    public static String getShift_StartHour()
    {
        return "shift.START_HOUR";
    }
  
    /**
     * shift.END_HOUR
     * @return the column name for the END_HOUR field
     * @deprecated use ShiftPeer.shift.END_HOUR constant
     */
    public static String getShift_EndHour()
    {
        return "shift.END_HOUR";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("shift");
        TableMap tMap = dbMap.getTable("shift");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("shift.SHIFT_ID", "");
                          tMap.addColumn("shift.SHIFT_CODE", "");
                          tMap.addColumn("shift.SHIFT_NAME", "");
                          tMap.addColumn("shift.START_HOUR", "");
                          tMap.addColumn("shift.END_HOUR", "");
          }
}

package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.VendorPeer;
import com.ssti.enterprise.pos.om.VendorPriceList;
import com.ssti.enterprise.pos.om.VendorPriceListDetail;
import com.ssti.enterprise.pos.om.VendorPriceListDetailPeer;
import com.ssti.enterprise.pos.om.VendorPriceListPeer;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: VendorPriceListTool.java,v 1.12 2008/06/29 07:11:57 albert Exp $ <br>
 *
 * <pre>
 * $Log: VendorPriceListTool.java,v $
 * Revision 1.12  2008/06/29 07:11:57  albert
 * *** empty log message ***
 *
 * Revision 1.11  2007/06/07 17:23:58  albert
 * *** empty log message ***
 *
 * Revision 1.10  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class VendorPriceListTool extends BaseTool 
{
	public static List getAllVendorPriceList()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.addJoin(VendorPriceListPeer.VENDOR_ID,VendorPeer.VENDOR_ID);
		oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		return VendorPriceListPeer.doSelect(oCrit);
    }

	public static List getAllActiveVendorPriceList()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add (VendorPriceListPeer.CREATE_DATE, new Date(), Criteria.LESS_EQUAL);
		oCrit.add (VendorPriceListPeer.EXPIRED_DATE, DateUtil.getEndOfDayDate(new Date()), Criteria.GREATER_EQUAL);
		return VendorPriceListPeer.doSelect(oCrit);
    }    
    
	public static VendorPriceList getVendorPriceListByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(VendorPriceListPeer.VENDOR_PRICE_LIST_ID, _sID);
        List vData = VendorPriceListPeer.doSelect(oCrit);
        if (vData.size() > 0)
        {
			return (VendorPriceList) vData.get(0);
		}
		return null;
	}
	
	public static VendorPriceList getVendorPriceListByCode(String _sCode, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(VendorPriceListPeer.VENDOR_PL_CODE, _sCode);
	    List vData = VendorPriceListPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0)
	    {
			return (VendorPriceList) vData.get(0);
		}
		return null;
	}

	
	public static String getVendorPriceListIDByDetailID(String _sDetailID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_DETAIL_ID, _sDetailID);
        oCrit.setLimit (1);
        List vData = VendorPriceListDetailPeer.doSelect(oCrit);
        if (vData.size() > 0) 
        {
			return ((VendorPriceListDetail) vData.get(0)).getVendorPriceListId();
		}
		return "";
	}

	public static void importVendorItem (String _sID, String _sVendorID)
    	throws Exception
    {
		List vItem = ItemTool.getItemByPreferedVendorID(_sVendorID);

		for (int i = 0; i < vItem.size(); i++) 
		{
			VendorPriceListDetail oDet = new VendorPriceListDetail ();
			oDet.setVendorPriceListDetailId ( IDGenerator.generateSysID() );
			oDet.setVendorPriceListId (_sID);
			Item oItem = (Item) vItem.get(i);
			mapItemToDetail (oItem, oDet);
			oDet.save();
		}
	}

	public static VendorPriceListDetail getVendorPriceListDetailByID(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_DETAIL_ID, _sID);
	    return (VendorPriceListDetail) VendorPriceListDetailPeer.doSelect(oCrit).get(0);
	}

	public static List getDetailByVendorPriceListID(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, _sID);
	    oCrit.addAscendingOrderByColumn(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_DETAIL_ID);
	    return VendorPriceListDetailPeer.doSelect(oCrit);
	}

	//search
	public static LargeSelect findData( String _sID,
										int _iCondition, 
									    int _iSortBy, 
									    int _iViewLimit,
									    String _sKatID,
									    String _sKeywords )
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		
		oCrit.add (VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, _sID);
		
		//search by keywords		
		if (_sKeywords != null && !_sKeywords.equals("")) {
			if (_iCondition == 1) {
				oCrit.add(VendorPriceListDetailPeer.ITEM_CODE,
					(Object) SqlUtil.like (VendorPriceListDetailPeer.ITEM_CODE, _sKeywords), Criteria.CUSTOM);
			} 
			else if (_iCondition == 2) {
				oCrit.add(VendorPriceListDetailPeer.ITEM_NAME,
					(Object) SqlUtil.like (VendorPriceListDetailPeer.ITEM_NAME, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 3) {
				oCrit.add(VendorPriceListDetailPeer.PURCHASE_PRICE, Double.valueOf(_sKeywords).doubleValue(), Criteria.EQUAL);
			}
		}
		//define kategori criteria
		if (_sKatID != null && !_sKatID.equals(""))
		{
			List vID = KategoriTool.getChildIDList(_sKatID);
	        if (vID.size()  > 0) {
	        	oCrit.addJoin(VendorPriceListDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
		        vID.add(_sKatID);
		        oCrit.addIn(ItemPeer.KATEGORI_ID, vID);
	        }
	        else {
	        	oCrit.addJoin(VendorPriceListDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
		        oCrit.add(ItemPeer.KATEGORI_ID, _sKatID);
	        }
	    }		
	    //define sorting criteria
	    if (_iSortBy == 1) {
	    	oCrit.addAscendingOrderByColumn(VendorPriceListDetailPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
        	oCrit.addAscendingOrderByColumn(VendorPriceListDetailPeer.ITEM_NAME);
		}
		else if (_iSortBy == 4) {
			oCrit.addAscendingOrderByColumn(VendorPriceListDetailPeer.PURCHASE_PRICE);
		}
		return new LargeSelect(oCrit, _iViewLimit, "com.ssti.enterprise.pos.om.VendorPriceListDetailPeer");
	}

	
	public static void updateDetailByID(String _sDetailID, double _dPrice, String _sDiscountAmount)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(VendorPriceListDetailPeer.PURCHASE_PRICE, new BigDecimal(_dPrice));
	    oCrit.add(VendorPriceListDetailPeer.DISCOUNT_AMOUNT, _sDiscountAmount);
	    Criteria oSelectCrit = new Criteria();
	    oSelectCrit.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_DETAIL_ID, _sDetailID);
	    VendorPriceListDetailPeer.doUpdate(oSelectCrit,oCrit);
	}

	public static VendorPriceList addItemToVendorPriceList (String _sItemID, String _sVendorPriceListID)
    	throws Exception
    {
		VendorPriceList oVPL = getVendorPriceListByID(_sVendorPriceListID);
		if (oVPL != null)
		{
			Item oItem = ItemTool.getItemByID (_sItemID);
			if (oItem != null) 
			{
				VendorPriceListDetail oDet = new VendorPriceListDetail ();
				oDet.setVendorPriceListId (_sVendorPriceListID);
				mapItemToDetail (oItem, oDet);		
				oDet.setVendorPriceListDetailId (IDGenerator.generateSysID());			
				oDet.save();
				
				ItemVendorTool.save(_sItemID, oVPL.getVendorId(), false);
			}
		}
		return oVPL;
	}

	private static void mapItemToDetail (Item _oItem, VendorPriceListDetail _oDet)
		throws Exception
	{
		//Purchase Price in Vendor Price List should be in Purchase Unit
		//so we must multiply Last Purchase Price (in Base Unit) to Unit Conversion
		double dLastPurch = _oItem.getLastPurchasePrice ().doubleValue() * 
							_oItem.getUnitConversion().doubleValue();
		
		_oDet.setItemId  		(_oItem.getItemId  ());	
        _oDet.setItemCode		(_oItem.getItemCode());
        _oDet.setItemName       (_oItem.getItemName());
        _oDet.setPurchasePrice  (new BigDecimal(dLastPurch));
        _oDet.setDiscountAmount ("0");
	}

	public static boolean isItemExist(String _sItemID, String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
	    oCrit.add(VendorPriceListDetailPeer.ITEM_ID, _sItemID);
	    oCrit.add(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, _sID);
	    List vCust = VendorPriceListDetailPeer.doSelect(oCrit);
		if (vCust.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// method used to lookup detail by vendor and item id
	//////////////////////////////////////////////////////////////////////////////////
	
	public static VendorPriceListDetail getPriceListDetailByVendorID (String _sVendorID, 
																	  String _sItemID,
																	  Date _dTransDate) 
		throws Exception
	{
		if (_dTransDate == null) _dTransDate = new Date();
		//check if this customer has certain price list
		Criteria oCrit = new Criteria();
		oCrit.add (VendorPriceListPeer.VENDOR_ID, _sVendorID);
		oCrit.add (VendorPriceListPeer.CREATE_DATE, _dTransDate, Criteria.LESS_EQUAL);
		oCrit.add (VendorPriceListPeer.EXPIRED_DATE, DateUtil.getEndOfDayDate(_dTransDate), Criteria.GREATER_EQUAL);
		oCrit.add (VendorPriceListDetailPeer.ITEM_ID, _sItemID);
		oCrit.addJoin (VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, VendorPriceListPeer.VENDOR_PRICE_LIST_ID);

		List vData = VendorPriceListDetailPeer.doSelect (oCrit);
		if (vData.size() > 0) {
			return (VendorPriceListDetail) vData.get(0);
		}
		return null;
	}
	
	public static List getPriceListDetailByVendorID (String _sVendorID) 
		throws Exception
	{
		//check if this customer has certain price list
		Criteria oCrit = new Criteria();
		oCrit.add (VendorPriceListPeer.VENDOR_ID, _sVendorID);
		oCrit.add (VendorPriceListPeer.CREATE_DATE,new Date(), Criteria.LESS_EQUAL);
		oCrit.add (VendorPriceListPeer.EXPIRED_DATE,DateUtil.getEndOfDayDate(new Date()), Criteria.GREATER_EQUAL);
		oCrit.addJoin (VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, VendorPriceListPeer.VENDOR_PRICE_LIST_ID);

		List vData = VendorPriceListDetailPeer.doSelect (oCrit);
	    return vData;
	}
	
	public static List getListOfItemInPriceList(String _sVendorID)
	 throws Exception
	{
	    log.debug("getListOfItemInPriceList begin");
	    List vVendPriceDet = getPriceListDetailByVendorID(_sVendorID);
	    List vData = new ArrayList();
	    log.debug("vVendPriceDet.size():"+vVendPriceDet.size());
   		for (int i=0; i<vVendPriceDet.size(); i++)
		{
		    VendorPriceListDetail oVendPriceDetail = (VendorPriceListDetail)vVendPriceDet.get(i);
		    vData.add(oVendPriceDetail.getItemId());
		}
		log.debug("getListOfItemInPriceList end");
		return vData;
    }
	
	public static VendorPriceListDetail getPLIDAndItemID (String _sPLID, String _sItemID, Connection _oConn) 
	throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (VendorPriceListPeer.VENDOR_PRICE_LIST_ID, _sPLID);
		oCrit.add (VendorPriceListDetailPeer.ITEM_ID, _sItemID);
		oCrit.addJoin (VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID, VendorPriceListPeer.VENDOR_PRICE_LIST_ID);		
		List vData = VendorPriceListDetailPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0) {
			return (VendorPriceListDetail) vData.get(0);
		}
		return null;
	}

}
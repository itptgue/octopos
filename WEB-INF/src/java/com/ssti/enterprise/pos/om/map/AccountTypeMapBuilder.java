package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class AccountTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.AccountTypeMapBuilder";

    /**
     * Item
     * @deprecated use AccountTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "account_type";
    }

  
    /**
     * account_type.ACCOUNT_TYPE_ID
     * @return the column name for the ACCOUNT_TYPE_ID field
     * @deprecated use AccountTypePeer.account_type.ACCOUNT_TYPE_ID constant
     */
    public static String getAccountType_AccountTypeId()
    {
        return "account_type.ACCOUNT_TYPE_ID";
    }
  
    /**
     * account_type.ACCOUNT_TYPE
     * @return the column name for the ACCOUNT_TYPE field
     * @deprecated use AccountTypePeer.account_type.ACCOUNT_TYPE constant
     */
    public static String getAccountType_AccountType()
    {
        return "account_type.ACCOUNT_TYPE";
    }
  
    /**
     * account_type.ACCOUNT_TYPE_CODE
     * @return the column name for the ACCOUNT_TYPE_CODE field
     * @deprecated use AccountTypePeer.account_type.ACCOUNT_TYPE_CODE constant
     */
    public static String getAccountType_AccountTypeCode()
    {
        return "account_type.ACCOUNT_TYPE_CODE";
    }
  
    /**
     * account_type.ACCOUNT_TYPE_NAME
     * @return the column name for the ACCOUNT_TYPE_NAME field
     * @deprecated use AccountTypePeer.account_type.ACCOUNT_TYPE_NAME constant
     */
    public static String getAccountType_AccountTypeName()
    {
        return "account_type.ACCOUNT_TYPE_NAME";
    }
  
    /**
     * account_type.GL_TYPE
     * @return the column name for the GL_TYPE field
     * @deprecated use AccountTypePeer.account_type.GL_TYPE constant
     */
    public static String getAccountType_GlType()
    {
        return "account_type.GL_TYPE";
    }
  
    /**
     * account_type.NORMAL_BALANCE
     * @return the column name for the NORMAL_BALANCE field
     * @deprecated use AccountTypePeer.account_type.NORMAL_BALANCE constant
     */
    public static String getAccountType_NormalBalance()
    {
        return "account_type.NORMAL_BALANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("account_type");
        TableMap tMap = dbMap.getTable("account_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("account_type.ACCOUNT_TYPE_ID", "");
                            tMap.addColumn("account_type.ACCOUNT_TYPE", Integer.valueOf(0));
                          tMap.addColumn("account_type.ACCOUNT_TYPE_CODE", "");
                          tMap.addColumn("account_type.ACCOUNT_TYPE_NAME", "");
                            tMap.addColumn("account_type.GL_TYPE", Integer.valueOf(0));
                            tMap.addColumn("account_type.NORMAL_BALANCE", Integer.valueOf(0));
          }
}

package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.PaymentTypePeer;
import com.ssti.framework.tools.StringUtil;

public class PaymentTypeManager
{   
	private static Log log = LogFactory.getLog(PaymentTypeManager.class);
	 
    private static PaymentTypeManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private PaymentTypeManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static PaymentTypeManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (PaymentTypeManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new PaymentTypeManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllPaymentType ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("AllPaymentType");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addDescendingOrderByColumn (PaymentTypePeer.IS_DEFAULT);	
			oCrit.addDescendingOrderByColumn (PaymentTypePeer.IS_CREDIT_CARD);	
			oCrit.addAscendingOrderByColumn (PaymentTypePeer.PAYMENT_TYPE_CODE);	
			
			List vData = PaymentTypePeer.doSelect(oCrit);
			oCache.put(new Element ("AllPaymentType", (Serializable) vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	

	public PaymentType getPaymentTypeByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("PaymentType" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(PaymentTypePeer.PAYMENT_TYPE_ID, _sID);
				List vData = PaymentTypePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("PaymentType" + _sID, (Serializable)vData.get(0))); 
					return (PaymentType) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (PaymentType) oElement.getValue();
		}
	}

	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (PaymentType _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if (_oData != null && StringUtil.isNotEmpty(_oData.getPaymentTypeId())) 
        {
            oCache.remove("PaymentType" + _oData.getPaymentTypeId());
            oCache.put(new Element ("PaymentType" + _oData.getPaymentTypeId(), _oData));
        }        
        oCache.remove("AllPaymentType");
    }
	
	//for deleete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("PaymentType" + _sID);
        oCache.remove("AllPaymentType");
    } 
}
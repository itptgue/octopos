package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Principal
 */
public abstract class BasePrincipal extends BaseObject
{
    /** The Peer class */
    private static final PrincipalPeer peer =
        new PrincipalPeer();

        
    /** The value for the principalId field */
    private String principalId;
      
    /** The value for the principalCode field */
    private String principalCode;
                                                
    /** The value for the principalName field */
    private String principalName = "";
                                                
    /** The value for the description field */
    private String description = "";
                                                
    /** The value for the logo field */
    private String logo = "";
  
    
    /**
     * Get the PrincipalId
     *
     * @return String
     */
    public String getPrincipalId()
    {
        return principalId;
    }

                        
    /**
     * Set the value of PrincipalId
     *
     * @param v new value
     */
    public void setPrincipalId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.principalId, v))
              {
            this.principalId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrincipalCode
     *
     * @return String
     */
    public String getPrincipalCode()
    {
        return principalCode;
    }

                        
    /**
     * Set the value of PrincipalCode
     *
     * @param v new value
     */
    public void setPrincipalCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.principalCode, v))
              {
            this.principalCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrincipalName
     *
     * @return String
     */
    public String getPrincipalName()
    {
        return principalName;
    }

                        
    /**
     * Set the value of PrincipalName
     *
     * @param v new value
     */
    public void setPrincipalName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.principalName, v))
              {
            this.principalName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Logo
     *
     * @return String
     */
    public String getLogo()
    {
        return logo;
    }

                        
    /**
     * Set the value of Logo
     *
     * @param v new value
     */
    public void setLogo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.logo, v))
              {
            this.logo = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PrincipalId");
              fieldNames.add("PrincipalCode");
              fieldNames.add("PrincipalName");
              fieldNames.add("Description");
              fieldNames.add("Logo");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PrincipalId"))
        {
                return getPrincipalId();
            }
          if (name.equals("PrincipalCode"))
        {
                return getPrincipalCode();
            }
          if (name.equals("PrincipalName"))
        {
                return getPrincipalName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("Logo"))
        {
                return getLogo();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PrincipalPeer.PRINCIPAL_ID))
        {
                return getPrincipalId();
            }
          if (name.equals(PrincipalPeer.PRINCIPAL_CODE))
        {
                return getPrincipalCode();
            }
          if (name.equals(PrincipalPeer.PRINCIPAL_NAME))
        {
                return getPrincipalName();
            }
          if (name.equals(PrincipalPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PrincipalPeer.LOGO))
        {
                return getLogo();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPrincipalId();
            }
              if (pos == 1)
        {
                return getPrincipalCode();
            }
              if (pos == 2)
        {
                return getPrincipalName();
            }
              if (pos == 3)
        {
                return getDescription();
            }
              if (pos == 4)
        {
                return getLogo();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PrincipalPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PrincipalPeer.doInsert((Principal) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PrincipalPeer.doUpdate((Principal) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key principalId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPrincipalId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPrincipalId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPrincipalId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Principal copy() throws TorqueException
    {
        return copyInto(new Principal());
    }
  
    protected Principal copyInto(Principal copyObj) throws TorqueException
    {
          copyObj.setPrincipalId(principalId);
          copyObj.setPrincipalCode(principalCode);
          copyObj.setPrincipalName(principalName);
          copyObj.setDescription(description);
          copyObj.setLogo(logo);
  
                    copyObj.setPrincipalId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PrincipalPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Principal\n");
        str.append("---------\n")
           .append("PrincipalId          : ")
           .append(getPrincipalId())
           .append("\n")
           .append("PrincipalCode        : ")
           .append(getPrincipalCode())
           .append("\n")
           .append("PrincipalName        : ")
           .append(getPrincipalName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("Logo                 : ")
           .append(getLogo())
           .append("\n")
        ;
        return(str.toString());
    }
}

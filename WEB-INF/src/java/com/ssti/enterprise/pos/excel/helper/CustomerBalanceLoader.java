package com.ssti.enterprise.pos.excel.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2016-03-01
 * - update createTrans set dueDate again in trans after save, so aging for OB SI will be set by user 
 * 
 * </pre><br>
 */
public class CustomerBalanceLoader extends ARPBalanceLoader
{    
	private static Log log = LogFactory.getLog ( CustomerBalanceLoader.class );
	
	public static final String s_START_HEADING = "Customer Code";
	    
	public CustomerBalanceLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {				
		if (started (_oRow, _iIdx))	
		{
			setParam(_oRow, _iIdx);
			
			if (StringUtil.isNotEmpty(sCode))
			{				
				setObject();
			    
				boolean bValid = true;				
				log.debug ("Column " + _iIdx + " Customer Code : " + sCode);
				
				if (oCust == null) {bValid = false; oReject.append("Customer ").append(sCode).append(" Not Found\n");}
				bValid = validate(bValid);
				
                if(bValid)
				{
                	try
                	{
                		createTrans();
                		logSuccess();
                	}
                	catch (Exception _oEx)
                	{
                		logError(_oEx);
                	}
				}
                else
                {
                	logReject();
                }
			}
		}    
    }

	private void createTrans() throws Exception 
	{
		SalesTransaction oTR = new SalesTransaction();
		oTR.setCustomerId(oCust.getCustomerId());
		oTR.setCustomerName(oCust.getCustomerName());
		oTR.setLocationId(oLoc.getLocationId());
		oTR.setTransactionDate(dAsOf);
		oTR.setSalesId(sSalesID);
		oTR.setCashierName(sCreateBy);
		oTR.setPaymentTypeId(sTypeID);
		oTR.setPaymentTermId(sTermID);
		oTR.setTotalDiscountPct("0");
		oTR.setTotalAmount(bdAmt);		
		oTR.setPaymentAmount(bdAmt);
		oTR.setChangeAmount(bd_ZERO);
		oTR.setIsInclusiveTax(false);
		oTR.setIsTaxable(false);
		
    	oTR.setStatus(i_PROCESSED);
    	oTR.setCourierId("");
    	oTR.setShippingPrice(bd_ZERO);
    	oTR.setRemark(sRemark + "\nImported Opening Balance Trans : " + sInvNo + " by " + m_sUserName);
    	
    	oTR.setShipTo("");
    	oTR.setFobId("");
    	oTR.setFreightAccountId("");
    	
    	//updated by processPayment
    	oTR.setPaidAmount(bd_ZERO); 
    	oTR.setPaymentAmount(bd_ZERO); //updated when saved
    	oTR.setPaymentDate(oTR.getTransactionDate()); //updated when saved
    	oTR.setDueDate(dDue);
    	
	    oTR.setCurrencyId(oCurr.getCurrencyId());
	    oTR.setCurrencyRate(bdRate);
	    oTR.setFiscalRate(bdFisc);
	    oTR.setInvoiceNo(sInvNo);
	    
	    SalesTransactionDetail oTD = new SalesTransactionDetail();
	    
	    oTD.setItemId(oItem.getItemId());
	    oTD.setItemCode(oItem.getItemCode());
	    oTD.setItemName(oItem.getItemName());
	    oTD.setDescription(oItem.getDescription());
	    oTD.setQty(bd_ONE);
	    oTD.setQtyBase(bd_ONE);
	    oTD.setUnitId(oItem.getUnitId());
	    oTD.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));
	    oTD.setItemPrice(bdAmt);
	    oTD.setTaxId(oTax.getTaxId());
	    oTD.setTaxAmount(oTax.getAmount());
		oTD.setDiscountId("");
		oTD.setDiscount("0");
		oTD.setProjectId("");
		oTD.setDepartmentId("");
		//OTHER FIELD
		oTD.setDeliveryOrderId("");
		oTD.setDeliveryOrderDetailId("");
		oTD.setSubTotal(bdAmt);
		oTD.setSubTotalDisc(bd_ZERO);
		oTD.setSubTotalTax(bd_ZERO);
		oTD.setItemCost(bd_ZERO);
		oTD.setSubTotalCost(bd_ZERO);

		List vTD = new ArrayList(1);
		List vPMT = new ArrayList(1);
		vTD.add(oTD);

		 //single payment
		InvoicePayment oPMT 	 = new InvoicePayment();
        oPMT.setPaymentTypeId    (oTR.getPaymentTypeId());
        oPMT.setPaymentTermId    (oTR.getPaymentTermId());
        oPMT.setPaymentAmount    (oTR.getTotalAmount());
        oPMT.setTransTotalAmount (oTR.getTotalAmount());
        oPMT.setVoucherId        ("");
        vPMT.add(oPMT);
		
		TransactionTool.setHeaderProperties(oTR, vTD, null);
		
		if (dDue != null)
		{
			oTR.setDueDate(dDue);
		}
		
		TransactionTool.saveData(oTR, vTD, vPMT);
		
		if (dDue != null)
		{
			oTR.setDueDate(dDue);
			oTR.save();
		}
		
	}
}

package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class GlTransactionLogMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.GlTransactionLogMapBuilder";

    /**
     * Item
     * @deprecated use GlTransactionLogPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "gl_transaction_log";
    }

  
    /**
     * gl_transaction_log.GL_TRANSACTION_ID
     * @return the column name for the GL_TRANSACTION_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.GL_TRANSACTION_ID constant
     */
    public static String getGlTransactionLog_GlTransactionId()
    {
        return "gl_transaction_log.GL_TRANSACTION_ID";
    }
  
    /**
     * gl_transaction_log.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.TRANSACTION_TYPE constant
     */
    public static String getGlTransactionLog_TransactionType()
    {
        return "gl_transaction_log.TRANSACTION_TYPE";
    }
  
    /**
     * gl_transaction_log.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.TRANSACTION_ID constant
     */
    public static String getGlTransactionLog_TransactionId()
    {
        return "gl_transaction_log.TRANSACTION_ID";
    }
  
    /**
     * gl_transaction_log.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.TRANSACTION_NO constant
     */
    public static String getGlTransactionLog_TransactionNo()
    {
        return "gl_transaction_log.TRANSACTION_NO";
    }
  
    /**
     * gl_transaction_log.GL_TRANSACTION_NO
     * @return the column name for the GL_TRANSACTION_NO field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.GL_TRANSACTION_NO constant
     */
    public static String getGlTransactionLog_GlTransactionNo()
    {
        return "gl_transaction_log.GL_TRANSACTION_NO";
    }
  
    /**
     * gl_transaction_log.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.TRANSACTION_DATE constant
     */
    public static String getGlTransactionLog_TransactionDate()
    {
        return "gl_transaction_log.TRANSACTION_DATE";
    }
  
    /**
     * gl_transaction_log.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.PROJECT_ID constant
     */
    public static String getGlTransactionLog_ProjectId()
    {
        return "gl_transaction_log.PROJECT_ID";
    }
  
    /**
     * gl_transaction_log.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.DEPARTMENT_ID constant
     */
    public static String getGlTransactionLog_DepartmentId()
    {
        return "gl_transaction_log.DEPARTMENT_ID";
    }
  
    /**
     * gl_transaction_log.PERIOD_ID
     * @return the column name for the PERIOD_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.PERIOD_ID constant
     */
    public static String getGlTransactionLog_PeriodId()
    {
        return "gl_transaction_log.PERIOD_ID";
    }
  
    /**
     * gl_transaction_log.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.ACCOUNT_ID constant
     */
    public static String getGlTransactionLog_AccountId()
    {
        return "gl_transaction_log.ACCOUNT_ID";
    }
  
    /**
     * gl_transaction_log.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.CURRENCY_ID constant
     */
    public static String getGlTransactionLog_CurrencyId()
    {
        return "gl_transaction_log.CURRENCY_ID";
    }
  
    /**
     * gl_transaction_log.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.CURRENCY_RATE constant
     */
    public static String getGlTransactionLog_CurrencyRate()
    {
        return "gl_transaction_log.CURRENCY_RATE";
    }
  
    /**
     * gl_transaction_log.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.AMOUNT constant
     */
    public static String getGlTransactionLog_Amount()
    {
        return "gl_transaction_log.AMOUNT";
    }
  
    /**
     * gl_transaction_log.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.AMOUNT_BASE constant
     */
    public static String getGlTransactionLog_AmountBase()
    {
        return "gl_transaction_log.AMOUNT_BASE";
    }
  
    /**
     * gl_transaction_log.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.DESCRIPTION constant
     */
    public static String getGlTransactionLog_Description()
    {
        return "gl_transaction_log.DESCRIPTION";
    }
  
    /**
     * gl_transaction_log.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.USER_NAME constant
     */
    public static String getGlTransactionLog_UserName()
    {
        return "gl_transaction_log.USER_NAME";
    }
  
    /**
     * gl_transaction_log.DEBIT_CREDIT
     * @return the column name for the DEBIT_CREDIT field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.DEBIT_CREDIT constant
     */
    public static String getGlTransactionLog_DebitCredit()
    {
        return "gl_transaction_log.DEBIT_CREDIT";
    }
  
    /**
     * gl_transaction_log.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.CREATE_DATE constant
     */
    public static String getGlTransactionLog_CreateDate()
    {
        return "gl_transaction_log.CREATE_DATE";
    }
  
    /**
     * gl_transaction_log.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.LOCATION_ID constant
     */
    public static String getGlTransactionLog_LocationId()
    {
        return "gl_transaction_log.LOCATION_ID";
    }
  
    /**
     * gl_transaction_log.SUB_LEDGER_TYPE
     * @return the column name for the SUB_LEDGER_TYPE field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.SUB_LEDGER_TYPE constant
     */
    public static String getGlTransactionLog_SubLedgerType()
    {
        return "gl_transaction_log.SUB_LEDGER_TYPE";
    }
  
    /**
     * gl_transaction_log.SUB_LEDGER_ID
     * @return the column name for the SUB_LEDGER_ID field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.SUB_LEDGER_ID constant
     */
    public static String getGlTransactionLog_SubLedgerId()
    {
        return "gl_transaction_log.SUB_LEDGER_ID";
    }
  
    /**
     * gl_transaction_log.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use GlTransactionLogPeer.gl_transaction_log.UPDATE_DATE constant
     */
    public static String getGlTransactionLog_UpdateDate()
    {
        return "gl_transaction_log.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("gl_transaction_log");
        TableMap tMap = dbMap.getTable("gl_transaction_log");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("gl_transaction_log.GL_TRANSACTION_ID", "");
                            tMap.addColumn("gl_transaction_log.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction_log.TRANSACTION_ID", "");
                          tMap.addColumn("gl_transaction_log.TRANSACTION_NO", "");
                          tMap.addColumn("gl_transaction_log.GL_TRANSACTION_NO", "");
                          tMap.addColumn("gl_transaction_log.TRANSACTION_DATE", new Date());
                          tMap.addColumn("gl_transaction_log.PROJECT_ID", "");
                          tMap.addColumn("gl_transaction_log.DEPARTMENT_ID", "");
                          tMap.addColumn("gl_transaction_log.PERIOD_ID", "");
                          tMap.addColumn("gl_transaction_log.ACCOUNT_ID", "");
                          tMap.addColumn("gl_transaction_log.CURRENCY_ID", "");
                            tMap.addColumn("gl_transaction_log.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("gl_transaction_log.AMOUNT", bd_ZERO);
                            tMap.addColumn("gl_transaction_log.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("gl_transaction_log.DESCRIPTION", "");
                          tMap.addColumn("gl_transaction_log.USER_NAME", "");
                            tMap.addColumn("gl_transaction_log.DEBIT_CREDIT", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction_log.CREATE_DATE", new Date());
                          tMap.addColumn("gl_transaction_log.LOCATION_ID", "");
                            tMap.addColumn("gl_transaction_log.SUB_LEDGER_TYPE", Integer.valueOf(0));
                          tMap.addColumn("gl_transaction_log.SUB_LEDGER_ID", "");
                          tMap.addColumn("gl_transaction_log.UPDATE_DATE", new Date());
          }
}

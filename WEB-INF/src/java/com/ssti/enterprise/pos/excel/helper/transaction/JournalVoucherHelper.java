package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.om.JournalVoucherDetail;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;

public class JournalVoucherHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Journal Voucher Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {        
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        String sKeywords = (String) getFromParam(oParam, "Keywords", null);        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        int iStatus = (Integer) getFromParam(oParam, "Status", Integer.class);        
                
        List vData = JournalVoucherTool.findData(iCond, sKeywords, dStart, dEnd, iStatus);        
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	JournalVoucher oTR = (JournalVoucher) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = JournalVoucherTool.getDetailsByID(oTR.getJournalVoucherId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		JournalVoucherDetail oTD = (JournalVoucherDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {
    	/*
    	Trans No	
    	Transaction Date	
    	Remark	
    	*/
    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;	
		createHeaderCell(oWB, oRow, (short)iCol, "Create By"); iCol++;			
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		
    }
    
	private void createMasterContent(HSSFRow oRow, JournalVoucher oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getTransactionNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getTransactionDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getDescription()); iCol++;
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Account Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Rate"); iCol++;
    	createHeaderCell(oWB, oRow, (short)iCol, "Debit"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Credit"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Memo"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Project Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Department Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Sub Ledger Type"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Sub Ledger Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Sub Ledger Desc"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Reference No"); iCol++;
    }
    
    private void createDetailContent(HSSFRow oRow, JournalVoucherDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getAccountCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTD.getCurrencyId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getCurrencyRate().toString()); iCol++;
		
		if (oTD.getDebitCredit() == GlAttributes.i_DEBIT)
		{
			createCell(oWB, oRow, (short)iCol, oTD.getAmount().toString()); iCol++;
		}
		else
		{
			createCell(oWB, oRow, (short)iCol, bd_ZERO.toString()); iCol++;
		}
		if (oTD.getDebitCredit() == GlAttributes.i_CREDIT)
		{
			createCell(oWB, oRow, (short)iCol, oTD.getAmount().toString()); iCol++;
		}
		else
		{
			createCell(oWB, oRow, (short)iCol, bd_ZERO.toString()); iCol++;
		}
		createCell(oWB, oRow, (short)iCol, oTD.getMemo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTD.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, ""); iCol++;
		createCell(oWB, oRow, (short)iCol, ""); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTD.getSubLedgerType()); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTD.getSubLedgerCode()); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTD.getSubLedgerDesc()); iCol++;		
		createCell(oWB, oRow, (short)iCol, oTD.getReferenceNo()); iCol++;		
    }
}

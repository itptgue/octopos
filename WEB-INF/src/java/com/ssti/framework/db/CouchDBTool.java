package com.ssti.framework.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PostgreSQLTool.java,v 1.3 2008/08/17 02:17:02 albert Exp $ <br>
 *
 * <pre>
 * $Log: PostgreSQLTool.java,v $
 * Revision 1.3  2008/08/17 02:17:02  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/02/23 14:11:39  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public class CouchDBTool implements DBTool 
{
	static final Log log = LogFactory.getLog(CouchDBTool.class); 

	String couchURL = "http://localhost:5984";
	static final int NOT_FOUND = 404;
		
	public String getCouchURL() {
		return couchURL;
	}

	public void setCouchURL(String couchURL) 
	{
		if(StringUtil.isNotEmpty(couchURL))
		{
			this.couchURL = couchURL;
		}
	}

	@Override
	public void backup() throws Exception {
		
	}

	@Override
	public void restore() throws Exception {
		
	}

	@Override
	public void newDB() throws Exception {
		
	}

	public int newDB(String db, String url) 
		throws Exception 
	{
		setCouchURL(url);
		String dbUrl = db;
		if(!db.contains("http")) dbUrl = getCouchURL() + "/" + db;
		try 
		{
			HttpResponse getResp = Unirest.get(dbUrl).asJson();
			if(getResp.getStatus() == NOT_FOUND)
			{
				HttpResponse putResp = Unirest.put(dbUrl).asJson();
				return putResp.getStatus();
			}
			return getResp.getStatus();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return -1;		
	}
	
	public int save(String db, String doc, String url) 
		throws Exception 
	{
		setCouchURL(url);
		JSONObject json = new JSONObject(doc);			
		if(json != null && StringUtil.isNotEmpty(json.getString("_id")))
		{
			String dbUrl = db;
			if(!db.contains("http")) dbUrl = getCouchURL() + "/" + db;		

			String docUrl = dbUrl + "/" + json.getString("_id");
			log.debug("save: CouchDB doc URL: " + docUrl);
			try 
			{
				HttpResponse getResp = Unirest.get(docUrl).asJson();
				if(getResp.getStatus() != 404)
				{
					JSONObject exist = ((JsonNode) getResp.getBody()).getObject();					
					json.put("_rev", exist.getString("_rev"));					
				}
				HttpResponse putResp = Unirest.put(docUrl).body(json).asJson();
				return putResp.getStatus();
			} 
			catch (Exception e) 
			{
				log.error(e);
				e.printStackTrace();
			}
		}
		return -1;		
	}
	
	public int updateValue(String db, String id, String key, String newval, String url) 
		throws Exception 
	{
		setCouchURL(url);
		if(StringUtil.isNotEmpty(id))
		{
			String dbUrl = db;
			if(!db.contains("http")) dbUrl = getCouchURL() + "/" + db;		

			String docUrl = dbUrl + "/" + id;
			log.info("updateValue: CouchDB doc URL: " + docUrl);
			try 
			{
				HttpResponse getResp = Unirest.get(docUrl).asJson();
				if(getResp.getStatus() != 404)
				{
					JSONObject exist = ((JsonNode) getResp.getBody()).getObject();					
					exist.put("_rev", exist.getString("_rev"));	
					exist.put(key, newval);					
					HttpResponse putResp = Unirest.put(docUrl).body(exist).asJson();
					return putResp.getStatus();
				}
				return getResp.getStatus();
			} 
			catch (Exception e) 
			{
				log.error(e);
				e.printStackTrace();
			}
		}
		return -1;		
	}
	
	@Override
	public String getResult() {
		return null;
	}

}

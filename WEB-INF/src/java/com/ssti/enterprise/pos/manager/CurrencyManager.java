package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyDetail;
import com.ssti.enterprise.pos.om.CurrencyDetailPeer;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.framework.tools.StringUtil;

public class CurrencyManager
{   
	private static Log log = LogFactory.getLog(CurrencyManager.class);
	 
    private static CurrencyManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private CurrencyManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static CurrencyManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (CurrencyManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new CurrencyManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllCurrency ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllCurrency");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addDescendingOrderByColumn (CurrencyPeer.IS_DEFAULT);	
			List vData = CurrencyPeer.doSelect(oCrit);
			oCache.put(new Element ("AllCurrency", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	

	public Currency getCurrencyByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Currency" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CurrencyPeer.CURRENCY_ID, _sID);
				List vData = CurrencyPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("Currency" + _sID, (Serializable)vData.get(0))); 
					return (Currency) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (Currency) oElement.getValue();
		}
	}

	public Currency getCurrencyByCode(String _sCode, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("CurrencyByCode" + _sCode);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CurrencyPeer.CURRENCY_CODE, _sCode);
				List vData = CurrencyPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("CurrencyByCode" + _sCode, (Serializable)vData.get(0))); 
					return (Currency) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (Currency) oElement.getValue();
		}
	}	
	
	public Currency getDefaultCurrency(Connection _oConn)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("DefaultCurrency");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(CurrencyPeer.IS_DEFAULT, true);
        	List vData = CurrencyPeer.doSelect(oCrit, _oConn);
			if (vData.size() > 0) {
				oCache.put(new Element ("DefaultCurrency", (Serializable)vData.get(0))); 
				return (Currency) vData.get(0);
			}
			return null;
		}
		else 
		{
			return (Currency) oElement.getValue();
		}
    }

    public CurrencyDetail getLatestDetail(String _sID, Connection _oConn)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("LatestDetail" + _sID);
		if (oElement == null)
		{
			Criteria oCrit = CurrencyTool.setDateCriteria (_sID, new Date());
			List vData = CurrencyDetailPeer.doSelect(oCrit, _oConn);
			if (vData.size() > 0) 
			{
				CurrencyDetail oDetail = (CurrencyDetail)vData.get(0);
				oCache.put(new Element("LatestDetail" + _sID, oDetail));
				return oDetail;
			}
			else
			{
				oCache.put(new Element("LatestDetail" + _sID, null));
			}
		}
		else
		{
			return (CurrencyDetail) oElement.getValue();
		}
		return null;
	}
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Currency _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getCurrencyId()) ) 
        {
            oCache.remove("Currency" + _oData.getCurrencyId());
            oCache.remove("CurrencyByCode" + _oData.getCurrencyCode());
            oCache.put(new Element ("Currency" + _oData.getCurrencyId(), _oData));
            oCache.put(new Element ("CurrencyByCode" + _oData.getCurrencyCode(), _oData));
            oCache.remove("LatestDetail" + _oData.getCurrencyId());
            oCache.remove("AllCurrency");
            oCache.remove("DefaultCurrency");
        }
        else           
        {
            oCache.removeAll();
        }
    }
	
	//for delete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if (StringUtil.isNotEmpty(_sID))
        {
            oCache.remove("Currency" + _sID);
            oCache.remove("LatestDetail" + _sID);
            oCache.remove("AllCurrency");
            oCache.remove("DefaultCurrency");        
        }
        else           
        {
            oCache.removeAll();
        }
    } 
}
package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PriceList
 */
public abstract class BasePriceList extends BaseObject
{
    /** The Peer class */
    private static final PriceListPeer peer =
        new PriceListPeer();

        
    /** The value for the priceListId field */
    private String priceListId;
      
    /** The value for the priceListCode field */
    private String priceListCode;
                                                
    /** The value for the customerId field */
    private String customerId = "";
      
    /** The value for the customerTypeId field */
    private String customerTypeId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the priceListType field */
    private int priceListType;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the expiredDate field */
    private Date expiredDate;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the PriceListId
     *
     * @return String
     */
    public String getPriceListId()
    {
        return priceListId;
    }

                                              
    /**
     * Set the value of PriceListId
     *
     * @param v new value
     */
    public void setPriceListId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.priceListId, v))
              {
            this.priceListId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PriceListDetail
        if (collPriceListDetails != null)
        {
            for (int i = 0; i < collPriceListDetails.size(); i++)
            {
                ((PriceListDetail) collPriceListDetails.get(i))
                    .setPriceListId(v);
            }
        }
                                }
  
    /**
     * Get the PriceListCode
     *
     * @return String
     */
    public String getPriceListCode()
    {
        return priceListCode;
    }

                        
    /**
     * Set the value of PriceListCode
     *
     * @param v new value
     */
    public void setPriceListCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.priceListCode, v))
              {
            this.priceListCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerTypeId
     *
     * @return String
     */
    public String getCustomerTypeId()
    {
        return customerTypeId;
    }

                        
    /**
     * Set the value of CustomerTypeId
     *
     * @param v new value
     */
    public void setCustomerTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerTypeId, v))
              {
            this.customerTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PriceListType
     *
     * @return int
     */
    public int getPriceListType()
    {
        return priceListType;
    }

                        
    /**
     * Set the value of PriceListType
     *
     * @param v new value
     */
    public void setPriceListType(int v) 
    {
    
                  if (this.priceListType != v)
              {
            this.priceListType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpiredDate
     *
     * @return Date
     */
    public Date getExpiredDate()
    {
        return expiredDate;
    }

                        
    /**
     * Set the value of ExpiredDate
     *
     * @param v new value
     */
    public void setExpiredDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.expiredDate, v))
              {
            this.expiredDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collPriceListDetails
     */
    protected List collPriceListDetails;

    /**
     * Temporary storage of collPriceListDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPriceListDetails()
    {
        if (collPriceListDetails == null)
        {
            collPriceListDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a PriceListDetail object to this object
     * through the PriceListDetail foreign key attribute
     *
     * @param l PriceListDetail
     * @throws TorqueException
     */
    public void addPriceListDetail(PriceListDetail l) throws TorqueException
    {
        getPriceListDetails().add(l);
        l.setPriceList((PriceList) this);
    }

    /**
     * The criteria used to select the current contents of collPriceListDetails
     */
    private Criteria lastPriceListDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPriceListDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPriceListDetails() throws TorqueException
    {
              if (collPriceListDetails == null)
        {
            collPriceListDetails = getPriceListDetails(new Criteria(10));
        }
        return collPriceListDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PriceList has previously
     * been saved, it will retrieve related PriceListDetails from storage.
     * If this PriceList is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPriceListDetails(Criteria criteria) throws TorqueException
    {
              if (collPriceListDetails == null)
        {
            if (isNew())
            {
               collPriceListDetails = new ArrayList();
            }
            else
            {
                        criteria.add(PriceListDetailPeer.PRICE_LIST_ID, getPriceListId() );
                        collPriceListDetails = PriceListDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PriceListDetailPeer.PRICE_LIST_ID, getPriceListId());
                            if (!lastPriceListDetailsCriteria.equals(criteria))
                {
                    collPriceListDetails = PriceListDetailPeer.doSelect(criteria);
                }
            }
        }
        lastPriceListDetailsCriteria = criteria;

        return collPriceListDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPriceListDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPriceListDetails(Connection con) throws TorqueException
    {
              if (collPriceListDetails == null)
        {
            collPriceListDetails = getPriceListDetails(new Criteria(10), con);
        }
        return collPriceListDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PriceList has previously
     * been saved, it will retrieve related PriceListDetails from storage.
     * If this PriceList is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPriceListDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPriceListDetails == null)
        {
            if (isNew())
            {
               collPriceListDetails = new ArrayList();
            }
            else
            {
                         criteria.add(PriceListDetailPeer.PRICE_LIST_ID, getPriceListId());
                         collPriceListDetails = PriceListDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PriceListDetailPeer.PRICE_LIST_ID, getPriceListId());
                             if (!lastPriceListDetailsCriteria.equals(criteria))
                 {
                     collPriceListDetails = PriceListDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPriceListDetailsCriteria = criteria;

         return collPriceListDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PriceList is new, it will return
     * an empty collection; or if this PriceList has previously
     * been saved, it will retrieve related PriceListDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PriceList.
     */
    protected List getPriceListDetailsJoinPriceList(Criteria criteria)
        throws TorqueException
    {
                    if (collPriceListDetails == null)
        {
            if (isNew())
            {
               collPriceListDetails = new ArrayList();
            }
            else
            {
                              criteria.add(PriceListDetailPeer.PRICE_LIST_ID, getPriceListId());
                              collPriceListDetails = PriceListDetailPeer.doSelectJoinPriceList(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PriceListDetailPeer.PRICE_LIST_ID, getPriceListId());
                                    if (!lastPriceListDetailsCriteria.equals(criteria))
            {
                collPriceListDetails = PriceListDetailPeer.doSelectJoinPriceList(criteria);
            }
        }
        lastPriceListDetailsCriteria = criteria;

        return collPriceListDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PriceListId");
              fieldNames.add("PriceListCode");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerTypeId");
              fieldNames.add("LocationId");
              fieldNames.add("PriceListType");
              fieldNames.add("CreateDate");
              fieldNames.add("ExpiredDate");
              fieldNames.add("Remark");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PriceListId"))
        {
                return getPriceListId();
            }
          if (name.equals("PriceListCode"))
        {
                return getPriceListCode();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerTypeId"))
        {
                return getCustomerTypeId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("PriceListType"))
        {
                return Integer.valueOf(getPriceListType());
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("ExpiredDate"))
        {
                return getExpiredDate();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PriceListPeer.PRICE_LIST_ID))
        {
                return getPriceListId();
            }
          if (name.equals(PriceListPeer.PRICE_LIST_CODE))
        {
                return getPriceListCode();
            }
          if (name.equals(PriceListPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(PriceListPeer.CUSTOMER_TYPE_ID))
        {
                return getCustomerTypeId();
            }
          if (name.equals(PriceListPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PriceListPeer.PRICE_LIST_TYPE))
        {
                return Integer.valueOf(getPriceListType());
            }
          if (name.equals(PriceListPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(PriceListPeer.EXPIRED_DATE))
        {
                return getExpiredDate();
            }
          if (name.equals(PriceListPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(PriceListPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPriceListId();
            }
              if (pos == 1)
        {
                return getPriceListCode();
            }
              if (pos == 2)
        {
                return getCustomerId();
            }
              if (pos == 3)
        {
                return getCustomerTypeId();
            }
              if (pos == 4)
        {
                return getLocationId();
            }
              if (pos == 5)
        {
                return Integer.valueOf(getPriceListType());
            }
              if (pos == 6)
        {
                return getCreateDate();
            }
              if (pos == 7)
        {
                return getExpiredDate();
            }
              if (pos == 8)
        {
                return getRemark();
            }
              if (pos == 9)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PriceListPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PriceListPeer.doInsert((PriceList) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PriceListPeer.doUpdate((PriceList) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPriceListDetails != null)
            {
                for (int i = 0; i < collPriceListDetails.size(); i++)
                {
                    ((PriceListDetail) collPriceListDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key priceListId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPriceListId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPriceListId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPriceListId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PriceList copy() throws TorqueException
    {
        return copyInto(new PriceList());
    }
  
    protected PriceList copyInto(PriceList copyObj) throws TorqueException
    {
          copyObj.setPriceListId(priceListId);
          copyObj.setPriceListCode(priceListCode);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerTypeId(customerTypeId);
          copyObj.setLocationId(locationId);
          copyObj.setPriceListType(priceListType);
          copyObj.setCreateDate(createDate);
          copyObj.setExpiredDate(expiredDate);
          copyObj.setRemark(remark);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setPriceListId((String)null);
                                                                  
                                      
                            
        List v = getPriceListDetails();
        for (int i = 0; i < v.size(); i++)
        {
            PriceListDetail obj = (PriceListDetail) v.get(i);
            copyObj.addPriceListDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PriceListPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PriceList\n");
        str.append("---------\n")
           .append("PriceListId          : ")
           .append(getPriceListId())
           .append("\n")
           .append("PriceListCode        : ")
           .append(getPriceListCode())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerTypeId       : ")
           .append(getCustomerTypeId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("PriceListType        : ")
           .append(getPriceListType())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("ExpiredDate          : ")
           .append(getExpiredDate())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.2  2008/03/03 02:21:00  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/07/02 15:37:32  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CreditMemoLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( CreditMemoLoader.class );

	private static final String s_START_HEADING = "Trans No";	

	public CreditMemoLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		
		CreditMemo oTR = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader)
    			{
    				bTransHeader = true;    				
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && !sValue.equals(s_START_HEADING) && bTransHeader)
    			{    				
    				m_bTransError = false;    				
    				m_iTotalTrans++;
    				
    				oTR = new CreditMemo();
    				mapTrans (oRow, oTR);

    				log.debug("ROW : " + iRow + " TR " + oTR);
    				
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			CreditMemoTool.saveCM(oTR, null);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;	
    			}
    			else 
    			{
    				logInvalidFile();
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, CreditMemo _oTR)
    	throws Exception
    {    	
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
    	String sCustomerCode = getString(_oRow, iCol); iCol++;
    	Customer oCust = CustomerTool.getCustomerByCode(sCustomerCode);
    	if (oCust == null) {logError("Customer Code", sCustomerCode);}
    	else 
    	{
    		_oTR.setCustomerId(oCust.getCustomerId()); 
    		_oTR.setCustomerName(oCust.getCustomerName());
    	}

    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setTransactionDate(dTrans);}

    	BigDecimal dAmount = getBigDecimal(_oRow, iCol); iCol++;
    	if (dAmount == null || dAmount.doubleValue() == 0) {logError("Amount", dAmount);}
    	else {_oTR.setAmount(dAmount);}
    	
    	String sCurrencyCode = getString(_oRow, iCol); iCol++;
    	BigDecimal dRate = getBigDecimal(_oRow, iCol); iCol++;
    	BigDecimal dFiscal = getBigDecimal(_oRow, iCol); iCol++;
    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurrencyCode);
    	if (oCurr == null) 
    	{
    		_oTR.setCurrencyId(CurrencyTool.getDefaultCurrency(null).getCurrencyId());
    		_oTR.setCurrencyRate(bd_ONE);
    		_oTR.setFiscalRate(bd_ONE);
    	}
    	else 
    	{
    		_oTR.setCurrencyId(oCurr.getCurrencyId()); 
    		if (oCurr.getIsDefault())
    		{
        		_oTR.setCurrencyRate(bd_ONE);
        		_oTR.setFiscalRate(bd_ONE);    			
    		}
    		else
    		{
    			if (dRate != null && dFiscal != null && dRate.doubleValue() != 0 && dFiscal.doubleValue() != 0)
    			{
	        		_oTR.setCurrencyRate(dRate);
	        		_oTR.setFiscalRate(dFiscal);    			    			
    			}
    			else
    			{
    				logError("Currency Rate", dRate);
    				logError("Fiscal Rate", dFiscal);
    			}
    		}
    	}

    	
    	String sAccount = getString(_oRow, iCol); iCol++;
    	Account oAccount = AccountTool.getAccountByCode(sAccount);
    	if (oAccount == null) {logError("Account", sAccount);}  
    	else{_oTR.setAccountId(oAccount.getAccountId());}    	

    	String sCrossAcc = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sCrossAcc))
    	{
        	Account oCrossAcc = AccountTool.getAccountByCode(sCrossAcc);
    		if (oCrossAcc == null) {logError("Cross Account", sCrossAcc);}  
        	else{_oTR.setCrossAccountId(oCrossAcc.getAccountId());}    	
    	}
    	else
    	{
    		_oTR.setCrossAccountId("");
    	}
    	
    	String sCreateBy = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sCreateBy)) {logError("Create By", sCreateBy);}
    	else {_oTR.setUserName(sCreateBy);}
		
    	String sRemark = getString(_oRow, iCol); iCol++;
    	if (sRemark != null) _oTR.setRemark(sRemark); else _oTR.setRemark("");
    	
    	String sCashFlowType = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sCashFlowType))
    	{
    		CashFlowType oCashFlowType = CashFlowTypeTool.getTypeByCode(sCashFlowType, null);
    		if (oCashFlowType == null) {logError("CashFlowType", sCashFlowType);}  
    		else{_oTR.setCashFlowTypeId(oCashFlowType.getCashFlowTypeId());}    	
    	}
    	else
    	{
    		_oTR.setCashFlowTypeId("");
    	}

    	String sLocationCode = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sLocationCode))
    	{
	    	Location oLocation = LocationTool.getLocationByCode(sLocationCode);
	    	if (oLocation == null) {logError("Location Code", sLocationCode);} 
	    	else {_oTR.setLocationId(oLocation.getLocationId());}
    	}
    	else
    	{
    		_oTR.setLocationId("");
    	}
    	
    	String sBankCode = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sBankCode))
    	{
	    	Bank oBank = BankTool.getBankByCode(sBankCode);
	    	if (oBank == null) {logError("Bank Code", sBankCode);} 
	    	else 
	    	{
	    		if (oBank.getCurrencyId().equals(_oTR.getCurrencyId()))
	    		{
	    			_oTR.setBankId(oBank.getBankId());
	    		}
	    		else
	    		{
	    			logError("Bank Code (Currency Not Match)", sBankCode);
	    		}
	    	}
    	}
    	else
    	{
    		_oTR.setBankId("");
    	}
    	
    	String sBankIssuer = getString(_oRow, iCol); iCol++;
    	if (sBankIssuer != null) _oTR.setBankIssuer(sBankIssuer); else _oTR.setBankIssuer("");
    	
    	String sReferenceNo = getString(_oRow, iCol); iCol++;
    	if (sReferenceNo != null) _oTR.setReferenceNo(sReferenceNo); else _oTR.setReferenceNo("");

    	Date dDue = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dDue == null) {logError("Due Date", dDue);}
    	else {_oTR.setDueDate(dDue);}

    	//OTHER FIELD
    	if (!m_bTransError)
    	{
    		double dAmountBase = _oTR.getAmount().doubleValue() * _oTR.getCurrencyRate().doubleValue();
    		_oTR.setAmountBase(new BigDecimal(dAmountBase));
    		
    		_oTR.setCreditMemoNo("");
	    	_oTR.setStatus(i_MEMO_OPEN);	    
	    	_oTR.setRemark(_oTR.getRemark() + setDesc(sTransNo, m_sUserName));
    	}
    }
}

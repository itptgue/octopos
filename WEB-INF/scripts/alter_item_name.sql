ALTER TABLE item ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE item ALTER COLUMN item_sku_name TYPE VARCHAR(100);
ALTER TABLE item ALTER COLUMN vendor_item_name TYPE VARCHAR(100);
ALTER TABLE item_group ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE price_list_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE vendor_price_list_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE quotation_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE sales_order_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE delivery_order_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE sales_transaction_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE sales_return_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE purchase_request_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE purchase_order_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE purchase_receipt_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE purchase_invoice_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE purchase_return_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE packing_list_detail ALTER COLUMN item_name TYPE VARCHAR(100);
ALTER TABLE medicine_sales_detail ALTER COLUMN item_name TYPE VARCHAR(100);




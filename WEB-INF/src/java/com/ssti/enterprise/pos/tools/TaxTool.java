package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.TaxManager;
import com.ssti.enterprise.pos.om.PurchaseTaxSerial;
import com.ssti.enterprise.pos.om.PurchaseTaxSerialPeer;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.TaxPeer;
import com.ssti.enterprise.pos.om.TaxSerial;
import com.ssti.enterprise.pos.om.TaxSerialPeer;
import com.ssti.framework.tools.SortingTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: TaxTool.java,v 1.19 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: TaxTool.java,v $
 * 
 * 2017-01-03
 * - change and override method savePurchaseSerial, add PurchaseTaxDate column 
 * 
 * </pre><br>
 */
public class TaxTool extends BaseTool 
{
	private static final Log log = LogFactory.getLog(TaxTool.class);
	
	public static List getAllTax()
		throws Exception
	{
	    return getAllTax (true);
	}

	public static List getAllTax(boolean _bIsSalesDefault)
		throws Exception
	{
		List vData = TaxManager.getInstance().getAllTax();
		Criteria oCrit = new Criteria();
		if (_bIsSalesDefault)
		{
			vData = SortingTool.sort(vData, "defaultSalesTax", i_DESC);
		}
		else 
		{
			vData = SortingTool.sort(vData, "defaultPurchaseTax", i_DESC);
	    }
		return vData;
	}
	
	public static Tax getTaxByID(String _sID)
    	throws Exception
    {
        return TaxManager.getInstance().getTaxByID(_sID, null);
	}

	public static Tax getTaxByID(String _sID, Connection _oConn)
		throws Exception
	{
	    return TaxManager.getInstance().getTaxByID(_sID, _oConn);
	}

	public static Tax getDefaultSalesTax()
    	throws Exception
    {
        return TaxManager.getInstance().getDefaultSalesTax();
	}

	public static Tax getDefaultPurchaseTax()
    	throws Exception
    {
        return TaxManager.getInstance().getDefaultSalesTax();
	}

	public static Tax getByCode(String _sCode)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(TaxPeer.TAX_CODE, _sCode);
	    List vData = TaxPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((Tax) vData.get(0));
		}
		return null;
	}
	
	public static String getIDByCode(String _sCode)
    	throws Exception
    {
		Tax oTax = getByCode(_sCode);
		if (oTax != null)
		{
			return oTax.getTaxId();
		}
		return "";
	}

	public static String getCodeByID(String _sID)
    	throws Exception
    {
		Tax oTax = getTaxByID(_sID);
		if (oTax != null) {
			return oTax.getTaxCode();
		}
		return "";
	}
	
	public static String getDescriptionByID(String _sID)
		throws Exception
	{
		Tax oTax = getTaxByID(_sID);
		if (oTax != null) {
			return oTax.getDescription();
		}
		return "";
	}

	public static BigDecimal getAmountByID(String _sID)
    	throws Exception
    {
		Tax oTax = getTaxByID(_sID);
		if (oTax != null) {
			return oTax.getAmount();
		}
		return bd_ZERO;
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// TAX SERIAL
	///////////////////////////////////////////////////////////////////////////
	
	public static String getSerial(String _sTransID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(TaxSerialPeer.TRANS_ID, _sTransID);
		List vData = TaxSerialPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((TaxSerial) vData.get(0)).getTaxSerial();
		}
		return null;
	}

	public static void saveSerial(String _sTransID, String _sSerialNo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(TaxSerialPeer.TRANS_ID, _sTransID);
		List vData = TaxSerialPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			TaxSerial oData = (TaxSerial) vData.get(0);
			oData.setTaxSerial(_sSerialNo);
			oData.save();
		}
		else
		{
			TaxSerial oData = new TaxSerial();
			oData.setTransId(_sTransID);
			oData.setTaxSerial(_sSerialNo);
			oData.save();			
		}
	}
	
	public static String lastSerial()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addDescendingOrderByColumn(TaxSerialPeer.TAX_SERIAL);
		oCrit.setLimit(1);
		List vData = TaxSerialPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((TaxSerial) vData.get(0)).getTaxSerial();
		}
		return null;
	}

    public static String lastSerial(String _sPrefix)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.addDescendingOrderByColumn(TaxSerialPeer.TAX_SERIAL);
        if (StringUtil.isNotEmpty(_sPrefix))
        {
            oCrit.add(TaxSerialPeer.TAX_SERIAL, (Object)(_sPrefix + "%"), Criteria.ILIKE);
        }
        oCrit.setLimit(1);
        List vData = TaxSerialPeer.doSelect(oCrit);
        if (vData.size() > 0)
        {
            return ((TaxSerial) vData.get(0)).getTaxSerial();
        }
        return null;
    }    
    
    public static String lastSerialExclPrefix(String _sPrefix,int _iPrefLen)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        if (StringUtil.isNotEmpty(_sPrefix))
        {
            oCrit.add(TaxSerialPeer.TAX_SERIAL, (Object)("%" + _sPrefix + "%"), Criteria.ILIKE);
        }
        oCrit.addDescendingOrderByColumn(TaxSerialPeer.TAX_SERIAL);
        List vData = TaxSerialPeer.doSelect(oCrit);
        if (vData.size() > 0)
        {
            String[] aSer = new String[vData.size()];
            for (int i = 0; i < vData.size(); i++)
            {
                TaxSerial oTax = (TaxSerial)vData.get(i);
                String sSerial = oTax.getTaxSerial();                
                System.out.print("Serial No:" + sSerial);
                sSerial = sSerial.substring(_iPrefLen);
                System.out.println(" w/o Pref:" + sSerial);
                aSer[i] = sSerial;
            }
            Arrays.sort(aSer);
            return aSer[aSer.length - 1];
        }
        return null;
    }  
    
    
	public static String newSerial()
		throws Exception
	{
		String sLast = lastSerial();
		log.debug("Last TAX SERIAL: " + sLast);
		if (sLast != null)
		{
			int iLength = sLast.length();
			int iNumber = CONFIG.getInt("tax.serial.length", 7);
			if (iLength > iNumber)
			{
				try
				{
					String sInc = sLast.substring(iLength - iNumber);
					log.debug ("sInc " + sInc);
					
					String sFix = sLast.substring(0, iLength - iNumber);
					log.debug (sFix);
	
					int iInc = Integer.parseInt(sInc); //probably throws number format
					iInc ++;
					return sFix + StringUtil.formatNumberString(Integer.valueOf(iInc).toString(), iNumber);
				}
				catch (Exception _oEx) //invalid NUMBER
				{
					log.error("ERROR: Generating TAX Serial " + _oEx.getMessage());
				}
			}
		}
		return sLast;
	}	
	
	///////////////////////////////////////////////////////////////////////////
	// PURCHASE TAX SERIAL
	///////////////////////////////////////////////////////////////////////////
	
	public static String getPurchaseSerial(String _sTransID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseTaxSerialPeer.TRANS_ID, _sTransID);
		List vData = PurchaseTaxSerialPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((PurchaseTaxSerial) vData.get(0)).getPurchaseTaxSerial();
		}
		return null;
	}
	
	public static PurchaseTaxSerial getPurchSerialOM(String _sTransID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseTaxSerialPeer.TRANS_ID, _sTransID);
		List vData = PurchaseTaxSerialPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((PurchaseTaxSerial) vData.get(0));
		}
		return null;
	}

	public static void savePurchaseSerial(String _sTransID, String _sSerialNo)
		throws Exception
	{
		savePurchaseSerial(_sTransID, _sSerialNo, new Date());
	}

	public static void savePurchaseSerial(String _sTransID, String _sSerialNo, Date _dTaxDate)
		throws Exception
	{
		savePurchaseSerial(_sTransID, _sSerialNo, new Date(), bd_ZERO, bd_ZERO);
	}
	
	public static void savePurchaseSerial(String _sTransID, String _sSerialNo, Date _dTaxDate, BigDecimal _dAmt, BigDecimal _dTax)
		throws Exception
	{
		if(_dTaxDate == null) _dTaxDate = new Date();
		
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseTaxSerialPeer.TRANS_ID, _sTransID);
		List vData = PurchaseTaxSerialPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			PurchaseTaxSerial oData = (PurchaseTaxSerial) vData.get(0);
			oData.setPurchaseTaxSerial(_sSerialNo);
			oData.setPurchaseTaxDate(_dTaxDate);
			oData.setAmount(_dAmt);
			oData.setAmountTax(_dTax);
			oData.save();
		}
		else
		{
			PurchaseTaxSerial oData = new PurchaseTaxSerial();
			oData.setTransId(_sTransID);
			oData.setPurchaseTaxSerial(_sSerialNo);
			oData.setPurchaseTaxDate(_dTaxDate);
			oData.setAmount(_dAmt);
			oData.setAmountTax(_dTax);
			oData.save();			
		}
	}
			
    //select box
	public static String createJSArray ()
		throws Exception
	{	
	 	List vData = getAllTax();

	 	StringBuilder oSB = new StringBuilder ();
		oSB.append ("var aOFTax = new Array (");
		oSB.append (vData.size());
		oSB.append (");\n");
	
	 	for (int i = 0; i < vData.size(); i++)
	 	{
		 	Tax oField = (Tax) vData.get(i);
			oSB.append ("aOFTax[");
			oSB.append (i);
			oSB.append ("] = new Array ('");
			oSB.append (oField.getAmount());
			oSB.append ("');\n");
	 	}
	 	return oSB.toString();
	}
	
	public static double calculateBeforeTax(String _sTaxID, boolean _bInclusive, double _dAmount)
	{
        if (_bInclusive)
        {
        	try 
        	{
        		Tax oTax = TaxTool.getTaxByID(_sTaxID);
            	if (oTax != null)
            	{
            		_dAmount = _dAmount / ((100 + oTax.getAmount().doubleValue()) / 100);
            	}				
			} 
        	catch (Exception _oEx) 
        	{
        		log.error(_oEx);
        	}        	
        }
        return _dAmount;
	}
}

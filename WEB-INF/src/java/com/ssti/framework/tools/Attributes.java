package com.ssti.framework.tools;

import java.math.BigDecimal;

import org.apache.commons.configuration.Configuration;
import org.apache.turbine.Turbine;

import com.ssti.framework.om.CardioConfig;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base of all attributes in system
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: Attributes.java,v 1.23 2008/06/29 07:11:30 albert Exp $ <br>
 * 
 * <pre>
 * $Log: Attributes.java,v $
 * Revision 1.23  2008/06/29 07:11:30  albert
 * *** empty log message ***
 *
 * Revision 1.22  2008/02/26 05:16:23  albert
 * *** empty log message ***
 *
 * Revision 1.21  2007/07/10 04:52:23  albert
 * *** empty log message ***
 *
 * Revision 1.20  2007/05/02 06:49:06  albert
 * *** empty log message ***
 *
 * Revision 1.19  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br> 
 * 
 */

public interface Attributes
{   	
	public static CardioConfig CONFIGDB = CardioConfigTool.getCardioConfig();
	public static Configuration CONFIG = Turbine.getConfiguration();
		
	//default hardcoded retailsoft db username & password
	public static final String s_DB_NAME = "pos";    	
	public static final String s_DB_USER = "root";
	public static final String s_DB_PWD  = "crossfire";	

	public static final String s_DEFAULT_DATE_FORMAT 	= CONFIGDB.getDefaultDateFormat();
	public static final String s_LONG_DATE_FORMAT 		= CONFIGDB.getLongDateFormat();//getString("long.date.format");	
	public static final String s_LONG_DATE_TIME_FORMAT 	= CONFIGDB.getLongDatetimeFormat();//getString("long.datetime.format");
	public static final String s_DEFAULT_TIME_FORMAT 	= CONFIGDB.getDefaultTimeFormat(); //getString("default.time.format");
	public static final String s_DATE_TIME_FORMAT 		= CONFIGDB.getDefaultDatetimeFormat();//getString("default.datetime.format");
	
	public static final String s_NUMBER_FORMAT = CONFIGDB.getDefaultNumberFormat();//getString("default.number.format");
	public static final String s_SIMPLE_FORMAT = CONFIGDB.getDefaultSimpleFormat();//("default.simple.format");

	public static final String s_ACCOUNTING_FORMAT = CONFIGDB.getDefaultAccFormat();//getString("default.accounting.format"); 
	public static final String s_ALIGNED_FORMAT    = CONFIGDB.getDefaultAlignedFormat();//getString("default.aligned.format");
		
	public static final String s_SQL_DATE_FORMAT 		= "yyyy-MM-dd HH:mm";
	public static final String s_FILE_DATE_TIME_FORMAT  = "yyyy-MM-dd_HH-mm";

	public static final String s_RUPIAH_FORMAT = "Rp#,##0.00";
	public static final String s_DOLLAR_FORMAT = "$#,##0.00";
	//"#,##0.##;(#,##0.##)";
	
	public static final String s_CONF_NAME = "conf"; 
	public static final String s_USER_PREF = "userPref";
	public static final String s_PRINT_PREF = "printPref";
	
	//Gender
	public static final int i_GENDER_MALE = 1;
	public static final int i_GENDER_FEMALE = 2;
	
	//CHARSET
	public static final String s_DEFAULT_ENCODING = "ISO-8859-1";
	
    public static final String s_HOME_ENV = "retailsoft.home";    
    
	public static final int i_ASC  = 1;
	public static final int i_DESC = 2;	
	
	public static final String s_LOCALHOST = "127.0.0.1";
	public static final String s_FILE_SEPARATOR = System.getProperty("file.separator");
	public static final String s_LINE_SEPARATOR = System.getProperty("line.separator");
	public static final String s_OS_NAME = System.getProperty("os.name");

	public static final String s_UNIX_LINE_SEPARATOR = "\r\n";
	public static final String s_WIN_LINE_SEPARATOR = "\n";
	
	public static final BigDecimal bd_ZERO = new BigDecimal(0);
	public static final BigDecimal bd_ONE = new BigDecimal(1);
	public static final String s_ZERO = "0";	
}

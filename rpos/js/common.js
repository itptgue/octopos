/**
 * Common function javascript
 * 2016-07-11
 * - move notif from GlobalMacros
 *
 * 2016-06-29
 * - Depends on jsGlobalVar in GlobalMacros
 * - move CustomerCommonJS function
 * - move VendorCommonJS function
 * - move home, logout, viewHelp && handleSubmit from TransCommonJS
 */
function highlightRow(rowid, isHigh, highCol, oriCol)
{
    var row = document.getElementById(rowid);
    if(isHigh){row.bgColor = highCol;}
    else {row.bgColor = oriCol;}
    return true;
}
function getKey(e)
{
    if(e) {if(window.event) {return e.keyCode;} else { return e.which;}}
}
function init()
{
    try
    {
        initPage();
    }
    catch (e)
    {
    }
}
function incSAS()
{
    var f = document.frm;
    if(f) var s = f._session_access_counter;
    if(s) s.value = parseInt(s.value) + 1;
}
function updateSAS()
{
    var s = document.frm._session_access_counter;
    var url = sTPL + 'transaction,ajax,SAS.vm';
    jQuery.get(url, function setSAS(data){
        if(s) s.value=data;
    });
}
function evalResponse(response)
{
    if(response) eval(response.responseText);
}
function replaceHtml(el, html)
{
	var oldEl = typeof el === "string" ? document.getElementById(el) : el;
	var newEl = oldEl.cloneNode(false);
	newEl.innerHTML = html;
	oldEl.parentNode.replaceChild(newEl, oldEl);
	return newEl;
}
function processing(_txt)
{
     if (!_txt || _txt == '')
     {
        _txt = 'Please wait, your request is being processed ...';
	 }
	 var b = document.body;
	 var X = (b.clientWidth/2 - 180) + "px";
	 var Y = (b.clientHeight/2 - 60) + "px";
	 var ts = 'top:' + Y + ';left:' + X + ';';
	 var S = "<table id='processBox' style='" + ts + "'><tr><td align=center>";
	 S += _txt + "<br><br><br><br></td></tr></table>";
	 var a = document.createElement("div");
	 a.innerHTML = S;
	 a.id = "lightbox";
	 b.appendChild(a);
}
function loading(_txt)
{
     if (!_txt || _txt == '')
     {
        _txt = 'Loading ...';
	 }
	 var b = document.body;
	 var X = (b.clientWidth - 85) + "px";
	 var S = "<table OnClick='unload();' id='loadingBar' style='top:0;left:" +  X +";'>";
	 S = S + "<tr><td align=center>" + _txt + "</td></tr></table>";

	 var a = document.createElement("div");
	 a.innerHTML = S ;
	 a.id = "lightbox";
	 a.setAttribute("OnClick","unload();");
	 b.appendChild(a);
}
function unload()
{
    var a = document.getElementById('lightbox');
    if(a) {
        a.style.display='none';
        document.body.removeChild(a);
    }
}
function setCookie(nm,val,days)
{
    var exdt=new Date();
    exdt.setDate(exdt.getDate() + days);
    var cval=escape(val) + ((days==null) ? "" : "; expires="+exdt.toUTCString());
    document.cookie=nm + "=" + cval;
}
function getCookie(nm)
{
    var ck = document.cookie;
    var idx = ck.indexOf(" " + nm + "=");
    if (idx == -1)
    {
        idx = ck.indexOf(nm + "=");
    }
    if (idx == -1)
    {
        ck = null;
    }
    else
    {
        idx = ck.indexOf("=", idx) + 1;
        var end = ck.indexOf(";", idx);
        if (end == -1)
        {
            end = ck.length;
        }
        ck = unescape(ck.substring(idx,end));
    }
    return ck;
}
function checkRegex (str, valexp)
{
	return String(str).search(valexp) != -1;
}
function checkEqual(str1,str2) {
	if (str1 != str2) {
		return false;
	}
	return true;
}
function checkRange(str, min, max) {
	var allValid = true;
	allValid = checkDigit (str);
	if (allValid) {
		if ((str < min) || (str > max)) {
			allValid = false;
		}
	}
	return allValid;
}
function checkEmpty(str)
{
	var	checkStr = str;
	var allValid = true;
	if (checkStr.length < 1) {
		allValid = false;
	}
	return allValid;
}
function checkDigit(str)
{
	var	checkStr = str;
	var allValid = true;
	for (i = 0; i < checkStr.length; i++)
	{
		ch = checkStr.charAt(i);
		if (ch < '0' ||	ch > '9')
		{
			allValid = false;
			break;
		}
	}
	return allValid;
}
function checkEmail(str)
{
	var	checkStr = str;
	var allValid = false;
	for (i = 0; i < checkStr.length; i++)
	{
		ch = checkStr.charAt(i);
		if (ch == '@')
		{
	         allValid = true;
		}
	}
	return allValid;
}
function checkValidChar(str)
{
	var	checkStr = str;
	var allValid = true;
	for (i = 0; i < checkStr.length; i++)
	{
		ch = checkStr.charAt(i);
		if (ch == "'" ||ch == '"' || ch == "`" || ch == ";" )
		{
			allValid = false;
			break;
		}
	}
	return allValid;
}
function checkLength(str, max)
{
	if (str.length > max)
	{
	    return false;
	}
	return true;
}
function isNumber(n)
{
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function viewPic(picFile)
{
    if(jQuery('#lightbox').length <= 0)
    {
        var lightbox ='<div id="lightbox"></div>';
    	jQuery('body').append(lightbox);
    }
    var tbs = '<table style="border:0px;width:100%;height:100%"><tr><td align="center" valign="middle">';
    var tbe = '</td></tr></table>';
    jQuery('#lightbox').html(tbs + '<img src="' + picFile + '" />' + tbe);
    jQuery('#lightbox').show();
    jQuery('#lightbox').on('click', function() {
        jQuery('#lightbox').hide();
    });
}
function viewFile(docFile)
{
    jQuery.modal('<iframe src="' + docFile + '" class="modal-iframe">',{overlayClose:true});
}
function setMultiCBtoCSV(cbname,fldname)
{
    if(document.frm)
    {
       var f = document.frm;
       var a = f.elements[cbname];
       var fld = '';
       eval('fld = f.' + fldname);
       fld.value = '';
       for (var i=0; i < a.length; i++)
       {
           if(a[i].checked)
           {
               if(i != 0 && fld.value != '')
               {
                   fld.value += ',';
               }
               fld.value = fld.value + '' + a[i].value;
           }
       }
    }
}
//if(!!window.EventSource)
//{
//   jQuery(window).load(function() {
//       sse = new EventSource('$!data.ContextPath/sse');
//       sse.addEventListener('message', function(evt) {
//         showNotification(evt.data);
//         console.log('msg: ' + evt.data);
//       }, false);
//       window.onbeforeunload = function(){
//          //sse.close();
//       };
//   });
//}
//function showNotification(msg)
//{
//    try{
//        var jmsg = JSON.parse(msg);
//        var disp = false;
//        if(jmsg.topic == 'newSO'    && $aclPerm.containsName("Create Delivery Order")  ) disp = true;
//        if(jmsg.topic == 'newPO'    && $aclPerm.containsName("Create Purchase Receipt")) disp = true;
//        if(jmsg.topic == 'newLogin' && $aclRole.containsName("Administrator")          ) disp = true;
//        if(jmsg.topic == 'general'  && jmsg.to == 'all' || jmsg.to == $sUSR) disp = true;
//        if(disp) jQuery.notify(jmsg.message, {className:"success",autoHide:false});
//    }
//    catch(e){
//        jQuery.notify(msg, 'success');
//    }
//}
function openNotifCenter(key, topic, msg)
{
    var param = 'key=' + key  + '&topic=' + topic + '&msg=' + msg;
    var url =  sTPL + 'transaction,notif,NotifCenter.vm?' + param;
    jQuery.modal(
       '<iframe src="'+ url +'" class="modal-iframe">', {overlayClose:true});
}
function sendNotification(idm, tpc, msg, snd, tor)
{
    var o = {
       topic: tpc,
       message: msg,
       sender: snd,
       to: tor
    };
    var jsmsg = JSON.stringify(o);
    jQuery.post('$!data.ContextPath/sse/', {op:'send', id: idm, msg: jsmsg});
}
function handleSubmit(e)
{
  if (getKey(e) == 13) document.frm.submit();
}
function logout(bValidate)
{
    if (confirm ('Logout Retailsoft ?'))
    {
        document.location.href = sCTX + '/weblayer/action/Logout';
    }
}
function home()
{
    document.location.href = sTPL + 'Index.vm';
}
function viewHelp()
{
	var hlpWnd = window.open ('$!preference.SysConfig.OnlineHelpUrl','hlpWnd',
	    'toolbar=no,scrollbars=yes,menubar=no,resizable=yes,width=960,height=640,left=20,top=20');
	hlpWnd.focus();
}
//customer common JS
function resetCustomer()
{
    var f = document.frm;
    f.CustomerId.value = '';
    f.CustomerCode.value = '';
    f.CustomerName.value = '';
}
function getCustomerDataByID()
{
    var url = sTPL + 'transaction,CustomerDataLookup.vm';
    var param = '&CustomerId=' + document.frm.CustomerId.value;
    //if(validateLimit) param = param + '&ValidateCreditLimit=true';
    jQuery.post(url, param, function(resp){eval(resp);});
}
var iMINLEN = 4;
function handleCustomerNameKeypress(e)
{
    var f = document.frm;
    var val = f.CustomerName.value;
    if(getKey(e) == 13){
        if(val && val.length < iMINLEN) {
            f.CustomerName.style.background = '#fcc';
        }
        else {
            f.CustomerName.style.background = '#fff';
            getCustomerDataByName();
        }
    }
}
function handleCustomerCodeKeypress(e)
{
    var f = document.frm;
    var val = f.CustomerCode.value;
    if(getKey(e) == 13){
        if(val && val.length < iMINLEN) {
            f.CustomerCode.style.background = '#fcc';
        }
        else {
            f.CustomerCode.style.background = '#fff';
            getCustomerDataByCode();
        }
    }
}
function getCustomerDataByName()
{
    var url = sTPL + 'transaction,CustomerNameLookup.vm';
    var param = '&CustomerName=' + document.frm.CustomerName.value;
    jQuery.post(url, param, function(resp){eval(resp);});
}
function getCustomerDataByCode()
{
    var url = sTPL + 'transaction,CustomerNameLookup.vm';
    var param = '&CustomerCode=' + document.frm.CustomerCode.value;
    jQuery.post(url, param, function(resp){eval(resp);});
}
function viewCustomerNameSelector()
{
    var url = sTPL + 'transaction,CustomerNameSelector.vm';
    if(sSCR != "undefined") url = url + '?from=' + sSCR;
    jQuery.modal('<iframe src="' + url + '" class="modal-iframe">', {overlayClose:true});
}
//vendor common JS
function handleVendorNameKeypress(e)
{
    var f = document.frm;
    if(f.VendorName.value != '' || f.VendorCode.value != '')
    {
        if(getKey(e) == 13)
        {
            f.VendorId.value = '';
            getVendorData();
        }
    }
}
function getVendorData()
{
    var f = document.frm;
    var url = sTPL + 'transaction,VendorDataLookup.vm';
    var param = 'VendorId=' + f.VendorId.value  + '&VendorName=' + f.VendorName.value +
                '&VendorCode=' + f.VendorCode.value;
    jQuery.post(url,param,function(resp){eval(resp);});
}
function viewVendorNameSelector ()
{
    var url = sTPL + 'transaction,VendorNameSelector.vm';
    if(sSCR != "undefined") url = url + '?from=' + sSCR;
    jQuery.modal('<iframe src="' + url + '" class="modal-iframe">', {overlayClose:true});
}
function getURLParam(name, url) 
{
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function checkNetwork(funcResult, param)
{                               
    if(!param) param = 'img=false&txt=true';
    jQuery.ajax({
        url: sCTX + '/weblayer/template/periodic,ConnectionStatus.vm?' + param,
        type: 'GET',
        timeout: 3000,
        success: funcResult,
        error: funcResult
    });
}
function showNetStat(resp, stat)
{
    if(stat != 'success')
    {        
        if(stat == 'error') stat = 'disconnected';
        jQuery('#inetStatus').text(' Offline (' + stat + ')').css('background', '#c33');
        jQuery('#btnSync').attr('disabled',true);                    
    }
    else
    {
        jQuery('#inetStatus').text(' Online ').css('background', 'green');
        jQuery('#btnSync').attr('disabled',false);                    
    }    
}

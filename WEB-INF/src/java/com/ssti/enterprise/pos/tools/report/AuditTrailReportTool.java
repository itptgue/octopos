package com.ssti.enterprise.pos.tools.report;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.model.TransactionPeerOM;
import com.ssti.enterprise.pos.om.ApPaymentPeer;
import com.ssti.enterprise.pos.om.ArPaymentPeer;
import com.ssti.enterprise.pos.om.CashFlowPeer;
import com.ssti.enterprise.pos.om.DeliveryOrderPeer;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.om.ItemTransferPeer;
import com.ssti.enterprise.pos.om.JournalVoucherPeer;
import com.ssti.enterprise.pos.om.PettyCashPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoicePeer;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceiptPeer;
import com.ssti.enterprise.pos.om.PurchaseReturnPeer;
import com.ssti.enterprise.pos.om.SalesOrderPeer;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.workingdogs.village.Record;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/report/SalesReportTool.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SalesReportTool.java,v 1.7 2009/05/04 02:05:09 albert Exp $
 *
 * $Log: SalesReportTool.java,v $
 * Revision 1.7  2009/05/04 02:05:09  albert
 * *** empty log message ***
 *
 */
public class AuditTrailReportTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( AuditTrailReportTool.class );
		
	static AuditTrailReportTool instance = null;
	
	public static synchronized AuditTrailReportTool getInstance() 
	{
		if (instance == null) instance = new AuditTrailReportTool();
		return instance;
	}	
	
	static Map mPEER = new HashMap(17);
	static Map mSCREEN = new HashMap(17);
	static Set mKEYS = new HashSet(17);
	static
	{
		mPEER.put("po", new PurchaseOrderPeer());
		mPEER.put("pr", new PurchaseReceiptPeer());
		mPEER.put("pi", new PurchaseInvoicePeer());
		mPEER.put("pt", new PurchaseReturnPeer());

		mPEER.put("so", new SalesOrderPeer());
		mPEER.put("do", new DeliveryOrderPeer());
		mPEER.put("si", new SalesTransactionPeer());
		mPEER.put("sr", new SalesReturnPeer());

		mPEER.put("ir", new IssueReceiptPeer());
		mPEER.put("it", new ItemTransferPeer());
		
		mPEER.put("ar", new ArPaymentPeer());
		mPEER.put("ap", new ApPaymentPeer());
		mPEER.put("pc", new PettyCashPeer());
		mPEER.put("cf", new CashFlowPeer());		
		mPEER.put("jv", new JournalVoucherPeer());
		
		mKEYS = mPEER.keySet();
	}
	
	static
	{
		mSCREEN.put("po", "PurchaseOrderTransaction.vm");
		mSCREEN.put("pr", "PurchaseReceiptTransaction.vm");
		mSCREEN.put("pi", "PurchaseInvoiceTransaction.vm");
		mSCREEN.put("pt", "PurchaseReturnTransaction.vm");

		mSCREEN.put("qt", "QuotationTrans.vm");
		mSCREEN.put("so", "SalesOrderTransaction.vm");
		mSCREEN.put("do", "DeliveryOrderTransaction.vm");
		mSCREEN.put("si", "SalesInvoiceMulti.vm");
		mSCREEN.put("sr", "SalesReturnTransaction.vm");

		mSCREEN.put("ir", "IssueReceiptTransaction.vm");
		mSCREEN.put("it", "ItemTransferTransaction.vm");
		mSCREEN.put("jc", "jobcost,JobCostingTransaction.vm");
		
		mSCREEN.put("ar", "ReceivablePayment.vm");
		mSCREEN.put("ap", "PayablePayment.vm");
		mSCREEN.put("pc", "PettyCashTransaction.vm");
		mSCREEN.put("cf", "CashManagement.vm");		
		mSCREEN.put("jv", "JournalVoucher.vm");
	}
	
	public static Set getKeys()
	{
		return mKEYS;
	}
	
	public static int countTrans(Date _dStart, Date _dEnd, String _sTrans, String _sCreateBy)
		throws Exception
	{
		TransactionPeerOM oPeer = (TransactionPeerOM) mPEER.get(_sTrans);
		log.debug(oPeer);
		if (oPeer != null)
		{
			String sTB = oPeer.getTableName();
			String sID = oPeer.getIDColumn();
			String sDT = oPeer.getDateColumn();
			String sCB = oPeer.getCreateByColumn();
			String sNO = oPeer.getNoColumn();
			
			
			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT COUNT(").append(sID).append(") FROM ")
				.append(sTB).append(" WHERE ").append(sDT).append(" >= '")
				.append(CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStart)))
				.append("' AND ").append(sDT).append(" <= '")
				.append(CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd)))
				.append("' AND ").append(sCB).append(" = '").append(_sCreateBy).append("' ");
			;
			
			log.debug(oSQL.toString());
			List vData = BasePeer.executeQuery(oSQL.toString());
			if(vData.size() > 0) 
			{	
				Record oRecord = (Record) vData.get(0);
				return oRecord.getValue(1).asInt();			
			}	
		}
		return 0;
	}
	
	public static List getTrans(Date _dStart, Date _dEnd, String _sTrans, String _sCreateBy)
		throws Exception
	{
		TransactionPeerOM oPeer = (TransactionPeerOM) mPEER.get(_sTrans);
		if (oPeer != null)
		{
			String sTB = oPeer.getTableName();
			String sID = oPeer.getIDColumn();
			String sDT = oPeer.getDateColumn();
			String sCB = oPeer.getCreateByColumn();
			String sNO = oPeer.getNoColumn();
			String sST = oPeer.getStatusColumn();
			
			StringBuilder oSQL = new StringBuilder();
			oSQL.append("SELECT ").append(sID).append(",").append(sNO).append(",").append(sDT).append(",").append(sST)
				.append(" FROM ").append(sTB).append(" WHERE ").append(sDT).append(" >= '")
				.append(CustomFormatter.formatSqlDate(DateUtil.getStartOfDayDate(_dStart)))
				.append("' AND ").append(sDT).append(" <= '")
				.append(CustomFormatter.formatSqlDate(DateUtil.getEndOfDayDate(_dEnd)))
				.append("' AND ").append(sCB).append(" = '").append(_sCreateBy).append("' ");
			;
			return BasePeer.executeQuery(oSQL.toString());	
		}
		return null;
	}
	
	public static String getScreen(String _sTrans)
	{
		return "transaction," + mSCREEN.get(_sTrans);
	}
}
package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to SalesReturn
 */
public abstract class BaseSalesReturn extends BaseObject
{
    /** The Peer class */
    private static final SalesReturnPeer peer =
        new SalesReturnPeer();

        
    /** The value for the salesReturnId field */
    private String salesReturnId;
                                          
    /** The value for the transactionType field */
    private int transactionType = 1;
                                                
    /** The value for the transactionId field */
    private String transactionId = "";
                                                
    /** The value for the locationId field */
    private String locationId = "";
      
    /** The value for the status field */
    private int status;
                                                
    /** The value for the transactionNo field */
    private String transactionNo = "";
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the returnNo field */
    private String returnNo;
      
    /** The value for the returnDate field */
    private Date returnDate;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the totalTax field */
    private BigDecimal totalTax;
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the createCreditMemo field */
    private boolean createCreditMemo;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
                                                
    /** The value for the salesId field */
    private String salesId = "";
                                                
    /** The value for the returnReasonId field */
    private String returnReasonId = "";
      
    /** The value for the cashReturn field */
    private boolean cashReturn;
                                                
    /** The value for the bankId field */
    private String bankId = "";
                                                
    /** The value for the bankIssuer field */
    private String bankIssuer = "";
      
    /** The value for the dueDate field */
    private Date dueDate;
                                                
    /** The value for the referenceNo field */
    private String referenceNo = "";
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
                                                
          
    /** The value for the roundingAmount field */
    private BigDecimal roundingAmount= bd_ZERO;
                                                
          
    /** The value for the returnedAmount field */
    private BigDecimal returnedAmount= bd_ZERO;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
                                                
    /** The value for the returnDiscAccId field */
    private String returnDiscAccId = "";
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the SalesReturnId
     *
     * @return String
     */
    public String getSalesReturnId()
    {
        return salesReturnId;
    }

                                              
    /**
     * Set the value of SalesReturnId
     *
     * @param v new value
     */
    public void setSalesReturnId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.salesReturnId, v))
              {
            this.salesReturnId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated SalesReturnDetail
        if (collSalesReturnDetails != null)
        {
            for (int i = 0; i < collSalesReturnDetails.size(); i++)
            {
                ((SalesReturnDetail) collSalesReturnDetails.get(i))
                    .setSalesReturnId(v);
            }
        }
                                }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnNo
     *
     * @return String
     */
    public String getReturnNo()
    {
        return returnNo;
    }

                        
    /**
     * Set the value of ReturnNo
     *
     * @param v new value
     */
    public void setReturnNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.returnNo, v))
              {
            this.returnNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnDate
     *
     * @return Date
     */
    public Date getReturnDate()
    {
        return returnDate;
    }

                        
    /**
     * Set the value of ReturnDate
     *
     * @param v new value
     */
    public void setReturnDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.returnDate, v))
              {
            this.returnDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalTax()
    {
        return totalTax;
    }

                        
    /**
     * Set the value of TotalTax
     *
     * @param v new value
     */
    public void setTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalTax, v))
              {
            this.totalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateCreditMemo
     *
     * @return boolean
     */
    public boolean getCreateCreditMemo()
    {
        return createCreditMemo;
    }

                        
    /**
     * Set the value of CreateCreditMemo
     *
     * @param v new value
     */
    public void setCreateCreditMemo(boolean v) 
    {
    
                  if (this.createCreditMemo != v)
              {
            this.createCreditMemo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesId
     *
     * @return String
     */
    public String getSalesId()
    {
        return salesId;
    }

                        
    /**
     * Set the value of SalesId
     *
     * @param v new value
     */
    public void setSalesId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesId, v))
              {
            this.salesId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnReasonId
     *
     * @return String
     */
    public String getReturnReasonId()
    {
        return returnReasonId;
    }

                        
    /**
     * Set the value of ReturnReasonId
     *
     * @param v new value
     */
    public void setReturnReasonId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.returnReasonId, v))
              {
            this.returnReasonId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashReturn
     *
     * @return boolean
     */
    public boolean getCashReturn()
    {
        return cashReturn;
    }

                        
    /**
     * Set the value of CashReturn
     *
     * @param v new value
     */
    public void setCashReturn(boolean v) 
    {
    
                  if (this.cashReturn != v)
              {
            this.cashReturn = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RoundingAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getRoundingAmount()
    {
        return roundingAmount;
    }

                        
    /**
     * Set the value of RoundingAmount
     *
     * @param v new value
     */
    public void setRoundingAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.roundingAmount, v))
              {
            this.roundingAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnedAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getReturnedAmount()
    {
        return returnedAmount;
    }

                        
    /**
     * Set the value of ReturnedAmount
     *
     * @param v new value
     */
    public void setReturnedAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.returnedAmount, v))
              {
            this.returnedAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnDiscAccId
     *
     * @return String
     */
    public String getReturnDiscAccId()
    {
        return returnDiscAccId;
    }

                        
    /**
     * Set the value of ReturnDiscAccId
     *
     * @param v new value
     */
    public void setReturnDiscAccId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.returnDiscAccId, v))
              {
            this.returnDiscAccId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collSalesReturnDetails
     */
    protected List collSalesReturnDetails;

    /**
     * Temporary storage of collSalesReturnDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initSalesReturnDetails()
    {
        if (collSalesReturnDetails == null)
        {
            collSalesReturnDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a SalesReturnDetail object to this object
     * through the SalesReturnDetail foreign key attribute
     *
     * @param l SalesReturnDetail
     * @throws TorqueException
     */
    public void addSalesReturnDetail(SalesReturnDetail l) throws TorqueException
    {
        getSalesReturnDetails().add(l);
        l.setSalesReturn((SalesReturn) this);
    }

    /**
     * The criteria used to select the current contents of collSalesReturnDetails
     */
    private Criteria lastSalesReturnDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getSalesReturnDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getSalesReturnDetails() throws TorqueException
    {
              if (collSalesReturnDetails == null)
        {
            collSalesReturnDetails = getSalesReturnDetails(new Criteria(10));
        }
        return collSalesReturnDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesReturn has previously
     * been saved, it will retrieve related SalesReturnDetails from storage.
     * If this SalesReturn is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getSalesReturnDetails(Criteria criteria) throws TorqueException
    {
              if (collSalesReturnDetails == null)
        {
            if (isNew())
            {
               collSalesReturnDetails = new ArrayList();
            }
            else
            {
                        criteria.add(SalesReturnDetailPeer.SALES_RETURN_ID, getSalesReturnId() );
                        collSalesReturnDetails = SalesReturnDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(SalesReturnDetailPeer.SALES_RETURN_ID, getSalesReturnId());
                            if (!lastSalesReturnDetailsCriteria.equals(criteria))
                {
                    collSalesReturnDetails = SalesReturnDetailPeer.doSelect(criteria);
                }
            }
        }
        lastSalesReturnDetailsCriteria = criteria;

        return collSalesReturnDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getSalesReturnDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getSalesReturnDetails(Connection con) throws TorqueException
    {
              if (collSalesReturnDetails == null)
        {
            collSalesReturnDetails = getSalesReturnDetails(new Criteria(10), con);
        }
        return collSalesReturnDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesReturn has previously
     * been saved, it will retrieve related SalesReturnDetails from storage.
     * If this SalesReturn is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getSalesReturnDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collSalesReturnDetails == null)
        {
            if (isNew())
            {
               collSalesReturnDetails = new ArrayList();
            }
            else
            {
                         criteria.add(SalesReturnDetailPeer.SALES_RETURN_ID, getSalesReturnId());
                         collSalesReturnDetails = SalesReturnDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(SalesReturnDetailPeer.SALES_RETURN_ID, getSalesReturnId());
                             if (!lastSalesReturnDetailsCriteria.equals(criteria))
                 {
                     collSalesReturnDetails = SalesReturnDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastSalesReturnDetailsCriteria = criteria;

         return collSalesReturnDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesReturn is new, it will return
     * an empty collection; or if this SalesReturn has previously
     * been saved, it will retrieve related SalesReturnDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SalesReturn.
     */
    protected List getSalesReturnDetailsJoinSalesReturn(Criteria criteria)
        throws TorqueException
    {
                    if (collSalesReturnDetails == null)
        {
            if (isNew())
            {
               collSalesReturnDetails = new ArrayList();
            }
            else
            {
                              criteria.add(SalesReturnDetailPeer.SALES_RETURN_ID, getSalesReturnId());
                              collSalesReturnDetails = SalesReturnDetailPeer.doSelectJoinSalesReturn(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(SalesReturnDetailPeer.SALES_RETURN_ID, getSalesReturnId());
                                    if (!lastSalesReturnDetailsCriteria.equals(criteria))
            {
                collSalesReturnDetails = SalesReturnDetailPeer.doSelectJoinSalesReturn(criteria);
            }
        }
        lastSalesReturnDetailsCriteria = criteria;

        return collSalesReturnDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("SalesReturnId");
              fieldNames.add("TransactionType");
              fieldNames.add("TransactionId");
              fieldNames.add("LocationId");
              fieldNames.add("Status");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("ReturnNo");
              fieldNames.add("ReturnDate");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("TotalQty");
              fieldNames.add("TotalTax");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("TotalDiscount");
              fieldNames.add("TotalAmount");
              fieldNames.add("UserName");
              fieldNames.add("Remark");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("CreateCreditMemo");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("SalesId");
              fieldNames.add("ReturnReasonId");
              fieldNames.add("CashReturn");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("DueDate");
              fieldNames.add("ReferenceNo");
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("RoundingAmount");
              fieldNames.add("ReturnedAmount");
              fieldNames.add("FiscalRate");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("ReturnDiscAccId");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("SalesReturnId"))
        {
                return getSalesReturnId();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("ReturnNo"))
        {
                return getReturnNo();
            }
          if (name.equals("ReturnDate"))
        {
                return getReturnDate();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("TotalTax"))
        {
                return getTotalTax();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("CreateCreditMemo"))
        {
                return Boolean.valueOf(getCreateCreditMemo());
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("SalesId"))
        {
                return getSalesId();
            }
          if (name.equals("ReturnReasonId"))
        {
                return getReturnReasonId();
            }
          if (name.equals("CashReturn"))
        {
                return Boolean.valueOf(getCashReturn());
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("RoundingAmount"))
        {
                return getRoundingAmount();
            }
          if (name.equals("ReturnedAmount"))
        {
                return getReturnedAmount();
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("ReturnDiscAccId"))
        {
                return getReturnDiscAccId();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(SalesReturnPeer.SALES_RETURN_ID))
        {
                return getSalesReturnId();
            }
          if (name.equals(SalesReturnPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(SalesReturnPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(SalesReturnPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(SalesReturnPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(SalesReturnPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(SalesReturnPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(SalesReturnPeer.RETURN_NO))
        {
                return getReturnNo();
            }
          if (name.equals(SalesReturnPeer.RETURN_DATE))
        {
                return getReturnDate();
            }
          if (name.equals(SalesReturnPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(SalesReturnPeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(SalesReturnPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(SalesReturnPeer.TOTAL_TAX))
        {
                return getTotalTax();
            }
          if (name.equals(SalesReturnPeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(SalesReturnPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(SalesReturnPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(SalesReturnPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(SalesReturnPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(SalesReturnPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(SalesReturnPeer.CREATE_CREDIT_MEMO))
        {
                return Boolean.valueOf(getCreateCreditMemo());
            }
          if (name.equals(SalesReturnPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(SalesReturnPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(SalesReturnPeer.SALES_ID))
        {
                return getSalesId();
            }
          if (name.equals(SalesReturnPeer.RETURN_REASON_ID))
        {
                return getReturnReasonId();
            }
          if (name.equals(SalesReturnPeer.CASH_RETURN))
        {
                return Boolean.valueOf(getCashReturn());
            }
          if (name.equals(SalesReturnPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(SalesReturnPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(SalesReturnPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(SalesReturnPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(SalesReturnPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(SalesReturnPeer.ROUNDING_AMOUNT))
        {
                return getRoundingAmount();
            }
          if (name.equals(SalesReturnPeer.RETURNED_AMOUNT))
        {
                return getReturnedAmount();
            }
          if (name.equals(SalesReturnPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(SalesReturnPeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(SalesReturnPeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(SalesReturnPeer.RETURN_DISC_ACC_ID))
        {
                return getReturnDiscAccId();
            }
          if (name.equals(SalesReturnPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(SalesReturnPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getSalesReturnId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 2)
        {
                return getTransactionId();
            }
              if (pos == 3)
        {
                return getLocationId();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 5)
        {
                return getTransactionNo();
            }
              if (pos == 6)
        {
                return getTransactionDate();
            }
              if (pos == 7)
        {
                return getReturnNo();
            }
              if (pos == 8)
        {
                return getReturnDate();
            }
              if (pos == 9)
        {
                return getCustomerId();
            }
              if (pos == 10)
        {
                return getCustomerName();
            }
              if (pos == 11)
        {
                return getTotalQty();
            }
              if (pos == 12)
        {
                return getTotalTax();
            }
              if (pos == 13)
        {
                return getTotalDiscountPct();
            }
              if (pos == 14)
        {
                return getTotalDiscount();
            }
              if (pos == 15)
        {
                return getTotalAmount();
            }
              if (pos == 16)
        {
                return getUserName();
            }
              if (pos == 17)
        {
                return getRemark();
            }
              if (pos == 18)
        {
                return getPaymentTypeId();
            }
              if (pos == 19)
        {
                return Boolean.valueOf(getCreateCreditMemo());
            }
              if (pos == 20)
        {
                return getCurrencyId();
            }
              if (pos == 21)
        {
                return getCurrencyRate();
            }
              if (pos == 22)
        {
                return getSalesId();
            }
              if (pos == 23)
        {
                return getReturnReasonId();
            }
              if (pos == 24)
        {
                return Boolean.valueOf(getCashReturn());
            }
              if (pos == 25)
        {
                return getBankId();
            }
              if (pos == 26)
        {
                return getBankIssuer();
            }
              if (pos == 27)
        {
                return getDueDate();
            }
              if (pos == 28)
        {
                return getReferenceNo();
            }
              if (pos == 29)
        {
                return getCashFlowTypeId();
            }
              if (pos == 30)
        {
                return getRoundingAmount();
            }
              if (pos == 31)
        {
                return getReturnedAmount();
            }
              if (pos == 32)
        {
                return getFiscalRate();
            }
              if (pos == 33)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 34)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 35)
        {
                return getReturnDiscAccId();
            }
              if (pos == 36)
        {
                return getCancelBy();
            }
              if (pos == 37)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(SalesReturnPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        SalesReturnPeer.doInsert((SalesReturn) this, con);
                        setNew(false);
                    }
                    else
                    {
                        SalesReturnPeer.doUpdate((SalesReturn) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collSalesReturnDetails != null)
            {
                for (int i = 0; i < collSalesReturnDetails.size(); i++)
                {
                    ((SalesReturnDetail) collSalesReturnDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key salesReturnId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setSalesReturnId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setSalesReturnId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getSalesReturnId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public SalesReturn copy() throws TorqueException
    {
        return copyInto(new SalesReturn());
    }
  
    protected SalesReturn copyInto(SalesReturn copyObj) throws TorqueException
    {
          copyObj.setSalesReturnId(salesReturnId);
          copyObj.setTransactionType(transactionType);
          copyObj.setTransactionId(transactionId);
          copyObj.setLocationId(locationId);
          copyObj.setStatus(status);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setReturnNo(returnNo);
          copyObj.setReturnDate(returnDate);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setTotalQty(totalQty);
          copyObj.setTotalTax(totalTax);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setUserName(userName);
          copyObj.setRemark(remark);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setCreateCreditMemo(createCreditMemo);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setSalesId(salesId);
          copyObj.setReturnReasonId(returnReasonId);
          copyObj.setCashReturn(cashReturn);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setDueDate(dueDate);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setRoundingAmount(roundingAmount);
          copyObj.setReturnedAmount(returnedAmount);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setReturnDiscAccId(returnDiscAccId);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setSalesReturnId((String)null);
                                                                                                                                                                                                                                          
                                      
                            
        List v = getSalesReturnDetails();
        for (int i = 0; i < v.size(); i++)
        {
            SalesReturnDetail obj = (SalesReturnDetail) v.get(i);
            copyObj.addSalesReturnDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public SalesReturnPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("SalesReturn\n");
        str.append("-----------\n")
           .append("SalesReturnId        : ")
           .append(getSalesReturnId())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("ReturnNo             : ")
           .append(getReturnNo())
           .append("\n")
           .append("ReturnDate           : ")
           .append(getReturnDate())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("TotalTax             : ")
           .append(getTotalTax())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("CreateCreditMemo     : ")
           .append(getCreateCreditMemo())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("SalesId              : ")
           .append(getSalesId())
           .append("\n")
           .append("ReturnReasonId       : ")
           .append(getReturnReasonId())
           .append("\n")
           .append("CashReturn           : ")
           .append(getCashReturn())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("RoundingAmount       : ")
           .append(getRoundingAmount())
           .append("\n")
           .append("ReturnedAmount       : ")
           .append(getReturnedAmount())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("ReturnDiscAccId      : ")
           .append(getReturnDiscAccId())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.framework.om.manager;

import java.io.Serializable;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.framework.om.CardioConfig;
import com.ssti.framework.om.CardioConfigPeer;

public class CardioConfigManager
{
	private static Log log = LogFactory.getLog(CardioConfigManager.class);
	    
    private static CardioConfigManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "preference";   

    private static final String s_KEY_CONF = "CardioConfig";        
    private CardioConfigManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static CardioConfigManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (CardioConfigManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new CardioConfigManager();
                }
            }
        }
        return m_oInstance;
    }

	public CardioConfig getCardioConfig() 
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
        Element oElement = oCache.get(s_KEY_CONF);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			List vData = CardioConfigPeer.doSelect(oCrit);
			if (vData.size() > 0)
			{
				oCache.put(new Element (s_KEY_CONF, (Serializable) vData.get(0))); 
				return (CardioConfig)vData.get(0);
			}
			return null;
		}
		else 
		{
			return (CardioConfig) oElement.getValue();
		}
	}

	public void refreshCache () 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        oCache.removeAll();          
    }

	public void refreshCache (CardioConfig _oSC) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);
	    oCache.put(new Element (s_KEY_CONF, _oSC));        
	}    
}
package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankTransfer;
import com.ssti.enterprise.pos.om.BankTransferPeer;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.financial.BankTransferTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.6  2009/05/04 01:38:37  albert
 * *** empty log message ***
 *
 * Revision 1.5  2008/08/17 02:17:44  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/04/18 06:46:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class BankTransferLoader extends TransactionLoader
{    
    private static Log log = LogFactory.getLog (BankTransferLoader.class);
    private static final String s_START_HEADING = "Trans No"; 
    
    public BankTransferLoader ()
    {
        m_sUserName = " from (Excel)";
    }
    
    public BankTransferLoader (String _sUserName)
    {
        m_sUserName = _sUserName + " from (Excel)";
    }
    
    Currency oDefault = null;
    BankTransfer oBT = null;    
    String sTransNo = "";
    /**
     * load data from input stream
     * 
     * @param _oInput
     * @throws Exception
     */
    public void loadData (InputStream _oInput)  
        throws Exception
    {
        POIFSFileSystem oPOI = new POIFSFileSystem(_oInput);
        HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
        HSSFSheet oSheet = oWB.getSheetAt(0);
        m_iTotalRows = oSheet.getPhysicalNumberOfRows();
        oDefault = CurrencyTool.getDefaultCurrency();

        //get first row
        HSSFRow oRow = oSheet.getRow(0);
        if (oRow != null)
        {
            String sHeader = getString(oRow, i_START);
            if (!StringUtil.equals(s_START_HEADING,sHeader))
            {
                throw new Exception ("Invalid Header " + sHeader + " should be " + s_START_HEADING);
            }
        }
        
        //start from 2nd row
        for (int iRow = 1; iRow < m_iTotalRows; iRow++)
        {
            oRow = oSheet.getRow(iRow);
            if (oRow != null)
            {
                sTransNo = getString(oRow, i_START);
                if (StringUtil.isNotEmpty(sTransNo))
                {                   
                    log.debug("* TRANS NO: " + sTransNo);               

                    m_bTransError = false;
                    m_iTotalTrans++;

                    oBT = new BankTransfer();

                    mapTrans (oRow, oBT);                    
                    log.debug("* oTR " + oBT);
                    
                    if (!m_bTransError)
                    {
                        try
                        {
                            BankTransferTool.saveData(oBT, null);
                            logSuccess();
                        }
                        catch (Exception _oEx)
                        {
                            String sMsg = _oEx.getMessage();
                            log.error(_oEx);
                            logSaveError(sMsg);
                        }
                    }
                    else
                    {
                        logReject();
                    }
                    m_bTransError = false;
                }
                else
                {
                    logReject(); 
                }
            }
        } 
    }   
    
    protected void mapTrans (HSSFRow row, BankTransfer oBT)
        throws Exception
    {
        int iCol = 0;
        String sTransNo = getString(row, iCol); iCol++;
        String sTransDate = getString(row, iCol); iCol++;
        String sRemark = getString(row, iCol); iCol++;
        BigDecimal dAmount = getBigDecimal(row, iCol); iCol++;
        
        sRemark = sRemark + " source:" + sTransNo + " from Excel";
        
        m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo)
                 .append(LocaleTool.getString("trans_date")).append(": ").append(sTransDate).append(s_LINE_SEPARATOR);

        
        Date dTrans = CustomParser.parseDate(sTransDate);
        if (dTrans == null) {logError("Trans Date ", dTrans);}
        else {oBT.setTransDate(dTrans); }
        
        oBT.setUserName(m_sUserName);
        oBT.setDescription(sRemark);
        
        if (dAmount == null) {logError("Amount ", dTrans);}
        else 
        {
            oBT.setAmount(dAmount);
            oBT.setAmountBase(dAmount);
            oBT.setCurrencyRate(bd_ONE);
            oBT.setCurrencyId(oDefault.getCurrencyId());
        }
                
        String sOutBank = getString(row, iCol); iCol++;
        Bank oOutBank = BankTool.getBankByCode(sOutBank);
        if (oOutBank == null) {logError("From Bank ", sOutBank);}
        else {oBT.setOutBankId(oOutBank.getBankId()); }
        
        String sOutLoc = getString (row, iCol); iCol++;
        Location oOutLoc = LocationTool.getLocationByCode(sOutLoc);
        if (oOutLoc == null) {logError("From Loc", sOutLoc);}
        else {oBT.setOutLocId(oOutLoc.getLocationId());}

        String sOutAcc = getString (row, iCol); iCol++;
        Account oOutAcc = AccountTool.getAccountByCode(sOutAcc);
        if (oOutAcc == null) {logError("From Acc", sOutAcc);}
        else {oBT.setOutAccId(oOutAcc.getAccountId());}

        String sOutCFT = getString (row, iCol); iCol++;
        CashFlowType oOutCFT = CashFlowTypeTool.getTypeByCode(sOutCFT,null);
        if (oOutCFT == null) {logError("From CFT", sOutCFT);}
        else {oBT.setOutCftId(oOutCFT.getCashFlowTypeId());}


        String sInBank = getString(row, iCol); iCol++;
        Bank oInBank = BankTool.getBankByCode(sInBank);
        if (oInBank == null) {logError("To Bank ", sInBank);}
        else {oBT.setInBankId(oInBank.getBankId()); }
        
        String sInLoc = getString (row, iCol); iCol++;
        Location oInLoc = LocationTool.getLocationByCode(sInLoc);
        if (oInLoc == null) {logError("To Loc", sInLoc);}
        else {oBT.setInLocId(oInLoc.getLocationId());}

        String sInAcc = getString (row, iCol); iCol++;
        Account oInAcc = AccountTool.getAccountByCode(sInAcc);
        if (oInAcc == null) {logError("To Acc", sInAcc);}
        else {oBT.setInAccId(oInAcc.getAccountId());}

        String sInCFT = getString (row, iCol); iCol++;
        CashFlowType oInCFT = CashFlowTypeTool.getTypeByCode(sInCFT,null);
        if (oInCFT == null) {logError("To CFT", sInCFT);}
        else {oBT.setInCftId(oInCFT.getCashFlowTypeId());}
        
        oBT.setStatus(i_PROCESSED);
    }
    
    public boolean isExists()
        throws Exception
    {        
        if (StringUtil.isNotEmpty(sTransNo))
        {
            Criteria oCrit = new Criteria();
            oCrit.add(BankTransferPeer.TRANS_NO, sTransNo);
            oCrit.add(BankTransferPeer.STATUS, i_PROCESSED);            
            List vData = BankTransferPeer.doSelect(oCrit);
            if (vData.size() > 0) 
            {
                return true;
            }
        }
        return false;
    }
}

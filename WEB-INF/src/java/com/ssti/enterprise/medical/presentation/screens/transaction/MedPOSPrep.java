package com.ssti.enterprise.medical.presentation.screens.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.MedicineSales;
import com.ssti.enterprise.medical.om.MedicineSalesDetail;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalSalesTool;
import com.ssti.enterprise.pos.presentation.screens.transaction.SalesSecureScreen;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-06-16
 * - initial Commit 
 * </pre><br>
 */
public class MedPOSPrep extends SalesSecureScreen
{
	protected static final Log log = LogFactory.getLog(MedPOSPrep.class);
	
	protected static final String[] a_PERM = {"Sales Transaction", "View Sales Invoice"};
	
    //start screen method
	protected static final int i_OP_VIEW_TRANS  = 4; //after saved or view details   
	protected static final int i_OP_SAVE = 12;
	
	protected String m_sClientIP = "";    
    protected MedicineSales oTR = null;    
    protected List vTD = null;
    static final String s_MTR = "mprp";
    static final String s_MTD = "mprpDet";
    
    protected HttpSession oSes;
    protected boolean b_UPDATEABLE_DETAIL = false; 
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	m_sClientIP = data.getRemoteAddr();
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {	    	
    	super.doBuildTemplate(data, context);
    	
    	int iOp = data.getParameters().getInt("op");
		initSession (data);		
		try 
		{	    	
			if (iOp == i_OP_VIEW_TRANS ) {
				getData (data);
			}
			else if (iOp == i_OP_SAVE) {
				saveData(data);
			}
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage("ERROR : " + _oEx.getMessage());
		}

    	context.put(s_MTD, oSes.getAttribute (s_MTD));
    	context.put(s_MTR, oSes.getAttribute (s_MTR));
    	
    	//set med related variable & tools in context
    	MedConfigTool.setContextTool(context);
    }

	protected void initSession ( RunData data )
	{	
		oSes = data.getSession();
		if (oSes.getAttribute (s_MTR) == null) 
		{
			oTR = new MedicineSales();
			vTD = new ArrayList();
		}
		oTR = (MedicineSales) oSes.getAttribute (s_MTR);
		vTD = (List) oSes.getAttribute (s_MTD);	
	}	      
   
    protected void getData ( RunData data ) 
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID)) 
    	{
    		try 
    		{
    			oSes.setAttribute (s_MTR, MedicalSalesTool.getHeaderByID (sID));
    			oSes.setAttribute (s_MTD, MedicalSalesTool.getDetailsByID (sID));
    		}
    		catch (Exception _oEx) 
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ()); 
    		}
    	}
    }
    
    protected void saveData ( RunData data ) 
    	throws Exception
    {
    	if(oTR != null && StringUtil.isNotEmpty(oTR.getMedicineSalesId()) && oTR.getStatus() != i_CANCELLED && vTD.size() > 0) 
    	{
    		
			oTR.setPatientName(data.getParameters().getString("patientName"));
    		oTR.setPatientAge(data.getParameters().getString("patientAge"));
    		oTR.setPatientPhone(data.getParameters().getString("patientPhone"));
    		oTR.setPatientAddress(data.getParameters().getString("patientAddress"));
    		oTR.setShipTo(data.getParameters().getString("shipTo",""));
    		oTR.setDoctorName(data.getParameters().getString("doctorName")); 
    		oTR.setPrepStatus(data.getParameters().getInt("PrepStatus"));
    		if(oTR.getPrepStatus() == i_PROCESSED) {
    			oTR.setPrepEnd(new Date());
    			oTR.setPrepBy(data.getUser().getName());
    		}    		
    		oTR.save();
    		
    		for(int i = 0; i < vTD.size(); i++)
    		{
    			MedicineSalesDetail oTD = (MedicineSalesDetail) vTD.get(i);
    			oTD.setDosage(data.getParameters().getString("Dosage" + (i + 1),""));
    			oTD.setBeforeAfter(data.getParameters().getString("BeforeAfter" + (i + 1),""));
    			oTD.setInstruction(data.getParameters().getString("Instruction" + (i + 1),""));  
    			oTD.save();
    		}    		
    		data.setMessage(LocaleTool.getString("save_success"));
    	}
    }    
}

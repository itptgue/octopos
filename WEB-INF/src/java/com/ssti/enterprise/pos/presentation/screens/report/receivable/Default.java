package com.ssti.enterprise.pos.presentation.screens.report.receivable;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;

public class Default extends ReportSecureScreen
{
	String sCustomerID = "";
	String sBankID = "";
	String sCFTID = "";
	int iStatus = -1; 
	
	public void doBuildTemplate(RunData data, Context context)
	{
		super.doBuildTemplate(data, context);
		setParams(data);
		
		sCustomerID = data.getParameters().getString("CustomerId");
		sBankID = data.getParameters().getString("BankId", "");
		sCFTID = data.getParameters().getString("CashFlowTypeId", "");
		iStatus = data.getParameters().getInt("Status", -1);
		
		context.put ("arpayment", ReceivablePaymentTool.getInstance());
	}
}

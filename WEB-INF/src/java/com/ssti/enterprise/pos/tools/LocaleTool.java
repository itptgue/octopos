package com.ssti.enterprise.pos.tools;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.services.localization.Localization;
import org.apache.turbine.services.pull.ApplicationTool;
import org.apache.turbine.util.RunData;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: LocaleTool.java,v 1.4 2007/02/23 14:44:42 albert Exp $ <br>
 *
 * <pre>
 * $Log: LocaleTool.java,v $
 * Revision 1.4  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * 2017-09-24 
 * - add method getDOW(Date, Locale) 
 * 
 * </pre><br>
 */
public class LocaleTool implements ApplicationTool
{
    /** Logging */
    private static Log log = LogFactory.getLog(LocaleTool.class);

    /**
     * The language and country information parsed from the request's
     * <code>Accept-Language</code> header.  Reset on each request.
     */
    protected Locale locale;

    /**
     * The bundle for this request.
     */
    private ResourceBundle bundle;

    /**
     * The name of the bundle for this tool to use.
     */
    private String bundleName;

    /**
     * Creates a new instance.  Used by <code>PullService</code>.
     */
    public LocaleTool()
    {
        refresh();
    }

    /**
     * <p>Performs text lookups for localization.</p>
     *
     * <p>Assuming there is a instance of this class with a HTTP
     * request set in your template's context named <code>l10n</code>,
     * the VTL <code>$l10n.HELLO</code> would render to
     * <code>hello</code> for English requests and <code>hola</code>
     * in Spanish (depending on the value of the HTTP request's
     * <code>Accept-Language</code> header).</p>
     *
     * @param key The identifier for the localized text to retrieve.
     * @return The localized text.
     */
    public String get(String key)
    {
        try
        {
        	String sTxt = "";
        	if (PreferenceTool.medicalInstalled())
        	{        		
        		sTxt = Localization.getString("medical", getLocale(), key);	
        	}
        	else //if (StringUtil.isEmpty(sTxt))
        	{
        		sTxt = Localization.getString(getBundleName(null), getLocale(), key);	
        	}
            return sTxt;
        }
        catch (MissingResourceException noKey)
        {
            log.error(noKey);
            return key;
        }
    }

    /**
     * Gets the current locale.
     *
     * @return The locale currently in use.
     */
    public Locale getLocale()
    {
        return locale;
    }

    /**
     * The return value of this method is used to set the name of the
     * bundle used by this tool.  Useful as a hook for using a
     * different bundle than specifed in your
     * <code>LocalizationService</code> configuration.
     *
     * @param data The inputs passed from {@link #init(Object)}.
     * (ignored by this implementation).
     */
    protected String getBundleName(Object data)
    {
        return Localization.getDefaultBundleName();
    }
    

    // ApplicationTool implmentation

    /**
     * Sets the request to get the <code>Accept-Language</code> header
     * from (reset on each request).
     */
    public final void init(Object data)
    {
        if (data instanceof RunData)
        {
            // Pull necessary information out of RunData while we have
            // a reference to it.
            locale = PreferenceTool.getLocale((RunData) data);
            bundleName = getBundleName(data);
        }
    }

    /**
     * No-op.
     */
    public void refresh()
    {
        locale = PreferenceTool.getLocale();
        bundle = null;
        bundleName = null;
    }
   
    /**
     * Static accessor
     * @param key
     * @return localized string
     */
    public static String getString(String key)
    {
        try
        {
            return Localization.getString(key, PreferenceTool.getLocale());
        }
        catch (MissingResourceException noKey)
        {
            log.error(noKey);
            return key;
        }
    }

    public static String get(String _sBundleName, String key)
    {
        return getString(_sBundleName, key);
    }  
    
    public static String getString(String _sBundleName, String key)
    {
        try
        {
        	return Localization.getString(_sBundleName, PreferenceTool.getLocale(), key);
        }
        catch (MissingResourceException noKey)
        {
            log.error(noKey);
            return key;
        }
    }
    
    public static String getString(String _sBundleName, String key, int _iLangID)
    {
        try
        {
        	return Localization.getString(_sBundleName, PreferenceTool.a_LOCALE[_iLangID], key);
        }
        catch (MissingResourceException noKey)
        {
            log.error(noKey);
            return key;
        }
    }

	public static String getDOW(Date _dDate)
	{
		return getDOW(_dDate, null);
	}  
	
	public static String getDOW(Date _dDate, Locale _oLocale)
	{
		Calendar oCal = Calendar.getInstance();
		if (_dDate != null) oCal.setTime(_dDate);
		int iDay = oCal.get(Calendar.DAY_OF_WEEK);
		if(_oLocale == null)
		{
			if (iDay == Calendar.SUNDAY) return getString("sunday");
			if (iDay == Calendar.MONDAY) return getString("monday");
			if (iDay == Calendar.TUESDAY) return getString("tuesday");
			if (iDay == Calendar.WEDNESDAY) return getString("wednesday");
			if (iDay == Calendar.THURSDAY) return getString("thursday");
			if (iDay == Calendar.FRIDAY) return getString("friday");
			if (iDay == Calendar.SATURDAY) return getString("saturday");
		}
		else
		{
			if (iDay == Calendar.SUNDAY) return Localization.getString("sunday",_oLocale);
			if (iDay == Calendar.MONDAY) return Localization.getString("monday",_oLocale);
			if (iDay == Calendar.TUESDAY) return Localization.getString("tuesday",_oLocale);
			if (iDay == Calendar.WEDNESDAY) return Localization.getString("wednesday",_oLocale);
			if (iDay == Calendar.THURSDAY) return Localization.getString("thursday",_oLocale);
			if (iDay == Calendar.FRIDAY) return Localization.getString("friday",_oLocale);
			if (iDay == Calendar.SATURDAY) return Localization.getString("saturday",_oLocale);
			
		}
		return "";
	}  	
}

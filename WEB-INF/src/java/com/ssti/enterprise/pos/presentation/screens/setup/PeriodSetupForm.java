package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.framework.tools.StringUtil;

public class PeriodSetupForm extends PeriodSetup
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	String sMode = data.getParameters().getString("mode", "");
    	String sID = "";
		try
    	{
			sID = data.getParameters().getString("id");
			if (StringUtil.isNotEmpty(sID))
			{
				if (sMode.equals("update"))
				{
				}
				else if (sMode.equals("close"))
				{				
					context.put("bValidForClose",Boolean.valueOf(PeriodTool.isValidForClose(sID)));
					context.put("bPeriodEnd",Boolean.valueOf(PeriodTool.isEndOfPeriod(sID)));	
				}
				else if (sMode.equals("closefa"))
				{				
					context.put("bValidForFAClose",Boolean.valueOf(PeriodTool.isValidForFAClose(sID)));
					context.put("bPeriodEnd",Boolean.valueOf(PeriodTool.isEndOfPeriod(sID)));	
				}				
				else if (sMode.equals("reopen"))
				{				
					context.put("bValidForReopen",Boolean.valueOf(PeriodTool.isValidForReopen(sID)));
				}		
				else if (sMode.equals("reopenfa"))
				{				
					context.put("bValidForFAReopen",Boolean.valueOf(PeriodTool.isValidForFAReopen(sID)));
				}		
				
				if (StringUtil.isNotEmpty(sMode))
				{
					context.put("Period", PeriodTool.getPeriodByID(sID));
					context.put("LastFAClosed", PeriodTool.getLastFAClosedPeriod());								
					context.put("LastClosed", PeriodTool.getLastClosedPeriod());								
				}
			}
    	}
    	catch(Exception _oEx)
    	{
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}

package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to DeliveryOrder
 */
public abstract class BaseDeliveryOrder extends BaseObject
{
    /** The Peer class */
    private static final DeliveryOrderPeer peer =
        new DeliveryOrderPeer();

        
    /** The value for the deliveryOrderId field */
    private String deliveryOrderId;
      
    /** The value for the salesOrderId field */
    private String salesOrderId;
                                                
    /** The value for the customerPoNo field */
    private String customerPoNo = "";
      
    /** The value for the customerPoDate field */
    private Date customerPoDate;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the deliveryOrderNo field */
    private String deliveryOrderNo;
      
    /** The value for the deliveryOrderDate field */
    private Date deliveryOrderDate;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the createBy field */
    private String createBy;
      
    /** The value for the confirmBy field */
    private String confirmBy;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the confirmDate field */
    private Date confirmDate;
      
    /** The value for the status field */
    private int status;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the remark field */
    private String remark;
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
      
    /** The value for the isBill field */
    private boolean isBill;
                                                
    /** The value for the shipTo field */
    private String shipTo = "";
                                          
    /** The value for the printTimes field */
    private int printTimes = 0;
                                                
    /** The value for the salesId field */
    private String salesId = "";
                                                
    /** The value for the courierId field */
    private String courierId = "";
                                                
    /** The value for the fobId field */
    private String fobId = "";
                                                
    /** The value for the freightNo field */
    private String freightNo = "";
                                                
          
    /** The value for the estimatedFreight field */
    private BigDecimal estimatedFreight= bd_ZERO;
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
                                                
    /** The value for the freightAccountId field */
    private String freightAccountId = "";
      
    /** The value for the isInclusiveFreight field */
    private boolean isInclusiveFreight;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the DeliveryOrderId
     *
     * @return String
     */
    public String getDeliveryOrderId()
    {
        return deliveryOrderId;
    }

                                              
    /**
     * Set the value of DeliveryOrderId
     *
     * @param v new value
     */
    public void setDeliveryOrderId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.deliveryOrderId, v))
              {
            this.deliveryOrderId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated DeliveryOrderDetail
        if (collDeliveryOrderDetails != null)
        {
            for (int i = 0; i < collDeliveryOrderDetails.size(); i++)
            {
                ((DeliveryOrderDetail) collDeliveryOrderDetails.get(i))
                    .setDeliveryOrderId(v);
            }
        }
                                }
  
    /**
     * Get the SalesOrderId
     *
     * @return String
     */
    public String getSalesOrderId()
    {
        return salesOrderId;
    }

                        
    /**
     * Set the value of SalesOrderId
     *
     * @param v new value
     */
    public void setSalesOrderId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesOrderId, v))
              {
            this.salesOrderId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerPoNo
     *
     * @return String
     */
    public String getCustomerPoNo()
    {
        return customerPoNo;
    }

                        
    /**
     * Set the value of CustomerPoNo
     *
     * @param v new value
     */
    public void setCustomerPoNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerPoNo, v))
              {
            this.customerPoNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerPoDate
     *
     * @return Date
     */
    public Date getCustomerPoDate()
    {
        return customerPoDate;
    }

                        
    /**
     * Set the value of CustomerPoDate
     *
     * @param v new value
     */
    public void setCustomerPoDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.customerPoDate, v))
              {
            this.customerPoDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryOrderNo
     *
     * @return String
     */
    public String getDeliveryOrderNo()
    {
        return deliveryOrderNo;
    }

                        
    /**
     * Set the value of DeliveryOrderNo
     *
     * @param v new value
     */
    public void setDeliveryOrderNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryOrderNo, v))
              {
            this.deliveryOrderNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DeliveryOrderDate
     *
     * @return Date
     */
    public Date getDeliveryOrderDate()
    {
        return deliveryOrderDate;
    }

                        
    /**
     * Set the value of DeliveryOrderDate
     *
     * @param v new value
     */
    public void setDeliveryOrderDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.deliveryOrderDate, v))
              {
            this.deliveryOrderDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmBy
     *
     * @return String
     */
    public String getConfirmBy()
    {
        return confirmBy;
    }

                        
    /**
     * Set the value of ConfirmBy
     *
     * @param v new value
     */
    public void setConfirmBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmBy, v))
              {
            this.confirmBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmDate
     *
     * @return Date
     */
    public Date getConfirmDate()
    {
        return confirmDate;
    }

                        
    /**
     * Set the value of ConfirmDate
     *
     * @param v new value
     */
    public void setConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmDate, v))
              {
            this.confirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsBill
     *
     * @return boolean
     */
    public boolean getIsBill()
    {
        return isBill;
    }

                        
    /**
     * Set the value of IsBill
     *
     * @param v new value
     */
    public void setIsBill(boolean v) 
    {
    
                  if (this.isBill != v)
              {
            this.isBill = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShipTo
     *
     * @return String
     */
    public String getShipTo()
    {
        return shipTo;
    }

                        
    /**
     * Set the value of ShipTo
     *
     * @param v new value
     */
    public void setShipTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shipTo, v))
              {
            this.shipTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrintTimes
     *
     * @return int
     */
    public int getPrintTimes()
    {
        return printTimes;
    }

                        
    /**
     * Set the value of PrintTimes
     *
     * @param v new value
     */
    public void setPrintTimes(int v) 
    {
    
                  if (this.printTimes != v)
              {
            this.printTimes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesId
     *
     * @return String
     */
    public String getSalesId()
    {
        return salesId;
    }

                        
    /**
     * Set the value of SalesId
     *
     * @param v new value
     */
    public void setSalesId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesId, v))
              {
            this.salesId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FobId
     *
     * @return String
     */
    public String getFobId()
    {
        return fobId;
    }

                        
    /**
     * Set the value of FobId
     *
     * @param v new value
     */
    public void setFobId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fobId, v))
              {
            this.fobId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightNo
     *
     * @return String
     */
    public String getFreightNo()
    {
        return freightNo;
    }

                        
    /**
     * Set the value of FreightNo
     *
     * @param v new value
     */
    public void setFreightNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightNo, v))
              {
            this.freightNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EstimatedFreight
     *
     * @return BigDecimal
     */
    public BigDecimal getEstimatedFreight()
    {
        return estimatedFreight;
    }

                        
    /**
     * Set the value of EstimatedFreight
     *
     * @param v new value
     */
    public void setEstimatedFreight(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.estimatedFreight, v))
              {
            this.estimatedFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightAccountId
     *
     * @return String
     */
    public String getFreightAccountId()
    {
        return freightAccountId;
    }

                        
    /**
     * Set the value of FreightAccountId
     *
     * @param v new value
     */
    public void setFreightAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightAccountId, v))
              {
            this.freightAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveFreight
     *
     * @return boolean
     */
    public boolean getIsInclusiveFreight()
    {
        return isInclusiveFreight;
    }

                        
    /**
     * Set the value of IsInclusiveFreight
     *
     * @param v new value
     */
    public void setIsInclusiveFreight(boolean v) 
    {
    
                  if (this.isInclusiveFreight != v)
              {
            this.isInclusiveFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collDeliveryOrderDetails
     */
    protected List collDeliveryOrderDetails;

    /**
     * Temporary storage of collDeliveryOrderDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initDeliveryOrderDetails()
    {
        if (collDeliveryOrderDetails == null)
        {
            collDeliveryOrderDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a DeliveryOrderDetail object to this object
     * through the DeliveryOrderDetail foreign key attribute
     *
     * @param l DeliveryOrderDetail
     * @throws TorqueException
     */
    public void addDeliveryOrderDetail(DeliveryOrderDetail l) throws TorqueException
    {
        getDeliveryOrderDetails().add(l);
        l.setDeliveryOrder((DeliveryOrder) this);
    }

    /**
     * The criteria used to select the current contents of collDeliveryOrderDetails
     */
    private Criteria lastDeliveryOrderDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getDeliveryOrderDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getDeliveryOrderDetails() throws TorqueException
    {
              if (collDeliveryOrderDetails == null)
        {
            collDeliveryOrderDetails = getDeliveryOrderDetails(new Criteria(10));
        }
        return collDeliveryOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DeliveryOrder has previously
     * been saved, it will retrieve related DeliveryOrderDetails from storage.
     * If this DeliveryOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getDeliveryOrderDetails(Criteria criteria) throws TorqueException
    {
              if (collDeliveryOrderDetails == null)
        {
            if (isNew())
            {
               collDeliveryOrderDetails = new ArrayList();
            }
            else
            {
                        criteria.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, getDeliveryOrderId() );
                        collDeliveryOrderDetails = DeliveryOrderDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, getDeliveryOrderId());
                            if (!lastDeliveryOrderDetailsCriteria.equals(criteria))
                {
                    collDeliveryOrderDetails = DeliveryOrderDetailPeer.doSelect(criteria);
                }
            }
        }
        lastDeliveryOrderDetailsCriteria = criteria;

        return collDeliveryOrderDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getDeliveryOrderDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getDeliveryOrderDetails(Connection con) throws TorqueException
    {
              if (collDeliveryOrderDetails == null)
        {
            collDeliveryOrderDetails = getDeliveryOrderDetails(new Criteria(10), con);
        }
        return collDeliveryOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DeliveryOrder has previously
     * been saved, it will retrieve related DeliveryOrderDetails from storage.
     * If this DeliveryOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getDeliveryOrderDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collDeliveryOrderDetails == null)
        {
            if (isNew())
            {
               collDeliveryOrderDetails = new ArrayList();
            }
            else
            {
                         criteria.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, getDeliveryOrderId());
                         collDeliveryOrderDetails = DeliveryOrderDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, getDeliveryOrderId());
                             if (!lastDeliveryOrderDetailsCriteria.equals(criteria))
                 {
                     collDeliveryOrderDetails = DeliveryOrderDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastDeliveryOrderDetailsCriteria = criteria;

         return collDeliveryOrderDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DeliveryOrder is new, it will return
     * an empty collection; or if this DeliveryOrder has previously
     * been saved, it will retrieve related DeliveryOrderDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in DeliveryOrder.
     */
    protected List getDeliveryOrderDetailsJoinDeliveryOrder(Criteria criteria)
        throws TorqueException
    {
                    if (collDeliveryOrderDetails == null)
        {
            if (isNew())
            {
               collDeliveryOrderDetails = new ArrayList();
            }
            else
            {
                              criteria.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, getDeliveryOrderId());
                              collDeliveryOrderDetails = DeliveryOrderDetailPeer.doSelectJoinDeliveryOrder(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(DeliveryOrderDetailPeer.DELIVERY_ORDER_ID, getDeliveryOrderId());
                                    if (!lastDeliveryOrderDetailsCriteria.equals(criteria))
            {
                collDeliveryOrderDetails = DeliveryOrderDetailPeer.doSelectJoinDeliveryOrder(criteria);
            }
        }
        lastDeliveryOrderDetailsCriteria = criteria;

        return collDeliveryOrderDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DeliveryOrderId");
              fieldNames.add("SalesOrderId");
              fieldNames.add("CustomerPoNo");
              fieldNames.add("CustomerPoDate");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("DeliveryOrderNo");
              fieldNames.add("DeliveryOrderDate");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("CreateBy");
              fieldNames.add("ConfirmBy");
              fieldNames.add("CreateDate");
              fieldNames.add("ConfirmDate");
              fieldNames.add("Status");
              fieldNames.add("TotalQty");
              fieldNames.add("Remark");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("IsBill");
              fieldNames.add("ShipTo");
              fieldNames.add("PrintTimes");
              fieldNames.add("SalesId");
              fieldNames.add("CourierId");
              fieldNames.add("FobId");
              fieldNames.add("FreightNo");
              fieldNames.add("EstimatedFreight");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("FiscalRate");
              fieldNames.add("FreightAccountId");
              fieldNames.add("IsInclusiveFreight");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DeliveryOrderId"))
        {
                return getDeliveryOrderId();
            }
          if (name.equals("SalesOrderId"))
        {
                return getSalesOrderId();
            }
          if (name.equals("CustomerPoNo"))
        {
                return getCustomerPoNo();
            }
          if (name.equals("CustomerPoDate"))
        {
                return getCustomerPoDate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("DeliveryOrderNo"))
        {
                return getDeliveryOrderNo();
            }
          if (name.equals("DeliveryOrderDate"))
        {
                return getDeliveryOrderDate();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("ConfirmBy"))
        {
                return getConfirmBy();
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("ConfirmDate"))
        {
                return getConfirmDate();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("IsBill"))
        {
                return Boolean.valueOf(getIsBill());
            }
          if (name.equals("ShipTo"))
        {
                return getShipTo();
            }
          if (name.equals("PrintTimes"))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals("SalesId"))
        {
                return getSalesId();
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("FobId"))
        {
                return getFobId();
            }
          if (name.equals("FreightNo"))
        {
                return getFreightNo();
            }
          if (name.equals("EstimatedFreight"))
        {
                return getEstimatedFreight();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("FreightAccountId"))
        {
                return getFreightAccountId();
            }
          if (name.equals("IsInclusiveFreight"))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DeliveryOrderPeer.DELIVERY_ORDER_ID))
        {
                return getDeliveryOrderId();
            }
          if (name.equals(DeliveryOrderPeer.SALES_ORDER_ID))
        {
                return getSalesOrderId();
            }
          if (name.equals(DeliveryOrderPeer.CUSTOMER_PO_NO))
        {
                return getCustomerPoNo();
            }
          if (name.equals(DeliveryOrderPeer.CUSTOMER_PO_DATE))
        {
                return getCustomerPoDate();
            }
          if (name.equals(DeliveryOrderPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(DeliveryOrderPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(DeliveryOrderPeer.DELIVERY_ORDER_NO))
        {
                return getDeliveryOrderNo();
            }
          if (name.equals(DeliveryOrderPeer.DELIVERY_ORDER_DATE))
        {
                return getDeliveryOrderDate();
            }
          if (name.equals(DeliveryOrderPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(DeliveryOrderPeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(DeliveryOrderPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(DeliveryOrderPeer.CONFIRM_BY))
        {
                return getConfirmBy();
            }
          if (name.equals(DeliveryOrderPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(DeliveryOrderPeer.CONFIRM_DATE))
        {
                return getConfirmDate();
            }
          if (name.equals(DeliveryOrderPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(DeliveryOrderPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(DeliveryOrderPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(DeliveryOrderPeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(DeliveryOrderPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(DeliveryOrderPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(DeliveryOrderPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(DeliveryOrderPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(DeliveryOrderPeer.IS_BILL))
        {
                return Boolean.valueOf(getIsBill());
            }
          if (name.equals(DeliveryOrderPeer.SHIP_TO))
        {
                return getShipTo();
            }
          if (name.equals(DeliveryOrderPeer.PRINT_TIMES))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals(DeliveryOrderPeer.SALES_ID))
        {
                return getSalesId();
            }
          if (name.equals(DeliveryOrderPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(DeliveryOrderPeer.FOB_ID))
        {
                return getFobId();
            }
          if (name.equals(DeliveryOrderPeer.FREIGHT_NO))
        {
                return getFreightNo();
            }
          if (name.equals(DeliveryOrderPeer.ESTIMATED_FREIGHT))
        {
                return getEstimatedFreight();
            }
          if (name.equals(DeliveryOrderPeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(DeliveryOrderPeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(DeliveryOrderPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(DeliveryOrderPeer.FREIGHT_ACCOUNT_ID))
        {
                return getFreightAccountId();
            }
          if (name.equals(DeliveryOrderPeer.IS_INCLUSIVE_FREIGHT))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals(DeliveryOrderPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(DeliveryOrderPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDeliveryOrderId();
            }
              if (pos == 1)
        {
                return getSalesOrderId();
            }
              if (pos == 2)
        {
                return getCustomerPoNo();
            }
              if (pos == 3)
        {
                return getCustomerPoDate();
            }
              if (pos == 4)
        {
                return getLocationId();
            }
              if (pos == 5)
        {
                return getLocationName();
            }
              if (pos == 6)
        {
                return getDeliveryOrderNo();
            }
              if (pos == 7)
        {
                return getDeliveryOrderDate();
            }
              if (pos == 8)
        {
                return getCustomerId();
            }
              if (pos == 9)
        {
                return getCustomerName();
            }
              if (pos == 10)
        {
                return getCreateBy();
            }
              if (pos == 11)
        {
                return getConfirmBy();
            }
              if (pos == 12)
        {
                return getCreateDate();
            }
              if (pos == 13)
        {
                return getConfirmDate();
            }
              if (pos == 14)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 15)
        {
                return getTotalQty();
            }
              if (pos == 16)
        {
                return getRemark();
            }
              if (pos == 17)
        {
                return getTotalDiscountPct();
            }
              if (pos == 18)
        {
                return getPaymentTypeId();
            }
              if (pos == 19)
        {
                return getPaymentTermId();
            }
              if (pos == 20)
        {
                return getCurrencyId();
            }
              if (pos == 21)
        {
                return getCurrencyRate();
            }
              if (pos == 22)
        {
                return Boolean.valueOf(getIsBill());
            }
              if (pos == 23)
        {
                return getShipTo();
            }
              if (pos == 24)
        {
                return Integer.valueOf(getPrintTimes());
            }
              if (pos == 25)
        {
                return getSalesId();
            }
              if (pos == 26)
        {
                return getCourierId();
            }
              if (pos == 27)
        {
                return getFobId();
            }
              if (pos == 28)
        {
                return getFreightNo();
            }
              if (pos == 29)
        {
                return getEstimatedFreight();
            }
              if (pos == 30)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 31)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 32)
        {
                return getFiscalRate();
            }
              if (pos == 33)
        {
                return getFreightAccountId();
            }
              if (pos == 34)
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
              if (pos == 35)
        {
                return getCancelBy();
            }
              if (pos == 36)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DeliveryOrderPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DeliveryOrderPeer.doInsert((DeliveryOrder) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DeliveryOrderPeer.doUpdate((DeliveryOrder) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collDeliveryOrderDetails != null)
            {
                for (int i = 0; i < collDeliveryOrderDetails.size(); i++)
                {
                    ((DeliveryOrderDetail) collDeliveryOrderDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key deliveryOrderId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setDeliveryOrderId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setDeliveryOrderId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDeliveryOrderId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public DeliveryOrder copy() throws TorqueException
    {
        return copyInto(new DeliveryOrder());
    }
  
    protected DeliveryOrder copyInto(DeliveryOrder copyObj) throws TorqueException
    {
          copyObj.setDeliveryOrderId(deliveryOrderId);
          copyObj.setSalesOrderId(salesOrderId);
          copyObj.setCustomerPoNo(customerPoNo);
          copyObj.setCustomerPoDate(customerPoDate);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setDeliveryOrderNo(deliveryOrderNo);
          copyObj.setDeliveryOrderDate(deliveryOrderDate);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setCreateBy(createBy);
          copyObj.setConfirmBy(confirmBy);
          copyObj.setCreateDate(createDate);
          copyObj.setConfirmDate(confirmDate);
          copyObj.setStatus(status);
          copyObj.setTotalQty(totalQty);
          copyObj.setRemark(remark);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setIsBill(isBill);
          copyObj.setShipTo(shipTo);
          copyObj.setPrintTimes(printTimes);
          copyObj.setSalesId(salesId);
          copyObj.setCourierId(courierId);
          copyObj.setFobId(fobId);
          copyObj.setFreightNo(freightNo);
          copyObj.setEstimatedFreight(estimatedFreight);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setFreightAccountId(freightAccountId);
          copyObj.setIsInclusiveFreight(isInclusiveFreight);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setDeliveryOrderId((String)null);
                                                                                                                                                                                                                                    
                                      
                            
        List v = getDeliveryOrderDetails();
        for (int i = 0; i < v.size(); i++)
        {
            DeliveryOrderDetail obj = (DeliveryOrderDetail) v.get(i);
            copyObj.addDeliveryOrderDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DeliveryOrderPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("DeliveryOrder\n");
        str.append("-------------\n")
           .append("DeliveryOrderId      : ")
           .append(getDeliveryOrderId())
           .append("\n")
           .append("SalesOrderId         : ")
           .append(getSalesOrderId())
           .append("\n")
           .append("CustomerPoNo         : ")
           .append(getCustomerPoNo())
           .append("\n")
           .append("CustomerPoDate       : ")
           .append(getCustomerPoDate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("DeliveryOrderNo      : ")
           .append(getDeliveryOrderNo())
           .append("\n")
           .append("DeliveryOrderDate    : ")
           .append(getDeliveryOrderDate())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("ConfirmBy            : ")
           .append(getConfirmBy())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("ConfirmDate          : ")
           .append(getConfirmDate())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("IsBill               : ")
           .append(getIsBill())
           .append("\n")
           .append("ShipTo               : ")
           .append(getShipTo())
           .append("\n")
           .append("PrintTimes           : ")
           .append(getPrintTimes())
           .append("\n")
           .append("SalesId              : ")
           .append(getSalesId())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("FobId                : ")
           .append(getFobId())
           .append("\n")
           .append("FreightNo            : ")
           .append(getFreightNo())
           .append("\n")
           .append("EstimatedFreight     : ")
           .append(getEstimatedFreight())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("FreightAccountId     : ")
           .append(getFreightAccountId())
           .append("\n")
           .append("IsInclusiveFreight   : ")
           .append(getIsInclusiveFreight())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

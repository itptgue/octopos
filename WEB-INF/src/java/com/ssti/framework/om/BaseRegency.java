package com.ssti.framework.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Regency
 */
public abstract class BaseRegency extends BaseObject
{
    /** The Peer class */
    private static final RegencyPeer peer =
        new RegencyPeer();

        
    /** The value for the regencyId field */
    private String regencyId;
      
    /** The value for the provinceId field */
    private String provinceId;
      
    /** The value for the regency field */
    private String regency;
  
    
    /**
     * Get the RegencyId
     *
     * @return String
     */
    public String getRegencyId()
    {
        return regencyId;
    }

                                              
    /**
     * Set the value of RegencyId
     *
     * @param v new value
     */
    public void setRegencyId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.regencyId, v))
              {
            this.regencyId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated District
        if (collDistricts != null)
        {
            for (int i = 0; i < collDistricts.size(); i++)
            {
                ((District) collDistricts.get(i))
                    .setRegencyId(v);
            }
        }
                                }
  
    /**
     * Get the ProvinceId
     *
     * @return String
     */
    public String getProvinceId()
    {
        return provinceId;
    }

                              
    /**
     * Set the value of ProvinceId
     *
     * @param v new value
     */
    public void setProvinceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.provinceId, v))
              {
            this.provinceId = v;
            setModified(true);
        }
    
                          
                if (aProvince != null && !ObjectUtils.equals(aProvince.getProvinceId(), v))
                {
            aProvince = null;
        }
      
              }
  
    /**
     * Get the Regency
     *
     * @return String
     */
    public String getRegency()
    {
        return regency;
    }

                        
    /**
     * Set the value of Regency
     *
     * @param v new value
     */
    public void setRegency(String v) 
    {
    
                  if (!ObjectUtils.equals(this.regency, v))
              {
            this.regency = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Province aProvince;

    /**
     * Declares an association between this object and a Province object
     *
     * @param v Province
     * @throws TorqueException
     */
    public void setProvince(Province v) throws TorqueException
    {
            if (v == null)
        {
                  setProvinceId((String) null);
              }
        else
        {
            setProvinceId(v.getProvinceId());
        }
            aProvince = v;
    }

                                            
    /**
     * Get the associated Province object
     *
     * @return the associated Province object
     * @throws TorqueException
     */
    public Province getProvince() throws TorqueException
    {
        if (aProvince == null && (!ObjectUtils.equals(this.provinceId, null)))
        {
                          aProvince = ProvincePeer.retrieveByPK(SimpleKey.keyFor(this.provinceId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Province obj = ProvincePeer.retrieveByPK(this.provinceId);
               obj.addRegencys(this);
            */
        }
        return aProvince;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setProvinceKey(ObjectKey key) throws TorqueException
    {
      
                        setProvinceId(key.toString());
                  }
       
                                
            
          /**
     * Collection to store aggregation of collDistricts
     */
    protected List collDistricts;

    /**
     * Temporary storage of collDistricts to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initDistricts()
    {
        if (collDistricts == null)
        {
            collDistricts = new ArrayList();
        }
    }

    /**
     * Method called to associate a District object to this object
     * through the District foreign key attribute
     *
     * @param l District
     * @throws TorqueException
     */
    public void addDistrict(District l) throws TorqueException
    {
        getDistricts().add(l);
        l.setRegency((Regency) this);
    }

    /**
     * The criteria used to select the current contents of collDistricts
     */
    private Criteria lastDistrictsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getDistricts(new Criteria())
     *
     * @throws TorqueException
     */
    public List getDistricts() throws TorqueException
    {
              if (collDistricts == null)
        {
            collDistricts = getDistricts(new Criteria(10));
        }
        return collDistricts;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Regency has previously
     * been saved, it will retrieve related Districts from storage.
     * If this Regency is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getDistricts(Criteria criteria) throws TorqueException
    {
              if (collDistricts == null)
        {
            if (isNew())
            {
               collDistricts = new ArrayList();
            }
            else
            {
                        criteria.add(DistrictPeer.REGENCY_ID, getRegencyId() );
                        collDistricts = DistrictPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(DistrictPeer.REGENCY_ID, getRegencyId());
                            if (!lastDistrictsCriteria.equals(criteria))
                {
                    collDistricts = DistrictPeer.doSelect(criteria);
                }
            }
        }
        lastDistrictsCriteria = criteria;

        return collDistricts;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getDistricts(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getDistricts(Connection con) throws TorqueException
    {
              if (collDistricts == null)
        {
            collDistricts = getDistricts(new Criteria(10), con);
        }
        return collDistricts;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Regency has previously
     * been saved, it will retrieve related Districts from storage.
     * If this Regency is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getDistricts(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collDistricts == null)
        {
            if (isNew())
            {
               collDistricts = new ArrayList();
            }
            else
            {
                         criteria.add(DistrictPeer.REGENCY_ID, getRegencyId());
                         collDistricts = DistrictPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(DistrictPeer.REGENCY_ID, getRegencyId());
                             if (!lastDistrictsCriteria.equals(criteria))
                 {
                     collDistricts = DistrictPeer.doSelect(criteria, con);
                 }
             }
         }
         lastDistrictsCriteria = criteria;

         return collDistricts;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Regency is new, it will return
     * an empty collection; or if this Regency has previously
     * been saved, it will retrieve related Districts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Regency.
     */
    protected List getDistrictsJoinRegency(Criteria criteria)
        throws TorqueException
    {
                    if (collDistricts == null)
        {
            if (isNew())
            {
               collDistricts = new ArrayList();
            }
            else
            {
                              criteria.add(DistrictPeer.REGENCY_ID, getRegencyId());
                              collDistricts = DistrictPeer.doSelectJoinRegency(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(DistrictPeer.REGENCY_ID, getRegencyId());
                                    if (!lastDistrictsCriteria.equals(criteria))
            {
                collDistricts = DistrictPeer.doSelectJoinRegency(criteria);
            }
        }
        lastDistrictsCriteria = criteria;

        return collDistricts;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("RegencyId");
              fieldNames.add("ProvinceId");
              fieldNames.add("Regency");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("RegencyId"))
        {
                return getRegencyId();
            }
          if (name.equals("ProvinceId"))
        {
                return getProvinceId();
            }
          if (name.equals("Regency"))
        {
                return getRegency();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(RegencyPeer.REGENCY_ID))
        {
                return getRegencyId();
            }
          if (name.equals(RegencyPeer.PROVINCE_ID))
        {
                return getProvinceId();
            }
          if (name.equals(RegencyPeer.REGENCY))
        {
                return getRegency();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getRegencyId();
            }
              if (pos == 1)
        {
                return getProvinceId();
            }
              if (pos == 2)
        {
                return getRegency();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(RegencyPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        RegencyPeer.doInsert((Regency) this, con);
                        setNew(false);
                    }
                    else
                    {
                        RegencyPeer.doUpdate((Regency) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collDistricts != null)
            {
                for (int i = 0; i < collDistricts.size(); i++)
                {
                    ((District) collDistricts.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key regencyId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setRegencyId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setRegencyId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getRegencyId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Regency copy() throws TorqueException
    {
        return copyInto(new Regency());
    }
  
    protected Regency copyInto(Regency copyObj) throws TorqueException
    {
          copyObj.setRegencyId(regencyId);
          copyObj.setProvinceId(provinceId);
          copyObj.setRegency(regency);
  
                    copyObj.setRegencyId((String)null);
                        
                                      
                            
        List v = getDistricts();
        for (int i = 0; i < v.size(); i++)
        {
            District obj = (District) v.get(i);
            copyObj.addDistrict(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public RegencyPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Regency\n");
        str.append("-------\n")
           .append("RegencyId            : ")
           .append(getRegencyId())
           .append("\n")
           .append("ProvinceId           : ")
           .append(getProvinceId())
           .append("\n")
           .append("Regency              : ")
           .append(getRegency())
           .append("\n")
        ;
        return(str.toString());
    }
}

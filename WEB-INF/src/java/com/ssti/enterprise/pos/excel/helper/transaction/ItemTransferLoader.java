package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemTransferLoader.java,v 1.1 2008/02/17 12:57:57 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemTransferLoader.java,v $
 * Revision 1.1  2008/02/17 12:57:57  albert
 * *** empty log message ***
 *
 * Revision 1.1  2008/01/29 23:27:38  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/07/02 15:37:32  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ItemTransferLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( ItemTransferLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Item Code";
	private static final String s_END_TRANS = "End Trans";	
	
	public ItemTransferLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		
		ItemTransfer oTR = null;
		List vTD = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new ItemTransfer();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{    				
    				ItemTransferDetail oTD = new ItemTransferDetail();
    				mapDetails (oRow, oTR, oTD);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			ItemTransferTool.saveData(oTR, vTD, null);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;		
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	/*
	createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Trans No"); iCol++;		
	createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Transaction Date"); iCol++;	
	createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Create By"); iCol++;			
	createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Adjustment Type"); iCol++;
	createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"From Location"); iCol++;		
	createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"To Location"); iCol++;		
	createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Remark"); iCol++;	
    */
    protected void mapTrans (HSSFRow _oRow, ItemTransfer _oTR)
    	throws Exception
    {
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	if(StringUtil.isNotEmpty(sTransNo)) _oTR.setTransactionNo(sTransNo);
    	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setTransactionDate(dTrans);}

    	String sCreateBy = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sCreateBy)) {logError("Create By", sCreateBy);}
    	else {_oTR.setUserName(sCreateBy);}

    	String sAdjCode = getString (_oRow, iCol); iCol++;
    	AdjustmentType oAdj = AdjustmentTypeTool.getByCode(sAdjCode);
    	if (oAdj == null) {logError("Adjustment Type", sAdjCode);}
    	else {_oTR.setAdjustmentTypeId(oAdj.getAdjustmentTypeId());}
    	
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("From Location", sLocationCode);}
    	else 
    	{
    		_oTR.setFromLocationId(oLoc.getLocationId());
    		_oTR.setFromLocationName(oLoc.getLocationName());        	
    	}
    	
    	sLocationCode = getString (_oRow, iCol); iCol++;
    	oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("From Location", sLocationCode);}
    	else 
    	{
    		_oTR.setToLocationId(oLoc.getLocationId());
    		_oTR.setToLocationName(oLoc.getLocationName());
    	}

    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setDescription(sRemark);
    	 
    	//OTHER FIELD    	
    	if (!m_bTransError)
    	{
	    	_oTR.setStatus(i_PROCESSED);
	    	_oTR.setCourierId("");
	    	_oTR.setDescription(_oTR.getDescription() + setDesc(sTransNo, m_sUserName));
    	}
    }
    
    protected void mapDetails (HSSFRow _oRow, ItemTransfer _oTR, ItemTransferDetail _oTD)
		throws Exception
	{    	
    	int iCol = 0;
    	String sItemCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("item_code")).append(": ").append(sItemCode).append(s_LINE_SEPARATOR);
    	
    	Item oItem = ItemTool.getItemByCode(sItemCode);
    	if (oItem == null) {logError("Item Code", sItemCode);}
    	else 
    	{
    		_oTD.setItemId(oItem.getItemId());
    		_oTD.setItemCode(oItem.getItemCode());
    	}

    	BigDecimal dQty = getBigDecimal (_oRow, iCol); iCol++;
    	if (dQty == null) {logError("Qty", dQty);}
    	else 
    	{
    		_oTD.setQtyChanges(dQty);
    		_oTD.setQtyBase(UnitTool.getBaseQty(_oTD.getItemId(), _oTD.getUnitId(), dQty));
    	}

    	String sUnitCode = getString (_oRow, iCol); iCol++;
    	Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
    	if (oUnit == null) {logError("Unit Code", sUnitCode);}
    	else 
    	{
    		_oTD.setUnitId(oUnit.getUnitId());
    		_oTD.setUnitCode(oUnit.getUnitCode());
    	}

		BigDecimal dCost = getBigDecimal(_oRow, iCol); iCol++;
        _oTD.setCostPerUnit (dCost);	    	    	
	}   
}

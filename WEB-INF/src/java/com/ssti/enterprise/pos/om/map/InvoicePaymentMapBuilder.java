package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class InvoicePaymentMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.InvoicePaymentMapBuilder";

    /**
     * Item
     * @deprecated use InvoicePaymentPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "invoice_payment";
    }

  
    /**
     * invoice_payment.INVOICE_PAYMENT_ID
     * @return the column name for the INVOICE_PAYMENT_ID field
     * @deprecated use InvoicePaymentPeer.invoice_payment.INVOICE_PAYMENT_ID constant
     */
    public static String getInvoicePayment_InvoicePaymentId()
    {
        return "invoice_payment.INVOICE_PAYMENT_ID";
    }
  
    /**
     * invoice_payment.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use InvoicePaymentPeer.invoice_payment.TRANSACTION_ID constant
     */
    public static String getInvoicePayment_TransactionId()
    {
        return "invoice_payment.TRANSACTION_ID";
    }
  
    /**
     * invoice_payment.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use InvoicePaymentPeer.invoice_payment.TRANSACTION_TYPE constant
     */
    public static String getInvoicePayment_TransactionType()
    {
        return "invoice_payment.TRANSACTION_TYPE";
    }
  
    /**
     * invoice_payment.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use InvoicePaymentPeer.invoice_payment.PAYMENT_TYPE_ID constant
     */
    public static String getInvoicePayment_PaymentTypeId()
    {
        return "invoice_payment.PAYMENT_TYPE_ID";
    }
  
    /**
     * invoice_payment.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use InvoicePaymentPeer.invoice_payment.PAYMENT_TERM_ID constant
     */
    public static String getInvoicePayment_PaymentTermId()
    {
        return "invoice_payment.PAYMENT_TERM_ID";
    }
  
    /**
     * invoice_payment.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use InvoicePaymentPeer.invoice_payment.BANK_ID constant
     */
    public static String getInvoicePayment_BankId()
    {
        return "invoice_payment.BANK_ID";
    }
  
    /**
     * invoice_payment.VOUCHER_ID
     * @return the column name for the VOUCHER_ID field
     * @deprecated use InvoicePaymentPeer.invoice_payment.VOUCHER_ID constant
     */
    public static String getInvoicePayment_VoucherId()
    {
        return "invoice_payment.VOUCHER_ID";
    }
  
    /**
     * invoice_payment.TRANS_TOTAL_AMOUNT
     * @return the column name for the TRANS_TOTAL_AMOUNT field
     * @deprecated use InvoicePaymentPeer.invoice_payment.TRANS_TOTAL_AMOUNT constant
     */
    public static String getInvoicePayment_TransTotalAmount()
    {
        return "invoice_payment.TRANS_TOTAL_AMOUNT";
    }
  
    /**
     * invoice_payment.PAYMENT_AMOUNT
     * @return the column name for the PAYMENT_AMOUNT field
     * @deprecated use InvoicePaymentPeer.invoice_payment.PAYMENT_AMOUNT constant
     */
    public static String getInvoicePayment_PaymentAmount()
    {
        return "invoice_payment.PAYMENT_AMOUNT";
    }
  
    /**
     * invoice_payment.CASH_AMOUNT
     * @return the column name for the CASH_AMOUNT field
     * @deprecated use InvoicePaymentPeer.invoice_payment.CASH_AMOUNT constant
     */
    public static String getInvoicePayment_CashAmount()
    {
        return "invoice_payment.CASH_AMOUNT";
    }
  
    /**
     * invoice_payment.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use InvoicePaymentPeer.invoice_payment.TRANSACTION_DATE constant
     */
    public static String getInvoicePayment_TransactionDate()
    {
        return "invoice_payment.TRANSACTION_DATE";
    }
  
    /**
     * invoice_payment.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use InvoicePaymentPeer.invoice_payment.DUE_DATE constant
     */
    public static String getInvoicePayment_DueDate()
    {
        return "invoice_payment.DUE_DATE";
    }
  
    /**
     * invoice_payment.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use InvoicePaymentPeer.invoice_payment.BANK_ISSUER constant
     */
    public static String getInvoicePayment_BankIssuer()
    {
        return "invoice_payment.BANK_ISSUER";
    }
  
    /**
     * invoice_payment.MDR_AMOUNT
     * @return the column name for the MDR_AMOUNT field
     * @deprecated use InvoicePaymentPeer.invoice_payment.MDR_AMOUNT constant
     */
    public static String getInvoicePayment_MdrAmount()
    {
        return "invoice_payment.MDR_AMOUNT";
    }
  
    /**
     * invoice_payment.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use InvoicePaymentPeer.invoice_payment.REFERENCE_NO constant
     */
    public static String getInvoicePayment_ReferenceNo()
    {
        return "invoice_payment.REFERENCE_NO";
    }
  
    /**
     * invoice_payment.APPROVAL_NO
     * @return the column name for the APPROVAL_NO field
     * @deprecated use InvoicePaymentPeer.invoice_payment.APPROVAL_NO constant
     */
    public static String getInvoicePayment_ApprovalNo()
    {
        return "invoice_payment.APPROVAL_NO";
    }
  
    /**
     * invoice_payment.TERMINAL_ID
     * @return the column name for the TERMINAL_ID field
     * @deprecated use InvoicePaymentPeer.invoice_payment.TERMINAL_ID constant
     */
    public static String getInvoicePayment_TerminalId()
    {
        return "invoice_payment.TERMINAL_ID";
    }
  
    /**
     * invoice_payment.TRACE_NO
     * @return the column name for the TRACE_NO field
     * @deprecated use InvoicePaymentPeer.invoice_payment.TRACE_NO constant
     */
    public static String getInvoicePayment_TraceNo()
    {
        return "invoice_payment.TRACE_NO";
    }
  
    /**
     * invoice_payment.ENTRY_MODE
     * @return the column name for the ENTRY_MODE field
     * @deprecated use InvoicePaymentPeer.invoice_payment.ENTRY_MODE constant
     */
    public static String getInvoicePayment_EntryMode()
    {
        return "invoice_payment.ENTRY_MODE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("invoice_payment");
        TableMap tMap = dbMap.getTable("invoice_payment");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("invoice_payment.INVOICE_PAYMENT_ID", "");
                          tMap.addColumn("invoice_payment.TRANSACTION_ID", "");
                            tMap.addColumn("invoice_payment.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("invoice_payment.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("invoice_payment.PAYMENT_TERM_ID", "");
                          tMap.addColumn("invoice_payment.BANK_ID", "");
                          tMap.addColumn("invoice_payment.VOUCHER_ID", "");
                            tMap.addColumn("invoice_payment.TRANS_TOTAL_AMOUNT", bd_ZERO);
                            tMap.addColumn("invoice_payment.PAYMENT_AMOUNT", bd_ZERO);
                            tMap.addColumn("invoice_payment.CASH_AMOUNT", bd_ZERO);
                          tMap.addColumn("invoice_payment.TRANSACTION_DATE", new Date());
                          tMap.addColumn("invoice_payment.DUE_DATE", new Date());
                          tMap.addColumn("invoice_payment.BANK_ISSUER", "");
                            tMap.addColumn("invoice_payment.MDR_AMOUNT", bd_ZERO);
                          tMap.addColumn("invoice_payment.REFERENCE_NO", "");
                          tMap.addColumn("invoice_payment.APPROVAL_NO", "");
                          tMap.addColumn("invoice_payment.TERMINAL_ID", "");
                          tMap.addColumn("invoice_payment.TRACE_NO", "");
                          tMap.addColumn("invoice_payment.ENTRY_MODE", "");
          }
}

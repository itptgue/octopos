package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class KategoriAccountMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.KategoriAccountMapBuilder";

    /**
     * Item
     * @deprecated use KategoriAccountPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "kategori_account";
    }

  
    /**
     * kategori_account.KATEGORI_ID
     * @return the column name for the KATEGORI_ID field
     * @deprecated use KategoriAccountPeer.kategori_account.KATEGORI_ID constant
     */
    public static String getKategoriAccount_KategoriId()
    {
        return "kategori_account.KATEGORI_ID";
    }
  
    /**
     * kategori_account.INVENTORY_ACCOUNT
     * @return the column name for the INVENTORY_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.INVENTORY_ACCOUNT constant
     */
    public static String getKategoriAccount_InventoryAccount()
    {
        return "kategori_account.INVENTORY_ACCOUNT";
    }
  
    /**
     * kategori_account.SALES_ACCOUNT
     * @return the column name for the SALES_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.SALES_ACCOUNT constant
     */
    public static String getKategoriAccount_SalesAccount()
    {
        return "kategori_account.SALES_ACCOUNT";
    }
  
    /**
     * kategori_account.SALES_RETURN_ACCOUNT
     * @return the column name for the SALES_RETURN_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.SALES_RETURN_ACCOUNT constant
     */
    public static String getKategoriAccount_SalesReturnAccount()
    {
        return "kategori_account.SALES_RETURN_ACCOUNT";
    }
  
    /**
     * kategori_account.ITEM_DISCOUNT_ACCOUNT
     * @return the column name for the ITEM_DISCOUNT_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.ITEM_DISCOUNT_ACCOUNT constant
     */
    public static String getKategoriAccount_ItemDiscountAccount()
    {
        return "kategori_account.ITEM_DISCOUNT_ACCOUNT";
    }
  
    /**
     * kategori_account.COGS_ACCOUNT
     * @return the column name for the COGS_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.COGS_ACCOUNT constant
     */
    public static String getKategoriAccount_CogsAccount()
    {
        return "kategori_account.COGS_ACCOUNT";
    }
  
    /**
     * kategori_account.PURCHASE_RETURN_ACCOUNT
     * @return the column name for the PURCHASE_RETURN_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.PURCHASE_RETURN_ACCOUNT constant
     */
    public static String getKategoriAccount_PurchaseReturnAccount()
    {
        return "kategori_account.PURCHASE_RETURN_ACCOUNT";
    }
  
    /**
     * kategori_account.EXPENSE_ACCOUNT
     * @return the column name for the EXPENSE_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.EXPENSE_ACCOUNT constant
     */
    public static String getKategoriAccount_ExpenseAccount()
    {
        return "kategori_account.EXPENSE_ACCOUNT";
    }
  
    /**
     * kategori_account.UNBILLED_GOODS_ACCOUNT
     * @return the column name for the UNBILLED_GOODS_ACCOUNT field
     * @deprecated use KategoriAccountPeer.kategori_account.UNBILLED_GOODS_ACCOUNT constant
     */
    public static String getKategoriAccount_UnbilledGoodsAccount()
    {
        return "kategori_account.UNBILLED_GOODS_ACCOUNT";
    }
  
    /**
     * kategori_account.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use KategoriAccountPeer.kategori_account.CREATE_BY constant
     */
    public static String getKategoriAccount_CreateBy()
    {
        return "kategori_account.CREATE_BY";
    }
  
    /**
     * kategori_account.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use KategoriAccountPeer.kategori_account.UPDATE_DATE constant
     */
    public static String getKategoriAccount_UpdateDate()
    {
        return "kategori_account.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("kategori_account");
        TableMap tMap = dbMap.getTable("kategori_account");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("kategori_account.KATEGORI_ID", "");
                          tMap.addColumn("kategori_account.INVENTORY_ACCOUNT", "");
                          tMap.addColumn("kategori_account.SALES_ACCOUNT", "");
                          tMap.addColumn("kategori_account.SALES_RETURN_ACCOUNT", "");
                          tMap.addColumn("kategori_account.ITEM_DISCOUNT_ACCOUNT", "");
                          tMap.addColumn("kategori_account.COGS_ACCOUNT", "");
                          tMap.addColumn("kategori_account.PURCHASE_RETURN_ACCOUNT", "");
                          tMap.addColumn("kategori_account.EXPENSE_ACCOUNT", "");
                          tMap.addColumn("kategori_account.UNBILLED_GOODS_ACCOUNT", "");
                          tMap.addColumn("kategori_account.CREATE_BY", "");
                          tMap.addColumn("kategori_account.UPDATE_DATE", new Date());
          }
}

package com.ssti.enterprise.pos.tools.data;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.ApPaymentPeer;
import com.ssti.enterprise.pos.om.ArPaymentPeer;
import com.ssti.enterprise.pos.om.CashFlowPeer;
import com.ssti.enterprise.pos.om.CreditMemoPeer;
import com.ssti.enterprise.pos.om.DebitMemoPeer;
import com.ssti.enterprise.pos.om.DeliveryOrderPeer;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.om.ItemTransferPeer;
import com.ssti.enterprise.pos.om.JournalVoucherPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoicePeer;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceiptPeer;
import com.ssti.enterprise.pos.om.PurchaseRequestPeer;
import com.ssti.enterprise.pos.om.PurchaseReturnPeer;
import com.ssti.enterprise.pos.om.SalesOrderPeer;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.om.TransEdit;
import com.ssti.enterprise.pos.om.TransEditPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Update Transaction Field
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $

 * </pre><br>
 */
public class TransEditTool extends BaseTool implements AppAttributes, TransactionAttributes
{
	static Log log = LogFactory.getLog(TransEditTool.class);

    static TransEditTool instance = null;    
    public static synchronized TransEditTool getInstance() 
    {
        if (instance == null) instance = new TransEditTool();
        return instance;
    }
    
    static String s_RMRK = "remark";
    static String s_DESC = "description";

    //refer i_TRANS to Transaction Attributes
    //refer m_TRANS to BaseTool
    public static Map m_FIELD = new HashMap();
    public static Map m_TBL = new HashMap();
    static
    {
        try
        {
            String[] a_RMR = {s_RMRK};
            String[] a_DES = {s_DESC};
            String[] a_CF1 = {s_RMRK, "bankIssuer", "referenceNo"};
            String[] a_CF2 = {s_DESC, "bankIssuer", "referenceNo"};            
            String[] a_FAC = {"serialNo", "fixedAssetName"};            
            
            m_FIELD.put(i_PO, a_RMR);
            m_FIELD.put(i_PR, a_RMR);
            m_FIELD.put(i_PI, a_RMR);
            m_FIELD.put(i_PT, a_RMR);
            m_FIELD.put(i_SO, a_RMR);
            m_FIELD.put(i_DO, a_RMR);
            m_FIELD.put(i_SI, a_RMR);
            m_FIELD.put(i_SR, a_RMR);            
            m_FIELD.put(i_RQ, a_RMR);
            
            m_FIELD.put(i_IR, a_DES);
            m_FIELD.put(i_IT, a_DES);

            m_FIELD.put(i_CM, a_CF1);
            m_FIELD.put(i_DM, a_CF1);
            m_FIELD.put(i_RP, a_CF1);
            m_FIELD.put(i_PP, a_CF1);
            m_FIELD.put(i_CF, a_CF2);            

            m_FIELD.put(i_JV, a_DES);            
            m_FIELD.put(i_FA, a_FAC);            

            m_TBL.put(i_PO, PurchaseOrderPeer   .TABLE_NAME);
            m_TBL.put(i_PR, PurchaseReceiptPeer .TABLE_NAME);
            m_TBL.put(i_PI, PurchaseInvoicePeer .TABLE_NAME);
            m_TBL.put(i_PT, PurchaseReturnPeer  .TABLE_NAME);
            m_TBL.put(i_SO, SalesOrderPeer      .TABLE_NAME);
            m_TBL.put(i_DO, DeliveryOrderPeer   .TABLE_NAME);
            m_TBL.put(i_SI, SalesTransactionPeer.TABLE_NAME);
            m_TBL.put(i_SR, SalesReturnPeer     .TABLE_NAME);
            m_TBL.put(i_RQ, PurchaseRequestPeer .TABLE_NAME);
            m_TBL.put(i_IT, ItemTransferPeer    .TABLE_NAME);
            m_TBL.put(i_IR, IssueReceiptPeer    .TABLE_NAME);

            m_TBL.put(i_CM, CreditMemoPeer      .TABLE_NAME);
            m_TBL.put(i_DM, DebitMemoPeer       .TABLE_NAME);
            m_TBL.put(i_RP, ArPaymentPeer       .TABLE_NAME);
            m_TBL.put(i_PP, ApPaymentPeer       .TABLE_NAME);
            m_TBL.put(i_CF, CashFlowPeer        .TABLE_NAME);
            m_TBL.put(i_JV, JournalVoucherPeer  .TABLE_NAME);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
    }

    public static TransEdit getByID(String _sID)
    {
        try
        {
            Criteria oCrit = new Criteria();
            oCrit.add(TransEditPeer.TRANS_EDIT_ID, _sID);
            List v = TransEditPeer.doSelect(oCrit);
            if(v.size() > 0)
            {
                return (TransEdit)v.get(0);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
        return null;
    }
    
    public static Map getTrans()
    {        
        return m_TRANS;
    }

    public static Map getNoField()
    {        
        return m_TRNOF;
    }

    public static Map getField()
    {        
        return m_FIELD;
    }
    
    public static List findByNo(int _iType, String _sNo)
    {
        try
        {
            Criteria oCrit = new Criteria();
            oCrit.add((String)m_TRNO.get(_iType), (Object)_sNo, Criteria.ILIKE);
            
            Class cParam[] = {Criteria.class, Connection.class};
            Object oParam[] = {oCrit, null}; 
            
            return (List)((Class)m_PCLS.get(_iType)).getMethod("doSelect",cParam).invoke(null, oParam);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
        return null;
    }

    public static Object getByNo(int _iType, String _sNo)
    {
        try
        {
            List v = findByNo(_iType, _sNo);
            if (v != null && v.size() > 0)
            {
                return v.get(0);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
        return null;
    }

    public static void updateTrans(TransEdit _oTR)
        throws Exception
    {
        if (_oTR != null)
        {
            String sTblField = StringUtil.field2Table(_oTR.getFieldName());
            int iTR = _oTR.getTransType();
            StringBuilder oSQL = new StringBuilder();
            oSQL.append("UPDATE ").append(m_TBL.get(iTR))
                .append(" SET ").append(sTblField).append(" = '").append(_oTR.getNewValue()).append("'")
                .append(" WHERE ").append(m_TRNO.get(iTR)).append(" = '").append(_oTR.getTransNo()).append("'")
                ;
            
            log.debug(oSQL.toString());
            System.out.println(oSQL.toString());

            Connection oConn = null;
            try
            {
                oConn = beginTrans();
                BasePeer.executeStatement(oSQL.toString(),oConn);                
                if (StringUtil.isEmpty(_oTR.getTransEditId()))
                {
                	_oTR.setEditDate(new Date());
                    _oTR.setTransEditId(IDGenerator.generateSysID());
                }
                _oTR.save(oConn);
                commit(oConn);
            }
            catch (Exception e)
            {
                rollback(oConn);
                throw new NestableException(
                        "Failed Updating Trans: " + _oTR.getTransNo() + " MSG:" + e.getMessage(), e);
            }            
        }
    }
    
    public static List findData(int _iCond, String _sKey, Date _dStart, Date _dEnd)
    {
        try
        {
            Criteria oCrit = new Criteria();
            if (StringUtil.isNotEmpty(_sKey))
            {
                if (_iCond ==  1)
                {
                    oCrit.add(TransEditPeer.TRANS_NO, (Object)_sKey, Criteria.ILIKE);
                }
                if (_iCond ==  2)
                {
                    oCrit.add(TransEditPeer.OLD_VALUE, (Object)_sKey, Criteria.ILIKE);
                }
                if (_iCond ==  3)
                {
                    oCrit.add(TransEditPeer.NEW_VALUE, (Object)_sKey, Criteria.ILIKE);
                }    
                if (_iCond ==  3)
                {
                    oCrit.add(TransEditPeer.USER_NAME, (Object)_sKey, Criteria.ILIKE);
                }    
                
            }
            if (_dStart != null)
            {
                oCrit.add(TransEditPeer.EDIT_DATE, _dStart, Criteria.GREATER_EQUAL);                
            }
            if (_dEnd != null)
            {
                oCrit.add(TransEditPeer.EDIT_DATE, _dEnd, Criteria.LESS_EQUAL);                
            }
            return TransEditPeer.doSelect(oCrit);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
        return null;
    }
    
    
    /**
     * create new trans edit directly from templates
     * 
     * @param _iType
     * @param _sField
     * @param _sNewValue
     * @return
     */
    public static TransEdit createTransEdit(int _iType, String _sTransID, String _sTransNo, String _sFieldName, String _sNewValue)
    {
    	TransEdit oTE = new TransEdit();
    	oTE.setTransType(_iType);
    	oTE.setTransId(_sTransID);
    	oTE.setTransNo(_sTransNo);    	
    	oTE.setFieldName(_sFieldName);
    	oTE.setNewValue(_sNewValue);    	
    	try 
    	{
    		oTE.loadTrans();    	
        	if (oTE.getTrans() != null)
        	{
        		String sOldValue = "";
        		Object oOld = BeanUtil.invokeGetter(oTE.getTrans(), _sFieldName);
        		if(oOld != null)
        		{
        			sOldValue = oOld.toString();
        		}
        		oTE.setOldValue(sOldValue);
            	updateTrans(oTE);
        	}	
		} 
    	catch (Exception e) 
    	{
    		log.error(e);
    	}
    	return oTE;
    }
}

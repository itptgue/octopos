package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PaymentTypeBank
 */
public abstract class BasePaymentTypeBank extends BaseObject
{
    /** The Peer class */
    private static final PaymentTypeBankPeer peer =
        new PaymentTypeBankPeer();

        
    /** The value for the paymentTypeBankId field */
    private String paymentTypeBankId;
      
    /** The value for the bankCode field */
    private String bankCode;
      
    /** The value for the bankName field */
    private String bankName;
  
    
    /**
     * Get the PaymentTypeBankId
     *
     * @return String
     */
    public String getPaymentTypeBankId()
    {
        return paymentTypeBankId;
    }

                        
    /**
     * Set the value of PaymentTypeBankId
     *
     * @param v new value
     */
    public void setPaymentTypeBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeBankId, v))
              {
            this.paymentTypeBankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankCode
     *
     * @return String
     */
    public String getBankCode()
    {
        return bankCode;
    }

                        
    /**
     * Set the value of BankCode
     *
     * @param v new value
     */
    public void setBankCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankCode, v))
              {
            this.bankCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankName
     *
     * @return String
     */
    public String getBankName()
    {
        return bankName;
    }

                        
    /**
     * Set the value of BankName
     *
     * @param v new value
     */
    public void setBankName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankName, v))
              {
            this.bankName = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PaymentTypeBankId");
              fieldNames.add("BankCode");
              fieldNames.add("BankName");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PaymentTypeBankId"))
        {
                return getPaymentTypeBankId();
            }
          if (name.equals("BankCode"))
        {
                return getBankCode();
            }
          if (name.equals("BankName"))
        {
                return getBankName();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PaymentTypeBankPeer.PAYMENT_TYPE_BANK_ID))
        {
                return getPaymentTypeBankId();
            }
          if (name.equals(PaymentTypeBankPeer.BANK_CODE))
        {
                return getBankCode();
            }
          if (name.equals(PaymentTypeBankPeer.BANK_NAME))
        {
                return getBankName();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPaymentTypeBankId();
            }
              if (pos == 1)
        {
                return getBankCode();
            }
              if (pos == 2)
        {
                return getBankName();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PaymentTypeBankPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PaymentTypeBankPeer.doInsert((PaymentTypeBank) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PaymentTypeBankPeer.doUpdate((PaymentTypeBank) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key paymentTypeBankId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPaymentTypeBankId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPaymentTypeBankId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPaymentTypeBankId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PaymentTypeBank copy() throws TorqueException
    {
        return copyInto(new PaymentTypeBank());
    }
  
    protected PaymentTypeBank copyInto(PaymentTypeBank copyObj) throws TorqueException
    {
          copyObj.setPaymentTypeBankId(paymentTypeBankId);
          copyObj.setBankCode(bankCode);
          copyObj.setBankName(bankName);
  
                    copyObj.setPaymentTypeBankId((String)null);
                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PaymentTypeBankPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PaymentTypeBank\n");
        str.append("---------------\n")
           .append("PaymentTypeBankId    : ")
           .append(getPaymentTypeBankId())
           .append("\n")
           .append("BankCode             : ")
           .append(getBankCode())
           .append("\n")
           .append("BankName             : ")
           .append(getBankName())
           .append("\n")
        ;
        return(str.toString());
    }
}

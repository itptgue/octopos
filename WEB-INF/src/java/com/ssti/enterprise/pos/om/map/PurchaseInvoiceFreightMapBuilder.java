package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseInvoiceFreightMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseInvoiceFreightMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseInvoiceFreightPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_invoice_freight";
    }

  
    /**
     * purchase_invoice_freight.PURCHASE_INVOICE_FREIGHT_ID
     * @return the column name for the PURCHASE_INVOICE_FREIGHT_ID field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.PURCHASE_INVOICE_FREIGHT_ID constant
     */
    public static String getPurchaseInvoiceFreight_PurchaseInvoiceFreightId()
    {
        return "purchase_invoice_freight.PURCHASE_INVOICE_FREIGHT_ID";
    }
  
    /**
     * purchase_invoice_freight.PURCHASE_INVOICE_ID
     * @return the column name for the PURCHASE_INVOICE_ID field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.PURCHASE_INVOICE_ID constant
     */
    public static String getPurchaseInvoiceFreight_PurchaseInvoiceId()
    {
        return "purchase_invoice_freight.PURCHASE_INVOICE_ID";
    }
  
    /**
     * purchase_invoice_freight.FREIGHT_PI_ID
     * @return the column name for the FREIGHT_PI_ID field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.FREIGHT_PI_ID constant
     */
    public static String getPurchaseInvoiceFreight_FreightPiId()
    {
        return "purchase_invoice_freight.FREIGHT_PI_ID";
    }
  
    /**
     * purchase_invoice_freight.FREIGHT_PI_NO
     * @return the column name for the FREIGHT_PI_NO field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.FREIGHT_PI_NO constant
     */
    public static String getPurchaseInvoiceFreight_FreightPiNo()
    {
        return "purchase_invoice_freight.FREIGHT_PI_NO";
    }
  
    /**
     * purchase_invoice_freight.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.ACCOUNT_ID constant
     */
    public static String getPurchaseInvoiceFreight_AccountId()
    {
        return "purchase_invoice_freight.ACCOUNT_ID";
    }
  
    /**
     * purchase_invoice_freight.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.DEPARTMENT_ID constant
     */
    public static String getPurchaseInvoiceFreight_DepartmentId()
    {
        return "purchase_invoice_freight.DEPARTMENT_ID";
    }
  
    /**
     * purchase_invoice_freight.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.PROJECT_ID constant
     */
    public static String getPurchaseInvoiceFreight_ProjectId()
    {
        return "purchase_invoice_freight.PROJECT_ID";
    }
  
    /**
     * purchase_invoice_freight.FREIGHT_AMOUNT
     * @return the column name for the FREIGHT_AMOUNT field
     * @deprecated use PurchaseInvoiceFreightPeer.purchase_invoice_freight.FREIGHT_AMOUNT constant
     */
    public static String getPurchaseInvoiceFreight_FreightAmount()
    {
        return "purchase_invoice_freight.FREIGHT_AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_invoice_freight");
        TableMap tMap = dbMap.getTable("purchase_invoice_freight");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_invoice_freight.PURCHASE_INVOICE_FREIGHT_ID", "");
                          tMap.addForeignKey(
                "purchase_invoice_freight.PURCHASE_INVOICE_ID", "" , "purchase_invoice" ,
                "purchase_invoice_id");
                          tMap.addColumn("purchase_invoice_freight.FREIGHT_PI_ID", "");
                          tMap.addColumn("purchase_invoice_freight.FREIGHT_PI_NO", "");
                          tMap.addColumn("purchase_invoice_freight.ACCOUNT_ID", "");
                          tMap.addColumn("purchase_invoice_freight.DEPARTMENT_ID", "");
                          tMap.addColumn("purchase_invoice_freight.PROJECT_ID", "");
                            tMap.addColumn("purchase_invoice_freight.FREIGHT_AMOUNT", bd_ZERO);
          }
}

package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.IssueReceiptDetail;
import com.ssti.enterprise.pos.om.IssueReceiptPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.om.ItemTransferDetail;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InventoryJournalTool.java,v 1.18 2008/06/29 07:15:44 albert Exp $ <br>
 *
 * <pre>
 * $Log: InventoryJournalTool.java,v $
 * Revision 1.18  2008/06/29 07:15:44  albert
 * *** empty log message ***
 *
 * 2016-12-27
 * - see ItemTransferTool
 * - add m_iTRFType -1 create transfer out and in together, 1 create out journal only, 2 create in journal only
 * - change constructor add type
 *
 * 2015-03-03
 * Very Important Change in createIssueReceiptJournal, cost must be calculated against UoM because
 * In transaction we allow user to use different UoM
 * 
 * </pre><br>
 */
public class InventoryJournalTool extends BaseJournalTool
{
	private static Log log = LogFactory.getLog ( InventoryJournalTool.class );

	private Currency m_oCurrency = null;
	private Currency m_oBaseCurrency = null;
	private double m_dRate = 1;
	private int m_iTRFType = -1;

	/**
	 * init inventory journal from issue receipt
	 * 
	 * @param _oTR IssueReceipt
	 * @param _oConn Connection
	 * @throws Exception
	 */
	public InventoryJournalTool(IssueReceipt _oTR, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		m_oConn = _oConn;
		
	    m_oPeriod = PeriodTool.getPeriodByDate(_oTR.getTransactionDate(), m_oConn);
	    m_oCurrency = CurrencyTool.getDefaultCurrency(m_oConn);
	    
	    m_sTransID = _oTR.getIssueReceiptId();
	    m_sTransNo = _oTR.getTransactionNo();
		m_sUserName = _oTR.getUserName();
		m_sLocationID = _oTR.getLocationId();    	
	
        StringBuilder oRemark = new StringBuilder(_oTR.getDescription());
    	if (StringUtil.isEmpty(_oTR.getDescription()))
    	{
    		oRemark.append(LocaleTool.getString("issue_receipt")).append(" ").append(m_sTransNo);
    	}		
    	m_sRemark = oRemark.toString();
	
		m_dTransDate = _oTR.getTransactionDate();
		m_dCreate = new Date();
	}
	
	/**
	 * init inventory journal from Item Transfer
	 * 
	 * @param _oTR ItemTransfer
	 * @param _oConn Connection
	 * @throws Exception
	 */
	public InventoryJournalTool(ItemTransfer _oTR, int _iType, Connection _oConn)
		throws Exception
	{
		validate(_oConn);
		m_oConn = _oConn;
		
	    m_oPeriod = PeriodTool.getPeriodByDate(_oTR.getTransactionDate(), m_oConn);
	    m_oCurrency = CurrencyTool.getDefaultCurrency(m_oConn);
	    
	    m_sTransID = _oTR.getItemTransferId();
	    m_sTransNo = _oTR.getTransactionNo();
		m_sUserName = _oTR.getUserName();
		m_sLocationID = _oTR.getFromLocationId();    	
	
	    StringBuilder oRemark = new StringBuilder(_oTR.getDescription());
		m_sRemark = oRemark.toString();
	
		m_dTransDate = _oTR.getTransactionDate();
		m_dCreate = new Date();
		m_iTRFType = _iType;
		
		if(m_iTRFType == ItemTransferTool.i_TI)
		{
			m_dTransDate = _oTR.getReceiveDate();
		}
	}	
		
	/**
	 * Create IssueReceipt Journal
	 * Receipt Unplanned : Inventory On Adjustment
	 * Issue Unplanned : Adjustment On Inventory
	 * 
	 * @param _oTR IssueReceipt
	 * @param _vTD List of IssueReceiptDetail
	 * @throws Exception
	 */
	public void createIssueReceiptJournal(IssueReceipt _oTR, List _vTD) 
    	throws Exception
    {   
		validate(m_oConn);
        try
        {
            List vGLTrans = new ArrayList(_vTD.size());
            for (int i = 0; i < _vTD.size(); i++)
            {
                IssueReceiptDetail oTD = (IssueReceiptDetail) _vTD.get(i);
                String sItemID = oTD.getItemId();
                Item oItem = ItemTool.getItemByID (sItemID, m_oConn);
                createIssueReceiptGLTrans (_oTR, oTD, oItem, vGLTrans);
            }
            setGLTrans (vGLTrans);
            log.debug ("vGLTrans : " + vGLTrans);
            
            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();
            throw new NestableException("GL Journal Failed : " + _oEx.getMessage(), _oEx);
        }
	}
	
	/**
	 * Create GLTransaction Pair from single Item in Detail
	 * and add to existing list
	 * 
	 * @param _oTR IssueReceipt
	 * @param _oTD IssueReceiptDetail
	 * @param _oItem Item
	 * @param _vGLTrans List of existing GlTransaction
	 * @throws Exception
	 */
	private void createIssueReceiptGLTrans (IssueReceipt _oTR, 
	                                        IssueReceiptDetail _oTD, 
	                                        Item _oItem, 
	                                        List _vGLTrans) 
    	throws Exception
    {
		double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(), _oTD.getUnitId(), null);
		double dQtyChange = _oTD.getQtyChanges().doubleValue();
		double dQtyBase   = dQtyChange * dBaseValue;
		double dCost      = _oTD.getCost().doubleValue() / dBaseValue;
		double dAmount    = dCost * dQtyBase;
        
        //Inventory
        String sInventory = _oItem.getInventoryAccount(); 
        Account oInv = AccountTool.getAccountByID (sInventory, m_oConn);                
        validate (oInv, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);

        GlTransaction oInvTrans = new GlTransaction();
    
	    if (dAmount >= 0) oInvTrans.setDebitCredit (GlAttributes.i_DEBIT);
        if (dAmount <  0) oInvTrans.setDebitCredit (GlAttributes.i_CREDIT);
    
        if (dAmount < 0) dAmount = dAmount * -1;
        BigDecimal bdAmount = new BigDecimal (dAmount);
        
        oInvTrans.setTransactionType  (GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED);
        oInvTrans.setProjectId        (_oTD.getProjectId());
        oInvTrans.setDepartmentId     (_oTD.getDepartmentId());
        oInvTrans.setAccountId        (oInv.getAccountId());
        oInvTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
        oInvTrans.setCurrencyRate     (bd_ONE);
        oInvTrans.setAmount           (bdAmount);
        oInvTrans.setAmountBase       (bdAmount);
    
        //ADJUSMENT ACCOUNT 
        String sAdjustment  = _oTR.getAccountId();
        Account oAdjustment = AccountTool.getAccountByID (sAdjustment, m_oConn);
        validate (oAdjustment, _oTR.getTransactionNo(), IssueReceiptPeer.ACCOUNT_ID);

        if (oAdjustment != null)
        {
            GlTransaction oAdjTrans       = new GlTransaction();

            oAdjTrans.setTransactionType  (GlAttributes.i_GL_TRANS_RECEIPT_UNPLANNED);
            oAdjTrans.setProjectId        (_oTD.getProjectId());
            oAdjTrans.setDepartmentId     (_oTD.getDepartmentId());
            oAdjTrans.setAccountId        (oAdjustment.getAccountId());
            oAdjTrans.setCurrencyId       (m_oCurrency.getCurrencyId());
            oAdjTrans.setCurrencyRate     (bd_ONE);
            oAdjTrans.setAmount           (bdAmount);
            oAdjTrans.setAmountBase       (bdAmount);
            
            if (oInvTrans.getDebitCredit() == GlAttributes.i_CREDIT) oAdjTrans.setDebitCredit(GlAttributes.i_DEBIT);
            if (oInvTrans.getDebitCredit() == GlAttributes.i_DEBIT ) oAdjTrans.setDebitCredit(GlAttributes.i_CREDIT); 
                        
            addJournal (oInvTrans, oInv, _vGLTrans);	
            addJournal (oAdjTrans, oAdjustment, _vGLTrans);	
	    }
	    else 
	    {
	        throw new Exception ("Adjusment Journal Invalid, Adjustment Account is not set properly");
	    }
	}	

	/**
	 * Create Item Transfer Journal
	 * Inventory (From Location) On Inventory (To Location)
	 * 
	 * @param _oTR ItemTransfer
	 * @param _vTD List of ItemTransferDetail
	 * @throws Exception
	 */
	public void createItemTransferJournal(ItemTransfer _oTR, List _vTD) 
		throws Exception
	{   
		validate(m_oConn);
	    try
	    {
	        List vGLTrans = new ArrayList(_vTD.size());
	        for (int i = 0; i < _vTD.size(); i++)
	        {
	            ItemTransferDetail oTD = (ItemTransferDetail) _vTD.get(i);
	            String sItemID = oTD.getItemId();
	            Item oItem = ItemTool.getItemByID (sItemID, m_oConn);
	            createItemTransferGLTrans (_oTR, oTD, oItem, vGLTrans);
	        }
	        setGLTrans (vGLTrans);
	        log.debug ("vGLTrans : " + vGLTrans );
	        
	        GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);
	    }
	    catch (Exception _oEx)
	    {
	        throw new NestableException("GL Journal Failed : " + _oEx.getMessage(), _oEx);
	    }
	}
	
	/**
	 * Create GLTransaction Pair from single Item in Detail
	 * and add to existing list
	 * 
	 * @param _oTR ItemTransfer
	 * @param _oTD ItemTransferDetail
	 * @param _oItem Item
	 * @param _vGLTrans List of existing GlTransaction
	 * @param _iType type of transfer
	 * @throws Exception
	 */
	private void createItemTransferGLTrans (ItemTransfer _oTR, 
	                                        ItemTransferDetail _oTD, 
	                                        Item _oItem, 
	                                        List _vGLTrans)
		throws Exception
	{
	    double dCost = _oTD.getCostPerUnit().doubleValue();
	    double dQty = _oTD.getQtyChanges().doubleValue();
	    double dQtyBase = UnitTool.getBaseQty(_oTD.getItemId(), _oTD.getUnitId(), dQty);
	    double dAmount = dCost * dQtyBase;
	    
	    if (dCost < 0 || dQtyBase < 0)
	    {
	    	throw new Exception ("Invalid Cost / Qty (Minus) for Item " + _oItem.getItemCode());
	    }
	    BigDecimal bdAmount = new BigDecimal (dAmount);
	    
	    //Inventory	    	   
	    String sInventory = _oItem.getInventoryAccount(); 
	    Account oInvAcc = AccountTool.getAccountByID (sInventory, m_oConn);                
	    validate (oInvAcc, _oItem.getItemCode(), ItemPeer.INVENTORY_ACCOUNT);

	    if(m_iTRFType <= 0 || m_iTRFType == ItemTransferTool.i_TO)
	    {
		    GlTransaction oTo = new GlTransaction();	
		    oTo.setTransactionType  (GlAttributes.i_GL_TRANS_ITEM_TRANSFER);	    
		    oTo.setLocationId		(_oTR.getFromLocationId());
		    oTo.setProjectId        ("");
		    oTo.setDepartmentId     ("");
		    oTo.setAccountId        (oInvAcc.getAccountId());
		    oTo.setCurrencyId       (m_oCurrency.getCurrencyId());
		    oTo.setCurrencyRate     (bd_ONE);
		    oTo.setAmount           (bdAmount);
		    oTo.setAmountBase       (bdAmount);
		    oTo.setDebitCredit	  	(i_CREDIT);
		    oTo.setDescription 		(m_sRemark + " Transfer To " + _oTR.getToLocationName());	    
		    addJournal (oTo, oInvAcc, _vGLTrans);	
	    }
	    if(m_iTRFType <= 0 || m_iTRFType == ItemTransferTool.i_TI)
	    {
		    GlTransaction oFrom = new GlTransaction();
		    oFrom.setTransactionType  (GlAttributes.i_GL_TRANS_ITEM_TRANSFER);
		    oFrom.setLocationId		  (_oTR.getToLocationId());
		    oFrom.setProjectId        ("");
		    oFrom.setDepartmentId     ("");
		    oFrom.setAccountId        (oInvAcc.getAccountId());
		    oFrom.setCurrencyId       (m_oCurrency.getCurrencyId());
		    oFrom.setCurrencyRate     (bd_ONE);
		    oFrom.setAmount           (bdAmount);
		    oFrom.setAmountBase       (bdAmount);
		    oFrom.setDebitCredit	  (i_DEBIT);
		    oFrom.setDescription 	  (m_sRemark + " Transfer From " + _oTR.getFromLocationName());	    	    
		    addJournal (oFrom, oInvAcc, _vGLTrans);	
	    }
	    
	    Location oFRLoc = LocationTool.getLocationByID(_oTR.getFromLocationId(),m_oConn);
	    Location oTOLoc = LocationTool.getLocationByID(_oTR.getToLocationId(),m_oConn);

	    //if separate journal between TO and TI then location Transfer Account must be filled in 
	    if(m_iTRFType == ItemTransferTool.i_TO || m_iTRFType == ItemTransferTool.i_TI)
	    {
	    	Account oFRAcc = AccountTool.getAccountByID(oFRLoc.getTransferAccount(), m_oConn); 
	    	Account oTOAcc = AccountTool.getAccountByID(oTOLoc.getTransferAccount(), m_oConn);
	    	validate(oFRAcc, oTOLoc.getLocationCode(), LocationPeer.TRANSFER_ACCOUNT);
	    	validate(oTOAcc, oFRLoc.getLocationCode(), LocationPeer.TRANSFER_ACCOUNT);
	    }
	    if (StringUtil.isNotEmpty(oFRLoc.getTransferAccount()) && StringUtil.isNotEmpty(oTOLoc.getTransferAccount()))
	    {
	    	if(m_iTRFType <= 0 || m_iTRFType == ItemTransferTool.i_TO)
		    {
			    GlTransaction oFromCross = new GlTransaction();	
			    oFromCross.setTransactionType  (GlAttributes.i_GL_TRANS_ITEM_TRANSFER);	    
			    oFromCross.setLocationId	   (_oTR.getFromLocationId()); 
			    oFromCross.setProjectId        ("");
			    oFromCross.setDepartmentId     ("");
			    oFromCross.setAccountId        (oTOLoc.getTransferAccount());
			    oFromCross.setCurrencyId       (m_oCurrency.getCurrencyId());
			    oFromCross.setCurrencyRate     (bd_ONE);
			    oFromCross.setAmount           (bdAmount);
			    oFromCross.setAmountBase       (bdAmount);
			    oFromCross.setDebitCredit	   (i_DEBIT);
			    oFromCross.setDescription 	   (m_sRemark + " Transfer To " + _oTR.getToLocationName());
			    addJournal (oFromCross, oInvAcc, _vGLTrans);
		    }
	    	if(m_iTRFType <= 0 || m_iTRFType == ItemTransferTool.i_TI)
		    {
		    	GlTransaction oToCross = new GlTransaction();
			    oToCross.setTransactionType  (GlAttributes.i_GL_TRANS_ITEM_TRANSFER);
			    oToCross.setLocationId	     (_oTR.getToLocationId());
			    oToCross.setProjectId        ("");
			    oToCross.setDepartmentId     ("");
			    oToCross.setAccountId        (oFRLoc.getTransferAccount()); 
			    oToCross.setCurrencyId       (m_oCurrency.getCurrencyId());
			    oToCross.setCurrencyRate     (bd_ONE);
			    oToCross.setAmount           (bdAmount);
			    oToCross.setAmountBase       (bdAmount);
			    oToCross.setDebitCredit	     (i_CREDIT);
			    oToCross.setDescription 	 (m_sRemark + " Transfer From " + _oTR.getFromLocationName());
			    addJournal (oToCross, oInvAcc, _vGLTrans);
		    }
	    }
	}	
			
	/**
	 * update old IR journal 
	 * 
	 * @param _sTransID
	 * @throws Exception
	 */
	public void updateIRJournal(IssueReceipt _oTR) 
    	throws Exception
    {   		
		if(_oTR != null)
		{
			validate(m_oConn);
			List _vTD = IssueReceiptTool.getDetailsByID(_oTR.getIssueReceiptId(), m_oConn);
			if (_vTD != null)
			{				
		        try
		        {
		        	deleteJournal(i_GL_TRANS_RECEIPT_UNPLANNED, _oTR.getIssueReceiptId(), m_oConn);
		        	
		            List vGLTrans = new ArrayList(_vTD.size());
		            for (int i = 0; i < _vTD.size(); i++)
		            {
		                IssueReceiptDetail oTD = (IssueReceiptDetail) _vTD.get(i);
		                Item oItem = ItemTool.getItemByID (oTD.getItemId(), m_oConn);
		                createIssueReceiptGLTrans(_oTR, oTD, oItem, vGLTrans);
		            }
		            setGLTrans (vGLTrans);
		            log.debug ("vGLTrans : " + vGLTrans);
		            
		            GlTransactionTool.saveGLTransaction (vGLTrans, m_oConn);
		        }
		        catch (Exception _oEx)
		        {
		        	_oEx.printStackTrace();
		            throw new NestableException("Update IR GL Journal Failed : " + _oEx.getMessage(), _oEx);
		        }
			}
		}
	}
}
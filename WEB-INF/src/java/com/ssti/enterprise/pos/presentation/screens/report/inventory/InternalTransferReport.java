package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

public class InternalTransferReport extends IssueReceiptReport
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
	    	super.doBuildTemplate(data, context);
	    }   
    	catch (Exception _oEx) {
    		data.setMessage("Data LookUp Failed !");
    	}    
    }    
}

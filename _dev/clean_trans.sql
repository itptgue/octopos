﻿--clear transaction

truncate table issue_receipt_detail cascade;
truncate table issue_receipt cascade;

truncate table item_transfer_detail cascade;
truncate table item_transfer cascade;

truncate table job_costing_detail cascade;
truncate table job_costing cascade;

truncate table jc_rollover_detail cascade;
truncate table jc_rollover cascade;

truncate table purchase_order_detail cascade;
truncate table purchase_order cascade;

truncate table purchase_receipt_detail cascade;
truncate table purchase_receipt cascade;

truncate table purchase_invoice_detail cascade;
truncate table purchase_invoice cascade;

truncate table purchase_return_detail cascade;
truncate table purchase_return cascade;

truncate table ap_payment_detail cascade;
truncate table ap_payment cascade;

truncate table debit_memo cascade;

truncate table inventory_location cascade;
truncate table inventory_transaction cascade;
truncate table inv_trans_update cascade;
truncate table inv_detail cascade;

truncate table account_payable cascade;
truncate table vendor_balance cascade;

truncate table sales_order_detail cascade;
truncate table sales_order cascade;

truncate table delivery_order_detail cascade;
truncate table delivery_order cascade;

truncate table sales_transaction_detail cascade;
truncate table sales_transaction cascade;

truncate table sales_return_detail cascade;
truncate table sales_return cascade;

truncate table ar_payment_detail cascade;
truncate table ar_payment cascade;

truncate table credit_memo cascade;

truncate table account_receivable cascade;
truncate table customer_balance cascade;

truncate table cash_flow_journal cascade;
truncate table cash_flow cascade;
truncate table bank_transfer cascade;
truncate table bank_book cascade;
update bank set opening_balance = 0;

truncate table petty_cash cascade;
truncate table petty_cash_balance cascade;

truncate table journal_voucher_detail cascade;
truncate table journal_voucher cascade;

truncate table gl_transaction cascade;
truncate table account_balance cascade;

truncate table discount_coupon cascade;
truncate table voucher_no cascade;

truncate table cashier_balance cascade;
truncate table cashier_bills cascade;
truncate table cashier_coupon cascade;

truncate table drawer_log cascade;
truncate table printing_log cascade;

truncate table remote_store_data cascade;
truncate table synchronization cascade;

--specific module
truncate table point_transaction cascade;
truncate table customer_point cascade;

truncate table patient_reservation cascade;
truncate table patient_registration cascade;

truncate table medical_record_detail cascade;
truncate table medical_record cascade;

truncate table lab_order_detail cascade;
truncate table lab_order cascade;

truncate table prescription_detail cascade;
truncate table prescription cascade;

truncate table medicine_sales_detail cascade;
truncate table medicine_sales cascade;

truncate table agency_contract cascade;
truncate table agency_invoice cascade;
truncate table principal_sales cascade;





package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceFreight;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.2  2007/05/10 23:22:58  albert
 * *** empty log message ***
 *
 * Revision 1.1  2007/05/08 11:55:11  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PIFreightCostSelector extends TransactionSecureScreen
{
    private static final Log log = LogFactory.getLog(PIFreightCostSelector.class);
	
    private static final int i_OP_SET_PI  	   = 7; 
    private static final int i_OP_ADD_FREIGHT  = 1; 
    private static final int i_OP_DEL_FREIGHT  = 2; 
    private static final int i_OP_SET_FREIGHT  = 3; 
    
	List vPIF = null; 
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
    	vPIF = (List) data.getSession().getAttribute(s_PIF);
    	if (vPIF == null) vPIF = new ArrayList();
    	
		int iOp = data.getParameters().getInt("op");
		
		try 
		{
			if (iOp == i_OP_SET_PI)
			{
				setPI(data, context);
			}
			else if (iOp == i_OP_ADD_FREIGHT)
			{
				addFreight(data);
			}
			else if (iOp == i_OP_DEL_FREIGHT)
			{
				delFreight(data);
			}
			
			else if (iOp == i_OP_SET_FREIGHT)
			{
				setFreight(data);
			}
			context.put (s_PI, data.getSession().getAttribute(s_PI));
			context.put (s_PIF, data.getSession().getAttribute(s_PIF));
		} 
		catch (Exception e) 
		{
			log.error(e);
			data.setMessage("ERROR: " + e.getMessage());
			e.printStackTrace();
		}			
    }

	private void setPI(RunData data, Context context ) 
		throws Exception
	{
		String sID = data.getParameters().getString("TransId");
		PurchaseInvoice oPI = PurchaseInvoiceTool.getHeaderByID(sID);

		if (oPI != null)
		{
			List vDet = oPI.getPurchaseInvoiceDetails();
			List vExp = oPI.getPurchaseInvoiceExps();
			
			if (vDet.size() > 0 || vExp.size() > 0)
			{
				if(vDet.size() > 0)
				{
					PurchaseInvoiceDetail oPIDet = (PurchaseInvoiceDetail) vDet.get(0);
					Item oItem = ItemTool.getItemByID(oPIDet.getItemId());
					if (oItem != null && oItem.getItemType() == i_NON_INVENTORY_PART)
					{
						Account oExpAcc = AccountTool.getAccountByID(oItem.getExpenseAccount());
						if (oExpAcc != null)
						{
							PurchaseInvoiceFreight oPIF = new PurchaseInvoiceFreight();
							oPIF.setFreightPiId(sID);
							oPIF.setFreightPiNo(oPI.getPurchaseInvoiceNo());
							oPIF.setAccountId(oExpAcc.getAccountId());
							oPIF.setProjectId(oPIDet.getProjectId());
							oPIF.setDepartmentId(oPIDet.getDepartmentId());
							
							double dAmount = oPIDet.getSubTotal().doubleValue();
							dAmount = dAmount * oPI.getCurrencyRate().doubleValue();						
							oPIF.setFreightAmount(new BigDecimal(dAmount));
							
							context.put("oPIF", oPIF);
						}
						else
						{
							data.setMessage("Invalid PI Item Account Expense " + oItem.getItemCode());													
						}
					}				
					else
					{
						data.setMessage("Invalid PI Item " + oPIDet.getItemCode());						
					}
				}
			}
			else
			{
				data.setMessage("Invalid PI Details (" + vDet.size() + ")");
			}
		}
		else
		{
			data.setMessage("Invalid PI Selected ");
		}
	}
	
	private void addFreight(RunData data) 
		throws Exception
	{	
		PurchaseInvoiceFreight oPIF = new PurchaseInvoiceFreight();
		data.getParameters().setProperties(oPIF);
		vPIF.add(oPIF);
		data.getSession().setAttribute(s_PIF, vPIF);
	}

	private void delFreight(RunData data) 
		throws Exception
	{
		int iNo = data.getParameters().getInt("no");
		if (iNo < vPIF.size()) vPIF.remove(iNo);
		data.getSession().setAttribute(s_PIF, vPIF);
	}
	
	private void setFreight(RunData data) 
		throws Exception
	{
		PurchaseInvoice oPI = (PurchaseInvoice) data.getSession().getAttribute(s_PI);
		if (oPI != null)
		{
			double dTotalExpense = data.getParameters().getDouble("TotalExpense"); 
			oPI.setTotalExpense (new BigDecimal (dTotalExpense));
			oPI.setFreightAsCost(true);
			oPI.setFreightCurrencyId(CurrencyTool.getDefaultCurrency().getCurrencyId());
		}	
	}
}

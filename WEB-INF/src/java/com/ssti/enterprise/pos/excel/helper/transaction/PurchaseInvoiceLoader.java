package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseInvoiceLoader.java,v 1.1 2008/04/18 06:46:40 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseInvoiceLoader.java,v $
 * Revision 1.1  2008/04/18 06:46:40  albert
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/03 02:21:00  albert
 * *** empty log message ***
 *
 * Revision 1.2  2007/07/02 15:37:32  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PurchaseInvoiceLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( PurchaseInvoiceLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Item Code";
	private static final String s_END_TRANS = "End Trans";	
	
	public PurchaseInvoiceLoader ()
	{
	}
	
	public PurchaseInvoiceLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		
		PurchaseInvoice oTR = null;
		List vTD = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new PurchaseInvoice();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{    				
    				PurchaseInvoiceDetail oTD = new PurchaseInvoiceDetail();
    				mapDetails (oRow, oTR, oTD);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			List vPIFE = new ArrayList(1);
    		    			PurchaseInvoiceTool.setHeaderProperties(oTR, vTD, null);
    		    			PurchaseInvoiceTool.saveData(oTR, vTD, vPIFE, vPIFE, null);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;		
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, PurchaseInvoice _oTR)
    	throws Exception
    {
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("Location", sLocationCode);}
    	else {_oTR.setLocationId(oLoc.getLocationId());}
    	
    	String sVendorCode = getString(_oRow, iCol); iCol++;
    	Vendor oVendor = VendorTool.getVendorByCode(sVendorCode);
    	if (oVendor == null) {logError("Vendor Code", sVendorCode);}
    	else 
    	{
    		_oTR.setVendorId(oVendor.getVendorId()); 
    		_oTR.setVendorName(oVendor.getVendorName());
    	}

    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setPurchaseInvoiceDate(dTrans);}

    	String sVendInvNo = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sVendInvNo)) {_oTR.setVendorInvoiceNo(sVendInvNo);}
    	else {_oTR.setVendorInvoiceNo("");}
    	
    	String sCashier = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sCashier)) {logError("Create By", sCashier);}
    	else {_oTR.setCreateBy(sCashier);}
		
    	String sTypeCode = getString(_oRow, iCol); iCol++;
    	PaymentType oPT = PaymentTypeTool.getPaymentTypeByCode(sTypeCode);
    	if (oPT == null) {logError("Payment Type", sTypeCode);}
    	else {_oTR.setPaymentTypeId(oPT.getPaymentTypeId());}
    	
    	String sTermCode = getString(_oRow, iCol); iCol++;
    	PaymentTerm oPTerm = PaymentTermTool.getPaymentTermByCode(sTermCode);
    	if (oPTerm == null) {logError("Payment Term", sTermCode);}
    	else {_oTR.setPaymentTermId(oPTerm.getPaymentTermId());}

    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setRemark(sRemark);
    	
    	String sTotalDiscPct = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sTotalDiscPct)) sTotalDiscPct = "0";
    	_oTR.setTotalDiscountPct(sTotalDiscPct);

    	String sInclusive = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isNotEmpty(sInclusive)) 
    	{
    		_oTR.setIsTaxable(Boolean.valueOf(sInclusive));
    		_oTR.setIsInclusiveTax(Boolean.valueOf(sInclusive));
    	}
    	
    	String sCurrencyCode = getString(_oRow, iCol); iCol++;
    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurrencyCode);
    	if (oCurr == null) {logError("Currency Code", sCurrencyCode);}
    	else 
    	{
    		_oTR.setCurrencyId(oCurr.getCurrencyId()); 
    	}    	
    	BigDecimal dRate = getBigDecimal(_oRow, iCol); iCol++;
    	if (dRate == null || dRate.equals(bd_ZERO)) {logError("Currency Rate",dRate);}
    	else
    	{
    		_oTR.setCurrencyRate(dRate);
    	}    	
    	BigDecimal dFiscalRate = getBigDecimal(_oRow, iCol); iCol++;
    	if (dRate == null || dRate.equals(bd_ZERO)) {logError("Fiscal Rate",dRate);}
    	else
    	{
    		_oTR.setCurrencyRate(dFiscalRate);
    	}
    	
    	int iStatus = i_PENDING;
    	String sStatus = getString(_oRow, iCol); iCol++;    	
    	if (StringUtil.equalsIgnoreCase(sStatus, "PROCESSED"))
    	{
    		iStatus = i_PROCESSED;
    	}
    	//OTHER FIELD
    	if (!m_bTransError)
    	{
    		//_oTR.setPurchaseInvoiceNo(sTransNo);    		
	    	_oTR.setStatus(iStatus);
	    	_oTR.setCourierId("");
	    	_oTR.setShippingPrice(bd_ZERO);
	    	_oTR.setRemark(_oTR.getRemark() + setDesc(sTransNo, m_sUserName));
	    	_oTR.setFobId("");
	    	_oTR.setFreightAccountId("");
	    	
	    	//updated by processPayment
	    	_oTR.setPaidAmount(bd_ZERO); 
	    	_oTR.setTotalExpense(bd_ZERO);
	    	_oTR.setDueDate(_oTR.getPurchaseInvoiceDate());	    	
    	}
    }
    

    /**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @throws Exception
	 */
	protected void mapDetails (HSSFRow _oRow, PurchaseInvoice _oTR, PurchaseInvoiceDetail _oTD)
		throws Exception
	{    	
    	mapDetails(_oRow,_oTR,_oTD,0);
	}
	
	/**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @param _iStartCol
	 * @throws Exception
	 */
    protected void mapDetails (HSSFRow _oRow, PurchaseInvoice _oTR, PurchaseInvoiceDetail _oTD,  int _iStartCol)
		throws Exception
	{    	
    	int iCol = _iStartCol;
    	String sItemCode = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("item_code")).append(": ").append(sItemCode).append(s_LINE_SEPARATOR);
    	
    	Item oItem = ItemTool.getItemByCode(sItemCode);
    	if (oItem == null) {logError("Item Code", sItemCode);}
    	else 
    	{
    		_oTD.setItemId(oItem.getItemId());
    		_oTD.setItemCode(oItem.getItemCode());
    		_oTD.setItemName(oItem.getItemName());
    	}

    	BigDecimal dQty = getBigDecimal (_oRow, iCol); iCol++;
    	if (dQty == null) {logError("Qty", dQty);}
    	else 
    	{
    		_oTD.setQty(dQty);
    		_oTD.setReturnedQty(bd_ZERO);
    	}

    	String sUnitCode = getString (_oRow, iCol); iCol++;
    	Unit oUnit = UnitTool.getUnitByCode(sUnitCode);
    	if (oUnit == null) {logError("Unit Code", sUnitCode);}
    	else 
    	{
    		_oTD.setUnitId(oUnit.getUnitId());
    		_oTD.setUnitCode(oUnit.getUnitCode());
    	}

    	BigDecimal dPrice = getBigDecimal (_oRow, iCol); iCol++;
    	if (dPrice == null) {logError("Item Price", dPrice);}
    	else 
    	{
    		_oTD.setItemPrice(dPrice);
    	}

    	String sTaxCode = getString(_oRow, iCol); iCol++;
    	Tax oTax = TaxTool.getTaxByID(TaxTool.getIDByCode(sTaxCode));
    	if (oTax == null) {logError("Tax Code", sTaxCode);}
    	else
    	{
    		_oTD.setTaxId(oTax.getTaxId());
    		_oTD.setTaxAmount(oTax.getAmount());
    	}
    	
    	String sDisc = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sDisc)) {sDisc = "0";}
    	_oTD.setDiscount(sDisc);
    	
    	//OTHER FIELD
    	_oTD.setPurchaseReceiptId("");
    	_oTD.setPurchaseReceiptDetailId("");
    	_oTD.setPurchaseOrderId("");
    	_oTD.setWeight(bd_ZERO);
    	_oTD.setProjectId("");
    	_oTD.setDepartmentId("");
    	
		//CALCULATED FIELD
    	if (!m_bTransError)
    	{    		
   			double dBaseValue = UnitTool.getBaseValue(_oTD.getItemId(), _oTD.getUnitId());
            double dQtyBase = _oTD.getQty().doubleValue() * dBaseValue; 
    		_oTD.setQtyBase(new BigDecimal(dQtyBase));

            if (dQtyBase > 0)
            {
	    		double dDiscount = 0;
	    		double dPriceBase = _oTD.getItemPrice().doubleValue() * _oTR.getCurrencyRate().doubleValue();
	    		//if discount was not % then divide discount with purchase qty 
	    		if(_oTD.getDiscount().contains(Calculator.s_PCT))
	    		{
	    			dDiscount = Calculator.calculateDiscount(_oTD.getDiscount(),dPriceBase);
	    		}
	    		else
	    		{
	    			dDiscount = Calculator.calculateDiscount(_oTD.getDiscount(),dPriceBase) / _oTD.getQty().doubleValue();
	    		}
	    		double dRate = _oTR.getCurrencyRate().doubleValue();
	    		double dCostPerUnit = dPriceBase - dDiscount;
	    		dCostPerUnit = dCostPerUnit / dBaseValue;
	    		double dRealCost = dCostPerUnit * 1;
	    		_oTD.setCostPerUnit(new BigDecimal(dRealCost));
            }
            else
            {
            	_oTD.setCostPerUnit(bd_ZERO);
            }
    		
    		double dSubTotal = _oTD.getItemPrice().doubleValue() * _oTD.getQty().doubleValue();
    		double dSubTotalDisc = Calculator.calculateDiscount(_oTD.getDiscount(), dSubTotal);
    		dSubTotal = dSubTotal - dSubTotalDisc;
    		double dSubTotalTax = _oTD.getTaxAmount().doubleValue() / 100 * dSubTotal;
    		    		
    		_oTD.setSubTotal(new BigDecimal(dSubTotal));
    		_oTD.setSubTotalDisc(new BigDecimal(dSubTotalDisc));
    		_oTD.setSubTotalTax(new BigDecimal(dSubTotalTax));
    		_oTD.setSubTotalExp(bd_ZERO);
    	}    	
	}   
}

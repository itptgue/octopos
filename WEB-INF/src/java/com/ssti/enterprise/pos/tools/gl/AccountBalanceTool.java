package com.ssti.enterprise.pos.tools.gl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountBalance;
import com.ssti.enterprise.pos.om.AccountBalancePeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * this class used as object model controller for AccountBalance OM
 * the OM is presenting current balance of an account
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AccountBalanceTool.java,v 1.10 2009/05/04 02:04:20 albert Exp $ <br>
 *
 * <pre>
 * $Log: AccountBalanceTool.java,v $
 * Revision 1.10  2009/05/04 02:04:20  albert
 * *** empty log message ***
 *
 * Revision 1.9  2008/06/29 07:12:48  albert
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/03 03:04:20  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/10 04:50:31  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/04/17 13:08:34  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class AccountBalanceTool implements GlAttributes
{
	private static Log log = LogFactory.getLog ( AccountBalanceTool.class );
	
	public static AccountBalance getDataByAccountID (String _sAccountID) 
		throws Exception
	{
		return getDataByAccountID (_sAccountID, null);
	}
	
	/**
	 * 
	 * @param _sAccountID
	 * @param _oConn
	 * @return AccountBalance
	 * @throws Exception
	 */
	public static AccountBalance getDataByAccountID (String _sAccountID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (AccountBalancePeer.ACCOUNT_ID, _sAccountID);
        List vData = AccountBalancePeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
		    return (AccountBalance) vData.get(0);
		}
		return null;
	}

	/**
	 * get AccountBalance for update, will lock row
	 * 
	 * @param _sAccountID
	 * @param _oConn
	 * @return Account Balance
	 * @throws Exception
	 */
	private static AccountBalance getForUpdate (String _sAccountID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (AccountBalancePeer.ACCOUNT_ID, _sAccountID);
	    oCrit.setForUpdate(true);
		List vData = AccountBalancePeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
		    return (AccountBalance) vData.get(0);
		}
		return null;
	}
	
	public static void deleteDataByAccountID (String _sAccountID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (AccountBalancePeer.ACCOUNT_ID, _sAccountID);
		AccountBalancePeer.doDelete (oCrit);
	}	
	
	/**
	 * get current balance of a GL Account for sub (dept/prj/loc)
	 * then this method will calculate all happened transaction 
	 * to get the current sub balance
	 * 
	 * if debit account -> sum all debit trans then minus with sum of all credit trans
	 * if credit account -> sum all credit trans then minus with sum of all debit trans
	 * 
	 * @param _sAccountID
	 * @param _sSubType (dept/prj/loc)
	 * @param _sSubID (dept_id/prj_id/loc_id)
	 * @param _dAsOf
	 * @return array of base balance [1] & prime balance [2]
	 * @throws Exception
	 */
	private static double[] getSubBalance (String _sAccountID, String _sSubType, String _sSubID, Date _dAsOf)
		throws Exception
	{
		Account oAccount = AccountTool.getAccountByID(_sAccountID);
		Date dNow = new Date();
		Date dAsDate = oAccount.getAsDate();

		double[] dBalance = new double[2];
		
        double[] dDebit = 
        	GlTransactionTool.getChanges (_sAccountID, _sSubType, _sSubID, dAsDate, dNow, i_DEBIT);
        
        double[] dCredit = 
        	GlTransactionTool.getChanges (_sAccountID, _sSubType, _sSubID, dAsDate, dNow, i_CREDIT);		            
                
        if (oAccount.getNormalBalance() == i_DEBIT)
        {
            dBalance[i_BASE] = dDebit[i_BASE] - dCredit[i_BASE];
            dBalance[i_PRIME] = dDebit[i_PRIME] - dCredit[i_PRIME];
        }
        else if (oAccount.getNormalBalance() == i_CREDIT)
        {
            dBalance[i_BASE] = dCredit[i_BASE] - dDebit[i_BASE];
            dBalance[i_PRIME] = dCredit[i_PRIME] - dDebit[i_PRIME];
        }
		return dBalance;
	}

	/**
	 * get account current balance or total balance for parent account
	 * 
	 * @param _sAccountID
	 * @param _dAsOf
	 * @param _bBase
	 * @return base balance
	 * @throws Exception
	 */
	public static double getBalance (String _sAccountID, Date _dAsOf, boolean _bBase) 
		throws Exception
	{
		return getBalance (_sAccountID, "", "", _dAsOf, _bBase);
	}

	/**
	 * get account current balance or total balance for parent account
	 * 
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @param _dAsOf
	 * @param _bBase is base value or not 
	 * @return balance 
	 * @throws Exception
	 */
	public static double getBalance (String _sAccountID, String _sSubType, 
									 String _sSubID, Date _dAsOf, boolean _bBase) 
		throws Exception
	{
		if (!_bBase) return getBalance (_sAccountID, _sSubType, _sSubID, _dAsOf)[i_PRIME] ;
		return getBalance (_sAccountID, _sSubType, _sSubID, _dAsOf)[i_BASE] ;
	}		
	
	/**
	 * get account current balance or total balance for parent account
	 * 
	 * @param _sAccountID
	 * @param _dAsOf
	 * @return array of base balance [1] & prime balance [2]
	 * @throws Exception
	 */
	public static double[] getBalance (String _sAccountID, Date _dAsOf) 
		throws Exception
	{
		return getBalance (_sAccountID, "", "", _dAsOf) ;
	}
	
	/**
	 * get account current balance or total balance for parent account
	 * 
	 * @param _sAccountID
	 * @param _sSubType (dept/prj/loc)
	 * @param _sSubID (dept_id/prj_id/loc_id)
	 * @param _dAsOf 
	 * @return array of base balance [1] & prime balance [2]
	 * @throws Exception
	 */
	public static double[] getBalance (String _sAccountID, String _sSubType, String _sSubID, Date _dAsOf) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
        Account oAccount = AccountTool.getAccountByID(_sAccountID);
        
        double[] dTotalBalance = new double[2];
        dTotalBalance[i_BASE] = 0;
        dTotalBalance[i_PRIME] = 0;

        if (oAccount != null)
        {
        	if (oAccount.getHasChild()) 
        	{
        		List vAccount = AccountTool.getAccountTreeList (null, _sAccountID);
        		List vID = AccountTool.getAccountIDList (vAccount);		    
        		oCrit.addIn (AccountBalancePeer.ACCOUNT_ID, vID);
        	}
        	else 
        	{
        		oCrit.add (AccountBalancePeer.ACCOUNT_ID, _sAccountID);
        	}
        	List vBalances = AccountBalancePeer.doSelect (oCrit);        
        	
        	for (int i = 0; i < vBalances.size(); i++)
        	{
        		AccountBalance oBalance = (AccountBalance) vBalances.get(i);
        		String sAccountID = oBalance.getAccountId();
        		double[] dTotalTrans = GlTransactionTool.getTotalChanges(sAccountID, _sSubType, _sSubID, _dAsOf);
        		double dCurrentBaseBalance = 0;
        		double dCurrentPrimeBalance = 0;
        		
        		if (StringUtil.isNotEmpty(_sSubID))
        		{
        			double[] dSub = getSubBalance(sAccountID, _sSubType, _sSubID, _dAsOf);
        			dCurrentBaseBalance = dSub[i_BASE];
        			dCurrentPrimeBalance = dSub[i_PRIME];
        		}
        		else
        		{
        			dCurrentBaseBalance = oBalance.getBalance().doubleValue();
        			dCurrentPrimeBalance = oBalance.getPrimeBalance().doubleValue();
        		}
        		
        		if (log.isDebugEnabled())
        		{
        			log.debug ("Account : " + AccountTool.getAccountNameByID(oBalance.getAccountId(), null));
        			log.debug ("Current Base Balance : " + dCurrentBaseBalance);
        			log.debug ("Current Prime Balance : " + dCurrentPrimeBalance);	            
        			log.debug ("Total Trans AsOf " + _dAsOf + " : " + dTotalTrans[i_BASE] + "," + dTotalTrans[i_PRIME]);
        		}
        		dCurrentBaseBalance = dCurrentBaseBalance + dTotalTrans[i_BASE];
        		dCurrentPrimeBalance = dCurrentPrimeBalance + dTotalTrans[i_PRIME];        		
        		if (log.isDebugEnabled())
        		{
        			log.debug ("Base Balance : " + dCurrentBaseBalance);
        			log.debug ("Prime Balance : " + dCurrentPrimeBalance);
        		}
        		dTotalBalance[i_BASE] = dTotalBalance[i_BASE] + dCurrentBaseBalance;             
        		dTotalBalance[i_PRIME] = dTotalBalance[i_PRIME] + dCurrentPrimeBalance;             
        	}
        }
	    return dTotalBalance;
	}	
	
	
	//---------------------------------------------------------
	// update methods
	//---------------------------------------------------------    
	
	public static void updateBalance(GlTransaction _oOldTrans,  
	                                 GlTransaction _oNewTrans,  
	                                 Connection _oConn)
    	throws Exception
    {
        rollbackBalance(_oOldTrans, _oConn);
        updateBalance  (_oNewTrans, _oConn);
	}	

	public static void updateBalance(List _vOldTrans,  
	                                 List _vNewTrans,  
	                                 Connection _oConn)
    	throws Exception
    {
        rollbackBalance(_vOldTrans, _oConn);
        updateBalance  (_vNewTrans, _oConn);
	}	

	public static void updateBalance(List _vTrans, Connection _oConn)
    	throws Exception
    {
		for (int i = 0; i < _vTrans.size(); i++)
		{
			GlTransaction oTrans = (GlTransaction) _vTrans.get(i);
            updateBalance (oTrans, _oConn);
		}
	}	
	
	/**
	 * 
	 * @param _oTrans
	 * @param _oConn
	 * @throws Exception
	 */
	private static void updateBalance(GlTransaction _oTrans,  Connection _oConn)
    	throws Exception
    {
        //prime amount
		double dAmount = AccountTool.checkBalancePlusMinus (_oTrans.getAccountId(), 
															_oTrans.getDebitCredit(),
															_oTrans.getAmount().doubleValue());
		//base amount
		double dAmountBase = AccountTool.checkBalancePlusMinus (_oTrans.getAccountId(), 
                                                            	_oTrans.getDebitCredit(),
                                                            	_oTrans.getAmountBase().doubleValue());

        AccountBalance oBalance = getForUpdate (_oTrans.getAccountId(), _oConn);       
         
        if (oBalance != null) 
        {
        	//prime balance
            double dPrimeBalance  =  oBalance.getPrimeBalance().doubleValue();
            dPrimeBalance = dPrimeBalance + dAmount;            
            oBalance.setPrimeBalance (new BigDecimal (dPrimeBalance));                

            //base balance
        	double dBalanceBase  =  oBalance.getBalance().doubleValue();
            dBalanceBase = dBalanceBase + dAmountBase;            
            oBalance.setBalance (new BigDecimal (dBalanceBase));                
        }
        else 
        {
            oBalance = new AccountBalance();
            oBalance.setAccountBalanceId (IDGenerator.generateSysID());            
            oBalance.setAccountId (_oTrans.getAccountId());
            oBalance.setBalance (new BigDecimal (dAmountBase));                
            oBalance.setPrimeBalance (new BigDecimal (dAmount));                
        }        
        oBalance.save (_oConn);
	}

	//---------------------------------------------------------
	// rollback methods
	//---------------------------------------------------------    

	public static void rollbackBalance(List _vOldTrans, Connection _oConn)
    	throws Exception
    {
		for (int i = 0; i < _vOldTrans.size(); i++)
		{
			GlTransaction oOldTrans = (GlTransaction) _vOldTrans.get(i);
            rollbackBalance (oOldTrans, _oConn);
		}
	}	
	
	/**
	 * 
	 * @param _oOldTrans
	 * @param _oConn
	 * @throws Exception
	 */
	private static void rollbackBalance(GlTransaction _oOldTrans,  Connection _oConn)
    	throws Exception
    {
        //amount
		double dOldAmount= AccountTool.checkBalancePlusMinus (_oOldTrans.getAccountId(), 
					  										  _oOldTrans.getDebitCredit(),
					  										  _oOldTrans.getAmount().doubleValue());
		//amount base
		double dOldAmountBase = AccountTool.checkBalancePlusMinus (_oOldTrans.getAccountId(), 
                                                               	   _oOldTrans.getDebitCredit(),
                                                               	   _oOldTrans.getAmountBase().doubleValue());

        AccountBalance oBalance = getForUpdate (_oOldTrans.getAccountId(), _oConn);
        
        if (oBalance != null) 
        {
        	//prime balance
            double dPrimeBalance = oBalance.getPrimeBalance().doubleValue();
            dPrimeBalance = dPrimeBalance - dOldAmount;
            oBalance.setPrimeBalance (new BigDecimal (dPrimeBalance)); 
            
            //balance base
        	double dBalanceBase = oBalance.getBalance().doubleValue();
            dBalanceBase = dBalanceBase - dOldAmountBase;
            oBalance.setBalance (new BigDecimal (dBalanceBase)); 
            
            oBalance.save (_oConn);
        }
	}		     
}
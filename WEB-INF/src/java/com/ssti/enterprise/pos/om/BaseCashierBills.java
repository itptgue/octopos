package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
    
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CashierBills
 */
public abstract class BaseCashierBills extends BaseObject
{
    /** The Peer class */
    private static final CashierBillsPeer peer =
        new CashierBillsPeer();

        
    /** The value for the cashierBillsId field */
    private String cashierBillsId;
      
    /** The value for the cashierBalanceId field */
    private String cashierBalanceId;
      
    /** The value for the paymentBillsId field */
    private String paymentBillsId;
                                          
    /** The value for the billsAmount field */
    private int billsAmount = 0;
                                          
    /** The value for the qty field */
    private int qty = 0;
      
    /** The value for the amount field */
    private BigDecimal amount;
  
    
    /**
     * Get the CashierBillsId
     *
     * @return String
     */
    public String getCashierBillsId()
    {
        return cashierBillsId;
    }

                        
    /**
     * Set the value of CashierBillsId
     *
     * @param v new value
     */
    public void setCashierBillsId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashierBillsId, v))
              {
            this.cashierBillsId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashierBalanceId
     *
     * @return String
     */
    public String getCashierBalanceId()
    {
        return cashierBalanceId;
    }

                              
    /**
     * Set the value of CashierBalanceId
     *
     * @param v new value
     */
    public void setCashierBalanceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.cashierBalanceId, v))
              {
            this.cashierBalanceId = v;
            setModified(true);
        }
    
                          
                if (aCashierBalance != null && !ObjectUtils.equals(aCashierBalance.getCashierBalanceId(), v))
                {
            aCashierBalance = null;
        }
      
              }
  
    /**
     * Get the PaymentBillsId
     *
     * @return String
     */
    public String getPaymentBillsId()
    {
        return paymentBillsId;
    }

                              
    /**
     * Set the value of PaymentBillsId
     *
     * @param v new value
     */
    public void setPaymentBillsId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.paymentBillsId, v))
              {
            this.paymentBillsId = v;
            setModified(true);
        }
    
                          
                if (aPaymentBills != null && !ObjectUtils.equals(aPaymentBills.getPaymentBillsId(), v))
                {
            aPaymentBills = null;
        }
      
              }
  
    /**
     * Get the BillsAmount
     *
     * @return int
     */
    public int getBillsAmount()
    {
        return billsAmount;
    }

                        
    /**
     * Set the value of BillsAmount
     *
     * @param v new value
     */
    public void setBillsAmount(int v) 
    {
    
                  if (this.billsAmount != v)
              {
            this.billsAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return int
     */
    public int getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(int v) 
    {
    
                  if (this.qty != v)
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private CashierBalance aCashierBalance;

    /**
     * Declares an association between this object and a CashierBalance object
     *
     * @param v CashierBalance
     * @throws TorqueException
     */
    public void setCashierBalance(CashierBalance v) throws TorqueException
    {
            if (v == null)
        {
                  setCashierBalanceId((String) null);
              }
        else
        {
            setCashierBalanceId(v.getCashierBalanceId());
        }
            aCashierBalance = v;
    }

                                            
    /**
     * Get the associated CashierBalance object
     *
     * @return the associated CashierBalance object
     * @throws TorqueException
     */
    public CashierBalance getCashierBalance() throws TorqueException
    {
        if (aCashierBalance == null && (!ObjectUtils.equals(this.cashierBalanceId, null)))
        {
                          aCashierBalance = CashierBalancePeer.retrieveByPK(SimpleKey.keyFor(this.cashierBalanceId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               CashierBalance obj = CashierBalancePeer.retrieveByPK(this.cashierBalanceId);
               obj.addCashierBillss(this);
            */
        }
        return aCashierBalance;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setCashierBalanceKey(ObjectKey key) throws TorqueException
    {
      
                        setCashierBalanceId(key.toString());
                  }
    
    
                  
    
        private PaymentBills aPaymentBills;

    /**
     * Declares an association between this object and a PaymentBills object
     *
     * @param v PaymentBills
     * @throws TorqueException
     */
    public void setPaymentBills(PaymentBills v) throws TorqueException
    {
            if (v == null)
        {
                  setPaymentBillsId((String) null);
              }
        else
        {
            setPaymentBillsId(v.getPaymentBillsId());
        }
            aPaymentBills = v;
    }

                                            
    /**
     * Get the associated PaymentBills object
     *
     * @return the associated PaymentBills object
     * @throws TorqueException
     */
    public PaymentBills getPaymentBills() throws TorqueException
    {
        if (aPaymentBills == null && (!ObjectUtils.equals(this.paymentBillsId, null)))
        {
                          aPaymentBills = PaymentBillsPeer.retrieveByPK(SimpleKey.keyFor(this.paymentBillsId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PaymentBills obj = PaymentBillsPeer.retrieveByPK(this.paymentBillsId);
               obj.addCashierBillss(this);
            */
        }
        return aPaymentBills;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPaymentBillsKey(ObjectKey key) throws TorqueException
    {
      
                        setPaymentBillsId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CashierBillsId");
              fieldNames.add("CashierBalanceId");
              fieldNames.add("PaymentBillsId");
              fieldNames.add("BillsAmount");
              fieldNames.add("Qty");
              fieldNames.add("Amount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CashierBillsId"))
        {
                return getCashierBillsId();
            }
          if (name.equals("CashierBalanceId"))
        {
                return getCashierBalanceId();
            }
          if (name.equals("PaymentBillsId"))
        {
                return getPaymentBillsId();
            }
          if (name.equals("BillsAmount"))
        {
                return Integer.valueOf(getBillsAmount());
            }
          if (name.equals("Qty"))
        {
                return Integer.valueOf(getQty());
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CashierBillsPeer.CASHIER_BILLS_ID))
        {
                return getCashierBillsId();
            }
          if (name.equals(CashierBillsPeer.CASHIER_BALANCE_ID))
        {
                return getCashierBalanceId();
            }
          if (name.equals(CashierBillsPeer.PAYMENT_BILLS_ID))
        {
                return getPaymentBillsId();
            }
          if (name.equals(CashierBillsPeer.BILLS_AMOUNT))
        {
                return Integer.valueOf(getBillsAmount());
            }
          if (name.equals(CashierBillsPeer.QTY))
        {
                return Integer.valueOf(getQty());
            }
          if (name.equals(CashierBillsPeer.AMOUNT))
        {
                return getAmount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCashierBillsId();
            }
              if (pos == 1)
        {
                return getCashierBalanceId();
            }
              if (pos == 2)
        {
                return getPaymentBillsId();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getBillsAmount());
            }
              if (pos == 4)
        {
                return Integer.valueOf(getQty());
            }
              if (pos == 5)
        {
                return getAmount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CashierBillsPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CashierBillsPeer.doInsert((CashierBills) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CashierBillsPeer.doUpdate((CashierBills) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cashierBillsId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCashierBillsId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCashierBillsId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCashierBillsId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CashierBills copy() throws TorqueException
    {
        return copyInto(new CashierBills());
    }
  
    protected CashierBills copyInto(CashierBills copyObj) throws TorqueException
    {
          copyObj.setCashierBillsId(cashierBillsId);
          copyObj.setCashierBalanceId(cashierBalanceId);
          copyObj.setPaymentBillsId(paymentBillsId);
          copyObj.setBillsAmount(billsAmount);
          copyObj.setQty(qty);
          copyObj.setAmount(amount);
  
                    copyObj.setCashierBillsId((String)null);
                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CashierBillsPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CashierBills\n");
        str.append("------------\n")
           .append("CashierBillsId       : ")
           .append(getCashierBillsId())
           .append("\n")
           .append("CashierBalanceId     : ")
           .append(getCashierBalanceId())
           .append("\n")
           .append("PaymentBillsId       : ")
           .append(getPaymentBillsId())
           .append("\n")
           .append("BillsAmount          : ")
           .append(getBillsAmount())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}

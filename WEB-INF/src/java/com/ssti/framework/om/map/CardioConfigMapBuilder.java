package com.ssti.framework.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CardioConfigMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.framework.om.map.CardioConfigMapBuilder";

    /**
     * Item
     * @deprecated use CardioConfigPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "cardio_config";
    }

  
    /**
     * cardio_config.CARDIO_CONFIG_ID
     * @return the column name for the CARDIO_CONFIG_ID field
     * @deprecated use CardioConfigPeer.cardio_config.CARDIO_CONFIG_ID constant
     */
    public static String getCardioConfig_CardioConfigId()
    {
        return "cardio_config.CARDIO_CONFIG_ID";
    }
  
    /**
     * cardio_config.PRINTER_HOST
     * @return the column name for the PRINTER_HOST field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_HOST constant
     */
    public static String getCardioConfig_PrinterHost()
    {
        return "cardio_config.PRINTER_HOST";
    }
  
    /**
     * cardio_config.PRINTER_PORT
     * @return the column name for the PRINTER_PORT field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_PORT constant
     */
    public static String getCardioConfig_PrinterPort()
    {
        return "cardio_config.PRINTER_PORT";
    }
  
    /**
     * cardio_config.PRINTER_PAGE_LENGTH
     * @return the column name for the PRINTER_PAGE_LENGTH field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_PAGE_LENGTH constant
     */
    public static String getCardioConfig_PrinterPageLength()
    {
        return "cardio_config.PRINTER_PAGE_LENGTH";
    }
  
    /**
     * cardio_config.PRINTER_PRINT_MODE
     * @return the column name for the PRINTER_PRINT_MODE field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_PRINT_MODE constant
     */
    public static String getCardioConfig_PrinterPrintMode()
    {
        return "cardio_config.PRINTER_PRINT_MODE";
    }
  
    /**
     * cardio_config.PRINTER_FORM_FEED
     * @return the column name for the PRINTER_FORM_FEED field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_FORM_FEED constant
     */
    public static String getCardioConfig_PrinterFormFeed()
    {
        return "cardio_config.PRINTER_FORM_FEED";
    }
  
    /**
     * cardio_config.PRINTER_CUTPAPER
     * @return the column name for the PRINTER_CUTPAPER field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_CUTPAPER constant
     */
    public static String getCardioConfig_PrinterCutpaper()
    {
        return "cardio_config.PRINTER_CUTPAPER";
    }
  
    /**
     * cardio_config.PRINTER_CUTPAPER_CMD
     * @return the column name for the PRINTER_CUTPAPER_CMD field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_CUTPAPER_CMD constant
     */
    public static String getCardioConfig_PrinterCutpaperCmd()
    {
        return "cardio_config.PRINTER_CUTPAPER_CMD";
    }
  
    /**
     * cardio_config.PRINTER_DETAIL_LINE
     * @return the column name for the PRINTER_DETAIL_LINE field
     * @deprecated use CardioConfigPeer.cardio_config.PRINTER_DETAIL_LINE constant
     */
    public static String getCardioConfig_PrinterDetailLine()
    {
        return "cardio_config.PRINTER_DETAIL_LINE";
    }
  
    /**
     * cardio_config.DRAWER_USE
     * @return the column name for the DRAWER_USE field
     * @deprecated use CardioConfigPeer.cardio_config.DRAWER_USE constant
     */
    public static String getCardioConfig_DrawerUse()
    {
        return "cardio_config.DRAWER_USE";
    }
  
    /**
     * cardio_config.DRAWER_TYPE
     * @return the column name for the DRAWER_TYPE field
     * @deprecated use CardioConfigPeer.cardio_config.DRAWER_TYPE constant
     */
    public static String getCardioConfig_DrawerType()
    {
        return "cardio_config.DRAWER_TYPE";
    }
  
    /**
     * cardio_config.DRAWER_PORT
     * @return the column name for the DRAWER_PORT field
     * @deprecated use CardioConfigPeer.cardio_config.DRAWER_PORT constant
     */
    public static String getCardioConfig_DrawerPort()
    {
        return "cardio_config.DRAWER_PORT";
    }
  
    /**
     * cardio_config.DRAWER_OPEN_COMMAND
     * @return the column name for the DRAWER_OPEN_COMMAND field
     * @deprecated use CardioConfigPeer.cardio_config.DRAWER_OPEN_COMMAND constant
     */
    public static String getCardioConfig_DrawerOpenCommand()
    {
        return "cardio_config.DRAWER_OPEN_COMMAND";
    }
  
    /**
     * cardio_config.DISPLAYER_USE
     * @return the column name for the DISPLAYER_USE field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_USE constant
     */
    public static String getCardioConfig_DisplayerUse()
    {
        return "cardio_config.DISPLAYER_USE";
    }
  
    /**
     * cardio_config.DISPLAYER_HOST
     * @return the column name for the DISPLAYER_HOST field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_HOST constant
     */
    public static String getCardioConfig_DisplayerHost()
    {
        return "cardio_config.DISPLAYER_HOST";
    }
  
    /**
     * cardio_config.DISPLAYER_PORT
     * @return the column name for the DISPLAYER_PORT field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_PORT constant
     */
    public static String getCardioConfig_DisplayerPort()
    {
        return "cardio_config.DISPLAYER_PORT";
    }
  
    /**
     * cardio_config.DISPLAYER_DEF_TEXT
     * @return the column name for the DISPLAYER_DEF_TEXT field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_DEF_TEXT constant
     */
    public static String getCardioConfig_DisplayerDefText()
    {
        return "cardio_config.DISPLAYER_DEF_TEXT";
    }
  
    /**
     * cardio_config.DISPLAYER_DEF_SCROLL
     * @return the column name for the DISPLAYER_DEF_SCROLL field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_DEF_SCROLL constant
     */
    public static String getCardioConfig_DisplayerDefScroll()
    {
        return "cardio_config.DISPLAYER_DEF_SCROLL";
    }
  
    /**
     * cardio_config.DISPLAYER_CLEAR_CMD
     * @return the column name for the DISPLAYER_CLEAR_CMD field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_CLEAR_CMD constant
     */
    public static String getCardioConfig_DisplayerClearCmd()
    {
        return "cardio_config.DISPLAYER_CLEAR_CMD";
    }
  
    /**
     * cardio_config.DISPLAYER_FL_PREF
     * @return the column name for the DISPLAYER_FL_PREF field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_FL_PREF constant
     */
    public static String getCardioConfig_DisplayerFlPref()
    {
        return "cardio_config.DISPLAYER_FL_PREF";
    }
  
    /**
     * cardio_config.DISPLAYER_SL_PREF
     * @return the column name for the DISPLAYER_SL_PREF field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_SL_PREF constant
     */
    public static String getCardioConfig_DisplayerSlPref()
    {
        return "cardio_config.DISPLAYER_SL_PREF";
    }
  
    /**
     * cardio_config.DISPLAYER_LINE1_TEXT
     * @return the column name for the DISPLAYER_LINE1_TEXT field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_LINE1_TEXT constant
     */
    public static String getCardioConfig_DisplayerLine1Text()
    {
        return "cardio_config.DISPLAYER_LINE1_TEXT";
    }
  
    /**
     * cardio_config.DISPLAYER_LINE2_TEXT
     * @return the column name for the DISPLAYER_LINE2_TEXT field
     * @deprecated use CardioConfigPeer.cardio_config.DISPLAYER_LINE2_TEXT constant
     */
    public static String getCardioConfig_DisplayerLine2Text()
    {
        return "cardio_config.DISPLAYER_LINE2_TEXT";
    }
  
    /**
     * cardio_config.BARCODE_PRINTER_USE
     * @return the column name for the BARCODE_PRINTER_USE field
     * @deprecated use CardioConfigPeer.cardio_config.BARCODE_PRINTER_USE constant
     */
    public static String getCardioConfig_BarcodePrinterUse()
    {
        return "cardio_config.BARCODE_PRINTER_USE";
    }
  
    /**
     * cardio_config.DEFAULT_NUMBER_LOCALE
     * @return the column name for the DEFAULT_NUMBER_LOCALE field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_NUMBER_LOCALE constant
     */
    public static String getCardioConfig_DefaultNumberLocale()
    {
        return "cardio_config.DEFAULT_NUMBER_LOCALE";
    }
  
    /**
     * cardio_config.DEFAULT_NUMBER_FORMAT
     * @return the column name for the DEFAULT_NUMBER_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_NUMBER_FORMAT constant
     */
    public static String getCardioConfig_DefaultNumberFormat()
    {
        return "cardio_config.DEFAULT_NUMBER_FORMAT";
    }
  
    /**
     * cardio_config.DEFAULT_SIMPLE_FORMAT
     * @return the column name for the DEFAULT_SIMPLE_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_SIMPLE_FORMAT constant
     */
    public static String getCardioConfig_DefaultSimpleFormat()
    {
        return "cardio_config.DEFAULT_SIMPLE_FORMAT";
    }
  
    /**
     * cardio_config.DEFAULT_ACC_FORMAT
     * @return the column name for the DEFAULT_ACC_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_ACC_FORMAT constant
     */
    public static String getCardioConfig_DefaultAccFormat()
    {
        return "cardio_config.DEFAULT_ACC_FORMAT";
    }
  
    /**
     * cardio_config.DEFAULT_ALIGNED_FORMAT
     * @return the column name for the DEFAULT_ALIGNED_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_ALIGNED_FORMAT constant
     */
    public static String getCardioConfig_DefaultAlignedFormat()
    {
        return "cardio_config.DEFAULT_ALIGNED_FORMAT";
    }
  
    /**
     * cardio_config.DEFAULT_DATE_FORMAT
     * @return the column name for the DEFAULT_DATE_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_DATE_FORMAT constant
     */
    public static String getCardioConfig_DefaultDateFormat()
    {
        return "cardio_config.DEFAULT_DATE_FORMAT";
    }
  
    /**
     * cardio_config.DEFAULT_DATETIME_FORMAT
     * @return the column name for the DEFAULT_DATETIME_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_DATETIME_FORMAT constant
     */
    public static String getCardioConfig_DefaultDatetimeFormat()
    {
        return "cardio_config.DEFAULT_DATETIME_FORMAT";
    }
  
    /**
     * cardio_config.DEFAULT_TIME_FORMAT
     * @return the column name for the DEFAULT_TIME_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.DEFAULT_TIME_FORMAT constant
     */
    public static String getCardioConfig_DefaultTimeFormat()
    {
        return "cardio_config.DEFAULT_TIME_FORMAT";
    }
  
    /**
     * cardio_config.LONG_DATE_FORMAT
     * @return the column name for the LONG_DATE_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.LONG_DATE_FORMAT constant
     */
    public static String getCardioConfig_LongDateFormat()
    {
        return "cardio_config.LONG_DATE_FORMAT";
    }
  
    /**
     * cardio_config.LONG_DATETIME_FORMAT
     * @return the column name for the LONG_DATETIME_FORMAT field
     * @deprecated use CardioConfigPeer.cardio_config.LONG_DATETIME_FORMAT constant
     */
    public static String getCardioConfig_LongDatetimeFormat()
    {
        return "cardio_config.LONG_DATETIME_FORMAT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("cardio_config");
        TableMap tMap = dbMap.getTable("cardio_config");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("cardio_config.CARDIO_CONFIG_ID", "");
                          tMap.addColumn("cardio_config.PRINTER_HOST", "");
                          tMap.addColumn("cardio_config.PRINTER_PORT", "");
                          tMap.addColumn("cardio_config.PRINTER_PAGE_LENGTH", "");
                          tMap.addColumn("cardio_config.PRINTER_PRINT_MODE", "");
                          tMap.addColumn("cardio_config.PRINTER_FORM_FEED", "");
                          tMap.addColumn("cardio_config.PRINTER_CUTPAPER", "");
                          tMap.addColumn("cardio_config.PRINTER_CUTPAPER_CMD", "");
                          tMap.addColumn("cardio_config.PRINTER_DETAIL_LINE", "");
                          tMap.addColumn("cardio_config.DRAWER_USE", "");
                          tMap.addColumn("cardio_config.DRAWER_TYPE", "");
                          tMap.addColumn("cardio_config.DRAWER_PORT", "");
                          tMap.addColumn("cardio_config.DRAWER_OPEN_COMMAND", "");
                          tMap.addColumn("cardio_config.DISPLAYER_USE", "");
                          tMap.addColumn("cardio_config.DISPLAYER_HOST", "");
                          tMap.addColumn("cardio_config.DISPLAYER_PORT", "");
                          tMap.addColumn("cardio_config.DISPLAYER_DEF_TEXT", "");
                          tMap.addColumn("cardio_config.DISPLAYER_DEF_SCROLL", "");
                          tMap.addColumn("cardio_config.DISPLAYER_CLEAR_CMD", "");
                          tMap.addColumn("cardio_config.DISPLAYER_FL_PREF", "");
                          tMap.addColumn("cardio_config.DISPLAYER_SL_PREF", "");
                          tMap.addColumn("cardio_config.DISPLAYER_LINE1_TEXT", "");
                          tMap.addColumn("cardio_config.DISPLAYER_LINE2_TEXT", "");
                          tMap.addColumn("cardio_config.BARCODE_PRINTER_USE", "");
                          tMap.addColumn("cardio_config.DEFAULT_NUMBER_LOCALE", "");
                          tMap.addColumn("cardio_config.DEFAULT_NUMBER_FORMAT", "");
                          tMap.addColumn("cardio_config.DEFAULT_SIMPLE_FORMAT", "");
                          tMap.addColumn("cardio_config.DEFAULT_ACC_FORMAT", "");
                          tMap.addColumn("cardio_config.DEFAULT_ALIGNED_FORMAT", "");
                          tMap.addColumn("cardio_config.DEFAULT_DATE_FORMAT", "");
                          tMap.addColumn("cardio_config.DEFAULT_DATETIME_FORMAT", "");
                          tMap.addColumn("cardio_config.DEFAULT_TIME_FORMAT", "");
                          tMap.addColumn("cardio_config.LONG_DATE_FORMAT", "");
                          tMap.addColumn("cardio_config.LONG_DATETIME_FORMAT", "");
          }
}

package com.ssti.framework.tools;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.jar.Manifest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;

import com.ssti.framework.om.AppConfig;
import com.ssti.framework.om.AppConfigPeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Various date related utilites
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DateUtil.java,v 1.29 2008/08/17 02:16:53 albert Exp $ <br>
 * 
 * <pre>
 * $Log: DateUtil.java,v $
 * Revision 1.29  2008/08/17 02:16:53  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class AppConfigTool implements Attributes
{    	
	static final Log log = LogFactory.getLog(AppConfigTool.class);
	
	public static Manifest getJarManifest(Class clazz)
	{
		if (clazz != null)
		{
			try
			{
				String className = clazz.getSimpleName();
				String classFileName = className + ".class";
				String pathToThisClass = clazz.getResource(classFileName).toString();		
				log.debug("path to class " + pathToThisClass);
				if (pathToThisClass.contains("!"))
				{
					int mark = pathToThisClass.indexOf("!") ;
					String pathToManifest = pathToThisClass.toString().substring(0,mark+1) ;
					pathToManifest += "/META-INF/MANIFEST.MF" ;			
					log.debug("path to manifest " + pathToManifest);
					
					Manifest mf = new Manifest(new URL(pathToManifest).openStream()); 
					log.debug("manifest " + mf);
					
					return mf;
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return null;
	}
	public static String getAppVersion(Class clazz) 
	{
		Manifest mf = getJarManifest(clazz);
		if (mf != null)
		{
			return mf.getMainAttributes().getValue(
				java.util.jar.Attributes.Name.IMPLEMENTATION_VERSION);	
		}
		if (com.ssti.framework.tools.Attributes.CONFIG != null)
		{
			return com.ssti.framework.tools.Attributes.CONFIG.getString("version");
		}
		return "";
	}
	
	public static List getAppConfig() 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.addAscendingOrderByColumn(AppConfigPeer.PART);
		oCrit.addAscendingOrderByColumn(AppConfigPeer.NAME);
		return AppConfigPeer.doSelect(oCrit);
	}

	static final String APP = "Application";
	static final String POS = "POS";
	
	
	public static void importAppConfig(RunData data) 
		throws Exception
	{
		String sApp = IOTool.getRealPath(data, "WEB-INF/conf/Application.properties");
		String sPOS = IOTool.getRealPath(data, "WEB-INF/conf/POS.properties");

		Properties oApp = new Properties();
		oApp.load(new FileInputStream(sApp));

		Iterator iter = oApp.keySet().iterator();
		while (iter.hasNext())
		{
			String sKey = (String) iter.next();
			String sValue = oApp.getProperty(sKey);
			
			List vExist = findAppConfig(APP,sKey);
			if (vExist.size() == 0) //if not exist
			{
				AppConfig oConfig = new AppConfig();
				oConfig.setAppConfigId(IDGenerator.generateSysID());
				oConfig.setPart(APP);
				oConfig.setName(sKey);
				oConfig.setValue(sValue);
				oConfig.save();
			}
		}
			
		
		Properties oPOS = new Properties();
		oPOS.load(new FileInputStream(sPOS));

		iter = oPOS.keySet().iterator();
		while (iter.hasNext())
		{
			String sKey = (String) iter.next();

			List vExist = findAppConfig(POS,sKey);
			if (vExist.size() == 0) //if not exist
			{
				String sValue = oPOS.getProperty(sKey);
				AppConfig oConfig = new AppConfig();
				oConfig.setAppConfigId(IDGenerator.generateSysID());
				oConfig.setPart(POS);
				oConfig.setName(sKey);
				oConfig.setValue(sValue);
				oConfig.save();
			}
		}		
	}
	
	public static List findAppConfig(String _sPart, String _sName) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sPart))oCrit.add(AppConfigPeer.PART, _sPart);
		if (StringUtil.isNotEmpty(_sName))oCrit.add(AppConfigPeer.NAME, (Object)_sName, Criteria.ILIKE);
		oCrit.addAscendingOrderByColumn(AppConfigPeer.PART);		
		oCrit.addAscendingOrderByColumn(AppConfigPeer.NAME);
		return AppConfigPeer.doSelect(oCrit);
	}
	
	public static AppConfig getAppConfigByID(String _sID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(AppConfigPeer.APP_CONFIG_ID, _sID);
		List vData = AppConfigPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((AppConfig)vData.get(0));
		}
		return null;
	}
	
	public static void update(RunData data, String _sID, String _sValue) 
		throws Exception
	{
		AppConfig oData = getAppConfigByID(_sID);
		if (oData != null)
		{
			oData.setValue(_sValue);
			oData.save();
			
			String sApp = IOTool.getRealPath(data, "WEB-INF/conf/Application.properties");
			String sPOS = IOTool.getRealPath(data, "WEB-INF/conf/POS.properties");
			OutputStream out = null;
			Properties oProp = new Properties();
			
			if (APP.equals(oData.getPart()))
			{
				oProp.load(new FileInputStream(sApp));
				out = new FileOutputStream(sApp);
			}
			else if (POS.equals(oData.getPart()))
			{
				oProp.load(new FileInputStream(sPOS));
				out = new FileOutputStream(sPOS);
			}
			oProp.setProperty(oData.getName(), _sValue);
			oProp.store(out, "");
			
			//update to prop
			//CONFIG.setProperty(oData.getName(), _sValue);	
			//System.out.println(TransactionAttributes.i_PURCHASE_COMMA_SCALE);
		}
	}
}


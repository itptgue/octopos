package com.ssti.enterprise.pos.presentation.screens.company;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureScreen;

public class CompanySecureScreen extends SecureScreen
{
	private static final String[] a_PERM = {"Setup Master Data", "View Master Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
}

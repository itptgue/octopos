package com.ssti.enterprise.pos.om;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Thu Aug 26 17:15:19 ICT 2010]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class LocationCtlPeer
    extends com.ssti.enterprise.pos.om.BaseLocationCtlPeer
{
}

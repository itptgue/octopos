package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.ApPaymentDetail;
import com.ssti.enterprise.pos.om.ApPaymentDetailPeer;
import com.ssti.enterprise.pos.om.ApPaymentPeer;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.journal.BaseJournalTool;
import com.ssti.enterprise.pos.tools.journal.PayablePaymentJournalTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PayablePaymentTool.java,v 1.20 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: PayablePaymentTool.java,v $
 * Revision 1.20  2009/05/04 02:04:13  albert
 * *** empty log message ***
 *
 * Revision 1.19  2008/06/29 07:12:27  albert
 * *** empty log message ***
 *
 * Revision 1.18  2008/04/14 09:16:19  albert
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/03 03:04:26  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class PayablePaymentTool extends BaseTool
{
	private static Log log = LogFactory.getLog(PayablePaymentTool.class);
    
	protected static Map m_FIND_PEER = new HashMap();
	
	public static final String s_INV_NO = new StringBuilder(ApPaymentPeer.AP_PAYMENT_ID).append(",")
		.append(ApPaymentDetailPeer.AP_PAYMENT_ID).append(",")
		.append(ApPaymentDetailPeer.PURCHASE_INVOICE_NO).toString();
	
    static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), ApPaymentPeer.AP_PAYMENT_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), ApPaymentPeer.REMARK);          
		m_FIND_PEER.put (Integer.valueOf(i_VENDOR	  ), ApPaymentPeer.VENDOR_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), ApPaymentPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), ApPaymentPeer.REFERENCE_NO);             	
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), ApPaymentPeer.CURRENCY_ID);
        m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), ApPaymentPeer.BANK_ID);		
        m_FIND_PEER.put (Integer.valueOf(i_ISSUER	  ), ApPaymentPeer.BANK_ISSUER);		
        m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), ApPaymentPeer.LOCATION_ID); 
        m_FIND_PEER.put (Integer.valueOf(i_CFT 		  ), ApPaymentPeer.CASH_FLOW_TYPE_ID);      
        m_FIND_PEER.put (Integer.valueOf(i_INV_NO     ), s_INV_NO);              
	}   

	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}    
	
	static PayablePaymentTool instance = null;
	
	public static synchronized PayablePaymentTool getInstance() 
	{
		if (instance == null) instance = new PayablePaymentTool();
		return instance;
	}	
	
	public static ApPayment getHeaderByID(String _sID)
		throws Exception
	{
		return getHeaderByID(_sID, null);
	}
	
	public static ApPayment getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ApPaymentPeer.AP_PAYMENT_ID, _sID);
        List vData = ApPaymentPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (ApPayment) vData.get(0);
		}
		return null;
	}

	public static List getDetailsByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ApPaymentDetailPeer.AP_PAYMENT_ID, _sID);
		return ApPaymentDetailPeer.doSelect(oCrit);
	}
	
	/**
	 * delete transaction details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	private static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ApPaymentDetailPeer.AP_PAYMENT_ID, _sID);
        ApPaymentDetailPeer.doDelete (oCrit, _oConn);
	}

	/**
	 * check whether a purchase invoice is exist in any OPEN or CLOSED ApPayment
	 * not included if ApPayment is CANCELLED 
	 * 
	 * @param _sPIID
	 * @param _oConn
	 * @return is invoice ever paid
	 * @throws Exception
	 */
	public static boolean isInvoicePaid(String _sPIID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ApPaymentPeer.STATUS, i_PAYMENT_CANCELLED, Criteria.NOT_EQUAL);
		oCrit.addJoin(ApPaymentPeer.AP_PAYMENT_ID,ApPaymentDetailPeer.AP_PAYMENT_ID);
	    oCrit.add(ApPaymentDetailPeer.PURCHASE_INVOICE_ID, _sPIID);
		List vData = ApPaymentDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	//-------------------------------------------------------------------------
	// create from PI
	//-------------------------------------------------------------------------
	
	public static void createFromPI (PurchaseInvoice _oPI, 
									 double _dPaidAmount, 
									 Connection _oConn) 
	throws Exception
	{
		StringBuilder oDesc = new StringBuilder ();
		if (StringUtil.isNotEmpty(_oPI.getRemark()))
		{
			oDesc.append (_oPI.getRemark());	
			oDesc.append("\n");
		}		
		oDesc.append ("Direct Payment To ");
		oDesc.append (_oPI.getVendorName());
		oDesc.append (" From (");
		oDesc.append (_oPI.getPurchaseInvoiceNo());
		oDesc.append (") ");
		
		double dRate = _oPI.getCurrencyRate().doubleValue();
		BigDecimal dPaid = new BigDecimal(_dPaidAmount);
		
		ApPayment oPP = new ApPayment();
		oPP.setBankId  	 	   (_oPI.getBankId());
		oPP.setBankIssuer  	   (_oPI.getBankIssuer());
		oPP.setReferenceNo     (_oPI.getReferenceNo());
		oPP.setCashFlowTypeId  (_oPI.getCashFlowTypeId());
		oPP.setCurrencyId  	   (BankTool.getBankCurrencyID(_oPI.getBankId()));
		oPP.setCurrencyRate    (new BigDecimal(dRate));
		oPP.setApPaymentDate   (_oPI.getPurchaseInvoiceDate());
		oPP.setApPaymentDueDate(_oPI.getDueDate());
		oPP.setUserName  	   (_oPI.getCreateBy());
		oPP.setRemark          (oDesc.toString());  
		oPP.setLocationId	   (_oPI.getLocationId());
		oPP.setVendorId		   (_oPI.getVendorId());
		oPP.setVendorName	   (_oPI.getVendorName());
		oPP.setTotalAmount     (dPaid);
		oPP.setTotalDiscount   (bd_ZERO);
		oPP.setTotalDownPayment(bd_ZERO);
		
		ApPaymentDetail oPPD = new ApPaymentDetail();
		List vPPD = new ArrayList(1);
		
		oPPD.setApPaymentDetailId    (""); 
		oPPD.setApPaymentId 		 ("");
		oPPD.setPurchaseInvoiceId    (_oPI.getPurchaseInvoiceId());
		oPPD.setPurchaseInvoiceNo    (_oPI.getPurchaseInvoiceNo());
		oPPD.setPurchaseInvoiceAmount(_oPI.getTotalAmount());
		oPPD.setInvoiceRate			 (_oPI.getCurrencyRate());
		oPPD.setDiscountAccountId    ("");
		oPPD.setDiscountAmount	     (bd_ZERO);
		oPPD.setDownPayment			 (bd_ZERO);
		oPPD.setPaidAmount			 (dPaid);
		oPPD.setPaymentAmount		 (dPaid);
		oPPD.setProjectId		     ("");
		oPPD.setDepartmentId 		 ("");   

		vPPD.add (oPPD);
		saveData (oPP, vPPD, new ArrayList(1), _oConn);   
		
	}	
	
	//-------------------------------------------------------------------------
	//trans processing
	//-------------------------------------------------------------------------
	
	public static void setHeaderProperties (ApPayment _oAP, List _vAPD, RunData data) 
    	throws Exception
    {
		try 
		{
			if (data != null)
			{
				data.getParameters().setProperties (_oAP);
				_oAP.setApPaymentDate(CustomParser.parseDate(data.getParameters().getString("ApPaymentDate")));
				_oAP.setApPaymentDueDate(CustomParser.parseDate(data.getParameters().getString("ApPaymentDueDate")));
			}
			double dTotalDiscount = countTotalDiscount(_vAPD);
			double dTotalAmount = countTotalPaymentAmount(_vAPD);
			_oAP.setTotalDiscount (new BigDecimal(dTotalDiscount));
			_oAP.setTotalAmount (new BigDecimal(dTotalAmount - dTotalDiscount));
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
    private static double countTotalDiscount (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			ApPaymentDetail oDet = (ApPaymentDetail) _vDetails.get(i);
			dAmt += oDet.getDiscountAmount().doubleValue();
		}
		return dAmt;
	}	
	
    private static double countTotalPaymentAmount (List _vDetails) 
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++) 
		{
			ApPaymentDetail oDet = (ApPaymentDetail) _vDetails.get(i);
			dAmt += oDet.getPaymentAmount().doubleValue();
		}
		return dAmt;
	}
    
    private static void validateTrans (ApPayment _oAP, List _vAPD, List _vDM, Connection _oConn)
		throws Exception
	{
    	//validate currency
    	Bank oBank = BankTool.getBankByID(_oAP.getBankId(), _oConn);
    	Vendor oVendor = VendorTool.getVendorByID(_oAP.getVendorId(), _oConn);
    	Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);
    	
    	if (!oBankCurr.getIsDefault())
    	{
    		if (!oBankCurr.getCurrencyId().equals(oVendor.getDefaultCurrencyId()))
    		{
    			throw new NestableException ("Invalid Cash/Bank Selection");
    		} 
    	}    	
    	
		for (int i = 0; i < _vAPD.size(); i++) 
		{
			ApPaymentDetail oDet = (ApPaymentDetail) _vAPD.get(i);
			if (StringUtil.isNotEmpty(oDet.getPurchaseInvoiceId()))
			{
				PurchaseInvoice oTR = PurchaseInvoiceTool.getHeaderByID(oDet.getPurchaseInvoiceId(), _oConn);
				//validate status
				if (oTR.getStatus() != i_PROCESSED)
				{
					throw new NestableException ("Purchase Invoice " + oTR.getPurchaseInvoiceNo() +
						" status is invalid (" + PurchaseInvoiceTool.getStatusString(oTR.getStatus()) + ")");
				}
			}			
		}
		
		//validate Memo
		double dTotalMemo = 0;
		for(int i = 0; i < _vDM.size(); i++)
		{
			List vData = (List)_vDM.get(i);
			for(int j = 0;j < vData.size(); j++)
			{      
				String sID = (String)vData.get(j);
				log.debug("MemoID: " + sID);
				DebitMemo oMemo = DebitMemoTool.getDebitMemoByID(sID, _oConn);
				if (oMemo != null)
				{
					if (oMemo.getStatus() != i_MEMO_OPEN)
					{
						throw new NestableException ("Debit Memo " + oMemo.getDebitMemoNo() +
								" status is invalid (" + DebitMemoTool.getStatusString(oMemo.getStatus()) + ")");
					}
					else
					{
						dTotalMemo += oMemo.getAmount().doubleValue();
					}
				}
				else
				{
					throw new NestableException ("Invalid / not found Debit Memo");
				}
			}   
		}
		log.debug("Total Memo " + dTotalMemo + " Total DP " + _oAP.getTotalDownPayment().doubleValue());
		if (dTotalMemo != _oAP.getTotalDownPayment().doubleValue())
		{
			double dDelta = dTotalMemo - _oAP.getTotalDownPayment().doubleValue();
			if (dDelta < 0) dDelta = dDelta * -1;
			String sMsg = "Total Memo Amount != Total Down Payment Delta (" + dDelta + ")";
			log.info(sMsg);
			if (dDelta > bd_ONE.doubleValue())
			{
				throw new NestableException (sMsg);
			}
		}
	}
    
	/**
	 * 
	 * @param _oAPP
	 * @param _vAPD
	 * @param _vDebitMemo
	 * @param data
	 * @throws Exception
	 */
	public static void saveData (ApPayment _oAPP, List _vAPD, List _vDebitMemo, Connection _oConn)
		throws Exception
	{
		boolean bNew = false;
		boolean bStartTrans = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try 
		{
			log.debug (_oAPP + "\n" +  _vAPD + "\n" +_vDebitMemo);
			if(StringUtil.isEmpty(_oAPP.getApPaymentId())) bNew = true;		
			
			//validate date
			validateDate(_oAPP.getApPaymentDate(), _oConn);
			
			//validate ar payment
			validateTrans(_oAPP, _vAPD, _vDebitMemo, _oConn);
			
			//process save Data
			processSaveAPPaymentData (_oAPP, _vAPD, _oConn);

			//update debit memo TEMP for pending
			DebitMemoTool.savePendingPayment(_vDebitMemo, _oAPP, _vAPD, _oConn);			

			if (_oAPP.getStatus() == i_PROCESSED)
			{
				//create ap entry
				AccountPayableTool.createAPEntryFromApPayment(_oAPP, _oConn);

				//update debit memo
				DebitMemoTool.closeDM(_vDebitMemo, _oAPP, _oConn);

				boolean bIsFuture = isFutureDueDate(_oAPP);
				if (bIsFuture && StringUtil.isNotEmpty(GLConfigTool.getGLConfig().getApPaymentTemp()))
				{
					//create temprorary account journal (only if pending payment)
					PayablePaymentJournalTool oJournal = new PayablePaymentJournalTool(_oAPP, _vAPD, _oConn);
					oJournal.createJournal();
				}

				//create cash flow entry			
				CashFlow oCF = CashFlowTool.createFromAPPayment(_oAPP, _vAPD, bIsFuture, _oConn);

				//update ARPayment
				_oAPP.setCfStatus(oCF.getStatus());
				_oAPP.setCashFlowId(oCF.getCashFlowId());
				_oAPP.save(_oConn);
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx) 
		{
			if (_oAPP.getStatus() == i_PROCESSED)
			{
				_oAPP.setStatus(i_PENDING);
				_oAPP.setApPaymentNo("");
			}
			if (bNew)
			{
				_oAPP.setApPaymentId("");				
			}
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			_oEx.printStackTrace();
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}
	
	//save data processor
	private static void processSaveAPPaymentData (ApPayment _oAPP, List _vAPD, Connection _oConn)
		throws Exception
	{
		if (StringUtil.isEmpty(_oAPP.getApPaymentId()))
		{
			_oAPP.setApPaymentId (IDGenerator.generateSysID());
			_oAPP.setNew (true);
			validateID(getHeaderByID(_oAPP.getApPaymentId(), _oConn), "apPaymentNo");
		}
		if (_oAPP.getStatus() == i_PROCESSED)
		{
			String sLocCode = LocationTool.getLocationCodeByID(_oAPP.getLocationId(), _oConn);
			_oAPP.setApPaymentNo (LastNumberTool.get(s_PP_FORMAT, LastNumberTool.i_AP_PAYMENT, sLocCode, _oConn));
			validateNo(ApPaymentPeer.AP_PAYMENT_NO,_oAPP.getApPaymentNo(),ApPaymentPeer.class, _oConn);
		}
		_oAPP.save (_oConn);
		
		deleteDetailsByID (_oAPP.getApPaymentId(), _oConn);

		for ( int i = 0; i < _vAPD.size(); i++ )
		{
			ApPaymentDetail oPPD = (ApPaymentDetail) _vAPD.get(i);
			
			oPPD.setApPaymentDetailId ( IDGenerator.generateSysID() );
			oPPD.setApPaymentId ( _oAPP.getApPaymentId());
			oPPD.setNew (true);
			oPPD.save(_oConn);
			
			if(_oAPP.getStatus() == i_PROCESSED && 
				(oPPD.getPaymentAmount().doubleValue() > 0 || oPPD.getDownPayment().doubleValue() > 0))
			{
				PurchaseInvoiceTool.updatePaidAmount(oPPD.getPurchaseInvoiceId(), 
				                                     oPPD.getPaymentAmount().doubleValue(),
				                                     oPPD.getDownPayment().doubleValue(),
				                                     oPPD.getTaxPayment(),
				                                     _oConn);
			}
		}
	}
	
	
	/**
	 * cancel receivable payment transaction
	 * 
	 * @param _oPP
	 * @param _vARD
	 * @param data
	 * @throws Exception
	 */
	public static void cancelTransaction (ApPayment _oPP, List _vARD, String _sCancelBy)
		throws Exception
	{
		Connection oConn = beginTrans ();
		try 
		{
			//validate date
			validateDate(_oPP.getApPaymentDate(), oConn);

			//rollback ar entry
			AccountPayableTool.rollbackAP(_oPP.getApPaymentId(),oConn);
			
			//rollback credit memo
			DebitMemoTool.rollbackDMFromAPPayment(_oPP, oConn);
		
			//cancel cash flow entry						
			CashFlowTool.cancelCashFlowByTransID (_oPP.getApPaymentId(), _sCancelBy, oConn);
			
			//rollback any GL Transaction
			BaseJournalTool.deleteJournal(GlAttributes.i_GL_TRANS_AP_PAYMENT, _oPP.getApPaymentId(), oConn);
			
			//process save ar payment Data
			processCancel (_oPP, _vARD, _sCancelBy, oConn);
	
			commit (oConn);
		}
		catch (Exception _oEx) 
		{
			rollback (oConn);

			_oEx.printStackTrace();
			throw new Exception (_oEx.getMessage());
		}
	}
	
	//process cancel data 
	private static void processCancel (ApPayment _oPP, List _vAPD, String _sCancelBy, Connection _oConn)
		throws Exception
	{
		//Generate Sys ID
		_oPP.setRemark(cancelledBy(_oPP.getRemark(), _sCancelBy));
		_oPP.setStatus(i_PAYMENT_CANCELLED);
		_oPP.save (_oConn);
	
		for ( int i = 0; i < _vAPD.size(); i++ )
		{
			ApPaymentDetail oPPD = (ApPaymentDetail) _vAPD.get(i);
			
			if(oPPD.getPaymentAmount().doubleValue() > 0 || oPPD.getDownPayment().doubleValue() > 0)
			{	
				PurchaseInvoiceTool.updatePaidAmount(oPPD.getPurchaseInvoiceId(), 
													 (oPPD.getPaymentAmount().doubleValue() * -1), 
													 (oPPD.getDownPayment().doubleValue()* -1), 
													 oPPD.getTaxPayment(),
													 _oConn);
			}
		}
	}
	
	/**
	 * check whether ArPayment due date is in the future 
	 * 
	 * @param _oAPP
	 * @return
	 */
	private static boolean isFutureDueDate (ApPayment _oAPP)
	{
		if (_oAPP != null)
		{
			if (DateUtil.isAfter(_oAPP.getDueDate(), _oAPP.getTransactionDate()))
			{
				return true;			
			}
		}
		return false;
	}
	

	//-------------------------------------------------------------------------
	//finder
	//-------------------------------------------------------------------------
	private static Criteria buildCriteria(int _iCond, 
										  String _sKeywords, 
										  String _sEntityID, 
										  String _sLocationID,
										  Date _dStart, 
										  Date _dEnd, 
										  Date _dStartDue, 
										  Date _dEndDue,
										  int _iStatus,
										  int _iCFStatus,
										  String _sCurrencyID) 
		throws Exception
	{
		Criteria oCrit =  buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				ApPaymentPeer.AP_PAYMENT_DATE, _dStart, _dEnd, _sCurrencyID);
		
		if (_dStartDue != null && _dEndDue != null)
		{
			oCrit.add(ApPaymentPeer.AP_PAYMENT_DUE_DATE, DateUtil.getStartOfDayDate(_dStartDue), Criteria.GREATER_EQUAL);
			oCrit.and(ApPaymentPeer.AP_PAYMENT_DUE_DATE, DateUtil.getEndOfDayDate(_dEndDue), Criteria.LESS_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sEntityID))
		{
			oCrit.add(ApPaymentPeer.VENDOR_ID, _sEntityID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(ApPaymentPeer.LOCATION_ID, _sLocationID);
		}		
		if (_iStatus > 0)
		{
			oCrit.add(ApPaymentPeer.STATUS, _iStatus);
		}
		if (_iCFStatus > 0)
		{
			oCrit.add(ApPaymentPeer.CF_STATUS, _iCFStatus);						
		}
		return oCrit;
	}
	
	/**
	 * 
	 * @param _sApPaymentNo
	 * @param _sEntityID
	 * @param _dStart
	 * @param _dEnd
	 * @param _dStartDue
	 * @param _dEndDue
	 * @return query result in LargeSelect
	 * @throws Exception
	 */	
	public static LargeSelect findData(int _iCond, 
					   				   String _sKeywords, 
			                           String _sEntityID,
			                           String _sLocationID,
			                           Date _dStart,
			                           Date _dEnd,
			                           Date _dStartDue,
			                           Date _dEndDue,
			                           int _iLimit, 
			                           int _iStatus,
			                           int _iCFStatus,
			                           String _sCurrencyID)
    	throws Exception
    {
		Criteria oCrit = buildCriteria(_iCond, _sKeywords, _sEntityID, _sLocationID, 
				   _dStart, _dEnd, _dStartDue, _dEndDue, _iStatus, _iCFStatus, _sCurrencyID);
		return new LargeSelect(oCrit,_iLimit,"com.ssti.enterprise.pos.om.ApPaymentPeer");
	}
	
	public static List findTrans(int _iCond, 
								 String _sKeywords, 
								 String _sEntityID, 
								 String _sLocationID,
								 Date _dStart, 
								 Date _dEnd, 
								 Date _dStartDue, 
								 Date _dEndDue,
								 int _iStatus,
								 int _iCFStatus,
								 String _sCurrencyID)
	throws Exception
	{
		Criteria oCrit = buildCriteria(_iCond, _sKeywords, _sEntityID, _sLocationID, 
				_dStart, _dEnd, _dStartDue, _dEndDue, _iStatus, _iCFStatus, _sCurrencyID);
		return ApPaymentPeer.doSelect(oCrit);
	}
	
	public static List findTrans(String _sVendorID, 
								 String _sBankID, 
								 String _sLocationID,
								 String _sCashFlowTypeID,
							     Date _dFrom, 
								 Date _dTo, 
								 int _iStatus,
								 int _iCFStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		
		if (_dFrom != null)
		{
			oCrit.add(ApPaymentPeer.AP_PAYMENT_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
		}
		if (_dTo != null)
		{
			oCrit.and(ApPaymentPeer.AP_PAYMENT_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sVendorID))
		{
			oCrit.add(ApPaymentPeer.VENDOR_ID, _sVendorID);
		}
		if (StringUtil.isNotEmpty(_sBankID))
		{
			oCrit.add(ApPaymentPeer.BANK_ID, _sBankID);
		}		
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(ApPaymentPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sCashFlowTypeID))
		{
			oCrit.add(ApPaymentPeer.CASH_FLOW_TYPE_ID, _sCashFlowTypeID);
		}
		if (_iStatus > 0)
		{
			oCrit.add(ApPaymentPeer.STATUS, _iStatus);			
		}
		else
		{
			oCrit.add(ApPaymentPeer.STATUS, i_PROCESSED);
		}
		if (_iCFStatus > 0)
		{
			oCrit.add(ApPaymentPeer.CF_STATUS, _iCFStatus);						
		}
		return ApPaymentPeer.doSelect(oCrit);
	}

    //Report methods
	public static List getVendorList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			ApPayment oTrans = (ApPayment) _vData.get(i);			
			if (!isVendorInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isVendorInList (List _vTrans, ApPayment _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			ApPayment oTrans = (ApPayment) _vTrans.get(i);			
			if (oTrans.getVendorId().equals(_oTrans.getVendorId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByVendorID (List _vTrans, String _sVendorID) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		ApPayment oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (ApPayment ) oIter.next();
			if (!oTrans.getVendorId().equals(_sVendorID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}	

	public static List getBankList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			ApPayment oTrans = (ApPayment) _vData.get(i);			
			if (!isBankInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isBankInList (List _vTrans, ApPayment _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			ApPayment oTrans = (ApPayment) _vTrans.get(i);			
			if (oTrans.getBankId().equals(_oTrans.getBankId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByBankID (List _vTrans, String _sBankID) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		ApPayment oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (ApPayment ) oIter.next();
			if (!oTrans.getBankId().equals(_sBankID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}

	public static String getStatusString (Integer _iStatus)
	{
		if (_iStatus != null)
		{
			if (_iStatus.intValue() == i_TRANS_PROCESSED) return LocaleTool.getString ("processed");
			if (_iStatus.intValue() == i_TRANS_CANCELLED) return LocaleTool.getString ("cancelled");
		}
		return LocaleTool.getString ("open");
	}	
	
	public static String getDetailTransIDString (List _vARD)
	{
		StringBuilder oSB = new StringBuilder();
		for (int i = 0; i < _vARD.size(); i++)
		{
			ApPaymentDetail oAPD = (ApPaymentDetail) _vARD.get(i);
			oSB.append(oAPD.getPurchaseInvoiceId());
			if (i != _vARD.size() - 1) oSB.append(",");
		}
		return oSB.toString();
	}
	
    //find by PI NO
    public static List getByPIID(String _sPIID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.addJoin(ApPaymentPeer.AP_PAYMENT_ID, ApPaymentDetailPeer.AP_PAYMENT_ID);     
        oCrit.add(ApPaymentDetailPeer.PURCHASE_INVOICE_ID, _sPIID);
        oCrit.add(ApPaymentPeer.STATUS, i_TRANS_PROCESSED);
        return ApPaymentPeer.doSelect(oCrit);  
    }
    
    //next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(ApPaymentPeer.class, ApPaymentPeer.AP_PAYMENT_ID, _sID, _bIsNext);
	}	

    //-------------------------------------------------------------------------
    //SYNC
    //-------------------------------------------------------------------------
    /**
     * get AR Payment created in store
     * 
     * @return AR Payment created in store since last store 2 ho
     * @throws Exception
     */
    public static List getStoreTrans()
        throws Exception
    {
        Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
        Criteria oCrit = new Criteria();
        if (dLastSyncDate  != null) {
            oCrit.add(ApPaymentPeer.AP_PAYMENT_DUE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
        }
        oCrit.add(ApPaymentPeer.LOCATION_ID, PreferenceTool.getLocationID());       
        oCrit.add(ApPaymentPeer.STATUS, i_PROCESSED);
        return ApPaymentPeer.doSelect(oCrit);
    }
}
package com.ssti.enterprise.pos.presentation.screens.report.sales;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/report/sales/SalesReport.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SalesReport.java,v 1.15 2006/03/22 15:49:25 albert Exp $
 *
 * $Log: SalesReport.java,v $
 * Revision 1.15  2006/03/22 15:49:25  albert
 * *** empty log message ***
 *
 *
 */
public class SalesReport extends ReportSecureScreen
{	
	public void doBuildTemplate(RunData data, Context context)
    {
		super.doBuildTemplate(data, context);
    	context.put ("salesreport", o_REPORT_TOOL);
    	try 
		{
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findSalesResult", "Invoices");    		
		}   
    	catch (Exception _oEx) 
		{
    		log.error(_oEx);
    		data.setMessage("Sales Report LookUp Failed : " + _oEx.getMessage());
    	}    
    }    
}

package com.ssti.enterprise.pos.presentation.screens.company;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.framework.tools.StringUtil;

public class TransactionNumber extends CompanySecureScreen
{
    public boolean isAuthorized(RunData data)
	    throws Exception
	{
    	return data.getACL().hasRole("Administrator");
	}
    
    public void doBuildTemplate(RunData data, Context context)
    {   
    	int iOp = data.getParameters().getInt("op");
        try
        {
        	context.put("lastnumber", new LastNumberTool());        	
        	if (iOp == 1)
        	{
        		int iType = data.getParameters().getInt("TransType");
        		String sTransNo = data.getParameters().getString("NewNumber");
        		if (StringUtil.isNotEmpty(sTransNo))
        		{
	        		LastNumberTool.manualUpdate(iType, sTransNo);
	        		data.setMessage("Transaction Number Set Successfully to " + sTransNo);
        		}
        	}        
        	if (iOp == 2)
        	{
        		String sCtlTable = data.getParameters().getString("CtlTable");
        		String sTransNo = data.getParameters().getString("NewNumber");
        		if (StringUtil.isNotEmpty(sCtlTable) && StringUtil.isNotEmpty(sTransNo))
        		{
	        		LastNumberTool.manualUpdate(sCtlTable, sTransNo);
	        		data.setMessage("Transaction Number Set Successfully to " + sTransNo);
        		}
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage("Set Transaction Number Failed : " + _oEx.getMessage());
        }        
    }    
}

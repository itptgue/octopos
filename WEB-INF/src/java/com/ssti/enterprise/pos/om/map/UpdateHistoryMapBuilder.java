package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class UpdateHistoryMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.UpdateHistoryMapBuilder";

    /**
     * Item
     * @deprecated use UpdateHistoryPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "update_history";
    }

  
    /**
     * update_history.HISTORY_ID
     * @return the column name for the HISTORY_ID field
     * @deprecated use UpdateHistoryPeer.update_history.HISTORY_ID constant
     */
    public static String getUpdateHistory_HistoryId()
    {
        return "update_history.HISTORY_ID";
    }
  
    /**
     * update_history.MASTER_TYPE
     * @return the column name for the MASTER_TYPE field
     * @deprecated use UpdateHistoryPeer.update_history.MASTER_TYPE constant
     */
    public static String getUpdateHistory_MasterType()
    {
        return "update_history.MASTER_TYPE";
    }
  
    /**
     * update_history.MASTER_ID
     * @return the column name for the MASTER_ID field
     * @deprecated use UpdateHistoryPeer.update_history.MASTER_ID constant
     */
    public static String getUpdateHistory_MasterId()
    {
        return "update_history.MASTER_ID";
    }
  
    /**
     * update_history.MASTER_CODE
     * @return the column name for the MASTER_CODE field
     * @deprecated use UpdateHistoryPeer.update_history.MASTER_CODE constant
     */
    public static String getUpdateHistory_MasterCode()
    {
        return "update_history.MASTER_CODE";
    }
  
    /**
     * update_history.UPDATE_PART
     * @return the column name for the UPDATE_PART field
     * @deprecated use UpdateHistoryPeer.update_history.UPDATE_PART constant
     */
    public static String getUpdateHistory_UpdatePart()
    {
        return "update_history.UPDATE_PART";
    }
  
    /**
     * update_history.UPDATE_DESC
     * @return the column name for the UPDATE_DESC field
     * @deprecated use UpdateHistoryPeer.update_history.UPDATE_DESC constant
     */
    public static String getUpdateHistory_UpdateDesc()
    {
        return "update_history.UPDATE_DESC";
    }
  
    /**
     * update_history.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use UpdateHistoryPeer.update_history.UPDATE_DATE constant
     */
    public static String getUpdateHistory_UpdateDate()
    {
        return "update_history.UPDATE_DATE";
    }
  
    /**
     * update_history.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use UpdateHistoryPeer.update_history.USER_NAME constant
     */
    public static String getUpdateHistory_UserName()
    {
        return "update_history.USER_NAME";
    }
  
    /**
     * update_history.OLD_VALUE
     * @return the column name for the OLD_VALUE field
     * @deprecated use UpdateHistoryPeer.update_history.OLD_VALUE constant
     */
    public static String getUpdateHistory_OldValue()
    {
        return "update_history.OLD_VALUE";
    }
  
    /**
     * update_history.NEW_VALUE
     * @return the column name for the NEW_VALUE field
     * @deprecated use UpdateHistoryPeer.update_history.NEW_VALUE constant
     */
    public static String getUpdateHistory_NewValue()
    {
        return "update_history.NEW_VALUE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("update_history");
        TableMap tMap = dbMap.getTable("update_history");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("update_history.HISTORY_ID", "");
                          tMap.addColumn("update_history.MASTER_TYPE", "");
                          tMap.addColumn("update_history.MASTER_ID", "");
                          tMap.addColumn("update_history.MASTER_CODE", "");
                          tMap.addColumn("update_history.UPDATE_PART", "");
                          tMap.addColumn("update_history.UPDATE_DESC", "");
                          tMap.addColumn("update_history.UPDATE_DATE", new Date());
                          tMap.addColumn("update_history.USER_NAME", "");
                          tMap.addColumn("update_history.OLD_VALUE", "");
                          tMap.addColumn("update_history.NEW_VALUE", "");
          }
}

package com.ssti.enterprise.pos.presentation.screens.setup;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.framework.presentation.SecureScreen;

public class SetupSecureScreen extends SecureScreen implements AppAttributes
{
	public static final int i_SETUP_OP_VIEW = 1;
	public static final int i_SETUP_OP_SAVE = 2;
	public static final int i_SETUP_OP_DEL = 3;

}

package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseReturnDetail
 */
public abstract class BasePurchaseReturnDetail extends BaseObject
{
    /** The Peer class */
    private static final PurchaseReturnDetailPeer peer =
        new PurchaseReturnDetailPeer();

        
    /** The value for the purchaseReturnDetailId field */
    private String purchaseReturnDetailId;
      
    /** The value for the purchaseReturnId field */
    private String purchaseReturnId;
      
    /** The value for the transactionDetailId field */
    private String transactionDetailId;
      
    /** The value for the indexNo field */
    private int indexNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
                                                
    /** The value for the description field */
    private String description = "";
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
      
    /** The value for the itemPrice field */
    private BigDecimal itemPrice;
      
    /** The value for the taxId field */
    private String taxId;
      
    /** The value for the taxAmount field */
    private BigDecimal taxAmount;
      
    /** The value for the subTotalTax field */
    private BigDecimal subTotalTax;
      
    /** The value for the itemCost field */
    private BigDecimal itemCost;
      
    /** The value for the subTotalCost field */
    private BigDecimal subTotalCost;
      
    /** The value for the returnAmount field */
    private BigDecimal returnAmount;
                                                
    /** The value for the projectId field */
    private String projectId = "";
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
  
    
    /**
     * Get the PurchaseReturnDetailId
     *
     * @return String
     */
    public String getPurchaseReturnDetailId()
    {
        return purchaseReturnDetailId;
    }

                        
    /**
     * Set the value of PurchaseReturnDetailId
     *
     * @param v new value
     */
    public void setPurchaseReturnDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseReturnDetailId, v))
              {
            this.purchaseReturnDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseReturnId
     *
     * @return String
     */
    public String getPurchaseReturnId()
    {
        return purchaseReturnId;
    }

                              
    /**
     * Set the value of PurchaseReturnId
     *
     * @param v new value
     */
    public void setPurchaseReturnId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseReturnId, v))
              {
            this.purchaseReturnId = v;
            setModified(true);
        }
    
                          
                if (aPurchaseReturn != null && !ObjectUtils.equals(aPurchaseReturn.getPurchaseReturnId(), v))
                {
            aPurchaseReturn = null;
        }
      
              }
  
    /**
     * Get the TransactionDetailId
     *
     * @return String
     */
    public String getTransactionDetailId()
    {
        return transactionDetailId;
    }

                        
    /**
     * Set the value of TransactionDetailId
     *
     * @param v new value
     */
    public void setTransactionDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDetailId, v))
              {
            this.transactionDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IndexNo
     *
     * @return int
     */
    public int getIndexNo()
    {
        return indexNo;
    }

                        
    /**
     * Set the value of IndexNo
     *
     * @param v new value
     */
    public void setIndexNo(int v) 
    {
    
                  if (this.indexNo != v)
              {
            this.indexNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

                        
    /**
     * Set the value of ItemPrice
     *
     * @param v new value
     */
    public void setItemPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPrice, v))
              {
            this.itemPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxId
     *
     * @return String
     */
    public String getTaxId()
    {
        return taxId;
    }

                        
    /**
     * Set the value of TaxId
     *
     * @param v new value
     */
    public void setTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxId, v))
              {
            this.taxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTaxAmount()
    {
        return taxAmount;
    }

                        
    /**
     * Set the value of TaxAmount
     *
     * @param v new value
     */
    public void setTaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.taxAmount, v))
              {
            this.taxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotalTax()
    {
        return subTotalTax;
    }

                        
    /**
     * Set the value of SubTotalTax
     *
     * @param v new value
     */
    public void setSubTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotalTax, v))
              {
            this.subTotalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCost
     *
     * @return BigDecimal
     */
    public BigDecimal getItemCost()
    {
        return itemCost;
    }

                        
    /**
     * Set the value of ItemCost
     *
     * @param v new value
     */
    public void setItemCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCost, v))
              {
            this.itemCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotalCost
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotalCost()
    {
        return subTotalCost;
    }

                        
    /**
     * Set the value of SubTotalCost
     *
     * @param v new value
     */
    public void setSubTotalCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotalCost, v))
              {
            this.subTotalCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getReturnAmount()
    {
        return returnAmount;
    }

                        
    /**
     * Set the value of ReturnAmount
     *
     * @param v new value
     */
    public void setReturnAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.returnAmount, v))
              {
            this.returnAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private PurchaseReturn aPurchaseReturn;

    /**
     * Declares an association between this object and a PurchaseReturn object
     *
     * @param v PurchaseReturn
     * @throws TorqueException
     */
    public void setPurchaseReturn(PurchaseReturn v) throws TorqueException
    {
            if (v == null)
        {
                  setPurchaseReturnId((String) null);
              }
        else
        {
            setPurchaseReturnId(v.getPurchaseReturnId());
        }
            aPurchaseReturn = v;
    }

                                            
    /**
     * Get the associated PurchaseReturn object
     *
     * @return the associated PurchaseReturn object
     * @throws TorqueException
     */
    public PurchaseReturn getPurchaseReturn() throws TorqueException
    {
        if (aPurchaseReturn == null && (!ObjectUtils.equals(this.purchaseReturnId, null)))
        {
                          aPurchaseReturn = PurchaseReturnPeer.retrieveByPK(SimpleKey.keyFor(this.purchaseReturnId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PurchaseReturn obj = PurchaseReturnPeer.retrieveByPK(this.purchaseReturnId);
               obj.addPurchaseReturnDetails(this);
            */
        }
        return aPurchaseReturn;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPurchaseReturnKey(ObjectKey key) throws TorqueException
    {
      
                        setPurchaseReturnId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseReturnDetailId");
              fieldNames.add("PurchaseReturnId");
              fieldNames.add("TransactionDetailId");
              fieldNames.add("IndexNo");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("Description");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("Qty");
              fieldNames.add("QtyBase");
              fieldNames.add("ItemPrice");
              fieldNames.add("TaxId");
              fieldNames.add("TaxAmount");
              fieldNames.add("SubTotalTax");
              fieldNames.add("ItemCost");
              fieldNames.add("SubTotalCost");
              fieldNames.add("ReturnAmount");
              fieldNames.add("ProjectId");
              fieldNames.add("DepartmentId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseReturnDetailId"))
        {
                return getPurchaseReturnDetailId();
            }
          if (name.equals("PurchaseReturnId"))
        {
                return getPurchaseReturnId();
            }
          if (name.equals("TransactionDetailId"))
        {
                return getTransactionDetailId();
            }
          if (name.equals("IndexNo"))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("ItemPrice"))
        {
                return getItemPrice();
            }
          if (name.equals("TaxId"))
        {
                return getTaxId();
            }
          if (name.equals("TaxAmount"))
        {
                return getTaxAmount();
            }
          if (name.equals("SubTotalTax"))
        {
                return getSubTotalTax();
            }
          if (name.equals("ItemCost"))
        {
                return getItemCost();
            }
          if (name.equals("SubTotalCost"))
        {
                return getSubTotalCost();
            }
          if (name.equals("ReturnAmount"))
        {
                return getReturnAmount();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseReturnDetailPeer.PURCHASE_RETURN_DETAIL_ID))
        {
                return getPurchaseReturnDetailId();
            }
          if (name.equals(PurchaseReturnDetailPeer.PURCHASE_RETURN_ID))
        {
                return getPurchaseReturnId();
            }
          if (name.equals(PurchaseReturnDetailPeer.TRANSACTION_DETAIL_ID))
        {
                return getTransactionDetailId();
            }
          if (name.equals(PurchaseReturnDetailPeer.INDEX_NO))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals(PurchaseReturnDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(PurchaseReturnDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(PurchaseReturnDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(PurchaseReturnDetailPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PurchaseReturnDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(PurchaseReturnDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(PurchaseReturnDetailPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(PurchaseReturnDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(PurchaseReturnDetailPeer.ITEM_PRICE))
        {
                return getItemPrice();
            }
          if (name.equals(PurchaseReturnDetailPeer.TAX_ID))
        {
                return getTaxId();
            }
          if (name.equals(PurchaseReturnDetailPeer.TAX_AMOUNT))
        {
                return getTaxAmount();
            }
          if (name.equals(PurchaseReturnDetailPeer.SUB_TOTAL_TAX))
        {
                return getSubTotalTax();
            }
          if (name.equals(PurchaseReturnDetailPeer.ITEM_COST))
        {
                return getItemCost();
            }
          if (name.equals(PurchaseReturnDetailPeer.SUB_TOTAL_COST))
        {
                return getSubTotalCost();
            }
          if (name.equals(PurchaseReturnDetailPeer.RETURN_AMOUNT))
        {
                return getReturnAmount();
            }
          if (name.equals(PurchaseReturnDetailPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(PurchaseReturnDetailPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseReturnDetailId();
            }
              if (pos == 1)
        {
                return getPurchaseReturnId();
            }
              if (pos == 2)
        {
                return getTransactionDetailId();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getIndexNo());
            }
              if (pos == 4)
        {
                return getItemId();
            }
              if (pos == 5)
        {
                return getItemCode();
            }
              if (pos == 6)
        {
                return getItemName();
            }
              if (pos == 7)
        {
                return getDescription();
            }
              if (pos == 8)
        {
                return getUnitId();
            }
              if (pos == 9)
        {
                return getUnitCode();
            }
              if (pos == 10)
        {
                return getQty();
            }
              if (pos == 11)
        {
                return getQtyBase();
            }
              if (pos == 12)
        {
                return getItemPrice();
            }
              if (pos == 13)
        {
                return getTaxId();
            }
              if (pos == 14)
        {
                return getTaxAmount();
            }
              if (pos == 15)
        {
                return getSubTotalTax();
            }
              if (pos == 16)
        {
                return getItemCost();
            }
              if (pos == 17)
        {
                return getSubTotalCost();
            }
              if (pos == 18)
        {
                return getReturnAmount();
            }
              if (pos == 19)
        {
                return getProjectId();
            }
              if (pos == 20)
        {
                return getDepartmentId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseReturnDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseReturnDetailPeer.doInsert((PurchaseReturnDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseReturnDetailPeer.doUpdate((PurchaseReturnDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseReturnDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPurchaseReturnDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPurchaseReturnDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseReturnDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseReturnDetail copy() throws TorqueException
    {
        return copyInto(new PurchaseReturnDetail());
    }
  
    protected PurchaseReturnDetail copyInto(PurchaseReturnDetail copyObj) throws TorqueException
    {
          copyObj.setPurchaseReturnDetailId(purchaseReturnDetailId);
          copyObj.setPurchaseReturnId(purchaseReturnId);
          copyObj.setTransactionDetailId(transactionDetailId);
          copyObj.setIndexNo(indexNo);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setDescription(description);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setQty(qty);
          copyObj.setQtyBase(qtyBase);
          copyObj.setItemPrice(itemPrice);
          copyObj.setTaxId(taxId);
          copyObj.setTaxAmount(taxAmount);
          copyObj.setSubTotalTax(subTotalTax);
          copyObj.setItemCost(itemCost);
          copyObj.setSubTotalCost(subTotalCost);
          copyObj.setReturnAmount(returnAmount);
          copyObj.setProjectId(projectId);
          copyObj.setDepartmentId(departmentId);
  
                    copyObj.setPurchaseReturnDetailId((String)null);
                                                                                                                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseReturnDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseReturnDetail\n");
        str.append("--------------------\n")
            .append("PurchaseReturnDetailId   : ")
           .append(getPurchaseReturnDetailId())
           .append("\n")
           .append("PurchaseReturnId     : ")
           .append(getPurchaseReturnId())
           .append("\n")
           .append("TransactionDetailId  : ")
           .append(getTransactionDetailId())
           .append("\n")
           .append("IndexNo              : ")
           .append(getIndexNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("ItemPrice            : ")
           .append(getItemPrice())
           .append("\n")
           .append("TaxId                : ")
           .append(getTaxId())
           .append("\n")
           .append("TaxAmount            : ")
           .append(getTaxAmount())
           .append("\n")
           .append("SubTotalTax          : ")
           .append(getSubTotalTax())
           .append("\n")
           .append("ItemCost             : ")
           .append(getItemCost())
           .append("\n")
           .append("SubTotalCost         : ")
           .append(getSubTotalCost())
           .append("\n")
           .append("ReturnAmount         : ")
           .append(getReturnAmount())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
        ;
        return(str.toString());
    }
}

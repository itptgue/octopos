package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.CurrencyManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.CurrencyDetail;
import com.ssti.enterprise.pos.om.CurrencyDetailPeer;
import com.ssti.enterprise.pos.om.CurrencyPeer;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: this class used as object model controller for Currency OM
 * the OM is representing a currency 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CurrencyTool.java,v 1.32 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: CurrencyTool.java,v $
 * Revision 1.32  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.31  2008/06/29 07:11:57  albert
 * *** empty log message ***
 *
 * Revision 1.30  2008/05/07 03:35:41  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CurrencyTool extends BaseTool 
{
	public static List getAllCurrency()
		throws Exception
	{
		return CurrencyManager.getInstance().getAllCurrency();
	}
	
	public static List getAllDetails()
		throws Exception
	{
		return CurrencyDetailPeer.doSelect(new Criteria());
	}
	
	public static Currency getCurrencyByID(String _sID)
		throws Exception
	{
		return getCurrencyByID(_sID, null);
	}
	
	public static Currency getCurrencyByID(String _sID, Connection _oConn)
		throws Exception
	{
		return CurrencyManager.getInstance().getCurrencyByID(_sID, _oConn);
	}
	
    public static Currency getDefaultCurrency()
		throws Exception
    {
		return CurrencyManager.getInstance().getDefaultCurrency(null);
    }

    public static Currency getDefaultCurrency(Connection _oConn)
		throws Exception
	{
		return CurrencyManager.getInstance().getDefaultCurrency(_oConn);
	}
    
    public static Currency getCurrencyByCode(String _sCode)
		throws Exception
    {
		return CurrencyManager.getInstance().getCurrencyByCode(_sCode, null);
    }    
    
	public static String getCodeByID(String _sID)
    	throws Exception
    {
    	Currency oCurrency = getCurrencyByID(_sID);
    	if (oCurrency != null) 
    	{
    	    return oCurrency.getCurrencyCode();
	    }
	    return "";
	}

	public static String getDescriptionByID(String _sID)
    	throws Exception
    {
    	Currency oCurrency = getCurrencyByID(_sID);
    	if (oCurrency != null) 
    	{
    	    return oCurrency.getDescription();
	    }
	    return "";
	}

	public static String getSymbolByID(String _sID)
    	throws Exception
    {
    	Currency oCurrency = getCurrencyByID(_sID);
    	if (oCurrency != null) 
    	{
    	    return oCurrency.getSymbol();
	    }
	    return "";
	}
	
	public static CurrencyDetail getDetailByID(String _sDetailID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CurrencyDetailPeer.CURRENCY_DETAIL_ID, _sDetailID);
		List vData = CurrencyDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (CurrencyDetail) vData.get(0);
		}
		return null;
	}

    public static CurrencyDetail getLatestDetailByID(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CurrencyDetailPeer.CURRENCY_ID, _sID);
		oCrit.addDescendingOrderByColumn(CurrencyDetailPeer.END_DATE);
		oCrit.setLimit(1);
		List vData = CurrencyDetailPeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			return (CurrencyDetail)vData.get(0);
		}
		return null;
	}
	
    public static CurrencyDetail getLatestDetail(String _sID, Connection _oConn)
		throws Exception
	{
    	return CurrencyManager.getInstance().getLatestDetail(_sID, _oConn);
	}
    
    public static Criteria setDateCriteria(String _sID, Date _dDate)
    {
    	Criteria oCrit = new Criteria();
		oCrit.add(CurrencyDetailPeer.CURRENCY_ID, _sID);
    	oCrit.add(CurrencyDetailPeer.BEGIN_DATE, _dDate, Criteria.LESS_EQUAL);
		oCrit.and(CurrencyDetailPeer.END_DATE,   DateUtil.getEndOfDayDate(_dDate), Criteria.GREATER_EQUAL);
		return oCrit;
    }
    
    public static double getCurrentRateByID(String _sID)
    	throws Exception
    {
    	return getCurrentRateByID(_sID, null);
    }

    public static double getCurrentRateByID(String _sID, Connection _oConn)
		throws Exception
	{
    	Currency oCurr = getCurrencyByID(_sID, _oConn);
    	if (oCurr != null && !oCurr.getIsDefault())
    	{
			CurrencyDetail oDet = getLatestDetail (_sID, _oConn);
			if (oDet != null)
			{
				return oDet.getExchangeRate().doubleValue();
			}
    	}
		return 1; //default rate is always 1
	}
    
    public static double getFiscalRate(String _sID)
    	throws Exception
    {
    	return getFiscalRate(_sID, null);
    }
    
    public static double getFiscalRate(String _sID, Connection _oConn)
		throws Exception
	{
    	Currency oCurr = getCurrencyByID(_sID, _oConn);
    	if (oCurr != null && !oCurr.getIsDefault())
    	{
	    	CurrencyDetail oDet = getLatestDetail (_sID, _oConn);
	    	if (oDet != null)
	    	{
	    		return oDet.getFiscalRate().doubleValue();
	    	}
    	}
    	return 1; //default rate is always 1
	}

    public static double[] getRates(String _sID)
		throws Exception
	{
		return getRates(_sID, null);
	}
    
    /**
     * 
     * @param _sID CurrencyId
     * @param _oConn
     * @return double[] array of rates dRates[0] = exch rate, dRates[1] = fiscal rate
     * @throws Exception
     */
    public static double[] getRates(String _sID, Connection _oConn)
		throws Exception
	{
		double[] dRates = {1, 1};
		Currency oCurr = getCurrencyByID(_sID, _oConn);
    	if (!oCurr.getIsDefault())
    	{
	    	CurrencyDetail oDet = getLatestDetail (_sID, _oConn);
	    	if (oDet != null)
	    	{
				dRates [0] = oDet.getExchangeRate().doubleValue();
				dRates [1] = oDet.getFiscalRate().doubleValue();		
	    	}
    	}
		return dRates; 
	}

    /**
     * get Detail by ID & Date
     * 8 March 2014
     * 
     * @param _sID
     * @param _dDate
     * @return
     * @throws Exception
     */
    public static CurrencyDetail getDetailByIDAndDate(String _sID, Date _dDate)
    	throws Exception
    {
    	Currency oCurr = getCurrencyByID(_sID);
    	if (oCurr != null && !oCurr.getIsDefault())
    	{
    		Criteria oCrit = setDateCriteria (_sID, _dDate);
    		List vData = CurrencyDetailPeer.doSelect(oCrit);
    		if (vData.size() > 0) 
    		{
    			return (CurrencyDetail)vData.get(0);
    		}
    	}
    	return null;
    }
    
    public static double getRateByID(String _sID, Date _dDate)
    	throws Exception
    {
    	CurrencyDetail oDet = getDetailByIDAndDate(_sID, _dDate);
    	if (oDet != null)
    	{
    		return oDet.getExchangeRate().doubleValue();    		
    	}
    	return 1;
    }
    
    public static boolean isRateSet (Currency _oCurr, double _dRate)
		throws Exception
	{
		if (_oCurr != null)
		{
			if (!_oCurr.getIsDefault())
			{
				if (_dRate != 1) return true;
			}
			else 
			{
				if (_dRate == 1) return true;
			}
		}
		return false; 
	}    

	public static List getDetails(String _sID, Date _dStart, Date _dEnd)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(CurrencyDetailPeer.CURRENCY_ID, _sID);
		oCrit.addDescendingOrderByColumn(CurrencyDetailPeer.BEGIN_DATE);
		if (_dStart != null)
		{
			oCrit.add(CurrencyDetailPeer.BEGIN_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
		}
		if (_dEnd != null)
		{
			oCrit.add(CurrencyDetailPeer.END_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		}		
		return CurrencyDetailPeer.doSelect(oCrit);
    }
    
    public static String getCurrencyIDByStatus(boolean _bStat)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(CurrencyPeer.IS_DEFAULT, _bStat);
		List vData = CurrencyPeer.doSelect(oCrit);
		if (vData.size() > 0) {
			return ((Currency)vData.get(0)).getCurrencyId();
    	}
		return "";
	}
	
    public static boolean checkDefaultStatus()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(CurrencyPeer.IS_DEFAULT, true);
		List vData = CurrencyPeer.doSelect(oCrit);
		if (vData.size() > 0) {
			return true;
    	}
    	return false;
    }
    
    public static void setAllStatus(boolean bStat)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		Criteria oSelectCrit = new Criteria();
		oCrit.add(CurrencyPeer.IS_DEFAULT, bStat);
		CurrencyPeer.doUpdate(oSelectCrit,oCrit);
		CurrencyManager.getInstance().refreshCache("");
    }

    public static void createDefaultDetailRate(Currency _oCurr)
    	throws Exception
    {
        CurrencyDetail oDetail = new CurrencyDetail ();
        oDetail.setCurrencyId (_oCurr.getCurrencyId());
        oDetail.setCurrencyDetailId (IDGenerator.generateSysID());
        Date dBegin = (new GregorianCalendar (1990, 1, 1)).getTime();
        Date dEnd = (new GregorianCalendar (2050, 1, 1)).getTime();
        oDetail.setBeginDate (dBegin);
        oDetail.setEndDate (dEnd);
        oDetail.setExchangeRate (bd_ONE);
        oDetail.setFiscalRate(bd_ONE);
        oDetail.setUpdateDate(new Date());
        oDetail.save();
    }    
    
	public static String createJSArray (Date _dRateDate)
		throws Exception
	{	
		List vData = getAllCurrency();
		
		StringBuilder oSB = new StringBuilder ();
		oSB.append ("var aOF = new Array (");
		oSB.append (vData.size());
		oSB.append (");\n");
		
		for (int i = 0; i < vData.size(); i++)
		{
			Currency oCurr = (Currency) vData.get(i);
			oSB.append ("aOF[").append (i).append ("] = new Array ('")
			   .append (oCurr.getIsDefault()).append ("', '")
			   .append (getRateByID(oCurr.getCurrencyId(), _dRateDate)).append ("');\n");
		}
		return oSB.toString();
	}    
	
    public static void validateOBRate (String sCurrencyID, Number _dRate)
		throws Exception
	{
	    if (StringUtil.isNotEmpty(sCurrencyID))
	    {
	    	if (sCurrencyID.equals(CurrencyTool.getDefaultCurrency(null).getCurrencyId()))
	    	{
	    		_dRate = bd_ONE; //make default ob rate value is correct
	    	}
	    	else
	    	{
	    		if (_dRate == null || _dRate.doubleValue() == 0 || _dRate.doubleValue() < 0)
	    		{
	    			throw new Exception ("Invalid Rate");
	    		}
	    	}
	    }
	    else
	    {
	    	throw new Exception ("Invalid Currency ");
	    }
	}	
    
    public static void validateCurrency(String _sCurrencyID, String _sAccountID) 
		throws Exception
	{
		Account oAcc = AccountTool.getAccountByID(_sAccountID);
		if (oAcc != null)
		{
			if (StringUtil.isNotEmpty(_sCurrencyID))
			{
				if (!_sCurrencyID.equals(oAcc.getCurrencyId()))
					
				{
					throw new Exception("Currency in Bank and selected Account not Matched");
				}
			}
			else
			{
				throw new Exception("Invalid Currency selected ");				
			}
		}
		else
		{
			throw new Exception("Invalid Account selected");
		}
	}
    
	/**
	 * 
	 * @param _iSyncType
	 * @return List of updated currency details
	 * @throws Exception
	 */
	public static List getUpdatedDetails ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) 
		{
			oCrit.add(CurrencyDetailPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
		}
		return CurrencyDetailPeer.doSelect(oCrit);
	}	
}

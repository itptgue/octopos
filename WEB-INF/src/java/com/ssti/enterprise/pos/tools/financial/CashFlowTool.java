package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.model.PaymentOM;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.ApPaymentDetail;
import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.ArPaymentDetail;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankTransfer;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.om.CashFlowJournalPeer;
import com.ssti.enterprise.pos.om.CashFlowPeer;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.journal.CashFlowJournalTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for Cash Flow and Cash Flow Journal OM 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashFlowTool.java,v 1.33 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashFlowTool.java,v $
 *
 * 2016-02-16
 * - Override getBalanceAsOf add parameter base to have options to get base balance 
 * </pre><br>
 */
public class CashFlowTool extends BaseTool
{
    private static Log log = LogFactory.getLog(CashFlowTool.class);
    
//	public static final int i_CF_AR_PAYMENT      = 1;
//	public static final int i_CF_AP_PAYMENT      = 2;
//	public static final int i_CF_NORMAL          = 3;	
//	public static final int i_CF_OPENING_BALANCE = 4;
//	public static final int i_CF_DIRECT_SALES    = 5;
//	public static final int i_CF_CREDIT_MEMO     = 6;
//	public static final int i_CF_DEBIT_MEMO      = 7;
//  public static final int i_CF_BANK_TRANSFER   = 8;
   
	protected static Map m_FIND_PEER = new HashMap();
	
	public static final String s_ACCOUNT = new StringBuilder(CashFlowPeer.CASH_FLOW_ID).append(",")
		.append(CashFlowJournalPeer.CASH_FLOW_ID).append(",")
		.append(CashFlowJournalPeer.ACCOUNT_ID).toString();

	public static final String s_CFT = new StringBuilder(CashFlowPeer.CASH_FLOW_ID).append(",")
		.append(CashFlowJournalPeer.CASH_FLOW_ID).append(",")
		.append(CashFlowJournalPeer.CASH_FLOW_TYPE_ID).toString();
	
    static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), CashFlowPeer.CASH_FLOW_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), CashFlowPeer.DESCRIPTION);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), CashFlowPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), CashFlowPeer.REFERENCE_NO);             	
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), CashFlowPeer.LOCATION_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), CashFlowPeer.CURRENCY_ID);
        m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), CashFlowPeer.BANK_ID);		        
        m_FIND_PEER.put (Integer.valueOf(i_ISSUER	  ), CashFlowPeer.BANK_ISSUER);	
        m_FIND_PEER.put (Integer.valueOf(i_ACCOUNT    ), s_ACCOUNT);
        m_FIND_PEER.put (Integer.valueOf(i_CFT 		  ), s_CFT);        
	}   
	
    static CashFlowTool instance = null;
	
	public static synchronized CashFlowTool getInstance() 
	{
		if (instance == null) instance = new CashFlowTool();
		return instance;
	}
    
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);} 	
    
	//static methods
	public static CashFlow getHeaderByID(String _sID)
    	throws Exception
    {
		return getHeaderByID (_sID, null);
	}

	public static CashFlow getHeaderByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(CashFlowPeer.CASH_FLOW_ID, _sID);
	    List vData = CashFlowPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) 
	    {
			return (CashFlow) vData.get(0);
		}
		return null;
	}

	public static CashFlow getHeaderByTransID(String _sTransID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(CashFlowPeer.TRANSACTION_ID, _sTransID);
	    List vData = CashFlowPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) 
	    {
			return (CashFlow) vData.get(0);
		}
		return null;
	}	
	
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(CashFlowJournalPeer.CASH_FLOW_ID, _sID);
		return CashFlowJournalPeer.doSelect(oCrit);
	}

	protected static void deleteByID(String _sID, Connection _oConn)
    	throws Exception
    {
		CashFlow oCF = getHeaderByID(_sID, _oConn);
		if(oCF != null)
		{
			if(oCF.getStatus() != i_PROCESSED)
			{
				deleteDetailsByID(_sID, _oConn);
				Criteria oCrit = new Criteria();
		        oCrit.add(CashFlowPeer.CASH_FLOW_ID, _sID);
				CashFlowPeer.doDelete(oCrit, _oConn);
			}
			else
			{
				throw new Exception("Transaction is Processed, Can Not be Deleted!");
			}			
		}
	}
	
	protected static void deleteDetailsByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(CashFlowJournalPeer.CASH_FLOW_ID, _sID);
		CashFlowJournalPeer.doDelete(oCrit, _oConn);
	}	

	public static boolean isBankUsed(String _sBankID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CashFlowPeer.BANK_ID, _sBankID);
		oCrit.setLimit(1);
		List vData = CashFlowPeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			return true;
		}
		return false;
	}
	
    static int getLastNoType(CashFlow _oCF)
    {
        int iType = LastNumberTool.i_CASH_FLOW;
        if (_oCF != null && PreferenceTool.useCfInOut())
        {
            if(_oCF.getCashFlowType()== i_DEPOSIT)
            {
                iType = LastNumberTool.i_CASH_FLOW_IN;
            }
            if(_oCF.getCashFlowType()== i_WITHDRAWAL)
            {
                iType = LastNumberTool.i_CASH_FLOW_OUT;
            }
        }
        return iType;
    }
    
	/**
	 * 
	 */
	public static void generateNo(CashFlow _oCF, Connection _oConn)
		throws Exception
	{
		String sFmt = s_CF_FORMAT;
		log.debug("CF NUMBER SEPARATE IN / OUT: " + PreferenceTool.useCfInOut());

		int iType = LastNumberTool.i_CASH_FLOW;
		if (PreferenceTool.useCfInOut())
		{
            Bank oBank = BankTool.getBankByID(_oCF.getBankId(), _oConn);
            String sPref = "cf";
            if (_oCF.getCashFlowType() == i_DEPOSIT) 
            {
                iType = LastNumberTool.i_CASH_FLOW_IN;
                if (oBank.getIsCash())
                {
                    sPref = "cfc";
                    iType = LastNumberTool.i_CF_CASH_IN;
                }
                sFmt = CONFIG.getString(sPref + ".in.format");
            }
            else if (_oCF.getCashFlowType() == i_WITHDRAWAL) 
            {
                iType = LastNumberTool.i_CASH_FLOW_OUT;
                if (oBank.getIsCash())
                {
                    sPref = "cfc";
                    iType = LastNumberTool.i_CF_CASH_OUT;
                }
                sFmt = CONFIG.getString(sPref + ".out.format");
            }
        }
		String sLocCode = LocationTool.getLocationCodeByID(_oCF.getLocationId(), _oConn);
		_oCF.setCashFlowNo (LastNumberTool.get(sFmt, iType, sLocCode, _oConn));
		validateNo(CashFlowPeer.CASH_FLOW_NO, _oCF.getCashFlowNo(), CashFlowPeer.class, _oConn);

	}
	
	/**
	 * save cash flow journal
	 * 
	 * @param _oCF
	 * @param _vCFD
	 * @throws Exception
	 */
	public static void saveData (CashFlow _oCF, List _vCFD)
		throws Exception
	{
        saveData(_oCF, _vCFD, true, null);
    }
	
	/**
	 * save cash flow journal
	 * 
	 * @param _oCF
	 * @param _vCFD
	 * @param _bCashJournal
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData (CashFlow _oCF, List _vCFD, boolean _bCashJournal, Connection _oConn)
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null)
	    {
		    _oConn = beginTrans();
		    bStartTrans = true;
		}
	    validate(_oConn);
		try 
		{
			//validate date
			validateDate(_oCF.getExpectedDate(), _oConn);

		    if (validateJournal(_oCF))
		    {                		        
			    //process save cash flow Data
			    processSaveCashFlowData (_oCF, _vCFD, _oConn);
			    
			    if (_oCF.getStatus() == i_PAYMENT_PROCESSED)
			    {  			    
					validateDate(_oCF.getDueDate(), _oConn);

			        //update bank book
                    BankBookTool.updateBankBook(_oCF, _oConn);
                    
                    if (_bCashJournal)
                    {
                        //create journal
                        CashFlowJournalTool oJournal = new CashFlowJournalTool();
						oJournal.createJournal (_oCF, _vCFD, _oConn);
                    }
                    
                    //update source trans
                    updateSourceTrans(_oCF, _oConn);
                }
	            if (bStartTrans)
	            {                
                    commit (_oConn);            
                }
            }
		}
		catch (Exception _oEx) 
		{
		    if (_oCF.getStatus() == i_PAYMENT_PROCESSED)
		    {  			    
		    	_oCF.setCashFlowNo("");
		    	_oCF.setStatus(i_PAYMENT_RECEIVED);
		    }			
			if (bStartTrans)
	        {    
			    rollback (_oConn);
		    }
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}

	/**
	 * process save cf trans
	 * 
	 * @param _oCF
	 * @param _vCFD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSaveCashFlowData (CashFlow _oCF, List _vCFD, Connection _oConn)
		throws Exception
	{	    
		validate(_oConn);
		if (StringUtil.isEmpty(_oCF.getCashFlowId()))
		{
            _oCF.setCashFlowId (IDGenerator.generateSysID());
            _oCF.setNew(true); //make sure no duplicate ID allowed by DB
        }

		validateID(getHeaderByID(_oCF.getCashFlowId(), _oConn), "cashFlowNo");
		
		if (StringUtil.isEmpty(_oCF.getCashFlowNo())) 
        {         	         
        	if (_oCF.getStatus() == i_PAYMENT_PROCESSED)
    	    {
        		generateNo(_oCF,_oConn);
        		validateNo(CashFlowPeer.CASH_FLOW_NO, _oCF.getCashFlowNo(), CashFlowPeer.class, _oConn);
    		}
        	else
        	{
        		_oCF.setCashFlowNo ("");	
        	}
        }  
	    _oCF.setSayAmount (CustomFormatter.numberToStr(PreferenceTool.getLocale(), _oCF.getCashFlowAmount()));   

        //delete all existing detail from pending transaction
        if (StringUtil.isNotEmpty(_oCF.getCashFlowId()))
        {
        	deleteDetailsByID (_oCF.getCashFlowId(), _oConn);
        }			
		_oCF.save (_oConn);

		for ( int i = 0; i < _vCFD.size(); i++ )
		{
			CashFlowJournal oCFD = (CashFlowJournal) _vCFD.get(i);
			//always set the value, because we probably have deleted all the details
			oCFD.setModified (true);
            oCFD.setNew (true);
		    if (StringUtil.isEmpty(oCFD.getCashFlowJournalId()))
		    {			
		        oCFD.setCashFlowJournalId(IDGenerator.generateSysID());
			}
			oCFD.setCashFlowId(_oCF.getCashFlowId());			
			oCFD.save(_oConn);
		}
	}

	/**
	 * cancel cash flow from outside transaction
	 * 
	 * @param _sTransID
	 */
	public static void cancelCashFlowByTransID (String _sTransID, String _sUserName, Connection _oConn)
		throws Exception
	{
		//get cf trans to cancel
		CashFlow oCF = getHeaderByTransID(_sTransID, _oConn);
		if (oCF != null)
		{
			if (oCF.getStatus() == i_PENDING)
			{
				deleteByID(oCF.getCashFlowId(), _oConn);
			}
			else
			{
				//process trans cancel
				cancelCashFlow(oCF, _sUserName, _oConn);
			}
		}		
	}
	
	/**
	 * cancel Cash Flow Transaction
	 * 
	 * @param _sCFID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelCashFlow (CashFlow _oCF, String _sCancelBy, Connection _oConn)
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null)
	    {
		    _oConn = beginTrans();
		    bStartTrans = true;
		}
		try 
		{			
			if (_oCF != null)
			{
				//validate date
				validateDate(_oCF.getDueDate(), _oConn);

				if (_oCF.getStatus() == i_PAYMENT_PROCESSED)
				{  			    
					//reverse cash flow type to rollback bank book balance
					reverseType (_oCF);
						
					//update bank book
		            BankBookTool.updateBankBook(_oCF, _oConn);
		            
					//reverse again, to save the original cash flow type
					reverseType (_oCF);
		            		            
		            //delete journal created by this cash flow transaction
		            CashFlowJournalTool oJournal = new CashFlowJournalTool();
		            oJournal.deleteCashFlowJournal(
		            	_oCF.getTransactionType(), _oCF.getCashFlowId(), _oConn
					);		            
				}
				
				_oCF.setDescription(cancelledBy(_oCF.getDescription(), _sCancelBy));
	 			_oCF.setStatus(i_PAYMENT_CANCELLED);
	            _oCF.save(_oConn);
			}

			if (bStartTrans)
            {                
                commit (_oConn);            
            }			
		}
		catch (Exception _oEx) 
		{
			if (bStartTrans)
	        {    
			    rollback (_oConn);
		    }
			_oEx.printStackTrace();
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}	


	private static void updateSourceTrans(CashFlow _oCF, Connection _oConn)
		throws Exception
	{
		if (_oCF != null)
		{
			PaymentOM oPMT = null;
			if (_oCF.getTransactionType() == i_CF_AR_PAYMENT)
			{
				oPMT = ReceivablePaymentTool.getHeaderByID(_oCF.getTransactionId(),_oConn);
			}
			if (_oCF.getTransactionType() == i_CF_AP_PAYMENT)
			{
				oPMT = PayablePaymentTool.getHeaderByID(_oCF.getTransactionId(),_oConn);
			}
			if (oPMT != null)
			{
				oPMT.setCashFlowId(_oCF.getCashFlowId());
				oPMT.setCfStatus(_oCF.getStatus());
				oPMT.setCfDate(_oCF.getDueDate());
				oPMT.save(_oConn);
			}
		}
	}
	
	/**
	 * 
	 * @param _oCF
	 * @param _vCFD
	 * @param data
	 * @param _bAll
	 * @throws Exception
	 */
	public static void setHeaderProperties (CashFlow _oCF, List _vCFD, RunData data, boolean _bAll) 
    	throws Exception
    {
		try 
		{
		    if(data != null)
            {
                String sBankID = data.getParameters().getString("BankId");
                double dAmount = data.getParameters().getDouble("CashFlowAmount");
                double dRate = data.getParameters().getDouble("CurrencyRate");
                double dBase = dAmount * dRate;

                String sCurrencyID = "";
                Bank oBank = BankTool.getBankByID(sBankID);
                if (oBank != null) sCurrencyID = oBank.getCurrencyId();
                          
                if (dAmount <= 0)
                {
                    throw new Exception ("Invalid Amount, Please Fill in The Correct Amount");
                }
                if (dRate <= 0)
                {
                    throw new Exception ("Invalid Currency Rate, Please Setup Currency Properly");
                }

                data.getParameters().setProperties(_oCF);
                _oCF.setExpectedDate    (CustomParser.parseDate(data.getParameters().getString("ExpectedDate")));
                _oCF.setDueDate         (CustomParser.parseDate(data.getParameters().getString("DueDate")));
                _oCF.setCurrencyId      (sCurrencyID);          
                _oCF.setCurrencyRate    (new BigDecimal (dRate));           
                _oCF.setCashFlowAmount  (new BigDecimal (dAmount));         
                _oCF.setAmountBase      (new BigDecimal (dBase));
            }
	        
	        if (_oCF.getTransactionType() <= 0) 
	        {
	        	_oCF.setTransactionType (i_CF_NORMAL);	    
	        }
	        if (StringUtil.isEmpty(_oCF.getTransactionId()) || _oCF.getTransactionType() == i_CF_NORMAL) 
	        {
	        	_oCF.setTransactionId ("");    
		        _oCF.setTransactionNo ("");                    
	        }
	        
			setDetailProperties (_oCF, _vCFD, data, _bAll);
            if (_oCF.getDueDate().after(new Date()))
            {
	            _oCF.setStatus(i_PAYMENT_RECEIVED);   //pending                 
            }   
		}       
		catch (Exception _oEx) 
		{       
			log.error (_oEx);
            _oEx.printStackTrace();
			throw new NestableException ("Set CF Properties Failed : " + _oEx.getMessage (), _oEx); 
		}       
    }           
                
	/**
	 * 
	 * @param _oCF
	 * @param _vCFD
	 * @param data
	 * @param _bAll
	 * @throws Exception
	 */
	private static void setDetailProperties (CashFlow _oCF, List _vCFD, RunData data, boolean _bAll) 
    	throws Exception
    {           
        double dTotal = 0;
                
		for (int i = 0; i < _vCFD.size(); i++)
		{		
		    int iNo = i + 1;    
		    String sNo = (Integer.valueOf(iNo)).toString();    
		    if (!_bAll && (i == (_vCFD.size() - 1))) sNo = "0";
                
		    CashFlowJournal oCFD = (CashFlowJournal) _vCFD.get(i);		    
		    double dCFRate = _oCF.getCurrencyRate().doubleValue();
            double dRate = 1;   
            double dAmount = 0; 
            double dAmountBase = 0; 
            
            if(oCFD.getCurrencyRate() != null) dRate = oCFD.getCurrencyRate().doubleValue();
            if(oCFD.getAmount() != null) dAmount = oCFD.getAmount().doubleValue();
            if(oCFD.getAmountBase() != null) dAmountBase = oCFD.getAmountBase().doubleValue();            
            if (data != null)
            {
                dRate = data.getParameters().getDouble("CurrencyRate" + sNo);                
    		    dAmount = data.getParameters().getDouble("Amount" + sNo);
                dAmountBase = dAmount * dRate;
    
                if(StringUtil.isEqual(_oCF.getCurrencyId(), oCFD.getCurrencyId()))
                {
                	dRate = dCFRate;
                }
                
    		    oCFD.setCurrencyRate (new BigDecimal (dRate));
    		    oCFD.setAmount       (new BigDecimal (dAmount));
    		    oCFD.setAmountBase   (new BigDecimal (dAmountBase));
    		    
    		    oCFD.setLocationId     (data.getParameters().getString("LocationId" + sNo, ""));	
    		    oCFD.setCashFlowTypeId (data.getParameters().getString("CashFlowTypeId" + sNo, ""));	
            }
		    if (oCFD.getProjectId() == null) oCFD.setProjectId("");
		    if (oCFD.getDepartmentId() == null) oCFD.setDepartmentId("");

		    //oCFD.setProjectId     (data.getParameters().getString("ProjectId" + sNo,""));
		    //oCFD.setDepartmentId  (data.getParameters().getString("DepartmentId" + sNo,""));
            
            //if a CASH/BANK DEPOSIT then whe should credit any other account in journal
            if (_oCF.getCashFlowType() == i_DEPOSIT) 
            {
		        oCFD.setDebitCredit  (GlAttributes.i_CREDIT); 
            }		
            else //if CASH/BANK WITHDRAWAL we should debit any other account
            {
		        oCFD.setDebitCredit  (GlAttributes.i_DEBIT);             
            }
            dTotal = dTotal + dAmountBase;
         }
		 _oCF.setJournalAmount (new BigDecimal (dTotal));
    }    

	/**
	 * get cash flow journal post
	 * 
	 * @param _oCF
	 */
	private static void setStatus (CashFlow _oCF)
	{
		if (_oCF != null)
		{
			if (DateUtil.isAfter(_oCF.getDueDate(), _oCF.getExpectedDate()))
			{
				_oCF.setStatus (i_PAYMENT_RECEIVED);                    
			}
			else
			{
				_oCF.setStatus (i_PAYMENT_PROCESSED);                    
			}
		}
	}
	
	/**
	 * reverse deposit and withdrawal
	 * 
	 * @param _oCF
	 */
	private static void reverseType (CashFlow _oCF)
	{
		if (_oCF != null)
		{
			if (_oCF.getCashFlowType() == i_DEPOSIT) _oCF.setCashFlowType(i_WITHDRAWAL);
			else if (_oCF.getCashFlowType() == i_WITHDRAWAL)  _oCF.setCashFlowType(i_DEPOSIT);
			else log.error("Invalid CF Type " + _oCF.getCashFlowType());
		}
	}
	
	/**
	 * check for minus amount in cash flow, reverse type if minus
	 * 
	 * @param _oCF
	 */
	private static boolean checkAmount (CashFlow _oCF)
	{
		if (_oCF != null)
		{
			double dAmount = _oCF.getCashFlowAmount().doubleValue();
			double dAmountBase = _oCF.getAmountBase().doubleValue();
			
			if (dAmount < 0)
			{
				reverseType (_oCF);
				dAmount = dAmount * -1;
				dAmountBase = dAmountBase * -1;
				_oCF.setCashFlowAmount(new BigDecimal(dAmount));
				_oCF.setAmountBase(new BigDecimal(dAmountBase));
				_oCF.setDescription(_oCF.getDescription() + " (REVERSED)");
				return true;
			}
		}
		return false;
	}

	/**
	 * get cash flow journal post
	 * 
	 * @param _oCF
	 */
	private static int debitCredit (CashFlow _oCF)
	{
		if (_oCF != null)
		{
			if (_oCF.getCashFlowType() == i_DEPOSIT) return GlAttributes.i_CREDIT;
			else if (_oCF.getCashFlowType() == i_WITHDRAWAL)  return GlAttributes.i_DEBIT;
			else log.error("Invalid CF Type " + _oCF.getCashFlowType());
		}
		return -1;
	}
	
	/**
	 * 
	 * @param _sBankID
	 * @param _sCurrencyID
	 * @param _sUserName
	 * @param _dAsDate
	 * @param _dOBChange
	 * @param _dRate
	 * @param _bIsUpdate
	 * @throws Exception
	 */
    public static void updateOpeningBalance (Bank _oBank,
											 String _sUserName, 
			                                 double _dOBChange, 
											 boolean _bIsUpdate)
        throws Exception
    {   
    	String sOB = LocaleTool.getString("opening_balance");
        int iType = i_DEPOSIT;
        String sDesc = sOB + " " + _oBank.getDescription();
        
        if (_bIsUpdate) 
        {
        	sDesc = LocaleTool.getString("update") + " " + 
			sOB + " " + _oBank.getDescription();
        }
        
        if (_dOBChange < 0) 
        {
            iType = i_WITHDRAWAL; 
            _dOBChange = _dOBChange * -1;
        }
        double dAmountBase =   (_dOBChange * _oBank.getObRate().doubleValue());
        BigDecimal bdAmountBase = new BigDecimal(dAmountBase);
        	
		CashFlow oCF           = new CashFlow();
    	oCF.setCashFlowType    (iType);
    	oCF.setBankId  	 	   (_oBank.getBankId());
    	oCF.setBankIssuer  	   ("-");
    	oCF.setReferenceNo     (sOB);
    	oCF.setLocationId	   (_oBank.getLocationId());
		oCF.setCurrencyId  	   (_oBank.getCurrencyId());
    	oCF.setCashFlowAmount  (new BigDecimal(_dOBChange));
    	oCF.setCurrencyRate    (_oBank.getObRate());
    	oCF.setAmountBase  	   (bdAmountBase);
    	oCF.setExpectedDate    (_oBank.getAsDate());
    	oCF.setDueDate  	   (_oBank.getAsDate());
    	oCF.setUserName  	   (_sUserName);
    	oCF.setDescription     (sDesc);  
		oCF.setTransactionId   (_oBank.getBankId());                     
		oCF.setTransactionNo   ("");                    
		oCF.setTransactionType (i_CF_NORMAL);		
		oCF.setStatus          (i_PAYMENT_PROCESSED);                    
		oCF.setSayAmount       ("");                    
		oCF.setJournalAmount   (new BigDecimal (dAmountBase));                    

        Account oEquity = AccountTool.getOpeningBalanceEquity(null);

        CashFlowJournal oCFD = new CashFlowJournal();
        List vCFD = new ArrayList(1);        

        oCFD.setCashFlowJournalId   (""); 
        oCFD.setCashFlowId 		    ("");
        oCFD.setAccountId  	 		(oEquity.getAccountId());   
        oCFD.setAccountCode 		(oEquity.getAccountCode());   
        oCFD.setAccountName 		(oEquity.getAccountName());   
        oCFD.setCurrencyId 		    (oEquity.getCurrencyId());  //must be in default currency
        oCFD.setCurrencyRate  	    (bd_ONE);   
        oCFD.setAmount	  		    (bdAmountBase);   
        oCFD.setAmountBase 	  	    (bdAmountBase);
        oCFD.setCashFlowTypeId		("");
        oCFD.setLocationId			(_oBank.getLocationId());
        oCFD.setProjectId		    ("");
        oCFD.setDepartmentId 		("");   
        oCFD.setMemo                (sDesc);
        oCFD.setDebitCredit  	    (debitCredit(oCF)); 	
        vCFD.add (oCFD);
    	saveData (oCF, vCFD);    	    	
    } 
    
    /**
     * create CF From Credit Memo
     * 
     * @param _oData
     * @param _oConn
     * @throws Exception
     */
	public static void createFromCreditMemo (CreditMemo _oData, Connection _oConn)
		throws Exception
	{
		StringBuilder oDesc = new StringBuilder ();
		if (StringUtil.isNotEmpty(_oData.getRemark()))
		{
			oDesc.append (_oData.getRemark());	
			oDesc.append("\n");
		}
	    oDesc.append (LocaleTool.getString("credit_memo"));
	    oDesc.append (" : ");
	    oDesc.append (_oData.getCustomerName());
	    oDesc.append (" (");
	    oDesc.append (_oData.getCreditMemoNo());
	    oDesc.append (")");
	    
	    int iCFType = i_DEPOSIT;
	    
	    //if cash sales return then withdrawal cash flow
	    if (_oData.getTransactionType() == i_AR_TRANS_SALES_RETURN)
	    {
	    	iCFType = i_WITHDRAWAL;
	    }

	    String sCurrencyID = _oData.getCurrencyId();	    
	    BigDecimal bdAmount = _oData.getAmount();
	    BigDecimal bdRate = _oData.getCurrencyRate();
	    
		double dRate = bdRate.doubleValue();
    	double dAmountBase = bdAmount.doubleValue() * dRate;
    	BigDecimal bdAmountBase = new BigDecimal (dAmountBase);

	    Bank oBank = BankTool.getBankByID(_oData.getBankId(), _oConn);
	    Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);
	    if (!oBankCurr.getCurrencyId().equals(_oData.getCurrencyId()) && oBankCurr.getIsDefault())
	    {
		    sCurrencyID = oBankCurr.getCurrencyId();
		    bdAmount = bdAmountBase;
		    bdRate = bd_ONE;
	    }
	    
	    boolean bCross = false;
	    String sCrossAccID = _oData.getCrossAccountId();
	    Account oCrossAcc = AccountTool.getAccountByID(sCrossAccID,_oConn);
	    if (StringUtil.isNotEmpty(sCrossAccID) && oCrossAcc != null)
	    {
	    	bdAmount = bd_ZERO;
	    	bdAmountBase = bd_ZERO;	    	
	    	bCross = true;
	    }
	    
		CashFlow oCF = new CashFlow();
		oCF.setCashFlowType    (iCFType);
		oCF.setBankId  	 	   (_oData.getBankId());
		oCF.setBankIssuer  	   (_oData.getBankIssuer());
		oCF.setReferenceNo     (_oData.getReferenceNo());
		oCF.setCurrencyId  	   (sCurrencyID);
		oCF.setCurrencyRate    (bdRate);
		oCF.setExpectedDate    (_oData.getTransactionDate());
		oCF.setDueDate  	   (_oData.getDueDate());
		oCF.setCashFlowAmount  (bdAmount);
		oCF.setAmountBase  	   (bdAmountBase);
		oCF.setUserName  	   (_oData.getUserName());
		oCF.setDescription     (oDesc.toString());  	 
		oCF.setTransactionId   (_oData.getCreditMemoId());                     
		oCF.setTransactionNo   (_oData.getCreditMemoNo());                    
		oCF.setTransactionType (i_CF_CREDIT_MEMO);	    	
		oCF.setLocationId	   (_oData.getLocationId());
		
		boolean bReversed = checkAmount (oCF);
		setStatus (oCF);
		
		oCF.setSayAmount       ("");                    
		oCF.setJournalAmount   (oCF.getAmountBase());     
		log.debug (oCF);
		
		Account oCM = AccountTool.getAccountByID(_oData.getAccountId(), _oConn);
		List vCFD = new ArrayList(1);
	    
		CashFlowJournal oCFD = new CashFlowJournal();	    
		oCFD.setCashFlowJournalId   (""); 
	    oCFD.setCashFlowId 		    ("");
	    oCFD.setAccountId  	 		(oCM.getAccountId());   
	    oCFD.setAccountCode 		(oCM.getAccountCode());   
	    oCFD.setAccountName 		(oCM.getAccountName());   
	    oCFD.setCurrencyId 		    (_oData.getCurrencyId());   
	    oCFD.setCurrencyRate  	    (_oData.getCurrencyRate());
	    
	    if (bReversed)
	    {
		    oCFD.setAmount	  		(new BigDecimal(Calculator.mul(_oData.getAmount(), -1)));   
		    oCFD.setAmountBase 	  	(new BigDecimal(Calculator.mul(_oData.getAmountBase(), -1)));  	       	    	
	    }
	    else
	    {
		    oCFD.setAmount	  		(_oData.getAmount());   
		    oCFD.setAmountBase 	  	(_oData.getAmountBase());  	       
		}
	    
	    oCFD.setCashFlowTypeId		(_oData.getCashFlowTypeId());
	    oCFD.setLocationId			(_oData.getLocationId());
	    oCFD.setProjectId		    ("");
	    oCFD.setDepartmentId 		("");   
	    oCFD.setMemo                (oDesc.toString());
	    oCFD.setDebitCredit  	    (debitCredit(oCF)); 	
	    
	    oCFD.setSubLedgerId			(_oData.getEntityId());
	    oCFD.setSubLedgerType		(i_SUB_LEDGER_AR);
	    oCFD.setSubLedgerTransId	(_oData.getMemoId());
	    oCFD.setSubLedgerDesc		(_oData.getEntityName());
	    vCFD.add (oCFD);
	    
	    if (StringUtil.isNotEmpty(sCrossAccID) && oCrossAcc != null)
	    {
	    	BigDecimal bdCross = new BigDecimal(_oData.getAmountBase().doubleValue() * -1);
	    	BigDecimal bdCrossBase = new BigDecimal(_oData.getAmountBase().doubleValue() * -1);
	    	
			CashFlowJournal oCross = new CashFlowJournal();	    
			oCross.setCashFlowJournalId (""); 
		    oCross.setCashFlowId 		("");
		    oCross.setAccountId  	 	(oCrossAcc.getAccountId());   
		    oCross.setAccountCode 		(oCrossAcc.getAccountCode());   
		    oCross.setAccountName 		(oCrossAcc.getAccountName());   
		    oCross.setCurrencyId 		(CurrencyTool.getDefaultCurrency().getCurrencyId());   
		    oCross.setCurrencyRate  	(bd_ONE);   
		    oCross.setAmount	  		(bdCross);   
		    oCross.setAmountBase 	  	(bdCrossBase);    
		    oCross.setCashFlowTypeId	(_oData.getCashFlowTypeId());
		    oCross.setLocationId		(_oData.getLocationId());
		    oCross.setProjectId		    ("");
		    oCross.setDepartmentId 		("");   
		    oCross.setMemo              (oDesc.toString());
		    oCross.setDebitCredit  	    (debitCredit(oCF)); 	
		    
		    oCross.setSubLedgerId		(_oData.getEntityId());
		    oCross.setSubLedgerType		(i_SUB_LEDGER_AR);
		    oCross.setSubLedgerTransId	(_oData.getMemoId());
		    oCross.setSubLedgerDesc		(_oData.getEntityName());
		    vCFD.add (oCross);
	    }
		saveData (oCF, vCFD, true, _oConn);    	
	}
	
	/**
	 * Create from AR Payment 
	 * 
	 * @param _oARP
	 * @param _vARD
	 * @param _oConn
	 * @throws Exception
	 */
	public static CashFlow createFromARPayment (ArPayment _oARP, 
												List _vARD,
												boolean _bIsFuture,
												Connection _oConn)
		throws Exception
	{		
		StringBuilder oDesc = new StringBuilder ();
		if (StringUtil.isNotEmpty(_oARP.getRemark()))
		{
			oDesc.append (_oARP.getRemark());	
			oDesc.append("\n");
		}		
		oDesc.append ("Receive Payment From : ");
		oDesc.append (_oARP.getCustomerName());
		oDesc.append (" (");
		oDesc.append (_oARP.getArPaymentNo());
		oDesc.append (") ");	    
	    
		String sCurrencyID = _oARP.getCurrencyId();	    
	    BigDecimal bdAmount = _oARP.getTotalAmount();
	    BigDecimal bdRate = _oARP.getCurrencyRate();
	    
		double dRate = bdRate.doubleValue();
    	double dAmountBase = bdAmount.doubleValue() * dRate;
    	BigDecimal bdAmountBase = new BigDecimal (dAmountBase);

	    Bank oBank = BankTool.getBankByID(_oARP.getBankId(), _oConn);
	    Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);
	    if (!oBankCurr.getCurrencyId().equals(_oARP.getCurrencyId()) && oBankCurr.getIsDefault())
	    {
	    	sCurrencyID = oBankCurr.getCurrencyId();
	    	bdAmount = bdAmountBase;
	    	bdRate = bd_ONE;
	    }
	    		
		CashFlow oCF = new CashFlow();
		oCF.setCashFlowType    (i_DEPOSIT);
		oCF.setBankId  	 	   (_oARP.getBankId());
		oCF.setBankIssuer  	   (_oARP.getBankIssuer());
		oCF.setReferenceNo     (_oARP.getReferenceNo());
		oCF.setCurrencyId  	   (sCurrencyID);		
		oCF.setCurrencyRate    (bdRate);
		oCF.setExpectedDate    (_oARP.getArPaymentDate());
		oCF.setDueDate  	   (_oARP.getArPaymentDueDate());
		oCF.setCashFlowAmount  (bdAmount);
		oCF.setAmountBase  	   (bdAmountBase);
		oCF.setUserName  	   (_oARP.getUserName());
		oCF.setDescription     (oDesc.toString());  
		oCF.setTransactionId   (_oARP.getArPaymentId());                     
		oCF.setTransactionNo   (_oARP.getArPaymentNo());                    
		oCF.setTransactionType (i_CF_AR_PAYMENT);	    	    	
		oCF.setLocationId	   (_oARP.getLocationId());
		
		boolean bReversed = checkAmount(oCF);
		setStatus (oCF);
			
		oCF.setJournalAmount   (oCF.getAmountBase());                    		
		oCF.setSayAmount       (CustomFormatter.numberToStr(PreferenceTool.getLocale(), dAmountBase));

        int iDebitCredit = debitCredit(oCF);
		
		List vCFD = new ArrayList();
		for (int i = 0; i < _vARD.size(); i++)
		{
			ArPaymentDetail oAPD = (ArPaymentDetail) _vARD.get(i);
			
            boolean bARTemp = false;
			Account oAR = null; 
			String sARTemp = GLConfigTool.getGLConfig(_oConn).getArPaymentTemp();
			if (_bIsFuture && StringUtil.isNotEmpty(sARTemp)) //use TEMP account
			{
				
				oAR = AccountTool.getAccountByID(sARTemp, _oConn);
                bARTemp = true;
			}
			else //use AR account
			{
				oAR = AccountTool.getAccountReceivable(_oARP.getCurrencyId(), _oARP.getCustomerId(), _oConn);
				if (oAPD.getTaxPayment())
				{
					oAR = AccountTool.getAccountReceivable(CurrencyTool.getDefaultCurrency(_oConn).getCurrencyId(), "", _oConn);					
				}
			}
			CashFlowJournal oCFD = new CashFlowJournal();

			Currency oBaseCurr = CurrencyTool.getDefaultCurrency(_oConn);
			double dInvRate = oAPD.getInvoiceRate().doubleValue();
			double dPayment = oAPD.getPaymentAmount().doubleValue();
			double dPaymentBase = dPayment * dInvRate;
			double dGainLoss = 0;
			
			if (dRate != dInvRate) dGainLoss = (dPayment * dRate) - (dPaymentBase);
			if (bReversed) 
			{
				dPayment = dPayment * -1;
				dPaymentBase = dPaymentBase * -1;
				dGainLoss = dGainLoss * -1;		
				log.debug("REVERSED PAYMENT : " + dPayment + " " + dPaymentBase);				
			}
			
			oCFD.setCashFlowJournalId   (""); 
			oCFD.setCashFlowId 		    ("");
			oCFD.setAccountId  	 		(oAR.getAccountId());   
			oCFD.setAccountCode 		(oAR.getAccountCode());   
			oCFD.setAccountName 		(oAR.getAccountName());   

            if (bARTemp)
            {
                oCFD.setCurrencyId          (oBaseCurr.getCurrencyId());   
                oCFD.setCurrencyRate        (bd_ONE);   
                oCFD.setAmount              (new BigDecimal(dPaymentBase));   
                oCFD.setAmountBase          (new BigDecimal(dPaymentBase));                   
            }
            else
            {
                oCFD.setCurrencyId          (sCurrencyID);   
                oCFD.setCurrencyRate        (new BigDecimal(dInvRate));   
                oCFD.setAmount              (new BigDecimal(dPayment));   
                oCFD.setAmountBase          (new BigDecimal(dPaymentBase)); 
            }

            oCFD.setCashFlowTypeId		(_oARP.getCashFlowTypeId());        
			oCFD.setLocationId		    (_oARP.getLocationId());        
			oCFD.setProjectId		    ("");
			oCFD.setDepartmentId 		("");   
			oCFD.setMemo                (oDesc.toString());
			oCFD.setDebitCredit  	    (iDebitCredit); 
			oCFD.setSubLedgerId			(_oARP.getEntityId());
			oCFD.setSubLedgerType		(i_SUB_LEDGER_AR);
			oCFD.setSubLedgerTransId	(_oARP.getTransactionId());
			oCFD.setSubLedgerDesc		(_oARP.getEntityName());
			vCFD.add (oCFD);
			
			if (dGainLoss != 0)
			{
				log.debug("GAIN LOSS :  " +  dGainLoss);
				Account oAPGL = AccountTool.getRealUnrealGainLoss(_oARP.getCurrencyId(), true, _oConn);
				CashFlowJournal oGainLoss = new CashFlowJournal();
				
				oGainLoss.setCashFlowJournalId  (""); 
				oGainLoss.setCashFlowId 		("");
				oGainLoss.setAccountId  	 	(oAPGL.getAccountId());   
				oGainLoss.setAccountCode 		(oAPGL.getAccountCode());   
				oGainLoss.setAccountName 		(oAPGL.getAccountName());   
				oGainLoss.setCurrencyId 		(oBaseCurr.getCurrencyId());   
				oGainLoss.setCurrencyRate  	    (bd_ONE);   
				oGainLoss.setAmount	  		    (new BigDecimal(dGainLoss));   
				oGainLoss.setAmountBase 	  	(new BigDecimal(dGainLoss));     
				oGainLoss.setCashFlowTypeId		(_oARP.getCashFlowTypeId());        
				oGainLoss.setLocationId		    (_oARP.getLocationId());        
				oGainLoss.setProjectId		    ("");
				oGainLoss.setDepartmentId 		("");   
				oGainLoss.setMemo               (oDesc.toString());
				oGainLoss.setDebitCredit  	    (iDebitCredit);                 
				oGainLoss.setSubLedgerId		(_oARP.getEntityId());
				oGainLoss.setSubLedgerType		(i_SUB_LEDGER_AR);
				oGainLoss.setSubLedgerTransId	(_oARP.getTransactionId());
				oGainLoss.setSubLedgerDesc		(_oARP.getEntityName());
				vCFD.add (oGainLoss);
			}
			
			double dDiscAmt  = oAPD.getDiscountAmount().doubleValue();
			String sDiscAccID = oAPD.getDiscountAccountId();
			if (dDiscAmt != 0 && StringUtil.isNotEmpty(sDiscAccID))
			{
				Account oAPDisc = AccountTool.getAccountByID(sDiscAccID);
				CashFlowJournal oDisc = new CashFlowJournal();
				
				dDiscAmt = dDiscAmt * -1; //minus the discount towards AP
				double dDiscBase = dDiscAmt * dRate;
				if (bReversed)
				{
					dDiscAmt = dDiscAmt * -1; 
					dDiscBase = dDiscBase * -1;
				}
				
				oDisc.setCashFlowJournalId  (""); 
				oDisc.setCashFlowId 		("");
				oDisc.setAccountId  	 	(oAPDisc.getAccountId());   
				oDisc.setAccountCode 		(oAPDisc.getAccountCode());   
				oDisc.setAccountName 		(oAPDisc.getAccountName());   
				oDisc.setCurrencyId 		(_oARP.getCurrencyId());   
				oDisc.setCurrencyRate  	    (_oARP.getCurrencyRate());   
				oDisc.setAmount	  		    (new BigDecimal(dDiscAmt));   
				oDisc.setAmountBase 	  	(new BigDecimal(dDiscBase));  
				oDisc.setCashFlowTypeId		(_oARP.getCashFlowTypeId());    
				oDisc.setLocationId		    (_oARP.getLocationId());        
				oDisc.setProjectId		    (oAPD.getProjectId());
				oDisc.setDepartmentId 		(oAPD.getDepartmentId());   
				oDisc.setMemo               (oDesc.toString());
				oDisc.setDebitCredit  	    (iDebitCredit);  	
				oDisc.setSubLedgerId		(_oARP.getEntityId());
				oDisc.setSubLedgerType		(i_SUB_LEDGER_AR);
				oDisc.setSubLedgerTransId	(_oARP.getTransactionId());
				oDisc.setSubLedgerDesc		(_oARP.getEntityName());				
				vCFD.add (oDisc);
			}
		}        
		saveData (oCF, vCFD, true, _oConn);    
		return oCF;
	}
	
	/**
	 * Create CF From AP Payment
	 * 
	 * @param _oAPP
	 * @param _vAPD
	 * @param _oConn
	 * @throws Exception
	 */
	public static CashFlow createFromAPPayment (ApPayment _oAPP, 
												List _vAPD,
												boolean _bIsFuture,
												Connection _oConn)
		throws Exception
	{		
	    StringBuilder oDesc = new StringBuilder ();
		if (StringUtil.isNotEmpty(_oAPP.getRemark()))
		{
			oDesc.append (_oAPP.getRemark());	
			oDesc.append("\n");
		}
		oDesc.append ("Make Payment To ");
	    oDesc.append (_oAPP.getVendorName());
	    oDesc.append (" (");
	    oDesc.append (_oAPP.getApPaymentNo());
        oDesc.append (") ");	    

	    String sCurrencyID = _oAPP.getCurrencyId();	    
	    BigDecimal bdAmount = _oAPP.getTotalAmount();
	    BigDecimal bdRate = _oAPP.getCurrencyRate();
	    
		double dRate = bdRate.doubleValue();
    	double dAmountBase = bdAmount.doubleValue() * dRate;
    	BigDecimal bdAmountBase = new BigDecimal (dAmountBase);

	    Bank oBank = BankTool.getBankByID(_oAPP.getBankId(), _oConn);
	    Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);
	    if (!oBankCurr.getCurrencyId().equals(_oAPP.getCurrencyId()) && oBankCurr.getIsDefault())
	    {
	    	sCurrencyID = oBankCurr.getCurrencyId();
	    	bdAmount = bdAmountBase;
	    	bdRate = bd_ONE;
	    }
	    
		CashFlow oCF = new CashFlow();
        oCF.setCashFlowType    (i_WITHDRAWAL);
    	oCF.setBankId  	 	   (_oAPP.getBankId());
    	oCF.setBankIssuer  	   (_oAPP.getBankIssuer());
    	oCF.setReferenceNo     (_oAPP.getReferenceNo());
    	oCF.setCurrencyId  	   (sCurrencyID);		
    	oCF.setCurrencyRate    (bdRate);
    	oCF.setExpectedDate    (_oAPP.getApPaymentDate());
    	oCF.setDueDate  	   (_oAPP.getApPaymentDueDate());
    	oCF.setCashFlowAmount  (bdAmount);
    	oCF.setAmountBase  	   (bdAmountBase);
    	oCF.setUserName  	   (_oAPP.getUserName());
    	oCF.setDescription     (oDesc.toString());  
		oCF.setTransactionId   (_oAPP.getApPaymentId());                     
		oCF.setTransactionNo   (_oAPP.getApPaymentNo());                    
		oCF.setTransactionType (i_CF_AP_PAYMENT);	
		oCF.setLocationId	   (_oAPP.getLocationId());
				
		boolean bReversed = checkAmount (oCF);
		setStatus (oCF);
		
		oCF.setJournalAmount   (oCF.getAmountBase());                    		
		oCF.setSayAmount       (CustomFormatter.numberToStr(PreferenceTool.getLocale(), dAmountBase));

		int iDebitCredit = debitCredit(oCF);
			
        List vCFD = new ArrayList();
        for (int i = 0; i < _vAPD.size(); i++)
        {
        	ApPaymentDetail oAPD = (ApPaymentDetail) _vAPD.get(i);

            boolean bAPTemp = false;
			Account oAP = null; 
			String sAPTemp = GLConfigTool.getGLConfig(_oConn).getApPaymentTemp();
			if (_bIsFuture && StringUtil.isNotEmpty(sAPTemp)) //use TEMP account
			{
				oAP = AccountTool.getAccountByID(sAPTemp, _oConn);
                bAPTemp = true;
			}
			else //use AP account
			{
				oAP = AccountTool.getAccountPayable(_oAPP.getCurrencyId(), _oAPP.getVendorId(), _oConn);
				if (oAPD.getTaxPayment())
				{
					oAP = AccountTool.getAccountPayable(CurrencyTool.getDefaultCurrency(_oConn).getCurrencyId(), "", _oConn);					
				}
			}
        	//Account oAPTemp = AccountTool.getAccountByID(GLConfigTool.getGLConfig(_oConn).getApPaymentTemp(), _oConn);
            //Account oAP = AccountTool.getAccountPayable(_oAPP.getCurrencyId(), _oAPP.getVendorId(), _oConn);
    		CashFlowJournal oCFD = new CashFlowJournal();
    		
    		Currency oBaseCurr = CurrencyTool.getDefaultCurrency(_oConn);
    		double dInvRate = oAPD.getInvoiceRate().doubleValue();
    		double dPayment = oAPD.getPaymentAmount().doubleValue();
            double dPaymentBase = dPayment * dInvRate;
            double dGainLoss = 0;
            
            if (dRate != dInvRate) dGainLoss = (dPayment * dRate) - (dPaymentBase);
			
            if (bReversed) 
			{
				dPayment = dPayment * -1;
				dPaymentBase = dPaymentBase * -1;
				dGainLoss = dGainLoss * -1;						
				log.debug("REVERSED PAYMENT : " + dPayment + " " + dPaymentBase);				
			}            

            oCFD.setCashFlowJournalId   (""); 
            oCFD.setCashFlowId          ("");
            oCFD.setAccountId           (oAP.getAccountId());   
            oCFD.setAccountCode         (oAP.getAccountCode());   
            oCFD.setAccountName         (oAP.getAccountName());   
            
            if (bAPTemp)
            {
                oCFD.setCurrencyId          (oBaseCurr.getCurrencyId());   
                oCFD.setCurrencyRate        (bd_ONE);   
                oCFD.setAmount              (new BigDecimal(dPaymentBase));   
                oCFD.setAmountBase          (new BigDecimal(dPaymentBase));                   
            }
            else
            {
                oCFD.setCurrencyId          (sCurrencyID);   
                oCFD.setCurrencyRate        (new BigDecimal(dInvRate));   
                oCFD.setAmount              (new BigDecimal(dPayment));   
                oCFD.setAmountBase          (new BigDecimal(dPaymentBase)); 
            }
            oCFD.setCashFlowTypeId      (_oAPP.getCashFlowTypeId());        
            oCFD.setLocationId          (_oAPP.getLocationId());        
            oCFD.setProjectId           ("");
            oCFD.setDepartmentId        ("");   
            oCFD.setMemo                (oDesc.toString());
            oCFD.setDebitCredit         (iDebitCredit); 
            oCFD.setSubLedgerId         (_oAPP.getEntityId());
            oCFD.setSubLedgerType       (i_SUB_LEDGER_AP);
            oCFD.setSubLedgerTransId    (_oAPP.getTransactionId());
            oCFD.setSubLedgerDesc       (_oAPP.getEntityName());
            vCFD.add (oCFD);

            //TODO: CHECK INVALID CODE 
            /*
            oCFD.setCashFlowJournalId   (""); 
            oCFD.setCashFlowId 		    ("");
            oCFD.setAccountId  	 		(oAP.getAccountId());   
            oCFD.setAccountCode 		(oAP.getAccountCode());   
            oCFD.setAccountName 		(oAP.getAccountName());   
            oCFD.setCurrencyId 		    (oBaseCurr.getCurrencyId());   
            oCFD.setCurrencyRate  	    (bd_ONE);   
            oCFD.setAmount	  		    (new BigDecimal(dPaymentBase));   
            oCFD.setAmountBase 	  	    (new BigDecimal(dPaymentBase));     
            oCFD.setCashFlowTypeId		(_oAPP.getCashFlowTypeId());        
            oCFD.setLocationId		    (_oAPP.getLocationId());        
            oCFD.setProjectId		    ("");
            oCFD.setDepartmentId 		("");   
            oCFD.setMemo                (oDesc.toString());
            oCFD.setDebitCredit  	    (iDebitCredit); 
            oCFD.setSubLedgerId			(_oAPP.getEntityId());
            oCFD.setSubLedgerType		(i_SUB_LEDGER_AP);
            oCFD.setSubLedgerTransId    (_oAPP.getTransactionId());
            oCFD.setSubLedgerDesc		(_oAPP.getEntityName());
            vCFD.add (oCFD);
            */
            
            if (dGainLoss != 0)
            {
            	log.debug("GAIN LOSS :  " +  dGainLoss);
            	Account oAPGL = AccountTool.getRealUnrealGainLoss(_oAPP.getCurrencyId(), true, _oConn);
                CashFlowJournal oGainLoss = new CashFlowJournal();

                oGainLoss.setCashFlowJournalId  (""); 
                oGainLoss.setCashFlowId 		("");
                oGainLoss.setAccountId  	 	(oAPGL.getAccountId());   
                oGainLoss.setAccountCode 		(oAPGL.getAccountCode());   
                oGainLoss.setAccountName 		(oAPGL.getAccountName());   
                oGainLoss.setCurrencyId 		(CurrencyTool.getDefaultCurrency(_oConn).getCurrencyId());   
                oGainLoss.setCurrencyRate  	    (bd_ONE);   
                oGainLoss.setAmount	  		    (new BigDecimal(dGainLoss));   
                oGainLoss.setAmountBase 	  	(new BigDecimal(dGainLoss));     
                oGainLoss.setCashFlowTypeId		(_oAPP.getCashFlowTypeId());        
                oGainLoss.setLocationId		    (_oAPP.getLocationId());        
                oGainLoss.setProjectId		    ("");
                oGainLoss.setDepartmentId 		("");   
                oGainLoss.setMemo               (oDesc.toString());
                oGainLoss.setDebitCredit  	    (iDebitCredit);                 
                oGainLoss.setSubLedgerId		(_oAPP.getEntityId());
                oGainLoss.setSubLedgerType		(i_SUB_LEDGER_AP);
                oGainLoss.setSubLedgerTransId   (_oAPP.getTransactionId());
                oGainLoss.setSubLedgerDesc		(_oAPP.getEntityName());                
                vCFD.add (oGainLoss);
            }
            
    		double dDiscAmt  = oAPD.getDiscountAmount().doubleValue();
        	String sDiscAccID = oAPD.getDiscountAccountId();
        	if (dDiscAmt != 0 && StringUtil.isNotEmpty(sDiscAccID))
      		{
                Account oAPDisc = AccountTool.getAccountByID(sDiscAccID);
                CashFlowJournal oDisc = new CashFlowJournal();
                
        		dDiscAmt = dDiscAmt * -1; //minus the discount towards AP
        		double dDiscBase = dDiscAmt * dRate;
				if (bReversed)
				{
					dDiscAmt = dDiscAmt * -1; 
					dDiscBase = dDiscBase * -1;
				}
				
                oDisc.setCashFlowJournalId  (""); 
                oDisc.setCashFlowId 		("");
                oDisc.setAccountId  	 	(oAPDisc.getAccountId());   
                oDisc.setAccountCode 		(oAPDisc.getAccountCode());   
                oDisc.setAccountName 		(oAPDisc.getAccountName());   
                oDisc.setCurrencyId 		(_oAPP.getCurrencyId());   
                oDisc.setCurrencyRate  	    (_oAPP.getCurrencyRate());   
                oDisc.setAmount	  		    (new BigDecimal(dDiscAmt));   
                oDisc.setAmountBase 	  	(new BigDecimal(dDiscBase));  
                oDisc.setCashFlowTypeId		(_oAPP.getCashFlowTypeId());    
                oDisc.setLocationId		    (_oAPP.getLocationId());        
                oDisc.setProjectId		    (oAPD.getProjectId());
                oDisc.setDepartmentId 		(oAPD.getDepartmentId());   
                oDisc.setMemo               (oDesc.toString());
                oDisc.setDebitCredit  	    (iDebitCredit);  	
                oDisc.setSubLedgerId		(_oAPP.getEntityId());
                oDisc.setSubLedgerType		(i_SUB_LEDGER_AP);
                oDisc.setSubLedgerTransId   (_oAPP.getTransactionId());
                oDisc.setSubLedgerDesc		(_oAPP.getEntityName());                
                vCFD.add (oDisc);
        	}
        }        
    	saveData (oCF, vCFD, true, _oConn);
    	return oCF;
	}
	
	/**
	 * Create CashFlow Data from Debit Memo
	 * 
	 * @param _oData
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createFromDebitMemo (DebitMemo _oData, boolean _bCashReturn, Connection _oConn)
		throws Exception
	{		
	    StringBuilder oDesc = new StringBuilder ();
		if (StringUtil.isNotEmpty(_oData.getRemark()))
		{
			oDesc.append (_oData.getRemark());	
			oDesc.append("\n");
		}
	    oDesc.append ("Debit Memo To : ");
	    oDesc.append (_oData.getVendorName());
	    oDesc.append (" (");
	    oDesc.append (_oData.getDebitMemoNo());
	    oDesc.append (") ");

	    int iCFType = i_WITHDRAWAL;
	    if (_bCashReturn)
	    {
	    	iCFType = i_DEPOSIT;
	    }

	    String sCurrencyID = _oData.getCurrencyId();	    
	    BigDecimal bdAmount = _oData.getAmount();
	    BigDecimal bdRate = _oData.getCurrencyRate();
	    
		double dRate = bdRate.doubleValue();
    	double dAmountBase = bdAmount.doubleValue() * dRate;
    	BigDecimal bdAmountBase = new BigDecimal (dAmountBase);

	    Bank oBank = BankTool.getBankByID(_oData.getBankId(), _oConn);
	    Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);
	    if (!oBankCurr.getCurrencyId().equals(_oData.getCurrencyId()) && oBankCurr.getIsDefault())
	    {
		    sCurrencyID = oBankCurr.getCurrencyId();
		    bdAmount = bdAmountBase;
		    bdRate = bd_ONE;
	    }
	    
	    boolean bCross = false;
	    String sCrossAccID = _oData.getCrossAccountId();
	    Account oCrossAcc = AccountTool.getAccountByID(sCrossAccID,_oConn);
	    if (StringUtil.isNotEmpty(sCrossAccID) && oCrossAcc != null)
	    {
	    	bdAmount = bd_ZERO;
	    	bdAmountBase = bd_ZERO;	    	
	    	bCross = true;
	    }
	    
		CashFlow oCF = new CashFlow();
		oCF.setCashFlowType    (iCFType);
		oCF.setBankId  	 	   (_oData.getBankId());
		oCF.setBankIssuer  	   (_oData.getBankIssuer());
		oCF.setReferenceNo     (_oData.getReferenceNo());
		oCF.setCurrencyId  	   (sCurrencyID);
		oCF.setCurrencyRate    (bdRate);
		oCF.setExpectedDate    (_oData.getTransactionDate());
		oCF.setDueDate  	   (_oData.getDueDate());
		oCF.setCashFlowAmount  (bdAmount);
		oCF.setAmountBase  	   (bdAmountBase);
		oCF.setUserName  	   (_oData.getUserName());
		oCF.setDescription     (oDesc.toString());  	 
		oCF.setTransactionId   (_oData.getDebitMemoId());                     
		oCF.setTransactionNo   (_oData.getDebitMemoNo());                    
		oCF.setTransactionType (i_CF_DEBIT_MEMO);	    	
		oCF.setLocationId	   (_oData.getLocationId());
				
		boolean bReversed  = checkAmount(oCF);
		setStatus(oCF);
				
		oCF.setSayAmount       ("");                    
		oCF.setJournalAmount   (oCF.getAmountBase());                    
		
		Account oDM = AccountTool.getAccountByID(_oData.getAccountId(), _oConn);
		List vCFD = new ArrayList(1);
			    
		CashFlowJournal oCFD = new CashFlowJournal();	    
		oCFD.setCashFlowJournalId   (""); 
	    oCFD.setCashFlowId 		    ("");
	    oCFD.setAccountId  	 		(oDM.getAccountId());   
	    oCFD.setAccountCode 		(oDM.getAccountCode());   
	    oCFD.setAccountName 		(oDM.getAccountName());   
	    oCFD.setCurrencyId 		    (_oData.getCurrencyId());   
	    oCFD.setCurrencyRate  	    (_oData.getCurrencyRate());
	    
	    if (bReversed)
	    {
		    oCFD.setAmount	  		(new BigDecimal(Calculator.mul(_oData.getAmount(), -1)));   
		    oCFD.setAmountBase 	  	(new BigDecimal(Calculator.mul(_oData.getAmountBase(), -1)));  	       	    	
	    }
	    else
	    {
		    oCFD.setAmount	  		(_oData.getAmount());   
		    oCFD.setAmountBase 	  	(_oData.getAmountBase());  	       
		}
	    
	    oCFD.setCashFlowTypeId		(_oData.getCashFlowTypeId());    
	    oCFD.setLocationId		    (_oData.getLocationId());	    
	    oCFD.setProjectId		    ("");
	    oCFD.setDepartmentId 		("");   
	    oCFD.setMemo                (oDesc.toString());
	    oCFD.setDebitCredit  	    (debitCredit(oCF)); 	
	    
	    oCFD.setSubLedgerId			(_oData.getEntityId());
	    oCFD.setSubLedgerType		(i_SUB_LEDGER_AP);
	    oCFD.setSubLedgerTransId	(_oData.getMemoId());
	    oCFD.setSubLedgerDesc		(_oData.getEntityName());

	    vCFD.add (oCFD);

	    if (StringUtil.isNotEmpty(sCrossAccID) && oCrossAcc != null) //cross account always in base currency
	    {
	    	BigDecimal bdCross 	  = new BigDecimal(_oData.getAmountBase().doubleValue() * -1);
	    	BigDecimal bdCrossBase = new BigDecimal(_oData.getAmountBase().doubleValue() * -1);
	    	
			CashFlowJournal oCross = new CashFlowJournal();	    
			oCross.setCashFlowJournalId (""); 
		    oCross.setCashFlowId 		("");
		    oCross.setAccountId  	 	(oCrossAcc.getAccountId());   
		    oCross.setAccountCode 		(oCrossAcc.getAccountCode());   
		    oCross.setAccountName 		(oCrossAcc.getAccountName());   
		    oCross.setCurrencyId 		(CurrencyTool.getDefaultCurrency().getId());   
		    oCross.setCurrencyRate  	(bd_ONE);   
		    oCross.setAmount	  		(bdCross);   
		    oCross.setAmountBase 	  	(bdCrossBase);    
		    oCross.setCashFlowTypeId	(_oData.getCashFlowTypeId());
		    oCross.setLocationId		(_oData.getLocationId());
		    oCross.setProjectId		    ("");
		    oCross.setDepartmentId 		("");   
		    oCross.setMemo              (oDesc.toString());
		    oCross.setDebitCredit  	    (debitCredit(oCF)); 	
		    
		    oCross.setSubLedgerId		(_oData.getEntityId());
		    oCross.setSubLedgerType		(i_SUB_LEDGER_AP);
		    oCross.setSubLedgerTransId	(_oData.getMemoId());
		    oCross.setSubLedgerDesc		(_oData.getEntityName());
		    vCFD.add (oCross);
	    }
	    
		//log.debug("create from debit memo 2 CF : " + oCF + " vCFD : \n" +  vCFD);
		saveData (oCF, vCFD, true, _oConn);    	
	}

    public static CashFlow createFromTransfer (BankTransfer _oData, int _iType, Connection _oConn)
        throws Exception
    {
        StringBuilder oDesc = new StringBuilder ();
        if (StringUtil.isNotEmpty(_oData.getDescription()))
        {
            oDesc.append (_oData.getDescription());  
            oDesc.append("\n");
        }
        
        String sCurrencyID = _oData.getCurrencyId();        
        BigDecimal bdAmount = _oData.getAmount();
        BigDecimal bdRate = _oData.getCurrencyRate();
        
        double dRate = bdRate.doubleValue();
        double dAmountBase = bdAmount.doubleValue() * dRate;
        BigDecimal bdAmountBase = new BigDecimal (dAmountBase);
    
        int iCFType = _iType;        
        String sBankID = _oData.getOutBankId();
        String sAccID = _oData.getOutAccId();
        String sLocID = _oData.getOutLocId();
        String sCFTID = _oData.getOutCftId();
        if (_iType == i_DEPOSIT) 
        {
            sBankID = _oData.getInBankId();
            sAccID = _oData.getInAccId();
            sLocID = _oData.getInLocId();
            sCFTID = _oData.getInCftId();    
        }
        
        Bank oBank = BankTool.getBankByID(sBankID, _oConn);
        Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);
        if (!oBankCurr.getCurrencyId().equals(_oData.getCurrencyId()) && oBankCurr.getIsDefault())
        {
            sCurrencyID = oBankCurr.getCurrencyId();
            bdAmount = bdAmountBase;
            bdRate = bd_ONE;
        }        
        Account oAcc = AccountTool.getAccountByID(sAccID,_oConn);
        Location oLoc = LocationTool.getLocationByID(sLocID,_oConn);        
        
        CashFlow oCF = new CashFlow();
        oCF.setCashFlowType    (iCFType);
        oCF.setBankId          (oBank.getBankId());
        oCF.setBankIssuer      ("Transfer");
        oCF.setReferenceNo     (_oData.getTransNo());
        oCF.setCurrencyId      (sCurrencyID);
        oCF.setCurrencyRate    (bdRate);
        oCF.setExpectedDate    (_oData.getTransDate());
        oCF.setDueDate         (_oData.getTransDate());
        oCF.setCashFlowAmount  (bdAmount);
        oCF.setAmountBase      (bdAmountBase);
        oCF.setUserName        (_oData.getUserName());
        oCF.setDescription     (oDesc.toString());       
        oCF.setTransactionId   (_oData.getTransactionId());                     
        oCF.setTransactionNo   (_oData.getTransactionNo());                    
        oCF.setTransactionType (i_CF_BANK_TRANSFER);          
        oCF.setLocationId      (oLoc.getLocationId());
        
        checkAmount (oCF);
        setStatus (oCF);
        
        oCF.setSayAmount       ("");                    
        oCF.setJournalAmount   (oCF.getAmountBase());     
        log.debug (oCF);
        
        List vCFD = new ArrayList(1);        
        CashFlowJournal oCFD = new CashFlowJournal();       
        oCFD.setCashFlowJournalId   (""); 
        oCFD.setCashFlowId          ("");
        oCFD.setAccountId           (oAcc.getAccountId());   
        oCFD.setAccountCode         (oAcc.getAccountCode());   
        oCFD.setAccountName         (oAcc.getAccountName());   
        oCFD.setCurrencyId          (sCurrencyID);   
        oCFD.setCurrencyRate        (bdRate);   
        oCFD.setAmount              (bdAmount);   
        oCFD.setAmountBase          (bdAmountBase);    
        oCFD.setCashFlowTypeId      (sCFTID);
        oCFD.setLocationId          (oLoc.getLocationId());
        oCFD.setProjectId           ("");
        oCFD.setDepartmentId        ("");   
        oCFD.setMemo                ("");
        oCFD.setDebitCredit         (debitCredit(oCF));     
        
        vCFD.add (oCFD);        
        saveData (oCF, vCFD, true, _oConn);    
        return oCF;
    }
        
	//-------------------------------------------------------------------------
	//finder
	//-------------------------------------------------------------------------
	private static Criteria buildCriteria(int _iCond, 
										  String _sKeywords, 
										  String _sBankID,
										  String _sLocationID,
										  int _iCashFlowType, 
										  int _iStatus,
										  int _iTransType,
										  Date _dStart, 
										  Date _dEnd, 
										  Date _dStartDue, 
										  Date _dEndDue,
										  String _sCurrencyID) 
	throws Exception
	{
		Criteria oCrit =  buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
				CashFlowPeer.EXPECTED_DATE, _dStart, _dEnd, _sCurrencyID);
		
		if (StringUtil.isNotEmpty(_sBankID))
		{
			oCrit.add(CashFlowPeer.BANK_ID, _sBankID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(CashFlowPeer.LOCATION_ID, _sLocationID);
		}		
		if (_iCashFlowType != 0)
		{
			oCrit.add(CashFlowPeer.CASH_FLOW_TYPE, _iCashFlowType);
		}
		if (_iStatus != 0)
		{
			oCrit.add(CashFlowPeer.STATUS, _iStatus);
		}		
		if (_iTransType > 0)
		{
			oCrit.add(CashFlowPeer.TRANSACTION_TYPE, _iTransType);
		}		
		if (_dStartDue != null)
		{
			oCrit.add(CashFlowPeer.DUE_DATE, _dStartDue, Criteria.GREATER_EQUAL);
		}
		if (_dEndDue != null)
		{
			oCrit.and(CashFlowPeer.DUE_DATE, DateUtil.getEndOfDayDate(_dEndDue), Criteria.LESS_EQUAL);
		}
		oCrit.addAscendingOrderByColumn (CashFlowPeer.EXPECTED_DATE);
		return oCrit;
	}
	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _sBankID
	 * @param _iCashFlowType
	 * @param _iStatus
	 * @param _dStart
	 * @param _dEnd
	 * @param _dStartDue
	 * @param _dEndDue
	 * @param _iLimit
	 * @return
	 * @throws Exception
	 */
	public static List findTrans(int _iCond, 
							     String _sKeywords, 
							     String _sBankID, 
							     String _sLocationID,
							     int _iCashFlowType, 
							     int _iStatus,
							     int _iTransType,
							     Date _dStart, 
							     Date _dEnd,
							     Date _dStartDue, 
							     Date _dEndDue)
    	throws Exception
    {
		Criteria oCrit = buildCriteria(_iCond, _sKeywords, _sBankID, _sLocationID, _iCashFlowType, 
									   _iStatus, _iTransType, _dStart, _dEnd, _dStartDue, _dEndDue, "");
		return CashFlowPeer.doSelect(oCrit);
	}
	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _sBankID
	 * @param _iCashFlowType
	 * @param _iStatus
	 * @param _dStart
	 * @param _dEnd
	 * @param _dStartDue
	 * @param _dEndDue
	 * @param _iLimit
	 * @return
	 * @throws Exception
	 */
	public static LargeSelect findData(int _iCond, 
									   String _sKeywords, 
									   String _sBankID, 
									   String _sLocationID,
									   int _iCashFlowType, 
									   int _iStatus,
									   int _iTransType,
									   Date _dStart, 
									   Date _dEnd,
									   Date _dStartDue, 
									   Date _dEndDue,
									   int _iLimit)
    	throws Exception
    {
		Criteria oCrit = buildCriteria(_iCond, _sKeywords, _sBankID, _sLocationID, _iCashFlowType, 
									   _iStatus, _iTransType, _dStart, _dEnd, _dStartDue, _dEndDue, "");
		return new LargeSelect(oCrit,_iLimit,"com.ssti.enterprise.pos.om.CashFlowPeer");
	}

	/**
	 * find cash flow journal
	 * 
	 * @param _sCashFlowTypeID
	 * @param _sCurrencyID
	 * @param _sBankID
	 * @param _iCashFlowType
	 * @param _dFrom
	 * @param _dTo
	 * @return
	 * @throws Exception
	 */
    public static List getCashFlowJournal(String _sCashFlowTypeID, 
    									  String _sCurrencyID, 
    									  String _sBankID, 
    									  Integer _iCashFlowType, 
    									  Date _dFrom, 
    									  Date _dTo)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        
        oCrit.addJoin (CashFlowJournalPeer.CASH_FLOW_ID, CashFlowPeer.CASH_FLOW_ID);
        oCrit.add (CashFlowPeer.STATUS, i_PAYMENT_PROCESSED);
        oCrit.add (CashFlowPeer.CASH_FLOW_TYPE, _iCashFlowType);
        oCrit.addAscendingOrderByColumn(CashFlowPeer.DUE_DATE);
        
        if (StringUtil.isNotEmpty(_sCashFlowTypeID))
        {
        	oCrit.add (CashFlowJournalPeer.CASH_FLOW_TYPE_ID, _sCashFlowTypeID);
        }
        if (StringUtil.isNotEmpty(_sCurrencyID))
        {
            oCrit.add (CashFlowJournalPeer.CURRENCY_ID, _sCurrencyID);
        }
        if (StringUtil.isNotEmpty(_sBankID))
        {
            oCrit.add (CashFlowPeer.BANK_ID, _sBankID);
        }
        if (_dFrom != null)
        {
            oCrit.add(CashFlowPeer.EXPECTED_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
        }
        if (_dTo != null)
        {
            oCrit.and(CashFlowPeer.EXPECTED_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
        }           
        return CashFlowJournalPeer.doSelect(oCrit);
    }

    public static double filterTotalAmountByType(List _vData, int _iType)
    	throws Exception
    {
        double dTotal = 0;
        for (int i = 0; i < _vData.size(); i++)
        {
            CashFlow oCash = (CashFlow)_vData.get(i);
            if (oCash != null)
            {
                if (oCash.getCashFlowType() == _iType)
                {
                    dTotal = dTotal + oCash.getCashFlowAmount().doubleValue();
                }
            }
        }
        return dTotal;
	}	

	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(CashFlowPeer.class, CashFlowPeer.CASH_FLOW_ID, _sID, _bIsNext);
	}	

	public static boolean validateJournal (CashFlow _oCF) 
    	throws Exception
    {
        double dTotalAmount = Calculator.precise(_oCF.getAmountBase(), 4);
        double dJournalAmount = Calculator.precise(_oCF.getJournalAmount(), 4);
        if (dTotalAmount != dJournalAmount)
        {   
            if (!Calculator.isInTolerance(dTotalAmount, dJournalAmount, GlAttributes.d_UNBALANCE_TOLERANCE))
            {
	        	StringBuilder oError = new StringBuilder ("Unbalanced Transaction : ");
	            oError.append (" Cash Flow Amount (");
	            oError.append (CustomFormatter.formatNumber(new BigDecimal (dTotalAmount)));
	            oError.append (") ");
	            
	            if (dTotalAmount < dJournalAmount) oError.append ("<");
	            if (dTotalAmount > dJournalAmount) oError.append (">");
	
	            oError.append (" Total Details (");
	            oError.append (CustomFormatter.formatNumber(new BigDecimal (dJournalAmount)));
	            oError.append (")");
	            throw new NestableException (oError.toString());
            }            
        }
        return true;
    }
	
	public static String getStatusString (int _iStatus)
	{
		if (_iStatus == i_PAYMENT_RECEIVED)  return LocaleTool.getString("open");
		if (_iStatus == i_PAYMENT_PROCESSED) return LocaleTool.getString("processed");
		if (_iStatus == i_PAYMENT_CANCELLED) return LocaleTool.getString("cancelled");
		return "";
	}
	
	//-------------------------------------------------------------------------
	// CASH FLOW TYPE
	//-------------------------------------------------------------------------
    
    public static CashFlowTypeTool getTypeTool()
        throws Exception
    {
        return CashFlowTypeTool.getInstance();
    }    
    
    @Deprecated
	public static List getAllType()
		throws Exception
	{
		return CashFlowTypeTool.getAllType();
	}
    
    @Deprecated	
	public static CashFlowType getTypeByID(String _sID, Connection _oConn)
		throws Exception
	{
        return CashFlowTypeTool.getTypeByID(_sID,_oConn);
	}

    @Deprecated
	public static CashFlowType getTypeByCode(String _sCode, Connection _oConn)
		throws Exception
	{
        return CashFlowTypeTool.getTypeByCode(_sCode,_oConn);
	}
	
    @Deprecated
	public static String getTypeCodeByID(String _sID, Connection _oConn)
		throws Exception
	{
        return CashFlowTypeTool.getTypeCodeByID(_sID,_oConn);
	}

	//-------------------------------------------------------------------------
	// CASH FLOW REPORTS
	//-------------------------------------------------------------------------
    public static List getCashFlowByBankID(String _sBankID, Date _dFrom, Date _dTo)
    	throws Exception
    {
		return getCashFlowByBankID(_sBankID, _dFrom, _dTo, null, null);
	}

    public static List getCashFlowByBankID(String _sBankID, 
    		                               Date _dFrom, 
    									   Date _dTo, 
    									   Date _dDueFrom, 
    									   Date _dDueTo)
    		throws Exception
    {
    	Criteria oCrit = new Criteria();

    	oCrit.add (CashFlowPeer.BANK_ID, _sBankID);
    	oCrit.add (CashFlowPeer.STATUS, i_PAYMENT_PROCESSED);
    	oCrit.addAscendingOrderByColumn(CashFlowPeer.DUE_DATE);
    	oCrit.addAscendingOrderByColumn(CashFlowPeer.TRANSACTION_NO);
    	if (_dFrom != null)
    	{
    		oCrit.add(CashFlowPeer.EXPECTED_DATE, _dFrom, Criteria.GREATER_EQUAL);
    	}
    	if (_dTo != null)
    	{
    		oCrit.and(CashFlowPeer.EXPECTED_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
    	}        	
    	if (_dDueFrom != null)
    	{
    		oCrit.add(CashFlowPeer.DUE_DATE, _dDueFrom, Criteria.GREATER_EQUAL);
    	}
    	if (_dDueTo != null)
    	{
    		oCrit.and(CashFlowPeer.DUE_DATE, DateUtil.getEndOfDayDate(_dDueTo), Criteria.LESS_EQUAL);
    	}        	
    	
    	return CashFlowPeer.doSelect(oCrit);
    }
    
    public static List getCashFlowJournalByTypeID(String _sCFTypeID, 
    											  String _sCurrencyID, 
    											  String _sBankID, 
    											  Integer _iCFType, 
    											  Date _dFrom, 
    											  Date _dTo)
        throws Exception
    {
        CashFlowType oCFT = CashFlowTypeTool.getTypeByID(_sCFTypeID,null);
                
        Criteria oCrit = new Criteria();        
        oCrit.addJoin (CashFlowJournalPeer.CASH_FLOW_ID, CashFlowPeer.CASH_FLOW_ID);
        if (oCFT != null && oCFT.getHasChild())
        {
            List vChild = CashFlowTypeTool.getChildIDList(oCFT.getId());
            oCrit.addIn(CashFlowJournalPeer.CASH_FLOW_TYPE_ID, vChild);
        }
        else
        {
            oCrit.add(CashFlowJournalPeer.CASH_FLOW_TYPE_ID, _sCFTypeID);
        }
        oCrit.add (CashFlowPeer.STATUS, i_PAYMENT_PROCESSED);
        oCrit.add (CashFlowPeer.CASH_FLOW_TYPE, _iCFType);
        oCrit.addAscendingOrderByColumn(CashFlowPeer.DUE_DATE);
        oCrit.addAscendingOrderByColumn(CashFlowPeer.TRANSACTION_NO);
        if (_sCurrencyID != null)
        {
            oCrit.add (CashFlowJournalPeer.CURRENCY_ID, _sCurrencyID);
        }
        if (_sBankID != null)
        {
            oCrit.add (CashFlowPeer.BANK_ID, _sBankID);
        }
        if (_dFrom != null)
        {
            oCrit.add(CashFlowPeer.EXPECTED_DATE, _dFrom, Criteria.GREATER_EQUAL);
        }
        if (_dTo != null)
        {
            oCrit.and(CashFlowPeer.EXPECTED_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
        }           
        return CashFlowJournalPeer.doSelect(oCrit);
    }	

    public static double getCFBalance (String _sCFTypeID, 
							           String _sCurrencyID, 
							           String _sBankID, 
							           Integer _iCFType, 
							           Date _dFrom, 
							           Date _dTo)
		throws Exception
	{
    	return getCFBalance(_sCFTypeID, _sCurrencyID, _sBankID, _iCFType, _dFrom, _dTo, false);
	}

    
    public static double getCFBalance (String _sCFTypeID, 
                                       String _sCurrencyID, 
                                       String _sBankID, 
                                       Integer _iCFType, 
                                       Date _dFrom, 
                                       Date _dTo,
                                       boolean _bBase) 
        throws Exception
    {         
        StringBuilder oSQL = new StringBuilder();
        oSQL.append (" SELECT ");
        if(!_bBase) oSQL.append (" SUM(cfj.AMOUNT) ");
        else oSQL.append (" SUM(cfj.AMOUNT_BASE) ");

        oSQL.append (" FROM cash_flow_journal cfj, cash_flow cf ");
        oSQL.append (" WHERE cf.CASH_FLOW_ID = cfj.CASH_FLOW_ID ");
        oSQL.append (" AND cf.STATUS = ").append(i_PROCESSED);        
        oSQL.append (" AND cf.CASH_FLOW_TYPE = ").append(_iCFType);
        if(StringUtil.isNotEmpty(_sCurrencyID))
        {
            oSQL.append (" AND cf.CURRENCY_ID = '").append(_sCurrencyID).append("' ");
        }
        if(StringUtil.isNotEmpty(_sBankID))
        {
            oSQL.append (" AND cf.BANK_ID = '").append(_sBankID).append("' ");            
        }
        if(_dFrom != null)
        {
            oSQL.append (" AND cf.EXPECTED_DATE >= ");
            oSQL.append (oDB.getDateString(DateUtil.getStartOfDayDate(_dFrom)));
        }
        if(_dTo != null)
        {
            oSQL.append (" AND cf.EXPECTED_DATE <= ");
            oSQL.append (oDB.getDateString(DateUtil.getEndOfDayDate(_dTo)));
        }        

        CashFlowType oCFT = CashFlowTypeTool.getTypeByID(_sCFTypeID,null);
        if (oCFT != null && oCFT.getHasChild())
        {
            List vChild = CashFlowTypeTool.getChildIDList(oCFT.getId());
            oSQL.append (" AND cfj.CASH_FLOW_TYPE_ID IN ");
            oSQL.append (SqlUtil.convertToINMode(vChild));
        }
        else
        {
            oSQL.append (" AND cfj.CASH_FLOW_TYPE_ID = '").append(_sCFTypeID).append("' ");
        }        
        log.debug (oSQL.toString());
        
        List vChanges = CashFlowJournalPeer.executeQuery(oSQL.toString());
        if (vChanges.size() > 0)
        {
            Record oData = (Record)vChanges.get(0);
            return oData.getValue(1).asDouble();                       
        }
        return 0;
    }
    
    //-------------------------------------------------------------------------
    // RECONCILE
    //-------------------------------------------------------------------------
    
    public static List getForReconcile(String _sBankID, 
    								   Date _dFrom, 
    								   Date _dTo, 
    								   Boolean _bReconciled)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		
		oCrit.add (CashFlowPeer.BANK_ID, _sBankID);
		oCrit.add (CashFlowPeer.STATUS, i_PAYMENT_PROCESSED);
		oCrit.addAscendingOrderByColumn(CashFlowPeer.DUE_DATE);
		
		if (_bReconciled != null)
		{
			oCrit.add(CashFlowPeer.RECONCILED, _bReconciled.booleanValue());
		}		
		if (_dFrom != null)
		{
			oCrit.add(CashFlowPeer.DUE_DATE, _dFrom, Criteria.GREATER_EQUAL);
		}
		if (_dTo != null)
		{
			oCrit.and(CashFlowPeer.DUE_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		}        	
	    return CashFlowPeer.doSelect(oCrit);
	}
    
    //-------------------------------------------------------------------------
    // SOURCE TRANS
    //-------------------------------------------------------------------------
    
	public static final String getTransactionTypeName (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);		
		if (iType == i_CF_AR_PAYMENT) return s_TRANS_AR_PAYMENT;
		if (iType == i_CF_AP_PAYMENT) return s_TRANS_AP_PAYMENT;
		if (iType == i_CF_CREDIT_MEMO) return s_TRANS_AR_MEMO;
		if (iType == i_CF_DEBIT_MEMO) return s_TRANS_AP_MEMO;
        if (iType == i_CF_BANK_TRANSFER) return s_TRANS_BANK_TRANSFER;
        return "";
	}
	
	public static final String getTransactionScreen (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);
		if (iType == i_CF_AR_PAYMENT) return s_SCREEN_AR_PAYMENT;
		if (iType == i_CF_AP_PAYMENT) return s_SCREEN_AP_PAYMENT;
		if (iType == i_CF_CREDIT_MEMO) return s_SCREEN_CREDIT_MEMO;
		if (iType == i_CF_DEBIT_MEMO) return s_SCREEN_DEBIT_MEMO;
        if (iType == i_CF_BANK_TRANSFER) return s_SCREEN_BANK_TRANSFER;
        return "";	
	}	
    
    //-------------------------------------------------------------------------
    // ALT CODE
    //-------------------------------------------------------------------------
    
    /**
     * get Accumulated Account Balance by Alt CODE
     * 
     * @param _sAltCode
     * @param _dFrom
     * @param _dTo
     * @return balance amount
     * @throws Exception
     */
    public double getBalanceByAltCode (String _sAltCode, Date _dFrom, Date _dTo) 
        throws Exception
    {        
        double dTotal = 0;
        if (StringUtil.isNotEmpty(_sAltCode))
        {
            Currency oCurr = CurrencyTool.getDefaultCurrency();            
            String sCurrID = oCurr.getCurrencyId();
            if(_dFrom == null) _dFrom = PreferenceTool.getStartDate();
            if (_dTo == null) _dTo = new Date();            
            List vAlt = CashFlowTypeTool.getByAltCode(_sAltCode);
            for (int i = 0; i < vAlt.size(); i++)
            {                
                CashFlowType oType = (CashFlowType) vAlt.get(i);
                double dIn = getCFBalance(oType.getId(),sCurrID,"",1,_dFrom,_dTo);
                double dOut = getCFBalance(oType.getId(),sCurrID,"",2,_dFrom,_dTo);
                double dBal = 0; 
                if (oType.getDefaultType() == 2)
                {
                    dBal = dOut - dIn;
                }
                else
                {
                    dBal = dIn - dOut;                    
                }
                dTotal = dTotal + dBal; 

                log.debug("CODE: " + _sAltCode + " CFT: " + oType.getCashFlowTypeCode() + " BAL: " + CustomFormatter.fmt(dBal));
            }
        }
        return dTotal;
    }      
    
    //-------------------------------------------------------------------------
    //SYNC
    //-------------------------------------------------------------------------
    
    /**
     * get cash flow created in store
     * 
     * @return transaction created in store since last store 2 ho
     * @throws Exception
     */
    public static List getStoreTrans()
        throws Exception
    {
        Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
        Criteria oCrit = new Criteria();
        if (dLastSyncDate  != null) 
        {
            oCrit.add(CashFlowPeer.DUE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
        }
        //only get NORMAL CF transfer
        oCrit.add(CashFlowPeer.TRANSACTION_TYPE, i_CF_NORMAL);
        oCrit.add(CashFlowPeer.LOCATION_ID, PreferenceTool.getLocationID());       
        oCrit.add(CashFlowPeer.STATUS, i_PROCESSED);
        return CashFlowPeer.doSelect(oCrit);
    }

    //-------------------------------------------------------------------------
    //AUTO GENERATE REF NO
    //-------------------------------------------------------------------------
    public static String generateRefNo(String _sPrefix, String _sBankID, int _iLength)
    {
        if (_sPrefix == null) _sPrefix = ""; 
        Criteria oCrit = new Criteria();
        if (StringUtil.isNotEmpty(_sPrefix))
        {
            oCrit.add(CashFlowPeer.REFERENCE_NO, (Object)(_sPrefix + "%"), Criteria.LIKE);
        }
        if (StringUtil.isNotEmpty(_sBankID))
        {
            oCrit.add(CashFlowPeer.BANK_ID, _sBankID);
            oCrit.add(CashFlowPeer.CASH_FLOW_TYPE, i_WITHDRAWAL);
        }
        oCrit.addDescendingOrderByColumn(CashFlowPeer.REFERENCE_NO);
        oCrit.setLimit(1);
        
        String sLastCode = "";
        String sNewCode = "";
        List vData;
        try 
        {
            vData = (List) CashFlowPeer.doSelect(oCrit);
            if (vData.size() > 0) 
            {
                sLastCode = (String)vData.get(0);
                log.debug("Last Reference No " + sLastCode);
            }
            int iLastNumber = 0;        
            if (StringUtil.isNotEmpty(sLastCode))
            {
                int iPrefLen = _sPrefix.length();
                String sNumber = sLastCode.substring(iPrefLen, sLastCode.length());
                iLastNumber = Integer.parseInt(sNumber)+ 1;
            }
            else
            {
                iLastNumber = 1;            
            }
            sNewCode = _sPrefix + StringUtil.formatNumberString(Integer.toString(iLastNumber), _iLength);
        } 
        catch (Exception _oEx) 
        {
            log.error("ERROR GENERATING CODE : " + _oEx.getMessage(), _oEx);
            _oEx.printStackTrace();
        }
        return sNewCode;
    }
}
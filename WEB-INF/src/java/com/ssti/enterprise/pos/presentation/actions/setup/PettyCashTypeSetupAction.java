package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.PettyCashTypeLoader;
import com.ssti.enterprise.pos.manager.PettyCashManager;
import com.ssti.enterprise.pos.om.PettyCashType;
import com.ssti.enterprise.pos.om.PettyCashTypePeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PettyCashTypeTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class PettyCashTypeSetupAction extends BankSetupAction
{
	private static final Log log = LogFactory.getLog(PettyCashTypeSetupAction.class);
	
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			PettyCashType oPettyCashType = new PettyCashType();
			setProperties (oPettyCashType, data);
			oPettyCashType.setPettyCashTypeId (IDGenerator.generateSysID());
	        oPettyCashType.save();
	        PettyCashManager.getInstance().refreshCache(oPettyCashType);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			PettyCashType oPCT = PettyCashTypeTool.getPettyCashTypeByID(data.getParameters().getString("ID"));
			PettyCashType oOld = oPCT.copy();
			setProperties (oPCT, data);
	        oPCT.save();
	        PettyCashManager.getInstance().refreshCache(oPCT);
            UpdateHistoryTool.createHistory(oOld, oPCT, data.getUser().getName(), null);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("petty_cash","petty_cash_type_id",sID))
    		{
    			PettyCashType oPCT = PettyCashTypeTool.getPettyCashTypeByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(PettyCashTypePeer.PETTY_CASH_TYPE_ID, sID);
	        	PettyCashTypePeer.doDelete(oCrit);
	        	PettyCashManager.getInstance().refreshCache(sID);
	        	UpdateHistoryTool.createDelHistory(oPCT, data.getUser().getName(), null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));        		
    		}
        	else
        	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
        }
    }

    private void setProperties (PettyCashType _oPettyCashType, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oPettyCashType);
	    	//boolean bIsOpenDefault = data.getParameters().getBoolean("DefaultOpenCashier");
	    	//boolean bIsCloseDefault = data.getParameters().getBoolean("DefaultCloseCashier");
		    //if(bIsOpenDefault)
		    // {
	    	//        Criteria oCrit = new Criteria();
	    	//        oCrit.add(PettyCashTypePeer.DEFAULT_OPEN_CASHIER,bIsOpenDefault);
	    	//        List vPettyCashType = PettyCashTypePeer.doSelect(oCrit);
	    	//        if(vPettyCashType.size()>0)
	    	//        {
	    	//            PettyCashType oPettyCashTypeTemp = new PettyCashType();
	    	//            oPettyCashTypeTemp = (PettyCashType)vPettyCashType.get(0);
	    	//            oPettyCashTypeTemp.setDefaultOpenCashier(false);
	    	//            oPettyCashTypeTemp.save();
	    	//        }
    	    //}
    	    //
   		    //if(bIsCloseDefault)
		    // {
	    	//        Criteria oCrit = new Criteria();
	    	//        oCrit.add(PettyCashTypePeer.DEFAULT_CLOSE_CASHIER,bIsCloseDefault);
	    	//        List vPettyCashType = PettyCashTypePeer.doSelect(oCrit);
	    	//        if(vPettyCashType.size()>0)
	    	//        {
	    	//            PettyCashType oPettyCashTypeTemp = new PettyCashType();
	    	//            oPettyCashTypeTemp = (PettyCashType)vPettyCashType.get(0);
	    	//            oPettyCashTypeTemp.setDefaultCloseCashier(false);
	    	//            oPettyCashTypeTemp.save();
	    	//        }
    	    //}
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
    
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			PettyCashTypeLoader oLoader = new PettyCashTypeLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
}

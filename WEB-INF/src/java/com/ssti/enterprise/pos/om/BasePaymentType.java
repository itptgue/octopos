package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PaymentType
 */
public abstract class BasePaymentType extends BaseObject
{
    /** The Peer class */
    private static final PaymentTypePeer peer =
        new PaymentTypePeer();

        
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTypeCode field */
    private String paymentTypeCode;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the isDefault field */
    private boolean isDefault;
      
    /** The value for the isCredit field */
    private boolean isCredit;
      
    /** The value for the isCreditCard field */
    private boolean isCreditCard;
      
    /** The value for the isCoupon field */
    private boolean isCoupon;
      
    /** The value for the isVoucher field */
    private boolean isVoucher;
      
    /** The value for the isMultiplePayment field */
    private boolean isMultiplePayment;
      
    /** The value for the isCashBack field */
    private boolean isCashBack;
      
    /** The value for the isPointReward field */
    private boolean isPointReward;
                                                                
    /** The value for the showInPos field */
    private boolean showInPos = true;
                                                
    /** The value for the directSalesAccount field */
    private String directSalesAccount = "";
  
    
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeCode
     *
     * @return String
     */
    public String getPaymentTypeCode()
    {
        return paymentTypeCode;
    }

                        
    /**
     * Set the value of PaymentTypeCode
     *
     * @param v new value
     */
    public void setPaymentTypeCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeCode, v))
              {
            this.paymentTypeCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsDefault
     *
     * @return boolean
     */
    public boolean getIsDefault()
    {
        return isDefault;
    }

                        
    /**
     * Set the value of IsDefault
     *
     * @param v new value
     */
    public void setIsDefault(boolean v) 
    {
    
                  if (this.isDefault != v)
              {
            this.isDefault = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsCredit
     *
     * @return boolean
     */
    public boolean getIsCredit()
    {
        return isCredit;
    }

                        
    /**
     * Set the value of IsCredit
     *
     * @param v new value
     */
    public void setIsCredit(boolean v) 
    {
    
                  if (this.isCredit != v)
              {
            this.isCredit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsCreditCard
     *
     * @return boolean
     */
    public boolean getIsCreditCard()
    {
        return isCreditCard;
    }

                        
    /**
     * Set the value of IsCreditCard
     *
     * @param v new value
     */
    public void setIsCreditCard(boolean v) 
    {
    
                  if (this.isCreditCard != v)
              {
            this.isCreditCard = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsCoupon
     *
     * @return boolean
     */
    public boolean getIsCoupon()
    {
        return isCoupon;
    }

                        
    /**
     * Set the value of IsCoupon
     *
     * @param v new value
     */
    public void setIsCoupon(boolean v) 
    {
    
                  if (this.isCoupon != v)
              {
            this.isCoupon = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsVoucher
     *
     * @return boolean
     */
    public boolean getIsVoucher()
    {
        return isVoucher;
    }

                        
    /**
     * Set the value of IsVoucher
     *
     * @param v new value
     */
    public void setIsVoucher(boolean v) 
    {
    
                  if (this.isVoucher != v)
              {
            this.isVoucher = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsMultiplePayment
     *
     * @return boolean
     */
    public boolean getIsMultiplePayment()
    {
        return isMultiplePayment;
    }

                        
    /**
     * Set the value of IsMultiplePayment
     *
     * @param v new value
     */
    public void setIsMultiplePayment(boolean v) 
    {
    
                  if (this.isMultiplePayment != v)
              {
            this.isMultiplePayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsCashBack
     *
     * @return boolean
     */
    public boolean getIsCashBack()
    {
        return isCashBack;
    }

                        
    /**
     * Set the value of IsCashBack
     *
     * @param v new value
     */
    public void setIsCashBack(boolean v) 
    {
    
                  if (this.isCashBack != v)
              {
            this.isCashBack = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsPointReward
     *
     * @return boolean
     */
    public boolean getIsPointReward()
    {
        return isPointReward;
    }

                        
    /**
     * Set the value of IsPointReward
     *
     * @param v new value
     */
    public void setIsPointReward(boolean v) 
    {
    
                  if (this.isPointReward != v)
              {
            this.isPointReward = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShowInPos
     *
     * @return boolean
     */
    public boolean getShowInPos()
    {
        return showInPos;
    }

                        
    /**
     * Set the value of ShowInPos
     *
     * @param v new value
     */
    public void setShowInPos(boolean v) 
    {
    
                  if (this.showInPos != v)
              {
            this.showInPos = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DirectSalesAccount
     *
     * @return String
     */
    public String getDirectSalesAccount()
    {
        return directSalesAccount;
    }

                        
    /**
     * Set the value of DirectSalesAccount
     *
     * @param v new value
     */
    public void setDirectSalesAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.directSalesAccount, v))
              {
            this.directSalesAccount = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTypeCode");
              fieldNames.add("Description");
              fieldNames.add("IsDefault");
              fieldNames.add("IsCredit");
              fieldNames.add("IsCreditCard");
              fieldNames.add("IsCoupon");
              fieldNames.add("IsVoucher");
              fieldNames.add("IsMultiplePayment");
              fieldNames.add("IsCashBack");
              fieldNames.add("IsPointReward");
              fieldNames.add("ShowInPos");
              fieldNames.add("DirectSalesAccount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTypeCode"))
        {
                return getPaymentTypeCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("IsDefault"))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals("IsCredit"))
        {
                return Boolean.valueOf(getIsCredit());
            }
          if (name.equals("IsCreditCard"))
        {
                return Boolean.valueOf(getIsCreditCard());
            }
          if (name.equals("IsCoupon"))
        {
                return Boolean.valueOf(getIsCoupon());
            }
          if (name.equals("IsVoucher"))
        {
                return Boolean.valueOf(getIsVoucher());
            }
          if (name.equals("IsMultiplePayment"))
        {
                return Boolean.valueOf(getIsMultiplePayment());
            }
          if (name.equals("IsCashBack"))
        {
                return Boolean.valueOf(getIsCashBack());
            }
          if (name.equals("IsPointReward"))
        {
                return Boolean.valueOf(getIsPointReward());
            }
          if (name.equals("ShowInPos"))
        {
                return Boolean.valueOf(getShowInPos());
            }
          if (name.equals("DirectSalesAccount"))
        {
                return getDirectSalesAccount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PaymentTypePeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(PaymentTypePeer.PAYMENT_TYPE_CODE))
        {
                return getPaymentTypeCode();
            }
          if (name.equals(PaymentTypePeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PaymentTypePeer.IS_DEFAULT))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals(PaymentTypePeer.IS_CREDIT))
        {
                return Boolean.valueOf(getIsCredit());
            }
          if (name.equals(PaymentTypePeer.IS_CREDIT_CARD))
        {
                return Boolean.valueOf(getIsCreditCard());
            }
          if (name.equals(PaymentTypePeer.IS_COUPON))
        {
                return Boolean.valueOf(getIsCoupon());
            }
          if (name.equals(PaymentTypePeer.IS_VOUCHER))
        {
                return Boolean.valueOf(getIsVoucher());
            }
          if (name.equals(PaymentTypePeer.IS_MULTIPLE_PAYMENT))
        {
                return Boolean.valueOf(getIsMultiplePayment());
            }
          if (name.equals(PaymentTypePeer.IS_CASH_BACK))
        {
                return Boolean.valueOf(getIsCashBack());
            }
          if (name.equals(PaymentTypePeer.IS_POINT_REWARD))
        {
                return Boolean.valueOf(getIsPointReward());
            }
          if (name.equals(PaymentTypePeer.SHOW_IN_POS))
        {
                return Boolean.valueOf(getShowInPos());
            }
          if (name.equals(PaymentTypePeer.DIRECT_SALES_ACCOUNT))
        {
                return getDirectSalesAccount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPaymentTypeId();
            }
              if (pos == 1)
        {
                return getPaymentTypeCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return Boolean.valueOf(getIsDefault());
            }
              if (pos == 4)
        {
                return Boolean.valueOf(getIsCredit());
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getIsCreditCard());
            }
              if (pos == 6)
        {
                return Boolean.valueOf(getIsCoupon());
            }
              if (pos == 7)
        {
                return Boolean.valueOf(getIsVoucher());
            }
              if (pos == 8)
        {
                return Boolean.valueOf(getIsMultiplePayment());
            }
              if (pos == 9)
        {
                return Boolean.valueOf(getIsCashBack());
            }
              if (pos == 10)
        {
                return Boolean.valueOf(getIsPointReward());
            }
              if (pos == 11)
        {
                return Boolean.valueOf(getShowInPos());
            }
              if (pos == 12)
        {
                return getDirectSalesAccount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PaymentTypePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PaymentTypePeer.doInsert((PaymentType) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PaymentTypePeer.doUpdate((PaymentType) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key paymentTypeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPaymentTypeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPaymentTypeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPaymentTypeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PaymentType copy() throws TorqueException
    {
        return copyInto(new PaymentType());
    }
  
    protected PaymentType copyInto(PaymentType copyObj) throws TorqueException
    {
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTypeCode(paymentTypeCode);
          copyObj.setDescription(description);
          copyObj.setIsDefault(isDefault);
          copyObj.setIsCredit(isCredit);
          copyObj.setIsCreditCard(isCreditCard);
          copyObj.setIsCoupon(isCoupon);
          copyObj.setIsVoucher(isVoucher);
          copyObj.setIsMultiplePayment(isMultiplePayment);
          copyObj.setIsCashBack(isCashBack);
          copyObj.setIsPointReward(isPointReward);
          copyObj.setShowInPos(showInPos);
          copyObj.setDirectSalesAccount(directSalesAccount);
  
                    copyObj.setPaymentTypeId((String)null);
                                                                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PaymentTypePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PaymentType\n");
        str.append("-----------\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTypeCode      : ")
           .append(getPaymentTypeCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("IsDefault            : ")
           .append(getIsDefault())
           .append("\n")
           .append("IsCredit             : ")
           .append(getIsCredit())
           .append("\n")
           .append("IsCreditCard         : ")
           .append(getIsCreditCard())
           .append("\n")
           .append("IsCoupon             : ")
           .append(getIsCoupon())
           .append("\n")
           .append("IsVoucher            : ")
           .append(getIsVoucher())
           .append("\n")
           .append("IsMultiplePayment    : ")
           .append(getIsMultiplePayment())
           .append("\n")
           .append("IsCashBack           : ")
           .append(getIsCashBack())
           .append("\n")
           .append("IsPointReward        : ")
           .append(getIsPointReward())
           .append("\n")
           .append("ShowInPos            : ")
           .append(getShowInPos())
           .append("\n")
           .append("DirectSalesAccount   : ")
           .append(getDirectSalesAccount())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.tools.financial.CashFlowTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashFlowLoader.java,v 1.1 2009/05/04 01:38:37 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashFlowLoader.java,v $
 * Revision 1.1  2009/05/04 01:38:37  albert
 * *** empty log message ***
 *
 * 
 * </pre><br>
 */
public class CashManagementFlatLoader extends CashManagementLoader
{    
	private static Log log = LogFactory.getLog ( CashManagementFlatLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Account Code";

	public CashManagementFlatLoader ()
	{
		super();
	}

	public CashManagementFlatLoader (String _sUserName)
	{
		super(_sUserName);
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFSheet oSheetDet = oWB.getSheetAt(1);    	
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		
		CashFlow oTR = null;
		List vTD = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING))
    			{
    				m_bTransError = false;    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader)
    			{    				    				
    		    	String sTxNo = getString (oRow, 0); 
    		    	if (StringUtil.isNotEmpty(sTxNo))
    		    	{
    		    		m_iTotalTrans++;
    		    		
    		    		oTR = new CashFlow();
        				vTD = new ArrayList();
	    				mapTrans (oRow, oTR);
	    				int iTotalDetRow = oSheetDet.getPhysicalNumberOfRows();
	    				for(int iDetRow = 1; iDetRow < iTotalDetRow; iDetRow++)   				
	    				{
	    					HSSFRow oDetRow = oSheetDet.getRow(iDetRow);
	    					CashFlowJournal oTD = new CashFlowJournal();
	    			    	String sTxDetNo = getString (oDetRow, 0); 
	    			    	if (StringUtil.isNotEmpty(sTxDetNo) && StringUtil.isEqual(sTxNo, sTxDetNo))
	    			    	{	    			    		
	    			    		mapDetails(oDetRow, oTR, oTD, 1);
	        					vTD.add(oTD);
	    			    	}
	    				}
	    				log.debug("** TR " + oTR);
	    				log.debug("** vTD " + vTD);
	    				if(!m_bTransError && oTR != null && vTD != null)
	    				{
	    		    		try
	    		    		{
	    		    			//CashFlowTool.setHeaderProperties(oTR, vTD, null);
	    		    			//calculate CF amount
	    		    			calculateCFAmount (oTR, vTD);
	    		    			CashFlowTool.saveData(oTR, vTD, true, null);
	    		    			logSuccess();
	    		    		}
	    		    		catch (Exception _oEx)
	    		    		{
	    		    			String sMsg = _oEx.getMessage();
	    		    			_oEx.printStackTrace();
	    		    			log.error(_oEx);
	    		    			logSaveError(sMsg);
	    		    		}
	    				}
	    				else
	    				{
	    					logReject();
	    				}
	    				m_bTransError = false; 
	    				oTR = null;
	    				vTD = null;	    				
    		    	}    				    				
    			}    			    			
			}
    	} 
    }	
}

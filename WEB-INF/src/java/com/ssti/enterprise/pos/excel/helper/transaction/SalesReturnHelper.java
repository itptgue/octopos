package com.ssti.enterprise.pos.excel.helper.transaction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SortingTool;

public class SalesReturnHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    	
	HSSFWorkbook oWB = new HSSFWorkbook();
    HSSFSheet oSheet = oWB.createSheet("Sales Transaction Worksheet");    
	
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {                
        int iCond = (Integer) getFromParam(oParam, "Condition", Integer.class);
        String sKeywords = (String) getFromParam(oParam, "Keywords", null);        
        Date dStart = CustomParser.parseDate((String)getFromParam(oParam, "StartDate", null));
        Date dEnd = CustomParser.parseDate((String)getFromParam(oParam, "EndDate", null));
        String sLocationID = (String) getFromParam(oParam, "LocationId", null);
        String sCustomerID = (String) getFromParam(oParam, "CustomerId", null);
        int iStatus = (Integer) getFromParam(oParam, "Status", Integer.class);
        String sCashier = (String) getFromParam(oParam, "Cashier", null);
        String sCurrencyID = (String) getFromParam(oParam, "CurrencyId", null);
        
        List vData = SalesReturnTool.findTrans(iCond, sKeywords, dStart, dEnd, 
        	sCustomerID, sLocationID, iStatus,  "", sCashier, sCurrencyID);
        
        vData = SortingTool.sort(vData,"returnNo");
        int iRow = 0;
        
        for (int i = 0; i < vData.size(); i++)
        {
        	SalesReturn oTR = (SalesReturn) vData.get(i);
        	
        	HSSFRow oRow = oSheet.createRow(iRow); iRow++;        	
        	createMasterHeader(oRow);
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createMasterContent(oRow, oTR);
        	
        	List vTD = SalesReturnTool.getDetailsByID(oTR.getSalesReturnId());        	
        	for (int j = 0; j < vTD.size(); j++)
        	{
        		SalesReturnDetail oTD = (SalesReturnDetail) vTD.get(j);
        		if (j == 0)
        		{
        			oRow = oSheet.createRow(iRow); iRow++;
        			createDetailHeader(oRow);
        		}
        		oRow = oSheet.createRow(iRow); iRow++;
            	createDetailContent(oRow, oTD);            	        		
        	}
        	
        	oRow = oSheet.createRow(iRow); iRow++;
        	createHeaderCell(oWB, oRow, (short)0, "End Trans");
        }		
        return oWB;
    }
    
    private void createMasterHeader(HSSFRow oRow)
    {
    	/*
    	Trans No	
    	Location Code	
    	Customer Code	
    	Transaction Date	
    	Salesman Code	
    	Cashier	
    	Payment Type Code	
    	Payment Term Code	
    	Remark	
    	Invoice Discount	
    	Payment Amount	
    	Change Amount	
    	Inclusive Tax
    	*/
    	
    	int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Customer Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Salesman Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Cashier"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Type Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Remark"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol, "Invoice Discount"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Inclusive Tax"); iCol++;
		
		if (TransactionAttributes.b_SALES_MULTI_CURRENCY)
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Currency Code"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Currency Rate"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Fiscal Rate"); iCol++;
		}
    }
    
	private void createMasterContent(HSSFRow oRow, SalesReturn oTR) 
		throws Exception
	{
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTR.getReturnNo()); iCol++;
		createCell(oWB, oRow, (short)iCol, LocationTool.getCodeByID(oTR.getLocationId())); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomerTool.getCustomerByID(oTR.getCustomerId()).getCustomerCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, CustomFormatter.formatDate(oTR.getReturnDate())); iCol++;
		createCell(oWB, oRow, (short)iCol, EmployeeTool.getEmployeeNameByID(oTR.getSalesId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getUserName()); iCol++;
		createCell(oWB, oRow, (short)iCol, PaymentTypeTool.getPaymentTypeCodeByID(oTR.getPaymentTypeId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getRemark()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTR.getTotalDiscountPct()); iCol++;
		createCell(oWB, oRow, (short)iCol, Boolean.valueOf(oTR.getIsInclusiveTax()).toString()); iCol++;
		
		if (TransactionAttributes.b_SALES_MULTI_CURRENCY)
		{
			createCell(oWB, oRow, (short)iCol, CurrencyTool.getCodeByID(oTR.getCurrencyId())); iCol++;
			createCell(oWB, oRow, (short)iCol, oTR.getCurrencyRate().toString()); iCol++;	
			createCell(oWB, oRow, (short)iCol, oTR.getFiscalRate().toString()); iCol++;	
		}
	}
    
    private void createDetailHeader(HSSFRow oRow)
    {
    	int iCol = 0;
    	createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Item Price"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Tax Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Discount"); iCol++;
		if (PreferenceTool.getMultiDept())
		{
			createHeaderCell(oWB, oRow, (short)iCol, "Department"); iCol++;
			createHeaderCell(oWB, oRow, (short)iCol, "Project"); iCol++;
		}
    }
    
    private void createDetailContent(HSSFRow oRow, SalesReturnDetail oTD) 
    	throws Exception 
    {
    	int iCol = 0;
    	createCell(oWB, oRow, (short)iCol, oTD.getItemCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getQty().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getUnitCode()); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getItemPrice().toString()); iCol++;
		createCell(oWB, oRow, (short)iCol, TaxTool.getCodeByID(oTD.getTaxId())); iCol++;
		createCell(oWB, oRow, (short)iCol, oTD.getDiscount()); iCol++;		
    }
}

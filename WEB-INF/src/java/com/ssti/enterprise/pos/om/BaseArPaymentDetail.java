package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ArPaymentDetail
 */
public abstract class BaseArPaymentDetail extends BaseObject
{
    /** The Peer class */
    private static final ArPaymentDetailPeer peer =
        new ArPaymentDetailPeer();

        
    /** The value for the arPaymentDetailId field */
    private String arPaymentDetailId;
      
    /** The value for the arPaymentId field */
    private String arPaymentId;
      
    /** The value for the salesTransactionId field */
    private String salesTransactionId;
      
    /** The value for the invoiceNo field */
    private String invoiceNo;
      
    /** The value for the salesTransactionAmount field */
    private BigDecimal salesTransactionAmount;
      
    /** The value for the invoiceRate field */
    private BigDecimal invoiceRate;
      
    /** The value for the paymentAmount field */
    private BigDecimal paymentAmount;
      
    /** The value for the downPayment field */
    private BigDecimal downPayment;
      
    /** The value for the paidAmount field */
    private BigDecimal paidAmount;
                                                
          
    /** The value for the discountAmount field */
    private BigDecimal discountAmount= bd_ZERO;
                                                
    /** The value for the discountAccountId field */
    private String discountAccountId = "";
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
                                                
    /** The value for the projectId field */
    private String projectId = "";
                                                
    /** The value for the memoId field */
    private String memoId = "";
                                                        
    /** The value for the taxPayment field */
    private boolean taxPayment = false;
  
    
    /**
     * Get the ArPaymentDetailId
     *
     * @return String
     */
    public String getArPaymentDetailId()
    {
        return arPaymentDetailId;
    }

                                              
    /**
     * Set the value of ArPaymentDetailId
     *
     * @param v new value
     */
    public void setArPaymentDetailId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.arPaymentDetailId, v))
              {
            this.arPaymentDetailId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated ArPaymentOther
        if (collArPaymentOthers != null)
        {
            for (int i = 0; i < collArPaymentOthers.size(); i++)
            {
                ((ArPaymentOther) collArPaymentOthers.get(i))
                    .setArPaymentDetailId(v);
            }
        }
                                }
  
    /**
     * Get the ArPaymentId
     *
     * @return String
     */
    public String getArPaymentId()
    {
        return arPaymentId;
    }

                              
    /**
     * Set the value of ArPaymentId
     *
     * @param v new value
     */
    public void setArPaymentId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.arPaymentId, v))
              {
            this.arPaymentId = v;
            setModified(true);
        }
    
                          
                if (aArPayment != null && !ObjectUtils.equals(aArPayment.getArPaymentId(), v))
                {
            aArPayment = null;
        }
      
              }
  
    /**
     * Get the SalesTransactionId
     *
     * @return String
     */
    public String getSalesTransactionId()
    {
        return salesTransactionId;
    }

                        
    /**
     * Set the value of SalesTransactionId
     *
     * @param v new value
     */
    public void setSalesTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesTransactionId, v))
              {
            this.salesTransactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceNo
     *
     * @return String
     */
    public String getInvoiceNo()
    {
        return invoiceNo;
    }

                        
    /**
     * Set the value of InvoiceNo
     *
     * @param v new value
     */
    public void setInvoiceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceNo, v))
              {
            this.invoiceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesTransactionAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getSalesTransactionAmount()
    {
        return salesTransactionAmount;
    }

                        
    /**
     * Set the value of SalesTransactionAmount
     *
     * @param v new value
     */
    public void setSalesTransactionAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.salesTransactionAmount, v))
              {
            this.salesTransactionAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvoiceRate
     *
     * @return BigDecimal
     */
    public BigDecimal getInvoiceRate()
    {
        return invoiceRate;
    }

                        
    /**
     * Set the value of InvoiceRate
     *
     * @param v new value
     */
    public void setInvoiceRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.invoiceRate, v))
              {
            this.invoiceRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaymentAmount()
    {
        return paymentAmount;
    }

                        
    /**
     * Set the value of PaymentAmount
     *
     * @param v new value
     */
    public void setPaymentAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAmount, v))
              {
            this.paymentAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DownPayment
     *
     * @return BigDecimal
     */
    public BigDecimal getDownPayment()
    {
        return downPayment;
    }

                        
    /**
     * Set the value of DownPayment
     *
     * @param v new value
     */
    public void setDownPayment(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.downPayment, v))
              {
            this.downPayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaidAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getPaidAmount()
    {
        return paidAmount;
    }

                        
    /**
     * Set the value of PaidAmount
     *
     * @param v new value
     */
    public void setPaidAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.paidAmount, v))
              {
            this.paidAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getDiscountAmount()
    {
        return discountAmount;
    }

                        
    /**
     * Set the value of DiscountAmount
     *
     * @param v new value
     */
    public void setDiscountAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.discountAmount, v))
              {
            this.discountAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountAccountId
     *
     * @return String
     */
    public String getDiscountAccountId()
    {
        return discountAccountId;
    }

                        
    /**
     * Set the value of DiscountAccountId
     *
     * @param v new value
     */
    public void setDiscountAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountAccountId, v))
              {
            this.discountAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the MemoId
     *
     * @return String
     */
    public String getMemoId()
    {
        return memoId;
    }

                        
    /**
     * Set the value of MemoId
     *
     * @param v new value
     */
    public void setMemoId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memoId, v))
              {
            this.memoId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxPayment
     *
     * @return boolean
     */
    public boolean getTaxPayment()
    {
        return taxPayment;
    }

                        
    /**
     * Set the value of TaxPayment
     *
     * @param v new value
     */
    public void setTaxPayment(boolean v) 
    {
    
                  if (this.taxPayment != v)
              {
            this.taxPayment = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private ArPayment aArPayment;

    /**
     * Declares an association between this object and a ArPayment object
     *
     * @param v ArPayment
     * @throws TorqueException
     */
    public void setArPayment(ArPayment v) throws TorqueException
    {
            if (v == null)
        {
                  setArPaymentId((String) null);
              }
        else
        {
            setArPaymentId(v.getArPaymentId());
        }
            aArPayment = v;
    }

                                            
    /**
     * Get the associated ArPayment object
     *
     * @return the associated ArPayment object
     * @throws TorqueException
     */
    public ArPayment getArPayment() throws TorqueException
    {
        if (aArPayment == null && (!ObjectUtils.equals(this.arPaymentId, null)))
        {
                          aArPayment = ArPaymentPeer.retrieveByPK(SimpleKey.keyFor(this.arPaymentId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               ArPayment obj = ArPaymentPeer.retrieveByPK(this.arPaymentId);
               obj.addArPaymentDetails(this);
            */
        }
        return aArPayment;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setArPaymentKey(ObjectKey key) throws TorqueException
    {
      
                        setArPaymentId(key.toString());
                  }
       
                                
            
          /**
     * Collection to store aggregation of collArPaymentOthers
     */
    protected List collArPaymentOthers;

    /**
     * Temporary storage of collArPaymentOthers to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initArPaymentOthers()
    {
        if (collArPaymentOthers == null)
        {
            collArPaymentOthers = new ArrayList();
        }
    }

    /**
     * Method called to associate a ArPaymentOther object to this object
     * through the ArPaymentOther foreign key attribute
     *
     * @param l ArPaymentOther
     * @throws TorqueException
     */
    public void addArPaymentOther(ArPaymentOther l) throws TorqueException
    {
        getArPaymentOthers().add(l);
        l.setArPaymentDetail((ArPaymentDetail) this);
    }

    /**
     * The criteria used to select the current contents of collArPaymentOthers
     */
    private Criteria lastArPaymentOthersCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getArPaymentOthers(new Criteria())
     *
     * @throws TorqueException
     */
    public List getArPaymentOthers() throws TorqueException
    {
              if (collArPaymentOthers == null)
        {
            collArPaymentOthers = getArPaymentOthers(new Criteria(10));
        }
        return collArPaymentOthers;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ArPaymentDetail has previously
     * been saved, it will retrieve related ArPaymentOthers from storage.
     * If this ArPaymentDetail is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getArPaymentOthers(Criteria criteria) throws TorqueException
    {
              if (collArPaymentOthers == null)
        {
            if (isNew())
            {
               collArPaymentOthers = new ArrayList();
            }
            else
            {
                        criteria.add(ArPaymentOtherPeer.AR_PAYMENT_DETAIL_ID, getArPaymentDetailId() );
                        collArPaymentOthers = ArPaymentOtherPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ArPaymentOtherPeer.AR_PAYMENT_DETAIL_ID, getArPaymentDetailId());
                            if (!lastArPaymentOthersCriteria.equals(criteria))
                {
                    collArPaymentOthers = ArPaymentOtherPeer.doSelect(criteria);
                }
            }
        }
        lastArPaymentOthersCriteria = criteria;

        return collArPaymentOthers;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getArPaymentOthers(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getArPaymentOthers(Connection con) throws TorqueException
    {
              if (collArPaymentOthers == null)
        {
            collArPaymentOthers = getArPaymentOthers(new Criteria(10), con);
        }
        return collArPaymentOthers;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ArPaymentDetail has previously
     * been saved, it will retrieve related ArPaymentOthers from storage.
     * If this ArPaymentDetail is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getArPaymentOthers(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collArPaymentOthers == null)
        {
            if (isNew())
            {
               collArPaymentOthers = new ArrayList();
            }
            else
            {
                         criteria.add(ArPaymentOtherPeer.AR_PAYMENT_DETAIL_ID, getArPaymentDetailId());
                         collArPaymentOthers = ArPaymentOtherPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ArPaymentOtherPeer.AR_PAYMENT_DETAIL_ID, getArPaymentDetailId());
                             if (!lastArPaymentOthersCriteria.equals(criteria))
                 {
                     collArPaymentOthers = ArPaymentOtherPeer.doSelect(criteria, con);
                 }
             }
         }
         lastArPaymentOthersCriteria = criteria;

         return collArPaymentOthers;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ArPaymentDetail is new, it will return
     * an empty collection; or if this ArPaymentDetail has previously
     * been saved, it will retrieve related ArPaymentOthers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ArPaymentDetail.
     */
    protected List getArPaymentOthersJoinArPaymentDetail(Criteria criteria)
        throws TorqueException
    {
                    if (collArPaymentOthers == null)
        {
            if (isNew())
            {
               collArPaymentOthers = new ArrayList();
            }
            else
            {
                              criteria.add(ArPaymentOtherPeer.AR_PAYMENT_DETAIL_ID, getArPaymentDetailId());
                              collArPaymentOthers = ArPaymentOtherPeer.doSelectJoinArPaymentDetail(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ArPaymentOtherPeer.AR_PAYMENT_DETAIL_ID, getArPaymentDetailId());
                                    if (!lastArPaymentOthersCriteria.equals(criteria))
            {
                collArPaymentOthers = ArPaymentOtherPeer.doSelectJoinArPaymentDetail(criteria);
            }
        }
        lastArPaymentOthersCriteria = criteria;

        return collArPaymentOthers;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ArPaymentDetailId");
              fieldNames.add("ArPaymentId");
              fieldNames.add("SalesTransactionId");
              fieldNames.add("InvoiceNo");
              fieldNames.add("SalesTransactionAmount");
              fieldNames.add("InvoiceRate");
              fieldNames.add("PaymentAmount");
              fieldNames.add("DownPayment");
              fieldNames.add("PaidAmount");
              fieldNames.add("DiscountAmount");
              fieldNames.add("DiscountAccountId");
              fieldNames.add("DepartmentId");
              fieldNames.add("ProjectId");
              fieldNames.add("MemoId");
              fieldNames.add("TaxPayment");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ArPaymentDetailId"))
        {
                return getArPaymentDetailId();
            }
          if (name.equals("ArPaymentId"))
        {
                return getArPaymentId();
            }
          if (name.equals("SalesTransactionId"))
        {
                return getSalesTransactionId();
            }
          if (name.equals("InvoiceNo"))
        {
                return getInvoiceNo();
            }
          if (name.equals("SalesTransactionAmount"))
        {
                return getSalesTransactionAmount();
            }
          if (name.equals("InvoiceRate"))
        {
                return getInvoiceRate();
            }
          if (name.equals("PaymentAmount"))
        {
                return getPaymentAmount();
            }
          if (name.equals("DownPayment"))
        {
                return getDownPayment();
            }
          if (name.equals("PaidAmount"))
        {
                return getPaidAmount();
            }
          if (name.equals("DiscountAmount"))
        {
                return getDiscountAmount();
            }
          if (name.equals("DiscountAccountId"))
        {
                return getDiscountAccountId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("MemoId"))
        {
                return getMemoId();
            }
          if (name.equals("TaxPayment"))
        {
                return Boolean.valueOf(getTaxPayment());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ArPaymentDetailPeer.AR_PAYMENT_DETAIL_ID))
        {
                return getArPaymentDetailId();
            }
          if (name.equals(ArPaymentDetailPeer.AR_PAYMENT_ID))
        {
                return getArPaymentId();
            }
          if (name.equals(ArPaymentDetailPeer.SALES_TRANSACTION_ID))
        {
                return getSalesTransactionId();
            }
          if (name.equals(ArPaymentDetailPeer.INVOICE_NO))
        {
                return getInvoiceNo();
            }
          if (name.equals(ArPaymentDetailPeer.SALES_TRANSACTION_AMOUNT))
        {
                return getSalesTransactionAmount();
            }
          if (name.equals(ArPaymentDetailPeer.INVOICE_RATE))
        {
                return getInvoiceRate();
            }
          if (name.equals(ArPaymentDetailPeer.PAYMENT_AMOUNT))
        {
                return getPaymentAmount();
            }
          if (name.equals(ArPaymentDetailPeer.DOWN_PAYMENT))
        {
                return getDownPayment();
            }
          if (name.equals(ArPaymentDetailPeer.PAID_AMOUNT))
        {
                return getPaidAmount();
            }
          if (name.equals(ArPaymentDetailPeer.DISCOUNT_AMOUNT))
        {
                return getDiscountAmount();
            }
          if (name.equals(ArPaymentDetailPeer.DISCOUNT_ACCOUNT_ID))
        {
                return getDiscountAccountId();
            }
          if (name.equals(ArPaymentDetailPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(ArPaymentDetailPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(ArPaymentDetailPeer.MEMO_ID))
        {
                return getMemoId();
            }
          if (name.equals(ArPaymentDetailPeer.TAX_PAYMENT))
        {
                return Boolean.valueOf(getTaxPayment());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getArPaymentDetailId();
            }
              if (pos == 1)
        {
                return getArPaymentId();
            }
              if (pos == 2)
        {
                return getSalesTransactionId();
            }
              if (pos == 3)
        {
                return getInvoiceNo();
            }
              if (pos == 4)
        {
                return getSalesTransactionAmount();
            }
              if (pos == 5)
        {
                return getInvoiceRate();
            }
              if (pos == 6)
        {
                return getPaymentAmount();
            }
              if (pos == 7)
        {
                return getDownPayment();
            }
              if (pos == 8)
        {
                return getPaidAmount();
            }
              if (pos == 9)
        {
                return getDiscountAmount();
            }
              if (pos == 10)
        {
                return getDiscountAccountId();
            }
              if (pos == 11)
        {
                return getDepartmentId();
            }
              if (pos == 12)
        {
                return getProjectId();
            }
              if (pos == 13)
        {
                return getMemoId();
            }
              if (pos == 14)
        {
                return Boolean.valueOf(getTaxPayment());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ArPaymentDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ArPaymentDetailPeer.doInsert((ArPaymentDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ArPaymentDetailPeer.doUpdate((ArPaymentDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collArPaymentOthers != null)
            {
                for (int i = 0; i < collArPaymentOthers.size(); i++)
                {
                    ((ArPaymentOther) collArPaymentOthers.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key arPaymentDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setArPaymentDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setArPaymentDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getArPaymentDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ArPaymentDetail copy() throws TorqueException
    {
        return copyInto(new ArPaymentDetail());
    }
  
    protected ArPaymentDetail copyInto(ArPaymentDetail copyObj) throws TorqueException
    {
          copyObj.setArPaymentDetailId(arPaymentDetailId);
          copyObj.setArPaymentId(arPaymentId);
          copyObj.setSalesTransactionId(salesTransactionId);
          copyObj.setInvoiceNo(invoiceNo);
          copyObj.setSalesTransactionAmount(salesTransactionAmount);
          copyObj.setInvoiceRate(invoiceRate);
          copyObj.setPaymentAmount(paymentAmount);
          copyObj.setDownPayment(downPayment);
          copyObj.setPaidAmount(paidAmount);
          copyObj.setDiscountAmount(discountAmount);
          copyObj.setDiscountAccountId(discountAccountId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setProjectId(projectId);
          copyObj.setMemoId(memoId);
          copyObj.setTaxPayment(taxPayment);
  
                    copyObj.setArPaymentDetailId((String)null);
                                                                                                
                                      
                            
        List v = getArPaymentOthers();
        for (int i = 0; i < v.size(); i++)
        {
            ArPaymentOther obj = (ArPaymentOther) v.get(i);
            copyObj.addArPaymentOther(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ArPaymentDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ArPaymentDetail\n");
        str.append("---------------\n")
           .append("ArPaymentDetailId    : ")
           .append(getArPaymentDetailId())
           .append("\n")
           .append("ArPaymentId          : ")
           .append(getArPaymentId())
           .append("\n")
           .append("SalesTransactionId   : ")
           .append(getSalesTransactionId())
           .append("\n")
           .append("InvoiceNo            : ")
           .append(getInvoiceNo())
           .append("\n")
            .append("SalesTransactionAmount   : ")
           .append(getSalesTransactionAmount())
           .append("\n")
           .append("InvoiceRate          : ")
           .append(getInvoiceRate())
           .append("\n")
           .append("PaymentAmount        : ")
           .append(getPaymentAmount())
           .append("\n")
           .append("DownPayment          : ")
           .append(getDownPayment())
           .append("\n")
           .append("PaidAmount           : ")
           .append(getPaidAmount())
           .append("\n")
           .append("DiscountAmount       : ")
           .append(getDiscountAmount())
           .append("\n")
           .append("DiscountAccountId    : ")
           .append(getDiscountAccountId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("MemoId               : ")
           .append(getMemoId())
           .append("\n")
           .append("TaxPayment           : ")
           .append(getTaxPayment())
           .append("\n")
        ;
        return(str.toString());
    }
}

(function(jQuery){
	jQuery.fn.styleddropdown = function(){
		return this.each(function(){
			var obj = jQuery(this)
			obj.find('.txtSelField').click(function() { //onclick event, 'list' fadein			    
			obj.find('.txtSelList').fadeIn(400);
			
			jQuery(document).keyup(function(event) { //keypress event, fadeout on 'escape'
				if(event.keyCode == 27) {
				obj.find('.txtSelList').fadeOut(400);
				}
			});
			
			obj.find('.txtSelList').hover(function(){ },
				function(){
					jQuery(this).fadeOut(400);
				});
			});
			
			obj.find('.txtSelList li').click(function() { //onclick event, change field value with selected 'list' item and fadeout 'list'
			    var val = jQuery(this).data("value");
			    if(!val || val == '') val = jQuery(this).html();
			    obj.find('.txtSelField').val(val);
			    obj.find('.txtSelList').fadeOut(400);
			});
		});
	};
})(jQuery);

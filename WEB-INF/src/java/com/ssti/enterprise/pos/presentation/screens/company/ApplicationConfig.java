package com.ssti.enterprise.pos.presentation.screens.company;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.PreferenceManager;
import com.ssti.enterprise.pos.om.SysConfig;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.SysConfigTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.om.CardioConfig;
import com.ssti.framework.om.manager.CardioConfigManager;
import com.ssti.framework.tools.AppConfigTool;
import com.ssti.framework.tools.CardioConfigTool;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change save call UpdateHistoryTool
 * </pre><br>
 */
public class ApplicationConfig extends CompanySecureScreen
{
	static final int i_IMPORT = 1;
	static final int i_SAVE = 2;
	static final int i_FILTER = 3;
	static final int i_UPDATE = 4;
	
	static final int i_SAVE_SYS_CONFIG = 9;
	
    public void doBuildTemplate(RunData data, Context context)
    {   
    	int op = data.getParameters().getInt("op");
    	try
        {
        	if (op == i_IMPORT)
        	{
        		AppConfigTool.importAppConfig(data);
        	}
        	else if (op == i_SAVE)
        	{
        		String sID = data.getParameters().getString("AppConfigId");
        		String sValue = data.getParameters().getString("Value");
        		AppConfigTool.update(data, sID, sValue);
        		data.setMessage("Updated Succesfully");
        	}
        	else if (op == i_FILTER)
        	{
        		String sPart = data.getParameters().getString("Part");
        		String sName = data.getParameters().getString("Name");
        		context.put("vData",AppConfigTool.findAppConfig(sPart, sName));
        		return;
        	}
        	else if (op == i_UPDATE)
        	{
        		String sID = data.getParameters().getString("id");
        		context.put("oData",AppConfigTool.getAppConfigByID(sID));
        	}
        	else if (op == i_SAVE_SYS_CONFIG)
        	{
        		SysConfig oSC = SysConfigTool.getSysConfig();
        		SysConfig oOS = oSC.copy();
        		
        		data.getParameters().setProperties(oSC);
        		oSC.setModified(true);
        		oSC.save();
        		
        		UpdateHistoryTool.createHistory(oOS, oSC, data.getUser().getName(), null);
        		PreferenceManager.getInstance().refreshCache(oSC);
        		
        		CardioConfig oCC = CardioConfigTool.getCardioConfig();
        		CardioConfig oOC = oCC.copy();
        		data.getParameters().setProperties(oCC);
        		oCC.setModified(true);
        		oCC.save();      
        		
        		UpdateHistoryTool.createHistory(oOC, oCC, data.getUser().getName(), null);
        		CardioConfigManager.getInstance().refreshCache(oCC);
        		
        		data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
        	}
        	
        	context.put("vData",AppConfigTool.getAppConfig());
        	context.put("sysconfig",SysConfigTool.getInstance());
        	context.put("SysConfig",SysConfigTool.getSysConfig());

        	context.put("cardioconfig",CardioConfigTool.getInstance());        	
        	context.put("CardioConfig",CardioConfigTool.getCardioConfig());        	
        }
        catch (Exception _oEx)
        {
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }        
    }    
}

package com.ssti.enterprise.medical.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Doctor
 */
public abstract class BaseDoctor extends BaseObject
{
    /** The Peer class */
    private static final DoctorPeer peer =
        new DoctorPeer();

        
    /** The value for the doctorId field */
    private String doctorId;
      
    /** The value for the doctorCode field */
    private String doctorCode;
      
    /** The value for the doctorName field */
    private String doctorName;
      
    /** The value for the specialization field */
    private String specialization;
                                                
    /** The value for the description field */
    private String description = "";
                                                
    /** The value for the homeAddress field */
    private String homeAddress = "";
                                                
    /** The value for the practiceAddress field */
    private String practiceAddress = "";
                                                
    /** The value for the phone1 field */
    private String phone1 = "20";
                                                
    /** The value for the phone2 field */
    private String phone2 = "20";
                                                        
    /** The value for the isInternal field */
    private boolean isInternal = false;
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                                
    /** The value for the userName field */
    private String userName = "";
  
    
    /**
     * Get the DoctorId
     *
     * @return String
     */
    public String getDoctorId()
    {
        return doctorId;
    }

                        
    /**
     * Set the value of DoctorId
     *
     * @param v new value
     */
    public void setDoctorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.doctorId, v))
              {
            this.doctorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DoctorCode
     *
     * @return String
     */
    public String getDoctorCode()
    {
        return doctorCode;
    }

                        
    /**
     * Set the value of DoctorCode
     *
     * @param v new value
     */
    public void setDoctorCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.doctorCode, v))
              {
            this.doctorCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DoctorName
     *
     * @return String
     */
    public String getDoctorName()
    {
        return doctorName;
    }

                        
    /**
     * Set the value of DoctorName
     *
     * @param v new value
     */
    public void setDoctorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.doctorName, v))
              {
            this.doctorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Specialization
     *
     * @return String
     */
    public String getSpecialization()
    {
        return specialization;
    }

                        
    /**
     * Set the value of Specialization
     *
     * @param v new value
     */
    public void setSpecialization(String v) 
    {
    
                  if (!ObjectUtils.equals(this.specialization, v))
              {
            this.specialization = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HomeAddress
     *
     * @return String
     */
    public String getHomeAddress()
    {
        return homeAddress;
    }

                        
    /**
     * Set the value of HomeAddress
     *
     * @param v new value
     */
    public void setHomeAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.homeAddress, v))
              {
            this.homeAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PracticeAddress
     *
     * @return String
     */
    public String getPracticeAddress()
    {
        return practiceAddress;
    }

                        
    /**
     * Set the value of PracticeAddress
     *
     * @param v new value
     */
    public void setPracticeAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.practiceAddress, v))
              {
            this.practiceAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone1
     *
     * @return String
     */
    public String getPhone1()
    {
        return phone1;
    }

                        
    /**
     * Set the value of Phone1
     *
     * @param v new value
     */
    public void setPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone1, v))
              {
            this.phone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone2
     *
     * @return String
     */
    public String getPhone2()
    {
        return phone2;
    }

                        
    /**
     * Set the value of Phone2
     *
     * @param v new value
     */
    public void setPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone2, v))
              {
            this.phone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInternal
     *
     * @return boolean
     */
    public boolean getIsInternal()
    {
        return isInternal;
    }

                        
    /**
     * Set the value of IsInternal
     *
     * @param v new value
     */
    public void setIsInternal(boolean v) 
    {
    
                  if (this.isInternal != v)
              {
            this.isInternal = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DoctorId");
              fieldNames.add("DoctorCode");
              fieldNames.add("DoctorName");
              fieldNames.add("Specialization");
              fieldNames.add("Description");
              fieldNames.add("HomeAddress");
              fieldNames.add("PracticeAddress");
              fieldNames.add("Phone1");
              fieldNames.add("Phone2");
              fieldNames.add("IsInternal");
              fieldNames.add("LocationId");
              fieldNames.add("UserName");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DoctorId"))
        {
                return getDoctorId();
            }
          if (name.equals("DoctorCode"))
        {
                return getDoctorCode();
            }
          if (name.equals("DoctorName"))
        {
                return getDoctorName();
            }
          if (name.equals("Specialization"))
        {
                return getSpecialization();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("HomeAddress"))
        {
                return getHomeAddress();
            }
          if (name.equals("PracticeAddress"))
        {
                return getPracticeAddress();
            }
          if (name.equals("Phone1"))
        {
                return getPhone1();
            }
          if (name.equals("Phone2"))
        {
                return getPhone2();
            }
          if (name.equals("IsInternal"))
        {
                return Boolean.valueOf(getIsInternal());
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DoctorPeer.DOCTOR_ID))
        {
                return getDoctorId();
            }
          if (name.equals(DoctorPeer.DOCTOR_CODE))
        {
                return getDoctorCode();
            }
          if (name.equals(DoctorPeer.DOCTOR_NAME))
        {
                return getDoctorName();
            }
          if (name.equals(DoctorPeer.SPECIALIZATION))
        {
                return getSpecialization();
            }
          if (name.equals(DoctorPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(DoctorPeer.HOME_ADDRESS))
        {
                return getHomeAddress();
            }
          if (name.equals(DoctorPeer.PRACTICE_ADDRESS))
        {
                return getPracticeAddress();
            }
          if (name.equals(DoctorPeer.PHONE1))
        {
                return getPhone1();
            }
          if (name.equals(DoctorPeer.PHONE2))
        {
                return getPhone2();
            }
          if (name.equals(DoctorPeer.IS_INTERNAL))
        {
                return Boolean.valueOf(getIsInternal());
            }
          if (name.equals(DoctorPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(DoctorPeer.USER_NAME))
        {
                return getUserName();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDoctorId();
            }
              if (pos == 1)
        {
                return getDoctorCode();
            }
              if (pos == 2)
        {
                return getDoctorName();
            }
              if (pos == 3)
        {
                return getSpecialization();
            }
              if (pos == 4)
        {
                return getDescription();
            }
              if (pos == 5)
        {
                return getHomeAddress();
            }
              if (pos == 6)
        {
                return getPracticeAddress();
            }
              if (pos == 7)
        {
                return getPhone1();
            }
              if (pos == 8)
        {
                return getPhone2();
            }
              if (pos == 9)
        {
                return Boolean.valueOf(getIsInternal());
            }
              if (pos == 10)
        {
                return getLocationId();
            }
              if (pos == 11)
        {
                return getUserName();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DoctorPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DoctorPeer.doInsert((Doctor) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DoctorPeer.doUpdate((Doctor) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key doctorId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDoctorId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDoctorId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDoctorId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Doctor copy() throws TorqueException
    {
        return copyInto(new Doctor());
    }
  
    protected Doctor copyInto(Doctor copyObj) throws TorqueException
    {
          copyObj.setDoctorId(doctorId);
          copyObj.setDoctorCode(doctorCode);
          copyObj.setDoctorName(doctorName);
          copyObj.setSpecialization(specialization);
          copyObj.setDescription(description);
          copyObj.setHomeAddress(homeAddress);
          copyObj.setPracticeAddress(practiceAddress);
          copyObj.setPhone1(phone1);
          copyObj.setPhone2(phone2);
          copyObj.setIsInternal(isInternal);
          copyObj.setLocationId(locationId);
          copyObj.setUserName(userName);
  
                    copyObj.setDoctorId((String)null);
                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DoctorPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Doctor\n");
        str.append("------\n")
           .append("DoctorId             : ")
           .append(getDoctorId())
           .append("\n")
           .append("DoctorCode           : ")
           .append(getDoctorCode())
           .append("\n")
           .append("DoctorName           : ")
           .append(getDoctorName())
           .append("\n")
           .append("Specialization       : ")
           .append(getSpecialization())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("HomeAddress          : ")
           .append(getHomeAddress())
           .append("\n")
           .append("PracticeAddress      : ")
           .append(getPracticeAddress())
           .append("\n")
           .append("Phone1               : ")
           .append(getPhone1())
           .append("\n")
           .append("Phone2               : ")
           .append(getPhone2())
           .append("\n")
           .append("IsInternal           : ")
           .append(getIsInternal())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
        ;
        return(str.toString());
    }
}

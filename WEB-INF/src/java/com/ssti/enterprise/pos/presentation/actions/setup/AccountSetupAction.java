package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.AccountLoader;
import com.ssti.enterprise.pos.manager.AccountManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountType;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.AccountTypeTool;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.presentation.SecureAction;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Account setup action class
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AccountSetupAction.java,v 1.17 2008/06/29 07:03:22 albert Exp $ <br>
 *
 * <pre>
 * $Log: AccountSetupAction.java,v $
 * 
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 *
 * </pre><br>
 */
public class AccountSetupAction extends SecureAction
{
    private static Log log = LogFactory.getLog(AccountSetupAction.class);

   	private static final String s_VIEW_PERM    = "View Chart Of Account";
	private static final String s_CREATE_PERM  = "Update Chart Of Account";
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_CREATE_PERM);
    }
    	
    public void doPerform(RunData data, Context context)
     throws Exception
    {
    	if(data.getParameters().getInt("op") == 1)
    	{
    		doDelete(data, context);
    	}
    	if(data.getParameters().getInt("op") == 2)
    	{
    		doUpdate(data, context);
    	}
    	if(data.getParameters().getInt("op") == 3)
    	{
    		doInsert(data, context);
    	}
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
		    String sParentID = data.getParameters().getString("parentId");
			Account oParent = AccountTool.getAccountByID(sParentID);			
            AccountTool.updateParentPosition (oParent, null);

    		Account oAcc = new Account();			
    		setProperties(oAcc, data);
			oAcc.setParentId(sParentID);

			AccountType oType = AccountTypeTool.getAccountType (oAcc.getAccountType());
			oAcc.setNormalBalance (oType.getNormalBalance());
			oAcc.setAccountId (IDGenerator.generateSysID(false));
			oAcc.setOpeningBalance(bd_ZERO); 
			oAcc.save(); 
			
			AccountManager.getInstance().refreshCache(oAcc);			
        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
		}
        catch (Exception _oEx)
        {
            log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

 	public void doUpdate(RunData data, Context context)
        throws Exception
    {
 		ValueParser formData = data.getParameters();
		String sID = formData.getString("parentAccountId");
		Account oAcc = null;
 		try
    	{    	            		
			oAcc = AccountTool.getAccountByID (sID);
			String sOldCurrID = oAcc.getCurrencyId();
			String sNewCurrID = formData.getString("parentCurrencyId", oAcc.getCurrencyId());
			
			if (!sOldCurrID.equals(sNewCurrID)) 
			{
				//if currency changed then check if account OB exists	
				if(GlTransactionTool.isExistInTransaction(oAcc.getAccountId(), -1, false))
				{
					data.setMessage(LocaleTool.getString("curr_maynot_changed"));
					return;
				}
			}
			
			if (oAcc != null)
			{
				Account oOld = oAcc.copy();
				Date dAsDate = CustomParser.parseDate(formData.getString("parentAsDate"));
				oAcc.setAsDate(DateUtil.getStartOfDayDate(dAsDate));
				oAcc.setCurrencyId(sNewCurrID);

				Currency oCurr = CurrencyTool.getCurrencyByID(sNewCurrID);
				if (oCurr != null && !oCurr.getIsDefault())
				{
					oAcc.setObRate(formData.getBigDecimal("parentObRate")); //anticipate rate change
				}
				else
				{
					oAcc.setObRate(bd_ONE); //fixed rate
				}	
				
		    	oAcc.setAccountType(formData.getInt("parentAccountType"));
			    oAcc.setAccountCode(formData.getString("parentAccountCode"));
			    oAcc.setAccountName(formData.getString("parentAccountName"));
			    oAcc.setDescription(formData.getString("parentDescription"));
			    oAcc.save();
			    AccountManager.getInstance().refreshCache(oAcc);
			    
			    UpdateHistoryTool.createHistory(oOld, oAcc, data.getUser().getName(), null);
			}
        	data.setMessage(LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	AccountManager.getInstance().refreshCache(oAcc);
        	_oEx.printStackTrace();
            log.error (_oEx);
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sColumn = "account_id";
    		String sID = data.getParameters().getString("parentAccountId");
    		if (SqlUtil.validateFKRef("journal_voucher_detail",sColumn,sID) &&
    			SqlUtil.validateFKRef("cash_flow_journal",sColumn,sID) &&
    			SqlUtil.validateFKRef("issue_receipt",sColumn,sID) &&
    			SqlUtil.validateFKRef("sub_balance",sColumn,sID) &&
    			SqlUtil.validateFKRef("bank",sColumn,sID))			
    		{	    		
    			Account oAcc = AccountTool.getAccountByID(sID);
    			AccountTool.deleteAccountByID(sID);
    			UpdateHistoryTool.createDelHistory(oAcc, data.getUser().getName(),null);
	            data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
    		}
			else 
			{
				data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
			}    		
    	}
        catch (Exception _oEx)
        {                                                                   
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }   
    }

    public void setProperties (Account _oAccount, RunData data)
    	throws Exception
    {
	    data.getParameters().setProperties(_oAccount);
	    Date dAsDate = CustomParser.parseDate(data.getParameters().getString("AsDate"));
	    if (dAsDate == null) dAsDate = new Date();
	    _oAccount.setAsDate(DateUtil.getStartOfDayDate(dAsDate));
	    _oAccount.setAccountLevel(AccountTool.setChildLevel(_oAccount.getParentId()));
    }

    public void doLoadfile (RunData data, Context context)
        throws Exception
    {
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
         	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			AccountLoader oLoader = new AccountLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
        	context.put ("LoadResult", oLoader.getResult());
        }
        catch (Exception _oEx) 
        {
        	String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
        	log.error ( sError, _oEx );
        	data.setMessage (sError);
        }
    }
}
package com.ssti.enterprise.pos.om;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Wed May 07 11:13:29 ICT 2008]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class BankReconcilePeer
    extends com.ssti.enterprise.pos.om.BaseBankReconcilePeer
{
}

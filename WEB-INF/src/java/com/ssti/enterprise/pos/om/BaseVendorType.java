package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to VendorType
 */
public abstract class BaseVendorType extends BaseObject
{
    /** The Peer class */
    private static final VendorTypePeer peer =
        new VendorTypePeer();

        
    /** The value for the vendorTypeId field */
    private String vendorTypeId;
      
    /** The value for the vendorTypeCode field */
    private String vendorTypeCode;
      
    /** The value for the description field */
    private String description;
  
    
    /**
     * Get the VendorTypeId
     *
     * @return String
     */
    public String getVendorTypeId()
    {
        return vendorTypeId;
    }

                        
    /**
     * Set the value of VendorTypeId
     *
     * @param v new value
     */
    public void setVendorTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorTypeId, v))
              {
            this.vendorTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorTypeCode
     *
     * @return String
     */
    public String getVendorTypeCode()
    {
        return vendorTypeCode;
    }

                        
    /**
     * Set the value of VendorTypeCode
     *
     * @param v new value
     */
    public void setVendorTypeCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorTypeCode, v))
              {
            this.vendorTypeCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VendorTypeId");
              fieldNames.add("VendorTypeCode");
              fieldNames.add("Description");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VendorTypeId"))
        {
                return getVendorTypeId();
            }
          if (name.equals("VendorTypeCode"))
        {
                return getVendorTypeCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VendorTypePeer.VENDOR_TYPE_ID))
        {
                return getVendorTypeId();
            }
          if (name.equals(VendorTypePeer.VENDOR_TYPE_CODE))
        {
                return getVendorTypeCode();
            }
          if (name.equals(VendorTypePeer.DESCRIPTION))
        {
                return getDescription();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVendorTypeId();
            }
              if (pos == 1)
        {
                return getVendorTypeCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VendorTypePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VendorTypePeer.doInsert((VendorType) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VendorTypePeer.doUpdate((VendorType) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key vendorTypeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setVendorTypeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setVendorTypeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVendorTypeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public VendorType copy() throws TorqueException
    {
        return copyInto(new VendorType());
    }
  
    protected VendorType copyInto(VendorType copyObj) throws TorqueException
    {
          copyObj.setVendorTypeId(vendorTypeId);
          copyObj.setVendorTypeCode(vendorTypeCode);
          copyObj.setDescription(description);
  
                    copyObj.setVendorTypeId((String)null);
                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VendorTypePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("VendorType\n");
        str.append("----------\n")
           .append("VendorTypeId         : ")
           .append(getVendorTypeId())
           .append("\n")
           .append("VendorTypeCode       : ")
           .append(getVendorTypeCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
        ;
        return(str.toString());
    }
}

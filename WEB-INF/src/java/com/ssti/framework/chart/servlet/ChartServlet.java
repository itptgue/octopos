package com.ssti.framework.chart.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jfree.chart.servlet.ChartDeleter;
import org.jfree.chart.servlet.ServletUtilities;

import com.ssti.framework.chart.helper.ChartHelper;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Generate JFreechart images 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ChartServlet.java,v 1.3 2007/02/23 14:11:28 albert Exp $ <br>
 * 
 * <pre>
 * $Log: ChartServlet.java,v $
 * Revision 1.3  2007/02/23 14:11:28  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public class ChartServlet extends HttpServlet 
{
	private static final String s_HELPER_CLASS = "helper_class";
    private Map oParam = null;

    public ChartServlet() {
        super();
    }

    public void init() throws ServletException {
        return;
    }

    public void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException 
	{

        HttpSession session = request.getSession();
		String filename = "";
		try {
			oParam = request.getParameterMap ();                         
			String[] aValues  = (String[]) oParam.get(s_HELPER_CLASS);
			String sClassName = aValues [0];
 					            
        	if (aValues != null && sClassName != null) {    
				ChartHelper oHelper = (ChartHelper) Class.forName(sClassName).newInstance();
				filename = oHelper.getChartFileName (oParam, session) ; 
        	} 
		}
		catch (Exception _oEx) {
			System.out.println ("Exception Occured : " +  _oEx.getMessage());
			_oEx.printStackTrace();
		}
		
        File file = new File(System.getProperty("java.io.tmpdir"), filename);
        if (!file.exists()) {
            throw new ServletException("File '" + file.getAbsolutePath() + "' does not exist");
        }

        //  Check that the graph being served was created by the current user
        //  or that it begins with "public"
        boolean isChartInUserList = false;
        ChartDeleter chartDeleter = (ChartDeleter) session.getAttribute("JFreeChart_Deleter");
        if (chartDeleter != null) {
            isChartInUserList = chartDeleter.isChartAvailable(filename);
        }

        boolean isChartPublic = false;
        if (filename.length() >= 6) {
            if (filename.substring(0, 6).equals("public")) {
                isChartPublic = true;
            }
        }

        if (isChartInUserList || isChartPublic) {
            //  Serve it up
            ServletUtilities.sendTempFile(file, response);
        }
        else {
            throw new ServletException("Chart image not found");
        }
        return;
    }

}
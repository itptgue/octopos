package com.ssti.enterprise.medical.model;

import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.om.Customer;

/**
 * Project, Department, 
 * @author Albert
 *
 */
public interface MedTrans extends Persistent
{
	public Customer getCustomer();
	
	public String getCustomerId();
	public String getPatientName();
	public String getPatientAge();

	public void setCustomerId(String custID);
	public void setPatientName(String name);
	public void setPatientAge(String age);
}


package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.tools.DiscountTool;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class DiscountCoupon
    extends com.ssti.enterprise.pos.om.BaseDiscountCoupon
    implements Persistent
{
	Discount discount = null;
	public Discount getDiscount()
	{
		if(discount == null)
		{
			try
			{
				discount= DiscountTool.getDiscountByID(getDiscountId());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return discount;
	}	
}

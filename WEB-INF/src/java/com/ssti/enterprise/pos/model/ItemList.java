package com.ssti.enterprise.pos.model;


import java.math.BigDecimal;
import java.util.Date;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.StringUtil;


/**
 * This class used to create object that will show as report:
 *
 * [9/30/2004 10:30AM]
 * Created by : Yudi
 * 
 * 2017-09-01
 * -add field: source, inputBy, inputDate
 *  
 */
 
public class ItemList
{	
    private String itemId;   
    private String itemCode;
    private String barcode;    
    private String itemName;
    private String itemSku;
    private String itemSkuName;   
    
    private String kategoriId;
    
    private String color;
    private String size;
    private String manufacturer;
    
    private String description;
    private BigDecimal qty;
    private BigDecimal cost;
    private String unitId;    
    private String unitCode;
    
    private Item item;
    
    private String source = "FORM";
    private String inputBy = "";
    private Date lastUpdate = null;
            
    public ItemList()
    {
    }
    
    public ItemList(Item _item)
    {
    	if (_item != null)
    	{
	    	this.itemId = _item.getItemId();	 
	    	this.barcode = _item.getBarcode();
	    	this.itemCode = _item.getItemCode();
	    	this.itemName = _item.getItemName();
	    	this.itemSku = _item.getItemSku();
	    	this.itemSkuName = _item.getItemSkuName();
	    	this.kategoriId = _item.getKategoriId();
	    	this.color = _item.getColor();
	    	this.size = _item.getSize();
	    	this.manufacturer = _item.getManufacturer();
	    	this.description  = _item.getDescription();
	    	this.unitId = _item.getUnitId();
	    	this.item = _item;
	    	
		    try 
		    {
		    	this.unitCode = UnitTool.getCodeByID(unitId);
			} 
		    catch (Exception e) 
		    {
		    	e.printStackTrace();
			}
	    }
    	this.item = _item;
    }      

	public void setItemId(String _itemId)
    {
        this.itemId = _itemId;
    }

    public void setBarode(String _barcode)
    {
        this.barcode = _barcode;
    }

    public void setItemCode(String _itemCode)
    {
        this.itemCode = _itemCode;
    }
    
    public void setItemName(String _itemName)
    {
        this.itemName = _itemName;
    }
    
    public void setDescription(String _description)
    {
        this.description = _description;
    }
 
    public void setQty(BigDecimal _qty)
    {
        this.qty = _qty;
    }
    
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}


	public void setItemSku(String itemSku) {
		this.itemSku = itemSku;
	}

	public void setItemSkuName(String itemSkuName) {
		this.itemSkuName = itemSkuName;
	}

	public void setKategoriId(String kategoriId) {
		this.kategoriId = kategoriId;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public void setItem(Item item) {
		this.item = item;
	}
	
    public String getItemId()
    {
        return itemId;
    }
    
    public String getItemCode()
    {
        return itemCode;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public String getItemName()
    {
        return itemName;
    }

    public String getDescription()
    {
        return description;
    }

    public BigDecimal getQty()
    {
        return qty;
    }
	public BigDecimal getCost() {
		return cost;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public String getUnitId() {
		return unitId;
	}


	public String getItemSku() {
		return itemSku;
	}

	public String getItemSkuName() {
		return itemSkuName;
	}

	public String getKategoriId() {
		return kategoriId;
	}

	public String getColor() {
		return color;
	}

	public String getSize() {
		return size;
	}

	public String getManufacturer() {
		return manufacturer;
	}
	
	public Item getItem() {
		if(item == null && StringUtil.isNotEmpty(getItemId()))
		{
			try 
			{
				item = ItemTool.getItemByID(getItemId());	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}			
		}
		return item;
	}

	public String getSource() {
		return source;
	}

	public String getInputBy() {
		return inputBy;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setInputBy(String inputBy) {
		this.inputBy = inputBy;
		this.lastUpdate = new Date();
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Item:\n");
        str.append("ItemId : ")
		   .append(getItemId())
           .append("\n");
        str.append("ItemCode : ")
           .append(getItemCode())
           .append("\n");
        str.append("ItemSku :")
           .append(getItemSku())
           .append("\n");
        str.append("ItemName : ")
           .append(getItemName())
		   .append("\n");
        str.append("Description : ")
           .append(getDescription())
		   .append("\n");
        str.append("Qty : ")
           .append(getQty())
		   .append("\n");
        str.append("Source : ")
           .append(getSource())
		   .append("\n");
        str.append("InputBy : ")
           .append(getInputBy())
		   .append("\n");
        str.append("LastUpdate : ")
        .append(getLastUpdate())
		   .append("\n");

        return(str.toString());
    }
}

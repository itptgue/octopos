package com.ssti.enterprise.medical.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.medical.om.MedicineSalesDetail;
import com.ssti.enterprise.medical.om.PresPowderDetail;
import com.ssti.enterprise.medical.om.Prescription;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Med Sales Invoice Filter/Helper Tool  
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DoctorTool.java,v 1.1 2009/05/04 01:38:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: DoctorTool.java,v $
 * Revision 1.1  2009/05/04 01:38:15  albert
 * *** empty log message ***
 *
 * Revision 1.7  2008/06/29 07:02:06  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class MedPrintTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( MedPrintTool.class );

	public static List getNonPres(List _vTD)
	{
		if (_vTD != null)
		{			
			try 
			{
				List v = new ArrayList(_vTD.size());
				for(int i = 0; i < _vTD.size(); i++)
				{
					Object oTD  = (Object) _vTD.get(i);
					int iTxType = Integer.parseInt(BeanUtil.invokeGetter(oTD, "txType").toString());
					int iItmType = Integer.parseInt(BeanUtil.invokeGetter(oTD, "itemType").toString());
					if(iTxType == MedicalAttributes.i_NON_PRES && iItmType == i_INVENTORY_PART)
					{
						v.add(oTD);
					}
				}
				return v;
			} 
			catch (Exception e) 
			{
				log.error(e);
			}
		}
		return null;
	}

	public static List getOthers(List _vTD)
	{
		if (_vTD != null)
		{
			List vData = new ArrayList();
			try 
			{
				List vGR = BeanUtil.filterListByFieldValue(_vTD, "itemType", i_GROUPING);				
				List vNI = BeanUtil.filterListByFieldValue(_vTD, "itemType", i_NON_INVENTORY_PART);
				List vSV = BeanUtil.filterListByFieldValue(_vTD, "itemType", i_SERVICE);
				if (vNI != null) vData.addAll(vNI);
				if (vSV != null) vData.addAll(vSV);
				if (vGR != null) vData.addAll(vGR);
			} 
			catch (Exception e) 
			{
				log.error(e);
			}
			return vData;			
		}
		return null;
	}
	
	/**
	 * get pres for normal trans from prescription module
	 * @param _vTD
	 * @return
	 */
	public static List getPres(List _vTD)
	{
		if (_vTD != null)
		{
			try
			{
				
				List v = BeanUtil.filterListByFieldValue(_vTD, "itemType", 5);
				if(v != null && v.size() > 0)
				{
					return BeanUtil.getDistinctListByField(v, "prescriptionId");
				}
			} 
			catch (Exception e) 
			{
				log.error(e);
			}
		}
		return null;
	}
	
	/**
	 * get pres for normal trans from prescription input via POS
	 * @param _vTD
	 * @return
	 */
	public static List getPres2(List _vTD)
	{
		if (_vTD != null)
		{
			try 
			{
				List v = new ArrayList(_vTD.size());
				for(int i = 0; i < _vTD.size(); i++)
				{
					MedicineSalesDetail oTD  = (MedicineSalesDetail) _vTD.get(i);
					if(oTD.getTxType() == MedicalAttributes.i_PRES_NORMAL)
					{
						v.add(oTD);
					}
					if(oTD.getTxType() == MedicalAttributes.i_PRES_POWDER && StringUtil.empty(oTD.getParentId()))
					{
						v.add(oTD);
					}
				}
				return v;
			} 
			catch (Exception e) 
			{
				log.error(e);
			}
		}
		return null;
	}	
	
	public static String getPresCreateBy(List _vTD)
	{
		String sCreateBy = "";
		try 
		{
			List vPres = getPres(_vTD);
			if (vPres != null)
			{
				for (int i = 0; i < vPres.size(); i++)
				{
					Object oTD = (Object) _vTD.get(i);
					
					String sPresID = (String) BeanUtil.invokeGetter(oTD, "prescriptionId");
					Prescription oPres = PrescriptionTool.getHeaderByID(sPresID, null);
					if (oPres != null)
					{
						sCreateBy += oPres.getCreateBy() + " ";
					}
				}
			}
		} 
		catch (Exception e) 
		{
			log.error(e);
		}
		return sCreateBy;
	}
	
	public static List removePackItem(List _vTD)
	{
		if (_vTD != null)
		{
			try
			{
				Iterator iter = _vTD.iterator();
				while(iter.hasNext())
				{
					PresPowderDetail oTD = (PresPowderDetail) iter.next();
					if (PowderTool.getByItemCode(oTD.getItemCode()) != null)
					{
						iter.remove();
					}
				}		
			} 
			catch (Exception e) 
			{
				log.error(e);
			}
		}
		return _vTD;
	}
}
package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseTaxSerialMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseTaxSerialMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseTaxSerialPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_tax_serial";
    }

  
    /**
     * purchase_tax_serial.TRANS_ID
     * @return the column name for the TRANS_ID field
     * @deprecated use PurchaseTaxSerialPeer.purchase_tax_serial.TRANS_ID constant
     */
    public static String getPurchaseTaxSerial_TransId()
    {
        return "purchase_tax_serial.TRANS_ID";
    }
  
    /**
     * purchase_tax_serial.PURCHASE_TAX_SERIAL
     * @return the column name for the PURCHASE_TAX_SERIAL field
     * @deprecated use PurchaseTaxSerialPeer.purchase_tax_serial.PURCHASE_TAX_SERIAL constant
     */
    public static String getPurchaseTaxSerial_PurchaseTaxSerial()
    {
        return "purchase_tax_serial.PURCHASE_TAX_SERIAL";
    }
  
    /**
     * purchase_tax_serial.PURCHASE_TAX_DATE
     * @return the column name for the PURCHASE_TAX_DATE field
     * @deprecated use PurchaseTaxSerialPeer.purchase_tax_serial.PURCHASE_TAX_DATE constant
     */
    public static String getPurchaseTaxSerial_PurchaseTaxDate()
    {
        return "purchase_tax_serial.PURCHASE_TAX_DATE";
    }
  
    /**
     * purchase_tax_serial.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use PurchaseTaxSerialPeer.purchase_tax_serial.AMOUNT constant
     */
    public static String getPurchaseTaxSerial_Amount()
    {
        return "purchase_tax_serial.AMOUNT";
    }
  
    /**
     * purchase_tax_serial.AMOUNT_TAX
     * @return the column name for the AMOUNT_TAX field
     * @deprecated use PurchaseTaxSerialPeer.purchase_tax_serial.AMOUNT_TAX constant
     */
    public static String getPurchaseTaxSerial_AmountTax()
    {
        return "purchase_tax_serial.AMOUNT_TAX";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_tax_serial");
        TableMap tMap = dbMap.getTable("purchase_tax_serial");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_tax_serial.TRANS_ID", "");
                          tMap.addColumn("purchase_tax_serial.PURCHASE_TAX_SERIAL", "");
                          tMap.addColumn("purchase_tax_serial.PURCHASE_TAX_DATE", new Date());
                            tMap.addColumn("purchase_tax_serial.AMOUNT", bd_ZERO);
                            tMap.addColumn("purchase_tax_serial.AMOUNT_TAX", bd_ZERO);
          }
}

package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.BankPeer;
import com.ssti.enterprise.pos.om.CashFlow;
import com.ssti.enterprise.pos.om.CashFlowJournal;
import com.ssti.enterprise.pos.om.CashFlowJournalPeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CashFlowJournalTool.java,v 1.12 2009/05/04 02:04:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: CashFlowJournalTool.java,v $
 * Revision 1.12  2009/05/04 02:04:46  albert
 * *** empty log message ***
 *
 * Revision 1.11  2008/06/29 07:15:44  albert
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/03 03:14:35  albert
 * *** empty log message ***
 *
 * Revision 1.9  2007/12/20 13:51:20  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/04/17 13:09:25  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class CashFlowJournalTool extends BaseJournalTool
{
	private static Log log = LogFactory.getLog ( CashFlowJournalTool.class );

	/**
	 * Create Cash Flow Journal
	 * 
	 * @param _oCF
	 * @param _vCFD
	 * @param _oConn
	 * @throws Exception
	 */
	public void createJournal(CashFlow _oCF, List _vCFD, Connection _oConn) 
		throws Exception
	{
		validate(_oConn);
		
		Period oPeriod = PeriodTool.getPeriodByDate(_oCF.getDueDate(), _oConn);
		Bank oBank = BankTool.getBankByID (_oCF.getBankId(), _oConn);
		Account oBankAcc = AccountTool.getAccountByID(oBank.getAccountId(), _oConn);
		validate (oBankAcc, oBank.getBankCode(), BankPeer.ACCOUNT_ID);
		
		List vTrans = new ArrayList(_vCFD.size());
		
		int iCFDebitCredit = _oCF.getCashFlowType();
		
		GlTransaction oCFTrans = new GlTransaction();
		oCFTrans.setGlTransactionId  (IDGenerator.generateSysID());
		oCFTrans.setTransactionType  (convertCFTypeToGLType(_oCF.getTransactionType())); 
		oCFTrans.setTransactionId    (_oCF.getCashFlowId());
		oCFTrans.setTransactionNo    (_oCF.getCashFlowNo());
		oCFTrans.setGlTransactionNo  (_oCF.getCashFlowNo()); 
		oCFTrans.setTransactionDate  (_oCF.getDueDate());
		oCFTrans.setLocationId		 (_oCF.getLocationId());
		oCFTrans.setDepartmentId     ("");
		oCFTrans.setProjectId        ("");
		oCFTrans.setPeriodId         (oPeriod.getPeriodId());
		oCFTrans.setAccountId        (oBankAcc.getAccountId());
		oCFTrans.setCurrencyId       (_oCF.getCurrencyId());
		oCFTrans.setCurrencyRate     (_oCF.getCurrencyRate());
		oCFTrans.setAmount           (_oCF.getCashFlowAmount());
		oCFTrans.setAmountBase       (_oCF.getAmountBase());
		oCFTrans.setDescription      (_oCF.getDescription());
		oCFTrans.setUserName         (_oCF.getUserName());
		oCFTrans.setDebitCredit      (iCFDebitCredit); //IF DEPOSIT -> DEBIT vice versa
		oCFTrans.setCreateDate       (new Date());
		oCFTrans.setSubLedgerType	 (i_SUB_LEDGER_CASH);
		oCFTrans.setSubLedgerId	 	 (_oCF.getBankId());
		oCFTrans.setSubLedgerDesc	 ("");
		
        addJournal (oCFTrans, oBankAcc, vTrans);			
		
		for (int i = 0; i < _vCFD.size(); i++)
		{
			CashFlowJournal oCFD = (CashFlowJournal) _vCFD.get(i);
			Account oAcc = AccountTool.getAccountByID(oCFD.getAccountId(), _oConn);
			validate (oAcc, oCFD.getAccountCode(), CashFlowJournalPeer.ACCOUNT_ID);

			double dAmount = oCFD.getAmount().doubleValue();
			int iDebitCredit = oCFD.getDebitCredit();
			if (dAmount < 0)
			{
				dAmount = dAmount * -1;
				iDebitCredit = GlTransactionTool.reverseDC(iDebitCredit);
			}
			double dAmountBase = dAmount * oCFD.getCurrencyRate().doubleValue();
			
			GlTransaction oTrans = new GlTransaction();
			oTrans.setGlTransactionId  (IDGenerator.generateSysID());
			oTrans.setTransactionType  (convertCFTypeToGLType(_oCF.getTransactionType()));
			oTrans.setTransactionId    (_oCF.getCashFlowId());
			oTrans.setTransactionNo    (_oCF.getCashFlowNo());
			oTrans.setGlTransactionNo  (_oCF.getCashFlowNo());
			oTrans.setTransactionDate  (_oCF.getDueDate());
			oTrans.setLocationId	   (oCFD.getLocationId());
			oTrans.setProjectId        (oCFD.getProjectId());
			oTrans.setDepartmentId     (oCFD.getDepartmentId());
			oTrans.setPeriodId         (oPeriod.getPeriodId());
			oTrans.setAccountId        (oCFD.getAccountId());
			oTrans.setCurrencyId       (oCFD.getCurrencyId());
			oTrans.setCurrencyRate     (oCFD.getCurrencyRate());
			oTrans.setAmount           (new BigDecimal(dAmount));
			oTrans.setAmountBase       (new BigDecimal(dAmountBase));
			oTrans.setUserName         (_oCF.getUserName());
			oTrans.setDebitCredit      (iDebitCredit); 
			oTrans.setCreateDate       (new Date());
			oTrans.setDescription      (oCFD.getMemo());
			oTrans.setSubLedgerType	   (oCFD.getSubLedgerType());
			oTrans.setSubLedgerId	   (oCFD.getSubLedgerId());
			oTrans.setSubLedgerDesc	   (oCFD.getSubLedgerDesc());
			
			if (StringUtil.isEmpty(oTrans.getDescription()))
			{
				oTrans.setDescription (_oCF.getDescription());	
			}
			addJournal (oTrans, oAcc, vTrans);			
		}
		GlTransactionTool.saveGLTransaction (vTrans, _oConn);
		
	}	

	/**
	 * delete CashFlow Journal Rollback, Update Account Balance 
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */	
	public void deleteCashFlowJournal(int _iTransType, String _sCFID, Connection _oConn) 
    	throws Exception
    {
		log.debug ("Delete CF Journal Type : " + _iTransType + ", iGLType " +  
					convertCFTypeToGLType(_iTransType) + " ID : " + _sCFID);
		deleteJournal (convertCFTypeToGLType(_iTransType), _sCFID, _oConn);
	}
	
	/**
	 * 
	 * @param _iCFType
	 * @return
	 */
	public static int convertCFTypeToGLType (int _iCFType)
	{
		if (_iCFType == AppAttributes.i_CF_AR_PAYMENT) 		return GlAttributes.i_GL_TRANS_AR_PAYMENT;
		if (_iCFType == AppAttributes.i_CF_AP_PAYMENT) 		return GlAttributes.i_GL_TRANS_AP_PAYMENT;
		if (_iCFType == AppAttributes.i_CF_OPENING_BALANCE) return GlAttributes.i_GL_TRANS_OPENING_BALANCE;
		if (_iCFType == AppAttributes.i_CF_DEBIT_MEMO) 		return GlAttributes.i_GL_TRANS_DEBIT_MEMO;
		if (_iCFType == AppAttributes.i_CF_CREDIT_MEMO) 	return GlAttributes.i_GL_TRANS_CREDIT_MEMO;
		if (_iCFType == AppAttributes.i_CF_DIRECT_SALES) 	return GlAttributes.i_GL_TRANS_SALES_INVOICE;
		return GlAttributes.i_GL_TRANS_CASH_MANAGEMENT;
	}
		
}
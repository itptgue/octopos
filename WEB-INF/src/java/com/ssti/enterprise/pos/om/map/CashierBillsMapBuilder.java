package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CashierBillsMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CashierBillsMapBuilder";

    /**
     * Item
     * @deprecated use CashierBillsPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "cashier_bills";
    }

  
    /**
     * cashier_bills.CASHIER_BILLS_ID
     * @return the column name for the CASHIER_BILLS_ID field
     * @deprecated use CashierBillsPeer.cashier_bills.CASHIER_BILLS_ID constant
     */
    public static String getCashierBills_CashierBillsId()
    {
        return "cashier_bills.CASHIER_BILLS_ID";
    }
  
    /**
     * cashier_bills.CASHIER_BALANCE_ID
     * @return the column name for the CASHIER_BALANCE_ID field
     * @deprecated use CashierBillsPeer.cashier_bills.CASHIER_BALANCE_ID constant
     */
    public static String getCashierBills_CashierBalanceId()
    {
        return "cashier_bills.CASHIER_BALANCE_ID";
    }
  
    /**
     * cashier_bills.PAYMENT_BILLS_ID
     * @return the column name for the PAYMENT_BILLS_ID field
     * @deprecated use CashierBillsPeer.cashier_bills.PAYMENT_BILLS_ID constant
     */
    public static String getCashierBills_PaymentBillsId()
    {
        return "cashier_bills.PAYMENT_BILLS_ID";
    }
  
    /**
     * cashier_bills.BILLS_AMOUNT
     * @return the column name for the BILLS_AMOUNT field
     * @deprecated use CashierBillsPeer.cashier_bills.BILLS_AMOUNT constant
     */
    public static String getCashierBills_BillsAmount()
    {
        return "cashier_bills.BILLS_AMOUNT";
    }
  
    /**
     * cashier_bills.QTY
     * @return the column name for the QTY field
     * @deprecated use CashierBillsPeer.cashier_bills.QTY constant
     */
    public static String getCashierBills_Qty()
    {
        return "cashier_bills.QTY";
    }
  
    /**
     * cashier_bills.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use CashierBillsPeer.cashier_bills.AMOUNT constant
     */
    public static String getCashierBills_Amount()
    {
        return "cashier_bills.AMOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("cashier_bills");
        TableMap tMap = dbMap.getTable("cashier_bills");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("cashier_bills.CASHIER_BILLS_ID", "");
                          tMap.addForeignKey(
                "cashier_bills.CASHIER_BALANCE_ID", "" , "cashier_balance" ,
                "cashier_balance_id");
                          tMap.addForeignKey(
                "cashier_bills.PAYMENT_BILLS_ID", "" , "payment_bills" ,
                "payment_bills_id");
                            tMap.addColumn("cashier_bills.BILLS_AMOUNT", Integer.valueOf(0));
                            tMap.addColumn("cashier_bills.QTY", Integer.valueOf(0));
                            tMap.addColumn("cashier_bills.AMOUNT", bd_ZERO);
          }
}

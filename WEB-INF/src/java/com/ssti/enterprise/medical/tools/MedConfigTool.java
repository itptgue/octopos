package com.ssti.enterprise.medical.tools;                                                                              
                                                                                                                        
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.manager.MedConfigManager;
import com.ssti.enterprise.medical.model.MedDetailOM;
import com.ssti.enterprise.medical.om.MedColor;
import com.ssti.enterprise.medical.om.MedColorPeer;
import com.ssti.enterprise.medical.om.MedConfig;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.framework.tools.AppConfigTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;
                                                                                                                        
/**                                                                                                                     
 *                                                                                                                      
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>                                                                 
 *                                                                                                                      
 * <b>Purpose: </b><br>                                                                                                 
 * Business object controller for MedicineSales OM                                                                      
 * <br>                                                                                                                 
 *                                                                                                                      
 * @author  $Author: albert $ <br>                                                                                      
 * @version $Id: MedConfigTool.java,v 1.1 2009/05/04 01:38:15 albert Exp $ <br>                                         
 *                                                                                                                      
 * <pre>                                                                                                                
 * $Log: MedConfigTool.java,v $                                                                                         
 * 
 * 2018-05-28
 * - new CustomerMargin schema
 * - change getPrice @see CustomerMarginTool
 * - old getPrice now deprecated
 * 
 * 2016-12-08
 * - Fix method getHome() prevent error when not empty                                                                              
 *                                                                                                                      
 * </pre><br>                                                                                                           
 */                                                                                                                     
public class MedConfigTool extends BaseTool implements MedicalAttributes                                                                            
{                                                                                                                       
    private static Log log = LogFactory.getLog ( MedConfigTool.class );                                                 
                                                                                                                        
    public static final String s_ZERO = "0";                                                                            
    public static final String s_DEFAULT_PRS_FORMAT = "PRS/yymm/xxxxx";                                                 
                                                                                                                        
    static MedConfigTool instance = null;                                                                               
                                                                                                                        
    public static synchronized MedConfigTool getInstance()                                                              
    {                                                                                                                   
        if (instance == null) instance = new MedConfigTool();                                                           
        return instance;                                                                                                
    }                                                                                                                   
                                                                                                                        
    public static MedConfig getConfig()                                                                                 
        throws Exception                                                                                                
    {                                                                                                                   
        return MedConfigManager.getInstance().getConfig(null);                                                          
    }          
    
    public static String getHome()
    	throws Exception
    {    	
    	return "MedicalMenu.vm";
    }
    
    public static String getPOSScreen()
    	throws Exception
    {
    	if(PreferenceTool.getDefaultPOSScreen() != null && 
    	   PreferenceTool.getDefaultPOSScreen().startsWith("Med") &&
    	   PreferenceTool.getDefaultPOSScreen().endsWith(".vm"))
		{
	    	return PreferenceTool.getDefaultPOSScreen();
		}
    	return "MedicalSalesTrans.vm";
    }    
                                                                                                                        
    //-------------------------------------------------------------------------
    // med color                                         
    //-------------------------------------------------------------------------                                                                                                                        
    
    public static List getColors()
    	throws Exception
    {
    	return MedConfigManager.getInstance().getColors(null);
    }
    
    public static MedColor getColor(String _sColor)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(MedColorPeer.COLOR, (Object)_sColor, Criteria.ILIKE);
    	List v = MedColorPeer.doSelect(oCrit);
    	if(v.size() > 0)
    	{
    		return (MedColor)v.get(0);
    	}
    	return null;
    }        
    
    public static List getNonPres()
    	throws Exception
    {
    	List vCols = getColors();
    	List vPres = new ArrayList();
    	for (int i = 0; i < vCols.size(); i++)
    	{
    		MedColor oCol = (MedColor)vCols.get(i);
    		if(!oCol.getIsPres() && !oCol.getIsWarn())
    		{
    			vPres.add(oCol);
    		}
    	}    	
    	return vPres;    			    			
    } 
    
    public static List getPres()
    	throws Exception
    {
    	List vCols = getColors();
    	List vPres = new ArrayList();
    	for (int i = 0; i < vCols.size(); i++)
    	{
    		MedColor oCol = (MedColor)vCols.get(i);
    		if(oCol.getIsPres())
    		{
    			vPres.add(oCol);
    		}
    	}    	
    	return vPres;    			    			
    }                

    public static List getWarn()
    	throws Exception
    {
    	List vCols = getColors();
    	List vRes = new ArrayList();
    	for (int i = 0; i < vCols.size(); i++)
    	{
    		MedColor oCol = (MedColor)vCols.get(i);
    		if(oCol.getIsWarn())
    		{
    			vRes.add(oCol);
    		}
    	}
    	return vRes;    			    			
    }          
    
    //-------------------------------------------------------------------------                                         
    // prescription                                                                                                     
    //-------------------------------------------------------------------------                                         
                                                                                                                        
    public static String getPresNoFormat()                                                                              
    {                                                                                                                   
        String sFormat = s_DEFAULT_PRS_FORMAT;                                                                          
        try                                                                                                             
        {                                                                                                               
            MedConfig oConf = getConfig();                                                                              
            if (oConf != null)                                                                                          
            {                                                                                                           
                if (StringUtil.isNotEmpty(oConf.getPresNoFormat()))                                                     
                {                                                                                                       
                    sFormat = oConf.getPresNoFormat();                                                                  
                }                                                                                                       
            }                                                                                                           
        }                                                                                                               
        catch (Exception _oEx)                                                                                          
        {                                                                                                               
            _oEx.printStackTrace();                                                                                     
        }                                                                                                               
        return sFormat;                                                                                                 
    }                                                                                                                   
                                                                                                                        
    public static String getRegNoFormat()                                                                               
    {                                                                                                                   
        String sFormat = "REG-lc/yymm/xxxxx";                                                                              
        try                                                                                                             
        {                                                                                                               
            MedConfig oConf = getConfig();                                                                              
            if (oConf != null)                                                                                          
            {                                                                                                           
                if (StringUtil.isNotEmpty(oConf.getRegNoFormat()))                                                      
                {                                                                                                       
                    sFormat = oConf.getRegNoFormat();                                                                   
                }                                                                                                       
            }                                                                                                           
        }                                                                                                               
        catch (Exception _oEx)                                                                                          
        {                                                                                                               
            _oEx.printStackTrace();                                                                                     
        }                                                                                                               
        return sFormat;                                                                                                 
    }                                                                                                                   
                                                                                                                        
    public static String getRecNoFormat()                                                                               
    {                                                                                                                   
        String sFormat = "MRC-lc/yymm/xxxxx";                                                                              
        try                                                                                                             
        {                                                                                                               
            MedConfig oConf = getConfig();                                                                              
            if (oConf != null)                                                                                          
            {                                                                                                           
                if (StringUtil.isNotEmpty(oConf.getRecNoFormat()))                                                      
                {                                                                                                       
                    sFormat = oConf.getRecNoFormat();                                                                   
                }                                                                                                       
            }                                                                                                           
        }                                                                                                               
        catch (Exception _oEx)                                                                                          
        {                                                                                                               
            _oEx.printStackTrace();                                                                                     
        }                                                                                                               
        return sFormat;                                                                                                 
    }                                                                                                                   
                                                                                                                        
    public static String getLabNoFormat()                                                                               
    {                                                                                                                   
        String sFormat = "LAB-lc/yymm/xxxxx";                                                                              
        try                                                                                                             
        {                                                                                                               
            MedConfig oConf = getConfig();                                                                              
            if (oConf != null)                                                                                          
            {                                                                                                           
                if (StringUtil.isNotEmpty(oConf.getLabNoFormat()))                                                      
                {                                                                                                       
                    sFormat = oConf.getLabNoFormat();                                                                   
                }                                                                                                       
            }                                                                                                           
        }                                                                                                               
        catch (Exception _oEx)                                                                                          
        {                                                                                                               
            _oEx.printStackTrace();                                                                                     
        }                                                                                                               
        return sFormat;                                                                                                 
    }                                                                                                                   
                                                                                                                        
                                                                                                                        
    public static String getDefaultRFee()                                                                               
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getDefaultRfee();                                                                              
        }                                                                                                               
        return s_ZERO;                                                                                                  
    }                                                                                                                   
                                                                                                                        
    public static String getDefaultPackaging()                                                                          
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getDefaultPackaging();                                                                         
        }                                                                                                               
        return s_ZERO;                                                                                                  
    }                                                                                                                   
                                                                                                                        
    public static boolean getRfeeEditable()                                                                             
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getRfeeEditable();                                                                             
        }                                                                                                               
        return true;                                                                                                    
    }                                                                                                                   
                                                                                                                        
    //-------------------------------------------------------------------------                                         
    // POS                                                                                                              
    //-------------------------------------------------------------------------                                         
                                                                                                                        
    public static String getPosDefaultRfee()                                                                            
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getPosDefaultRfee();                                                                           
        }                                                                                                               
        return s_ZERO;                                                                                                  
    }                                                                                                                   
                                                                                                                        
    public static String getPosDefaultPack()                                                                            
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getPosDefaultPack();                                                                           
        }                                                                                                               
        return s_ZERO;                                                                                                  
    }                                                                                                                   
                                                                                                                        
    public static boolean getPosRfeeEditable()                                                                          
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getPosRfeeEditable();                                                                          
        }                                                                                                               
        return true;                                                                                                    
    }                                                                                                                   
                                                                                                                        
    public static int getRoundingScale()                                                                                
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getRoundingScale();                                                                            
        }                                                                                                               
        return 0;                                                                                                       
    }                                                                                                                   
                                                                                                                        
    public static int getRoundingMode()                                                                                 
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getRoundingMode();                                                                             
        }                                                                                                               
        return BigDecimal.ROUND_HALF_UP;                                                                                
    }                                                                                                                   
                                                                                                                        
    public static Item getRoundingItem()                                                                                
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return ItemTool.getItemByCode(oConf.getRoundingItemCode());                                                 
        }                                                                                                               
        return null;                                                                                                    
    }                                                                                                                   
                                                                                                                        
    //-------------------------------------------------------------------------                                         
    // Item Link Related Config                                                                                         
    //-------------------------------------------------------------------------                                         
                                                                                                                        
    public static String getPowderItemCode()                                                                            
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getPowderItemCode();                                                                           
        }                                                                                                               
        return "";                                                                                                      
    }                                                                                                                   
                                                                                                                        
    public static String getPresRootKategori()                                                                          
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getPresRootKategori();                                                                         
        }                                                                                                               
        return "";                                                                                                      
    }                                                                                                                   
                                                                                                                        
    public static String getPresItemsGroup()                                                                            
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getPresItemsGroup();                                                                           
        }                                                                                                               
        return "";                                                                                                      
    }                                                                                                                   
                                                                                                                        
    public static boolean isPrescriptionItem(String _sItemID)                                                           
        throws Exception                                                                                                
    {                                                                                                                   
        return isPrescriptionItem(ItemTool.getItemByID(_sItemID));                                                      
    }                                                                                                                   
                                                                                                                        
    public static boolean isPrescriptionItem(Item _oItem)                                                               
        throws Exception                                                                                                
    {                                                                                                                   
        if (_oItem != null)                                                                                             
        {                                                                                                               
            String sKatID = _oItem.getKategoriId();                                                                     
            String sPresRootKat = getPresRootKategori();                                                                
            if (StringUtil.isNotEmpty(sPresRootKat)) //define prescription items by category                            
            {                                                                                                           
                Kategori oRoot = KategoriTool.getRootParent(sKatID);                                                    
                if (oRoot != null)                                                                                      
                {                                                                                                       
                    if (oRoot.getKategoriCode().equals(sPresRootKat))                                                   
                    {                                                                                                   
                        return true;                                                                                    
                    }                                                                                                   
                }                                                                                                       
            }                                                                                                           
            else //define prescription items by group (color)                                                           
            {                                                                                                           
                String sPresItems = getPresItemsGroup();                                                                
                if (StringUtil.isNotEmpty(sPresItems))                                                                  
                {                                                                                                       
                    StringTokenizer oToken = new StringTokenizer(sPresItems, ",");                                      
                    while (oToken.hasMoreTokens())                                                                      
                    {                                                                                                   
                        if (StringUtil.equals(_oItem.getColor(), oToken.nextToken()))                                   
                        {                                                                                               
                            return true;                                                                                
                        }                                                                                               
                    }                                                                                                   
                }                                                                                                       
            }                                                                                                           
        }                                                                                                               
        return false;                                                                                                   
    }                                                                                                                   
                                                                                                                        
    public static Item getPowderItem()                                                                                  
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return ItemTool.getItemByCode(oConf.getPowderItemCode());                                                   
        }                                                                                                               
        return null;                                                                                                    
    }                                                                                                                   
                                                                                                                        
    public static Item getRfeeItem()                                                                                    
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return ItemTool.getItemByCode(oConf.getRfeeItemCode());                                                     
        }                                                                                                               
        return null;                                                                                                    
    }                                                                                                                   
                                                                                                                        
    public static Item getPackagingItem()                                                                               
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return ItemTool.getItemByCode(oConf.getPackagingItemCode());                                                
        }                                                                                                               
        return null;                                                                                                    
    }                                                                                                                   
                                                                                                                        
    public static int getDecimalScale()                                                                                 
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getPosCommaScale();                                                                            
        }                                                                                                               
        return 0;                                                                                                       
    }                                                                                                                   
                                                                                                                        
    public static String getWarningItems()                                                                              
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getWarningItems();                                                                             
        }                                                                                                               
        return "";                                                                                                      
    }                                                                                                                   
                                                                                                                        
    public static boolean isWarningItems(String _sColor)                                                                
        throws Exception                                                                                                
    {                                                                                                                   
        String sWarning = getWarningItems();                                                                            
        if (StringUtil.isNotEmpty(sWarning))                                                                            
        {                                                                                                               
            StringTokenizer oToken = new StringTokenizer(sWarning, ",");                                                
            while (oToken.hasMoreTokens())                                                                              
            {                                                                                                           
                if (StringUtil.equals(_sColor, oToken.nextToken()))                                                     
                {                                                                                                       
                    return true;                                                                                        
                }                                                                                                       
            }                                                                                                           
        }                                                                                                               
        return false;                                                                                                   
    }                                                                                                                   
                                                                                                                        
    public static void setContextTool(Context context)                                                                  
    {                                                                                                                   
        if (context != null)                                                                                            
        {                                                                                                               
            context.put("medconfig", MedConfigTool.getInstance());                                                      
            context.put("custmargin", CustomerMarginTool.getInstance());                                                
            context.put("prescription", PrescriptionTool.getInstance());                                                
            context.put("medorder", MedOrderTool.getInstance());
            context.put("medsales", MedicalSalesTool.getInstance());                                                   
            context.put("doctor", DoctorTool.getInstance());                                                            
            context.put("powder", PowderTool.getInstance());                                                            
            context.put("batchtool", BatchTransactionTool.getInstance());                                               
            
            if(PreferenceTool.medicalType() == MedicalAttributes.i_LABORATORY)
			{
            	context.put("isLab", true);
			}
            if(PreferenceTool.medicalType() == MedicalAttributes.i_CLINIC)
			{
            	context.put("isClinic", true);
			}            
        }
    }
                                                                                                                        
    public static double[] calcRfeePack(String sRfee,                                                                   
                                        String sPack,                                                                   
                                        boolean _bPOS,                                                                  
                                        double dQty,                                                                    
                                        double dSubTotal)                                                               
    {                                                                                                                   
        double[] dRes = {0, 0};                                                                                         
        try                                                                                                             
        {                                                                                                               
            MedConfig oCfg = getConfig();                                                                               
            boolean bMultiplied = oCfg.getRfeeMultiplied();                                                             
            double dMinRfee = oCfg.getRfeeMinAmount().doubleValue();                                                    
            double dMaxRfee = oCfg.getRfeeMaxAmount().doubleValue();                                                    
            double dMinPack = oCfg.getPackMinAmount().doubleValue();                                                    
            double dMaxPack = oCfg.getPackMaxAmount().doubleValue();                                                    
                                                                                                                        
            if (_bPOS)                                                                                                  
            {                                                                                                           
                bMultiplied = oCfg.getPosRfeeMultiplied();                                                              
                dMinRfee = oCfg.getPosRfeeMinAmount().doubleValue();                                                    
                dMaxRfee = oCfg.getPosRfeeMaxAmount().doubleValue();                                                    
                dMinPack = oCfg.getPosPackMinAmount().doubleValue();                                                    
                dMaxPack = oCfg.getPosPackMaxAmount().doubleValue();                                                    
            }                                                                                                           
                                                                                                                        
            double dRfee = 0;                                                                                           
            double dPack = 0;                                                                                           
                                                                                                                        
            if (StringUtil.isNotEmpty(sRfee) && !StringUtil.isEqual(sRfee,s_ZERO))                                      
            {                                                                                                           
                if (StringUtil.contains(sRfee,"%"))                                                                     
                {                                                                                                       
                    dRfee = Calculator.calculateDiscount(sRfee, dSubTotal);                                             
                }                                                                                                       
                else                                                                                                    
                {                                                                                                       
                    dRfee = Double.parseDouble(sRfee);                                                                  
                    if (bMultiplied) dRfee = dRfee * dQty;                                                              
                }                                                                                                       
                if (dMinRfee != 0 && dRfee < dMinRfee)                                                                  
                {                                                                                                       
                    dRfee = dMinRfee;                                                                                   
                }                                                                                                       
                if (dMaxRfee != 0 && dRfee > dMaxRfee)                                                                  
                {                                                                                                       
                    dRfee = dMaxRfee;                                                                                   
                }                                                                                                       
            }                                                                                                           
                                                                                                                        
            if (StringUtil.isNotEmpty(sPack) && !StringUtil.isEqual(sRfee,s_ZERO))                                      
            {                                                                                                           
                if (StringUtil.contains(sPack,"%"))                                                                     
                {                                                                                                       
                    dPack = Calculator.calculateDiscount(sPack, dSubTotal);                                             
                }                                                                                                       
                else                                                                                                    
                {                                                                                                       
                    dPack = Double.parseDouble(sPack);                                                                  
                    if (bMultiplied) dPack = dPack * dQty;                                                              
                }                                                                                                       
                if (dMinPack != 0 && dPack < dMinPack)                                                                  
                {                                                                                                       
                    dPack = dMinPack;                                                                                   
                }                                                                                                       
                if (dMaxPack != 0 && dPack > dMaxPack)                                                                  
                {                                                                                                       
                    dPack = dMaxPack;                                                                                   
                }                                                                                                       
            }                                                                                                           
            dRes[0] = dRfee;                                                                                            
            dRes[1] = dPack;                                                                                            
        }                                                                                                               
        catch (Exception _oEx)                                                                                          
        {                                                                                                               
            log.error("ERROR calculating RFEE : " + _oEx.getMessage(), _oEx);                                           
            _oEx.printStackTrace();                                                                                     
        }                                                                                                               
        return dRes;                                                                                                    
    }                                                                                                                   
                                                                                                                        
    /**                                                                                                                 
     *                                                                                                                  
     * @param oDet                                                                                                      
     * @param _bPOS                                                                                                     
     * @param dQty                                                                                                      
     * @param dSubTotal                                                                                                 
     */                                                                                                                 
    public static void calculateRfee(MedDetailOM oDet,                                                                  
                                     boolean _bPOS,                                                                     
                                     double dQty,                                                                       
                                     double dSubTotal)                                                                  
    {                                                                                                                   
        try                                                                                                             
        {                                                                                                               
                                                                                                                        
            double dRfee = 0;                                                                                           
            double dPack = 0;                                                                                           
                                                                                                                        
            String sRfee = oDet.getRfee();                                                                              
            String sPack = oDet.getPackaging();                                                                         
                                                                                                                        
            double dRes[] = calcRfeePack(sRfee,sPack,_bPOS,dQty,dSubTotal);                                             
            dRfee = dRes[0];                                                                                            
            dPack = dRes[1];                                                                                            
                                                                                                                        
            //if POS & prescription item or not in POS trans                                                            
            if ((_bPOS && MedConfigTool.isPrescriptionItem(oDet.getItemId())))                                          
            {                                                                                                           
                oDet.setRfee(new BigDecimal(dRfee).setScale(0, BigDecimal.ROUND_HALF_UP).toString());                   
                oDet.setPackaging(new BigDecimal(dPack).setScale(0, BigDecimal.ROUND_HALF_UP).toString());              
            }                                                                                                           
        }                                                                                                               
        catch (Exception _oEx)                                                                                          
        {                                                                                                               
            log.error("ERROR calculating RFEE : " + _oEx.getMessage(), _oEx);                                           
            _oEx.printStackTrace();                                                                                     
        }                                                                                                               
    }                                                                                                                   
                                                                                                                        
    //------------------------------------------------------------------------                                          
    // PRICE SETTER                                                                                                     
    //------------------------------------------------------------------------                                          
    public static double getPresItemsMargin()                                                                           
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null && StringUtil.isNotEmpty(oConf.getPresItemsMargin()) && StringUtil.isNumeric(oConf.getPresItemsMargin()))                                                                                              
        {                                                                                                               
            return Calculator.string2BD(oConf.getPresItemsMargin()).doubleValue();                                      
        }                                                                                                               
        return 0;                                                                                                       
    }                                                                                                                   
                                                                                                                        
    public static double getGenItemsMargin()                                                                            
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null && StringUtil.isNotEmpty(oConf.getGenItemsMargin()) && StringUtil.isNumeric(oConf.getGenItemsMargin()))                                                                                              
        {                                                                                                               
            return Calculator.string2BD(oConf.getGenItemsMargin()).doubleValue();                                       
        }                                                                                                               
        return 0;                                                                                                       
    }                                                                                                                   
                                                                                                                        
    public static boolean getGenUsePresMargin()                                                                         
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getGenUsePresMargin();                                                                         
        }                                                                                                               
        return false;                                                                                                   
    }
    
    /**
     * @deprecated
     * @param _oItem
     * @param _sCustID
     * @param _bFromPres
     * @return
     * @throws Exception
     */
    public static BigDecimal getPrice (Item _oItem, String _sCustID, boolean _bFromPres)                                                 
	    throws Exception                                                                                                
	{                                                     
    	if(_bFromPres) return getPrice(_oItem, _sCustID, "", 1);
    	return getPrice(_oItem, _sCustID, "", 2);
	}
    
    /**
     * 
     * @param _oItem
     * @param _sCustID
     * @param _sLocID
     * @param _iTxType
     * @return
     * @throws Exception
     */
    public static BigDecimal getPrice (Item _oItem, 
    								   String _sCustID, 
    								   String _sLocID, 
    								   int _iTxType)                            
        throws Exception                                                                                                
    {                                                                                                                   
        boolean bMarginSetup = false;                                                                                    
        if (_oItem != null)                                                                                             
        {      
        	double dMargin = 0;
            //4th priority margin from med config                                                                       
            //double dMargin = getGenItemsMargin();                                                                       
            //if (isPrescriptionItem(_oItem) || (getGenUsePresMargin() && _bFromPres))                                    
            //{                                                                                                           
            //    dMargin = getPresItemsMargin();                                                                         
            //} 
            
            //3rd priority margin from item margin                                                            
            String sItemMargin = _oItem.getProfitMargin();                                                              
            if (StringUtil.isNotEmpty(sItemMargin) && !StringUtil.equals(sItemMargin, "0"))                                                                     
            {                                                                                                           
                sItemMargin = sItemMargin.replace("%","");                                                              
                dMargin = CustomParser.parseNumber(sItemMargin).doubleValue();                                          
            }  
            
            //2nd priority MarginSetup                                                                                           
            String sMargin = CustomerMarginTool.getMargin(_sCustID, _sLocID, _oItem, _iTxType);                     
            if (StringUtil.isNotEmpty(sMargin))                                                                 
            {                                                                                                       
                sMargin = sMargin.replace("%","");                                                          
                dMargin = CustomParser.parseNumber(sMargin).doubleValue();                                      
                bMarginSetup = true;                                                                                 
            }                                                                                                       
                                                                                                          
            //1st priority get from pricelist   
            boolean bPL = false;
            if (_oItem.isFromPriceList())                                                                               
            {                                                                                                           
                dMargin = 0;                                                                                            
                bMarginSetup = false;
                bPL = true;
            }                                                                                                           
            log.debug("getPrice " + _oItem.getItemCode() + 
            		  ", TxType: " + CustomerMarginTool.getTypeDesc(_iTxType) +                               
                      ", MarginSetup: " + bMarginSetup + ", Margin: " + dMargin + ", FromPL:" + bPL);
            
            if (dMargin > 0 || bMarginSetup) //if margin is set or if margin is from customer                            
            {                                                                                                           
                double dLastPurch = _oItem.getLastPurchasePrice().doubleValue();   
                if(dLastPurch > 0) //only calc margin if margin & last purchase != 0
                {
                	double dSlsTaxRate = _oItem.getTax().getAmount().doubleValue();
                	if(dSlsTaxRate > 0  && PreferenceTool.getPiLastPurchMethod() == i_ITEM_PRICE_METHOD)
                	{
                		dLastPurch = dLastPurch + (dSlsTaxRate / 100 * dLastPurch);
                	}
                	double dPrice = dLastPurch + ((dMargin/100) * dLastPurch);                                              
                	return new BigDecimal(dPrice).setScale(0, BigDecimal.ROUND_HALF_UP);
                }
            }                                                                                                           
            return _oItem.getItemPrice();                                                                               
        }                                                                                                               
        return bd_ZERO;                                                                                                 
    }                                                                                                                   
                                                                                                                        
    //-------------------------------------------------------------------------                                         
    // ALTERTER                                                                                                         
    //-------------------------------------------------------------------------                                         
                                                                                                                        
    public static boolean getAlertPrescription()                                                                        
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getAlertPrescription();                                                                        
        }                                                                                                               
        return false;                                                                                                   
    }                                                                                                                   
                                                                                                                        
    public static String getAlertHosts()                                                                                
        throws Exception                                                                                                
    {                                                                                                                   
        MedConfig oConf = getConfig();                                                                                  
        if (oConf != null)                                                                                              
        {                                                                                                               
            return oConf.getAlertHosts();                                                                               
        }                                                                                                               
        return "";                                                                                                      
    }                                                                                                                   
                                                                                                                        
    public static void sendAlert(String _sMsg)                                                                          
        throws Exception                                                                                                
    {                                                                                                                   
        if (getAlertPrescription())                                                                                     
        {                                                                                                               
            String sHosts = getAlertHosts();                                                                            
            if (StringUtil.isNotEmpty(sHosts))                                                                          
            {                                                                                                           
                List vHosts = StringUtil.toList(sHosts, ",");                                                           
                Alerter oAlert = new Alerter(vHosts, _sMsg);                                                            
                Thread t = new Thread(oAlert);                                                                          
                t.start();                                                                                              
            }                                                                                                           
        }                                                                                                               
    }                                                                                                                   
                                                                                                                        
    public static String getMedicalVersion()                                                                            
    {                                                                                                                   
        return AppConfigTool.getAppVersion(MedConfigTool.class);                                                        
    }                        

    //-------------------------------------------------------------------------                                         
    // INSURANCE                                                                                                         
    //-------------------------------------------------------------------------                                             
    public static PaymentType getInsPType()
    {
    	try {
    		if(StringUtil.isNotEmpty(getConfig().getInsurancePtype()))
        	{
    			return PaymentTypeTool.getPaymentTypeByCode(getConfig().getInsurancePtype());
        	}	
		} 
    	catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }        
}                                                                                                                       
package com.ssti.framework.perf;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryNotificationInfo;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.util.ArrayList;
import java.util.List;

import javax.management.Notification;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * This memory warning system will call the listener when we
 * exceed the percentage of available memory specified.  There
 * should only be one instance of this object created, since the
 * usage threshold can only be set to one number.
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: MemoryWarningSystem.java,v 1.3 2007/02/23 14:13:08 albert Exp $ <br>
 *
 * <pre>
 * $Log: MemoryWarningSystem.java,v $
 * Revision 1.3  2007/02/23 14:13:08  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public class MemoryWarningSystem 
{
    final List listeners = new ArrayList();

    static final MemoryPoolMXBean tenuredGenPool = findTenuredGenPool();

    public interface Listener 
    {
        public void memoryUsageLow(long usedMemory, long maxMemory);
    }

    public MemoryWarningSystem() 
    {
        MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
        NotificationEmitter emitter = (NotificationEmitter) mbean;
        emitter.addNotificationListener(
            new NotificationListener() 
            {
            	public void handleNotification(Notification n, Object hb) 
            	{
	                if (n.getType().equals(MemoryNotificationInfo.MEMORY_THRESHOLD_EXCEEDED)) 
	                {
		                  long maxMemory = tenuredGenPool.getUsage().getMax();
		                  long usedMemory = tenuredGenPool.getUsage().getUsed();
		                  for (int i = 0; i < listeners.size(); i++) 
		                  {
			                    Listener listener = (Listener)listeners.get(i);
			                  	listener.memoryUsageLow(usedMemory, maxMemory);
		                  }
	                }
              }
            }, null, null);
    }
    
    public boolean addListener(Listener listener) 
    {
    	return listeners.add(listener);
    }
    
    public boolean removeListener(Listener listener) 
    {
    	return listeners.remove(listener);
    }
    
    
    public static void setPercentageUsageThreshold(double percentage) 
    {
    	if (percentage <= 0.0 || percentage > 1.0) 
	    {
	    	throw new IllegalArgumentException("Percentage not in range");
	    }
	    long maxMemory = tenuredGenPool.getUsage().getMax();
	    long warningThreshold = (long) (maxMemory * percentage);
	    tenuredGenPool.setUsageThreshold(warningThreshold);
    }

    /**
     * Tenured Space Pool can be determined by it being of type
     * HEAP and by it being possible to set the usage threshold.
     */
    private static MemoryPoolMXBean findTenuredGenPool() 
    {
    	List vMXBean = ManagementFactory.getMemoryPoolMXBeans();
        for ( int i = 0; i < vMXBean.size(); i++) 
        {
        	MemoryPoolMXBean pool = (MemoryPoolMXBean) vMXBean.get(i);
        	// I don't know whether this approach is better, or whether
            // we should rather check for the pool name "Tenured Gen"?
            if (pool.getType() == MemoryType.HEAP && pool.isUsageThresholdSupported()) 
            {
            	return pool;
            }
        }
        throw new AssertionError("Could not find tenured space");
    }
    

}
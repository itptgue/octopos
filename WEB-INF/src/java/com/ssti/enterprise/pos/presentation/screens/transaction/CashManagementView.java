package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class CashManagementView extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Cash Management"};
	
	@Override
	protected boolean isAuthorized(RunData data)
		throws Exception 
	{
		return super.isAuthorized(data, a_PERM);
	}

    public void doBuildTemplate(RunData data, Context context)
    {
        try 
    	{
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findCashFlowResult", "vCF");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}

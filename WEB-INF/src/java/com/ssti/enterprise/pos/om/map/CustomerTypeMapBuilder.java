package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerTypeMapBuilder";

    /**
     * Item
     * @deprecated use CustomerTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_type";
    }

  
    /**
     * customer_type.CUSTOMER_TYPE_ID
     * @return the column name for the CUSTOMER_TYPE_ID field
     * @deprecated use CustomerTypePeer.customer_type.CUSTOMER_TYPE_ID constant
     */
    public static String getCustomerType_CustomerTypeId()
    {
        return "customer_type.CUSTOMER_TYPE_ID";
    }
  
    /**
     * customer_type.CUSTOMER_TYPE_CODE
     * @return the column name for the CUSTOMER_TYPE_CODE field
     * @deprecated use CustomerTypePeer.customer_type.CUSTOMER_TYPE_CODE constant
     */
    public static String getCustomerType_CustomerTypeCode()
    {
        return "customer_type.CUSTOMER_TYPE_CODE";
    }
  
    /**
     * customer_type.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use CustomerTypePeer.customer_type.DESCRIPTION constant
     */
    public static String getCustomerType_Description()
    {
        return "customer_type.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_type");
        TableMap tMap = dbMap.getTable("customer_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_type.CUSTOMER_TYPE_ID", "");
                          tMap.addColumn("customer_type.CUSTOMER_TYPE_CODE", "");
                          tMap.addColumn("customer_type.DESCRIPTION", "");
          }
}

package com.ssti.enterprise.pos.presentation.screens.transaction.serial;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.SerialNoLoader;
import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.model.TransactionMasterOM;
import com.ssti.enterprise.pos.om.IssueReceipt;
import com.ssti.enterprise.pos.om.ItemSerial;
import com.ssti.enterprise.pos.om.SerialTransaction;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.framework.tools.StringUtil;


/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/DetailBatchNoSetter.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: DetailBatchNoSetter.java,v 1.1 2009/05/04 02:02:36 albert Exp $
 *
 * $Log: DetailBatchNoSetter.java,v $
 * Revision 1.1  2009/05/04 02:02:36  albert
 * *** empty log message ***
 *
 * 
 */

public class DetailSerialNoSetter extends TransactionSecureScreen 
{
	String sKey = "";
	String sLocID = "";
	int iNo = 0; 					
	int iOp = -1; 
	boolean bOut = false;
	boolean bEditable = true;
	
	public void doBuildTemplate(RunData data, Context context)
	{
		context.put("itemserial", ItemSerialTool.getInstance());
		try 
		{
			super.doBuildTemplate(data, context);
			sKey = data.getParameters().getString("key", "");
			sLocID = data.getParameters().getString("LocationId", "");
			iNo = data.getParameters().getInt("no"); 					
			bOut = data.getParameters().getBoolean("out");
			iOp = data.getParameters().getInt("op");
			bEditable = data.getParameters().getBoolean("edit");

			log.debug("op " + iOp + " no: " + iNo + " out: " + bOut); //line no
			
			if (iOp == 1) //process set serial no
			{
				List vDet = (List) data.getSession().getAttribute(sKey);			
				if (vDet != null && vDet.size() >= iNo)
				{
					InventoryDetailOM oInv = (InventoryDetailOM) vDet.get(iNo - 1);
					log.debug(oInv);

					String sItemID = oInv.getItemId();					
					int iQty = data.getParameters().getInt("qty");
					log.debug("qty : " + iQty);

					StringBuilder oExists = new StringBuilder();
					StringBuilder oNotExists = new StringBuilder();					
					StringBuilder oDuplicate = new StringBuilder();
					
					List vSerial = new ArrayList(iQty);
					for (int i = 1; i <= iQty; i++)
					{
						boolean bValid = true;
						String sSerialNo = data.getParameters().getString("SerialNo" + i);					
						if (!bOut) //validate exists
						{
							ItemSerial oIS = ItemSerialTool.getByIDAndSN(sItemID, sSerialNo);
							if (oIS != null && oIS.getStatus() == true)
							{
								if (oExists.length() == 0) oExists.append("SerialNo: ");
								oExists.append(sSerialNo);
								if (i != iQty) oExists.append(",");
								bValid = false;
							}
						}
						else //validate not exists
						{
							ItemSerial oIS = ItemSerialTool.getByIDAndSN(sItemID, sSerialNo, sLocID);
							if (oIS == null || oIS.getStatus() == false)
							{
								if (oNotExists.length() == 0) oNotExists.append("SerialNo: ");
								oNotExists.append(sSerialNo);
								if (i != iQty) oNotExists.append(",");
								bValid = false;
							}							
						}
						if (isExists(sSerialNo, vSerial)) //validate in vSerial
						{
							if (oDuplicate.length() == 0) oDuplicate.append("SerialNo: ");
							oDuplicate.append(sSerialNo);
							if (i != iQty) oDuplicate.append(",");
							bValid = false;
						}
						SerialTransaction oST = new SerialTransaction();
						if (bValid) //if valid
						{
							oST.setSerialNo(sSerialNo);
						}
						vSerial.add(oST);
						log.debug("SerialNo" + i + ": " + sSerialNo);
					}//end for qty
					
					boolean bValid = true;
					if (oExists.length() != 0)  //exists in Database
					{
						oExists.append(" Already Exists ");
						data.setMessage(oExists.toString());
						bValid = false;
					}
					if (oNotExists.length() != 0)  //NOT exists in Database
					{
						oNotExists.append(" Not Exists in Inventory ");
						data.setMessage(oNotExists.toString());
						bValid = false;
					}
					if (oDuplicate.length() != 0)  //duplicate Input
					{
						oDuplicate.append(" Duplicate Input ");
						if (StringUtil.isNotEmpty(data.getMessage()))
						{
							oDuplicate.insert(0, data.getMessage() + " ");
						}
						data.setMessage(oDuplicate.toString());
						bValid = false;
					}			
					if (!bValid)
					{
						display(data,context);
					}
					oInv.setSerialTrans(vSerial);
					//vDet.set(iNo, oInv);
					//data.getSession().setAttribute(sKey,vDet);
				}
			}
			else if (iOp == 2) //load from excel
			{
				loadExcel(data, context);	
				display(data, context);
			}	
			else //view / input
			{
				display(data, context);
			}
		}				
		catch (Exception e) 
		{
			data.setMessage("ERROR:" + e.getMessage());
		}
	}
	
	private void display(RunData data, Context context)
	{
		bOut = false;
		if (bEditable) //check if this is inventory out
		{
		    String sMasterKey = "";
			if (StringUtil.equals(sKey, s_DOD) || StringUtil.isEqual(sKey, s_TD) ||
				StringUtil.equals(sKey, s_PRTD) || StringUtil.isEqual(sKey, s_ITD) || StringUtil.isEqual(sKey, s_JCD))
			{
				if (StringUtil.isEqual(sKey, s_DOD) ) sMasterKey = s_DO ;
				if (StringUtil.isEqual(sKey, s_TD)  ) sMasterKey = s_TR ;
				if (StringUtil.isEqual(sKey, s_PRTD)) sMasterKey = s_PRT; 
				if (StringUtil.isEqual(sKey, s_ITD) ) sMasterKey = s_IT;
				if (StringUtil.isEqual(sKey, s_JCD) ) sMasterKey = s_JC;
				
				TransactionMasterOM oTR = (TransactionMasterOM) data.getSession().getAttribute(sMasterKey);
				if (oTR != null)
				{
					context.put("oTR", oTR);
					context.put("locid", oTR.getLocationId());						
				}   
				bOut = true;
			}
			//issue receipt is special case because itu can be out or in
			if (StringUtil.equals(sKey, s_IRD))
			{
				IssueReceipt oIR = (IssueReceipt) data.getSession().getAttribute(s_IR);
				if (oIR != null)
				{
					double dQty = data.getParameters().getDouble("Qty");
					context.put("oTR", oIR);
					context.put("locid", oIR.getLocationId());
					if (oIR.getTransactionType() == 2 || (oIR.getTransactionType() == 1 && dQty < 0)) //issue
					{
						bOut = true;		
					}
				}
			}			
		}
		context.put("bOut", Boolean.valueOf(bOut));
	}
	
	private boolean isExists(String _sSerialNo, List _vSerial) 
	{
		for (int i = 0; i < _vSerial.size(); i++)
		{
			SerialTransaction oST = (SerialTransaction) _vSerial.get(i);
			if (StringUtil.isEqual(oST.getSerialNo(), _sSerialNo))				
			{
				return true;
			}
		}
		return false;
	}

	private void loadExcel(RunData data, Context context)
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			SerialNoLoader oLoader = new SerialNoLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			Map mSN = oLoader.getMapResult();

			StringBuilder oSNQty = new StringBuilder();
			StringBuilder oExists = new StringBuilder();
			StringBuilder oNotExists = new StringBuilder();
			StringBuilder oDuplicate = new StringBuilder();
			
			boolean bValid = true;

			List vDet = (List) data.getSession().getAttribute(sKey);
			Map mDet = toMap(vDet);
			Iterator iter = mDet.keySet().iterator();
			while (iter.hasNext())
			{
				String sCode = (String)iter.next();
				InventoryDetailOM oInv = (InventoryDetailOM) mDet.get(sCode);				
				String sItemID = oInv.getItemId();
				List vSN = (List) mSN.get(sCode);
				if (vSN != null && vSN.size() > 0)
				{
					int iQty = oInv.getQty().intValue();					
					List vSerial = new ArrayList(vSN.size());
					if (vSN.size() != iQty)
					{
						oSNQty.append("WARNING: Serial No For Code " + sCode + " in Excel File (")
						      .append(vSN.size()).append(") != Trans Qty (").append(iQty).append(")<br>");	
					}
					for (int i = 0; i < vSN.size(); i++)
					{
						String sSerialNo = (String) vSN.get(i);					
						if (!bOut) //validate exists
						{
							ItemSerial oIS = ItemSerialTool.getByIDAndSN(sItemID, sSerialNo);
							if (oIS != null && oIS.getStatus() == true)
							{
								if (oExists.length() == 0) oExists.append("SerialNo: ");
								oExists.append(sSerialNo);
								if (i != iQty) oExists.append(",");
								bValid = false;
							}
						}
						else //validate not exists
						{
							ItemSerial oIS = ItemSerialTool.getByIDAndSN(sItemID, sSerialNo, sLocID);
							if (oIS == null || oIS.getStatus() == false)
							{
								if (oNotExists.length() == 0) oNotExists.append("SerialNo: ");
								oNotExists.append(sSerialNo);
								if (i != iQty) oNotExists.append(",");
								bValid = false;
							}							
						}					
						if (isExists(sSerialNo, vSerial)) //validate in vSerial
						{
							if (oDuplicate.length() == 0) oDuplicate.append("SerialNo: ");
							oDuplicate.append(sSerialNo);
							if (i != iQty) oDuplicate.append(",");
							bValid = false;
						}						
						SerialTransaction oST = new SerialTransaction();
						oST.setSerialNo(sSerialNo);
						vSerial.add(oST);
						log.debug("SerialNo" + i + ": " + sSerialNo);	
					}
					oInv.setSerialTrans(vSerial);
				}
			} //end iter
			StringBuilder oErr = new StringBuilder();
			if (oExists.length() != 0) 
			{
				oExists.append(" Already Exists ");
				oErr.append(oExists).append("<br>");
			}
			if (oNotExists.length() != 0) 
			{
				oNotExists.append(" Not Exists in Inventory");
				oErr.append(oNotExists).append("<br>");
			}					
			if (oDuplicate.length() != 0) 
			{
				oDuplicate.append(" Duplicate Input ");
				oErr.append(oDuplicate).append("<br>");
			}							
			if (!bValid)
			{
				display(data,context);
			}
			if (oErr.length() != 0)
			{
				data.setMessage(oErr.toString());				
			}
			if (oSNQty.length() != 0) 
			{
				data.setMessage(oSNQty.toString());
			}	
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}
	
	private static Map toMap(List _vDet)
	{
		Map mCode = new HashMap(_vDet.size());
		for (int i = 0; i < _vDet.size(); i++)
		{
			InventoryDetailOM oInv = (InventoryDetailOM) _vDet.get(i);
			mCode.put(oInv.getItemCode(),oInv);
		}
		return mCode;
	}
}

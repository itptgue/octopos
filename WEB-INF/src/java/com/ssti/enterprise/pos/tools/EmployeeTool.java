package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.om.security.Group;
import org.apache.turbine.om.security.User;
import org.apache.turbine.services.security.TurbineSecurity;

import com.ssti.enterprise.pos.manager.EmployeeManager;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.EmployeePeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.TurbineRole;
import com.ssti.enterprise.pos.om.TurbineRolePeer;
import com.ssti.enterprise.pos.om.TurbineRolePermission;
import com.ssti.enterprise.pos.om.TurbineRolePermissionPeer;
import com.ssti.enterprise.pos.om.TurbineUser;
import com.ssti.enterprise.pos.om.TurbineUserGroupRole;
import com.ssti.enterprise.pos.om.TurbineUserGroupRolePeer;
import com.ssti.enterprise.pos.om.TurbineUserPeer;
import com.ssti.framework.tools.PersistentTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: EmployeeTool.java,v 1.26 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: EmployeeTool.java,v $
 * 
 * 2018-08-27
 * - add method getRoles, get Employee Multiple Roles Data
 * 
 * 2018-07-25
 * - add method getName, get EmployeeName from userName
 * 
 * 2016-04-12
 * - add method getHOUsers to get List of TurbineUser which is registered as Employee in HO.
 * 
 * 2015-10-17
 * - change find Data add condition 20 to query birth date.
 * - Template change EmployeeView
 * 
 * 2015-07-31
 * - add getRoleConfigForPO here and in EmployeeManager
 * 
 * 2015-03-10
 * - add deleteUser when Employee is set to non active
 * 
 * 2015-02-15
 * - add getTargets (vid, start, end) 
 * 
 * </pre><br>
 */
public class EmployeeTool extends BaseTool 
{
	private static Log log = LogFactory.getLog(EmployeeTool.class);
	public static int i_MALE = 1;
	public static int i_FEMALE = 2;
	
	public static List getAllEmployee()
		throws Exception
	{
		return EmployeeManager.getInstance().getAllEmployee();
	}
	
	public static List getAllSystemUser()
		throws Exception
	{
		return TurbineUserPeer.doSelect(new Criteria());
	}    

	public static List getSalesman()
		throws Exception
	{
		return EmployeeManager.getInstance().getSalesman();
	}
	
	public static List getSalesmanByLocID(String _sLocationID)
		throws Exception
	{
		return EmployeeManager.getInstance().getSalesmanByLocID(_sLocationID);
	}

	public static List getEmployeeByLocationID(String _sLocationID)
		throws Exception
	{
		return EmployeeManager.getInstance().getEmployeeByLocationID(_sLocationID);
	}
	
	public static Employee getEmployeeByID(String _sID)
		throws Exception
	{
		return EmployeeManager.getInstance().getEmployeeByID(_sID, null);
	}
	
	public static Employee getEmployeeByID(String _sID, Connection _oConn)
		throws Exception
	{
		return EmployeeManager.getInstance().getEmployeeByID(_sID, _oConn);
	}	

	public static Employee getEmployeeByCode(String _sCode, Connection _oConn)	
		throws Exception
	{
		return EmployeeManager.getInstance().getEmployeeByCode(_sCode, _oConn);
	}	
	
	public static String getEmployeeNameByID(String _sID)
		throws Exception
	{
		Employee oEmp = getEmployeeByID (_sID);
		if (oEmp != null)
		{
			return oEmp.getEmployeeName();
		}
		return "";
	}

	public static String getCodeByID(String _sID)
		throws Exception
	{
		Employee oEmp = getEmployeeByID (_sID);
		if (oEmp != null)
		{
			return oEmp.getEmployeeCode();
		}
		return "";
	}
	
	public static Employee getEmployeeByName(String _sName)
			throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.EMPLOYEE_NAME, _sName);
		List vEmp = EmployeePeer.doSelect(oCrit);
		if (vEmp.size() > 0)
		{
			return (Employee)vEmp.get(0);
		}
		return null;
	}

	public static String getIDByCode(String _sCode)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.EMPLOYEE_CODE, _sCode);
		List vEmp = EmployeePeer.doSelect(oCrit);
		if (vEmp.size() > 0)
		{
			return ((Employee)vEmp.get(0)).getEmployeeId();
		}
		return "";
	}

	public static String getIDByName(String _sName)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.EMPLOYEE_NAME, _sName);
		List vEmp = EmployeePeer.doSelect(oCrit);
		if (vEmp.size() > 0)
		{
			return ((Employee)vEmp.get(0)).getEmployeeId();
		}
		return "";
	}
	
	public static void deleteEmployeeData(String _sUserName)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.USER_NAME, _sUserName);
		EmployeePeer.doDelete(oCrit);
	}

	public static Employee getEmployeeByUsername(String _sUserName)
		throws Exception
	{
		return EmployeeManager.getInstance().getEmployeeByUsername(_sUserName, null);
	}
	
	public static String getName(String _sUserName)
		throws Exception
	{
		Employee oEmp = getEmployeeByUsername(_sUserName);
		if(oEmp != null && StringUtil.isNotEmpty(oEmp.getName())) 
		{
			return oEmp.getName();
		}
		return _sUserName;
	}			
	
	public static String getGender(int _iGender)
	{
		String sGen = LocaleTool.getString("male");
		if (_iGender == i_FEMALE)
		{
			sGen = LocaleTool.getString("female");
		}
		return sGen;
	}
	
	public static int getGenderCode(String _sGender)
	{
		String sFemale = LocaleTool.getString("female");		
		if (StringUtil.isEqual(_sGender, sFemale))
		{
			return i_FEMALE;
		}
		return i_MALE;
	}

	/**
	 * 
	 * @param _iCondition
	 * @param _sKeywords
	 * @return
	 * @throws Exception
	 */
	public static List findData(int _iCondition, String _sKeywords, String _sLocationID)
		throws Exception
	{
		return findData(_iCondition, _sKeywords, _sLocationID, "", "", "", false);
	}
	
	/**
	 * 
	 * @param _iCondition
	 * @param _sKeywords
	 * @return
	 * @throws Exception
	 */
	public static List findData(int _iCondition, 
								String _sKeywords, 
								String _sLocationID, 
								String _sSalesAreaID, 
								String _sTerritoryID, 
								String _sBossID,
								boolean _bNonActive)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sKeywords))
		{
			if (_iCondition == 1)
			{
				oCrit.add(EmployeePeer.EMPLOYEE_NAME,
						(Object) SqlUtil.like (EmployeePeer.EMPLOYEE_NAME, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 2)
			{
				oCrit.add(EmployeePeer.JOB_TITLE,
						(Object) SqlUtil.like (EmployeePeer.JOB_TITLE, _sKeywords), Criteria.CUSTOM);
			}
			else if (_iCondition == 3)
			{
				oCrit.addJoin(EmployeePeer.LOCATION_ID, LocationPeer.LOCATION_ID);				
				oCrit.add(LocationPeer.LOCATION_NAME, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 4)
			{
				oCrit.add(EmployeePeer.USER_NAME, _sKeywords);
			}		
			else if (_iCondition == 5)
			{
				oCrit.add(EmployeePeer.EMPLOYEE_CODE, (Object)_sKeywords, Criteria.ILIKE);
			}					
			else if (_iCondition == 20)
			{
				SqlUtil.setBirthDayCriteria(oCrit, EmployeePeer.BIRTH_DATE, _sKeywords);
			}								
			else {}
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(EmployeePeer.LOCATION_ID, _sLocationID);
		}				
		if (StringUtil.isNotEmpty(_sBossID))
		{
			List vEM = getChildIDList(null, _sBossID);
			vEM.add(_sBossID);
			oCrit.addIn(EmployeePeer.PARENT_ID, vEM);
			oCrit.or(EmployeePeer.PARENT_ID, "");					
		}		
		if(!_bNonActive)
		{
			oCrit.add(EmployeePeer.IS_ACTIVE, true);
		}
		else
		{
			oCrit.add(EmployeePeer.IS_ACTIVE, false);			
		}
		oCrit.addAscendingOrderByColumn (EmployeePeer.EMPLOYEE_NAME);
		
		return EmployeePeer.doSelect(oCrit);
	}

	public static List getEmployees(String _sUserName, boolean _bAllLoc, boolean _bAllSite, boolean _bSalesOnly)		
			throws Exception
	{
		Criteria oCrit = new Criteria();		
		if (StringUtil.isNotEmpty(_sUserName))
		{
			Employee oEmp = EmployeeTool.getEmployeeByUsername(_sUserName);
			if (oEmp != null && StringUtil.isNotEmpty(oEmp.getLocationId()))
			{
				List vLocs = LocationTool.getByUserName(_sUserName, _bAllLoc, _bAllSite);								
				List vLocIDs = PersistentTool.getListOfID(vLocs);
				Location oLoc = LocationTool.getLocationByID(oEmp.getLocationId());
				if (oLoc != null)
				{
					if (!_bAllLoc)
					{						
						vLocIDs.add(oLoc.getLocationId());
					}
				}
				if (vLocIDs.size() > 0)
				{
					oCrit.addIn(EmployeePeer.LOCATION_ID, vLocIDs);
				}
			}
			if(_bSalesOnly)
			{
				oCrit.add(EmployeePeer.IS_SALESMAN,true);
			}
			oCrit.add(EmployeePeer.IS_ACTIVE, true);
		}
		log.debug(oCrit);
		return EmployeePeer.doSelect(oCrit);
	}		
	
	public static String createJSArray () 
		throws Exception
	{	
		List vData = LocationTool.getAllLocation();
		StringBuilder oSB = new StringBuilder ();
		oSB.append ("var aOF = new Array (");
		oSB.append (vData.size());
		oSB.append (");\n");
		
		for (int i = 0; i < vData.size(); i++)
		{
			Location oLocation = (Location) vData.get(i);
			List vSub = getEmployeeByLocationID (oLocation.getLocationId());
			oSB.append ("aOF[");
			oSB.append (i);
			oSB.append ("] = new Array (");
			oSB.append (vSub.size());
			oSB.append (");\n");
			for (int j = 0; j < vSub.size(); j++)
			{
				Employee oSub = (Employee) vSub.get(j);
				
				oSB.append ("\t aOF[");
				oSB.append (i);
				oSB.append ("][");
				oSB.append (j);
				oSB.append ("] = new subof ('");
				oSB.append (oSub.getUserName());
				oSB.append ("','");
				oSB.append (oSub.getEmployeeName());
				oSB.append ("');\n");								
			}				 		 
		}
		return oSB.toString();
	}
	
	private static void setUserRoles(Employee oEmployee)
		throws Exception
	{
		String username = oEmployee.getUserName();
		if(StringUtil.isNotEmpty(username))
		{
			User user = TurbineSecurity.getUser(username);
			TurbineSecurity.revokeAll(user);
			org.apache.turbine.om.security.Role roles[] = TurbineSecurity.getAllRoles().getRolesArray();
			Group groups[] = TurbineSecurity.getAllGroups().getGroupsArray();
			for(int j = 0; j < roles.length; j++)
			{
				org.apache.turbine.om.security.TurbineRole oRole = (org.apache.turbine.om.security.TurbineRole)roles[j];
				String sPrimaryKey = oRole.getPrimaryKey().getValue().toString();
				if( Integer.parseInt(sPrimaryKey)== oEmployee.getRoleId())
				{
					TurbineSecurity.grant(user, groups[0], roles[j]);
				}
			}
		}
	} 
	
	public static void createUserFromEmployee(Employee _oEmployee)
		throws Exception
	{
		if(_oEmployee != null)
		{
			if(!TurbineSecurity.accountExists(_oEmployee.getUserName())
					&& StringUtil.isNotEmpty(_oEmployee.getUserName()))
			{
				Date now = new Date();
				
				//default is equal to user name
				String password = _oEmployee.getUserName();
				User user = TurbineSecurity.getUserInstance();
				user.setName(_oEmployee.getUserName());
				user.setFirstName(_oEmployee.getEmployeeName());
				user.setLastName(" ");
				user.setCreateDate(now);
				user.setLastLogin(new Date(0L));
				TurbineSecurity.addUser(user, password);
				
				log.debug("User " + _oEmployee.getUserName() + " Added Successfully");

				if(_oEmployee.getRoleId() > 0)
				{
					setUserRoles(_oEmployee);
				}
			}
			else
			{
				log.debug("User Already Exist");
				if(_oEmployee.getRoleId() > 0)
				{
					log.debug("Update user Role");
					setUserRoles(_oEmployee);
				}
			}
		}
	}

	/**
	 * delete user when employee is set to non active
	 * 
	 * @param _oEmployee
	 * @throws Exception
	 */
	public static void deleteUser(Employee _oEmployee)
			throws Exception
	{
		if(_oEmployee != null && _oEmployee.getIsActive() == false)
		{
			String sUserName = _oEmployee.getUserName();
			if(!TurbineSecurity.accountExists(sUserName)
					&& StringUtil.isNotEmpty(sUserName))
			{
				User user = TurbineSecurity.getUser(sUserName);
				if(user != null) //delete user from turbine user
				{
					TurbineSecurity.removeUser(user);
				}
				log.debug("User " + _oEmployee.getUserName() + " Deleted Successfully");				
			}
		}
	}
	
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(EmployeePeer.class, EmployeePeer.EMPLOYEE_ID, _sID, _bIsNext);
	}	
	
	/////////////////////////////////////////////////////////////////////////////////////
	//synchronization methods
	/////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 * @return List of updated employees since last ho 2 store
	 * @throws Exception
	 */
	public static List getStoreEmployees (String _sLocID)
		throws Exception
	{
		Criteria oCrit = new Criteria();        
        Criteria.Criterion oStore = oCrit.getNewCriterion (EmployeePeer.LOCATION_ID, _sLocID, Criteria.EQUAL);
        Criteria.Criterion oEmpty = oCrit.getNewCriterion (EmployeePeer.LOCATION_ID, "", Criteria.EQUAL);
        Criteria.Criterion oHO = oCrit.getNewCriterion (EmployeePeer.LOCATION_ID, PreferenceTool.getLocationID(), Criteria.EQUAL);
        oCrit.add(oStore.or(oEmpty).or(oHO)); 
        log.debug(oCrit);
		return EmployeePeer.doSelect(oCrit);
	}	
	
	/**
	 * 
	 * @return List of updated employees since last ho 2 store
	 * @throws Exception
	 */
	public static List getUpdatedEmployees ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		Criteria oCrit = new Criteria();
		if (dLastSyncDate  != null) 
		{
			oCrit.add(EmployeePeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);        
		}
		return EmployeePeer.doSelect(oCrit);
	}	

	public static List getHOUsers()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.LOCATION_ID, PreferenceTool.getLocationID());
		List vEmp = EmployeePeer.doSelect(oCrit);
		List vUsr = new ArrayList<String>(vEmp.size());
		for(int i = 0; i < vEmp.size(); i++)
		{
			Employee oEmp = (Employee) vEmp.get(i);
			vUsr.add(oEmp.getUserName());
		}		
		oCrit = new Criteria();
		oCrit.add(TurbineUserPeer.LOGIN_NAME, vUsr, Criteria.IN);
		return TurbineUserPeer.doSelect(oCrit);
	}

	public static List getAllRole()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return TurbineRolePeer.doSelect(oCrit);
	}

	public static TurbineRole getRoleByID(int _iID)
		throws Exception
	{
		return EmployeeManager.getInstance().getRoleByID(_iID);
	}
	
	public static List getAllRolePerm()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return TurbineRolePermissionPeer.doSelect(oCrit);
	}

	public static TurbineRolePermission getRolePerm(int _iRoleID, int _iPermID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(TurbineRolePermissionPeer.ROLE_ID, _iRoleID);
		oCrit.add(TurbineRolePermissionPeer.PERMISSION_ID, _iPermID);		
		List v = TurbineRolePermissionPeer.doSelect(oCrit);
		if (v.size() > 0)
		{
			return (TurbineRolePermission) v.get(0);
		}
		return null;
	}
	
	public static List getRoles(String _sUserName)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(TurbineUserPeer.LOGIN_NAME, _sUserName);
		List vRoles = new ArrayList(5);
		List v = TurbineUserPeer.doSelect(oCrit);		
		if(v.size() > 0)
		{
			TurbineUser oUser = (TurbineUser) v.get(0);
			oCrit = new Criteria();
			oCrit.add(TurbineUserGroupRolePeer.USER_ID, oUser.getUserId());
			List vUserRoles = TurbineUserGroupRolePeer.doSelect(oCrit);			
			for (int i = 0; i < vUserRoles.size(); i++)
			{
				TurbineUserGroupRole oUGR = (TurbineUserGroupRole) vUserRoles.get(i);
				TurbineRole oRole = getRoleByID(oUGR.getRoleId());
				vRoles.add(oRole);
			}
		}		
		return vRoles;				
	}

	//-------------------------------------------------------------------------
	// TREE 
	//-------------------------------------------------------------------------
	
	public static List getRootParent()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.PARENT_ID, "");        	
		oCrit.addAscendingOrderByColumn(EmployeePeer.EMPLOYEE_NAME);
		List vData = EmployeePeer.doSelect(oCrit);
		return vData;
	}
	
	public static List getByParentID(String _sParentID)
		throws Exception
	{
		return EmployeeManager.getInstance().getByParentID(_sParentID);
	}
	
	public static void updateParentHasChild(String _sParentID)
		throws Exception
	{
		if (StringUtil.isNotEmpty(_sParentID))
		{
			Employee oEmployee = getEmployeeByID (_sParentID);
			if (oEmployee != null)
			{
	            oEmployee.setHasChild(true);
	            oEmployee.save();
	        }
		}
	}

	public static int setChildLevel(String _sParentID)
		throws Exception
	{
		int iLevel = 1;
		if (_sParentID != null && (!_sParentID.equals("")) )
		{
			Employee oParent = getEmployeeByID(_sParentID);
			iLevel = oParent.getEmployeeLevel() + 1;
		}
		return iLevel;
	}

	public static List getChildIDList (List _vID, String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(EmployeePeer.PARENT_ID, _sID);
		List vData = EmployeePeer.doSelect(oCrit);
		for (int i=0; i < vData.size(); i++)
		{
			Employee oEmployee = (Employee)vData.get(i);
			getChildIDList (_vID, oEmployee.getEmployeeId());
			_vID.add(oEmployee.getEmployeeId());
		}
		return _vID;
	} 

	public static boolean hasChild (String _sParentID)
		throws Exception
	{
		List vData = getByParentID(_sParentID);
		if (vData.size() > 0) return true;
		return false;
	}
	
	private static void buildTree(List _vLevel, List _vResult)
		throws Exception
	{
		for (int i = 0; i < _vLevel.size(); i++)
		{
			Employee oEmployee = (Employee) _vLevel.get(i);
			_vResult.add (oEmployee);
			List vChildren = getByParentID(oEmployee.getEmployeeId());
			if (vChildren.size() > 0)
			{
				buildTree(vChildren, _vResult);
			}
		}
	}

	public static List getEmployeeTree(String _sID)
		throws Exception
	{
		List vLevel1 = null;
		if(StringUtil.isNotEmpty(_sID))
		{
			vLevel1 = getByParentID(_sID);
		}
		else
		{
			vLevel1 = getRootParent();			
		}
		List vResult = new ArrayList();
		buildTree(vLevel1, vResult);
		return vResult;
	}

	public static void deleteEmployeeByID(String _sID)
			throws Exception
	{	
		Criteria oCrit = new Criteria();
		oCrit.add(EmployeePeer.PARENT_ID, _sID);
		List vChild = EmployeePeer.doSelect(oCrit);
		for (int i = 0; i< vChild.size(); i++ )
		{
			Employee oEmployee = (Employee) vChild.get(i);
			deleteEmployeeByID (oEmployee.getEmployeeId());
		}
		oCrit = new Criteria();
		oCrit.add(EmployeePeer.EMPLOYEE_ID, _sID);
		EmployeePeer.doDelete(oCrit);
	}			
}

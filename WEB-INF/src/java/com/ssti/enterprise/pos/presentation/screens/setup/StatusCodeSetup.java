package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.ItemStatusCodeTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class StatusCodeSetup extends ItemSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
        	context.put("ItemStatusCodes", ItemStatusCodeTool.getAllItemStatusCode());
        }
        catch(Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
}

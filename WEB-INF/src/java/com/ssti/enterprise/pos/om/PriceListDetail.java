
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.framework.tools.StringUtil;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Mon Feb 07 14:16:35 ICT 2005]
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class PriceListDetail
    extends com.ssti.enterprise.pos.om.BasePriceListDetail
    implements Persistent, MasterOM
{

	@Override
	public String getId() {
		return getPriceListDetailId();
	}

	@Override
	public String getCode() {
		
		String code = "";
		getPL();
		if(pl != null) code = pl.getCode() + "-";
		code += getItemCode();
		return code;
	}
	
	PriceList pl = null;
	public PriceList getPL()
	{
		try
		{
			if (StringUtil.isNotEmpty(getPriceListId()) && pl == null)
			{
				pl = PriceListTool.getPriceListByID(getPriceListId());			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return pl;

	}
	
	Item item = null;
	public Item getItem()
	{
		try
		{
			if (StringUtil.isNotEmpty(getItemId()) && item == null)
			{
				item = ItemTool.getItemByID(getItemId());			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return item;
	}
}

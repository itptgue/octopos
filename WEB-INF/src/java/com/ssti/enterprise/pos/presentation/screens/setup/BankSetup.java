package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class BankSetup extends SetupSecureScreen
{
	private static final String[] a_PERM = {"View Bank Setup"};	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
        	context.put("Banks", BankTool.getAllBank());
        }
        catch(Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
}

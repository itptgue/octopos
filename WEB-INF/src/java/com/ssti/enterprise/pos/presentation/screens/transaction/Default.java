package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;


/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/Default.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: Default.java,v 1.3 2009/05/04 02:02:36 albert Exp $
 *
 * $Log: Default.java,v $
 * Revision 1.3  2009/05/04 02:02:36  albert
 * *** empty log message ***
 *
 * Revision 1.2  2005/12/27 04:14:31  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/08/24 07:34:25  albert
 * *** empty log message ***
 *
 */

public class Default extends TransactionSecureScreen 
{
	public void doBuildTemplate(RunData data, Context context)
	{
		super.doBuildTemplate(data, context);
	}	
}

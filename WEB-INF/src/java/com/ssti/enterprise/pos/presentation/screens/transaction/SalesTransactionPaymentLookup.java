package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.CustomParser;

public class SalesTransactionPaymentLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
			String sInvNo = data.getParameters().getString("InvoiceNo", "");
    		String sPaymentTypeID = data.getParameters().getString("PaymentTypeId", "");
    		String sCustomerID = data.getParameters().getString("CustomerId");
    		String sLocationID = data.getParameters().getString("LocationId", "");
    		boolean bTax = data.getParameters().getBoolean("ViewTax");
    		
    		Customer oCust = CustomerTool.getCustomerByID(sCustomerID);    		
   		    String sCurrencyID = oCust.getDefaultCurrencyId();
    		Date dInvDate = CustomParser.parseDate(data.getParameters().getString("TransactionDate", ""));
    		Date dDueDate = CustomParser.parseDate(data.getParameters().getString("DueDate", ""));	
        	
    		List vTR = TransactionTool.findCreditTrans(sInvNo, 
													   dInvDate, 
													   dInvDate, 													   
													   dDueDate, 
													   dDueDate, 													   
													   sCustomerID, 
													   sLocationID,
			                                           sPaymentTypeID, 
													   sCurrencyID,
													   bTax);
    		
    		List vARD = (List) data.getSession().getAttribute(s_ARD);			
			context.put("sTransDetID", ReceivablePaymentTool.getDetailTransIDString(vARD));
    		context.put("vInvoices", vTR);
    		context.put("tool", ReceivablePaymentTool.getInstance());
    	}
    	catch (Exception _oEx)
    	{
    		data.setMessage("Sales Invoice Lookup Failed : " + _oEx.getMessage());
    	}
    }
}
package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CashierBalanceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CashierBalanceMapBuilder";

    /**
     * Item
     * @deprecated use CashierBalancePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "cashier_balance";
    }

  
    /**
     * cashier_balance.CASHIER_BALANCE_ID
     * @return the column name for the CASHIER_BALANCE_ID field
     * @deprecated use CashierBalancePeer.cashier_balance.CASHIER_BALANCE_ID constant
     */
    public static String getCashierBalance_CashierBalanceId()
    {
        return "cashier_balance.CASHIER_BALANCE_ID";
    }
  
    /**
     * cashier_balance.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use CashierBalancePeer.cashier_balance.LOCATION_ID constant
     */
    public static String getCashierBalance_LocationId()
    {
        return "cashier_balance.LOCATION_ID";
    }
  
    /**
     * cashier_balance.CASHIER_NAME
     * @return the column name for the CASHIER_NAME field
     * @deprecated use CashierBalancePeer.cashier_balance.CASHIER_NAME constant
     */
    public static String getCashierBalance_CashierName()
    {
        return "cashier_balance.CASHIER_NAME";
    }
  
    /**
     * cashier_balance.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use CashierBalancePeer.cashier_balance.TRANSACTION_DATE constant
     */
    public static String getCashierBalance_TransactionDate()
    {
        return "cashier_balance.TRANSACTION_DATE";
    }
  
    /**
     * cashier_balance.TRANS_AMOUNT
     * @return the column name for the TRANS_AMOUNT field
     * @deprecated use CashierBalancePeer.cashier_balance.TRANS_AMOUNT constant
     */
    public static String getCashierBalance_TransAmount()
    {
        return "cashier_balance.TRANS_AMOUNT";
    }
  
    /**
     * cashier_balance.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use CashierBalancePeer.cashier_balance.TOTAL_AMOUNT constant
     */
    public static String getCashierBalance_TotalAmount()
    {
        return "cashier_balance.TOTAL_AMOUNT";
    }
  
    /**
     * cashier_balance.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use CashierBalancePeer.cashier_balance.DESCRIPTION constant
     */
    public static String getCashierBalance_Description()
    {
        return "cashier_balance.DESCRIPTION";
    }
  
    /**
     * cashier_balance.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use CashierBalancePeer.cashier_balance.TRANSACTION_TYPE constant
     */
    public static String getCashierBalance_TransactionType()
    {
        return "cashier_balance.TRANSACTION_TYPE";
    }
  
    /**
     * cashier_balance.HOST_NAME
     * @return the column name for the HOST_NAME field
     * @deprecated use CashierBalancePeer.cashier_balance.HOST_NAME constant
     */
    public static String getCashierBalance_HostName()
    {
        return "cashier_balance.HOST_NAME";
    }
  
    /**
     * cashier_balance.IP_ADDRESS
     * @return the column name for the IP_ADDRESS field
     * @deprecated use CashierBalancePeer.cashier_balance.IP_ADDRESS constant
     */
    public static String getCashierBalance_IpAddress()
    {
        return "cashier_balance.IP_ADDRESS";
    }
  
    /**
     * cashier_balance.OPEN_CASHIER_ID
     * @return the column name for the OPEN_CASHIER_ID field
     * @deprecated use CashierBalancePeer.cashier_balance.OPEN_CASHIER_ID constant
     */
    public static String getCashierBalance_OpenCashierId()
    {
        return "cashier_balance.OPEN_CASHIER_ID";
    }
  
    /**
     * cashier_balance.AUDIT_LOG
     * @return the column name for the AUDIT_LOG field
     * @deprecated use CashierBalancePeer.cashier_balance.AUDIT_LOG constant
     */
    public static String getCashierBalance_AuditLog()
    {
        return "cashier_balance.AUDIT_LOG";
    }
  
    /**
     * cashier_balance.SHIFT
     * @return the column name for the SHIFT field
     * @deprecated use CashierBalancePeer.cashier_balance.SHIFT constant
     */
    public static String getCashierBalance_Shift()
    {
        return "cashier_balance.SHIFT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("cashier_balance");
        TableMap tMap = dbMap.getTable("cashier_balance");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("cashier_balance.CASHIER_BALANCE_ID", "");
                          tMap.addColumn("cashier_balance.LOCATION_ID", "");
                          tMap.addColumn("cashier_balance.CASHIER_NAME", "");
                          tMap.addColumn("cashier_balance.TRANSACTION_DATE", new Date());
                            tMap.addColumn("cashier_balance.TRANS_AMOUNT", bd_ZERO);
                            tMap.addColumn("cashier_balance.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("cashier_balance.DESCRIPTION", "");
                            tMap.addColumn("cashier_balance.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("cashier_balance.HOST_NAME", "");
                          tMap.addColumn("cashier_balance.IP_ADDRESS", "");
                          tMap.addColumn("cashier_balance.OPEN_CASHIER_ID", "");
                          tMap.addColumn("cashier_balance.AUDIT_LOG", "");
                          tMap.addColumn("cashier_balance.SHIFT", "");
          }
}

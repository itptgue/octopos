package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseTaxSerial
 */
public abstract class BasePurchaseTaxSerial extends BaseObject
{
    /** The Peer class */
    private static final PurchaseTaxSerialPeer peer =
        new PurchaseTaxSerialPeer();

        
    /** The value for the transId field */
    private String transId;
      
    /** The value for the purchaseTaxSerial field */
    private String purchaseTaxSerial;
      
    /** The value for the purchaseTaxDate field */
    private Date purchaseTaxDate;
                                                
          
    /** The value for the amount field */
    private BigDecimal amount= bd_ZERO;
                                                
          
    /** The value for the amountTax field */
    private BigDecimal amountTax= bd_ZERO;
  
    
    /**
     * Get the TransId
     *
     * @return String
     */
    public String getTransId()
    {
        return transId;
    }

                        
    /**
     * Set the value of TransId
     *
     * @param v new value
     */
    public void setTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transId, v))
              {
            this.transId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseTaxSerial
     *
     * @return String
     */
    public String getPurchaseTaxSerial()
    {
        return purchaseTaxSerial;
    }

                        
    /**
     * Set the value of PurchaseTaxSerial
     *
     * @param v new value
     */
    public void setPurchaseTaxSerial(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseTaxSerial, v))
              {
            this.purchaseTaxSerial = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseTaxDate
     *
     * @return Date
     */
    public Date getPurchaseTaxDate()
    {
        return purchaseTaxDate;
    }

                        
    /**
     * Set the value of PurchaseTaxDate
     *
     * @param v new value
     */
    public void setPurchaseTaxDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseTaxDate, v))
              {
            this.purchaseTaxDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountTax
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountTax()
    {
        return amountTax;
    }

                        
    /**
     * Set the value of AmountTax
     *
     * @param v new value
     */
    public void setAmountTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountTax, v))
              {
            this.amountTax = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("TransId");
              fieldNames.add("PurchaseTaxSerial");
              fieldNames.add("PurchaseTaxDate");
              fieldNames.add("Amount");
              fieldNames.add("AmountTax");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("TransId"))
        {
                return getTransId();
            }
          if (name.equals("PurchaseTaxSerial"))
        {
                return getPurchaseTaxSerial();
            }
          if (name.equals("PurchaseTaxDate"))
        {
                return getPurchaseTaxDate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountTax"))
        {
                return getAmountTax();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseTaxSerialPeer.TRANS_ID))
        {
                return getTransId();
            }
          if (name.equals(PurchaseTaxSerialPeer.PURCHASE_TAX_SERIAL))
        {
                return getPurchaseTaxSerial();
            }
          if (name.equals(PurchaseTaxSerialPeer.PURCHASE_TAX_DATE))
        {
                return getPurchaseTaxDate();
            }
          if (name.equals(PurchaseTaxSerialPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(PurchaseTaxSerialPeer.AMOUNT_TAX))
        {
                return getAmountTax();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getTransId();
            }
              if (pos == 1)
        {
                return getPurchaseTaxSerial();
            }
              if (pos == 2)
        {
                return getPurchaseTaxDate();
            }
              if (pos == 3)
        {
                return getAmount();
            }
              if (pos == 4)
        {
                return getAmountTax();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseTaxSerialPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseTaxSerialPeer.doInsert((PurchaseTaxSerial) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseTaxSerialPeer.doUpdate((PurchaseTaxSerial) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key transId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setTransId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setTransId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getTransId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseTaxSerial copy() throws TorqueException
    {
        return copyInto(new PurchaseTaxSerial());
    }
  
    protected PurchaseTaxSerial copyInto(PurchaseTaxSerial copyObj) throws TorqueException
    {
          copyObj.setTransId(transId);
          copyObj.setPurchaseTaxSerial(purchaseTaxSerial);
          copyObj.setPurchaseTaxDate(purchaseTaxDate);
          copyObj.setAmount(amount);
          copyObj.setAmountTax(amountTax);
  
                    copyObj.setTransId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseTaxSerialPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseTaxSerial\n");
        str.append("-----------------\n")
           .append("TransId              : ")
           .append(getTransId())
           .append("\n")
           .append("PurchaseTaxSerial    : ")
           .append(getPurchaseTaxSerial())
           .append("\n")
           .append("PurchaseTaxDate      : ")
           .append(getPurchaseTaxDate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountTax            : ")
           .append(getAmountTax())
           .append("\n")
        ;
        return(str.toString());
    }
}

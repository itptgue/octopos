package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.framework.tools.DateUtil;

public class PeriodSetup extends GLSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
    	    int  iYear = data.getParameters().getInt("Year", DateUtil.getCurrentYear());
    	    int  iOp = data.getParameters().getInt("Op", -1);
    	    if (iYear > 0)
    	    {
    	        if (iOp == 1)
    	        {
    	            try 
    	            {
    	                PeriodTool.generatePeriod(iYear, null);
    	                data.setMessage(LocaleTool.getString("period") + iYear + 
    	                	LocaleTool.getString(s_GENERATE_SUCCESS));
    	            }
    	            catch (Exception _oEx)
    	            {
    	            	log.error(_oEx);
    	                data.setMessage(LocaleTool.getString(s_GENERATE_FAILED) + _oEx.getMessage());
    	            }
    	        }
                context.put("Periods", PeriodTool.getPeriodByYear(iYear, null));
    	    }
			else {
			    context.put("Periods", PeriodTool.getAllPeriod());
		    }
		}
		catch(Exception _oEx)
		{
			log.error(_oEx);
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
		}
    }
}

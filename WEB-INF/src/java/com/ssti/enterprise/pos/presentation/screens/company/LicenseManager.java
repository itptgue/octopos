package com.ssti.enterprise.pos.presentation.screens.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Calendar;
import java.util.Date;

import org.apache.turbine.modules.screens.VelocitySecureScreen;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.license.License;
import com.ssti.framework.presentation.SecureAttributes;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class LicenseManager extends VelocitySecureScreen implements SecureAttributes
{
	
	License oLicense = null;
	int iDaysLeft = 0;
	int iExpWarn = 7;
	
	boolean bTPLValid = false;	
	String sTPL = "/WEB-INF/templates/app/screens/company/LicenseManager.vm";
	String sStatus = "FULL VERSION";
	String sLogo = "";
	String sWarning = "";
	String sExpired = "";
	String sMidCol = "</td><td>:</td>";
	
	@Override
	protected boolean isAuthorized(RunData data) throws Exception
	{
		AccessControlList acl = data.getACL();
        if (acl == null) 
        {
            data.setScreenTemplate(s_LOGIN_TPL);
            return false;
        }
        return true;
	}
	
	public void doBuildTemplate(RunData data, Context context) throws Exception 
	{
		checkTpl(data);			
		if (!bTPLValid)
		{
			throw new Exception ("Invalid License Manager Template / Probably Due  To Unauthorized License File Usage ");
		}
		try
		{						
			sLogo = data.getContextPath() + "/images/product_logo.gif";
			oLicense = (License)data.getSession().getAttribute("License");
			if (oLicense != null)
			{
				int iLicenseType = oLicense.getLicenseType();
				Date dExpired = null;

				if (iLicenseType == 1)
				{
					sExpired = CustomFormatter.formatLongDate(oLicense.getExpiredDate(),PreferenceTool.getLocale());
					dExpired = CustomParser.parseDate(oLicense.getExpiredDate());
					sStatus = "DEMO";
					Calendar cNow = Calendar.getInstance();
					Calendar cExpired = Calendar.getInstance();
					cExpired.setTime(dExpired);

					iDaysLeft = 1;
					while (cNow.before(cExpired))
					{
						cNow.add(5, 1);
						iDaysLeft++;
					}
				}				
			}
			context.put("info",getHTML());
		}
		catch (Exception _oEx)
		{
			data.setMessage(_oEx.getMessage());
		}
	}

	public String getHTML() throws Exception
	{
		StringBuilder s = new StringBuilder();
		
		s.append("<table width='90%' border='0' cellspacing='0' cellpadding='4'> ");
		s.append("  <tr><td colspan='4'><br><img src='").append(sLogo).append("'></td></tr>");

		if(iDaysLeft > 0 && iDaysLeft <= iExpWarn)
		{
			sWarning = "WARNING : LICENSE FILE WILL BE EXPIRED IN " + iDaysLeft + "DAYS";
			
			s.append("  <tr>                           ");
			s.append("    <td width='50'></td>         ");		
			s.append("    <td colspan='3'><br>         ");
			s.append("      <span class='txtMessage'>  ");
			s.append(        sWarning).append(" AT ").append(sExpired);
			s.append("       <br>Please contact support@retailsoftcloud.com  ");
			s.append("      </span>                    ");
			s.append("    </td>                        ");
			s.append("  </tr>                          ");
		} 
		s.append("  <tr><td width='10' rowspan='10'></td><td colspan='3'>&nbsp;</td></tr> ");
		if(oLicense == null)
		{
			s.append("  <tr>                                 ");
			s.append("    <td colspan='3'>                   ");
			s.append("      <span class='txtMessage'>        ");
			s.append("        &nbsp; ").append(LocaleTool.getString("invalid_warning"));
			s.append("        <br><br>                       ");
			s.append("        &nbsp; ").append(LocaleTool.getString("obtain_license"));
			s.append("      </span>                          ");
			s.append("    </td>                              ");
			s.append("  </tr>                                ");
		} 
		else
		{
			s.append("  <tr> ");
			s.append("	  <td width='20%'></td> ");
			s.append("    <td width='2%' ></td>	");
			s.append("    <td width='80%'></td> ");
			s.append("  </tr><tr>");				
			s.append("    <td>").append(LocaleTool.getString("product_name")).append(sMidCol);   
			s.append("    <td>").append(oLicense.getProductName()).append("</td> ");				
			s.append("  </tr><tr>");
			s.append("    <td>").append(LocaleTool.getString("version")).append(sMidCol);
			s.append("    <td>").append(PreferenceTool.getAppVersion()).append("</td> ");
			s.append("  </tr><tr>");				
			s.append("    <td>DB ").append(LocaleTool.getString("version")).append(sMidCol);
			s.append("    <td>").append(PreferenceTool.getDBVersion()).append("</td> ");
			s.append("  </tr><tr>");				
			s.append("    <td>").append(LocaleTool.getString("licensed_to")).append(sMidCol);
			s.append("    <td>").append(oLicense.getCompanyName()).append("</td> ");
			s.append("  </tr><tr>");				
			s.append("    <td>").append(LocaleTool.getString("install_location")).append(sMidCol);
			s.append("    <td>").append(oLicense.getInstallLocation()).append("</td> ");
			s.append("  </tr><tr>");				
			s.append("    <td>").append(LocaleTool.getString("license_status")).append(sMidCol);
			s.append("    <td><b>").append(sStatus).append("</td> 				");
			s.append("  </tr> ");
			s.append("  </tr><tr>");				
			s.append("    <td>").append(LocaleTool.getString("user")).append(sMidCol);
			s.append("    <td><b>").append(oLicense.getMaxUser()).append("</td>  ");
			s.append("  </tr> ");


			if(oLicense.getLicenseType() == 1)
			{
				if(iDaysLeft > 0 && iDaysLeft <= iExpWarn)
				{
					sExpired = "<span class='txtMessage'>" + sExpired +"</span>";
				}
				s.append("  <tr>  ");
				s.append("    <td>").append(LocaleTool.getString("expired_date")).append(sMidCol);
				s.append("    <td><b>").append(sExpired).append("</td>         ");
				s.append("  </tr> ");
			}			
		}
		s.append("</table>");
		return s.toString();
	}
	
	/**
	 * 
	 * @param data
	 * @throws Exception
	 */
	private void checkTpl(RunData data) throws Exception
	{
		//check template
		sTPL = IOTool.getRealPath(data, sTPL);
		BufferedReader br = new BufferedReader(new FileReader(sTPL));
		StringBuilder sb = new StringBuilder();
		String sLine = br.readLine();
		int iLine = 1;
		while (sLine != null) 
		{
			iLine++;
			sb.append(sLine);
			sb.append("\n");
			sLine = br.readLine();			
			if (iLine == 3 && StringUtil.isEqual(sLine, "$info"))
			{
				bTPLValid = true;
			}							
		}
		br.close();
	}

}

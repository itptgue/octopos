package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountBalance;
import com.ssti.enterprise.pos.tools.gl.AccountBalanceTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate Account Balance value & GL Transaction 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: GLValidator.java,v 1.5 2007/12/20 13:48:25 albert Exp $ <br>
 *
 * <pre>
 * $Log: GLValidator.java,v $
 * Revision 1.5  2007/12/20 13:48:25  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class GLValidator extends BaseValidator implements GlAttributes
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Account Code", "Account Name", "Debit", "Credit", "Activity Balance", "Current Balance"};
	static final int[] l1 = {5, 16, 25, 20, 20, 20, 20};
	static final int[] a1 = {0, 0, 0, 1, 1, 1, 1};

	public void validateBalance()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate GL Result : ", s1, l1, a1);			
		
		try
		{							
			List vData = AccountTool.getAllAccount();
			
			for (int i = 0; i < vData.size(); i++)
			{
				Account oData = (Account) vData.get(i);
				String sID = oData.getAccountId();
			
				Date dAsOf = oData.getAsDate();
				
				AccountBalance oBalance = AccountBalanceTool.getDataByAccountID(sID);
				if (oBalance != null)
				{
					double dBaseBalance = oBalance.getBalance().setScale(2,4).doubleValue();
					double dPrimeBalance = oBalance.getPrimeBalance().setScale(2,4).doubleValue();
					
		            double[] dDebit = GlTransactionTool.getChanges (sID, "", "", dAsOf, dNow, GlAttributes.i_DEBIT);
		            double[] dCredit = GlTransactionTool.getChanges (sID, "", "", dAsOf, dNow, GlAttributes.i_CREDIT);		            
		            double[] dTrans = new double[2];
		            
		            if (oData.getNormalBalance() == GlAttributes.i_DEBIT)
		            {
		                dTrans[i_BASE]  = dDebit[i_BASE]  - dCredit[i_BASE];
		                dTrans[i_PRIME] = dDebit[i_PRIME] - dCredit[i_PRIME];
		            }
		            else if (oData.getNormalBalance() == GlAttributes.i_CREDIT)
		            {
		                dTrans[i_BASE]  = dCredit[i_BASE]  - dDebit[i_BASE];
		                dTrans[i_PRIME] = dCredit[i_PRIME] - dDebit[i_PRIME];
		            }
					dTrans[i_BASE] = (new BigDecimal(dTrans[i_BASE]).setScale(2,4)).doubleValue();
					dTrans[i_PRIME] = (new BigDecimal(dTrans[i_PRIME]).setScale(2,4)).doubleValue();
		            
					if (dBaseBalance != dTrans[i_BASE])
					{
						m_iInvalidResult ++;
						m_oResult.append("\n");
						
						append (null, l1[0]);
						append (StringUtil.cut(oData.getAccountCode(),14,".."), l1[1]);
						append (StringUtil.cut(oData.getAccountName(),22,".."), l1[2]);
						
						append (new Double(dDebit[i_BASE]), l1[3]);
						append (new Double(dCredit[i_BASE]), l1[4]);
						
						append (new Double(dTrans[i_BASE]), l1[5]);
						append (new Double(dBaseBalance), l1[6]);
					}
				}
			}
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
}

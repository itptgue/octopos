package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class LocationCtlMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.LocationCtlMapBuilder";

    /**
     * Item
     * @deprecated use LocationCtlPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "location_ctl";
    }

  
    /**
     * location_ctl.LOCATION_CTL_ID
     * @return the column name for the LOCATION_CTL_ID field
     * @deprecated use LocationCtlPeer.location_ctl.LOCATION_CTL_ID constant
     */
    public static String getLocationCtl_LocationCtlId()
    {
        return "location_ctl.LOCATION_CTL_ID";
    }
  
    /**
     * location_ctl.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use LocationCtlPeer.location_ctl.LOCATION_ID constant
     */
    public static String getLocationCtl_LocationId()
    {
        return "location_ctl.LOCATION_ID";
    }
  
    /**
     * location_ctl.TRANS_CODE
     * @return the column name for the TRANS_CODE field
     * @deprecated use LocationCtlPeer.location_ctl.TRANS_CODE constant
     */
    public static String getLocationCtl_TransCode()
    {
        return "location_ctl.TRANS_CODE";
    }
  
    /**
     * location_ctl.CTL_TABLE
     * @return the column name for the CTL_TABLE field
     * @deprecated use LocationCtlPeer.location_ctl.CTL_TABLE constant
     */
    public static String getLocationCtl_CtlTable()
    {
        return "location_ctl.CTL_TABLE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("location_ctl");
        TableMap tMap = dbMap.getTable("location_ctl");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("location_ctl.LOCATION_CTL_ID", "");
                          tMap.addColumn("location_ctl.LOCATION_ID", "");
                          tMap.addColumn("location_ctl.TRANS_CODE", "");
                          tMap.addColumn("location_ctl.CTL_TABLE", "");
          }
}

package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2016-03-01
 * - call ItemView.setUserPref to set selected user fields
 * 
 * </pre><br>
 */
public class ItemLookup extends TransactionSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");
			String sDisplayItemID = data.getParameters().getString ("DisplayItemID", "");
			if (!sDisplayItemID.equals(""))
    		{
    			context.put ("ItemQtyDetails", InventoryLocationTool.getDataByItemID(sDisplayItemID));
    		}
			
			com.ssti.enterprise.pos.presentation.screens.setup.ItemView.setUserPref(data,context);
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}    	
    }
}
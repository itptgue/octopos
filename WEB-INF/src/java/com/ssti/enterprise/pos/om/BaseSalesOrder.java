package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to SalesOrder
 */
public abstract class BaseSalesOrder extends BaseObject
{
    /** The Peer class */
    private static final SalesOrderPeer peer =
        new SalesOrderPeer();

        
    /** The value for the salesOrderId field */
    private String salesOrderId;
      
    /** The value for the transactionStatus field */
    private int transactionStatus;
      
    /** The value for the salesOrderNo field */
    private String salesOrderNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the dueDate field */
    private Date dueDate;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the customerPoNo field */
    private String customerPoNo;
      
    /** The value for the customerPoDate field */
    private Date customerPoDate;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the totalDiscount field */
    private BigDecimal totalDiscount;
      
    /** The value for the totalFee field */
    private BigDecimal totalFee;
      
    /** The value for the totalTax field */
    private BigDecimal totalTax;
      
    /** The value for the remark field */
    private String remark;
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
      
    /** The value for the locationId field */
    private String locationId;
                                          
    /** The value for the printTimes field */
    private int printTimes = 0;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the confirmBy field */
    private String confirmBy;
      
    /** The value for the confirmDate field */
    private Date confirmDate;
                                                
          
    /** The value for the downPayment field */
    private BigDecimal downPayment= bd_ZERO;
                                                
    /** The value for the shipTo field */
    private String shipTo = "";
      
    /** The value for the shipDate field */
    private Date shipDate;
                                                
    /** The value for the bankId field */
    private String bankId = "";
                                                
    /** The value for the bankIssuer field */
    private String bankIssuer = "";
                                                
    /** The value for the dpAccountId field */
    private String dpAccountId = "";
      
    /** The value for the dpDueDate field */
    private Date dpDueDate;
                                                
    /** The value for the referenceNo field */
    private String referenceNo = "";
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
                                                
    /** The value for the salesId field */
    private String salesId = "";
                                                
    /** The value for the courierId field */
    private String courierId = "";
                                                
    /** The value for the fobId field */
    private String fobId = "";
                                                
          
    /** The value for the estimatedFreight field */
    private BigDecimal estimatedFreight= bd_ZERO;
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
                                                
    /** The value for the freightAccountId field */
    private String freightAccountId = "";
      
    /** The value for the isInclusiveFreight field */
    private boolean isInclusiveFreight;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the SalesOrderId
     *
     * @return String
     */
    public String getSalesOrderId()
    {
        return salesOrderId;
    }

                                              
    /**
     * Set the value of SalesOrderId
     *
     * @param v new value
     */
    public void setSalesOrderId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.salesOrderId, v))
              {
            this.salesOrderId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated SalesOrderDetail
        if (collSalesOrderDetails != null)
        {
            for (int i = 0; i < collSalesOrderDetails.size(); i++)
            {
                ((SalesOrderDetail) collSalesOrderDetails.get(i))
                    .setSalesOrderId(v);
            }
        }
                                }
  
    /**
     * Get the TransactionStatus
     *
     * @return int
     */
    public int getTransactionStatus()
    {
        return transactionStatus;
    }

                        
    /**
     * Set the value of TransactionStatus
     *
     * @param v new value
     */
    public void setTransactionStatus(int v) 
    {
    
                  if (this.transactionStatus != v)
              {
            this.transactionStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesOrderNo
     *
     * @return String
     */
    public String getSalesOrderNo()
    {
        return salesOrderNo;
    }

                        
    /**
     * Set the value of SalesOrderNo
     *
     * @param v new value
     */
    public void setSalesOrderNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesOrderNo, v))
              {
            this.salesOrderNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerPoNo
     *
     * @return String
     */
    public String getCustomerPoNo()
    {
        return customerPoNo;
    }

                        
    /**
     * Set the value of CustomerPoNo
     *
     * @param v new value
     */
    public void setCustomerPoNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerPoNo, v))
              {
            this.customerPoNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerPoDate
     *
     * @return Date
     */
    public Date getCustomerPoDate()
    {
        return customerPoDate;
    }

                        
    /**
     * Set the value of CustomerPoDate
     *
     * @param v new value
     */
    public void setCustomerPoDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.customerPoDate, v))
              {
            this.customerPoDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalDiscount()
    {
        return totalDiscount;
    }

                        
    /**
     * Set the value of TotalDiscount
     *
     * @param v new value
     */
    public void setTotalDiscount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscount, v))
              {
            this.totalDiscount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalFee
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalFee()
    {
        return totalFee;
    }

                        
    /**
     * Set the value of TotalFee
     *
     * @param v new value
     */
    public void setTotalFee(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalFee, v))
              {
            this.totalFee = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalTax
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalTax()
    {
        return totalTax;
    }

                        
    /**
     * Set the value of TotalTax
     *
     * @param v new value
     */
    public void setTotalTax(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalTax, v))
              {
            this.totalTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrintTimes
     *
     * @return int
     */
    public int getPrintTimes()
    {
        return printTimes;
    }

                        
    /**
     * Set the value of PrintTimes
     *
     * @param v new value
     */
    public void setPrintTimes(int v) 
    {
    
                  if (this.printTimes != v)
              {
            this.printTimes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmBy
     *
     * @return String
     */
    public String getConfirmBy()
    {
        return confirmBy;
    }

                        
    /**
     * Set the value of ConfirmBy
     *
     * @param v new value
     */
    public void setConfirmBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmBy, v))
              {
            this.confirmBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmDate
     *
     * @return Date
     */
    public Date getConfirmDate()
    {
        return confirmDate;
    }

                        
    /**
     * Set the value of ConfirmDate
     *
     * @param v new value
     */
    public void setConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmDate, v))
              {
            this.confirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DownPayment
     *
     * @return BigDecimal
     */
    public BigDecimal getDownPayment()
    {
        return downPayment;
    }

                        
    /**
     * Set the value of DownPayment
     *
     * @param v new value
     */
    public void setDownPayment(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.downPayment, v))
              {
            this.downPayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShipTo
     *
     * @return String
     */
    public String getShipTo()
    {
        return shipTo;
    }

                        
    /**
     * Set the value of ShipTo
     *
     * @param v new value
     */
    public void setShipTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shipTo, v))
              {
            this.shipTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShipDate
     *
     * @return Date
     */
    public Date getShipDate()
    {
        return shipDate;
    }

                        
    /**
     * Set the value of ShipDate
     *
     * @param v new value
     */
    public void setShipDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.shipDate, v))
              {
            this.shipDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DpAccountId
     *
     * @return String
     */
    public String getDpAccountId()
    {
        return dpAccountId;
    }

                        
    /**
     * Set the value of DpAccountId
     *
     * @param v new value
     */
    public void setDpAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.dpAccountId, v))
              {
            this.dpAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DpDueDate
     *
     * @return Date
     */
    public Date getDpDueDate()
    {
        return dpDueDate;
    }

                        
    /**
     * Set the value of DpDueDate
     *
     * @param v new value
     */
    public void setDpDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dpDueDate, v))
              {
            this.dpDueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesId
     *
     * @return String
     */
    public String getSalesId()
    {
        return salesId;
    }

                        
    /**
     * Set the value of SalesId
     *
     * @param v new value
     */
    public void setSalesId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesId, v))
              {
            this.salesId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FobId
     *
     * @return String
     */
    public String getFobId()
    {
        return fobId;
    }

                        
    /**
     * Set the value of FobId
     *
     * @param v new value
     */
    public void setFobId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fobId, v))
              {
            this.fobId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EstimatedFreight
     *
     * @return BigDecimal
     */
    public BigDecimal getEstimatedFreight()
    {
        return estimatedFreight;
    }

                        
    /**
     * Set the value of EstimatedFreight
     *
     * @param v new value
     */
    public void setEstimatedFreight(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.estimatedFreight, v))
              {
            this.estimatedFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FreightAccountId
     *
     * @return String
     */
    public String getFreightAccountId()
    {
        return freightAccountId;
    }

                        
    /**
     * Set the value of FreightAccountId
     *
     * @param v new value
     */
    public void setFreightAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.freightAccountId, v))
              {
            this.freightAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveFreight
     *
     * @return boolean
     */
    public boolean getIsInclusiveFreight()
    {
        return isInclusiveFreight;
    }

                        
    /**
     * Set the value of IsInclusiveFreight
     *
     * @param v new value
     */
    public void setIsInclusiveFreight(boolean v) 
    {
    
                  if (this.isInclusiveFreight != v)
              {
            this.isInclusiveFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collSalesOrderDetails
     */
    protected List collSalesOrderDetails;

    /**
     * Temporary storage of collSalesOrderDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initSalesOrderDetails()
    {
        if (collSalesOrderDetails == null)
        {
            collSalesOrderDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a SalesOrderDetail object to this object
     * through the SalesOrderDetail foreign key attribute
     *
     * @param l SalesOrderDetail
     * @throws TorqueException
     */
    public void addSalesOrderDetail(SalesOrderDetail l) throws TorqueException
    {
        getSalesOrderDetails().add(l);
        l.setSalesOrder((SalesOrder) this);
    }

    /**
     * The criteria used to select the current contents of collSalesOrderDetails
     */
    private Criteria lastSalesOrderDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getSalesOrderDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getSalesOrderDetails() throws TorqueException
    {
              if (collSalesOrderDetails == null)
        {
            collSalesOrderDetails = getSalesOrderDetails(new Criteria(10));
        }
        return collSalesOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesOrder has previously
     * been saved, it will retrieve related SalesOrderDetails from storage.
     * If this SalesOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getSalesOrderDetails(Criteria criteria) throws TorqueException
    {
              if (collSalesOrderDetails == null)
        {
            if (isNew())
            {
               collSalesOrderDetails = new ArrayList();
            }
            else
            {
                        criteria.add(SalesOrderDetailPeer.SALES_ORDER_ID, getSalesOrderId() );
                        collSalesOrderDetails = SalesOrderDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(SalesOrderDetailPeer.SALES_ORDER_ID, getSalesOrderId());
                            if (!lastSalesOrderDetailsCriteria.equals(criteria))
                {
                    collSalesOrderDetails = SalesOrderDetailPeer.doSelect(criteria);
                }
            }
        }
        lastSalesOrderDetailsCriteria = criteria;

        return collSalesOrderDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getSalesOrderDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getSalesOrderDetails(Connection con) throws TorqueException
    {
              if (collSalesOrderDetails == null)
        {
            collSalesOrderDetails = getSalesOrderDetails(new Criteria(10), con);
        }
        return collSalesOrderDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesOrder has previously
     * been saved, it will retrieve related SalesOrderDetails from storage.
     * If this SalesOrder is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getSalesOrderDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collSalesOrderDetails == null)
        {
            if (isNew())
            {
               collSalesOrderDetails = new ArrayList();
            }
            else
            {
                         criteria.add(SalesOrderDetailPeer.SALES_ORDER_ID, getSalesOrderId());
                         collSalesOrderDetails = SalesOrderDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(SalesOrderDetailPeer.SALES_ORDER_ID, getSalesOrderId());
                             if (!lastSalesOrderDetailsCriteria.equals(criteria))
                 {
                     collSalesOrderDetails = SalesOrderDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastSalesOrderDetailsCriteria = criteria;

         return collSalesOrderDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SalesOrder is new, it will return
     * an empty collection; or if this SalesOrder has previously
     * been saved, it will retrieve related SalesOrderDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SalesOrder.
     */
    protected List getSalesOrderDetailsJoinSalesOrder(Criteria criteria)
        throws TorqueException
    {
                    if (collSalesOrderDetails == null)
        {
            if (isNew())
            {
               collSalesOrderDetails = new ArrayList();
            }
            else
            {
                              criteria.add(SalesOrderDetailPeer.SALES_ORDER_ID, getSalesOrderId());
                              collSalesOrderDetails = SalesOrderDetailPeer.doSelectJoinSalesOrder(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(SalesOrderDetailPeer.SALES_ORDER_ID, getSalesOrderId());
                                    if (!lastSalesOrderDetailsCriteria.equals(criteria))
            {
                collSalesOrderDetails = SalesOrderDetailPeer.doSelectJoinSalesOrder(criteria);
            }
        }
        lastSalesOrderDetailsCriteria = criteria;

        return collSalesOrderDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("SalesOrderId");
              fieldNames.add("TransactionStatus");
              fieldNames.add("SalesOrderNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("DueDate");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("CustomerPoNo");
              fieldNames.add("CustomerPoDate");
              fieldNames.add("TotalQty");
              fieldNames.add("TotalAmount");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("TotalDiscount");
              fieldNames.add("TotalFee");
              fieldNames.add("TotalTax");
              fieldNames.add("Remark");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("LocationId");
              fieldNames.add("PrintTimes");
              fieldNames.add("UserName");
              fieldNames.add("ConfirmBy");
              fieldNames.add("ConfirmDate");
              fieldNames.add("DownPayment");
              fieldNames.add("ShipTo");
              fieldNames.add("ShipDate");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("DpAccountId");
              fieldNames.add("DpDueDate");
              fieldNames.add("ReferenceNo");
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("SalesId");
              fieldNames.add("CourierId");
              fieldNames.add("FobId");
              fieldNames.add("EstimatedFreight");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("FiscalRate");
              fieldNames.add("FreightAccountId");
              fieldNames.add("IsInclusiveFreight");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("SalesOrderId"))
        {
                return getSalesOrderId();
            }
          if (name.equals("TransactionStatus"))
        {
                return Integer.valueOf(getTransactionStatus());
            }
          if (name.equals("SalesOrderNo"))
        {
                return getSalesOrderNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("CustomerPoNo"))
        {
                return getCustomerPoNo();
            }
          if (name.equals("CustomerPoDate"))
        {
                return getCustomerPoDate();
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("TotalDiscount"))
        {
                return getTotalDiscount();
            }
          if (name.equals("TotalFee"))
        {
                return getTotalFee();
            }
          if (name.equals("TotalTax"))
        {
                return getTotalTax();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("PrintTimes"))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("ConfirmBy"))
        {
                return getConfirmBy();
            }
          if (name.equals("ConfirmDate"))
        {
                return getConfirmDate();
            }
          if (name.equals("DownPayment"))
        {
                return getDownPayment();
            }
          if (name.equals("ShipTo"))
        {
                return getShipTo();
            }
          if (name.equals("ShipDate"))
        {
                return getShipDate();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("DpAccountId"))
        {
                return getDpAccountId();
            }
          if (name.equals("DpDueDate"))
        {
                return getDpDueDate();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("SalesId"))
        {
                return getSalesId();
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("FobId"))
        {
                return getFobId();
            }
          if (name.equals("EstimatedFreight"))
        {
                return getEstimatedFreight();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("FreightAccountId"))
        {
                return getFreightAccountId();
            }
          if (name.equals("IsInclusiveFreight"))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(SalesOrderPeer.SALES_ORDER_ID))
        {
                return getSalesOrderId();
            }
          if (name.equals(SalesOrderPeer.TRANSACTION_STATUS))
        {
                return Integer.valueOf(getTransactionStatus());
            }
          if (name.equals(SalesOrderPeer.SALES_ORDER_NO))
        {
                return getSalesOrderNo();
            }
          if (name.equals(SalesOrderPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(SalesOrderPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(SalesOrderPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(SalesOrderPeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(SalesOrderPeer.CUSTOMER_PO_NO))
        {
                return getCustomerPoNo();
            }
          if (name.equals(SalesOrderPeer.CUSTOMER_PO_DATE))
        {
                return getCustomerPoDate();
            }
          if (name.equals(SalesOrderPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(SalesOrderPeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(SalesOrderPeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(SalesOrderPeer.TOTAL_DISCOUNT))
        {
                return getTotalDiscount();
            }
          if (name.equals(SalesOrderPeer.TOTAL_FEE))
        {
                return getTotalFee();
            }
          if (name.equals(SalesOrderPeer.TOTAL_TAX))
        {
                return getTotalTax();
            }
          if (name.equals(SalesOrderPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(SalesOrderPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(SalesOrderPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(SalesOrderPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(SalesOrderPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(SalesOrderPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(SalesOrderPeer.PRINT_TIMES))
        {
                return Integer.valueOf(getPrintTimes());
            }
          if (name.equals(SalesOrderPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(SalesOrderPeer.CONFIRM_BY))
        {
                return getConfirmBy();
            }
          if (name.equals(SalesOrderPeer.CONFIRM_DATE))
        {
                return getConfirmDate();
            }
          if (name.equals(SalesOrderPeer.DOWN_PAYMENT))
        {
                return getDownPayment();
            }
          if (name.equals(SalesOrderPeer.SHIP_TO))
        {
                return getShipTo();
            }
          if (name.equals(SalesOrderPeer.SHIP_DATE))
        {
                return getShipDate();
            }
          if (name.equals(SalesOrderPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(SalesOrderPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(SalesOrderPeer.DP_ACCOUNT_ID))
        {
                return getDpAccountId();
            }
          if (name.equals(SalesOrderPeer.DP_DUE_DATE))
        {
                return getDpDueDate();
            }
          if (name.equals(SalesOrderPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(SalesOrderPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(SalesOrderPeer.SALES_ID))
        {
                return getSalesId();
            }
          if (name.equals(SalesOrderPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(SalesOrderPeer.FOB_ID))
        {
                return getFobId();
            }
          if (name.equals(SalesOrderPeer.ESTIMATED_FREIGHT))
        {
                return getEstimatedFreight();
            }
          if (name.equals(SalesOrderPeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(SalesOrderPeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(SalesOrderPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(SalesOrderPeer.FREIGHT_ACCOUNT_ID))
        {
                return getFreightAccountId();
            }
          if (name.equals(SalesOrderPeer.IS_INCLUSIVE_FREIGHT))
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
          if (name.equals(SalesOrderPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(SalesOrderPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getSalesOrderId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getTransactionStatus());
            }
              if (pos == 2)
        {
                return getSalesOrderNo();
            }
              if (pos == 3)
        {
                return getTransactionDate();
            }
              if (pos == 4)
        {
                return getDueDate();
            }
              if (pos == 5)
        {
                return getCustomerId();
            }
              if (pos == 6)
        {
                return getCustomerName();
            }
              if (pos == 7)
        {
                return getCustomerPoNo();
            }
              if (pos == 8)
        {
                return getCustomerPoDate();
            }
              if (pos == 9)
        {
                return getTotalQty();
            }
              if (pos == 10)
        {
                return getTotalAmount();
            }
              if (pos == 11)
        {
                return getTotalDiscountPct();
            }
              if (pos == 12)
        {
                return getTotalDiscount();
            }
              if (pos == 13)
        {
                return getTotalFee();
            }
              if (pos == 14)
        {
                return getTotalTax();
            }
              if (pos == 15)
        {
                return getRemark();
            }
              if (pos == 16)
        {
                return getPaymentTypeId();
            }
              if (pos == 17)
        {
                return getPaymentTermId();
            }
              if (pos == 18)
        {
                return getCurrencyId();
            }
              if (pos == 19)
        {
                return getCurrencyRate();
            }
              if (pos == 20)
        {
                return getLocationId();
            }
              if (pos == 21)
        {
                return Integer.valueOf(getPrintTimes());
            }
              if (pos == 22)
        {
                return getUserName();
            }
              if (pos == 23)
        {
                return getConfirmBy();
            }
              if (pos == 24)
        {
                return getConfirmDate();
            }
              if (pos == 25)
        {
                return getDownPayment();
            }
              if (pos == 26)
        {
                return getShipTo();
            }
              if (pos == 27)
        {
                return getShipDate();
            }
              if (pos == 28)
        {
                return getBankId();
            }
              if (pos == 29)
        {
                return getBankIssuer();
            }
              if (pos == 30)
        {
                return getDpAccountId();
            }
              if (pos == 31)
        {
                return getDpDueDate();
            }
              if (pos == 32)
        {
                return getReferenceNo();
            }
              if (pos == 33)
        {
                return getCashFlowTypeId();
            }
              if (pos == 34)
        {
                return getSalesId();
            }
              if (pos == 35)
        {
                return getCourierId();
            }
              if (pos == 36)
        {
                return getFobId();
            }
              if (pos == 37)
        {
                return getEstimatedFreight();
            }
              if (pos == 38)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 39)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 40)
        {
                return getFiscalRate();
            }
              if (pos == 41)
        {
                return getFreightAccountId();
            }
              if (pos == 42)
        {
                return Boolean.valueOf(getIsInclusiveFreight());
            }
              if (pos == 43)
        {
                return getCancelBy();
            }
              if (pos == 44)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(SalesOrderPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        SalesOrderPeer.doInsert((SalesOrder) this, con);
                        setNew(false);
                    }
                    else
                    {
                        SalesOrderPeer.doUpdate((SalesOrder) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collSalesOrderDetails != null)
            {
                for (int i = 0; i < collSalesOrderDetails.size(); i++)
                {
                    ((SalesOrderDetail) collSalesOrderDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key salesOrderId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setSalesOrderId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setSalesOrderId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getSalesOrderId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public SalesOrder copy() throws TorqueException
    {
        return copyInto(new SalesOrder());
    }
  
    protected SalesOrder copyInto(SalesOrder copyObj) throws TorqueException
    {
          copyObj.setSalesOrderId(salesOrderId);
          copyObj.setTransactionStatus(transactionStatus);
          copyObj.setSalesOrderNo(salesOrderNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setDueDate(dueDate);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setCustomerPoNo(customerPoNo);
          copyObj.setCustomerPoDate(customerPoDate);
          copyObj.setTotalQty(totalQty);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setTotalDiscount(totalDiscount);
          copyObj.setTotalFee(totalFee);
          copyObj.setTotalTax(totalTax);
          copyObj.setRemark(remark);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setLocationId(locationId);
          copyObj.setPrintTimes(printTimes);
          copyObj.setUserName(userName);
          copyObj.setConfirmBy(confirmBy);
          copyObj.setConfirmDate(confirmDate);
          copyObj.setDownPayment(downPayment);
          copyObj.setShipTo(shipTo);
          copyObj.setShipDate(shipDate);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setDpAccountId(dpAccountId);
          copyObj.setDpDueDate(dpDueDate);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setSalesId(salesId);
          copyObj.setCourierId(courierId);
          copyObj.setFobId(fobId);
          copyObj.setEstimatedFreight(estimatedFreight);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setFreightAccountId(freightAccountId);
          copyObj.setIsInclusiveFreight(isInclusiveFreight);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setSalesOrderId((String)null);
                                                                                                                                                                                                                                                                                    
                                      
                            
        List v = getSalesOrderDetails();
        for (int i = 0; i < v.size(); i++)
        {
            SalesOrderDetail obj = (SalesOrderDetail) v.get(i);
            copyObj.addSalesOrderDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public SalesOrderPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("SalesOrder\n");
        str.append("----------\n")
           .append("SalesOrderId         : ")
           .append(getSalesOrderId())
           .append("\n")
           .append("TransactionStatus    : ")
           .append(getTransactionStatus())
           .append("\n")
           .append("SalesOrderNo         : ")
           .append(getSalesOrderNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("CustomerPoNo         : ")
           .append(getCustomerPoNo())
           .append("\n")
           .append("CustomerPoDate       : ")
           .append(getCustomerPoDate())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("TotalDiscount        : ")
           .append(getTotalDiscount())
           .append("\n")
           .append("TotalFee             : ")
           .append(getTotalFee())
           .append("\n")
           .append("TotalTax             : ")
           .append(getTotalTax())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("PrintTimes           : ")
           .append(getPrintTimes())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("ConfirmBy            : ")
           .append(getConfirmBy())
           .append("\n")
           .append("ConfirmDate          : ")
           .append(getConfirmDate())
           .append("\n")
           .append("DownPayment          : ")
           .append(getDownPayment())
           .append("\n")
           .append("ShipTo               : ")
           .append(getShipTo())
           .append("\n")
           .append("ShipDate             : ")
           .append(getShipDate())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("DpAccountId          : ")
           .append(getDpAccountId())
           .append("\n")
           .append("DpDueDate            : ")
           .append(getDpDueDate())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("SalesId              : ")
           .append(getSalesId())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("FobId                : ")
           .append(getFobId())
           .append("\n")
           .append("EstimatedFreight     : ")
           .append(getEstimatedFreight())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("FreightAccountId     : ")
           .append(getFreightAccountId())
           .append("\n")
           .append("IsInclusiveFreight   : ")
           .append(getIsInclusiveFreight())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

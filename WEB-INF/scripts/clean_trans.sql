
-- auto-schema.xml
TRUNCATE TABLE vehicle_sales CASCADE;
TRUNCATE TABLE vehicle_sales_detail CASCADE;
-- TRUNCATE TABLE work_order CASCADE;
-- TRUNCATE TABLE work_order_svc CASCADE;
-- TRUNCATE TABLE work_order_usage CASCADE;
-- TRUNCATE TABLE work_order_part CASCADE;
-- TRUNCATE TABLE work_order_ctl CASCADE;

-- ctl-schema.xml
TRUNCATE TABLE ctl_sales_order CASCADE;
TRUNCATE TABLE ctl_delivery_order CASCADE;
TRUNCATE TABLE ctl_sales_invoice CASCADE;
TRUNCATE TABLE ctl_sales_return CASCADE;
TRUNCATE TABLE ctl_purchase_request CASCADE;
TRUNCATE TABLE ctl_purchase_order CASCADE;
TRUNCATE TABLE ctl_purchase_receipt CASCADE;
TRUNCATE TABLE ctl_purchase_invoice CASCADE;
TRUNCATE TABLE ctl_purchase_return CASCADE;
TRUNCATE TABLE ctl_issue_receipt CASCADE;
TRUNCATE TABLE ctl_item_transfer CASCADE;
TRUNCATE TABLE ctl_cycle_count CASCADE;
TRUNCATE TABLE ctl_debit_memo CASCADE;
TRUNCATE TABLE ctl_credit_memo CASCADE;
TRUNCATE TABLE ctl_ar_payment CASCADE;
TRUNCATE TABLE ctl_ap_payment CASCADE;
TRUNCATE TABLE ctl_bank_transfer CASCADE;
TRUNCATE TABLE ctl_cash_flow CASCADE;
TRUNCATE TABLE ctl_cash_flow_in CASCADE;
TRUNCATE TABLE ctl_cash_flow_out CASCADE;
TRUNCATE TABLE ctl_petty_cash CASCADE;
TRUNCATE TABLE ctl_journal_voucher CASCADE;
TRUNCATE TABLE ctl_fixed_asset CASCADE;
TRUNCATE TABLE ctl_job_costing CASCADE;
TRUNCATE TABLE ctl_packing_list CASCADE;
TRUNCATE TABLE ctl_opening_balance CASCADE;
TRUNCATE TABLE ctl_point_reward CASCADE;
TRUNCATE TABLE ctl_tax_invoice CASCADE;  
TRUNCATE TABLE ctl_tax_do CASCADE;  
TRUNCATE TABLE ctl_tax_so CASCADE;  
TRUNCATE TABLE ctl_cost_adjustment CASCADE;
TRUNCATE TABLE ctl_quotation CASCADE;
TRUNCATE TABLE ctl_customer_trans CASCADE;
TRUNCATE TABLE ctl_vendor_trans CASCADE;
TRUNCATE TABLE ctl_lend_in CASCADE;
TRUNCATE TABLE ctl_lend_out CASCADE;
 
-- customreport-schema.xml

-- disc-cs-schema.xml

-- fixedasset-schema.xml
TRUNCATE TABLE fixed_asset CASCADE;
TRUNCATE TABLE fixed_asset_expend CASCADE;
TRUNCATE TABLE fixed_asset_notes CASCADE;
TRUNCATE TABLE fixed_asset_svc CASCADE;
TRUNCATE TABLE fixed_asset_pic CASCADE;
TRUNCATE TABLE fixed_asset_depr CASCADE;
TRUNCATE TABLE fixed_asset_reval CASCADE;
TRUNCATE TABLE fixed_asset_transfer CASCADE;

-- jobcost-schema.xml
TRUNCATE TABLE job_costing CASCADE;
TRUNCATE TABLE job_costing_detail CASCADE;
TRUNCATE TABLE job_costing_expense CASCADE;
TRUNCATE TABLE jc_rollover CASCADE;
TRUNCATE TABLE jc_rollover_detail CASCADE;
TRUNCATE TABLE jc_rollover_account CASCADE;

-- lending-schema.xml
 TRUNCATE TABLE lending CASCADE;

-- medical-schema.xml

TRUNCATE TABLE prescription_ctl CASCADE;
TRUNCATE TABLE prescription CASCADE;
TRUNCATE TABLE prescription_detail CASCADE;
TRUNCATE TABLE pres_powder_detail CASCADE;
-- TRUNCATE TABLE checkup_result CASCADE;
-- TRUNCATE TABLE medical_record CASCADE;
-- TRUNCATE TABLE medical_record_item CASCADE;
-- TRUNCATE TABLE medical_record_ctl CASCADE;
TRUNCATE TABLE medicine_sales CASCADE;
TRUNCATE TABLE medicine_sales_detail CASCADE;
-- TRUNCATE TABLE med_order CASCADE;
-- TRUNCATE TABLE med_order_detail CASCADE;
-- TRUNCATE TABLE reservation CASCADE;
-- TRUNCATE TABLE reservation_ctl CASCADE;
-- TRUNCATE TABLE registration CASCADE;
-- TRUNCATE TABLE registration_ctl CASCADE;
-- TRUNCATE TABLE lab_order CASCADE;
-- TRUNCATE TABLE lab_order_detail CASCADE;
-- TRUNCATE TABLE lab_order_ctl CASCADE;
-- 
 
-- pointreward-schema.xml
TRUNCATE TABLE point_transaction CASCADE;
TRUNCATE TABLE customer_point CASCADE;

-- pos-schema.xml
TRUNCATE TABLE gl_transaction CASCADE;
TRUNCATE TABLE account_balance CASCADE;
TRUNCATE TABLE sub_balance CASCADE;
TRUNCATE TABLE journal_voucher CASCADE;
TRUNCATE TABLE journal_voucher_detail CASCADE;
TRUNCATE TABLE recurring_transaction CASCADE;
TRUNCATE TABLE serial_transaction CASCADE;
TRUNCATE TABLE inventory_transaction CASCADE;
TRUNCATE TABLE fifo_in CASCADE;
TRUNCATE TABLE fifo_out CASCADE;
TRUNCATE TABLE inventory_location CASCADE;
TRUNCATE TABLE batch_location CASCADE;
TRUNCATE TABLE batch_transaction CASCADE;
TRUNCATE TABLE issue_receipt CASCADE;
TRUNCATE TABLE issue_receipt_detail CASCADE;
TRUNCATE TABLE item_transfer CASCADE;
TRUNCATE TABLE item_transfer_detail CASCADE;
TRUNCATE TABLE cycle_count CASCADE;
TRUNCATE TABLE cycle_count_detail CASCADE;
TRUNCATE TABLE quotation CASCADE;
TRUNCATE TABLE quotation_detail CASCADE;
TRUNCATE TABLE sales_order CASCADE;
TRUNCATE TABLE sales_order_detail CASCADE;
TRUNCATE TABLE delivery_order CASCADE;
TRUNCATE TABLE delivery_order_detail CASCADE;
TRUNCATE TABLE sales_transaction CASCADE;
TRUNCATE TABLE sales_transaction_detail CASCADE;
TRUNCATE TABLE invoice_payment CASCADE;
TRUNCATE TABLE item_exchange CASCADE;
TRUNCATE TABLE item_exchange_detail CASCADE;
TRUNCATE TABLE drawer_log CASCADE;
TRUNCATE TABLE printing_log CASCADE;
TRUNCATE TABLE sales_return CASCADE;
TRUNCATE TABLE sales_return_detail CASCADE;
TRUNCATE TABLE customer_balance CASCADE;
TRUNCATE TABLE account_receivable CASCADE;
TRUNCATE TABLE ar_payment CASCADE;
TRUNCATE TABLE ar_payment_detail CASCADE;
TRUNCATE TABLE credit_memo CASCADE;
TRUNCATE TABLE purchase_request CASCADE;
TRUNCATE TABLE purchase_request_detail CASCADE;
TRUNCATE TABLE purchase_order CASCADE;
TRUNCATE TABLE purchase_order_detail CASCADE;
TRUNCATE TABLE purchase_receipt CASCADE;
TRUNCATE TABLE purchase_receipt_detail CASCADE;
TRUNCATE TABLE purchase_invoice CASCADE;
TRUNCATE TABLE purchase_invoice_detail CASCADE;
TRUNCATE TABLE purchase_invoice_freight CASCADE;
TRUNCATE TABLE purchase_return CASCADE;
TRUNCATE TABLE purchase_return_detail CASCADE;
TRUNCATE TABLE packing_list CASCADE;
TRUNCATE TABLE packing_list_detail CASCADE;
TRUNCATE TABLE ap_payment CASCADE;
TRUNCATE TABLE ap_payment_detail CASCADE;
TRUNCATE TABLE account_payable CASCADE;
TRUNCATE TABLE debit_memo CASCADE;
TRUNCATE TABLE vendor_balance CASCADE;
TRUNCATE TABLE bank_book CASCADE;
TRUNCATE TABLE cash_flow CASCADE;
TRUNCATE TABLE cash_flow_journal CASCADE;
TRUNCATE TABLE bank_transfer CASCADE;
TRUNCATE TABLE bank_reconcile CASCADE;
TRUNCATE TABLE petty_cash CASCADE;
TRUNCATE TABLE petty_cash_balance CASCADE;
TRUNCATE TABLE cashier_balance CASCADE;
TRUNCATE TABLE remote_store_data CASCADE;
TRUNCATE TABLE synchronization CASCADE;
TRUNCATE TABLE database_backup CASCADE;
TRUNCATE TABLE last_number CASCADE;
TRUNCATE TABLE tax_serial CASCADE;
TRUNCATE TABLE purchase_tax_serial CASCADE;
TRUNCATE TABLE discount_coupon CASCADE;
TRUNCATE TABLE voucher_no CASCADE; 
TRUNCATE TABLE recurring CASCADE; 
TRUNCATE TABLE recurring_log CASCADE; 

-- edi-schema.xml
TRUNCATE TABLE rq_approval;
TRUNCATE TABLE rq_quote;

-- pwp-schema.xml
TRUNCATE TABLE pwp CASCADE;
TRUNCATE TABLE pwp_location CASCADE;
TRUNCATE TABLE pwp_buy CASCADE;
TRUNCATE TABLE pwp_get CASCADE;

-- turbine-schema.xml

-- sfa-schema.xml
--TRUNCATE TABLE sfa_visit CASCADE;
--TRUNCATE TABLE sfa_journey CASCADE;
--TRUNCATE TABLE sfa_message CASCADE;
--TRUNCATE TABLE sfa_survey CASCADE;
--TRUNCATE TABLE sfa_pic CASCADE;
--TRUNCATE TABLE sfa_visit_trans CASCADE;


--webstore-schema.xml

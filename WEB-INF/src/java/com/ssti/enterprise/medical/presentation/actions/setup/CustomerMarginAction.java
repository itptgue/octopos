package com.ssti.enterprise.medical.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.excel.helper.CustomerMarginLoader;
import com.ssti.enterprise.pos.presentation.actions.setup.SetupSecureAction;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class CustomerMarginAction extends SetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }
 	
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			CustomerMarginLoader oLoader = new CustomerMarginLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));			
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
}

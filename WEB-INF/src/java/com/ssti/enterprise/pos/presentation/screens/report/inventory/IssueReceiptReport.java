package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.IssueReceiptTool;

public class IssueReceiptReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
	    	super.doBuildTemplate(data, context);
            int iCond = data.getParameters().getInt("Condition",-1);
            String sKey = data.getParameters().getString("Keywords","");
            String sUserName = data.getParameters().getString("userName","");
            String sConfirmBy = data.getParameters().getString("confirmBy","");

            context.put("issuereceipt", new IssueReceiptTool());
	        context.put ("IssueReceipts", 
	        	IssueReceiptTool.findData(iCond,
	        							  sKey,
	        							  dStart, 
	        							  dEnd, 
	        							  sLocationID,
	        							  sAdjTypeID,"",
	        							  sUserName,
	        							  sConfirmBy,
	        							  i_PROCESSED,false));
	    }   
    	catch (Exception _oEx) {
    		data.setMessage("Data LookUp Failed !");
    	}    
    }    
}

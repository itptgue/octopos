package com.ssti.framework.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to District
 */
public abstract class BaseDistrict extends BaseObject
{
    /** The Peer class */
    private static final DistrictPeer peer =
        new DistrictPeer();

        
    /** The value for the districtId field */
    private String districtId;
      
    /** The value for the regencyId field */
    private String regencyId;
      
    /** The value for the district field */
    private String district;
  
    
    /**
     * Get the DistrictId
     *
     * @return String
     */
    public String getDistrictId()
    {
        return districtId;
    }

                                              
    /**
     * Set the value of DistrictId
     *
     * @param v new value
     */
    public void setDistrictId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.districtId, v))
              {
            this.districtId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated Village
        if (collVillages != null)
        {
            for (int i = 0; i < collVillages.size(); i++)
            {
                ((Village) collVillages.get(i))
                    .setDistrictId(v);
            }
        }
                                }
  
    /**
     * Get the RegencyId
     *
     * @return String
     */
    public String getRegencyId()
    {
        return regencyId;
    }

                              
    /**
     * Set the value of RegencyId
     *
     * @param v new value
     */
    public void setRegencyId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.regencyId, v))
              {
            this.regencyId = v;
            setModified(true);
        }
    
                          
                if (aRegency != null && !ObjectUtils.equals(aRegency.getRegencyId(), v))
                {
            aRegency = null;
        }
      
              }
  
    /**
     * Get the District
     *
     * @return String
     */
    public String getDistrict()
    {
        return district;
    }

                        
    /**
     * Set the value of District
     *
     * @param v new value
     */
    public void setDistrict(String v) 
    {
    
                  if (!ObjectUtils.equals(this.district, v))
              {
            this.district = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Regency aRegency;

    /**
     * Declares an association between this object and a Regency object
     *
     * @param v Regency
     * @throws TorqueException
     */
    public void setRegency(Regency v) throws TorqueException
    {
            if (v == null)
        {
                  setRegencyId((String) null);
              }
        else
        {
            setRegencyId(v.getRegencyId());
        }
            aRegency = v;
    }

                                            
    /**
     * Get the associated Regency object
     *
     * @return the associated Regency object
     * @throws TorqueException
     */
    public Regency getRegency() throws TorqueException
    {
        if (aRegency == null && (!ObjectUtils.equals(this.regencyId, null)))
        {
                          aRegency = RegencyPeer.retrieveByPK(SimpleKey.keyFor(this.regencyId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Regency obj = RegencyPeer.retrieveByPK(this.regencyId);
               obj.addDistricts(this);
            */
        }
        return aRegency;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setRegencyKey(ObjectKey key) throws TorqueException
    {
      
                        setRegencyId(key.toString());
                  }
       
                                
            
          /**
     * Collection to store aggregation of collVillages
     */
    protected List collVillages;

    /**
     * Temporary storage of collVillages to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initVillages()
    {
        if (collVillages == null)
        {
            collVillages = new ArrayList();
        }
    }

    /**
     * Method called to associate a Village object to this object
     * through the Village foreign key attribute
     *
     * @param l Village
     * @throws TorqueException
     */
    public void addVillage(Village l) throws TorqueException
    {
        getVillages().add(l);
        l.setDistrict((District) this);
    }

    /**
     * The criteria used to select the current contents of collVillages
     */
    private Criteria lastVillagesCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getVillages(new Criteria())
     *
     * @throws TorqueException
     */
    public List getVillages() throws TorqueException
    {
              if (collVillages == null)
        {
            collVillages = getVillages(new Criteria(10));
        }
        return collVillages;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this District has previously
     * been saved, it will retrieve related Villages from storage.
     * If this District is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getVillages(Criteria criteria) throws TorqueException
    {
              if (collVillages == null)
        {
            if (isNew())
            {
               collVillages = new ArrayList();
            }
            else
            {
                        criteria.add(VillagePeer.DISTRICT_ID, getDistrictId() );
                        collVillages = VillagePeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(VillagePeer.DISTRICT_ID, getDistrictId());
                            if (!lastVillagesCriteria.equals(criteria))
                {
                    collVillages = VillagePeer.doSelect(criteria);
                }
            }
        }
        lastVillagesCriteria = criteria;

        return collVillages;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getVillages(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getVillages(Connection con) throws TorqueException
    {
              if (collVillages == null)
        {
            collVillages = getVillages(new Criteria(10), con);
        }
        return collVillages;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this District has previously
     * been saved, it will retrieve related Villages from storage.
     * If this District is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getVillages(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collVillages == null)
        {
            if (isNew())
            {
               collVillages = new ArrayList();
            }
            else
            {
                         criteria.add(VillagePeer.DISTRICT_ID, getDistrictId());
                         collVillages = VillagePeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(VillagePeer.DISTRICT_ID, getDistrictId());
                             if (!lastVillagesCriteria.equals(criteria))
                 {
                     collVillages = VillagePeer.doSelect(criteria, con);
                 }
             }
         }
         lastVillagesCriteria = criteria;

         return collVillages;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this District is new, it will return
     * an empty collection; or if this District has previously
     * been saved, it will retrieve related Villages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in District.
     */
    protected List getVillagesJoinDistrict(Criteria criteria)
        throws TorqueException
    {
                    if (collVillages == null)
        {
            if (isNew())
            {
               collVillages = new ArrayList();
            }
            else
            {
                              criteria.add(VillagePeer.DISTRICT_ID, getDistrictId());
                              collVillages = VillagePeer.doSelectJoinDistrict(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(VillagePeer.DISTRICT_ID, getDistrictId());
                                    if (!lastVillagesCriteria.equals(criteria))
            {
                collVillages = VillagePeer.doSelectJoinDistrict(criteria);
            }
        }
        lastVillagesCriteria = criteria;

        return collVillages;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DistrictId");
              fieldNames.add("RegencyId");
              fieldNames.add("District");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DistrictId"))
        {
                return getDistrictId();
            }
          if (name.equals("RegencyId"))
        {
                return getRegencyId();
            }
          if (name.equals("District"))
        {
                return getDistrict();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DistrictPeer.DISTRICT_ID))
        {
                return getDistrictId();
            }
          if (name.equals(DistrictPeer.REGENCY_ID))
        {
                return getRegencyId();
            }
          if (name.equals(DistrictPeer.DISTRICT))
        {
                return getDistrict();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDistrictId();
            }
              if (pos == 1)
        {
                return getRegencyId();
            }
              if (pos == 2)
        {
                return getDistrict();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DistrictPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DistrictPeer.doInsert((District) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DistrictPeer.doUpdate((District) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collVillages != null)
            {
                for (int i = 0; i < collVillages.size(); i++)
                {
                    ((Village) collVillages.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key districtId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setDistrictId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setDistrictId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDistrictId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public District copy() throws TorqueException
    {
        return copyInto(new District());
    }
  
    protected District copyInto(District copyObj) throws TorqueException
    {
          copyObj.setDistrictId(districtId);
          copyObj.setRegencyId(regencyId);
          copyObj.setDistrict(district);
  
                    copyObj.setDistrictId((String)null);
                        
                                      
                            
        List v = getVillages();
        for (int i = 0; i < v.size(); i++)
        {
            Village obj = (Village) v.get(i);
            copyObj.addVillage(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DistrictPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("District\n");
        str.append("--------\n")
           .append("DistrictId           : ")
           .append(getDistrictId())
           .append("\n")
           .append("RegencyId            : ")
           .append(getRegencyId())
           .append("\n")
           .append("District             : ")
           .append(getDistrict())
           .append("\n")
        ;
        return(str.toString());
    }
}

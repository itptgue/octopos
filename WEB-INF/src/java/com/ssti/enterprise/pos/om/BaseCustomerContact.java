package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerContact
 */
public abstract class BaseCustomerContact extends BaseObject
{
    /** The Peer class */
    private static final CustomerContactPeer peer =
        new CustomerContactPeer();

        
    /** The value for the customerContactId field */
    private String customerContactId;
      
    /** The value for the customerId field */
    private String customerId;
                                                
    /** The value for the contactName field */
    private String contactName = "";
                                                
    /** The value for the contactPhone field */
    private String contactPhone = "";
                                                
    /** The value for the contactMobile field */
    private String contactMobile = "";
                                                
    /** The value for the contactEmail field */
    private String contactEmail = "";
                                                
    /** The value for the contactJobTitle field */
    private String contactJobTitle = "";
                                                
    /** The value for the contactDept field */
    private String contactDept = "";
                                                
    /** The value for the contactBirthPlace field */
    private String contactBirthPlace = "";
      
    /** The value for the contactBirthDate field */
    private Date contactBirthDate;
                                                
    /** The value for the contactReligion field */
    private String contactReligion = "";
                                                
    /** The value for the contactRemark field */
    private String contactRemark = "";
                                                
    /** The value for the contactSm1 field */
    private String contactSm1 = "";
                                                
    /** The value for the contactSm2 field */
    private String contactSm2 = "";
                                                
    /** The value for the contactSm3 field */
    private String contactSm3 = "";
      
    /** The value for the contactJson field */
    private String contactJson;
  
    
    /**
     * Get the CustomerContactId
     *
     * @return String
     */
    public String getCustomerContactId()
    {
        return customerContactId;
    }

                        
    /**
     * Set the value of CustomerContactId
     *
     * @param v new value
     */
    public void setCustomerContactId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerContactId, v))
              {
            this.customerContactId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                              
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
                          
                if (aCustomer != null && !ObjectUtils.equals(aCustomer.getCustomerId(), v))
                {
            aCustomer = null;
        }
      
              }
  
    /**
     * Get the ContactName
     *
     * @return String
     */
    public String getContactName()
    {
        return contactName;
    }

                        
    /**
     * Set the value of ContactName
     *
     * @param v new value
     */
    public void setContactName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName, v))
              {
            this.contactName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactPhone
     *
     * @return String
     */
    public String getContactPhone()
    {
        return contactPhone;
    }

                        
    /**
     * Set the value of ContactPhone
     *
     * @param v new value
     */
    public void setContactPhone(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactPhone, v))
              {
            this.contactPhone = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactMobile
     *
     * @return String
     */
    public String getContactMobile()
    {
        return contactMobile;
    }

                        
    /**
     * Set the value of ContactMobile
     *
     * @param v new value
     */
    public void setContactMobile(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactMobile, v))
              {
            this.contactMobile = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactEmail
     *
     * @return String
     */
    public String getContactEmail()
    {
        return contactEmail;
    }

                        
    /**
     * Set the value of ContactEmail
     *
     * @param v new value
     */
    public void setContactEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactEmail, v))
              {
            this.contactEmail = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactJobTitle
     *
     * @return String
     */
    public String getContactJobTitle()
    {
        return contactJobTitle;
    }

                        
    /**
     * Set the value of ContactJobTitle
     *
     * @param v new value
     */
    public void setContactJobTitle(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactJobTitle, v))
              {
            this.contactJobTitle = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactDept
     *
     * @return String
     */
    public String getContactDept()
    {
        return contactDept;
    }

                        
    /**
     * Set the value of ContactDept
     *
     * @param v new value
     */
    public void setContactDept(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactDept, v))
              {
            this.contactDept = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactBirthPlace
     *
     * @return String
     */
    public String getContactBirthPlace()
    {
        return contactBirthPlace;
    }

                        
    /**
     * Set the value of ContactBirthPlace
     *
     * @param v new value
     */
    public void setContactBirthPlace(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactBirthPlace, v))
              {
            this.contactBirthPlace = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactBirthDate
     *
     * @return Date
     */
    public Date getContactBirthDate()
    {
        return contactBirthDate;
    }

                        
    /**
     * Set the value of ContactBirthDate
     *
     * @param v new value
     */
    public void setContactBirthDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.contactBirthDate, v))
              {
            this.contactBirthDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactReligion
     *
     * @return String
     */
    public String getContactReligion()
    {
        return contactReligion;
    }

                        
    /**
     * Set the value of ContactReligion
     *
     * @param v new value
     */
    public void setContactReligion(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactReligion, v))
              {
            this.contactReligion = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactRemark
     *
     * @return String
     */
    public String getContactRemark()
    {
        return contactRemark;
    }

                        
    /**
     * Set the value of ContactRemark
     *
     * @param v new value
     */
    public void setContactRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactRemark, v))
              {
            this.contactRemark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactSm1
     *
     * @return String
     */
    public String getContactSm1()
    {
        return contactSm1;
    }

                        
    /**
     * Set the value of ContactSm1
     *
     * @param v new value
     */
    public void setContactSm1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactSm1, v))
              {
            this.contactSm1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactSm2
     *
     * @return String
     */
    public String getContactSm2()
    {
        return contactSm2;
    }

                        
    /**
     * Set the value of ContactSm2
     *
     * @param v new value
     */
    public void setContactSm2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactSm2, v))
              {
            this.contactSm2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactSm3
     *
     * @return String
     */
    public String getContactSm3()
    {
        return contactSm3;
    }

                        
    /**
     * Set the value of ContactSm3
     *
     * @param v new value
     */
    public void setContactSm3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactSm3, v))
              {
            this.contactSm3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactJson
     *
     * @return String
     */
    public String getContactJson()
    {
        return contactJson;
    }

                        
    /**
     * Set the value of ContactJson
     *
     * @param v new value
     */
    public void setContactJson(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactJson, v))
              {
            this.contactJson = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Customer aCustomer;

    /**
     * Declares an association between this object and a Customer object
     *
     * @param v Customer
     * @throws TorqueException
     */
    public void setCustomer(Customer v) throws TorqueException
    {
            if (v == null)
        {
                  setCustomerId((String) null);
              }
        else
        {
            setCustomerId(v.getCustomerId());
        }
            aCustomer = v;
    }

                                            
    /**
     * Get the associated Customer object
     *
     * @return the associated Customer object
     * @throws TorqueException
     */
    public Customer getCustomer() throws TorqueException
    {
        if (aCustomer == null && (!ObjectUtils.equals(this.customerId, null)))
        {
                          aCustomer = CustomerPeer.retrieveByPK(SimpleKey.keyFor(this.customerId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Customer obj = CustomerPeer.retrieveByPK(this.customerId);
               obj.addCustomerContacts(this);
            */
        }
        return aCustomer;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setCustomerKey(ObjectKey key) throws TorqueException
    {
      
                        setCustomerId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerContactId");
              fieldNames.add("CustomerId");
              fieldNames.add("ContactName");
              fieldNames.add("ContactPhone");
              fieldNames.add("ContactMobile");
              fieldNames.add("ContactEmail");
              fieldNames.add("ContactJobTitle");
              fieldNames.add("ContactDept");
              fieldNames.add("ContactBirthPlace");
              fieldNames.add("ContactBirthDate");
              fieldNames.add("ContactReligion");
              fieldNames.add("ContactRemark");
              fieldNames.add("ContactSm1");
              fieldNames.add("ContactSm2");
              fieldNames.add("ContactSm3");
              fieldNames.add("ContactJson");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerContactId"))
        {
                return getCustomerContactId();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("ContactName"))
        {
                return getContactName();
            }
          if (name.equals("ContactPhone"))
        {
                return getContactPhone();
            }
          if (name.equals("ContactMobile"))
        {
                return getContactMobile();
            }
          if (name.equals("ContactEmail"))
        {
                return getContactEmail();
            }
          if (name.equals("ContactJobTitle"))
        {
                return getContactJobTitle();
            }
          if (name.equals("ContactDept"))
        {
                return getContactDept();
            }
          if (name.equals("ContactBirthPlace"))
        {
                return getContactBirthPlace();
            }
          if (name.equals("ContactBirthDate"))
        {
                return getContactBirthDate();
            }
          if (name.equals("ContactReligion"))
        {
                return getContactReligion();
            }
          if (name.equals("ContactRemark"))
        {
                return getContactRemark();
            }
          if (name.equals("ContactSm1"))
        {
                return getContactSm1();
            }
          if (name.equals("ContactSm2"))
        {
                return getContactSm2();
            }
          if (name.equals("ContactSm3"))
        {
                return getContactSm3();
            }
          if (name.equals("ContactJson"))
        {
                return getContactJson();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerContactPeer.CUSTOMER_CONTACT_ID))
        {
                return getCustomerContactId();
            }
          if (name.equals(CustomerContactPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerContactPeer.CONTACT_NAME))
        {
                return getContactName();
            }
          if (name.equals(CustomerContactPeer.CONTACT_PHONE))
        {
                return getContactPhone();
            }
          if (name.equals(CustomerContactPeer.CONTACT_MOBILE))
        {
                return getContactMobile();
            }
          if (name.equals(CustomerContactPeer.CONTACT_EMAIL))
        {
                return getContactEmail();
            }
          if (name.equals(CustomerContactPeer.CONTACT_JOB_TITLE))
        {
                return getContactJobTitle();
            }
          if (name.equals(CustomerContactPeer.CONTACT_DEPT))
        {
                return getContactDept();
            }
          if (name.equals(CustomerContactPeer.CONTACT_BIRTH_PLACE))
        {
                return getContactBirthPlace();
            }
          if (name.equals(CustomerContactPeer.CONTACT_BIRTH_DATE))
        {
                return getContactBirthDate();
            }
          if (name.equals(CustomerContactPeer.CONTACT_RELIGION))
        {
                return getContactReligion();
            }
          if (name.equals(CustomerContactPeer.CONTACT_REMARK))
        {
                return getContactRemark();
            }
          if (name.equals(CustomerContactPeer.CONTACT_SM1))
        {
                return getContactSm1();
            }
          if (name.equals(CustomerContactPeer.CONTACT_SM2))
        {
                return getContactSm2();
            }
          if (name.equals(CustomerContactPeer.CONTACT_SM3))
        {
                return getContactSm3();
            }
          if (name.equals(CustomerContactPeer.CONTACT_JSON))
        {
                return getContactJson();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerContactId();
            }
              if (pos == 1)
        {
                return getCustomerId();
            }
              if (pos == 2)
        {
                return getContactName();
            }
              if (pos == 3)
        {
                return getContactPhone();
            }
              if (pos == 4)
        {
                return getContactMobile();
            }
              if (pos == 5)
        {
                return getContactEmail();
            }
              if (pos == 6)
        {
                return getContactJobTitle();
            }
              if (pos == 7)
        {
                return getContactDept();
            }
              if (pos == 8)
        {
                return getContactBirthPlace();
            }
              if (pos == 9)
        {
                return getContactBirthDate();
            }
              if (pos == 10)
        {
                return getContactReligion();
            }
              if (pos == 11)
        {
                return getContactRemark();
            }
              if (pos == 12)
        {
                return getContactSm1();
            }
              if (pos == 13)
        {
                return getContactSm2();
            }
              if (pos == 14)
        {
                return getContactSm3();
            }
              if (pos == 15)
        {
                return getContactJson();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerContactPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerContactPeer.doInsert((CustomerContact) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerContactPeer.doUpdate((CustomerContact) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerContactId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomerContactId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomerContactId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerContactId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerContact copy() throws TorqueException
    {
        return copyInto(new CustomerContact());
    }
  
    protected CustomerContact copyInto(CustomerContact copyObj) throws TorqueException
    {
          copyObj.setCustomerContactId(customerContactId);
          copyObj.setCustomerId(customerId);
          copyObj.setContactName(contactName);
          copyObj.setContactPhone(contactPhone);
          copyObj.setContactMobile(contactMobile);
          copyObj.setContactEmail(contactEmail);
          copyObj.setContactJobTitle(contactJobTitle);
          copyObj.setContactDept(contactDept);
          copyObj.setContactBirthPlace(contactBirthPlace);
          copyObj.setContactBirthDate(contactBirthDate);
          copyObj.setContactReligion(contactReligion);
          copyObj.setContactRemark(contactRemark);
          copyObj.setContactSm1(contactSm1);
          copyObj.setContactSm2(contactSm2);
          copyObj.setContactSm3(contactSm3);
          copyObj.setContactJson(contactJson);
  
                    copyObj.setCustomerContactId((String)null);
                                                                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerContactPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerContact\n");
        str.append("---------------\n")
           .append("CustomerContactId    : ")
           .append(getCustomerContactId())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("ContactName          : ")
           .append(getContactName())
           .append("\n")
           .append("ContactPhone         : ")
           .append(getContactPhone())
           .append("\n")
           .append("ContactMobile        : ")
           .append(getContactMobile())
           .append("\n")
           .append("ContactEmail         : ")
           .append(getContactEmail())
           .append("\n")
           .append("ContactJobTitle      : ")
           .append(getContactJobTitle())
           .append("\n")
           .append("ContactDept          : ")
           .append(getContactDept())
           .append("\n")
           .append("ContactBirthPlace    : ")
           .append(getContactBirthPlace())
           .append("\n")
           .append("ContactBirthDate     : ")
           .append(getContactBirthDate())
           .append("\n")
           .append("ContactReligion      : ")
           .append(getContactReligion())
           .append("\n")
           .append("ContactRemark        : ")
           .append(getContactRemark())
           .append("\n")
           .append("ContactSm1           : ")
           .append(getContactSm1())
           .append("\n")
           .append("ContactSm2           : ")
           .append(getContactSm2())
           .append("\n")
           .append("ContactSm3           : ")
           .append(getContactSm3())
           .append("\n")
           .append("ContactJson          : ")
           .append(getContactJson())
           .append("\n")
        ;
        return(str.toString());
    }
}

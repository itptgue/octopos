package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemMapBuilder";

    /**
     * Item
     * @deprecated use ItemPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item";
    }

  
    /**
     * item.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemPeer.item.ITEM_ID constant
     */
    public static String getItem_ItemId()
    {
        return "item.ITEM_ID";
    }
  
    /**
     * item.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use ItemPeer.item.ITEM_CODE constant
     */
    public static String getItem_ItemCode()
    {
        return "item.ITEM_CODE";
    }
  
    /**
     * item.BARCODE
     * @return the column name for the BARCODE field
     * @deprecated use ItemPeer.item.BARCODE constant
     */
    public static String getItem_Barcode()
    {
        return "item.BARCODE";
    }
  
    /**
     * item.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use ItemPeer.item.ITEM_NAME constant
     */
    public static String getItem_ItemName()
    {
        return "item.ITEM_NAME";
    }
  
    /**
     * item.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use ItemPeer.item.DESCRIPTION constant
     */
    public static String getItem_Description()
    {
        return "item.DESCRIPTION";
    }
  
    /**
     * item.ITEM_SKU
     * @return the column name for the ITEM_SKU field
     * @deprecated use ItemPeer.item.ITEM_SKU constant
     */
    public static String getItem_ItemSku()
    {
        return "item.ITEM_SKU";
    }
  
    /**
     * item.ITEM_SKU_NAME
     * @return the column name for the ITEM_SKU_NAME field
     * @deprecated use ItemPeer.item.ITEM_SKU_NAME constant
     */
    public static String getItem_ItemSkuName()
    {
        return "item.ITEM_SKU_NAME";
    }
  
    /**
     * item.VENDOR_ITEM_CODE
     * @return the column name for the VENDOR_ITEM_CODE field
     * @deprecated use ItemPeer.item.VENDOR_ITEM_CODE constant
     */
    public static String getItem_VendorItemCode()
    {
        return "item.VENDOR_ITEM_CODE";
    }
  
    /**
     * item.VENDOR_ITEM_NAME
     * @return the column name for the VENDOR_ITEM_NAME field
     * @deprecated use ItemPeer.item.VENDOR_ITEM_NAME constant
     */
    public static String getItem_VendorItemName()
    {
        return "item.VENDOR_ITEM_NAME";
    }
  
    /**
     * item.ITEM_TYPE
     * @return the column name for the ITEM_TYPE field
     * @deprecated use ItemPeer.item.ITEM_TYPE constant
     */
    public static String getItem_ItemType()
    {
        return "item.ITEM_TYPE";
    }
  
    /**
     * item.KATEGORI_ID
     * @return the column name for the KATEGORI_ID field
     * @deprecated use ItemPeer.item.KATEGORI_ID constant
     */
    public static String getItem_KategoriId()
    {
        return "item.KATEGORI_ID";
    }
  
    /**
     * item.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use ItemPeer.item.UNIT_ID constant
     */
    public static String getItem_UnitId()
    {
        return "item.UNIT_ID";
    }
  
    /**
     * item.PURCHASE_UNIT_ID
     * @return the column name for the PURCHASE_UNIT_ID field
     * @deprecated use ItemPeer.item.PURCHASE_UNIT_ID constant
     */
    public static String getItem_PurchaseUnitId()
    {
        return "item.PURCHASE_UNIT_ID";
    }
  
    /**
     * item.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use ItemPeer.item.TAX_ID constant
     */
    public static String getItem_TaxId()
    {
        return "item.TAX_ID";
    }
  
    /**
     * item.PURCHASE_TAX_ID
     * @return the column name for the PURCHASE_TAX_ID field
     * @deprecated use ItemPeer.item.PURCHASE_TAX_ID constant
     */
    public static String getItem_PurchaseTaxId()
    {
        return "item.PURCHASE_TAX_ID";
    }
  
    /**
     * item.PREFERED_VENDOR_ID
     * @return the column name for the PREFERED_VENDOR_ID field
     * @deprecated use ItemPeer.item.PREFERED_VENDOR_ID constant
     */
    public static String getItem_PreferedVendorId()
    {
        return "item.PREFERED_VENDOR_ID";
    }
  
    /**
     * item.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use ItemPeer.item.ITEM_PRICE constant
     */
    public static String getItem_ItemPrice()
    {
        return "item.ITEM_PRICE";
    }
  
    /**
     * item.MIN_PRICE
     * @return the column name for the MIN_PRICE field
     * @deprecated use ItemPeer.item.MIN_PRICE constant
     */
    public static String getItem_MinPrice()
    {
        return "item.MIN_PRICE";
    }
  
    /**
     * item.IS_FIXED_PRICE
     * @return the column name for the IS_FIXED_PRICE field
     * @deprecated use ItemPeer.item.IS_FIXED_PRICE constant
     */
    public static String getItem_IsFixedPrice()
    {
        return "item.IS_FIXED_PRICE";
    }
  
    /**
     * item.IS_DISCONTINUE
     * @return the column name for the IS_DISCONTINUE field
     * @deprecated use ItemPeer.item.IS_DISCONTINUE constant
     */
    public static String getItem_IsDiscontinue()
    {
        return "item.IS_DISCONTINUE";
    }
  
    /**
     * item.IS_CONSIGNMENT
     * @return the column name for the IS_CONSIGNMENT field
     * @deprecated use ItemPeer.item.IS_CONSIGNMENT constant
     */
    public static String getItem_IsConsignment()
    {
        return "item.IS_CONSIGNMENT";
    }
  
    /**
     * item.ITEM_PICTURE
     * @return the column name for the ITEM_PICTURE field
     * @deprecated use ItemPeer.item.ITEM_PICTURE constant
     */
    public static String getItem_ItemPicture()
    {
        return "item.ITEM_PICTURE";
    }
  
    /**
     * item.TAGS
     * @return the column name for the TAGS field
     * @deprecated use ItemPeer.item.TAGS constant
     */
    public static String getItem_Tags()
    {
        return "item.TAGS";
    }
  
    /**
     * item.DISPLAY_STORE
     * @return the column name for the DISPLAY_STORE field
     * @deprecated use ItemPeer.item.DISPLAY_STORE constant
     */
    public static String getItem_DisplayStore()
    {
        return "item.DISPLAY_STORE";
    }
  
    /**
     * item.LAST_PURCHASE_PRICE
     * @return the column name for the LAST_PURCHASE_PRICE field
     * @deprecated use ItemPeer.item.LAST_PURCHASE_PRICE constant
     */
    public static String getItem_LastPurchasePrice()
    {
        return "item.LAST_PURCHASE_PRICE";
    }
  
    /**
     * item.LAST_PURCHASE_CURR
     * @return the column name for the LAST_PURCHASE_CURR field
     * @deprecated use ItemPeer.item.LAST_PURCHASE_CURR constant
     */
    public static String getItem_LastPurchaseCurr()
    {
        return "item.LAST_PURCHASE_CURR";
    }
  
    /**
     * item.LAST_PURCHASE_RATE
     * @return the column name for the LAST_PURCHASE_RATE field
     * @deprecated use ItemPeer.item.LAST_PURCHASE_RATE constant
     */
    public static String getItem_LastPurchaseRate()
    {
        return "item.LAST_PURCHASE_RATE";
    }
  
    /**
     * item.PROFIT_MARGIN
     * @return the column name for the PROFIT_MARGIN field
     * @deprecated use ItemPeer.item.PROFIT_MARGIN constant
     */
    public static String getItem_ProfitMargin()
    {
        return "item.PROFIT_MARGIN";
    }
  
    /**
     * item.MINIMUM_STOCK
     * @return the column name for the MINIMUM_STOCK field
     * @deprecated use ItemPeer.item.MINIMUM_STOCK constant
     */
    public static String getItem_MinimumStock()
    {
        return "item.MINIMUM_STOCK";
    }
  
    /**
     * item.REORDER_POINT
     * @return the column name for the REORDER_POINT field
     * @deprecated use ItemPeer.item.REORDER_POINT constant
     */
    public static String getItem_ReorderPoint()
    {
        return "item.REORDER_POINT";
    }
  
    /**
     * item.MAXIMUM_STOCK
     * @return the column name for the MAXIMUM_STOCK field
     * @deprecated use ItemPeer.item.MAXIMUM_STOCK constant
     */
    public static String getItem_MaximumStock()
    {
        return "item.MAXIMUM_STOCK";
    }
  
    /**
     * item.RACK_LOCATION
     * @return the column name for the RACK_LOCATION field
     * @deprecated use ItemPeer.item.RACK_LOCATION constant
     */
    public static String getItem_RackLocation()
    {
        return "item.RACK_LOCATION";
    }
  
    /**
     * item.WAREHOUSE_ID
     * @return the column name for the WAREHOUSE_ID field
     * @deprecated use ItemPeer.item.WAREHOUSE_ID constant
     */
    public static String getItem_WarehouseId()
    {
        return "item.WAREHOUSE_ID";
    }
  
    /**
     * item.DELIVERY_TYPE
     * @return the column name for the DELIVERY_TYPE field
     * @deprecated use ItemPeer.item.DELIVERY_TYPE constant
     */
    public static String getItem_DeliveryType()
    {
        return "item.DELIVERY_TYPE";
    }
  
    /**
     * item.ITEM_STATUS_CODE_ID
     * @return the column name for the ITEM_STATUS_CODE_ID field
     * @deprecated use ItemPeer.item.ITEM_STATUS_CODE_ID constant
     */
    public static String getItem_ItemStatusCodeId()
    {
        return "item.ITEM_STATUS_CODE_ID";
    }
  
    /**
     * item.STATUS_COLOR
     * @return the column name for the STATUS_COLOR field
     * @deprecated use ItemPeer.item.STATUS_COLOR constant
     */
    public static String getItem_StatusColor()
    {
        return "item.STATUS_COLOR";
    }
  
    /**
     * item.DISCOUNT_AMOUNT
     * @return the column name for the DISCOUNT_AMOUNT field
     * @deprecated use ItemPeer.item.DISCOUNT_AMOUNT constant
     */
    public static String getItem_DiscountAmount()
    {
        return "item.DISCOUNT_AMOUNT";
    }
  
    /**
     * item.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use ItemPeer.item.CREATE_BY constant
     */
    public static String getItem_CreateBy()
    {
        return "item.CREATE_BY";
    }
  
    /**
     * item.ADD_DATE
     * @return the column name for the ADD_DATE field
     * @deprecated use ItemPeer.item.ADD_DATE constant
     */
    public static String getItem_AddDate()
    {
        return "item.ADD_DATE";
    }
  
    /**
     * item.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use ItemPeer.item.UPDATE_DATE constant
     */
    public static String getItem_UpdateDate()
    {
        return "item.UPDATE_DATE";
    }
  
    /**
     * item.LAST_ADJUSTMENT
     * @return the column name for the LAST_ADJUSTMENT field
     * @deprecated use ItemPeer.item.LAST_ADJUSTMENT constant
     */
    public static String getItem_LastAdjustment()
    {
        return "item.LAST_ADJUSTMENT";
    }
  
    /**
     * item.SIZE
     * @return the column name for the SIZE field
     * @deprecated use ItemPeer.item.SIZE constant
     */
    public static String getItem_Size()
    {
        return "item.SIZE";
    }
  
    /**
     * item.COLOR
     * @return the column name for the COLOR field
     * @deprecated use ItemPeer.item.COLOR constant
     */
    public static String getItem_Color()
    {
        return "item.COLOR";
    }
  
    /**
     * item.SIZE_UNIT
     * @return the column name for the SIZE_UNIT field
     * @deprecated use ItemPeer.item.SIZE_UNIT constant
     */
    public static String getItem_SizeUnit()
    {
        return "item.SIZE_UNIT";
    }
  
    /**
     * item.WEIGHT
     * @return the column name for the WEIGHT field
     * @deprecated use ItemPeer.item.WEIGHT constant
     */
    public static String getItem_Weight()
    {
        return "item.WEIGHT";
    }
  
    /**
     * item.VOLUME
     * @return the column name for the VOLUME field
     * @deprecated use ItemPeer.item.VOLUME constant
     */
    public static String getItem_Volume()
    {
        return "item.VOLUME";
    }
  
    /**
     * item.UNIT_CONVERSION
     * @return the column name for the UNIT_CONVERSION field
     * @deprecated use ItemPeer.item.UNIT_CONVERSION constant
     */
    public static String getItem_UnitConversion()
    {
        return "item.UNIT_CONVERSION";
    }
  
    /**
     * item.BRAND
     * @return the column name for the BRAND field
     * @deprecated use ItemPeer.item.BRAND constant
     */
    public static String getItem_Brand()
    {
        return "item.BRAND";
    }
  
    /**
     * item.MANUFACTURER
     * @return the column name for the MANUFACTURER field
     * @deprecated use ItemPeer.item.MANUFACTURER constant
     */
    public static String getItem_Manufacturer()
    {
        return "item.MANUFACTURER";
    }
  
    /**
     * item.TRACK_SERIAL_NO
     * @return the column name for the TRACK_SERIAL_NO field
     * @deprecated use ItemPeer.item.TRACK_SERIAL_NO constant
     */
    public static String getItem_TrackSerialNo()
    {
        return "item.TRACK_SERIAL_NO";
    }
  
    /**
     * item.TRACK_BATCH_NO
     * @return the column name for the TRACK_BATCH_NO field
     * @deprecated use ItemPeer.item.TRACK_BATCH_NO constant
     */
    public static String getItem_TrackBatchNo()
    {
        return "item.TRACK_BATCH_NO";
    }
  
    /**
     * item.ITEM_STATUS
     * @return the column name for the ITEM_STATUS field
     * @deprecated use ItemPeer.item.ITEM_STATUS constant
     */
    public static String getItem_ItemStatus()
    {
        return "item.ITEM_STATUS";
    }
  
    /**
     * item.INVENTORY_ACCOUNT
     * @return the column name for the INVENTORY_ACCOUNT field
     * @deprecated use ItemPeer.item.INVENTORY_ACCOUNT constant
     */
    public static String getItem_InventoryAccount()
    {
        return "item.INVENTORY_ACCOUNT";
    }
  
    /**
     * item.SALES_ACCOUNT
     * @return the column name for the SALES_ACCOUNT field
     * @deprecated use ItemPeer.item.SALES_ACCOUNT constant
     */
    public static String getItem_SalesAccount()
    {
        return "item.SALES_ACCOUNT";
    }
  
    /**
     * item.SALES_RETURN_ACCOUNT
     * @return the column name for the SALES_RETURN_ACCOUNT field
     * @deprecated use ItemPeer.item.SALES_RETURN_ACCOUNT constant
     */
    public static String getItem_SalesReturnAccount()
    {
        return "item.SALES_RETURN_ACCOUNT";
    }
  
    /**
     * item.ITEM_DISCOUNT_ACCOUNT
     * @return the column name for the ITEM_DISCOUNT_ACCOUNT field
     * @deprecated use ItemPeer.item.ITEM_DISCOUNT_ACCOUNT constant
     */
    public static String getItem_ItemDiscountAccount()
    {
        return "item.ITEM_DISCOUNT_ACCOUNT";
    }
  
    /**
     * item.COGS_ACCOUNT
     * @return the column name for the COGS_ACCOUNT field
     * @deprecated use ItemPeer.item.COGS_ACCOUNT constant
     */
    public static String getItem_CogsAccount()
    {
        return "item.COGS_ACCOUNT";
    }
  
    /**
     * item.PURCHASE_RETURN_ACCOUNT
     * @return the column name for the PURCHASE_RETURN_ACCOUNT field
     * @deprecated use ItemPeer.item.PURCHASE_RETURN_ACCOUNT constant
     */
    public static String getItem_PurchaseReturnAccount()
    {
        return "item.PURCHASE_RETURN_ACCOUNT";
    }
  
    /**
     * item.EXPENSE_ACCOUNT
     * @return the column name for the EXPENSE_ACCOUNT field
     * @deprecated use ItemPeer.item.EXPENSE_ACCOUNT constant
     */
    public static String getItem_ExpenseAccount()
    {
        return "item.EXPENSE_ACCOUNT";
    }
  
    /**
     * item.UNBILLED_GOODS_ACCOUNT
     * @return the column name for the UNBILLED_GOODS_ACCOUNT field
     * @deprecated use ItemPeer.item.UNBILLED_GOODS_ACCOUNT constant
     */
    public static String getItem_UnbilledGoodsAccount()
    {
        return "item.UNBILLED_GOODS_ACCOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item");
        TableMap tMap = dbMap.getTable("item");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item.ITEM_ID", "");
                          tMap.addColumn("item.ITEM_CODE", "");
                          tMap.addColumn("item.BARCODE", "");
                          tMap.addColumn("item.ITEM_NAME", "");
                          tMap.addColumn("item.DESCRIPTION", "");
                          tMap.addColumn("item.ITEM_SKU", "");
                          tMap.addColumn("item.ITEM_SKU_NAME", "");
                          tMap.addColumn("item.VENDOR_ITEM_CODE", "");
                          tMap.addColumn("item.VENDOR_ITEM_NAME", "");
                            tMap.addColumn("item.ITEM_TYPE", Integer.valueOf(0));
                          tMap.addForeignKey(
                "item.KATEGORI_ID", "" , "kategori" ,
                "kategori_id");
                          tMap.addColumn("item.UNIT_ID", "");
                          tMap.addColumn("item.PURCHASE_UNIT_ID", "");
                          tMap.addColumn("item.TAX_ID", "");
                          tMap.addColumn("item.PURCHASE_TAX_ID", "");
                          tMap.addColumn("item.PREFERED_VENDOR_ID", "");
                            tMap.addColumn("item.ITEM_PRICE", bd_ZERO);
                            tMap.addColumn("item.MIN_PRICE", bd_ZERO);
                          tMap.addColumn("item.IS_FIXED_PRICE", Boolean.TRUE);
                          tMap.addColumn("item.IS_DISCONTINUE", Boolean.TRUE);
                          tMap.addColumn("item.IS_CONSIGNMENT", Boolean.TRUE);
                          tMap.addColumn("item.ITEM_PICTURE", "");
                          tMap.addColumn("item.TAGS", "");
                          tMap.addColumn("item.DISPLAY_STORE", Boolean.TRUE);
                            tMap.addColumn("item.LAST_PURCHASE_PRICE", bd_ZERO);
                          tMap.addColumn("item.LAST_PURCHASE_CURR", "");
                            tMap.addColumn("item.LAST_PURCHASE_RATE", bd_ZERO);
                          tMap.addColumn("item.PROFIT_MARGIN", "");
                            tMap.addColumn("item.MINIMUM_STOCK", bd_ZERO);
                            tMap.addColumn("item.REORDER_POINT", bd_ZERO);
                            tMap.addColumn("item.MAXIMUM_STOCK", bd_ZERO);
                          tMap.addColumn("item.RACK_LOCATION", "");
                          tMap.addColumn("item.WAREHOUSE_ID", "");
                            tMap.addColumn("item.DELIVERY_TYPE", Integer.valueOf(0));
                          tMap.addColumn("item.ITEM_STATUS_CODE_ID", "");
                          tMap.addColumn("item.STATUS_COLOR", "");
                          tMap.addColumn("item.DISCOUNT_AMOUNT", "");
                          tMap.addColumn("item.CREATE_BY", "");
                          tMap.addColumn("item.ADD_DATE", new Date());
                          tMap.addColumn("item.UPDATE_DATE", new Date());
                          tMap.addColumn("item.LAST_ADJUSTMENT", new Date());
                          tMap.addColumn("item.SIZE", "");
                          tMap.addColumn("item.COLOR", "");
                          tMap.addColumn("item.SIZE_UNIT", "");
                            tMap.addColumn("item.WEIGHT", bd_ZERO);
                            tMap.addColumn("item.VOLUME", bd_ZERO);
                            tMap.addColumn("item.UNIT_CONVERSION", bd_ZERO);
                          tMap.addColumn("item.BRAND", "");
                          tMap.addColumn("item.MANUFACTURER", "");
                          tMap.addColumn("item.TRACK_SERIAL_NO", Boolean.TRUE);
                          tMap.addColumn("item.TRACK_BATCH_NO", Boolean.TRUE);
                          tMap.addColumn("item.ITEM_STATUS", Boolean.TRUE);
                          tMap.addColumn("item.INVENTORY_ACCOUNT", "");
                          tMap.addColumn("item.SALES_ACCOUNT", "");
                          tMap.addColumn("item.SALES_RETURN_ACCOUNT", "");
                          tMap.addColumn("item.ITEM_DISCOUNT_ACCOUNT", "");
                          tMap.addColumn("item.COGS_ACCOUNT", "");
                          tMap.addColumn("item.PURCHASE_RETURN_ACCOUNT", "");
                          tMap.addColumn("item.EXPENSE_ACCOUNT", "");
                          tMap.addColumn("item.UNBILLED_GOODS_ACCOUNT", "");
          }
}

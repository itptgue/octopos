package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class MedicineSalesDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.MedicineSalesDetailMapBuilder";

    /**
     * Item
     * @deprecated use MedicineSalesDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "medicine_sales_detail";
    }

  
    /**
     * medicine_sales_detail.MEDICINE_SALES_DETAIL_ID
     * @return the column name for the MEDICINE_SALES_DETAIL_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.MEDICINE_SALES_DETAIL_ID constant
     */
    public static String getMedicineSalesDetail_MedicineSalesDetailId()
    {
        return "medicine_sales_detail.MEDICINE_SALES_DETAIL_ID";
    }
  
    /**
     * medicine_sales_detail.SALES_ORDER_ID
     * @return the column name for the SALES_ORDER_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.SALES_ORDER_ID constant
     */
    public static String getMedicineSalesDetail_SalesOrderId()
    {
        return "medicine_sales_detail.SALES_ORDER_ID";
    }
  
    /**
     * medicine_sales_detail.DELIVERY_ORDER_ID
     * @return the column name for the DELIVERY_ORDER_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.DELIVERY_ORDER_ID constant
     */
    public static String getMedicineSalesDetail_DeliveryOrderId()
    {
        return "medicine_sales_detail.DELIVERY_ORDER_ID";
    }
  
    /**
     * medicine_sales_detail.DELIVERY_ORDER_DETAIL_ID
     * @return the column name for the DELIVERY_ORDER_DETAIL_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.DELIVERY_ORDER_DETAIL_ID constant
     */
    public static String getMedicineSalesDetail_DeliveryOrderDetailId()
    {
        return "medicine_sales_detail.DELIVERY_ORDER_DETAIL_ID";
    }
  
    /**
     * medicine_sales_detail.MEDICINE_SALES_ID
     * @return the column name for the MEDICINE_SALES_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.MEDICINE_SALES_ID constant
     */
    public static String getMedicineSalesDetail_MedicineSalesId()
    {
        return "medicine_sales_detail.MEDICINE_SALES_ID";
    }
  
    /**
     * medicine_sales_detail.INDEX_NO
     * @return the column name for the INDEX_NO field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.INDEX_NO constant
     */
    public static String getMedicineSalesDetail_IndexNo()
    {
        return "medicine_sales_detail.INDEX_NO";
    }
  
    /**
     * medicine_sales_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.ITEM_ID constant
     */
    public static String getMedicineSalesDetail_ItemId()
    {
        return "medicine_sales_detail.ITEM_ID";
    }
  
    /**
     * medicine_sales_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.ITEM_CODE constant
     */
    public static String getMedicineSalesDetail_ItemCode()
    {
        return "medicine_sales_detail.ITEM_CODE";
    }
  
    /**
     * medicine_sales_detail.ITEM_NAME
     * @return the column name for the ITEM_NAME field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.ITEM_NAME constant
     */
    public static String getMedicineSalesDetail_ItemName()
    {
        return "medicine_sales_detail.ITEM_NAME";
    }
  
    /**
     * medicine_sales_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.DESCRIPTION constant
     */
    public static String getMedicineSalesDetail_Description()
    {
        return "medicine_sales_detail.DESCRIPTION";
    }
  
    /**
     * medicine_sales_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.UNIT_ID constant
     */
    public static String getMedicineSalesDetail_UnitId()
    {
        return "medicine_sales_detail.UNIT_ID";
    }
  
    /**
     * medicine_sales_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.UNIT_CODE constant
     */
    public static String getMedicineSalesDetail_UnitCode()
    {
        return "medicine_sales_detail.UNIT_CODE";
    }
  
    /**
     * medicine_sales_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.QTY constant
     */
    public static String getMedicineSalesDetail_Qty()
    {
        return "medicine_sales_detail.QTY";
    }
  
    /**
     * medicine_sales_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.QTY_BASE constant
     */
    public static String getMedicineSalesDetail_QtyBase()
    {
        return "medicine_sales_detail.QTY_BASE";
    }
  
    /**
     * medicine_sales_detail.RETURNED_QTY
     * @return the column name for the RETURNED_QTY field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.RETURNED_QTY constant
     */
    public static String getMedicineSalesDetail_ReturnedQty()
    {
        return "medicine_sales_detail.RETURNED_QTY";
    }
  
    /**
     * medicine_sales_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.ITEM_PRICE constant
     */
    public static String getMedicineSalesDetail_ItemPrice()
    {
        return "medicine_sales_detail.ITEM_PRICE";
    }
  
    /**
     * medicine_sales_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.TAX_ID constant
     */
    public static String getMedicineSalesDetail_TaxId()
    {
        return "medicine_sales_detail.TAX_ID";
    }
  
    /**
     * medicine_sales_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.TAX_AMOUNT constant
     */
    public static String getMedicineSalesDetail_TaxAmount()
    {
        return "medicine_sales_detail.TAX_AMOUNT";
    }
  
    /**
     * medicine_sales_detail.SUB_TOTAL_TAX
     * @return the column name for the SUB_TOTAL_TAX field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.SUB_TOTAL_TAX constant
     */
    public static String getMedicineSalesDetail_SubTotalTax()
    {
        return "medicine_sales_detail.SUB_TOTAL_TAX";
    }
  
    /**
     * medicine_sales_detail.DISCOUNT_ID
     * @return the column name for the DISCOUNT_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.DISCOUNT_ID constant
     */
    public static String getMedicineSalesDetail_DiscountId()
    {
        return "medicine_sales_detail.DISCOUNT_ID";
    }
  
    /**
     * medicine_sales_detail.DISCOUNT
     * @return the column name for the DISCOUNT field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.DISCOUNT constant
     */
    public static String getMedicineSalesDetail_Discount()
    {
        return "medicine_sales_detail.DISCOUNT";
    }
  
    /**
     * medicine_sales_detail.SUB_TOTAL_DISC
     * @return the column name for the SUB_TOTAL_DISC field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.SUB_TOTAL_DISC constant
     */
    public static String getMedicineSalesDetail_SubTotalDisc()
    {
        return "medicine_sales_detail.SUB_TOTAL_DISC";
    }
  
    /**
     * medicine_sales_detail.ITEM_COST
     * @return the column name for the ITEM_COST field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.ITEM_COST constant
     */
    public static String getMedicineSalesDetail_ItemCost()
    {
        return "medicine_sales_detail.ITEM_COST";
    }
  
    /**
     * medicine_sales_detail.SUB_TOTAL_COST
     * @return the column name for the SUB_TOTAL_COST field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.SUB_TOTAL_COST constant
     */
    public static String getMedicineSalesDetail_SubTotalCost()
    {
        return "medicine_sales_detail.SUB_TOTAL_COST";
    }
  
    /**
     * medicine_sales_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.SUB_TOTAL constant
     */
    public static String getMedicineSalesDetail_SubTotal()
    {
        return "medicine_sales_detail.SUB_TOTAL";
    }
  
    /**
     * medicine_sales_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.PROJECT_ID constant
     */
    public static String getMedicineSalesDetail_ProjectId()
    {
        return "medicine_sales_detail.PROJECT_ID";
    }
  
    /**
     * medicine_sales_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.DEPARTMENT_ID constant
     */
    public static String getMedicineSalesDetail_DepartmentId()
    {
        return "medicine_sales_detail.DEPARTMENT_ID";
    }
  
    /**
     * medicine_sales_detail.ITEM_TYPE
     * @return the column name for the ITEM_TYPE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.ITEM_TYPE constant
     */
    public static String getMedicineSalesDetail_ItemType()
    {
        return "medicine_sales_detail.ITEM_TYPE";
    }
  
    /**
     * medicine_sales_detail.TX_TYPE
     * @return the column name for the TX_TYPE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.TX_TYPE constant
     */
    public static String getMedicineSalesDetail_TxType()
    {
        return "medicine_sales_detail.TX_TYPE";
    }
  
    /**
     * medicine_sales_detail.PARENT_ID
     * @return the column name for the PARENT_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.PARENT_ID constant
     */
    public static String getMedicineSalesDetail_ParentId()
    {
        return "medicine_sales_detail.PARENT_ID";
    }
  
    /**
     * medicine_sales_detail.PRESCRIPTION_DETAIL_ID
     * @return the column name for the PRESCRIPTION_DETAIL_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.PRESCRIPTION_DETAIL_ID constant
     */
    public static String getMedicineSalesDetail_PrescriptionDetailId()
    {
        return "medicine_sales_detail.PRESCRIPTION_DETAIL_ID";
    }
  
    /**
     * medicine_sales_detail.PRESCRIPTION_ID
     * @return the column name for the PRESCRIPTION_ID field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.PRESCRIPTION_ID constant
     */
    public static String getMedicineSalesDetail_PrescriptionId()
    {
        return "medicine_sales_detail.PRESCRIPTION_ID";
    }
  
    /**
     * medicine_sales_detail.RFEE
     * @return the column name for the RFEE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.RFEE constant
     */
    public static String getMedicineSalesDetail_Rfee()
    {
        return "medicine_sales_detail.RFEE";
    }
  
    /**
     * medicine_sales_detail.PACKAGING
     * @return the column name for the PACKAGING field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.PACKAGING constant
     */
    public static String getMedicineSalesDetail_Packaging()
    {
        return "medicine_sales_detail.PACKAGING";
    }
  
    /**
     * medicine_sales_detail.TOTAL
     * @return the column name for the TOTAL field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.TOTAL constant
     */
    public static String getMedicineSalesDetail_Total()
    {
        return "medicine_sales_detail.TOTAL";
    }
  
    /**
     * medicine_sales_detail.BATCH_NO
     * @return the column name for the BATCH_NO field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.BATCH_NO constant
     */
    public static String getMedicineSalesDetail_BatchNo()
    {
        return "medicine_sales_detail.BATCH_NO";
    }
  
    /**
     * medicine_sales_detail.DOSAGE
     * @return the column name for the DOSAGE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.DOSAGE constant
     */
    public static String getMedicineSalesDetail_Dosage()
    {
        return "medicine_sales_detail.DOSAGE";
    }
  
    /**
     * medicine_sales_detail.BEFORE_AFTER
     * @return the column name for the BEFORE_AFTER field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.BEFORE_AFTER constant
     */
    public static String getMedicineSalesDetail_BeforeAfter()
    {
        return "medicine_sales_detail.BEFORE_AFTER";
    }
  
    /**
     * medicine_sales_detail.INSTRUCTION
     * @return the column name for the INSTRUCTION field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.INSTRUCTION constant
     */
    public static String getMedicineSalesDetail_Instruction()
    {
        return "medicine_sales_detail.INSTRUCTION";
    }
  
    /**
     * medicine_sales_detail.PRES_FILE
     * @return the column name for the PRES_FILE field
     * @deprecated use MedicineSalesDetailPeer.medicine_sales_detail.PRES_FILE constant
     */
    public static String getMedicineSalesDetail_PresFile()
    {
        return "medicine_sales_detail.PRES_FILE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("medicine_sales_detail");
        TableMap tMap = dbMap.getTable("medicine_sales_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("medicine_sales_detail.MEDICINE_SALES_DETAIL_ID", "");
                          tMap.addColumn("medicine_sales_detail.SALES_ORDER_ID", "");
                          tMap.addColumn("medicine_sales_detail.DELIVERY_ORDER_ID", "");
                          tMap.addColumn("medicine_sales_detail.DELIVERY_ORDER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "medicine_sales_detail.MEDICINE_SALES_ID", "" , "medicine_sales" ,
                "medicine_sales_id");
                            tMap.addColumn("medicine_sales_detail.INDEX_NO", Integer.valueOf(0));
                          tMap.addColumn("medicine_sales_detail.ITEM_ID", "");
                          tMap.addColumn("medicine_sales_detail.ITEM_CODE", "");
                          tMap.addColumn("medicine_sales_detail.ITEM_NAME", "");
                          tMap.addColumn("medicine_sales_detail.DESCRIPTION", "");
                          tMap.addColumn("medicine_sales_detail.UNIT_ID", "");
                          tMap.addColumn("medicine_sales_detail.UNIT_CODE", "");
                            tMap.addColumn("medicine_sales_detail.QTY", bd_ZERO);
                            tMap.addColumn("medicine_sales_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("medicine_sales_detail.RETURNED_QTY", bd_ZERO);
                            tMap.addColumn("medicine_sales_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("medicine_sales_detail.TAX_ID", "");
                            tMap.addColumn("medicine_sales_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("medicine_sales_detail.SUB_TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("medicine_sales_detail.DISCOUNT_ID", "");
                          tMap.addColumn("medicine_sales_detail.DISCOUNT", "");
                            tMap.addColumn("medicine_sales_detail.SUB_TOTAL_DISC", bd_ZERO);
                            tMap.addColumn("medicine_sales_detail.ITEM_COST", bd_ZERO);
                            tMap.addColumn("medicine_sales_detail.SUB_TOTAL_COST", bd_ZERO);
                            tMap.addColumn("medicine_sales_detail.SUB_TOTAL", bd_ZERO);
                          tMap.addColumn("medicine_sales_detail.PROJECT_ID", "");
                          tMap.addColumn("medicine_sales_detail.DEPARTMENT_ID", "");
                            tMap.addColumn("medicine_sales_detail.ITEM_TYPE", Integer.valueOf(0));
                            tMap.addColumn("medicine_sales_detail.TX_TYPE", Integer.valueOf(0));
                          tMap.addColumn("medicine_sales_detail.PARENT_ID", "");
                          tMap.addColumn("medicine_sales_detail.PRESCRIPTION_DETAIL_ID", "");
                          tMap.addColumn("medicine_sales_detail.PRESCRIPTION_ID", "");
                          tMap.addColumn("medicine_sales_detail.RFEE", "");
                          tMap.addColumn("medicine_sales_detail.PACKAGING", "");
                            tMap.addColumn("medicine_sales_detail.TOTAL", bd_ZERO);
                          tMap.addColumn("medicine_sales_detail.BATCH_NO", "");
                          tMap.addColumn("medicine_sales_detail.DOSAGE", "");
                          tMap.addColumn("medicine_sales_detail.BEFORE_AFTER", "");
                          tMap.addColumn("medicine_sales_detail.INSTRUCTION", "");
                          tMap.addColumn("medicine_sales_detail.PRES_FILE", "");
          }
}

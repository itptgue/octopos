package com.ssti.enterprise.pos.model;

import org.apache.torque.om.Persistent;

/**
 * Project, Department, 
 * @author Albert
 *
 */
public interface MasterOM extends Persistent
{
	public String getId();	
	public String getCode();
}

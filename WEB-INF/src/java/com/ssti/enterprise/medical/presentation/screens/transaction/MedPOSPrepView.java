package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-12-08
 * - change name from MedicineSalesView
 * 
 * </pre><br>
 */
public class MedPOSPrepView extends MedicalSalesView
{    
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
	}
}
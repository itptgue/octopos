package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PaymentEdcMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PaymentEdcMapBuilder";

    /**
     * Item
     * @deprecated use PaymentEdcPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "payment_edc";
    }

  
    /**
     * payment_edc.PAYMENT_EDC_ID
     * @return the column name for the PAYMENT_EDC_ID field
     * @deprecated use PaymentEdcPeer.payment_edc.PAYMENT_EDC_ID constant
     */
    public static String getPaymentEdc_PaymentEdcId()
    {
        return "payment_edc.PAYMENT_EDC_ID";
    }
  
    /**
     * payment_edc.START_DATE
     * @return the column name for the START_DATE field
     * @deprecated use PaymentEdcPeer.payment_edc.START_DATE constant
     */
    public static String getPaymentEdc_StartDate()
    {
        return "payment_edc.START_DATE";
    }
  
    /**
     * payment_edc.END_DATE
     * @return the column name for the END_DATE field
     * @deprecated use PaymentEdcPeer.payment_edc.END_DATE constant
     */
    public static String getPaymentEdc_EndDate()
    {
        return "payment_edc.END_DATE";
    }
  
    /**
     * payment_edc.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use PaymentEdcPeer.payment_edc.PAYMENT_TERM_ID constant
     */
    public static String getPaymentEdc_PaymentTermId()
    {
        return "payment_edc.PAYMENT_TERM_ID";
    }
  
    /**
     * payment_edc.ISSUER_ID
     * @return the column name for the ISSUER_ID field
     * @deprecated use PaymentEdcPeer.payment_edc.ISSUER_ID constant
     */
    public static String getPaymentEdc_IssuerId()
    {
        return "payment_edc.ISSUER_ID";
    }
  
    /**
     * payment_edc.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PaymentEdcPeer.payment_edc.DESCRIPTION constant
     */
    public static String getPaymentEdc_Description()
    {
        return "payment_edc.DESCRIPTION";
    }
  
    /**
     * payment_edc.RATE
     * @return the column name for the RATE field
     * @deprecated use PaymentEdcPeer.payment_edc.RATE constant
     */
    public static String getPaymentEdc_Rate()
    {
        return "payment_edc.RATE";
    }
  
    /**
     * payment_edc.PAYMENT_DAY
     * @return the column name for the PAYMENT_DAY field
     * @deprecated use PaymentEdcPeer.payment_edc.PAYMENT_DAY constant
     */
    public static String getPaymentEdc_PaymentDay()
    {
        return "payment_edc.PAYMENT_DAY";
    }
  
    /**
     * payment_edc.PAY_HOLIDAY
     * @return the column name for the PAY_HOLIDAY field
     * @deprecated use PaymentEdcPeer.payment_edc.PAY_HOLIDAY constant
     */
    public static String getPaymentEdc_PayHoliday()
    {
        return "payment_edc.PAY_HOLIDAY";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("payment_edc");
        TableMap tMap = dbMap.getTable("payment_edc");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("payment_edc.PAYMENT_EDC_ID", "");
                          tMap.addColumn("payment_edc.START_DATE", new Date());
                          tMap.addColumn("payment_edc.END_DATE", new Date());
                          tMap.addColumn("payment_edc.PAYMENT_TERM_ID", "");
                          tMap.addColumn("payment_edc.ISSUER_ID", "");
                          tMap.addColumn("payment_edc.DESCRIPTION", "");
                            tMap.addColumn("payment_edc.RATE", bd_ZERO);
                            tMap.addColumn("payment_edc.PAYMENT_DAY", Integer.valueOf(0));
                          tMap.addColumn("payment_edc.PAY_HOLIDAY", Boolean.TRUE);
          }
}

package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.report.SalesReportTool;

public class KategoriSalesReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    	    if (data.getParameters().getInt("Op") == 1)
    	    {
    	    	setParams(data);    	    	
    	    	String sKategoriID = data.getParameters().getString("KategoriId");    	    	
				String sCustID     = data.getParameters().getString("CustomerId");
				String sCustTypeID = data.getParameters().getString("CustomerTypeId");
				String sPmtTypeID  = data.getParameters().getString("PaymentTypeId");
				String sCashier    = data.getParameters().getString("Cashier");
				String sEmpID      = data.getParameters().getString("EmployeeId");
				String sVendorID   = data.getParameters().getString("VendorId");
				String sSiteID     = data.getParameters().getString("SiteId");
				boolean bSO = data.getParameters().getBoolean("FromSO");
				boolean bRet = data.getParameters().getBoolean("IncludeReturn");
				
				List vSales  = null;
				List vReturn = null;
				if (!bSO)
	    	    {
					vSales  = SalesReportTool.getTotalSalesPerKategori(
			    	        dStart, dEnd, sCustID, sCustTypeID, sPmtTypeID, sCashier, sEmpID, sVendorID, sSiteID, 
			    	        sLocationID, sDepartmentID, sProjectID, i_TRANS_PROCESSED, bIncTax);

					if(bRet)
					{
						vReturn = SalesReportTool.getTotalReturnPerKategori(
								  dStart, dEnd, sCustID, sCustTypeID, sPmtTypeID, sCashier, sEmpID, sVendorID, sSiteID, 
								  sLocationID, sDepartmentID, sProjectID, i_TRANS_PROCESSED, bIncTax);
					}
	    	    }
				else
				{
					vSales  = SalesReportTool.getTotalSOPerKategori(
			    	        dStart, dEnd, sCustID, sCustTypeID, sPmtTypeID, sCashier, sEmpID, sVendorID, sSiteID, 
			    	        sLocationID, sDepartmentID, sProjectID, i_TRANS_PROCESSED, bIncTax);					
				}
	    	    List vTree   = KategoriTool.getKategoriTreeList(sKategoriID, true);
			    
	    	    SalesReportTool oTool = SalesReportTool.getInstance();
	    	    context.put ("vResult", oTool.mergeSalesKategoriData (vTree, vSales, vReturn));
	        }
	    }   
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}    
    }    
}

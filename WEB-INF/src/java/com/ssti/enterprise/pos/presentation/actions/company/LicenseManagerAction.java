/*
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: E:/CVS/REPO/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/company/LicenseManagerAction.java,v $
 * Purpose: LicenseManager. To handle action related to license
 *
 * @author  $Author: Albert $
 * @version $Id: LicenseManagerAction.java,v 1.6 2005/03/28 09:18:10 Albert Exp $
 *
 * $Log: LicenseManagerAction.java,v $
 * Revision 1.6  2005/03/28 09:18:10  Albert
 * *** empty log message ***
 *
 * Revision 1.5  2004/11/18 07:17:25  albert
 * *** empty log message ***
 *
 */

package com.ssti.enterprise.pos.presentation.actions.company;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

public class LicenseManagerAction extends CompanySecureAction
{
    public void doPerform(RunData data, Context context)
    {
    	doActivate (data,context);
    }
    
	public void doActivate(RunData data, Context context)
    {
		//TODO this method maybe needed later if we'd like to upload license file
		//try {
        //
        //}
        //catch (Exception _oEx) 
        //{        	
        //    _oEx.printStackTrace();	
        //}
    }    
}

package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to DiscountCoupon
 */
public abstract class BaseDiscountCoupon extends BaseObject
{
    /** The Peer class */
    private static final DiscountCouponPeer peer =
        new DiscountCouponPeer();

        
    /** The value for the discountCouponId field */
    private String discountCouponId;
      
    /** The value for the discountId field */
    private String discountId;
      
    /** The value for the couponNo field */
    private String couponNo;
                                                
          
    /** The value for the couponValue field */
    private BigDecimal couponValue= bd_ZERO;
                                          
    /** The value for the couponType field */
    private int couponType = 1;
      
    /** The value for the createTransId field */
    private String createTransId;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the validFrom field */
    private Date validFrom;
      
    /** The value for the validTo field */
    private Date validTo;
                                                
    /** The value for the paymentTypeId field */
    private String paymentTypeId = "";
                                                
    /** The value for the useTransId field */
    private String useTransId = "";
                                                
    /** The value for the useCashier field */
    private String useCashier = "";
      
    /** The value for the useDate field */
    private Date useDate;
  
    
    /**
     * Get the DiscountCouponId
     *
     * @return String
     */
    public String getDiscountCouponId()
    {
        return discountCouponId;
    }

                        
    /**
     * Set the value of DiscountCouponId
     *
     * @param v new value
     */
    public void setDiscountCouponId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountCouponId, v))
              {
            this.discountCouponId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountId
     *
     * @return String
     */
    public String getDiscountId()
    {
        return discountId;
    }

                        
    /**
     * Set the value of DiscountId
     *
     * @param v new value
     */
    public void setDiscountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountId, v))
              {
            this.discountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponNo
     *
     * @return String
     */
    public String getCouponNo()
    {
        return couponNo;
    }

                        
    /**
     * Set the value of CouponNo
     *
     * @param v new value
     */
    public void setCouponNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.couponNo, v))
              {
            this.couponNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponValue
     *
     * @return BigDecimal
     */
    public BigDecimal getCouponValue()
    {
        return couponValue;
    }

                        
    /**
     * Set the value of CouponValue
     *
     * @param v new value
     */
    public void setCouponValue(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.couponValue, v))
              {
            this.couponValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CouponType
     *
     * @return int
     */
    public int getCouponType()
    {
        return couponType;
    }

                        
    /**
     * Set the value of CouponType
     *
     * @param v new value
     */
    public void setCouponType(int v) 
    {
    
                  if (this.couponType != v)
              {
            this.couponType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateTransId
     *
     * @return String
     */
    public String getCreateTransId()
    {
        return createTransId;
    }

                        
    /**
     * Set the value of CreateTransId
     *
     * @param v new value
     */
    public void setCreateTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createTransId, v))
              {
            this.createTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ValidFrom
     *
     * @return Date
     */
    public Date getValidFrom()
    {
        return validFrom;
    }

                        
    /**
     * Set the value of ValidFrom
     *
     * @param v new value
     */
    public void setValidFrom(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.validFrom, v))
              {
            this.validFrom = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ValidTo
     *
     * @return Date
     */
    public Date getValidTo()
    {
        return validTo;
    }

                        
    /**
     * Set the value of ValidTo
     *
     * @param v new value
     */
    public void setValidTo(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.validTo, v))
              {
            this.validTo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseTransId
     *
     * @return String
     */
    public String getUseTransId()
    {
        return useTransId;
    }

                        
    /**
     * Set the value of UseTransId
     *
     * @param v new value
     */
    public void setUseTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useTransId, v))
              {
            this.useTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseCashier
     *
     * @return String
     */
    public String getUseCashier()
    {
        return useCashier;
    }

                        
    /**
     * Set the value of UseCashier
     *
     * @param v new value
     */
    public void setUseCashier(String v) 
    {
    
                  if (!ObjectUtils.equals(this.useCashier, v))
              {
            this.useCashier = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UseDate
     *
     * @return Date
     */
    public Date getUseDate()
    {
        return useDate;
    }

                        
    /**
     * Set the value of UseDate
     *
     * @param v new value
     */
    public void setUseDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.useDate, v))
              {
            this.useDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("DiscountCouponId");
              fieldNames.add("DiscountId");
              fieldNames.add("CouponNo");
              fieldNames.add("CouponValue");
              fieldNames.add("CouponType");
              fieldNames.add("CreateTransId");
              fieldNames.add("CreateDate");
              fieldNames.add("ValidFrom");
              fieldNames.add("ValidTo");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("UseTransId");
              fieldNames.add("UseCashier");
              fieldNames.add("UseDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("DiscountCouponId"))
        {
                return getDiscountCouponId();
            }
          if (name.equals("DiscountId"))
        {
                return getDiscountId();
            }
          if (name.equals("CouponNo"))
        {
                return getCouponNo();
            }
          if (name.equals("CouponValue"))
        {
                return getCouponValue();
            }
          if (name.equals("CouponType"))
        {
                return Integer.valueOf(getCouponType());
            }
          if (name.equals("CreateTransId"))
        {
                return getCreateTransId();
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("ValidFrom"))
        {
                return getValidFrom();
            }
          if (name.equals("ValidTo"))
        {
                return getValidTo();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("UseTransId"))
        {
                return getUseTransId();
            }
          if (name.equals("UseCashier"))
        {
                return getUseCashier();
            }
          if (name.equals("UseDate"))
        {
                return getUseDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(DiscountCouponPeer.DISCOUNT_COUPON_ID))
        {
                return getDiscountCouponId();
            }
          if (name.equals(DiscountCouponPeer.DISCOUNT_ID))
        {
                return getDiscountId();
            }
          if (name.equals(DiscountCouponPeer.COUPON_NO))
        {
                return getCouponNo();
            }
          if (name.equals(DiscountCouponPeer.COUPON_VALUE))
        {
                return getCouponValue();
            }
          if (name.equals(DiscountCouponPeer.COUPON_TYPE))
        {
                return Integer.valueOf(getCouponType());
            }
          if (name.equals(DiscountCouponPeer.CREATE_TRANS_ID))
        {
                return getCreateTransId();
            }
          if (name.equals(DiscountCouponPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(DiscountCouponPeer.VALID_FROM))
        {
                return getValidFrom();
            }
          if (name.equals(DiscountCouponPeer.VALID_TO))
        {
                return getValidTo();
            }
          if (name.equals(DiscountCouponPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(DiscountCouponPeer.USE_TRANS_ID))
        {
                return getUseTransId();
            }
          if (name.equals(DiscountCouponPeer.USE_CASHIER))
        {
                return getUseCashier();
            }
          if (name.equals(DiscountCouponPeer.USE_DATE))
        {
                return getUseDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getDiscountCouponId();
            }
              if (pos == 1)
        {
                return getDiscountId();
            }
              if (pos == 2)
        {
                return getCouponNo();
            }
              if (pos == 3)
        {
                return getCouponValue();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getCouponType());
            }
              if (pos == 5)
        {
                return getCreateTransId();
            }
              if (pos == 6)
        {
                return getCreateDate();
            }
              if (pos == 7)
        {
                return getValidFrom();
            }
              if (pos == 8)
        {
                return getValidTo();
            }
              if (pos == 9)
        {
                return getPaymentTypeId();
            }
              if (pos == 10)
        {
                return getUseTransId();
            }
              if (pos == 11)
        {
                return getUseCashier();
            }
              if (pos == 12)
        {
                return getUseDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(DiscountCouponPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        DiscountCouponPeer.doInsert((DiscountCoupon) this, con);
                        setNew(false);
                    }
                    else
                    {
                        DiscountCouponPeer.doUpdate((DiscountCoupon) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key discountCouponId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setDiscountCouponId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setDiscountCouponId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getDiscountCouponId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public DiscountCoupon copy() throws TorqueException
    {
        return copyInto(new DiscountCoupon());
    }
  
    protected DiscountCoupon copyInto(DiscountCoupon copyObj) throws TorqueException
    {
          copyObj.setDiscountCouponId(discountCouponId);
          copyObj.setDiscountId(discountId);
          copyObj.setCouponNo(couponNo);
          copyObj.setCouponValue(couponValue);
          copyObj.setCouponType(couponType);
          copyObj.setCreateTransId(createTransId);
          copyObj.setCreateDate(createDate);
          copyObj.setValidFrom(validFrom);
          copyObj.setValidTo(validTo);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setUseTransId(useTransId);
          copyObj.setUseCashier(useCashier);
          copyObj.setUseDate(useDate);
  
                    copyObj.setDiscountCouponId((String)null);
                                                                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public DiscountCouponPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("DiscountCoupon\n");
        str.append("--------------\n")
           .append("DiscountCouponId     : ")
           .append(getDiscountCouponId())
           .append("\n")
           .append("DiscountId           : ")
           .append(getDiscountId())
           .append("\n")
           .append("CouponNo             : ")
           .append(getCouponNo())
           .append("\n")
           .append("CouponValue          : ")
           .append(getCouponValue())
           .append("\n")
           .append("CouponType           : ")
           .append(getCouponType())
           .append("\n")
           .append("CreateTransId        : ")
           .append(getCreateTransId())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("ValidFrom            : ")
           .append(getValidFrom())
           .append("\n")
           .append("ValidTo              : ")
           .append(getValidTo())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("UseTransId           : ")
           .append(getUseTransId())
           .append("\n")
           .append("UseCashier           : ")
           .append(getUseCashier())
           .append("\n")
           .append("UseDate              : ")
           .append(getUseDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

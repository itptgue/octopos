package com.ssti.enterprise.medical.tools;

import java.util.List;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Business object controller for MedicineSales OM
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.8  2009/05/04 01:38:15  albert
 * *** empty log message ***
 *
 * Revision 1.7  2008/06/29 07:02:06  albert
 * *** empty log message ***
 *
 * </pre><br>
 */
public class MedReturnTool extends BaseTool
{
	static MedReturnTool instance = null;
	
	public static synchronized MedReturnTool getInstance() 
	{
		if (instance == null) instance = new MedReturnTool();
		return instance;
	}		
	
	public static double[] calcRfeePackReturn (SalesReturn _oSR) 
		throws Exception
	{
		Item oRfee = MedConfigTool.getRfeeItem();
		Item oPack = MedConfigTool.getPackagingItem();
		double dRfee = 0;
		double dPack = 0;
		if (_oSR != null && oRfee != null && oPack != null)
		{
			List vDet = SalesReturnTool.getDetailsByID(_oSR.getSalesReturnId());
			for (int i = 0; i < vDet.size(); i++)
			{
				SalesReturnDetail oDet = (SalesReturnDetail) vDet.get(i);
				if (oDet.getItemId().equals(oRfee.getItemId())) 
				{
					dRfee += oDet.getReturnAmount().doubleValue();
				}
				if (oDet.getItemId().equals(oPack.getItemId())) 
				{
					dPack += oDet.getReturnAmount().doubleValue();
				}
			}
		}
		double[] dResult = {dRfee, dPack};
		return dResult;
	}
}
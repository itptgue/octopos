package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorBalance;
import com.ssti.enterprise.pos.om.VendorBalancePeer;
import com.ssti.enterprise.pos.om.VendorPeer;
import com.ssti.enterprise.pos.om.VendorType;
import com.ssti.enterprise.pos.om.VendorTypePeer;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: VendorManager.java,v 1.7 2008/03/03 01:54:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: VendorManager.java,v $
 * Revision 1.7  2008/03/03 01:54:17  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/07/10 03:32:56  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/03/12 08:53:43  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class VendorManager
{   
	private static Log log = LogFactory.getLog(VendorManager.class);
	 
    private static VendorManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "vendor";      

    private VendorManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static VendorManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (VendorManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new VendorManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllVendorType ()
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllVendorType");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (VendorTypePeer.VENDOR_TYPE_CODE);	
			List vData = VendorTypePeer.doSelect(oCrit);
			oCache.put(new Element("AllVendorType", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public VendorType getVendorTypeByID(String _sID, Connection _oConn)
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("VendorType" + _sID);
		if (oElement == null )
		{		
			if (StringUtil.isNotEmpty(_sID))
			{			
				Criteria oCrit = new Criteria();
				oCrit.add(VendorTypePeer.VENDOR_TYPE_ID, _sID);
				List vData = VendorTypePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("VendorType" + _sID, (Serializable)vData.get(0))); 
					return (VendorType) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (VendorType) oElement.getValue();
		}
	}	
    
	public List getAllVendor ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllVendor");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (VendorPeer.VENDOR_NAME);	
			List vData = VendorPeer.doSelect(oCrit);
			oCache.put(new Element ("AllVendor", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	

	public List getActiveVendor()
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("ActiveVendor");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(VendorPeer.VENDOR_STATUS, 1);
			oCrit.addAscendingOrderByColumn (VendorPeer.VENDOR_NAME);	
			List vData = VendorPeer.doSelect(oCrit);
			oCache.put(new Element ("ActiveVendor", (Serializable)vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	
	
	public Vendor getVendorByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Vendor" + _sID);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty (_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(VendorPeer.VENDOR_ID, _sID);
				List vData = VendorPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("Vendor" + _sID, (Serializable)vData.get(0))); 
					return (Vendor) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Vendor) oElement.getValue();
		}
	}

	public Vendor getVendorByCode(String _sCode, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("VendorByCode" + _sCode);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty (_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(VendorPeer.VENDOR_CODE, _sCode);
				List vData = VendorPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("VendorByCode" + _sCode, (Serializable)vData.get(0))); 
					return (Vendor) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Vendor) oElement.getValue();
		}
	}	
	
	public VendorBalance getVendorBalance(String _sID, Connection _oConn)
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("VendorBalance" + _sID);
		
		if (oElement == null)
		{
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(VendorBalancePeer.VENDOR_ID, _sID);
				List vData = VendorBalancePeer.doSelect(oCrit, _oConn);
				if(vData.size() > 0)
				{
					VendorBalance oBalance = (VendorBalance) vData.get(0);
					oCache.put(new Element ("VendorBalance" + _sID, (Serializable)oBalance)); 
					return oBalance;
				}
				else
				{
					oCache.put(new Element ("VendorBalance" + _sID, null)); 				
				}
			}
			return null;
		}
		return (VendorBalance) oElement.getValue();
	}	
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Vendor _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if (_oData != null && StringUtil.isNotEmpty(_oData.getVendorId())) 
        {
            oCache.remove("Vendor" + _oData.getVendorId());
            oCache.remove("VendorByCode" + _oData.getVendorCode());    
            oCache.put(new Element ("Vendor" + _oData.getVendorId(), _oData));
            oCache.put(new Element ("VendorByCode" + _oData.getVendorCode(), _oData));
            oCache.remove("AllVendor");
            oCache.remove("ActiveVendor");
        }
        else
        {
            oCache.removeAll();
        }
    }
	
	//for delete
	public void refreshCache (String _sID, String _sCode) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if (StringUtil.isNotEmpty(_sID))
        {
            oCache.remove("Vendor" + _sID);
            oCache.remove("VendorByCode" + _sCode);
            oCache.remove("AllVendor");
            oCache.remove("ActiveVendor");
            oCache.remove("VendorBalance" + _sID);  
        }
        else
        {
            oCache.removeAll();
        }
    } 
	
	public void refreshCache (VendorBalance _oData) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE); 
	    if (_oData != null && StringUtil.isNotEmpty(_oData.getVendorId()) )
	    {
	    	oCache.remove("VendorBalance" + _oData.getVendorId());
	    }
	}
	
	public void refreshType (VendorType _oData) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);         
	    if (_oData != null && StringUtil.isNotEmpty(_oData.getVendorTypeId()))
	    {
	        oCache.remove("VendorType" + _oData.getVendorTypeId());
            oCache.put(new Element ("VendorType" + _oData.getVendorTypeId(), _oData));
            oCache.remove("AllVendorType");
        }	    
        else
        {
            oCache.removeAll();
        }
	}	

}
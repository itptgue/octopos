package com.ssti.framework.presentation;

import org.apache.turbine.modules.screens.VelocitySecureScreen;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base class for all non secure screen
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SecureScreen.java,v 1.12 2007/07/21 13:50:32 albert Exp $ <br>
 *
 * <pre>
 * $Log: NonSecureScreen.java,v $
 * 
 * 2015-06-08
 * - New BaseClass for screens accessible without login
 * 
 * </pre><br>
 *
 */
public class NonSecureScreen extends VelocitySecureScreen implements SecureAttributes
{	
    public void doBuildTemplate (RunData data, Context context)
    {
    }
	
    protected boolean isAuthorized (RunData data)  
    	throws Exception 
    {        
        return true;
    }

    protected boolean isAuthorized(RunData data, String[] _aPerms)
		throws Exception
	{
	    return true;
	}
}

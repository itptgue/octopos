package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemField
 */
public abstract class BaseItemField extends BaseObject
{
    /** The Peer class */
    private static final ItemFieldPeer peer =
        new ItemFieldPeer();

        
    /** The value for the itemFieldId field */
    private String itemFieldId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the fieldName field */
    private String fieldName;
      
    /** The value for the fieldValue field */
    private String fieldValue;
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the ItemFieldId
     *
     * @return String
     */
    public String getItemFieldId()
    {
        return itemFieldId;
    }

                        
    /**
     * Set the value of ItemFieldId
     *
     * @param v new value
     */
    public void setItemFieldId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemFieldId, v))
              {
            this.itemFieldId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldName
     *
     * @return String
     */
    public String getFieldName()
    {
        return fieldName;
    }

                        
    /**
     * Set the value of FieldName
     *
     * @param v new value
     */
    public void setFieldName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldName, v))
              {
            this.fieldName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FieldValue
     *
     * @return String
     */
    public String getFieldValue()
    {
        return fieldValue;
    }

                        
    /**
     * Set the value of FieldValue
     *
     * @param v new value
     */
    public void setFieldValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fieldValue, v))
              {
            this.fieldValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemFieldId");
              fieldNames.add("ItemId");
              fieldNames.add("FieldName");
              fieldNames.add("FieldValue");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemFieldId"))
        {
                return getItemFieldId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("FieldName"))
        {
                return getFieldName();
            }
          if (name.equals("FieldValue"))
        {
                return getFieldValue();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemFieldPeer.ITEM_FIELD_ID))
        {
                return getItemFieldId();
            }
          if (name.equals(ItemFieldPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemFieldPeer.FIELD_NAME))
        {
                return getFieldName();
            }
          if (name.equals(ItemFieldPeer.FIELD_VALUE))
        {
                return getFieldValue();
            }
          if (name.equals(ItemFieldPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemFieldId();
            }
              if (pos == 1)
        {
                return getItemId();
            }
              if (pos == 2)
        {
                return getFieldName();
            }
              if (pos == 3)
        {
                return getFieldValue();
            }
              if (pos == 4)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemFieldPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemFieldPeer.doInsert((ItemField) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemFieldPeer.doUpdate((ItemField) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemFieldId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemFieldId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemFieldId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemFieldId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemField copy() throws TorqueException
    {
        return copyInto(new ItemField());
    }
  
    protected ItemField copyInto(ItemField copyObj) throws TorqueException
    {
          copyObj.setItemFieldId(itemFieldId);
          copyObj.setItemId(itemId);
          copyObj.setFieldName(fieldName);
          copyObj.setFieldValue(fieldValue);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setItemFieldId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemFieldPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemField\n");
        str.append("---------\n")
           .append("ItemFieldId          : ")
           .append(getItemFieldId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("FieldName            : ")
           .append(getFieldName())
           .append("\n")
           .append("FieldValue           : ")
           .append(getFieldValue())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

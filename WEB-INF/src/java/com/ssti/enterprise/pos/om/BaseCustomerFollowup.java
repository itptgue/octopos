package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CustomerFollowup
 */
public abstract class BaseCustomerFollowup extends BaseObject
{
    /** The Peer class */
    private static final CustomerFollowupPeer peer =
        new CustomerFollowupPeer();

        
    /** The value for the customerFollowupId field */
    private String customerFollowupId;
      
    /** The value for the customerId field */
    private String customerId;
                                                
    /** The value for the customerContactId field */
    private String customerContactId = "";
                                                
    /** The value for the contactName field */
    private String contactName = "";
      
    /** The value for the contactDate field */
    private Date contactDate;
      
    /** The value for the userName field */
    private String userName;
                                                
    /** The value for the contactTopic field */
    private String contactTopic = "";
      
    /** The value for the contactRemark field */
    private String contactRemark;
  
    
    /**
     * Get the CustomerFollowupId
     *
     * @return String
     */
    public String getCustomerFollowupId()
    {
        return customerFollowupId;
    }

                        
    /**
     * Set the value of CustomerFollowupId
     *
     * @param v new value
     */
    public void setCustomerFollowupId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerFollowupId, v))
              {
            this.customerFollowupId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                              
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
                          
                if (aCustomer != null && !ObjectUtils.equals(aCustomer.getCustomerId(), v))
                {
            aCustomer = null;
        }
      
              }
  
    /**
     * Get the CustomerContactId
     *
     * @return String
     */
    public String getCustomerContactId()
    {
        return customerContactId;
    }

                        
    /**
     * Set the value of CustomerContactId
     *
     * @param v new value
     */
    public void setCustomerContactId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerContactId, v))
              {
            this.customerContactId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName
     *
     * @return String
     */
    public String getContactName()
    {
        return contactName;
    }

                        
    /**
     * Set the value of ContactName
     *
     * @param v new value
     */
    public void setContactName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName, v))
              {
            this.contactName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactDate
     *
     * @return Date
     */
    public Date getContactDate()
    {
        return contactDate;
    }

                        
    /**
     * Set the value of ContactDate
     *
     * @param v new value
     */
    public void setContactDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.contactDate, v))
              {
            this.contactDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactTopic
     *
     * @return String
     */
    public String getContactTopic()
    {
        return contactTopic;
    }

                        
    /**
     * Set the value of ContactTopic
     *
     * @param v new value
     */
    public void setContactTopic(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactTopic, v))
              {
            this.contactTopic = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactRemark
     *
     * @return String
     */
    public String getContactRemark()
    {
        return contactRemark;
    }

                        
    /**
     * Set the value of ContactRemark
     *
     * @param v new value
     */
    public void setContactRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactRemark, v))
              {
            this.contactRemark = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Customer aCustomer;

    /**
     * Declares an association between this object and a Customer object
     *
     * @param v Customer
     * @throws TorqueException
     */
    public void setCustomer(Customer v) throws TorqueException
    {
            if (v == null)
        {
                  setCustomerId((String) null);
              }
        else
        {
            setCustomerId(v.getCustomerId());
        }
            aCustomer = v;
    }

                                            
    /**
     * Get the associated Customer object
     *
     * @return the associated Customer object
     * @throws TorqueException
     */
    public Customer getCustomer() throws TorqueException
    {
        if (aCustomer == null && (!ObjectUtils.equals(this.customerId, null)))
        {
                          aCustomer = CustomerPeer.retrieveByPK(SimpleKey.keyFor(this.customerId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Customer obj = CustomerPeer.retrieveByPK(this.customerId);
               obj.addCustomerFollowups(this);
            */
        }
        return aCustomer;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setCustomerKey(ObjectKey key) throws TorqueException
    {
      
                        setCustomerId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CustomerFollowupId");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerContactId");
              fieldNames.add("ContactName");
              fieldNames.add("ContactDate");
              fieldNames.add("UserName");
              fieldNames.add("ContactTopic");
              fieldNames.add("ContactRemark");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CustomerFollowupId"))
        {
                return getCustomerFollowupId();
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerContactId"))
        {
                return getCustomerContactId();
            }
          if (name.equals("ContactName"))
        {
                return getContactName();
            }
          if (name.equals("ContactDate"))
        {
                return getContactDate();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("ContactTopic"))
        {
                return getContactTopic();
            }
          if (name.equals("ContactRemark"))
        {
                return getContactRemark();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CustomerFollowupPeer.CUSTOMER_FOLLOWUP_ID))
        {
                return getCustomerFollowupId();
            }
          if (name.equals(CustomerFollowupPeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(CustomerFollowupPeer.CUSTOMER_CONTACT_ID))
        {
                return getCustomerContactId();
            }
          if (name.equals(CustomerFollowupPeer.CONTACT_NAME))
        {
                return getContactName();
            }
          if (name.equals(CustomerFollowupPeer.CONTACT_DATE))
        {
                return getContactDate();
            }
          if (name.equals(CustomerFollowupPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(CustomerFollowupPeer.CONTACT_TOPIC))
        {
                return getContactTopic();
            }
          if (name.equals(CustomerFollowupPeer.CONTACT_REMARK))
        {
                return getContactRemark();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCustomerFollowupId();
            }
              if (pos == 1)
        {
                return getCustomerId();
            }
              if (pos == 2)
        {
                return getCustomerContactId();
            }
              if (pos == 3)
        {
                return getContactName();
            }
              if (pos == 4)
        {
                return getContactDate();
            }
              if (pos == 5)
        {
                return getUserName();
            }
              if (pos == 6)
        {
                return getContactTopic();
            }
              if (pos == 7)
        {
                return getContactRemark();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CustomerFollowupPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CustomerFollowupPeer.doInsert((CustomerFollowup) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CustomerFollowupPeer.doUpdate((CustomerFollowup) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key customerFollowupId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCustomerFollowupId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCustomerFollowupId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCustomerFollowupId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CustomerFollowup copy() throws TorqueException
    {
        return copyInto(new CustomerFollowup());
    }
  
    protected CustomerFollowup copyInto(CustomerFollowup copyObj) throws TorqueException
    {
          copyObj.setCustomerFollowupId(customerFollowupId);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerContactId(customerContactId);
          copyObj.setContactName(contactName);
          copyObj.setContactDate(contactDate);
          copyObj.setUserName(userName);
          copyObj.setContactTopic(contactTopic);
          copyObj.setContactRemark(contactRemark);
  
                    copyObj.setCustomerFollowupId((String)null);
                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CustomerFollowupPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CustomerFollowup\n");
        str.append("----------------\n")
           .append("CustomerFollowupId   : ")
           .append(getCustomerFollowupId())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerContactId    : ")
           .append(getCustomerContactId())
           .append("\n")
           .append("ContactName          : ")
           .append(getContactName())
           .append("\n")
           .append("ContactDate          : ")
           .append(getContactDate())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("ContactTopic         : ")
           .append(getContactTopic())
           .append("\n")
           .append("ContactRemark        : ")
           .append(getContactRemark())
           .append("\n")
        ;
        return(str.toString());
    }
}

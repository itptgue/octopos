package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.BankTransferTool;
import com.ssti.framework.turbine.LargeSelectHandler;

public class BankTransferView extends TransactionViewScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
        try 
    	{
            super.doBuildTemplate(data, context);
            int iOp = data.getParameters().getInt("op");
            System.out.println("op" + iOp);
            if (iOp == 1)
            {
                doFind(data,context);
                data.getParameters().setString("op","next");
            }
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findBankTrfResult", "vBT");
            context.put(BankTransferTool.s_CTX,BankTransferTool.getInstance());
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
    
    
    public void doFind(RunData data, Context context)
    {
        try
        {
            super.doFind(data);
            String sBankID = data.getParameters().getString ("BankId");
            int iStatus = data.getParameters().getInt ("Status");            
            LargeSelect vTR = BankTransferTool.findData (iCond, sKeywords, sBankID, sLocationID,
                                                         iStatus, dStart, dEnd, iLimit);
            
            data.getUser().setTemp("findBankTrfResult", vTR);
        }
        catch (Exception _oEx)
        {
            _oEx.printStackTrace();         
            data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }
    
}

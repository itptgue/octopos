package com.ssti.enterprise.pos.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.BankManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

public class BankLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (BankLoader.class );
	
	public static final String s_START_HEADING = "Bank Code";
	
	public BankLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sBankCode = getString(_oRow, _iIdx + i); i++;
			String sBankName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sBankCode))
			{				
				Bank oBank = BankTool.getBankByCode(sBankCode);
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Bank Code : " + sBankCode);
				
				if (oBank == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					oBank = new Bank();
					oBank.setBankId (IDGenerator.generateSysID());
					oBank.setBankCode(sBankCode); 	
					bNew = true;
				}
				else 
				{
					bNew = false;
				}
				oBank.setDescription(sBankName); 
				bValid = validateString("Bank Name", oBank.getDescription(), 100, oEmpty, oLength);

				String sCurrCode = getString(_oRow, _iIdx + i); i++;
				Currency oCurr = CurrencyTool.getCurrencyByCode(sCurrCode);
	            if (oCurr == null)
	            {
	                m_oRejected.append (sBankCode + " Currency Code '" + sCurrCode + "' Is Invalid \n"); 
	                bValid = false;   
	                m_iRejected++; 
	            }
	            else
	            {
	            	oBank.setCurrencyId(oCurr.getCurrencyId());
	            }
				oBank.setAccountNo (getString(_oRow, _iIdx + i)); i++;

                String sBankAcc = getString(_oRow, _iIdx + i); i++;
                Account oAcc = AccountTool.getAccountByCode(sBankAcc, false);
				String sAccID = "";
                if (oAcc != null) sAccID = oAcc.getAccountId();
                oBank.setAccountId(sAccID);

                String sLoc = getString(_oRow, _iIdx + i); i++;
                Location oLoc = LocationTool.getLocationByCode(sLoc);
				String sLocID = "";
                if (oLoc != null) sLocID = oLoc.getLocationId();
                oBank.setLocationId(sLocID);

                oBank.save();
                BankManager.getInstance().refreshCache(oBank);
            	
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sBankCode,30));
						m_oNewData.append (StringUtil.left(sBankName,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sBankCode,30));
						m_oUpdated.append (StringUtil.left(sBankName,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sBankCode,30));
				    m_oRejected.append (StringUtil.left(sBankName,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}    
		
	}
}

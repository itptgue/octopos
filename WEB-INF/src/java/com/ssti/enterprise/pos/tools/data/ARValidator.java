package com.ssti.enterprise.pos.tools.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerBalance;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.financial.AccountReceivableTool;
import com.ssti.enterprise.pos.tools.financial.CustomerBalanceTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate CustomerBalance and AccountReceivable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ARValidator.java,v 1.5 2007/12/20 13:48:25 albert Exp $ <br>
 *
 * <pre>
 * $Log: ARValidator.java,v $
 * Revision 1.5  2007/12/20 13:48:25  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class ARValidator extends BaseValidator implements AppAttributes
{
	Log log = LogFactory.getLog(getClass());
	
	static final String[] s1 = {"No", "Customer", "Debit", "Credit", "Activity Balance", "Current Balance"};
	static final int[] l1 = {5, 25, 20, 20, 20, 20};
	static final int[] a1 = {0, 0, 1, 1, 1, 1};
	
	/**
	 * validate AR balance between account_receivable entry
	 * and customer_balance
	 */
	public void validateBalance()
	{
		Date dNow = new Date();		
		prepareTitle ("Validate AR Result : ", s1, l1, a1);			
		
		try
		{							
			List vData = CustomerTool.getAllCustomer();
			List vCurrency = CurrencyTool.getAllCurrency();
			
			for (int i = 0; i < vData.size(); i++)
			{
				Customer oData = (Customer) vData.get(i);
				String sID = oData.getCustomerId();
			
				Date dAsOf = oData.getAsDate();
				
				CustomerBalance oBalance = CustomerBalanceTool.getCustomerBalance(sID);
				if (oBalance != null)
				{
					double dBalance = oBalance.getArBalance().setScale(2,4).doubleValue();					
					//get all cashflow transaction since beginning balance until now then recalculate 
				    List vTrans = AccountReceivableTool.getTransactionHistory(dAsOf, dNow, sID, -1);
			        double[] dDebit = AccountReceivableTool.filterTotalAmountByType (vTrans, 1);
				    double[] dCredit = AccountReceivableTool.filterTotalAmountByType (vTrans, 2);
					double dTrans = dDebit[i_BASE] - dCredit[i_BASE];
					dTrans = (new BigDecimal(dTrans).setScale(2,4)).doubleValue();
					
					if (dBalance != dTrans)
					{
						m_iInvalidResult ++;
						m_oResult.append("\n");
						
						append (null, l1[0]);
						append (StringUtil.cut(oData.getCustomerName(),22,".."), l1[1]);
						append (new Double(dDebit[i_BASE]), l1[2]);
						append (new Double(dCredit[i_BASE]), l1[3]);
						
						append (new Double(dTrans), l1[4]);
						append (new Double(dBalance), l1[5]);
					}	
				}
			}
			footer();
		}
		catch (Exception _oEx)
		{
			handleError (oConn, _oEx);
		}
	}
}

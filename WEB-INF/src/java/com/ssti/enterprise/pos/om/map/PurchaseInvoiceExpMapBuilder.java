package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseInvoiceExpMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseInvoiceExpMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseInvoiceExpPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_invoice_exp";
    }

  
    /**
     * purchase_invoice_exp.PURCHASE_INVOICE_EXP_ID
     * @return the column name for the PURCHASE_INVOICE_EXP_ID field
     * @deprecated use PurchaseInvoiceExpPeer.purchase_invoice_exp.PURCHASE_INVOICE_EXP_ID constant
     */
    public static String getPurchaseInvoiceExp_PurchaseInvoiceExpId()
    {
        return "purchase_invoice_exp.PURCHASE_INVOICE_EXP_ID";
    }
  
    /**
     * purchase_invoice_exp.PURCHASE_INVOICE_ID
     * @return the column name for the PURCHASE_INVOICE_ID field
     * @deprecated use PurchaseInvoiceExpPeer.purchase_invoice_exp.PURCHASE_INVOICE_ID constant
     */
    public static String getPurchaseInvoiceExp_PurchaseInvoiceId()
    {
        return "purchase_invoice_exp.PURCHASE_INVOICE_ID";
    }
  
    /**
     * purchase_invoice_exp.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use PurchaseInvoiceExpPeer.purchase_invoice_exp.ACCOUNT_ID constant
     */
    public static String getPurchaseInvoiceExp_AccountId()
    {
        return "purchase_invoice_exp.ACCOUNT_ID";
    }
  
    /**
     * purchase_invoice_exp.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PurchaseInvoiceExpPeer.purchase_invoice_exp.DESCRIPTION constant
     */
    public static String getPurchaseInvoiceExp_Description()
    {
        return "purchase_invoice_exp.DESCRIPTION";
    }
  
    /**
     * purchase_invoice_exp.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use PurchaseInvoiceExpPeer.purchase_invoice_exp.AMOUNT constant
     */
    public static String getPurchaseInvoiceExp_Amount()
    {
        return "purchase_invoice_exp.AMOUNT";
    }
  
    /**
     * purchase_invoice_exp.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use PurchaseInvoiceExpPeer.purchase_invoice_exp.PROJECT_ID constant
     */
    public static String getPurchaseInvoiceExp_ProjectId()
    {
        return "purchase_invoice_exp.PROJECT_ID";
    }
  
    /**
     * purchase_invoice_exp.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use PurchaseInvoiceExpPeer.purchase_invoice_exp.DEPARTMENT_ID constant
     */
    public static String getPurchaseInvoiceExp_DepartmentId()
    {
        return "purchase_invoice_exp.DEPARTMENT_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_invoice_exp");
        TableMap tMap = dbMap.getTable("purchase_invoice_exp");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_invoice_exp.PURCHASE_INVOICE_EXP_ID", "");
                          tMap.addForeignKey(
                "purchase_invoice_exp.PURCHASE_INVOICE_ID", "" , "purchase_invoice" ,
                "purchase_invoice_id");
                          tMap.addColumn("purchase_invoice_exp.ACCOUNT_ID", "");
                          tMap.addColumn("purchase_invoice_exp.DESCRIPTION", "");
                            tMap.addColumn("purchase_invoice_exp.AMOUNT", bd_ZERO);
                          tMap.addColumn("purchase_invoice_exp.PROJECT_ID", "");
                          tMap.addColumn("purchase_invoice_exp.DEPARTMENT_ID", "");
          }
}

package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.PurchaseInvoiceLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.TransactionLoader;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: Purchase Invoice Action Class</b><br>
 *  
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseInvoiceAction.java,v 1.35 2008/06/29 07:04:44 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseInvoiceAction.java,v $
 * 
 * 2018-04-02
 * - change method doSave, add parameter from PurchaseTaxAmt,PurchaseTaxAmtTax to savePurchaseSerial
 * 
 * 2017-01-03
 * - change method doSave, add parameter from PurchaseTaxDate to savePurchaseSerial
 * 
 * </pre><br>
 */
public class PurchaseInvoiceAction extends TransactionSecureAction
{
	//TODO: implements update in purchase invoice, permission already prepared, 
	//but add in turbine-security.xml also

	private static final String s_VIEW_PERM   = "View Purchase Invoice";
	private static final String s_CREATE_PERM = "Create Purchase Invoice";
	private static final String s_UPDATE_PERM = "Update Purchase Invoice";
	private static final String s_CANCEL_PERM = "Cancel Purchase Invoice";
	
    private static final String[] a_CREATE_PERM = {s_PURCHASE_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_UPDATE_PERM = {s_PURCHASE_PERM, s_VIEW_PERM, s_UPDATE_PERM};    
    private static final String[] a_CANCEL_PERM = {s_PURCHASE_PERM, s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM   = {s_PURCHASE_PERM, s_VIEW_PERM};
    
    private static final Log log = LogFactory.getLog(PurchaseInvoiceAction.class);
    
	private HttpSession oPISes = null;
    private PurchaseInvoice oPI;
    private List vPID;
    private List vPIF;
    private List vPIE;
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        if (data.getParameters().getInt("save") == 1)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
        else if (data.getParameters().getInt("save") == 2)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
    	else 
    	{
    		return isAuthorized (data, a_VIEW_PERM);    	    
        }
    }
    
    public void doPerform (RunData data, Context context)
        throws Exception
    {
    	if (data.getParameters().getInt("save") == 1) 
    	{
    		doSave (data,context);
    	}
    	else if (data.getParameters().getInt("save") == 2) 
    	{
    		doCancel (data,context);
    	}
    	else 
    	{
    		doFind (data,context);
    	}
    }

    public void doFind (RunData data, Context context)
        throws Exception
    {
    	super.doFind(data);
    	String sVendorID = data.getParameters().getString("VendorId", "");    	
    	int iPaymentStatus = data.getParameters().getInt("PaymentStatus");
		int iStatus = data.getParameters().getInt("Status");
		LargeSelect vTR = PurchaseInvoiceTool.findData (
			iCond, sKeywords, dStart, dEnd, sVendorID, sLocationID, iPaymentStatus, iStatus, iLimit, sCurrencyID
		);
		data.getUser().setTemp("findPurchaseInvoiceResult", vTR);	
	}
	
	public void doSave (RunData data, Context context)
        throws Exception
    {
		try
		{
			initSession(data);
			PurchaseInvoiceTool.updateDetail(vPID,data);
			PurchaseInvoiceTool.setHeaderProperties (oPI,vPID,vPIE,data);
			prepareTransData (data);
			PurchaseInvoiceTool.saveData (oPI,vPID,vPIF,vPIE,null);			
        	data.setMessage(LocaleTool.getString("pi_save_success"));
	       	resetSession(data);
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();
        	handleError (data, LocaleTool.getString("pi_save_failed"), _oEx);
        }
        
        //save purchase tax serial
		try
		{
			String sTaxNo = data.getParameters().getString("PurchaseTaxSerial"); 
			String sTaxDt = data.getParameters().getString("PurchaseTaxDate","");
			BigDecimal dAmt = data.getParameters().getBigDecimal("PurchaseTaxAmt",oPI.getTotalAmount());
			BigDecimal dTax = data.getParameters().getBigDecimal("PurchaseTaxAmtTax",oPI.getTotalTax());
			Date dTaxDt = new Date();
			if(StringUtil.isNotEmpty(sTaxDt))
			{
				dTaxDt = CustomParser.parseDate(sTaxDt);
			}
			if (StringUtil.isNotEmpty(sTaxNo))
			{
				TaxTool.savePurchaseSerial(oPI.getPurchaseInvoiceId(), sTaxNo, dTaxDt, dAmt, dTax);
			}
		}
        catch (Exception _oEx)
        {
        	log.error("ERROR Saving Purchase Tax Serial: " + _oEx.getMessage(), _oEx);
        }        
    }

	/**
	 * 
	 * @param data
	 * @param context
	 * @throws Exception
	 */
	public void doSaveEdit (RunData data, Context context) throws Exception
	{
		try
		{
			String sInvNo = data.getParameters().getString("EditInvNo");
			log.debug("Save Edit Purchase Invoice: " + sInvNo);
			if (StringUtil.isNotEmpty(sInvNo))
			{
				initSession(data);
				PurchaseInvoiceTool.updateDetail(vPID,data);
				PurchaseInvoiceTool.setHeaderProperties (oPI,vPID,vPIE,data);

				prepareTransData(data);	        	        
				oPI.setPurchaseInvoiceNo(sInvNo);
				oPI.setStatus(i_PROCESSED);

				PurchaseInvoiceTool.saveEdit(oPI, vPID, vPIF, vPIE, data.getUser().getName());      
				data.setMessage(LocaleTool.getString("pi_save_success") + " (EDIT)");
				resetSession (data);
				context.put(s_PRINT_READY, Boolean.valueOf(true));

				//reset state
				data.getParameters().setString("op","99");
			}
			else
			{
				data.setMessage("No Invoice To Edit");
			}
		}
		catch (Exception _oEx) 
		{
			//must create new trans, no retry allowed 
			handleError (data, LocaleTool.getString("pi_save_failed"), _oEx);
		}
	}

	public void doCancel (RunData data, Context context)
	    throws Exception
	{
		try
		{
			initSession(data);
			PurchaseInvoiceTool.cancelPI(oPI, vPID, vPIF, data.getUser().getName(), null);
	    	data.setMessage(LocaleTool.getString("pi_cancel_success"));
	       	resetSession(data);
	    }
	    catch (Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("pi_cancel_failed"), _oEx);
	    }
	}
	
	
    private void initSession ( RunData data )
    	throws Exception
    {
    	synchronized(this)
    	{
    		oPISes = data.getSession();
    		if ( oPISes.getAttribute (s_PI) == null ||
    				oPISes.getAttribute (s_PID) == null )
    		{
    			throw new Exception ("Purchase Invoice Data Invalid");
    		}
    		oPI = (PurchaseInvoice) oPISes.getAttribute (s_PI);
    		vPID = (List) oPISes.getAttribute (s_PID);
    		vPIF = (List) oPISes.getAttribute (s_PIF);
    		vPIE = (List) oPISes.getAttribute (s_PIE);    		
    	}
    }

    private void prepareTransData ( RunData data )
    	throws Exception
    {
    	//check transaction data
    	if(vPIE != null && vPIE.size() == 0)
    	{
    		if (oPI.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 0"); } //-- should never happen
    		if (vPID.size() < 1) {throw new Exception ("Total Item Purchased < 1"); } //-- should never happen
    	}
    }

    private void resetSession ( RunData data )
    	throws Exception
    {
    	synchronized(this)
    	{
			oPISes.setAttribute (s_PI, oPI);
			oPISes.setAttribute (s_PID, vPID);
			oPISes.setAttribute (s_PIF, vPIF);
			oPISes.setAttribute (s_PIE, vPIE);			
    	}
    }
    
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
            TransactionLoader oLoader = null;
            String sLoader = data.getParameters().getString("loader");
            if (StringUtil.isNotEmpty(sLoader))
            {
                oLoader = (TransactionLoader) Class.forName(sLoader).newInstance();
                oLoader.setUserName(data.getUser().getName());
            }
            else
            {
                oLoader = new PurchaseInvoiceLoader(data.getUser().getName());
            }            
	     	oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());			
	    	context.put ("LoadResult", oLoader.getResult());
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}     
}
package com.ssti.enterprise.pos.presentation.screens.report.receivable;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;

public class CreditMemoReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	try 
    	{
	    	context.put ("vTrans", 
	    		CreditMemoTool.findTrans(sCustomerID, sBankID, sLocationID, sCFTID, dStart, dEnd, iStatus));
	    }   
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		_oEx.printStackTrace();
    	}    
    }    
}

package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class UserPrefMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.UserPrefMapBuilder";

    /**
     * Item
     * @deprecated use UserPrefPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "user_pref";
    }

  
    /**
     * user_pref.USER_PREF_ID
     * @return the column name for the USER_PREF_ID field
     * @deprecated use UserPrefPeer.user_pref.USER_PREF_ID constant
     */
    public static String getUserPref_UserPrefId()
    {
        return "user_pref.USER_PREF_ID";
    }
  
    /**
     * user_pref.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use UserPrefPeer.user_pref.USER_NAME constant
     */
    public static String getUserPref_UserName()
    {
        return "user_pref.USER_NAME";
    }
  
    /**
     * user_pref.PREF_OBJ
     * @return the column name for the PREF_OBJ field
     * @deprecated use UserPrefPeer.user_pref.PREF_OBJ constant
     */
    public static String getUserPref_PrefObj()
    {
        return "user_pref.PREF_OBJ";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("user_pref");
        TableMap tMap = dbMap.getTable("user_pref");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("user_pref.USER_PREF_ID", "");
                          tMap.addColumn("user_pref.USER_NAME", "");
                          tMap.addColumn("user_pref.PREF_OBJ", new Object());
          }
}

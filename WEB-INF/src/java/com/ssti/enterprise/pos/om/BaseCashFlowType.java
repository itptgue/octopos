package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CashFlowType
 */
public abstract class BaseCashFlowType extends BaseObject
{
    /** The Peer class */
    private static final CashFlowTypePeer peer =
        new CashFlowTypePeer();

        
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId;
      
    /** The value for the cashFlowTypeCode field */
    private String cashFlowTypeCode;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the cashFlowTypeGroup field */
    private String cashFlowTypeGroup;
                                          
    /** The value for the defaultType field */
    private int defaultType = 1;
                                          
    /** The value for the cashFlowLevel field */
    private int cashFlowLevel = 1;
                                                
    /** The value for the parentId field */
    private String parentId = "";
                                                                
    /** The value for the hasChild field */
    private boolean hasChild = false;
                                                
    /** The value for the altCode field */
    private String altCode = "";
                                                                
    /** The value for the defaultCm field */
    private boolean defaultCm = false;
                                                                
    /** The value for the defaultDm field */
    private boolean defaultDm = false;
                                                                
    /** The value for the defaultRp field */
    private boolean defaultRp = false;
                                                                
    /** The value for the defaultPp field */
    private boolean defaultPp = false;
                                                                
    /** The value for the defaultCf field */
    private boolean defaultCf = false;
  
    
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeCode
     *
     * @return String
     */
    public String getCashFlowTypeCode()
    {
        return cashFlowTypeCode;
    }

                        
    /**
     * Set the value of CashFlowTypeCode
     *
     * @param v new value
     */
    public void setCashFlowTypeCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeCode, v))
              {
            this.cashFlowTypeCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeGroup
     *
     * @return String
     */
    public String getCashFlowTypeGroup()
    {
        return cashFlowTypeGroup;
    }

                        
    /**
     * Set the value of CashFlowTypeGroup
     *
     * @param v new value
     */
    public void setCashFlowTypeGroup(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeGroup, v))
              {
            this.cashFlowTypeGroup = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultType
     *
     * @return int
     */
    public int getDefaultType()
    {
        return defaultType;
    }

                        
    /**
     * Set the value of DefaultType
     *
     * @param v new value
     */
    public void setDefaultType(int v) 
    {
    
                  if (this.defaultType != v)
              {
            this.defaultType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowLevel
     *
     * @return int
     */
    public int getCashFlowLevel()
    {
        return cashFlowLevel;
    }

                        
    /**
     * Set the value of CashFlowLevel
     *
     * @param v new value
     */
    public void setCashFlowLevel(int v) 
    {
    
                  if (this.cashFlowLevel != v)
              {
            this.cashFlowLevel = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ParentId
     *
     * @return String
     */
    public String getParentId()
    {
        return parentId;
    }

                        
    /**
     * Set the value of ParentId
     *
     * @param v new value
     */
    public void setParentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.parentId, v))
              {
            this.parentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HasChild
     *
     * @return boolean
     */
    public boolean getHasChild()
    {
        return hasChild;
    }

                        
    /**
     * Set the value of HasChild
     *
     * @param v new value
     */
    public void setHasChild(boolean v) 
    {
    
                  if (this.hasChild != v)
              {
            this.hasChild = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AltCode
     *
     * @return String
     */
    public String getAltCode()
    {
        return altCode;
    }

                        
    /**
     * Set the value of AltCode
     *
     * @param v new value
     */
    public void setAltCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.altCode, v))
              {
            this.altCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultCm
     *
     * @return boolean
     */
    public boolean getDefaultCm()
    {
        return defaultCm;
    }

                        
    /**
     * Set the value of DefaultCm
     *
     * @param v new value
     */
    public void setDefaultCm(boolean v) 
    {
    
                  if (this.defaultCm != v)
              {
            this.defaultCm = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultDm
     *
     * @return boolean
     */
    public boolean getDefaultDm()
    {
        return defaultDm;
    }

                        
    /**
     * Set the value of DefaultDm
     *
     * @param v new value
     */
    public void setDefaultDm(boolean v) 
    {
    
                  if (this.defaultDm != v)
              {
            this.defaultDm = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultRp
     *
     * @return boolean
     */
    public boolean getDefaultRp()
    {
        return defaultRp;
    }

                        
    /**
     * Set the value of DefaultRp
     *
     * @param v new value
     */
    public void setDefaultRp(boolean v) 
    {
    
                  if (this.defaultRp != v)
              {
            this.defaultRp = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultPp
     *
     * @return boolean
     */
    public boolean getDefaultPp()
    {
        return defaultPp;
    }

                        
    /**
     * Set the value of DefaultPp
     *
     * @param v new value
     */
    public void setDefaultPp(boolean v) 
    {
    
                  if (this.defaultPp != v)
              {
            this.defaultPp = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultCf
     *
     * @return boolean
     */
    public boolean getDefaultCf()
    {
        return defaultCf;
    }

                        
    /**
     * Set the value of DefaultCf
     *
     * @param v new value
     */
    public void setDefaultCf(boolean v) 
    {
    
                  if (this.defaultCf != v)
              {
            this.defaultCf = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CashFlowTypeId");
              fieldNames.add("CashFlowTypeCode");
              fieldNames.add("Description");
              fieldNames.add("CashFlowTypeGroup");
              fieldNames.add("DefaultType");
              fieldNames.add("CashFlowLevel");
              fieldNames.add("ParentId");
              fieldNames.add("HasChild");
              fieldNames.add("AltCode");
              fieldNames.add("DefaultCm");
              fieldNames.add("DefaultDm");
              fieldNames.add("DefaultRp");
              fieldNames.add("DefaultPp");
              fieldNames.add("DefaultCf");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          if (name.equals("CashFlowTypeCode"))
        {
                return getCashFlowTypeCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("CashFlowTypeGroup"))
        {
                return getCashFlowTypeGroup();
            }
          if (name.equals("DefaultType"))
        {
                return Integer.valueOf(getDefaultType());
            }
          if (name.equals("CashFlowLevel"))
        {
                return Integer.valueOf(getCashFlowLevel());
            }
          if (name.equals("ParentId"))
        {
                return getParentId();
            }
          if (name.equals("HasChild"))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals("AltCode"))
        {
                return getAltCode();
            }
          if (name.equals("DefaultCm"))
        {
                return Boolean.valueOf(getDefaultCm());
            }
          if (name.equals("DefaultDm"))
        {
                return Boolean.valueOf(getDefaultDm());
            }
          if (name.equals("DefaultRp"))
        {
                return Boolean.valueOf(getDefaultRp());
            }
          if (name.equals("DefaultPp"))
        {
                return Boolean.valueOf(getDefaultPp());
            }
          if (name.equals("DefaultCf"))
        {
                return Boolean.valueOf(getDefaultCf());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CashFlowTypePeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          if (name.equals(CashFlowTypePeer.CASH_FLOW_TYPE_CODE))
        {
                return getCashFlowTypeCode();
            }
          if (name.equals(CashFlowTypePeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(CashFlowTypePeer.CASH_FLOW_TYPE_GROUP))
        {
                return getCashFlowTypeGroup();
            }
          if (name.equals(CashFlowTypePeer.DEFAULT_TYPE))
        {
                return Integer.valueOf(getDefaultType());
            }
          if (name.equals(CashFlowTypePeer.CASH_FLOW_LEVEL))
        {
                return Integer.valueOf(getCashFlowLevel());
            }
          if (name.equals(CashFlowTypePeer.PARENT_ID))
        {
                return getParentId();
            }
          if (name.equals(CashFlowTypePeer.HAS_CHILD))
        {
                return Boolean.valueOf(getHasChild());
            }
          if (name.equals(CashFlowTypePeer.ALT_CODE))
        {
                return getAltCode();
            }
          if (name.equals(CashFlowTypePeer.DEFAULT_CM))
        {
                return Boolean.valueOf(getDefaultCm());
            }
          if (name.equals(CashFlowTypePeer.DEFAULT_DM))
        {
                return Boolean.valueOf(getDefaultDm());
            }
          if (name.equals(CashFlowTypePeer.DEFAULT_RP))
        {
                return Boolean.valueOf(getDefaultRp());
            }
          if (name.equals(CashFlowTypePeer.DEFAULT_PP))
        {
                return Boolean.valueOf(getDefaultPp());
            }
          if (name.equals(CashFlowTypePeer.DEFAULT_CF))
        {
                return Boolean.valueOf(getDefaultCf());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCashFlowTypeId();
            }
              if (pos == 1)
        {
                return getCashFlowTypeCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return getCashFlowTypeGroup();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getDefaultType());
            }
              if (pos == 5)
        {
                return Integer.valueOf(getCashFlowLevel());
            }
              if (pos == 6)
        {
                return getParentId();
            }
              if (pos == 7)
        {
                return Boolean.valueOf(getHasChild());
            }
              if (pos == 8)
        {
                return getAltCode();
            }
              if (pos == 9)
        {
                return Boolean.valueOf(getDefaultCm());
            }
              if (pos == 10)
        {
                return Boolean.valueOf(getDefaultDm());
            }
              if (pos == 11)
        {
                return Boolean.valueOf(getDefaultRp());
            }
              if (pos == 12)
        {
                return Boolean.valueOf(getDefaultPp());
            }
              if (pos == 13)
        {
                return Boolean.valueOf(getDefaultCf());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CashFlowTypePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CashFlowTypePeer.doInsert((CashFlowType) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CashFlowTypePeer.doUpdate((CashFlowType) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cashFlowTypeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCashFlowTypeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCashFlowTypeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCashFlowTypeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CashFlowType copy() throws TorqueException
    {
        return copyInto(new CashFlowType());
    }
  
    protected CashFlowType copyInto(CashFlowType copyObj) throws TorqueException
    {
          copyObj.setCashFlowTypeId(cashFlowTypeId);
          copyObj.setCashFlowTypeCode(cashFlowTypeCode);
          copyObj.setDescription(description);
          copyObj.setCashFlowTypeGroup(cashFlowTypeGroup);
          copyObj.setDefaultType(defaultType);
          copyObj.setCashFlowLevel(cashFlowLevel);
          copyObj.setParentId(parentId);
          copyObj.setHasChild(hasChild);
          copyObj.setAltCode(altCode);
          copyObj.setDefaultCm(defaultCm);
          copyObj.setDefaultDm(defaultDm);
          copyObj.setDefaultRp(defaultRp);
          copyObj.setDefaultPp(defaultPp);
          copyObj.setDefaultCf(defaultCf);
  
                    copyObj.setCashFlowTypeId((String)null);
                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CashFlowTypePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CashFlowType\n");
        str.append("------------\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
           .append("CashFlowTypeCode     : ")
           .append(getCashFlowTypeCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("CashFlowTypeGroup    : ")
           .append(getCashFlowTypeGroup())
           .append("\n")
           .append("DefaultType          : ")
           .append(getDefaultType())
           .append("\n")
           .append("CashFlowLevel        : ")
           .append(getCashFlowLevel())
           .append("\n")
           .append("ParentId             : ")
           .append(getParentId())
           .append("\n")
           .append("HasChild             : ")
           .append(getHasChild())
           .append("\n")
           .append("AltCode              : ")
           .append(getAltCode())
           .append("\n")
           .append("DefaultCm            : ")
           .append(getDefaultCm())
           .append("\n")
           .append("DefaultDm            : ")
           .append(getDefaultDm())
           .append("\n")
           .append("DefaultRp            : ")
           .append(getDefaultRp())
           .append("\n")
           .append("DefaultPp            : ")
           .append(getDefaultPp())
           .append("\n")
           .append("DefaultCf            : ")
           .append(getDefaultCf())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Voucher;
import com.ssti.enterprise.pos.om.VoucherPeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentVoucherTool;
import com.ssti.framework.tools.IDGenerator;

public class VoucherSetup extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_SETUP_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", PaymentVoucherTool.getByID(sID));
    		}
			if (iOp == i_SETUP_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				Voucher oData = PaymentVoucherTool.getByID(sID);
				if (oData == null)
				{
					oData = new Voucher();
					oData.setVoucherId(IDGenerator.generateSysID());					
				}				
				data.getParameters().setProperties(oData);			
				oData.save();				
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_SETUP_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				Voucher oData = PaymentVoucherTool.getByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(VoucherPeer.VOUCHER_ID, oData.getVoucherId());
					VoucherPeer.doDelete(oCrit);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}
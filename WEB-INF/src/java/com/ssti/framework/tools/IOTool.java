package com.ssti.framework.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ResourceNotFoundException;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Various IO related utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: IOTool.java,v 1.8 2008/04/11 03:53:21 albert Exp $ <br>
 * 
 * <pre>
 * $Log: IOTool.java,v $
 * 
 * 2016-11-10
 * -add isTemplateExists to check whether vm template exists before parsing
 * -used by GlobalMacros.vm #safeParse($file)
 * -move method copyFile, isFileExists, deleteAllFilesinDir from IOUtil
 * 
 * 2015-09-15
 * - add method zipFile
 *
 * 2015-05-10
 * - add methods showFiles, readFileContent, writeFileContent
 * </pre><br>
 */
public class IOTool 
{
	static final Log log = LogFactory.getLog(IOTool.class);
	
    //extension used by system 
	public static final String s_EXT_XML 	 = ".xml";
	public static final String s_EXT_JRXML 	 = ".jrxml";	
    public static final String s_EXT_JASPER  = ".jasper";
    public static final String s_EXT_JRPRINT = ".jrprint";
    
    public static final String s_EXT_HELPER = ".helper";
    public static final String s_EXT_VM 	= ".vm";
    public static final String s_EXT_JAVA   = ".java";
    public static final String s_EXT_CLASS  = ".class";
    public static final String s_EXT_XLS    = ".xls";
    public static final String s_EXT_PDF    = ".pdf";
    public static final String s_EXT_HTML   = ".html";
    public static final String s_EXT_HTM    = ".htm";

	//to get Real Path System
	public static String getRealPath (RunData data, String _sPathDest) 
	{    		
		ServletContext oServletContext = data.getServletContext();
		String sFilePath = oServletContext.getRealPath(_sPathDest);	
		return sFilePath;
	}
	
	public static String getFileExtension (String _sFile) 
	{    		
		String sExt = "";
		if (_sFile != null)
		{
			int iLastDot = _sFile.lastIndexOf('.');
			if (iLastDot > 0)
			{
				sExt = _sFile.substring(iLastDot);
			}
		}
		return sExt;
	}

	public static String replaceExtension (String _sFile, String _sExt) 
	{    		
		String sResult = "";
		if (_sFile != null)
		{
			int iLastDot = _sFile.lastIndexOf('.');
			if (iLastDot > 0)
			{
				sResult = _sFile.substring(0, iLastDot) + _sExt;
			}
		}
		return sResult;
	}	
	
	/**
	 * make a file executable in unix system
	 * @param _sFile
	 */
	public static void makeExecutable(String _sFile)
	{
		String sOS = System.getProperty("os.name");
        if (System.getProperty("file.separator").equals(":") && (!sOS.startsWith("mac") || sOS.endsWith("x")))
        {
    		String[] aCmd = {"/bin/chmod", "u+x", _sFile};
        	try
			{
        		StringBuilder m_oResult = new StringBuilder();
        		        		
        		ProcessBuilder oPB = new ProcessBuilder(aCmd);
        		
        		Process oProc = oPB.start(); 
        		
        		DataStreamer oErrorStreamer  = new DataStreamer(oProc.getErrorStream(), "ERROR", m_oResult);            
        		DataStreamer oOutStreamer  = new DataStreamer(oProc.getInputStream(), "OUTPUT", m_oResult);            
        		
        		oErrorStreamer.start();
        		oOutStreamer.start();
        		
        		int iExitCode = oProc.waitFor();           
        		
        		if (iExitCode != 0)
        		{
        			log.error("Executing : " + aCmd[0] + " " + aCmd[1] + " " + aCmd[2] + " Failed : " + m_oResult);
        		}
			}
        	catch (Exception _oEx)
			{    			
        		_oEx.printStackTrace();
			}
        }        
	}
	
    private static final String s_UNIX_DEFAULT = "/opt";
    private static final String s_WIN_DEFAULT = "c:";
    private static final String s_WIN_DEFAULT2 = "C:";
	
	public static boolean isUnix()
	{
		String sOS = System.getProperty("os.name");
        if ((!sOS.startsWith("mac") && sOS.endsWith("x")) || sOS.contains("Linux") || sOS.contains("nix"))
        {
        	return true;
        }
        return false;
	}
	
	public static boolean isWindows()
	{
		String sOS = System.getProperty("os.name");
        if (sOS.contains("Windows"))
        {
        	return true;
        }
        return false;
	}
	
	public static boolean isMac()
	{
		String sOS = System.getProperty("os.name");
		sOS = sOS.toLowerCase();
        if ((sOS.startsWith("mac") && sOS.endsWith("x")))
        {
        	return true;
        }
        return false;
	}
		
	public static String checkPath(String _sPath)
	{
    	if ((isUnix() || isMac()) && StringUtil.containsIgnoreCase(_sPath, s_WIN_DEFAULT))
    	{
    		log.warn("Unix System, Windows Path: " + _sPath);
    		_sPath = StringUtil.replace(_sPath, s_WIN_DEFAULT, s_UNIX_DEFAULT);
    		_sPath = StringUtil.replace(_sPath, s_WIN_DEFAULT2, s_UNIX_DEFAULT);
    		log.warn("Path Replaced to: " + _sPath);
    	}
    	if (isWindows() && StringUtil.contains(_sPath, s_UNIX_DEFAULT))
    	{
    		log.warn("Windows System, Unix Path: " + _sPath);
    		_sPath = StringUtil.replace(_sPath, s_UNIX_DEFAULT, s_WIN_DEFAULT);
    		log.warn("Path Replaced to: " + _sPath);
    	}    	
    	return _sPath;
	}
	
	public static File[] showFiles(String _sPath)
	{
		try 
		{
			File f = new File(_sPath);
			if(f != null && f.exists() && f.isDirectory())
			{				
				return f.listFiles();
			}
		} 
		catch (Exception e) 
		{			
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
	public static String readFileContent(String _sPath)
	{
		StringBuilder sb = new StringBuilder();
		try 
		{
			File f = new File(_sPath);			
			if(f != null && f.exists() && !f.isDirectory())
			{				
				BufferedReader br = new BufferedReader(new FileReader(f));				
				String sLine = br.readLine();
				while(sLine != null) 
				{
					sb.append(sLine);
					sb.append(Attributes.s_LINE_SEPARATOR);
					sLine = br.readLine();			
				}
				br.close();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return sb.toString();
	}

	public static String writeFileContent(String _sPath, String _sContent)
	{
		StringBuilder sb = new StringBuilder();
		try 
		{
			File f = new File(_sPath);				
			if(f != null && f.exists() && !f.isDirectory())
			{				
				BufferedWriter bw = new BufferedWriter(new FileWriter(f));
				bw.write(_sContent);
				bw.close();
			}			
		} 
		catch (Exception e) 
		{
			sb.append("ERROR: " + e.getMessage());
			e.printStackTrace();
			log.error(e);			
		}
		return sb.toString();
	}
	
	public static void zipFile(String _sInput, String _sOutput, boolean _bDelAfter)
	{		
		if(StringUtil.isNotEmpty(_sInput) && StringUtil.isNotEmpty(_sOutput))
		{
			byte[] buffer = new byte[1024];    
	    	try
	    	{
	    		String sZip = _sInput.substring(_sInput.lastIndexOf(Attributes.s_FILE_SEPARATOR));
	    		FileOutputStream oFOS = new FileOutputStream(_sOutput);
	    		ZipOutputStream oZOS = new ZipOutputStream(oFOS);
	    		ZipEntry oZE = new ZipEntry(sZip);
	    		oZOS.putNextEntry(oZE);
	    		
	    		FileInputStream in = new FileInputStream(_sInput);
	   	   
	    		int len;
	    		while ((len = in.read(buffer)) > 0) {
	    			oZOS.write(buffer, 0, len);
	    		}
	    		in.close();
	    		oZOS.closeEntry();
	           
	    		//remember close it
	    		oZOS.close();         
	    		
	    		if(_bDelAfter)
	    		{
	    			File oFile = new File(_sInput);
	    			oFile.delete();
	    		}
	    	}
	    	catch(IOException oIOEX)
	    	{
	    	   oIOEX.printStackTrace();
	    	   log.error(oIOEX);
	    	}
		}
	}

	/**
	 * copy file to another
	 * 
	 * @param _sFrom
	 * @param _sTo
	 * @throws IOException
	 */
	public static void copyFile ( String _sFrom, String _sTo ) 
		throws IOException
	{
		FileInputStream fis = new FileInputStream (_sFrom);
		FileOutputStream fos = new FileOutputStream (_sTo);
		
		FileChannel fcin = fis.getChannel();
		FileChannel fcout = fos.getChannel();
		
		fcin.transferTo (0, fcin.size(), fcout);
		
		fcin.close();
		fcout.close();
		
		fis.close();
		fos.close();
	}

	/**
	 * check if file exist
	 * @param _sPath
	 * @return file exist or not
	 */
	public static boolean isFileExist (String _sPath)
	{
        try 
		{
        	FileInputStream oInput = new FileInputStream (_sPath);
        	oInput.close();
        	return true;
		}
        catch (FileNotFoundException _oEx)
		{
        	System.err.println("File  " + _sPath + " Not Found "); 
        	return false;
		}
        catch (IOException _oIOEx)
		{
        	System.err.println("Error Closing File " + _sPath); 
            return false;
		}
	}
	
	/**
	 * delete all files in a directory
	 * 
	 * @param _sDirName
	 * @throws IOException
	 */
	public static void deleteAllFilesInDirectory ( String _sDirName ) 
		throws IOException
	{
		File oDir  = new File (_sDirName);
		if (oDir.isDirectory())
		{
			File[] aFiles = oDir.listFiles();
			
			for (int i = 0; i < aFiles.length; i++)
			{
				File oFile = aFiles[i];
				oFile.delete();
			}
		}
	}
	
	/**
	 * check if velocity template is exists
	 * 
	 * @param _sPath
	 * @return
	 */
	public static boolean isTemplateExists (String _sPath)
	{
        try 
		{
        	VelocityContext oCtx = new VelocityContext();
	    	StringWriter oWriter = new StringWriter();
        	Velocity.mergeTemplate(_sPath, Attributes.s_DEFAULT_ENCODING, oCtx, oWriter);
        	return true;
		}
        catch (ResourceNotFoundException e) 
        {
			System.err.println(_sPath + " Is not exists, Message " + e.getMessage());
			return false;
		}        
        catch (Exception e) {
        	System.err.println(_sPath + " Template ERROR, Message " + e.getMessage());
			e.printStackTrace();
		}
        return false;
	}
	
	/**
	 * set context in SW JS
	 * 
	 * @param _sPath
	 * @return
	 */
	public static void setJSCtx (RunData data, String _sRelPath, String _sFileName, String _sFileToSave)
	{
		String sPath = getRealPath(data, _sRelPath);
		if(!sPath.endsWith("/")) sPath += "/";
		String sCtx = data.getContextPath();
		
		System.out.println("PATH:" + sPath);
		String sNewFile = sPath + _sFileToSave;
		try {
			File oNewFile = new File(sNewFile);
			oNewFile.createNewFile();
			sPath = sPath + _sFileName;
			if(isFileExist(sPath))
			{
				String sSW = readFileContent(sPath);
				System.out.println("CONTENT BEF:" + sSW);
				if(StringUtil.contains(sSW, "{{CTX_PATH}}"))
				{
					sSW = sSW.replace("{{CTX_PATH}}", sCtx);
				}
				System.out.println("CONTENT AFT:" + sSW);
				writeFileContent(sNewFile, sSW);
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}		
	}
	

	
}
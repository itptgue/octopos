package com.ssti.framework.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * a multi threading data streamer, mostly used when calling to native function <br>
 * such as database dump and load
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DataStreamer.java,v 1.2 2007/02/23 14:14:05 albert Exp $ <br>
 * 
 * <pre>
 * $Log: DataStreamer.java,v $
 * Revision 1.2  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class DataStreamer extends Thread
{
    InputStream m_oInput;
    String m_sType;
    OutputStream m_oRedirect;
    StringBuilder m_oResult;
    
    public DataStreamer(InputStream _oInput, String _sType)
    {
        this(_oInput, _sType, null, null);
    }

    public DataStreamer(InputStream _oInput, String _sType, OutputStream _oOut)
    {
        this(_oInput, _sType, _oOut, null);
    }

    public DataStreamer(InputStream _oInput, String _sType, StringBuilder _oSB)
    {
        this(_oInput, _sType, null, _oSB);
    }    
    
    public DataStreamer(InputStream _oInput, String _sType, OutputStream _sRedirectStream, StringBuilder _oSB)
    {
        this.m_oInput    = _oInput;
        this.m_sType     = _sType;
        this.m_oRedirect = _sRedirectStream;
        this.m_oResult	 = _oSB;
    }
    
    
    public void run()
    {
        try
        {
            PrintWriter oPW = null;
			if (m_oRedirect != null)
			{
                oPW = new PrintWriter(m_oRedirect);
			}   
            InputStreamReader oReader = new InputStreamReader(m_oInput);
            BufferedReader oBuffer = new BufferedReader(oReader);
            String sLine = null;
            while ( (sLine = oBuffer.readLine()) != null)
            {
                if (oPW != null) oPW .println(sLine); 
                if (m_oResult != null) m_oResult.append(sLine);
            }
            if (oPW != null)
            {
				oPW.flush();
        	}
        } 
        catch (IOException _IOException)
        {
        	_IOException.printStackTrace();  
        }
    }
}
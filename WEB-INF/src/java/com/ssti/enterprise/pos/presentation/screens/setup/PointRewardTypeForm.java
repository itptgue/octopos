package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CommonManager;
import com.ssti.enterprise.pos.om.PointType;
import com.ssti.enterprise.pos.om.PointTypePeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PointRewardTool;
import com.ssti.framework.tools.IDGenerator;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2012 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2015-09-07
 * - Initial Version
 * 
 * </pre><br>
 */
public class PointRewardTypeForm extends Default
{	
	public static final int i_OP_VIEW = 1;
	public static final int i_OP_SAVE = 2;
	public static final int i_OP_DEL = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", PointRewardTool.getPointType(sID));
    		}
			if (iOp == i_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				PointType oData = PointRewardTool.getPointType(sID);
				if (oData == null)
				{
					oData = new PointType();
					oData.setPointTypeId(IDGenerator.generateSysID());					
				}
				data.getParameters().setProperties(oData);
				oData.setForSi(data.getParameters().getBoolean("ForSi",false));
				oData.setForSr(data.getParameters().getBoolean("ForSr",false));
				
				oData.save();				
				CommonManager.getInstance().refreshPTCache();
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				PointType oData = PointRewardTool.getPointType(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(PointTypePeer.POINT_TYPE_ID, oData.getPointTypeId());
					PointTypePeer.doDelete(oCrit);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
					CommonManager.getInstance().refreshPTCache();
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}
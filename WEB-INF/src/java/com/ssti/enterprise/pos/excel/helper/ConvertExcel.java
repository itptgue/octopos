package com.ssti.enterprise.pos.excel.helper;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.turbine.util.TurbineConfig;

import com.ssti.enterprise.pos.model.ItemList;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ConvertExcel.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ConvertExcel.java,v 1.8 2008/06/29 07:02:30 albert Exp $
 *
 * $Log: ConvertExcel.java,v $
 * Revision 1.8  2008/06/29 07:02:30  albert
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/26 05:11:26  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/06/30 13:31:56  albert
 * *** empty log message ***
 *
 * Revision 1.5  2005/10/10 03:34:53  albert
 * *** empty log message ***
 *
 * Revision 1.4  2005/10/04 11:48:49  albert
 * *** empty log message ***
 *
 * Revision 1.3  2005/07/14 04:03:56  albert
 * *** empty log message ***
 *
 * Revision 1.2  2005/07/07 13:06:11  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/04/27 04:06:01  Albert
 * *** empty log message ***
 *
 */

public class ConvertExcel 
	extends BaseExcelHelper 
	implements ExcelHelper
{
	private static Log log = LogFactory.getLog ( ConvertExcel.class );
	
	//CONF
	int excelType = 1;
	
	String destFile = "";
	StringBuilder error;  
	StringBuilder success;  
	StringBuilder result;  
	
	int totalError = 0;
	int totalSuccess = 0;
	
	//SOURCE
	private HSSFRow headerRow = null;
	private int totalRows = 0;
	private int startRow = 0;	
	private int currentRow = 0;	
	private boolean startProcess = false;
	
	//RESULT
	private HSSFWorkbook resultWB = null;
	private HSSFSheet resultSheet = null;
	private int resultRow = 0;
	
	//List of ItemList
	private List itemList;
	
    int iInventoryScale = PreferenceTool.getInvenCommaScale();
    int iCostScale = PreferenceTool.getInvenCommaScale();
	
	public static void main(String[] args) 
	{
		TurbineConfig config = null;
		config = new TurbineConfig("E:/Project/POS", "WEB-INF/conf/TurbineResources.properties");
		config.initialize();
		ConvertExcel oLoader = new ConvertExcel (Integer.parseInt(args[0]),args[1]);
		try 
		{
			oLoader.loadData(new FileInputStream(args[2]));
			oLoader.saveWB();
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println ("File Not Found");
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			System.err.println ("ERROR : ");
			e.printStackTrace();
		}
	}

	public ConvertExcel(int _iExcelType, String _sDestFile)
	{
		excelType = _iExcelType;
		destFile = _sDestFile;
		
		error = new StringBuilder();
		success = new StringBuilder();
		result = new StringBuilder();
		
		prepareResultWB();
	}
	
	private void prepareResultWB()
	{
		resultWB = new HSSFWorkbook();	
        resultSheet = resultWB.createSheet("Stock Opname Worksheet");
        HSSFRow oRow = resultSheet.createRow(resultRow);

        int iCol = 0;

		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Item Code");   iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Item Name");   iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Full Name");   iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Qty"); 		iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"New Qty"); 	iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"+/-"); 		iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Unit"); 	    iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Description"); iCol++;
		createHeaderCell(resultWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Cost"); 	    iCol++;
		
		resultRow++;
	}
	
	/**
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	
		totalRows = oSheet.getPhysicalNumberOfRows();
		itemList = new ArrayList(totalRows);
		
		result.append("PROCESSED " + totalRows + " ROWS IN SOURCE FILE ");
		
		for (int iRow = 0; iRow < totalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		
    		if (oRow != null)
    		{
    			short iFirstCol = oRow.getFirstCellNum();
				HSSFCell oCell = oRow.getCell(iFirstCol);
				if (!startProcess) 
				{
					if (oCell.getStringCellValue() != null && oCell.getStringCellValue().equals("SKU")) 
					{ 
						result.append("\nPROCESS START FROM ROW (HEADER) : " + iRow);
						startProcess = true; 
						startRow = iRow; 
						headerRow = oRow;
					}
				}   
				else 
				{
					currentRow = iRow;
					log.debug ("PROCESSING ROW " + iRow);
					if (!destFile.equals(""))
					{
						createRows (oRow, iFirstCol);
					}
					else
					{
						createItemList (oRow, iFirstCol);
					}
				}
			}
    	} 		
		result.append("\nPROCESS RESULT : " + totalSuccess + " SUCCESSFUL ROW ");
		result.append(success);
		
		result.append("\n\nERROR RESULT : "  + totalError + " ROW " );		
		result.append(error);
    }
    
    /**
     * 
     * @param _oRow
     * @param _iIdx
     * @throws Exception
     */
    private void createRows (HSSFRow _oRow, short _iIdx)	
		throws Exception
	{		
		int iLastCol = _oRow.getLastCellNum();

		String sSKU = getString(_oRow, _iIdx);

		if (!sSKU.equals("SKU") && StringUtil.isNotEmpty(sSKU))				
		{
			sSKU = sSKU.trim();
			
			for (int i = _iIdx + 1; i < iLastCol; i++)
			{				
				String sCode = getString(headerRow, _iIdx + i);
				if (StringUtil.isNotEmpty(sCode))
				{
					sCode = sCode.trim();
				}
				sCode = sSKU + sCode; 
				
				log.debug("SKU : " + sSKU + " CODE : " + sCode);
				
				BigDecimal dNewQty = getBigDecimal(_oRow, i);				
				Item oItem = ItemTool.getItemByCode(sCode);
				
				if (oItem != null)
				{
                    InventoryLocation oInvLoc = 
                    	InventoryLocationTool.getDataByItemAndLocationID(oItem.getItemId(),
                    			PreferenceTool.getLocationID());
                    
                    double dQoH = 0;
                    double dCost = 0;

                    if(oInvLoc != null)
                    {
                       dQoH = oInvLoc.getCurrentQty().setScale(iInventoryScale,4).doubleValue();
                       dCost = oInvLoc.getItemCost().setScale(iCostScale,4).doubleValue();
                    }
					
                    if (dCost == 0) dCost = oItem.getLastPurchasePrice().doubleValue();
                    
					HSSFRow oRow2 = resultSheet.createRow(resultRow); 
	                
	                int iCol = 0;
	                
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, oItem.getItemCode   ());                              iCol++; 
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, oItem.getItemName   ());                              iCol++; 
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, oItem.getDescription());                              iCol++; 
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, new BigDecimal(dQoH).setScale(iInventoryScale,4).toString()); iCol++; 
	                 
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, dNewQty.setScale(iInventoryScale,4).toString()); iCol++; 
	                 
	                 
	                double dChangeQty = dNewQty.setScale(iInventoryScale,4).doubleValue() - dQoH;
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, new BigDecimal(dChangeQty).setScale(iInventoryScale,4).toString()); iCol++; 
	                 
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, UnitTool.getAlternateUnitByID(oItem.getUnitId()).getUnitCode()); iCol++; 
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, "");                                                  			iCol++; 
	                createCell(resultWB, oRow2, (short)iCol, HSSFCellStyle.ALIGN_LEFT, new BigDecimal(dCost).setScale(iCostScale,4).toString()); 		iCol++; 

	                if (log.isDebugEnabled())
	                {
	                	success
						.append("\nSOURCE ROW : ")
						.append(StringUtil.formatNumberString(Integer.valueOf(currentRow).toString(), 3)) 
						.append(" COL : ")
						.append(StringUtil.formatNumberString(Integer.valueOf(i).toString(), 3))
						.append(" SKU : ")
						.append(StringUtil.left(sSKU, 20))
						.append(" CODE : ")
						.append(StringUtil.left(sCode, 20)) 
						.append(" ADDED TO DEST. ROW : ")
						.append(resultRow); 
	                }
	                totalSuccess++;
	                resultRow++;
				}	
				else
				{
					error
					.append("\nERROR AT SOURCE ROW : ") 
					.append(StringUtil.formatNumberString(Integer.valueOf(currentRow).toString(), 3))
					.append(" COL : ")
					.append(StringUtil.formatNumberString(Integer.valueOf(i).toString(), 3)) 
					.append(" SKU : ")  
					.append(StringUtil.left(sSKU, 20)) 
					.append(" ITEM WITH CODE : ")  
					.append(StringUtil.left(sCode, 20))
					.append(" IS NOT FOUND !"); 
	                
					totalError++;
				}
			}			
		}
		else
		{
			error.append("\nERROR AT SOURCE ROW : ")
				 .append(StringUtil.formatNumberString(Integer.valueOf(currentRow).toString(), 3))
				 .append(" UNPROCESSED BECAUSE OF EMPTY/INVALID SKU : ").append(sSKU);
		}
	}

    public void createItemList (HSSFRow _oRow, short _iIdx)	
		throws Exception
	{
		log.debug("iIdx " + _iIdx);
		
		int iLastCol = _oRow.getLastCellNum();
		
		log.debug("iLastCol " + iLastCol);
		
		if (!getString(_oRow, _iIdx).equals("SKU"))				
		{
			for (int i = _iIdx + 1; i < iLastCol; i++)
			{
				String sSKU = getString(_oRow, _iIdx);
				
				log.debug("COL i " + i);
				
				log.debug("sSKU " + sSKU);
				
				String sCode = sSKU + getString(headerRow, _iIdx + i);

				log.debug("sCode " + sCode);

				BigDecimal dNewQty = getBigDecimal(_oRow, _iIdx + i);

				log.debug("dQty " + dNewQty);
				
				Item oItem = ItemTool.getItemByCode(sCode);

				//log.debug("oItem " + oItem);

				if (oItem != null)
				{
				
					ItemList oItemList = new ItemList();
					oItemList.setItemId(oItem.getItemId());
					oItemList.setItemCode(oItem.getItemCode());
					oItemList.setItemName(oItem.getItemName());
					oItemList.setDescription(oItem.getDescription());
					oItemList.setQty(dNewQty);
					
					log.debug(oItemList);
					
					itemList.add (oItemList);
				}
			}			
		}
	}

    public HSSFWorkbook getWorkbook(Map _mParams, HttpSession _oSession) throws Exception 
	{	
    	return resultWB;
    }
    
    public List getItemList ()
		throws Exception
	{
    	return itemList;
	}
    
    public void saveWB ()
    	throws Exception
    {
    	resultWB.write(new FileOutputStream(destFile));
    }
    
	public StringBuilder getResult() {
		return result;
	}	
	
	public void writeResult()
		throws Exception
	{
		if (StringUtil.isNotEmpty(destFile))
		{
			String sLog = destFile + "_import_log.txt";
			Writer writer = new BufferedWriter(new FileWriter(sLog));
			writer.write(result.toString());
			writer.close();
			result.append("\n\nRESULT WRITTEN TO : " + log);
		}
	}
}

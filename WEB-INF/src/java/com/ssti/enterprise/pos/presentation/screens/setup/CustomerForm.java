package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CustomerContact;
import com.ssti.enterprise.pos.om.CustomerContactPeer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.StringUtil;

public class CustomerForm extends CustomerSecureScreen
{
	public static final int i_SAVE_CONTACT = 1;
	public static final int i_DEL_CONTACT  = 2;
	public static final int i_VIEW_CONTACT = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_SAVE_CONTACT)
			{
				saveContact(data,context);
			}
			else if (iOp == i_DEL_CONTACT)
			{
				delContact(data,context);
			}
			else if (iOp == i_VIEW_CONTACT)
			{
				viewContact(data,context);
			}
			
			if (sMode.equals("delete") || (iOp == 4 || sMode.equals("update")) || sMode.equals("view") )
			{
				sID = data.getParameters().getString("id");
				context.put("Customer", CustomerTool.getCustomerByID(sID));
    		}
			PreferenceTool.setLocationInContext(data, context);
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx);
    	}
    }

	protected void saveContact(RunData data, Context context) 
	{
		try 
		{
			String sCTID = data.getParameters().getString("CustomerContactId");
			String sID = data.getParameters().getString("ID");
			CustomerContact oCT = CustomerTool.getContactByID(sCTID);
			System.out.println(oCT);
			boolean bNew = false;
			if (oCT == null)
			{
				oCT = new CustomerContact();
				bNew = true;
			}
			System.out.println("new " + bNew);
			data.getParameters().setProperties(oCT);
			if (bNew) 
			{
				oCT.setCustomerContactId(IDGenerator.generateSysID());
				oCT.setCustomerId(sID);
			}
			oCT.save();			
			data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_SAVE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
	protected void delContact(RunData data, Context context) 
	{
		try 
		{
			String sCTID = data.getParameters().getString("CustomerContactId");
			if (StringUtil.isNotEmpty(sCTID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerContactPeer.CUSTOMER_CONTACT_ID, sCTID);
				CustomerContactPeer.doDelete(oCrit);
				data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
			}
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
	protected void viewContact(RunData data, Context context) 
	{
		try 
		{
			String sCTID = data.getParameters().getString("CustomerContactId");
			if (StringUtil.isNotEmpty(sCTID))
			{
				context.put("Contact", CustomerTool.getContactByID(sCTID));
			}
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
}

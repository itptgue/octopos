package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.data.TransEditTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 * 
 * 
 * @author  $Author: albert $
 * @version $Id: TransactionEditor,v 1.9 2009/05/04 02:02:36 albert Exp $
 *
 * $Log: TransactionEditor,v $
 *
 */
public class TransactionEditorView extends TransactionSecureScreen
{ 	    
    protected boolean isAuthorized(RunData data) throws Exception
    {
        if (data.getACL() != null && data.getACL().getRoles() != null)
        {
            return data.getACL().getRoles().containsName("Administrator");
        }
        return false;
    }
    
	public void doBuildTemplate(RunData data, Context context)
	{        
	    super.doBuildTemplate(data, context);
        context.put("tedit", TransEditTool.getInstance());
    }
}

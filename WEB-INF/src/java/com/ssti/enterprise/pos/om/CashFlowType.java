
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Fri May 04 09:49:49 ICT 2007]
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class CashFlowType
    extends com.ssti.enterprise.pos.om.BaseCashFlowType
    implements Persistent, MasterOM
{

    public String getId()
    {
        return getCashFlowTypeId();
    }

    public String getCode()
    {
        return getCashFlowTypeCode();
    }
}

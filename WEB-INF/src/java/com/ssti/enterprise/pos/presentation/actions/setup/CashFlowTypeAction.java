package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.AccountCFTypeLoader;
import com.ssti.enterprise.pos.excel.helper.CashFlowTypeLoader;
import com.ssti.enterprise.pos.manager.CashFlowTypeManager;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.CashFlowTypePeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class CashFlowTypeAction extends BankSetupAction
{
	private static final Log log = LogFactory.getLog(CashFlowTypeAction.class);

    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			CashFlowType oCFT = new CashFlowType();
			setProperties (oCFT, data);
			oCFT.setCashFlowTypeId (IDGenerator.generateSysID());
	        oCFT.save();
	        CashFlowTypeManager.getInstance().refreshCache(oCFT);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			CashFlowType oCFT = CashFlowTypeTool.getTypeByID(data.getParameters().getString("ID"), null);
			CashFlowType oOld = oCFT.copy();			
			setProperties (oCFT, data);
	        oCFT.save();
	        CashFlowTypeManager.getInstance().refreshCache(oCFT);
	        UpdateHistoryTool.createHistory(oOld, oCFT, data.getUser().getName(),null);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("cash_flow_journal","cash_flow_type_id",sID))
    		{
    			CashFlowType oCFT = CashFlowTypeTool.getTypeByID(sID, null);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(CashFlowTypePeer.CASH_FLOW_TYPE_ID, data.getParameters().getString("ID"));
	        	CashFlowTypePeer.doDelete(oCrit);
	        	CashFlowTypeManager.getInstance().refreshCache(sID);
	        	UpdateHistoryTool.createDelHistory(oCFT, data.getUser().getName(),null);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        		
    		}
        	else
        	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        	}
        }
        catch (Exception _oEx)
        {
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
        }
    }

    private void setProperties (CashFlowType _oCashFlowType, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oCashFlowType);  
	    	_oCashFlowType.setDefaultRp(data.getParameters().getBoolean("DefaultRp"));
	    	_oCashFlowType.setDefaultPp(data.getParameters().getBoolean("DefaultPp"));
	    	_oCashFlowType.setDefaultCm(data.getParameters().getBoolean("DefaultCm"));
	    	_oCashFlowType.setDefaultDm(data.getParameters().getBoolean("DefaultDm"));
	    	_oCashFlowType.setDefaultCf(data.getParameters().getBoolean("DefaultCf"));	    	
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
    
    public void doLoadfile (RunData data, Context context)
		throws Exception
	{
		try 
		{
            String sLoader = data.getParameters().getString("loader");
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			if (StringUtil.isEmpty(sLoader))
            {
			    CashFlowTypeLoader oLoader = new CashFlowTypeLoader(data.getUser().getName());
			    oLoader.loadData (oBufferStream);
                data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));            
                context.put ("LoadResult", oLoader.getResult());
            }
            else if (StringUtil.isEqual(sLoader, "com.ssti.enterprise.pos.excel.helper.AccountCFTLoader"))
            {
                AccountCFTypeLoader oLoader = new AccountCFTypeLoader(data.getUser().getName());
                oLoader.loadData (oBufferStream);
                data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));            
                context.put ("LoadResult", oLoader.getResult());                
            }
		}
		catch (Exception _oEx) 
		{
			log.error(_oEx);
			_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage());
		}
	}
}

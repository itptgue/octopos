package com.ssti.enterprise.pos.presentation.screens.setup;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.mail.Message.RecipientType;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.MailTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class CustomerEmail extends CustomerSecureScreen
{
	private static final int i_SEND_EMAIL = 1;
	private static final int i_SAVE_EMAIL = 2;

    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_SEND_EMAIL)
			{
				sendEmail(data,context);
			}
			else if (iOp == i_SAVE_EMAIL)
			{
				saveEmail(data,context);
			}
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage(_oEx.getMessage());
    	}
    }

    private void saveEmail(RunData data, Context context)
	{
		String sCustID = data.getParameters().getString("CustomerId");
		String sEmail = data.getParameters().getString("Email");
		if (StringUtil.isNotEmpty(sCustID) && StringUtil.isNotEmpty(sEmail))
		{
			List vCust = (List)data.getSession().getAttribute("Customers");
	    	if (vCust != null)
	    	{
	    		for (int i = 0; i < vCust.size(); i++)
		    	{
	    			Customer oCust = (Customer) vCust.get(i);
	    			if (StringUtil.isEqual(oCust.getCustomerId(), sCustID))
	    			{
	    				try
						{
		    				oCust.setEmail(sEmail);
		    				oCust.save();			
		    				data.setMessage("Email Saved Successfully");
						}
						catch (Exception e)
						{
							data.setMessage("ERROR: " + e.getMessage());
							log.error(e);
						}
	    			}
		    	}
	    	}
		}
	}

	public void sendEmail(RunData data,Context context)
		throws Exception
	{
    	String sSub = data.getParameters().getString("Subject");
    	String sBody = data.getParameters().getString("Body");
    	List vCust = (List)data.getSession().getAttribute("Customers");
    	if (vCust != null)
    	{
    		String sAtt = processUpload(data);
	    	List vEmail = new ArrayList(vCust.size());
	    	for (int i = 0; i < vCust.size(); i++)
	    	{
	    		Customer oCust = (Customer) vCust.get(i);
	    		if (StringUtil.isNotEmpty(oCust.getEmail()) && StringUtil.contains(oCust.getEmail(),"@"))
	    		{
	    			if (data.getParameters().getBoolean("cb"+ oCust.getId()))
	    			{
	    				vEmail.add(oCust.getEmail());
	    			}
	    		}
	    	}
		
	    	StringBuilder msg = new StringBuilder();
	    	msg.append("Email Succesfully Sent to: <br>");
	    	
	    	String[] aTo = new String[vEmail.size()];
	    	for (int i = 0; i < vEmail.size(); i++)
	    	{
	    		String sEmail = (String) vEmail.get(i);
	    		aTo[i] = sEmail;
	    		msg.append(sEmail).append("<br>");
	    	}
	    	
	    	MailTool.sendEmail("", "", aTo, sSub, sBody, sAtt, null, RecipientType.BCC, "");	 
	    		    	
	    	data.setMessage(msg.toString());
	    }
	}

	private String processUpload(RunData data) throws Exception
	{
		try
		{
			//get file item
			FileItem oFileItem = data.getParameters().getFileItem("Attachment");
			if (oFileItem != null)
			{
				String sFileName   = oFileItem.getName();	
				if (StringUtil.isNotEmpty(sFileName)) 
				{
					String sSeparator  = System.getProperty("file.separator");
					String sPathDest   = PreferenceTool.getPicturePath();
					String sFilePath   = IOTool.getRealPath(data, sPathDest);										
					int iSeparatorIndex = sFileName.lastIndexOf (sSeparator.charAt(0));
					if (iSeparatorIndex > 0)
					{
						sFileName = sFileName.substring (iSeparatorIndex);
					}
					sFilePath = sFilePath + sSeparator + sFileName;

					System.out.println("File Path:" + sFilePath);

					oFileItem.write (new File (sFilePath)); //save picture file
					StringBuilder oSB = new StringBuilder();
					oSB.append(data.getContextPath());
					oSB.append(sPathDest);
					oSB.append(sFileName);        	
					String sFilePathToSafe = (oSB.toString()).replace('\\','/');

					System.out.println("File Path:" + sFilePathToSafe);
					
					return sFilePath;
				}
			}
		}
		catch (Exception _oEx)
		{
			throw new Exception (LocaleTool.getString(s_UPLOAD_PIC_FAILED) +  _oEx.getMessage());
		}
		return "";
	}
}

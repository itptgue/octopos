package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.report.InventoryReportTool;
import com.ssti.framework.turbine.LargeSelectHandler;


public class StockCardSite extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findItemResult", "Items");    		
			context.put("inventoryreport", InventoryReportTool.getInstance());
			context.put("vSelected", new ArrayList());
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}
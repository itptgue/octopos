package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.CustomFieldManager;
import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Brand;
import com.ssti.enterprise.pos.om.BrandPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemField;
import com.ssti.enterprise.pos.om.ItemFieldPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.Principal;
import com.ssti.enterprise.pos.om.PrincipalPeer;
import com.ssti.enterprise.pos.om.Tag;
import com.ssti.enterprise.pos.om.TagPeer;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemFieldTool.java,v 1.11 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemFieldTool.java,v $
 * 
 * 2018-08-08
 * - Add getManufacturer helper to return PrincipalName by PrincipalID OR Item.Manufacturer
 * - Add getBrand helper to return BrandName By BrandID OR Item.Brand
 * 
 * 2018-04-07
 * - Add Principal related query
 * 
 * 2016-10-25
 * -add method getItemFields see {@ItemManager}
 * 
 * 2015-10-20
 * -add method getNumber to get number from field value
 * 
 * 2016-07-21
 * - Add brand and tag related query
 * 
 * 2015-07-31
 * -add getForUpdate get ItemField not from cache, because we keep empty object in cache
 * -change saveField to get query not from cache but from DB.
 * -change saveField to enable emptying value of field name
 * </pre><br>
 */
public class ItemFieldTool extends BaseTool
{
	public static List getAllItemField()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return ItemFieldPeer.doSelect(oCrit);
	}
	
	public static List getAllCustomField()
		throws Exception
	{
		return CustomFieldManager.getInstance().getAllCustomField();
	}
	
	public static void deleteByItemID(String _sItemID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemFieldPeer.ITEM_ID, _sItemID);
		ItemFieldPeer.doDelete(oCrit);
	}

	public static ItemField getItemFieldByID(String _sItemFieldID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemFieldPeer.ITEM_FIELD_ID, _sItemFieldID);
		List vData = ItemFieldPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (ItemField) vData.get(0);
		}
		return null;
	}

	private static ItemField getForUpdate(String _sItemID, String _sField, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemFieldPeer.ITEM_ID, _sItemID);
		oCrit.add(ItemFieldPeer.FIELD_NAME, _sField);
		List vData = ItemFieldPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			ItemField oField = (ItemField) vData.get(0);
			return oField;
		}
		return null;
	}

	public static List getItemFields(String _sItemID) 
		throws Exception
	{
		return ItemManager.getInstance().getItemFields(_sItemID,  null);
	}

	public static ItemField getItemField(String _sItemID, String _sField) 
		throws Exception
	{
		return ItemManager.getInstance().getItemField(_sItemID, _sField, null);
	}

	public static ItemField getItemField(String _sItemID, String _sField, Connection _oConn) 
		throws Exception
	{
		return ItemManager.getInstance().getItemField(_sItemID, _sField, _oConn);
	}	
	
	public static String getValue(String _sItemID, String _sField) 
		throws Exception
	{
		ItemField oField = getItemField (_sItemID, _sField);
		if (oField != null)
		{
			return oField.getFieldValue();			
		}
		return "";	
	}

	public static Number getNumber(String _sItemID, String _sField) 
			throws Exception
	{
		ItemField oField = getItemField (_sItemID, _sField);
		if (oField != null && StringUtil.isNotEmpty(oField.getFieldValue()))
		{
			try 
			{
				Number n = CustomParser.parseNumber(oField.getFieldValue());			
				if(n != null)
				{
					return n.doubleValue();
				}				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error("ERROR: " + e);
			}
		}
		return 0;	
	}

	/**
	 * 
	 * @param _sItemID
	 * @param _sField
	 * @param _sValue
	 * @throws Exception
	 */
	public static void saveField(String _sItemID, String _sField, String _sValue) 
		throws Exception
	{
//		if (StringUtil.isNotEmpty(_sValue))
//		{
			ItemField oField = getForUpdate (_sItemID, _sField, null);
			if (oField == null) //if new itemField and have value
			{
				if(StringUtil.isNotEmpty(_sValue))
				{
					oField = new ItemField ();
					oField.setItemId(_sItemID);
					oField.setFieldName(_sField);
					oField.setFieldValue(_sValue);
					oField.setItemFieldId(IDGenerator.generateSysID());
				}
			}
			else
			{
				oField.setFieldValue(_sValue);
			}
			if(oField != null)
			{
				oField.setUpdateDate(new Date());
				oField.save();
				ItemManager.getInstance().refreshFieldCache(_sItemID, _sField, oField);
			}
//		}
	}

	public static List getUpdatedField (boolean _bActiveOnly)
		throws Exception
	{
		return getUpdatedField(_bActiveOnly, null);
	}		
	
	public static List getUpdatedField (boolean _bActiveOnly, Date _dLast)
		throws Exception
	{
		if (_dLast == null)
		{
			_dLast = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE);
		}
		Criteria oCrit = new Criteria();
		if (_dLast  != null) 
		{
	    	oCrit.add(ItemFieldPeer.UPDATE_DATE, _dLast, Criteria.GREATER_EQUAL);   
	    	if ( _bActiveOnly ) 
	    	{
	    		oCrit.addJoin(ItemPeer.ITEM_ID, ItemFieldPeer.ITEM_ID);
	    		oCrit.add(ItemPeer.ITEM_STATUS, b_ACTIVE_ITEM);
			}
		}
		return ItemFieldPeer.doSelect(oCrit);
	}		
	
	public static List getNewFields(List _vItems)
		throws Exception
	{
		if (_vItems != null && _vItems.size() > 0)
		{
			List vIDs = new ArrayList(_vItems.size());
			for (int i = 0; i < _vItems.size(); i++)
			{
				Item oItem = (Item) _vItems.get(i);
				vIDs.add(oItem.getItemId());
			}
			if (vIDs.size() > 0) 
			{
				Criteria oCrit = new Criteria();
				oCrit.addIn(ItemFieldPeer.ITEM_ID, vIDs);
				return ItemFieldPeer.doSelect(oCrit);
			}
		}		
		return new ArrayList(1);		
	}	
			
	//-------------------------------------------------------------------------
	// Brand
	//-------------------------------------------------------------------------
	
	public static List getAllBrand()
    	throws Exception
    {
		return ItemManager.getInstance().getAllBrand();
    }
	
	public static Brand getBrandByID(String _sBrandID)
    	throws Exception
    {
		return ItemManager.getInstance().getBrandByID(_sBrandID, null);
    }

	public static Brand getBrandByCode(String _sBrandCode)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BrandPeer.BRAND_CODE, _sBrandCode);
		List v = BrandPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			Brand oBrand = (Brand) v.get(0);
			return oBrand;
		}
		return null;
    }

	public static Brand getBrandByName(String _sBrandName)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(BrandPeer.BRAND_NAME, (Object)StringUtil.trim(_sBrandName), Criteria.ILIKE);
		List v = BrandPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			Brand oBrand = (Brand) v.get(0);
			return oBrand;
		}
		return null;
    }
	
	public static String getBrandIDByCode(String _sBrandCode)
		throws Exception
	{
		Brand oBrand = getBrandByCode(_sBrandCode);
		if(oBrand != null)
		{
			return oBrand.getId();
		}
		return "";
	}
	
	public static String getBrandNameByID(String _sBrandID)
		throws Exception
	{
		Brand oBrand = getBrandByID(_sBrandID);
		if(oBrand != null)
		{
			return oBrand.getName();
		}
		return "";
	}
	
	public static List getBrandByPrincID(String _sPrincID)
    	throws Exception
    {
		return ItemManager.getInstance().getBrandByPrincID(_sPrincID);
    }
	
	public static List getGenericBrand()
	    	throws Exception
	    {
			return ItemManager.getInstance().getGenericBrand();
	    }
	
	public static int getTotalItemsByBrand(String _sBrandID, String _sKategoriID, boolean _bWebStore)
		throws Exception
	{
		StringBuilder sql = new StringBuilder("SELECT count(item_id) AS total FROM item WHERE item_status = true ");		
		if (StringUtil.isNotEmpty(_sBrandID))
		{
			sql.append (" AND brand = '").append(getBrandNameByID(_sBrandID)).append("' ");
		}
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			List vKat = KategoriTool.getChildIDList(_sKategoriID);
			vKat.add(_sKategoriID);
			sql.append (" AND kategori_id IN ").append(SqlUtil.convertToINMode(vKat));
		}
		if(_bWebStore) sql.append(" AND display_store = true ");
		List v = SqlUtil.executeQuery(sql.toString());
		if(v.size() > 0)
		{
			Record r = (Record) v.get(0);
			return r.getValue("total").asInt();
		}
		return 0;
	}
	
	public static String getBrand(String _sItemID)
		throws Exception
	{
		return getBrand(ItemTool.getItemByID(_sItemID));
	}
	
	public static String getBrand(Item _oItem)
		throws Exception
	{
		if(_oItem != null)
		{
			if(getAllBrand().size() > 0)
			{
				return getBrandNameByID(_oItem.getBrand());
			}
			return _oItem.getBrand();
		}
		return "";
	}
	
	//-------------------------------------------------------------------------
	// Principal
	//-------------------------------------------------------------------------
	
	public static List getAllPrincipal()
    	throws Exception
    {
		return ItemManager.getInstance().getAllPrincipal();
    }
	
	public static Principal getPrincipalByID(String _sPrincipalID)
    	throws Exception
    {
		return ItemManager.getInstance().getPrincipalByID(_sPrincipalID, null);
    }

	public static Principal getPrincipalByCode(String _sPrincipalCode)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(PrincipalPeer.PRINCIPAL_CODE, _sPrincipalCode);
		List v = PrincipalPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			Principal oPrincipal = (Principal) v.get(0);
			return oPrincipal;
		}
		return null;
    }

	public static Principal getPrincipalByName(String _sPrincipalName)
    	throws Exception
    {		
		Criteria oCrit = new Criteria();
		oCrit.add(PrincipalPeer.PRINCIPAL_NAME, (Object)StringUtil.trim(_sPrincipalName), Criteria.ILIKE);
		List v = PrincipalPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			Principal oPrincipal = (Principal) v.get(0);
			return oPrincipal;
		}
		return null;
    }
	
	public static String getPrincipalIDByCode(String _sPrincipalCode)
		throws Exception
	{
		Principal oPrincipal = getPrincipalByCode(_sPrincipalCode);
		if(oPrincipal != null)
		{
			return oPrincipal.getId();
		}
		return "";
	}
	
	public static String getPrincipalNameByID(String _sPrincipalID)
		throws Exception
	{
		Principal oPrincipal = getPrincipalByID(_sPrincipalID);
		if(oPrincipal != null)
		{
			return oPrincipal.getName();
		}
		return "";
	}
	
	public static int getTotalItemsByPrincipal(String _sPrincipalID, String _sKategoriID, boolean _bWebStore)
		throws Exception
	{
		StringBuilder sql = new StringBuilder("SELECT count(item_id) AS total FROM item WHERE item_status = true ");		
		if (StringUtil.isNotEmpty(_sPrincipalID))
		{
			sql.append (" AND manufacturer = '").append(getPrincipalNameByID(_sPrincipalID)).append("' ");
		}
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			List vKat = KategoriTool.getChildIDList(_sKategoriID);
			vKat.add(_sKategoriID);
			sql.append (" AND kategori_id IN ").append(SqlUtil.convertToINMode(vKat));
		}
		if(_bWebStore) sql.append(" AND display_store = true ");
		List v = SqlUtil.executeQuery(sql.toString());
		if(v.size() > 0)
		{
			Record r = (Record) v.get(0);
			return r.getValue("total").asInt();
		}
		return 0;
	}	
	
	public static String getManufacturer(String _sItemID)
		throws Exception
	{
		return getManufacturer(ItemTool.getItemByID(_sItemID));
	}
	
	public static String getManufacturer(Item _oItem)
		throws Exception
	{
		if(_oItem != null)
		{
			if(getAllPrincipal().size() > 0)
			{
				return getPrincipalNameByID(_oItem.getManufacturer());
			}
			return _oItem.getManufacturer();
		}
		return "";
	}
	
	//-------------------------------------------------------------------------
	// Tag
	//-------------------------------------------------------------------------
	
	public static List getAllTag()
    	throws Exception
    {
		return ItemManager.getInstance().getAllTag();
    }
	
	public static Tag getTagByID(String _sTagID)
    	throws Exception
    {
		return ItemManager.getInstance().getTagByID(_sTagID, null);
    }

	public static Tag getTagByName(String _sTag)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(TagPeer.TAG_NAME, _sTag);
		List v = TagPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			Tag oTag = (Tag) v.get(0);
			return oTag;
		}
		return null;
    }

	public static String getTagIDByCode(String _sTag)
		throws Exception
	{
		Tag oTag = getTagByName(_sTag);
		if(oTag != null)
		{
			return oTag.getId();
		}
		return "";
	}
	
	public static int getTotalItemsByTag(String _sTag, String _sKategoriID, boolean _bWebStore)
		throws Exception
	{
		StringBuilder sql = new StringBuilder("SELECT count(item_id) AS total FROM item WHERE item_status = true ");		
		if (StringUtil.isNotEmpty(_sTag))
		{
			sql.append (" AND tags = ilike '%").append(_sTag).append("%' ");
		}
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			List vKat = KategoriTool.getChildIDList(_sKategoriID);
			vKat.add(_sKategoriID);
			sql.append (" AND kategori_id IN ").append(SqlUtil.convertToINMode(vKat));
		}
		if(_bWebStore) sql.append(" AND display_store = true ");
		List v = SqlUtil.executeQuery(sql.toString());
		if(v.size() > 0)
		{
			Record r = (Record) v.get(0);
			return r.getValue("total").asInt();
		}
		return 0;
	}
	
	public static String validateTags(String _sTag)
		throws Exception
	{
		StringBuilder sbResult = new StringBuilder("");
		List v = getAllTag();
		if(StringUtil.isNotEmpty(_sTag))
		{
			String[] aTags = StringUtil.split(_sTag);
			for(int i = 0; i< aTags.length; i++)
			{
				String sTag = aTags[i];
				boolean bExist = false;
				for(int j = 0; j< v.size(); j++)
				{
					Tag oTag = (Tag) v.get(j);
					if(StringUtil.isEqual(oTag.getName(), sTag))
					{
						bExist = true;
					}
				}
				if(!bExist)
				{
					if(sbResult.length() > 0) sbResult.append(",");
					sbResult.append("Tag: '" + sTag + "' is Invalid! ");
				}
			}
		}
		return sbResult.toString();
	}
}

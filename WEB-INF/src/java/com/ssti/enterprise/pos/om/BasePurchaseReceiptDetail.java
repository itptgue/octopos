package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseReceiptDetail
 */
public abstract class BasePurchaseReceiptDetail extends BaseObject
{
    /** The Peer class */
    private static final PurchaseReceiptDetailPeer peer =
        new PurchaseReceiptDetailPeer();

        
    /** The value for the purchaseReceiptDetailId field */
    private String purchaseReceiptDetailId;
      
    /** The value for the purchaseOrderDetailId field */
    private String purchaseOrderDetailId;
      
    /** The value for the purchaseReceiptId field */
    private String purchaseReceiptId;
      
    /** The value for the indexNo field */
    private int indexNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
                                                
    /** The value for the description field */
    private String description = "";
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
      
    /** The value for the returnedQty field */
    private BigDecimal returnedQty;
      
    /** The value for the billedQty field */
    private BigDecimal billedQty;
      
    /** The value for the itemPriceInPo field */
    private BigDecimal itemPriceInPo;
      
    /** The value for the taxId field */
    private String taxId;
      
    /** The value for the taxAmount field */
    private BigDecimal taxAmount;
                                                
    /** The value for the discount field */
    private String discount = "0";
      
    /** The value for the lastPurchase field */
    private BigDecimal lastPurchase;
      
    /** The value for the costPerUnit field */
    private BigDecimal costPerUnit;
      
    /** The value for the subTotal field */
    private BigDecimal subTotal;
                                                
    /** The value for the projectId field */
    private String projectId = "";
                                                
    /** The value for the departmentId field */
    private String departmentId = "";
  
    
    /**
     * Get the PurchaseReceiptDetailId
     *
     * @return String
     */
    public String getPurchaseReceiptDetailId()
    {
        return purchaseReceiptDetailId;
    }

                        
    /**
     * Set the value of PurchaseReceiptDetailId
     *
     * @param v new value
     */
    public void setPurchaseReceiptDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseReceiptDetailId, v))
              {
            this.purchaseReceiptDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseOrderDetailId
     *
     * @return String
     */
    public String getPurchaseOrderDetailId()
    {
        return purchaseOrderDetailId;
    }

                        
    /**
     * Set the value of PurchaseOrderDetailId
     *
     * @param v new value
     */
    public void setPurchaseOrderDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseOrderDetailId, v))
              {
            this.purchaseOrderDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseReceiptId
     *
     * @return String
     */
    public String getPurchaseReceiptId()
    {
        return purchaseReceiptId;
    }

                              
    /**
     * Set the value of PurchaseReceiptId
     *
     * @param v new value
     */
    public void setPurchaseReceiptId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseReceiptId, v))
              {
            this.purchaseReceiptId = v;
            setModified(true);
        }
    
                          
                if (aPurchaseReceipt != null && !ObjectUtils.equals(aPurchaseReceipt.getPurchaseReceiptId(), v))
                {
            aPurchaseReceipt = null;
        }
      
              }
  
    /**
     * Get the IndexNo
     *
     * @return int
     */
    public int getIndexNo()
    {
        return indexNo;
    }

                        
    /**
     * Set the value of IndexNo
     *
     * @param v new value
     */
    public void setIndexNo(int v) 
    {
    
                  if (this.indexNo != v)
              {
            this.indexNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReturnedQty
     *
     * @return BigDecimal
     */
    public BigDecimal getReturnedQty()
    {
        return returnedQty;
    }

                        
    /**
     * Set the value of ReturnedQty
     *
     * @param v new value
     */
    public void setReturnedQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.returnedQty, v))
              {
            this.returnedQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BilledQty
     *
     * @return BigDecimal
     */
    public BigDecimal getBilledQty()
    {
        return billedQty;
    }

                        
    /**
     * Set the value of BilledQty
     *
     * @param v new value
     */
    public void setBilledQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.billedQty, v))
              {
            this.billedQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemPriceInPo
     *
     * @return BigDecimal
     */
    public BigDecimal getItemPriceInPo()
    {
        return itemPriceInPo;
    }

                        
    /**
     * Set the value of ItemPriceInPo
     *
     * @param v new value
     */
    public void setItemPriceInPo(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemPriceInPo, v))
              {
            this.itemPriceInPo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxId
     *
     * @return String
     */
    public String getTaxId()
    {
        return taxId;
    }

                        
    /**
     * Set the value of TaxId
     *
     * @param v new value
     */
    public void setTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxId, v))
              {
            this.taxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTaxAmount()
    {
        return taxAmount;
    }

                        
    /**
     * Set the value of TaxAmount
     *
     * @param v new value
     */
    public void setTaxAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.taxAmount, v))
              {
            this.taxAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Discount
     *
     * @return String
     */
    public String getDiscount()
    {
        return discount;
    }

                        
    /**
     * Set the value of Discount
     *
     * @param v new value
     */
    public void setDiscount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discount, v))
              {
            this.discount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastPurchase
     *
     * @return BigDecimal
     */
    public BigDecimal getLastPurchase()
    {
        return lastPurchase;
    }

                        
    /**
     * Set the value of LastPurchase
     *
     * @param v new value
     */
    public void setLastPurchase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.lastPurchase, v))
              {
            this.lastPurchase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CostPerUnit
     *
     * @return BigDecimal
     */
    public BigDecimal getCostPerUnit()
    {
        return costPerUnit;
    }

                        
    /**
     * Set the value of CostPerUnit
     *
     * @param v new value
     */
    public void setCostPerUnit(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.costPerUnit, v))
              {
            this.costPerUnit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubTotal
     *
     * @return BigDecimal
     */
    public BigDecimal getSubTotal()
    {
        return subTotal;
    }

                        
    /**
     * Set the value of SubTotal
     *
     * @param v new value
     */
    public void setSubTotal(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.subTotal, v))
              {
            this.subTotal = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private PurchaseReceipt aPurchaseReceipt;

    /**
     * Declares an association between this object and a PurchaseReceipt object
     *
     * @param v PurchaseReceipt
     * @throws TorqueException
     */
    public void setPurchaseReceipt(PurchaseReceipt v) throws TorqueException
    {
            if (v == null)
        {
                  setPurchaseReceiptId((String) null);
              }
        else
        {
            setPurchaseReceiptId(v.getPurchaseReceiptId());
        }
            aPurchaseReceipt = v;
    }

                                            
    /**
     * Get the associated PurchaseReceipt object
     *
     * @return the associated PurchaseReceipt object
     * @throws TorqueException
     */
    public PurchaseReceipt getPurchaseReceipt() throws TorqueException
    {
        if (aPurchaseReceipt == null && (!ObjectUtils.equals(this.purchaseReceiptId, null)))
        {
                          aPurchaseReceipt = PurchaseReceiptPeer.retrieveByPK(SimpleKey.keyFor(this.purchaseReceiptId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               PurchaseReceipt obj = PurchaseReceiptPeer.retrieveByPK(this.purchaseReceiptId);
               obj.addPurchaseReceiptDetails(this);
            */
        }
        return aPurchaseReceipt;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setPurchaseReceiptKey(ObjectKey key) throws TorqueException
    {
      
                        setPurchaseReceiptId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseReceiptDetailId");
              fieldNames.add("PurchaseOrderDetailId");
              fieldNames.add("PurchaseReceiptId");
              fieldNames.add("IndexNo");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("Description");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("Qty");
              fieldNames.add("QtyBase");
              fieldNames.add("ReturnedQty");
              fieldNames.add("BilledQty");
              fieldNames.add("ItemPriceInPo");
              fieldNames.add("TaxId");
              fieldNames.add("TaxAmount");
              fieldNames.add("Discount");
              fieldNames.add("LastPurchase");
              fieldNames.add("CostPerUnit");
              fieldNames.add("SubTotal");
              fieldNames.add("ProjectId");
              fieldNames.add("DepartmentId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseReceiptDetailId"))
        {
                return getPurchaseReceiptDetailId();
            }
          if (name.equals("PurchaseOrderDetailId"))
        {
                return getPurchaseOrderDetailId();
            }
          if (name.equals("PurchaseReceiptId"))
        {
                return getPurchaseReceiptId();
            }
          if (name.equals("IndexNo"))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("ReturnedQty"))
        {
                return getReturnedQty();
            }
          if (name.equals("BilledQty"))
        {
                return getBilledQty();
            }
          if (name.equals("ItemPriceInPo"))
        {
                return getItemPriceInPo();
            }
          if (name.equals("TaxId"))
        {
                return getTaxId();
            }
          if (name.equals("TaxAmount"))
        {
                return getTaxAmount();
            }
          if (name.equals("Discount"))
        {
                return getDiscount();
            }
          if (name.equals("LastPurchase"))
        {
                return getLastPurchase();
            }
          if (name.equals("CostPerUnit"))
        {
                return getCostPerUnit();
            }
          if (name.equals("SubTotal"))
        {
                return getSubTotal();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_DETAIL_ID))
        {
                return getPurchaseReceiptDetailId();
            }
          if (name.equals(PurchaseReceiptDetailPeer.PURCHASE_ORDER_DETAIL_ID))
        {
                return getPurchaseOrderDetailId();
            }
          if (name.equals(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID))
        {
                return getPurchaseReceiptId();
            }
          if (name.equals(PurchaseReceiptDetailPeer.INDEX_NO))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals(PurchaseReceiptDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(PurchaseReceiptDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(PurchaseReceiptDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(PurchaseReceiptDetailPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PurchaseReceiptDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(PurchaseReceiptDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(PurchaseReceiptDetailPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(PurchaseReceiptDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(PurchaseReceiptDetailPeer.RETURNED_QTY))
        {
                return getReturnedQty();
            }
          if (name.equals(PurchaseReceiptDetailPeer.BILLED_QTY))
        {
                return getBilledQty();
            }
          if (name.equals(PurchaseReceiptDetailPeer.ITEM_PRICE_IN_PO))
        {
                return getItemPriceInPo();
            }
          if (name.equals(PurchaseReceiptDetailPeer.TAX_ID))
        {
                return getTaxId();
            }
          if (name.equals(PurchaseReceiptDetailPeer.TAX_AMOUNT))
        {
                return getTaxAmount();
            }
          if (name.equals(PurchaseReceiptDetailPeer.DISCOUNT))
        {
                return getDiscount();
            }
          if (name.equals(PurchaseReceiptDetailPeer.LAST_PURCHASE))
        {
                return getLastPurchase();
            }
          if (name.equals(PurchaseReceiptDetailPeer.COST_PER_UNIT))
        {
                return getCostPerUnit();
            }
          if (name.equals(PurchaseReceiptDetailPeer.SUB_TOTAL))
        {
                return getSubTotal();
            }
          if (name.equals(PurchaseReceiptDetailPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(PurchaseReceiptDetailPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseReceiptDetailId();
            }
              if (pos == 1)
        {
                return getPurchaseOrderDetailId();
            }
              if (pos == 2)
        {
                return getPurchaseReceiptId();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getIndexNo());
            }
              if (pos == 4)
        {
                return getItemId();
            }
              if (pos == 5)
        {
                return getItemCode();
            }
              if (pos == 6)
        {
                return getItemName();
            }
              if (pos == 7)
        {
                return getDescription();
            }
              if (pos == 8)
        {
                return getUnitId();
            }
              if (pos == 9)
        {
                return getUnitCode();
            }
              if (pos == 10)
        {
                return getQty();
            }
              if (pos == 11)
        {
                return getQtyBase();
            }
              if (pos == 12)
        {
                return getReturnedQty();
            }
              if (pos == 13)
        {
                return getBilledQty();
            }
              if (pos == 14)
        {
                return getItemPriceInPo();
            }
              if (pos == 15)
        {
                return getTaxId();
            }
              if (pos == 16)
        {
                return getTaxAmount();
            }
              if (pos == 17)
        {
                return getDiscount();
            }
              if (pos == 18)
        {
                return getLastPurchase();
            }
              if (pos == 19)
        {
                return getCostPerUnit();
            }
              if (pos == 20)
        {
                return getSubTotal();
            }
              if (pos == 21)
        {
                return getProjectId();
            }
              if (pos == 22)
        {
                return getDepartmentId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseReceiptDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseReceiptDetailPeer.doInsert((PurchaseReceiptDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseReceiptDetailPeer.doUpdate((PurchaseReceiptDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseReceiptDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPurchaseReceiptDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPurchaseReceiptDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseReceiptDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseReceiptDetail copy() throws TorqueException
    {
        return copyInto(new PurchaseReceiptDetail());
    }
  
    protected PurchaseReceiptDetail copyInto(PurchaseReceiptDetail copyObj) throws TorqueException
    {
          copyObj.setPurchaseReceiptDetailId(purchaseReceiptDetailId);
          copyObj.setPurchaseOrderDetailId(purchaseOrderDetailId);
          copyObj.setPurchaseReceiptId(purchaseReceiptId);
          copyObj.setIndexNo(indexNo);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setDescription(description);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setQty(qty);
          copyObj.setQtyBase(qtyBase);
          copyObj.setReturnedQty(returnedQty);
          copyObj.setBilledQty(billedQty);
          copyObj.setItemPriceInPo(itemPriceInPo);
          copyObj.setTaxId(taxId);
          copyObj.setTaxAmount(taxAmount);
          copyObj.setDiscount(discount);
          copyObj.setLastPurchase(lastPurchase);
          copyObj.setCostPerUnit(costPerUnit);
          copyObj.setSubTotal(subTotal);
          copyObj.setProjectId(projectId);
          copyObj.setDepartmentId(departmentId);
  
                    copyObj.setPurchaseReceiptDetailId((String)null);
                                                                                                                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseReceiptDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseReceiptDetail\n");
        str.append("---------------------\n")
            .append("PurchaseReceiptDetailId   : ")
           .append(getPurchaseReceiptDetailId())
           .append("\n")
            .append("PurchaseOrderDetailId   : ")
           .append(getPurchaseOrderDetailId())
           .append("\n")
           .append("PurchaseReceiptId    : ")
           .append(getPurchaseReceiptId())
           .append("\n")
           .append("IndexNo              : ")
           .append(getIndexNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("ReturnedQty          : ")
           .append(getReturnedQty())
           .append("\n")
           .append("BilledQty            : ")
           .append(getBilledQty())
           .append("\n")
           .append("ItemPriceInPo        : ")
           .append(getItemPriceInPo())
           .append("\n")
           .append("TaxId                : ")
           .append(getTaxId())
           .append("\n")
           .append("TaxAmount            : ")
           .append(getTaxAmount())
           .append("\n")
           .append("Discount             : ")
           .append(getDiscount())
           .append("\n")
           .append("LastPurchase         : ")
           .append(getLastPurchase())
           .append("\n")
           .append("CostPerUnit          : ")
           .append(getCostPerUnit())
           .append("\n")
           .append("SubTotal             : ")
           .append(getSubTotal())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CashFlowMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CashFlowMapBuilder";

    /**
     * Item
     * @deprecated use CashFlowPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "cash_flow";
    }

  
    /**
     * cash_flow.CASH_FLOW_ID
     * @return the column name for the CASH_FLOW_ID field
     * @deprecated use CashFlowPeer.cash_flow.CASH_FLOW_ID constant
     */
    public static String getCashFlow_CashFlowId()
    {
        return "cash_flow.CASH_FLOW_ID";
    }
  
    /**
     * cash_flow.CASH_FLOW_NO
     * @return the column name for the CASH_FLOW_NO field
     * @deprecated use CashFlowPeer.cash_flow.CASH_FLOW_NO constant
     */
    public static String getCashFlow_CashFlowNo()
    {
        return "cash_flow.CASH_FLOW_NO";
    }
  
    /**
     * cash_flow.CASH_FLOW_TYPE
     * @return the column name for the CASH_FLOW_TYPE field
     * @deprecated use CashFlowPeer.cash_flow.CASH_FLOW_TYPE constant
     */
    public static String getCashFlow_CashFlowType()
    {
        return "cash_flow.CASH_FLOW_TYPE";
    }
  
    /**
     * cash_flow.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use CashFlowPeer.cash_flow.BANK_ID constant
     */
    public static String getCashFlow_BankId()
    {
        return "cash_flow.BANK_ID";
    }
  
    /**
     * cash_flow.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use CashFlowPeer.cash_flow.BANK_ISSUER constant
     */
    public static String getCashFlow_BankIssuer()
    {
        return "cash_flow.BANK_ISSUER";
    }
  
    /**
     * cash_flow.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use CashFlowPeer.cash_flow.REFERENCE_NO constant
     */
    public static String getCashFlow_ReferenceNo()
    {
        return "cash_flow.REFERENCE_NO";
    }
  
    /**
     * cash_flow.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use CashFlowPeer.cash_flow.CURRENCY_ID constant
     */
    public static String getCashFlow_CurrencyId()
    {
        return "cash_flow.CURRENCY_ID";
    }
  
    /**
     * cash_flow.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use CashFlowPeer.cash_flow.CURRENCY_RATE constant
     */
    public static String getCashFlow_CurrencyRate()
    {
        return "cash_flow.CURRENCY_RATE";
    }
  
    /**
     * cash_flow.EXPECTED_DATE
     * @return the column name for the EXPECTED_DATE field
     * @deprecated use CashFlowPeer.cash_flow.EXPECTED_DATE constant
     */
    public static String getCashFlow_ExpectedDate()
    {
        return "cash_flow.EXPECTED_DATE";
    }
  
    /**
     * cash_flow.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use CashFlowPeer.cash_flow.LOCATION_ID constant
     */
    public static String getCashFlow_LocationId()
    {
        return "cash_flow.LOCATION_ID";
    }
  
    /**
     * cash_flow.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use CashFlowPeer.cash_flow.DUE_DATE constant
     */
    public static String getCashFlow_DueDate()
    {
        return "cash_flow.DUE_DATE";
    }
  
    /**
     * cash_flow.CASH_FLOW_AMOUNT
     * @return the column name for the CASH_FLOW_AMOUNT field
     * @deprecated use CashFlowPeer.cash_flow.CASH_FLOW_AMOUNT constant
     */
    public static String getCashFlow_CashFlowAmount()
    {
        return "cash_flow.CASH_FLOW_AMOUNT";
    }
  
    /**
     * cash_flow.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use CashFlowPeer.cash_flow.AMOUNT_BASE constant
     */
    public static String getCashFlow_AmountBase()
    {
        return "cash_flow.AMOUNT_BASE";
    }
  
    /**
     * cash_flow.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use CashFlowPeer.cash_flow.USER_NAME constant
     */
    public static String getCashFlow_UserName()
    {
        return "cash_flow.USER_NAME";
    }
  
    /**
     * cash_flow.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use CashFlowPeer.cash_flow.DESCRIPTION constant
     */
    public static String getCashFlow_Description()
    {
        return "cash_flow.DESCRIPTION";
    }
  
    /**
     * cash_flow.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use CashFlowPeer.cash_flow.TRANSACTION_ID constant
     */
    public static String getCashFlow_TransactionId()
    {
        return "cash_flow.TRANSACTION_ID";
    }
  
    /**
     * cash_flow.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use CashFlowPeer.cash_flow.TRANSACTION_NO constant
     */
    public static String getCashFlow_TransactionNo()
    {
        return "cash_flow.TRANSACTION_NO";
    }
  
    /**
     * cash_flow.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use CashFlowPeer.cash_flow.TRANSACTION_TYPE constant
     */
    public static String getCashFlow_TransactionType()
    {
        return "cash_flow.TRANSACTION_TYPE";
    }
  
    /**
     * cash_flow.STATUS
     * @return the column name for the STATUS field
     * @deprecated use CashFlowPeer.cash_flow.STATUS constant
     */
    public static String getCashFlow_Status()
    {
        return "cash_flow.STATUS";
    }
  
    /**
     * cash_flow.SAY_AMOUNT
     * @return the column name for the SAY_AMOUNT field
     * @deprecated use CashFlowPeer.cash_flow.SAY_AMOUNT constant
     */
    public static String getCashFlow_SayAmount()
    {
        return "cash_flow.SAY_AMOUNT";
    }
  
    /**
     * cash_flow.JOURNAL_AMOUNT
     * @return the column name for the JOURNAL_AMOUNT field
     * @deprecated use CashFlowPeer.cash_flow.JOURNAL_AMOUNT constant
     */
    public static String getCashFlow_JournalAmount()
    {
        return "cash_flow.JOURNAL_AMOUNT";
    }
  
    /**
     * cash_flow.RECONCILED
     * @return the column name for the RECONCILED field
     * @deprecated use CashFlowPeer.cash_flow.RECONCILED constant
     */
    public static String getCashFlow_Reconciled()
    {
        return "cash_flow.RECONCILED";
    }
  
    /**
     * cash_flow.RECONCILE_DATE
     * @return the column name for the RECONCILE_DATE field
     * @deprecated use CashFlowPeer.cash_flow.RECONCILE_DATE constant
     */
    public static String getCashFlow_ReconcileDate()
    {
        return "cash_flow.RECONCILE_DATE";
    }
  
    /**
     * cash_flow.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use CashFlowPeer.cash_flow.CANCEL_BY constant
     */
    public static String getCashFlow_CancelBy()
    {
        return "cash_flow.CANCEL_BY";
    }
  
    /**
     * cash_flow.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use CashFlowPeer.cash_flow.CANCEL_DATE constant
     */
    public static String getCashFlow_CancelDate()
    {
        return "cash_flow.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("cash_flow");
        TableMap tMap = dbMap.getTable("cash_flow");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("cash_flow.CASH_FLOW_ID", "");
                          tMap.addColumn("cash_flow.CASH_FLOW_NO", "");
                            tMap.addColumn("cash_flow.CASH_FLOW_TYPE", Integer.valueOf(0));
                          tMap.addColumn("cash_flow.BANK_ID", "");
                          tMap.addColumn("cash_flow.BANK_ISSUER", "");
                          tMap.addColumn("cash_flow.REFERENCE_NO", "");
                          tMap.addColumn("cash_flow.CURRENCY_ID", "");
                            tMap.addColumn("cash_flow.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("cash_flow.EXPECTED_DATE", new Date());
                          tMap.addColumn("cash_flow.LOCATION_ID", "");
                          tMap.addColumn("cash_flow.DUE_DATE", new Date());
                            tMap.addColumn("cash_flow.CASH_FLOW_AMOUNT", bd_ZERO);
                            tMap.addColumn("cash_flow.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("cash_flow.USER_NAME", "");
                          tMap.addColumn("cash_flow.DESCRIPTION", "");
                          tMap.addColumn("cash_flow.TRANSACTION_ID", "");
                          tMap.addColumn("cash_flow.TRANSACTION_NO", "");
                            tMap.addColumn("cash_flow.TRANSACTION_TYPE", Integer.valueOf(0));
                            tMap.addColumn("cash_flow.STATUS", Integer.valueOf(0));
                          tMap.addColumn("cash_flow.SAY_AMOUNT", "");
                            tMap.addColumn("cash_flow.JOURNAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("cash_flow.RECONCILED", Boolean.TRUE);
                          tMap.addColumn("cash_flow.RECONCILE_DATE", new Date());
                          tMap.addColumn("cash_flow.CANCEL_BY", "");
                          tMap.addColumn("cash_flow.CANCEL_DATE", new Date());
          }
}

package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Brand;
import com.ssti.enterprise.pos.om.BrandPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemField;
import com.ssti.enterprise.pos.om.ItemFieldPeer;
import com.ssti.enterprise.pos.om.ItemGroupPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.om.Principal;
import com.ssti.enterprise.pos.om.PrincipalPeer;
import com.ssti.enterprise.pos.om.Tag;
import com.ssti.enterprise.pos.om.TagPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.enterprise.pos.tools.report.SalesReportTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/manager/ItemManager.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ItemManager.java,v 1.26 2008/02/26 05:11:34 albert Exp $
 *
 * $Log: ItemManager.java,v $
 *
 * 2018-06-26
 * - Change findItemByCode, only use new price from price list detail if > 0
 * 
 * 2018-04-07
 * - Add principal related query
 * 
 * 2016-07-21
 * - Add brand and tag related query
 * 
 * 2015-07-31
 * - Add empty Reference for not exists ItemField to add more effective caching.
 */

public class ItemManager implements AppAttributes
{
	private static Log log = LogFactory.getLog(ItemManager.class);
	    
    private static ItemManager m_oInstance;
    private static CacheManager m_oManager;
    
    private static final String s_CACHE = "item";
    private static final String s_CACHE_GROUP = "item_group";    
    private static final String s_CACHE_FIND  = "find_item";
    private static final String s_CACHE_FIELD = "item_field";
    private static final String s_CACHE_AVGS  = "item_avg_sales";
    
    private static final int i_ALL_ITEM_LIMIT = 5000;
    private static boolean b_USE_CACHE = true;
    
    private ItemManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);          
        }
    }

    public static ItemManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (ItemManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new ItemManager();
                }
            }
        }
        return m_oInstance;
    }
    
    public CacheManager getCacheManager()
    {
    	return m_oManager;
    }

    public Cache getCache(String _sName)
    {
    	if(StringUtil.isNotEmpty(_sName) && 
    	   StringUtil.equals(_sName, s_CACHE) && 
    	   StringUtil.equals(_sName, s_CACHE_FIND) &&
    	   StringUtil.equals(_sName, s_CACHE_FIELD) && 
    	   StringUtil.equals(_sName, s_CACHE_GROUP))
    	{
    		return m_oManager.getCache(_sName);
    	}
    	return m_oManager.getCache(s_CACHE);
    }

    //-------------------------------------------------------------------------
    // BRAND
    //-------------------------------------------------------------------------    
    /**
     * 
     * @return
     * @throws Exception
     */
    public List getAllBrand()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("AllBrand");
		List vData = new ArrayList();
		if (oElement == null)
		{
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(BrandPeer.PRINCIPAL_ID);
			oCrit.addAscendingOrderByColumn(BrandPeer.BRAND_CODE);
			vData = BrandPeer.doSelect(oCrit);
			oCache.put(new Element("AllBrand", (Serializable)vData));			
		}
		else
		{
			vData = (List) oElement.getValue();
		}
		return vData;
    }

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public Brand getBrandByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("Brand" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(BrandPeer.BRAND_ID, _sID);
				List vData = BrandPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					Brand oBrand = (Brand) vData.get(0);
					oCache.put(new Element("Brand" + _sID, oBrand)); 
					return oBrand;
				}
			}
			return null;
		}
		return (Brand) oElement.getValue();
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public List getItemByBrandID(String _sID, boolean _bDisplayStore, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("ItemByBrand" + _sID);
		List vData = new ArrayList();
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(ItemPeer.BRAND, _sID);
				oCrit.add(ItemPeer.ITEM_STATUS, true);
				if(_bDisplayStore)
				{
					oCrit.add(ItemPeer.DISPLAY_STORE,true);
				}
				vData = ItemPeer.doSelect(oCrit, _oConn);
			}
		}
		else			
		{
			vData = (List) oElement.getValue();
		}
		return vData;
	}
	
	public List getBrandByPrincID(String _sID)
    	throws Exception
    {
		Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("BrandByPrincID" + _sID);
		List vData = new ArrayList();
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(BrandPeer.PRINCIPAL_ID, StringUtil.trim(_sID));
				oCrit.addAscendingOrderByColumn(BrandPeer.BRAND_CODE);
				vData =  BrandPeer.doSelect(oCrit);
			}
		}
		else			
		{
			vData = (List) oElement.getValue();
		}
		return vData;		
    }
	
	public List getGenericBrand()
    	throws Exception
    {
		Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("GenericBrand");
		List vData = new ArrayList();
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(BrandPeer.PRINCIPAL_ID, "");
			oCrit.addAscendingOrderByColumn(BrandPeer.BRAND_CODE);
			vData =  BrandPeer.doSelect(oCrit);
		}
		else			
		{
			vData = (List) oElement.getValue();
		}
		return vData;		
    }
	
	//-------------------------------------------------------------------------
    // PRINCIPAL
    //-------------------------------------------------------------------------    
    /**
     * 
     * @return
     * @throws Exception
     */
    public List getAllPrincipal()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("AllPrincipal");
		List vData = new ArrayList();
		if (oElement == null)
		{
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(PrincipalPeer.PRINCIPAL_CODE);
			vData = PrincipalPeer.doSelect(oCrit);
			oCache.put(new Element("AllPrincipal", (Serializable)vData));			
		}
		else
		{
			vData = (List) oElement.getValue();
		}
		return vData;
    }

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public Principal getPrincipalByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("Principal" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(PrincipalPeer.PRINCIPAL_ID, _sID);
				List vData = PrincipalPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					Principal oPrincipal = (Principal) vData.get(0);
					oCache.put(new Element("Principal" + _sID, oPrincipal)); 
					return oPrincipal;
				}
			}
			return null;
		}
		return (Principal) oElement.getValue();
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public List getItemByPrincipalID(String _sID, boolean _bDisplayStore, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("ItemByPrincipal" + _sID);
		List vData = new ArrayList();
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(ItemPeer.MANUFACTURER, _sID);
				oCrit.add(ItemPeer.ITEM_STATUS, true);
				if(_bDisplayStore)
				{
					oCrit.add(ItemPeer.DISPLAY_STORE,true);
				}
				vData = ItemPeer.doSelect(oCrit, _oConn);
			}
		}
		else			
		{
			vData = (List) oElement.getValue();
		}
		return vData;
	}
	

    //-------------------------------------------------------------------------
    // Tag
    //-------------------------------------------------------------------------    
	/**
     * 
     * @return
     * @throws Exception
     */
    public List getAllTag()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("AllTag");
		List vData = new ArrayList();
		if (oElement == null)
		{
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn(TagPeer.TAG_NAME);
			vData = TagPeer.doSelect(oCrit);
			oCache.put(new Element("AllTag", (Serializable)vData));			
		}
		else
		{
			vData = (List) oElement.getValue();
		}
		return vData;
    }

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public Tag getTagByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("Tag" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(TagPeer.TAG_ID, _sID);
				List vData = TagPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					Tag oTag = (Tag) vData.get(0);
					oCache.put(new Element("Tag" + _sID, oTag)); 
					return oTag;
				}
			}
			return null;
		}
		return (Tag) oElement.getValue();
	}
	
	/**
	 * 
	 * @param _sTag
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public List getItemByTags(String _sTag, boolean _bDisplayStore, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("ItemByTag" + _sTag);
		List vData = new ArrayList();
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sTag))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(ItemPeer.ITEM_STATUS, true);
				oCrit.add(ItemPeer.IS_DISCONTINUE,  false);
				if(_bDisplayStore)
				{
					oCrit.add(ItemPeer.DISPLAY_STORE, true);
				}
				oCrit.add(ItemPeer.TAGS, (Object)("%"+ _sTag + "%"), Criteria.ILIKE);
				vData = ItemPeer.doSelect(oCrit, _oConn);
			}
		}
		else			
		{
			vData = (List) oElement.getValue();
		}
		return vData;
	}
	
	//-------------------------------------------------------------------------
	// ITEM
	//-------------------------------------------------------------------------
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public Item getItemByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("Item" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(ItemPeer.ITEM_ID, _sID);
				List vData = ItemPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					Item oItem = (Item) vData.get(0);
					oCache.put(new Element("Item" + _sID, oItem)); 
					oCache.put(new Element("ItemByCode" + oItem.getItemCode(), oItem)); 
					return oItem;
				}
			}
			return null;
		}
		return (Item) oElement.getValue();
	}

	/**
	 * 
	 * @param _sCode
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public Item getItemByCode(String _sCode, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("ItemByCode" + _sCode);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(ItemPeer.ITEM_CODE, _sCode);
				List vData = ItemPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					Item oItem = (Item) vData.get(0);
					oCache.put(new Element("Item" + oItem.getItemId(), oItem)); 
					oCache.put(new Element("ItemByCode" + oItem.getItemCode(), oItem)); 
					return oItem;					
				}
			}
			return null;
		}
		return (Item) oElement.getValue();
	}
	
	/**
	 * 
	 * @param _sCode
	 * @param _sCustomerID
	 * @param _iTransGroup
	 * @return
	 * @throws Exception
	 */
	public Item findItemByCode (String _sCode, 
								String _sCustomerID, 
								String _sLocationID,
								int _iTransGroup,
								Date _dTransDate)
    	throws Exception
    {
		Cache oCache = m_oManager.getCache(s_CACHE_FIND);
		Element oElement = null;
		String sDate = CustomFormatter.formatCustomDate(_dTransDate, "yyyyMMdd");
		StringBuilder sKey = new StringBuilder();
		sKey.append("FindItem").append(_sCode).append(_sCustomerID).append(_sLocationID).append(sDate);
		if(b_USE_CACHE) oElement = oCache.get(sKey);
	    
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();			
        	Criteria.Criterion oBarcode = oCrit.getNewCriterion (ItemPeer.BARCODE, _sCode, Criteria.ILIKE);
        	Criteria.Criterion oItemCode = oCrit.getNewCriterion (ItemPeer.ITEM_CODE, _sCode, Criteria.ILIKE);
        	oCrit.add(oBarcode.or(oItemCode));
			oCrit.add(ItemPeer.ITEM_STATUS, AppAttributes.b_ACTIVE_ITEM);
			oCrit.setLimit(1);
			
			if(_iTransGroup > 0)
		    {
		    	int[] aItemType = ItemTool.getItemTypeAvaibility(_iTransGroup);
		    	oCrit.addIn (ItemPeer.ITEM_TYPE, aItemType);
		    }
			
			List vData = ItemPeer.doSelect(oCrit);
			Item oItem = null;
			if (vData.size() > 0) 
			{
				oItem =  (Item) vData.get(0);
				if (StringUtil.isNotEmpty(_sCustomerID)) 
				{   
					String sCustomerTypeID = CustomerTool.getCustomerTypeByID (_sCustomerID);
					String sItemID = oItem.getItemId();
					PriceListDetail oPLD = 
						PriceListTool.getDetail(_sCustomerID, sCustomerTypeID, _sLocationID, sItemID, _dTransDate);		
					
					if (oPLD != null) 
					{
						if(oPLD.getNewPrice().doubleValue() >= 0)
						{
							oItem.setItemPrice(oPLD.getNewPrice());
							oItem.setFromPriceList(true);
						}
						if(!StringUtil.empty(oPLD.getDiscountAmount()))
						{
							oItem.setDiscountAmount (oPLD.getDiscountAmount());
						}
					}
				} 
				if(b_USE_CACHE) oCache.put(new Element(sKey, oItem));	
			}
			return oItem;
		}
		return (Item) oElement.getValue();
	}

	/**
	 * 
	 * @param _sKatID
	 * @return
	 * @throws Exception
	 */
	public List getItemByKategoriID (String _sKatID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("ItemKategori" + _sKatID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add (ItemPeer.KATEGORI_ID, _sKatID);	
			oCrit.addAscendingOrderByColumn (ItemPeer.ITEM_CODE);	
			List vData = ItemPeer.doSelect(oCrit);
			oCache.put(new Element ("ItemKategori" + _sKatID, (Serializable) vData)); 
			return vData;
		}
		return (List) oElement.getValue();
	}	
	
	static final ItemField o_EMPTYFIELD = new ItemField();

	/**
	 * 
	 * @param _sItemID
	 * @return
	 * @throws Exception
	 */
	public List getItemFields(String _sItemID, Connection _oConn) 
		throws Exception
	{
		String sKey = "ItemField" + _sItemID;
        Cache oCache = m_oManager.getCache(s_CACHE_FIELD);
		Element oElement = oCache.get(sKey);
		
		if (oElement == null)
		{
			if (StringUtil.isNotEmpty(_sItemID))
			{
				Criteria oCrit = new Criteria();
			    oCrit.add(ItemFieldPeer.ITEM_ID, _sItemID);
			    List v = ItemFieldPeer.doSelect(oCrit);
			    oCache.put(new Element (sKey, (Serializable)v));
			    return v;
			}
			return null;
		}
		return (List) oElement.getValue();	
	}	
	
	/**
	 * 
	 * @param _sItemID
	 * @param _sField
	 * @return
	 * @throws Exception
	 */
	public ItemField getItemField(String _sItemID, String _sField, Connection _oConn) 
		throws Exception
	{
		List v = getItemFields(_sItemID, _oConn);
		if (v != null && v.size() > 0)
		{
			for (int i = 0; i < v.size(); i++)
			{
				ItemField oField = (ItemField) v.get(i);
				if(StringUtil.equalsIgnoreCase(oField.getFieldName(), _sField))
				{
					return oField;
				}
			}
		}
		return null;
	}	

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public List getItemGroupByGroupID(String _sID, Connection _oConn)
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE_GROUP);
		Element oElement = oCache.get("ItemGroup" + _sID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(ItemGroupPeer.GROUP_ID, _sID);
			List vData = ItemGroupPeer.doSelect(oCrit, _oConn);
			oCache.put(new Element("ItemGroup" + _sID, (Serializable)vData)); 
			return vData;
		}
		return (List) oElement.getValue();				
	}
	
	/**
	 * cache refresh which remove item in cache
	 * 
	 * @param _sItemID
	 * @param _sItemCode
	 * @param _sKategoriID
	 * @throws Exception
	 */
	public void refreshCache (String _sItemID, String _sItemCode, String _sKategoriID) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);
	    oCache.remove ("Item" + _sItemID);
	    oCache.remove ("ItemByCode" + _sItemCode);
	    oCache.remove ("ItemKategori" + _sKategoriID);

	    refreshFieldCache(null, null, null);
	    refreshFindCache();
	}	
	
	/**
	 * cache refresh which updating item in cache
	 * 
	 * @param _oData
	 * @throws Exception
	 */
	public void refreshCache (Item _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        if (_oData != null)
        {
            oCache.put(new Element ("Item" + _oData.getItemId(), _oData));
            oCache.put(new Element ("ItemByCode" + _oData.getItemCode(), _oData));
            oCache.remove ("ItemField" + _oData.getItemId());
            oCache.remove ("ItemKategori" + _oData.getKategoriId());        
            oCache.remove ("ItemGroup" + _oData.getItemId());   
            oCache.remove ("ItemByBrand" + _oData.getManufacturer());
            oCache.remove ("ItemByTag" + _oData.getTags());            
        }
        else
        {
        	oCache.removeAll();
        }
        refreshFieldCache(null, null, null);
        refreshFindCache();
        refreshAvgSalesCache();
    }

	/**
	 * 
	 * @param _sItemID
	 * @throws Exception
	 */
	public void refreshFieldCache (String _sItemID, String _sField, ItemField _oData) 
		throws Exception
	{	
		Cache oFieldCache = m_oManager.getCache(s_CACHE_FIELD);
		
		if (StringUtil.isNotEmpty(_sItemID) && StringUtil.isNotEmpty(_sField) )
		{
			oFieldCache.remove("ItemField" + _sItemID); 
		}
		else
		{
			oFieldCache.removeAll();
		}
	}

	/**
	 * 
	 * @param _sBrandID
	 * @throws Exception
	 */
	public void refreshBrand (String _sBrandID) 
		throws Exception
	{	
		Cache oCache = m_oManager.getCache(s_CACHE);		
		if (StringUtil.isNotEmpty(_sBrandID))
		{
			oCache.remove("Brand" + _sBrandID);
			oCache.remove("ItemByBrand" + _sBrandID);						
		}
		oCache.remove("GenericBrand");
		oCache.remove("AllBrand");
	}

	/**
	 * 
	 * @param _sPrincipalID
	 * @throws Exception
	 */
	public void refreshPrincipal (String _sPrincipalID) 
		throws Exception
	{	
		Cache oCache = m_oManager.getCache(s_CACHE);		
		if (StringUtil.isNotEmpty(_sPrincipalID))
		{
			oCache.remove("Principal" + _sPrincipalID);
			oCache.remove("ItemByPrincipal" + _sPrincipalID);	
			oCache.remove("BrandByPrincID" + _sPrincipalID);
		}
		oCache.remove("AllPrincipal");
	}

	/**
	 * 
	 * @param _sBrandID
	 * @throws Exception
	 */
	public void refreshTag (String _sTagID, String _sTag) 
		throws Exception
	{	
		Cache oCache = m_oManager.getCache(s_CACHE);		
		if (StringUtil.isNotEmpty(_sTagID))
		{
			oCache.remove("Tag" + _sTagID);
			oCache.remove("ItemByTag" + _sTagID);			
		}
		oCache.remove("AllTag");
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	void refreshFindCache () 
    	throws Exception
    {	
        Cache oFindCache = m_oManager.getCache(s_CACHE_FIND);
        oFindCache.removeAll();
    }	
	
	/**
	 * 
	 * @param _sItemID
	 * @throws Exception
	 */
	public void refreshGroupCache (String _sItemID) 
		throws Exception
	{	
		Cache oGroupCache = m_oManager.getCache(s_CACHE_GROUP);
		
		if (StringUtil.isNotEmpty(_sItemID) )
		{	
			oGroupCache.remove ("ItemGroup" + _sItemID);  
		}
		else
		{
			oGroupCache.removeAll();
		}
	}	
	
	public Double getAvgSales(String _sItemID, String _sLocID, int _iDays, Date _dTo)
		throws Exception
	{
		if(_dTo == null) _dTo = new Date();
		String sTo = CustomFormatter.formatCustomDateTime(_dTo, "yyMMdd");
        Cache oCache = m_oManager.getCache(s_CACHE_AVGS);
        if (oCache != null)
        {
	        String sKey = "ItemAvgSls" + _iDays + "DT" + sTo + "ID" + _sItemID + _sLocID;
	        Element oElement = oCache.get(sKey);
			if (oElement == null)
			{		
				double dAvg = SalesReportTool.getAverageSalesPerDay(_sItemID, _sLocID, _iDays, _dTo);
				oCache.put(new Element(sKey, (Serializable)Double.valueOf(dAvg))); 
				return Double.valueOf(dAvg);
			}        
			return (Double) oElement.getValue();
        }
        return null;
	}
	
	public void refreshAvgSalesCache() 
		throws Exception
	{	
		Cache oAvgCache = m_oManager.getCache(s_CACHE_AVGS);	
		if(oAvgCache != null) oAvgCache.removeAll();
	}
}
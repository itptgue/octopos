package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.PaymentTermManager;
import com.ssti.enterprise.pos.om.PaymentEdcConfig;
import com.ssti.enterprise.pos.om.PaymentEdcPeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.framework.tools.IDGenerator;

public class PaymentTermEDCConfig extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		String sPTID = data.getParameters().getString("ptid","");
		try
		{			
			if (iOp == i_SETUP_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", PaymentTermTool.getEDCConfigByID(sID));
    		}
			if (iOp == i_SETUP_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				PaymentEdcConfig oData = PaymentTermTool.getEDCConfigByID(sID);
				if (oData == null)
				{
					oData = new PaymentEdcConfig();
					oData.setConfigId(IDGenerator.generateSysID());					
				}				
				data.getParameters().setProperties(oData);						
				oData.save();			
				PaymentTermManager.getInstance().refreshCache(sPTID);
				PaymentTermManager.getInstance().refreshEDCConfig(oData.getPaymentTermId(), oData.getLocationId());
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_SETUP_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				PaymentEdcConfig oData = PaymentTermTool.getEDCConfigByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(PaymentEdcPeer.PAYMENT_EDC_ID, oData.getId());
					PaymentEdcPeer.doDelete(oCrit);
					PaymentTermManager.getInstance().refreshCache(sPTID);
					PaymentTermManager.getInstance().refreshEDCConfig(oData.getPaymentTermId(), oData.getLocationId());
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
				}
			}
			context.put("pt", PaymentTermTool.getPaymentTermByID(sPTID));
			List v = PaymentTermTool.findEDCConfig(sPTID);
			context.put("vData", v);
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}

package com.ssti.enterprise.medical.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.manager.MedConfigManager;
import com.ssti.enterprise.medical.om.MedColor;
import com.ssti.enterprise.medical.om.MedColorPeer;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class MedColorForm extends Default
{
	public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
    	try 
    	{
        	if (iOp == 1)
        	{
        		saveData(data);
        		data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
        	}
        	if (iOp == 2)
        	{
        		delData(data);
        		data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
        	}    				
		} 
    	catch (Exception e) 
    	{
    		data.setMessage("ERROR:" + e.getMessage());
    		log.error(e);
		}
    }

	private void saveData(RunData data) 
		throws Exception
	{
		String sColor = data.getParameters().getString("color");
		MedColor clr = MedConfigTool.getColor(sColor);
		if(clr == null) 
		{
			clr = new MedColor();
			clr.setColor(sColor);
		}
		data.getParameters().setProperties(clr);
		clr.save();
		MedConfigManager.getInstance().refreshCache();
	}
	
	private void delData(RunData data) 
		throws Exception
	{
		String sColor = data.getParameters().getString("color");
		Criteria oCrit = new Criteria();
		oCrit.add(MedColorPeer.COLOR, sColor);
		MedColorPeer.doDelete(oCrit);
		MedConfigManager.getInstance().refreshCache();
	}
}
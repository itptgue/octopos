package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Brand;
import com.ssti.enterprise.pos.om.BrandPeer;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.InventoryLocationPeer;
import com.ssti.enterprise.pos.om.InventoryTransactionPeer;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemFieldPeer;
import com.ssti.enterprise.pos.om.ItemGroupPeer;
import com.ssti.enterprise.pos.om.ItemInventoryPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemStatusCodePeer;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.om.Principal;
import com.ssti.enterprise.pos.om.PrincipalPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorPeer;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemTool.java,v 1.87 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemTool.java,v $
 *
 * 2018-08-29
 * - change method getItemTypeAvaibility, allow add grouping item in purchase
 * 
 * 2018-08-14
 * - change method updateLastPurchase, lastPurchase data from PurchaseInvoice now already in base unit
 * 
 * 2018-04-07
 * - Add Brand Field in Item, Item.Brand contains Brand Name
 * - Add Principal Table, Item.Manufacturer contains Principal Name
 * - change method findData, add parameter principal ID
 * - change Method createFindCriteria, now can search by manufacturer and by brand 
 *   
 * 2017-12-22
 * - Change method findData, add new view item_allloc_qty which keep qty in all location in SQL View
 *   findData now can handle empty locationId and query qty from all location
 * 
 * 2017-08-25
 * - Change findPurchaseItemByCode add item type validation, @see VendorTool
 *   Vendor Data now has ItemType field to define vendor allowed item type
 *   Venodr Data now has DefaultItemDisc, if not empty then set Item's DiscountAmount field.
 * 
 * 2017-01-03
 * - change getItemByName,getItemByDescription clear _sItemName parameters from strange characters
 * 
 * 2016-09-20
 * - Change findPurchaseItem, remove assign VendorPriceList value in findPurchaseItem method
 * 
 * 2016-07-20
 * - Add Brand & Tags functions in ItemFieldTool and search functions
 * 
 * 2015-10-10
 * - Change updateLastPurchase remove oItem.setPrice(MedConfigTool.getPrice) will create another update utility for medical item
 * 
 * 2015-07-31
 * - Add TODO refactor updateLastPurchase to remove dependency with medical module
 * 
 * </pre><br>
 */
public class ItemTool extends BaseTool
{
	private static Log log = LogFactory.getLog(ItemTool.class);
	
	public static ItemManager getManager()
	{
		return ItemManager.getInstance();
	}
	
	/**
	 * @return all item
	 * @throws Exception
	 */
	public static List getAllItem()
    	throws Exception
    {
		return ItemPeer.doSelect(new Criteria());
    }
	
	/**
	 * @return all item
	 * @throws Exception
	 */
	public static LargeSelect getAllItemLS()
    	throws Exception
    {
		return new LargeSelect(new Criteria(), 500, "com.ssti.enterprise.pos.om.ItemPeer");
    }

    public static Item getItemByID(String _sID)
    	throws Exception
    {
		return getItemByID(_sID, null);
	}
        
    public static Item getItemByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return ItemManager.getInstance().getItemByID(_sID, _oConn);
	}

	public static Item findItemByCode (String _sItemCode, String _sCustomerID, String _sLocationID)
		throws Exception
	{
		return findItemByCode (_sItemCode, _sCustomerID, _sLocationID, -1);
	}
    
	public static Item findItemByCode (String _sItemCode, 
									   String _sCustomerID, 
									   String _sLocationID,
									   int _iTransGroup)
    	throws Exception
    {
		return findItemByCode (_sItemCode, _sCustomerID, _sLocationID, _iTransGroup, new Date());
	}
	
	public static Item findItemByCode (String _sItemCode, 
									   String _sCustomerID, 
									   String _sLocationID,
									   int _iTransGroup,
									   Date _dTransDate)
	throws Exception
	{
		return ItemManager.getInstance().
			findItemByCode (_sItemCode, _sCustomerID, _sLocationID, _iTransGroup, _dTransDate);
	}	

	public static String getItemCodeByID(String _sID)
    	throws Exception
    {
		Item oItem = getItemByID (_sID);
		if (oItem != null) {
			return oItem.getItemCode();
		}
		return "";
	}

	public static String getBarcodeByID(String _sID)
	    throws Exception
    {
		Item oItem = getItemByID (_sID);
		if (oItem != null) {
			return oItem.getBarcode();
		}
		return "";
	}

	public static String getItemNameByID(String _sID)
    	throws Exception
    {
		Item oItem = getItemByID (_sID);
		if (oItem != null) {
			return oItem.getItemName();
		}
		return "";
	}
	
	public static String getItemDescriptionByID(String _sID)
    	throws Exception
    {
		Item oItem = getItemByID (_sID);
		if (oItem != null) {
			return oItem.getDescription();
		}
		return "";
	}
	
	public static int getItemTypeByID(String _sID)
    	throws Exception
    {
		return getItemTypeByID(_sID, null);
	}

	public static int getItemTypeByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Item oItem = getItemByID (_sID, _oConn);
		if (oItem != null) {
			return oItem.getItemType();
		}
		return -1;
	}	
	
	public static BigDecimal getPriceExclTax(String _sID, Connection _oConn)
    	throws Exception
    {
		Item oItem = getItemByID (_sID, _oConn);
		if (oItem != null) 
		{
			if (PreferenceTool.getSalesInclusiveTax())
			{
				if(oItem.getTax().getAmount().doubleValue() > 0)
				{
					double dTaxRate = oItem.getTax().getAmount().doubleValue();
					double dPrice = oItem.getItemPrice().doubleValue();
					dPrice = dPrice / ((100 + dTaxRate)/100);
					return new BigDecimal (dPrice);
				}
				return oItem.getItemPrice();
			}
			else
			{
				return oItem.getItemPrice();
			}
		}
		return bd_ZERO;
	}	

	public static BigDecimal getPrice(String _sItemID, String _sLocID, String _sCustID, Date _dTxDate)
	{
		try 
		{
			Item oItem = getItemByID(_sItemID);
			if(oItem != null && StringUtil.isNotEmpty(_sLocID) || StringUtil.isNotEmpty(_sCustID))
			{
				String sCustTypeID = "";
				if(StringUtil.isNotEmpty(_sCustID))
				{
					Customer oCust = CustomerTool.getCustomerByID(_sCustID);
					if(oCust != null) sCustTypeID = oCust.getCustomerTypeId(); 
				}
				if(_dTxDate == null) _dTxDate = new Date();
				PriceListDetail oPLD = PriceListTool.getDetail(_sCustID, sCustTypeID, _sLocID, _sItemID, _dTxDate);
				if(oPLD != null)
				{
					return oPLD.getNewPrice();
				}
			}
			return oItem.getItemPrice();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return bd_ZERO;
	}
		
		
	public static List compareChangesOnStore(List _vItems)
	    throws Exception
	{
   		List vItems = new ArrayList(_vItems); 
        Iterator oIter = vItems.iterator();		
        Item oItem = null;
        Item oItemTemp = null;
        
        while(oIter.hasNext())
        {
            oItem = (Item) oIter.next();   
            oItemTemp = getItemByID(oItem.getItemId());
            if( oItemTemp != null && 
                //(oItem.getItemName().equals(oItemTemp.getItemName()) &&
                 oItem.getItemPrice().doubleValue() == oItemTemp.getItemPrice().doubleValue())
                // && oItem.getItemCode().equals(oItemTemp.getItemCode())))
            {
            	oIter.remove();
            }
        }        
        return vItems;        
    }

	public static List getItemByKategoriID(String _sKatID)
    	throws Exception
    {
		return ItemManager.getInstance().getItemByKategoriID(_sKatID);    	
	}
	
	public static List getItemByKategoriID(List _vKatID)
    	throws Exception
    {
        return getItemByKategoriID(_vKatID, null, null, 0);
	}

	public static List getItemByKategoriID(List _vKatID, String _sStartDate, String _sEndDate, int _iOrderBy)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
        oCrit.addIn(ItemPeer.KATEGORI_ID, _vKatID);
        if(_sStartDate != null || _sEndDate != null)
        {
            Date dStartDate = CustomParser.parseDate(_sStartDate);
	    	Date dEndDate = CustomParser.parseDate(_sEndDate);
            if(_sStartDate.equals("")||_sEndDate.equals(""))
            {
                dStartDate = new Date();
	    	    dEndDate = new Date();
            }
       		oCrit.add(ItemPeer.ADD_DATE, DateUtil.getStartOfDayDate(dStartDate), Criteria.GREATER_EQUAL);   
		    oCrit.and(ItemPeer.ADD_DATE, DateUtil.getEndOfDayDate(dEndDate), Criteria.LESS_EQUAL);   
		    //define sorting criteria
	        if (_iOrderBy == 1) {
	    	    oCrit.addAscendingOrderByColumn(ItemPeer.ADD_DATE);
	        }
		    else if (_iOrderBy == 2) {
       		    oCrit.addJoin(ItemPeer.PREFERED_VENDOR_ID,VendorPeer.VENDOR_ID);
        	    oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		    }
	        oCrit.add(ItemPeer.ITEM_STATUS, true);
        }
        log.debug(oCrit);
		return ItemPeer.doSelect(oCrit);
	}	
	
	public static List getItemByKategoriID(String _sKatID, String _sStartDate, String _sEndDate, int _iOrderBy)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.KATEGORI_ID, _sKatID);
        if(_sStartDate != null || _sEndDate != null)
        {
            Date dStartDate = CustomParser.parseDate(_sStartDate);
	    	Date dEndDate = CustomParser.parseDate(_sEndDate);
            if ( (_sStartDate == null || _sStartDate.equals("")) || (_sEndDate == null || _sEndDate.equals("")))
            {
                dStartDate = new Date();
	    	    dEndDate = new Date();
            }
       		oCrit.add(ItemPeer.ADD_DATE, DateUtil.getStartOfDayDate(dStartDate), Criteria.GREATER_EQUAL);   
		    oCrit.and(ItemPeer.ADD_DATE, DateUtil.getEndOfDayDate(dEndDate), Criteria.LESS_EQUAL);   
   		    //define sorting criteria
	        if (_iOrderBy == 1) 
	        {
	    	    oCrit.addAscendingOrderByColumn(ItemPeer.ADD_DATE);
	        }
		    else if (_iOrderBy == 2) 
		    {
       		    oCrit.addJoin(ItemPeer.PREFERED_VENDOR_ID,VendorPeer.VENDOR_ID);
        	    oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		    }
        }
        log.debug(oCrit);
		return ItemPeer.doSelect(oCrit);
	}
	
    public static List getItemByStatusAndKategoriID(List _vKatID,boolean _bStatus,int _iSortBy)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(ItemPeer.ITEM_STATUS, _bStatus);
        oCrit.addIn(ItemPeer.KATEGORI_ID, _vKatID);
        
   	    if (_iSortBy == 1 || _iSortBy == 4) {
	    	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}
		else if (_iSortBy == 3) {
			oCrit.addAscendingOrderByColumn(ItemPeer.DESCRIPTION);
		}
		log.debug (oCrit);
        return ItemPeer.doSelect(oCrit);
    }

    public static List getItemByStatusAndKategoriID(String  _sKatID,boolean _bStatus,int _iSortBy)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.add(ItemPeer.ITEM_STATUS, _bStatus);
    	if (StringUtil.isNotEmpty(_sKatID))
    	{
    		oCrit.add(ItemPeer.KATEGORI_ID, _sKatID);
    	}
        if (_iSortBy == 1 || _iSortBy == 4) {
	    	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}
		else if (_iSortBy == 3) {
			oCrit.addAscendingOrderByColumn(ItemPeer.DESCRIPTION);
		}
		log.debug (oCrit);
        return ItemPeer.doSelect(oCrit);
    }

    //-------------------------------------------------------------------------
    // end by kat
    //-------------------------------------------------------------------------

	public static List getItemByBrandID(String _sBrandID, boolean _bDisplayStore)
    	throws Exception
    {
		return ItemManager.getInstance().getItemByBrandID(_sBrandID,_bDisplayStore,null);    	
	}

	public static List getItemByTag(String _sTag, boolean _bDisplayStore)
    	throws Exception
    {
		return ItemManager.getInstance().getItemByTags(_sTag,_bDisplayStore,null);    	
	}

	public static List getItemByPreferedVendorID(String _sVendorID)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.PREFERED_VENDOR_ID, _sVendorID);
		return ItemPeer.doSelect(oCrit);
	}	

    public static List getItemByName(String _sItemName)
    	throws Exception
    {
        return getItemByName(_sItemName,false,true);
    }
    
	public static List getItemByName(String _sItemName,boolean _bSuffix, boolean _bPreffix)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	_sItemName = StringUtil.cleanForJS(_sItemName);
        oCrit.add(ItemPeer.ITEM_NAME, 
        	(Object) SqlUtil.like (ItemPeer.ITEM_NAME, _sItemName, _bSuffix, _bPreffix), Criteria.CUSTOM );
        oCrit.add(ItemPeer.ITEM_STATUS,b_ACTIVE_ITEM);
        oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		return ItemPeer.doSelect(oCrit);
	}
	
	/**
	 * @param _sItemDescription get item description from input by user
	 * @param _bSuffix if true then giving % to SQL query syntax on suffix
	 * @param _bPreffix if true then giving % to SQL query syntax on prefix
	 * @return	list of item that the description same as _sItemDescription
	 * @throws Exception
	 */
	public static List getItemByDescription(String _sItemDescription, boolean _bSuffix, boolean _bPreffix)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	_sItemDescription = StringUtil.cleanForJS(_sItemDescription);
        oCrit.add(ItemPeer.DESCRIPTION, (Object) SqlUtil.like 
        	(ItemPeer.DESCRIPTION, _sItemDescription, _bSuffix, _bPreffix), Criteria.CUSTOM );
        oCrit.add(ItemPeer.ITEM_STATUS,b_ACTIVE_ITEM);
        oCrit.addAscendingOrderByColumn(ItemPeer.DESCRIPTION);
		return ItemPeer.doSelect(oCrit);
	}

	public static List getItemListByStatus(boolean _bStatus)
    	throws Exception
    {
        return getItemListByStatus(_bStatus,1);
    }
    
	public static List getItemListByStatus(boolean _bStatus,int iOrderBy)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.ITEM_STATUS, _bStatus);	
        if(iOrderBy == 1)
        {
            oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
        }
        if(iOrderBy == 2)
        {
            oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
        }
        
		return ItemPeer.doSelect(oCrit);
	}

	public static Item getItemByDescription(String _sDesc)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.ITEM_CODE, 
        	(Object) SqlUtil.like (ItemPeer.DESCRIPTION, _sDesc, false, true), Criteria.CUSTOM );	
        oCrit.setLimit(1);			
		List vData = ItemPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (Item) vData.get(0);
		}
		return null;		
	}

	public static Item getItemBySKU(String _sItemSKU)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.ITEM_SKU, _sItemSKU);	
		oCrit.setLimit(1);				
		List vData = ItemPeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (Item) vData.get(0);
		}
		return null;
	}

	public static Item getItemByCode(String _sItemCode)
    	throws Exception
    {
		return ItemManager.getInstance().getItemByCode(_sItemCode, null);	
	}

	public static Item getItemByCode(String _sItemCode, Connection _oConn)
		throws Exception
	{
		return ItemManager.getInstance().getItemByCode(_sItemCode, _oConn);	
	}

	public static Item getItemByBarcode(String _sBarcode, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemPeer.BARCODE, _sBarcode);
		List vData = ItemPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) 
		{
			Item oItem = (Item) vData.get(0);
			return oItem;					
		}
		return null;
	}	
		
	public static String getItemType(int _iItemType)
	{
		if (_iItemType == 2) return "Non Inventory" ;
		if (_iItemType == 3) return "Grouping" 	 	;
		if (_iItemType == 4) return "Service" 	    ;
		if (_iItemType == 5) return "Asset" 	    ;		
		return "Inventory" 	;
	}

	public static int getItemType(String _sItemType)
	{
		if (_sItemType.equals ("Inventory"     )) return 1;
		if (_sItemType.equals ("Non Inventory" )) return 2;
		if (_sItemType.equals ("Grouping" 	   )) return 3;
		if (_sItemType.equals ("Service" 	   )) return 4;
		if (_sItemType.equals ("Asset") || 
			_sItemType.equals ("Assets"        )) return 5;
		return 1; 
	}

	//TODO : define formula for ideal amount to purchase
	//for TR, min same as reorder point, so when QoH same as min, reorder Qty 
	//will calculate using reorder point formula
	public static double getInventoryAmountToPurchase(String _sID, double _iInventoryOnHand)
    	throws Exception
    {
		Item oItem = getItemByID(_sID);
		/*
		double iMaxQty = oItem.getMaximumStock().doubleValue();
		if (_iInventoryOnHand > iMaxQty)  return 0;
		*/
		double iMinQty = oItem.getMinimumStock().doubleValue();
	    if (_iInventoryOnHand > iMinQty)  return 0;	
		double iAmount =  iMinQty - _iInventoryOnHand;
		return iAmount;
	}

	//-------------------------------------------------------------------------
	// code generator
	//-------------------------------------------------------------------------
	
	private static final String s_CODE_SEPARATOR = 
		CONFIG.getString("generator.itemcode.separator","");
	
	private static final int i_AUTONUMBER_LENGTH = PreferenceTool.getItemCodeLength();
	
	private static final int i_SUFFIX_LENGTH = 
		CONFIG.getInt("generator.itemcode.suffix.length");
    
	/**
     * get last number by kategori code
     * 
     * @param _sKategoriCode
     * @return last number
     * @throws Exception
     */
    public static String getSequenceNumber (String _sKategoriCode)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.ITEM_CODE, (Object)(_sKategoriCode + s_CODE_SEPARATOR + "%"), Criteria.ILIKE);
        oCrit.addDescendingOrderByColumn(ItemPeer.ITEM_CODE);
		oCrit.setLimit(1);
		
		List vData = ItemPeer.doSelect(oCrit);
		if (vData.size() > 0) 
		{
			String sLastNumber =  "";
			try
			{
				Item oItem =  (Item) vData.get(0);
				String sLastItemCode = oItem.getItemCode();
				String sKategoriCodeWithSeparator = _sKategoriCode + s_CODE_SEPARATOR;
				String sManualCode = sLastItemCode.substring (sKategoriCodeWithSeparator.length());
	
				log.debug ("Last Item Code : " + sLastItemCode);
				log.debug ("KategoriCodeWithSeparator : " + sKategoriCodeWithSeparator );
				log.debug ("sManualLastCode : " + sManualCode );
	
				int iNumberOffset = sManualCode.length() -  i_SUFFIX_LENGTH;
				
				//check if the last number has no suffix 			
				if ( sManualCode.length() == i_AUTONUMBER_LENGTH )
				{
					iNumberOffset = sManualCode.length();
				}
				sLastNumber =  sManualCode.substring(0, iNumberOffset);
				log.debug ("sLastNumber : " + sLastNumber);

				int iLastNumber = Integer.parseInt(sLastNumber);
				int iNewNumber = iLastNumber + 1;
				return  Integer.toString(iNewNumber);
			}
			catch (Exception _oEx)
			{
				log.error("Invalid Last Item Code " + sLastNumber);
			}
		}
		return "1";
	}
    
    //TODO: refactor
    public static String generateItemCode (String _sKategoriID, String _sSuffix)
    	throws Exception
    {
		Kategori oKategori = KategoriTool.getKategoriByID(_sKategoriID);
		
		String sKategoriCode = KategoriTool.getItemCodeByCategory(oKategori);
		// get the last auto increment item code for this kategori code
		
		String sAutoNumber = getSequenceNumber(sKategoriCode);	
		
		StringBuilder sNewCode = new StringBuilder();
		sNewCode.append(sKategoriCode);
		sNewCode.append(s_CODE_SEPARATOR);
		sNewCode.append(StringUtil.formatNumberString(sAutoNumber, i_AUTONUMBER_LENGTH)); 
		sNewCode.append(_sSuffix);
		
		return sNewCode.toString(); 
	}	

	public static List getItemFromIDToID(String _sFromID, String _sToID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemPeer.ITEM_ID, (Object)_sFromID, Criteria.GREATER_EQUAL);
        oCrit.and(ItemPeer.ITEM_ID, (Object)_sToID, Criteria.LESS_EQUAL);
		return ItemPeer.doSelect(oCrit);
	}

	protected static Criteria transDateCriteria (Criteria _oCrit, Date _dStart, Date _dEnd)
	{
        if (_oCrit == null) _oCrit = new Criteria();
        
        if (_dStart != null)        	
        	_oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, 
        		DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);        	
        	
        if (_dEnd != null)
            _oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, 
            	DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        
        return _oCrit;
	}
	
	public static LargeSelect findData( int _iCondition, 
									    String _sKeywords, 
									    String _sKatID, 
									    int _iSortBy, 
									    int _iViewLimit, 
									    String _sGroupID, 
									    String _sVendorID,
									    String _sLocationID,
									    int _iStockStatus,
									    int _iItemStatus,
									    int _iStatusInLocation,
										int _iTransactionGroup)
    	throws Exception
    {
        return findData( _iCondition, _sKeywords, _sKatID, _iSortBy, _iViewLimit, _sGroupID, 
                         _sVendorID,_sLocationID, "", "", _iStockStatus,_iItemStatus,_iStatusInLocation,false,-1,-1,-1,_iTransactionGroup,-1);
    }    
    public static Criteria createFindCriteria(int _iCondition, 
                                              String _sKeywords,
                                              int _iSortBy,
                                              String _sKatID, 
                                              String _sVendorID)
        throws Exception
    {
        return createFindCriteria(_iCondition,_sKeywords,_iSortBy,_sKatID,_sVendorID,"");
    }

    public static Criteria createFindCriteria(int _iCondition, 
    									      String _sKeywords,
    									      int _iSortBy,
    									      String _sKatID, 
    									      String _sVendorID,
    									      String _sLocationID)
    	throws Exception
    {
    	return createFindCriteria(_iCondition,_sKeywords,_iSortBy,_sKatID,_sVendorID,_sLocationID,"","");
    }
    
	public static Criteria createFindCriteria(int _iCondition, 
											  String _sKeywords,
											  int _iSortBy,
											  String _sKatID, 
											  String _sVendorID,
                                              String _sLocationID,
                                              String _sBrandID,
                                              String _sPrincipalID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		//search by keywords
		if (StringUtil.isNotEmpty(_sKeywords)) 
		{
			oCrit.setIgnoreCase(true);
			
			if (_iCondition == 1) {
				oCrit.add(ItemPeer.ITEM_CODE, (Object)_sKeywords, Criteria.ILIKE);
			} 
			else if (_iCondition == 2) {
				oCrit.add(ItemPeer.ITEM_NAME, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 3) {
				oCrit.add(ItemPeer.DESCRIPTION, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 4) {
				oCrit.add(ItemPeer.ITEM_SKU, (Object)_sKeywords, Criteria.ILIKE);
			}						
			else if (_iCondition == 5) {
				List vVendor = VendorTool.getVendorByName(_sKeywords, false, false);
				if (vVendor.size() == 1) //find 
				{
					Vendor oVendor = (Vendor) vVendor.get(0);
					_sVendorID = oVendor.getVendorId(); 
				}
				else if (vVendor.size() > 1)
				{					
					List vItemID = ItemVendorTool.getItemIDByVendor(vVendor);
					oCrit.addIn (ItemPeer.ITEM_ID, vItemID);
					Criteria.Criterion oItemVendor = oCrit.getCriterion(ItemPeer.ITEM_ID);
					oItemVendor.or(oCrit.getNewCriterion(ItemPeer.PREFERED_VENDOR_ID, _sVendorID,Criteria.EQUAL));
				}
				else if(vVendor.size() == 0)
				{
					_sVendorID = "";
					oCrit.add(ItemPeer.PREFERED_VENDOR_ID, "-");
				}
			}
			else if (_iCondition == 6) {
				oCrit.add(ItemStatusCodePeer.STATUS_CODE, (Object)_sKeywords, Criteria.ILIKE);
				oCrit.addJoin(ItemPeer.ITEM_STATUS_CODE_ID,ItemStatusCodePeer.ITEM_STATUS_CODE_ID);
			}
			else if (_iCondition == 7) {
				oCrit.add(ItemPeer.BARCODE, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 8) {
				oCrit.add(ItemPeer.COLOR, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 9) {
				oCrit.add(ItemPeer.SIZE, (Object)_sKeywords, Criteria.ILIKE);
			}
			else if (_iCondition == 10) {
				String[] aDate = StringUtil.parseNameValue(_sKeywords);
				Date dFrom = CustomParser.parseDate(aDate[0]);
				Date dTo = CustomParser.parseDate(aDate[1]);
				if (dFrom != null) oCrit.add(ItemPeer.ADD_DATE, dFrom, Criteria.GREATER_EQUAL);
				if (dTo != null) oCrit.and(ItemPeer.ADD_DATE, DateUtil.getEndOfDayDate(dTo), Criteria.LESS_EQUAL);				
			}
			else if (_iCondition == 11) {
				String[] aDate = StringUtil.parseNameValue(_sKeywords);
				Date dFrom = CustomParser.parseDate(aDate[0]);
				Date dTo = CustomParser.parseDate(aDate[1]);
				if (dFrom != null) oCrit.add(ItemPeer.UPDATE_DATE, dFrom, Criteria.GREATER_EQUAL);
				if (dTo != null) oCrit.and(ItemPeer.UPDATE_DATE, DateUtil.getEndOfDayDate(dTo), Criteria.LESS_EQUAL);				
			}
			else if (_iCondition == 12) 
			{
				oCrit.addIn(ItemPeer.ITEM_CODE, StringUtil.toList(_sKeywords, ","));
			}
            else if (_iCondition == 13) 
            {
                oCrit.add(ItemPeer.RACK_LOCATION, (Object)_sKeywords, Criteria.ILIKE);
            }            
            else if (_iCondition == 14) 
            {
                oCrit.addJoin(ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
                oCrit.add(ItemInventoryPeer.RACK_LOCATION, (Object)_sKeywords, Criteria.ILIKE);
                oCrit.add(ItemInventoryPeer.LOCATION_ID, _sLocationID);                
            }           
            else if (_iCondition == 15) 
            {
            	List vManuf = ItemFieldTool.getAllPrincipal();            	
            	if(vManuf.size() > 0)
            	{
            		oCrit.addJoin(ItemPeer.MANUFACTURER, PrincipalPeer.PRINCIPAL_ID);
            		oCrit.add(PrincipalPeer.PRINCIPAL_NAME, (Object)_sKeywords, Criteria.ILIKE);
            	}
            	else
            	{
            		oCrit.add(ItemPeer.MANUFACTURER, (Object)_sKeywords, Criteria.ILIKE);
            	}
            } 
            else if (_iCondition == 16) 
            {            	
            	if(!_sKeywords.contains("%")) _sKeywords = "%" + _sKeywords + "%";
            	oCrit.add(ItemPeer.TAGS, (Object)_sKeywords, Criteria.ILIKE);
            } 
            else if (_iCondition == 17) 
            {
            	List vBrand = ItemFieldTool.getAllBrand();
            	if(vBrand.size() > 0)
            	{
            		oCrit.addJoin(ItemPeer.BRAND, BrandPeer.BRAND_ID);
            		oCrit.add(BrandPeer.BRAND_NAME, (Object)_sKeywords, Criteria.ILIKE);            		
            	}
            	else
            	{
            		oCrit.add(ItemPeer.BRAND, (Object)_sKeywords, Criteria.ILIKE);            		
            	}            	            	
            } 						
            else if (_iCondition == 98)
            {
            	oCrit.add(ItemPeer.ITEM_PICTURE, (Object)_sKeywords, Criteria.ILIKE);
            }
			else if (_iCondition == 99) 
			{ //item field
				List vData = StringUtil.toList(_sKeywords, ",");
				System.out.println("vData "  + vData  + vData.size());
				String sField = "";
				String sValue = "";
				if (vData.size() == 2) 
				{
					sField = (String) vData.get(0);
					sValue = (String) vData.get(1);
				}
				oCrit.addJoin(ItemPeer.ITEM_ID, ItemFieldPeer.ITEM_ID);
				oCrit.add(ItemFieldPeer.FIELD_NAME, sField);
				oCrit.add(ItemFieldPeer.FIELD_VALUE, (Object)sValue, Criteria.ILIKE);
			}
		}
		
		//define kategori criteria
		if (StringUtil.isNotEmpty(_sKatID))
		{
			List vID = KategoriTool.getChildIDList(_sKatID);
			if (vID.size()  > 0) {
				vID.add(_sKatID);
				oCrit.addIn(ItemPeer.KATEGORI_ID, vID);
			}
			else {
				oCrit.add(ItemPeer.KATEGORI_ID, _sKatID);
			}
		}
		
		if (StringUtil.isNotEmpty(_sBrandID))
		{
			Brand oBrand = ItemManager.getInstance().getBrandByID(_sBrandID, null);
			if(oBrand != null) oCrit.add(ItemPeer.BRAND, oBrand.getName());
		}
		
		if (StringUtil.isNotEmpty(_sPrincipalID))
		{
			Principal oPrincipal = ItemManager.getInstance().getPrincipalByID(_sPrincipalID, null);
			if(oPrincipal != null) oCrit.add(ItemPeer.MANUFACTURER, oPrincipal.getName());
		}		
		
	    //define sorting criteria
	    if (_iSortBy == 1) {
	    	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}
		else if (_iSortBy == 3) {
			oCrit.addAscendingOrderByColumn(ItemPeer.DESCRIPTION);
		}
		else if (_iSortBy == 4) {
			oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_SKU);
		}
		else if (_iSortBy == 5) {
			oCrit.addJoin (ItemPeer.PREFERED_VENDOR_ID, VendorPeer.VENDOR_ID);
			oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		}		
		else if (_iSortBy == 6) {
			oCrit.addJoin (ItemPeer.ITEM_STATUS_CODE_ID, ItemStatusCodePeer.ITEM_STATUS_CODE_ID);
			oCrit.addAscendingOrderByColumn(ItemStatusCodePeer.ITEM_STATUS_CODE_ID);
		}		
		else if (_iSortBy == 7) {
			oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_PRICE);
		}
		else if (_iSortBy == 8) {
			oCrit.addAscendingOrderByColumn(ItemPeer.COLOR);
		}	    
		else if (_iSortBy == 9) {
			oCrit.addAscendingOrderByColumn(ItemPeer.SIZE);
		}	    
		else if (_iSortBy == 10) {
			oCrit.addAscendingOrderByColumn(ItemPeer.ADD_DATE);
		}	    
		else if (_iSortBy == 11) {
			oCrit.addAscendingOrderByColumn(ItemPeer.UPDATE_DATE);
		}	 
		else if (_iSortBy == 13) {
			oCrit.addAscendingOrderByColumn(ItemPeer.RACK_LOCATION);
		}	 
		else if (_iSortBy == 14) {
            oCrit.addJoin(ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
            oCrit.add(ItemInventoryPeer.LOCATION_ID, _sLocationID);                			
			oCrit.addAscendingOrderByColumn(ItemInventoryPeer.RACK_LOCATION);
			oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}	 
	    
	    if (StringUtil.isNotEmpty(_sVendorID)) 
	    {
	    	List vItemID = ItemVendorTool.getItemIDByVendor(_sVendorID);
	    	if(vItemID.size() > 0) //from Vendor Item
	    	{
	    	    oCrit.addIn (ItemPeer.ITEM_ID,vItemID);
	    	    Criteria.Criterion oItemVendor = oCrit.getCriterion(ItemPeer.ITEM_ID);
	    	    oItemVendor.or(oCrit.getNewCriterion(ItemPeer.PREFERED_VENDOR_ID, _sVendorID,Criteria.EQUAL));
	    	 }
	    	 else //from Vendor Price List
	    	 {
	 	    	List vVPLItemID = VendorPriceListTool.getListOfItemInPriceList(_sVendorID);
		    	if(vVPLItemID.size() > 0)
		    	{
		    	    oCrit.addIn (ItemPeer.ITEM_ID, vVPLItemID);
		    	    Criteria.Criterion oItemPrefVend = oCrit.getCriterion(ItemPeer.ITEM_ID);
		    	    oItemPrefVend.or(oCrit.getNewCriterion(ItemPeer.PREFERED_VENDOR_ID, _sVendorID,Criteria.EQUAL));
		    	 }
		    	 else
		    	 {
		    	    oCrit.add(ItemPeer.PREFERED_VENDOR_ID, _sVendorID);
		    	 }
	    	 }
	    }	    
		return oCrit;
	}

	/**
	 * 
	 * @param _iCondition
	 * @param _sKeywords
	 * @param _sKatID
	 * @param _iSortBy
	 * @param _iViewLimit
	 * @param _sGroupID
	 * @param _sVendorID
	 * @param _sLocationID
	 * @param _iStockStatus
	 * @param _iItemStatus
	 * @param _iStatusInLocation
	 * @param _bGroupBySKU
	 * @param _iStockToView
	 * @param _iStockToViewEnd
	 * @param _iUsedBarcode
	 * @param _iTransactionGroup
	 * @return Large Select query result
	 * @throws Exception
	 */
	public static LargeSelect findData( int _iCondition, 
									    String _sKeywords, 
									    String _sKatID, 
									    int _iSortBy, 
									    int _iViewLimit, 
									    String _sGroupID, 
									    String _sVendorID,
									    String _sLocationID,
									    String _sBrandID,
									    String _sPrincipalID,
									    int _iStockStatus,
									    int _iItemStatus,
									    int _iStatusInLocation,
									    boolean _bGroupBySKU,
									    int _iStockToView,
									    int _iStockToViewEnd,
										int _iUsedBarcode,
										int _iTransactionGroup,
										int _iItemType)
    	throws Exception
    {
		Criteria oCrit = createFindCriteria(_iCondition, _sKeywords, _iSortBy, _sKatID, _sVendorID, _sLocationID, _sBrandID, _sPrincipalID);
	    
	    //if searching for item to be added to group do not include itself
	    if (StringUtil.isNotEmpty(_sGroupID)) {
	    	oCrit.add(ItemPeer.ITEM_ID, (Object) _sGroupID, Criteria.NOT_EQUAL);
	    }

	    String sQtyCol = "";
	    if (_iStockStatus > 0)
	    {	    	
	    	if(StringUtil.isNotEmpty(_sLocationID))
	    	{
	    		oCrit.addJoin (ItemPeer.ITEM_ID, InventoryLocationPeer.ITEM_ID);
	    		oCrit.add (InventoryLocationPeer.LOCATION_ID, _sLocationID);
	    		sQtyCol = InventoryLocationPeer.CURRENT_QTY;
	    	}
	    	else
	    	{
	    		oCrit.addJoin (ItemPeer.ITEM_ID, "item_allloc_qty.item_id");	    	
	    		sQtyCol = "item_allloc_qty.total_qty";
	    	}
	    }
	    
	    if (_iStockStatus == 1) 
	    {	        
			oCrit.addJoin (ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
	    	oCrit.add (ItemInventoryPeer.LOCATION_ID, _sLocationID);			
			oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery (sQtyCol, 
			ItemInventoryPeer.MINIMUM_QTY, "<=") , Criteria.CUSTOM);
	    }
	    if (_iStockStatus == 2) 
	    {	    	
			oCrit.addJoin (ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
	    	oCrit.add (ItemInventoryPeer.LOCATION_ID, _sLocationID);
			oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery (sQtyCol, 
			ItemInventoryPeer.REORDER_POINT, "<=") , Criteria.CUSTOM);
	    }
	    if (_iStockStatus == 3) 
	    {
			oCrit.add (sQtyCol, Integer.valueOf(0), Criteria.LESS_THAN); 
	    }
	    if (_iStockStatus == 4) 
	    {
	    	oCrit.add (sQtyCol, Integer.valueOf(0), Criteria.GREATER_THAN);
	    }
   	    if (_iStockStatus == 5) 
   	    {
   	    	oCrit.add (ItemInventoryPeer.LOCATION_ID, _sLocationID);
			oCrit.addJoin (ItemInventoryPeer.ITEM_ID,ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery (sQtyCol, 
			ItemInventoryPeer.MAXIMUM_QTY, ">=") , Criteria.CUSTOM);
	    }
   	    if (_iStockStatus == 6) 
   	    {
   	    	oCrit.add (sQtyCol, _iStockToView, Criteria.GREATER_THAN);
	    }
   	    if (_iStockStatus == 7) 
   	    {
   	    	oCrit.add (sQtyCol, _iStockToView, Criteria.LESS_EQUAL);
	    }
   	    if (_iStockStatus == 8) 
   	    {
   	    	oCrit.add (sQtyCol, _iStockToView, Criteria.LESS_THAN);
	    }
   	    if (_iStockStatus == 9) 
   	    {
   	    	oCrit.add (sQtyCol, _iStockToView, Criteria.EQUAL);
	    }
  	    if (_iStockStatus == 10) {
			oCrit.add (sQtyCol, _iStockToView, Criteria.GREATER_EQUAL);
			oCrit.and (sQtyCol, _iStockToViewEnd, Criteria.LESS_EQUAL);
	    }
  	    if (_iStockStatus == 11) 
  	    { //item exist in inventory location
  	    	if(StringUtil.isNotEmpty(_sLocationID))
  	    	{
  	    		oCrit.add (ItemPeer.ITEM_ID, (Object)"item.item_id IN (SELECT il.item_id FROM inventory_location il)", Criteria.CUSTOM);
  	    	}
	    }
        if (_iStockStatus == 12) 
        {
        	oCrit.add (sQtyCol, Integer.valueOf(0));
        }
        if (_iStockStatus == 13) 
        {
        	oCrit.add (sQtyCol, Integer.valueOf(0), Criteria.GREATER_EQUAL);            
        }
  	    if (_iStockStatus == 14) 
  	    { //item not exist in inventory location  	      	    	
  	    	oCrit.add (ItemPeer.ITEM_ID, (Object)"item.item_id NOT IN (SELECT il.item_id FROM inventory_location il)", Criteria.CUSTOM);			
  	    }  	    
  	    //end stock status
   	    if (_iItemStatus == 1) {
	    	oCrit.add(ItemPeer.ITEM_STATUS, b_ACTIVE_ITEM);	    	
	    }
   	    if (_iItemStatus == 2) {
	    	oCrit.add(ItemPeer.ITEM_STATUS, b_NONACTIVE_ITEM);		
	    }
   	    if (_iUsedBarcode == 1) 
   	    {
   	    	oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery 
   	    		(ItemPeer.BARCODE, ItemPeer.ITEM_CODE, "<>"), Criteria.CUSTOM);
   	    }
   	    if (_iUsedBarcode == 2) 
   	    {
   	    	oCrit.add(ItemPeer.ITEM_ID,(Object) SqlUtil.buildCompareQuery 
   	    		(ItemPeer.BARCODE, ItemPeer.ITEM_CODE, "="), Criteria.CUSTOM);
   	    }
	    if (_iStatusInLocation != -1)
	    {
	        oCrit.addJoin(ItemPeer.ITEM_ID,ItemInventoryPeer.ITEM_ID);
	        oCrit.add(ItemInventoryPeer.LOCATION_ID,_sLocationID);
	        if(_iStatusInLocation == 0)
	        {
	        	oCrit.add(ItemInventoryPeer.ACTIVE,false);
	        }
	        if(_iStatusInLocation == 1)
	        {
	        	oCrit.add(ItemInventoryPeer.ACTIVE,true);
	        }
	    }
	    if (_bGroupBySKU)
	    {
	        //oCrit.addGroupByColumn (ItemPeer.ITEM_SKU);
	    }
	    if(_iTransactionGroup != -1)
	    {
	    	int[] aItemType = getItemTypeAvaibility(_iTransactionGroup);
	    	oCrit.addIn (ItemPeer.ITEM_TYPE,aItemType);
	    }
	    if(_iItemType > 0)
	    {
	    	oCrit.add (ItemPeer.ITEM_TYPE, _iItemType);
	    }
	    
	    log.debug (oCrit);
		return new LargeSelect(oCrit, _iViewLimit, "com.ssti.enterprise.pos.om.ItemPeer");
	}
	
	public static int[] getItemTypeAvaibility (int _iTransactionGroup)
	{
		int[] aItemType = {i_INVENTORY_PART,i_NON_INVENTORY_PART,i_SERVICE,i_GROUPING,i_ASSET};
		
		if(_iTransactionGroup == i_PURCHASE_TRANSACTION)
		{
			aItemType = new int[] {i_INVENTORY_PART,i_NON_INVENTORY_PART,i_GROUPING,i_ASSET};
		}
		else if (_iTransactionGroup == i_INVENTORY_TRANSACTION)
		{
			aItemType = new int[] {i_INVENTORY_PART};
		}
		return aItemType;
	}

	
    /**
     * method to look up item code by vendor id
     * used by PO, PR 
     * @param _sCode
     * @param _sVendorID
     * @return Item
     * @throws Exception
     */
    public static Item findPurchaseItemByCode (String _sCode, String _sVendorID, Date _dTransDate)
    	throws Exception
    {
		Item oItem = findItemByCode (_sCode, null, null, i_PURCHASE_TRANSACTION);
		if (oItem != null)
		{
			String sItemID = oItem.getItemId();
		    //TODO:check & testagain
			double dLastPurch = oItem.getLastPurchasePrice().doubleValue() * oItem.getUnitConversion().doubleValue();
			oItem.setItemPrice(new BigDecimal(dLastPurch));
		    oItem.setTaxId(oItem.getPurchaseTaxId());
		    
		    //validate Item Type && Vendor Item Type
		    Vendor oVendor = VendorTool.getVendorByID(_sVendorID);
		    if(oVendor != null)
		    {	
		    	if(StringUtil.isNotEmpty(oVendor.getDefaultItemDisc()))
		    	{
		    		oItem.setDiscountAmount(oVendor.getDefaultItemDisc());
		    	}
		    	if(!oVendor.isItemTypeAllowed(oItem.getItemType()))
		    	{
		    		return null;
		    	}
		    }		    
		}
		return oItem;
	}
		
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(ItemPeer.class, ItemPeer.ITEM_ID, _sID, _bIsNext);
	}
	
	//Utility
	public static double countMargin (Number _dPrice, Number _dCost)
		throws Exception
	{
		if (_dPrice != null && _dCost != null && _dCost.doubleValue() != 0)
		{
			return countMargin(_dPrice.doubleValue(), _dCost.doubleValue());
		}
		return 0;
	}

	public static double countMargin (double _dPrice, double _dCost)
		throws Exception
	{
		if(_dPrice != 0)
		{
			if(_dCost > 0)
			{
				double dMargin = ((_dPrice - _dCost) / _dPrice) * 100; 
				return dMargin;
			}
			return 100;
		}
		return 0;
	}	
	
	/////////////////////////////////////////////////////////////////////////////////////
	//synchronization methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static List getUpdatedItem (boolean _bActiveOnly)
		throws Exception
	{
		return getUpdatedItem(_bActiveOnly, null);
	}	
	
	public static List getUpdatedItem (boolean _bActiveOnly, Date _dLast)
		throws Exception
	{
		if (_dLast == null)
		{
			_dLast = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE);
		}		
		Criteria oCrit = new Criteria();
		if (_dLast  != null) 
		{
	    	oCrit.add(ItemPeer.UPDATE_DATE, _dLast, Criteria.GREATER_EQUAL);   
	    	if ( _bActiveOnly ) 
	    	{
	    		oCrit.add(ItemPeer.ITEM_STATUS, b_ACTIVE_ITEM);
			}
		}
		return ItemPeer.doSelect(oCrit);
	}	
	
	public static List getNewStoreItems ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO);
		Criteria oCrit = new Criteria();
		if (dLastSyncDate != null) 
		{
	    	oCrit.add(ItemPeer.UPDATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
		String sLocationCode = PreferenceTool.getLocationCode() + "%"; 
    	oCrit.add(ItemPeer.ITEM_ID, (Object) sLocationCode, Criteria.LIKE);
		return ItemPeer.doSelect(oCrit);
	}		
	
	/////////////////////////////////////////////////////////////////////////////////////
	//end sync methods
	/////////////////////////////////////////////////////////////////////////////////////
		
	public static List getAllUpdatedItem (String _sStartDate, 
	  									  String _sEndDate, 
										  int _iSortBy, 
										  String _sKatID)
		throws Exception
	{
		Date dStart = CustomParser.parseDate (_sStartDate);
		Date dEnd   = CustomParser.parseDate (_sEndDate);
		Criteria oCrit = new Criteria();
		if (dStart != null) {
        	oCrit.add(ItemPeer.UPDATE_DATE, dStart, Criteria.GREATER_EQUAL);   
		}
		if (dEnd != null) {
        	oCrit.and(ItemPeer.UPDATE_DATE, dEnd, Criteria.LESS_EQUAL);   
		}
	    //define sorting criteria
	    if (_iSortBy == 1) {
	    	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
	    }
		else if (_iSortBy == 2) {
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}
		else if (_iSortBy == 3) {
			oCrit.addAscendingOrderByColumn(ItemPeer.DESCRIPTION);
		}
		else if (_iSortBy == 4) {
			oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_PRICE);
		}
		//define kategori criteria
		if (_sKatID != null && !_sKatID.equals(""))
		{
			List vID = KategoriTool.getChildIDList(_sKatID);
	        if (vID.size()  > 0) {
		        vID.add(_sKatID);
		        oCrit.addIn(ItemPeer.KATEGORI_ID, vID);
	        }
	        else {
		        oCrit.add(ItemPeer.KATEGORI_ID, _sKatID);
	        }
	    }
		return ItemPeer.doSelect(oCrit);
	}		
	
	public static List getNewItem (String _sStartDate, String _sEndDate, boolean _bUpdateDate,
								   String _sVendorName,String _sKatID,int _iOrderBy)
		throws Exception
	{
		Date dStart = CustomParser.parseDate (_sStartDate);
		Date dEnd   = CustomParser.parseDate (_sEndDate);
		if(dStart == null || dEnd == null)
	    {
	    	dStart = new Date();
            dEnd = new Date();
	    }
		Criteria oCrit = new Criteria();
		if(_bUpdateDate)
		{
			oCrit.add(ItemPeer.UPDATE_DATE, DateUtil.getStartOfDayDate(dStart), Criteria.GREATER_EQUAL);   
			oCrit.and(ItemPeer.UPDATE_DATE, DateUtil.getEndOfDayDate(dEnd), Criteria.LESS_EQUAL);   		
		}
		else
		{
			oCrit.add(ItemPeer.ADD_DATE, DateUtil.getStartOfDayDate(dStart), Criteria.GREATER_EQUAL);   
			oCrit.and(ItemPeer.ADD_DATE, DateUtil.getEndOfDayDate(dEnd), Criteria.LESS_EQUAL);   
		}
		if (StringUtil.isNotEmpty(_sVendorName)) 
		{
			oCrit.addJoin(ItemPeer.PREFERED_VENDOR_ID,VendorPeer.VENDOR_ID);
			oCrit.add(VendorPeer.VENDOR_NAME,
		    	(Object) SqlUtil.like (VendorPeer.VENDOR_NAME, _sVendorName), Criteria.CUSTOM);
		}
		//define sorting criteria
	    if (_iOrderBy == 1) 
	    {
	    	oCrit.addAscendingOrderByColumn(ItemPeer.ADD_DATE);
	    }
		else if (_iOrderBy == 2) 
		{
        	//oCrit.addAscendingOrderByColumn(VendorPeer.VENDOR_NAME);
		}
		else if (_iOrderBy == 3) 
		{
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_CODE);
		}
		else if (_iOrderBy == 4) 
		{
        	oCrit.addAscendingOrderByColumn(ItemPeer.ITEM_NAME);
		}
	    
		//define kategori criteria
		if (_sKatID != null && !_sKatID.equals(""))
		{
			List vID = KategoriTool.getChildIDList(_sKatID);
	        if (vID.size()  > 0) 
	        {
		        vID.add(_sKatID);
		        oCrit.addIn(ItemPeer.KATEGORI_ID, vID);
	        }
	        else {
		        oCrit.add(ItemPeer.KATEGORI_ID, _sKatID);
	        }
	    }
	    log.debug(oCrit);
		return ItemPeer.doSelect(oCrit);
	}		
	
	/**
	 * Update last purchase price
	 * 
	 * @param _sTransNo
	 * @param _sCurrencyID
	 * @param _sItemID
	 * @param _sUserName
	 * @param _sUnitID
	 * @param _dLastPurchase
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateLastPurchase (String _sTransNo, 
										   String _sCurrencyID,
										   String _sItemID, 
										   String _sUserName,
										   String _sUnitID,
										   double _dLastPurchase, 
										   double _dRate,
										   Connection _oConn)
		throws Exception
	{		
        boolean bUpdate = true;
        boolean bUpdatePrice = false;
        try
        {
            bUpdate = Boolean.valueOf(PreferenceTool.getSysConfig().getPiUpdateLastPurchase());
            bUpdatePrice = Boolean.valueOf(PreferenceTool.getSysConfig().getPiUpdateSalesPrice());
        }
        catch (Exception _oEx)
        {
            log.error(_oEx);
        }
        if(bUpdate)
        {            
        	//LastPurchase  DATA FROM PI now already in BASE UNIT
    		/*        	
    		//update Item's Last Purchase Price
    		double dUnitConv = oItem.getUnitConversion().doubleValue();
    		Unit oBaseUnit = UnitTool.getUnitByID(oItem.getUnitId(), _oConn);
    		if (StringUtil.equals(oBaseUnit.getUnitId(),_sUnitID)) //purchase is in base unit
    		{
    			dUnitConv = 1;
    		}
    		_dLastPurchase = _dLastPurchase / dUnitConv; //last purchase always in base unit
    		*/
        	Item oItem = getItemByID(_sItemID, _oConn);
    		Item oOldItemRef = oItem.copy();
    		
    		//TODO: only update if price up, probably need another config
    		double dOldPurch = oItem.getLastPurchasePrice().doubleValue();
    		if(_dLastPurchase > 0 && _dLastPurchase > dOldPurch) 
    		{
	    		oItem.setLastPurchasePrice(new BigDecimal(_dLastPurchase));
	    		oItem.setLastPurchaseCurr(CurrencyTool.getCodeByID(_sCurrencyID));
	    		oItem.setLastPurchaseRate(new BigDecimal(_dRate));
    		}
    		
    		String sMargin = oItem.getProfitMargin();
            if (bUpdatePrice && StringUtil.isNotEmpty(sMargin))
            {
        		if (PreferenceTool.medicalInstalled())
        		{
        			//TODO: find way to remove link between POS and MEDICAL MODULES
        			//update price if margin in med config set        			
        			//oItem.setItemPrice(MedConfigTool.getPrice(oItem,false));
        		}
                else
                {
                    //TODO: add method to calculate price
                }
            }    		
    		oItem.setUpdateDate(new Date());	
    		oItem.save(_oConn);
    		
    	    //set the item update history
    		ItemManager.getInstance().refreshCache(oItem);
    		ItemHistoryTool.createItemHistory (oOldItemRef, oItem, _sUserName + " From " + _sTransNo, _oConn);
        }
    }
	
	/**
	 * delete an item
	 * 
	 * @param _sID
	 * @return boolean 
	 * @throws Exception
	 */
	public static boolean delete (String _sID)
		throws Exception
	{
		if (    SqlUtil.validateFKRef("price_list_detail","item_id",_sID) 
			 && SqlUtil.validateFKRef("issue_receipt_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("item_transfer_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("sales_order_detail","item_id",_sID)			 			 
			 && SqlUtil.validateFKRef("delivery_order_detail","item_id",_sID)			 
			 && SqlUtil.validateFKRef("sales_transaction_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("sales_return_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("purchase_request_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("purchase_order_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("purchase_receipt_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("purchase_invoice_detail","item_id",_sID)			 
			 && SqlUtil.validateFKRef("purchase_return_detail","item_id",_sID)
			 && SqlUtil.validateFKRef("inventory_transaction","item_id",_sID) )
		{
		    // SHOULD ALSO BE DELETED
            // item_field
			// item_group
			// inventory_location
			// inventory_transaction
			
			ItemFieldTool.deleteByItemID(_sID);

    		Criteria oCrit = new Criteria();
        	oCrit.add(ItemGroupPeer.ITEM_ID, _sID);
        	ItemGroupPeer.doDelete(oCrit);

    		oCrit = new Criteria();
        	oCrit.add(InventoryTransactionPeer.ITEM_ID, _sID);
        	InventoryTransactionPeer.doDelete(oCrit);

    		oCrit = new Criteria();
        	oCrit.add(InventoryLocationPeer.ITEM_ID, _sID);
        	InventoryLocationPeer.doDelete(oCrit);
        	
        	Item oItem = getItemByID(_sID);
        	
        	if (oItem != null)
        	{
        		ItemManager.getInstance().refreshCache(
        			oItem.getItemId(), oItem.getItemCode(), oItem.getKategoriId()
				);
        	}
        	
    		oCrit = new Criteria();
        	oCrit.add(ItemPeer.ITEM_ID, _sID);
        	ItemPeer.doDelete(oCrit);

        	return true;
		}
    	else 
    	{
    		return false;
    	}		
	}
	
	public static boolean isGLValid (Item _oItem)
	{
		if (_oItem != null)
		{
			if(StringUtil.isNotEmpty(_oItem.getSalesAccount()) &&			   				
			   StringUtil.isNotEmpty(_oItem.getSalesReturnAccount()) &&			   
			   StringUtil.isNotEmpty(_oItem.getItemDiscountAccount()) )
			{			   					
				if (_oItem.getItemType() == i_INVENTORY_PART) 
				{
					if(StringUtil.isEmpty(_oItem.getInventoryAccount()) || 
					   StringUtil.isEmpty(_oItem.getCogsAccount()) || 
					   StringUtil.isEmpty(_oItem.getPurchaseReturnAccount()) || 
					   StringUtil.isEmpty(_oItem.getUnbilledGoodsAccount()) )
					{	   
						return false;
					}					
				}
				else if (_oItem.getItemType() == i_NON_INVENTORY_PART || _oItem.getItemType() == i_ASSET) 
				{
					if(StringUtil.isEmpty(_oItem.getExpenseAccount()) || 
					   StringUtil.isEmpty(_oItem.getPurchaseReturnAccount()) || 
					   StringUtil.isEmpty(_oItem.getUnbilledGoodsAccount()) ) 
					{				   
						return false;
					}
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean isRecentlyAdjusted(Item _oItem)
	{
		if (_oItem != null && _oItem.getLastAdjustment() != null)
		{
			try 
			{
				if (DateUtil.countDays(_oItem.getLastAdjustment(), new Date()) <= 7)
				{
					return true;
				}
			} 
			catch (Exception _oEx) 
			{
				log.error(_oEx);
			}
		}
		return false;
	}	
}
package com.ssti.enterprise.pos.presentation.screens.setup;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.tools.CustomFieldTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: Albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: ItemSetup.java,v $
 * Revision 1.25  2009/05/04 02:02:19  albert
 * *** empty log message ***
 *
 * 
 * </pre><br>
 */
public class ItemSetup extends ItemSecureScreen
{
	private static Log log = LogFactory.getLog(ItemSetup.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");		
		String sID = "";
		try
		{
			sID = data.getParameters().getString("id", "");
			if (StringUtil.isNotEmpty(sID))
			{
				Item oItem = ItemTool.getItemByID (sID);
				context.put ("Item", oItem);
				if (sMode.equals("update") || sMode.equals("delete") )
				{

				}
				else if (sMode.equals("adddet"))
				{
					addDetail(data, oItem);
	    		}
				else if (sMode.equals("deldet"))
	    		{
					delDetail(data, sID, oItem);
	    		}	    		
	    		
	    		if (oItem.getItemType() == i_GROUPING) 
	    		{
					setGroupDataContext (data, context, sID);
				}
	    	}
	    	if (sMode.equals("findByCode"))
	    	{
	    		String sCode = data.getParameters().getString("ItemCode");
	    		Item oItem = ItemTool.getItemByCode(sCode);
				context.put ("Item", oItem);
	    	}
	    	else if (sMode.equals("findByName"))
	    	{
	    		String sName = data.getParameters().getString("Description");
	    		Item oItem = ItemTool.getItemByDescription(sName);
				context.put ("Item", oItem );
	    	}	    	
            context.put(TransactionAttributes.s_MULTI_CURRENCY, TransactionAttributes.b_SALES_MULTI_CURRENCY);
			context.put("UseSerialNumber", Boolean.valueOf (PreferenceTool.useSerialNo()));				
			context.put("UseBatchNumber", Boolean.valueOf (PreferenceTool.useBatchNo()));				
			
			context.put("AutoCode", Boolean.valueOf (CONFIG.getBoolean(("generator.itemcode.automatic"), true) ) );				
			context.put("UseGL", Boolean.valueOf (CONFIG.getBoolean(("gl.installed"), true) ) );				
			context.put("customfield", CustomFieldTool.getInstance());
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error (_oEx);
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
		}
	}
    
    /**
     * set item grouping in context because after add new, it's common to add or delete item_group detail
     * 
     * @param data
     * @param context
     * @param sID Group ID
     */
	public static void setGroupDataContext(RunData data, Context context, String sID)
	{
		try 
		{
			context.put ("GroupItems", ItemGroupTool.getItemGroupByGroupID (sID));
			context.put("GroupingCOGSType", 
				Integer.valueOf (CONFIG.getInt(("gl.grouping.cogs.type"))));							
		}
		catch (Exception _oEx) {
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
		}
	}
	
	/**
	 * add item to item group 
	 * 
	 * @param data
	 * @param _oItem
	 */
	private void addDetail (RunData data, Item _oItem)

	{
		try
		{
			String sItemID = data.getParameters().getString("ItemId");
			Item oItem = ItemTool.getItemByID(sItemID);
			if(oItem != null)
			{
				ItemGroup oItemGroup = new ItemGroup ();					
				data.getParameters().setProperties(oItemGroup);
				oItemGroup.setItemCode(oItem.getItemCode());
				oItemGroup.setItemName(oItem.getItemName());
				oItemGroup.setUnitId(oItem.getUnitId());
				oItemGroup.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));				
				
				List vData = ItemGroupTool.getByGroupAndItemID(oItemGroup.getGroupId(),oItemGroup.getItemId(), null);
				ItemGroup oExistItemGroup = null;
		
				if(vData.size()> 0)
				{
					oExistItemGroup = (ItemGroup) vData.get(0);
					double dnewQty = (oExistItemGroup.getQty().doubleValue() + oItemGroup.getQty().doubleValue());
					oExistItemGroup.setQty(new BigDecimal(dnewQty));
					oExistItemGroup.save();
				}
				else
				{
					oItemGroup.setItemGroupId (IDGenerator.generateSysID());
					oItemGroup.save();
				}		
				ItemManager.getInstance().refreshGroupCache(oItemGroup.getGroupId());
				
				//set update date
				_oItem.setUpdateDate(new Date());
				_oItem.save();	
			}
		}
		catch (Exception _oEx) 
		{
			data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
		}
	}

	/**
	 * del item from item group
	 * 
	 * @param data
	 * @param _sGroupID
	 * @param _oItem
	 */
	private void delDetail (RunData data, String _sGroupID, Item _oItem)
	{
		try 
		{
			ItemGroupTool.delGroupItemByID (data.getParameters().getString("delid"));
			ItemManager.getInstance().refreshGroupCache(_sGroupID);
			
			//set update date
			_oItem.setUpdateDate(new Date());
			_oItem.save();
		}
		catch (Exception _oEx) 
		{
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
		}
	}
}
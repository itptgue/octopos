package com.ssti.enterprise.pos.tools.data;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.AdminLog;
import com.ssti.enterprise.pos.om.AdminLogPeer;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.framework.tools.IDGenerator;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Update Transaction Field
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $

 * </pre><br>
 */
public class AdminLogTool extends BaseTool implements AppAttributes, TransactionAttributes
{
	static Log log = LogFactory.getLog(AdminLogTool.class);

    static AdminLogTool instance = null;    
    public static synchronized AdminLogTool getInstance() 
    {
        if (instance == null) instance = new AdminLogTool();
        return instance;
    }
    
    public static void createLog(String _sUserName, String _sSQL, String _sResult)
    	throws Exception
    {
    	try
		{
			AdminLog oLog = new AdminLog();
			oLog.setAdminLogId(IDGenerator.generateSysID());
			oLog.setTransDate(new Date());
			oLog.setUserName(_sUserName);
			oLog.setSqlCmd(_sSQL);
			oLog.setSqlMsg(_sResult);
			oLog.save();			
		}
		catch (Exception e)
		{
			log.error(e);
			throw new Exception("ERROR Saving LOG: " + e.getMessage(),e);
		}
    }    
    
    public static List viewLog(String _sUserName, String _sSQL, Date _dStart, Date _dEnd)
    {
    	try
		{
    		Criteria oCrit = new Criteria();
    		if (StringUtil.isNotEmpty(_sUserName))
    		{
    			oCrit.add(AdminLogPeer.USER_NAME,(Object)_sUserName, Criteria.ILIKE);
    		}
    		if (StringUtil.isNotEmpty(_sSQL))
    		{
    			oCrit.add(AdminLogPeer.USER_NAME,(Object)_sSQL, Criteria.ILIKE);
    		}
    		if (_dStart != null)
    		{
    			oCrit.add(AdminLogPeer.TRANS_DATE,_dStart, Criteria.GREATER_EQUAL);
    		}
    		if (_dEnd != null)
    		{
    			oCrit.and(AdminLogPeer.TRANS_DATE,_dEnd, Criteria.LESS_EQUAL);
    		}
    		return AdminLogPeer.doSelect(oCrit);	
		}
		catch (Exception e)
		{
			log.error(e);
		}
    	return null;
    }    
    
}

package com.ssti.framework.mail;

import java.io.File;
import java.security.Provider;
import java.security.Security;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mashape.unirest.http.Unirest;
import com.ssti.framework.security.OAuth2SaslClientFactory;
import com.ssti.framework.tools.StringUtil;
import com.sun.mail.smtp.SMTPTransport;

public class MailSender implements Runnable
{
	private static Log log = LogFactory.getLog ( MailSender.class );

	public static final String GMAIL_SMTP = "smtp.gmail.com";
	public static final String YAHOO_SMTP = "smtp.yahoo.com";
	
	public static final int STD_PORT = 25;
	public static final int YAHOO_PORT = 465;	
	public static final int GMAIL_PORT = 587;
	
	public static final String GCLIENT_ID = "290865583651-6au9qolhog9i0gq7befmo9rb37crnc0i.apps.googleusercontent.com";
	public static final String GCLIENT_SECRET = "5oiTZjJee-CE2N_lenOLsRPe";
	public static final String GREFTOKEN = "1/Z-aCscXMkZHfdQ5ln2y4yq4vzB7Z2l3MG5CDnEKgpCRHwL2BbniTlW3T3flE5WaU";
	
	String from = "";
	String[] to = {""};
	String subject = "";
	String body = "";
	String attFile = "";
	File[] fileAtt = null;
	
	Properties props = null;
	Authenticator auth = null;
	Session session = null;
	SMTPTransport transport = null;
			
	String smtp = GMAIL_SMTP; 
	String usr = "";  
	String pwd = ""; 
	int port = STD_PORT;
	String errMsg = "";

	RecipientType recType = RecipientType.TO;
	
	public void setRecType(RecipientType recType)
	{
		this.recType = recType;
	}

	public void setFileAtt(File[] _att)
	{
		this.fileAtt = _att;
	}

	public void setPort(int _port)
	{
		port = _port;
	}

	public MailSender(String smtp,
					  String usr,
					  String pwd,
					  String from, 
					  String to[],
					  String subject,
					  String body, 
					  String attFile)
	{
		this.smtp = smtp;
		this.usr = usr;
		this.pwd = pwd;
		
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.body = body;
		this.attFile = attFile;
		this.port = STD_PORT;
		
		init();	
	}

	private void init()	
	{		
		auth = new Authenticator() 
		{				
			@Override			
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(usr, pwd);
			}
		};
		
		props = System.getProperties();
		try 
		{
			if (StringUtil.equalsIgnoreCase(smtp, GMAIL_SMTP))
			{						    	
		    	//String sender = usr;
		    	String refToken = GREFTOKEN; //retailsoft.smtp@gmail.com
		    	
		    	if(StringUtil.isNotEmpty(pwd)) {
		    		refToken = pwd;  
		    	}
		    	
		    	String url = "https://www.googleapis.com/oauth2/v4/token"+
		    				 "?client_id=" + GCLIENT_ID + 
					    	 "&client_secret=" + GCLIENT_SECRET +
					    	 "&refresh_token="+ refToken +
					    	 "&grant_type=refresh_token";		    			    	
		    	System.out.println("google url" + url);
		    	
		    	String key = Unirest.post(url).asJson().getBody().getObject().getString("access_token");			
				Security.addProvider(new OAuth2Provider());
				
		    	Properties props = new Properties();
		    	props.put("mail.smtp.starttls.enable", "true");
		    	props.put("mail.smtp.starttls.required", "true");
		    	props.put("mail.smtp.sasl.enable", "true");
		    	props.put("mail.smtp.sasl.mechanisms", "XOAUTH2");
		    	props.put(OAuth2SaslClientFactory.OAUTH_TOKEN_PROP, key);
		    	session = Session.getInstance(props);
		    	session.setDebug(true);
				
		    	transport = new SMTPTransport(session, null);
		    	transport.connect(smtp, GMAIL_PORT, usr, "");
							    	
	//			props.put("mail.smtp.starttls.enable", "true");
	//			props.put("mail.smtp.starttls.required", "true");
	//			props.put("mail.smtp.host", GMAIL_SMTP);
	//			props.put("mail.smtp.port", GMAIL_PORT); 
	//			props.put("mail.smtp.localhost", "retailsoft-server");
	//			props.put("mail.smtp.auth", "true");
	//			props.put("mail.debug", "true");
	//			props.put("mail.smtp.socketFactory.port", GMAIL_PORT);
	//			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	//			props.put("mail.smtp.socketFactory.fallback", "false");
				
	//			Properties props = new Properties();
	//		    props.put("mail.smtp.starttls.enable", "true");
	//		    props.put("mail.smtp.starttls.required", "true");
	//		    props.put("mail.smtp.sasl.enable", "true");
	//		    props.put("mail.smtp.sasl.mechanisms", "XOAUTH2");
	//		    props.put(OAuth2SaslClientFactory.OAUTH_TOKEN_PROP, oauthToken);
				
			}
			else if (StringUtil.equalsIgnoreCase(smtp, YAHOO_SMTP))
			{		
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", YAHOO_SMTP);
				props.put("mail.smtp.port", YAHOO_PORT); 
				props.put("mail.smtp.auth", "true");
				props.put("mail.debug", "true");
				props.put("mail.smtp.socketFactory.port", YAHOO_PORT);
				props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				props.put("mail.smtp.socketFactory.fallback", "false");
				
				session = Session.getInstance(props, auth);
				transport = new SMTPTransport(session, null);
		    	transport.connect(smtp, YAHOO_PORT, usr, pwd);
			}
			else
			{
				props.put("mail.smtp.host", smtp);								
				props.put("mail.smtp.port", port); 
				props.put("mail.debug", "true");
				
				if(StringUtil.isEmpty(pwd)) 
				{
					props.put("mail.smtp.auth", "true");
					auth = null;
				}
				
				session = Session.getInstance(props, auth);
				session.setDebug(true);

				//transport = new SMTPTransport(session, null);
				//transport.connect(smtp, port, usr, pwd);
			}		
		} 
		catch (Exception e) 
		{
			errMsg = e.getMessage();			
			log.error(e);
			e.printStackTrace();
		}	
	}
	
	public void send() throws Exception
	{
		try 
		{							
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from + "<" + usr + ">"));
			msg.setSubject(subject);
			
			InternetAddress[] toAddress = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++)
			{
				if (StringUtil.isNotEmpty(to[i]))
				{
				    toAddress[i] = new InternetAddress(to[i]);					
				}
			}
			
			msg.setRecipients(recType, toAddress);
					    
			Pattern p = Pattern.compile("<([^\\s>/]+)");
		    Matcher m = p.matcher(body);
		    boolean bhtml = false;
		    while(m.find()) 
		    {
		    	bhtml = true;
		    	String tag = m.group(1);
		        //System.out.println(tag);
		    }
			
			MimeBodyPart mimeBody = new MimeBodyPart();
			if(bhtml)
			{
				mimeBody.setContent(body, "text/html");
			}
			else
			{
				mimeBody.setText(body);
			}

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBody);
			
			//do attachment
			if (StringUtil.isNotEmpty(attFile))
			{
				File attachment = new File(attFile);

				MimeBodyPart mimeAttachment = new MimeBodyPart();
				FileDataSource dataSource = new FileDataSource(attachment);
				mimeAttachment.setDataHandler(new DataHandler(dataSource));
				mimeAttachment.setFileName(attachment.getName());
				mimeAttachment.setDisposition(MimeBodyPart.ATTACHMENT);				
				multipart.addBodyPart(mimeAttachment);
			}
			
			if(fileAtt != null && fileAtt.length > 0)
			{
				for(int i = 0; i < fileAtt.length; i++)
				{
					File attachment = fileAtt[i];

					MimeBodyPart mimeAttachment = new MimeBodyPart();
					FileDataSource dataSource = new FileDataSource(attachment);
					mimeAttachment.setDataHandler(new DataHandler(dataSource));
					mimeAttachment.setFileName(attachment.getName());
					mimeAttachment.setDisposition(MimeBodyPart.ATTACHMENT);				
					multipart.addBodyPart(mimeAttachment);
				}
			}
						
			msg.setContent(multipart);
			
			if(transport != null)
			{
				transport.sendMessage(msg,toAddress);
			}
			else
			{
				Transport.send(msg);
			}
		} 
		catch (AddressException ex) 
		{
			log.error(ex.getMessage(), ex);
			ex.printStackTrace();
			throw new NestableException(ex.getMessage(), ex);
		} 
		catch (MessagingException ex) 
		{
			log.error(ex.getMessage(), ex);
			ex.printStackTrace();
			throw new NestableException(ex.getMessage(), ex);
		}
		catch (Exception ex)
		{
			log.error(ex.getMessage(), ex);
			ex.printStackTrace();
			throw new NestableException(ex.getMessage(), ex);			
		}
	}

	public void run() 
	{
		try 
		{
			send();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static final class OAuth2Provider extends Provider {
	   private static final long serialVersionUID = 1L;

	   public OAuth2Provider() {
	     super("Google OAuth2 Provider", 1.0, "Provides the XOAUTH2 SASL Mechanism");
	     put("SaslClientFactory.XOAUTH2", "com.ssti.framework.security.OAuth2SaslClientFactory");
	   }
	 }
}

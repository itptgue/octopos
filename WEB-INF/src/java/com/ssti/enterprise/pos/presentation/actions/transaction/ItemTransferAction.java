package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.ItemTransferDetailLoader;
import com.ssti.enterprise.pos.excel.helper.transaction.ItemTransferLoader;
import com.ssti.enterprise.pos.om.ItemTransfer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-12-27
 * - change doLoadfile add load Excel function with detail format like issue receipt
 *   
 * </pre><br>
 */
public class ItemTransferAction extends TransactionSecureAction
{
    private static final String s_VIEW_PERM     = "View Item Transfer";
	private static final String s_CREATE_PERM   = "Create Item Transfer";
	private static final String s_CANCEL_PERM   = "Cancel Item Transfer";
    private static final String s_CONFIRM_PERM  = "Confirm Item Transfer";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_CONFIRM_PERM = {s_VIEW_PERM, s_CONFIRM_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    HttpSession oSes = null;
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        int iSave = data.getParameters().getInt("save");
    	if(iSave == 1) //save
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
    	if(iSave == 2) //cancel
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }
        if(iSave == 3) //receive
        {
            return isAuthorized (data, a_CREATE_PERM);
        }
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
	    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    	oSes = data.getSession();
        int iSave = data.getParameters().getInt("save");
        if (iSave == 1) 
        {
    		doSave (data,context);
    	}
    	if (iSave == 2) 
        {
    		doCancel (data,context);
    	}
        if (iSave == 3) 
        {
            doReceive (data,context);
        }        
    }

	public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);

			String sFromID = data.getParameters().getString("FromLocationId","");
			String sToID = data.getParameters().getString("ToLocationId","");
			int iStatus = data.getParameters().getInt("Status");
						
	    	LargeSelect vTR = 
	    		ItemTransferTool.findData (iCond, sKeywords, dStart, dEnd, sFromID, sToID, iStatus, iLimit);

	    	data.getUser().setTemp("findItemTransferResult", vTR);				
        }
        catch(Exception _oEx)
        {
	       	data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }

	public void doSave(RunData data, Context context)
        throws Exception
    {
		try
		{
			oSes = data.getSession();
			ItemTransfer oIT = (ItemTransfer) oSes.getAttribute(s_IT);
	    	List vITD = (List) oSes.getAttribute(s_ITD);

			ItemTransferTool.updateDetail(data, vITD);
			ItemTransferTool.setHeader(data, oIT);
	    	
	        if (oIT.getStatus() == i_PENDING)
	        {
	        	oIT.setUserName(data.getUser().getName());
	        }
	        if (oIT.getStatus() == i_PROCESSED)
	        {
	        	oIT.setConfirmBy (data.getUser().getName());
	        	oIT.setConfirmDate(new Date());
	        }
			
	        ItemTransferTool.saveData (oIT, vITD, null);
	        
//			REQUEST Status will be updated by saveData if sent complete	        
//	        String sRQID = oIT.getRequestId();
//	        if(StringUtil.isNotEmpty(sRQID))
//	        {
//	            PurchaseRequestTool.changeStatusByID(sRQID, i_RQ_TRANSFERED);
//	        }
        	data.setMessage(LocaleTool.getString("itr_save_success"));
        }
        catch(Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("itr_save_failed"), _oEx);
        }
    }
	
    public void doReceive(RunData data, Context context)
        throws Exception
    {
        try
        {
        	oSes = data.getSession();
            ItemTransfer oIT = (ItemTransfer) oSes.getAttribute(s_IT);
            List vITD = (List) oSes.getAttribute(s_ITD);
            ItemTransferTool.receiveTrans(oIT, data.getUser().getName());
            oIT = ItemTransferTool.getHeaderByID(oIT.getItemTransferId());
            oSes.setAttribute(s_IT, oIT);
            data.setMessage(LocaleTool.getString("itr_save_success"));
        }
        catch(Exception _oEx)
        {
            handleError (data, LocaleTool.getString("itr_save_failed"), _oEx);
        }
    }
    
	/**
	 * 
	 * @param data
	 * @param context
	 * @throws Exception
	 */
	public void doCancel(RunData data, Context context)
	    throws Exception
	{
		try
		{
			oSes = data.getSession();
			ItemTransfer oIT = (ItemTransfer) oSes.getAttribute(s_IT);
	    	List vITD = (List) oSes.getAttribute(s_ITD);
	    	
	    	ItemTransferTool.cancelTrans(oIT, vITD, data.getUser().getName(), null);

			data.setMessage(LocaleTool.getString("itr_cancel_success"));
	    }
	    catch(Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("itr_cancel_failed"), _oEx);
	    }
	}
	
	/**
	 * 
	 * @param data
	 * @param context
	 * @throws Exception
	 */
    public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
	     	InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
	     	boolean bDetail = data.getParameters().getBoolean("DetailLoader");
	     	if(bDetail) //item detail template like issue receipt
	     	{
	     		String sFromLocID = data.getParameters().getString("FromLocationId");
	     		if(StringUtil.isNotEmpty(sFromLocID))
	     		{
					ItemTransferDetailLoader oLoader = new ItemTransferDetailLoader(sFromLocID);
					oLoader.loadData (oBufferStream);	     										
					if(oLoader.getTotalError() > 0)
					{
						data.setMessage(oLoader.getErrorMessage());
					}
					else 
					{
						ItemTransfer oIT = new ItemTransfer();				
						data.getParameters().setProperties (oIT);
						oSes = data.getSession();
						oSes.setAttribute(s_IT,oIT);
						oSes.setAttribute(s_ITD, oLoader.getListResult());
	
						context.put("bClose", Boolean.valueOf(true));
					}
	     		}
	     		else
	     		{
	     			data.setMessage("From Location may not be empty");
	     		}
	     	}
	     	else //transaction template with heading, detail heads, end trans
	     	{
				ItemTransferLoader oLoader = new ItemTransferLoader(data.getUser().getName());
				oLoader.loadData (oBufferStream);
				data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS) + oLoader.getSummary());	
				context.put ("LoadResult", oLoader.getResult());
	     	}						    	
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError =  LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	} 
    
}
package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.ArPaymentDetail;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.CashFlowType;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.CreditMemoPeer;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.financial.CashFlowTypeTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ReceivablePaymentLoader.java,v 1.1 2009/05/04 01:38:37 albert Exp $ <br>
 *
 * <pre>
 * $Log: ReceivablePaymentLoader.java,v $
 * Revision 1.1  2009/05/04 01:38:37  albert
 * *** empty log message ***
 *
 * 
 * </pre><br>
 */
public class ReceivablePaymentLoader extends TransactionLoader
{    
	private static Log log = LogFactory.getLog ( ReceivablePaymentLoader.class );

	private static final String s_START_HEADING = "Trans No";	
	private static final String s_DETAIL_HEADING = "Invoice No";
	private static final String s_END_TRANS = "End Trans";	
	
	public ReceivablePaymentLoader()
	{
	}

	public ReceivablePaymentLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
	}
	
	/**
	 * load data from input stream
	 * 
	 * @param _oInput
	 * @throws Exception
	 */
    public void loadData (InputStream _oInput)	
    	throws Exception
    {
    	POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
    	HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
    	HSSFSheet oSheet = oWB.getSheetAt(0);
    	HSSFRow oRow = null;
    	    	
		m_iTotalRows = oSheet.getPhysicalNumberOfRows();
		boolean bTransHeader = false;
		boolean bTransProcessed = false;
		boolean bDetailProcess = false;
		
		ArPayment oTR = null;
		List vTD = null;
		List vMEMO = null;
		
		for (int iRow = 0; iRow < m_iTotalRows; iRow++)
		{
    		oRow = oSheet.getRow(iRow);
    		if (oRow != null)
    		{
    			String sValue = getString(oRow, i_START);
    			if (sValue.equals(s_START_HEADING) && !bTransHeader && !bTransProcessed && !bDetailProcess)
    			{
    				m_bTransError = false;
    				m_iTotalTrans++;
    				
    				bTransHeader = true;
    				log.debug("Found Trans HEADING :" + sValue);
    			}
    			else if (StringUtil.isNotEmpty(sValue) && bTransHeader && !bTransProcessed && !bDetailProcess)
    			{    				
    				oTR = new ArPayment();
    				vMEMO = new ArrayList();
    				mapTrans (oRow, oTR);
    				bTransProcessed = true;
    				
    				log.debug("* TR " + oTR);
    			}
    			else if (sValue.equals(s_DETAIL_HEADING) && bTransHeader && bTransProcessed && !bDetailProcess)
    			{
    				vTD = new ArrayList();
    				bDetailProcess = true;
    				
    				log.debug("Found Detail HEADING :" + sValue);
    			}
    			else if (!sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{    				
    				ArPaymentDetail oTD = new ArPaymentDetail();
    				mapDetails (oRow, oTR, oTD, vMEMO);
    				if (!m_bTransError) vTD.add(oTD);
    				
    				log.debug("* TD " + oTD);
    			}    			
    			else if (sValue.equals(s_END_TRANS) && bTransHeader && bTransProcessed && bDetailProcess)
    			{
    				if (!m_bTransError)
    				{
    		    		try
    		    		{
    		    			ReceivablePaymentTool.setHeaderProperties(oTR, vTD, null);
    		    			ReceivablePaymentTool.saveData(oTR, vTD, vMEMO);
    		    			logSuccess();
    		    		}
    		    		catch (Exception _oEx)
    		    		{
    		    			String sMsg = _oEx.getMessage();
    		    			_oEx.printStackTrace();
    		    			log.error(_oEx);
    		    			logSaveError(sMsg);
    		    		}
    				}
    				else
    				{
    					logReject();
    				}
    				m_bTransError = false;
    				
    				bTransHeader = false;
    				bTransProcessed = false;
    				bDetailProcess = false;
    			}
    			else 
    			{
    				logInvalidFile();
    				return;
    			}
			}
    	} 
    }	
	
    protected void mapTrans (HSSFRow _oRow, ArPayment _oTR)
    	throws Exception
    {
    	/*
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Trans No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Customer Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Bank Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Transaction Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Due Date"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Reference No"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Issuer Dest"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Create By"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,  HSSFCellStyle.ALIGN_LEFT,"Remark"); iCol++;
    	*/
    	
    	int iCol = 0;
    	String sTransNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("trans_no")).append(": ").append(sTransNo).append(s_LINE_SEPARATOR);
    	
    	String sLocationCode = getString (_oRow, iCol); iCol++;
    	Location oLoc = LocationTool.getLocationByCode(sLocationCode);
    	if (oLoc == null) {logError("Location", sLocationCode);}
    	else {_oTR.setLocationId(oLoc.getLocationId());}
    	
    	String sCustomerCode = getString(_oRow, iCol); iCol++;
    	Customer oCust = CustomerTool.getCustomerByCode(sCustomerCode);
    	if (oCust == null) {logError("Customer Code", sCustomerCode);}
    	else 
    	{
    		_oTR.setCustomerId(oCust.getCustomerId()); 
    		_oTR.setCustomerName(oCust.getCustomerName());
    	}

    	String sBankCode = getString(_oRow, iCol); iCol++;
    	Bank oBank = BankTool.getBankByCode(sBankCode);
    	if (oBank == null) {logError("Bank Code", sBankCode);}
    	else 
    	{
    		_oTR.setBankId(oBank.getBankId()); 
    	}
    	
    	Date dTrans = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dTrans == null) {logError("Transaction Date", dTrans);}
    	else {_oTR.setArPaymentDate(dTrans);}

    	Date dDue = CustomParser.parseDate(getString(_oRow, iCol)); iCol++;
    	if (dDue == null) {logError("Due Date", dDue);}
    	else {_oTR.setArPaymentDueDate(dDue);}

    	String sCFTCode = getString(_oRow, iCol); iCol++;
    	CashFlowType oCFT = CashFlowTypeTool.getTypeByCode(sCFTCode, null);
    	if (oCFT == null) 
    	{
    		if (StringUtil.isNotEmpty(sCFTCode)) 
    		logError("Cash Flow Type", sBankCode);
    	}
    	else 
    	{
    		_oTR.setCashFlowTypeId(oCFT.getCashFlowTypeId());
    	}
    	
    	String sRefNo = getString(_oRow, iCol); iCol++;
    	_oTR.setReferenceNo(sRefNo);

    	String sIssuer = getString(_oRow, iCol); iCol++;
    	_oTR.setBankIssuer(sIssuer);

    	String sUserName = getString(_oRow, iCol); iCol++;
    	if (StringUtil.isEmpty(sUserName)) {logError("Cashier", sUserName);}
    	else {_oTR.setUserName(sUserName);}
		
    	String sRemark = getString(_oRow, iCol); iCol++;
    	_oTR.setRemark(sRemark);
    	
    	//currency
    	String sCurr = getString(_oRow, iCol); iCol++;    	
    	Currency oCurr = CurrencyTool.getCurrencyByCode(sCurr);
    	if (oCurr != null)
    	{
    		_oTR.setCurrencyId(oCurr.getCurrencyId());
    		if (oCurr.getIsDefault())
    		{
    	    	_oTR.setCurrencyRate(bd_ONE);
    	    	_oTR.setFiscalRate(bd_ONE);    			
    		}
    		else
    		{
    			BigDecimal bdRate = getBigDecimal(_oRow, iCol); iCol++;
    			if (bdRate == null || bdRate.doubleValue() <= 0) {logError("Currency Rate", bdRate);}
    			else {_oTR.setCurrencyRate(bdRate);}

    			BigDecimal bdFiscal = getBigDecimal(_oRow, iCol); iCol++;
    			if (bdFiscal == null || bdFiscal.doubleValue() <= 0) {logError("Fiscal Rate", bdRate);}
    			else {_oTR.setFiscalRate(bdRate);}
    		}
    	}
    	else //user default currency
    	{
    		_oTR.setCurrencyId(CurrencyTool.getDefaultCurrency(null).getCurrencyId());
	    	_oTR.setCurrencyRate(bd_ONE);
	    	_oTR.setFiscalRate(bd_ONE);    			
    	}
    	
    	//OTHER FIELD
    	if (!m_bTransError)
    	{
            _oTR.setArPaymentNo("");
            _oTR.setStatus(i_PROCESSED);
    		//will be set by set header properties
    		_oTR.setTotalDiscount(bd_ZERO);
    		_oTR.setTotalDownPayment(bd_ZERO);
    		_oTR.setTotalAmount(bd_ZERO);
    		_oTR.setPaymentAmount(bd_ZERO);    		
    		
	    	_oTR.setRemark(_oTR.getRemark() + setDesc(sTransNo,m_sUserName));
    	}
    }
    
    protected void mapDetails (HSSFRow _oRow, ArPayment _oTR, ArPaymentDetail _oTD, List _vMEMO)
    		throws Exception
    	{    	
        	mapDetails(_oRow,_oTR,_oTD,_vMEMO,0);
    	}
    	
	/**
	 * 
	 * @param _oRow
	 * @param _oTR
	 * @param _oTD
	 * @param _iStartCol
	 * @throws Exception
	 */
    protected void mapDetails (HSSFRow _oRow, ArPayment _oTR, ArPaymentDetail _oTD, List _vMEMO, int _iStartCol)
		throws Exception
	{    
    	int iCol = _iStartCol;
    	String sInvNo = getString (_oRow, iCol); iCol++;
    	m_oResult.append(LocaleTool.getString("invoice_no")).append(": ").append(sInvNo).append(s_LINE_SEPARATOR);
    	
    	SalesTransaction oInv = (SalesTransaction) 
    		TransactionTool.getByTransNo(SalesTransactionPeer.INVOICE_NO, sInvNo, SalesTransactionPeer.class, null);
    	
    	if (oInv == null) {logError("Invoice No", sInvNo);}
    	else 
    	{
    		_oTD.setSalesTransactionId(oInv.getSalesTransactionId());
    		_oTD.setSalesTransactionAmount(oInv.getTotalAmount());
    		_oTD.setPaidAmount(oInv.getPaidAmount());
    		_oTD.setInvoiceNo(sInvNo);
    	
    		double dInvAmt = oInv.getTotalAmount().doubleValue();
    		double dPaidAmt = oInv.getPaidAmount().doubleValue();
    		
    		BigDecimal dPmt = getBigDecimal (_oRow, iCol); iCol++;
        	if (dPmt == null) {logError("Payment Amount", dPmt);}
        	else 
        	{
        		if (dPmt.doubleValue() > (dInvAmt - dPaidAmt))
        		{
        			logError("Payment Amount > Total Amount + Paid Amount", dPmt);
        		}
        		else
        		{
        			_oTD.setPaymentAmount(dPmt);
        		}
        	}
        	
        	BigDecimal dInvRate = getBigDecimal (_oRow, iCol); iCol++;
        	if (dInvRate == null || dInvRate.doubleValue() <= 0) dInvRate = bd_ONE;
        	_oTD.setInvoiceRate(dInvRate);
        	
        	BigDecimal dDisc = getBigDecimal (_oRow, iCol); iCol++;
        	if (dDisc == null) dDisc = bd_ZERO;
        	_oTD.setDiscountAmount(dDisc);
        	
        	String sAcc = getString(_oRow, iCol); iCol++;
        	Account oAcc = AccountTool.getAccountByCode(sAcc);
            if (dDisc.doubleValue() != 0)
            {
            	if (oAcc == null)
            	{
            		logError("Discount Account", sAcc);
            	}
            	else
            	{
            		if (oAcc.getHasChild()) logInvalidAcc(sAcc);
            		if (dDisc.doubleValue() != 0) _oTD.setDiscountAccountId(oAcc.getAccountId());
            	}
            }
            else
            {
                _oTD.setDiscountAccountId("");
            }
        	
        	String sCM = getString(_oRow, iCol); iCol++;
        	if (StringUtil.isNotEmpty(sCM))
        	{
        		CreditMemo oMemo = (CreditMemo) 
        			CreditMemoTool.getByTransNo(CreditMemoPeer.CREDIT_MEMO_NO, sCM, CreditMemoPeer.class, null);	
        		if (oMemo != null && oMemo.getStatus() == i_MEMO_OPEN)
        		{
        			_oTD.setDownPayment(oMemo.getAmount());
        			List vMID = new ArrayList(1);
        			vMID.add(oMemo.getCreditMemoId());
        			_vMEMO.add(vMID);
        		}
        		else if (oMemo != null && (oMemo.getStatus() == i_MEMO_CLOSED || oMemo.getStatus() == i_MEMO_CANCELLED))
        		{
        			logError("Credit Memo Status CLOSED ", sCM);
        		}
        		else if (oMemo == null)
        		{
        			logError("Credit Memo Not Found ", sCM);        			
        		}
        	}
        	else
        	{
        		_oTD.setDownPayment(bd_ZERO);
        	}
    	}
    	
    	if (!m_bTransError)
    	{
    		double dTotalDP = _oTR.getTotalDownPayment().doubleValue() + _oTD.getDownPayment().doubleValue();
    		_oTR.setTotalDownPayment(new BigDecimal(dTotalDP));
    	}
	}   
}

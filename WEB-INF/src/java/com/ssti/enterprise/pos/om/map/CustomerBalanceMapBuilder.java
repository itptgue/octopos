package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerBalanceMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerBalanceMapBuilder";

    /**
     * Item
     * @deprecated use CustomerBalancePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_balance";
    }

  
    /**
     * customer_balance.CUSTOMER_BALANCE_ID
     * @return the column name for the CUSTOMER_BALANCE_ID field
     * @deprecated use CustomerBalancePeer.customer_balance.CUSTOMER_BALANCE_ID constant
     */
    public static String getCustomerBalance_CustomerBalanceId()
    {
        return "customer_balance.CUSTOMER_BALANCE_ID";
    }
  
    /**
     * customer_balance.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerBalancePeer.customer_balance.CUSTOMER_ID constant
     */
    public static String getCustomerBalance_CustomerId()
    {
        return "customer_balance.CUSTOMER_ID";
    }
  
    /**
     * customer_balance.CUSTOMER_NAME
     * @return the column name for the CUSTOMER_NAME field
     * @deprecated use CustomerBalancePeer.customer_balance.CUSTOMER_NAME constant
     */
    public static String getCustomerBalance_CustomerName()
    {
        return "customer_balance.CUSTOMER_NAME";
    }
  
    /**
     * customer_balance.AR_BALANCE
     * @return the column name for the AR_BALANCE field
     * @deprecated use CustomerBalancePeer.customer_balance.AR_BALANCE constant
     */
    public static String getCustomerBalance_ArBalance()
    {
        return "customer_balance.AR_BALANCE";
    }
  
    /**
     * customer_balance.PRIME_BALANCE
     * @return the column name for the PRIME_BALANCE field
     * @deprecated use CustomerBalancePeer.customer_balance.PRIME_BALANCE constant
     */
    public static String getCustomerBalance_PrimeBalance()
    {
        return "customer_balance.PRIME_BALANCE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_balance");
        TableMap tMap = dbMap.getTable("customer_balance");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_balance.CUSTOMER_BALANCE_ID", "");
                          tMap.addColumn("customer_balance.CUSTOMER_ID", "");
                          tMap.addColumn("customer_balance.CUSTOMER_NAME", "");
                            tMap.addColumn("customer_balance.AR_BALANCE", bd_ZERO);
                            tMap.addColumn("customer_balance.PRIME_BALANCE", bd_ZERO);
          }
}

package com.ssti.enterprise.pos.presentation.screens.report.receivable;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;

public class ReceivablePaymentReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	try 
    	{	
    		int iCFStatus = data.getParameters().getInt("CfStatus");
	    	context.put ("Payments", ReceivablePaymentTool.findTrans(
	    			sCustomerID, sBankID, sLocationID, sCFTID, dStart, dEnd, iStatus, iCFStatus));
	    }
    	catch (Exception _oEx) 
    	{
    		log.error(_oEx);
    		_oEx.printStackTrace();
    	}    
    }    
}

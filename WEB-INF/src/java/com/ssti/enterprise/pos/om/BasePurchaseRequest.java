package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseRequest
 */
public abstract class BasePurchaseRequest extends BaseObject
{
    /** The Peer class */
    private static final PurchaseRequestPeer peer =
        new PurchaseRequestPeer();

        
    /** The value for the purchaseRequestId field */
    private String purchaseRequestId;
      
    /** The value for the transactionStatus field */
    private int transactionStatus;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the lastUpdateDate field */
    private Date lastUpdateDate;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the requestedBy field */
    private String requestedBy;
      
    /** The value for the approvedBy field */
    private String approvedBy;
      
    /** The value for the remark field */
    private String remark;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
      
    /** The value for the confirmDate field */
    private Date confirmDate;
      
    /** The value for the transferTransId field */
    private String transferTransId;
      
    /** The value for the transferBy field */
    private String transferBy;
      
    /** The value for the transferDate field */
    private Date transferDate;
                                                                
    /** The value for the isPurchase field */
    private boolean isPurchase = false;
                                                                
    /** The value for the isReimburse field */
    private boolean isReimburse = false;
      
    /** The value for the isExternal field */
    private boolean isExternal;
      
    /** The value for the crossEntityId field */
    private String crossEntityId;
  
    
    /**
     * Get the PurchaseRequestId
     *
     * @return String
     */
    public String getPurchaseRequestId()
    {
        return purchaseRequestId;
    }

                                              
    /**
     * Set the value of PurchaseRequestId
     *
     * @param v new value
     */
    public void setPurchaseRequestId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseRequestId, v))
              {
            this.purchaseRequestId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PurchaseRequestDetail
        if (collPurchaseRequestDetails != null)
        {
            for (int i = 0; i < collPurchaseRequestDetails.size(); i++)
            {
                ((PurchaseRequestDetail) collPurchaseRequestDetails.get(i))
                    .setPurchaseRequestId(v);
            }
        }
                                }
  
    /**
     * Get the TransactionStatus
     *
     * @return int
     */
    public int getTransactionStatus()
    {
        return transactionStatus;
    }

                        
    /**
     * Set the value of TransactionStatus
     *
     * @param v new value
     */
    public void setTransactionStatus(int v) 
    {
    
                  if (this.transactionStatus != v)
              {
            this.transactionStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateDate
     *
     * @return Date
     */
    public Date getLastUpdateDate()
    {
        return lastUpdateDate;
    }

                        
    /**
     * Set the value of LastUpdateDate
     *
     * @param v new value
     */
    public void setLastUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateDate, v))
              {
            this.lastUpdateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RequestedBy
     *
     * @return String
     */
    public String getRequestedBy()
    {
        return requestedBy;
    }

                        
    /**
     * Set the value of RequestedBy
     *
     * @param v new value
     */
    public void setRequestedBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.requestedBy, v))
              {
            this.requestedBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApprovedBy
     *
     * @return String
     */
    public String getApprovedBy()
    {
        return approvedBy;
    }

                        
    /**
     * Set the value of ApprovedBy
     *
     * @param v new value
     */
    public void setApprovedBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.approvedBy, v))
              {
            this.approvedBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmDate
     *
     * @return Date
     */
    public Date getConfirmDate()
    {
        return confirmDate;
    }

                        
    /**
     * Set the value of ConfirmDate
     *
     * @param v new value
     */
    public void setConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmDate, v))
              {
            this.confirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransferTransId
     *
     * @return String
     */
    public String getTransferTransId()
    {
        return transferTransId;
    }

                        
    /**
     * Set the value of TransferTransId
     *
     * @param v new value
     */
    public void setTransferTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transferTransId, v))
              {
            this.transferTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransferBy
     *
     * @return String
     */
    public String getTransferBy()
    {
        return transferBy;
    }

                        
    /**
     * Set the value of TransferBy
     *
     * @param v new value
     */
    public void setTransferBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transferBy, v))
              {
            this.transferBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransferDate
     *
     * @return Date
     */
    public Date getTransferDate()
    {
        return transferDate;
    }

                        
    /**
     * Set the value of TransferDate
     *
     * @param v new value
     */
    public void setTransferDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transferDate, v))
              {
            this.transferDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsPurchase
     *
     * @return boolean
     */
    public boolean getIsPurchase()
    {
        return isPurchase;
    }

                        
    /**
     * Set the value of IsPurchase
     *
     * @param v new value
     */
    public void setIsPurchase(boolean v) 
    {
    
                  if (this.isPurchase != v)
              {
            this.isPurchase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsReimburse
     *
     * @return boolean
     */
    public boolean getIsReimburse()
    {
        return isReimburse;
    }

                        
    /**
     * Set the value of IsReimburse
     *
     * @param v new value
     */
    public void setIsReimburse(boolean v) 
    {
    
                  if (this.isReimburse != v)
              {
            this.isReimburse = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsExternal
     *
     * @return boolean
     */
    public boolean getIsExternal()
    {
        return isExternal;
    }

                        
    /**
     * Set the value of IsExternal
     *
     * @param v new value
     */
    public void setIsExternal(boolean v) 
    {
    
                  if (this.isExternal != v)
              {
            this.isExternal = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CrossEntityId
     *
     * @return String
     */
    public String getCrossEntityId()
    {
        return crossEntityId;
    }

                        
    /**
     * Set the value of CrossEntityId
     *
     * @param v new value
     */
    public void setCrossEntityId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.crossEntityId, v))
              {
            this.crossEntityId = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collPurchaseRequestDetails
     */
    protected List collPurchaseRequestDetails;

    /**
     * Temporary storage of collPurchaseRequestDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPurchaseRequestDetails()
    {
        if (collPurchaseRequestDetails == null)
        {
            collPurchaseRequestDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a PurchaseRequestDetail object to this object
     * through the PurchaseRequestDetail foreign key attribute
     *
     * @param l PurchaseRequestDetail
     * @throws TorqueException
     */
    public void addPurchaseRequestDetail(PurchaseRequestDetail l) throws TorqueException
    {
        getPurchaseRequestDetails().add(l);
        l.setPurchaseRequest((PurchaseRequest) this);
    }

    /**
     * The criteria used to select the current contents of collPurchaseRequestDetails
     */
    private Criteria lastPurchaseRequestDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseRequestDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPurchaseRequestDetails() throws TorqueException
    {
              if (collPurchaseRequestDetails == null)
        {
            collPurchaseRequestDetails = getPurchaseRequestDetails(new Criteria(10));
        }
        return collPurchaseRequestDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseRequest has previously
     * been saved, it will retrieve related PurchaseRequestDetails from storage.
     * If this PurchaseRequest is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPurchaseRequestDetails(Criteria criteria) throws TorqueException
    {
              if (collPurchaseRequestDetails == null)
        {
            if (isNew())
            {
               collPurchaseRequestDetails = new ArrayList();
            }
            else
            {
                        criteria.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, getPurchaseRequestId() );
                        collPurchaseRequestDetails = PurchaseRequestDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, getPurchaseRequestId());
                            if (!lastPurchaseRequestDetailsCriteria.equals(criteria))
                {
                    collPurchaseRequestDetails = PurchaseRequestDetailPeer.doSelect(criteria);
                }
            }
        }
        lastPurchaseRequestDetailsCriteria = criteria;

        return collPurchaseRequestDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseRequestDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseRequestDetails(Connection con) throws TorqueException
    {
              if (collPurchaseRequestDetails == null)
        {
            collPurchaseRequestDetails = getPurchaseRequestDetails(new Criteria(10), con);
        }
        return collPurchaseRequestDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseRequest has previously
     * been saved, it will retrieve related PurchaseRequestDetails from storage.
     * If this PurchaseRequest is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseRequestDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPurchaseRequestDetails == null)
        {
            if (isNew())
            {
               collPurchaseRequestDetails = new ArrayList();
            }
            else
            {
                         criteria.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, getPurchaseRequestId());
                         collPurchaseRequestDetails = PurchaseRequestDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, getPurchaseRequestId());
                             if (!lastPurchaseRequestDetailsCriteria.equals(criteria))
                 {
                     collPurchaseRequestDetails = PurchaseRequestDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPurchaseRequestDetailsCriteria = criteria;

         return collPurchaseRequestDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseRequest is new, it will return
     * an empty collection; or if this PurchaseRequest has previously
     * been saved, it will retrieve related PurchaseRequestDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PurchaseRequest.
     */
    protected List getPurchaseRequestDetailsJoinPurchaseRequest(Criteria criteria)
        throws TorqueException
    {
                    if (collPurchaseRequestDetails == null)
        {
            if (isNew())
            {
               collPurchaseRequestDetails = new ArrayList();
            }
            else
            {
                              criteria.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, getPurchaseRequestId());
                              collPurchaseRequestDetails = PurchaseRequestDetailPeer.doSelectJoinPurchaseRequest(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PurchaseRequestDetailPeer.PURCHASE_REQUEST_ID, getPurchaseRequestId());
                                    if (!lastPurchaseRequestDetailsCriteria.equals(criteria))
            {
                collPurchaseRequestDetails = PurchaseRequestDetailPeer.doSelectJoinPurchaseRequest(criteria);
            }
        }
        lastPurchaseRequestDetailsCriteria = criteria;

        return collPurchaseRequestDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseRequestId");
              fieldNames.add("TransactionStatus");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("LastUpdateDate");
              fieldNames.add("TotalQty");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("LocationId");
              fieldNames.add("RequestedBy");
              fieldNames.add("ApprovedBy");
              fieldNames.add("Remark");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("ConfirmDate");
              fieldNames.add("TransferTransId");
              fieldNames.add("TransferBy");
              fieldNames.add("TransferDate");
              fieldNames.add("IsPurchase");
              fieldNames.add("IsReimburse");
              fieldNames.add("IsExternal");
              fieldNames.add("CrossEntityId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseRequestId"))
        {
                return getPurchaseRequestId();
            }
          if (name.equals("TransactionStatus"))
        {
                return Integer.valueOf(getTransactionStatus());
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("LastUpdateDate"))
        {
                return getLastUpdateDate();
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("RequestedBy"))
        {
                return getRequestedBy();
            }
          if (name.equals("ApprovedBy"))
        {
                return getApprovedBy();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("ConfirmDate"))
        {
                return getConfirmDate();
            }
          if (name.equals("TransferTransId"))
        {
                return getTransferTransId();
            }
          if (name.equals("TransferBy"))
        {
                return getTransferBy();
            }
          if (name.equals("TransferDate"))
        {
                return getTransferDate();
            }
          if (name.equals("IsPurchase"))
        {
                return Boolean.valueOf(getIsPurchase());
            }
          if (name.equals("IsReimburse"))
        {
                return Boolean.valueOf(getIsReimburse());
            }
          if (name.equals("IsExternal"))
        {
                return Boolean.valueOf(getIsExternal());
            }
          if (name.equals("CrossEntityId"))
        {
                return getCrossEntityId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseRequestPeer.PURCHASE_REQUEST_ID))
        {
                return getPurchaseRequestId();
            }
          if (name.equals(PurchaseRequestPeer.TRANSACTION_STATUS))
        {
                return Integer.valueOf(getTransactionStatus());
            }
          if (name.equals(PurchaseRequestPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(PurchaseRequestPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(PurchaseRequestPeer.LAST_UPDATE_DATE))
        {
                return getLastUpdateDate();
            }
          if (name.equals(PurchaseRequestPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(PurchaseRequestPeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(PurchaseRequestPeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(PurchaseRequestPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PurchaseRequestPeer.REQUESTED_BY))
        {
                return getRequestedBy();
            }
          if (name.equals(PurchaseRequestPeer.APPROVED_BY))
        {
                return getApprovedBy();
            }
          if (name.equals(PurchaseRequestPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(PurchaseRequestPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(PurchaseRequestPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(PurchaseRequestPeer.CONFIRM_DATE))
        {
                return getConfirmDate();
            }
          if (name.equals(PurchaseRequestPeer.TRANSFER_TRANS_ID))
        {
                return getTransferTransId();
            }
          if (name.equals(PurchaseRequestPeer.TRANSFER_BY))
        {
                return getTransferBy();
            }
          if (name.equals(PurchaseRequestPeer.TRANSFER_DATE))
        {
                return getTransferDate();
            }
          if (name.equals(PurchaseRequestPeer.IS_PURCHASE))
        {
                return Boolean.valueOf(getIsPurchase());
            }
          if (name.equals(PurchaseRequestPeer.IS_REIMBURSE))
        {
                return Boolean.valueOf(getIsReimburse());
            }
          if (name.equals(PurchaseRequestPeer.IS_EXTERNAL))
        {
                return Boolean.valueOf(getIsExternal());
            }
          if (name.equals(PurchaseRequestPeer.CROSS_ENTITY_ID))
        {
                return getCrossEntityId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseRequestId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getTransactionStatus());
            }
              if (pos == 2)
        {
                return getTransactionNo();
            }
              if (pos == 3)
        {
                return getTransactionDate();
            }
              if (pos == 4)
        {
                return getLastUpdateDate();
            }
              if (pos == 5)
        {
                return getTotalQty();
            }
              if (pos == 6)
        {
                return getVendorId();
            }
              if (pos == 7)
        {
                return getVendorName();
            }
              if (pos == 8)
        {
                return getLocationId();
            }
              if (pos == 9)
        {
                return getRequestedBy();
            }
              if (pos == 10)
        {
                return getApprovedBy();
            }
              if (pos == 11)
        {
                return getRemark();
            }
              if (pos == 12)
        {
                return getCancelBy();
            }
              if (pos == 13)
        {
                return getCancelDate();
            }
              if (pos == 14)
        {
                return getConfirmDate();
            }
              if (pos == 15)
        {
                return getTransferTransId();
            }
              if (pos == 16)
        {
                return getTransferBy();
            }
              if (pos == 17)
        {
                return getTransferDate();
            }
              if (pos == 18)
        {
                return Boolean.valueOf(getIsPurchase());
            }
              if (pos == 19)
        {
                return Boolean.valueOf(getIsReimburse());
            }
              if (pos == 20)
        {
                return Boolean.valueOf(getIsExternal());
            }
              if (pos == 21)
        {
                return getCrossEntityId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseRequestPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseRequestPeer.doInsert((PurchaseRequest) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseRequestPeer.doUpdate((PurchaseRequest) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPurchaseRequestDetails != null)
            {
                for (int i = 0; i < collPurchaseRequestDetails.size(); i++)
                {
                    ((PurchaseRequestDetail) collPurchaseRequestDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseRequestId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPurchaseRequestId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPurchaseRequestId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseRequestId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseRequest copy() throws TorqueException
    {
        return copyInto(new PurchaseRequest());
    }
  
    protected PurchaseRequest copyInto(PurchaseRequest copyObj) throws TorqueException
    {
          copyObj.setPurchaseRequestId(purchaseRequestId);
          copyObj.setTransactionStatus(transactionStatus);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setLastUpdateDate(lastUpdateDate);
          copyObj.setTotalQty(totalQty);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setLocationId(locationId);
          copyObj.setRequestedBy(requestedBy);
          copyObj.setApprovedBy(approvedBy);
          copyObj.setRemark(remark);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setConfirmDate(confirmDate);
          copyObj.setTransferTransId(transferTransId);
          copyObj.setTransferBy(transferBy);
          copyObj.setTransferDate(transferDate);
          copyObj.setIsPurchase(isPurchase);
          copyObj.setIsReimburse(isReimburse);
          copyObj.setIsExternal(isExternal);
          copyObj.setCrossEntityId(crossEntityId);
  
                    copyObj.setPurchaseRequestId((String)null);
                                                                                                                                          
                                      
                            
        List v = getPurchaseRequestDetails();
        for (int i = 0; i < v.size(); i++)
        {
            PurchaseRequestDetail obj = (PurchaseRequestDetail) v.get(i);
            copyObj.addPurchaseRequestDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseRequestPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseRequest\n");
        str.append("---------------\n")
           .append("PurchaseRequestId    : ")
           .append(getPurchaseRequestId())
           .append("\n")
           .append("TransactionStatus    : ")
           .append(getTransactionStatus())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("LastUpdateDate       : ")
           .append(getLastUpdateDate())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("RequestedBy          : ")
           .append(getRequestedBy())
           .append("\n")
           .append("ApprovedBy           : ")
           .append(getApprovedBy())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("ConfirmDate          : ")
           .append(getConfirmDate())
           .append("\n")
           .append("TransferTransId      : ")
           .append(getTransferTransId())
           .append("\n")
           .append("TransferBy           : ")
           .append(getTransferBy())
           .append("\n")
           .append("TransferDate         : ")
           .append(getTransferDate())
           .append("\n")
           .append("IsPurchase           : ")
           .append(getIsPurchase())
           .append("\n")
           .append("IsReimburse          : ")
           .append(getIsReimburse())
           .append("\n")
           .append("IsExternal           : ")
           .append(getIsExternal())
           .append("\n")
           .append("CrossEntityId        : ")
           .append(getCrossEntityId())
           .append("\n")
        ;
        return(str.toString());
    }
}

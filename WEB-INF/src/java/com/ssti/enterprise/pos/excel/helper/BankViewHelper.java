package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.financial.BankBookTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class BankViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = BankTool.getAllBank();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Bank View Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        
		createHeaderCell(oWB, oRow, (short)iCol,"Bank Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Currency Code"); iCol++;				
		createHeaderCell(oWB, oRow, (short)iCol,"Account No"); iCol++;		
		createHeaderCell(oWB, oRow, (short)iCol,"Account Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol,"Current Balance"); iCol++;
	
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Bank oBank = (Bank) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oBank.getBankCode());     iCol++; 
                createCell(oWB, oRow2, (short)iCol, oBank.getDescription());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, CurrencyTool.getCodeByID(oBank.getCurrencyId()));	iCol++;                                
                createCell(oWB, oRow2, (short)iCol, oBank.getAccountNo());	iCol++;            
                createCell(oWB, oRow2, (short)iCol, AccountTool.getCodeByID(oBank.getAccountId())); iCol++;
                createCell(oWB, oRow2, (short)iCol, LocationTool.getCodeByID(oBank.getLocationId()));	iCol++;            
                
                BigDecimal oBal = new BigDecimal(BankBookTool.getBalanceAsOf(oBank.getBankId(),new Date()));                
                createCell(oWB, oRow2, (short)iCol, oBal.toString()); iCol++;
            }        
        }
        return oWB;
    }
}

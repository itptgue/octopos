package com.ssti.framework.print.helper;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.turbine.services.pull.TurbinePull;
import org.apache.turbine.services.rundata.TurbineRunDataFacade;
import org.apache.turbine.util.RunData;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Base class for all TXT & PDF print helper
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BasePrintHelper.java,v 1.21 2008/08/17 02:16:32 albert Exp $ <br>
 *
 * <pre>
 * $Log: BasePrintHelper.java,v $
 * Revision 1.21  2008/08/17 02:16:32  albert
 * *** empty log message ***
 *
 * </pre><br>
 *
 */
public abstract class BasePrintHelper implements PrintHelper, PrintAttributes
{    
    protected String s_PDF_TPL_NAME = "";
    protected String s_TXT_TPL_NAME = "";
    protected String s_HTML_TPL_NAME = "";
    
    protected String s_TPL_PATH = "/print/";
    
    protected static final String s_PARAM_ID   = "id";
    protected static final String s_PARAM_USER = "user_name";
 
    protected static final String s_TXT_TPL    = "txt_tpl";
    protected static final String s_PDF_TPL    = "pdf_tpl";
    protected static final String s_HTML_TPL   = "html_tpl";    
    
    //context var name
    protected static final String s_CTX_TRANS 		 = "trans";
    protected static final String s_CTX_TRANSDET 	 = "transDet";    
    protected static final String s_CTX_CURRENT_PAGE = "current_page";
    protected static final String s_CTX_OF_PAGE 	 = "of_page";    
    protected static final String s_CTX_COMMA_SCALE	 = "commaScale";    

    protected static String  s_EJECT_STRING;  
    protected static String  s_BOLD_ON;  
    protected static String  s_BOLD_OFF;  
    protected static String  s_ITALIC_ON;  
    protected static String  s_ITALIC_OFF; 
    protected static String  s_UDRLINE_ON;  
    protected static String  s_UDRLINE_OFF; 
    protected static String  s_DBLWIDT_ON;  
    protected static String  s_DBLWIDT_OFF; 
    
    protected static String[] s_FONTS;
    
    public static byte[] b_CUTPAPER;  
    
    protected String  s_INIT_STRING;      
    
    //@Override 
    protected byte[] b_PAGE_LENGTH = {33}; //default page length (half folio)
	static
	{
    	try
		{			
    		s_EJECT_STRING = new String (b_PRINTER_FORM_FEED_CHAR, s_ASCII_CHARSET);
    		s_BOLD_ON = new String (b_PRINTER_BOLD_ON, s_ASCII_CHARSET);
    		s_BOLD_OFF = new String (b_PRINTER_BOLD_OFF, s_ASCII_CHARSET);
    		s_ITALIC_ON = new String (b_PRINTER_ITALIC_ON, s_ASCII_CHARSET);
    		s_ITALIC_OFF = new String (b_PRINTER_ITALIC_OFF, s_ASCII_CHARSET);
    		s_UDRLINE_ON = StringUtil.createString(ESC,45,49,0,0,0);
            s_UDRLINE_OFF = StringUtil.createString(ESC,45,48,0,0,0);
            s_DBLWIDT_ON = StringUtil.createString(ESC,87,49,0,0,0);
            s_DBLWIDT_OFF = StringUtil.createString(ESC,87,48,0,0,0);

            s_FONTS = new String[127];
            for (int i = 0; i < s_FONTS.length; i++)
            {
                s_FONTS[i] = StringUtil.createString(ESC,33,i,0,0,0);
            }           
		}
    	catch(Exception ex){ex.printStackTrace();}
	}
    
    protected VelocityContext oCtx = null;
    protected StringWriter oWriter = null;
    protected static RunData data = null;
    
	public void init (HttpServletRequest request, HttpServletResponse response, ServletConfig config)
	{
		try
		{
			StringBuilder init = new StringBuilder()
				.append (new String (b_PRINTER_ESC_P_CHAR, s_ASCII_CHARSET))
				.append (new String (b_PRINTER_PAGE_MODE_CHAR, s_ASCII_CHARSET))
				.append (new String (b_PAGE_LENGTH, s_ASCII_CHARSET))
				.append (new String (b_PRINTER_FONT_MODE_CHAR, s_ASCII_CHARSET));
		
			s_INIT_STRING = init.toString();

			data = TurbineRunDataFacade.getRunData(request, response, config);
			prepareContext();
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
		}
	}
    
 	protected BasePrintHelper () 
 	{
    	try 
		{	    	
			oWriter = new StringWriter ();
 		}
 		catch (Exception _oEx) {
 			_oEx.printStackTrace ();
 		}
 	}   
 	
 	/**
 	 * prepare tools to use in context 
 	 * 
 	 * @throws Exception
 	 */
    private void prepareContext()
    	throws Exception
    {
		//prepare tool that we would like to have in context
		oCtx = new VelocityContext();
		
		Context oGlobal = TurbinePull.getGlobalContext();		
		for (int i = 0; i < oGlobal.getKeys().length; i++)
		{
			String sKey = (String) oGlobal.getKeys()[i];
			oCtx.put(sKey, oGlobal.get(sKey));
		}		
		TurbinePull.populateContext(oCtx, data); 
				
		//put company object in context
		oCtx.put("data", data);
		oCtx.put("INIT", s_INIT_STRING);
		oCtx.put("EJECT", s_EJECT_STRING);
		
		oCtx.put("BON",  s_BOLD_ON);
		oCtx.put("BOFF", s_BOLD_OFF);		
		oCtx.put("ION",  s_ITALIC_ON);
		oCtx.put("IOFF", s_ITALIC_OFF);
        oCtx.put("UON",  s_UDRLINE_ON);
        oCtx.put("UOFF", s_UDRLINE_OFF);
        
        if (s_FONTS != null)
        {
            for (int i = 0; i < s_FONTS.length; i++)
            {
                oCtx.put("FONT" + i, s_FONTS[i]);
            }
        }
    }

    /**
     * get TXT page to display
     * 
     * @param _oTR
     * @param _vTD
     * @param _iDetailPerPage
     * @param _sTPL
     * @return current page
     * @throws Exception
     */
    public String getPage (Object _oTR, List _vTD, int _iDetailPerPage, String _sTPL)	
		throws Exception
	{
		//prepare a buffer for the merged templates
		StringBuilder oSB = new StringBuilder ();
		
		if (_oTR != null && _vTD != null)
		{
			//get number of transaction details
			int iNumOfTDet = _vTD.size();
			
			//get number of page
			int iNumOfPage = iNumOfTDet / _iDetailPerPage;
			
			//get number of td left in next page
			int iLeft = iNumOfTDet % _iDetailPerPage;
			
			//if there are some left details, prepare a page for them
			if (iLeft > 0) iNumOfPage = iNumOfPage + 1;
			
			int iUpper = 0;
			int iLower = 0;
			if (iNumOfTDet <= _iDetailPerPage) 
			{
				iUpper = iNumOfTDet - 1;
			}
			else 
			{
				iUpper = _iDetailPerPage - 1;
			}
			for (int i = 0; i < iNumOfPage; i++) 
			{ 
		    	oCtx.put(s_CTX_TRANS, 	  	   _oTR);
		    	oCtx.put(s_CTX_CURRENT_PAGE,   Integer.valueOf(i + 1));
		    	oCtx.put(s_CTX_OF_PAGE,  	   Integer.valueOf(iNumOfPage));
		    	oCtx.put(s_CTX_TRANSDET,  	   getTransDetElement(_vTD, iLower, iUpper));
		    	oCtx.put(s_DETAIL_PER_PAGE,	   Integer.valueOf(_iDetailPerPage));
		    	
		    	Velocity.mergeTemplate(_sTPL, s_DEFAULT_ENCODING, oCtx, oWriter );
		    	oSB.append(s_INIT_STRING);
				oSB.append(oWriter.toString());		
		    	oSB.append(s_EJECT_STRING);
				
				//refresh the stringwriter, or else the previous merged template will be included
				oWriter = new StringWriter();
				iLower = iUpper + 1;
				
				//if this is the last page
				if (((i + 1)== (iNumOfPage - 1)) && (iLeft > 0))
				{
					iUpper += iLeft;
				}
				else 
				{
					iUpper += _iDetailPerPage ;
				}
			}
		}
		return oSB.toString();
	}

    protected List getTransDetElement (List _vTD, int iLower, int iUpper)
    {
		List vElement = new ArrayList ((iUpper - iLower) + 1);
		for (int i = iLower; i <= iUpper; i++)
		{
			vElement.add (_vTD.get(i));
		}
		return vElement;
	}
    
    /**
     * method to check whether template name is specified in Param
     * 
     * @param _oParam Map of servlet parameters 
     * @throws Exception
     */
    protected void checkTemplate(Map _oParam)
    	throws Exception
    {		            	
    	String sTXTTpl = getString(_oParam, s_TXT_TPL);
    	if (StringUtil.isNotEmpty(sTXTTpl))
    	{
    		if (!sTXTTpl.contains(s_TPL_PATH))
    		{
    			sTXTTpl = s_TPL_PATH + sTXTTpl;
    		}
    		s_TXT_TPL_NAME = sTXTTpl;
    	}
    	
    	String sPDFTpl = getString(_oParam, s_PDF_TPL);
    	if (StringUtil.isNotEmpty(sPDFTpl))
    	{
    		if (!sPDFTpl.contains(s_TPL_PATH))
    		{
    			sPDFTpl = s_TPL_PATH + sPDFTpl;
    		}				
    		s_PDF_TPL_NAME = sPDFTpl;
    	}
    	
    	String sHTMLTpl = getString(_oParam, s_HTML_TPL);
    	if (StringUtil.isNotEmpty(sHTMLTpl))
    	{
    		if (!sHTMLTpl.contains(s_TPL_PATH))
    		{
    			sHTMLTpl = s_TPL_PATH + sHTMLTpl;
    		}				
    		s_HTML_TPL_NAME = sHTMLTpl;
    	}    	
    } 
    
    public String getString (Map _oParam, String _sKey)
    {
		return StringUtil.getString(_oParam, _sKey);
    }
    
    public String appendInitEject(String _sDoc)
    {
    	StringBuilder sb = new StringBuilder(s_INIT_STRING);
    	sb.append(_sDoc);
    	sb.append(s_EJECT_STRING);
    	return sb.toString();
    }
    
    public String cleanInitEject(String _sDoc)
    {
    	String sDoc = "";
    	if (StringUtil.isNotEmpty(_sDoc))
    	{
	    	sDoc = _sDoc.replace(s_INIT_STRING, "");
	    	sDoc = sDoc.replace(s_BOLD_ON,"");
	    	sDoc = sDoc.replace(s_BOLD_OFF,"");
	    	sDoc = sDoc.replace(s_ITALIC_ON,"");
	    	sDoc = sDoc.replace(s_ITALIC_OFF,"");
	    	sDoc = sDoc.replace(s_EJECT_STRING, "");
            
            if (s_FONTS != null)
            {
                for (int i = 0; i < s_FONTS.length; i++)
                {
                    sDoc = sDoc.replace(s_FONTS[i],"");
                }
            }            
        }
    	return sDoc;
    }
}

package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerPointMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerPointMapBuilder";

    /**
     * Item
     * @deprecated use CustomerPointPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_point";
    }

  
    /**
     * customer_point.CUSTOMER_POINT_ID
     * @return the column name for the CUSTOMER_POINT_ID field
     * @deprecated use CustomerPointPeer.customer_point.CUSTOMER_POINT_ID constant
     */
    public static String getCustomerPoint_CustomerPointId()
    {
        return "customer_point.CUSTOMER_POINT_ID";
    }
  
    /**
     * customer_point.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerPointPeer.customer_point.CUSTOMER_ID constant
     */
    public static String getCustomerPoint_CustomerId()
    {
        return "customer_point.CUSTOMER_ID";
    }
  
    /**
     * customer_point.TOTAL_POINT
     * @return the column name for the TOTAL_POINT field
     * @deprecated use CustomerPointPeer.customer_point.TOTAL_POINT constant
     */
    public static String getCustomerPoint_TotalPoint()
    {
        return "customer_point.TOTAL_POINT";
    }
  
    /**
     * customer_point.LAST_UPDATE
     * @return the column name for the LAST_UPDATE field
     * @deprecated use CustomerPointPeer.customer_point.LAST_UPDATE constant
     */
    public static String getCustomerPoint_LastUpdate()
    {
        return "customer_point.LAST_UPDATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_point");
        TableMap tMap = dbMap.getTable("customer_point");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_point.CUSTOMER_POINT_ID", "");
                          tMap.addColumn("customer_point.CUSTOMER_ID", "");
                            tMap.addColumn("customer_point.TOTAL_POINT", bd_ZERO);
                          tMap.addColumn("customer_point.LAST_UPDATE", new Date());
          }
}

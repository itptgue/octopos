package com.ssti.enterprise.pos.presentation.actions.company;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureAction;

public class CompanySecureAction extends SecureAction
{
	private static final String[] a_PERM = {"Setup Master Data", "Update Master Data", "View Master Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }	
}

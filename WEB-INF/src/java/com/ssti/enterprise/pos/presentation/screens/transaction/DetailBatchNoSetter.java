package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;


/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/DetailBatchNoSetter.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: DetailBatchNoSetter.java,v 1.1 2009/05/04 02:02:36 albert Exp $
 *
 * $Log: DetailBatchNoSetter.java,v $
 * Revision 1.1  2009/05/04 02:02:36  albert
 * *** empty log message ***
 *
 * 
 */

public class DetailBatchNoSetter extends TransactionSecureScreen 
{
	public void doBuildTemplate(RunData data, Context context)
	{
		super.doBuildTemplate(data, context);
	}	
}

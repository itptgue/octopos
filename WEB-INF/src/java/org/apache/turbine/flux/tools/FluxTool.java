package org.apache.turbine.flux.tools;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.torque.util.Criteria;
import org.apache.turbine.Turbine;
import org.apache.turbine.om.security.Group;
import org.apache.turbine.om.security.Permission;
import org.apache.turbine.om.security.Role;
import org.apache.turbine.om.security.User;
import org.apache.turbine.om.security.peer.PermissionPeer;
import org.apache.turbine.om.security.peer.TurbineUserPeer;
import org.apache.turbine.services.pull.ApplicationTool;
import org.apache.turbine.services.security.TurbineSecurity;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.pool.Recyclable;
import org.apache.turbine.util.security.AccessControlList;
import org.apache.turbine.util.template.SelectorBox;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: FluxTool.java,v 1.9 2008/02/26 05:18:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: FluxTool.java,v $
 * Revision 1.9  2008/02/26 05:18:15  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/05/30 05:15:17  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class FluxTool
    implements Recyclable, ApplicationTool
{
    private RunData data;
    private Group group;
    private Role role;
    private Permission permission;
    private User user;
    private boolean disposed;
    
    public static Configuration CONFIG = Turbine.getConfiguration();
    public static String s_ADMIN = "Administrator";
    
	public void init(Object _data)
	{
		this.data = (RunData)_data;
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public void recycle()
	{
		disposed = false;
	}

	public void refresh()
	{
	}

    public FluxTool()
    {
        group = null;
        role = null;
        permission = null;
        user = null;
    }

    public void dispose()
    {
        data = null;
        user = null;
        group = null;
        role = null;
        permission = null;
        disposed = true;
    }

    public AccessControlList getACL()
        throws Exception
    {
        return TurbineSecurity.getACL(getUser());
    }

    public SelectorBox getFieldList()
        throws Exception
    {
        Object names[] = {
            "username", "firstname", "middlename", "lastname"
        };
        Object values[] = {
            "Username", "First Name", "Middle Name", "Last Name"
        };
        return new SelectorBox("fieldList", names, values);
    }

    public Group getGroup()
        throws Exception
    {
        if(group == null)
        {
            String name = data.getParameters().getString("name");
            if(name == null || name.length() == 0)
                group = TurbineSecurity.getGroupInstance(null);
            else
                group = TurbineSecurity.getGroupByName(name);
        }
        return group;
    }

    public Group[] getGroups()
        throws Exception
    {
        return TurbineSecurity.getAllGroups().getGroupsArray();
    }
    
    public List getGroupList()
	    throws Exception
	{
	    return Arrays.asList(TurbineSecurity.getAllGroups().getGroupsArray());
	}
    
    public String getMode()
    {
        return data.getParameters().getString("mode","");
    }

    public String getSort()
    {
        return data.getParameters().getString("sort","");
    }

    public int getACDC()
    {
    	if (data.getParameters().getInt("ascdsc") == 1)
    	{
    		return 2;
    	}
    	return 1;
    }

    public int getASCP()
    {
    	return data.getParameters().getInt("ascdsc",1);
    }
    
    public Permission getPermission()
        throws Exception
    {
        if(permission == null)
        {
            String name = data.getParameters().getString("name");
            if(name == null || name.length() == 0)
                permission = TurbineSecurity.getPermissionInstance(null);
            else
                permission = TurbineSecurity.getPermissionByName(name);
        }
        return permission;
    }

    public Permission[] getPermissions()
        throws Exception
    {
        return TurbineSecurity.getAllPermissions().getPermissionsArray();
    }

    public List getPermissionList()
	    throws Exception
	{
	    return Arrays.asList(TurbineSecurity.getAllPermissions().getPermissionsArray());
	}
    
    public List getAllPermissions()
	    throws Exception
	{
    	return PermissionPeer.doSelect(new Criteria());
	}    
    
    public Role getRole()
        throws Exception
    {
        if(role == null)
        {
            String name = data.getParameters().getString("name"); 
            if(name == null || name.length() == 0)
                role = TurbineSecurity.getRoleInstance(null);
            else
                role = TurbineSecurity.getRoleByName(name);
        }
        return role;
    }

    public Role[] getRoles()
        throws Exception
    {
        return TurbineSecurity.getAllRoles().getRolesArray();
    }

    public List getRoleList()
	    throws Exception
	{
	    return Arrays.asList(TurbineSecurity.getAllRoles().getRolesArray());
	}
    
    public User getUser()
        throws Exception
    {
        if(user == null)
        {
            String name = data.getParameters().getString("username");
            if(name == null || name.length() == 0)
                user = TurbineSecurity.getUserInstance();
            else
                user = TurbineSecurity.getUser(name);
        }
        return user;
    }

    public SelectorBox getUserFieldList()
        throws Exception
    {
        Object names[] = {
            TurbineUserPeer.USERNAME, TurbineUserPeer.FIRST_NAME, TurbineUserPeer.LAST_NAME
        };
        Object values[] = {
            "User Name", "First Name", "Last Name"
        };
        return new SelectorBox("fieldList", names, values);
    }

    public User[] getUsers()
        throws Exception
    {
        Criteria criteria = new Criteria();
        String fieldList = data.getParameters().getString("fieldList");
        if(fieldList != null)
        {
            String searchField = data.getParameters().getString("searchField");
            criteria.add(fieldList, searchField, (Object)Criteria.LIKE);
        }
        return TurbineSecurity.getUsers(criteria);
    }

    public List getUserList()
	    throws Exception
	{
	    return Arrays.asList(TurbineSecurity.getUsers(new Criteria()));
	}
}
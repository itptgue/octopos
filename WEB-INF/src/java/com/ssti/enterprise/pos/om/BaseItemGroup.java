package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
    
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemGroup
 */
public abstract class BaseItemGroup extends BaseObject
{
    /** The Peer class */
    private static final ItemGroupPeer peer =
        new ItemGroupPeer();

        
    /** The value for the itemGroupId field */
    private String itemGroupId;
      
    /** The value for the groupId field */
    private String groupId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
                                                
          
    /** The value for the cost field */
    private BigDecimal cost= bd_ZERO;
  
    
    /**
     * Get the ItemGroupId
     *
     * @return String
     */
    public String getItemGroupId()
    {
        return itemGroupId;
    }

                        
    /**
     * Set the value of ItemGroupId
     *
     * @param v new value
     */
    public void setItemGroupId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemGroupId, v))
              {
            this.itemGroupId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GroupId
     *
     * @return String
     */
    public String getGroupId()
    {
        return groupId;
    }

                              
    /**
     * Set the value of GroupId
     *
     * @param v new value
     */
    public void setGroupId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.groupId, v))
              {
            this.groupId = v;
            setModified(true);
        }
    
                                                                  
                if (aItemRelatedByGroupId != null && !ObjectUtils.equals(aItemRelatedByGroupId.getItemId(), v))
                {
            aItemRelatedByGroupId = null;
        }
      
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                              
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
                                                                  
                if (aItemRelatedByItemId != null && !ObjectUtils.equals(aItemRelatedByItemId.getItemId(), v))
                {
            aItemRelatedByItemId = null;
        }
      
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Cost
     *
     * @return BigDecimal
     */
    public BigDecimal getCost()
    {
        return cost;
    }

                        
    /**
     * Set the value of Cost
     *
     * @param v new value
     */
    public void setCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.cost, v))
              {
            this.cost = v;
            setModified(true);
        }
    
          
              }
  
      
    
                        
        
        private Item aItemRelatedByGroupId;

    /**
     * Declares an association between this object and a Item object
     *
     * @param v Item
     * @throws TorqueException
     */
    public void setItemRelatedByGroupId(Item v) throws TorqueException
    {
            if (v == null)
        {
                  setGroupId((String) null);
              }
        else
        {
            setGroupId(v.getItemId());
        }
            aItemRelatedByGroupId = v;
    }

                                            
    /**
     * Get the associated Item object
     *
     * @return the associated Item object
     * @throws TorqueException
     */
    public Item getItemRelatedByGroupId() throws TorqueException
    {
        if (aItemRelatedByGroupId == null && (!ObjectUtils.equals(this.groupId, null)))
        {
                          aItemRelatedByGroupId = ItemPeer.retrieveByPK(SimpleKey.keyFor(this.groupId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Item obj = ItemPeer.retrieveByPK(this.groupId);
               obj.addItemGroupsRelatedByGroupId(this);
            */
        }
        return aItemRelatedByGroupId;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setItemRelatedByGroupIdKey(ObjectKey key) throws TorqueException
    {
      
                        setGroupId(key.toString());
                  }
    
    
                        
        
        private Item aItemRelatedByItemId;

    /**
     * Declares an association between this object and a Item object
     *
     * @param v Item
     * @throws TorqueException
     */
    public void setItemRelatedByItemId(Item v) throws TorqueException
    {
            if (v == null)
        {
                  setItemId((String) null);
              }
        else
        {
            setItemId(v.getItemId());
        }
            aItemRelatedByItemId = v;
    }

                                            
    /**
     * Get the associated Item object
     *
     * @return the associated Item object
     * @throws TorqueException
     */
    public Item getItemRelatedByItemId() throws TorqueException
    {
        if (aItemRelatedByItemId == null && (!ObjectUtils.equals(this.itemId, null)))
        {
                          aItemRelatedByItemId = ItemPeer.retrieveByPK(SimpleKey.keyFor(this.itemId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Item obj = ItemPeer.retrieveByPK(this.itemId);
               obj.addItemGroupsRelatedByItemId(this);
            */
        }
        return aItemRelatedByItemId;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setItemRelatedByItemIdKey(ObjectKey key) throws TorqueException
    {
      
                        setItemId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemGroupId");
              fieldNames.add("GroupId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("Qty");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("Cost");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemGroupId"))
        {
                return getItemGroupId();
            }
          if (name.equals("GroupId"))
        {
                return getGroupId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("Cost"))
        {
                return getCost();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemGroupPeer.ITEM_GROUP_ID))
        {
                return getItemGroupId();
            }
          if (name.equals(ItemGroupPeer.GROUP_ID))
        {
                return getGroupId();
            }
          if (name.equals(ItemGroupPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemGroupPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(ItemGroupPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(ItemGroupPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(ItemGroupPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(ItemGroupPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(ItemGroupPeer.COST))
        {
                return getCost();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemGroupId();
            }
              if (pos == 1)
        {
                return getGroupId();
            }
              if (pos == 2)
        {
                return getItemId();
            }
              if (pos == 3)
        {
                return getItemCode();
            }
              if (pos == 4)
        {
                return getItemName();
            }
              if (pos == 5)
        {
                return getQty();
            }
              if (pos == 6)
        {
                return getUnitId();
            }
              if (pos == 7)
        {
                return getUnitCode();
            }
              if (pos == 8)
        {
                return getCost();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemGroupPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemGroupPeer.doInsert((ItemGroup) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemGroupPeer.doUpdate((ItemGroup) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemGroupId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemGroupId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemGroupId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemGroupId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemGroup copy() throws TorqueException
    {
        return copyInto(new ItemGroup());
    }
  
    protected ItemGroup copyInto(ItemGroup copyObj) throws TorqueException
    {
          copyObj.setItemGroupId(itemGroupId);
          copyObj.setGroupId(groupId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setQty(qty);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setCost(cost);
  
                    copyObj.setItemGroupId((String)null);
                                                            
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemGroupPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemGroup\n");
        str.append("---------\n")
           .append("ItemGroupId          : ")
           .append(getItemGroupId())
           .append("\n")
           .append("GroupId              : ")
           .append(getGroupId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("Cost                 : ")
           .append(getCost())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.framework.tools;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.services.pool.TurbinePool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Reflection based utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BeanUtil.java,v 1.12 2009/05/04 01:29:39 albert Exp $ <br>
 * 
 * <pre>
 * $Log: BeanUtil.java,v $
 *
 * 2015-06-16
 * add method getMethodName: getMethodName(depth) to get stack trace method callings
 *
 * 2015-06-16
 * add method getInstance: template helper to execute getInstance method on singleton classes
 * </pre><br>
 */
public class BeanUtil
{    		
	private static Log log = LogFactory.getLog(BeanUtil.class);

	/**
	 * template helper to load an object by it's class name
	 * 
	 * @param _sClassName
	 * @return new object instance from class
	 */
	public static Object loadClass(String _sClassName)
	{
		try
		{
			Object oObj = null;
			try
			{
				oObj = TurbinePool.getInstance(_sClassName);
			}
			catch (Exception ex)
			{
				System.err.println("Turbine not instantiated properly ");				
			}	
			if (oObj != null) 
			{
				return oObj;
			}
			else
			{
				Class oClass = Class.forName(_sClassName);
				Object oInstance = oClass.newInstance(); 
				TurbinePool.putInstance(oInstance);
				return oInstance;
			}
		}
		catch (ClassNotFoundException _oCNFEx)
		{
			System.err.println("Class " + _sClassName + " Not Found : " + _oCNFEx.getMessage());
			_oCNFEx.printStackTrace();
		} 
		catch (InstantiationException _oINEx) 
		{
			System.err.println("Failed Instantiating " + _sClassName + " ERROR : " + _oINEx.getMessage());
			_oINEx.printStackTrace();
		} 
		catch (IllegalAccessException _oIAEx) 
		{
			System.err.println("Illegal Access To " + _sClassName + " ERROR : " + _oIAEx.getMessage());
			_oIAEx.printStackTrace();
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
		}
		return null;
	}
	
	/**
	 * template helper to execute getInstance method on singleton classes
	 * 
	 * @param _sClassName
	 * @return
	 */
	public static Object getInstance(String _sClassName)
	{
		try
		{
			Class oClass = Class.forName(_sClassName);
			Method m = oClass.getMethod("getInstance",null);
			return m.invoke(null, null);
		}
		catch (ClassNotFoundException _oCNFEx)
		{
			System.err.println("Class " + _sClassName + " Not Found : " + _oCNFEx.getMessage());
			_oCNFEx.printStackTrace();
		} 
		catch (IllegalAccessException _oIAEx) 
		{
			System.err.println("Illegal Access To " + _sClassName + " ERROR : " + _oIAEx.getMessage());
			_oIAEx.printStackTrace();
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
		}
		return null;
	}
	
    /**
     * Uses bean introspection to set writable properties of bean from
     * another bean, where a (case-insensitive) name match between
     * the bean property and the parameter is looked for.
     *
     * @param _oSource An Object.
     * @param _oTarget An Object.
     * @exception Exception a generic exception.
     */
     
    public static void setBean2BeanProperties (Object _oSource, Object _oTarget) 
    	throws Exception
    {
    	String sTargetProp = "";
    	if (_oSource != null && _oTarget != null)
    	{
	        try 
			{
	        	Class cSourceBeanClass = _oSource.getClass();
	        	
	        	PropertyDescriptor[] aSourceProps
	        	        = Introspector.getBeanInfo(cSourceBeanClass).getPropertyDescriptors();
	        	
	        	Class cTargetBeanClass = _oTarget.getClass();
	
	        	PropertyDescriptor[] aTargetProps
	        	        = Introspector.getBeanInfo(cTargetBeanClass).getPropertyDescriptors();
	        	        	
	        	for (int i = 0; i < aTargetProps.length; i++)
	        	{
	        	    sTargetProp = aTargetProps[i].getName();
	        	    if (!sTargetProp.equals("class"))
	        	    {
	        	    	//log.debug ("Target Field : " + i + " : "  + sTargetProp);
	        	    	Method mTargetSetter = aTargetProps[i].getWriteMethod();
	        	    	//log.debug ("Target Field Setter Method : " + mTargetSetter.getName());
						PropertyDescriptor oSourceProp = getPropertyDescriptorByName(aSourceProps, sTargetProp);
	        	    	//log.debug ("Source Property Descriptor Field Name : " + oSourceProp.getName());
	        	    	Method mSourceGetter = null;
	        	    	if (oSourceProp != null) mSourceGetter = oSourceProp.getReadMethod();
	        	    	//log.debug ("Source Field GetterMethod : " + mSourceGetter.getName());
	        	    	
	        	   		Object[] aTargetParams = {null};
	        	    	
	        	    	if (mTargetSetter != null && mSourceGetter != null)
	        	    	{
	        	   			aTargetParams[0] = mSourceGetter.invoke (_oSource, (Object[])null);
	        	   			
				    	   	//log.debug ("Source Field GetterMethod INVOKE Result : " + aTargetParams[0] );
	        	    	
							mTargetSetter.invoke(_oTarget, aTargetParams);
	        	    		            	
				    	   	//log.debug ("Target Field SetterMethod INVOKED ");
	        	    	}
	        		}
	        	}
	    	}
	        catch (IllegalArgumentException _oIAEx)
	        {
	    		log.warn (" Set Bean " + _oSource.getClass().getName() +  
	    							 " To Bean "  + _oTarget.getClass().getName() + 
	    							 " Field "  + sTargetProp +
	    							 " Properties Failed, ERROR : " + _oIAEx.getMessage(), _oIAEx);
	        	
	        }
	    	catch (Exception _oEx)
	    	{
	    		log.error(_oEx);
	    		_oEx.printStackTrace();
	    		throw new Exception (" Set Bean " + _oSource.getClass().getName() +  
	    							 " To Bean "  + _oTarget.getClass().getName() + 
	    							 " Field "  + sTargetProp +
	    							 " Properties Failed, ERROR : " + _oEx.getMessage(), _oEx);
	    	}
    	}
    }
    
    /**
     * get Descriptor By Name
     * 
     * @param _aSourceProps
     * @param _sName
     * @return property desriptio
     */
    public static PropertyDescriptor getPropertyDescriptorByName (PropertyDescriptor[] _aSourceProps, String _sName)
    {
    	for (int i = 0; i < _aSourceProps.length; i++)
    	{
			if (_aSourceProps[i].getName() != null) 
    		{
    			//log.debug(_aSourceProps[i].getName());
    			if (_aSourceProps[i].getName().equals(_sName)) {
    				return _aSourceProps[i];
    			}
    		}	
    	}
    	return null;
    }

    /**
     * filter list by certain field
     * 
     * @param _vData
     * @param _sDistinctField
     * @return distinct list 
     */
    public static List getDistinctListByField (List _vData, String _sDistinctField)
		throws Exception	
    {
    	if (_vData != null)
    	{
	    	List vResult = new ArrayList (_vData.size());
		    for (int i = 0; i < _vData.size(); i++)
		    {
		    	Object oData = _vData.get(i);
		    	if (!isExist (vResult, oData, _sDistinctField))
		    	{
		    		vResult.add(oData);
		    	}
		    }
	    	return vResult;
    	}
    	return null;
    }
    
    /**
     * method to check whether object with certain field value (i.e LocationId) 
     * already exist in the List which passed as parameter
     * 
     * @param _vResult
     * @param _oDataToCheck
     * @param _sFieldName
     * @return whether object is exist
     * @throws IntrospectionException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
	public static boolean isExist (List _vResult, Object _oDataToCheck, String _sFieldName) 
		throws Exception	
	{
		for (int i = 0; i < _vResult.size(); i++) 
		{
			Object oDataInList = _vResult.get(i);			
			Object oDataInListValue  = invokeGetter (oDataInList, _sFieldName);
			Object oDataToCheckValue = invokeGetter (_oDataToCheck, _sFieldName);
			if (oDataInListValue != null && oDataInListValue.equals(oDataToCheckValue)) 
	   		{
	   			return true;
	   		}
    	}
		return false;
	}
	
	/**
	 * filter List of object by supplied field name and field value
	 *  
	 * @param _vData
	 * @param _sFieldName
	 * @param _sFieldValue
	 * @return filtered list
	 * @throws Exception
	 */
	public static List filterListByFieldValue (List _vData, String _sFieldName, Object _sFieldValue) 
		throws Exception
	{ 		
		List vResult = new ArrayList(_vData); 

		Object oData = null;
		Iterator oIter = vResult.iterator();
		while (oIter.hasNext()) 
		{
			oData = oIter.next();
			Object oResult = invokeGetter (oData, _sFieldName);
			if (oResult != null && !oResult.equals(_sFieldValue))
			{	
				oIter.remove();				
			}				
		}		
		return vResult;
	}		
	
	/**
	 * sum field value in list of object
	 *  
	 * @param _vData list of object
	 * @param _sFieldName field we'd like to sum up
	 * @return total sum up result
	 * @throws Exception
	 */
	public static double sumUp (List _vData, String _sFieldName) 
		throws Exception
	{ 
		double dTotal = 0;
		
		for (int i = 0; i < _vData.size(); i++)
		{
			Object oResult = invokeGetter (_vData.get(i), _sFieldName);
			if (oResult != null && (oResult instanceof Number))
			{	
				dTotal += ((Number) oResult).doubleValue();
			}
		}
		return dTotal;
	}	

	/**
	 * invoke getter method on an object
	 * 
	 * @param _oData
	 * @param _sFieldName
	 * @return method invoke result
	 */
    public static Object invokeGetter (Object _oData, String _sFieldName) 
    {
    	PropertyDescriptor oProperty = null;
    	Method mGetter = null;
    	try
		{
    		Class cData = _oData.getClass();
			PropertyDescriptor[] aProperties = Introspector.getBeanInfo(cData).getPropertyDescriptors();
			oProperty = getPropertyDescriptorByName(aProperties, _sFieldName);
			if (oProperty != null)
			{
				mGetter = oProperty.getReadMethod();	
				Object[] aInvokeResult = {null};
				if (mGetter != null) 
				{
					aInvokeResult[0] = mGetter.invoke( _oData, (Object[])null);				
					return aInvokeResult[0];
				}	
			}
			else
			{
				log.warn("Property " + _sFieldName + " Not Found in " + _oData);
			}
		}
		catch (IntrospectionException _oIEx)
		{
			log.error("ERROR Interospecting Data : " + _oData, _oIEx);
		}
		catch (IllegalAccessException _oIAEx)
		{
			log.error("Illegal Access to Property : " + oProperty + " of : " + _oData,_oIAEx);
		}
		catch (InvocationTargetException _oITEx)
		{
			log.error("ERROR Invoking Method : " + mGetter + " in : " + _oData, _oITEx);
		}		
		return null;
    }

    /**
     * set property of a field 
     * 
     * @param _oData
     * @param _sField
     * @param _oValue
     * @throws Exception
     */
    public static void setProperty (Object _oData, String _sField, Object _oValue) 
	{
		String sFieldName = "";
		String sClass = "";
		if (_oData != null && StringUtil.isNotEmpty(_sField) && _oValue != null)
		{
	        try 
			{
	        	Class cClass = _oData.getClass();
	        	PropertyDescriptor[] aProps = Introspector.getBeanInfo(cClass).getPropertyDescriptors();	        	
	        	for (int i = 0; i < aProps.length; i++)
	        	{
	        		PropertyDescriptor propDesc = aProps[i];
	        	    sFieldName = propDesc.getName();	        	    
	        	    if (!sFieldName.equals("class") && sFieldName.equalsIgnoreCase(_sField))
	        	    {	        	    	
	        	    	Method setterMethod = propDesc.getWriteMethod();	
	        	    	Class paramClass = propDesc.getPropertyType();
	        	    	sClass = paramClass.getCanonicalName();
	        	    	
	        	    	log.debug("Field Name " + sFieldName);
	        	    	log.debug("Field Class " + sClass);
	
	        	    	if (paramClass != null && StringUtil.isEqual(sClass, "java.util.Date"))
	        	    	{
	        	    		if (_oValue instanceof String)
	        	    		{
		        	    		_oValue = CustomParser.parseDate(_oValue.toString());	        	    			
	        	    		}
	        	    	}	        	    	
	        	    	Object[] aTargetParams = {_oValue};	        	    	
	        	    	if (setterMethod != null)
	        	    	{	        	   				        	    	
							setterMethod.invoke(_oData, aTargetParams);
	        	    	}
	        		}
	        	}
	    	}
	    	catch (Exception _oEx)
	    	{	    		
	    		_oEx.printStackTrace();
	    		log.error (" setProperties " + _oData.getClass().getName() +  
	    				   " Field "  + sFieldName + " Class " + sClass + " Value " + _oValue + 
	    				   " Failed, ERROR : " + _oEx.getMessage(), _oEx);
	    	}
		}
	}
	    
    /**
     * check whether field exists in Object
     * 
     * @param _oData
     * @param _sFieldName
     * @return whether field exists in Object
     */
    public static boolean hasField (Object _oData, String _sFieldName) 
    {
    	if (_oData != null)
    	{
	    	PropertyDescriptor oProperty = null;
	    	try
			{
	    		Class cData = _oData.getClass();
				PropertyDescriptor[] aProperties = Introspector.getBeanInfo(cData).getPropertyDescriptors();
				oProperty = getPropertyDescriptorByName(aProperties, _sFieldName);
				if (oProperty != null)
				{
					return true;
				}
			}
			catch (IntrospectionException _oIEx)
			{
				log.error("ERROR Interospecting Data : " + _oData, _oIEx);
			}
			catch (Exception _oEx)
			{
				log.error("ERROR : "  + _oData, _oEx);
			}		
    	}
		return false;
    }    
    
    public static void setBDScale (int _iScale, List _v)
    {
    	setBDScale(_iScale, _v, null);
    }
    
    public static void setBDScale (int _iScale, List _v, String[] _skipFields)
    {
    	if (_v != null)
    	{
	    	for (int i = 0; i < _v.size(); i++)
	    	{	
	    		setBDScale(_iScale, _v.get(i), _skipFields);
	    	}
    	}
    }
    
    public static void setBDScale (int _iScale, Object _o)
    {
    	setBDScale(_iScale, _o, null);
    }
    
    /**
     * set BigDecimal field scale in objects
     * 
     * @param _iScale
     * @param _o
     */
    public static void setBDScale (int _iScale, Object _o, String[] _skipFields)
    {
    	if (_o != null)
    	{
	    	PropertyDescriptor oProperty = null;
	    	try
			{
	    		Class c = _o.getClass();
				PropertyDescriptor[] aProps = Introspector.getBeanInfo(c).getPropertyDescriptors();
		    	for (int i = 0; i < aProps.length; i++)
		    	{
		    		if (aProps[i] != null && BigDecimal.class.equals(aProps[i].getPropertyType()))
		    		{
		    			boolean bSkip = false;
			    		if (_skipFields != null)
			    		{
			    			bSkip = validateSkip(aProps[i].getName(), _skipFields);
			    		}
			    		if (!bSkip)
			    		{
			    			BigDecimal bd = (BigDecimal) aProps[i].getReadMethod().invoke(_o, null);
				    		if (bd != null) bd = bd.setScale(_iScale, BigDecimal.ROUND_HALF_UP);
				    		aProps[i].getWriteMethod().invoke(_o, bd);
				    		
				    		if (log.isDebugEnabled())
				    		{
				    			//log.debug(aProps[i].getName() + " " + aProps[i].getPropertyType() + " " + bd);
				    		}
			    		}
		    		}
		    	}
			}
			catch (Exception _oEx)
			{
				log.error("ERROR : "  + _o, _oEx);
			}
    	}
    }

	private static boolean validateSkip(String _sFieldName, String[] _aSkipFields) 
	{
		for (int i = 0; i < _aSkipFields.length; i++)
		{
			if (StringUtil.isEqual(_aSkipFields[i],_sFieldName))
			{
				log.debug("Skip set scale on field " + _sFieldName);
				return true;
			}
		}
		return false;
	}
	
	public static String displayPublicMethod(Object o, boolean _bSimple) 
	{
		if (o != null)
		{
			try 
			{
				Class clazz = o.getClass();
				StringBuilder sRes = new StringBuilder();
				Method aMethod[] = clazz.getDeclaredMethods();
				for(int i = 0; i < aMethod.length; i++)
				{
					Method m = aMethod[i];
					if (!_bSimple)
					{
						sRes.append(m.toGenericString());
					}
					else
					{
						sRes.append(m.getReturnType().getSimpleName()).append(" ").append(m.getName()).append("(");
						Class aParams[] = m.getParameterTypes();
						for(int j = 0; j < aParams.length; j++)
						{
							sRes.append(aParams[j].getSimpleName());
							if (j != (aParams.length - 1)) sRes.append(",");
						}
						sRes.append(")");
					}
					sRes.append("\n");
				}
				return sRes.toString();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}				
		}
		return "";
	}

	/**
	 * get field class
	 * 
	 * @param _o
	 * @param _sFieldName
	 * @return
	 */
	public static Class getFieldClass(Object _o, String _sFieldName)
	{
		if(_o != null)
		{
			try 
			{			
				Class cData = _o.getClass();
				PropertyDescriptor[] aProperties = Introspector.getBeanInfo(cData).getPropertyDescriptors();
				PropertyDescriptor oProperty = getPropertyDescriptorByName(aProperties, _sFieldName);
				if (oProperty != null)
				{
					return oProperty.getPropertyType();
				}
				else
				{
					log.warn("Property " + _sFieldName + " Not Found in " + _o);
				}
			} 
			catch (Exception e) 
			{
				log.error(e);
			}
		}
		return null;
	}
	
	/**
	 * set Java Object Properties from Existing Map From JSON/GSON
	 * 
	 * @param oData
	 * @param _mJSON
	 * @return
	 * @throws Exception
	 */
	public static Object setFromJSONMap(Object oData, Map _mJSON)
		throws Exception
	{
		Class cVT = oData.getClass(); 
		PropertyDescriptor[] propdesc
    		= Introspector.getBeanInfo(cVT).getPropertyDescriptors();
	
		for (int i = 0; i < propdesc.length; i++)
		{
		    String prop = propdesc[i].getName();
		    if (!prop.equals("class"))
		    {
		    	Method setter = propdesc[i].getWriteMethod();		    	
		    	if (setter != null && _mJSON.get(prop) != null)
		    	{
		    		Class cparam = setter.getParameterTypes()[0];	        	    		
	    	    	//log.debug ("Field : " + i + " : "  + prop + " class: " + cparam.getCanonicalName() + " data class: " + _mJSON.get(prop).getClass().getCanonicalName() + " data value: " + _mJSON.get(prop));	        	    	
	    	    		
		    		if(cparam.equals(BigDecimal.class))
		    		{
		    			BigDecimal bd = new BigDecimal((Double)_mJSON.get(prop));
		    			if (bd != null)
		    			{
	    	    			setter.invoke(oData, bd);	        	    		        	    				
		    			}	        	    			
		    		}
		    		else if(cparam.equals(int.class))
		    		{
		    			int bd = Integer.valueOf(((Double)_mJSON.get(prop)).intValue());
	    	    		setter.invoke(oData, bd);	        	    		        	    					        	    			
		    		}
		    		else if(cparam.equals(Date.class))
		    		{

		    			setter.invoke(oData, CustomParser.parseCustom((String)_mJSON.get(prop),"MMM dd, yyyy h:mm:ss a"));	        	    		        	    					        	    			
		    		}		    		
		    		else
		    		{
		    			setter.invoke(oData, _mJSON.get(prop));	        	    	
		    		}
		    	}
		    }
		}
		return oData;
	}
	
	/**
	 * Get the method name for a depth in call stack. <br />
	 * Utility function
	 * @param depth depth in the call stack (0 means current method, 1 means call method, ...)
	 * @return method name
	 */
	public static String getMethodName(final int depth)
	{
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		//System. out.println(ste[ste.length-depth].getClassName()+"#"+ste[ste.length-depth].getMethodName());	  
		return ste[ste.length - 1 -  depth].getClassName() + "#" + ste[ste.length - 1 - depth].getMethodName(); 
	}
	
}
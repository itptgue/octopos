package com.ssti.enterprise.pos.presentation.screens.periodic;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureScreen;

public class EndYearClosing extends SecureScreen
{
	private static final String[] a_PERM = {"GL Setup", "View GL Setup", "View Periodic Transaction"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
}

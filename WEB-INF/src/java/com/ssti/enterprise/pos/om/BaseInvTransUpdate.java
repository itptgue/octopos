package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to InvTransUpdate
 */
public abstract class BaseInvTransUpdate extends BaseObject
{
    /** The Peer class */
    private static final InvTransUpdatePeer peer =
        new InvTransUpdatePeer();

        
    /** The value for the updateId field */
    private String updateId;
      
    /** The value for the invtransId field */
    private String invtransId;
      
    /** The value for the txType field */
    private int txType;
      
    /** The value for the glType field */
    private int glType;
      
    /** The value for the transId field */
    private String transId;
      
    /** The value for the transdetId field */
    private String transdetId;
      
    /** The value for the transNo field */
    private String transNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the invAccount field */
    private String invAccount;
      
    /** The value for the othAccount field */
    private String othAccount;
      
    /** The value for the oldSubcost field */
    private BigDecimal oldSubcost;
      
    /** The value for the newSubcost field */
    private BigDecimal newSubcost;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the updateBy field */
    private String updateBy;
      
    /** The value for the updateTx field */
    private String updateTx;
  
    
    /**
     * Get the UpdateId
     *
     * @return String
     */
    public String getUpdateId()
    {
        return updateId;
    }

                        
    /**
     * Set the value of UpdateId
     *
     * @param v new value
     */
    public void setUpdateId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updateId, v))
              {
            this.updateId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvtransId
     *
     * @return String
     */
    public String getInvtransId()
    {
        return invtransId;
    }

                        
    /**
     * Set the value of InvtransId
     *
     * @param v new value
     */
    public void setInvtransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invtransId, v))
              {
            this.invtransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TxType
     *
     * @return int
     */
    public int getTxType()
    {
        return txType;
    }

                        
    /**
     * Set the value of TxType
     *
     * @param v new value
     */
    public void setTxType(int v) 
    {
    
                  if (this.txType != v)
              {
            this.txType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GlType
     *
     * @return int
     */
    public int getGlType()
    {
        return glType;
    }

                        
    /**
     * Set the value of GlType
     *
     * @param v new value
     */
    public void setGlType(int v) 
    {
    
                  if (this.glType != v)
              {
            this.glType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransId
     *
     * @return String
     */
    public String getTransId()
    {
        return transId;
    }

                        
    /**
     * Set the value of TransId
     *
     * @param v new value
     */
    public void setTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transId, v))
              {
            this.transId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransdetId
     *
     * @return String
     */
    public String getTransdetId()
    {
        return transdetId;
    }

                        
    /**
     * Set the value of TransdetId
     *
     * @param v new value
     */
    public void setTransdetId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transdetId, v))
              {
            this.transdetId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransNo
     *
     * @return String
     */
    public String getTransNo()
    {
        return transNo;
    }

                        
    /**
     * Set the value of TransNo
     *
     * @param v new value
     */
    public void setTransNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transNo, v))
              {
            this.transNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InvAccount
     *
     * @return String
     */
    public String getInvAccount()
    {
        return invAccount;
    }

                        
    /**
     * Set the value of InvAccount
     *
     * @param v new value
     */
    public void setInvAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.invAccount, v))
              {
            this.invAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OthAccount
     *
     * @return String
     */
    public String getOthAccount()
    {
        return othAccount;
    }

                        
    /**
     * Set the value of OthAccount
     *
     * @param v new value
     */
    public void setOthAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.othAccount, v))
              {
            this.othAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OldSubcost
     *
     * @return BigDecimal
     */
    public BigDecimal getOldSubcost()
    {
        return oldSubcost;
    }

                        
    /**
     * Set the value of OldSubcost
     *
     * @param v new value
     */
    public void setOldSubcost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.oldSubcost, v))
              {
            this.oldSubcost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NewSubcost
     *
     * @return BigDecimal
     */
    public BigDecimal getNewSubcost()
    {
        return newSubcost;
    }

                        
    /**
     * Set the value of NewSubcost
     *
     * @param v new value
     */
    public void setNewSubcost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.newSubcost, v))
              {
            this.newSubcost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateBy
     *
     * @return String
     */
    public String getUpdateBy()
    {
        return updateBy;
    }

                        
    /**
     * Set the value of UpdateBy
     *
     * @param v new value
     */
    public void setUpdateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updateBy, v))
              {
            this.updateBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateTx
     *
     * @return String
     */
    public String getUpdateTx()
    {
        return updateTx;
    }

                        
    /**
     * Set the value of UpdateTx
     *
     * @param v new value
     */
    public void setUpdateTx(String v) 
    {
    
                  if (!ObjectUtils.equals(this.updateTx, v))
              {
            this.updateTx = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("UpdateId");
              fieldNames.add("InvtransId");
              fieldNames.add("TxType");
              fieldNames.add("GlType");
              fieldNames.add("TransId");
              fieldNames.add("TransdetId");
              fieldNames.add("TransNo");
              fieldNames.add("ItemId");
              fieldNames.add("LocationId");
              fieldNames.add("InvAccount");
              fieldNames.add("OthAccount");
              fieldNames.add("OldSubcost");
              fieldNames.add("NewSubcost");
              fieldNames.add("UpdateDate");
              fieldNames.add("UpdateBy");
              fieldNames.add("UpdateTx");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("UpdateId"))
        {
                return getUpdateId();
            }
          if (name.equals("InvtransId"))
        {
                return getInvtransId();
            }
          if (name.equals("TxType"))
        {
                return Integer.valueOf(getTxType());
            }
          if (name.equals("GlType"))
        {
                return Integer.valueOf(getGlType());
            }
          if (name.equals("TransId"))
        {
                return getTransId();
            }
          if (name.equals("TransdetId"))
        {
                return getTransdetId();
            }
          if (name.equals("TransNo"))
        {
                return getTransNo();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("InvAccount"))
        {
                return getInvAccount();
            }
          if (name.equals("OthAccount"))
        {
                return getOthAccount();
            }
          if (name.equals("OldSubcost"))
        {
                return getOldSubcost();
            }
          if (name.equals("NewSubcost"))
        {
                return getNewSubcost();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("UpdateBy"))
        {
                return getUpdateBy();
            }
          if (name.equals("UpdateTx"))
        {
                return getUpdateTx();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(InvTransUpdatePeer.UPDATE_ID))
        {
                return getUpdateId();
            }
          if (name.equals(InvTransUpdatePeer.INVTRANS_ID))
        {
                return getInvtransId();
            }
          if (name.equals(InvTransUpdatePeer.TX_TYPE))
        {
                return Integer.valueOf(getTxType());
            }
          if (name.equals(InvTransUpdatePeer.GL_TYPE))
        {
                return Integer.valueOf(getGlType());
            }
          if (name.equals(InvTransUpdatePeer.TRANS_ID))
        {
                return getTransId();
            }
          if (name.equals(InvTransUpdatePeer.TRANSDET_ID))
        {
                return getTransdetId();
            }
          if (name.equals(InvTransUpdatePeer.TRANS_NO))
        {
                return getTransNo();
            }
          if (name.equals(InvTransUpdatePeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(InvTransUpdatePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(InvTransUpdatePeer.INV_ACCOUNT))
        {
                return getInvAccount();
            }
          if (name.equals(InvTransUpdatePeer.OTH_ACCOUNT))
        {
                return getOthAccount();
            }
          if (name.equals(InvTransUpdatePeer.OLD_SUBCOST))
        {
                return getOldSubcost();
            }
          if (name.equals(InvTransUpdatePeer.NEW_SUBCOST))
        {
                return getNewSubcost();
            }
          if (name.equals(InvTransUpdatePeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(InvTransUpdatePeer.UPDATE_BY))
        {
                return getUpdateBy();
            }
          if (name.equals(InvTransUpdatePeer.UPDATE_TX))
        {
                return getUpdateTx();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getUpdateId();
            }
              if (pos == 1)
        {
                return getInvtransId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getTxType());
            }
              if (pos == 3)
        {
                return Integer.valueOf(getGlType());
            }
              if (pos == 4)
        {
                return getTransId();
            }
              if (pos == 5)
        {
                return getTransdetId();
            }
              if (pos == 6)
        {
                return getTransNo();
            }
              if (pos == 7)
        {
                return getItemId();
            }
              if (pos == 8)
        {
                return getLocationId();
            }
              if (pos == 9)
        {
                return getInvAccount();
            }
              if (pos == 10)
        {
                return getOthAccount();
            }
              if (pos == 11)
        {
                return getOldSubcost();
            }
              if (pos == 12)
        {
                return getNewSubcost();
            }
              if (pos == 13)
        {
                return getUpdateDate();
            }
              if (pos == 14)
        {
                return getUpdateBy();
            }
              if (pos == 15)
        {
                return getUpdateTx();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(InvTransUpdatePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        InvTransUpdatePeer.doInsert((InvTransUpdate) this, con);
                        setNew(false);
                    }
                    else
                    {
                        InvTransUpdatePeer.doUpdate((InvTransUpdate) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key updateId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setUpdateId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setUpdateId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getUpdateId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public InvTransUpdate copy() throws TorqueException
    {
        return copyInto(new InvTransUpdate());
    }
  
    protected InvTransUpdate copyInto(InvTransUpdate copyObj) throws TorqueException
    {
          copyObj.setUpdateId(updateId);
          copyObj.setInvtransId(invtransId);
          copyObj.setTxType(txType);
          copyObj.setGlType(glType);
          copyObj.setTransId(transId);
          copyObj.setTransdetId(transdetId);
          copyObj.setTransNo(transNo);
          copyObj.setItemId(itemId);
          copyObj.setLocationId(locationId);
          copyObj.setInvAccount(invAccount);
          copyObj.setOthAccount(othAccount);
          copyObj.setOldSubcost(oldSubcost);
          copyObj.setNewSubcost(newSubcost);
          copyObj.setUpdateDate(updateDate);
          copyObj.setUpdateBy(updateBy);
          copyObj.setUpdateTx(updateTx);
  
                    copyObj.setUpdateId((String)null);
                                                                                                      
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public InvTransUpdatePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("InvTransUpdate\n");
        str.append("--------------\n")
           .append("UpdateId             : ")
           .append(getUpdateId())
           .append("\n")
           .append("InvtransId           : ")
           .append(getInvtransId())
           .append("\n")
           .append("TxType               : ")
           .append(getTxType())
           .append("\n")
           .append("GlType               : ")
           .append(getGlType())
           .append("\n")
           .append("TransId              : ")
           .append(getTransId())
           .append("\n")
           .append("TransdetId           : ")
           .append(getTransdetId())
           .append("\n")
           .append("TransNo              : ")
           .append(getTransNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("InvAccount           : ")
           .append(getInvAccount())
           .append("\n")
           .append("OthAccount           : ")
           .append(getOthAccount())
           .append("\n")
           .append("OldSubcost           : ")
           .append(getOldSubcost())
           .append("\n")
           .append("NewSubcost           : ")
           .append(getNewSubcost())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("UpdateBy             : ")
           .append(getUpdateBy())
           .append("\n")
           .append("UpdateTx             : ")
           .append(getUpdateTx())
           .append("\n")
        ;
        return(str.toString());
    }
}

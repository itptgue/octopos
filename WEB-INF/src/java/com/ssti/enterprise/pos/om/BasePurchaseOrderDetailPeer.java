package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.PurchaseOrderDetailMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


  
/**
 */
public abstract class BasePurchaseOrderDetailPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "purchase_order_detail";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(PurchaseOrderDetailMapBuilder.CLASS_NAME);
    }

      /** the column name for the PURCHASE_ORDER_DETAIL_ID field */
    public static final String PURCHASE_ORDER_DETAIL_ID;
      /** the column name for the PURCHASE_ORDER_ID field */
    public static final String PURCHASE_ORDER_ID;
      /** the column name for the REQUEST_TYPE field */
    public static final String REQUEST_TYPE;
      /** the column name for the REQUEST_DETAIL_ID field */
    public static final String REQUEST_DETAIL_ID;
      /** the column name for the REQUEST_ID field */
    public static final String REQUEST_ID;
      /** the column name for the INDEX_NO field */
    public static final String INDEX_NO;
      /** the column name for the ITEM_ID field */
    public static final String ITEM_ID;
      /** the column name for the ITEM_CODE field */
    public static final String ITEM_CODE;
      /** the column name for the ITEM_NAME field */
    public static final String ITEM_NAME;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the UNIT_ID field */
    public static final String UNIT_ID;
      /** the column name for the UNIT_CODE field */
    public static final String UNIT_CODE;
      /** the column name for the TAX_ID field */
    public static final String TAX_ID;
      /** the column name for the TAX_AMOUNT field */
    public static final String TAX_AMOUNT;
      /** the column name for the SUB_TOTAL_TAX field */
    public static final String SUB_TOTAL_TAX;
      /** the column name for the DISCOUNT field */
    public static final String DISCOUNT;
      /** the column name for the SUB_TOTAL_DISC field */
    public static final String SUB_TOTAL_DISC;
      /** the column name for the QTY field */
    public static final String QTY;
      /** the column name for the QTY_BASE field */
    public static final String QTY_BASE;
      /** the column name for the QTY_AUTO field */
    public static final String QTY_AUTO;
      /** the column name for the ITEM_PRICE field */
    public static final String ITEM_PRICE;
      /** the column name for the SUB_TOTAL field */
    public static final String SUB_TOTAL;
      /** the column name for the LAST_PURCHASE field */
    public static final String LAST_PURCHASE;
      /** the column name for the COST_PER_UNIT field */
    public static final String COST_PER_UNIT;
      /** the column name for the RECEIVED_QTY field */
    public static final String RECEIVED_QTY;
      /** the column name for the CLOSED field */
    public static final String CLOSED;
      /** the column name for the CHANGED_MANUAL field */
    public static final String CHANGED_MANUAL;
      /** the column name for the PROJECT_ID field */
    public static final String PROJECT_ID;
      /** the column name for the DEPARTMENT_ID field */
    public static final String DEPARTMENT_ID;
  
    static
    {
          PURCHASE_ORDER_DETAIL_ID = "purchase_order_detail.PURCHASE_ORDER_DETAIL_ID";
          PURCHASE_ORDER_ID = "purchase_order_detail.PURCHASE_ORDER_ID";
          REQUEST_TYPE = "purchase_order_detail.REQUEST_TYPE";
          REQUEST_DETAIL_ID = "purchase_order_detail.REQUEST_DETAIL_ID";
          REQUEST_ID = "purchase_order_detail.REQUEST_ID";
          INDEX_NO = "purchase_order_detail.INDEX_NO";
          ITEM_ID = "purchase_order_detail.ITEM_ID";
          ITEM_CODE = "purchase_order_detail.ITEM_CODE";
          ITEM_NAME = "purchase_order_detail.ITEM_NAME";
          DESCRIPTION = "purchase_order_detail.DESCRIPTION";
          UNIT_ID = "purchase_order_detail.UNIT_ID";
          UNIT_CODE = "purchase_order_detail.UNIT_CODE";
          TAX_ID = "purchase_order_detail.TAX_ID";
          TAX_AMOUNT = "purchase_order_detail.TAX_AMOUNT";
          SUB_TOTAL_TAX = "purchase_order_detail.SUB_TOTAL_TAX";
          DISCOUNT = "purchase_order_detail.DISCOUNT";
          SUB_TOTAL_DISC = "purchase_order_detail.SUB_TOTAL_DISC";
          QTY = "purchase_order_detail.QTY";
          QTY_BASE = "purchase_order_detail.QTY_BASE";
          QTY_AUTO = "purchase_order_detail.QTY_AUTO";
          ITEM_PRICE = "purchase_order_detail.ITEM_PRICE";
          SUB_TOTAL = "purchase_order_detail.SUB_TOTAL";
          LAST_PURCHASE = "purchase_order_detail.LAST_PURCHASE";
          COST_PER_UNIT = "purchase_order_detail.COST_PER_UNIT";
          RECEIVED_QTY = "purchase_order_detail.RECEIVED_QTY";
          CLOSED = "purchase_order_detail.CLOSED";
          CHANGED_MANUAL = "purchase_order_detail.CHANGED_MANUAL";
          PROJECT_ID = "purchase_order_detail.PROJECT_ID";
          DEPARTMENT_ID = "purchase_order_detail.DEPARTMENT_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(PurchaseOrderDetailMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(PurchaseOrderDetailMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  29;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.PurchaseOrderDetail";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseOrderDetailPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CLOSED))
        {
            Object possibleBoolean = criteria.get(CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CHANGED_MANUAL))
        {
            Object possibleBoolean = criteria.get(CHANGED_MANUAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CHANGED_MANUAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(PURCHASE_ORDER_DETAIL_ID);
          criteria.addSelectColumn(PURCHASE_ORDER_ID);
          criteria.addSelectColumn(REQUEST_TYPE);
          criteria.addSelectColumn(REQUEST_DETAIL_ID);
          criteria.addSelectColumn(REQUEST_ID);
          criteria.addSelectColumn(INDEX_NO);
          criteria.addSelectColumn(ITEM_ID);
          criteria.addSelectColumn(ITEM_CODE);
          criteria.addSelectColumn(ITEM_NAME);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(UNIT_ID);
          criteria.addSelectColumn(UNIT_CODE);
          criteria.addSelectColumn(TAX_ID);
          criteria.addSelectColumn(TAX_AMOUNT);
          criteria.addSelectColumn(SUB_TOTAL_TAX);
          criteria.addSelectColumn(DISCOUNT);
          criteria.addSelectColumn(SUB_TOTAL_DISC);
          criteria.addSelectColumn(QTY);
          criteria.addSelectColumn(QTY_BASE);
          criteria.addSelectColumn(QTY_AUTO);
          criteria.addSelectColumn(ITEM_PRICE);
          criteria.addSelectColumn(SUB_TOTAL);
          criteria.addSelectColumn(LAST_PURCHASE);
          criteria.addSelectColumn(COST_PER_UNIT);
          criteria.addSelectColumn(RECEIVED_QTY);
          criteria.addSelectColumn(CLOSED);
          criteria.addSelectColumn(CHANGED_MANUAL);
          criteria.addSelectColumn(PROJECT_ID);
          criteria.addSelectColumn(DEPARTMENT_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static PurchaseOrderDetail row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            PurchaseOrderDetail obj = (PurchaseOrderDetail) cls.newInstance();
            PurchaseOrderDetailPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      PurchaseOrderDetail obj)
        throws TorqueException
    {
        try
        {
                obj.setPurchaseOrderDetailId(row.getValue(offset + 0).asString());
                  obj.setPurchaseOrderId(row.getValue(offset + 1).asString());
                  obj.setRequestType(row.getValue(offset + 2).asInt());
                  obj.setRequestDetailId(row.getValue(offset + 3).asString());
                  obj.setRequestId(row.getValue(offset + 4).asString());
                  obj.setIndexNo(row.getValue(offset + 5).asInt());
                  obj.setItemId(row.getValue(offset + 6).asString());
                  obj.setItemCode(row.getValue(offset + 7).asString());
                  obj.setItemName(row.getValue(offset + 8).asString());
                  obj.setDescription(row.getValue(offset + 9).asString());
                  obj.setUnitId(row.getValue(offset + 10).asString());
                  obj.setUnitCode(row.getValue(offset + 11).asString());
                  obj.setTaxId(row.getValue(offset + 12).asString());
                  obj.setTaxAmount(row.getValue(offset + 13).asBigDecimal());
                  obj.setSubTotalTax(row.getValue(offset + 14).asBigDecimal());
                  obj.setDiscount(row.getValue(offset + 15).asString());
                  obj.setSubTotalDisc(row.getValue(offset + 16).asBigDecimal());
                  obj.setQty(row.getValue(offset + 17).asBigDecimal());
                  obj.setQtyBase(row.getValue(offset + 18).asBigDecimal());
                  obj.setQtyAuto(row.getValue(offset + 19).asBigDecimal());
                  obj.setItemPrice(row.getValue(offset + 20).asBigDecimal());
                  obj.setSubTotal(row.getValue(offset + 21).asBigDecimal());
                  obj.setLastPurchase(row.getValue(offset + 22).asBigDecimal());
                  obj.setCostPerUnit(row.getValue(offset + 23).asBigDecimal());
                  obj.setReceivedQty(row.getValue(offset + 24).asBigDecimal());
                  obj.setClosed(row.getValue(offset + 25).asBoolean());
                  obj.setChangedManual(row.getValue(offset + 26).asBoolean());
                  obj.setProjectId(row.getValue(offset + 27).asString());
                  obj.setDepartmentId(row.getValue(offset + 28).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BasePurchaseOrderDetailPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CLOSED))
        {
            Object possibleBoolean = criteria.get(CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CHANGED_MANUAL))
        {
            Object possibleBoolean = criteria.get(CHANGED_MANUAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CHANGED_MANUAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(PurchaseOrderDetailPeer.row2Object(row, 1,
                PurchaseOrderDetailPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BasePurchaseOrderDetailPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(PURCHASE_ORDER_DETAIL_ID, criteria.remove(PURCHASE_ORDER_DETAIL_ID));
                                                                                                                                                                                                                                                                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CLOSED))
        {
            Object possibleBoolean = criteria.get(CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CHANGED_MANUAL))
        {
            Object possibleBoolean = criteria.get(CHANGED_MANUAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CHANGED_MANUAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         PurchaseOrderDetailPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CLOSED))
        {
            Object possibleBoolean = criteria.get(CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CHANGED_MANUAL))
        {
            Object possibleBoolean = criteria.get(CHANGED_MANUAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CHANGED_MANUAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(PurchaseOrderDetail obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseOrderDetail obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseOrderDetail obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseOrderDetail obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(PurchaseOrderDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(PurchaseOrderDetail obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(PurchaseOrderDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(PurchaseOrderDetail obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(PurchaseOrderDetail) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(PurchaseOrderDetail obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BasePurchaseOrderDetailPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(PURCHASE_ORDER_DETAIL_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( PurchaseOrderDetail obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(PURCHASE_ORDER_DETAIL_ID, obj.getPurchaseOrderDetailId());
              criteria.add(PURCHASE_ORDER_ID, obj.getPurchaseOrderId());
              criteria.add(REQUEST_TYPE, obj.getRequestType());
              criteria.add(REQUEST_DETAIL_ID, obj.getRequestDetailId());
              criteria.add(REQUEST_ID, obj.getRequestId());
              criteria.add(INDEX_NO, obj.getIndexNo());
              criteria.add(ITEM_ID, obj.getItemId());
              criteria.add(ITEM_CODE, obj.getItemCode());
              criteria.add(ITEM_NAME, obj.getItemName());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(UNIT_ID, obj.getUnitId());
              criteria.add(UNIT_CODE, obj.getUnitCode());
              criteria.add(TAX_ID, obj.getTaxId());
              criteria.add(TAX_AMOUNT, obj.getTaxAmount());
              criteria.add(SUB_TOTAL_TAX, obj.getSubTotalTax());
              criteria.add(DISCOUNT, obj.getDiscount());
              criteria.add(SUB_TOTAL_DISC, obj.getSubTotalDisc());
              criteria.add(QTY, obj.getQty());
              criteria.add(QTY_BASE, obj.getQtyBase());
              criteria.add(QTY_AUTO, obj.getQtyAuto());
              criteria.add(ITEM_PRICE, obj.getItemPrice());
              criteria.add(SUB_TOTAL, obj.getSubTotal());
              criteria.add(LAST_PURCHASE, obj.getLastPurchase());
              criteria.add(COST_PER_UNIT, obj.getCostPerUnit());
              criteria.add(RECEIVED_QTY, obj.getReceivedQty());
              criteria.add(CLOSED, obj.getClosed());
              criteria.add(CHANGED_MANUAL, obj.getChangedManual());
              criteria.add(PROJECT_ID, obj.getProjectId());
              criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( PurchaseOrderDetail obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(PURCHASE_ORDER_DETAIL_ID, obj.getPurchaseOrderDetailId());
                          criteria.add(PURCHASE_ORDER_ID, obj.getPurchaseOrderId());
                          criteria.add(REQUEST_TYPE, obj.getRequestType());
                          criteria.add(REQUEST_DETAIL_ID, obj.getRequestDetailId());
                          criteria.add(REQUEST_ID, obj.getRequestId());
                          criteria.add(INDEX_NO, obj.getIndexNo());
                          criteria.add(ITEM_ID, obj.getItemId());
                          criteria.add(ITEM_CODE, obj.getItemCode());
                          criteria.add(ITEM_NAME, obj.getItemName());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(UNIT_ID, obj.getUnitId());
                          criteria.add(UNIT_CODE, obj.getUnitCode());
                          criteria.add(TAX_ID, obj.getTaxId());
                          criteria.add(TAX_AMOUNT, obj.getTaxAmount());
                          criteria.add(SUB_TOTAL_TAX, obj.getSubTotalTax());
                          criteria.add(DISCOUNT, obj.getDiscount());
                          criteria.add(SUB_TOTAL_DISC, obj.getSubTotalDisc());
                          criteria.add(QTY, obj.getQty());
                          criteria.add(QTY_BASE, obj.getQtyBase());
                          criteria.add(QTY_AUTO, obj.getQtyAuto());
                          criteria.add(ITEM_PRICE, obj.getItemPrice());
                          criteria.add(SUB_TOTAL, obj.getSubTotal());
                          criteria.add(LAST_PURCHASE, obj.getLastPurchase());
                          criteria.add(COST_PER_UNIT, obj.getCostPerUnit());
                          criteria.add(RECEIVED_QTY, obj.getReceivedQty());
                          criteria.add(CLOSED, obj.getClosed());
                          criteria.add(CHANGED_MANUAL, obj.getChangedManual());
                          criteria.add(PROJECT_ID, obj.getProjectId());
                          criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrderDetail retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrderDetail retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrderDetail retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        PurchaseOrderDetail retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static PurchaseOrderDetail retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (PurchaseOrderDetail)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( PURCHASE_ORDER_DETAIL_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



          
                                              
                
                

    /**
     * selects a collection of PurchaseOrderDetail objects pre-filled with their
     * PurchaseOrder objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PurchaseOrderDetailPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinPurchaseOrder(Criteria criteria)
        throws TorqueException
    {
        setDbName(criteria);

        PurchaseOrderDetailPeer.addSelectColumns(criteria);
        int offset = numColumns + 1;
        PurchaseOrderPeer.addSelectColumns(criteria);


                        criteria.addJoin(PurchaseOrderDetailPeer.PURCHASE_ORDER_ID,
            PurchaseOrderPeer.PURCHASE_ORDER_ID);
        

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CLOSED))
        {
            Object possibleBoolean = criteria.get(CLOSED);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CLOSED, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                    // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(CHANGED_MANUAL))
        {
            Object possibleBoolean = criteria.get(CHANGED_MANUAL);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(CHANGED_MANUAL, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                      
        List rows = BasePeer.doSelect(criteria);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = PurchaseOrderDetailPeer.getOMClass();
                    PurchaseOrderDetail obj1 = PurchaseOrderDetailPeer
                .row2Object(row, 1, omClass);
                     omClass = PurchaseOrderPeer.getOMClass();
                    PurchaseOrder obj2 = PurchaseOrderPeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                PurchaseOrderDetail temp_obj1 = (PurchaseOrderDetail)results.get(j);
                PurchaseOrder temp_obj2 = temp_obj1.getPurchaseOrder();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                              temp_obj2.addPurchaseOrderDetail(obj1);
                              break;
                }
            }
                      if (newObject)
            {
                obj2.initPurchaseOrderDetails();
                obj2.addPurchaseOrderDetail(obj1);
            }
                      results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

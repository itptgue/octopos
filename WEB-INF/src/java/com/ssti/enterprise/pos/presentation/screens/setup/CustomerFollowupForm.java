package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.CustomerFollowup;
import com.ssti.enterprise.pos.om.CustomerFollowupPeer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.StringUtil;

public class CustomerFollowupForm extends CustomerSecureScreen
{
	public static int i_SAVE_ADDRESS = 1;
	public static int i_DEL_ADDRESS  = 2;
	public static int i_VIEW_ADDRESS = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_SAVE_ADDRESS)
			{
				saveFollowup(data,context);
			}
			else if (iOp == i_DEL_ADDRESS)
			{
				delFollowup(data,context);
			}
			else if (iOp == i_VIEW_ADDRESS)
			{
				viewFollowup(data,context);				
			}
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx);
    	}
    }

	private void saveFollowup(RunData data, Context context) 
	{
		try 
		{
			String sCTID = data.getParameters().getString("CustomerFollowupId");
			String sID = data.getParameters().getString("ID");
			CustomerFollowup oCA = CustomerTool.getFollowupByID(sCTID);
			System.out.println(oCA);
			boolean bNew = false;
			if (oCA == null)
			{
				oCA = new CustomerFollowup();
				bNew = true;
			}
			System.out.println("new " + bNew);
			data.getParameters().setProperties(oCA);
			if (bNew) 
			{
				oCA.setCustomerFollowupId(IDGenerator.generateSysID());
				oCA.setCustomerId(sID);
			}
			oCA.save();			
			data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_SAVE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
	private void delFollowup(RunData data, Context context) 
	{
		try 
		{
			String sCTID = data.getParameters().getString("CustomerFollowupId");
			if (StringUtil.isNotEmpty(sCTID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerFollowupPeer.CUSTOMER_FOLLOWUP_ID, sCTID);
				CustomerFollowupPeer.doDelete(oCrit);
				data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
			}
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
	
	private void viewFollowup(RunData data, Context context) 
	{
		try 
		{
			String sFUID = data.getParameters().getString("CustomerFollowupId");
			if (StringUtil.isNotEmpty(sFUID))
			{
				context.put("Followup", CustomerTool.getFollowupByID(sFUID));
			}
		} 
		catch (Exception _oEx) 
		{
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
			_oEx.printStackTrace();
		}		
	}
}

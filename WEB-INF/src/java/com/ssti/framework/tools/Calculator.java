package com.ssti.framework.tools;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI <br><br>
 *
 * Purpose: <br>
 * Calculator used mostly in templates and external activities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: Calculator.java,v 1.32 2009/05/04 01:33:27 albert Exp $ <br>
 * 
 * <pre>
 * $Log: Calculator.java,v $
 * 
 * 2016-08-29
 * - add new method toShort to get short value of number, used as excel helper in VM
 * 
 * 2015-07-27
 * - add New parameter and override function of calculate discount
 * - Change calculate discount method 
 * - Now multiple pct disc must be input 10%+5%
 * - Each qty discount amount support 1000* => discount each item price with 1000 
 * </pre><br>
 */
public class Calculator
{   
	private static Log log = LogFactory.getLog(Calculator.class);

	public static final String s_PLUS = "+";
	public static final String s_PCT  = "%";
	public static final String s_MUL  = "*";
	public static final String s_SUB  = "-";	
	
	public static double add (Number _n1, Number _n2)
	{
		double dResult = 0;
		if (_n1 != null && _n2 != null)
		{
			dResult = _n1.doubleValue() + _n2.doubleValue();
		}
		return dResult;
	}

	public static double sub (Number _n1, Number _n2)
	{
		double dResult = 0;
		if (_n1 != null && _n2 != null)
		{
			dResult = _n1.doubleValue() - _n2.doubleValue();
		}
		return dResult;
	}

	public static double mul (Number _n1, Number _n2)
	{
		double dResult = 0;
		if (_n1 != null && _n2 != null)
		{
			if(_n1.doubleValue() != 0 && _n2.doubleValue() != 0)
			{
				dResult = _n1.doubleValue() * _n2.doubleValue();
			}
		}
		return dResult;
	}

	public static double div (Number _n1, Number _n2)
	{
		double dResult = 0;
		if (_n1 != null && _n2 != null)
		{			
			if (_n2.doubleValue() != 0)
			{	
				dResult = _n1.doubleValue() / _n2.doubleValue();
			}
		}
		return dResult;
	}

	public static double mod (Number _n1, Number _n2)
	{
		double dResult = 0;
		if (_n1 != null && _n2 != null)
		{
			dResult = _n1.doubleValue() % _n2.doubleValue();
		}
		return dResult;
	}

	/**
	 * calculate pct of n1/n2
	 * 
	 * @param _n1
	 * @param _n2
	 * @return
	 */
    public static double pct (Number _n1, Number _n2)
    {
		double dResult = 0;
		if (_n1 != null && _n2 != null && _n1.doubleValue() != 0 &&  _n2.doubleValue() != 0)
		{
			dResult = _n1.doubleValue() / _n2.doubleValue() * 100;
		}
		return dResult;
    }		

    /**
     * calculate pct
     * 
     * @param _sPct
     * @param _n1
     * @return
     */
    public static double pct (String _sPct, Number _n1)
    {
		double dResult = 0;
		double dDisc = 0;
		if (_sPct != null && _n1 != null)
		{
			if(_sPct.indexOf(s_PCT) > 0)
			{
				_sPct = _sPct.substring(0,_sPct.indexOf(s_PCT));
			}
			try
			{
				dDisc = Double.parseDouble(_sPct);
			}
			catch(NumberFormatException  _oEx)
			{
				log.error ("Invalid Discount " + _sPct);
			}			
			dResult = dDisc / 100 * _n1.doubleValue();
		}
		return dResult;
    }	    
    
    /**
     * Convert Number to BigDecimal
     * 
     * @param _n1
     * @return
     */
	public static BigDecimal toBigDecimal(Number _n1)
	{
		if (_n1 != null)
		{
			return new BigDecimal(_n1.doubleValue());
		}
		return Attributes.bd_ZERO;
	}
    
	/**
	 *	ROUND_DOWN 	1
	 *	ROUND_CEILING 	2
	 *	ROUND_FLOOR 	3
	 *	ROUND_HALF_UP 	4
	 *	ROUND_HALF_DOWN 	5
	 *	ROUND_UNNECESSARY 	7
	 *	ROUND_UP 	0
	 * 
	 * 
	 * @param _n1
	 * @param _iRoundingMode
	 * @return rounded big decimal
	 */
	public static BigDecimal roundUp(Number _n1, int _iRoundingMode)
	{
		if (_n1 != null)
		{
			return new BigDecimal(_n1.doubleValue()).setScale(0, _iRoundingMode);
		}
		return Attributes.bd_ZERO;
	}	
	
	/**
	 * compare 2 number using certain comparator, VM template helper
	 * needed becuase VM can not compare number other than int
	 * 
	 * @param _n1
	 * @param _n2
	 * @param _sComparator
	 * 
	 * @return compare result
	 */
    public static boolean compare(Number _n1, Number _n2, String _sComparator)
    {
		if (_n1 != null && _n2 != null)
		{
			double d1 = _n1.doubleValue();
			double d2 = _n2.doubleValue();
			
	        if(_sComparator.equals(SearchTool.s_EQUAL )) 	    return d1 == d2;
	        if(_sComparator.equals(SearchTool.s_LESS_EQUAL))    return d1 <= d2;
	        if(_sComparator.equals(SearchTool.s_GREATER_EQUAL)) return d1 >= d2;
	        if(_sComparator.equals(SearchTool.s_LESS)) 		 	return d1 <  d2;
	        if(_sComparator.equals(SearchTool.s_GREATER)) 		return d1 >  d2;
	        if(_sComparator.equals(SearchTool.s_NOT_EQUAL)) 	return d1 != d2;
		}
        return false;
    }    

    public static double calculateDiscount (String _sDiscount, double _dTotal)
    {
    	return calculateDiscount (_sDiscount, new Double (_dTotal));
    }

	public static double calculateDiscount (String _sDiscount, Number _n1)
    {
		return calculateDiscount(_sDiscount, _n1, 1);
    }
	
	/**
	 * calculate discount amount
	 * 
	 * @param _sDiscount the discount 10% or 10000
	 * @param _n1
	 * @return double discount amount
	 */
	public static double calculateDiscount (String _sDiscount, Number _n1, Number _nQty)
    {
        try 
		{
			double dDiscAmt = 0;
        	if (_n1 != null)
        	{
	        	double dTotal = _n1.doubleValue();
		        double dDiscVal = 0;
		        if(StringUtil.isEmpty(_sDiscount)) 
		        {
		        	_sDiscount = "0";
		        }
		        else
		        {
		        	log.debug("_sDiscount: " + _sDiscount);
		        	String sDiscPart = "";
		        	int iPlsPos = _sDiscount.indexOf(s_PLUS);
		            int iPctPos = _sDiscount.indexOf(s_PCT);
		            int iEacPos = _sDiscount.indexOf(s_MUL);
		            if(iPctPos > -1 || iPlsPos > 0 || iEacPos > 0) //if pct / multiple discount
		            {
		                while(iPlsPos > -1)
		                {
		                    sDiscPart = _sDiscount.substring(0, iPlsPos);		                    
		                    log.debug("sDiscPct: " + sDiscPart);
		                    if(StringUtil.contains(sDiscPart, s_PCT))
		                    {
		                    	sDiscPart = _sDiscount.substring(0, sDiscPart.indexOf(s_PCT));
			                    dDiscVal = Double.parseDouble(sDiscPart)/100;		                    	
			                    dDiscAmt += (dTotal * dDiscVal);
			                    dTotal = dTotal - (dTotal * dDiscVal);
			                    
			                    log.debug("sDiscPct2: " + sDiscPart);
			                    log.debug("dDiscVal: " + dDiscVal);
		                    }		                    
		                    else
		                    {
		                    	dDiscVal = Double.parseDouble(sDiscPart);
		                    	log.debug("dDiscVal: " + dDiscVal);
		                    	dDiscAmt += dDiscVal;
			                    dTotal = dTotal - dDiscVal;
		                    }
		                    _sDiscount = _sDiscount.substring(iPlsPos + 1,_sDiscount.length());
		                    iPlsPos = _sDiscount.indexOf(s_PLUS);
		                }
		                log.debug("noPlus/lastDisc: " + _sDiscount);
	                    if(StringUtil.contains(_sDiscount, s_PCT))
	                    {
			                sDiscPart = _sDiscount.substring(0, _sDiscount.indexOf(s_PCT));
			                dDiscVal = Double.parseDouble(sDiscPart) / 100;
			                if(dDiscVal <= 1)
			                {
			                    dDiscAmt += (dTotal * dDiscVal);
			                }
			                log.debug("sDiscPartLast: " + sDiscPart);
			                log.debug("dDiscVal: " + dDiscVal);
	                    }
	                    else 
	                    {
	                    	if(StringUtil.contains(_sDiscount, s_MUL) && _nQty != null) //each qty value discount
		                    {
	                    		sDiscPart = _sDiscount.substring(0, _sDiscount.indexOf(s_MUL));
	                    		dDiscVal = Double.parseDouble(sDiscPart);
	                    		dDiscAmt += (_nQty.doubleValue() * dDiscVal);
		                    }
	                    	else
	                    	{
	                    		dDiscVal = Double.parseDouble(_sDiscount);
	                    		dDiscAmt += dDiscVal;
		                    }
	                    }
		            }
		            else //if discount amount then just parse the string to double
		            {
		                dDiscAmt = Double.parseDouble(_sDiscount);
		            }
		            log.debug("DiscAmt: " + dDiscAmt);
		        }
        	}
	        return dDiscAmt;
		}
        catch (Exception _oEx)
		{
        	_oEx.printStackTrace();
        	log.error ("Invalid discount input : " + _sDiscount + " " + _oEx.getMessage());
        	return 0;
		}
    }    	
	
	/**
	 * parse string to Double, VM template helper
	 * 
	 * @param s
	 * @return string parse result 
	 */
	public static Double parse (String s)
	{
		try 
		{
			if (StringUtil.isNotEmpty(s) && 
				StringUtil.isNumeric(s)) 
			{
				return Double.valueOf(s);
			}
		}
		catch (Exception _oEx)
		{
			log.error("Error parsing to double, Invalid parameter : " + s);
			_oEx.printStackTrace();
		}
		return Double.valueOf(0);
	}
	
	/**
	 * 
	 * @param d
	 * @return double with 2 decimal precission
	 */
	public static double precise (double d)
	{
		return precise(Double.valueOf(d), 2);
	}

	/**
	 * 
	 * @param n number
	 * @param _iScale expected precision
	 * @return double with scale decimal precission
	 */
	public static double precise (Number n, int _iScale)
	{
		if (n != null)
		{
			//TODO: java is using half even rounding mode, consider using the mode to prevent 
			//calculation difference between java native calculation and application calculation
			return new BigDecimal(n.doubleValue()).setScale(_iScale, RoundingMode.HALF_UP).doubleValue();
		}
		return 0;
	}	
    
    /**
     * Template Helper
     * 
     * @param n number
     * @return int value of the number
     */
    public static int toInt(Number n)
    {
        if (n != null)
        {
            return n.intValue();
        }
        return 0;
    }
	
    /**
     * check if delta between 2 numbers greater than certain amount
     * 
     * @param n1 
     * @param n2
     * @param _dTolerance
     * @return false if delta > specified amount, true if delta < specified amount
     */
    public static boolean isInTolerance (Number n1, Number n2, double _dTolerance)
    {
    	if (n1 != null && n2 != null)
    	{
    		double d1 = n1.doubleValue();
    		double d2 = n2.doubleValue();
    		double dDelta = 0;
    		if (d1 > d2)
    		{
    			dDelta = d1 - d2;    			
    		}
    		else if (d1 < d2)
    		{
    			dDelta = d2 - d1;
    		}
    		else if (d1 == d2)
    		{
    			return true;
    		}
    		if (dDelta > _dTolerance)  
    		{
    			return false;
    		}
    		else
    		{
    			return true;
    		}
    	}
    	return false;
    }
    
    /**
     * round number to smallest scale defined by round to
     * etc. number: 1550, round to: 100, round half up => 1600 
     * ROUND_UP          = 0
	 * ROUND_DOWN        = 1
	 * ROUND_CEILING     = 2
	 * ROUND_FLOOR       = 3
	 * ROUND_HALF_UP     = 4
	 * ROUND_HALF_DOWN   = 5
	 * ROUND_HALF_EVEN   = 6
	 * ROUND_UNNECESSARY = 7
     * 
     * @param n
     * @param _iRoundTo
     * @param _iRoundingMode
     * @return
     */
    public static double roundNumber (Number n, int _iRoundTo, int _iRoundingMode)
    {
    	if (n != null)
    	{
    		if (n.doubleValue() == 0)
    		{
    			return 0;
    		}
    		else if (n.doubleValue() > 0 && n.doubleValue() < _iRoundTo)
    		{
    			return (double) _iRoundTo;
    		}
    		else
    		{
    			double dLeft = n.doubleValue() % _iRoundTo;
    			if (_iRoundTo <= 0) _iRoundTo = 1;
    			int dScale = (int) n.doubleValue() / _iRoundTo;
    			
    			//default is ROUND DOWN
    			if ((_iRoundingMode == BigDecimal.ROUND_HALF_UP && dLeft >= (_iRoundTo / 2)) || 
    				(_iRoundingMode == BigDecimal.ROUND_HALF_DOWN && dLeft > (_iRoundTo / 2)) ||
    				(_iRoundingMode == BigDecimal.ROUND_UP && dLeft > 0))
    			{
	    			dScale++;
    			}
    			double dResult = dScale * _iRoundTo;
    			return dResult;
    		}
    	}
    	return _iRoundTo;
    }
    
    /**
     * convert string to BigDecimal
     * 
     * @param s
     * @return
     */
    public static BigDecimal string2BD (String s)
    {
    	try
    	{
        	return new BigDecimal(s);    		
    	}
    	catch (Exception e) 
    	{
    		log.error("ERROR PARSING STRING TO BIG DECIMAL : " + s);
    	}
    	return Attributes.bd_ZERO;
    }

    public static boolean isZero(Number n)
    {
    	double d = precise(n, 2);
    	if(d == 0)
    	{
    		return true;
    	}
    	return false;
    }

    public static short toShort(Number n)
    {
    	if(n != null)
    	{
    		return (short)n.shortValue();
    	}
    	return (short)0;
    }

	/**
	 * @deprecated use add
	 * @param _dNumber1
	 * @param _dNumber2
	 * @return result
	 */
	public static double addDouble (double _dNumber1, double _dNumber2)
	{
		log.warn("add double deprecated method called ");
		return _dNumber1 + _dNumber2;
	}
	
	/**
	 * @deprecated use mul
	 * @param _dNumber1
	 * @param _dNumber2
	 * @return result
	 */	
	public static double multiplyDouble (double _dNumber1, double _dNumber2)
	{
		log.warn("multiply double deprecated method called ");
		double dResult = _dNumber1 * _dNumber2;
		return dResult;
	}
	
	/**
	 * @deprecated use min
	 * @param _dNumber1
	 * @param _dNumber2
	 * @return result
	 */
	public static double minusDouble (double _dNumber1, double _dNumber2)
	{
		log.warn("minus double deprecated method called ");
		double dResult = _dNumber1 - _dNumber2;
		return dResult;
	}
	
	private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();
    static 
    {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }

    public static String toRoman(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number-l);
    }
}
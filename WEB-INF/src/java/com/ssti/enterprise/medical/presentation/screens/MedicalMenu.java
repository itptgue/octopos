package com.ssti.enterprise.medical.presentation.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.presentation.screens.transaction.MedSecureScreen;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class MedicalMenu extends MedSecureScreen
{ 	
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    }
}

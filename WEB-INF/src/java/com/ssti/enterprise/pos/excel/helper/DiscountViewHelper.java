package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;

public class DiscountViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        int iCond = (Integer)getFromParam(oParam,"condition", Integer.class);
        String sKey = (String)getFromParam(oParam,"keywords",null);
    	String sStart = (String)getFromParam(oParam,"startdate",null);  
		String sEnd = (String)getFromParam(oParam,"enddate",null);   
        String sKatID = (String)getFromParam(oParam,"kategoriid",null);             
		String sLocID = (String)getFromParam(oParam,"locationid",null);             
        int iStatus = (Integer)getFromParam(oParam,"status", Integer.class);
        int iDiscType = (Integer)getFromParam(oParam,"discounttype", Integer.class);
		
        List vData = DiscountTool.findData(iCond, sKey, sStart, sEnd, sKatID, sLocID, iDiscType, iStatus);
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Discount Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
		createHeaderCell(oWB, oRow, (short)iCol, "Promo Code"); iCol++;        
		createHeaderCell(oWB, oRow, (short)iCol, "Discount Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Discount Type"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Description"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Location Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Customer Type Code"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Customers"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Payment Type"); iCol++;
		
		createHeaderCell(oWB, oRow, (short)iCol, "Item SKU"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Items"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Kategori"); iCol++;
		
		createHeaderCell(oWB, oRow, (short)iCol, "Event Begin"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Event End"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Event Disc"); iCol++;
		
		createHeaderCell(oWB, oRow, (short)iCol, "Qty 1"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Disc 1"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty 2"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Disc 2"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty 3"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Disc 3"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty 4"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Disc 4"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Qty 5"); iCol++;
		createHeaderCell(oWB, oRow, (short)iCol, "Disc 5"); iCol++;

		/*
		Discount 
		-------- 
		DiscountId : KG114354121775003750288 
		DiscountCode : 214 
		LocationId : 
			ItemId : 
			ItemSku : 214 
			KategoriId : 
			CustomerTypeId : 
			DiscountType : 2 
			QtyAmount1 : 3.0000 
			QtyAmount2 : 0.0000 
			QtyAmount3 : 0.0000 
			QtyAmount4 : 0.0000 
			QtyAmount5 : 0.0000 
			DiscountValue1 : 2.5% 
			DiscountValue2 : 0 
			DiscountValue3 : 
			DiscountValue4 : 
			DiscountValue5 : 
			EventBeginDate : 2006-03-28 00:00:00.0 
			EventEndDate : 3000-03-28 23:59:00.0 
			EventDiscount : 
			Description :
		*/
		
		for (int i = 0; i < vData.size(); i++)
		{
			Discount oDisc = (Discount) vData.get(i);
			HSSFRow oRow2 = oSheet.createRow(1 + i); 
			
			iCol = 0;
			createCell(oWB, oRow2, (short)iCol, oDisc.getPromoCode()); iCol++; 			
			createCell(oWB, oRow2, (short)iCol, oDisc.getDiscountCode()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, DiscountTool.getTypeStr(oDisc.getDiscountType()));iCol++;                
			createCell(oWB, oRow2, (short)iCol, oDisc.getDescription()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, LocationTool.getCodeByID(oDisc.getLocationId())); iCol++; 
			createCell(oWB, oRow2, (short)iCol, CustomerTypeTool.getCustomerTypeCodeByID(oDisc.getCustomerTypeId())); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getCustomers()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, PaymentTypeTool.getCodeByID(oDisc.getPaymentTypeId())); iCol++; 
			
			createCell(oWB, oRow2, (short)iCol, oDisc.getItemSku()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getItems()); iCol++; 					
			createCell(oWB, oRow2, (short)iCol, KategoriTool.getParentToChildCodeByID(oDisc.getKategoriId())); iCol++; 					

			createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDate(oDisc.getEventBeginDate())); iCol++; 
			createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDate(oDisc.getEventEndDate())); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getEventDiscount()); iCol++; 
			
			createCell(oWB, oRow2, (short)iCol, oDisc.getQtyAmount1().toString()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getDiscountValue1()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getQtyAmount2().toString()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getDiscountValue2()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getQtyAmount3().toString()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getDiscountValue3()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getQtyAmount4().toString()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getDiscountValue4()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getQtyAmount5().toString()); iCol++; 
			createCell(oWB, oRow2, (short)iCol, oDisc.getDiscountValue5()); iCol++; 
		}        
		return oWB;
    }
}

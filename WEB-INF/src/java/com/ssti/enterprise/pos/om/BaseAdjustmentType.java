package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AdjustmentType
 */
public abstract class BaseAdjustmentType extends BaseObject
{
    /** The Peer class */
    private static final AdjustmentTypePeer peer =
        new AdjustmentTypePeer();

        
    /** The value for the adjustmentTypeId field */
    private String adjustmentTypeId;
      
    /** The value for the adjustmentTypeCode field */
    private String adjustmentTypeCode;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the defaultAccountId field */
    private String defaultAccountId;
                                          
    /** The value for the defaultTransType field */
    private int defaultTransType = 1;
      
    /** The value for the defaultCostAdj field */
    private boolean defaultCostAdj;
      
    /** The value for the isTransfer field */
    private boolean isTransfer;
  
    
    /**
     * Get the AdjustmentTypeId
     *
     * @return String
     */
    public String getAdjustmentTypeId()
    {
        return adjustmentTypeId;
    }

                        
    /**
     * Set the value of AdjustmentTypeId
     *
     * @param v new value
     */
    public void setAdjustmentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.adjustmentTypeId, v))
              {
            this.adjustmentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AdjustmentTypeCode
     *
     * @return String
     */
    public String getAdjustmentTypeCode()
    {
        return adjustmentTypeCode;
    }

                        
    /**
     * Set the value of AdjustmentTypeCode
     *
     * @param v new value
     */
    public void setAdjustmentTypeCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.adjustmentTypeCode, v))
              {
            this.adjustmentTypeCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultAccountId
     *
     * @return String
     */
    public String getDefaultAccountId()
    {
        return defaultAccountId;
    }

                        
    /**
     * Set the value of DefaultAccountId
     *
     * @param v new value
     */
    public void setDefaultAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultAccountId, v))
              {
            this.defaultAccountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTransType
     *
     * @return int
     */
    public int getDefaultTransType()
    {
        return defaultTransType;
    }

                        
    /**
     * Set the value of DefaultTransType
     *
     * @param v new value
     */
    public void setDefaultTransType(int v) 
    {
    
                  if (this.defaultTransType != v)
              {
            this.defaultTransType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultCostAdj
     *
     * @return boolean
     */
    public boolean getDefaultCostAdj()
    {
        return defaultCostAdj;
    }

                        
    /**
     * Set the value of DefaultCostAdj
     *
     * @param v new value
     */
    public void setDefaultCostAdj(boolean v) 
    {
    
                  if (this.defaultCostAdj != v)
              {
            this.defaultCostAdj = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTransfer
     *
     * @return boolean
     */
    public boolean getIsTransfer()
    {
        return isTransfer;
    }

                        
    /**
     * Set the value of IsTransfer
     *
     * @param v new value
     */
    public void setIsTransfer(boolean v) 
    {
    
                  if (this.isTransfer != v)
              {
            this.isTransfer = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AdjustmentTypeId");
              fieldNames.add("AdjustmentTypeCode");
              fieldNames.add("Description");
              fieldNames.add("DefaultAccountId");
              fieldNames.add("DefaultTransType");
              fieldNames.add("DefaultCostAdj");
              fieldNames.add("IsTransfer");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AdjustmentTypeId"))
        {
                return getAdjustmentTypeId();
            }
          if (name.equals("AdjustmentTypeCode"))
        {
                return getAdjustmentTypeCode();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("DefaultAccountId"))
        {
                return getDefaultAccountId();
            }
          if (name.equals("DefaultTransType"))
        {
                return Integer.valueOf(getDefaultTransType());
            }
          if (name.equals("DefaultCostAdj"))
        {
                return Boolean.valueOf(getDefaultCostAdj());
            }
          if (name.equals("IsTransfer"))
        {
                return Boolean.valueOf(getIsTransfer());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AdjustmentTypePeer.ADJUSTMENT_TYPE_ID))
        {
                return getAdjustmentTypeId();
            }
          if (name.equals(AdjustmentTypePeer.ADJUSTMENT_TYPE_CODE))
        {
                return getAdjustmentTypeCode();
            }
          if (name.equals(AdjustmentTypePeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(AdjustmentTypePeer.DEFAULT_ACCOUNT_ID))
        {
                return getDefaultAccountId();
            }
          if (name.equals(AdjustmentTypePeer.DEFAULT_TRANS_TYPE))
        {
                return Integer.valueOf(getDefaultTransType());
            }
          if (name.equals(AdjustmentTypePeer.DEFAULT_COST_ADJ))
        {
                return Boolean.valueOf(getDefaultCostAdj());
            }
          if (name.equals(AdjustmentTypePeer.IS_TRANSFER))
        {
                return Boolean.valueOf(getIsTransfer());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAdjustmentTypeId();
            }
              if (pos == 1)
        {
                return getAdjustmentTypeCode();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return getDefaultAccountId();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getDefaultTransType());
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getDefaultCostAdj());
            }
              if (pos == 6)
        {
                return Boolean.valueOf(getIsTransfer());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AdjustmentTypePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AdjustmentTypePeer.doInsert((AdjustmentType) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AdjustmentTypePeer.doUpdate((AdjustmentType) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key adjustmentTypeId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAdjustmentTypeId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAdjustmentTypeId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAdjustmentTypeId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AdjustmentType copy() throws TorqueException
    {
        return copyInto(new AdjustmentType());
    }
  
    protected AdjustmentType copyInto(AdjustmentType copyObj) throws TorqueException
    {
          copyObj.setAdjustmentTypeId(adjustmentTypeId);
          copyObj.setAdjustmentTypeCode(adjustmentTypeCode);
          copyObj.setDescription(description);
          copyObj.setDefaultAccountId(defaultAccountId);
          copyObj.setDefaultTransType(defaultTransType);
          copyObj.setDefaultCostAdj(defaultCostAdj);
          copyObj.setIsTransfer(isTransfer);
  
                    copyObj.setAdjustmentTypeId((String)null);
                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AdjustmentTypePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AdjustmentType\n");
        str.append("--------------\n")
           .append("AdjustmentTypeId     : ")
           .append(getAdjustmentTypeId())
           .append("\n")
           .append("AdjustmentTypeCode   : ")
           .append(getAdjustmentTypeCode())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("DefaultAccountId     : ")
           .append(getDefaultAccountId())
           .append("\n")
           .append("DefaultTransType     : ")
           .append(getDefaultTransType())
           .append("\n")
           .append("DefaultCostAdj       : ")
           .append(getDefaultCostAdj())
           .append("\n")
           .append("IsTransfer           : ")
           .append(getIsTransfer())
           .append("\n")
        ;
        return(str.toString());
    }
}

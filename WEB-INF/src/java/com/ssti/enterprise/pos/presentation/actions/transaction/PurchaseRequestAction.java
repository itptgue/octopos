package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.transaction.PurchaseRequestItemLoader;
import com.ssti.enterprise.pos.om.PurchaseRequest;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.inventory.PurchaseRequestTool;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/transaction/PurchaseRequestAction.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseRequestAction.java,v 1.19 2007/11/09 01:12:15 albert Exp $
 *
 * $Log: PurchaseRequestAction.java,v $
 *  
 * 2018-03-13
 * -add isPurchase & isReimburse in PurchaeRequest
 * -change doFind add param bPur & bRmb 
 * -change in @PurchaeRequestTool
 * 
 */
public class PurchaseRequestAction extends TransactionSecureAction
{
	private static final String s_VIEW_PERM    = "View Item Request";
	private static final String s_CREATE_PERM  = "Create Item Request";
	private static final String s_CONFIRM_PERM = "Confirm Item Request";
	
    private static final String[] a_CREATE_PERM  = {s_INVENTORY_PERM, s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CONFIRM_PERM = {s_INVENTORY_PERM, s_CONFIRM_PERM, s_CONFIRM_PERM};
    private static final String[] a_VIEW_PERM    = {s_INVENTORY_PERM, s_VIEW_PERM};
    
    private static final Log log = LogFactory.getLog(PurchaseRequestAction.class);
    
    private HttpSession oSes = null;
    private PurchaseRequest oRQ;
    private List vRQD;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	int iSave = data.getParameters().getInt("save");
    	int iStatus = data.getParameters().getInt("TransactionStatus");    	
    	if(iSave > 0)
    	{
	        if (iStatus < i_RQ_CONFIRMED)
	        {
	        	return isAuthorized (data, a_CREATE_PERM);
	        }
	        if (iStatus >= i_RQ_CONFIRMED)
	        {
	        	return isAuthorized (data, a_CONFIRM_PERM);
	        }
    	}
        return isAuthorized (data, a_VIEW_PERM);   		
    }
    
    public void doPerform ( RunData data, Context context )
        throws Exception
    {
    	if (data.getParameters().getInt("save") > 0) 
    	{
    		doSave (data,context);
    	}
    	else 
    	{
    		doFind (data,context);
    	}
    }

    /**
     * 
     * @param data
     * @param context
     * @throws Exception
     */
    public void doFind (RunData data, Context context)
        throws Exception
    {
    	super.doFind(data);
    	    	
    	String sVendorID = data.getParameters().getString("VendorId", "");
    	String sLocID = data.getParameters().getString("LocationId", "");
    	String sReqBy = data.getParameters().getString("RequestedBY", "");		
    	int iStatus = data.getParameters().getInt("Status");
		boolean bExt = data.getParameters().getBoolean("IsExternal");
		boolean bPur = data.getParameters().getBoolean("IsPurchase");
		boolean bRmb = data.getParameters().getBoolean("IsReimburse");				
		
		LargeSelect vTR = PurchaseRequestTool.findData(iCond, sKeywords, dStart, dEnd, 
													   sVendorID, iStatus, iLimit, sLocID, sReqBy, bExt, bPur, bRmb);

		data.getUser().setTemp("findPurchaseRequestResult", vTR);	
    }

    /**
     * 
     * @param data
     * @param context
     * @throws Exception
     */
	public void doSave (RunData data, Context context)
        throws Exception
    {
		try
		{
			initSession ( data );
			if(data.getParameters().getInt("ConfirmRequest") != 1)
			{
			    PurchaseRequestTool.updateDetailQty (vRQD, data);			
			}
			else
			{
				//confirm request
			    PurchaseRequestTool.updateDetailQty (vRQD, data, 1);
		    }
			PurchaseRequestTool.setHeaderProperties (oRQ, vRQD, data);
			prepareTransData (data);
			PurchaseRequestTool.saveData (oRQ, vRQD, null);
        	data.setMessage(LocaleTool.getString("prq_save_success"));
	       	resetSession (data);
        }
        catch (Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("prq_save_failed"), _oEx);
        }
    }
 
    private void initSession (RunData data)
    	throws Exception
    {
    	synchronized (this)
    	{
    		oSes = data.getSession();
    		if (oSes.getAttribute (s_RQ) == null ||
    			oSes.getAttribute (s_RQD) == null )
    		{
    			throw new Exception ("Purchase Request Data Invalid");
    		}
    		oRQ = (PurchaseRequest) oSes.getAttribute (s_RQ);
    		vRQD = (List) oSes.getAttribute (s_RQD);
    	}
    }

    private void prepareTransData (RunData data)
    	throws Exception
    {
    	//check transaction data
    	if ( oRQ.getTotalQty().doubleValue() < 0) {throw new Exception ("Total Qty < 1"); } //-- should never happen
		if ( vRQD.size () < 1) {throw new Exception ("Total Requested Item < 1"); } //-- should never happen
    }

    private void resetSession (RunData data)
    	throws Exception
    {
    	synchronized (this)
    	{
			oSes.setAttribute (s_RQ, oRQ );
			oSes.setAttribute (s_RQD, vRQD );
    	}
    }
    
	public void doLoadfile (RunData data, Context context)
	    throws Exception
	{
		try 
		{   
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			String sLocationID = data.getParameters().getString("LocationId");

			
	     	InputStream oBufferStream  = new BufferedInputStream(oFileItem.getInputStream());
			PurchaseRequestItemLoader oLoader = new PurchaseRequestItemLoader(sLocationID);
			oLoader.loadData (oBufferStream);
		
			PurchaseRequest oRQD = new PurchaseRequest();
			List vRQD = oLoader.getListResult();
			PurchaseRequestTool.setHeaderProperties(oRQD, vRQD, data);
			
			oSes = data.getSession();
			oSes.setAttribute (s_RQ,oRQD);
			oSes.setAttribute (s_RQD, vRQD);
			
			if (oLoader.getTotalError() > 0)
			{
				data.setMessage(oLoader.getErrorMessage());
			}
			else 
			{
				context.put("bClose", Boolean.valueOf(true));
			}
	    }
	    catch (Exception _oEx) 
	    {
	    	String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
	    	log.error ( sError, _oEx );
	    	data.setMessage (sError);
	    }
	}	
}
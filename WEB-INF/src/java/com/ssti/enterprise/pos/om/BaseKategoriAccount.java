package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to KategoriAccount
 */
public abstract class BaseKategoriAccount extends BaseObject
{
    /** The Peer class */
    private static final KategoriAccountPeer peer =
        new KategoriAccountPeer();

        
    /** The value for the kategoriId field */
    private String kategoriId;
                                                
    /** The value for the inventoryAccount field */
    private String inventoryAccount = "";
                                                
    /** The value for the salesAccount field */
    private String salesAccount = "";
                                                
    /** The value for the salesReturnAccount field */
    private String salesReturnAccount = "";
                                                
    /** The value for the itemDiscountAccount field */
    private String itemDiscountAccount = "";
                                                
    /** The value for the cogsAccount field */
    private String cogsAccount = "";
                                                
    /** The value for the purchaseReturnAccount field */
    private String purchaseReturnAccount = "";
                                                
    /** The value for the expenseAccount field */
    private String expenseAccount = "";
                                                
    /** The value for the unbilledGoodsAccount field */
    private String unbilledGoodsAccount = "";
                                                
    /** The value for the createBy field */
    private String createBy = "";
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the KategoriId
     *
     * @return String
     */
    public String getKategoriId()
    {
        return kategoriId;
    }

                        
    /**
     * Set the value of KategoriId
     *
     * @param v new value
     */
    public void setKategoriId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.kategoriId, v))
              {
            this.kategoriId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InventoryAccount
     *
     * @return String
     */
    public String getInventoryAccount()
    {
        return inventoryAccount;
    }

                        
    /**
     * Set the value of InventoryAccount
     *
     * @param v new value
     */
    public void setInventoryAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inventoryAccount, v))
              {
            this.inventoryAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAccount
     *
     * @return String
     */
    public String getSalesAccount()
    {
        return salesAccount;
    }

                        
    /**
     * Set the value of SalesAccount
     *
     * @param v new value
     */
    public void setSalesAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAccount, v))
              {
            this.salesAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesReturnAccount
     *
     * @return String
     */
    public String getSalesReturnAccount()
    {
        return salesReturnAccount;
    }

                        
    /**
     * Set the value of SalesReturnAccount
     *
     * @param v new value
     */
    public void setSalesReturnAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesReturnAccount, v))
              {
            this.salesReturnAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemDiscountAccount
     *
     * @return String
     */
    public String getItemDiscountAccount()
    {
        return itemDiscountAccount;
    }

                        
    /**
     * Set the value of ItemDiscountAccount
     *
     * @param v new value
     */
    public void setItemDiscountAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemDiscountAccount, v))
              {
            this.itemDiscountAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CogsAccount
     *
     * @return String
     */
    public String getCogsAccount()
    {
        return cogsAccount;
    }

                        
    /**
     * Set the value of CogsAccount
     *
     * @param v new value
     */
    public void setCogsAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cogsAccount, v))
              {
            this.cogsAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchaseReturnAccount
     *
     * @return String
     */
    public String getPurchaseReturnAccount()
    {
        return purchaseReturnAccount;
    }

                        
    /**
     * Set the value of PurchaseReturnAccount
     *
     * @param v new value
     */
    public void setPurchaseReturnAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseReturnAccount, v))
              {
            this.purchaseReturnAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpenseAccount
     *
     * @return String
     */
    public String getExpenseAccount()
    {
        return expenseAccount;
    }

                        
    /**
     * Set the value of ExpenseAccount
     *
     * @param v new value
     */
    public void setExpenseAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.expenseAccount, v))
              {
            this.expenseAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnbilledGoodsAccount
     *
     * @return String
     */
    public String getUnbilledGoodsAccount()
    {
        return unbilledGoodsAccount;
    }

                        
    /**
     * Set the value of UnbilledGoodsAccount
     *
     * @param v new value
     */
    public void setUnbilledGoodsAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unbilledGoodsAccount, v))
              {
            this.unbilledGoodsAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("KategoriId");
              fieldNames.add("InventoryAccount");
              fieldNames.add("SalesAccount");
              fieldNames.add("SalesReturnAccount");
              fieldNames.add("ItemDiscountAccount");
              fieldNames.add("CogsAccount");
              fieldNames.add("PurchaseReturnAccount");
              fieldNames.add("ExpenseAccount");
              fieldNames.add("UnbilledGoodsAccount");
              fieldNames.add("CreateBy");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("KategoriId"))
        {
                return getKategoriId();
            }
          if (name.equals("InventoryAccount"))
        {
                return getInventoryAccount();
            }
          if (name.equals("SalesAccount"))
        {
                return getSalesAccount();
            }
          if (name.equals("SalesReturnAccount"))
        {
                return getSalesReturnAccount();
            }
          if (name.equals("ItemDiscountAccount"))
        {
                return getItemDiscountAccount();
            }
          if (name.equals("CogsAccount"))
        {
                return getCogsAccount();
            }
          if (name.equals("PurchaseReturnAccount"))
        {
                return getPurchaseReturnAccount();
            }
          if (name.equals("ExpenseAccount"))
        {
                return getExpenseAccount();
            }
          if (name.equals("UnbilledGoodsAccount"))
        {
                return getUnbilledGoodsAccount();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(KategoriAccountPeer.KATEGORI_ID))
        {
                return getKategoriId();
            }
          if (name.equals(KategoriAccountPeer.INVENTORY_ACCOUNT))
        {
                return getInventoryAccount();
            }
          if (name.equals(KategoriAccountPeer.SALES_ACCOUNT))
        {
                return getSalesAccount();
            }
          if (name.equals(KategoriAccountPeer.SALES_RETURN_ACCOUNT))
        {
                return getSalesReturnAccount();
            }
          if (name.equals(KategoriAccountPeer.ITEM_DISCOUNT_ACCOUNT))
        {
                return getItemDiscountAccount();
            }
          if (name.equals(KategoriAccountPeer.COGS_ACCOUNT))
        {
                return getCogsAccount();
            }
          if (name.equals(KategoriAccountPeer.PURCHASE_RETURN_ACCOUNT))
        {
                return getPurchaseReturnAccount();
            }
          if (name.equals(KategoriAccountPeer.EXPENSE_ACCOUNT))
        {
                return getExpenseAccount();
            }
          if (name.equals(KategoriAccountPeer.UNBILLED_GOODS_ACCOUNT))
        {
                return getUnbilledGoodsAccount();
            }
          if (name.equals(KategoriAccountPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(KategoriAccountPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getKategoriId();
            }
              if (pos == 1)
        {
                return getInventoryAccount();
            }
              if (pos == 2)
        {
                return getSalesAccount();
            }
              if (pos == 3)
        {
                return getSalesReturnAccount();
            }
              if (pos == 4)
        {
                return getItemDiscountAccount();
            }
              if (pos == 5)
        {
                return getCogsAccount();
            }
              if (pos == 6)
        {
                return getPurchaseReturnAccount();
            }
              if (pos == 7)
        {
                return getExpenseAccount();
            }
              if (pos == 8)
        {
                return getUnbilledGoodsAccount();
            }
              if (pos == 9)
        {
                return getCreateBy();
            }
              if (pos == 10)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(KategoriAccountPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        KategoriAccountPeer.doInsert((KategoriAccount) this, con);
                        setNew(false);
                    }
                    else
                    {
                        KategoriAccountPeer.doUpdate((KategoriAccount) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key kategoriId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setKategoriId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setKategoriId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getKategoriId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public KategoriAccount copy() throws TorqueException
    {
        return copyInto(new KategoriAccount());
    }
  
    protected KategoriAccount copyInto(KategoriAccount copyObj) throws TorqueException
    {
          copyObj.setKategoriId(kategoriId);
          copyObj.setInventoryAccount(inventoryAccount);
          copyObj.setSalesAccount(salesAccount);
          copyObj.setSalesReturnAccount(salesReturnAccount);
          copyObj.setItemDiscountAccount(itemDiscountAccount);
          copyObj.setCogsAccount(cogsAccount);
          copyObj.setPurchaseReturnAccount(purchaseReturnAccount);
          copyObj.setExpenseAccount(expenseAccount);
          copyObj.setUnbilledGoodsAccount(unbilledGoodsAccount);
          copyObj.setCreateBy(createBy);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setKategoriId((String)null);
                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public KategoriAccountPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("KategoriAccount\n");
        str.append("---------------\n")
           .append("KategoriId           : ")
           .append(getKategoriId())
           .append("\n")
           .append("InventoryAccount     : ")
           .append(getInventoryAccount())
           .append("\n")
           .append("SalesAccount         : ")
           .append(getSalesAccount())
           .append("\n")
           .append("SalesReturnAccount   : ")
           .append(getSalesReturnAccount())
           .append("\n")
           .append("ItemDiscountAccount  : ")
           .append(getItemDiscountAccount())
           .append("\n")
           .append("CogsAccount          : ")
           .append(getCogsAccount())
           .append("\n")
            .append("PurchaseReturnAccount   : ")
           .append(getPurchaseReturnAccount())
           .append("\n")
           .append("ExpenseAccount       : ")
           .append(getExpenseAccount())
           .append("\n")
           .append("UnbilledGoodsAccount   : ")
           .append(getUnbilledGoodsAccount())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

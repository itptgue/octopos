package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.ShiftTool;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

public class CashierDailyReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    	    if(data.getParameters().getInt("op") == 1)
    	    {
	    	    Date dStart = CustomParser.parseDate(data.getParameters().getString("StartDate"));
	    	    Date dEnd = CustomParser.parseDate(data.getParameters().getString("EndDate"));
	    	    String sSHour = data.getParameters().getString("StartHour","00");
	    	    String sSMinute = data.getParameters().getString("StartMinute","00");
	    	    String sEHour = data.getParameters().getString("EndHour","23");
	    	    String sEMinute = data.getParameters().getString("EndMinute","59");
	    	    String sCashier = data.getParameters().getString ("CashierName", "");
	    	    String sLocID = data.getParameters().getString ("LocationId", "");
                
	    	    if (dStart == null)dStart = new Date();
	    	    if (dEnd == null) dEnd = new Date();

			    dStart = DateUtil.getDateWithHourMinute(dStart,sSHour,sSMinute);
			    dEnd = DateUtil.getDateWithHourMinute(dEnd,sEHour,sEMinute);
			    
			    if(StringUtil.isEmpty(sCashier))
			    {
			        if(StringUtil.isNotEmpty(sLocID))
			        {
			            context.put ("vCashier",EmployeeTool.getEmployeeByLocationID(sLocID));
			        }
			        else
			        {
			            context.put ("vCashier",EmployeeTool.getAllEmployee());
			        }
			    }
			    else
			    {
			        context.put ("vCashier",EmployeeTool.findData(4, sCashier, sLocID));
			    }
	    	 }
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Sales Report LookUp Failed : " + _oEx.getMessage());
    	}    
    	context.put("cashierbalance", CashierBalanceTool.getInstance());
    	context.put("shift", ShiftTool.getInstance());
    	
    }   
}

package com.ssti.enterprise.pos.tools.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/inventory/SKUTool.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SKUTool.java,v 1.2 2005/08/29 10:13:22 albert Exp $
 *
 * $Log: SKUTool.java,v $
 * Revision 1.2  2005/08/29 10:13:22  albert
 * *** empty log message ***
 *
 * Revision 1.1  2005/05/27 02:05:56  Albert
 * *** empty log message ***
 *
 */

public class SKUTool extends BaseTool
{
	private static final Log log = LogFactory.getLog(SKUTool.class);
	/**
	 * get all existing plu for this SKU
	 * 
	 * @param _sSKU
	 * @return
	 * @throws Exception
	 */
	public static List getAllExistingPLU(String _sSKU)
		throws Exception
	{
		String sSQL = "select item_code from item where item_sku = '" + _sSKU + "'";
		return BasePeer.executeQuery(sSQL);
	}

	/**
	 * get all existing plu for this SKU
	 * 
	 * @param _sSKU
	 * @return
	 * @throws Exception
	 */
	public static List getAllExistingPLU(Map _mSKU)
		throws Exception
	{
		List vPLU = new ArrayList();
		if (_mSKU != null)
		{
			Iterator iter = _mSKU.keySet().iterator();
			while (iter.hasNext())
			{
				String sSKU = (String) iter.next();
				List vPLUTMP = getAllExistingPLU(sSKU);
				vPLU.addAll(vPLUTMP);
			}
		}
		return vPLU;
	}
	
	/**
	 * get all existing plu for this SKU
	 * 
	 * @param _sSKU
	 * @return
	 * @throws Exception
	 */
	public static Set getUniquePLUCode(Map _mSKU)
		throws Exception
	{
		SortedSet vCode = new TreeSet();
		if (_mSKU != null)
		{
			Iterator iter = _mSKU.keySet().iterator();
			while (iter.hasNext())
			{
				String sSKU = (String) iter.next();
				List vPLU = getAllExistingPLU(sSKU);
				for (int i = 0; i < vPLU.size(); i++)
				{
					String sPLU = ((Record)vPLU.get(i)).getValue(1).asString();											
					if (sPLU.length() >= sSKU.length())
					{
						String sCode = sPLU.substring(sSKU.length());
						vCode.add(sCode);
					}
				}					
			}
		}
		return vCode;
	}

	
	/**
	 * get all existing plu for this SKU
	 * 
	 * @param _sSKU
	 * @return
	 * @throws Exception
	 */
	public static Object filterByCode (String _sSKU, String _sDiff, List _vTransDet)
		throws Exception
	{
		String sCode = _sSKU + _sDiff;
		List vTD = new ArrayList();
		for (int i = 0; i < _vTransDet.size(); i++)
		{
			Object oDetail = _vTransDet.get(i);
			String sItemCode = (String) BeanUtil.invokeGetter(oDetail, "itemCode");
			if (sItemCode != null && sItemCode.equals(sCode))
			{
				vTD.add(oDetail);
				//return oDetail;
			}
		}
		return vTD;		 
	}

	/**
	 * build Horizontal view map that contains a Map of SKU that matched the view
	 * 
	 * @param a_HORIZONTAL_VIEW
	 * @param _vTransDet
	 * @return
	 * @throws Exception
	 */
	public static Map buildHorizontalMatrix(String[] a_HORIZONTAL_VIEW, List _vTransDet)
		throws Exception
	{
		Map hResult = null;
		if (a_HORIZONTAL_VIEW != null)
		{
			hResult = new HashMap(a_HORIZONTAL_VIEW.length);
			for (int i = 0; i < a_HORIZONTAL_VIEW.length; i++)
			{
				String sHView = a_HORIZONTAL_VIEW[i];
				List vFiltered = filterTransDetByHView (sHView, _vTransDet);
				hResult.put(sHView, mapBySKU(vFiltered));
			}
		}
		return hResult;
	}
	
	/**
	 * filter transaction detail to include only matched PLU
	 * if sHView = "27,28,29,30" and PLU = "ABC<b>30</b>" and SKU = "ABC" 
	 * then it will be included to the result List
	 * this method uses <code>java.lang.String</code> contains method 
	 * each sHView must be very unique among others
	 * 
	 * @param _sHView
	 * @param _vTransDet
	 * @return
	 * @throws Exception
	 */
	public static List filterTransDetByHView (String _sHView, List _vTransDet)
		throws Exception
	{
		log.debug("sHView : " + _sHView);
		
		List vResult = new ArrayList (_vTransDet.size());
		for (int i = 0; i < _vTransDet.size(); i++)
		{
			Persistent oDetail = (Persistent) _vTransDet.get(i);
			String sItemID = (String) BeanUtil.invokeGetter(oDetail, "itemId");
			Item oItem = ItemTool.getItemByID(sItemID);					
			String sCodeDiff = oItem.getItemCode().substring(oItem.getItemSku().length());

			log.debug("Item Code  : " + oItem.getItemCode());
			log.debug("Item SKU   : " + oItem.getItemSku());
			log.debug("Difference : " + sCodeDiff);
			
			String[] aHView = StringUtil.split(_sHView, ",");
			for (int j = 0; j < aHView.length; j++)
			{
				if (aHView[j].equals(sCodeDiff))
				{
					log.debug("Code Included : " + sCodeDiff );
					vResult.add(oDetail);					
				}
			}
		}
		return vResult;
	}
	
	/**
	 * map by SKU
	 * 
	 * @param _vTransDet
	 * @return
	 * @throws Exception
	 */
	public static Map mapBySKU(List _vTransDet)
		throws Exception
	{
		Map hResult = new TreeMap();
		Collection vSKU = getDistinctSKU (_vTransDet);
		Iterator oIter = vSKU.iterator();
		while (oIter.hasNext())
		{
			String sSKU = (String) oIter.next();
			List vTransPerSKU = filterTransBySKU(_vTransDet, sSKU);
			hResult.put(sSKU, vTransPerSKU);
		}
		return hResult;
	}
	
	public static Collection getDistinctSKU (List _vTransDet)
		throws Exception
	{
		Set vSKU = new HashSet(_vTransDet.size());
		for (int i = 0; i < _vTransDet.size(); i++)
		{
			Object oDetail = (Object) _vTransDet.get(i);
			String sItemID = (String) BeanUtil.invokeGetter(oDetail, "itemId");
			Item oItem = ItemTool.getItemByID(sItemID);
			if (oItem != null)
			{
				vSKU.add(oItem.getItemSku());
			}
		}
		return vSKU;
	}

	/**
	 * filter 
	 * @param _vTransDet
	 * @param _sSKU
	 * @return
	 * @throws Exception
	 */
	public static List filterTransBySKU (List _vTransDet, String _sSKU)
		throws Exception
	{
		List vPLU = new ArrayList(_vTransDet.size());
		for (int i = 0; i < _vTransDet.size(); i++)
		{
			Object oDetail = (Object) _vTransDet.get(i);
			String sItemID = (String) BeanUtil.invokeGetter(oDetail, "itemId");
			Item oItem = ItemTool.getItemByID(sItemID);
			if (oItem != null)
			{
				if (oItem.getItemSku().equals(_sSKU))
				{
					vPLU.add(oDetail);
				}
			}
		}
		return vPLU;
	}	
}

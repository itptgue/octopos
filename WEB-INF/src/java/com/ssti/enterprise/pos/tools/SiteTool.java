package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.SiteManager;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.Site;
import com.ssti.enterprise.pos.om.SitePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SiteTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: SiteTool.java,v $
 * Revision 1.8  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/10 04:51:48  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class SiteTool extends BaseTool 
{
	public static List getAllSite()
		throws Exception
	{
		return SiteManager.getInstance().getAllSite();
	}
	
	public static Site getSiteByID(String _sID)
    	throws Exception
    {
		return SiteManager.getInstance().getSiteByID(_sID, null);
	}
	public static Site getSiteByID(String _sID, Connection _oConn)
		throws Exception
	{
		return SiteManager.getInstance().getSiteByID(_sID, _oConn);
	}
	
	public static Site getSiteByName(String _sName)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(SitePeer.SITE_NAME, _sName);
		List vData = SitePeer.doSelect(oCrit);
		if (vData.size() > 0) {
			return (Site) vData.get(0);
		}
		return null;
	}
	
	public static String getIDByName(String _sName)
		throws Exception
	{
		String sID = "";
		Site oSite = getSiteByName(_sName);
		if(oSite != null)
		{
			sID = oSite.getSiteId();	
		}
		
		return sID;
	}

	public static String getDescriptionByID(String _sID)
    	throws Exception
    {
		Site oSite = getSiteByID (_sID);
		if (oSite != null) {
			return oSite.getDescription();
		}
		return "";
	}
	
	public static String getSiteNameByID(String _sID)
	throws Exception
	{
		Site oSite = getSiteByID (_sID);
		if (oSite != null) {
			return oSite.getSiteName();
		}
		return "";
	}

	public static int getCostingMethodByID(String _sID)
    	throws Exception
    {
		Site oSite = getSiteByID (_sID);
		if (oSite != null) {
			return oSite.getCostingMethod();
		}
		return -1;
	}
	
	public static String getNameByLocID(String _sLocID)
		throws Exception
	{
		Location oLoc = LocationTool.getLocationByID(_sLocID);
		if (oLoc != null)
		{
			Site oSite = getSiteByID (oLoc.getSiteId());
			if (oSite != null) 
			{
				return oSite.getSiteName();
			}
		}
		return "";
	}
}

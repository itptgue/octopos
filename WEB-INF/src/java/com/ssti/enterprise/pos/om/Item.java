package com.ssti.enterprise.pos.om;
import java.math.BigDecimal;
import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.manager.DiscountManager;
import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.ItemStatusCodeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseAnalysisTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/om/Item.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: Item.java,v 1.4 2006/03/22 15:53:58 albert Exp $
 *
 * $Log: Item.java,v $
 * 
 * 2018-08-25
 * -add getBestDisc && getBestDiscVendor
 *
 */
public  class Item
    extends com.ssti.enterprise.pos.om.BaseItem
    implements Persistent, MasterOM
{
	public String getId()
	{
		return getItemId();
	}

	public String getCode()
	{
		return getItemCode();
	}
	
	public String getName()
	{
		return getItemName();
	}
	//non-persistent helper field
	boolean fromPriceList = false;
	
	public boolean isFromPriceList() {
		return fromPriceList;
	}
	public void setFromPriceList(boolean fromPriceList) {
		this.fromPriceList = fromPriceList;
	}
	
	public String getLastPurchCurrCode()
	{
		return getLastPurchaseCurr();
	}
	
	public String getLastPurchCurrId()
	{
		try 
		{
			Currency oCurr = CurrencyTool.getCurrencyByCode(getLastPurchaseCurr());
			if (oCurr != null) return oCurr.getCurrencyId();		
		} 
		catch (Exception e) 
		{
		}
		return "";
	}
	
	public BigDecimal getPrice(String _sLocID, String _sCustID)
	{
		return ItemTool.getPrice(getItemId(), _sLocID, _sCustID, null);
	}
		
	double qtyOnSO = -1;
	public double getOnSO()
	{
		if (qtyOnSO < 0)
		{
			try
			{
				qtyOnSO = SalesOrderTool.getOnOrderQty(getItemId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return qtyOnSO;
	}

	double qtyOnPO = -1;
	public double getOnPO()
	{
		if (qtyOnPO < 0)
		{
			try
			{
				qtyOnPO = PurchaseOrderTool.getOnOrderQty(getItemId());				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return qtyOnPO;
	}

	Brand brandData;
	public Brand getBrandData()
	{
		try
		{
			if (brandData == null)
			{
				brandData = ItemFieldTool.getBrandByName(getBrand());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return brandData;
	}

	Principal principal;
	public Principal getPrincipalData()
	{
		try
		{
			if (principal == null)
			{
				principal = ItemFieldTool.getPrincipalByName(getManufacturer());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return principal;
	}

	
	Kategori kategori;
	public Kategori getKategori()
	{
		try
		{
			if (kategori == null)
			{
				kategori = KategoriTool.getKategoriByID(getKategoriId());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return kategori;
	}
	
	Unit unit;
	public Unit getUnit()
	{
		try
		{
			if (unit == null) unit = UnitTool.getUnitByID(getUnitId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return unit;
	}

	Unit purchaseUnit;
	public Unit getPurchaseUnit()
	{
		try
		{
			if(purchaseUnit == null) purchaseUnit = UnitTool.getUnitByID(getPurchaseUnitId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return purchaseUnit;
	}

	Tax tax;
	public Tax getTax()
	{
		try
		{
			if(tax == null) tax = TaxTool.getTaxByID(getTaxId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return tax;
	}

	Tax purchTax;
	public Tax getPurchaseTax()
	{
		try
		{
			if(purchTax == null) purchTax = TaxTool.getTaxByID(getPurchaseTaxId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return purchTax;
	}

	ItemStatusCode statCode;
	public ItemStatusCode getItemStatusCode()
	{
		try
		{
			if(statCode == null && StringUtil.isNotEmpty(getItemStatusCodeId())) 
			{
				statCode = ItemStatusCodeTool.getItemStatusCodeByID(getItemStatusCodeId());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return statCode;
	}

	Vendor prefVendor;
	public Vendor getPreferedVendor()
	{
		try
		{
			if(prefVendor == null && StringUtil.isNotEmpty(getPreferedVendorId())) 
			{
				prefVendor = VendorTool.getVendorByID(getPreferedVendorId());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return prefVendor;
	}
	
	Location warehouseLoc;
	public Location getWarehouseLoc()
	{
		try
		{
			if(warehouseLoc == null && StringUtil.isNotEmpty(getWarehouseId())) 
			{
				warehouseLoc = LocationTool.getLocationByID(getWarehouseId());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return warehouseLoc;
	}
	
	public String getValue(String fieldName)
	{
		if(StringUtil.isNotEmpty(fieldName))
		{
			try
			{
				return ItemFieldTool.getValue(getItemId(), fieldName);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return "";
	}
	
	public InventoryLocation getInvLoc(String locId)
	{
		if(StringUtil.isNotEmpty(getItemId()) && StringUtil.isNotEmpty(locId))
		{
			try
			{
				return InventoryLocationTool.getDataByItemAndLocationID(getItemId(), locId);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;		
	}
	
	public ItemInventory getInvLevel(String locId)
	{
		if(StringUtil.isNotEmpty(getItemId()) && StringUtil.isNotEmpty(locId))
		{
			try
			{
				return ItemInventoryTool.getItemInventory(getItemId(), locId);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;		
	}	
	
	public double getAvgSales(String _sLocID, int _iDays) 
		throws Exception
	{
		Double dAvg = ItemManager.getInstance().getAvgSales(getItemId(), _sLocID, _iDays, null);
		if (dAvg == null) dAvg = Double.valueOf(0);
		return dAvg;
	}
	
	public String getItemTypeDesc()
	{
		if(getItemType() == AppAttributes.i_INVENTORY_PART) return "Inventory";     
		if(getItemType() == AppAttributes.i_NON_INVENTORY_PART) return "Non Inventory"; 
		if(getItemType() == AppAttributes.i_GROUPING) return "Grouping";      
		if(getItemType() == AppAttributes.i_SERVICE) return "Service";  
		if(getItemType() == AppAttributes.i_ASSET) return "Asset";
		return "";
	}
	
	public String getDeliveryTypeDesc()
	{
		if(getDeliveryType() == 1) return "Direct"; else return "Normal";
	}
	
	public String getActiveStatusDesc()
	{
		if(getItemStatus()) return "Active"; else return "Non Active";
	}	

	public String getActiveStatusCode()
	{
		if(getItemStatus()) return "T"; else return "F";
	}	
	
	public double getProfitRate(Number _nCost) 
		throws Exception
	{
		try
		{
			
			double dPrice = getItemPrice().doubleValue();
			if(PreferenceTool.getSalesInclusiveTax())		
			{
				dPrice = dPrice / ((100 + getTax().getAmount().doubleValue()) / 100) ;
			}				
			//System.out.println("0 _dCost " + _nCost + " dPrice " + dPrice + " tax " + getTax().getAmount());
			return ItemTool.countMargin(dPrice, _nCost);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}		
		return 0;
	}
	
	public List getPromo(String _sLocID)
	{
		try 
		{
			return DiscountManager.getInstance().getByItem(getItemId(), getItemCode(), _sLocID);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean isPromo(String _sLocID)
	{
		if(getPromo(_sLocID) != null && getPromo(_sLocID).size() > 0)
		{
			return true;
		}
		return false;
	}

	/**
	 * Get best purchase by discount record
	 */
	Record bestDisc = null;
	public Record getBestDisc()
	{
		try 
		{
			if(bestDisc == null)
			{
				List v = PurchaseAnalysisTool.getBestPurchase(getItemId(), null, "", "", null, null, 1);
				if(v.size() > 0)
				{
					bestDisc = (Record) v.get(0);							
				}
				else
				{									
					bestDisc = new Record(); //prevent query again
				}
			}
		} 
		catch (Exception e) {			
			e.printStackTrace();
		}
		return bestDisc;
	}			
}

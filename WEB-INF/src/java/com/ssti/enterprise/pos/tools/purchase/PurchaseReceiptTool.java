package com.ssti.enterprise.pos.tools.purchase;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;

import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseOrderDetail;
import com.ssti.enterprise.pos.om.PurchaseOrderPeer;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetail;
import com.ssti.enterprise.pos.om.PurchaseReceiptDetailPeer;
import com.ssti.enterprise.pos.om.PurchaseReceiptPeer;
import com.ssti.enterprise.pos.om.PurchaseReturnDetail;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.inventory.BatchTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryCostingTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryTransactionTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;
import com.ssti.enterprise.pos.tools.journal.PurchaseJournalTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Business object controller for PurchaseReceipt and PurchaseReceiptDetail OM
 * @see PurchaseOrderTool, PurchaseReceipt, PurchaseReceiptDetail, PurchaseInvoiceTool
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseReceiptTool.java,v 1.30 2009/05/04 02:04:54 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseReceiptTool.java,v $
 * 
 * 2018-09-08
 * - change method updatePRFromPI, update cost now check from InventoryTransaction.updatePRCost
 * - change method updateFromPIDetail, add parameter update cost check from updatePRFromPI
 * 
 * 2016-03-08
 * - change method mapPODetailToPRDetail correct method auto set PO to PR qty
 * 
 * 2015-12-10
 * - change method saveData, use new InventoryCostingTool class for creating inventory transaction journal
 * - change method cancelTrans, use new InventoryCostingTool class for deleting inventory transaction journal 
 *
 * </pre><br>
 */
public class PurchaseReceiptTool extends BaseTool
{	
	private static Log log = LogFactory.getLog(PurchaseReceiptTool.class);
	
	private static final boolean b_AUTO_SETQTY = PreferenceTool.getPurchPrQtyEqualPo();
	
	public static final String s_ITEM = new StringBuilder(PurchaseReceiptPeer.PURCHASE_RECEIPT_ID).append(",")
	.append(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID).append(",")
	.append(PurchaseReceiptDetailPeer.ITEM_ID).toString();

	public static final String s_TAX = new StringBuilder(PurchaseReceiptPeer.PURCHASE_RECEIPT_ID).append(",")
	.append(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID).append(",")
	.append(PurchaseReceiptDetailPeer.TAX_ID).toString();
	
	protected static Map m_FIND_PEER = new HashMap();
	
	static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), PurchaseReceiptPeer.RECEIPT_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), PurchaseReceiptPeer.REMARK);        
		m_FIND_PEER.put (Integer.valueOf(i_VENDOR	  ), PurchaseReceiptPeer.VENDOR_ID);
		m_FIND_PEER.put (Integer.valueOf(i_VEND_DO	  ), PurchaseReceiptPeer.VENDOR_DELIVERY_NO);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), PurchaseReceiptPeer.CREATE_BY);                 
		m_FIND_PEER.put (Integer.valueOf(i_CONFIRM_BY ), PurchaseReceiptPeer.CONFIRM_BY);               
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), "");             
		m_FIND_PEER.put (Integer.valueOf(i_LOCATION   ), PurchaseReceiptPeer.LOCATION_ID);             		
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TYPE   ), "");
		m_FIND_PEER.put (Integer.valueOf(i_PMT_TERM   ), "");
		m_FIND_PEER.put (Integer.valueOf(i_COURIER    ), PurchaseReceiptPeer.COURIER_ID);
		m_FIND_PEER.put (Integer.valueOf(i_FOB 	      ), PurchaseReceiptPeer.FOB_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), PurchaseReceiptPeer.CURRENCY_ID);
		m_FIND_PEER.put (Integer.valueOf(i_PO_NO 	  ), PurchaseReceiptPeer.PURCHASE_ORDER_ID);        
		addInv(m_FIND_PEER, s_ITEM);
	}   

	protected static Map m_GROUP_PEER = new HashMap();

	static
	{   		
		m_GROUP_PEER.put (Integer.valueOf(i_ORDER_BY_DATE ), PurchaseReceiptPeer.RECEIPT_DATE); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_NONE  	  ), ""); 		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CUST_VEND  ), PurchaseReceiptPeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_STATUS     ), PurchaseReceiptPeer.STATUS);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOCATION   ), PurchaseReceiptPeer.LOCATION_ID);             
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CREATE_BY  ), PurchaseReceiptPeer.CREATE_BY);                 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CONFIRM_BY ), PurchaseReceiptPeer.CONFIRM_BY);               
		m_GROUP_PEER.put (Integer.valueOf(i_GB_PMT_TYPE   ), "");
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CURRENCY   ), PurchaseReceiptPeer.CURRENCY_ID);
		
		m_GROUP_PEER.put (Integer.valueOf(i_GB_TAX   	  ), s_TAX);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CATEGORY   ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_ITEM	 	  ), s_ITEM);
		m_GROUP_PEER.put (Integer.valueOf(i_GB_CV_ITEM    ), PurchaseReceiptPeer.VENDOR_ID); 
		m_GROUP_PEER.put (Integer.valueOf(i_GB_LOC_ITEM   ), PurchaseReceiptPeer.LOCATION_ID); 
	}
    
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}
	public static String groupSB (int _iSelected) {return groupSB(m_GROUP_PEER, true, _iSelected);}

	public static PurchaseReceipt getHeaderByID(String _sID)
		throws Exception
	{
		return getHeaderByID(_sID, null);
	}
	
	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return PurchaseReceipt object
	 * @throws Exception
	 */
	public static PurchaseReceipt getHeaderByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptPeer.PURCHASE_RECEIPT_ID, _sID);
        List vData = PurchaseReceiptPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) 
        {
			return (PurchaseReceipt) vData.get(0);
		}
		return null;
	}
	
	/**
	 * 
	 * @param _sID
	 * @return List of PurchaseReceiptDetail
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID)
    	throws Exception
    {
        return getDetailsByID(_sID,null);
    }

	/**
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return List of PurchaseReceiptDetail
	 * @throws Exception
	 */
	public static List getDetailsByID(String _sID,Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, _sID);
        oCrit.addAscendingOrderByColumn(PurchaseReceiptDetailPeer.INDEX_NO);        
        return PurchaseReceiptDetailPeer.doSelect(oCrit,_oConn);
	}
	
	/**
	 * getDetailByDetailID 
	 * 
	 * @param _sID
	 * @param _oConn
	 * @return PurchaseReceiptDetail object
	 * @throws Exception
	 */
	public static PurchaseReceiptDetail getDetailByDetailID(String _sID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_DETAIL_ID, _sID);
	    List vData = PurchaseReceiptDetailPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0) return (PurchaseReceiptDetail) vData.get(0);
	    return null;
	}		
	
	/**
	 * delete transaction details by ID
	 * 
	 * @param _sID
	 * @throws Exception
	 */
	public static void deleteDetailsByID (String _sID, Connection _oConn) 
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, _sID);
        PurchaseReceiptDetailPeer.doDelete (oCrit, _oConn);
	}	
	
	/**
	 * get receipt details by PO detail ID
	 * 
	 * @param _sPODetailID
	 * @return List of Purchase Receipt Details
	 * @throws Exception
	 */
	public static List getDetailsByPODetailID(String _sPODetailID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_ORDER_DETAIL_ID,_sPODetailID);
		List vData = PurchaseReceiptDetailPeer.doSelect (oCrit);
		return vData;
	}
	
	public static List getDetailsByPOIDAndStatus(String _sID, int _iStatus)
    	throws Exception
    {
        return getDetailsByPOIDAndStatus(_sID, _iStatus, null);
    }
	
	public static List getDetailsByPOIDAndStatus(String _sID, int _iStatus, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptPeer.PURCHASE_ORDER_ID, _sID);
        oCrit.add(PurchaseReceiptPeer.STATUS, _iStatus);
        oCrit.addJoin(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, PurchaseReceiptPeer.PURCHASE_RECEIPT_ID);
		return PurchaseReceiptDetailPeer.doSelect(oCrit,_oConn);
	}
		
	public static List getPurchaseReceiptByVendorID(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseReceiptPeer.VENDOR_ID, _sID);
		List vData = PurchaseReceiptPeer.doSelect(oCrit);
		return vData;
	}
		
	public static String getIDByPurchaseReceiptNo(String _sPRNo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseReceiptPeer.RECEIPT_NO, 
			(Object) SqlUtil.like (PurchaseReceiptPeer.RECEIPT_NO, _sPRNo), Criteria.CUSTOM);
		oCrit.add(PurchaseReceiptPeer.STATUS,i_PR_APPROVED);
		oCrit.setLimit(1);
		List vData = PurchaseReceiptPeer.doSelect (oCrit);
		if (vData.size() > 0) 
		{
			return ((PurchaseReceipt) vData.get(0)).getPurchaseReceiptId();
		}
		return "";
	}
	
	public static String getStatusString (int _iStatus)
	{
		if (_iStatus == i_PR_APPROVED)  return LocaleTool.getString("received");
		if (_iStatus == i_PR_REJECTED)  return LocaleTool.getString("rejected");
		if (_iStatus == i_PR_INVOICED)  return LocaleTool.getString("invoiced");
		if (_iStatus == i_PR_CANCELLED) return LocaleTool.getString("cancelled");
		return LocaleTool.getString("new");
	}	

	public static boolean isWithoutPO (String _sPurchaseReceiptID)
		throws Exception
	{
		List vPRD = getDetailsByID(_sPurchaseReceiptID);
		for(int i = 0; i < vPRD.size(); i++)
		{
			PurchaseReceiptDetail oDet = (PurchaseReceiptDetail) vPRD.get(i);
			if (StringUtil.isEmpty(oDet.getPurchaseOrderDetailId())) return true;
		}
		return false;	
	}

	/**
	 * check whether a PR is already exist in any PI

	 * @param _sPRID
	 * @return is po exist in any PI
	 * @throws Exception
	 */
	public static boolean isPOReceived(String _sPOID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PurchaseReceiptPeer.STATUS, i_PR_CANCELLED, Criteria.NOT_EQUAL);
	    oCrit.add(PurchaseReceiptPeer.PURCHASE_RECEIPT_ID, _sPOID);
		List vData = PurchaseInvoiceDetailPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	/**
 	 * this is the method to update PR from purchase invoice
 	 * first it will iterate through Invoice Detail then, foreach detail, it will 
 	 * take the inv detail PRID and PRDetailID
 	 * secondly, it will get the PR Detail from database then update the PR detail billed Qty
 	 * third, calls checkPaidStatus, if paid then set the PR Status to INVOICED
	 *
	 * @param List _vSIDet, List of Invoice Detail
	 * @param Connection _oConn, Connection Object
	 */	
	
	public static Map updatePRFromPI (PurchaseInvoice _oPI, 
									  List _vPID, 
									  boolean _bCancel,
									  Connection _oConn)
		throws Exception
	{
		Set vPRID = new HashSet(_vPID.size());
	    for(int i = 0; i < _vPID.size(); i++)
		{
	    	PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail)_vPID.get(i);
	    	if (StringUtil.isNotEmpty(oPID.getPurchaseReceiptId()))
	    	{
	    		vPRID.add(oPID.getPurchaseReceiptId());
	    	}
		}
	    
	    HashMap mPR = new HashMap(vPRID.size());
	    Iterator iter = vPRID.iterator();
	    while(iter.hasNext())
	    {
	    	String sPRID = (String) iter.next();
	    	PurchaseReceipt oPR = getHeaderByID(sPRID, _oConn);
	    	if (oPR != null)
	    	{
	    		boolean bUpdatePR = InventoryTransactionTool.updatePRCost(sPRID, _oConn);    		
	    		List vPRD = getDetailsByID(sPRID, _oConn);
	    		for (int i = 0; i < vPRD.size(); i++)
	    		{
	    			PurchaseReceiptDetail oPRD = (PurchaseReceiptDetail) vPRD.get(i);
	    			//validate PR Date if period is closed do not update PR
	    			updateFromPIDetail (oPR, oPRD, _vPID, bUpdatePR, _bCancel, _oConn);
	    		}	    		
	    		oPR.setDetail(vPRD);	    		
	    		if (!_bCancel) //set status INVOICED
	    		{
	    			updateStatus(oPR, true, i_PR_INVOICED,_oConn);
	    		}
	    		else //set status back to APPROVED
	    		{
	    			updateStatus(oPR, false, i_PR_APPROVED,_oConn);				
	    		}
	    		mPR.put(sPRID, oPR);
	    	}
	    }
	    return mPR; 
	}
	
	private static void updateFromPIDetail(PurchaseReceipt _oPR, 
										   PurchaseReceiptDetail _oPRD, 
										   List _vPID, boolean _bUpdatePRCost, boolean _bCancel,
										   Connection _oConn) 
		throws Exception 
	{
		for (int i = 0; i < _vPID.size(); i++)
		{
			PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail)_vPID.get(i);
			if (StringUtil.isEqual(_oPRD.getPurchaseReceiptDetailId(), oPID.getPurchaseReceiptDetailId()))
			{
				if (_bUpdatePRCost)
				{
					_oPRD.setCostPerUnit	(oPID.getCostPerUnit());
			        _oPRD.setItemPriceInPo	(oPID.getItemPrice());
			        _oPRD.setTaxId		    (oPID.getTaxId());
			        _oPRD.setTaxAmount      (oPID.getTaxAmount());
			        _oPRD.setDiscount       (oPID.getDiscount());
			        _oPRD.setSubTotal		(oPID.getSubTotal());
			        _oPRD.setLastPurchase	(oPID.getLastPurchase());				
				}
				if (!_bCancel)
				{
					_oPRD.setBilledQty(oPID.getQty());
				}
				else
				{
					_oPRD.setBilledQty(bd_ZERO);
				}
				_oPRD.save(_oConn);
			}
		}
	}
	
	/**
	 * update return qty from purchase return detail
	 * @param _sPRID
	 * @param _vReturnDetail
	 * @param _bCancel
	 * @param _oConn
	 * @throws Exception
	 */
	public static void updateReturnQty(String _sPRID, List _vReturnDetail, boolean _bCancel, Connection _oConn )
		throws Exception
	{
        try
        {
            for(int i = 0; i < _vReturnDetail.size(); i++)
            {
                PurchaseReturnDetail oPRetDet = (PurchaseReturnDetail) _vReturnDetail.get(i);
                PurchaseReceiptDetail oPRDet = 
                	getDetailByDetailID (oPRetDet.getTransactionDetailId(), _oConn);
                if (oPRDet != null)
                {
                    double dLastReturnedQty = oPRDet.getReturnedQty().doubleValue();
					double dReturnedQty = oPRetDet.getQty().doubleValue();
					if (_bCancel) dReturnedQty = dReturnedQty * -1;
                    oPRDet.setReturnedQty(new BigDecimal(dLastReturnedQty + dReturnedQty));
                    oPRDet.save(_oConn);
                }
            }
        }
        catch(Exception _oEx)
		{
		 	String sMsg = "Update Returned Qty From Purchase Return Error : " + _oEx.getMessage();
		 	_oEx.printStackTrace();
		 	log.error(sMsg, _oEx);
		 	throw new NestableException (sMsg, _oEx);
		}
	}	
	
	///////////////////////////////////////////////////////////////////////////
	// TRANSACTION PROCESSING
	///////////////////////////////////////////////////////////////////////////
		
	/**
	 * set purchase receipt properties with data from screen
	 * 
	 * @param _oPR
	 * @param _vPRD
	 * @param data
	 * @throws Exception
	 */
    public static void setHeaderProperties (PurchaseReceipt _oPR, List _vPRD, RunData data)
    	throws Exception
    {
    	
    	try
		{
    		if (data != null)
    		{
	    		ValueParser formData = data.getParameters();
				formData.setProperties (_oPR);
				_oPR.setVendorName(VendorTool.getVendorNameByID(_oPR.getVendorId()) );
				_oPR.setReceiptDate(CustomParser.parseDate(formData.getString("ReceiptDate")));
				if( _oPR.getStatus() != i_PR_NEW )
				{
					_oPR.setConfirmDate ( CustomParser.parseDate(formData.getString("ConfirmDate")) );
				}
				if( formData.getInt("Status") != i_PR_APPROVED &&
					formData.getInt("Status") != i_PR_REJECTED) 
				{
					_oPR.setCreateDate ( CustomParser.parseDate(formData.getString("CreateDate")) );
				}
    		}
    		_oPR.setTotalQty ( new BigDecimal(countQty (_vPRD)) );	
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException ("Set Header Properties Failed : " + _oEx.getMessage (), _oEx); 
		}
    }
    
	 
    /**
     * count total item received in a purchase receipt transaction
     * used when we need to check return amount
     * 
     * @param _sTransID
     * @param _sItemID
     * @return total item qty 
     * @throws Exception
     */
	public static double countTotalItemQty (String _sTransID, String _sItemID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, _sTransID);
        oCrit.add(PurchaseReceiptDetailPeer.ITEM_ID, _sItemID);
        List vData = PurchaseReceiptDetailPeer.doSelect(oCrit);
        if (vData.size() > 0) 
        {
        	PurchaseReceiptDetail oDetail = (PurchaseReceiptDetail) vData.get(0);
        	return oDetail.getQty().doubleValue();
        }
        return 0;
	}
    
	public static double countQty (List _vDetails)
		throws Exception
	{
		double dQty = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseReceiptDetail oDet = (PurchaseReceiptDetail) _vDetails.get(i);
			if (oDet.getQtyBase() == null) //if from add detail
			{
				oDet.setQtyBase (UnitTool.getBaseQty(oDet.getItemId(), oDet.getUnitId(), oDet.getQty()));
			}
			dQty += oDet.getQtyBase().doubleValue();
		}
		return dQty;
	}

	public static BigDecimal countSubTotal (List _vDetails)
	{
		double dAmt = 0;
		for (int i = 0; i < _vDetails.size(); i++)
		{
			PurchaseReceiptDetail oDet = (PurchaseReceiptDetail) _vDetails.get(i);
			dAmt += oDet.getSubTotal().doubleValue();
		}
		return new BigDecimal(dAmt);
	}    
	
	/**
	 * set list of detail data 
	 * @param _vDet
	 * @param data
	 * @throws Exception
	 */
    public static void updateDetail (List _vDet, RunData data)
    	throws Exception
    {
		try
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				PurchaseReceiptDetail oDetail = (PurchaseReceiptDetail) _vDet.get(i);
				log.debug ("Before update : "  + oDetail);
				
				oDetail.setQty(data.getParameters().getBigDecimal("Qty" + (i + 1)) );
				oDetail.setQtyBase(UnitTool.getBaseQty(oDetail.getItemId(), oDetail.getUnitId(), oDetail.getQty()));				
				oDetail.setSubTotal(data.getParameters().getBigDecimal("SubTotal" + (i + 1)) );

				log.debug ("After update : "  + oDetail);
				_vDet.set (i, oDetail);
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			throw new NestableException ("UPDATE Detail QTY Failed : " + _oEx.getMessage (), _oEx); 
		}
    }	
	//end update from screens function


	/**
	 * map po object to pi object
	 * 
	 * @param _oPO
	 * @param _oPR
	 * @throws Exception
	 */
	public static void mapPOHeaderToPRHeader (PurchaseOrder _oPO, 
											  PurchaseReceipt _oPR)
		throws Exception
	{
		String sLocationID = _oPO.getLocationId();
		if (StringUtil.isEmpty(sLocationID)) sLocationID = PreferenceTool.getLocationID();
		Location oLocation = LocationTool.getLocationByID (sLocationID);
		
		double dRate = CurrencyTool.getRateByID(_oPO.getCurrencyId(), _oPR.getReceiptDate());		
		if (dRate == 1) dRate = _oPO.getCurrencyRate().doubleValue();
		
		_oPR.setPurchaseOrderId		(_oPO.getPurchaseOrderId	    ());	
        _oPR.setLocationId			(oLocation.getLocationId   		());
        _oPR.setLocationName      	(oLocation.getLocationName 		());
        
	    //_oPR.setReceiptDate		(new Date());
        _oPR.setVendorId			(_oPO.getVendorId());
        _oPR.setVendorName			(_oPO.getVendorName());
        _oPR.setTotalQty			(_oPO.getTotalQty());
        _oPR.setCreateBy			(_oPO.getConfirmBy());
        _oPR.setRemark				(_oPO.getRemark());
        _oPR.setTotalDiscountPct	(_oPO.getTotalDiscountPct());
        _oPR.setPaymentTypeId		(_oPO.getPaymentTypeId());
        _oPR.setPaymentTermId		(_oPO.getPaymentTermId());
        _oPR.setCurrencyId		    (_oPO.getCurrencyId());
        _oPR.setCurrencyRate		(new BigDecimal(dRate));
        _oPR.setFiscalRate			(_oPO.getFiscalRate());
        _oPR.setFobId				(_oPO.getFobId());
        _oPR.setCourierId			(_oPO.getCourierId());
        _oPR.setIsInclusiveTax		(_oPO.getIsInclusiveTax());
        _oPR.setIsTaxable			(_oPO.getIsTaxable());
        
        _oPR.setStatus 			    (i_PR_NEW);
	}
	
	/**
	 * map po detail to pr detail
	 * 
	 * @param _oPODet
	 * @param _oPRDet
	 * @throws Exception
	 */
	public static void mapPODetailToPRDetail (PurchaseOrder _oPO,
											  PurchaseReceipt _oPR,
											  PurchaseOrderDetail _oPODet, 
											  PurchaseReceiptDetail _oPRDet)
		throws Exception
	{
		_oPRDet.setPurchaseOrderDetailId (_oPODet.getPurchaseOrderDetailId());	
		_oPRDet.setIndexNo				 (_oPODet.getIndexNo());
        _oPRDet.setItemId       		 (_oPODet.getItemId());
        _oPRDet.setItemCode       		 (_oPODet.getItemCode());
        _oPRDet.setItemName       		 (_oPODet.getItemName());
        _oPRDet.setDescription			 (_oPODet.getDescription());
        _oPRDet.setUnitId				 (_oPODet.getUnitId());
        _oPRDet.setUnitCode				 (_oPODet.getUnitCode());
        double dUnitBaseValue = UnitTool.getBaseValue(_oPRDet.getItemId(), _oPRDet.getUnitId());
        double dNewQty = _oPODet.getQty().doubleValue() - _oPODet.getReceivedQty().doubleValue();
        
        if(b_AUTO_SETQTY)
        {
            _oPRDet.setQty	 (new BigDecimal(dNewQty));
            _oPRDet.setQtyBase (new BigDecimal(dNewQty * dUnitBaseValue));
        }
        else
        {
            _oPRDet.setQty	 (bd_ZERO);
            _oPRDet.setQtyBase (bd_ZERO);
        }
        
        //recalculate cost perunit
        double dPORate = _oPO.getCurrencyRate().doubleValue();
        double dPRRate = _oPR.getCurrencyRate().doubleValue();
    	double dCost = _oPODet.getCostPerUnit().doubleValue();
    	
        if (dPORate != dPRRate)
        {
        	dCost = dCost / dPORate * dPRRate;
        }
        
        _oPRDet.setItemPriceInPo		(_oPODet.getItemPrice());
        _oPRDet.setTaxId		        (_oPODet.getTaxId());
        _oPRDet.setTaxAmount      		(_oPODet.getTaxAmount());
        _oPRDet.setDiscount             (_oPODet.getDiscount());
        _oPRDet.setSubTotal				(_oPODet.getSubTotal());
        _oPRDet.setLastPurchase			(_oPODet.getLastPurchase());
        _oPRDet.setCostPerUnit			(new BigDecimal(dCost));   
        _oPRDet.setDepartmentId			(_oPODet.getDepartmentId());
        _oPRDet.setProjectId			(_oPODet.getProjectId());        
	}
	//END from PO methods    
    	
	private static void updateStatus(PurchaseReceipt _oPR, boolean _bStatus, int _iStatus, Connection _oConn)
		throws Exception
	{
		_oPR.setIsBill(_bStatus);
		_oPR.setStatus(_iStatus);
		_oPR.save(_oConn);	
	}
	
	public static double getCostPerUnitByDetailID(String _sPRDetailID)
	 	throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_DETAIL_ID, _sPRDetailID);
        List vData = PurchaseReceiptDetailPeer.doSelect(oCrit);
        if (vData.size() > 0) 
        {
        	PurchaseReceiptDetail oDetail = (PurchaseReceiptDetail) vData.get(0);
        	return oDetail.getCostPerUnit().doubleValue();
        }
        return 0;
	}
	
	/**
	 * Save purchase receipt Data
	 * 
	 * @param _oPR
	 * @param _vPRD
	 * @param _oConn
	 * @param _iConfirm
	 * @param _bClosePO
	 * @throws Exception
	 */
	public static void saveData (PurchaseReceipt _oPR, 
	                             List _vPRD, 
	                             Connection _oConn, 
	                             int _iConfirm, 
	                             boolean _bClosePO)
		throws Exception
	{
		boolean bStartTrans = false;
		boolean bNew = false;
		if (_oConn == null) 
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		validate(_oConn);
		try
		{
			//validate date
			validateDate(_oPR.getReceiptDate(), _oConn);
			
			if (_iConfirm == i_PR_APPROVED)
			{
				//validate batch
				BatchTransactionTool.validateBatch(_vPRD, _oConn);
				ItemSerialTool.validateSerial(_vPRD,_oConn);
			}
			
	        if(StringUtil.isEmpty(_oPR.getPurchaseReceiptId())) bNew = true;
	        
			processSavePRData (_oPR, _vPRD, _oConn);
			
			if(_oPR.getStatus() == i_PR_APPROVED && _iConfirm == i_PR_APPROVED)
			{
				//create inventory transaction and update inventory location
				InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
				oCT.createFromPR(_oPR, _vPRD);
   			
                //Update PO From PR
    			PurchaseOrderTool.updatePOFromPR(_oPR.getPurchaseOrderId(), _vPRD, _bClosePO, false, _oConn);
    			
    			//if transaction not in store Journal to GL
				if (b_USE_GL && PreferenceTool.getLocationFunction() != i_STORE)
				{
					PurchaseJournalTool oJournal = new PurchaseJournalTool(_oPR, _oConn);
					oJournal.createPRJournal (_oPR, _vPRD);
				}
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx)
		{
			_oPR.setStatus(i_PR_NEW);
			_oPR.setReceiptNo("");
			
			if (bNew)
			{
				_oPR.setNew(true);
				_oPR.setPurchaseReceiptId("");
			}
			
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException (_oEx.getMessage(),_oEx);
		}
	}
	
	/**
	 * save PR and Details to DB
	 * 
	 * @param _oPR
	 * @param _vPRD
	 * @param _oConn
	 * @throws Exception
	 */
	private static void processSavePRData (PurchaseReceipt _oPR, List _vPRD, Connection _oConn) 
		throws Exception
	{
		validate(_oConn);
        if(StringUtil.isEmpty(_oPR.getPurchaseReceiptId()))
        {
        	_oPR.setPurchaseReceiptId (IDGenerator.generateSysID());
        	_oPR.setNew(true);
        }
        
        validateID(getHeaderByID(_oPR.getPurchaseReceiptId(), _oConn), "receiptNo");

		String sLocationID = _oPR.getLocationId();
		if (StringUtil.isEmpty(sLocationID)) sLocationID = PreferenceTool.getLocationID();
        Location oLocation = LocationTool.getLocationByID (sLocationID, _oConn);
  	 	_oPR.setLocationName (oLocation.getLocationName());
  	 	String sLocCode = "";
		if (!PreferenceTool.syncInstalled())
		{
			sLocCode = oLocation.getLocationCode();
		}
  	 	
		//id status approved then generate trans number
		if(_oPR.getStatus() == i_PR_APPROVED || _oPR.getStatus() == i_PR_REJECTED)
        {
			if (StringUtil.isEmpty(_oPR.getReceiptNo()))
			{
				_oPR.setConfirmDate(new Date());
				_oPR.setReceiptNo(
			        LastNumberTool.generateByVendor(_oPR.getVendorId(),_oPR.getLocationId(),sLocCode,s_PR_FORMAT,LastNumberTool.i_PURCHASE_RECEIPT, _oConn)						
				);
			}
			validateNo(PurchaseReceiptPeer.RECEIPT_NO,_oPR.getReceiptNo(),PurchaseReceiptPeer.class, _oConn);
        }
        else
        {
        	_oPR.setCreateDate(new Date());
	    	_oPR.setConfirmBy ("");
            _oPR.setReceiptNo("");
        }
        
		_oPR.save (_oConn);
	 	
		//delete all existing detail from pending transaction
		if (StringUtil.isNotEmpty(_oPR.getPurchaseReceiptId()))
		{
			deleteDetailsByID (_oPR.getPurchaseReceiptId(), _oConn);
		}			
		
		//delete existing tmp batch
		BatchTransactionTool.clearTmpBatch(_oPR.getPurchaseReceiptId(), i_INV_TRANS_PURCHASE_RECEIPT, _oConn);
		
		renumber(_vPRD);
		//map of PO Detail, because PR can be split row for batch transaction
		Iterator oIter = _vPRD.iterator();
		while (oIter.hasNext())
		{
			PurchaseReceiptDetail oTD = (PurchaseReceiptDetail) oIter.next();
			if(StringUtil.isEmpty(oTD.getPurchaseReceiptDetailId())) 
			{
				oTD.setPurchaseReceiptDetailId ( IDGenerator.generateSysID() );
			}
			oTD.setNew(true);
			oTD.setModified(true);
			oTD.setReturnedQty(bd_ZERO);
			oTD.setBilledQty(bd_ZERO);
			oTD.setPurchaseReceiptId (_oPR.getPurchaseReceiptId());
            
			double dQty = oTD.getQty().doubleValue();
			double dQtyBase =  dQty * UnitTool.getBaseValue(oTD.getItemId(), oTD.getUnitId(), _oConn);
   			oTD.setQtyBase (new BigDecimal(dQtyBase));
			   			
   			if (!b_ALLOW_GREATER_PR_QTY && StringUtil.isNotEmpty(oTD.getPurchaseReceiptDetailId()))
   			{   				
   				//validate PR qty > PO qty
   				PurchaseOrderDetail oPOD = PurchaseOrderTool.getDetailsByPODetailID(oTD.getPurchaseOrderDetailId(), _oConn);
   				if(oPOD != null)
   				{
   					double dQtyByPOD = dQty;
   					if(PreferenceTool.useBatchNo())
   					{
   						List vByPOD = BeanUtil.filterListByFieldValue(_vPRD, "purchaseOrderDetailId", oTD.getPurchaseOrderDetailId());
   						dQtyByPOD = BeanUtil.sumUp(vByPOD, "qty");
   					}
   					double dPOQty = oPOD.getQty().doubleValue();
   					double dReceived = oPOD.getReceivedQty().doubleValue();
   					if (dQtyByPOD + dReceived > dPOQty)
   					{
   	   					throw new Exception (
   	    				  "Item " + oTD.getItemCode() + " PR Qty(" + dQtyByPOD + 
   	    				  ") + Received Qty(" + dReceived + ") may not > PO Qty (" + dPOQty + ")");
   					}
   				}
   			}
   			
   			if (_oPR.getStatus() == i_PR_NEW) //save tmp batch if PR is new
   			{
	   			BatchTransactionTool.saveTmpBatch(oTD.getItemId(),
	   											  oTD.getPurchaseReceiptId(), 
	   											  oTD.getPurchaseReceiptDetailId(), 
	   											  i_INV_TRANS_PURCHASE_RECEIPT, 
	   											  oTD.getBatchTransaction(), _oConn);
   			}
   			
   			if(_oPR.getStatus() == i_PR_APPROVED || 
   	           _oPR.getStatus() == i_PR_REJECTED)
            {
                //only save Receipt which qty > 0
   				if(oTD.getQty().doubleValue() > 0)
			    {
                	oTD.save (_oConn);
			    }
   				else //if qty == 0 then remove from list iterator
   				{
   					PurchaseReceiptDetailPeer.doDelete(
   						new Criteria().add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_DETAIL_ID, 
   							oTD.getPurchaseReceiptDetailId()), _oConn
					);
   					//oIter.remove();
   				}
			}
   			else //if status is new then we saved all details including those which qty is 0
   			{
   				oTD.save(_oConn);
   			}
		}
	}
		
	/**
	 * Cancel Purchase Receipt
	 * 
	 * @param _sPRID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelPR (PurchaseReceipt _oPR, List _vPRD, String _sCancelBy, Connection _oConn)
    	throws Exception
	{
		boolean bStartTrans = false;
		try
		{
			if (_oConn == null) 
			{
				_oConn = beginTrans();
				bStartTrans = true;
			}	    
			//validate date
			validateDate(_oPR.getReceiptDate(), _oConn);

			//check if this PR not exist in any PI
			if (!PurchaseInvoiceTool.isPRInvoiced(_oPR.getPurchaseReceiptId(), _oConn))
			{
				//check if PR already returned
				if (!PurchaseReturnTool.isReturned(_oPR.getPurchaseReceiptId(), _oConn))
				{
					if(_oPR.getStatus() == i_PR_APPROVED)
				    {
						//1. delete invTrans
						InventoryCostingTool oCT = new InventoryCostingTool(_oConn);
						oCT.delete(_oPR, _oPR.getPurchaseReceiptId(), i_INV_TRANS_PURCHASE_RECEIPT); 
						
				        //2. update PO (received qty)
				        //3. recheck PO status	        	
				        PurchaseOrderTool.updatePOFromPR(_oPR.getPurchaseOrderId(), _vPRD, false, true, _oConn);
				        	
				        //4. cancel GL Journal
						if (b_USE_GL && PreferenceTool.getLocationFunction() != i_STORE)
						{
							PurchaseJournalTool.deletePurchaseJournal
								(GlAttributes.i_GL_TRANS_PURCHASE_RECEIPT, _oPR.getPurchaseReceiptId(), _oConn);                
						}
				    }
					_oPR.setCancelBy(_sCancelBy);
					_oPR.setCancelDate(new Date());
					_oPR.setRemark(cancelledBy(_oPR.getRemark(), _sCancelBy));
				    _oPR.setStatus(i_PR_CANCELLED);
				    _oPR.save(_oConn);
				}
				else
				{
					throw new NestableException (LocaleTool.getString("pr_cancel_returned"));				
				}
			}
			//the PR may not be cancelled because it already exist in PI
			else 
			{
				throw new NestableException (LocaleTool.getString("pr_cancel_invoiced"));
			}
			if (bStartTrans) 
			{
				commit (_oConn);
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans) 
			{
				rollback (_oConn);
			}
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);		
		}
	}
	
	/**
	 * 
	 * @param _iCond
	 * @param _sKeywords
	 * @param _dStart
	 * @param _dEnd
	 * @param _sVendorID
	 * @param _sLocationID
	 * @param _iStatus
	 * @param _iLimit
	 * @param _sCurrencyID
	 * @return query result in LargeSelect 
	 * @throws Exception
	 */
	public static LargeSelect findData (int _iCond, 
										String _sKeywords, 
										Date _dStart, 
										Date _dEnd, 
								 		String _sVendorID, 
										String _sLocationID, 
										int _iStatus, 
										int _iLimit, 
										String _sCurrencyID) 
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			PurchaseReceiptPeer.RECEIPT_DATE, _dStart, _dEnd, _sCurrencyID
		);
		if (_iCond == i_PO_NO && StringUtil.isNotEmpty(_sKeywords))
		{
			oCrit.addJoin(PurchaseReceiptPeer.PURCHASE_ORDER_ID, PurchaseOrderPeer.PURCHASE_ORDER_ID);
			oCrit.add(PurchaseOrderPeer.PURCHASE_ORDER_NO, (Object)_sKeywords ,Criteria.ILIKE);		
		}
		if (StringUtil.isNotEmpty(_sVendorID)) 
		{
			oCrit.add(PurchaseReceiptPeer.VENDOR_ID, _sVendorID);	
		}
		if (StringUtil.isNotEmpty(_sLocationID)) 
		{
			oCrit.add(PurchaseReceiptPeer.LOCATION_ID, _sLocationID);	
		}		
		if (_iStatus > 0) 
		{
			oCrit.add(PurchaseReceiptPeer.STATUS, _iStatus);	
		}		
		oCrit.addAscendingOrderByColumn(PurchaseReceiptPeer.RECEIPT_DATE);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.PurchaseReceiptPeer");
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	//report methods
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static List getTransactionInDateRange (Date _dStart, Date _dEnd, 
												  int _iGroupBy, String _sKeywords, int _iOrderBy, int _iStatus)
    	throws Exception
	{
		Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptPeer.RECEIPT_DATE, DateUtil.getStartOfDayDate( _dStart), Criteria.GREATER_EQUAL);
        oCrit.and(PurchaseReceiptPeer.RECEIPT_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		if(_iGroupBy == 2)
        {
            oCrit.add(PurchaseReceiptPeer.VENDOR_NAME, 
                (Object) SqlUtil.like (PurchaseReceiptPeer.VENDOR_NAME, _sKeywords,false,true), Criteria.CUSTOM);
        }
        if(_iGroupBy == 4)
        {
            oCrit.add(LocationPeer.LOCATION_NAME, 
                (Object) SqlUtil.like (LocationPeer.LOCATION_NAME, _sKeywords,false,true), Criteria.CUSTOM);
            oCrit.addJoin(PurchaseReceiptPeer.LOCATION_ID,LocationPeer.LOCATION_ID);
        }
        if(_iGroupBy == 6)
        {
            oCrit.add(PurchaseReceiptPeer.CREATE_BY, 
                (Object) SqlUtil.like (PurchaseReceiptPeer.CREATE_BY, _sKeywords,false,true), Criteria.CUSTOM);
        }

        if(_iOrderBy == 1)
        {
            oCrit.addAscendingOrderByColumn(PurchaseReceiptPeer.RECEIPT_NO);
        }
        if(_iOrderBy == 2)
        {
            oCrit.addAscendingOrderByColumn(PurchaseReceiptPeer.VENDOR_NAME);
        }
        if(_iOrderBy == 3)
        {
            oCrit.addAscendingOrderByColumn(PurchaseReceiptPeer.CREATE_BY);
        }
        if(_iOrderBy == 4)
        {
            oCrit.addAscendingOrderByColumn(PurchaseReceiptPeer.CONFIRM_BY);
        }
        if(_iStatus != -1)
        {
            oCrit.add(PurchaseReceiptPeer.STATUS,_iStatus);
        }
        log.debug(oCrit);
		return PurchaseReceiptPeer.doSelect(oCrit);
	}

	public static List getPRByStatusAndVendorID (String _sVendorID, 
												 int _iStatus, 
												 boolean _bPaidStatus, 
												 String _sCurrencyID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
        if (StringUtil.isNotEmpty(_sVendorID))
        {
            oCrit.add(PurchaseReceiptPeer.VENDOR_ID, _sVendorID);
        }
	    oCrit.add(PurchaseReceiptPeer.STATUS, _iStatus);
	   	oCrit.add(PurchaseReceiptPeer.IS_BILL, _bPaidStatus);
	   	if(StringUtil.isNotEmpty(_sCurrencyID))
	   	{
	   	    oCrit.add(PurchaseReceiptPeer.CURRENCY_ID, _sCurrencyID);
	   	    oCrit.or(PurchaseReceiptPeer.CURRENCY_ID, "");
	   	}
	    return PurchaseReceiptPeer.doSelect(oCrit);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//report method to group data by item
	/////////////////////////////////////////////////////////////////////////////////////
	
	public static PurchaseReceipt filterTransByPurchaseReceiptID (List _vPR, String _sPRID)
    	throws Exception
    {
		PurchaseReceipt oPR = null;
		for (int i = 0; i < _vPR.size(); i++)
		{
			oPR = (PurchaseReceipt) _vPR.get(i);
			if ( oPR.getPurchaseReceiptId().equals(_sPRID) )
			{
				return oPR;
			}
		}
		return oPR;
	}

    public static List getTransDetails (List _vPR)
    	throws Exception
    {
        return getTransDetails(_vPR,"");
    }
	
    public static List getTransDetails (List _vPR,String _sItemName)
    	throws Exception
    {
		List vPRID = new ArrayList (_vPR.size());
		for (int i = 0; i < _vPR.size(); i++)
		{
			PurchaseReceipt oPR = (PurchaseReceipt) _vPR.get(i);
			vPRID.add (oPR.getPurchaseReceiptId());
		}
		Criteria oCrit = new Criteria();
		if(StringUtil.isNotEmpty(_sItemName))
		{
		    oCrit.add(PurchaseReceiptDetailPeer.ITEM_NAME,
		        (Object) SqlUtil.like (PurchaseReceiptDetailPeer.ITEM_NAME, _sItemName,false,true), Criteria.CUSTOM);
		}
        if(vPRID.size()>0)
        {
		    oCrit.addIn (PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, vPRID);
		}
		return PurchaseReceiptDetailPeer.doSelect(oCrit);
	}	
	
    public static double getReceiptQty(String _sPODetailID)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        oCrit.add(PurchaseReceiptDetailPeer.PURCHASE_ORDER_DETAIL_ID, _sPODetailID);
        oCrit.addJoin(PurchaseReceiptPeer.PURCHASE_RECEIPT_ID, PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID);
        Criteria.Criterion oApr = oCrit.getNewCriterion(PurchaseReceiptPeer.STATUS, i_PR_APPROVED, Criteria.EQUAL);
        Criteria.Criterion oInv = oCrit.getNewCriterion(PurchaseReceiptPeer.STATUS, i_PR_INVOICED, Criteria.EQUAL);
        oCrit.add(oApr.or(oInv));
        
        double dQty = 0;
        List vPR = PurchaseReceiptDetailPeer.doSelect(oCrit);
        for (int i = 0; i < vPR.size(); i++)
        {
            PurchaseReceiptDetail oPRD = (PurchaseReceiptDetail) vPR.get(i);
            dQty += oPRD.getQtyBase().doubleValue();
        }
        return dQty;
    }
    
	/////////////////////////////////////////////////////////////////////////////////////
	//end report methods
	/////////////////////////////////////////////////////////////////////////////////////

	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
		
	/**
	 * get List of PurchaseReceipt created since last store 2 ho
	 * 
	 * @return List of PurchaseReceipt created since last store 2 ho
	 * @throws Exception
	 */
	public static List getStorePurchaseReceipt ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();
		
		if (dLastSyncDate  != null) 
		{
        	oCrit.add(PurchaseReceiptPeer.CONFIRM_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
    	oCrit.add(PurchaseReceiptPeer.STATUS, i_PR_APPROVED);
		return PurchaseReceiptPeer.doSelect(oCrit);
	}
    
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PurchaseReceiptPeer.class, PurchaseReceiptPeer.PURCHASE_RECEIPT_ID, _sID, _bIsNext);
	}			
	//end next prev
}

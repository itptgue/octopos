package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class TurbinePermissionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.TurbinePermissionMapBuilder";

    /**
     * Item
     * @deprecated use TurbinePermissionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "TURBINE_PERMISSION";
    }

  
    /**
     * TURBINE_PERMISSION.PERMISSION_ID
     * @return the column name for the PERMISSION_ID field
     * @deprecated use TurbinePermissionPeer.TURBINE_PERMISSION.PERMISSION_ID constant
     */
    public static String getTurbinePermission_PermissionId()
    {
        return "TURBINE_PERMISSION.PERMISSION_ID";
    }
  
    /**
     * TURBINE_PERMISSION.PERMISSION_NAME
     * @return the column name for the PERMISSION_NAME field
     * @deprecated use TurbinePermissionPeer.TURBINE_PERMISSION.PERMISSION_NAME constant
     */
    public static String getTurbinePermission_PermissionName()
    {
        return "TURBINE_PERMISSION.PERMISSION_NAME";
    }
  
    /**
     * TURBINE_PERMISSION.OBJECTDATA
     * @return the column name for the OBJECTDATA field
     * @deprecated use TurbinePermissionPeer.TURBINE_PERMISSION.OBJECTDATA constant
     */
    public static String getTurbinePermission_Objectdata()
    {
        return "TURBINE_PERMISSION.OBJECTDATA";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("TURBINE_PERMISSION");
        TableMap tMap = dbMap.getTable("TURBINE_PERMISSION");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("TURBINE_PERMISSION_SEQ");

                      tMap.addPrimaryKey("TURBINE_PERMISSION.PERMISSION_ID", Integer.valueOf(0));
                          tMap.addColumn("TURBINE_PERMISSION.PERMISSION_NAME", "");
                          tMap.addColumn("TURBINE_PERMISSION.OBJECTDATA", new Object());
          }
}

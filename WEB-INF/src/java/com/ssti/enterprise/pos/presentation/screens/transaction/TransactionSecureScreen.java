package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TransactionAttributes;
import com.ssti.framework.presentation.SecureScreen;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/TransactionSecureScreen.java,v $
 * Purpose: Transaction Secure screen, super class for all transaction screen
 * implements TransactionAttributes to hold various transaction related constants
 *
 * @author  $Author: albert $
 * @version $Id: TransactionSecureScreen.java,v 1.9 2009/05/04 02:02:36 albert Exp $
 *
 * $Log: TransactionSecureScreen.java,v $
 * Revision 1.9  2009/05/04 02:02:36  albert
 * *** empty log message ***
 *
 */
public class TransactionSecureScreen extends SecureScreen implements TransactionAttributes, AppAttributes
{ 	
	protected static final int i_OP_IMPORT_FROM_PDT  = 20;
	
	protected void checkError(RunData data, Context context)
	{
		if (isRefresh(data, context))
		{
			//TODO: currenty useless
			ValueParser oPrevParams = (ValueParser) data.getUser().getTemp("prev_parameters");
			if (oPrevParams != null)
			{
				if (context != null) 
				{
					context.put (s_POST_ERROR, oPrevParams.getBooleanObject(s_POST_ERROR));
				}
			}
		}
		else
		{
			context.put (s_POST_ERROR, data.getParameters().getBooleanObject(s_POST_ERROR));
		}
	}	
	
	public void setContextVar(Context context)
	{
    	context.put(s_ALLOW_COPY_TRANS, Boolean.valueOf(b_ALLOW_COPY_TRANS));    	
	}
	
	public void doBuildTemplate(RunData data, Context context)
	{
		this.setContextVar(context);
		PreferenceTool.setLocationInContext(data, context);
		checkError(data, context);		
	}
	
	/**
     * change to display stacktrace on nullPointer
     * 
     * @param data
     * @param _sDesc
     * @param t
     */
	protected void handleError(RunData data, 
							   String _sDesc, 
							   Throwable t)
	{
    	String sError = _sDesc + t.getMessage(); 
    	String sStack = BaseTool.displayStackTrace(t,null);
    	if (StringUtil.isNotEmpty(sStack))
    	{
    		StringBuilder sb = new StringBuilder();    		
    		sError += BaseTool.displayStackTpl(data,  sStack, null);      		
    	}
    	data.getParameters().add(s_POST_ERROR, Boolean.valueOf(true).toString());        	
    	log.error (sError, t); 
    	data.setMessage (sError);
	}
}

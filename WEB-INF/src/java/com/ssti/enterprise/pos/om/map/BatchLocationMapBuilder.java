package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BatchLocationMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BatchLocationMapBuilder";

    /**
     * Item
     * @deprecated use BatchLocationPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "batch_location";
    }

  
    /**
     * batch_location.BATCH_LOCATION_ID
     * @return the column name for the BATCH_LOCATION_ID field
     * @deprecated use BatchLocationPeer.batch_location.BATCH_LOCATION_ID constant
     */
    public static String getBatchLocation_BatchLocationId()
    {
        return "batch_location.BATCH_LOCATION_ID";
    }
  
    /**
     * batch_location.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use BatchLocationPeer.batch_location.ITEM_ID constant
     */
    public static String getBatchLocation_ItemId()
    {
        return "batch_location.ITEM_ID";
    }
  
    /**
     * batch_location.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use BatchLocationPeer.batch_location.ITEM_CODE constant
     */
    public static String getBatchLocation_ItemCode()
    {
        return "batch_location.ITEM_CODE";
    }
  
    /**
     * batch_location.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use BatchLocationPeer.batch_location.LOCATION_ID constant
     */
    public static String getBatchLocation_LocationId()
    {
        return "batch_location.LOCATION_ID";
    }
  
    /**
     * batch_location.BATCH_NO
     * @return the column name for the BATCH_NO field
     * @deprecated use BatchLocationPeer.batch_location.BATCH_NO constant
     */
    public static String getBatchLocation_BatchNo()
    {
        return "batch_location.BATCH_NO";
    }
  
    /**
     * batch_location.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use BatchLocationPeer.batch_location.DESCRIPTION constant
     */
    public static String getBatchLocation_Description()
    {
        return "batch_location.DESCRIPTION";
    }
  
    /**
     * batch_location.QTY
     * @return the column name for the QTY field
     * @deprecated use BatchLocationPeer.batch_location.QTY constant
     */
    public static String getBatchLocation_Qty()
    {
        return "batch_location.QTY";
    }
  
    /**
     * batch_location.EXPIRED_DATE
     * @return the column name for the EXPIRED_DATE field
     * @deprecated use BatchLocationPeer.batch_location.EXPIRED_DATE constant
     */
    public static String getBatchLocation_ExpiredDate()
    {
        return "batch_location.EXPIRED_DATE";
    }
  
    /**
     * batch_location.LAST_UPDATE
     * @return the column name for the LAST_UPDATE field
     * @deprecated use BatchLocationPeer.batch_location.LAST_UPDATE constant
     */
    public static String getBatchLocation_LastUpdate()
    {
        return "batch_location.LAST_UPDATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("batch_location");
        TableMap tMap = dbMap.getTable("batch_location");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("batch_location.BATCH_LOCATION_ID", "");
                          tMap.addColumn("batch_location.ITEM_ID", "");
                          tMap.addColumn("batch_location.ITEM_CODE", "");
                          tMap.addColumn("batch_location.LOCATION_ID", "");
                          tMap.addColumn("batch_location.BATCH_NO", "");
                          tMap.addColumn("batch_location.DESCRIPTION", "");
                            tMap.addColumn("batch_location.QTY", bd_ZERO);
                          tMap.addColumn("batch_location.EXPIRED_DATE", new Date());
                          tMap.addColumn("batch_location.LAST_UPDATE", new Date());
          }
}

package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.SiteManager;
import com.ssti.enterprise.pos.om.Site;
import com.ssti.enterprise.pos.om.SitePeer;
import com.ssti.enterprise.pos.presentation.actions.company.CompanySecureAction;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.SiteTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * 
 * </pre><br>
 */
public class SiteSetupAction extends CompanySecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Site oSite = new Site();
			setProperties (oSite, data);
			oSite.setSiteId (IDGenerator.generateSysID());
	        oSite.save();
	        SiteManager.getInstance().refreshCache(oSite);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch(Exception _oEx)
        {
	       	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Site oSite = SiteTool.getSiteByID(data.getParameters().getString("ID"));
			Site oOld = oSite.copy();			
			setProperties (oSite, data);
	        oSite.save();
	        SiteManager.getInstance().refreshCache(oSite);
	        
	        UpdateHistoryTool.createHistory(oOld, oSite, data.getUser().getName(), null);	        
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("location","site_id",sID)
    			&& SqlUtil.validateFKRef("preference","site_id",sID))
    		{
    			Site oSite = SiteTool.getSiteByID(sID);    			
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(SitePeer.SITE_ID, sID);
	        	SitePeer.doDelete(oCrit);
	        	SiteManager.getInstance().refreshCache(sID);
	        	
	        	UpdateHistoryTool.createDelHistory(oSite, data.getUser().getName(), null);
	        	data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
    		}   
        	else
        	{
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));        		
        	}
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

    private void setProperties (Site _oSite, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oSite);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
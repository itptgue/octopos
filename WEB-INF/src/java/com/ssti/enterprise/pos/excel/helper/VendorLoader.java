package com.ssti.enterprise.pos.excel.helper;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.VendorManager;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.VendorTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.AddressTool;
import com.ssti.framework.tools.StringUtil;

public class VendorLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( VendorLoader.class );
	
	private static final String s_START_HEADING = "Vendor Code";	
	
	public VendorLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	m_bValidate = true;
    	m_aHeaders = VendorViewHelper.a_HEADER;
    	sHeading = s_START_HEADING;
	}

    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {		
    	StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sVendorCode = getString(_oRow, _iIdx + i); i++;
			String sVendorName = getString(_oRow, _iIdx + i); i++;
			String sTypeCode   = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sVendorCode))
			{	
				Vendor oVendor = VendorTool.getVendorByCode(sVendorCode);
				Vendor oOld = null;
				
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Vendor Code : " + sVendorCode);
				
				if (oVendor == null) 
				{	
					log.debug ("Code Not Exist, create new Code");

					oVendor = new Vendor();
					oVendor.setVendorId (IDGenerator.generateSysID());
					oVendor.setVendorCode(sVendorCode); 	
					oVendor.setCreateBy(m_sUserName);					
					bNew = true;
				}
				else 
				{
					bNew = false;
					oOld = oVendor.copy();
				}
				oVendor.setVendorName(sVendorName); 
				bValid = validateString("Vendor Name", oVendor.getVendorName(), 100, oEmpty, oLength);
				
				oVendor.setVendorTypeId(VendorTypeTool.getIDByCode(sTypeCode));
				
				String sItemType = getString(_oRow, _iIdx + i); i++;
				if(StringUtil.isNotEmpty(sItemType) && 
				   !StringUtil.equalsIgnoreCase(sItemType, "all") && 
				   !StringUtil.equalsIgnoreCase(sItemType, LocaleTool.getString("all")) )
				{
					oVendor.setItemType(ItemTool.getItemType(sItemType));
				}
				else
				{
					oVendor.setItemType(0); //all type
				}
				
				oVendor.setTaxNo (getString(_oRow, _iIdx + i)); i++;
				
				String sVendorStatus = getString(_oRow, _iIdx + i);i++;
				if (sVendorStatus.equals("Non Active")) 
				{
				    oVendor.setVendorStatus(2); 
				}
				else
				{
				    oVendor.setVendorStatus(1); //active
				}
				    
				oVendor.setContactName1     (getString(_oRow, _iIdx + i)); i++;
				oVendor.setContactPhone1    (getString(_oRow, _iIdx + i)); i++;
				oVendor.setJobTitle1        (getString(_oRow, _iIdx + i)); i++;
				oVendor.setContactName2     (getString(_oRow, _iIdx + i)); i++;
				oVendor.setContactPhone2    (getString(_oRow, _iIdx + i)); i++;
				oVendor.setJobTitle2        (getString(_oRow, _iIdx + i)); i++;
				oVendor.setContactName3     (getString(_oRow, _iIdx + i)); i++;
				oVendor.setContactPhone3    (getString(_oRow, _iIdx + i)); i++;
				oVendor.setJobTitle3        (getString(_oRow, _iIdx + i)); i++;
				oVendor.setAddress          (getString(_oRow, _iIdx + i)); i++;
				oVendor.setZip              (getString(_oRow, _iIdx + i)); i++;
				oVendor.setCountry          (getString(_oRow, _iIdx + i)); i++;

				AddressTool.setProvinceStr(oVendor, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setRegencyStr (oVendor, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setDistrictStr(oVendor, getString(_oRow, _iIdx + i)); i++;
				AddressTool.setVillageStr (oVendor, getString(_oRow, _iIdx + i)); i++;
				
				oVendor.setPhone1           (getString(_oRow, _iIdx + i)); i++;
                oVendor.setPhone2           (getString(_oRow, _iIdx + i)); i++;
                oVendor.setFax              (getString(_oRow, _iIdx + i)); i++;
                oVendor.setEmail            (getString(_oRow, _iIdx + i)); i++;
                oVendor.setWebSite          (getString(_oRow, _iIdx + i)); i++;
                
                //tax
                String sTaxCode = getString(_oRow, _iIdx + i); i++;
                oVendor.setDefaultTaxId (TaxTool.getIDByCode(sTaxCode));
                
                //payment type
                String sPaymentType = getString(_oRow, _iIdx + i); i++;
                PaymentType oType = PaymentTypeTool.getPaymentTypeByCode(sPaymentType);
                String sTypeID = "";
                if (oType != null) sTypeID = oType.getPaymentTypeId();
                oVendor.setDefaultTypeId(sTypeID);

                String sPaymentTerm = getString(_oRow, _iIdx + i); i++;
                PaymentTerm oTerm = PaymentTermTool.getPaymentTermByCode(sPaymentTerm);
                String sTermID = "";
                if (oTerm != null) sTermID = oTerm.getPaymentTermId();
                oVendor.setDefaultTermId(sTermID);

                String sEmployeeName = getString(_oRow, _iIdx + i); i++;
                Employee oEmp = EmployeeTool.getEmployeeByName (sEmployeeName);				
                String sEmpID = "";
                if (oEmp != null) sEmpID = oEmp.getEmployeeId();
                oVendor.setDefaultEmployeeId(sEmpID);
                
                String sCode = getString(_oRow, _iIdx + i); i++;
                Currency oCurr = CurrencyTool.getCurrencyByCode(sCode);
				String sCurrID = "";
                if (oCurr != null) sCurrID = oCurr.getCurrencyId();
                oVendor.setDefaultCurrencyId(sCurrID);
                
                oVendor.setDefaultDiscountAmount ( getString(_oRow, _iIdx + i) ); i++;
                oVendor.setDefaultItemDisc ( getString(_oRow, _iIdx + i) ); i++;
                oVendor.setCreditLimit ( getBigDecimal(_oRow, _iIdx + i) ); i++;
                oVendor.setMemo   ( getString(_oRow, _iIdx + i) ); i++;
                oVendor.setField1 ( getString(_oRow, _iIdx + i) ); i++;
                oVendor.setValue1 ( getString(_oRow, _iIdx + i) ); i++;
                oVendor.setField2 ( getString(_oRow, _iIdx + i) ); i++;
                oVendor.setValue2 ( getString(_oRow, _iIdx + i) ); i++;

                String sAP = getString(_oRow, _iIdx + i); i++;
                Account oAcc = AccountTool.getAccountByCode(sAP, false);
				String sAccID = "";
                if (oAcc != null) sAccID = oAcc.getAccountId();
                oVendor.setApAccount(sAccID);
                
                if (PreferenceTool.useCustomerTrPrefix())
                {
                    oVendor.setTransPrefix(getString(_oRow, _iIdx + i)); i++;
                }

                oVendor.setUpdateDate(new Date());
    			oVendor.setLastUpdateLocationId(PreferenceTool.getLocationID());
				oVendor.setLastUpdateBy(m_sUserName);
                oVendor.save();
                
                if(oOld != null)
                {
                	UpdateHistoryTool.createHistory(oOld, oVendor, m_sUserName, null);
                }
                if(bValid)
                {
                	if (bNew) 
                	{	
                		m_iNewData++;
                		m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
                		m_oNewData.append (StringUtil.left(sVendorCode,30));
                		m_oNewData.append (StringUtil.left(sVendorName,100));
                		m_oNewData.append ("\n");
                	}
                	else 
                	{
                		m_iUpdated++;
                		m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
                		m_oUpdated.append (StringUtil.left(sVendorCode,30));
                		m_oUpdated.append (StringUtil.left(sVendorName,100));
                		m_oUpdated.append ("\n");
                	}
                	VendorManager.getInstance().refreshCache(oVendor);
                }
                else
                {
                	m_iRejected++;
                	m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
                	m_oRejected.append (StringUtil.left(sVendorCode,30));
                	m_oRejected.append (StringUtil.left(sVendorName,50));
                	if (oEmpty.length() > s_EMPTY_REJECT.length())
                	{				    
                		m_oRejected.append (oEmpty);
                	}
                	if (oLength.length() > s_LENGTH_REJECT.length())
                	{
                		m_oRejected.append ("; ");				    
                		m_oRejected.append (oLength);
                	}
                	m_oRejected.append ("\n");
                }
			}
		}    
    }
}

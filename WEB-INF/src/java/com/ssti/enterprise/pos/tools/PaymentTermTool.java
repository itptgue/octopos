package com.ssti.enterprise.pos.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.PaymentTermManager;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.PaymentEdc;
import com.ssti.enterprise.pos.om.PaymentEdcConfig;
import com.ssti.enterprise.pos.om.PaymentEdcConfigPeer;
import com.ssti.enterprise.pos.om.PaymentEdcPeer;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentTermPeer;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PaymentTermTool.java,v 1.15 2008/03/27 02:22:24 albert Exp $ <br>
 *
 * <pre>
 * $Log: PaymentTermTool.java,v $
 * Revision 1.15  2008/03/27 02:22:24  albert
 * *** empty log message ***
 *
 * Revision 1.14  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class PaymentTermTool extends BaseTool 
{
	public static List getAll()
		throws Exception
	{
		return PaymentTermManager.getInstance().getAll();
	}

	public static List getAllPaymentTerm()
		throws Exception
	{
		return PaymentTermManager.getInstance().getAllPaymentTerm();
	}

	public static List getAllIncludeEDC()
		throws Exception
	{
		return PaymentTermPeer.doSelect(new Criteria());
	}

	public static PaymentTerm getPaymentTermByID(String _sID)
    	throws Exception
    {
		return getPaymentTermByID(_sID, null);
    }
    
	public static PaymentTerm getPaymentTermByID(String _sID, Connection _oConn)
    	throws Exception
    {
		return PaymentTermManager.getInstance().getPaymentTermByID(_sID, _oConn);
	}


	/**
	 * @param _sCode term code
	 * @return pament term  
	 */
	public static PaymentTerm getPaymentTermByCode(String _sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (PaymentTermPeer.PAYMENT_TERM_CODE, _sCode);
		List vData = PaymentTermPeer.doSelect(oCrit);
		if (vData.size()>0)
		{
			return (PaymentTerm)vData.get(0);
		}
		return null;
	}
	
	public static String getCodeByID(String _sID)
		throws Exception
	{
		return getPaymentTermCodeByID(_sID);
	}
	
	public static String getPaymentTermCodeByID(String _sID)
    	throws Exception
    {
		PaymentTerm oPayment = getPaymentTermByID(_sID);
		if ( oPayment != null) {
			return oPayment.getPaymentTermCode();
		}
		return "";
	}

	public static String getDescriptionByID(String _sID)
		throws Exception
	{
		PaymentTerm oPayment = getPaymentTermByID(_sID);
		if ( oPayment != null) {
			return oPayment.getDescription();
		}
		return "";
	}
	
	public static int getNetPaymentDaysByID(String _sID)
    	throws Exception
    {
		return getNetPaymentDaysByID(_sID, null);
	}
	
	public static int getNetPaymentDaysByID(String _sID, Connection _oConn)
    	throws Exception
    {
		PaymentTerm oPayment = getPaymentTermByID(_sID, _oConn);
		if ( oPayment != null) 
		{
			return oPayment.getNetDay();
		}
		return 0;
	}

	public static PaymentTerm getDefaultPaymentTerm()
    	throws Exception
    {
		List vData = getAllPaymentTerm();
        if (vData.size() > 0) {
			return (PaymentTerm) vData.get(0);
		}
		return null;
	}
	
	public static PaymentTerm getMultiType()
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(PaymentTermPeer.IS_MULTIPLE_PAYMENT, true);
		List vData = PaymentTermPeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (PaymentTerm) vData.get(0);
		}
		return null;
	}
	
	public static boolean isCashPaymentTerm (String _sID)
    	throws Exception
    {
		return isCashPaymentTerm (_sID, null);
	}

	public static boolean isCashPaymentTerm (String _sID, Connection _oConn)
    	throws Exception
    {
		PaymentTerm oPayment = getPaymentTermByID(_sID, _oConn);
		if (oPayment != null) return oPayment.getCashPayment();
		return false;
	}	
	
	public static String getIDByCode(String _sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (PaymentTermPeer.PAYMENT_TERM_CODE, _sCode);
		List vData = PaymentTermPeer.doSelect(oCrit);
		if (vData.size()>0)
		{
			return ((PaymentTerm)vData.get(0)).getPaymentTermId();
		}
		return "";
	}
	
	//-------------------------------------------------------------------------
	// EDC Related Method
	//-------------------------------------------------------------------------	
	public static List findEDCConfig(String _sPTID)
		throws Exception
	{
		return PaymentTermManager.getInstance().findEDCConfig(_sPTID);	
	}

	public static PaymentEdcConfig getEDCConfig(String _sPTID, String _sLocID)
		throws Exception
	{
		return PaymentTermManager.getInstance().getEDCConfig(_sPTID, _sLocID);	
	}

	public static PaymentEdcConfig getEDCConfigByID(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PaymentEdcConfigPeer.CONFIG_ID, _sID);
		List v = PaymentEdcConfigPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			return (PaymentEdcConfig) v.get(0);
		}
		return null;
	}

	public static PaymentEdcConfig getEDCConfigByMerchantID(String _sPTCode, String _sMerchantID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PaymentEdcConfigPeer.PAYMENT_TERM_ID, getIDByCode(_sPTCode));
		oCrit.add(PaymentEdcConfigPeer.MERCHANT_ID, _sMerchantID);		
		List v = PaymentEdcConfigPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			return (PaymentEdcConfig) v.get(0);
		}
		return null;
	}

	public static List getEDC()
		throws Exception
	{
		return PaymentTermManager.getInstance().getEDC();	
	}
	
	public static PaymentEdc getPaymentEDCByID(String _sID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PaymentEdcPeer.PAYMENT_EDC_ID, _sID);
		List v = PaymentEdcPeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			return (PaymentEdc)v.get(0);
		}
		return null;
	}
	
	public static List findPaymentEDC(String _sEDCID, Date _dStart, Date _dEnd, int _iStatus)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if(StringUtil.isNotEmpty(_sEDCID))
		{
			oCrit.add(PaymentEdcPeer.PAYMENT_TERM_ID, _sEDCID);
		}
		if(_iStatus == 1)
		{
			if(_dStart != null)
			{
				oCrit.add(PaymentEdcPeer.START_DATE, _dStart, Criteria.GREATER_EQUAL);
			}
			if(_dEnd != null)
			{
				oCrit.add(PaymentEdcPeer.END_DATE, _dEnd, Criteria.LESS_EQUAL);
			}
		}
		else if(_iStatus == 2)
		{
			Date dNow = DateUtil.getTodayDate();
			oCrit.add(PaymentEdcPeer.START_DATE, dNow, Criteria.LESS_EQUAL);
			oCrit.add(PaymentEdcPeer.END_DATE, dNow, Criteria.GREATER_EQUAL);						
		}
		
		return PaymentEdcPeer.doSelect(oCrit);
	}

	public static PaymentEdc getPaymentEDC(String _sEDCID, String _sIssuerID, Date _dTx, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PaymentEdcPeer.PAYMENT_TERM_ID, _sEDCID);
		oCrit.add(PaymentEdcPeer.ISSUER_ID, _sIssuerID);
		if(_dTx != null)
		{
			oCrit.add(PaymentEdcPeer.START_DATE, _dTx, Criteria.LESS_EQUAL);
			oCrit.and(PaymentEdcPeer.END_DATE, _dTx, Criteria.GREATER_EQUAL);
		}
		else
		{
			Date dNow = DateUtil.getTodayDate();
			oCrit.add(PaymentEdcPeer.START_DATE, dNow, Criteria.LESS_EQUAL);
			oCrit.add(PaymentEdcPeer.END_DATE, dNow, Criteria.GREATER_EQUAL);			
		}
		List vData = PaymentEdcPeer.doSelect(oCrit, _oConn);
		if(vData.size() > 0)
		{
			return (PaymentEdc) vData.get(0);
		}
		return null;		 
	}
	
	public static void setMDR(InvoicePayment _oPmt, Connection _oConn)
		throws Exception
	{
		if(_oPmt != null)
		{
			PaymentEdc oPE = PaymentTermTool.getPaymentEDC(_oPmt.getPaymentTermId(),_oPmt.getBankIssuer(), null, _oConn);
	    	if (oPE != null && oPE.getRate() != null)
	    	{
	    		double dMDR = Calculator.mul(_oPmt.getPaymentAmount(), Calculator.div(oPE.getRate(),100));
	    		_oPmt.setMdrAmount(new BigDecimal(dMDR));
	    	}
		}
	}
}

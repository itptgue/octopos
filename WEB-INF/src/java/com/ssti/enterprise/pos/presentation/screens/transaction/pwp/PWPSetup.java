package com.ssti.enterprise.pos.presentation.screens.transaction.pwp;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Pwp;
import com.ssti.enterprise.pos.om.PwpBuy;
import com.ssti.enterprise.pos.om.PwpGet;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.pwp.PWPTool;
import com.ssti.framework.tools.StringUtil;

public class PWPSetup extends TransactionSecureScreen
{
	protected static final Log log = LogFactory.getLog(Pwp.class);

    private static final String[] a_VIEW = {"View Discount Setup"};
    private static final String[] a_UPDT = {"Update Discount Setup"};

    private static final int i_OP_ADD_BUY	 = 1;
    private static final int i_OP_DEL_BUY	 = 2;
	private static final int i_OP_NEW_TRANS	 = 3;
	private static final int i_OP_VIEW_TRANS = 4; //after saved or view details
	private static final int i_OP_ADD_GET	 = 5;
    private static final int i_OP_DEL_GET	 = 6;
    private static final int i_OP_SAVE	 	 = 7;    
    private static final int i_OP_DEL	 	 = 8;    
    
    private static final String s_PWP = "pwp";
    private static final String s_PWP_BUY = "pwpBuy";
    private static final String s_PWP_GET = "pwpGet";
    
    private Pwp oPWP = null;
    private List vPWPBuy = null;
    private List vPWPGet = null;    
    private HttpSession oPWPSes;

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        int iOp = data.getParameters().getInt("op");
        if (iOp != 4)
        {
            return isAuthorized (data, a_UPDT);            
        }
    	return isAuthorized (data, a_VIEW);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{			
			if ( iOp == i_OP_ADD_BUY && !b_REFRESH)
			{
				addBuy (data);
			}  
			else if ( iOp == i_OP_DEL_BUY && !b_REFRESH)
			{
				delBuy (data);
			}
			else if ( iOp == i_OP_NEW_TRANS )
			{
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS )
			{
				getData (data);
			}
			else if (iOp == i_OP_ADD_GET) {
				addGet (data);
			}			
			else if (iOp == i_OP_DEL_GET) {
				delGet (data);
			}		
			else if (iOp == i_OP_SAVE) {
				saveData (data);
			}				
			else if (iOp == i_OP_DEL) {
				delData (data);
			}							
		}
		catch (Exception _oEx) 
		{
		    _oEx.printStackTrace();
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_PWP, oPWPSes.getAttribute(s_PWP));
		context.put(s_PWP_BUY, oPWPSes.getAttribute(s_PWP_BUY));
		context.put(s_PWP_GET, oPWPSes.getAttribute(s_PWP_GET));			
    	context.put("pwptool", PWPTool.getInstance());
    }

	private void initSession ( RunData data )
	{
		synchronized (this)
		{
			oPWPSes = data.getSession();
			if (oPWPSes.getAttribute (s_PWP) == null)
			{
				createNewTrans(data);
			}
			oPWP = (Pwp) oPWPSes.getAttribute (s_PWP);
			vPWPBuy = (List) oPWPSes.getAttribute (s_PWP_BUY);
			vPWPGet = (List) oPWPSes.getAttribute (s_PWP_GET);		
		}
	}

    private void addBuy (RunData data)
    	throws Exception
    {
    	data.getParameters().setProperties (oPWP);	
    	
		PwpBuy oPWPB = new PwpBuy();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("BuyItemId"));
		if (oItem != null)
		{
			//set PWP Detail Prop
			data.getParameters().setProperties (oPWPB);
		}
		else
		{
			return;
		}				

		if (!isBuyExist (oPWPB,data))
		{
   			vPWPBuy.add (oPWPB);
		}
		synchronized (this) 
		{
			oPWPSes.setAttribute (s_PWP_BUY, vPWPBuy);
			oPWPSes.setAttribute (s_PWP, oPWP);
		}
    }

    private void delBuy (RunData data)
    	throws Exception
    {
		if (vPWPBuy.size() > (data.getParameters().getInt("No") - 1))
		{
			vPWPBuy.remove ( data.getParameters().getInt("No") - 1 );
			synchronized (this) 
			{
				oPWPSes.setAttribute (s_PWP_BUY, vPWPBuy);
				oPWPSes.setAttribute (s_PWP, oPWP);
			}
		}
    }
    
    private boolean isBuyExist (PwpBuy _oPWPB, RunData data)
	    throws Exception
	{
	    boolean bIsExist = false; 
	    
		for (int i = 0; i < vPWPBuy.size(); i++)
		{
			PwpBuy oExist = (PwpBuy) vPWPBuy.get (i);
			if (oExist.getBuyItemId().equals(_oPWPB.getBuyItemId()) && 
				oExist.getBuyQty().doubleValue() == _oPWPB.getBuyQty().doubleValue() &&
				oExist.getBuyPrice().doubleValue() == _oPWPB.getBuyPrice().doubleValue())
			{
				bIsExist = true;
			}
		}
		return bIsExist;
	}
    
    private void addGet (RunData data)
    	throws Exception
    {
    	data.getParameters().setProperties (oPWP);	
    	
    	PwpGet oPWPG = new PwpGet();
    	Item oItem = ItemTool.getItemByID(data.getParameters().getString("GetItemId"));
    	if (oItem != null)
    	{
    		//set PWP Detail Prop
    		data.getParameters().setProperties (oPWPG);
    	}
    	else
    	{
    		return;
    	}				
    	
    	if (!isGetExist (oPWPG,data))
    	{
    		vPWPGet.add (oPWPG);
    	}

    	synchronized (this) 
    	{
    		oPWPSes.setAttribute (s_PWP_GET, vPWPGet);
    		oPWPSes.setAttribute (s_PWP, oPWP);
    	}
    }
    
    private void delGet (RunData data)
    	throws Exception
    {
    	//save current updated qty first
    	//PWPTool.updateDetail (vPWPGet,data);
    	
    	if (vPWPGet.size() > (data.getParameters().getInt("No") - 1))
    	{
    		vPWPGet.remove ( data.getParameters().getInt("No") - 1 );
    		synchronized (this) 
    		{
    			oPWPSes.setAttribute (s_PWP_GET, vPWPGet);
    			oPWPSes.setAttribute (s_PWP, oPWP);
    		}
    	}
    }
    
    private boolean isGetExist (PwpGet _oPWPB, RunData data)
    	throws Exception
    {
    	boolean bIsExist = false; 
    	
    	for (int i = 0; i < vPWPGet.size(); i++)
    	{
    		PwpGet oExist = (PwpGet) vPWPGet.get (i);
    		if (oExist.getGetItemId().equals(_oPWPB.getGetItemId()) && 
    			oExist.getGetQty().doubleValue() == _oPWPB.getGetQty().doubleValue() &&
    			oExist.getGetPrice().doubleValue() == _oPWPB.getGetPrice().doubleValue())
    		{
    			bIsExist = true;
    		}
    	}
    	return bIsExist;
    }
    
    private void createNewTrans (RunData data)
    {
    	synchronized (this)
    	{
    		oPWP = new Pwp();
    		vPWPBuy = new ArrayList();
    		vPWPGet = new ArrayList();
    		
    		oPWPSes.setAttribute (s_PWP, oPWP);
    		oPWPSes.setAttribute (s_PWP_BUY, vPWPBuy);
    		oPWPSes.setAttribute (s_PWP_GET, vPWPGet);
    	}
    }

    private void getData (RunData data)
    	throws Exception
    {
        String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID))
    	{
    		try
    		{
    			synchronized (this) 
    			{
	    			oPWP = PWPTool.getByID (sID);
	        		vPWPBuy = PWPTool.getBuyByID (sID, null);
	        		vPWPGet = PWPTool.getGetByID (sID, null);
	        		
	    			oPWPSes.setAttribute (s_PWP, oPWP);
					oPWPSes.setAttribute (s_PWP_BUY, vPWPBuy);
					oPWPSes.setAttribute (s_PWP_GET, vPWPGet);
    			}
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    	else
    	{
    		createNewTrans(data);
    	}
    }
        
    private void saveData (RunData data)
		throws Exception
	{
    	data.getParameters().setProperties (oPWP);
    	
    	System.out.println("pwp "  + oPWP);
    	System.out.println("buy "  + vPWPBuy);
    	System.out.println("get "  + vPWPGet);
    	
    	PWPTool.saveData(oPWP, vPWPBuy, vPWPGet);
    	data.setMessage("PWP Saved Successfully");
	}
    
    private void delData (RunData data)
		throws Exception
	{
    	if (oPWP != null)
		{
    		data.getParameters().setProperties (oPWP);		    		
    		PWPTool.deleteData(oPWP);
    		data.setMessage("PWP Deleted Successfully");
    		createNewTrans(data);
		}
	}
}


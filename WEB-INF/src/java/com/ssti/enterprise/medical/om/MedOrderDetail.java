
package com.ssti.enterprise.medical.om;


import java.math.BigDecimal;
import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.enterprise.medical.model.MedDetailOM;
import com.ssti.enterprise.medical.tools.MedicalSalesTool;
import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.om.BatchTransaction;
import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class MedOrderDetail
    extends com.ssti.enterprise.medical.om.BaseMedOrderDetail
    implements InventoryDetailOM, Persistent, MedDetailOM
{

	@Override
	public String getTransDetId() {
		return getMedOrderDetailId();
	}

	@Override
	public String getTransId() {
		return getMedOrderId();
	}

	BatchTransaction batchTrans = null;
	public void setBatchTransaction(BatchTransaction batchTrans) {this.batchTrans = batchTrans;}
	public BatchTransaction getBatchTransaction() {return batchTrans;}	
	
	List serialTrans = null;
	public void setSerialTrans(List serialTrans) {this.serialTrans = serialTrans;}
	public List getSerialTrans() {return this.serialTrans;}
	
	Item oItem = null;
	public Item getItem()
	{
		try
		{
			if (StringUtil.isNotEmpty(getItemId()) && oItem == null)
			{
				oItem = ItemTool.getItemByID(getItemId());			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return oItem;
	}
	
	Unit oUnit = null;
	public Unit getUnit()
	{
		try
		{
			if (StringUtil.isNotEmpty(getUnitId()) && oUnit == null)
			{
				oUnit = UnitTool.getUnitByID(getUnitId());			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return oUnit;
	}
	
	Tax oTax = null;
	public Tax getTax()
	{
		try
		{
			if (StringUtil.isNotEmpty(getTaxId()) && oTax == null)
			{
				oTax = TaxTool.getTaxByID(getTaxId());			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return oTax;
	}

	public String getTxTypeCode()
	{		
		return MedicalSalesTool.getTxTypeCode(getTxType(), getItemId());	
	}
	
	//-------------------------------------------------------------------------
	// function method
	//-------------------------------------------------------------------------
	public void recalulate()
	{
		if(getItemPrice() != null && getQty() != null && getTaxAmount() != null)
		{
	        double dSubTotal =  getItemPrice().doubleValue() * getQty().doubleValue();            
	        double dSubTotalDisc = Calculator.calculateDiscount(getDiscount(), dSubTotal, getQty());
	        dSubTotal = dSubTotal - dSubTotalDisc;
	        double dSubTotalTax = (getTaxAmount().doubleValue() / 100) * dSubTotal;
	        
	        setSubTotalDisc (new BigDecimal (dSubTotalDisc));
	        setSubTotalTax  (new BigDecimal (dSubTotalTax));
	        setSubTotal     (new BigDecimal (dSubTotal));			
		}
	}
	
	//-------------------------------------------------------------------------
	// Discount method
	//-------------------------------------------------------------------------
	
	Discount discountData = null;
	public Discount getDiscountData()
	{
		try
		{
			if (StringUtil.isNotEmpty(getDiscountId()) && discountData == null)
			{
				discountData = DiscountTool.getDiscountByID(getDiscountId());			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return discountData;
	}
	
	public boolean isDiscByAmount()
	{
		if(StringUtil.isNotEmpty(getDiscount()))
		{
			if (!StringUtil.isEqual(getDiscount(),"0") && !StringUtil.containsIgnoreCase(getDiscount(), "%"))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean isSpecialDiscByAmount()
	{
		if(isDiscByAmount())
		{
			if(StringUtil.isEqual(getDiscountId(), PreferenceTool.posSpcDiscCode()))
			{
				return true;
			}
		}		
		return false;
	}

	public boolean isQtyDiscount()
	{
		getDiscountData();
		if(discountData != null)
		{
			if(discountData.getDiscountType() == DiscountTool.i_BY_EVTQTY || 
			   discountData.getDiscountType() == DiscountTool.i_BY_QTY)
			{
				return true;
			}
		}
		return false;
	}

	public boolean isMultiUnit()
	{
		getDiscountData();
		if(discountData != null)
		{
			if(discountData.getDiscountType() == DiscountTool.i_BY_MULTI)
			{
				return true;
			}
		}
		return false;
	}

	public boolean isPwpDiscount()
	{
		getDiscountData();
		if(discountData != null)
		{
			if(discountData.getDiscountType() == DiscountTool.i_BY_PWP)
			{
				return true;
			}
		}
		return false;
	}
		
	int pwpType = 0;
	public void setPwpType(int _pwpType)
	{
		pwpType = _pwpType;
	}
	public int getPwpType()
	{
		return pwpType;
	}
	BigDecimal qoh = bd_ZERO;
	public void setQoh(BigDecimal _qoh)
	{
		qoh = _qoh;
	}
	
	public BigDecimal getQoh()
	{
		return qoh;
	}
}

package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.AdjustmentTypeManager;
import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.om.AdjustmentTypePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: AdjustmentTypeTool.java,v 1.10 2008/04/18 09:43:38 albert Exp $ <br>
 *
 * <pre>
 * $Log: AdjustmentTypeTool.java,v $
 * Revision 1.10  2008/04/18 09:43:38  albert
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/17 13:46:34  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/07/12 03:46:16  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/04/17 13:05:10  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class AdjustmentTypeTool extends BaseTool 
{
	public static List getAllAdjustmentType()
		throws Exception
	{
        return AdjustmentTypeManager.getInstance().getAllAdjustmentType();
	}

	public static List getTrfAdjustmentType()
		throws Exception
	{
        return AdjustmentTypeManager.getInstance().getTrfAdjustmentType();
	}

	public static AdjustmentType getAdjustmentTypeByID(String _sID)
    	throws Exception
    {
        return AdjustmentTypeManager.getInstance().getAdjustmentTypeByID(_sID, null);
	}

	public static AdjustmentType getAdjustmentTypeByID(String _sID, Connection _oConn)
		throws Exception
	{
	    return AdjustmentTypeManager.getInstance().getAdjustmentTypeByID(_sID, _oConn);
	}
	
	public static AdjustmentType getDefaultAdjustmentType()
    	throws Exception
    {
		List vData = getAllAdjustmentType();
		if (vData.size() > 0) {
			return (AdjustmentType) vData.get(0);
		}
		return null;
	}

	public static String getCodeByID(String _sID)
    	throws Exception
    {
		AdjustmentType oAdjustmentType = getAdjustmentTypeByID (_sID);
		if (oAdjustmentType != null) {
			return oAdjustmentType.getAdjustmentTypeCode();
		}
		return "";
	}
	
	public static String getDescriptionByID(String _sID)
    	throws Exception
    {
		AdjustmentType oAdjustmentType = getAdjustmentTypeByID (_sID);
		if (oAdjustmentType != null) {
			return oAdjustmentType.getDescription();
		}
		return "";
	}
	
	public static AdjustmentType getByCode(String _sCode)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(AdjustmentTypePeer.ADJUSTMENT_TYPE_CODE, _sCode);
		List vData =  AdjustmentTypePeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return ((AdjustmentType)vData.get(0));
		}
		return null;
	}
}

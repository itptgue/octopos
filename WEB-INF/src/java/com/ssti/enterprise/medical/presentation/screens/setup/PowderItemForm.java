package com.ssti.enterprise.medical.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class PowderItemForm extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
		String sMode = data.getParameters().getString("mode", "");
		String sID = "";
		try
		{
			if (sMode.equals("update") || sMode.equals("delete"))
			{
				sID = data.getParameters().getString("PowderItemId");
				context.put("pwdit", PowderTool.getPowderItemByID(sID));
    		}
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}
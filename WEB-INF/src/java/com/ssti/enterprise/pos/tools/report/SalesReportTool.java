package com.ssti.enterprise.pos.tools.report;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.model.AnalyticalOM;
import com.ssti.enterprise.pos.om.DiscountPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.SalesOrderPeer;
import com.ssti.enterprise.pos.om.SalesReturnDetailPeer;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionDetailPeer;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.presentation.screens.report.sales.KategoriSalesReportData;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.financial.CashierBalanceTool;
import com.ssti.enterprise.pos.tools.sales.SalesAnalysisTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/report/SalesReportTool.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: SalesReportTool.java,v 1.7 2009/05/04 02:05:09 albert Exp $
 *
 * $Log: SalesReportTool.java,v $
 *
 * 2017-05-27
 * - Change get method getAverageSalesPerDay reduce with sd.returned_qty
 * - Change in SalesAnlysisTool.sumDetailAmount add AS total_amount as needed
 * 
 * 2015-04-28
 * - Add method getSalesAndReturn 
 */
public class SalesReportTool extends BaseTool
{
	private static Log log = LogFactory.getLog ( SalesReportTool.class );
	
	private static final int i_KAT_ID   = 1;
	private static final int i_KAT_QTY  = 2;
	private static final int i_KAT_AMT  = 3;
	private static final int i_KAT_COST = 4;
	
	public static final int i_DET_ORDER_BY_CODE = 1;
	public static final int i_DET_ORDER_BY_NAME = 2;
	
	static SalesReportTool instance = null;
	
	public static synchronized SalesReportTool getInstance() 
	{
		if (instance == null) instance = new SalesReportTool();
		return instance;
	}	
	
	///////////////////////////////////////////////////////////////////////////
	// SALES PER KATEGORI 
	///////////////////////////////////////////////////////////////////////////
	
	public static List getTotalSalesPerKategori (Date _dStart, Date _dEnd, String _sLocationID)
		throws Exception
	{		
	    return getTotalSalesPerKategori (_dStart, _dEnd, _sLocationID, -1);
    }
    
	public static List getTotalSalesPerKategori (Date _dStart, Date _dEnd, String _sLocationID, int _iStatus)
		throws Exception
	{
	    return getTotalSalesPerKategori (_dStart, _dEnd, _sLocationID, "", "", _iStatus, false);		
	}

	public static List getTotalSalesPerKategori (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 
												 String _sDepartmentID,
												 String _sProjectID,
												 int _iStatus, 
												 boolean _bIncTax)
		throws Exception
	{		
		return getTotalSalesPerKategori (_dStart, _dEnd, "", "", "", "", "", "", "", _sLocationID, _sDepartmentID, _sProjectID, _iStatus, _bIncTax);
	}
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _iStatus
	 * @param _bIncTax
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSalesPerKategori (Date _dStart, 
												 Date _dEnd, 
												 String _sCustID,
												 String _sCustTypeID,
												 String _sPmtTypeID,
												 String _sCashier,
												 String _sEmpID,
												 String _sVendorID,
												 String _sSiteID,												 
												 String _sLocationID, 											
												 String _sDepartmentID,
												 String _sProjectID,
												 int _iStatus, 
												 boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT k.kategori_id, SUM(sd.qty_base), ");
		SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
		oSQL.append (",");
		
		oSQL.append (" SUM(sd.sub_total_cost), ");
		oSQL.append (" COUNT(DISTINCT s.sales_transaction_id) AS total_trans ");			
		oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd, kategori k, item i ");
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{
			oSQL.append(" ,customer c ");
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append(" ,location l ");
		}				
		oSQL.append (" WHERE " );
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append (" AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append (" AND sd.item_id = i.item_id ");
		oSQL.append (" AND i.kategori_id = k.kategori_id ");
		if (StringUtil.isNotEmpty(_sPmtTypeID))
		{
			oSQL.append (" AND s.payment_type_id = '").append(_sPmtTypeID).append("' ");				
		}				
		if (StringUtil.isNotEmpty(_sCustID))
		{
			oSQL.append (" AND s.customer_id = '").append(_sCustID).append("' ");				
		}
		if (StringUtil.isNotEmpty(_sEmpID))
		{
			oSQL.append (" AND s.sales_id = '").append(_sEmpID).append("' ");				
		}		
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{
			oSQL.append (" AND s.customer_id = c.customer_id ");
			oSQL.append (" AND c.customer_type_id = '").append(_sCustTypeID).append("' ");
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append (" AND s.location_id = l.location_id ");
			oSQL.append (" AND l.site_id = '").append(_sSiteID).append("' ");
		}		
		oSQL.append (" GROUP BY k.kategori_id, s.is_inclusive_tax ");		
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _iStatus
	 * @param _bIncTax
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSOPerKategori (Date _dStart, 
											  Date _dEnd, 
											  String _sCustID,
											  String _sCustTypeID,
											  String _sPmtTypeID,
											  String _sCashier,
											  String _sEmpID,
											  String _sVendorID,
											  String _sSiteID,												 
											  String _sLocationID, 											
											  String _sDepartmentID,
											  String _sProjectID,
											  int _iStatus, 
											  boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT k.kategori_id, SUM(sd.qty_base), ");
		SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
		oSQL.append (",");
		
		oSQL.append (" SUM(sd.cost_per_unit * sd.qty_base), ");
		oSQL.append (" COUNT(s.sales_order_no) AS total_trans ");			
		oSQL.append (" FROM sales_order s, sales_order_detail sd, kategori k, item i ");
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{
			oSQL.append(" ,customer c ");
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append(" ,location l ");
		}				
		oSQL.append (" WHERE " );
		SalesAnalysisTool.buildDateQuery(oSQL, "s.transaction_date", _dStart, _dEnd);
		
		oSQL.append (" AND s.sales_order_id = sd.sales_order_id ");
		oSQL.append (" AND sd.item_id = i.item_id ");
		oSQL.append (" AND i.kategori_id = k.kategori_id ");
		oSQL.append (" AND (s.transaction_status = ").append(i_SO_ORDERED)
		    .append (" OR s.transaction_status = ").append(i_SO_DELIVERED).append(") ");
		
		if (StringUtil.isNotEmpty(_sPmtTypeID))
		{
			oSQL.append (" AND s.payment_type_id = '").append(_sPmtTypeID).append("' ");				
		}				
		if (StringUtil.isNotEmpty(_sCustID))
		{
			oSQL.append (" AND s.customer_id = '").append(_sCustID).append("' ");				
		}
		if (StringUtil.isNotEmpty(_sEmpID))
		{
			oSQL.append (" AND s.sales_id = '").append(_sEmpID).append("' ");				
		}		
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{
			oSQL.append (" AND s.customer_id = c.customer_id ");
			oSQL.append (" AND c.customer_type_id = '").append(_sCustTypeID).append("' ");
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append (" AND s.location_id = l.location_id ");
			oSQL.append (" AND l.site_id = '").append(_sSiteID).append("' ");
		}			
		oSQL.append (" GROUP BY k.kategori_id, s.is_inclusive_tax ");		
		log.debug (oSQL.toString());
		return SalesOrderPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _iStatus
	 * @param _bIncTax
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerKategori(Date _dStart, 
												 Date _dEnd, 
												 String _sCustID,
												 String _sCustTypeID,
												 String _sPmtTypeID,
												 String _sCashier,
												 String _sEmpID,
												 String _sVendorID,
												 String _sSiteID,												 
												 String _sLocationID, 											
												 String _sDepartmentID,
												 String _sProjectID,
												 int _iStatus, 
												 boolean _bIncTax)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT k.kategori_id, SUM(sd.qty_base), ");
		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");			
		}
		
		//oSQL.append (",");
		
		oSQL.append (" SUM(sd.sub_total_cost), ");
		oSQL.append (" COUNT(DISTINCT s.sales_return_id) AS total_trans ");			
		oSQL.append (" FROM sales_return s, sales_return_detail sd, kategori k, item i ");
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{
			oSQL.append(" ,customer c ");
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append(" ,location l ");
		}				
		oSQL.append (" WHERE " );
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append (" AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append (" AND sd.item_id = i.item_id ");
		oSQL.append (" AND i.kategori_id = k.kategori_id ");
//		if (StringUtil.isNotEmpty(_sPmtTypeID))
//		{
//			oSQL.append (" AND s.payment_type_id = '").append(_sPmtTypeID).append("' ");				
//		}				
		if (StringUtil.isNotEmpty(_sCustID))
		{
			oSQL.append (" AND s.customer_id = '").append(_sCustID).append("' ");				
		}
		if (StringUtil.isNotEmpty(_sEmpID))
		{
			oSQL.append (" AND s.sales_id = '").append(_sEmpID).append("' ");				
		}		
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{
			oSQL.append (" AND s.customer_id = c.customer_id ");
			oSQL.append (" AND c.customer_type_id = '").append(_sCustTypeID).append("' ");
		}
		if (StringUtil.isNotEmpty(_sSiteID))
		{
			oSQL.append (" AND s.location_id = l.location_id ");
			oSQL.append (" AND l.site_id = '").append(_sSiteID).append("' ");
		}		
		oSQL.append (" GROUP BY k.kategori_id, s.is_inclusive_tax ");		
		log.debug (oSQL.toString());
		return SalesReturnPeer.executeQuery(oSQL.toString());	
	}		
	
	public List mergeSalesKategoriData (List _vTree, List _vSales)
	    throws Exception
	{
		return mergeSalesKategoriData(_vTree, _vSales, null);
	}
	
	public List mergeSalesKategoriData (List _vTree, List _vSales, List _vReturn)
	    throws Exception
	{
	    List vResult = new ArrayList (_vTree.size());
	    for (int i = 0; i < _vTree.size(); i++)
	    {
            Kategori oKat = (Kategori) _vTree.get(i);
            if (oKat != null) 
            {
                KategoriSalesReportData oData = new KategoriSalesReportData();       
                oData.setKategoriId    (oKat.getKategoriId());
                oData.setKategoriCode  (oKat.getKategoriCode());
                oData.setDescription   (oKat.getDescription());
                oData.setKategoriLevel (oKat.getKategoriLevel());
                oData.setParentId      (oKat.getParentId());
                setSalesData           (oData, _vSales, _vReturn, vResult);
                vResult.add            (oData);            
            }
        }
        return vResult;
    }
    
    private void setSalesData (KategoriSalesReportData _oData, List _vSales, List _vReturn, List _vResult)
        throws Exception
    {
        double dTotalQty  = 0;
        double dTotalAmt  = 0;
        double dTotalCost = 0;
        int iTotalTrans = 0;
        List vIDList = KategoriTool.getChildIDList(_oData.getKategoriId());
        vIDList.add (_oData.getKategoriId());
        
        for (int i = 0; i < vIDList.size(); i++)
        {
            String sKatID = (String) vIDList.get(i);
            for (int j = 0; j < _vSales.size(); j++)
            {
                Record oSalesData = (Record) _vSales.get(j);
                if (oSalesData.getValue(i_KAT_ID).asString().equals(sKatID))
                {
                    dTotalQty  += oSalesData.getValue(i_KAT_QTY).asDouble();
                    dTotalAmt  += oSalesData.getValue(i_KAT_AMT).asDouble();
                    dTotalCost += oSalesData.getValue(i_KAT_COST).asDouble();
                    iTotalTrans += oSalesData.getValue("total_trans").asInt();
                }
            }

            if(_vReturn != null)
            {
	            for (int k = 0; k < _vReturn.size(); k++)
	            {
	                Record oReturnData = (Record) _vReturn.get(k);
	                if (oReturnData.getValue(i_KAT_ID).asString().equals(sKatID))
	                {
	                    dTotalQty  -= oReturnData.getValue(i_KAT_QTY).asDouble();
	                    dTotalAmt  -= oReturnData.getValue(i_KAT_AMT).asDouble();
	                    dTotalCost -= oReturnData.getValue(i_KAT_COST).asDouble();
	                    iTotalTrans -= oReturnData.getValue("total_trans").asInt();
	                }
	            }
            }
        }
        
        _oData.setTotalTrans  (iTotalTrans);
        _oData.setTotalQty    (new BigDecimal (dTotalQty ));
        _oData.setTotalAmount (new BigDecimal (dTotalAmt ));
        _oData.setTotalCost   (new BigDecimal (dTotalCost));
        _oData.setGrossProfit (new BigDecimal (dTotalAmt - dTotalCost));
        _oData.setPercentage  (calculatePercentage (_oData, _vResult));   
    }
    
    public BigDecimal calculatePercentage (KategoriSalesReportData _oData,  List _vResult)
    {
        double dPct = 100;
        
        for (int i = 0; i < _vResult.size(); i++)
        {
            KategoriSalesReportData oParent = (KategoriSalesReportData) _vResult.get(i);
            if (oParent.getKategoriId().equals(_oData.getParentId()))
            {
                double dTotal  = oParent.getTotalAmount().doubleValue();
                double dAmount = _oData.getTotalAmount().doubleValue();
                if (dTotal != 0) 
                {
                    dPct = (dAmount / dTotal ) * 100;
                }
                else 
                {
                    dPct = 0;
                }
                return new BigDecimal (dPct);           
            }
        }
        return new BigDecimal (dPct);
    }    

	/**
	 * get sales trans detail list by kategori
	 * @param _dStart
	 * @param _dEnd
	 * @param _bPaymentType
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static List getDetailsByKategori (List _vTrans, List _vKatID)
		throws Exception
	{		
		Criteria oCrit = new Criteria();
		List vID = new ArrayList(_vTrans.size());
		Iterator iter = _vTrans.iterator();
		while (iter.hasNext())
		{
			vID.add(((SalesTransaction)iter.next()).getSalesTransactionId());
		}
		oCrit.addIn(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, vID);
		oCrit.addJoin(SalesTransactionDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
		oCrit.addIn(ItemPeer.KATEGORI_ID, _vKatID);
		oCrit.addGroupByColumn(SalesTransactionDetailPeer.ITEM_ID);
		log.debug(oCrit);
		return SalesTransactionDetailPeer.doSelect(oCrit);
	}		
	
	public static KategoriSalesReportData filterKatResult(List _vSLKat, String _sKatID)
	{
		for (int i =0; i < _vSLKat.size(); i++)
		{
			KategoriSalesReportData oData = (KategoriSalesReportData) _vSLKat.get(i);
			if (StringUtil.isEqual(oData.getKategoriId(), _sKatID))
			{
				return oData;
			}
		}
		return null;
	}

	///////////////////////////////////////////////////////////////////////////
	// SALES PER ITEM
	///////////////////////////////////////////////////////////////////////////

	public static List getTotalSalesPerItemByCust (Date _dStart, 
			 								 	   Date _dEnd, 
			 								 	   String _sLocationID, 
			 								 	   String _sCustomerID)
        throws Exception
	{	
		return getTotalSalesPerItem (_dStart, _dEnd, _sLocationID, "", "", "", "", "", _sCustomerID, "", "", "", "", false, false, 2, 1, null, null);
	}
	
	public static List getTotalSalesPerItem (Date _dStart, 
						 					 Date _dEnd, 
						 					 String _sLocationID, 
						 					 String _sKategoriID, 
						 					 int _iStatus)
		throws Exception
	{	
		return getTotalSalesPerItem (_dStart, _dEnd, _sLocationID, "", "", _sKategoriID, "", "", "", "", "", "", 
									 false, false, _iStatus, -1);
	}

	public static List getTotalSalesNewItem (Date _dStart, 
			 								 Date _dEnd, 
			 								 String _sLocationID, 
			 								 String _sKategoriID,
			 								 String _sVendorID,
			 								 String _sEmployeeID,			 								 
			 								 int _iStatus,
			 								 int _iSortBy,
			 								 Date _dSAddDate,
			 								 Date _dEAddDate)
	throws Exception
	{	
		return getTotalSalesPerItem (_dStart, _dEnd, _sLocationID, "", "", _sKategoriID, "", _sVendorID, "", _sEmployeeID, "", "", "", 
								     false, false, _iStatus, -1, _dSAddDate, _dEAddDate);
	}
	
	public static List getTotalSalesPerItem (Date _dStart, 
											 Date _dEnd, 
											 String _sLocationID, 											 
											 String _sDepartmentID,
											 String _sProjectID,
											 String _sKategoriID, 
											 String _sItemID,
											 String _sVendorID,
											 String _sEmployeeID,
											 String _sEmpDetID,
											 String _sCashier,
											 String _sColor,
											 boolean _bIncTax,
											 boolean _bDiscount,
											 int _iStatus,
											 int _iSortBy) throws Exception
	{
		return getTotalSalesPerItem (_dStart, _dEnd, _sLocationID, _sDepartmentID, 
									 _sProjectID, _sKategoriID, _sItemID, _sVendorID, "",
									 _sEmployeeID, _sEmpDetID, _sCashier, _sColor, _bIncTax, 
									 _bDiscount, _iStatus, _iSortBy, null, null);
	}
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sKategoriID
	 * @param _sItemID
	 * @param _sVendorID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _sColor
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @param _dSAddDate
	 * @param _dEAddDate
	 * @return
	 * @throws Exception
	 */	
	
	public static List getTotalSalesPerItem (Date _dStart, 
											 Date _dEnd, 
											 String _sLocationID, 											 
											 String _sDepartmentID,
											 String _sProjectID,
											 String _sKategoriID, 
											 String _sItemID,
											 String _sVendorID,
											 String _sCustomerID,
											 String _sEmployeeID,
											 String _sEmpDetID,
											 String _sCashier,
											 String _sColor,
											 boolean _bIncTax,
											 boolean _bDiscount,
											 int _iStatus,
											 int _iSortBy,
											 Date _dSAddDate, //Item Add Date
											 Date _dEAddDate) 
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT sd.item_id, sd.item_code, sd.item_name, SUM(sd.qty_base) AS total_qty, ");

		SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
		oSQL.append (",");
		
		oSQL.append (" SUM(sd.sub_total_cost), COUNT(sd.item_id), COUNT(s.sales_transaction_id) AS total_trans ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc), sd.discount ");
		}
		oSQL.append (" FROM sales_transaction s, ");
		oSQL.append (" sales_transaction_detail sd, ");
		oSQL.append (" kategori k, item i  WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append ("  AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append ("  AND sd.item_id = i.item_id ");
        oSQL.append ("  AND i.kategori_id = k.kategori_id ");
		
        if (StringUtil.isNotEmpty(_sItemID))
        {
		    oSQL.append ("  AND i.item_id = '");
		    oSQL.append (_sItemID);
		    oSQL.append ("'");
        }
        else
        {
			if (StringUtil.isNotEmpty(_sKategoriID))
			{		
	    		List vChildID = KategoriTool.getChildIDList(_sKategoriID);
	    		
	    		if (vChildID.size() > 0)
	    		{
	    		    vChildID.add (_sKategoriID);
	    		    oSQL.append ("  AND k.kategori_id IN ");
	                oSQL.append (SqlUtil.convertToINMode(vChildID));
	    		}
	    		else 
	    		{
	    		    oSQL.append ("  AND k.kategori_id = '");
	    		    oSQL.append (_sKategoriID);
	    		    oSQL.append ("'");
		        }
		    }  
        }
        if (StringUtil.isNotEmpty(_sCustomerID))
        {
		    oSQL.append ("  AND s.customer_id = '");
		    oSQL.append (_sCustomerID);
		    oSQL.append ("'");        	
        }
        if (StringUtil.isNotEmpty(_sEmployeeID))
        {
		    oSQL.append ("  AND s.sales_id = '");
		    oSQL.append (_sEmployeeID);
		    oSQL.append ("'");
        }
        if (StringUtil.isNotEmpty(_sEmpDetID))
        {
		    oSQL.append ("  AND sd.employee_id = '");
		    oSQL.append (_sEmpDetID);
		    oSQL.append ("'");
        }        
        if (StringUtil.isNotEmpty(_sCashier))
        {
		    oSQL.append ("  AND s.cashier_name = '");
		    oSQL.append (_sCashier);
		    oSQL.append ("'");
        }
        
        if (StringUtil.isNotEmpty(_sVendorID))
        {
		    oSQL.append ("  AND i.prefered_vendor_id = '");
		    oSQL.append (_sVendorID);
		    oSQL.append ("'");
        }        
        if (StringUtil.isNotEmpty(_sColor))
        {
		    oSQL.append ("  AND i.color = '");
		    oSQL.append (_sColor);
		    oSQL.append ("'");
        }
        if (_dSAddDate != null)
        {
		    oSQL.append ("  AND i.add_date >= '");
		    oSQL.append (CustomFormatter.formatSqlDate(_dSAddDate));
		    oSQL.append ("'");
        }
        if (_dEAddDate != null)
        {
		    oSQL.append ("  AND i.add_date <= '");
		    oSQL.append (CustomFormatter.formatSqlDate(_dEAddDate));
		    oSQL.append ("'");
        }
        
		oSQL.append (" GROUP BY sd.item_id, sd.item_code, sd.item_name, s.is_inclusive_tax ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(" , sd.discount ");
		}
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY sd.item_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY sd.item_name ");
		}
				
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sKategoriID
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerItem(Date _dStart, 
											 Date _dEnd, 
											 String _sLocationID, 											 
											 String _sDepartmentID,
											 String _sProjectID,
											 String _sKategoriID, 
											 String _sItemID,
											 String _sVendorID,
											 String _sEmployeeID,
											 String _sCashier,
											 String _sColor,
											 boolean _bIncTax,
											 boolean _bDiscount,
											 int _iStatus,
											 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT sd.item_id, sd.item_code, sd.item_name, SUM(sd.qty_base), ");
		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");			
		}
		oSQL.append (" SUM(sd.sub_total_cost), COUNT(sd.item_id), COUNT(s.sales_return_id) ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc), sd.discount ");
		}
		
		oSQL.append (" FROM sales_return s, ");
		oSQL.append (" sales_return_detail sd, ");
		oSQL.append (" kategori k, item i  WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append ("  AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append ("  AND sd.item_id = i.item_id ");
        oSQL.append ("  AND i.kategori_id = k.kategori_id ");
        oSQL.append ("  AND s.transaction_type != 1 ");
		
        if (StringUtil.isNotEmpty(_sItemID))
        {
		    oSQL.append ("  AND i.item_id = '");
		    oSQL.append (_sItemID);
		    oSQL.append ("'");
        }
        else
        {
			if (StringUtil.isNotEmpty(_sKategoriID))
			{		
	    		List vChildID = KategoriTool.getChildIDList(_sKategoriID);
	    		
	    		if (vChildID.size() > 0)
	    		{
	    		    vChildID.add (_sKategoriID);
	    		    oSQL.append ("  AND k.kategori_id IN ");
	                oSQL.append (SqlUtil.convertToINMode(vChildID));
	    		}
	    		else 
	    		{
	    		    oSQL.append ("  AND k.kategori_id = '");
	    		    oSQL.append (_sKategoriID);
	    		    oSQL.append ("'");
		        }
		    }  
        }
        if (StringUtil.isNotEmpty(_sEmployeeID))
        {
		    oSQL.append ("  AND s.sales_id = '");
		    oSQL.append (_sEmployeeID);
		    oSQL.append ("'");
        }
        if (StringUtil.isNotEmpty(_sCashier))
        {
		    oSQL.append ("  AND s.cashier_name = '");
		    oSQL.append (_sCashier);
		    oSQL.append ("'");
        }
        
        if (StringUtil.isNotEmpty(_sVendorID))
        {
		    oSQL.append ("  AND i.prefered_vendor_id = '");
		    oSQL.append (_sVendorID);
		    oSQL.append ("'");
        }        
        if (StringUtil.isNotEmpty(_sColor))
        {
		    oSQL.append ("  AND i.color = '");
		    oSQL.append (_sColor);
		    oSQL.append ("'");
        }
        
		oSQL.append (" GROUP BY sd.item_id, sd.item_code, sd.item_name, s.is_inclusive_tax ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(" , sd.discount ");
		}
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY sd.item_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY sd.item_name ");
		}
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	
	
	public static List getSalesBySub (Date _dStart, 
									  Date _dEnd, 
									  AnalyticalOM _oSub,
									  String _sKategoriID,
									  String _sItemID,
									  int _iStatus)
		throws Exception
	{
		return getSalesBySub(_dStart, _dEnd, _oSub, _sKategoriID, _sItemID, false, false, _iStatus);
	}
	

	public static List getSalesBySub (Date _dStart, 
			 						  Date _dEnd, 
			 						  AnalyticalOM _oSub,
			 						  String _sKategoriID,
			 						  String _sItemID,
			 						  boolean _bIncTax,
			 						  boolean _bDisc,
			 						  int _iStatus)
		throws Exception
	{
		if (_oSub != null)
		{
			if (_oSub instanceof Location) 
			{
				return getTotalSalesPerItem (_dStart, _dEnd, _oSub.getId(), "", "", _sKategoriID, _sItemID, "", "", "", "", "", _bIncTax, _bDisc, _iStatus, -1);
			}			
		}
		return getTotalSalesPerItem (_dStart, _dEnd, "", "", "", _sKategoriID, _sItemID, "", "", "", "", "", _bIncTax, _bDisc, _iStatus, -1);
	}
    
	//-------------------------------------------------------------------------
	// CUSTOMER SALES REPORT
	//-------------------------------------------------------------------------
	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustomerTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSalesPerCustomer (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sCustomerTypeID, 
												 String _sEmployeeID,
												 String _sCashier,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{
		return getTotalSalesPerCustomer(_dStart, _dEnd, _sLocationID, _sDepartmentID, _sProjectID, 
										_sCustomerTypeID, "", _sEmployeeID, _sCashier, _bIncTax, _bDiscount, _iStatus, _iSortBy);	
	}

	public static List getTotalSalesPerCustomer (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sCustomerTypeID, 
												 String _sCustomerID,
												 String _sEmployeeID,
												 String _sCashier,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
					throws Exception
	{
		return getTotalSalesPerCustomer(_dStart, _dEnd, _sLocationID, _sDepartmentID, _sProjectID, 
				_sCustomerTypeID, _sCustomerID, "", "", "", "", _sEmployeeID, _sCashier, _bIncTax, _bDiscount, _iStatus, _iSortBy);	
	}

	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustomerTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSalesPerCustomer (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sCustomerTypeID,
												 String _sCustomerID,
												 String _sItemID,
												 String _sKategoriID,
												 String _sVendorID,
												 String _sBrand,
												 String _sEmployeeID,
												 String _sCashier,												 
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();

		if(StringUtil.isNotEmpty(_sDepartmentID) || StringUtil.isNotEmpty(_sProjectID) || 
		   StringUtil.isNotEmpty(_sKategoriID) || StringUtil.isNotEmpty(_sItemID) || StringUtil.isNotEmpty(_sVendorID))
		{
			oSQL.append ("SELECT c.customer_id, c.customer_type_id, c.customer_code, c.customer_name,   ");
			oSQL.append (" SUM(sd.qty), ");
	
			SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
			oSQL.append (" AS total_amount ,SUM(sd.sub_total_cost) AS total_cost");
			oSQL.append (" ,COUNT(s.sales_transaction_id) AS total_trans ");
			if (_bDiscount) //include discount
			{
				oSQL.append(",SUM(sd.sub_total_disc)");
			}		
			oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd, customer c, item i ");			
			oSQL.append (" WHERE ");			
			SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);

			oSQL.append(" AND s.sales_transaction_id = sd.sales_transaction_id ");
			oSQL.append(" AND sd.item_id = i.item_id ");
			
	        if (StringUtil.isNotEmpty(_sItemID))
	        {
			    oSQL.append ("  AND i.item_id = '");
			    oSQL.append (_sItemID);
			    oSQL.append ("'");
	        }
	        else
	        {
				if (StringUtil.isNotEmpty(_sKategoriID))
				{		
		    		List vChildID = KategoriTool.getChildIDList(_sKategoriID);
		    		
		    		if (vChildID.size() > 0)
		    		{
		    		    vChildID.add (_sKategoriID);
		    		    oSQL.append ("  AND i.kategori_id IN ");
		                oSQL.append (SqlUtil.convertToINMode(vChildID));
		    		}
		    		else 
		    		{
		    		    oSQL.append ("  AND i.kategori_id = '");
		    		    oSQL.append (_sKategoriID);
		    		    oSQL.append ("'");
			        }
			    }  
	        }
	        if(StringUtil.isNotEmpty(_sVendorID))
	        {
			    oSQL.append ("  AND i.prefered_vendor_id = '");
			    oSQL.append (_sVendorID);
			    oSQL.append ("'");	        	
	        }			
		}
		else
		{
		
			oSQL.append ("SELECT c.customer_id, c.customer_type_id, c.customer_code, c.customer_name, SUM(s.total_qty), ");
	
			SalesAnalysisTool.sumTotalAmount(oSQL,_bIncTax);
			oSQL.append (" ,SUM(s.total_cost) AS total_cost ");		
			oSQL.append (" ,COUNT(s.sales_transaction_id) AS total_trans ");
			
			if (_bDiscount) //include discount
			{
				oSQL.append(",SUM(s.total_discount)");
			}
			
			oSQL.append (" FROM sales_transaction s, customer c  WHERE ");
			SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
		}
		
		oSQL.append (" AND s.customer_id = c.customer_id ");
		
		if (StringUtil.isNotEmpty(_sCustomerID))
		{
			oSQL.append ("  AND s.customer_id = '");
			oSQL.append (_sCustomerID);
			oSQL.append ("'");			
		}
		else
		{
			if (StringUtil.isNotEmpty(_sCustomerTypeID))
			{		
				oSQL.append ("  AND c.customer_type_id = '");
				oSQL.append (_sCustomerTypeID);
				oSQL.append ("'");
			}
		}
		if (StringUtil.isNotEmpty(_sEmployeeID))
		{
			oSQL.append ("  AND s.sales_id = '");
			oSQL.append (_sEmployeeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sCashier))
		{
			oSQL.append ("  AND s.cashier_name = '");
			oSQL.append (_sCashier);
			oSQL.append ("'");
		}
		
		oSQL.append (" GROUP BY c.customer_id, c.customer_type_id, c.customer_code, c.customer_name, s.is_inclusive_tax ");
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY c.customer_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY c.customer_name ");
		}
		else if (_iSortBy == 10)
		{
			oSQL.append(" ORDER BY total_trans ");
		}
		else if (_iSortBy == 11)
		{
			oSQL.append(" ORDER BY total_amount ");
		}

		System.out.println(oSQL);
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerCustomer(Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sCustTypeID, 
												 String _sEmployeeID,
												 String _sCashier,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT c.customer_id, c.customer_type_id, c.customer_code, c.customer_name, SUM(sd.qty_base), ");
		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");			
		}
		oSQL.append (" SUM(sd.sub_total_cost) ");
		oSQL.append (" ,COUNT(s.sales_return_id) ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc) ");
		}
		
		oSQL.append (" FROM sales_return s, sales_return_detail sd, customer c WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append ("  AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append ("  AND s.customer_id = c.customer_id ");
		
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{		
			oSQL.append ("  AND c.customer_type_id = '");
			oSQL.append (_sCustTypeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sEmployeeID))
		{
			oSQL.append ("  AND s.sales_id = '");
			oSQL.append (_sEmployeeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sCashier))
		{
			oSQL.append ("  AND s.cashier_name = '");
			oSQL.append (_sCashier);
			oSQL.append ("'");
		}
		
		oSQL.append (" GROUP BY c.customer_id, c.customer_type_id, c.customer_code, c.customer_name, s.is_inclusive_tax, s.sales_return_id ");
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY c.customer_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY c.customer_name ");
		}
		log.debug (oSQL.toString());
		return SalesReturnPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * @@deprecated
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sCustomerID
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerCustomer(Date _dStart, 
											 	 Date _dEnd, 
											 	 String _sLocationID, 	
											 	 String _sCustomerID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.customer_id, SUM(s.total_qty), SUM(s.total_discount), SUM(s.total_tax), ");
		oSQL.append (" SUM(s.total_amount),  SUM(s.returned_amount) ");
		oSQL.append (" FROM sales_return s ");
		oSQL.append (" WHERE s.customer_id = '").append(_sCustomerID).append("'");
		oSQL.append (" AND s.return_date >= '").append(CustomFormatter.formatSqlDate(_dStart)).append("'");
		oSQL.append (" AND s.return_date <= '").append(CustomFormatter.formatSqlDate(_dEnd)).append("'");
		oSQL.append (" AND s.status = 2 ");		
		oSQL.append (" GROUP BY s.customer_id ");
		
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * calculate Item Discount from SalesTransactionDetail
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static double calcSubTotalDisc(String _sID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append (" SELECT SUM(sd.sub_total_disc) FROM sales_transaction_detail sd, sales_transaction s ");
		oSQL.append (" WHERE s.sales_transaction_id = sd.sales_transaction_id  ");
		oSQL.append (" AND s.sales_transaction_id = '").append(_sID).append("'");		
		log.debug (oSQL.toString());
		List v = SalesTransactionPeer.executeQuery(oSQL.toString());	
		if (v.size() > 0)
		{
			Record oRec = (Record) v.get(0);
			return oRec.getValue(1).asDouble();
		}
		return 0;
	}	
	
	/**
	 * calculate Item Discount from SalesReturnDetail
	 * @param _sID
	 * @return
	 * @throws Exception
	 */
	public static double calcReturnSubTotalDisc(String _sID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append (" SELECT SUM(sd.sub_total_disc) FROM sales_return_detail sd, sales_return s ");
		oSQL.append (" WHERE s.sales_return_id = sd.sales_return_id  ");
		oSQL.append (" AND s.sales_return_id = '").append(_sID).append("'");		
		log.debug (oSQL.toString());
		List v = SalesReturnPeer.executeQuery(oSQL.toString());	
		if (v.size() > 0)
		{
			Record oRec = (Record) v.get(0);
			return oRec.getValue(1).asDouble();
		}
		return 0;
	}	
	
	//-------------------------------------------------------------------------
	// SALES PER CUSTOMER TYPE
	//-------------------------------------------------------------------------	
	
	public static Map createCustomerTypeMap(List _vCustID)
	{
		Map mCT = new HashMap();
		try 
		{
			for (int i = 0; i < _vCustID.size(); i++)
			{
				String sCustID = (String) _vCustID.get(i);		
				String sCTID = CustomerTool.getCustomerTypeByID(sCustID);
				List vCust = (List)mCT.get(sCTID);
				if (vCust == null)
				{
					vCust = new ArrayList();
				}
				vCust.add(sCustID);
				mCT.put(sCTID, vCust);
			}
		} 
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx.getMessage(), _oEx);
		}
		return mCT;
	}

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSalesPerCustType (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sEmployeeID,
												 String _sCashier,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();

		if(StringUtil.isNotEmpty(_sDepartmentID) || StringUtil.isNotEmpty(_sProjectID))
		{
			oSQL.append ("SELECT ct.customer_type_id, ct.customer_type_code, ct.description,   ");
			oSQL.append (" SUM(sd.qty), ");
	
			SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
			oSQL.append (" AS total_amount ,SUM(sd.sub_total_cost) AS total_cost ");
			oSQL.append (" ,COUNT(s.sales_transaction_id) AS total_trans ");
			if (_bDiscount) //include discount
			{
				oSQL.append(",SUM(sd.sub_total_disc)");
			}		
			oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd, customer c, customer_type ct  ");			
			oSQL.append (" WHERE ");			
			SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);

			oSQL.append(" AND s.sales_transaction_id = sd.sales_transaction_id ");
		}
		else
		{
		
			oSQL.append ("SELECT ct.customer_type_id, ct.customer_type_code, ct.description, SUM(s.total_qty), ");
	
			SalesAnalysisTool.sumTotalAmount(oSQL,_bIncTax);
			oSQL.append (",");
			
			oSQL.append (" SUM(s.total_cost) ");
			oSQL.append (" ,COUNT(s.sales_transaction_id) ");
			
			if (_bDiscount) //include discount
			{
				oSQL.append(",SUM(s.total_discount)");
			}
			
			oSQL.append (" FROM sales_transaction s, customer c, customer_type ct  WHERE ");
			
			SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
		}
		oSQL.append (" AND s.customer_id = c.customer_id ");
		oSQL.append (" AND c.customer_type_id = ct.customer_type_id ");
		
		if (StringUtil.isNotEmpty(_sEmployeeID))
		{
			oSQL.append ("  AND s.sales_id = '");
			oSQL.append (_sEmployeeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sCashier))
		{
			oSQL.append ("  AND s.cashier_name = '");
			oSQL.append (_sCashier);
			oSQL.append ("'");
		}
		
		oSQL.append (" GROUP BY ct.customer_type_id, ct.customer_type_code, ct.description, s.is_inclusive_tax ");
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY ct.customer_type_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY ct.description ");
		}
		
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerCustType(Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sEmployeeID,
												 String _sCashier,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT ct.customer_type_id, ct.customer_type_code, ct.description, SUM(sd.qty_base), ");
		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");			
		}
		oSQL.append (" SUM(sd.sub_total_cost) ");
		oSQL.append (" ,COUNT(s.sales_return_id) ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc) ");
		}
		
		oSQL.append (" FROM sales_return s, sales_return_detail sd, customer c, customer_type ct WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append ("  AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append ("  AND s.customer_id = c.customer_id ");
		oSQL.append ("  AND c.customer_type_id = ct.customer_type_id ");
		
		if (StringUtil.isNotEmpty(_sEmployeeID))
		{
			oSQL.append ("  AND s.sales_id = '");
			oSQL.append (_sEmployeeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sCashier))
		{
			oSQL.append ("  AND s.cashier_name = '");
			oSQL.append (_sCashier);
			oSQL.append ("'");
		}
		
		oSQL.append (" GROUP BY ct.customer_type_id, ct.customer_type_code, ct.description, s.is_inclusive_tax ");
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY ct.customer_type_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY ct.description ");
		}
		log.debug (oSQL.toString());
		return SalesReturnPeer.executeQuery(oSQL.toString());	
	}		
	
	//-------------------------------------------------------------------------
	// SALES PER VENDOR
	//-------------------------------------------------------------------------	

    /**
     * Sales Group By Prefered Vendor Methods
     * 
     * @param _vData
     * @return
     * @throws Exception
     */
	public static Set getVendorList (List _vData) 
		throws Exception
	{		
		Set vData = new HashSet(_vData.size()); 
		for (int i = 0; i < _vData.size(); i++) 
		{
			SalesTransactionDetail oTrans = (SalesTransactionDetail) _vData.get(i);			
			String sVendorID = ItemTool.getItemByID(oTrans.getItemId()).getPreferedVendorId();
			vData.add(sVendorID);
		}
		return vData;
	}   

	public static List filterListByVendorID(List _vData, String _sVendorID) 
		throws Exception
	{		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) 
		{
			SalesTransactionDetail oTrans = (SalesTransactionDetail) _vData.get(i);			
			String sVendorID = ItemTool.getItemByID(oTrans.getItemId()).getPreferedVendorId();
			if (sVendorID != null && sVendorID.equals(_sVendorID))
			{
				vTrans.add(oTrans);
			}
		}
		return vTrans;
	}   

	public static List getVendorList (Date _dStart, 
			  						  Date _dEnd, 
			  						  String _sLocationID, 
									  String _sDeptID, 
									  String _sProjectID, 
									  String _sVendorID,	
									  int _iSortBy)
		throws Exception
	{
		return getVendorList(_dStart, _dEnd, _sLocationID, _sDeptID, _sProjectID, _sVendorID, "", _iSortBy);	
	}
	
	public static List getVendorList (Date _dStart, 
									  Date _dEnd, 
									  String _sLocationID, 
									  String _sDeptID, 
									  String _sProjectID, 
                                      String _sVendorID,
                                      String _sKategoriID,
									  int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT distinct v.vendor_id, v.vendor_code, v.vendor_name FROM ");
		oSQL.append (" sales_transaction s, sales_transaction_detail sd, item i, vendor v");
		oSQL.append (" WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, i_PROCESSED, _sLocationID, _sDeptID, _sProjectID);
		
		oSQL.append ("  AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append ("  AND sd.item_id = i.item_id ");
		oSQL.append ("  AND i.prefered_vendor_id = v.vendor_id ");

        if (StringUtil.isNotEmpty(_sVendorID))
        {
            oSQL.append ("  AND v.vendor_id = '").append(_sVendorID).append("'");            
        }
        
        if (StringUtil.isNotEmpty(_sKategoriID))
        {		
        	List vChildID = KategoriTool.getChildIDList(_sKategoriID);

        	if (vChildID.size() > 0)
        	{
        		vChildID.add (_sKategoriID);
        		oSQL.append ("  AND i.kategori_id IN ");
        		oSQL.append (SqlUtil.convertToINMode(vChildID));
        	}
        	else 
        	{
        		oSQL.append ("  AND i.kategori_id = '");
        		oSQL.append (_sKategoriID);
        		oSQL.append ("'");
        	}
        }          


		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY v.vendor_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY v.vendor_name ");
		}
		
		log.info(oSQL.toString());
		return BasePeer.executeQuery(oSQL.toString());			
	}	
	
	public static List getSalesDetList (Date _dStart, 
				  				        Date _dEnd, 
				  				        String _sLocationID,
				  				        String _sDeptID,
				  				        String _sProjectID,
				  				        String _sKategoriID,
				  				        int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT distinct sd.employee_id, e.employee_code, e.employee_name FROM ");
		oSQL.append (" sales_transaction s, sales_transaction_detail sd, employee e, item i ");
		oSQL.append (" WHERE ");

		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, i_PROCESSED, _sLocationID, _sDeptID, _sProjectID);

		oSQL.append ("  AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append ("  AND sd.employee_id = e.employee_id ");
		oSQL.append ("  AND sd.item_id = i.item_id ");
		
		if (StringUtil.isNotEmpty(_sKategoriID))
		{		
			List vChildID = KategoriTool.getChildIDList(_sKategoriID);

			if (vChildID.size() > 0)
			{
				vChildID.add (_sKategoriID);
				oSQL.append ("  AND i.kategori_id IN ");
				oSQL.append (SqlUtil.convertToINMode(vChildID));
			}
			else 
			{
				oSQL.append ("  AND i.kategori_id = '");
				oSQL.append (_sKategoriID);
				oSQL.append ("'");
			}
		}          
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY e.employee_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY e.employee_name ");
		}
		log.info(oSQL.toString());
		return BasePeer.executeQuery(oSQL.toString());			
	}	

	
	/**
	 * 
	 * @param _sItemID Item to check
	 * @param _iDays number of days to calculate backward
	 * @param _dToDate to date
	 * @return average sales / day
	 * @throws Exception
	 */
	public static double getAverageSalesPerDay (String _sItemID, String _sLocationID, int _iDays, Date _dToDate)
		throws Exception
	{		
		if (StringUtil.isNotEmpty(_sItemID) && StringUtil.isNotEmpty(_sLocationID))
		{
			Date dFrom = DateUtil.addDays(_dToDate, (_iDays * -1));
			StringBuilder oSQL = new StringBuilder ();
			oSQL.append ("SELECT SUM(sd.qty_base - sd.returned_qty) ");
			oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd WHERE ");
			SalesAnalysisTool.buildStdQuery(oSQL, dFrom, _dToDate, i_PROCESSED, _sLocationID, "", "");
			oSQL.append (" AND s.sales_transaction_id = sd.sales_transaction_id ");
			oSQL.append (" AND sd.item_id = '").append(_sItemID).append("'");
			
			log.debug (oSQL.toString());
			try 
			{
				List vData = SalesTransactionPeer.executeQuery(oSQL.toString());	
				if (vData.size() > 0)
				{
					Record oRecord = (Record) vData.get(0);
					double dTotalQty = oRecord.getValue(1).asDouble();
					if (_iDays > 0) return dTotalQty / _iDays;
				}
			} 
			catch (Exception e) 
			{
				log.error(e);
			}
		}
		return 0;
	}	
	
	
	//-------------------------------------------------------------------------
	// SALES ORDER DETAIL 
	//-------------------------------------------------------------------------	

	public static List getDetailForPO(List _vSOID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.sales_order_id, s.sales_order_no, s.transaction_date, s.location_id, s.transaction_status, ")
			.append (" sd.item_id, sd.item_code, i.item_name, sd.qty, sd.delivered_qty, (sd.qty - sd.delivered_qty) AS left, ")
			.append (" sd.unit_id, v.vendor_id, v.vendor_code, v.vendor_name ")
			.append (" FROM sales_order s  ")
			.append (" LEFT JOIN sales_order_detail sd ON s.sales_order_id = sd.sales_order_id ")
			.append (" LEFT JOIN item i ON sd.item_id = i.item_id ")		
			.append (" LEFT JOIN vendor v ON i.prefered_vendor_id = v.vendor_id ")		
			.append (" WHERE ")
			.append (" s.sales_order_id IN ").append(SqlUtil.convertToINMode(_vSOID))
			.append (" ORDER BY v.vendor_code ");
			
		log.debug(oSQL.toString());
		return BasePeer.executeQuery(oSQL.toString());			
	}
	
	public static Map createSOVendorMap(List vSOID)
		throws Exception
	{
		Map mVendor = new HashMap(vSOID.size());
		List vSOD = SalesReportTool.getDetailForPO(vSOID);
		for (int i = 0; i < vSOD.size(); i++)
		{
			Record oSOD = (Record)vSOD.get(i);
			String sVendID = oSOD.getValue("vendor_id").asString();
			if (StringUtil.isNotEmpty(sVendID))
			{
				List vItem = (List) mVendor.get(sVendID);
				if (vItem == null)
				{
					vItem = new ArrayList();
				}
				vItem.add(oSOD);
				mVendor.put(sVendID, vItem);			
			}
		}
		return mVendor;
	}
	
	//-------------------------------------------------------------------------
	// SALESMAN SALES REPORT
	//-------------------------------------------------------------------------
	public static List getTotalSalesPerSalesman (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{
		return getTotalSalesPerSalesman(_dStart, _dEnd, null, _sLocationID, _sDepartmentID, _sProjectID, _bIncTax, _bDiscount, _iStatus, _iSortBy);	
	}
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustomerTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSalesPerSalesman (Date _dStart, 
												 Date _dEnd, 
												 List _vSalesID,
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{				
		StringBuilder oSQL = new StringBuilder ();
		if(StringUtil.isNotEmpty(_sDepartmentID) || StringUtil.isNotEmpty(_sProjectID))
		{
			oSQL.append ("SELECT  ");
			oSQL.append (" s.sales_id, ");
			oSQL.append (" SUM(sd.qty), ");
	
			SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
			oSQL.append (" ,SUM(sd.sub_total_cost) ");
			oSQL.append (" ,COUNT(s.sales_transaction_id) ");
			if (_bDiscount) //include discount
			{
				oSQL.append(",SUM(sd.sub_total_disc)");
			}		
			oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd ");			
			oSQL.append (" WHERE ");			
			SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
			if(_vSalesID != null)
			{
				oSQL.append(" AND s.sales_id IN ").append(SqlUtil.convertToINMode(_vSalesID));
			}
			oSQL.append(" AND s.sales_transaction_id = sd.sales_transaction_id ");
		}
		else
		{
			oSQL.append ("SELECT  ");
			oSQL.append (" s.sales_id, ");
			oSQL.append (" SUM(s.total_qty), ");
	
			SalesAnalysisTool.sumTotalAmount(oSQL,_bIncTax);
			oSQL.append (" ,SUM(s.total_cost) ");
			oSQL.append (" ,COUNT(s.sales_transaction_id) ");
			if (_bDiscount) //include discount
			{
				oSQL.append(",SUM(s.total_discount)");
			}		
			oSQL.append (" FROM sales_transaction s ");			
			oSQL.append (" WHERE ");
			SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
			if(_vSalesID != null)
			{
				oSQL.append(" AND s.sales_id IN ").append(SqlUtil.convertToINMode(_vSalesID));
			}
		}
		oSQL.append (" GROUP BY s.sales_id, s.is_inclusive_tax ");		
		log.debug ("getSalesPerSalesman:" +  oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerSalesman(Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.sales_id, SUM(sd.qty_base), ");
		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");			
		}
		oSQL.append (" SUM(sd.sub_total_cost) ");
		oSQL.append(" ,COUNT(s.sales_return_id) ");		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc) ");
		}
		
		oSQL.append (" FROM sales_return s, sales_return_detail sd WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append ("  AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append (" GROUP BY s.sales_id, s.is_inclusive_tax, s.sales_return_id ");
		
		log.debug ("getReturnPerSalesman:" + oSQL.toString());
		return SalesReturnPeer.executeQuery(oSQL.toString());	
	}	
	
	//-------------------------------------------------------------------------
	// LOCATION SALES REPORT
	//-------------------------------------------------------------------------

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustomerTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSalesPerLocation (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sCustomerTypeID,
												 String _sCustomerID,
												 String _sItemID,
												 String _sKategoriID,
												 String _sVendorID,
												 String _sBrand,
												 String _sEmployeeID,
												 String _sCashier,												 
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();

		oSQL.append ("SELECT l.location_id, l.site_id, l.location_code, l.location_name,   ");
		oSQL.append (" SUM(sd.qty), ");

		SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
		oSQL.append (" AS total_amount ,SUM(sd.sub_total_cost) AS total_cost");
		oSQL.append (" ,COUNT(s.sales_transaction_id) AS total_trans ");
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc)");
		}		
		oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd, location l, customer c, item i ");			
		oSQL.append (" WHERE ");			
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);

		oSQL.append(" AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append(" AND sd.item_id = i.item_id ");
		oSQL.append(" AND s.location_id = c.customer_id ");
		
        if (StringUtil.isNotEmpty(_sItemID))
        {
		    oSQL.append ("  AND i.item_id = '");
		    oSQL.append (_sItemID);
		    oSQL.append ("'");
        }
        else
        {
			if (StringUtil.isNotEmpty(_sKategoriID))
			{		
	    		List vChildID = KategoriTool.getChildIDList(_sKategoriID);
	    		
	    		if (vChildID.size() > 0)
	    		{
	    		    vChildID.add (_sKategoriID);
	    		    oSQL.append ("  AND i.kategori_id IN ");
	                oSQL.append (SqlUtil.convertToINMode(vChildID));
	    		}
	    		else 
	    		{
	    		    oSQL.append ("  AND i.kategori_id = '");
	    		    oSQL.append (_sKategoriID);
	    		    oSQL.append ("'");
		        }
		    }  
        }
        if(StringUtil.isNotEmpty(_sVendorID))
        {
		    oSQL.append ("  AND i.prefered_vendor_id = '");
		    oSQL.append (_sVendorID);
		    oSQL.append ("'");	        	
        }			
	
		
		oSQL.append (" AND s.location_id = l.location_id ");
		
		if (StringUtil.isNotEmpty(_sCustomerID))
		{
			oSQL.append ("  AND s.customer_id = '");
			oSQL.append (_sCustomerID);
			oSQL.append ("'");			
		}
		else
		{
			if (StringUtil.isNotEmpty(_sCustomerTypeID))
			{		
				oSQL.append ("  AND c.customer_type_id = '");
				oSQL.append (_sCustomerTypeID);
				oSQL.append ("'");
			}
		}
		if (StringUtil.isNotEmpty(_sEmployeeID))
		{
			oSQL.append ("  AND s.sales_id = '");
			oSQL.append (_sEmployeeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sCashier))
		{
			oSQL.append ("  AND s.cashier_name = '");
			oSQL.append (_sCashier);
			oSQL.append ("'");
		}
		
		oSQL.append (" GROUP BY l.location_id, l.site_id, l.location_code, l.location_name, s.is_inclusive_tax ");
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY l.location_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY l.location_name ");
		}
		else if (_iSortBy == 10)
		{
			oSQL.append(" ORDER BY total_trans ");
		}
		else if (_iSortBy == 11)
		{
			oSQL.append(" ORDER BY total_amount ");
		}

		System.out.println(oSQL);
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerLocation(Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 String _sDepartmentID,
												 String _sProjectID,
												 String _sCustTypeID, 
												 String _sEmployeeID,
												 String _sCashier,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus,
												 int _iSortBy)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT l.location_id, l.site_id, l.location_code, l.location_name, SUM(sd.qty_base), ");
		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");			
		}
		oSQL.append (" SUM(sd.sub_total_cost) ");
		oSQL.append (" ,COUNT(s.sales_return_id) ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc) ");
		}
		
		oSQL.append (" FROM sales_return s, sales_return_detail sd, customer c WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append ("  AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append ("  AND s.customer_id = c.customer_id ");
		
		if (StringUtil.isNotEmpty(_sCustTypeID))
		{		
			oSQL.append ("  AND c.customer_type_id = '");
			oSQL.append (_sCustTypeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sEmployeeID))
		{
			oSQL.append ("  AND s.sales_id = '");
			oSQL.append (_sEmployeeID);
			oSQL.append ("'");
		}
		if (StringUtil.isNotEmpty(_sCashier))
		{
			oSQL.append ("  AND s.cashier_name = '");
			oSQL.append (_sCashier);
			oSQL.append ("'");
		}
		
		oSQL.append (" GROUP BY l.location_id, l.site_id, l.location_code, l.location_name, s.is_inclusive_tax, s.sales_return_id ");
		
		if (_iSortBy == i_DET_ORDER_BY_CODE)
		{
			oSQL.append(" ORDER BY l.location_code ");
		}
		else if (_iSortBy == i_DET_ORDER_BY_NAME)
		{
			oSQL.append(" ORDER BY l.location_name ");
		}
		log.debug (oSQL.toString());
		return SalesReturnPeer.executeQuery(oSQL.toString());	
	}
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalSalesPerLocation (Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID,
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus)
	throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT  ");
		oSQL.append (" s.location_id, ");
		oSQL.append (" SUM(s.total_qty), ");

		SalesAnalysisTool.sumTotalAmount(oSQL,_bIncTax);
		oSQL.append (" ,SUM(s.total_cost) ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(s.total_discount)");
		}
		
		oSQL.append (" FROM sales_transaction s ");
		oSQL.append (" WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, "", "");
				
		oSQL.append (" GROUP BY s.location_id, s.is_inclusive_tax ");		
		log.debug (oSQL.toString());
		return SalesTransactionPeer.executeQuery(oSQL.toString());	
	}	

	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getTotalReturnPerLocation(Date _dStart, 
												 Date _dEnd, 
												 String _sLocationID, 											 
												 boolean _bIncTax,
												 boolean _bDiscount,
												 int _iStatus)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.location_id, SUM(sd.qty_base), ");
		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(((sd.qty * sd.item_price) - sd.sub_total_disc) * s.currency_rate) END, ");			
		}
		oSQL.append (" SUM(sd.sub_total_cost) ");
		
		if (_bDiscount) //include discount
		{
			oSQL.append(",SUM(sd.sub_total_disc) ");
		}
		
		oSQL.append (" FROM sales_return s, sales_return_detail sd WHERE ");
		
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, "", "");
		
		oSQL.append ("  AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append (" GROUP BY s.location_id, s.is_inclusive_tax ");
		
		log.debug (oSQL.toString());
		return SalesReturnPeer.executeQuery(oSQL.toString());	
	}	
	
	public double getSoldQty(String _sItemID, Date _dStart, Date _dEnd, String _sLocationID, boolean _bIncRet) 
		throws Exception
	{
		double dSold = 0;
		StringBuilder oSQL = new StringBuilder();
		oSQL.append("SELECT item_id, SUM(sd.qty_base) FROM sales_transaction_detail sd,sales_transaction s WHERE ");
		if(_dStart == null) oSQL.append(" item_id != '' ");
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, i_TRANS_PROCESSED, "s.transaction_date", _sLocationID, "", "");		
		oSQL.append(" AND s.sales_transaction_id = sd.sales_transaction_id ");
		if (StringUtil.isNotEmpty(_sItemID)) oSQL.append(" AND sd.item_id = '").append(_sItemID).append("'");		
		oSQL.append(" GROUP BY sd.item_id");		
		log.debug (oSQL.toString());
		List vData = SalesTransactionPeer.executeQuery(oSQL.toString());
		if(vData.size() > 0)
		{
			Record oRec = (Record) vData.get(0);
			dSold = oRec.getValue(2).asDouble();
		}
		if (_bIncRet)
		{
			dSold = dSold - getReturnQty(_sItemID, _dStart, _dEnd, _sLocationID);
		}
		return dSold;			
	}
	
	public double getReturnQty(String _sItemID, Date _dStart, Date _dEnd, String _sLocationID) 
		throws Exception
	{
		double dRet = 0;
		StringBuilder oSQL = new StringBuilder();
		oSQL.append("SELECT item_id, SUM(sd.qty_base) FROM sales_return_detail sd,sales_return s WHERE ");		
		if(_dStart == null) oSQL.append(" item_id != '' ");
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, i_TRANS_PROCESSED, "s.return_date", _sLocationID, "", "");
		oSQL.append(" AND s.sales_return_id = sd.sales_return_id AND transaction_type != 1 "); //exclude return From DO
		if (StringUtil.isNotEmpty(_sItemID)) oSQL.append(" AND sd.item_id = '").append(_sItemID).append("'");		
		oSQL.append(" GROUP BY sd.item_id");		
		log.debug (oSQL.toString());
		List vData = SalesReturnPeer.executeQuery(oSQL.toString());
		if(vData.size() > 0)
		{
			Record oRec = (Record) vData.get(0);
			dRet = oRec.getValue(2).asDouble();
		}		
		return dRet;		
	}
	
    public List getSalesDetailByPeer (Date _dStart, Date _dEnd, String _sPeer, Object _sValue)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        if (_dStart != null) oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        if (_dEnd != null) oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        oCrit.addJoin(SalesTransactionPeer.SALES_TRANSACTION_ID,SalesTransactionDetailPeer.SALES_TRANSACTION_ID);                
        oCrit.addJoin(SalesTransactionDetailPeer.ITEM_ID,ItemPeer.ITEM_ID);
        oCrit.add(_sPeer,_sValue,Criteria.ILIKE);
        oCrit.addAscendingOrderByColumn(SalesTransactionPeer.TRANSACTION_DATE);
        oCrit.add(SalesTransactionPeer.STATUS,i_PROCESSED);
        return SalesTransactionDetailPeer.doSelect(oCrit);
    } 

    public List getReturnDetailByPeer (Date _dStart, Date _dEnd, String _sPeer, Object _sValue)
        throws Exception
    {
        Criteria oCrit = new Criteria();
        if (_dStart != null) oCrit.add(SalesReturnPeer.RETURN_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
        if (_dEnd != null) oCrit.and(SalesReturnPeer.RETURN_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
        oCrit.addJoin(SalesReturnPeer.SALES_RETURN_ID,SalesReturnDetailPeer.SALES_RETURN_ID);                
        oCrit.addJoin(SalesReturnDetailPeer.ITEM_ID,ItemPeer.ITEM_ID);
        oCrit.add(_sPeer,_sValue,Criteria.ILIKE);
        oCrit.addAscendingOrderByColumn(SalesReturnPeer.TRANSACTION_DATE);
        oCrit.add(SalesReturnPeer.STATUS,i_PROCESSED);
        return SalesReturnDetailPeer.doSelect(oCrit);
    }     
    
    //-------------------------------------------------------------------------
    //	Credit Card Report
    //-------------------------------------------------------------------------
    
    public List getCCSales(int _iCond, String _sKeywords,
    					   Date _dStart, 
    					   Date _dEnd,
    					   String _sPTypeID,
    					   String _sPTermID,
    					   String _sBankCode, 
    					   String _sLocID, 
    					   String _sCashier)
    	throws Exception
    {
    	StringBuilder oSQL = new StringBuilder();
    	oSQL.append("SELECT ip.bank_issuer, ip.reference_no, ip.approval_no, ip.payment_type_id, ip.payment_term_id, ip.transaction_id, ") 
    		.append(" ip.payment_amount, ip.mdr_amount, ip.invoice_payment_id, ip.trace_no, ip.terminal_id, ip.entry_mode, " )
    		.append(" tr.location_id, tr.invoice_no, tr.transaction_date, tr.cashier_name, tr.total_amount " )    		
    		.append(" FROM invoice_payment ip, sales_transaction tr, payment_type pt WHERE ")
    		.append(" ip.transaction_id = tr.sales_transaction_id AND ip.payment_type_id = pt.payment_type_id ")
    		.append(" AND pt.is_credit_card = true ")
    	;
    	if(StringUtil.isNotEmpty(_sPTypeID))
    	{
    		oSQL.append(" AND ip.payment_type_id = '").append(_sPTypeID).append("' ");
    	}
    	if(StringUtil.isNotEmpty(_sPTermID))
    	{
    		oSQL.append(" AND ip.payment_term_id = '").append(_sPTermID).append("' ");    		
    	}    	
    	if(StringUtil.isNotEmpty(_sBankCode))
    	{
    		oSQL.append(" AND ip.bank_issuer = '").append(_sBankCode).append("' ");
    	}
    	if(StringUtil.isNotEmpty(_sLocID))
    	{
    		oSQL.append(" AND tr.location_id = '").append(_sLocID).append("' ");
    	}
    	if(StringUtil.isNotEmpty(_sCashier))
    	{
    		oSQL.append(" AND tr.cashier_name = '").append(_sCashier).append("' ");    		
    	}
    	if(_dStart != null)
    	{
    		_dStart = DateUtil.getStartOfDayDate(_dStart);
    		oSQL.append(" AND tr.transaction_date >= '").append(CustomFormatter.formatSqlDate(_dStart)).append("' ");
    	}
    	if(_dEnd != null)
    	{
    		_dEnd = DateUtil.getEndOfDayDate(_dEnd);
    		oSQL.append(" AND tr.transaction_date <= '").append(CustomFormatter.formatSqlDate(_dEnd)).append("' ");
    	}
    	if(_iCond > 0)
    	{
    		if(!StringUtil.containsIgnoreCase(_sKeywords, "%"))
    		{
    			_sKeywords = "%" + _sKeywords + "%";
    		}
    		if(_iCond == 1)
    		{
        		oSQL.append(" AND tr.invoice_no ILIKE '").append(_sKeywords).append("' ");    			
    		}
    		if(_iCond == 2)
    		{
        		oSQL.append(" AND ip.reference_no ILIKE '").append(_sKeywords).append("' ");    			
    		}
    		if(_iCond == 3)
    		{
        		oSQL.append(" AND ip.approval_no ILIKE '").append(_sKeywords).append("' ");    			
    		}    		
    	}   
    	oSQL.append(" ORDER BY ip.payment_term_id, ip.bank_issuer ");
    	log.debug("getCCSales SQL " + oSQL.toString());    	
    	return BasePeer.executeQuery(oSQL.toString());    	
    }
    
    //-------------------------------------------------------------------------
    // Time Slot
    //-------------------------------------------------------------------------
        
    /**
     * 
     * @param _dStart
     * @param _iCondition
     * @param _sKeywords
     * @param _sLocationID
     * @param _sKategoriID
     * @param _sVendorID
     * @param _sEmployeeID
     * @param _sCashier
     * @param _sColor
     * @param _bIncTax
     * @param _bDiscount
     * @param _iSortBy
     * @param _iMinute
     * @return
     * @throws Exception
     */
    public static List getSalesByTimeSlot (Date _dStart,   
    									   int _iCondition,
    									   String _sKeywords,
    									   String _sLocationID, 											 
    									   String _sKategoriID, 
    									   String _sVendorID,
    									   String _sEmployeeID,
    									   String _sCashier,
    									   String _sColor,
    									   boolean _bIncTax,
    									   boolean _bDiscount,
    									   int _iSortBy,
    									   int _iMinute) 
    	throws Exception
   {		
    	int iSecs = _iMinute * 60;
    	StringBuilder oSQL = new StringBuilder ();
    	oSQL.append ("SELECT ts_round(transaction_date,").append(iSecs).append(" ) AS TSLOT, SUM(sd.qty_base) AS qty, ");

    	SalesAnalysisTool.sumDetailAmount(oSQL,_bIncTax);
    	oSQL.append (" AS amt, ");

    	oSQL.append (" SUM(sd.sub_total_cost) AS cost, COUNT(DISTINCT s.sales_transaction_id) AS no_trans, ");
    	oSQL.append (" SUM(sd.sub_total_tax) AS tax ");

    	if (_bDiscount) //include discount
    	{
    		oSQL.append(",SUM(sd.sub_total_disc), sd.discount ");
    	}

    	oSQL.append (" FROM sales_transaction s, ");
    	oSQL.append (" sales_transaction_detail sd, ");
    	oSQL.append (" kategori k, item i  WHERE ");

    	Date dEnd = DateUtil.getEndOfDayDate(_dStart);    	
    	SalesAnalysisTool.buildStdQuery(oSQL, _dStart, dEnd, i_PROCESSED, _sLocationID, "", "");

    	oSQL.append ("  AND s.sales_transaction_id = sd.sales_transaction_id ");
    	oSQL.append ("  AND sd.item_id = i.item_id ");
    	oSQL.append ("  AND i.kategori_id = k.kategori_id ");

    	if (_iCondition > 0 && StringUtil.isNotEmpty(_sKeywords))
    	{
    		String sCol = "i.item_code";
    		if(_iCondition == 2) sCol = "i.barcode";
    		if(_iCondition == 3) sCol = "i.item_name";
    		
    		oSQL.append ("  AND ").append(sCol).append(" ILIKE '");
    		oSQL.append (_sKeywords);
    		oSQL.append ("'");
    	}    	
    	else
    	{
    		if (StringUtil.isNotEmpty(_sKategoriID))
    		{		
    			List vChildID = KategoriTool.getChildIDList(_sKategoriID);

    			if (vChildID.size() > 0)
    			{
    				vChildID.add (_sKategoriID);
    				oSQL.append ("  AND k.kategori_id IN ");
    				oSQL.append (SqlUtil.convertToINMode(vChildID));
    			}
    			else 
    			{
    				oSQL.append ("  AND k.kategori_id = '");
    				oSQL.append (_sKategoriID);
    				oSQL.append ("'");
    			}
    		}  
    	}
    	if (StringUtil.isNotEmpty(_sEmployeeID))
    	{
    		oSQL.append ("  AND s.sales_id = '");
    		oSQL.append (_sEmployeeID);
    		oSQL.append ("'");
    	}    	
    	if (StringUtil.isNotEmpty(_sCashier))
    	{
    		oSQL.append ("  AND s.cashier_name = '");
    		oSQL.append (_sCashier);
    		oSQL.append ("'");
    	}

    	if (StringUtil.isNotEmpty(_sVendorID))
    	{
    		oSQL.append ("  AND i.prefered_vendor_id = '");
    		oSQL.append (_sVendorID);
    		oSQL.append ("'");
    	}        
    	if (StringUtil.isNotEmpty(_sColor))
    	{
    		oSQL.append ("  AND i.color = '");
    		oSQL.append (_sColor);
    		oSQL.append ("'");
    	}
    	

    	oSQL.append (" GROUP BY TSLOT, s.is_inclusive_tax ");

    	if (_bDiscount) //include discount
    	{
    		oSQL.append(" , sd.discount ");
    	}

    	System.out.println(oSQL.toString());
    	log.debug (oSQL.toString());
    	return SalesTransactionPeer.executeQuery(oSQL.toString());	
    }

    public static List getReturnByTimeSlot (Date _dStart,   
    										int _iCondition,
								    		String _sKeywords,
								    		String _sLocationID, 											 
								    		String _sKategoriID, 
								    		String _sVendorID,
								    		String _sEmployeeID,
								    		String _sUserName,
								    		String _sColor,
								    		boolean _bIncTax,
								    		boolean _bDiscount,
								    		int _iSortBy,
								    		int _iMinute) 
    				throws Exception
    {		
    	int iSecs = _iMinute * 60;
    	StringBuilder oSQL = new StringBuilder ();
    	oSQL.append ("SELECT ts_round(return_date,").append(iSecs).append(" ) AS TSLOT, SUM(sd.qty_base) AS qty, ");

		if (!_bIncTax)
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((sd.return_amount * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
			oSQL.append (" ELSE SUM(sd.return_amount * s.currency_rate) END ");
		}
		else
		{
			oSQL.append (" CASE WHEN s.is_inclusive_tax = 'FALSE' THEN SUM((sd.return_amount * s.currency_rate) + (sd.sub_total_tax * s.fiscal_rate))  ");
			oSQL.append (" ELSE SUM(sd.return_amount * s.currency_rate) END ");
		}

    	oSQL.append (" AS amt, ");

    	oSQL.append (" SUM(sd.sub_total_cost) AS cost, COUNT(DISTINCT s.sales_return_id) AS no_trans, ");
    	oSQL.append (" SUM(sd.sub_total_tax) AS tax ");

    	if (_bDiscount) //include discount
    	{
    		oSQL.append(",SUM(sd.sub_total_disc), sd.discount ");
    	}

    	oSQL.append (" FROM sales_return s, ");
    	oSQL.append (" sales_return_detail sd, ");
    	oSQL.append (" kategori k, item i  WHERE ");

    	Date dEnd = DateUtil.getEndOfDayDate(_dStart);
    	SalesAnalysisTool.buildStdQuery(oSQL, _dStart, dEnd, i_PROCESSED, "s.return_date", _sLocationID, "", "");

    	oSQL.append ("  AND s.sales_return_id = sd.sales_return_id ");
    	oSQL.append ("  AND sd.item_id = i.item_id ");
    	oSQL.append ("  AND i.kategori_id = k.kategori_id ");

    	if (_iCondition > 0 && StringUtil.isNotEmpty(_sKeywords))
    	{
    		String sCol = "i.item_code";
    		if(_iCondition == 2) sCol = "i.barcode";
    		if(_iCondition == 3) sCol = "i.item_name";

    		oSQL.append ("  AND ").append(sCol).append(" ILIKE '");
    		oSQL.append (_sKeywords);
    		oSQL.append ("'");
    	}    	
    	else
    	{
    		if (StringUtil.isNotEmpty(_sKategoriID))
    		{		
    			List vChildID = KategoriTool.getChildIDList(_sKategoriID);

    			if (vChildID.size() > 0)
    			{
    				vChildID.add (_sKategoriID);
    				oSQL.append ("  AND k.kategori_id IN ");
    				oSQL.append (SqlUtil.convertToINMode(vChildID));
    			}
    			else 
    			{
    				oSQL.append ("  AND k.kategori_id = '");
    				oSQL.append (_sKategoriID);
    				oSQL.append ("'");
    			}
    		}  
    	}
    	if (StringUtil.isNotEmpty(_sEmployeeID))
    	{
    		oSQL.append ("  AND s.sales_id = '");
    		oSQL.append (_sEmployeeID);
    		oSQL.append ("'");
    	}    	
    	if (StringUtil.isNotEmpty(_sUserName))
    	{
    		oSQL.append ("  AND s.user_name = '");
    		oSQL.append (_sUserName);
    		oSQL.append ("'");
    	}

    	if (StringUtil.isNotEmpty(_sVendorID))
    	{
    		oSQL.append ("  AND i.prefered_vendor_id = '");
    		oSQL.append (_sVendorID);
    		oSQL.append ("'");
    	}        
    	if (StringUtil.isNotEmpty(_sColor))
    	{
    		oSQL.append ("  AND i.color = '");
    		oSQL.append (_sColor);
    		oSQL.append ("'");
    	}


    	oSQL.append (" GROUP BY TSLOT, s.is_inclusive_tax ");

    	if (_bDiscount) //include discount
    	{
    		oSQL.append(" , sd.discount ");
    	}


    	log.debug (oSQL.toString());
    	return SalesTransactionPeer.executeQuery(oSQL.toString());	
    }
    
    public static Record filterByTS(Date _dStart, List _vTSData)
    	throws Exception
    {
    	if (_vTSData != null && _vTSData.size() > 0)
    	{
    		for (int i = 0;i < _vTSData.size(); i++)
    		{
    			Record oData = (Record) _vTSData.get(i);
    			Date dTS = oData.getValue("tslot").asTimestamp();
    			if (dTS != null && dTS.getTime() == _dStart.getTime())
    			{
    				System.out.println(" Time Slot Found: " + _dStart + "  " + dTS );
    				return oData;    				
    			}
    		}
    	}
    	return null;
    }
        
    //-------------------------------------------------------------------------
    // Promo Reports
    //-------------------------------------------------------------------------

    public static List getDetailByPromo(int _iCond, 
    									int _iDiscountType,
    								    String _sKeywords,
    								    String _sLocID,
    								    String _sPTypeID,
    								    Date _dStart, 
    								    Date _dEnd,
    								    boolean _bCCOnly)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.addJoin(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, SalesTransactionPeer.SALES_TRANSACTION_ID);
        oCrit.addJoin(SalesTransactionDetailPeer.DISCOUNT_ID, DiscountPeer.DISCOUNT_ID);
    	if(_dStart != null)
    	{
    		oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
    	}
    	if(_dEnd != null)
    	{
    		oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
    	}
    	if(StringUtil.isNotEmpty(_sLocID))
    	{
    		oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocID);    		
    	}    
    	if(StringUtil.isNotEmpty(_sPTypeID))
    	{
    		oCrit.add(DiscountPeer.PAYMENT_TYPE_ID, _sPTypeID);
    	}        	
    	if(_iDiscountType > 0)
    	{
        	oCrit.add(DiscountPeer.DISCOUNT_TYPE, _iDiscountType);    		
    	}
    	if(_bCCOnly)
    	{
        	oCrit.add(DiscountPeer.IS_CC_DISC, _bCCOnly);    		    		
    	}
    	if(_iCond > 0 && StringUtil.isNotEmpty(_sKeywords))
    	{

        	if(!StringUtil.contains(_sKeywords, "%")) _sKeywords = "%"+_sKeywords+"%";
        	if(_iCond == 1)
    		{
    			oCrit.add(DiscountPeer.PROMO_CODE, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 2)
    		{
    			oCrit.add(DiscountPeer.DISCOUNT_CODE, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 3)
    		{
    			oCrit.add(DiscountPeer.DESCRIPTION, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 4)
    		{
    			oCrit.add(SalesTransactionDetailPeer.ITEM_CODE, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 5)
    		{
    			oCrit.add(SalesTransactionDetailPeer.ITEM_NAME, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 6)
    		{
    			oCrit.addJoin(SalesTransactionDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
    			oCrit.add(ItemPeer.BARCODE, (Object)_sKeywords, Criteria.ILIKE);
    		}    		    	
    	}
    	return SalesTransactionDetailPeer.doSelect(oCrit);
    }

    public static List getPromoTotal(int _iCond, 
    								 int _iDiscountType,
    								 String _sKeywords,
    								 String _sLocID,
    								 String _sPTypeID,
    								 Date _dStart, 
    								 Date _dEnd,
    								 boolean _bCCOnly)
    	throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.addJoin(SalesTransactionPeer.TOTAL_DISCOUNT_ID, DiscountPeer.DISCOUNT_ID);
    	if(_dStart != null)
    	{
    		oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
    	}
    	if(_dEnd != null)
    	{
    		oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
    	}
    	if(StringUtil.isNotEmpty(_sLocID))
    	{
    		oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocID);    		
    	}    
    	if(StringUtil.isNotEmpty(_sPTypeID))
    	{
    		oCrit.add(DiscountPeer.PAYMENT_TYPE_ID, _sPTypeID);
    	}        	
    	if(_iDiscountType > 0)
    	{
    		oCrit.add(DiscountPeer.DISCOUNT_TYPE, _iDiscountType);    		
    	}
    	if(_bCCOnly)
    	{
    		oCrit.add(DiscountPeer.IS_CC_DISC, _bCCOnly);    		    		
    	}
    	if(_iCond > 0 && StringUtil.isNotEmpty(_sKeywords))
    	{

    		if(!StringUtil.contains(_sKeywords, "%")) _sKeywords = "%"+_sKeywords+"%";
    		if(_iCond == 1)
    		{
    			oCrit.add(DiscountPeer.PROMO_CODE, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 2)
    		{
    			oCrit.add(DiscountPeer.DISCOUNT_CODE, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 3)
    		{
    			oCrit.add(DiscountPeer.DESCRIPTION, (Object)_sKeywords, Criteria.ILIKE);
    		}    		    		    	
    	}
    	return SalesTransactionPeer.doSelect(oCrit);
    }
    
    public static List getDetailBySpcDisc(int _iCond, 
    									  String _sKeywords, 
    									  String _sLocID, 
    									  String _sPTypeID,
    									  Date _dStart, 
    									  Date _dEnd) throws Exception
    {
    	Criteria oCrit = new Criteria();
    	oCrit.addJoin(SalesTransactionDetailPeer.SALES_TRANSACTION_ID, SalesTransactionPeer.SALES_TRANSACTION_ID);
    	if(_dStart != null)
    	{
    		oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
    	}
    	if(_dEnd != null)
    	{
    		oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
    	}
    	if(StringUtil.isNotEmpty(_sLocID))
    	{
    		oCrit.add(SalesTransactionPeer.LOCATION_ID, _sLocID);    		
    	}    
    	if(StringUtil.isNotEmpty(_sPTypeID))
    	{
    		oCrit.add(SalesTransactionPeer.PAYMENT_TYPE_ID, _sPTypeID);    		
    	}        	
    	oCrit.add(SalesTransactionDetailPeer.DISCOUNT_ID, PreferenceTool.posSpcDiscCode());
    	if(_iCond > 0 && StringUtil.isNotEmpty(_sKeywords))
    	{

    		if(!StringUtil.contains(_sKeywords, "%")) _sKeywords = "%"+_sKeywords+"%";
    		if(_iCond == 4)
    		{
    			oCrit.add(SalesTransactionDetailPeer.ITEM_CODE, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 5)
    		{
    			oCrit.add(SalesTransactionDetailPeer.ITEM_NAME, (Object)_sKeywords, Criteria.ILIKE);
    		}
    		if(_iCond == 6)
    		{
    			oCrit.addJoin(SalesTransactionDetailPeer.ITEM_ID, ItemPeer.ITEM_ID);
    			oCrit.add(ItemPeer.BARCODE, (Object)_sKeywords, Criteria.ILIKE);
    		}    		    	
    	}
    	return SalesTransactionDetailPeer.doSelect(oCrit);
    }

    
    //-------------------------------------------------------------------------
    // Item SOLD VIEW Reports
    //-------------------------------------------------------------------------
    public static List getSoldItems (Date _dStart, 
								     Date _dEnd, 
								     String _sLocationID, 											 
								     String _sKategoriID, 
								     String _sItemID,
								     String _sVendorID,
								     String _sEmployeeID,
								     String _sEmpDetID,
								     String _sCashier,
								     String _sColor,
								     int _iSortBy,
								     Date _dSAddDate, //Item Add Date
								     Date _dEAddDate) 
    	throws Exception
    {		
    	StringBuilder oSQL = new StringBuilder ();
    	oSQL.append ("SELECT * ");
    	oSQL.append (" FROM item_sold_view ");
    	oSQL.append (" WHERE ");
    	SalesAnalysisTool.buildDateQuery(oSQL, "transaction_date", _dStart, _dEnd);
    	
    	if (StringUtil.isNotEmpty(_sItemID))
    	{
    		oSQL.append ("  AND item_id = '");
    		oSQL.append (_sItemID);
    		oSQL.append ("'");
    	}
    	else
    	{
    		if (StringUtil.isNotEmpty(_sKategoriID))
    		{		
    			List vChildID = KategoriTool.getChildIDList(_sKategoriID);
    			if (vChildID.size() > 0)
    			{
    				vChildID.add (_sKategoriID);
    				oSQL.append ("  AND kategori_id IN ");
    				oSQL.append (SqlUtil.convertToINMode(vChildID));
    			}
    			else 
    			{
    				oSQL.append ("  AND kategori_id = '");
    				oSQL.append (_sKategoriID);
    				oSQL.append ("'");
    			}
    		}  
    	}
    	if (StringUtil.isNotEmpty(_sLocationID))
    	{
    		oSQL.append ("  AND location_id = '");
    		oSQL.append (_sLocationID);
    		oSQL.append ("'");
    	}
    	if (StringUtil.isNotEmpty(_sEmployeeID))
    	{
    		oSQL.append ("  AND sales_id = '");
    		oSQL.append (_sEmployeeID);
    		oSQL.append ("'");
    	}
    	if (StringUtil.isNotEmpty(_sEmpDetID))
    	{
    		oSQL.append ("  AND employee_id = '");
    		oSQL.append (_sEmpDetID);
    		oSQL.append ("'");
    	}        
    	if (StringUtil.isNotEmpty(_sCashier))
    	{
    		oSQL.append ("  AND cashier_name = '");
    		oSQL.append (_sCashier);
    		oSQL.append ("'");
    	}

    	if (StringUtil.isNotEmpty(_sVendorID))
    	{
    		oSQL.append ("  AND prefered_vendor_id = '");
    		oSQL.append (_sVendorID);
    		oSQL.append ("'");
    	}        
    	if (StringUtil.isNotEmpty(_sColor))
    	{
    		oSQL.append ("  AND color = '");
    		oSQL.append (_sColor);
    		oSQL.append ("'");
    	}
    	if (_dSAddDate != null)
    	{
    		oSQL.append ("  AND add_date >= '");
    		oSQL.append (CustomFormatter.formatSqlDate(_dSAddDate));
    		oSQL.append ("'");
    	}
    	if (_dEAddDate != null)
    	{
    		oSQL.append ("  AND add_date <= '");
    		oSQL.append (CustomFormatter.formatSqlDate(_dEAddDate));
    		oSQL.append ("'");
    	}

    	if(_iSortBy == i_DET_ORDER_BY_CODE)
    	{
    		oSQL.append(" ORDER BY item_code ");
    	}
    	else if(_iSortBy == i_DET_ORDER_BY_NAME)
    	{
    		oSQL.append(" ORDER BY item_name ");
    	}

    	log.debug (oSQL.toString());
    	return BasePeer.executeQuery(oSQL.toString());	
    }	
    
    public static double sumSoldItemsField(List _vSoldItems, String _sSumBy, String _sSumVal, String _sField) throws Exception
	{
		double dTotal = 0;
		if (_vSoldItems != null)
		{			
			for (int i = 0; i < _vSoldItems.size(); i++)
			{
				Record oRecord = (Record)_vSoldItems.get(i);
				String sVal = _sSumVal;
				String sCol = _sSumBy;
				String sVals = _sSumVal;
				if (StringUtil.isEqual(_sSumBy,"kategori_id")) //if kategori
				{
					List vChild = KategoriTool.getChildIDList(_sSumVal);
					vChild.add(_sSumVal);
					sVals = SqlUtil.convertToINMode(vChild);
				}				
				String sValue = oRecord.getValue(sCol).asString();
				if(StringUtil.isEqual(sValue, sVal) || StringUtil.contains(sVals, sVal))
				{				
					dTotal += oRecord.getValue(_sField).asDouble();									
				}				
			}
		}
		return dTotal;
	}           
   
    //-------------------------------------------------------------------------
    // CAshierBalance
    //-------------------------------------------------------------------------
    public static CashierBalanceTool getCashierBalanceTool()
    {
    	return CashierBalanceTool.getInstance();
    }         
    
    //
    // Sales Order
    //
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _sDepartmentID
	 * @param _sProjectID
	 * @param _sCustomerTypeID
	 * @param _sEmployeeID
	 * @param _sCashier
	 * @param _bIncTax
	 * @param _bDiscount
	 * @param _iStatus
	 * @param _iSortBy
	 * @return
	 * @throws Exception
	 */
	public static List getEffectiveCall (Date _dStart, 
										 Date _dEnd, 
										 List _vSalesID,
										 String _sLocationID, 											 
										 String _sDepartmentID,
										 String _sProjectID,
										 String _sAreaID,
										 String _sTerrID)
		throws Exception
	{				
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT  ");
		oSQL.append (" s.sales_id, ");
		oSQL.append (" COUNT(s.sales_order_id), ");		
		oSQL.append (" SUM(s.total_qty), ");
		//oSQL.append (" SUM(s.total_cost), ");

		SalesAnalysisTool.sumTotalAmount(oSQL,false);
	
		oSQL.append (" FROM sales_order s ");
		oSQL.append (" WHERE ");
		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, -2, _sLocationID, _sDepartmentID, _sProjectID);		
		if(_vSalesID != null)
		{
			oSQL.append(" AND s.sales_id IN ").append(SqlUtil.convertToINMode(_vSalesID));
		}		
		oSQL.append(" AND transaction_status IN ( ")
		    .append(i_SO_ORDERED).append(",")
		    .append(i_SO_DELIVERED).append(",")
		    .append(i_SO_CLOSED).append(") ");
		
		
		oSQL.append (" GROUP BY s.sales_id, s.is_inclusive_tax ");		
		log.debug ("getEffCall SQL " + oSQL.toString());
		return SalesOrderPeer.executeQuery(oSQL.toString());	
	}	  
	
	//
	public static List getSalesAndReturn(Date _dStart, Date _dEnd, String _sCustomerID, String _sLocationID)
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder();
		oSQL.append(" SELECT                            ")
		    .append("   tr.sales_transaction_id AS trid,")		  
		    .append("   tr.invoice_no,                  ")
		    .append("   tr.sales_id,                    ")
		    .append("   tr.customer_id,                 ")
		    .append("   tr.location_id,                 ")
		    .append("   tr.total_qty AS qty,            ")
		    .append("   tr.transaction_date AS trdate,  ")
		    .append("   tr.total_amount * tr.currency_rate AS amt,  ")
		    .append("   tr.total_tax * tr.fiscal_rate AS tax, ")
		    .append("   sr.return_no AS retno,         ")
		    .append("   sr.return_date AS retdate,     ")
		    .append("   sr.total_tax AS rettax,        ")
		    .append("   sr.total_amount AS rettamt,    ")
		    .append("   sr.returned_amount AS retamt,   ")
		    .append("   cm.amount * cm.currency_rate AS clsamt ")		    
		    .append(" FROM sales_transaction tr,       ")
		    .append("      sales_return sr LEFT OUTER JOIN credit_memo cm ON (cm.transaction_id = sr.sales_return_id) ")    
		    .append(" WHERE                ")
		    .append("   tr.sales_transaction_id = sr.transaction_id ")    
		    .append("   AND tr.status = 2  ")
		    .append("   AND sr.status = 2  ")
		    .append("   AND cm.status = 1  ")
		    
		    ;
		if(_dStart != null) oSQL.append(" AND tr.transaction_date >= '").append(CustomFormatter.formatSqlDate(_dStart)).append("'");
		if(_dEnd != null)   oSQL.append(" AND tr.transaction_date <= '").append(CustomFormatter.formatSqlDate(_dEnd)).append("'");
		if(StringUtil.isNotEmpty(_sCustomerID)) oSQL.append(" AND tr.customer_id = '").append(_sCustomerID).append("'");
		log.debug(oSQL.toString());
		return SqlUtil.executeQuery(oSQL.toString());
	}
}
package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to GlTransactionLog
 */
public abstract class BaseGlTransactionLog extends BaseObject
{
    /** The Peer class */
    private static final GlTransactionLogPeer peer =
        new GlTransactionLogPeer();

        
    /** The value for the glTransactionId field */
    private String glTransactionId;
      
    /** The value for the transactionType field */
    private int transactionType;
      
    /** The value for the transactionId field */
    private String transactionId;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the glTransactionNo field */
    private String glTransactionNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the projectId field */
    private String projectId;
      
    /** The value for the departmentId field */
    private String departmentId;
      
    /** The value for the periodId field */
    private String periodId;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
                                                
    /** The value for the description field */
    private String description = "";
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the debitCredit field */
    private int debitCredit;
      
    /** The value for the createDate field */
    private Date createDate;
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                          
    /** The value for the subLedgerType field */
    private int subLedgerType = 0;
                                                
    /** The value for the subLedgerId field */
    private String subLedgerId = "";
      
    /** The value for the updateDate field */
    private Date updateDate;
  
    
    /**
     * Get the GlTransactionId
     *
     * @return String
     */
    public String getGlTransactionId()
    {
        return glTransactionId;
    }

                        
    /**
     * Set the value of GlTransactionId
     *
     * @param v new value
     */
    public void setGlTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.glTransactionId, v))
              {
            this.glTransactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the GlTransactionNo
     *
     * @return String
     */
    public String getGlTransactionNo()
    {
        return glTransactionNo;
    }

                        
    /**
     * Set the value of GlTransactionNo
     *
     * @param v new value
     */
    public void setGlTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.glTransactionNo, v))
              {
            this.glTransactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PeriodId
     *
     * @return String
     */
    public String getPeriodId()
    {
        return periodId;
    }

                        
    /**
     * Set the value of PeriodId
     *
     * @param v new value
     */
    public void setPeriodId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.periodId, v))
              {
            this.periodId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DebitCredit
     *
     * @return int
     */
    public int getDebitCredit()
    {
        return debitCredit;
    }

                        
    /**
     * Set the value of DebitCredit
     *
     * @param v new value
     */
    public void setDebitCredit(int v) 
    {
    
                  if (this.debitCredit != v)
              {
            this.debitCredit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerType
     *
     * @return int
     */
    public int getSubLedgerType()
    {
        return subLedgerType;
    }

                        
    /**
     * Set the value of SubLedgerType
     *
     * @param v new value
     */
    public void setSubLedgerType(int v) 
    {
    
                  if (this.subLedgerType != v)
              {
            this.subLedgerType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerId
     *
     * @return String
     */
    public String getSubLedgerId()
    {
        return subLedgerId;
    }

                        
    /**
     * Set the value of SubLedgerId
     *
     * @param v new value
     */
    public void setSubLedgerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.subLedgerId, v))
              {
            this.subLedgerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("GlTransactionId");
              fieldNames.add("TransactionType");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("GlTransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("ProjectId");
              fieldNames.add("DepartmentId");
              fieldNames.add("PeriodId");
              fieldNames.add("AccountId");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("Amount");
              fieldNames.add("AmountBase");
              fieldNames.add("Description");
              fieldNames.add("UserName");
              fieldNames.add("DebitCredit");
              fieldNames.add("CreateDate");
              fieldNames.add("LocationId");
              fieldNames.add("SubLedgerType");
              fieldNames.add("SubLedgerId");
              fieldNames.add("UpdateDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("GlTransactionId"))
        {
                return getGlTransactionId();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("GlTransactionNo"))
        {
                return getGlTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("PeriodId"))
        {
                return getPeriodId();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("DebitCredit"))
        {
                return Integer.valueOf(getDebitCredit());
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("SubLedgerType"))
        {
                return Integer.valueOf(getSubLedgerType());
            }
          if (name.equals("SubLedgerId"))
        {
                return getSubLedgerId();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(GlTransactionLogPeer.GL_TRANSACTION_ID))
        {
                return getGlTransactionId();
            }
          if (name.equals(GlTransactionLogPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(GlTransactionLogPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(GlTransactionLogPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(GlTransactionLogPeer.GL_TRANSACTION_NO))
        {
                return getGlTransactionNo();
            }
          if (name.equals(GlTransactionLogPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(GlTransactionLogPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(GlTransactionLogPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(GlTransactionLogPeer.PERIOD_ID))
        {
                return getPeriodId();
            }
          if (name.equals(GlTransactionLogPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(GlTransactionLogPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(GlTransactionLogPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(GlTransactionLogPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(GlTransactionLogPeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(GlTransactionLogPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(GlTransactionLogPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(GlTransactionLogPeer.DEBIT_CREDIT))
        {
                return Integer.valueOf(getDebitCredit());
            }
          if (name.equals(GlTransactionLogPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(GlTransactionLogPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(GlTransactionLogPeer.SUB_LEDGER_TYPE))
        {
                return Integer.valueOf(getSubLedgerType());
            }
          if (name.equals(GlTransactionLogPeer.SUB_LEDGER_ID))
        {
                return getSubLedgerId();
            }
          if (name.equals(GlTransactionLogPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getGlTransactionId();
            }
              if (pos == 1)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 2)
        {
                return getTransactionId();
            }
              if (pos == 3)
        {
                return getTransactionNo();
            }
              if (pos == 4)
        {
                return getGlTransactionNo();
            }
              if (pos == 5)
        {
                return getTransactionDate();
            }
              if (pos == 6)
        {
                return getProjectId();
            }
              if (pos == 7)
        {
                return getDepartmentId();
            }
              if (pos == 8)
        {
                return getPeriodId();
            }
              if (pos == 9)
        {
                return getAccountId();
            }
              if (pos == 10)
        {
                return getCurrencyId();
            }
              if (pos == 11)
        {
                return getCurrencyRate();
            }
              if (pos == 12)
        {
                return getAmount();
            }
              if (pos == 13)
        {
                return getAmountBase();
            }
              if (pos == 14)
        {
                return getDescription();
            }
              if (pos == 15)
        {
                return getUserName();
            }
              if (pos == 16)
        {
                return Integer.valueOf(getDebitCredit());
            }
              if (pos == 17)
        {
                return getCreateDate();
            }
              if (pos == 18)
        {
                return getLocationId();
            }
              if (pos == 19)
        {
                return Integer.valueOf(getSubLedgerType());
            }
              if (pos == 20)
        {
                return getSubLedgerId();
            }
              if (pos == 21)
        {
                return getUpdateDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(GlTransactionLogPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        GlTransactionLogPeer.doInsert((GlTransactionLog) this, con);
                        setNew(false);
                    }
                    else
                    {
                        GlTransactionLogPeer.doUpdate((GlTransactionLog) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key glTransactionId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setGlTransactionId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setGlTransactionId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getGlTransactionId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public GlTransactionLog copy() throws TorqueException
    {
        return copyInto(new GlTransactionLog());
    }
  
    protected GlTransactionLog copyInto(GlTransactionLog copyObj) throws TorqueException
    {
          copyObj.setGlTransactionId(glTransactionId);
          copyObj.setTransactionType(transactionType);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setGlTransactionNo(glTransactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setProjectId(projectId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setPeriodId(periodId);
          copyObj.setAccountId(accountId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setAmount(amount);
          copyObj.setAmountBase(amountBase);
          copyObj.setDescription(description);
          copyObj.setUserName(userName);
          copyObj.setDebitCredit(debitCredit);
          copyObj.setCreateDate(createDate);
          copyObj.setLocationId(locationId);
          copyObj.setSubLedgerType(subLedgerType);
          copyObj.setSubLedgerId(subLedgerId);
          copyObj.setUpdateDate(updateDate);
  
                    copyObj.setGlTransactionId((String)null);
                                                                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public GlTransactionLogPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("GlTransactionLog\n");
        str.append("----------------\n")
           .append("GlTransactionId      : ")
           .append(getGlTransactionId())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("GlTransactionNo      : ")
           .append(getGlTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("PeriodId             : ")
           .append(getPeriodId())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("DebitCredit          : ")
           .append(getDebitCredit())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("SubLedgerType        : ")
           .append(getSubLedgerType())
           .append("\n")
           .append("SubLedgerId          : ")
           .append(getSubLedgerId())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

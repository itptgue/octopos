package com.ssti.enterprise.pos.presentation.screens.report.sales;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.presentation.screens.report.ReportSecureScreen;
import com.ssti.enterprise.pos.tools.report.ReportTool;
import com.ssti.enterprise.pos.tools.sales.VoidTransTool;
import com.ssti.framework.tools.CustomParser;

public class CashierVoidReport extends ReportSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    	    if(data.getParameters().getInt("op") == 1)
    	    {
	    	    Date dStart = CustomParser.parseDate(data.getParameters().getString("StartDate"));
	    	    Date dEnd = CustomParser.parseDate(data.getParameters().getString("EndDate"));
	    	    String sCashier = data.getParameters().getString ("CashierName", "");
	    	    String sLocID = data.getParameters().getString ("LocationId", "");
                
	    	    if (dStart == null)dStart = new Date();
	    	    if (dEnd == null) dEnd = new Date();			    
	    	    
	    	    List vData = VoidTransTool.findVoid(dStart, dEnd, sCashier);
	    	    Map mData = ReportTool.toMap (vData, "cashierName");
	    	    context.put("vData", vData);
	    	    context.put("mData", mData);
	    	 }
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Sales Report LookUp Failed : " + _oEx.getMessage());
    	}    
    	context.put("voidtranstool", VoidTransTool.getInstance());
    }
}

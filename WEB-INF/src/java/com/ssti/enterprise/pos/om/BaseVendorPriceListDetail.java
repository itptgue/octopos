package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to VendorPriceListDetail
 */
public abstract class BaseVendorPriceListDetail extends BaseObject
{
    /** The Peer class */
    private static final VendorPriceListDetailPeer peer =
        new VendorPriceListDetailPeer();

        
    /** The value for the vendorPriceListDetailId field */
    private String vendorPriceListDetailId;
      
    /** The value for the vendorPriceListId field */
    private String vendorPriceListId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the itemName field */
    private String itemName;
      
    /** The value for the purchasePrice field */
    private BigDecimal purchasePrice;
                                                
    /** The value for the discountAmount field */
    private String discountAmount = "0";
  
    
    /**
     * Get the VendorPriceListDetailId
     *
     * @return String
     */
    public String getVendorPriceListDetailId()
    {
        return vendorPriceListDetailId;
    }

                        
    /**
     * Set the value of VendorPriceListDetailId
     *
     * @param v new value
     */
    public void setVendorPriceListDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorPriceListDetailId, v))
              {
            this.vendorPriceListDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorPriceListId
     *
     * @return String
     */
    public String getVendorPriceListId()
    {
        return vendorPriceListId;
    }

                              
    /**
     * Set the value of VendorPriceListId
     *
     * @param v new value
     */
    public void setVendorPriceListId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.vendorPriceListId, v))
              {
            this.vendorPriceListId = v;
            setModified(true);
        }
    
                          
                if (aVendorPriceList != null && !ObjectUtils.equals(aVendorPriceList.getVendorPriceListId(), v))
                {
            aVendorPriceList = null;
        }
      
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemName
     *
     * @return String
     */
    public String getItemName()
    {
        return itemName;
    }

                        
    /**
     * Set the value of ItemName
     *
     * @param v new value
     */
    public void setItemName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemName, v))
              {
            this.itemName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PurchasePrice
     *
     * @return BigDecimal
     */
    public BigDecimal getPurchasePrice()
    {
        return purchasePrice;
    }

                        
    /**
     * Set the value of PurchasePrice
     *
     * @param v new value
     */
    public void setPurchasePrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.purchasePrice, v))
              {
            this.purchasePrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DiscountAmount
     *
     * @return String
     */
    public String getDiscountAmount()
    {
        return discountAmount;
    }

                        
    /**
     * Set the value of DiscountAmount
     *
     * @param v new value
     */
    public void setDiscountAmount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discountAmount, v))
              {
            this.discountAmount = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private VendorPriceList aVendorPriceList;

    /**
     * Declares an association between this object and a VendorPriceList object
     *
     * @param v VendorPriceList
     * @throws TorqueException
     */
    public void setVendorPriceList(VendorPriceList v) throws TorqueException
    {
            if (v == null)
        {
                  setVendorPriceListId((String) null);
              }
        else
        {
            setVendorPriceListId(v.getVendorPriceListId());
        }
            aVendorPriceList = v;
    }

                                            
    /**
     * Get the associated VendorPriceList object
     *
     * @return the associated VendorPriceList object
     * @throws TorqueException
     */
    public VendorPriceList getVendorPriceList() throws TorqueException
    {
        if (aVendorPriceList == null && (!ObjectUtils.equals(this.vendorPriceListId, null)))
        {
                          aVendorPriceList = VendorPriceListPeer.retrieveByPK(SimpleKey.keyFor(this.vendorPriceListId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               VendorPriceList obj = VendorPriceListPeer.retrieveByPK(this.vendorPriceListId);
               obj.addVendorPriceListDetails(this);
            */
        }
        return aVendorPriceList;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setVendorPriceListKey(ObjectKey key) throws TorqueException
    {
      
                        setVendorPriceListId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VendorPriceListDetailId");
              fieldNames.add("VendorPriceListId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("ItemName");
              fieldNames.add("PurchasePrice");
              fieldNames.add("DiscountAmount");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VendorPriceListDetailId"))
        {
                return getVendorPriceListDetailId();
            }
          if (name.equals("VendorPriceListId"))
        {
                return getVendorPriceListId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("ItemName"))
        {
                return getItemName();
            }
          if (name.equals("PurchasePrice"))
        {
                return getPurchasePrice();
            }
          if (name.equals("DiscountAmount"))
        {
                return getDiscountAmount();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_DETAIL_ID))
        {
                return getVendorPriceListDetailId();
            }
          if (name.equals(VendorPriceListDetailPeer.VENDOR_PRICE_LIST_ID))
        {
                return getVendorPriceListId();
            }
          if (name.equals(VendorPriceListDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(VendorPriceListDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(VendorPriceListDetailPeer.ITEM_NAME))
        {
                return getItemName();
            }
          if (name.equals(VendorPriceListDetailPeer.PURCHASE_PRICE))
        {
                return getPurchasePrice();
            }
          if (name.equals(VendorPriceListDetailPeer.DISCOUNT_AMOUNT))
        {
                return getDiscountAmount();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVendorPriceListDetailId();
            }
              if (pos == 1)
        {
                return getVendorPriceListId();
            }
              if (pos == 2)
        {
                return getItemId();
            }
              if (pos == 3)
        {
                return getItemCode();
            }
              if (pos == 4)
        {
                return getItemName();
            }
              if (pos == 5)
        {
                return getPurchasePrice();
            }
              if (pos == 6)
        {
                return getDiscountAmount();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VendorPriceListDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VendorPriceListDetailPeer.doInsert((VendorPriceListDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VendorPriceListDetailPeer.doUpdate((VendorPriceListDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key vendorPriceListDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setVendorPriceListDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setVendorPriceListDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVendorPriceListDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public VendorPriceListDetail copy() throws TorqueException
    {
        return copyInto(new VendorPriceListDetail());
    }
  
    protected VendorPriceListDetail copyInto(VendorPriceListDetail copyObj) throws TorqueException
    {
          copyObj.setVendorPriceListDetailId(vendorPriceListDetailId);
          copyObj.setVendorPriceListId(vendorPriceListId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setItemName(itemName);
          copyObj.setPurchasePrice(purchasePrice);
          copyObj.setDiscountAmount(discountAmount);
  
                    copyObj.setVendorPriceListDetailId((String)null);
                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VendorPriceListDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("VendorPriceListDetail\n");
        str.append("---------------------\n")
            .append("VendorPriceListDetailId   : ")
           .append(getVendorPriceListDetailId())
           .append("\n")
           .append("VendorPriceListId    : ")
           .append(getVendorPriceListId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("ItemName             : ")
           .append(getItemName())
           .append("\n")
           .append("PurchasePrice        : ")
           .append(getPurchasePrice())
           .append("\n")
           .append("DiscountAmount       : ")
           .append(getDiscountAmount())
           .append("\n")
        ;
        return(str.toString());
    }
}

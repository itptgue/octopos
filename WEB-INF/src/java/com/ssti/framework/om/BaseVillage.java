package com.ssti.framework.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Village
 */
public abstract class BaseVillage extends BaseObject
{
    /** The Peer class */
    private static final VillagePeer peer =
        new VillagePeer();

        
    /** The value for the villageId field */
    private String villageId;
      
    /** The value for the districtId field */
    private String districtId;
      
    /** The value for the village field */
    private String village;
  
    
    /**
     * Get the VillageId
     *
     * @return String
     */
    public String getVillageId()
    {
        return villageId;
    }

                        
    /**
     * Set the value of VillageId
     *
     * @param v new value
     */
    public void setVillageId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.villageId, v))
              {
            this.villageId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DistrictId
     *
     * @return String
     */
    public String getDistrictId()
    {
        return districtId;
    }

                              
    /**
     * Set the value of DistrictId
     *
     * @param v new value
     */
    public void setDistrictId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.districtId, v))
              {
            this.districtId = v;
            setModified(true);
        }
    
                          
                if (aDistrict != null && !ObjectUtils.equals(aDistrict.getDistrictId(), v))
                {
            aDistrict = null;
        }
      
              }
  
    /**
     * Get the Village
     *
     * @return String
     */
    public String getVillage()
    {
        return village;
    }

                        
    /**
     * Set the value of Village
     *
     * @param v new value
     */
    public void setVillage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.village, v))
              {
            this.village = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private District aDistrict;

    /**
     * Declares an association between this object and a District object
     *
     * @param v District
     * @throws TorqueException
     */
    public void setDistrict(District v) throws TorqueException
    {
            if (v == null)
        {
                  setDistrictId((String) null);
              }
        else
        {
            setDistrictId(v.getDistrictId());
        }
            aDistrict = v;
    }

                                            
    /**
     * Get the associated District object
     *
     * @return the associated District object
     * @throws TorqueException
     */
    public District getDistrict() throws TorqueException
    {
        if (aDistrict == null && (!ObjectUtils.equals(this.districtId, null)))
        {
                          aDistrict = DistrictPeer.retrieveByPK(SimpleKey.keyFor(this.districtId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               District obj = DistrictPeer.retrieveByPK(this.districtId);
               obj.addVillages(this);
            */
        }
        return aDistrict;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setDistrictKey(ObjectKey key) throws TorqueException
    {
      
                        setDistrictId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VillageId");
              fieldNames.add("DistrictId");
              fieldNames.add("Village");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VillageId"))
        {
                return getVillageId();
            }
          if (name.equals("DistrictId"))
        {
                return getDistrictId();
            }
          if (name.equals("Village"))
        {
                return getVillage();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VillagePeer.VILLAGE_ID))
        {
                return getVillageId();
            }
          if (name.equals(VillagePeer.DISTRICT_ID))
        {
                return getDistrictId();
            }
          if (name.equals(VillagePeer.VILLAGE))
        {
                return getVillage();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVillageId();
            }
              if (pos == 1)
        {
                return getDistrictId();
            }
              if (pos == 2)
        {
                return getVillage();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VillagePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VillagePeer.doInsert((Village) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VillagePeer.doUpdate((Village) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key villageId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setVillageId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setVillageId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVillageId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Village copy() throws TorqueException
    {
        return copyInto(new Village());
    }
  
    protected Village copyInto(Village copyObj) throws TorqueException
    {
          copyObj.setVillageId(villageId);
          copyObj.setDistrictId(districtId);
          copyObj.setVillage(village);
  
                    copyObj.setVillageId((String)null);
                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VillagePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Village\n");
        str.append("-------\n")
           .append("VillageId            : ")
           .append(getVillageId())
           .append("\n")
           .append("DistrictId           : ")
           .append(getDistrictId())
           .append("\n")
           .append("Village              : ")
           .append(getVillage())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.excel.helper.transaction;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.JournalVoucher;
import com.ssti.enterprise.pos.om.JournalVoucherDetail;
import com.ssti.enterprise.pos.om.JournalVoucherPeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author$ <br>
 * @version $Id$ <br>
 *
 * <pre>
 * $Log$
 * Revision 1.6  2009/05/04 01:38:37  albert
 * *** empty log message ***
 *
 * Revision 1.5  2008/08/17 02:17:44  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/04/18 06:46:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class JournalVoucherCustomLoader extends TransactionLoader
{    
    private static Log log = LogFactory.getLog (JournalVoucherCustomLoader.class);
    private static final String s_START_HEADING = "nopk"; 
    
    public JournalVoucherCustomLoader ()
    {
    }
    
    public JournalVoucherCustomLoader (String _sUserName)
    {
        m_sUserName = _sUserName + " from (Excel)";
    }
    
    Currency defCurr = null;
    HSSFSheet oSheet = null;
    JournalVoucher oTR = null;
    List vTD = null;
    
    String sTransNo = "";
    /**
     * load data from input stream
     * 
     * @param _oInput
     * @throws Exception
     */
    public void loadData (InputStream _oInput)  
        throws Exception
    {
        POIFSFileSystem oPOI  = new POIFSFileSystem(_oInput);
        HSSFWorkbook oWB = new HSSFWorkbook(oPOI);
        oSheet = oWB.getSheetAt(0);
        m_iTotalRows = oSheet.getPhysicalNumberOfRows();
        boolean bTransHeader = false;
        defCurr = CurrencyTool.getDefaultCurrency();
        
        for (int iRow = 1; iRow < m_iTotalRows; iRow++)
        {
            HSSFRow oRow = oSheet.getRow(iRow);
            if (oRow != null)
            {
                sTransNo = getString(oRow, i_START);
                if (StringUtil.isNotEmpty(sTransNo))
                {                   
                    log.debug("* TRANS NO: " + sTransNo);               

                    m_bTransError = false;
                    m_iTotalTrans++;

                    oTR = new JournalVoucher();
                    vTD = new ArrayList();

                    mapTrans (oRow, oTR, vTD);
                    
                    log.debug("* oTR " + oTR);
                    log.debug("* vTD " + vTD);
                    
                    if (!m_bTransError)
                    {
                        try
                        {
                            JournalVoucherTool.setHeaderProperties(oTR, vTD, null, true);
                            JournalVoucherTool.saveData(oTR, vTD);
                            logSuccess();
                        }
                        catch (Exception _oEx)
                        {
                            String sMsg = _oEx.getMessage();
                            log.error(_oEx);
                            logSaveError(sMsg);
                        }
                    }
                    else
                    {
                        logReject();
                    }
                    m_bTransError = false;
                }
                else 
                {
                    logInvalidFile();
                    return;
                }
            }
        } 
    }   
    
    protected void mapTrans (HSSFRow row, JournalVoucher jv, List vDet)
        throws Exception
    {
        /*
         * nopk 
         * kodecbg 
         * nama    
         * type    
         * namabrg 
         * tgljual 
         * ph  
         * adm 
         * premiass    
         * premiakr    
         * refundass   
         * refundadm   
         * bsurvey 
         * potstnk
         * pencairan
         * 

nopk	
kodecbg	
tgljual	
nama	
merk	
tipe	
tahun	
tenor	
ph	
admin	
provisi	
premiakr	
refundass	
refundadm	
bsurvey	
potstnk	
byrpok	
byrbung	
adm2	
provisi2	
bsurvey2	
Potlunas	
pencairan

         */
        int col = 0;
        String nopk = getString (row, col); col++;
        String lcod = getString (row, col); col++;
        String tglj = getString (row, col); col++;
        String nama = getString (row, col); col++;
        String merk = getString (row, col); col++;
        String tipe = getString (row, col); col++;
        String tahun = getString (row, col); col++;
        
        BigDecimal tenor = getBigDecimal(row, col); col++;

        System.out.println("nopk " + nopk);
        System.out.println("lcod " + lcod);
        System.out.println("nama " + nama);
        System.out.println("tipe " + merk);
        System.out.println("nbrg " + tipe);
        System.out.println("tglj " + tglj);

        sTransNo = nopk;
        if (isExists()) logError("Trans Already Exist", sTransNo);

        BigDecimal ph = getBigDecimal(row, col); col++;
        BigDecimal adm = getBigDecimal(row, col); col++;
        BigDecimal prmass = getBigDecimal(row, col); col++;
        BigDecimal prmakr = getBigDecimal(row, col); col++;
        BigDecimal refass = getBigDecimal(row, col); col++;
        BigDecimal refadm = getBigDecimal(row, col); col++;
        BigDecimal bsurv = getBigDecimal(row, col); col++;
        BigDecimal pstnk = getBigDecimal(row, col); col++;
        
        
//        byrpok	
//        byrbung	
//        adm2	
//        provisi2	
//        bsurvey2	
//        Potlunas
        
        BigDecimal byrpok = getBigDecimal(row, col); col++;
        BigDecimal byrbun = getBigDecimal(row, col); col++;
        BigDecimal badm2 = getBigDecimal(row, col); col++;
        BigDecimal bprov2 = getBigDecimal(row, col); col++;
        BigDecimal bsurv2 = getBigDecimal(row, col); col++;
        BigDecimal potlun = getBigDecimal(row, col); col++;
        
        BigDecimal pencr = getBigDecimal(row, col); col++;
        
        String desc = "Penj a/n " + nama + " " + nopk + " (" + 
                                   merk + " " + tipe + " " + 
                                   tahun + " " + tenor + " bln )";
        
        jv.setTransactionNo(nopk);
        jv.setDescription(desc);
        jv.setTransactionDate(CustomParser.parseDate(tglj));
        jv.setStatus(i_PROCESSED);
        jv.setUserName("");
        

        /*
      Dr.>1.13.01.01.00    Piutang Pembiayaan  1,430,000.00        
      Cr.>1.13.04.01.00           Piutang Amortise - Adm        200,000.00  200,000 
      Cr.>1.13.04.02.00           Piutang Amortise - Provisi        30,000.00        => prmass
      Cr.>4.99.01.05.00           Piutang Amortise - By Srvy        75,000.00        => bsurv
      Cr.>7.10.09.00.00           Hutang Titipan STNK       		300,000.00       => pstnk
      Cr.>2.12.01.00.00           Hutang Dealer     				825,000.00       => pencr
      
      
1.13.01.01.00	Piutang PK-Motor			4,620,000.00 	
	1.13.04.01.00	Piutang Amort PK Motor-Adm			    300,000.00 
	1.13.04.02.00	Piutang Amort PK Motor-Provisi			120,000.00 
	4.99.01.05.00	Piutang Amort PK Motor-By Survey		125,000.00 
	2.12.01.00.00	Hutang Dealer PK-Motor			      2,241,000.00 
	7.10.09.00.00	Hutang Titipan STNK			                  0.00
	 
	1.13.01.01.00	Piutang PK-Motor			            312,636.00 
	4.10.01.00.00	Pendapatan Bunga PK Motor			    171,364.00 
	2.12.01.00.00	Hutang Dealer PK-Motor			      1,350,000.00 				
					
1.13.04.01.00	Piutang Amort PK Motor-Adm			     38,528.00 	
1.13.04.02.00	Piutang Amort PK Motor-Provisi			 15,411.00 	
4.99.01.05.00	Piutang Amort PK Motor-By Survey		 16,053.00 	       4.99.01.05.00
	4.10.02.01.00	Pendapatan Adm Amort PK Motor	            38,528.00 
	4.10.02.02.00	Pendapatan Provisi Amort PK Motor			15,411.00 
	4.99.01.05.00	Pendapatan By Survey Amort PK Motor			16,053.00       
      
         */
        Location oLoc = LocationTool.getLocationByCode(lcod);
        
        JournalVoucherDetail jv1 = new JournalVoucherDetail();
        Account ac1 = AccountTool.getAccountByCode("1.13.01.01.00");
        jv1.setAccountId(ac1.getAccountId());
        jv1.setAccountCode(ac1.getAccountCode());
        jv1.setAccountName(ac1.getAccountName());
        jv1.setAmount(ph);
        jv1.setAmountBase(ph);
        jv1.setCreditAmount(bd_ZERO);
        jv1.setCurrencyId(defCurr.getCurrencyId());
        jv1.setCurrencyRate(bd_ONE);
        jv1.setDebitAmount(ph);
        jv1.setDebitCredit(GlAttributes.i_DEBIT);
        jv1.setLocationId(oLoc.getLocationId());
        jv1.setDepartmentId("");
        jv1.setProjectId("");
        jv1.setReferenceNo("");        
        jv1.setMemo("");

        BigDecimal admf = new BigDecimal(adm.doubleValue() - refadm.doubleValue());
        JournalVoucherDetail jv2 = new JournalVoucherDetail();
        Account ac2 = AccountTool.getAccountByCode("1.13.04.01.00");
        jv2.setAccountId(ac2.getAccountId());
        jv2.setAccountCode(ac2.getAccountCode());
        jv2.setAccountName(ac2.getAccountName());
        jv2.setAmount(admf);
        jv2.setAmountBase(admf);
        jv2.setCreditAmount(admf);
        jv2.setCurrencyId(defCurr.getCurrencyId());
        jv2.setCurrencyRate(bd_ONE);
        jv2.setDebitAmount(bd_ZERO);
        jv2.setDebitCredit(GlAttributes.i_CREDIT);
        jv2.setLocationId(oLoc.getLocationId());
        jv2.setDepartmentId("");
        jv2.setProjectId("");
        jv2.setReferenceNo("");
        jv2.setMemo("");

        JournalVoucherDetail jv3 = new JournalVoucherDetail();
        Account ac3 = AccountTool.getAccountByCode("1.13.04.02.00");
        jv3.setAccountId(ac3.getAccountId());
        jv3.setAccountCode(ac3.getAccountCode());
        jv3.setAccountName(ac3.getAccountName());
        jv3.setAmount(prmass);
        jv3.setAmountBase(prmass);
        jv3.setCreditAmount(prmass);
        jv3.setCurrencyId(defCurr.getCurrencyId());
        jv3.setCurrencyRate(bd_ONE);
        jv3.setDebitAmount(bd_ZERO);
        jv3.setDebitCredit(GlAttributes.i_CREDIT);
        jv3.setLocationId(oLoc.getLocationId());
        jv3.setDepartmentId("");
        jv3.setProjectId("");
        jv3.setReferenceNo("");
        jv3.setMemo("");
        
        JournalVoucherDetail jv4 = new JournalVoucherDetail();
        Account ac4 = AccountTool.getAccountByCode("4.99.01.05.00");
        jv4.setAccountId(ac4.getAccountId());
        jv4.setAccountCode(ac4.getAccountCode());
        jv4.setAccountName(ac4.getAccountName());
        jv4.setAmount(bsurv);
        jv4.setAmountBase(bsurv);
        jv4.setCreditAmount(bsurv);
        jv4.setCurrencyId(defCurr.getCurrencyId());
        jv4.setCurrencyRate(bd_ONE);
        jv4.setDebitAmount(bd_ZERO);
        jv4.setDebitCredit(GlAttributes.i_CREDIT);
        jv4.setLocationId(oLoc.getLocationId());
        jv4.setDepartmentId("");
        jv4.setProjectId("");
        jv4.setReferenceNo("");
        jv4.setMemo("");
        
        if (pstnk.doubleValue() != 0)
        {
            JournalVoucherDetail jv5 = new JournalVoucherDetail();
            Account ac5 = AccountTool.getAccountByCode("7.10.09.00.00");
            jv5.setAccountId(ac5.getAccountId());
            jv5.setAccountCode(ac5.getAccountCode());
            jv5.setAccountName(ac5.getAccountName());
            jv5.setAmount(pstnk);
            jv5.setAmountBase(pstnk);
            jv5.setCreditAmount(pstnk);
            jv5.setCurrencyId(defCurr.getCurrencyId());
            jv5.setCurrencyRate(bd_ONE);
            jv5.setDebitAmount(bd_ZERO);
            jv5.setDebitCredit(GlAttributes.i_CREDIT);
            jv5.setLocationId(oLoc.getLocationId());
            jv5.setDepartmentId("");
            jv5.setProjectId("");
            jv5.setReferenceNo("");
            jv5.setMemo("");
            
            vDet.add(jv5);
        }

//    1.13.01.01.00	Piutang PK-Motor			4,620,000.00 	
//    	1.13.04.01.00	Piutang Amort PK Motor-Adm			    300,000.00 
//    	1.13.04.02.00	Piutang Amort PK Motor-Provisi			120,000.00 
//    	4.99.01.05.00	Piutang Amort PK Motor-By Survey		125,000.00 
//    	2.12.01.00.00	Hutang Dealer PK-Motor			      2,241,000.00 
//    	7.10.09.00.00	Hutang Titipan STNK			                  0.00
//    	 
//    	1.13.01.01.00	Piutang PK-Motor			            312,636.00 
//    	4.10.01.00.00	Pendapatan Bunga PK Motor			    171,364.00 
//    	2.12.01.00.00	Hutang Dealer PK-Motor			      1,350,000.00 				
//    	
        if (byrpok.doubleValue() != 0)
        {
	        JournalVoucherDetail jv6 = new JournalVoucherDetail();
	        Account ac6 = AccountTool.getAccountByCode("1.13.01.01.00");
	        jv6.setAccountId(ac6.getAccountId());
	        jv6.setAccountCode(ac6.getAccountCode());
	        jv6.setAccountName(ac6.getAccountName());
	        jv6.setAmount(byrpok);
	        jv6.setAmountBase(byrpok);
	        jv6.setCreditAmount(byrpok);
	        jv6.setCurrencyId(defCurr.getCurrencyId());
	        jv6.setCurrencyRate(bd_ONE);
	        jv6.setDebitAmount(bd_ZERO);
	        jv6.setDebitCredit(GlAttributes.i_CREDIT);
	        jv6.setLocationId(oLoc.getLocationId());
	        jv6.setDepartmentId("");
	        jv6.setProjectId("");
	        jv6.setReferenceNo("");
	        jv6.setMemo("");
	        vDet.add(jv6);
        }
        
        if (byrbun.doubleValue() != 0)
        {
	        JournalVoucherDetail jv7 = new JournalVoucherDetail();
	        Account ac7 = AccountTool.getAccountByCode("4.10.01.00.00");
	        jv7.setAccountId(ac7.getAccountId());
	        jv7.setAccountCode(ac7.getAccountCode());
	        jv7.setAccountName(ac7.getAccountName());
	        jv7.setAmount(byrbun);
	        jv7.setAmountBase(byrbun);
	        jv7.setCreditAmount(byrbun);
	        jv7.setCurrencyId(defCurr.getCurrencyId());
	        jv7.setCurrencyRate(bd_ONE);
	        jv7.setDebitAmount(bd_ZERO);
	        jv7.setDebitCredit(GlAttributes.i_CREDIT);
	        jv7.setLocationId(oLoc.getLocationId());
	        jv7.setDepartmentId("");
	        jv7.setProjectId("");
	        jv7.setReferenceNo("");
	        jv7.setMemo("");
	        vDet.add(jv7);
        }

        if (potlun.doubleValue() != 0)
        {
	        JournalVoucherDetail jv7a = new JournalVoucherDetail();
	        Account ac7a = AccountTool.getAccountByCode("2.12.01.00.00");
	        jv7a.setAccountId(ac7a.getAccountId());
	        jv7a.setAccountCode(ac7a.getAccountCode());
	        jv7a.setAccountName(ac7a.getAccountName());
	        jv7a.setAmount(potlun);
	        jv7a.setAmountBase(potlun);
	        jv7a.setCreditAmount(potlun);
	        jv7a.setCurrencyId(defCurr.getCurrencyId());
	        jv7a.setCurrencyRate(bd_ONE);
	        jv7a.setDebitAmount(bd_ZERO);
	        jv7a.setDebitCredit(GlAttributes.i_CREDIT);
	        jv7a.setLocationId(oLoc.getLocationId());
	        jv7a.setDepartmentId("");
	        jv7a.setProjectId("");
	        jv7a.setReferenceNo("");
	        jv7a.setMemo("");
	        vDet.add(jv7a);
	    }
        
        JournalVoucherDetail jv8 = new JournalVoucherDetail();
        Account ac8 = AccountTool.getAccountByCode("2.12.01.00.00");
        jv8.setAccountId(ac8.getAccountId());
        jv8.setAccountCode(ac8.getAccountCode());
        jv8.setAccountName(ac8.getAccountName());
        jv8.setAmount(pencr);
        jv8.setAmountBase(pencr);
        jv8.setCreditAmount(pencr);
        jv8.setCurrencyId(defCurr.getCurrencyId());
        jv8.setCurrencyRate(bd_ONE);
        jv8.setDebitAmount(bd_ZERO);
        jv8.setDebitCredit(GlAttributes.i_CREDIT);
        jv8.setLocationId(oLoc.getLocationId());
        jv8.setDepartmentId("");
        jv8.setProjectId("");
        jv8.setReferenceNo("");
        jv8.setMemo("");

//      1.13.04.01.00	Piutang Amort PK Motor-Adm			     38,528.00 	
//      1.13.04.02.00	Piutang Amort PK Motor-Provisi			 15,411.00 	
//      4.99.01.05.00	Piutang Amort PK Motor-By Survey		 16,053.00 	      4.99.01.05.00
//      	4.10.02.01.00	Pendapatan Adm Amort PK Motor	            38,528.00 
//      	4.10.02.02.00	Pendapatan Provisi Amort PK Motor			15,411.00 
//      	4.99.01.05.00	Pendapatan By Survey Amort PK Motor			16,053.00       
        
        if (badm2.doubleValue() != 0)
        {
	        JournalVoucherDetail jv9 = new JournalVoucherDetail();
	        Account ac9 = AccountTool.getAccountByCode("1.13.04.01.00");
	        jv9.setAccountId(ac9.getAccountId());
	        jv9.setAccountCode(ac9.getAccountCode());
	        jv9.setAccountName(ac9.getAccountName());
	        jv9.setAmount(badm2);
	        jv9.setAmountBase(badm2);
	        jv9.setDebitAmount(badm2);
	        jv9.setCurrencyId(defCurr.getCurrencyId());
	        jv9.setCurrencyRate(bd_ONE);
	        jv9.setCreditAmount(bd_ZERO);
	        jv9.setDebitCredit(GlAttributes.i_DEBIT);
	        jv9.setLocationId(oLoc.getLocationId());
	        jv9.setDepartmentId("");
	        jv9.setProjectId("");
	        jv9.setReferenceNo("");
	        jv9.setMemo("");
	
	        JournalVoucherDetail jv12 = new JournalVoucherDetail();
	        Account ac12 = AccountTool.getAccountByCode("4.10.02.01.00");
	        jv12.setAccountId(ac12.getAccountId());
	        jv12.setAccountCode(ac12.getAccountCode());
	        jv12.setAccountName(ac12.getAccountName());
	        jv12.setAmount(badm2);
	        jv12.setAmountBase(badm2);
	        jv12.setDebitAmount(bd_ZERO);
	        jv12.setCurrencyId(defCurr.getCurrencyId());
	        jv12.setCurrencyRate(bd_ONE);
	        jv12.setCreditAmount(badm2);
	        jv12.setDebitCredit(GlAttributes.i_CREDIT);
	        jv12.setLocationId(oLoc.getLocationId());
	        jv12.setDepartmentId("");
	        jv12.setProjectId("");
	        jv12.setReferenceNo("");
	        jv12.setMemo("");

	        vDet.add(jv12);        
	        vDet.add(jv9);                
        }

        if (bprov2.doubleValue() != 0)
        {
	        JournalVoucherDetail jv10 = new JournalVoucherDetail();
	        Account ac10 = AccountTool.getAccountByCode("1.13.04.02.00");
	        jv10.setAccountId(ac10.getAccountId());
	        jv10.setAccountCode(ac10.getAccountCode());
	        jv10.setAccountName(ac10.getAccountName());
	        jv10.setAmount(bprov2);
	        jv10.setAmountBase(bprov2);
	        jv10.setDebitAmount(bprov2);
	        jv10.setCurrencyId(defCurr.getCurrencyId());
	        jv10.setCurrencyRate(bd_ONE);
	        jv10.setCreditAmount(bd_ZERO);
	        jv10.setDebitCredit(GlAttributes.i_DEBIT);
	        jv10.setLocationId(oLoc.getLocationId());
	        jv10.setDepartmentId("");
	        jv10.setProjectId("");
	        jv10.setReferenceNo("");
	        jv10.setMemo("");
	
	        JournalVoucherDetail jv13 = new JournalVoucherDetail();
	        Account ac13 = AccountTool.getAccountByCode("4.10.02.02.00");
	        jv13.setAccountId(ac13.getAccountId());
	        jv13.setAccountCode(ac13.getAccountCode());
	        jv13.setAccountName(ac13.getAccountName());
	        jv13.setAmount(bprov2);
	        jv13.setAmountBase(bprov2);
	        jv13.setDebitAmount(bd_ZERO);
	        jv13.setCurrencyId(defCurr.getCurrencyId());
	        jv13.setCurrencyRate(bd_ONE);
	        jv13.setCreditAmount(bprov2);
	        jv13.setDebitCredit(GlAttributes.i_CREDIT);
	        jv13.setLocationId(oLoc.getLocationId());
	        jv13.setDepartmentId("");
	        jv13.setProjectId("");
	        jv13.setReferenceNo("");
	        jv13.setMemo("");
	        
	        vDet.add(jv13);                
	        vDet.add(jv10);        
        }
        
        if (bsurv2.doubleValue() != 0)
        {
	        JournalVoucherDetail jv11 = new JournalVoucherDetail();
	        Account ac11 = AccountTool.getAccountByCode("4.99.01.05.00");
	        jv11.setAccountId(ac11.getAccountId());
	        jv11.setAccountCode(ac11.getAccountCode());
	        jv11.setAccountName(ac11.getAccountName());
	        jv11.setAmount(bsurv2);
	        jv11.setAmountBase(bsurv2);
	        jv11.setDebitAmount(bsurv2);
	        jv11.setCurrencyId(defCurr.getCurrencyId());
	        jv11.setCurrencyRate(bd_ONE);
	        jv11.setCreditAmount(bd_ZERO);
	        jv11.setDebitCredit(GlAttributes.i_DEBIT);
	        jv11.setLocationId(oLoc.getLocationId());
	        jv11.setDepartmentId("");
	        jv11.setProjectId("");
	        jv11.setReferenceNo("");
	        jv11.setMemo("");
	
	        JournalVoucherDetail jv14 = new JournalVoucherDetail();
	        Account ac14 = AccountTool.getAccountByCode("4.99.01.05.00");
	        jv14.setAccountId(ac14.getAccountId());
	        jv14.setAccountCode(ac14.getAccountCode());
	        jv14.setAccountName(ac14.getAccountName());
	        jv14.setAmount(bsurv2);
	        jv14.setAmountBase(bsurv2);
	        jv14.setDebitAmount(bd_ZERO);
	        jv14.setCurrencyId(defCurr.getCurrencyId());
	        jv14.setCurrencyRate(bd_ONE);
	        jv14.setCreditAmount(bsurv2);
	        jv14.setDebitCredit(GlAttributes.i_CREDIT);
	        jv14.setLocationId(oLoc.getLocationId());
	        jv14.setDepartmentId("");
	        jv14.setProjectId("");
	        jv14.setReferenceNo("");
	        jv14.setMemo("");

	        vDet.add(jv14);        
	        vDet.add(jv11);
        }

        vDet.add(jv8);        
        vDet.add(jv4);
        vDet.add(jv3);
        vDet.add(jv2);
        vDet.add(jv1);        
    }
    

    private BigDecimal parseNumber(String _sField, String _sNumber) 
        throws Exception
    {
        String sResult = "";
        log.debug("** PARSE NUMBER " + _sField + " " + _sNumber);
        if (StringUtil.isNotEmpty(_sNumber))
        {
            boolean bDecimal = false;
            sResult = StringUtil.replace(_sNumber,",00","");
            sResult = StringUtil.replace(sResult,"Rp. ","");
            sResult = StringUtil.replace(sResult,".","");
            if (StringUtil.contains(sResult,","))
            {
                sResult = StringUtil.replace(sResult,",",".");
                bDecimal = true;
            }
            log.debug("** PARSE NUMBER " + _sField + " " + sResult);
            try
            {
                BigDecimal bdResult = new BigDecimal(sResult);  
                if(!bDecimal)
                {
                    bdResult = bdResult.setScale(0,4);
                }
                else
                {
                    bdResult = bdResult.setScale(2,4);                    
                }
                return bdResult;
            }
            catch (Exception e)
            {
                logError(_sField,_sNumber);
            }
        }
        return bd_ZERO;
    }

    public boolean isExists()
        throws Exception
    {        
        if (StringUtil.isNotEmpty(sTransNo))
        {
            Criteria oCrit = new Criteria();
            oCrit.add(JournalVoucherPeer.TRANSACTION_NO, sTransNo);
            oCrit.add(JournalVoucherPeer.STATUS, i_PROCESSED);            
            List vData = JournalVoucherPeer.doSelect(oCrit);
            if (vData.size() > 0) 
            {
                return true;
            }
        }
        return false;
    }
}

package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class DrawerLogMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.DrawerLogMapBuilder";

    /**
     * Item
     * @deprecated use DrawerLogPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "drawer_log";
    }

  
    /**
     * drawer_log.DRAWER_LOG_ID
     * @return the column name for the DRAWER_LOG_ID field
     * @deprecated use DrawerLogPeer.drawer_log.DRAWER_LOG_ID constant
     */
    public static String getDrawerLog_DrawerLogId()
    {
        return "drawer_log.DRAWER_LOG_ID";
    }
  
    /**
     * drawer_log.OPEN_DATE
     * @return the column name for the OPEN_DATE field
     * @deprecated use DrawerLogPeer.drawer_log.OPEN_DATE constant
     */
    public static String getDrawerLog_OpenDate()
    {
        return "drawer_log.OPEN_DATE";
    }
  
    /**
     * drawer_log.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use DrawerLogPeer.drawer_log.USER_NAME constant
     */
    public static String getDrawerLog_UserName()
    {
        return "drawer_log.USER_NAME";
    }
  
    /**
     * drawer_log.ACCESS_TYPE
     * @return the column name for the ACCESS_TYPE field
     * @deprecated use DrawerLogPeer.drawer_log.ACCESS_TYPE constant
     */
    public static String getDrawerLog_AccessType()
    {
        return "drawer_log.ACCESS_TYPE";
    }
  
    /**
     * drawer_log.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use DrawerLogPeer.drawer_log.AMOUNT constant
     */
    public static String getDrawerLog_Amount()
    {
        return "drawer_log.AMOUNT";
    }
  
    /**
     * drawer_log.REASON
     * @return the column name for the REASON field
     * @deprecated use DrawerLogPeer.drawer_log.REASON constant
     */
    public static String getDrawerLog_Reason()
    {
        return "drawer_log.REASON";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("drawer_log");
        TableMap tMap = dbMap.getTable("drawer_log");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("drawer_log.DRAWER_LOG_ID", "");
                          tMap.addColumn("drawer_log.OPEN_DATE", new Date());
                          tMap.addColumn("drawer_log.USER_NAME", "");
                            tMap.addColumn("drawer_log.ACCESS_TYPE", Integer.valueOf(0));
                            tMap.addColumn("drawer_log.AMOUNT", bd_ZERO);
                          tMap.addColumn("drawer_log.REASON", "");
          }
}

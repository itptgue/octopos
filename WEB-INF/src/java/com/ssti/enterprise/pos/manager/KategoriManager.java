package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.KategoriAccount;
import com.ssti.enterprise.pos.om.KategoriAccountPeer;
import com.ssti.enterprise.pos.om.KategoriPeer;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.framework.tools.StringUtil;

public class KategoriManager
{
	private static Log log = LogFactory.getLog(KategoriManager.class);
	    
    private static KategoriManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "kategori";    
    
    private static final String s_ALL      = "AllKategori";
    private static final String s_BY_ID    = "Kategori";
    private static final String s_CHILDREN = "KategoriChildren";
    private static final String s_JS_TREE  = "KategoriJSTree";
    private static final String s_OBJ_TREE = "KategoriTree";
    private static final String s_ACC 	   = "KategoriAccount";
    private static final String s_LEVEL	   = "KategoriLevel";
    
    private static final String[] a_ALL_KEYS = 
    {
    	s_ALL, s_CHILDREN, s_JS_TREE, s_OBJ_TREE
    };

    private static final String[] a_BY_ID_KEYS =
    {
    	s_BY_ID, s_CHILDREN, s_OBJ_TREE, s_ACC
    };

    
    private KategoriManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static KategoriManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (KategoriManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new KategoriManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllKategori ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_ALL);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			//oCrit.addDescendingOrderByColumn (KategoriPeer.PARENT_ID);	
			oCrit.addAscendingOrderByColumn (KategoriPeer.KATEGORI_CODE);
			List vData = KategoriPeer.doSelect(oCrit);
			oCache.put(new Element (s_ALL, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	/**
	 * get all kategori at certain level 
	 * 
	 * @param _iLevel
	 * @return list of kategori at level
	 * @throws Exception
	 */
	public List getKategoriAtLevel(int _iLevel)
	    throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_LEVEL + _iLevel);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (KategoriPeer.KATEGORI_CODE);
	        oCrit.add (KategoriPeer.KATEGORI_LEVEL, _iLevel);
			List vData = KategoriPeer.doSelect(oCrit);
			oCache.put(new Element (s_LEVEL + _iLevel, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}		
	}	

	
	public Kategori getKategoriByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if (StringUtil.isNotEmpty(_sID))
        {
			Element oElement = oCache.get(s_BY_ID + _sID);
			if (oElement == null)
			{		
				if (StringUtil.isNotEmpty(_sID))
				{
					Criteria oCrit = new Criteria();
					oCrit.add(KategoriPeer.KATEGORI_ID, _sID);
					List vData = KategoriPeer.doSelect(oCrit, _oConn);				
					if (vData.size() > 0) 
					{
						oCache.put(new Element (s_BY_ID + _sID, (Serializable) vData.get(0))); 
						return (Kategori) vData.get(0);
					}
				}
				return null;
			}
			else 
			{
				return (Kategori) oElement.getValue();
			}
        }
        return null;
	}

	public List getChildren(String _sParentID, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_CHILDREN + _sParentID);
		if (oElement == null)
		{					
			Criteria oCrit = new Criteria();
			if (StringUtil.isEmpty(_sParentID)) //prevent null kategori parent_id 
			{
				oCrit.add(KategoriPeer.KATEGORI_LEVEL, 1);
			}
			else
			{
				oCrit.add(KategoriPeer.PARENT_ID, _sParentID);
			}
			oCrit.addAscendingOrderByColumn(KategoriPeer.KATEGORI_CODE);
			List vData = new ArrayList();
        	if (_oConn != null) 
        	{
	        	vData = KategoriPeer.doSelect(oCrit, _oConn);
			}
        	else {
	        	vData = KategoriPeer.doSelect(oCrit);        	
        	}
        	oCache.put(new Element (s_CHILDREN + _sParentID, (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}
	
	public String getKategoriJSTree()
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 	    
		Element oElement = oCache.get(s_JS_TREE);
		if (oElement == null)
		{
			String sTree = KategoriTool.createJSTree();			
			oCache.put(new Element (s_JS_TREE, sTree)); 
			return sTree;
		}
		else 
		{
			return (String)	oElement.getValue();
		}
	}
    
	public List getKategoriTreeList (List _vKategori, String _sID)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_OBJ_TREE + _sID);
		if (oElement == null)
		{		
		    List vKategori = KategoriTool.getKategoriTreeList(_vKategori, _sID);			
			oCache.put(new Element (s_OBJ_TREE + _sID, (Serializable) vKategori)); 
			return vKategori;
		}
		else 
		{
            return (List) oElement.getValue();
		}
	} 	
	
	/**
	 * get Kategori Account
	 * @param _sKatID
	 * @param _oConn
	 * @return Kategori Account
	 * @throws Exception
	 */
	public KategoriAccount getKategoriAccount(String _sKatID, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(s_ACC + _sKatID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sKatID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(KategoriAccountPeer.KATEGORI_ID, _sKatID);
				List vData = new ArrayList();
				if (_oConn != null) 
				{
					vData = KategoriAccountPeer.doSelect(oCrit, _oConn);
				}
				else 
				{
					vData = KategoriAccountPeer.doSelect(oCrit);        	
				}				
				if (vData.size() > 0) 
				{
					oCache.put(new Element (s_ACC + _sKatID, (Serializable) vData.get(0))); 
					return (KategoriAccount) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (KategoriAccount) oElement.getValue();
		}
	}	

	/**
	 * refresh kategori if update happened
	 * 
	 * @param _oData
	 * @throws Exception
	 */
	public void refreshCache (Kategori _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if (_oData != null)
        {
	        refreshCache (_oData.getKategoriId());
	        refreshCache (_oData.getParentId());
	        oCache.put(new Element (s_BY_ID + _oData.getKategoriId(), _oData));
        }
        KategoriTool.updateHasChild();
    }

	/**
	 * refresh kategori if deleted
	 * 
	 * @param _oData
	 * @throws Exception
	 */
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        for (int i = 0; i < a_ALL_KEYS.length; i++)
        {
        	oCache.remove(a_ALL_KEYS[i]);
        }

        for (int i = 0; i < a_BY_ID_KEYS.length; i++)
        {
        	oCache.remove(a_BY_ID_KEYS[i] + _sID);
        }
        KategoriTool.updateHasChild();
        oCache.removeAll();        
    }      
}
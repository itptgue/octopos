package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.LocationMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseLocationPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "location";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(LocationMapBuilder.CLASS_NAME);
    }

      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the LOCATION_CODE field */
    public static final String LOCATION_CODE;
      /** the column name for the LOCATION_NAME field */
    public static final String LOCATION_NAME;
      /** the column name for the LOCATION_ADDRESS field */
    public static final String LOCATION_ADDRESS;
      /** the column name for the SITE_ID field */
    public static final String SITE_ID;
      /** the column name for the INVENTORY_TYPE field */
    public static final String INVENTORY_TYPE;
      /** the column name for the HO_STORE_LEADTIME field */
    public static final String HO_STORE_LEADTIME;
      /** the column name for the DESCRIPTION field */
    public static final String DESCRIPTION;
      /** the column name for the LOCATION_TYPE field */
    public static final String LOCATION_TYPE;
      /** the column name for the OB_TRANS_ID field */
    public static final String OB_TRANS_ID;
      /** the column name for the UPDATE_DATE field */
    public static final String UPDATE_DATE;
      /** the column name for the LOCATION_PHONE field */
    public static final String LOCATION_PHONE;
      /** the column name for the LOCATION_EMAIL field */
    public static final String LOCATION_EMAIL;
      /** the column name for the LOCATION_URL field */
    public static final String LOCATION_URL;
      /** the column name for the PARENT_LOCATION_ID field */
    public static final String PARENT_LOCATION_ID;
      /** the column name for the TRANSFER_ACCOUNT field */
    public static final String TRANSFER_ACCOUNT;
      /** the column name for the SALES_AREA_ID field */
    public static final String SALES_AREA_ID;
      /** the column name for the TERRITORY_ID field */
    public static final String TERRITORY_ID;
      /** the column name for the LONGITUDES field */
    public static final String LONGITUDES;
      /** the column name for the LATITUDES field */
    public static final String LATITUDES;
  
    static
    {
          LOCATION_ID = "location.LOCATION_ID";
          LOCATION_CODE = "location.LOCATION_CODE";
          LOCATION_NAME = "location.LOCATION_NAME";
          LOCATION_ADDRESS = "location.LOCATION_ADDRESS";
          SITE_ID = "location.SITE_ID";
          INVENTORY_TYPE = "location.INVENTORY_TYPE";
          HO_STORE_LEADTIME = "location.HO_STORE_LEADTIME";
          DESCRIPTION = "location.DESCRIPTION";
          LOCATION_TYPE = "location.LOCATION_TYPE";
          OB_TRANS_ID = "location.OB_TRANS_ID";
          UPDATE_DATE = "location.UPDATE_DATE";
          LOCATION_PHONE = "location.LOCATION_PHONE";
          LOCATION_EMAIL = "location.LOCATION_EMAIL";
          LOCATION_URL = "location.LOCATION_URL";
          PARENT_LOCATION_ID = "location.PARENT_LOCATION_ID";
          TRANSFER_ACCOUNT = "location.TRANSFER_ACCOUNT";
          SALES_AREA_ID = "location.SALES_AREA_ID";
          TERRITORY_ID = "location.TERRITORY_ID";
          LONGITUDES = "location.LONGITUDES";
          LATITUDES = "location.LATITUDES";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(LocationMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(LocationMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  20;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.Location";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseLocationPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(LOCATION_CODE);
          criteria.addSelectColumn(LOCATION_NAME);
          criteria.addSelectColumn(LOCATION_ADDRESS);
          criteria.addSelectColumn(SITE_ID);
          criteria.addSelectColumn(INVENTORY_TYPE);
          criteria.addSelectColumn(HO_STORE_LEADTIME);
          criteria.addSelectColumn(DESCRIPTION);
          criteria.addSelectColumn(LOCATION_TYPE);
          criteria.addSelectColumn(OB_TRANS_ID);
          criteria.addSelectColumn(UPDATE_DATE);
          criteria.addSelectColumn(LOCATION_PHONE);
          criteria.addSelectColumn(LOCATION_EMAIL);
          criteria.addSelectColumn(LOCATION_URL);
          criteria.addSelectColumn(PARENT_LOCATION_ID);
          criteria.addSelectColumn(TRANSFER_ACCOUNT);
          criteria.addSelectColumn(SALES_AREA_ID);
          criteria.addSelectColumn(TERRITORY_ID);
          criteria.addSelectColumn(LONGITUDES);
          criteria.addSelectColumn(LATITUDES);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Location row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Location obj = (Location) cls.newInstance();
            LocationPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Location obj)
        throws TorqueException
    {
        try
        {
                obj.setLocationId(row.getValue(offset + 0).asString());
                  obj.setLocationCode(row.getValue(offset + 1).asString());
                  obj.setLocationName(row.getValue(offset + 2).asString());
                  obj.setLocationAddress(row.getValue(offset + 3).asString());
                  obj.setSiteId(row.getValue(offset + 4).asString());
                  obj.setInventoryType(row.getValue(offset + 5).asInt());
                  obj.setHoStoreLeadtime(row.getValue(offset + 6).asInt());
                  obj.setDescription(row.getValue(offset + 7).asString());
                  obj.setLocationType(row.getValue(offset + 8).asInt());
                  obj.setObTransId(row.getValue(offset + 9).asString());
                  obj.setUpdateDate(row.getValue(offset + 10).asUtilDate());
                  obj.setLocationPhone(row.getValue(offset + 11).asString());
                  obj.setLocationEmail(row.getValue(offset + 12).asString());
                  obj.setLocationUrl(row.getValue(offset + 13).asString());
                  obj.setParentLocationId(row.getValue(offset + 14).asString());
                  obj.setTransferAccount(row.getValue(offset + 15).asString());
                  obj.setSalesAreaId(row.getValue(offset + 16).asString());
                  obj.setTerritoryId(row.getValue(offset + 17).asString());
                  obj.setLongitudes(row.getValue(offset + 18).asBigDecimal());
                  obj.setLatitudes(row.getValue(offset + 19).asBigDecimal());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseLocationPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                          
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(LocationPeer.row2Object(row, 1,
                LocationPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseLocationPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(LOCATION_ID, criteria.remove(LOCATION_ID));
                                                                                                                                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         LocationPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Location obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Location obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Location obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Location obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Location) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Location obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Location) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Location obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Location) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Location obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseLocationPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(LOCATION_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Location obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(LOCATION_CODE, obj.getLocationCode());
              criteria.add(LOCATION_NAME, obj.getLocationName());
              criteria.add(LOCATION_ADDRESS, obj.getLocationAddress());
              criteria.add(SITE_ID, obj.getSiteId());
              criteria.add(INVENTORY_TYPE, obj.getInventoryType());
              criteria.add(HO_STORE_LEADTIME, obj.getHoStoreLeadtime());
              criteria.add(DESCRIPTION, obj.getDescription());
              criteria.add(LOCATION_TYPE, obj.getLocationType());
              criteria.add(OB_TRANS_ID, obj.getObTransId());
              criteria.add(UPDATE_DATE, obj.getUpdateDate());
              criteria.add(LOCATION_PHONE, obj.getLocationPhone());
              criteria.add(LOCATION_EMAIL, obj.getLocationEmail());
              criteria.add(LOCATION_URL, obj.getLocationUrl());
              criteria.add(PARENT_LOCATION_ID, obj.getParentLocationId());
              criteria.add(TRANSFER_ACCOUNT, obj.getTransferAccount());
              criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
              criteria.add(TERRITORY_ID, obj.getTerritoryId());
              criteria.add(LONGITUDES, obj.getLongitudes());
              criteria.add(LATITUDES, obj.getLatitudes());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( Location obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(LOCATION_CODE, obj.getLocationCode());
                          criteria.add(LOCATION_NAME, obj.getLocationName());
                          criteria.add(LOCATION_ADDRESS, obj.getLocationAddress());
                          criteria.add(SITE_ID, obj.getSiteId());
                          criteria.add(INVENTORY_TYPE, obj.getInventoryType());
                          criteria.add(HO_STORE_LEADTIME, obj.getHoStoreLeadtime());
                          criteria.add(DESCRIPTION, obj.getDescription());
                          criteria.add(LOCATION_TYPE, obj.getLocationType());
                          criteria.add(OB_TRANS_ID, obj.getObTransId());
                          criteria.add(UPDATE_DATE, obj.getUpdateDate());
                          criteria.add(LOCATION_PHONE, obj.getLocationPhone());
                          criteria.add(LOCATION_EMAIL, obj.getLocationEmail());
                          criteria.add(LOCATION_URL, obj.getLocationUrl());
                          criteria.add(PARENT_LOCATION_ID, obj.getParentLocationId());
                          criteria.add(TRANSFER_ACCOUNT, obj.getTransferAccount());
                          criteria.add(SALES_AREA_ID, obj.getSalesAreaId());
                          criteria.add(TERRITORY_ID, obj.getTerritoryId());
                          criteria.add(LONGITUDES, obj.getLongitudes());
                          criteria.add(LATITUDES, obj.getLatitudes());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Location retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Location retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Location retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Location retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Location retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Location)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( LOCATION_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

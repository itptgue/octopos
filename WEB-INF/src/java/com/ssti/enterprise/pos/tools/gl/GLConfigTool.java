package com.ssti.enterprise.pos.tools.gl;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.manager.GLConfigManager;
import com.ssti.enterprise.pos.om.GlConfig;
import com.ssti.framework.tools.BasePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * GlConfig OM controller
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: GLConfigTool.java,v 1.5 2008/03/15 12:42:00 albert Exp $ <br>
 *
 * <pre>
 * $Log: GLConfigTool.java,v $
 * Revision 1.5  2008/03/15 12:42:00  albert
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 03:04:20  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/03/12 09:38:10  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class GLConfigTool 
{
	private static Log log = LogFactory.getLog(GLConfigTool.class);

	//static methods	
	public static GlConfig getGLConfig()
		throws Exception
	{
		return GLConfigManager.getInstance().getGlConfig(null);
	}
	
	public static GlConfig getGLConfig(Connection _oConn)
    	throws Exception
    {
		return GLConfigManager.getInstance().getGlConfig(_oConn);
	}

	public static void refreshPref()
    	throws Exception
    {
		GLConfigManager.getInstance().refreshCache();
	}
	
	/**
	 * Update Item that gl config is not set yet
	 * 
	 * @param _oConfig
	 * @return updated item
	 * @throws Exception
	 */
	public static int updateItem(GlConfig _oConfig)
		throws Exception
	{
		StringBuilder oUpdateSQL = new StringBuilder();
		oUpdateSQL.append ("UPDATE item SET ")
				  .append ("inventory_account = '")          .append (_oConfig.getInventoryAccount     ())
		          .append ("', sales_account = '")           .append (_oConfig.getSalesAccount         ())
		          .append ("', sales_return_account = '")    .append (_oConfig.getSalesReturnAccount   ())
		          .append ("', item_discount_account = '")   .append (_oConfig.getItemDiscountAccount  ())
		          .append ("', cogs_account = '")            .append (_oConfig.getCogsAccount          ())
		          .append ("', purchase_return_account = '") .append (_oConfig.getPurchaseReturnAccount())
				  .append ("', expense_account = '")         .append (_oConfig.getExpenseAccount       ())
				  .append ("', unbilled_goods_account = '")  .append (_oConfig.getUnbilledGoodsAccount ())
				  .append ("' WHERE inventory_account = '' AND sales_account = ''");
		log.debug("Update SQL : " + oUpdateSQL);
		int iUpdated = BasePeer.executeStatement(oUpdateSQL.toString());
		log.debug("Item Updated : " + iUpdated);
		return iUpdated;
	}
}
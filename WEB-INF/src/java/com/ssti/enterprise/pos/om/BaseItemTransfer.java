package com.ssti.enterprise.pos.om;


 import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemTransfer
 */
public abstract class BaseItemTransfer extends BaseObject
{
    /** The Peer class */
    private static final ItemTransferPeer peer =
        new ItemTransferPeer();

        
    /** The value for the itemTransferId field */
    private String itemTransferId;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the adjustmentTypeId field */
    private String adjustmentTypeId;
      
    /** The value for the fromLocationId field */
    private String fromLocationId;
      
    /** The value for the fromLocationName field */
    private String fromLocationName;
      
    /** The value for the toLocationId field */
    private String toLocationId;
      
    /** The value for the toLocationName field */
    private String toLocationName;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the courierId field */
    private String courierId;
                                          
    /** The value for the status field */
    private int status = 1;
                                                
    /** The value for the requestId field */
    private String requestId = "";
                                                
    /** The value for the requestNo field */
    private String requestNo = "";
                                                
    /** The value for the confirmBy field */
    private String confirmBy = "";
      
    /** The value for the confirmDate field */
    private Date confirmDate;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
                                                
    /** The value for the receiveBy field */
    private String receiveBy = "";
      
    /** The value for the receiveDate field */
    private Date receiveDate;
  
    
    /**
     * Get the ItemTransferId
     *
     * @return String
     */
    public String getItemTransferId()
    {
        return itemTransferId;
    }

                                              
    /**
     * Set the value of ItemTransferId
     *
     * @param v new value
     */
    public void setItemTransferId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.itemTransferId, v))
              {
            this.itemTransferId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated ItemTransferDetail
        if (collItemTransferDetails != null)
        {
            for (int i = 0; i < collItemTransferDetails.size(); i++)
            {
                ((ItemTransferDetail) collItemTransferDetails.get(i))
                    .setItemTransferId(v);
            }
        }
                                }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AdjustmentTypeId
     *
     * @return String
     */
    public String getAdjustmentTypeId()
    {
        return adjustmentTypeId;
    }

                        
    /**
     * Set the value of AdjustmentTypeId
     *
     * @param v new value
     */
    public void setAdjustmentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.adjustmentTypeId, v))
              {
            this.adjustmentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FromLocationId
     *
     * @return String
     */
    public String getFromLocationId()
    {
        return fromLocationId;
    }

                        
    /**
     * Set the value of FromLocationId
     *
     * @param v new value
     */
    public void setFromLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fromLocationId, v))
              {
            this.fromLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FromLocationName
     *
     * @return String
     */
    public String getFromLocationName()
    {
        return fromLocationName;
    }

                        
    /**
     * Set the value of FromLocationName
     *
     * @param v new value
     */
    public void setFromLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fromLocationName, v))
              {
            this.fromLocationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ToLocationId
     *
     * @return String
     */
    public String getToLocationId()
    {
        return toLocationId;
    }

                        
    /**
     * Set the value of ToLocationId
     *
     * @param v new value
     */
    public void setToLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.toLocationId, v))
              {
            this.toLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ToLocationName
     *
     * @return String
     */
    public String getToLocationName()
    {
        return toLocationName;
    }

                        
    /**
     * Set the value of ToLocationName
     *
     * @param v new value
     */
    public void setToLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.toLocationName, v))
              {
            this.toLocationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RequestId
     *
     * @return String
     */
    public String getRequestId()
    {
        return requestId;
    }

                        
    /**
     * Set the value of RequestId
     *
     * @param v new value
     */
    public void setRequestId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.requestId, v))
              {
            this.requestId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the RequestNo
     *
     * @return String
     */
    public String getRequestNo()
    {
        return requestNo;
    }

                        
    /**
     * Set the value of RequestNo
     *
     * @param v new value
     */
    public void setRequestNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.requestNo, v))
              {
            this.requestNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmBy
     *
     * @return String
     */
    public String getConfirmBy()
    {
        return confirmBy;
    }

                        
    /**
     * Set the value of ConfirmBy
     *
     * @param v new value
     */
    public void setConfirmBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmBy, v))
              {
            this.confirmBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmDate
     *
     * @return Date
     */
    public Date getConfirmDate()
    {
        return confirmDate;
    }

                        
    /**
     * Set the value of ConfirmDate
     *
     * @param v new value
     */
    public void setConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmDate, v))
              {
            this.confirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReceiveBy
     *
     * @return String
     */
    public String getReceiveBy()
    {
        return receiveBy;
    }

                        
    /**
     * Set the value of ReceiveBy
     *
     * @param v new value
     */
    public void setReceiveBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.receiveBy, v))
              {
            this.receiveBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReceiveDate
     *
     * @return Date
     */
    public Date getReceiveDate()
    {
        return receiveDate;
    }

                        
    /**
     * Set the value of ReceiveDate
     *
     * @param v new value
     */
    public void setReceiveDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.receiveDate, v))
              {
            this.receiveDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collItemTransferDetails
     */
    protected List collItemTransferDetails;

    /**
     * Temporary storage of collItemTransferDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initItemTransferDetails()
    {
        if (collItemTransferDetails == null)
        {
            collItemTransferDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a ItemTransferDetail object to this object
     * through the ItemTransferDetail foreign key attribute
     *
     * @param l ItemTransferDetail
     * @throws TorqueException
     */
    public void addItemTransferDetail(ItemTransferDetail l) throws TorqueException
    {
        getItemTransferDetails().add(l);
        l.setItemTransfer((ItemTransfer) this);
    }

    /**
     * The criteria used to select the current contents of collItemTransferDetails
     */
    private Criteria lastItemTransferDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItemTransferDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getItemTransferDetails() throws TorqueException
    {
              if (collItemTransferDetails == null)
        {
            collItemTransferDetails = getItemTransferDetails(new Criteria(10));
        }
        return collItemTransferDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ItemTransfer has previously
     * been saved, it will retrieve related ItemTransferDetails from storage.
     * If this ItemTransfer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getItemTransferDetails(Criteria criteria) throws TorqueException
    {
              if (collItemTransferDetails == null)
        {
            if (isNew())
            {
               collItemTransferDetails = new ArrayList();
            }
            else
            {
                        criteria.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, getItemTransferId() );
                        collItemTransferDetails = ItemTransferDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, getItemTransferId());
                            if (!lastItemTransferDetailsCriteria.equals(criteria))
                {
                    collItemTransferDetails = ItemTransferDetailPeer.doSelect(criteria);
                }
            }
        }
        lastItemTransferDetailsCriteria = criteria;

        return collItemTransferDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getItemTransferDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItemTransferDetails(Connection con) throws TorqueException
    {
              if (collItemTransferDetails == null)
        {
            collItemTransferDetails = getItemTransferDetails(new Criteria(10), con);
        }
        return collItemTransferDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ItemTransfer has previously
     * been saved, it will retrieve related ItemTransferDetails from storage.
     * If this ItemTransfer is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getItemTransferDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collItemTransferDetails == null)
        {
            if (isNew())
            {
               collItemTransferDetails = new ArrayList();
            }
            else
            {
                         criteria.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, getItemTransferId());
                         collItemTransferDetails = ItemTransferDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, getItemTransferId());
                             if (!lastItemTransferDetailsCriteria.equals(criteria))
                 {
                     collItemTransferDetails = ItemTransferDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastItemTransferDetailsCriteria = criteria;

         return collItemTransferDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ItemTransfer is new, it will return
     * an empty collection; or if this ItemTransfer has previously
     * been saved, it will retrieve related ItemTransferDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ItemTransfer.
     */
    protected List getItemTransferDetailsJoinItemTransfer(Criteria criteria)
        throws TorqueException
    {
                    if (collItemTransferDetails == null)
        {
            if (isNew())
            {
               collItemTransferDetails = new ArrayList();
            }
            else
            {
                              criteria.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, getItemTransferId());
                              collItemTransferDetails = ItemTransferDetailPeer.doSelectJoinItemTransfer(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(ItemTransferDetailPeer.ITEM_TRANSFER_ID, getItemTransferId());
                                    if (!lastItemTransferDetailsCriteria.equals(criteria))
            {
                collItemTransferDetails = ItemTransferDetailPeer.doSelectJoinItemTransfer(criteria);
            }
        }
        lastItemTransferDetailsCriteria = criteria;

        return collItemTransferDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemTransferId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("AdjustmentTypeId");
              fieldNames.add("FromLocationId");
              fieldNames.add("FromLocationName");
              fieldNames.add("ToLocationId");
              fieldNames.add("ToLocationName");
              fieldNames.add("UserName");
              fieldNames.add("Description");
              fieldNames.add("CourierId");
              fieldNames.add("Status");
              fieldNames.add("RequestId");
              fieldNames.add("RequestNo");
              fieldNames.add("ConfirmBy");
              fieldNames.add("ConfirmDate");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames.add("ReceiveBy");
              fieldNames.add("ReceiveDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemTransferId"))
        {
                return getItemTransferId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("AdjustmentTypeId"))
        {
                return getAdjustmentTypeId();
            }
          if (name.equals("FromLocationId"))
        {
                return getFromLocationId();
            }
          if (name.equals("FromLocationName"))
        {
                return getFromLocationName();
            }
          if (name.equals("ToLocationId"))
        {
                return getToLocationId();
            }
          if (name.equals("ToLocationName"))
        {
                return getToLocationName();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("RequestId"))
        {
                return getRequestId();
            }
          if (name.equals("RequestNo"))
        {
                return getRequestNo();
            }
          if (name.equals("ConfirmBy"))
        {
                return getConfirmBy();
            }
          if (name.equals("ConfirmDate"))
        {
                return getConfirmDate();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          if (name.equals("ReceiveBy"))
        {
                return getReceiveBy();
            }
          if (name.equals("ReceiveDate"))
        {
                return getReceiveDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemTransferPeer.ITEM_TRANSFER_ID))
        {
                return getItemTransferId();
            }
          if (name.equals(ItemTransferPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(ItemTransferPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(ItemTransferPeer.ADJUSTMENT_TYPE_ID))
        {
                return getAdjustmentTypeId();
            }
          if (name.equals(ItemTransferPeer.FROM_LOCATION_ID))
        {
                return getFromLocationId();
            }
          if (name.equals(ItemTransferPeer.FROM_LOCATION_NAME))
        {
                return getFromLocationName();
            }
          if (name.equals(ItemTransferPeer.TO_LOCATION_ID))
        {
                return getToLocationId();
            }
          if (name.equals(ItemTransferPeer.TO_LOCATION_NAME))
        {
                return getToLocationName();
            }
          if (name.equals(ItemTransferPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(ItemTransferPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(ItemTransferPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(ItemTransferPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(ItemTransferPeer.REQUEST_ID))
        {
                return getRequestId();
            }
          if (name.equals(ItemTransferPeer.REQUEST_NO))
        {
                return getRequestNo();
            }
          if (name.equals(ItemTransferPeer.CONFIRM_BY))
        {
                return getConfirmBy();
            }
          if (name.equals(ItemTransferPeer.CONFIRM_DATE))
        {
                return getConfirmDate();
            }
          if (name.equals(ItemTransferPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(ItemTransferPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          if (name.equals(ItemTransferPeer.RECEIVE_BY))
        {
                return getReceiveBy();
            }
          if (name.equals(ItemTransferPeer.RECEIVE_DATE))
        {
                return getReceiveDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemTransferId();
            }
              if (pos == 1)
        {
                return getTransactionNo();
            }
              if (pos == 2)
        {
                return getTransactionDate();
            }
              if (pos == 3)
        {
                return getAdjustmentTypeId();
            }
              if (pos == 4)
        {
                return getFromLocationId();
            }
              if (pos == 5)
        {
                return getFromLocationName();
            }
              if (pos == 6)
        {
                return getToLocationId();
            }
              if (pos == 7)
        {
                return getToLocationName();
            }
              if (pos == 8)
        {
                return getUserName();
            }
              if (pos == 9)
        {
                return getDescription();
            }
              if (pos == 10)
        {
                return getCourierId();
            }
              if (pos == 11)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 12)
        {
                return getRequestId();
            }
              if (pos == 13)
        {
                return getRequestNo();
            }
              if (pos == 14)
        {
                return getConfirmBy();
            }
              if (pos == 15)
        {
                return getConfirmDate();
            }
              if (pos == 16)
        {
                return getCancelBy();
            }
              if (pos == 17)
        {
                return getCancelDate();
            }
              if (pos == 18)
        {
                return getReceiveBy();
            }
              if (pos == 19)
        {
                return getReceiveDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemTransferPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemTransferPeer.doInsert((ItemTransfer) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemTransferPeer.doUpdate((ItemTransfer) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collItemTransferDetails != null)
            {
                for (int i = 0; i < collItemTransferDetails.size(); i++)
                {
                    ((ItemTransferDetail) collItemTransferDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemTransferId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setItemTransferId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setItemTransferId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemTransferId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemTransfer copy() throws TorqueException
    {
        return copyInto(new ItemTransfer());
    }
  
    protected ItemTransfer copyInto(ItemTransfer copyObj) throws TorqueException
    {
          copyObj.setItemTransferId(itemTransferId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setAdjustmentTypeId(adjustmentTypeId);
          copyObj.setFromLocationId(fromLocationId);
          copyObj.setFromLocationName(fromLocationName);
          copyObj.setToLocationId(toLocationId);
          copyObj.setToLocationName(toLocationName);
          copyObj.setUserName(userName);
          copyObj.setDescription(description);
          copyObj.setCourierId(courierId);
          copyObj.setStatus(status);
          copyObj.setRequestId(requestId);
          copyObj.setRequestNo(requestNo);
          copyObj.setConfirmBy(confirmBy);
          copyObj.setConfirmDate(confirmDate);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
          copyObj.setReceiveBy(receiveBy);
          copyObj.setReceiveDate(receiveDate);
  
                    copyObj.setItemTransferId((String)null);
                                                                                                                              
                                      
                            
        List v = getItemTransferDetails();
        for (int i = 0; i < v.size(); i++)
        {
            ItemTransferDetail obj = (ItemTransferDetail) v.get(i);
            copyObj.addItemTransferDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemTransferPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemTransfer\n");
        str.append("------------\n")
           .append("ItemTransferId       : ")
           .append(getItemTransferId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("AdjustmentTypeId     : ")
           .append(getAdjustmentTypeId())
           .append("\n")
           .append("FromLocationId       : ")
           .append(getFromLocationId())
           .append("\n")
           .append("FromLocationName     : ")
           .append(getFromLocationName())
           .append("\n")
           .append("ToLocationId         : ")
           .append(getToLocationId())
           .append("\n")
           .append("ToLocationName       : ")
           .append(getToLocationName())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("RequestId            : ")
           .append(getRequestId())
           .append("\n")
           .append("RequestNo            : ")
           .append(getRequestNo())
           .append("\n")
           .append("ConfirmBy            : ")
           .append(getConfirmBy())
           .append("\n")
           .append("ConfirmDate          : ")
           .append(getConfirmDate())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
           .append("ReceiveBy            : ")
           .append(getReceiveBy())
           .append("\n")
           .append("ReceiveDate          : ")
           .append(getReceiveDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

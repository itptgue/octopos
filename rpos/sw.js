var sCTX = '/GPOS/rpos';
var CACHE_NAME = 'rpos-offline-statics';

var resourcesToCache = [
    sCTX + '/',             
    sCTX + '/css/font-awesome/css/font-awesome.min.css',
    sCTX + '/css/pikaday.css',
    sCTX + '/css/pos-touch.css',
    sCTX + '/css/stylesheet.css',
    sCTX + '/js/dt/datatables.min.css',  
    sCTX + '/js/dt/datatables.min.js',
    sCTX + '/js/select2/select2.min.css',
    sCTX + '/js/common.js',
    sCTX + '/js/pikaday.js',
    sCTX + '/js/shortcut.js',
    sCTX + '/js/format_number.js',
    sCTX + '/js/moment.min.js',
    sCTX + '/js/jquery.min.js',
    sCTX + '/js/jq/jquery.simplemodal.min.js',
    sCTX + '/js/notify.min.js',
    sCTX + '/js/jq/spin.min.js',
    sCTX + '/js/jq/jquery.spin.js',
    sCTX + '/js/select2/select2.min.js',
    sCTX + '/js/common_trans.js',    
    sCTX + '/js/db/pouchdb.min.js',
    sCTX + '/js/db/pouchdb.find.min.js',
    sCTX + '/js/db/pouchdb.quick-search.min.js',
    sCTX + '/js/db/jsonQ.min.js',
    sCTX + '/js/handlebars.min.js',
    sCTX + '/index.html', 
    sCTX + '/POS.html',
    sCTX + '/POSView.html',
    sCTX + '/POSPrint.html',
	sCTX + '/ItemView.html'
    //sCTX + '/sw.js'
];

self.addEventListener('install', function(event) {
    console.info('installing service worker');
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.info('cache opened');
                return cache.addAll(resourcesToCache);
            })
            .then(function() {
        		return self.skipWaiting();
      		})
    );
});

self.addEventListener('activate', function(event) {
    console.info('service worker activated');
    event.waitUntil(
      caches.keys().then(keyList => Promise.all(keyList.map(key => {
        if (key !== CACHE_NAME) {
          return caches.delete(key);
        }
      })))
    );
});

self.addEventListener('fetch', function(event) {
	var hasQuery = event.request.url.indexOf('?') != -1;	
    event.respondWith(
        caches.match(event.request, {ignoreSearch: hasQuery})
            .then(function(response) {                                
            if (response) {
                console.info(event.request.url + ' cache hit');
                return response;
            }
            console.info(event.request.url + ' get from server');
            return fetch(event.request);
        }) 
    ); 
    event.waitUntil(
        update(event.request)
    );
});

function update(request) {
  return caches.open(CACHE_NAME).then(function (cache) {
    return fetch(request).then(function (response) {
      //console.log(request);
      if(request.url.indexOf('.vm') < 0 && request.url.indexOf('chrome-extension') < 0)
      {     
          console.info(request.url + ' cache update');
          return cache.put(request, response.clone()).then(function () {
            return response;
          });
      }
    });
  });
}

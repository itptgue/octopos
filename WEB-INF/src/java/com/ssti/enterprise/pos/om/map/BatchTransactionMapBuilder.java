package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class BatchTransactionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.BatchTransactionMapBuilder";

    /**
     * Item
     * @deprecated use BatchTransactionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "batch_transaction";
    }

  
    /**
     * batch_transaction.BATCH_TRANSACTION_ID
     * @return the column name for the BATCH_TRANSACTION_ID field
     * @deprecated use BatchTransactionPeer.batch_transaction.BATCH_TRANSACTION_ID constant
     */
    public static String getBatchTransaction_BatchTransactionId()
    {
        return "batch_transaction.BATCH_TRANSACTION_ID";
    }
  
    /**
     * batch_transaction.INVENTORY_TRANSACTION_ID
     * @return the column name for the INVENTORY_TRANSACTION_ID field
     * @deprecated use BatchTransactionPeer.batch_transaction.INVENTORY_TRANSACTION_ID constant
     */
    public static String getBatchTransaction_InventoryTransactionId()
    {
        return "batch_transaction.INVENTORY_TRANSACTION_ID";
    }
  
    /**
     * batch_transaction.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use BatchTransactionPeer.batch_transaction.ITEM_ID constant
     */
    public static String getBatchTransaction_ItemId()
    {
        return "batch_transaction.ITEM_ID";
    }
  
    /**
     * batch_transaction.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use BatchTransactionPeer.batch_transaction.ITEM_CODE constant
     */
    public static String getBatchTransaction_ItemCode()
    {
        return "batch_transaction.ITEM_CODE";
    }
  
    /**
     * batch_transaction.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use BatchTransactionPeer.batch_transaction.LOCATION_ID constant
     */
    public static String getBatchTransaction_LocationId()
    {
        return "batch_transaction.LOCATION_ID";
    }
  
    /**
     * batch_transaction.BATCH_NO
     * @return the column name for the BATCH_NO field
     * @deprecated use BatchTransactionPeer.batch_transaction.BATCH_NO constant
     */
    public static String getBatchTransaction_BatchNo()
    {
        return "batch_transaction.BATCH_NO";
    }
  
    /**
     * batch_transaction.EXPIRED_DATE
     * @return the column name for the EXPIRED_DATE field
     * @deprecated use BatchTransactionPeer.batch_transaction.EXPIRED_DATE constant
     */
    public static String getBatchTransaction_ExpiredDate()
    {
        return "batch_transaction.EXPIRED_DATE";
    }
  
    /**
     * batch_transaction.QTY
     * @return the column name for the QTY field
     * @deprecated use BatchTransactionPeer.batch_transaction.QTY constant
     */
    public static String getBatchTransaction_Qty()
    {
        return "batch_transaction.QTY";
    }
  
    /**
     * batch_transaction.TRANSACTION_DETAIL_ID
     * @return the column name for the TRANSACTION_DETAIL_ID field
     * @deprecated use BatchTransactionPeer.batch_transaction.TRANSACTION_DETAIL_ID constant
     */
    public static String getBatchTransaction_TransactionDetailId()
    {
        return "batch_transaction.TRANSACTION_DETAIL_ID";
    }
  
    /**
     * batch_transaction.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use BatchTransactionPeer.batch_transaction.TRANSACTION_ID constant
     */
    public static String getBatchTransaction_TransactionId()
    {
        return "batch_transaction.TRANSACTION_ID";
    }
  
    /**
     * batch_transaction.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use BatchTransactionPeer.batch_transaction.TRANSACTION_NO constant
     */
    public static String getBatchTransaction_TransactionNo()
    {
        return "batch_transaction.TRANSACTION_NO";
    }
  
    /**
     * batch_transaction.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use BatchTransactionPeer.batch_transaction.TRANSACTION_DATE constant
     */
    public static String getBatchTransaction_TransactionDate()
    {
        return "batch_transaction.TRANSACTION_DATE";
    }
  
    /**
     * batch_transaction.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use BatchTransactionPeer.batch_transaction.TRANSACTION_TYPE constant
     */
    public static String getBatchTransaction_TransactionType()
    {
        return "batch_transaction.TRANSACTION_TYPE";
    }
  
    /**
     * batch_transaction.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use BatchTransactionPeer.batch_transaction.DESCRIPTION constant
     */
    public static String getBatchTransaction_Description()
    {
        return "batch_transaction.DESCRIPTION";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("batch_transaction");
        TableMap tMap = dbMap.getTable("batch_transaction");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("batch_transaction.BATCH_TRANSACTION_ID", "");
                          tMap.addColumn("batch_transaction.INVENTORY_TRANSACTION_ID", "");
                          tMap.addColumn("batch_transaction.ITEM_ID", "");
                          tMap.addColumn("batch_transaction.ITEM_CODE", "");
                          tMap.addColumn("batch_transaction.LOCATION_ID", "");
                          tMap.addColumn("batch_transaction.BATCH_NO", "");
                          tMap.addColumn("batch_transaction.EXPIRED_DATE", new Date());
                            tMap.addColumn("batch_transaction.QTY", bd_ZERO);
                          tMap.addColumn("batch_transaction.TRANSACTION_DETAIL_ID", "");
                          tMap.addColumn("batch_transaction.TRANSACTION_ID", "");
                          tMap.addColumn("batch_transaction.TRANSACTION_NO", "");
                          tMap.addColumn("batch_transaction.TRANSACTION_DATE", new Date());
                            tMap.addColumn("batch_transaction.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("batch_transaction.DESCRIPTION", "");
          }
}

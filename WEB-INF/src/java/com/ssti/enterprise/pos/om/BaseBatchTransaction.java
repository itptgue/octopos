package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to BatchTransaction
 */
public abstract class BaseBatchTransaction extends BaseObject
{
    /** The Peer class */
    private static final BatchTransactionPeer peer =
        new BatchTransactionPeer();

        
    /** The value for the batchTransactionId field */
    private String batchTransactionId;
      
    /** The value for the inventoryTransactionId field */
    private String inventoryTransactionId;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the locationId field */
    private String locationId;
                                                
    /** The value for the batchNo field */
    private String batchNo = "";
      
    /** The value for the expiredDate field */
    private Date expiredDate;
      
    /** The value for the qty field */
    private BigDecimal qty;
                                                
    /** The value for the transactionDetailId field */
    private String transactionDetailId = "";
                                                
    /** The value for the transactionId field */
    private String transactionId = "";
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the transactionType field */
    private int transactionType;
      
    /** The value for the description field */
    private String description;
  
    
    /**
     * Get the BatchTransactionId
     *
     * @return String
     */
    public String getBatchTransactionId()
    {
        return batchTransactionId;
    }

                        
    /**
     * Set the value of BatchTransactionId
     *
     * @param v new value
     */
    public void setBatchTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.batchTransactionId, v))
              {
            this.batchTransactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InventoryTransactionId
     *
     * @return String
     */
    public String getInventoryTransactionId()
    {
        return inventoryTransactionId;
    }

                        
    /**
     * Set the value of InventoryTransactionId
     *
     * @param v new value
     */
    public void setInventoryTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.inventoryTransactionId, v))
              {
            this.inventoryTransactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BatchNo
     *
     * @return String
     */
    public String getBatchNo()
    {
        return batchNo;
    }

                        
    /**
     * Set the value of BatchNo
     *
     * @param v new value
     */
    public void setBatchNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.batchNo, v))
              {
            this.batchNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpiredDate
     *
     * @return Date
     */
    public Date getExpiredDate()
    {
        return expiredDate;
    }

                        
    /**
     * Set the value of ExpiredDate
     *
     * @param v new value
     */
    public void setExpiredDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.expiredDate, v))
              {
            this.expiredDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDetailId
     *
     * @return String
     */
    public String getTransactionDetailId()
    {
        return transactionDetailId;
    }

                        
    /**
     * Set the value of TransactionDetailId
     *
     * @param v new value
     */
    public void setTransactionDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDetailId, v))
              {
            this.transactionDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("BatchTransactionId");
              fieldNames.add("InventoryTransactionId");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("LocationId");
              fieldNames.add("BatchNo");
              fieldNames.add("ExpiredDate");
              fieldNames.add("Qty");
              fieldNames.add("TransactionDetailId");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("TransactionType");
              fieldNames.add("Description");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("BatchTransactionId"))
        {
                return getBatchTransactionId();
            }
          if (name.equals("InventoryTransactionId"))
        {
                return getInventoryTransactionId();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("BatchNo"))
        {
                return getBatchNo();
            }
          if (name.equals("ExpiredDate"))
        {
                return getExpiredDate();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("TransactionDetailId"))
        {
                return getTransactionDetailId();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(BatchTransactionPeer.BATCH_TRANSACTION_ID))
        {
                return getBatchTransactionId();
            }
          if (name.equals(BatchTransactionPeer.INVENTORY_TRANSACTION_ID))
        {
                return getInventoryTransactionId();
            }
          if (name.equals(BatchTransactionPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(BatchTransactionPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(BatchTransactionPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(BatchTransactionPeer.BATCH_NO))
        {
                return getBatchNo();
            }
          if (name.equals(BatchTransactionPeer.EXPIRED_DATE))
        {
                return getExpiredDate();
            }
          if (name.equals(BatchTransactionPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(BatchTransactionPeer.TRANSACTION_DETAIL_ID))
        {
                return getTransactionDetailId();
            }
          if (name.equals(BatchTransactionPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(BatchTransactionPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(BatchTransactionPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(BatchTransactionPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(BatchTransactionPeer.DESCRIPTION))
        {
                return getDescription();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getBatchTransactionId();
            }
              if (pos == 1)
        {
                return getInventoryTransactionId();
            }
              if (pos == 2)
        {
                return getItemId();
            }
              if (pos == 3)
        {
                return getItemCode();
            }
              if (pos == 4)
        {
                return getLocationId();
            }
              if (pos == 5)
        {
                return getBatchNo();
            }
              if (pos == 6)
        {
                return getExpiredDate();
            }
              if (pos == 7)
        {
                return getQty();
            }
              if (pos == 8)
        {
                return getTransactionDetailId();
            }
              if (pos == 9)
        {
                return getTransactionId();
            }
              if (pos == 10)
        {
                return getTransactionNo();
            }
              if (pos == 11)
        {
                return getTransactionDate();
            }
              if (pos == 12)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 13)
        {
                return getDescription();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(BatchTransactionPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        BatchTransactionPeer.doInsert((BatchTransaction) this, con);
                        setNew(false);
                    }
                    else
                    {
                        BatchTransactionPeer.doUpdate((BatchTransaction) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key batchTransactionId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setBatchTransactionId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setBatchTransactionId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getBatchTransactionId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public BatchTransaction copy() throws TorqueException
    {
        return copyInto(new BatchTransaction());
    }
  
    protected BatchTransaction copyInto(BatchTransaction copyObj) throws TorqueException
    {
          copyObj.setBatchTransactionId(batchTransactionId);
          copyObj.setInventoryTransactionId(inventoryTransactionId);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setLocationId(locationId);
          copyObj.setBatchNo(batchNo);
          copyObj.setExpiredDate(expiredDate);
          copyObj.setQty(qty);
          copyObj.setTransactionDetailId(transactionDetailId);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setTransactionType(transactionType);
          copyObj.setDescription(description);
  
                    copyObj.setBatchTransactionId((String)null);
                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public BatchTransactionPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("BatchTransaction\n");
        str.append("----------------\n")
           .append("BatchTransactionId   : ")
           .append(getBatchTransactionId())
           .append("\n")
            .append("InventoryTransactionId   : ")
           .append(getInventoryTransactionId())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("BatchNo              : ")
           .append(getBatchNo())
           .append("\n")
           .append("ExpiredDate          : ")
           .append(getExpiredDate())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("TransactionDetailId  : ")
           .append(getTransactionDetailId())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
        ;
        return(str.toString());
    }
}

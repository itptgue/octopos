package com.ssti.framework.io;

import java.io.IOException;

import com.ssti.framework.tools.IOTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * various I/O related utilities
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: IOUtil.java,v 1.4 2007/02/23 14:12:09 albert Exp $ <br>
 *
 * <pre>
 * $Log: IOUtil.java,v $
 * 
 * 2016-11-10
 * -move to IOTool
 *  
 * </pre><br>
 *
 */
public class IOUtil
{
	/**
	 * @deprecated - move to IOTool	 
	 * copy file to another
	 * 
	 * @param _sFrom
	 * @param _sTo
	 * @throws IOException
	 */
	public static void copyFile ( String _sFrom, String _sTo ) 
		throws IOException
	{
		IOTool.copyFile(_sFrom, _sTo);
	}

	/**
	 * @deprecated - move to IOTool
	 * check if file exist
	 * @param _sPath
	 * @return file exist or not
	 */
	public static boolean isFileExist (String _sPath)
	{
        return IOTool.isFileExist(_sPath);
	}
	
	/**
	 * @deprecated - move to IOTool
	 * delete all files in a directory
	 * 
	 * @param _sDirName
	 * @throws IOException
	 */
	public static void deleteAllFilesInDirectory ( String _sDirName ) 
		throws IOException
	{
		IOTool.deleteAllFilesInDirectory(_sDirName);
	}	
}
package com.ssti.framework.networking;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Multi Threading Helper Class for HTTP Client POST
 * 
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class HttpClientPoster implements Runnable
{
	private static final Log log = LogFactory.getLog (HttpClientPoster.class);

	HttpClient httpClient;
	PostMethod postMethod;
	int httpResult;
	public HttpClientPoster(PostMethod _post)
	{
		postMethod = _post;
	}
	public void run()
	{
		if (postMethod != null)
		{
        	try
			{
	        	httpClient = new HttpClient();        		        	
        		httpResult = httpClient.executeMethod(postMethod);    			
        		if (httpResult != -1)
    			{
    				postMethod.releaseConnection();
    				log.info("HTTP Client POST to " + postMethod.getURI() + "  Result : " + httpResult);
    			}
			}
			catch (HttpException e)
			{
				e.printStackTrace();
				log.error("HTTP ERROR: " + e.getMessage(), e);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				log.error("IO ERROR: " + e.getMessage(), e);
			}
		}
	}
	public int getHttpResult()
	{
		return httpResult;
	}
}
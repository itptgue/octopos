package com.ssti.framework.xml;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.DOMReader;
import org.dom4j.io.SAXReader;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Small helper class that lazy loads DOM and SAX reader and keep them for fast use afterwards.
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: XMLHelper.java,v 1.3 2007/02/23 14:14:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: XMLHelper.java,v $
 * Revision 1.3  2007/02/23 14:14:17  albert
 * *** empty log message ***
 * 
 * </pre><br>
 *
 */
public final class XMLHelper 
{
	static final Log log = LogFactory.getLog(XMLHelper.class);

	private DOMReader domReader;
	private SAXReader saxReader;

	 /**
	  * Create a dom4j SAXReader which will append all validation errors
	  * to errorList
	  * 	  
	  * @param file
	  * @param errorsList
	  * @param entityResolver
	  * @return SAXReader
	  */
	public SAXReader createSAXReader(String file, List errorsList, EntityResolver entityResolver) 
	{
		saxReader = new SAXReader();
		saxReader.setEntityResolver(entityResolver);
		saxReader.setValidation(true);
		saxReader.setErrorHandler( new ErrorLogger(file, errorsList) );
		saxReader.setMergeAdjacentText(true);
		return saxReader;
	}
	
	/**
	 * create dom4j DOMReader
	 * @return DOMReader
	 */
	public DOMReader createDOMReader() {
		if (domReader==null) domReader = new DOMReader();
		return domReader;
	}
	
	public static class ErrorLogger implements ErrorHandler 
	{
		private String file;
		private List errors;
		ErrorLogger(String file, List errors) {
			this.file=file;
			this.errors = errors;
		}
		public void error(SAXParseException error) {
			log.error( "Error parsing XML: " + file + '(' + error.getLineNumber() + ") " + error.getMessage() );
			errors.add(error);
		}
		public void fatalError(SAXParseException error) {
			error(error);
		}
		public void warning(SAXParseException warn) {
			log.warn( "Warning parsing XML: " + file + '(' + warn.getLineNumber() + ") " + warn.getMessage() );
		}
	}
	
	/**
	 * generateDom4jElement
	 * 
	 * @param elementName
	 * @return Element
	 */
	public static Element generateDom4jElement(String elementName) 
	{
		return DocumentFactory.getInstance().createElement( elementName );
	}
}







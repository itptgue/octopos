package com.ssti.enterprise.pos.tools.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.financial.DebitMemoTool;
import com.ssti.enterprise.pos.tools.financial.PayablePaymentTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReturnTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesReturnTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SetGLTransSubLedger extends BaseValidator implements AppAttributes
{
	Log log = LogFactory.getLog(getClass());

	public static final int i_GL_TRANS_OPENING_BALANCE    = 1;
	public static final int i_GL_TRANS_RECEIPT_UNPLANNED  = 2;
	public static final int i_GL_TRANS_ISSUE_UNPLANNED    = 3;
	public static final int i_GL_TRANS_PURCHASE_RECEIPT   = 4;
	public static final int i_GL_TRANS_PURCHASE_INVOICE   = 5;
	public static final int i_GL_TRANS_PURCHASE_RETURN    = 6;
	public static final int i_GL_TRANS_ITEM_TRANSFER 	  = 7;
	public static final int i_GL_TRANS_DELIVERY_ORDER     = 8;
	public static final int i_GL_TRANS_SALES_INVOICE      = 9;
	public static final int i_GL_TRANS_SALES_RETURN 	  = 11;
	public static final int i_GL_TRANS_FIXED_ASSET 	      = 12;
	public static final int i_GL_TRANS_JOURNAL_VOUCHER    = 13;
	public static final int i_GL_TRANS_AR_PAYMENT         = 14;
	public static final int i_GL_TRANS_AP_PAYMENT         = 15;
	public static final int i_GL_TRANS_CREDIT_MEMO        = 16;
	public static final int i_GL_TRANS_DEBIT_MEMO         = 17;
	public static final int i_GL_TRANS_CASH_MANAGEMENT    = 18;
    public static final int i_GL_TRANS_JOB_COSTING        = 19;
    public static final int i_GL_TRANS_JC_ROLLOVER        = 20;

	Connection m_oConn = null;
	/**
	 * validate whether current_qty in inventory location is valid if 
	 * compared to last qty_balance in last inventory_transaction
	 *
	 */
	public void setSubLedger()
	{	

		try
		{							
			//AP
			updateAP();
			
			//AR
			updateAR();
		}
		catch (Exception _oEx)
		{
			try 
			{
				BaseTool.rollback(m_oConn);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			handleError (m_oConn, _oEx);
		}
	}
	
	private void updateAP()
		throws Exception
	{
		m_oConn = BaseTool.beginTrans();
		int iUpdated = 0;
		
		//AP 
		StringBuilder oSB = new StringBuilder();
		oSB.append("SELECT DISTINCT transaction_id, transaction_type FROM gl_transaction ");
		oSB.append(" WHERE transaction_type IN (4, 5, 6, 15, 17) ORDER BY transaction_type ");
		Statement oStmt = m_oConn.createStatement();
		ResultSet oRS = oStmt.executeQuery(oSB.toString());
		
		StringBuilder oSB1 = new StringBuilder();
		oSB1.append("UPDATE gl_transaction SET sub_ledger_type = 1, sub_ledger_id = ? WHERE transaction_id = ? ");
		PreparedStatement oPstmt = m_oConn.prepareStatement(oSB1.toString());
		
		while (oRS.next())
		{
			int iTransType = oRS.getInt("transaction_type");
			String sTransID = oRS.getString("transaction_id");
			String sVendorID = getVendorID(iTransType, sTransID);
			
			oPstmt.setString(1, sVendorID);
			oPstmt.setString(2, sTransID);
			oPstmt.execute();
			iUpdated++;
		}
		BaseTool.commit(m_oConn);		
		m_oResult.append(" Total " + iUpdated + " AP Transaction UPDATED ");				
	}
	
	private void updateAR()
		throws Exception
	{
		m_oConn = BaseTool.beginTrans();
		int iUpdated = 0;
		
		//AP 
		StringBuilder oSB = new StringBuilder();
		oSB.append("SELECT DISTINCT transaction_id, transaction_type FROM gl_transaction ");
		oSB.append(" WHERE transaction_type IN (8, 9, 11, 14, 16) ORDER BY transaction_type ");
		Statement oStmt = m_oConn.createStatement();
		ResultSet oRS = oStmt.executeQuery(oSB.toString());
		
		StringBuilder oSB1 = new StringBuilder();
		oSB1.append("UPDATE gl_transaction SET sub_ledger_type = 2, sub_ledger_id = ? WHERE transaction_id = ? ");
		PreparedStatement oPstmt = m_oConn.prepareStatement(oSB1.toString());
		
		while (oRS.next())
		{
			int iTransType = oRS.getInt("transaction_type");
			String sTransID = oRS.getString("transaction_id");
			String sCustID = getCustomerID(iTransType, sTransID);
			
			oPstmt.setString(1, sCustID);
			oPstmt.setString(2, sTransID);
			oPstmt.execute();
			iUpdated++;
		}
		BaseTool.commit(m_oConn);		
		m_oResult.append(" Total " + iUpdated + " AR Transaction UPDATED ");				
	}
	
	private String getCustomerID(int _iType, String _sID)
		throws Exception
	{
		if (_iType == i_GL_TRANS_DELIVERY_ORDER)
		{
			DeliveryOrder oTR = DeliveryOrderTool.getHeaderByID(_sID, m_oConn);
			return oTR.getCustomerId();
		}
		if (_iType == i_GL_TRANS_SALES_INVOICE)
		{
			SalesTransaction oTR = TransactionTool.getHeaderByID(_sID, m_oConn);
			return oTR.getCustomerId();
		}
		if (_iType == i_GL_TRANS_SALES_RETURN)
		{
			SalesReturn oTR = SalesReturnTool.getHeaderByID(_sID, m_oConn);
			return oTR.getCustomerId();			
		}
		if (_iType == i_GL_TRANS_AR_PAYMENT)
		{
			ArPayment oTR = ReceivablePaymentTool.getHeaderByID(_sID, m_oConn);
			return oTR.getCustomerId();			
		}
		if (_iType == i_GL_TRANS_CREDIT_MEMO)
		{
			CreditMemo oTR = CreditMemoTool.getCreditMemoByID(_sID, m_oConn);
			return oTR.getCustomerId();			
		}
		return "";
	}
		
	private String getVendorID(int _iType, String _sID)
		throws Exception
	{
		if (_iType == i_GL_TRANS_PURCHASE_RECEIPT)
		{
			PurchaseReceipt oTR = PurchaseReceiptTool.getHeaderByID(_sID, m_oConn);
			return oTR.getVendorId();
		}
		if (_iType == i_GL_TRANS_PURCHASE_INVOICE)
		{
			PurchaseInvoice oTR = PurchaseInvoiceTool.getHeaderByID(_sID, m_oConn);
			return oTR.getVendorId();
		}
		if (_iType == i_GL_TRANS_PURCHASE_RETURN)
		{
			PurchaseReturn oTR = PurchaseReturnTool.getHeaderByID(_sID, m_oConn);
			return oTR.getVendorId();			
		}
		if (_iType == i_GL_TRANS_AP_PAYMENT)
		{
			ApPayment oTR = PayablePaymentTool.getHeaderByID(_sID, m_oConn);
			return oTR.getVendorId();			
		}
		if (_iType == i_GL_TRANS_DEBIT_MEMO)
		{
			DebitMemo oTR = DebitMemoTool.getDebitMemoByID(_sID, m_oConn);
			return oTR.getVendorId();			
		}
		return "";
	}
	
}

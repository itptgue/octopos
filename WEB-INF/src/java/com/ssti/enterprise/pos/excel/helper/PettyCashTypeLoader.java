package com.ssti.enterprise.pos.excel.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.manager.PettyCashManager;
import com.ssti.enterprise.pos.om.PettyCashType;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.PettyCashTypeTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.StringUtil;

public class PettyCashTypeLoader extends BaseExcelLoader 
{
	private static Log log = LogFactory.getLog (PettyCashTypeLoader.class );
	
	public static final String s_START_HEADING = "Petty Cash Type Code";
	
	public PettyCashTypeLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
	
	protected void updateList(HSSFRow _oRow, short _iIdx) 
		throws Exception 
	{
		StringBuilder oEmpty = new StringBuilder(s_EMPTY_REJECT);
		StringBuilder oLength = new StringBuilder(s_LENGTH_REJECT);
		
		if (started (_oRow, _iIdx))	
		{
			short i = 0;
			
			String sCode = getString(_oRow, _iIdx + i); i++;
			String sName = getString(_oRow, _iIdx + i); i++;
						
			if (StringUtil.isNotEmpty(sCode))
			{				
				PettyCashType oData = PettyCashTypeTool.getByCode(sCode);
				boolean bNew = true;
				boolean bValid = true;
				
				log.debug ("Column " + _iIdx + " Petty Cash Type Code : " + sCode);
				
				if (oData == null) 
				{	
					log.debug ("Code Not Exist, create new Code");
					
					oData = new PettyCashType();
					oData.setPettyCashTypeId (IDGenerator.generateSysID());
					oData.setPettyCashCode(sCode); 	
					bNew = true;
				}
				else 
				{
					bNew = false;
				}
				oData.setDescription(sName); 
				bValid = validateString("Description", oData.getDescription(), 100, oEmpty, oLength);

				String sFlow = getString(_oRow, _iIdx + i); i++;
				oData.setIncomingOutgoing(i_WITHDRAWAL);
				if (sFlow.equals("Deposit"))
				
				{
					oData.setIncomingOutgoing(i_DEPOSIT);
				}
				
				String sAccCode = getString(_oRow, _iIdx + i); i++;
				oData.setAccountId(AccountTool.getIDByCode(sAccCode, true));
								
                oData.save();
                PettyCashManager.getInstance().refreshCache(oData);
            	
                if(bValid)
				{
					if (bNew) 
					{	
						m_iNewData++;
						m_oNewData.append (StringUtil.left(m_iNewData + ". ", 5));										
						m_oNewData.append (StringUtil.left(sCode,30));
						m_oNewData.append (StringUtil.left(sName,100));
						m_oNewData.append ("\n");
					}
					else 
					{
						m_iUpdated++;
						m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
						m_oUpdated.append (StringUtil.left(sCode,30));
						m_oUpdated.append (StringUtil.left(sName,100));
						m_oUpdated.append ("\n");
					}
				}
                else
                {
				    m_iRejected++;
				    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
				    m_oRejected.append (StringUtil.left(sCode,30));
				    m_oRejected.append (StringUtil.left(sName,50));
				    if (oEmpty.length() > s_EMPTY_REJECT.length())
				    {				    
				    	m_oRejected.append (oEmpty);
				    }
				    if (oLength.length() > s_LENGTH_REJECT.length())
				    {
				    	m_oRejected.append ("; ");				    
				    	m_oRejected.append (oLength);
				    }
				    m_oRejected.append ("\n");
                }
			}
		}    
		
	}
}

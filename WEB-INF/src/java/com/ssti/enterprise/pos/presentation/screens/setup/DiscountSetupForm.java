package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Discount;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.pwp.PWPTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

public class DiscountSetupForm extends DiscountSetup
{    
    public void doBuildTemplate(RunData data, Context context)
    {
    	String sMode = data.getParameters().getString("mode", "");
		String sID = "";
		try
    	{
			if (sMode.equals("view") || sMode.equals("update") || sMode.equals("delete"))
			{
				sID = data.getParameters().getString("id");
				context.put("Discount", DiscountTool.getDiscountByID(sID));
    		}
			else if (sMode.equals("findItem"))
			{
				sID = data.getParameters().getString("id");
				Discount oDisc = new Discount();				
				if (StringUtil.isNotEmpty(sID))
				{
					oDisc = DiscountTool.getDiscountByID(sID);
					data.getParameters().add("mode", "update");
				}
				setProperties(oDisc, data);
				String sItemID = data.getParameters().getString("ItemId"); 
				oDisc.setItemId(sItemID);
				context.put("Discount", oDisc);
			}
    	}
    	catch(Exception _oEx)
    	{
    		_oEx.printStackTrace();
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
        context.put("pwptool",PWPTool.getInstance());
    }
    
    private void setProperties (Discount  _oDiscount,  RunData data)
    {
		try
		{
			data.getParameters().setProperties(_oDiscount);
			_oDiscount.setEventBeginDate(DateUtil.getStartOfDayDate(CustomParser.parseDate(data.getParameters().getString("EventBeginDate"))));
			_oDiscount.setEventEndDate(DateUtil.getEndOfDayDate(CustomParser.parseDate(data.getParameters().getString("EventEndDate"))));
			_oDiscount.setPerItem(data.getParameters().getBoolean("PerItem"));
			_oDiscount.setMultiply(data.getParameters().getBoolean("Multiply"));			
		}
    	catch (Exception _oEx)
    	{
    		_oEx.printStackTrace();
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) + _oEx.getMessage());
    	}
    }
}

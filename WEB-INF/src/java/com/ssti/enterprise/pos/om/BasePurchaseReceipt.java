package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PurchaseReceipt
 */
public abstract class BasePurchaseReceipt extends BaseObject
{
    /** The Peer class */
    private static final PurchaseReceiptPeer peer =
        new PurchaseReceiptPeer();

        
    /** The value for the purchaseReceiptId field */
    private String purchaseReceiptId;
      
    /** The value for the purchaseOrderId field */
    private String purchaseOrderId;
                                                
    /** The value for the vendorDeliveryNo field */
    private String vendorDeliveryNo = "";
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the receiptNo field */
    private String receiptNo;
      
    /** The value for the receiptDate field */
    private Date receiptDate;
      
    /** The value for the vendorId field */
    private String vendorId;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the createBy field */
    private String createBy;
      
    /** The value for the confirmBy field */
    private String confirmBy;
      
    /** The value for the createDate field */
    private Date createDate;
      
    /** The value for the confirmDate field */
    private Date confirmDate;
      
    /** The value for the status field */
    private int status;
      
    /** The value for the totalQty field */
    private BigDecimal totalQty;
      
    /** The value for the remark field */
    private String remark;
                                                
    /** The value for the totalDiscountPct field */
    private String totalDiscountPct = "0";
      
    /** The value for the paymentTypeId field */
    private String paymentTypeId;
      
    /** The value for the paymentTermId field */
    private String paymentTermId;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the currencyRate field */
    private BigDecimal currencyRate= new BigDecimal(1);
      
    /** The value for the isBill field */
    private boolean isBill;
                                                
    /** The value for the courierId field */
    private String courierId = "";
                                                
    /** The value for the fobId field */
    private String fobId = "";
                                                
          
    /** The value for the estimatedFreight field */
    private BigDecimal estimatedFreight= bd_ZERO;
      
    /** The value for the isTaxable field */
    private boolean isTaxable;
      
    /** The value for the isInclusiveTax field */
    private boolean isInclusiveTax;
                                                
          
    /** The value for the fiscalRate field */
    private BigDecimal fiscalRate= new BigDecimal(1);
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the PurchaseReceiptId
     *
     * @return String
     */
    public String getPurchaseReceiptId()
    {
        return purchaseReceiptId;
    }

                                              
    /**
     * Set the value of PurchaseReceiptId
     *
     * @param v new value
     */
    public void setPurchaseReceiptId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.purchaseReceiptId, v))
              {
            this.purchaseReceiptId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated PurchaseReceiptDetail
        if (collPurchaseReceiptDetails != null)
        {
            for (int i = 0; i < collPurchaseReceiptDetails.size(); i++)
            {
                ((PurchaseReceiptDetail) collPurchaseReceiptDetails.get(i))
                    .setPurchaseReceiptId(v);
            }
        }
                                }
  
    /**
     * Get the PurchaseOrderId
     *
     * @return String
     */
    public String getPurchaseOrderId()
    {
        return purchaseOrderId;
    }

                        
    /**
     * Set the value of PurchaseOrderId
     *
     * @param v new value
     */
    public void setPurchaseOrderId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.purchaseOrderId, v))
              {
            this.purchaseOrderId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorDeliveryNo
     *
     * @return String
     */
    public String getVendorDeliveryNo()
    {
        return vendorDeliveryNo;
    }

                        
    /**
     * Set the value of VendorDeliveryNo
     *
     * @param v new value
     */
    public void setVendorDeliveryNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorDeliveryNo, v))
              {
            this.vendorDeliveryNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReceiptNo
     *
     * @return String
     */
    public String getReceiptNo()
    {
        return receiptNo;
    }

                        
    /**
     * Set the value of ReceiptNo
     *
     * @param v new value
     */
    public void setReceiptNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.receiptNo, v))
              {
            this.receiptNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReceiptDate
     *
     * @return Date
     */
    public Date getReceiptDate()
    {
        return receiptDate;
    }

                        
    /**
     * Set the value of ReceiptDate
     *
     * @param v new value
     */
    public void setReceiptDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.receiptDate, v))
              {
            this.receiptDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmBy
     *
     * @return String
     */
    public String getConfirmBy()
    {
        return confirmBy;
    }

                        
    /**
     * Set the value of ConfirmBy
     *
     * @param v new value
     */
    public void setConfirmBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmBy, v))
              {
            this.confirmBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateDate
     *
     * @return Date
     */
    public Date getCreateDate()
    {
        return createDate;
    }

                        
    /**
     * Set the value of CreateDate
     *
     * @param v new value
     */
    public void setCreateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createDate, v))
              {
            this.createDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConfirmDate
     *
     * @return Date
     */
    public Date getConfirmDate()
    {
        return confirmDate;
    }

                        
    /**
     * Set the value of ConfirmDate
     *
     * @param v new value
     */
    public void setConfirmDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.confirmDate, v))
              {
            this.confirmDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalQty
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalQty()
    {
        return totalQty;
    }

                        
    /**
     * Set the value of TotalQty
     *
     * @param v new value
     */
    public void setTotalQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalQty, v))
              {
            this.totalQty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalDiscountPct
     *
     * @return String
     */
    public String getTotalDiscountPct()
    {
        return totalDiscountPct;
    }

                        
    /**
     * Set the value of TotalDiscountPct
     *
     * @param v new value
     */
    public void setTotalDiscountPct(String v) 
    {
    
                  if (!ObjectUtils.equals(this.totalDiscountPct, v))
              {
            this.totalDiscountPct = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeId
     *
     * @return String
     */
    public String getPaymentTypeId()
    {
        return paymentTypeId;
    }

                        
    /**
     * Set the value of PaymentTypeId
     *
     * @param v new value
     */
    public void setPaymentTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeId, v))
              {
            this.paymentTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsBill
     *
     * @return boolean
     */
    public boolean getIsBill()
    {
        return isBill;
    }

                        
    /**
     * Set the value of IsBill
     *
     * @param v new value
     */
    public void setIsBill(boolean v) 
    {
    
                  if (this.isBill != v)
              {
            this.isBill = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FobId
     *
     * @return String
     */
    public String getFobId()
    {
        return fobId;
    }

                        
    /**
     * Set the value of FobId
     *
     * @param v new value
     */
    public void setFobId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fobId, v))
              {
            this.fobId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EstimatedFreight
     *
     * @return BigDecimal
     */
    public BigDecimal getEstimatedFreight()
    {
        return estimatedFreight;
    }

                        
    /**
     * Set the value of EstimatedFreight
     *
     * @param v new value
     */
    public void setEstimatedFreight(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.estimatedFreight, v))
              {
            this.estimatedFreight = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsTaxable
     *
     * @return boolean
     */
    public boolean getIsTaxable()
    {
        return isTaxable;
    }

                        
    /**
     * Set the value of IsTaxable
     *
     * @param v new value
     */
    public void setIsTaxable(boolean v) 
    {
    
                  if (this.isTaxable != v)
              {
            this.isTaxable = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInclusiveTax
     *
     * @return boolean
     */
    public boolean getIsInclusiveTax()
    {
        return isInclusiveTax;
    }

                        
    /**
     * Set the value of IsInclusiveTax
     *
     * @param v new value
     */
    public void setIsInclusiveTax(boolean v) 
    {
    
                  if (this.isInclusiveTax != v)
              {
            this.isInclusiveTax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the FiscalRate
     *
     * @return BigDecimal
     */
    public BigDecimal getFiscalRate()
    {
        return fiscalRate;
    }

                        
    /**
     * Set the value of FiscalRate
     *
     * @param v new value
     */
    public void setFiscalRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.fiscalRate, v))
              {
            this.fiscalRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collPurchaseReceiptDetails
     */
    protected List collPurchaseReceiptDetails;

    /**
     * Temporary storage of collPurchaseReceiptDetails to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initPurchaseReceiptDetails()
    {
        if (collPurchaseReceiptDetails == null)
        {
            collPurchaseReceiptDetails = new ArrayList();
        }
    }

    /**
     * Method called to associate a PurchaseReceiptDetail object to this object
     * through the PurchaseReceiptDetail foreign key attribute
     *
     * @param l PurchaseReceiptDetail
     * @throws TorqueException
     */
    public void addPurchaseReceiptDetail(PurchaseReceiptDetail l) throws TorqueException
    {
        getPurchaseReceiptDetails().add(l);
        l.setPurchaseReceipt((PurchaseReceipt) this);
    }

    /**
     * The criteria used to select the current contents of collPurchaseReceiptDetails
     */
    private Criteria lastPurchaseReceiptDetailsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseReceiptDetails(new Criteria())
     *
     * @throws TorqueException
     */
    public List getPurchaseReceiptDetails() throws TorqueException
    {
              if (collPurchaseReceiptDetails == null)
        {
            collPurchaseReceiptDetails = getPurchaseReceiptDetails(new Criteria(10));
        }
        return collPurchaseReceiptDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseReceipt has previously
     * been saved, it will retrieve related PurchaseReceiptDetails from storage.
     * If this PurchaseReceipt is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getPurchaseReceiptDetails(Criteria criteria) throws TorqueException
    {
              if (collPurchaseReceiptDetails == null)
        {
            if (isNew())
            {
               collPurchaseReceiptDetails = new ArrayList();
            }
            else
            {
                        criteria.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, getPurchaseReceiptId() );
                        collPurchaseReceiptDetails = PurchaseReceiptDetailPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, getPurchaseReceiptId());
                            if (!lastPurchaseReceiptDetailsCriteria.equals(criteria))
                {
                    collPurchaseReceiptDetails = PurchaseReceiptDetailPeer.doSelect(criteria);
                }
            }
        }
        lastPurchaseReceiptDetailsCriteria = criteria;

        return collPurchaseReceiptDetails;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getPurchaseReceiptDetails(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseReceiptDetails(Connection con) throws TorqueException
    {
              if (collPurchaseReceiptDetails == null)
        {
            collPurchaseReceiptDetails = getPurchaseReceiptDetails(new Criteria(10), con);
        }
        return collPurchaseReceiptDetails;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseReceipt has previously
     * been saved, it will retrieve related PurchaseReceiptDetails from storage.
     * If this PurchaseReceipt is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getPurchaseReceiptDetails(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collPurchaseReceiptDetails == null)
        {
            if (isNew())
            {
               collPurchaseReceiptDetails = new ArrayList();
            }
            else
            {
                         criteria.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, getPurchaseReceiptId());
                         collPurchaseReceiptDetails = PurchaseReceiptDetailPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, getPurchaseReceiptId());
                             if (!lastPurchaseReceiptDetailsCriteria.equals(criteria))
                 {
                     collPurchaseReceiptDetails = PurchaseReceiptDetailPeer.doSelect(criteria, con);
                 }
             }
         }
         lastPurchaseReceiptDetailsCriteria = criteria;

         return collPurchaseReceiptDetails;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this PurchaseReceipt is new, it will return
     * an empty collection; or if this PurchaseReceipt has previously
     * been saved, it will retrieve related PurchaseReceiptDetails from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in PurchaseReceipt.
     */
    protected List getPurchaseReceiptDetailsJoinPurchaseReceipt(Criteria criteria)
        throws TorqueException
    {
                    if (collPurchaseReceiptDetails == null)
        {
            if (isNew())
            {
               collPurchaseReceiptDetails = new ArrayList();
            }
            else
            {
                              criteria.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, getPurchaseReceiptId());
                              collPurchaseReceiptDetails = PurchaseReceiptDetailPeer.doSelectJoinPurchaseReceipt(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(PurchaseReceiptDetailPeer.PURCHASE_RECEIPT_ID, getPurchaseReceiptId());
                                    if (!lastPurchaseReceiptDetailsCriteria.equals(criteria))
            {
                collPurchaseReceiptDetails = PurchaseReceiptDetailPeer.doSelectJoinPurchaseReceipt(criteria);
            }
        }
        lastPurchaseReceiptDetailsCriteria = criteria;

        return collPurchaseReceiptDetails;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PurchaseReceiptId");
              fieldNames.add("PurchaseOrderId");
              fieldNames.add("VendorDeliveryNo");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("ReceiptNo");
              fieldNames.add("ReceiptDate");
              fieldNames.add("VendorId");
              fieldNames.add("VendorName");
              fieldNames.add("CreateBy");
              fieldNames.add("ConfirmBy");
              fieldNames.add("CreateDate");
              fieldNames.add("ConfirmDate");
              fieldNames.add("Status");
              fieldNames.add("TotalQty");
              fieldNames.add("Remark");
              fieldNames.add("TotalDiscountPct");
              fieldNames.add("PaymentTypeId");
              fieldNames.add("PaymentTermId");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("IsBill");
              fieldNames.add("CourierId");
              fieldNames.add("FobId");
              fieldNames.add("EstimatedFreight");
              fieldNames.add("IsTaxable");
              fieldNames.add("IsInclusiveTax");
              fieldNames.add("FiscalRate");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PurchaseReceiptId"))
        {
                return getPurchaseReceiptId();
            }
          if (name.equals("PurchaseOrderId"))
        {
                return getPurchaseOrderId();
            }
          if (name.equals("VendorDeliveryNo"))
        {
                return getVendorDeliveryNo();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("ReceiptNo"))
        {
                return getReceiptNo();
            }
          if (name.equals("ReceiptDate"))
        {
                return getReceiptDate();
            }
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("ConfirmBy"))
        {
                return getConfirmBy();
            }
          if (name.equals("CreateDate"))
        {
                return getCreateDate();
            }
          if (name.equals("ConfirmDate"))
        {
                return getConfirmDate();
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("TotalQty"))
        {
                return getTotalQty();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("TotalDiscountPct"))
        {
                return getTotalDiscountPct();
            }
          if (name.equals("PaymentTypeId"))
        {
                return getPaymentTypeId();
            }
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("IsBill"))
        {
                return Boolean.valueOf(getIsBill());
            }
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("FobId"))
        {
                return getFobId();
            }
          if (name.equals("EstimatedFreight"))
        {
                return getEstimatedFreight();
            }
          if (name.equals("IsTaxable"))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals("IsInclusiveTax"))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals("FiscalRate"))
        {
                return getFiscalRate();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PurchaseReceiptPeer.PURCHASE_RECEIPT_ID))
        {
                return getPurchaseReceiptId();
            }
          if (name.equals(PurchaseReceiptPeer.PURCHASE_ORDER_ID))
        {
                return getPurchaseOrderId();
            }
          if (name.equals(PurchaseReceiptPeer.VENDOR_DELIVERY_NO))
        {
                return getVendorDeliveryNo();
            }
          if (name.equals(PurchaseReceiptPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(PurchaseReceiptPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(PurchaseReceiptPeer.RECEIPT_NO))
        {
                return getReceiptNo();
            }
          if (name.equals(PurchaseReceiptPeer.RECEIPT_DATE))
        {
                return getReceiptDate();
            }
          if (name.equals(PurchaseReceiptPeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(PurchaseReceiptPeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(PurchaseReceiptPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(PurchaseReceiptPeer.CONFIRM_BY))
        {
                return getConfirmBy();
            }
          if (name.equals(PurchaseReceiptPeer.CREATE_DATE))
        {
                return getCreateDate();
            }
          if (name.equals(PurchaseReceiptPeer.CONFIRM_DATE))
        {
                return getConfirmDate();
            }
          if (name.equals(PurchaseReceiptPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(PurchaseReceiptPeer.TOTAL_QTY))
        {
                return getTotalQty();
            }
          if (name.equals(PurchaseReceiptPeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(PurchaseReceiptPeer.TOTAL_DISCOUNT_PCT))
        {
                return getTotalDiscountPct();
            }
          if (name.equals(PurchaseReceiptPeer.PAYMENT_TYPE_ID))
        {
                return getPaymentTypeId();
            }
          if (name.equals(PurchaseReceiptPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(PurchaseReceiptPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(PurchaseReceiptPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(PurchaseReceiptPeer.IS_BILL))
        {
                return Boolean.valueOf(getIsBill());
            }
          if (name.equals(PurchaseReceiptPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(PurchaseReceiptPeer.FOB_ID))
        {
                return getFobId();
            }
          if (name.equals(PurchaseReceiptPeer.ESTIMATED_FREIGHT))
        {
                return getEstimatedFreight();
            }
          if (name.equals(PurchaseReceiptPeer.IS_TAXABLE))
        {
                return Boolean.valueOf(getIsTaxable());
            }
          if (name.equals(PurchaseReceiptPeer.IS_INCLUSIVE_TAX))
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
          if (name.equals(PurchaseReceiptPeer.FISCAL_RATE))
        {
                return getFiscalRate();
            }
          if (name.equals(PurchaseReceiptPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(PurchaseReceiptPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPurchaseReceiptId();
            }
              if (pos == 1)
        {
                return getPurchaseOrderId();
            }
              if (pos == 2)
        {
                return getVendorDeliveryNo();
            }
              if (pos == 3)
        {
                return getLocationId();
            }
              if (pos == 4)
        {
                return getLocationName();
            }
              if (pos == 5)
        {
                return getReceiptNo();
            }
              if (pos == 6)
        {
                return getReceiptDate();
            }
              if (pos == 7)
        {
                return getVendorId();
            }
              if (pos == 8)
        {
                return getVendorName();
            }
              if (pos == 9)
        {
                return getCreateBy();
            }
              if (pos == 10)
        {
                return getConfirmBy();
            }
              if (pos == 11)
        {
                return getCreateDate();
            }
              if (pos == 12)
        {
                return getConfirmDate();
            }
              if (pos == 13)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 14)
        {
                return getTotalQty();
            }
              if (pos == 15)
        {
                return getRemark();
            }
              if (pos == 16)
        {
                return getTotalDiscountPct();
            }
              if (pos == 17)
        {
                return getPaymentTypeId();
            }
              if (pos == 18)
        {
                return getPaymentTermId();
            }
              if (pos == 19)
        {
                return getCurrencyId();
            }
              if (pos == 20)
        {
                return getCurrencyRate();
            }
              if (pos == 21)
        {
                return Boolean.valueOf(getIsBill());
            }
              if (pos == 22)
        {
                return getCourierId();
            }
              if (pos == 23)
        {
                return getFobId();
            }
              if (pos == 24)
        {
                return getEstimatedFreight();
            }
              if (pos == 25)
        {
                return Boolean.valueOf(getIsTaxable());
            }
              if (pos == 26)
        {
                return Boolean.valueOf(getIsInclusiveTax());
            }
              if (pos == 27)
        {
                return getFiscalRate();
            }
              if (pos == 28)
        {
                return getCancelBy();
            }
              if (pos == 29)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PurchaseReceiptPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PurchaseReceiptPeer.doInsert((PurchaseReceipt) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PurchaseReceiptPeer.doUpdate((PurchaseReceipt) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collPurchaseReceiptDetails != null)
            {
                for (int i = 0; i < collPurchaseReceiptDetails.size(); i++)
                {
                    ((PurchaseReceiptDetail) collPurchaseReceiptDetails.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key purchaseReceiptId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setPurchaseReceiptId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setPurchaseReceiptId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPurchaseReceiptId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PurchaseReceipt copy() throws TorqueException
    {
        return copyInto(new PurchaseReceipt());
    }
  
    protected PurchaseReceipt copyInto(PurchaseReceipt copyObj) throws TorqueException
    {
          copyObj.setPurchaseReceiptId(purchaseReceiptId);
          copyObj.setPurchaseOrderId(purchaseOrderId);
          copyObj.setVendorDeliveryNo(vendorDeliveryNo);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setReceiptNo(receiptNo);
          copyObj.setReceiptDate(receiptDate);
          copyObj.setVendorId(vendorId);
          copyObj.setVendorName(vendorName);
          copyObj.setCreateBy(createBy);
          copyObj.setConfirmBy(confirmBy);
          copyObj.setCreateDate(createDate);
          copyObj.setConfirmDate(confirmDate);
          copyObj.setStatus(status);
          copyObj.setTotalQty(totalQty);
          copyObj.setRemark(remark);
          copyObj.setTotalDiscountPct(totalDiscountPct);
          copyObj.setPaymentTypeId(paymentTypeId);
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setIsBill(isBill);
          copyObj.setCourierId(courierId);
          copyObj.setFobId(fobId);
          copyObj.setEstimatedFreight(estimatedFreight);
          copyObj.setIsTaxable(isTaxable);
          copyObj.setIsInclusiveTax(isInclusiveTax);
          copyObj.setFiscalRate(fiscalRate);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setPurchaseReceiptId((String)null);
                                                                                                                                                                                          
                                      
                            
        List v = getPurchaseReceiptDetails();
        for (int i = 0; i < v.size(); i++)
        {
            PurchaseReceiptDetail obj = (PurchaseReceiptDetail) v.get(i);
            copyObj.addPurchaseReceiptDetail(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PurchaseReceiptPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PurchaseReceipt\n");
        str.append("---------------\n")
           .append("PurchaseReceiptId    : ")
           .append(getPurchaseReceiptId())
           .append("\n")
           .append("PurchaseOrderId      : ")
           .append(getPurchaseOrderId())
           .append("\n")
           .append("VendorDeliveryNo     : ")
           .append(getVendorDeliveryNo())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("ReceiptNo            : ")
           .append(getReceiptNo())
           .append("\n")
           .append("ReceiptDate          : ")
           .append(getReceiptDate())
           .append("\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("ConfirmBy            : ")
           .append(getConfirmBy())
           .append("\n")
           .append("CreateDate           : ")
           .append(getCreateDate())
           .append("\n")
           .append("ConfirmDate          : ")
           .append(getConfirmDate())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("TotalQty             : ")
           .append(getTotalQty())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("TotalDiscountPct     : ")
           .append(getTotalDiscountPct())
           .append("\n")
           .append("PaymentTypeId        : ")
           .append(getPaymentTypeId())
           .append("\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("IsBill               : ")
           .append(getIsBill())
           .append("\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("FobId                : ")
           .append(getFobId())
           .append("\n")
           .append("EstimatedFreight     : ")
           .append(getEstimatedFreight())
           .append("\n")
           .append("IsTaxable            : ")
           .append(getIsTaxable())
           .append("\n")
           .append("IsInclusiveTax       : ")
           .append(getIsInclusiveTax())
           .append("\n")
           .append("FiscalRate           : ")
           .append(getFiscalRate())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.presentation.actions.transaction;

import java.math.BigDecimal;

import org.apache.torque.util.LargeSelect;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.PettyCash;
import com.ssti.enterprise.pos.om.PettyCashType;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PettyCashTypeTool;
import com.ssti.enterprise.pos.tools.financial.PettyCashTool;
import com.ssti.framework.tools.CustomParser;

public class PettyCashAction extends TransactionSecureAction
{
   	private static final String s_VIEW_PERM    = "View Petty Cash";
	private static final String s_CREATE_PERM  = "Create Petty Cash";
	private static final String s_CANCEL_PERM  = "Cancel Petty Cash";
	
    private static final String[] a_CREATE_PERM  = {s_VIEW_PERM, s_CREATE_PERM};
    private static final String[] a_CANCEL_PERM  = {s_VIEW_PERM, s_CANCEL_PERM};
    private static final String[] a_VIEW_PERM    = {s_VIEW_PERM};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	if(data.getParameters().getString("eventSubmit_doInsert", null) != null)
        {
        	return isAuthorized (data, a_CREATE_PERM);
        }
    	else if(data.getParameters().getString("eventSubmit_doCancel", null) != null)
        {
        	return isAuthorized (data, a_CANCEL_PERM);
        }    	
        else
        {
        	return isAuthorized (data, a_VIEW_PERM);
        }
    }
    
    public void doPerform(RunData data, Context context)
    	throws Exception
    {
    	int iSave = data.getParameters().getInt("save");
    	if (iSave == 1) 
    	{
    		doSave(data,context);
    	}
    	if (iSave == 2) 
    	{
    		doCancel(data,context);
    	}
    }
    
    public void doFind(RunData data, Context context)
        throws Exception
    {
		try
		{
			super.doFind(data);
			String sTypeID  =  data.getParameters().getString ("PettyCashTypeId", "");
			String sEmployeeID  =  data.getParameters().getString ("EmployeeId", "");
			int iStatus = data.getParameters().getInt("Status", -1);
			LargeSelect vTR = PettyCashTool.findData(
				iCond, sKeywords, sTypeID, sEmployeeID, sLocationID,
				dStart, dEnd, iStatus, iLimit);
			data.getUser().setTemp("findPettyCashResult", vTR);		
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
        }
    }

	public void doSave(RunData data, Context context)
        throws Exception
    {
		try
		{
			PettyCash oPettyCash = new PettyCash();
			setProperties (oPettyCash, data);
	
			PettyCashType oType = PettyCashTypeTool.getPettyCashTypeByID (oPettyCash.getPettyCashTypeId());
			if (oType != null) oPettyCash.setIncomingOutgoing(oType.getIncomingOutgoing());
	        
			PettyCashTool.saveData (oPettyCash, null);
        	data.setMessage(LocaleTool.getString("pc_save_success"));
        	context.put("PettyCash", oPettyCash);
        }
        catch(Exception _oEx)
        {
        	handleError (data, LocaleTool.getString("pc_save_failed"), _oEx);
        }
    }

	public void doCancel(RunData data, Context context)
	    throws Exception
	{
		try
		{
			String sID = data.getParameters().getString("ID");
			PettyCash oPC = (PettyCash) PettyCashTool.getPettyCashByID(sID, null);
			if (oPC != null)
			{
				String sCancelRemark = data.getParameters().getString("Remark","");
				oPC.setDescription(oPC.getDescription() + "\n" + sCancelRemark);
				PettyCashTool.cancelPettyCash (oPC, data.getUser().getName(), null);
		        data.setMessage(LocaleTool.getString("pc_cancel_success"));
			}
	    }
	    catch(Exception _oEx)
	    {
        	handleError (data, LocaleTool.getString("pc_cancel_failed"), _oEx);
	    }
	}
	
    private void setProperties (PettyCash _oPettyCash, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oPettyCash);
	    	_oPettyCash.setTransactionDate(
	    	    CustomParser.parseDate(data.getParameters().getString("TransactionDate"))
			);
	    	_oPettyCash.setPettyCashAmount(new BigDecimal(data.getParameters().getDouble("PettyCashAmount")));
	    }
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
package com.ssti.enterprise.pos.presentation.actions.company;

import java.io.File;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.PreferenceManager;
import com.ssti.enterprise.pos.om.CompanyData;
import com.ssti.enterprise.pos.om.CompanyDataPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.IOTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-03-3
 * - change method doSave call UpdateHistoryTool
 * </pre><br>
 */
public class CompanyDataAction extends CompanySecureAction
{
	private static Log log = LogFactory.getLog(CompanyDataAction.class);

    public void doPerform(RunData data, Context context)
    {
    }
    
	public void doSave(RunData data, Context context)
        throws Exception
    {
		try 
		{
			CompanyData oCompany = null;
			CompanyData oOld = null;
			String sCompanyID = data.getParameters().getString("CompanyID", null);
			if (StringUtil.isEmpty(sCompanyID)) 
			{
				oCompany = new CompanyData();
			}
			else 
			{
				Criteria oCrit = new Criteria();
				oCrit.add (CompanyDataPeer.COMPANY_DATA_ID, sCompanyID); 
				List vData = CompanyDataPeer.doSelect (oCrit);
				oCompany = (CompanyData) vData.get(0);
				oOld = oCompany.copy();
			}
			data.getParameters().setProperties(oCompany);
			if (StringUtil.isEmpty(sCompanyID)) 
			{
				//Generate Sys ID
				oCompany.setCompanyDataId (IDGenerator.generateSysID());
	    	}
            processPictureUpload(data, oCompany);
	        oCompany.save();
	        	        
	        UpdateHistoryTool.createHistory(oOld, oCompany, data.getUser().getName(), null);
	        
	        PreferenceManager.getInstance().refreshCache(oCompany);
	        
	        context.put("Company", oCompany);
        	data.setMessage (LocaleTool.getString("company_saved"));
        }
        catch (Exception _oEx) 
        {
        	String sError = LocaleTool.getString("company_save_failed") + _oEx.getMessage();
        	_oEx.printStackTrace();
        	log.error (sError, _oEx);
        	data.setMessage (sError);
        }
    }

	private void processPictureUpload ( RunData data, CompanyData _oData)
    	throws Exception
    {
		try
    	{
			//get file item
			FileItem oFileItem = data.getParameters().getFileItem("CompanyLogo");
			String sSaveFilePath = writeFile(oFileItem, data);
			if(StringUtil.isNotEmpty(sSaveFilePath)) _oData.setCompanyLogo (sSaveFilePath);

			//get file item
			oFileItem = data.getParameters().getFileItem("MainLogo");
			sSaveFilePath = writeFile(oFileItem, data);			
			if(StringUtil.isNotEmpty(sSaveFilePath)) _oData.setMainLogo (sSaveFilePath);        						

			//get file item
			oFileItem = data.getParameters().getFileItem("PosLogo");
			sSaveFilePath = writeFile(oFileItem, data);    			
			if(StringUtil.isNotEmpty(sSaveFilePath)) _oData.setPosLogo (sSaveFilePath);        		
		}
		catch (Exception _oEx)
    	{
    	    _oEx.printStackTrace();
        	throw new Exception (LocaleTool.getString(s_UPLOAD_PIC_FAILED) +  _oEx.getMessage());
        }
	}
	
	private String writeFile(FileItem oFileItem, RunData data)
		throws Exception
	{
		String sSeparator  = System.getProperty("file.separator");
		String sPathDest   = PreferenceTool.getPicturePath();
		String sFilePath   = IOTool.getRealPath(data, sPathDest);		
		String sSaveFilePath = "";
		
		if (oFileItem != null)
		{
			String sFileName = oFileItem.getName();	
			if(StringUtil.isNotEmpty(sFileName)) 
			{
				int iSeparatorIndex = sFileName.lastIndexOf (sSeparator.charAt(0));
				if (iSeparatorIndex > 0)
				{
					sFileName = sFileName.substring (iSeparatorIndex);
				}
				sFilePath = sFilePath + sSeparator + sFileName;					
				log.debug("File Path:" + sFilePath);
				
				oFileItem.write (new File (sFilePath)); //save picture file
				
				StringBuilder oSB = new StringBuilder();
				oSB.append(data.getContextPath()).append(sPathDest).append(sFileName);
				
				sSaveFilePath = (oSB.toString()).replace('\\','/');
				
				log.debug("File Path:" + sSaveFilePath);					
			}
		}
		return sSaveFilePath;
	}    
}

package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Iterator;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.UnitPeer;
import com.ssti.framework.tools.SortingTool;
import com.ssti.framework.tools.StringUtil;

public class UnitManager
{   
	private static Log log = LogFactory.getLog(UnitManager.class);
	 
    private static UnitManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private UnitManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static UnitManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (UnitManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new UnitManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllUnit ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
    	Element oElement = oCache.get("AllUnit");
		if (oElement == null)
		{	
			List vData = getAllBaseUnit();
			List vNonBase = getAllNonBaseUnit();
			SortingTool.sort(vNonBase, "unitCode");
			Iterator oIter = vNonBase.iterator();
			while (oIter.hasNext())
			{
				vData.add(oIter.next());
			}
			oCache.put( new Element ("AllUnit", (Serializable) vData)); 
			return vData;
		}
		else {
			return (List) oElement.getValue();
		}
	}	

	public List getAllBaseUnit()
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("BaseUnit");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(UnitPeer.BASE_UNIT, true);
			List vData = UnitPeer.doSelect(oCrit);
			oCache.put( new Element ("BaseUnit", (Serializable) vData));  
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}		
	}

	public List getAllNonBaseUnit()
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("NonBaseUnit");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(UnitPeer.BASE_UNIT, false);
			List vData = UnitPeer.doSelect(oCrit);
			oCache.put( new Element ("NonBaseUnit", (Serializable) vData));  
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}		
	}
	
	public Unit getUnitByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Unit" + _sID);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(UnitPeer.UNIT_ID, _sID);
				List vData = UnitPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put( new Element ("Unit" + _sID, (Serializable) vData.get(0))); 
					return (Unit) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (Unit) oElement.getValue();
		}
	}

	public Unit getUnitByCode(String _sCode, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("UnitByCode" + _sCode);
		if (oElement == null)
		{	
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(UnitPeer.UNIT_CODE, _sCode);
				List vData = UnitPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put( new Element ("UnitByCode" + _sCode, (Serializable) vData.get(0))); 
					return (Unit) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (Unit) oElement.getValue();
		}
	}
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Unit _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getUnitId()) ) 
        {
            oCache.remove("Unit" + _oData.getUnitId());
            oCache.remove("UnitByCode" + _oData.getUnitCode());
            
        }
        oCache.put( new Element ("Unit" + _oData.getUnitId(), _oData));
        oCache.put( new Element ("UnitByCode" + _oData.getUnitCode(), _oData));

        oCache.remove("AllUnit");
        oCache.remove("BaseUnit");
        oCache.remove("NonBaseUnit");
    }
	
	//for delete
	public void refreshCache (String _sID, String _sCode) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        oCache.remove("Unit" + _sID);
        oCache.remove("Unit" + _sCode);
        oCache.remove("AllUnit");
        oCache.remove("BaseUnit");
        oCache.remove("NonBaseUnit");
    } 
}

package com.ssti.enterprise.medical.om;


import org.apache.torque.om.Persistent;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class MedConfig
    extends com.ssti.enterprise.medical.om.BaseMedConfig
    implements Persistent
{
}

package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PriceList;
import com.ssti.enterprise.pos.om.PriceListDetail;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.PriceListTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/excel/helper/ItemLoader.java,v $
 * Purpose: load item data from excel 
 *
 * @author  $Author: albert $
 * @version $Id: ItemLoader.java,v 1.23 2009/05/04 01:38:29 albert Exp $
 *
 * $Log: ItemLoader.java,v $
 * Revision 1.23  2009/05/04 01:38:29  albert
 * *** empty log message ***
 *
 *
 */
public class PriceListLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( PriceListLoader.class );

	private static final String s_START_HEADING = "Price Code";	
		
	private String m_sUserName;
	private int m_iStartRow = 0;
	private int m_iTotalRows = 0;
	private Map m_mPL = null;
	
	public PriceListLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
    	throws Exception
    {
		Item oOldItemRef = null;		
		if (started (_oRow, _iIdx))				
		{
			String sPLCode = getString(_oRow, 0); 
			PriceList oPL = null;
			String sPLID = "";
			if (m_mPL == null) 
			{
				m_mPL = new HashMap();
				oPL = PriceListTool.getPriceListByCode(sPLCode,null);
				oPL.setUpdateDate(new Date());
				oPL.setRemark(oPL.getRemark() + AppAttributes.s_LINE_SEPARATOR + CustomFormatter.formatDateTime(oPL.getUpdateDate()) + " Update By:" + m_sUserName);
				oPL.save();
				m_mPL.put(sPLCode, oPL);
			}
			else
			{
				oPL = (PriceList) m_mPL.get(sPLCode);
				if(oPL == null) 
				{
					oPL = PriceListTool.getPriceListByCode(sPLCode,null);
					oPL.setUpdateDate(new Date());
					oPL.setRemark(oPL.getRemark() + AppAttributes.s_LINE_SEPARATOR + CustomFormatter.formatDateTime(oPL.getUpdateDate()) + " Update By:" + m_sUserName);
					oPL.save();									
					m_mPL.put(sPLCode, oPL);
				}
			}
			if (oPL != null)
			{
				sPLID = oPL.getPriceListId();
				String sItemCode = getString(_oRow, 1); 
				String sDiscount = getString(_oRow, 6); 
				BigDecimal dPrice = getBigDecimal(_oRow, 7);
				
				System.out.println (" disc " + sDiscount + " dprice " +  dPrice);
				
				if (StringUtil.isNotEmpty(sItemCode))
				{
					Item oItem = ItemTool.getItemByCode(sItemCode);
					if (oItem != null)
					{
						if (PriceListTool.isItemExistInActivePL(oItem.getItemId(), sPLID))
						{
							m_iRejected++;
						    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
						    m_oRejected.append (StringUtil.left(sItemCode,15));
						    m_oRejected.append (StringUtil.left(oItem.getItemName() + " Exist in Active PL ",30));
						    m_oRejected.append (StringUtil.right(CustomFormatter.formatNumber(dPrice),30));				    
						    m_oRejected.append ("\n");
						}
						else
						{
							PriceListDetail oPLD = PriceListTool.getDetailByPriceListAndItemID(sPLID, oItem.getItemId(), null);
							if (oPLD == null)
							{
								oPLD = new PriceListDetail();
								oPLD.setPriceListDetailId(IDGenerator.generateSysID());
								oPLD.setItemId(oItem.getItemId());
								oPLD.setItemCode(oItem.getItemCode());
								oPLD.setItemName(oItem.getItemName());
								oPLD.setOldPrice(oItem.getItemPrice());							
							    m_iNewData++;
							    m_oNewData.append (StringUtil.left(m_iNewData + ".", 5));					
							    m_oNewData.append (StringUtil.left(sItemCode,15));
							    m_oNewData.append (StringUtil.left(oItem.getItemName(),30));
							    m_oNewData.append (StringUtil.right(CustomFormatter.formatNumber(dPrice),30));				    
							    m_oNewData.append ("\n");
							}
							else
							{
							    m_iUpdated++;
							    m_oUpdated.append (StringUtil.left(m_iUpdated + ".", 5));					
							    m_oUpdated.append (StringUtil.left(sItemCode,15));
							    m_oUpdated.append (StringUtil.left(oItem.getItemName(),30));
							    m_oUpdated.append (StringUtil.right(CustomFormatter.formatNumber(dPrice),30));				    
							    m_oUpdated.append ("\n");
							}
							oPLD.setPriceListId(sPLID);
							oPLD.setDiscountAmount(sDiscount);
							oPLD.setNewPrice(dPrice);						
							oPLD.save();
						}
						//log.debug(oPLD);
					}
				}
			}
		}    
    }
}

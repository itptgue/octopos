package com.ssti.enterprise.pos.tools.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Validate VendorBalance and AccountPayable
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: APValidator.java,v 1.4 2009/05/04 02:04:05 albert Exp $ <br>
 *
 * <pre>
 * $Log: APValidator.java,v $
 * Revision 1.4  2009/05/04 02:04:05  albert
 * *** empty log message ***
 *
 * Revision 1.3  2007/04/17 13:05:40  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class IDTimestampCheck 
{
	public static void main(String[] args) 
	{
		if (args != null && args.length > 1)
		{
			String _sID = args[0];
			String _sLocCode = args[1];
			if (_sID != null && _sID.length() > 0)
			{			    
			    System.out.println("ID Timestamp " + getTime(_sID, _sLocCode));			    
			}
		}
		else
		{
			System.err.println("Usage java IDTimeCheck [id] [loccode]");
		}
	}
	
	public static String getTime(String _sID, String _sLocCode)
	{
		if (_sID != null && _sID.length() > 0 && _sLocCode != null)
		{
			int iStart = _sLocCode.length();			    
			String sID = _sID.substring(iStart);
			sID = sID.substring(0, sID.length() - 8);   
			
		    System.out.println("ID: " + _sID +  " LOC: " + _sLocCode + " TIMESTAMP: " + sID);
			
			Date d = new Date();
			d.setTime(Long.parseLong(sID));
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return df.format(d);
		}
		return "Invalid ID / Location Code";
	}
}

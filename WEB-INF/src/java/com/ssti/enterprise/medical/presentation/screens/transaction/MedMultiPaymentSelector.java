package com.ssti.enterprise.medical.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.MedicineSales;
import com.ssti.enterprise.medical.tools.MedicalAttributes;
import com.ssti.enterprise.pos.om.DiscountCoupon;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.VoucherNo;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.DiscountTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.PaymentTypeTool;
import com.ssti.enterprise.pos.tools.PaymentVoucherTool;
import com.ssti.enterprise.pos.tools.sales.InvoicePaymentTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/POSMultiPaymentSelector.java,v $
 * Purpose: POS Multi Payment Selector Screen Class
 *
 * @author  $Author: albert $
 * @version $Id: POSMultiPaymentSelector.java,v 1.8 2006/03/22 15:47:32 albert Exp $
 * @see MedicineSales
 *
 * $Log: POSMultiPaymentSelector.java,v $
 * Revision 1.8  2006/03/22 15:47:32  albert
 * *** empty log message ***
 *
 * Revision 1.7  2006/02/09 12:32:52  albert
 * refactor multiple payment (disallow if not setup properly)
 * remove list from session if multi payment type/term not setup
 *
 * Revision 1.6  2005/12/27 04:14:31  albert
 * *** empty log message ***
 *
 */

public class MedMultiPaymentSelector extends TransactionSecureScreen implements MedicalAttributes
{
	private static int i_OP_SET = 1;
	private static int i_OP_ADD_VCH = 2;
	private static int i_OP_DEL_VCH = 3;
	
	List vVch;
	List vPmt;
	HttpSession oSes;
	MedicineSales oTR;
	double dTotalTrans = 0;
	
    public void doBuildTemplate(RunData data, Context context)
    {
        int iOp = data.getParameters().getInt ("op", 0);        
        String sTR = data.getParameters().getString("transSesCode",s_TR);
        oSes = data.getSession();
        context.put("invoicepayment", InvoicePaymentTool.getInstance());
        
        if (oSes != null)
        {
            vPmt = (List) oSes.getAttribute(s_PMT);
            vVch = (List) oSes.getAttribute(s_PMT_VCH);            	
            oTR = (MedicineSales) oSes.getAttribute(s_MTR);
            if(oTR != null)
            {
	        	try 
		    	{	       
	        		dTotalTrans = oTR.getTotalAmount().doubleValue();
	                if (iOp == i_OP_SET)
	                {
	                    setPayment(data, context);
	                }    
	                else if (iOp == i_OP_ADD_VCH)
	                {
	                    addVoucher(data);
	                }
	                else if (iOp == i_OP_DEL_VCH)
	                {
	                    delVoucher(data);
	                }	    	 
	                	                
	                oSes.setAttribute(s_PMT_VCH, vVch);
	                context.put(s_PMT, vPmt);
	                context.put(s_PMT_VCH, vVch);
	                context.put(s_TR, oTR);
	                context.put("dTotalVoucher", sumTotalVoucher());
	                context.put("dTotalTrans", dTotalTrans);
	                context.put("dTotal", Calculator.sub(dTotalTrans,sumTotalVoucher()));                
	                context.put("dTotalChange", sumTotalChange());
	                context.put("dTotalPmt", sumTotalPayment());
		    	}
		    	catch (Exception _oEx)
		    	{
		    		_oEx.printStackTrace();
		    		data.setMessage("ERROR : " + _oEx.getMessage());
		    	}
            }
            else
            {
	    		data.setMessage("Invalid Transaction, Please Log Out!");            	
            }
        }
    }
    
    private void recalcPayment(RunData data, InvoicePayment _oPmtVch, boolean _bAdd)
	{
    	double dTotalVoucher = sumTotalVoucher();
		if(vPmt != null && vPmt.size() > 0)
		{	
			double dPmtVch = _oPmtVch.getPaymentAmount().doubleValue();
			for (int i = 0; i < vPmt.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment)vPmt.get(i);
				double dPmt = oPmt.getPaymentAmount().doubleValue();
				if(dPmt > dPmtVch &&  !oPmt.getIsVoucher() && !oPmt.getIsCoupon() && !oPmt.getIsPointReward())
				{
					if(_bAdd) //add voucher reduce payment
					{
						oPmt.setPaymentAmount(new BigDecimal(dPmt - dPmtVch));
					}
					else //del voucher add payment
					{
						oPmt.setPaymentAmount(new BigDecimal(dPmt + dPmtVch));						
					}
					break;
				}
			}
		}
	}

	private void delVoucher(RunData data)
	{
    	if (vVch != null &&  vVch.size() > 0)
    	{
    		int iDel = data.getParameters().getInt("VchDelIdx");
    		if(iDel < vVch.size())
    		{
    			InvoicePayment oPmt = (InvoicePayment)vVch.get(iDel);
                recalcPayment(data, oPmt, false);    			
    			vVch.remove(iDel);
    		}
    	}
	}

	private void addVoucher(RunData data)
    	throws Exception
	{
		if(vVch == null) vVch = new ArrayList();
		String sPTypeID = data.getParameters().get("VchPaymentTypeId"); 		
    	String sVoucherNo = data.getParameters().get("VoucherNo");    	
    	PaymentType oPType = PaymentTypeTool.getPaymentTypeByID(sPTypeID);
    	log.debug("VoucherNo:" + sVoucherNo + " PType: " + oPType.getCode());
    	
    	if(vVch.size() == 0 || (vVch.size() > 0 &&!isExits(sVoucherNo)))
    	{
    		if (oPType.getIsCoupon())
    		{
		    	processCoupon(data, sVoucherNo);
    		}
    		else
    		{
    			processVoucher(data, sVoucherNo);
    		}
    	}
	}

	private void processCoupon(RunData data, String sVoucherNo)
		throws Exception
	{
		DiscountCoupon oDC = DiscountTool.getByCouponNo(sVoucherNo, null);	    	
		if(oDC != null && StringUtil.isEmpty(oDC.getUseTransId()))
		{
			if(oDC.getCouponType() == 1)
			{
				Date dToday = new Date();
				if (DateUtil.isAfter(dToday, oDC.getValidFrom()))
				{
					if(DateUtil.isBefore(dToday, oDC.getValidTo()))
					{
						InvoicePayment oPmt = new InvoicePayment();
						String sTypeID = data.getParameters().getString("VchPaymentTypeId");
						String sTermID = PaymentTermTool.getDefaultPaymentTerm().getId();

						oPmt.setPaymentTypeId(sTypeID);
						oPmt.setPaymentTermId(sTermID);                            
						oPmt.setPaymentAmount(oDC.getCouponValue());
						oPmt.setVoucherId(oDC.getDiscountCouponId());
						oPmt.setReferenceNo(sVoucherNo);
						oPmt.setBankId("");
						oPmt.setBankIssuer("");
						oPmt.setApprovalNo("");                                                      
						oPmt.setDueDate(dToday);
						oPmt.setTransTotalAmount(data.getParameters().getBigDecimal("TotalAmount"));
						vVch.add(oPmt);    			

						recalcPayment(data, oPmt, true);
						log.debug("VoucherPmt:" + oPmt);		        			        		
					}
					else    		
					{
						setError("Invalid Data, Coupon Already Expired! Valid To: " + CustomFormatter.fmt(oDC.getValidTo()), data);    			
					}
				}
				else
				{
					setError("Invalid Data, Coupon Not Effective Yet! Valid From: " + CustomFormatter.fmt(oDC.getValidFrom()), data);    			
				}
			}
			else
			{
				setError("Invalid Coupon Type ", data);
			}
		}
		else
		{
			setError("Invalid Data, Coupon is Already Used", data);
		}
	}
	
	private void processVoucher(RunData data, String sVoucherNo)
		throws Exception
	{
		VoucherNo oVN = PaymentVoucherTool.getByVoucherNo(sVoucherNo, null);	    	
		if(oVN != null && StringUtil.isEmpty(oVN.getUseTransId()))
		{
			Date dToday = new Date();
			if (DateUtil.isAfter(dToday, oVN.getValidFrom()))
			{
				if(DateUtil.isBefore(dToday, oVN.getValidTo()))
				{
					InvoicePayment oPmt = new InvoicePayment();
					String sTypeID = data.getParameters().getString("VchPaymentTypeId");
					String sTermID = PaymentTermTool.getDefaultPaymentTerm().getId();

					oPmt.setPaymentTypeId(sTypeID);
					oPmt.setPaymentTermId(sTermID);                            
					oPmt.setPaymentAmount(oVN.getAmount());
					oPmt.setVoucherId(oVN.getVoucherNoId());
					oPmt.setReferenceNo(sVoucherNo);
					oPmt.setBankId("");
					oPmt.setBankIssuer("");
					oPmt.setApprovalNo("");                                                      
					oPmt.setDueDate(dToday);
					oPmt.setTransTotalAmount(data.getParameters().getBigDecimal("TotalAmount"));
					vVch.add(oPmt);    			

					recalcPayment(data, oPmt, true);
					log.debug("VoucherPmt:" + oPmt);		        			        		
				}
				else    		
				{
					setError("Invalid Data, Voucher Already Expired! Valid To: " + CustomFormatter.fmt(oVN.getValidTo()), data);    			
				}
			}
			else
			{
				setError("Invalid Data, Voucher Not Effective Yet! Valid From: " + CustomFormatter.fmt(oVN.getValidFrom()), data);    			
			}		
		}
		else
		{
			if(oVN == null)
			{
				setError("Invalid Data, Voucher Not Found", data);				
			}
			else
			{
				setError("Invalid Data, Voucher is Already Used", data);
			}
		}
	}

	private boolean isExits(String _sVoucherNo)
	{
		if(vVch != null && vVch.size() > 0)
		{
			for (int i = 0; i < vVch.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment)vVch.get(i);
				if(StringUtil.isEqual(oPmt.getReferenceNo(), _sVoucherNo))
				{
					return true;
				}				
			}
		}
		return false;
	}

	private double sumTotalVoucher()
	{
		double dTotalVoucher = 0;
		if(vVch != null && vVch.size() > 0)
		{
			for (int i = 0; i < vVch.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment)vVch.get(i);
				dTotalVoucher += oPmt.getPaymentAmount().doubleValue();			
			}
		}
		return dTotalVoucher;
	}

	private double sumTotalChange()
	{
		double dTotal = 0;
		if(vPmt != null && vPmt.size() > 0)
		{
			for (int i = 0; i < vPmt.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment)vPmt.get(i);
				dTotal += oPmt.getChangeAmount();			
			}
		}
		return dTotal;
	}

	private double sumTotalPayment()
	{
		double dTotal = 0;
		if(vPmt != null && vPmt.size() > 0)
		{
			for (int i = 0; i < vPmt.size(); i++)
			{
				InvoicePayment oPmt = (InvoicePayment)vPmt.get(i);
				if(!oPmt.getIsCash())
				{
					dTotal += oPmt.getPaymentAmount().doubleValue();			
				}
				else
				{
					dTotal += oPmt.getCashAmount().doubleValue();
				}
			}
		}
		return dTotal;
	}
	
	private void setError(String _sError, RunData data)
	{
		data.setMessage(_sError);
	}

	private void setPayment(RunData data, Context context)
    	throws Exception
    {
        int iNo = data.getParameters().getInt("TotalNo");
        vPmt = new ArrayList(iNo);
        for (int i = 0; i < iNo; i++)
        {
            double dAmount = data.getParameters().getDouble("Amount" + i, 0);
            if (dAmount != 0)
            {
                String sTypeID = data.getParameters().getString("PaymentTypeId" + i);
                String sTermID = data.getParameters().getString("PaymentTermId" + i);
                
                InvoicePayment oPmt = new InvoicePayment();
                oPmt.setPaymentTypeId(sTypeID);
                oPmt.setPaymentTermId(sTermID);                            
                oPmt.setPaymentAmount(new BigDecimal (dAmount));
                oPmt.setVoucherId(data.getParameters().getString("VoucherId" + i, ""));
                
                //if cash
                if(oPmt.getIsCash())
                {                	
                	double dCashAmt = data.getParameters().getDouble("CashAmount" + i, 0);
                	oPmt.setCashAmount(new BigDecimal(dCashAmt));
                	log.debug(i + " TYPE CASH: " + dCashAmt);                    
                }
                
                //parameters needed for non direct sales multiple payment
                oPmt.setBankId(data.getParameters().getString("BankId" + i, ""));
                oPmt.setBankIssuer(data.getParameters().getString("BankIssuer" + i, ""));
                oPmt.setReferenceNo(data.getParameters().getString("ReferenceNo" + i, "")); 
                oPmt.setApprovalNo(data.getParameters().getString("ApprovalNo" + i, "")); 
                
                Date dDueDate = CustomParser.parseDate(data.getParameters().getString("DueDate" + i));                
                log.debug("TypeID " + sTypeID + " Amt: " + dAmount + " RefNO: " + data.getParameters().getString("ReferenceNo" + i, ""));
                
                if (dDueDate == null) //let's set due date earlier to gave more info to user
                {
                	Calendar cDue = new GregorianCalendar();
                	if (oTR != null && oTR.getTransactionDate() != null) cDue.setTime (oTR.getTransactionDate());
                	cDue.add (Calendar.DATE, PaymentTermTool.getNetPaymentDaysByID(oPmt.getPaymentTermId()));
                }                        
                oPmt.setDueDate(dDueDate);
                oPmt.setTransTotalAmount(data.getParameters().getBigDecimal("TotalAmount"));
                vPmt.add(oPmt);
            }
        }                 
        if (vVch != null && vVch.size() > 0)
        {
        	vPmt.addAll(vVch);
        }
        context.put("PaymentValid", Boolean.valueOf(true));
        
        if (vPmt.size() ==  1)
        {
            InvoicePayment oInv = (InvoicePayment) vPmt.get(0);
            oTR.setPaymentTypeId(oInv.getPaymentTypeId());
            oTR.setPaymentTermId(oInv.getPaymentTermId());
            
            context.put("TypeId",   oInv.getPaymentTypeId());
            context.put("TermId",   oInv.getPaymentTermId());
            context.put("TypeCode", PaymentTypeTool.getPaymentTypeCodeByID(oInv.getPaymentTypeId()));
            context.put("TermCode", PaymentTermTool.getPaymentTermCodeByID(oInv.getPaymentTermId()));
        }
        else if (vPmt.size() > 1) 
        {
            PaymentTerm oTermMulti = PaymentTermTool.getMultiType();
            PaymentType oTypeMulti = PaymentTypeTool.getMultiType();            
            if (oTermMulti != null && oTypeMulti != null)
            {
                context.put("TypeId",   oTypeMulti.getPaymentTypeId());
                context.put("TypeCode", oTypeMulti.getPaymentTypeCode()); 
                context.put("TermId",   oTermMulti.getPaymentTermId());
                context.put("TermCode", oTermMulti.getPaymentTermCode());
            }
            else
            {
            	context.put("PaymentValid", Boolean.valueOf(false));
            	vPmt = null;
            	data.setMessage(LocaleTool.getString("multi_na"));
            }
        }                           
        else if (vPmt.size() <= 0)
        {
        	context.put("PaymentValid", Boolean.valueOf(false));
        	vPmt = null;
        }
        oTR.setIsPaymentSet();
        oTR.setInvPayments(vPmt);
        log.debug(vPmt);        
        oSes.setAttribute(s_PMT, vPmt);
    }
}
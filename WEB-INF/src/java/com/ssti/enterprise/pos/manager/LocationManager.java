package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.framework.tools.StringUtil;

public class LocationManager
{   
	private static Log log = LogFactory.getLog(LocationManager.class);
	 
    private static LocationManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private LocationManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static LocationManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (LocationManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new LocationManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllLocation ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllLocation");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (LocationPeer.LOCATION_NAME);	
			List vData = LocationPeer.doSelect(oCrit);
			oCache.put(new Element ("AllLocation", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public Location getLocationByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Location" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(LocationPeer.LOCATION_ID, _sID);
				List vData = LocationPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Location" + _sID, (Serializable)vData.get(0))); 
					return (Location) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Location) oElement.getValue();
		}
	}

	public Location getLocationByCode(String _sCode, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("LocationByCode" + _sCode);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(LocationPeer.LOCATION_CODE, _sCode);
				List vData = LocationPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("LocationByCode" + _sCode, (Serializable)vData.get(0))); 
					return (Location) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Location) oElement.getValue();
		}
	}	
	
	public List getBySiteID(String _sSiteID, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("LocationSite" + _sSiteID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sSiteID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(LocationPeer.SITE_ID, _sSiteID);
				List vData = LocationPeer.doSelect(oCrit, _oConn);				
				oCache.put(new Element("LocationSite" + _sSiteID, (Serializable)vData)); 
				return vData;				
			}
			return getAllLocation();
		}
		else {
			return (List) oElement.getValue();
		}
	}

	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Location _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getLocationId())) 
        {
            oCache.remove("Location" + _oData.getLocationId());
            oCache.remove("LocationByCode" + _oData.getLocationCode());
            oCache.put(new Element ("Location" + _oData.getLocationId(), _oData));
            oCache.put(new Element ("LocationByCode" + _oData.getLocationCode(), _oData));
            oCache.remove("LocationSite" + _oData.getSiteId());
        }
        else
        {
            oCache.removeAll();
        }
        oCache.remove("AllLocation");
    }
	
	//for delete
	public void refreshCache (String _sID, String _sCode, String _sSiteID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("Location" + _sID);
        oCache.remove("LocationByCode" + _sCode);        
        oCache.remove("LocationSite" + _sSiteID);
        oCache.remove("AllLocation");        
    } 
}
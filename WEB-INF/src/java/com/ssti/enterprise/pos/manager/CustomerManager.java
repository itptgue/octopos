package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerBalance;
import com.ssti.enterprise.pos.om.CustomerBalancePeer;
import com.ssti.enterprise.pos.om.CustomerCfield;
import com.ssti.enterprise.pos.om.CustomerCfieldPeer;
import com.ssti.enterprise.pos.om.CustomerFieldPeer;
import com.ssti.enterprise.pos.om.CustomerPeer;
import com.ssti.enterprise.pos.om.CustomerType;
import com.ssti.enterprise.pos.om.CustomerTypePeer;
import com.ssti.framework.tools.StringUtil;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: CustomerManager.java,v 1.8 2008/03/03 01:54:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: CustomerManager.java,v $
 *
 * 2015-09-17
 * -add CustomerCfield methods
 * </pre><br>
 */
public class CustomerManager
{   
	private static Log log = LogFactory.getLog(CustomerManager.class);
	 
    private static CustomerManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "customer";      

    private CustomerManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static CustomerManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (CustomerManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new CustomerManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllCustomerType ()
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllCustomerType");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (CustomerTypePeer.CUSTOMER_TYPE_CODE);	
			List vData = CustomerTypePeer.doSelect(oCrit);
			oCache.put(new Element("AllCustomerType", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	
    
	public List getAllCustomer ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllCustomer");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addDescendingOrderByColumn (CustomerPeer.IS_DEFAULT);	
			oCrit.addAscendingOrderByColumn (CustomerPeer.CUSTOMER_NAME);	
			oCrit.add(CustomerPeer.IS_ACTIVE, true);
			List vData = CustomerPeer.doSelect(oCrit);
			oCache.put(new Element("AllCustomer", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public CustomerType getCustomerTypeByID(String _sID, Connection _oConn)
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("CustomerType" + _sID);
		if (oElement == null )
		{		
			if (StringUtil.isNotEmpty(_sID))
			{			
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerTypePeer.CUSTOMER_TYPE_ID, _sID);
				List vData = CustomerTypePeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("CustomerType" + _sID, (Serializable)vData.get(0))); 
					return (CustomerType) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (CustomerType) oElement.getValue();
		}
	}	

	public Customer getDefaultCustomer()
		throws Exception
	{
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("DefaultCustomer");
		if (oElement == null )
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(CustomerPeer.IS_DEFAULT, true);
			List vData = CustomerPeer.doSelect(oCrit);
		    if (vData.size() > 0) 
		    {
		    	oCache.put(new Element ("DefaultCustomer", (Serializable)vData.get(0))); 
				return (Customer) vData.get(0);
			}
		    else //no default customer, yet put in  cache to prevent unnecessary DB access
		    {
		    	oCache.put(new Element ("DefaultCustomer", null));  		    	
		    }
		}
		else
		{
			return (Customer) oElement.getValue();		
		}
		return null;
	}	
		
	public Customer getCustomerByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Customer" + _sID);
		if (oElement == null )
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerPeer.CUSTOMER_ID, _sID);
				List vData = CustomerPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Customer" + _sID, (Serializable)vData.get(0))); 
					return (Customer) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (Customer) oElement.getValue();
		}
	}
	
	public Customer getCustomerByCode(String _sCode, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Customer" + _sCode);
		if (oElement == null )
		{		
			if (StringUtil.isNotEmpty(_sCode))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerPeer.CUSTOMER_CODE, _sCode);
				List vData = CustomerPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("CustomerByCode" + _sCode, (Serializable)vData.get(0))); 
					return (Customer) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (Customer) oElement.getValue();
		}
	}

	public CustomerBalance getCustomerBalance(String _sID, Connection _oConn)
		throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE);
		Element oElement = oCache.get("CustomerBalance" + _sID);
		
		if (oElement == null)
		{
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerBalancePeer.CUSTOMER_ID, _sID);
				List vData = CustomerBalancePeer.doSelect(oCrit, _oConn);
				if(vData.size() > 0)
				{
					CustomerBalance oBalance = (CustomerBalance) vData.get(0);
					oCache.put(new Element ("CustomerBalance" + _sID, (Serializable)oBalance)); 
					return oBalance;
				}
				else
				{
					oCache.put(new Element ("CustomerBalance" + _sID, null)); 				
				}
			}
			return null;
		}
		return (CustomerBalance) oElement.getValue();
	}	
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Customer _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if (_oData != null && StringUtil.isNotEmpty(_oData.getCustomerId())) 
        {
            oCache.remove("Customer" + _oData.getCustomerId());
            oCache.remove("CustFields" + _oData.getCustomerId());
            oCache.remove("CustomerByCode" + _oData.getCustomerCode());
            if (_oData.getIsDefault())
            {
                oCache.put(new Element ("DefaultCustomer",  _oData));
            }
            else
            {
            	oCache.remove("DefaultCustomer");
            }
            oCache.put(new Element ("Customer" + _oData.getCustomerId(), _oData));
            oCache.remove("AllCustomer");            
        }
        else
        {
            oCache.removeAll();
        }        
    }
	
	public void refreshCache (CustomerType _oData) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);         
	    if (_oData != null && StringUtil.isNotEmpty(_oData.getCustomerTypeId()))
	    {
	        oCache.remove("CustomerType" + _oData.getCustomerTypeId());
            oCache.put(new Element ("CustomerType" + _oData.getCustomerTypeId(), _oData));
            oCache.remove("AllCustomerType");
        }	    
        else
        {
            oCache.removeAll();
        }
	}	
	
	//for delete
	public void refreshCache (String _sID, String _sCode) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE); 
        if (StringUtil.isNotEmpty(_sID))
        {
            oCache.remove("Customer" + _sID);
            oCache.remove("CustFields" + _sID);
            oCache.remove("CustomerByCode" + _sCode);
            oCache.remove("DefaultCustomer");
            oCache.remove("AllCustomer");
            oCache.remove("CustomerBalance" + _sID);
        }
        else
        {
            oCache.removeAll();
        }
    } 
	
	public void refreshCache (CustomerBalance _oData) 
		throws Exception
	{	
	    Cache oCache = m_oManager.getCache(s_CACHE);         
	    if ( _oData != null && StringUtil.isNotEmpty(_oData.getCustomerId()) ) 
	    {
		    oCache.remove("CustomerBalance" + _oData.getCustomerId());		    
	    }
	}		
	
	//-------------------------------------------------------------------------
	// custom fields
	//-------------------------------------------------------------------------	
	public CustomerCfield getCFieldByID(String _sID, Connection _oConn)
			throws Exception
	{
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("CustomerCfield" + _sID);
		if (oElement == null )
		{		
			if (StringUtil.isNotEmpty(_sID))
			{			
				Criteria oCrit = new Criteria();
				oCrit.add(CustomerCfieldPeer.CFIELD_ID, _sID);
				List vData = CustomerCfieldPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) {
					oCache.put(new Element ("CustomerCfield" + _sID, (Serializable)vData.get(0))); 
					return (CustomerCfield) vData.get(0);
				}
			}
			return null;
		}
		else 
		{
			return (CustomerCfield) oElement.getValue();
		}
	}	

	public List getCFields(String _sCustTypeID) throws Exception
    {
		String sElName = "CustCfield" + _sCustTypeID;
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get(sElName);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			
			if (StringUtil.isNotEmpty(_sCustTypeID))
			{
				oCrit.add(CustomerCfieldPeer.CUSTOMER_TYPE_ID, _sCustTypeID);	
				oCrit.or(CustomerCfieldPeer.CUSTOMER_TYPE_ID, "");
			}			
			oCrit.addAscendingOrderByColumn(CustomerCfieldPeer.CUSTOMER_TYPE_ID);
			oCrit.addAscendingOrderByColumn(CustomerCfieldPeer.FIELD_GROUP);		
			oCrit.addAscendingOrderByColumn(CustomerCfieldPeer.FNO);		
			List vData = CustomerCfieldPeer.doSelect(oCrit);

			oCache.put(new Element (sElName, (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	
	
	public void refreshCField (CustomerCfield _oData) 
			throws Exception
	{	
		Cache oCache = m_oManager.getCache(s_CACHE);         	
		if(_oData != null)
		{
			oCache.remove("CustomerCfield" + _oData.getCfieldId());
			oCache.remove("CustCfield");
			oCache.remove("CustCfield" + _oData.getCustomerTypeId());
		}
		else
		{
			oCache.removeAll();
		}
	}	
	
	public List getFields(String _sCustomerID) throws Exception
    {
		String sElName = "CustFields" + _sCustomerID;
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get(sElName);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.add(CustomerFieldPeer.CUSTOMER_ID, _sCustomerID);
			List vData = CustomerFieldPeer.doSelect(oCrit);
			oCache.put(new Element (sElName, (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}			
}
var lastHeader;
var currShow;
function changeCont(tgt,header)
{                   
    if(typeof tab_on  == 'undefined') tab_on  = '#ccc';
    if(typeof tab_off == 'undefined') tab_off = '#eee';
    
    target=('T' +tgt);
    // Hide the last one, and flip the tab color back.
    currShow.style.visibility="hidden";
    currShow.style.display="none";
    if(lastHeader)
    {
       lastHeader.style.background = tab_off;
       lastHeader.style.fontWeight="normal";
    }

    // Show this one, and make the tab silver
    var flipOn = document.getElementById(target);
    flipOn.style.visibility="visible";
    flipOn.style.display="block";
    //flipOn.style.position="absolute";

    var thisTab = document.getElementById(header);
    thisTab.style.background = tab_on;
    thisTab.style.fontWeight = "bold";

    // Save for next go'round
    currShow=document.getElementById(target);
    lastHeader = document.getElementById(header);

    if(document.frm.tab){        
        document.frm.tab.value = tgt.replace('T',''); 
        console.log(document.frm.tab.value);
    }
    return false;
}
function DrawTabs()
{
    var output = '';       
    if(typeof num_rows == 'undefined') num_rows = 1;        
    for(var x = 1; x <= num_rows; x++ )
    {
        
        if(typeof itop   == 'undefined') itop = -1;
        if(typeof ileft  == 'undefined') ileft = -1;
        if(typeof iwidth == 'undefined') iwidth = 480;        
        if(typeof posit  == 'undefined') posit = 'relative';	
        
        //if(x > 1) {
        //  itop = itop + 20;
        //  ileft = ileft - 15;
        //  iwidth = iwidth + 15;
        //}				

        output += '<div id="tabstop" style="z-index:0; position:' + posit + '; ';
        if(ileft > 0) output += 'left:' + ileft + 'px;';
        if(itop  > 0) output += 'top:'  + itop  + 'px; ';
        
        if(typeof iwidth == 'string' && iwidth.indexOf('%') > 0) output += 'min-width:480px;width:' + iwidth + ';';
        else if(iwidth > 0) output += 'width:' + iwidth + 'px;';

        output += '">\n';
        output += '<table border="0" cellpadding="2" cellspacing="1">\n';
        output += '<tr valign="top">\n';

        for(var z = 1; z < rows[x].length; z++ )
        {
           var tid = "tab" + x + z;
           var did = x + z;
           output += '<td id="' + tid +'" class="tab-button"><a href="#" class="tab-link" onClick="changeCont(\'' + x + z + '\', \'' + tid + '\'); return false;" onFocus="if(this.blur)this.blur()">' + rows[x][z] + '</a></td>\n';
        }

        output += '</tr>\n';
        output += '</table>\n';
        output += '</div>\n\n';
    }
    //alert(output);
    self.document.write(output);
}
function showTab(tabid)
{    
    if(!tabid || tabid == '' || tabid == '0') tabid = '11';
    currShow=document.getElementById('T' + tabid);    
    changeCont(tabid, 'tab'  + tabid);    
}

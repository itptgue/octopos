package com.ssti.framework.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CardioConfig
 */
public abstract class BaseCardioConfig extends BaseObject
{
    /** The Peer class */
    private static final CardioConfigPeer peer =
        new CardioConfigPeer();

        
    /** The value for the cardioConfigId field */
    private String cardioConfigId;
                                                
    /** The value for the printerHost field */
    private String printerHost = "remote";
                                                
    /** The value for the printerPort field */
    private String printerPort = "LPT1";
                                                
    /** The value for the printerPageLength field */
    private String printerPageLength = "33";
                                                
    /** The value for the printerPrintMode field */
    private String printerPrintMode = "15";
                                                
    /** The value for the printerFormFeed field */
    private String printerFormFeed = "12";
                                                
    /** The value for the printerCutpaper field */
    private String printerCutpaper = "true";
                                                
    /** The value for the printerCutpaperCmd field */
    private String printerCutpaperCmd = "27,112,0,75,250";
                                                
    /** The value for the printerDetailLine field */
    private String printerDetailLine = "10";
                                                
    /** The value for the drawerUse field */
    private String drawerUse = "false";
                                                
    /** The value for the drawerType field */
    private String drawerType = "1";
                                                
    /** The value for the drawerPort field */
    private String drawerPort = "COM1";
                                                
    /** The value for the drawerOpenCommand field */
    private String drawerOpenCommand = "27,112,0,25,250";
                                                
    /** The value for the displayerUse field */
    private String displayerUse = "false";
                                                
    /** The value for the displayerHost field */
    private String displayerHost = "remote";
                                                
    /** The value for the displayerPort field */
    private String displayerPort = "COM2";
                                                
    /** The value for the displayerDefText field */
    private String displayerDefText = "Welcome";
                                                
    /** The value for the displayerDefScroll field */
    private String displayerDefScroll = "true";
                                                
    /** The value for the displayerClearCmd field */
    private String displayerClearCmd = "27";
                                                
    /** The value for the displayerFlPref field */
    private String displayerFlPref = "";
                                                
    /** The value for the displayerSlPref field */
    private String displayerSlPref = "";
                                                
    /** The value for the displayerLine1Text field */
    private String displayerLine1Text = "Welcome To";
                                                
    /** The value for the displayerLine2Text field */
    private String displayerLine2Text = "";
                                                
    /** The value for the barcodePrinterUse field */
    private String barcodePrinterUse = "false";
                                                
    /** The value for the defaultNumberLocale field */
    private String defaultNumberLocale = "en";
                                                
    /** The value for the defaultNumberFormat field */
    private String defaultNumberFormat = "#,##0.####";
                                                
    /** The value for the defaultSimpleFormat field */
    private String defaultSimpleFormat = "#,##0";
                                                
    /** The value for the defaultAccFormat field */
    private String defaultAccFormat = "#,##0.00;(#,##0.00)";
                                                
    /** The value for the defaultAlignedFormat field */
    private String defaultAlignedFormat = "#,##0.00";
                                                
    /** The value for the defaultDateFormat field */
    private String defaultDateFormat = "dd/MM/yyyy";
                                                
    /** The value for the defaultDatetimeFormat field */
    private String defaultDatetimeFormat = "dd/MM/yyyy HH:mm";
                                                
    /** The value for the defaultTimeFormat field */
    private String defaultTimeFormat = "HH:mm";
                                                
    /** The value for the longDateFormat field */
    private String longDateFormat = "dd MMMM yyyy";
                                                
    /** The value for the longDatetimeFormat field */
    private String longDatetimeFormat = "dd MMMM yyyy HH:mm";
  
    
    /**
     * Get the CardioConfigId
     *
     * @return String
     */
    public String getCardioConfigId()
    {
        return cardioConfigId;
    }

                        
    /**
     * Set the value of CardioConfigId
     *
     * @param v new value
     */
    public void setCardioConfigId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cardioConfigId, v))
              {
            this.cardioConfigId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterHost
     *
     * @return String
     */
    public String getPrinterHost()
    {
        return printerHost;
    }

                        
    /**
     * Set the value of PrinterHost
     *
     * @param v new value
     */
    public void setPrinterHost(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerHost, v))
              {
            this.printerHost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterPort
     *
     * @return String
     */
    public String getPrinterPort()
    {
        return printerPort;
    }

                        
    /**
     * Set the value of PrinterPort
     *
     * @param v new value
     */
    public void setPrinterPort(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerPort, v))
              {
            this.printerPort = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterPageLength
     *
     * @return String
     */
    public String getPrinterPageLength()
    {
        return printerPageLength;
    }

                        
    /**
     * Set the value of PrinterPageLength
     *
     * @param v new value
     */
    public void setPrinterPageLength(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerPageLength, v))
              {
            this.printerPageLength = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterPrintMode
     *
     * @return String
     */
    public String getPrinterPrintMode()
    {
        return printerPrintMode;
    }

                        
    /**
     * Set the value of PrinterPrintMode
     *
     * @param v new value
     */
    public void setPrinterPrintMode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerPrintMode, v))
              {
            this.printerPrintMode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterFormFeed
     *
     * @return String
     */
    public String getPrinterFormFeed()
    {
        return printerFormFeed;
    }

                        
    /**
     * Set the value of PrinterFormFeed
     *
     * @param v new value
     */
    public void setPrinterFormFeed(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerFormFeed, v))
              {
            this.printerFormFeed = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterCutpaper
     *
     * @return String
     */
    public String getPrinterCutpaper()
    {
        return printerCutpaper;
    }

                        
    /**
     * Set the value of PrinterCutpaper
     *
     * @param v new value
     */
    public void setPrinterCutpaper(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerCutpaper, v))
              {
            this.printerCutpaper = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterCutpaperCmd
     *
     * @return String
     */
    public String getPrinterCutpaperCmd()
    {
        return printerCutpaperCmd;
    }

                        
    /**
     * Set the value of PrinterCutpaperCmd
     *
     * @param v new value
     */
    public void setPrinterCutpaperCmd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerCutpaperCmd, v))
              {
            this.printerCutpaperCmd = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrinterDetailLine
     *
     * @return String
     */
    public String getPrinterDetailLine()
    {
        return printerDetailLine;
    }

                        
    /**
     * Set the value of PrinterDetailLine
     *
     * @param v new value
     */
    public void setPrinterDetailLine(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printerDetailLine, v))
              {
            this.printerDetailLine = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DrawerUse
     *
     * @return String
     */
    public String getDrawerUse()
    {
        return drawerUse;
    }

                        
    /**
     * Set the value of DrawerUse
     *
     * @param v new value
     */
    public void setDrawerUse(String v) 
    {
    
                  if (!ObjectUtils.equals(this.drawerUse, v))
              {
            this.drawerUse = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DrawerType
     *
     * @return String
     */
    public String getDrawerType()
    {
        return drawerType;
    }

                        
    /**
     * Set the value of DrawerType
     *
     * @param v new value
     */
    public void setDrawerType(String v) 
    {
    
                  if (!ObjectUtils.equals(this.drawerType, v))
              {
            this.drawerType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DrawerPort
     *
     * @return String
     */
    public String getDrawerPort()
    {
        return drawerPort;
    }

                        
    /**
     * Set the value of DrawerPort
     *
     * @param v new value
     */
    public void setDrawerPort(String v) 
    {
    
                  if (!ObjectUtils.equals(this.drawerPort, v))
              {
            this.drawerPort = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DrawerOpenCommand
     *
     * @return String
     */
    public String getDrawerOpenCommand()
    {
        return drawerOpenCommand;
    }

                        
    /**
     * Set the value of DrawerOpenCommand
     *
     * @param v new value
     */
    public void setDrawerOpenCommand(String v) 
    {
    
                  if (!ObjectUtils.equals(this.drawerOpenCommand, v))
              {
            this.drawerOpenCommand = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerUse
     *
     * @return String
     */
    public String getDisplayerUse()
    {
        return displayerUse;
    }

                        
    /**
     * Set the value of DisplayerUse
     *
     * @param v new value
     */
    public void setDisplayerUse(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerUse, v))
              {
            this.displayerUse = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerHost
     *
     * @return String
     */
    public String getDisplayerHost()
    {
        return displayerHost;
    }

                        
    /**
     * Set the value of DisplayerHost
     *
     * @param v new value
     */
    public void setDisplayerHost(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerHost, v))
              {
            this.displayerHost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerPort
     *
     * @return String
     */
    public String getDisplayerPort()
    {
        return displayerPort;
    }

                        
    /**
     * Set the value of DisplayerPort
     *
     * @param v new value
     */
    public void setDisplayerPort(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerPort, v))
              {
            this.displayerPort = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerDefText
     *
     * @return String
     */
    public String getDisplayerDefText()
    {
        return displayerDefText;
    }

                        
    /**
     * Set the value of DisplayerDefText
     *
     * @param v new value
     */
    public void setDisplayerDefText(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerDefText, v))
              {
            this.displayerDefText = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerDefScroll
     *
     * @return String
     */
    public String getDisplayerDefScroll()
    {
        return displayerDefScroll;
    }

                        
    /**
     * Set the value of DisplayerDefScroll
     *
     * @param v new value
     */
    public void setDisplayerDefScroll(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerDefScroll, v))
              {
            this.displayerDefScroll = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerClearCmd
     *
     * @return String
     */
    public String getDisplayerClearCmd()
    {
        return displayerClearCmd;
    }

                        
    /**
     * Set the value of DisplayerClearCmd
     *
     * @param v new value
     */
    public void setDisplayerClearCmd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerClearCmd, v))
              {
            this.displayerClearCmd = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerFlPref
     *
     * @return String
     */
    public String getDisplayerFlPref()
    {
        return displayerFlPref;
    }

                        
    /**
     * Set the value of DisplayerFlPref
     *
     * @param v new value
     */
    public void setDisplayerFlPref(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerFlPref, v))
              {
            this.displayerFlPref = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerSlPref
     *
     * @return String
     */
    public String getDisplayerSlPref()
    {
        return displayerSlPref;
    }

                        
    /**
     * Set the value of DisplayerSlPref
     *
     * @param v new value
     */
    public void setDisplayerSlPref(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerSlPref, v))
              {
            this.displayerSlPref = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerLine1Text
     *
     * @return String
     */
    public String getDisplayerLine1Text()
    {
        return displayerLine1Text;
    }

                        
    /**
     * Set the value of DisplayerLine1Text
     *
     * @param v new value
     */
    public void setDisplayerLine1Text(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerLine1Text, v))
              {
            this.displayerLine1Text = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DisplayerLine2Text
     *
     * @return String
     */
    public String getDisplayerLine2Text()
    {
        return displayerLine2Text;
    }

                        
    /**
     * Set the value of DisplayerLine2Text
     *
     * @param v new value
     */
    public void setDisplayerLine2Text(String v) 
    {
    
                  if (!ObjectUtils.equals(this.displayerLine2Text, v))
              {
            this.displayerLine2Text = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BarcodePrinterUse
     *
     * @return String
     */
    public String getBarcodePrinterUse()
    {
        return barcodePrinterUse;
    }

                        
    /**
     * Set the value of BarcodePrinterUse
     *
     * @param v new value
     */
    public void setBarcodePrinterUse(String v) 
    {
    
                  if (!ObjectUtils.equals(this.barcodePrinterUse, v))
              {
            this.barcodePrinterUse = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultNumberLocale
     *
     * @return String
     */
    public String getDefaultNumberLocale()
    {
        return defaultNumberLocale;
    }

                        
    /**
     * Set the value of DefaultNumberLocale
     *
     * @param v new value
     */
    public void setDefaultNumberLocale(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultNumberLocale, v))
              {
            this.defaultNumberLocale = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultNumberFormat
     *
     * @return String
     */
    public String getDefaultNumberFormat()
    {
        return defaultNumberFormat;
    }

                        
    /**
     * Set the value of DefaultNumberFormat
     *
     * @param v new value
     */
    public void setDefaultNumberFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultNumberFormat, v))
              {
            this.defaultNumberFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultSimpleFormat
     *
     * @return String
     */
    public String getDefaultSimpleFormat()
    {
        return defaultSimpleFormat;
    }

                        
    /**
     * Set the value of DefaultSimpleFormat
     *
     * @param v new value
     */
    public void setDefaultSimpleFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultSimpleFormat, v))
              {
            this.defaultSimpleFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultAccFormat
     *
     * @return String
     */
    public String getDefaultAccFormat()
    {
        return defaultAccFormat;
    }

                        
    /**
     * Set the value of DefaultAccFormat
     *
     * @param v new value
     */
    public void setDefaultAccFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultAccFormat, v))
              {
            this.defaultAccFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultAlignedFormat
     *
     * @return String
     */
    public String getDefaultAlignedFormat()
    {
        return defaultAlignedFormat;
    }

                        
    /**
     * Set the value of DefaultAlignedFormat
     *
     * @param v new value
     */
    public void setDefaultAlignedFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultAlignedFormat, v))
              {
            this.defaultAlignedFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultDateFormat
     *
     * @return String
     */
    public String getDefaultDateFormat()
    {
        return defaultDateFormat;
    }

                        
    /**
     * Set the value of DefaultDateFormat
     *
     * @param v new value
     */
    public void setDefaultDateFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultDateFormat, v))
              {
            this.defaultDateFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultDatetimeFormat
     *
     * @return String
     */
    public String getDefaultDatetimeFormat()
    {
        return defaultDatetimeFormat;
    }

                        
    /**
     * Set the value of DefaultDatetimeFormat
     *
     * @param v new value
     */
    public void setDefaultDatetimeFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultDatetimeFormat, v))
              {
            this.defaultDatetimeFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTimeFormat
     *
     * @return String
     */
    public String getDefaultTimeFormat()
    {
        return defaultTimeFormat;
    }

                        
    /**
     * Set the value of DefaultTimeFormat
     *
     * @param v new value
     */
    public void setDefaultTimeFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultTimeFormat, v))
              {
            this.defaultTimeFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LongDateFormat
     *
     * @return String
     */
    public String getLongDateFormat()
    {
        return longDateFormat;
    }

                        
    /**
     * Set the value of LongDateFormat
     *
     * @param v new value
     */
    public void setLongDateFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.longDateFormat, v))
              {
            this.longDateFormat = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LongDatetimeFormat
     *
     * @return String
     */
    public String getLongDatetimeFormat()
    {
        return longDatetimeFormat;
    }

                        
    /**
     * Set the value of LongDatetimeFormat
     *
     * @param v new value
     */
    public void setLongDatetimeFormat(String v) 
    {
    
                  if (!ObjectUtils.equals(this.longDatetimeFormat, v))
              {
            this.longDatetimeFormat = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CardioConfigId");
              fieldNames.add("PrinterHost");
              fieldNames.add("PrinterPort");
              fieldNames.add("PrinterPageLength");
              fieldNames.add("PrinterPrintMode");
              fieldNames.add("PrinterFormFeed");
              fieldNames.add("PrinterCutpaper");
              fieldNames.add("PrinterCutpaperCmd");
              fieldNames.add("PrinterDetailLine");
              fieldNames.add("DrawerUse");
              fieldNames.add("DrawerType");
              fieldNames.add("DrawerPort");
              fieldNames.add("DrawerOpenCommand");
              fieldNames.add("DisplayerUse");
              fieldNames.add("DisplayerHost");
              fieldNames.add("DisplayerPort");
              fieldNames.add("DisplayerDefText");
              fieldNames.add("DisplayerDefScroll");
              fieldNames.add("DisplayerClearCmd");
              fieldNames.add("DisplayerFlPref");
              fieldNames.add("DisplayerSlPref");
              fieldNames.add("DisplayerLine1Text");
              fieldNames.add("DisplayerLine2Text");
              fieldNames.add("BarcodePrinterUse");
              fieldNames.add("DefaultNumberLocale");
              fieldNames.add("DefaultNumberFormat");
              fieldNames.add("DefaultSimpleFormat");
              fieldNames.add("DefaultAccFormat");
              fieldNames.add("DefaultAlignedFormat");
              fieldNames.add("DefaultDateFormat");
              fieldNames.add("DefaultDatetimeFormat");
              fieldNames.add("DefaultTimeFormat");
              fieldNames.add("LongDateFormat");
              fieldNames.add("LongDatetimeFormat");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CardioConfigId"))
        {
                return getCardioConfigId();
            }
          if (name.equals("PrinterHost"))
        {
                return getPrinterHost();
            }
          if (name.equals("PrinterPort"))
        {
                return getPrinterPort();
            }
          if (name.equals("PrinterPageLength"))
        {
                return getPrinterPageLength();
            }
          if (name.equals("PrinterPrintMode"))
        {
                return getPrinterPrintMode();
            }
          if (name.equals("PrinterFormFeed"))
        {
                return getPrinterFormFeed();
            }
          if (name.equals("PrinterCutpaper"))
        {
                return getPrinterCutpaper();
            }
          if (name.equals("PrinterCutpaperCmd"))
        {
                return getPrinterCutpaperCmd();
            }
          if (name.equals("PrinterDetailLine"))
        {
                return getPrinterDetailLine();
            }
          if (name.equals("DrawerUse"))
        {
                return getDrawerUse();
            }
          if (name.equals("DrawerType"))
        {
                return getDrawerType();
            }
          if (name.equals("DrawerPort"))
        {
                return getDrawerPort();
            }
          if (name.equals("DrawerOpenCommand"))
        {
                return getDrawerOpenCommand();
            }
          if (name.equals("DisplayerUse"))
        {
                return getDisplayerUse();
            }
          if (name.equals("DisplayerHost"))
        {
                return getDisplayerHost();
            }
          if (name.equals("DisplayerPort"))
        {
                return getDisplayerPort();
            }
          if (name.equals("DisplayerDefText"))
        {
                return getDisplayerDefText();
            }
          if (name.equals("DisplayerDefScroll"))
        {
                return getDisplayerDefScroll();
            }
          if (name.equals("DisplayerClearCmd"))
        {
                return getDisplayerClearCmd();
            }
          if (name.equals("DisplayerFlPref"))
        {
                return getDisplayerFlPref();
            }
          if (name.equals("DisplayerSlPref"))
        {
                return getDisplayerSlPref();
            }
          if (name.equals("DisplayerLine1Text"))
        {
                return getDisplayerLine1Text();
            }
          if (name.equals("DisplayerLine2Text"))
        {
                return getDisplayerLine2Text();
            }
          if (name.equals("BarcodePrinterUse"))
        {
                return getBarcodePrinterUse();
            }
          if (name.equals("DefaultNumberLocale"))
        {
                return getDefaultNumberLocale();
            }
          if (name.equals("DefaultNumberFormat"))
        {
                return getDefaultNumberFormat();
            }
          if (name.equals("DefaultSimpleFormat"))
        {
                return getDefaultSimpleFormat();
            }
          if (name.equals("DefaultAccFormat"))
        {
                return getDefaultAccFormat();
            }
          if (name.equals("DefaultAlignedFormat"))
        {
                return getDefaultAlignedFormat();
            }
          if (name.equals("DefaultDateFormat"))
        {
                return getDefaultDateFormat();
            }
          if (name.equals("DefaultDatetimeFormat"))
        {
                return getDefaultDatetimeFormat();
            }
          if (name.equals("DefaultTimeFormat"))
        {
                return getDefaultTimeFormat();
            }
          if (name.equals("LongDateFormat"))
        {
                return getLongDateFormat();
            }
          if (name.equals("LongDatetimeFormat"))
        {
                return getLongDatetimeFormat();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CardioConfigPeer.CARDIO_CONFIG_ID))
        {
                return getCardioConfigId();
            }
          if (name.equals(CardioConfigPeer.PRINTER_HOST))
        {
                return getPrinterHost();
            }
          if (name.equals(CardioConfigPeer.PRINTER_PORT))
        {
                return getPrinterPort();
            }
          if (name.equals(CardioConfigPeer.PRINTER_PAGE_LENGTH))
        {
                return getPrinterPageLength();
            }
          if (name.equals(CardioConfigPeer.PRINTER_PRINT_MODE))
        {
                return getPrinterPrintMode();
            }
          if (name.equals(CardioConfigPeer.PRINTER_FORM_FEED))
        {
                return getPrinterFormFeed();
            }
          if (name.equals(CardioConfigPeer.PRINTER_CUTPAPER))
        {
                return getPrinterCutpaper();
            }
          if (name.equals(CardioConfigPeer.PRINTER_CUTPAPER_CMD))
        {
                return getPrinterCutpaperCmd();
            }
          if (name.equals(CardioConfigPeer.PRINTER_DETAIL_LINE))
        {
                return getPrinterDetailLine();
            }
          if (name.equals(CardioConfigPeer.DRAWER_USE))
        {
                return getDrawerUse();
            }
          if (name.equals(CardioConfigPeer.DRAWER_TYPE))
        {
                return getDrawerType();
            }
          if (name.equals(CardioConfigPeer.DRAWER_PORT))
        {
                return getDrawerPort();
            }
          if (name.equals(CardioConfigPeer.DRAWER_OPEN_COMMAND))
        {
                return getDrawerOpenCommand();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_USE))
        {
                return getDisplayerUse();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_HOST))
        {
                return getDisplayerHost();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_PORT))
        {
                return getDisplayerPort();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_DEF_TEXT))
        {
                return getDisplayerDefText();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_DEF_SCROLL))
        {
                return getDisplayerDefScroll();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_CLEAR_CMD))
        {
                return getDisplayerClearCmd();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_FL_PREF))
        {
                return getDisplayerFlPref();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_SL_PREF))
        {
                return getDisplayerSlPref();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_LINE1_TEXT))
        {
                return getDisplayerLine1Text();
            }
          if (name.equals(CardioConfigPeer.DISPLAYER_LINE2_TEXT))
        {
                return getDisplayerLine2Text();
            }
          if (name.equals(CardioConfigPeer.BARCODE_PRINTER_USE))
        {
                return getBarcodePrinterUse();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_NUMBER_LOCALE))
        {
                return getDefaultNumberLocale();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_NUMBER_FORMAT))
        {
                return getDefaultNumberFormat();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_SIMPLE_FORMAT))
        {
                return getDefaultSimpleFormat();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_ACC_FORMAT))
        {
                return getDefaultAccFormat();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_ALIGNED_FORMAT))
        {
                return getDefaultAlignedFormat();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_DATE_FORMAT))
        {
                return getDefaultDateFormat();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_DATETIME_FORMAT))
        {
                return getDefaultDatetimeFormat();
            }
          if (name.equals(CardioConfigPeer.DEFAULT_TIME_FORMAT))
        {
                return getDefaultTimeFormat();
            }
          if (name.equals(CardioConfigPeer.LONG_DATE_FORMAT))
        {
                return getLongDateFormat();
            }
          if (name.equals(CardioConfigPeer.LONG_DATETIME_FORMAT))
        {
                return getLongDatetimeFormat();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCardioConfigId();
            }
              if (pos == 1)
        {
                return getPrinterHost();
            }
              if (pos == 2)
        {
                return getPrinterPort();
            }
              if (pos == 3)
        {
                return getPrinterPageLength();
            }
              if (pos == 4)
        {
                return getPrinterPrintMode();
            }
              if (pos == 5)
        {
                return getPrinterFormFeed();
            }
              if (pos == 6)
        {
                return getPrinterCutpaper();
            }
              if (pos == 7)
        {
                return getPrinterCutpaperCmd();
            }
              if (pos == 8)
        {
                return getPrinterDetailLine();
            }
              if (pos == 9)
        {
                return getDrawerUse();
            }
              if (pos == 10)
        {
                return getDrawerType();
            }
              if (pos == 11)
        {
                return getDrawerPort();
            }
              if (pos == 12)
        {
                return getDrawerOpenCommand();
            }
              if (pos == 13)
        {
                return getDisplayerUse();
            }
              if (pos == 14)
        {
                return getDisplayerHost();
            }
              if (pos == 15)
        {
                return getDisplayerPort();
            }
              if (pos == 16)
        {
                return getDisplayerDefText();
            }
              if (pos == 17)
        {
                return getDisplayerDefScroll();
            }
              if (pos == 18)
        {
                return getDisplayerClearCmd();
            }
              if (pos == 19)
        {
                return getDisplayerFlPref();
            }
              if (pos == 20)
        {
                return getDisplayerSlPref();
            }
              if (pos == 21)
        {
                return getDisplayerLine1Text();
            }
              if (pos == 22)
        {
                return getDisplayerLine2Text();
            }
              if (pos == 23)
        {
                return getBarcodePrinterUse();
            }
              if (pos == 24)
        {
                return getDefaultNumberLocale();
            }
              if (pos == 25)
        {
                return getDefaultNumberFormat();
            }
              if (pos == 26)
        {
                return getDefaultSimpleFormat();
            }
              if (pos == 27)
        {
                return getDefaultAccFormat();
            }
              if (pos == 28)
        {
                return getDefaultAlignedFormat();
            }
              if (pos == 29)
        {
                return getDefaultDateFormat();
            }
              if (pos == 30)
        {
                return getDefaultDatetimeFormat();
            }
              if (pos == 31)
        {
                return getDefaultTimeFormat();
            }
              if (pos == 32)
        {
                return getLongDateFormat();
            }
              if (pos == 33)
        {
                return getLongDatetimeFormat();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CardioConfigPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CardioConfigPeer.doInsert((CardioConfig) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CardioConfigPeer.doUpdate((CardioConfig) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cardioConfigId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCardioConfigId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCardioConfigId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCardioConfigId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CardioConfig copy() throws TorqueException
    {
        return copyInto(new CardioConfig());
    }
  
    protected CardioConfig copyInto(CardioConfig copyObj) throws TorqueException
    {
          copyObj.setCardioConfigId(cardioConfigId);
          copyObj.setPrinterHost(printerHost);
          copyObj.setPrinterPort(printerPort);
          copyObj.setPrinterPageLength(printerPageLength);
          copyObj.setPrinterPrintMode(printerPrintMode);
          copyObj.setPrinterFormFeed(printerFormFeed);
          copyObj.setPrinterCutpaper(printerCutpaper);
          copyObj.setPrinterCutpaperCmd(printerCutpaperCmd);
          copyObj.setPrinterDetailLine(printerDetailLine);
          copyObj.setDrawerUse(drawerUse);
          copyObj.setDrawerType(drawerType);
          copyObj.setDrawerPort(drawerPort);
          copyObj.setDrawerOpenCommand(drawerOpenCommand);
          copyObj.setDisplayerUse(displayerUse);
          copyObj.setDisplayerHost(displayerHost);
          copyObj.setDisplayerPort(displayerPort);
          copyObj.setDisplayerDefText(displayerDefText);
          copyObj.setDisplayerDefScroll(displayerDefScroll);
          copyObj.setDisplayerClearCmd(displayerClearCmd);
          copyObj.setDisplayerFlPref(displayerFlPref);
          copyObj.setDisplayerSlPref(displayerSlPref);
          copyObj.setDisplayerLine1Text(displayerLine1Text);
          copyObj.setDisplayerLine2Text(displayerLine2Text);
          copyObj.setBarcodePrinterUse(barcodePrinterUse);
          copyObj.setDefaultNumberLocale(defaultNumberLocale);
          copyObj.setDefaultNumberFormat(defaultNumberFormat);
          copyObj.setDefaultSimpleFormat(defaultSimpleFormat);
          copyObj.setDefaultAccFormat(defaultAccFormat);
          copyObj.setDefaultAlignedFormat(defaultAlignedFormat);
          copyObj.setDefaultDateFormat(defaultDateFormat);
          copyObj.setDefaultDatetimeFormat(defaultDatetimeFormat);
          copyObj.setDefaultTimeFormat(defaultTimeFormat);
          copyObj.setLongDateFormat(longDateFormat);
          copyObj.setLongDatetimeFormat(longDatetimeFormat);
  
                    copyObj.setCardioConfigId((String)null);
                                                                                                                                                                                                                  
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CardioConfigPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CardioConfig\n");
        str.append("------------\n")
           .append("CardioConfigId       : ")
           .append(getCardioConfigId())
           .append("\n")
           .append("PrinterHost          : ")
           .append(getPrinterHost())
           .append("\n")
           .append("PrinterPort          : ")
           .append(getPrinterPort())
           .append("\n")
           .append("PrinterPageLength    : ")
           .append(getPrinterPageLength())
           .append("\n")
           .append("PrinterPrintMode     : ")
           .append(getPrinterPrintMode())
           .append("\n")
           .append("PrinterFormFeed      : ")
           .append(getPrinterFormFeed())
           .append("\n")
           .append("PrinterCutpaper      : ")
           .append(getPrinterCutpaper())
           .append("\n")
           .append("PrinterCutpaperCmd   : ")
           .append(getPrinterCutpaperCmd())
           .append("\n")
           .append("PrinterDetailLine    : ")
           .append(getPrinterDetailLine())
           .append("\n")
           .append("DrawerUse            : ")
           .append(getDrawerUse())
           .append("\n")
           .append("DrawerType           : ")
           .append(getDrawerType())
           .append("\n")
           .append("DrawerPort           : ")
           .append(getDrawerPort())
           .append("\n")
           .append("DrawerOpenCommand    : ")
           .append(getDrawerOpenCommand())
           .append("\n")
           .append("DisplayerUse         : ")
           .append(getDisplayerUse())
           .append("\n")
           .append("DisplayerHost        : ")
           .append(getDisplayerHost())
           .append("\n")
           .append("DisplayerPort        : ")
           .append(getDisplayerPort())
           .append("\n")
           .append("DisplayerDefText     : ")
           .append(getDisplayerDefText())
           .append("\n")
           .append("DisplayerDefScroll   : ")
           .append(getDisplayerDefScroll())
           .append("\n")
           .append("DisplayerClearCmd    : ")
           .append(getDisplayerClearCmd())
           .append("\n")
           .append("DisplayerFlPref      : ")
           .append(getDisplayerFlPref())
           .append("\n")
           .append("DisplayerSlPref      : ")
           .append(getDisplayerSlPref())
           .append("\n")
           .append("DisplayerLine1Text   : ")
           .append(getDisplayerLine1Text())
           .append("\n")
           .append("DisplayerLine2Text   : ")
           .append(getDisplayerLine2Text())
           .append("\n")
           .append("BarcodePrinterUse    : ")
           .append(getBarcodePrinterUse())
           .append("\n")
           .append("DefaultNumberLocale  : ")
           .append(getDefaultNumberLocale())
           .append("\n")
           .append("DefaultNumberFormat  : ")
           .append(getDefaultNumberFormat())
           .append("\n")
           .append("DefaultSimpleFormat  : ")
           .append(getDefaultSimpleFormat())
           .append("\n")
           .append("DefaultAccFormat     : ")
           .append(getDefaultAccFormat())
           .append("\n")
           .append("DefaultAlignedFormat   : ")
           .append(getDefaultAlignedFormat())
           .append("\n")
           .append("DefaultDateFormat    : ")
           .append(getDefaultDateFormat())
           .append("\n")
            .append("DefaultDatetimeFormat   : ")
           .append(getDefaultDatetimeFormat())
           .append("\n")
           .append("DefaultTimeFormat    : ")
           .append(getDefaultTimeFormat())
           .append("\n")
           .append("LongDateFormat       : ")
           .append(getLongDateFormat())
           .append("\n")
           .append("LongDatetimeFormat   : ")
           .append(getLongDatetimeFormat())
           .append("\n")
        ;
        return(str.toString());
    }
}

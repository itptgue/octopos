package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.VendorManager;
import com.ssti.enterprise.pos.om.VendorType;
import com.ssti.enterprise.pos.om.VendorTypePeer;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.enterprise.pos.tools.VendorTypeTool;
import com.ssti.framework.tools.IDGenerator;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2012 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2018-03-03
 * - change update & del, call UpdateHistoryTool.createHistory and createDelHistory
 * 
 * 2015-09-07
 * - Initial Version
 * 
 * </pre><br>
 */
public class VendorTypeForm extends Default
{	
	public static final int i_OP_VIEW = 1;
	public static final int i_OP_SAVE = 2;
	public static final int i_OP_DEL = 3;

    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_OP_VIEW)
			{
				sID = data.getParameters().getString("id");
				context.put("oData", VendorTypeTool.getVendorTypeByID(sID));
    		}
			if (iOp == i_OP_SAVE)
			{
				sID = data.getParameters().getString("id");
				VendorType oData = VendorTypeTool.getVendorTypeByID(sID);
				VendorType oOld = null;
				if (oData == null)
				{
					oData = new VendorType();
					oData.setVendorTypeId(IDGenerator.generateSysID());					
				}
				else
				{
					oOld = oData.copy();
				}
				data.getParameters().setProperties(oData);
				oData.save();	
				if(oOld != null) UpdateHistoryTool.createHistory(oOld, oData, data.getUser().getName(), null);
				VendorManager.getInstance().refreshType(oData);
				data.setMessage(LocaleTool.getString(s_SAVE_SUCCESS));
			}
			if (iOp == i_OP_DEL)
			{
				sID = data.getParameters().getString("id");
				VendorType oData = VendorTypeTool.getVendorTypeByID(sID);
				if (oData != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(VendorTypePeer.VENDOR_TYPE_ID, oData.getVendorTypeId());
					VendorTypePeer.doDelete(oCrit);
					UpdateHistoryTool.createDelHistory(oData, data.getUser().getName(), null);
					data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
					VendorManager.getInstance().refreshType(oData);
				}
			}			
    	}
    	catch(Exception _oEx)
    	{
    		log.error(_oEx);
    		data.setMessage("ERROR: " + _oEx.getMessage());
    	}
    }
}
package com.ssti.enterprise.medical.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.medical.om.CustomerMargin;
import com.ssti.enterprise.medical.tools.CustomerMarginTool;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class CustomerMarginHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
    	String sColor = LocaleTool.get("medical", "color");
    	String sRFee  = LocaleTool.get("medical", "rfee");
    	String sPack  = LocaleTool.get("medical", "packaging");
    	
    	String[] a_HEADER =
    	{
	    	"Customer Code",	
	    	"Customer Name",	
	    	sColor,
	    	"Margin",
	    	sRFee,	
	    	sPack

	    	
    	};

        List vData = CustomerMarginTool.getAllData();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Customer Margin Worksheet");
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        for (int i = 0; i < a_HEADER.length; i++)
        {
        	createHeaderCell(oWB, oRow, (short)iCol, a_HEADER[i]); iCol++;
        }
	
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                CustomerMargin oData = (CustomerMargin) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                Customer oCustomer = CustomerTool.getCustomerByID(oData.getCustomerId());
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oCustomer.getCustomerCode());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oCustomer.getCustomerName());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oData.getColor());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oData.getMargin());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oData.getRfeeApplied()));  iCol++;                 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oData.getPackApplied()));  iCol++;                 
            }        
        }
        return oWB;
    }
}

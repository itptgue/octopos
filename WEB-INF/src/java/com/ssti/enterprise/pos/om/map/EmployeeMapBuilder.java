package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class EmployeeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.EmployeeMapBuilder";

    /**
     * Item
     * @deprecated use EmployeePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "employee";
    }

  
    /**
     * employee.EMPLOYEE_ID
     * @return the column name for the EMPLOYEE_ID field
     * @deprecated use EmployeePeer.employee.EMPLOYEE_ID constant
     */
    public static String getEmployee_EmployeeId()
    {
        return "employee.EMPLOYEE_ID";
    }
  
    /**
     * employee.EMPLOYEE_CODE
     * @return the column name for the EMPLOYEE_CODE field
     * @deprecated use EmployeePeer.employee.EMPLOYEE_CODE constant
     */
    public static String getEmployee_EmployeeCode()
    {
        return "employee.EMPLOYEE_CODE";
    }
  
    /**
     * employee.EMPLOYEE_NAME
     * @return the column name for the EMPLOYEE_NAME field
     * @deprecated use EmployeePeer.employee.EMPLOYEE_NAME constant
     */
    public static String getEmployee_EmployeeName()
    {
        return "employee.EMPLOYEE_NAME";
    }
  
    /**
     * employee.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use EmployeePeer.employee.USER_NAME constant
     */
    public static String getEmployee_UserName()
    {
        return "employee.USER_NAME";
    }
  
    /**
     * employee.JOB_TITLE
     * @return the column name for the JOB_TITLE field
     * @deprecated use EmployeePeer.employee.JOB_TITLE constant
     */
    public static String getEmployee_JobTitle()
    {
        return "employee.JOB_TITLE";
    }
  
    /**
     * employee.GENDER
     * @return the column name for the GENDER field
     * @deprecated use EmployeePeer.employee.GENDER constant
     */
    public static String getEmployee_Gender()
    {
        return "employee.GENDER";
    }
  
    /**
     * employee.BIRTH_PLACE
     * @return the column name for the BIRTH_PLACE field
     * @deprecated use EmployeePeer.employee.BIRTH_PLACE constant
     */
    public static String getEmployee_BirthPlace()
    {
        return "employee.BIRTH_PLACE";
    }
  
    /**
     * employee.BIRTH_DATE
     * @return the column name for the BIRTH_DATE field
     * @deprecated use EmployeePeer.employee.BIRTH_DATE constant
     */
    public static String getEmployee_BirthDate()
    {
        return "employee.BIRTH_DATE";
    }
  
    /**
     * employee.START_DATE
     * @return the column name for the START_DATE field
     * @deprecated use EmployeePeer.employee.START_DATE constant
     */
    public static String getEmployee_StartDate()
    {
        return "employee.START_DATE";
    }
  
    /**
     * employee.QUIT_DATE
     * @return the column name for the QUIT_DATE field
     * @deprecated use EmployeePeer.employee.QUIT_DATE constant
     */
    public static String getEmployee_QuitDate()
    {
        return "employee.QUIT_DATE";
    }
  
    /**
     * employee.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use EmployeePeer.employee.LOCATION_ID constant
     */
    public static String getEmployee_LocationId()
    {
        return "employee.LOCATION_ID";
    }
  
    /**
     * employee.ADDRESS
     * @return the column name for the ADDRESS field
     * @deprecated use EmployeePeer.employee.ADDRESS constant
     */
    public static String getEmployee_Address()
    {
        return "employee.ADDRESS";
    }
  
    /**
     * employee.ZIP
     * @return the column name for the ZIP field
     * @deprecated use EmployeePeer.employee.ZIP constant
     */
    public static String getEmployee_Zip()
    {
        return "employee.ZIP";
    }
  
    /**
     * employee.COUNTRY
     * @return the column name for the COUNTRY field
     * @deprecated use EmployeePeer.employee.COUNTRY constant
     */
    public static String getEmployee_Country()
    {
        return "employee.COUNTRY";
    }
  
    /**
     * employee.PROVINCE
     * @return the column name for the PROVINCE field
     * @deprecated use EmployeePeer.employee.PROVINCE constant
     */
    public static String getEmployee_Province()
    {
        return "employee.PROVINCE";
    }
  
    /**
     * employee.CITY
     * @return the column name for the CITY field
     * @deprecated use EmployeePeer.employee.CITY constant
     */
    public static String getEmployee_City()
    {
        return "employee.CITY";
    }
  
    /**
     * employee.DISTRICT
     * @return the column name for the DISTRICT field
     * @deprecated use EmployeePeer.employee.DISTRICT constant
     */
    public static String getEmployee_District()
    {
        return "employee.DISTRICT";
    }
  
    /**
     * employee.VILLAGE
     * @return the column name for the VILLAGE field
     * @deprecated use EmployeePeer.employee.VILLAGE constant
     */
    public static String getEmployee_Village()
    {
        return "employee.VILLAGE";
    }
  
    /**
     * employee.HOME_PHONE
     * @return the column name for the HOME_PHONE field
     * @deprecated use EmployeePeer.employee.HOME_PHONE constant
     */
    public static String getEmployee_HomePhone()
    {
        return "employee.HOME_PHONE";
    }
  
    /**
     * employee.MOBILE_PHONE
     * @return the column name for the MOBILE_PHONE field
     * @deprecated use EmployeePeer.employee.MOBILE_PHONE constant
     */
    public static String getEmployee_MobilePhone()
    {
        return "employee.MOBILE_PHONE";
    }
  
    /**
     * employee.EMAIL
     * @return the column name for the EMAIL field
     * @deprecated use EmployeePeer.employee.EMAIL constant
     */
    public static String getEmployee_Email()
    {
        return "employee.EMAIL";
    }
  
    /**
     * employee.SALES_COMMISION_PCT
     * @return the column name for the SALES_COMMISION_PCT field
     * @deprecated use EmployeePeer.employee.SALES_COMMISION_PCT constant
     */
    public static String getEmployee_SalesCommisionPct()
    {
        return "employee.SALES_COMMISION_PCT";
    }
  
    /**
     * employee.MARGIN_COMMISION_PCT
     * @return the column name for the MARGIN_COMMISION_PCT field
     * @deprecated use EmployeePeer.employee.MARGIN_COMMISION_PCT constant
     */
    public static String getEmployee_MarginCommisionPct()
    {
        return "employee.MARGIN_COMMISION_PCT";
    }
  
    /**
     * employee.MONTHLY_SALARY
     * @return the column name for the MONTHLY_SALARY field
     * @deprecated use EmployeePeer.employee.MONTHLY_SALARY constant
     */
    public static String getEmployee_MonthlySalary()
    {
        return "employee.MONTHLY_SALARY";
    }
  
    /**
     * employee.OTHER_INCOME
     * @return the column name for the OTHER_INCOME field
     * @deprecated use EmployeePeer.employee.OTHER_INCOME constant
     */
    public static String getEmployee_OtherIncome()
    {
        return "employee.OTHER_INCOME";
    }
  
    /**
     * employee.FREQUENCY
     * @return the column name for the FREQUENCY field
     * @deprecated use EmployeePeer.employee.FREQUENCY constant
     */
    public static String getEmployee_Frequency()
    {
        return "employee.FREQUENCY";
    }
  
    /**
     * employee.MEMO
     * @return the column name for the MEMO field
     * @deprecated use EmployeePeer.employee.MEMO constant
     */
    public static String getEmployee_Memo()
    {
        return "employee.MEMO";
    }
  
    /**
     * employee.ADD_DATE
     * @return the column name for the ADD_DATE field
     * @deprecated use EmployeePeer.employee.ADD_DATE constant
     */
    public static String getEmployee_AddDate()
    {
        return "employee.ADD_DATE";
    }
  
    /**
     * employee.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use EmployeePeer.employee.UPDATE_DATE constant
     */
    public static String getEmployee_UpdateDate()
    {
        return "employee.UPDATE_DATE";
    }
  
    /**
     * employee.ROLE_ID
     * @return the column name for the ROLE_ID field
     * @deprecated use EmployeePeer.employee.ROLE_ID constant
     */
    public static String getEmployee_RoleId()
    {
        return "employee.ROLE_ID";
    }
  
    /**
     * employee.FIELD1
     * @return the column name for the FIELD1 field
     * @deprecated use EmployeePeer.employee.FIELD1 constant
     */
    public static String getEmployee_Field1()
    {
        return "employee.FIELD1";
    }
  
    /**
     * employee.FIELD2
     * @return the column name for the FIELD2 field
     * @deprecated use EmployeePeer.employee.FIELD2 constant
     */
    public static String getEmployee_Field2()
    {
        return "employee.FIELD2";
    }
  
    /**
     * employee.VALUE1
     * @return the column name for the VALUE1 field
     * @deprecated use EmployeePeer.employee.VALUE1 constant
     */
    public static String getEmployee_Value1()
    {
        return "employee.VALUE1";
    }
  
    /**
     * employee.VALUE2
     * @return the column name for the VALUE2 field
     * @deprecated use EmployeePeer.employee.VALUE2 constant
     */
    public static String getEmployee_Value2()
    {
        return "employee.VALUE2";
    }
  
    /**
     * employee.IS_ACTIVE
     * @return the column name for the IS_ACTIVE field
     * @deprecated use EmployeePeer.employee.IS_ACTIVE constant
     */
    public static String getEmployee_IsActive()
    {
        return "employee.IS_ACTIVE";
    }
  
    /**
     * employee.IS_SALESMAN
     * @return the column name for the IS_SALESMAN field
     * @deprecated use EmployeePeer.employee.IS_SALESMAN constant
     */
    public static String getEmployee_IsSalesman()
    {
        return "employee.IS_SALESMAN";
    }
  
    /**
     * employee.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use EmployeePeer.employee.DEPARTMENT_ID constant
     */
    public static String getEmployee_DepartmentId()
    {
        return "employee.DEPARTMENT_ID";
    }
  
    /**
     * employee.PARENT_ID
     * @return the column name for the PARENT_ID field
     * @deprecated use EmployeePeer.employee.PARENT_ID constant
     */
    public static String getEmployee_ParentId()
    {
        return "employee.PARENT_ID";
    }
  
    /**
     * employee.HAS_CHILD
     * @return the column name for the HAS_CHILD field
     * @deprecated use EmployeePeer.employee.HAS_CHILD constant
     */
    public static String getEmployee_HasChild()
    {
        return "employee.HAS_CHILD";
    }
  
    /**
     * employee.EMPLOYEE_LEVEL
     * @return the column name for the EMPLOYEE_LEVEL field
     * @deprecated use EmployeePeer.employee.EMPLOYEE_LEVEL constant
     */
    public static String getEmployee_EmployeeLevel()
    {
        return "employee.EMPLOYEE_LEVEL";
    }
  
    /**
     * employee.SALES_AREA_ID
     * @return the column name for the SALES_AREA_ID field
     * @deprecated use EmployeePeer.employee.SALES_AREA_ID constant
     */
    public static String getEmployee_SalesAreaId()
    {
        return "employee.SALES_AREA_ID";
    }
  
    /**
     * employee.SALES_AREA1_ID
     * @return the column name for the SALES_AREA1_ID field
     * @deprecated use EmployeePeer.employee.SALES_AREA1_ID constant
     */
    public static String getEmployee_SalesArea1Id()
    {
        return "employee.SALES_AREA1_ID";
    }
  
    /**
     * employee.SALES_AREA2_ID
     * @return the column name for the SALES_AREA2_ID field
     * @deprecated use EmployeePeer.employee.SALES_AREA2_ID constant
     */
    public static String getEmployee_SalesArea2Id()
    {
        return "employee.SALES_AREA2_ID";
    }
  
    /**
     * employee.SALES_AREA3_ID
     * @return the column name for the SALES_AREA3_ID field
     * @deprecated use EmployeePeer.employee.SALES_AREA3_ID constant
     */
    public static String getEmployee_SalesArea3Id()
    {
        return "employee.SALES_AREA3_ID";
    }
  
    /**
     * employee.TERRITORY_ID
     * @return the column name for the TERRITORY_ID field
     * @deprecated use EmployeePeer.employee.TERRITORY_ID constant
     */
    public static String getEmployee_TerritoryId()
    {
        return "employee.TERRITORY_ID";
    }
  
    /**
     * employee.TERRITORY1_ID
     * @return the column name for the TERRITORY1_ID field
     * @deprecated use EmployeePeer.employee.TERRITORY1_ID constant
     */
    public static String getEmployee_Territory1Id()
    {
        return "employee.TERRITORY1_ID";
    }
  
    /**
     * employee.TERRITORY2_ID
     * @return the column name for the TERRITORY2_ID field
     * @deprecated use EmployeePeer.employee.TERRITORY2_ID constant
     */
    public static String getEmployee_Territory2Id()
    {
        return "employee.TERRITORY2_ID";
    }
  
    /**
     * employee.TERRITORY3_ID
     * @return the column name for the TERRITORY3_ID field
     * @deprecated use EmployeePeer.employee.TERRITORY3_ID constant
     */
    public static String getEmployee_Territory3Id()
    {
        return "employee.TERRITORY3_ID";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("employee");
        TableMap tMap = dbMap.getTable("employee");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("employee.EMPLOYEE_ID", "");
                          tMap.addColumn("employee.EMPLOYEE_CODE", "");
                          tMap.addColumn("employee.EMPLOYEE_NAME", "");
                          tMap.addColumn("employee.USER_NAME", "");
                          tMap.addColumn("employee.JOB_TITLE", "");
                            tMap.addColumn("employee.GENDER", Integer.valueOf(0));
                          tMap.addColumn("employee.BIRTH_PLACE", "");
                          tMap.addColumn("employee.BIRTH_DATE", new Date());
                          tMap.addColumn("employee.START_DATE", new Date());
                          tMap.addColumn("employee.QUIT_DATE", new Date());
                          tMap.addColumn("employee.LOCATION_ID", "");
                          tMap.addColumn("employee.ADDRESS", "");
                          tMap.addColumn("employee.ZIP", "");
                          tMap.addColumn("employee.COUNTRY", "");
                          tMap.addColumn("employee.PROVINCE", "");
                          tMap.addColumn("employee.CITY", "");
                          tMap.addColumn("employee.DISTRICT", "");
                          tMap.addColumn("employee.VILLAGE", "");
                          tMap.addColumn("employee.HOME_PHONE", "");
                          tMap.addColumn("employee.MOBILE_PHONE", "");
                          tMap.addColumn("employee.EMAIL", "");
                            tMap.addColumn("employee.SALES_COMMISION_PCT", bd_ZERO);
                            tMap.addColumn("employee.MARGIN_COMMISION_PCT", bd_ZERO);
                            tMap.addColumn("employee.MONTHLY_SALARY", bd_ZERO);
                            tMap.addColumn("employee.OTHER_INCOME", bd_ZERO);
                            tMap.addColumn("employee.FREQUENCY", Integer.valueOf(0));
                          tMap.addColumn("employee.MEMO", "");
                          tMap.addColumn("employee.ADD_DATE", new Date());
                          tMap.addColumn("employee.UPDATE_DATE", new Date());
                            tMap.addColumn("employee.ROLE_ID", Integer.valueOf(0));
                          tMap.addColumn("employee.FIELD1", "");
                          tMap.addColumn("employee.FIELD2", "");
                          tMap.addColumn("employee.VALUE1", "");
                          tMap.addColumn("employee.VALUE2", "");
                          tMap.addColumn("employee.IS_ACTIVE", Boolean.TRUE);
                          tMap.addColumn("employee.IS_SALESMAN", Boolean.TRUE);
                          tMap.addColumn("employee.DEPARTMENT_ID", "");
                          tMap.addColumn("employee.PARENT_ID", "");
                          tMap.addColumn("employee.HAS_CHILD", Boolean.TRUE);
                            tMap.addColumn("employee.EMPLOYEE_LEVEL", Integer.valueOf(0));
                          tMap.addColumn("employee.SALES_AREA_ID", "");
                          tMap.addColumn("employee.SALES_AREA1_ID", "");
                          tMap.addColumn("employee.SALES_AREA2_ID", "");
                          tMap.addColumn("employee.SALES_AREA3_ID", "");
                          tMap.addColumn("employee.TERRITORY_ID", "");
                          tMap.addColumn("employee.TERRITORY1_ID", "");
                          tMap.addColumn("employee.TERRITORY2_ID", "");
                          tMap.addColumn("employee.TERRITORY3_ID", "");
          }
}

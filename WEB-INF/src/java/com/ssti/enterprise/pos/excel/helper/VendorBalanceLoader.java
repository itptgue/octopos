package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.framework.tools.StringUtil;

public class VendorBalanceLoader extends ARPBalanceLoader
{    
	private static Log log = LogFactory.getLog ( VendorBalanceLoader.class );
	
	public static final String s_START_HEADING = "Vendor Code";
    
	public VendorBalanceLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
		throws Exception
	{				
		if (started (_oRow, _iIdx))	
		{
			setParam(_oRow, _iIdx);
			if (StringUtil.isNotEmpty(sCode))
			{				
				setObject();
				
				boolean bValid = true;				
				log.debug ("Column " + _iIdx + " Vendor Code : " + sCode);
				
				if (oVend == null) {bValid = false; oReject.append("Vendor ").append(sCode).append(" Not Found\n");}
				bValid = validate(bValid);
				
	            if(bValid)
				{
	            	try
	            	{
	            		createTrans();
	            		logSuccess();
	            	}
	            	catch (Exception _oEx)
	            	{
	            		logError(_oEx);
	            	}
				}
	            else
	            {
	            	logReject();
	            }
			}
		}    
	}

	private void createTrans() throws Exception 
	{
		PurchaseInvoice oTR = new PurchaseInvoice();
		oTR.setVendorInvoiceNo(sInvNo);
		oTR.setVendorId(oVend.getVendorId());
		oTR.setVendorName(oVend.getVendorName());
		oTR.setLocationId(oLoc.getLocationId());
		oTR.setPurchaseInvoiceDate(dAsOf);
		oTR.setCreateBy(sCreateBy);
		oTR.setPaymentTypeId(sTypeID);
		oTR.setPaymentTermId(sTermID);
		oTR.setTotalDiscountPct("0");
		oTR.setIsInclusiveTax(false);
		oTR.setIsTaxable(false);
		
    	oTR.setStatus(i_PROCESSED);
    	oTR.setCourierId("");
    	oTR.setShippingPrice(bd_ZERO);
    	oTR.setRemark(sRemark + "\nImported Opening Balance Trans : " + sInvNo + " by " + m_sUserName);
    	
    	oTR.setFobId("");
    	oTR.setFreightAccountId("");
    	oTR.setPaidAmount(bd_ZERO); 
    	oTR.setTotalExpense(bd_ZERO); 
    	oTR.setDueDate(oTR.getTransactionDate());
    	
	    oTR.setCurrencyId(oCurr.getCurrencyId());
	    oTR.setCurrencyRate(bdRate);
	    oTR.setFiscalRate(bdFisc);
	    
	    PurchaseInvoiceDetail oTD = new PurchaseInvoiceDetail();
	    
	    BigDecimal bdBaseAmount = new BigDecimal(bdAmt.doubleValue() * bdRate.doubleValue());
	    
	    oTD.setItemId(oItem.getItemId());
	    oTD.setItemCode(oItem.getItemCode());
	    oTD.setItemName(oItem.getItemName());
	    oTD.setQty(bd_ONE);
	    oTD.setQtyBase(bd_ONE);
	    oTD.setUnitId(oItem.getUnitId());
	    oTD.setUnitCode(UnitTool.getCodeByID(oItem.getUnitId()));
	    oTD.setItemPrice(bdAmt);
	    oTD.setTaxId(oTax.getTaxId());
	    oTD.setTaxAmount(oTax.getAmount());
		oTD.setDiscount("0");
		oTD.setProjectId("");
		oTD.setDepartmentId("");
		//OTHER FIELD
		oTD.setPurchaseReceiptId("");
		oTD.setPurchaseReceiptDetailId("");
    	oTD.setPurchaseOrderId("");
    	oTD.setWeight(bd_ZERO);
		oTD.setSubTotal(bdAmt);
		oTD.setSubTotalDisc(bd_ZERO);
		oTD.setSubTotalTax(bd_ZERO);
		oTD.setSubTotalExp(bd_ZERO);		
		oTD.setCostPerUnit(bdBaseAmount);

		List vTD = new ArrayList(1);
		List vPIF = new ArrayList(1);
		List vPIE = new ArrayList(1);
		vTD.add(oTD);

		PurchaseInvoiceTool.setHeaderProperties(oTR, vTD, null);
		
		if (dDue != null)
		{
			oTR.setDueDate(dDue);
		}
		
		PurchaseInvoiceTool.saveData(oTR, vTD, vPIF, vPIE, null);
	}
}

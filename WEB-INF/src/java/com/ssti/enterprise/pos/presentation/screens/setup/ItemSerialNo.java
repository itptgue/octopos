package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.inventory.ItemSerialTool;

public class ItemSerialNo extends ItemSecureScreen
{
	
	private static Log log = LogFactory.getLog(ItemSerialNo.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
		int iOp =  data.getParameters().getInt("op", -1);
		try
		{
			String sItemID = data.getParameters().getString("ItemId");
			String sLocationID = data.getParameters().getString("LocationId");
			Item oItem = ItemTool.getItemByID(sItemID);

			if (iOp == 1) //save input serial
			{
				if (oItem != null)
				{
					String sTransID = oItem.getItemId();
					String sTransNo = oItem.getItemCode();
					Date dTransDate = new Date();
					ItemSerialTool.manualCreate(sItemID, sLocationID, data);
					data.setMessage("Item Serial Created Successfully");
				}
			}
			else if (iOp == 2) //issue serial
			{
				ItemSerialTool.manualIssue(sItemID, sLocationID, data);
				data.setMessage("Item Serial Issued Successfully");
			}						
			
			int iStatus = data.getParameters().getInt("Status", -1);
			List vData = ItemSerialTool.findData(sItemID, sLocationID, iStatus);
			
			context.put("ItemSerials", vData);
			context.put("itemserial", ItemSerialTool.getInstance());
    	}
    	catch(Exception _oEx)
    	{
    		log.error("Error : " + _oEx);
			data.setMessage( _oEx.getMessage());
    	}
    }
    
    
}

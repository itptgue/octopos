package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.DeliveryOrder;
import com.ssti.enterprise.pos.om.DeliveryOrderDetail;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.om.SalesOrderDetail;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.sales.DeliveryOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.StringUtil;

public class DeliveryOrderTransaction extends SalesSecureScreen
{
	protected static final String[] a_PERM = {"Sales Transaction", "View Delivery Order"};
	
    private static final int i_OP_ADD_DETAIL  = 1;
    private static final int i_OP_DEL_DETAIL  = 2;
	private static final int i_OP_NEW_TRANS   = 3;
	private static final int i_OP_VIEW_TRANS  = 4; //after saved or view details
	private static final int i_OP_ADD_ITEMS   = 5;
	private static final int i_OP_RELOAD      = 6;
	private static final int i_OP_IMPORT_SO   = 7;
	
    private DeliveryOrder oDO = null;
    private List vDOD = null;
    private HttpSession oSes;
    
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if ( iOp == i_OP_ADD_DETAIL && !b_REFRESH)
			{
				addDetailData (data);
			}
			else if ( iOp == i_OP_DEL_DETAIL && !b_REFRESH)
			{
				delDetailData (data);
			}

			else if ( iOp == i_OP_NEW_TRANS )
			{
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS )
			{
				getData (data);
			}
			else if (iOp == i_OP_IMPORT_SO && !b_REFRESH)
			{
				importSOData (data);
			}
		}
		catch (Exception _oEx) 
		{
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_DOD, oSes.getAttribute (s_DOD));
    	context.put(s_DO, oSes.getAttribute (s_DO));
    }

	private void initSession ( RunData data )
	{
		synchronized (this) 
		{		
			oSes = data.getSession();
			if ( oSes.getAttribute (s_DO) == null )
			{
				oSes.setAttribute (s_DO, new DeliveryOrder());
				oSes.setAttribute (s_DOD, new ArrayList());
			}
			oDO = (DeliveryOrder) oSes.getAttribute (s_DO);
			vDOD = (List) oSes.getAttribute (s_DOD);
		}
	}

    private void addDetailData ( RunData data )
    	throws Exception
    {
    	DeliveryOrderTool.updateDetail(vDOD,data);
    	
		DeliveryOrderDetail oDOD = new DeliveryOrderDetail();
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			data.getParameters().setProperties (oDOD);	
			oDOD.setItemCode (oItem.getItemCode());
			oDOD.setItemName (oItem.getItemName());
		}
		else
		{
			return;
		}				
		
		if (!isExist (oDOD))
		{
		    if (b_SALES_FIRST_LAST){
			    vDOD.add (0, oDOD);
			}
			else{
			    vDOD.add (oDOD);
			}
		}
		DeliveryOrderTool.setHeaderProperties (oDO, vDOD,data);
		oSes.setAttribute (s_DOD, vDOD);
		oSes.setAttribute (s_DO, oDO);
    }
    private void delDetailData ( RunData data )
    	throws Exception
    {
    	DeliveryOrderTool.updateDetail(vDOD,data);
    	
		if (vDOD.size() > (data.getParameters().getInt("No") - 1))
		{
			vDOD.remove ( data.getParameters().getInt("No") - 1 );
			oSes.setAttribute  ( s_DOD, vDOD );
			DeliveryOrderTool.setHeaderProperties ( oDO, vDOD, data );
			oSes.setAttribute ( s_DO, oDO );
		}
    }

    private void createNewTrans ( RunData data )
    {
    	synchronized (this)
    	{
    		oDO = new DeliveryOrder();
    		vDOD = new ArrayList();
    		oSes.setAttribute (s_DO,  oDO);
    		oSes.setAttribute (s_DOD, vDOD);
    	}
    }

    private boolean isExist (DeliveryOrderDetail _oDOD)
    {
    	for (int i = 0; i < vDOD.size(); i++)
    	{
    		DeliveryOrderDetail oExistPOD = (DeliveryOrderDetail) vDOD.get (i);
    		if ( oExistPOD.getItemCode().equals(_oDOD.getItemCode()) && 
    			 oExistPOD.getItemId().equals(_oDOD.getItemId()) && 
    			 oExistPOD.getUnitId().equals(_oDOD.getUnitId()) )
    		{
       		    if(b_SALES_MERGE_DETAIL)
    		    {
				    mergeData (oExistPOD, _oDOD);
				}
				else
				{
				    return false;
				}
				return true;
    		}
    	}
    	return false;
    }

    private void mergeData (DeliveryOrderDetail _oOldTD, DeliveryOrderDetail _oNewTD)
    {    	
    	double dQty 	  = _oOldTD.getQty().doubleValue() + _oNewTD.getQty().doubleValue();
    	double dTotalPurc = dQty * _oOldTD.getItemPriceInSo().doubleValue();
    	double dSubTotal  = dTotalPurc;
		
		//count shipping cost
		double dCostPerUnit   = (dSubTotal) / dQty;
	
    	_oOldTD.setQty 			( new BigDecimal (dQty) );    
    	_oOldTD.setSubTotal     ( new BigDecimal (dSubTotal)    );
		_oOldTD.setCostPerUnit  ( new BigDecimal (dCostPerUnit)	);
    	_oNewTD = null;        	
    }

    private void getData ( RunData data )
    	throws Exception
    {
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID))
    	{
    		try
    		{
    			oSes.setAttribute (s_DO, DeliveryOrderTool.getHeaderByID (sID));
				oSes.setAttribute (s_DOD, DeliveryOrderTool.getDetailsByID (sID));
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }

	private void importSOData ( RunData data )
    	throws Exception
    {
    	String sSOID = data.getParameters().getString ("SoId");
    	if (StringUtil.isNotEmpty(sSOID))
    	{
    		try
    		{
    			synchronized (this) 
				{
    				SalesOrder oSO = SalesOrderTool.getHeaderByID (sSOID);
    				List vSOD = SalesOrderTool.getDetailsByID (sSOID);
    				oDO = new DeliveryOrder();
    				DeliveryOrderTool.mapSOHeaderToDOHeader (oSO, oDO);
    				oDO.setCreateBy(data.getUser().getName());
    				vDOD = new ArrayList (vSOD.size());
    				double dTotalQty = 0;
    				for (int i = 0; i < vSOD.size(); i++)
    				{	
    					SalesOrderDetail  oSOD = (SalesOrderDetail) vSOD.get(i);
    					DeliveryOrderDetail oDOD = new DeliveryOrderDetail();
    					DeliveryOrderTool.mapSODetailToDODetail (oSOD, oDOD, oSO.getLocationId());
    					dTotalQty = dTotalQty + oDOD.getQty().doubleValue();
    					vDOD.add (oDOD);
    				}
    				oDO.setTotalQty(new BigDecimal(dTotalQty));
    				oSes.setAttribute (s_DO, oDO);
    				oSes.setAttribute (s_DOD, vDOD);
				}
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }
}

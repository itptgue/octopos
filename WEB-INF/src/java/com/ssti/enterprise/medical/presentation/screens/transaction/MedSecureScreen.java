package com.ssti.enterprise.medical.presentation.screens.transaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.security.PermissionSet;
import org.apache.turbine.util.security.RoleSet;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.medical.om.Doctor;
import com.ssti.enterprise.medical.tools.DoctorTool;
import com.ssti.enterprise.medical.tools.MedConfigTool;
import com.ssti.enterprise.medical.tools.MedicalAttributes;
import com.ssti.enterprise.pos.presentation.screens.transaction.TransactionSecureScreen;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SiteTool;

public class MedSecureScreen extends TransactionSecureScreen implements MedicalAttributes
{
	protected static Log log = LogFactory.getLog(MedSecureScreen.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	MedConfigTool.setContextTool(context);
    	try 
    	{
        	PermissionSet aclPerm = data.getACL().getPermissions();
        	RoleSet aclRole = data.getACL().getRoles();

    		boolean bAllLoc = aclPerm.containsName("Access All Location");
    		boolean bAllSite = aclPerm.containsName("Access All Site");    		
        	
    		context.put("aclPerm", aclPerm);
    		context.put("aclRole", aclRole);

    		context.put("bAllLoc", bAllLoc);
    		context.put("bAllSite", bAllSite);    		
    		context.put("bVPT", aclPerm.containsName("View Customer Data"));              
    		context.put("bCPT", aclPerm.containsName("Create Customer Data"));            
    		context.put("bVRG", aclPerm.containsName("View Patient Registration"));       
    		context.put("bCRG", aclPerm.containsName("Create Patient Registration"));     
    		context.put("bVMR", aclPerm.containsName("View Medical Record"));             
    		context.put("bCMR", aclPerm.containsName("Create Medical Record"));
    		context.put("bVLO", aclPerm.containsName("View Laboratory Order"));
    		context.put("bCLO", aclPerm.containsName("Create Labotratory Record"));
    		context.put("bVSI", aclPerm.containsName("View Sales Invoice"));            
    		context.put("bCSI", aclPerm.containsName("Create Sales Invoice"));
    		
    		String sLocID = PreferenceTool.getLocationID(data.getUser().getName());    		
    		String sSiteID = LocationTool.getLocationByID(sLocID).getSiteId();
    		context.put("USRLOC", sLocID);    		
    		context.put("USRSITE", sSiteID);    		
    		context.put("LOC",  LocationTool.getLocationByID(sLocID));    		
    		context.put("SITE", SiteTool.getSiteByID(sSiteID));    		    		    		
    		
    		if(PreferenceTool.medicalType() == i_CLINIC)
    		{
	        	Doctor oDoc = DoctorTool.getDoctorByUserName(data.getUser().getName());
	        	if (oDoc != null)
	        	{
	        		context.put("ISDOCTOR", true);
	        		context.put("DOCTOR", oDoc);
	        		context.put("bDOC", true);
	        	}	        	
    		}
    	}    		
    	catch (Exception e) 
    	{
			e.printStackTrace();
			log.error(e);
		}
    }
}

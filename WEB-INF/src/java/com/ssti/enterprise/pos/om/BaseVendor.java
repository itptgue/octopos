package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Vendor
 */
public abstract class BaseVendor extends BaseObject
{
    /** The Peer class */
    private static final VendorPeer peer =
        new VendorPeer();

        
    /** The value for the vendorId field */
    private String vendorId;
                                                
    /** The value for the vendorTypeId field */
    private String vendorTypeId = "";
      
    /** The value for the vendorCode field */
    private String vendorCode;
      
    /** The value for the vendorName field */
    private String vendorName;
      
    /** The value for the taxNo field */
    private String taxNo;
      
    /** The value for the vendorStatus field */
    private int vendorStatus;
                                                
    /** The value for the contactName1 field */
    private String contactName1 = "";
                                                
    /** The value for the contactPhone1 field */
    private String contactPhone1 = "";
                                                
    /** The value for the jobTitle1 field */
    private String jobTitle1 = "";
                                                
    /** The value for the contactName2 field */
    private String contactName2 = "";
                                                
    /** The value for the contactPhone2 field */
    private String contactPhone2 = "";
                                                
    /** The value for the jobTitle2 field */
    private String jobTitle2 = "";
                                                
    /** The value for the contactName3 field */
    private String contactName3 = "";
                                                
    /** The value for the contactPhone3 field */
    private String contactPhone3 = "";
                                                
    /** The value for the jobTitle3 field */
    private String jobTitle3 = "";
                                                
    /** The value for the address field */
    private String address = "";
                                                
    /** The value for the zip field */
    private String zip = "";
                                                
    /** The value for the country field */
    private String country = "";
                                                
    /** The value for the province field */
    private String province = "";
                                                
    /** The value for the city field */
    private String city = "";
                                                
    /** The value for the district field */
    private String district = "";
                                                
    /** The value for the village field */
    private String village = "";
                                                
    /** The value for the phone1 field */
    private String phone1 = "";
                                                
    /** The value for the phone2 field */
    private String phone2 = "";
                                                
    /** The value for the fax field */
    private String fax = "";
                                                
    /** The value for the email field */
    private String email = "";
                                                
    /** The value for the webSite field */
    private String webSite = "";
                                                
    /** The value for the defaultTaxId field */
    private String defaultTaxId = "";
                                                
    /** The value for the defaultTypeId field */
    private String defaultTypeId = "";
                                                
    /** The value for the defaultTermId field */
    private String defaultTermId = "";
                                                
    /** The value for the defaultEmployeeId field */
    private String defaultEmployeeId = "";
                                                
    /** The value for the defaultCurrencyId field */
    private String defaultCurrencyId = "";
                                                
    /** The value for the defaultLocationId field */
    private String defaultLocationId = "";
                                                
    /** The value for the defaultItemDisc field */
    private String defaultItemDisc = "0";
                                                
    /** The value for the defaultDiscountAmount field */
    private String defaultDiscountAmount = "0";
                                                
          
    /** The value for the creditLimit field */
    private BigDecimal creditLimit= bd_ZERO;
      
    /** The value for the memo field */
    private String memo;
      
    /** The value for the updateDate field */
    private Date updateDate;
      
    /** The value for the lastUpdateLocationId field */
    private String lastUpdateLocationId;
                                                
    /** The value for the field1 field */
    private String field1 = "";
                                                
    /** The value for the field2 field */
    private String field2 = "";
                                                
    /** The value for the value1 field */
    private String value1 = "";
                                                
    /** The value for the value2 field */
    private String value2 = "";
                                                
    /** The value for the apAccount field */
    private String apAccount = "";
                                                
          
    /** The value for the openingBalance field */
    private BigDecimal openingBalance= bd_ZERO;
      
    /** The value for the asDate field */
    private Date asDate;
                                                
    /** The value for the obTransId field */
    private String obTransId = "";
                                                
          
    /** The value for the obRate field */
    private BigDecimal obRate= new BigDecimal(1);
                                                
    /** The value for the salesAreaId field */
    private String salesAreaId = "";
                                                
    /** The value for the territoryId field */
    private String territoryId = "";
                                                
          
    /** The value for the longitudes field */
    private BigDecimal longitudes= bd_ZERO;
                                                
          
    /** The value for the latitudes field */
    private BigDecimal latitudes= bd_ZERO;
                                                
    /** The value for the transPrefix field */
    private String transPrefix = "";
                                                
    /** The value for the createBy field */
    private String createBy = "";
                                                
    /** The value for the lastUpdateBy field */
    private String lastUpdateBy = "";
                                          
    /** The value for the allowReturn field */
    private int allowReturn = 1;
                                          
    /** The value for the itemType field */
    private int itemType = 0;
                                                
    /** The value for the consignDisc field */
    private String consignDisc = "0";
                                                
    /** The value for the consignField field */
    private String consignField = "";
                                                
    /** The value for the paymentAccBank field */
    private String paymentAccBank = "";
                                                
    /** The value for the paymentAccNo field */
    private String paymentAccNo = "";
                                                
    /** The value for the paymentAccName field */
    private String paymentAccName = "";
                                                
    /** The value for the paymentEmailTo field */
    private String paymentEmailTo = "";
  
    
    /**
     * Get the VendorId
     *
     * @return String
     */
    public String getVendorId()
    {
        return vendorId;
    }

                        
    /**
     * Set the value of VendorId
     *
     * @param v new value
     */
    public void setVendorId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorId, v))
              {
            this.vendorId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorTypeId
     *
     * @return String
     */
    public String getVendorTypeId()
    {
        return vendorTypeId;
    }

                        
    /**
     * Set the value of VendorTypeId
     *
     * @param v new value
     */
    public void setVendorTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorTypeId, v))
              {
            this.vendorTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorCode
     *
     * @return String
     */
    public String getVendorCode()
    {
        return vendorCode;
    }

                        
    /**
     * Set the value of VendorCode
     *
     * @param v new value
     */
    public void setVendorCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorCode, v))
              {
            this.vendorCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

                        
    /**
     * Set the value of VendorName
     *
     * @param v new value
     */
    public void setVendorName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.vendorName, v))
              {
            this.vendorName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TaxNo
     *
     * @return String
     */
    public String getTaxNo()
    {
        return taxNo;
    }

                        
    /**
     * Set the value of TaxNo
     *
     * @param v new value
     */
    public void setTaxNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.taxNo, v))
              {
            this.taxNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the VendorStatus
     *
     * @return int
     */
    public int getVendorStatus()
    {
        return vendorStatus;
    }

                        
    /**
     * Set the value of VendorStatus
     *
     * @param v new value
     */
    public void setVendorStatus(int v) 
    {
    
                  if (this.vendorStatus != v)
              {
            this.vendorStatus = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName1
     *
     * @return String
     */
    public String getContactName1()
    {
        return contactName1;
    }

                        
    /**
     * Set the value of ContactName1
     *
     * @param v new value
     */
    public void setContactName1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName1, v))
              {
            this.contactName1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactPhone1
     *
     * @return String
     */
    public String getContactPhone1()
    {
        return contactPhone1;
    }

                        
    /**
     * Set the value of ContactPhone1
     *
     * @param v new value
     */
    public void setContactPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactPhone1, v))
              {
            this.contactPhone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JobTitle1
     *
     * @return String
     */
    public String getJobTitle1()
    {
        return jobTitle1;
    }

                        
    /**
     * Set the value of JobTitle1
     *
     * @param v new value
     */
    public void setJobTitle1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jobTitle1, v))
              {
            this.jobTitle1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName2
     *
     * @return String
     */
    public String getContactName2()
    {
        return contactName2;
    }

                        
    /**
     * Set the value of ContactName2
     *
     * @param v new value
     */
    public void setContactName2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName2, v))
              {
            this.contactName2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactPhone2
     *
     * @return String
     */
    public String getContactPhone2()
    {
        return contactPhone2;
    }

                        
    /**
     * Set the value of ContactPhone2
     *
     * @param v new value
     */
    public void setContactPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactPhone2, v))
              {
            this.contactPhone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JobTitle2
     *
     * @return String
     */
    public String getJobTitle2()
    {
        return jobTitle2;
    }

                        
    /**
     * Set the value of JobTitle2
     *
     * @param v new value
     */
    public void setJobTitle2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jobTitle2, v))
              {
            this.jobTitle2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactName3
     *
     * @return String
     */
    public String getContactName3()
    {
        return contactName3;
    }

                        
    /**
     * Set the value of ContactName3
     *
     * @param v new value
     */
    public void setContactName3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactName3, v))
              {
            this.contactName3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ContactPhone3
     *
     * @return String
     */
    public String getContactPhone3()
    {
        return contactPhone3;
    }

                        
    /**
     * Set the value of ContactPhone3
     *
     * @param v new value
     */
    public void setContactPhone3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.contactPhone3, v))
              {
            this.contactPhone3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JobTitle3
     *
     * @return String
     */
    public String getJobTitle3()
    {
        return jobTitle3;
    }

                        
    /**
     * Set the value of JobTitle3
     *
     * @param v new value
     */
    public void setJobTitle3(String v) 
    {
    
                  if (!ObjectUtils.equals(this.jobTitle3, v))
              {
            this.jobTitle3 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Address
     *
     * @return String
     */
    public String getAddress()
    {
        return address;
    }

                        
    /**
     * Set the value of Address
     *
     * @param v new value
     */
    public void setAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.address, v))
              {
            this.address = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Zip
     *
     * @return String
     */
    public String getZip()
    {
        return zip;
    }

                        
    /**
     * Set the value of Zip
     *
     * @param v new value
     */
    public void setZip(String v) 
    {
    
                  if (!ObjectUtils.equals(this.zip, v))
              {
            this.zip = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Country
     *
     * @return String
     */
    public String getCountry()
    {
        return country;
    }

                        
    /**
     * Set the value of Country
     *
     * @param v new value
     */
    public void setCountry(String v) 
    {
    
                  if (!ObjectUtils.equals(this.country, v))
              {
            this.country = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Province
     *
     * @return String
     */
    public String getProvince()
    {
        return province;
    }

                        
    /**
     * Set the value of Province
     *
     * @param v new value
     */
    public void setProvince(String v) 
    {
    
                  if (!ObjectUtils.equals(this.province, v))
              {
            this.province = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the City
     *
     * @return String
     */
    public String getCity()
    {
        return city;
    }

                        
    /**
     * Set the value of City
     *
     * @param v new value
     */
    public void setCity(String v) 
    {
    
                  if (!ObjectUtils.equals(this.city, v))
              {
            this.city = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the District
     *
     * @return String
     */
    public String getDistrict()
    {
        return district;
    }

                        
    /**
     * Set the value of District
     *
     * @param v new value
     */
    public void setDistrict(String v) 
    {
    
                  if (!ObjectUtils.equals(this.district, v))
              {
            this.district = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Village
     *
     * @return String
     */
    public String getVillage()
    {
        return village;
    }

                        
    /**
     * Set the value of Village
     *
     * @param v new value
     */
    public void setVillage(String v) 
    {
    
                  if (!ObjectUtils.equals(this.village, v))
              {
            this.village = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone1
     *
     * @return String
     */
    public String getPhone1()
    {
        return phone1;
    }

                        
    /**
     * Set the value of Phone1
     *
     * @param v new value
     */
    public void setPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone1, v))
              {
            this.phone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone2
     *
     * @return String
     */
    public String getPhone2()
    {
        return phone2;
    }

                        
    /**
     * Set the value of Phone2
     *
     * @param v new value
     */
    public void setPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone2, v))
              {
            this.phone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Fax
     *
     * @return String
     */
    public String getFax()
    {
        return fax;
    }

                        
    /**
     * Set the value of Fax
     *
     * @param v new value
     */
    public void setFax(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fax, v))
              {
            this.fax = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Email
     *
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

                        
    /**
     * Set the value of Email
     *
     * @param v new value
     */
    public void setEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.email, v))
              {
            this.email = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the WebSite
     *
     * @return String
     */
    public String getWebSite()
    {
        return webSite;
    }

                        
    /**
     * Set the value of WebSite
     *
     * @param v new value
     */
    public void setWebSite(String v) 
    {
    
                  if (!ObjectUtils.equals(this.webSite, v))
              {
            this.webSite = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTaxId
     *
     * @return String
     */
    public String getDefaultTaxId()
    {
        return defaultTaxId;
    }

                        
    /**
     * Set the value of DefaultTaxId
     *
     * @param v new value
     */
    public void setDefaultTaxId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultTaxId, v))
              {
            this.defaultTaxId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTypeId
     *
     * @return String
     */
    public String getDefaultTypeId()
    {
        return defaultTypeId;
    }

                        
    /**
     * Set the value of DefaultTypeId
     *
     * @param v new value
     */
    public void setDefaultTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultTypeId, v))
              {
            this.defaultTypeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultTermId
     *
     * @return String
     */
    public String getDefaultTermId()
    {
        return defaultTermId;
    }

                        
    /**
     * Set the value of DefaultTermId
     *
     * @param v new value
     */
    public void setDefaultTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultTermId, v))
              {
            this.defaultTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultEmployeeId
     *
     * @return String
     */
    public String getDefaultEmployeeId()
    {
        return defaultEmployeeId;
    }

                        
    /**
     * Set the value of DefaultEmployeeId
     *
     * @param v new value
     */
    public void setDefaultEmployeeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultEmployeeId, v))
              {
            this.defaultEmployeeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultCurrencyId
     *
     * @return String
     */
    public String getDefaultCurrencyId()
    {
        return defaultCurrencyId;
    }

                        
    /**
     * Set the value of DefaultCurrencyId
     *
     * @param v new value
     */
    public void setDefaultCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultCurrencyId, v))
              {
            this.defaultCurrencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultLocationId
     *
     * @return String
     */
    public String getDefaultLocationId()
    {
        return defaultLocationId;
    }

                        
    /**
     * Set the value of DefaultLocationId
     *
     * @param v new value
     */
    public void setDefaultLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultLocationId, v))
              {
            this.defaultLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultItemDisc
     *
     * @return String
     */
    public String getDefaultItemDisc()
    {
        return defaultItemDisc;
    }

                        
    /**
     * Set the value of DefaultItemDisc
     *
     * @param v new value
     */
    public void setDefaultItemDisc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultItemDisc, v))
              {
            this.defaultItemDisc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DefaultDiscountAmount
     *
     * @return String
     */
    public String getDefaultDiscountAmount()
    {
        return defaultDiscountAmount;
    }

                        
    /**
     * Set the value of DefaultDiscountAmount
     *
     * @param v new value
     */
    public void setDefaultDiscountAmount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.defaultDiscountAmount, v))
              {
            this.defaultDiscountAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreditLimit
     *
     * @return BigDecimal
     */
    public BigDecimal getCreditLimit()
    {
        return creditLimit;
    }

                        
    /**
     * Set the value of CreditLimit
     *
     * @param v new value
     */
    public void setCreditLimit(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.creditLimit, v))
              {
            this.creditLimit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Memo
     *
     * @return String
     */
    public String getMemo()
    {
        return memo;
    }

                        
    /**
     * Set the value of Memo
     *
     * @param v new value
     */
    public void setMemo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memo, v))
              {
            this.memo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateLocationId
     *
     * @return String
     */
    public String getLastUpdateLocationId()
    {
        return lastUpdateLocationId;
    }

                        
    /**
     * Set the value of LastUpdateLocationId
     *
     * @param v new value
     */
    public void setLastUpdateLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateLocationId, v))
              {
            this.lastUpdateLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field1
     *
     * @return String
     */
    public String getField1()
    {
        return field1;
    }

                        
    /**
     * Set the value of Field1
     *
     * @param v new value
     */
    public void setField1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field1, v))
              {
            this.field1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Field2
     *
     * @return String
     */
    public String getField2()
    {
        return field2;
    }

                        
    /**
     * Set the value of Field2
     *
     * @param v new value
     */
    public void setField2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.field2, v))
              {
            this.field2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value1
     *
     * @return String
     */
    public String getValue1()
    {
        return value1;
    }

                        
    /**
     * Set the value of Value1
     *
     * @param v new value
     */
    public void setValue1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value1, v))
              {
            this.value1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Value2
     *
     * @return String
     */
    public String getValue2()
    {
        return value2;
    }

                        
    /**
     * Set the value of Value2
     *
     * @param v new value
     */
    public void setValue2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.value2, v))
              {
            this.value2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ApAccount
     *
     * @return String
     */
    public String getApAccount()
    {
        return apAccount;
    }

                        
    /**
     * Set the value of ApAccount
     *
     * @param v new value
     */
    public void setApAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.apAccount, v))
              {
            this.apAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpeningBalance
     *
     * @return BigDecimal
     */
    public BigDecimal getOpeningBalance()
    {
        return openingBalance;
    }

                        
    /**
     * Set the value of OpeningBalance
     *
     * @param v new value
     */
    public void setOpeningBalance(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.openingBalance, v))
              {
            this.openingBalance = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AsDate
     *
     * @return Date
     */
    public Date getAsDate()
    {
        return asDate;
    }

                        
    /**
     * Set the value of AsDate
     *
     * @param v new value
     */
    public void setAsDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.asDate, v))
              {
            this.asDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObTransId
     *
     * @return String
     */
    public String getObTransId()
    {
        return obTransId;
    }

                        
    /**
     * Set the value of ObTransId
     *
     * @param v new value
     */
    public void setObTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.obTransId, v))
              {
            this.obTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObRate
     *
     * @return BigDecimal
     */
    public BigDecimal getObRate()
    {
        return obRate;
    }

                        
    /**
     * Set the value of ObRate
     *
     * @param v new value
     */
    public void setObRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.obRate, v))
              {
            this.obRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAreaId
     *
     * @return String
     */
    public String getSalesAreaId()
    {
        return salesAreaId;
    }

                        
    /**
     * Set the value of SalesAreaId
     *
     * @param v new value
     */
    public void setSalesAreaId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAreaId, v))
              {
            this.salesAreaId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TerritoryId
     *
     * @return String
     */
    public String getTerritoryId()
    {
        return territoryId;
    }

                        
    /**
     * Set the value of TerritoryId
     *
     * @param v new value
     */
    public void setTerritoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.territoryId, v))
              {
            this.territoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Longitudes
     *
     * @return BigDecimal
     */
    public BigDecimal getLongitudes()
    {
        return longitudes;
    }

                        
    /**
     * Set the value of Longitudes
     *
     * @param v new value
     */
    public void setLongitudes(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.longitudes, v))
              {
            this.longitudes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Latitudes
     *
     * @return BigDecimal
     */
    public BigDecimal getLatitudes()
    {
        return latitudes;
    }

                        
    /**
     * Set the value of Latitudes
     *
     * @param v new value
     */
    public void setLatitudes(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.latitudes, v))
              {
            this.latitudes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransPrefix
     *
     * @return String
     */
    public String getTransPrefix()
    {
        return transPrefix;
    }

                        
    /**
     * Set the value of TransPrefix
     *
     * @param v new value
     */
    public void setTransPrefix(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transPrefix, v))
              {
            this.transPrefix = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreateBy
     *
     * @return String
     */
    public String getCreateBy()
    {
        return createBy;
    }

                        
    /**
     * Set the value of CreateBy
     *
     * @param v new value
     */
    public void setCreateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createBy, v))
              {
            this.createBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdateBy
     *
     * @return String
     */
    public String getLastUpdateBy()
    {
        return lastUpdateBy;
    }

                        
    /**
     * Set the value of LastUpdateBy
     *
     * @param v new value
     */
    public void setLastUpdateBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdateBy, v))
              {
            this.lastUpdateBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AllowReturn
     *
     * @return int
     */
    public int getAllowReturn()
    {
        return allowReturn;
    }

                        
    /**
     * Set the value of AllowReturn
     *
     * @param v new value
     */
    public void setAllowReturn(int v) 
    {
    
                  if (this.allowReturn != v)
              {
            this.allowReturn = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemType
     *
     * @return int
     */
    public int getItemType()
    {
        return itemType;
    }

                        
    /**
     * Set the value of ItemType
     *
     * @param v new value
     */
    public void setItemType(int v) 
    {
    
                  if (this.itemType != v)
              {
            this.itemType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConsignDisc
     *
     * @return String
     */
    public String getConsignDisc()
    {
        return consignDisc;
    }

                        
    /**
     * Set the value of ConsignDisc
     *
     * @param v new value
     */
    public void setConsignDisc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.consignDisc, v))
              {
            this.consignDisc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ConsignField
     *
     * @return String
     */
    public String getConsignField()
    {
        return consignField;
    }

                        
    /**
     * Set the value of ConsignField
     *
     * @param v new value
     */
    public void setConsignField(String v) 
    {
    
                  if (!ObjectUtils.equals(this.consignField, v))
              {
            this.consignField = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAccBank
     *
     * @return String
     */
    public String getPaymentAccBank()
    {
        return paymentAccBank;
    }

                        
    /**
     * Set the value of PaymentAccBank
     *
     * @param v new value
     */
    public void setPaymentAccBank(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAccBank, v))
              {
            this.paymentAccBank = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAccNo
     *
     * @return String
     */
    public String getPaymentAccNo()
    {
        return paymentAccNo;
    }

                        
    /**
     * Set the value of PaymentAccNo
     *
     * @param v new value
     */
    public void setPaymentAccNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAccNo, v))
              {
            this.paymentAccNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentAccName
     *
     * @return String
     */
    public String getPaymentAccName()
    {
        return paymentAccName;
    }

                        
    /**
     * Set the value of PaymentAccName
     *
     * @param v new value
     */
    public void setPaymentAccName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentAccName, v))
              {
            this.paymentAccName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentEmailTo
     *
     * @return String
     */
    public String getPaymentEmailTo()
    {
        return paymentEmailTo;
    }

                        
    /**
     * Set the value of PaymentEmailTo
     *
     * @param v new value
     */
    public void setPaymentEmailTo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentEmailTo, v))
              {
            this.paymentEmailTo = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("VendorId");
              fieldNames.add("VendorTypeId");
              fieldNames.add("VendorCode");
              fieldNames.add("VendorName");
              fieldNames.add("TaxNo");
              fieldNames.add("VendorStatus");
              fieldNames.add("ContactName1");
              fieldNames.add("ContactPhone1");
              fieldNames.add("JobTitle1");
              fieldNames.add("ContactName2");
              fieldNames.add("ContactPhone2");
              fieldNames.add("JobTitle2");
              fieldNames.add("ContactName3");
              fieldNames.add("ContactPhone3");
              fieldNames.add("JobTitle3");
              fieldNames.add("Address");
              fieldNames.add("Zip");
              fieldNames.add("Country");
              fieldNames.add("Province");
              fieldNames.add("City");
              fieldNames.add("District");
              fieldNames.add("Village");
              fieldNames.add("Phone1");
              fieldNames.add("Phone2");
              fieldNames.add("Fax");
              fieldNames.add("Email");
              fieldNames.add("WebSite");
              fieldNames.add("DefaultTaxId");
              fieldNames.add("DefaultTypeId");
              fieldNames.add("DefaultTermId");
              fieldNames.add("DefaultEmployeeId");
              fieldNames.add("DefaultCurrencyId");
              fieldNames.add("DefaultLocationId");
              fieldNames.add("DefaultItemDisc");
              fieldNames.add("DefaultDiscountAmount");
              fieldNames.add("CreditLimit");
              fieldNames.add("Memo");
              fieldNames.add("UpdateDate");
              fieldNames.add("LastUpdateLocationId");
              fieldNames.add("Field1");
              fieldNames.add("Field2");
              fieldNames.add("Value1");
              fieldNames.add("Value2");
              fieldNames.add("ApAccount");
              fieldNames.add("OpeningBalance");
              fieldNames.add("AsDate");
              fieldNames.add("ObTransId");
              fieldNames.add("ObRate");
              fieldNames.add("SalesAreaId");
              fieldNames.add("TerritoryId");
              fieldNames.add("Longitudes");
              fieldNames.add("Latitudes");
              fieldNames.add("TransPrefix");
              fieldNames.add("CreateBy");
              fieldNames.add("LastUpdateBy");
              fieldNames.add("AllowReturn");
              fieldNames.add("ItemType");
              fieldNames.add("ConsignDisc");
              fieldNames.add("ConsignField");
              fieldNames.add("PaymentAccBank");
              fieldNames.add("PaymentAccNo");
              fieldNames.add("PaymentAccName");
              fieldNames.add("PaymentEmailTo");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("VendorId"))
        {
                return getVendorId();
            }
          if (name.equals("VendorTypeId"))
        {
                return getVendorTypeId();
            }
          if (name.equals("VendorCode"))
        {
                return getVendorCode();
            }
          if (name.equals("VendorName"))
        {
                return getVendorName();
            }
          if (name.equals("TaxNo"))
        {
                return getTaxNo();
            }
          if (name.equals("VendorStatus"))
        {
                return Integer.valueOf(getVendorStatus());
            }
          if (name.equals("ContactName1"))
        {
                return getContactName1();
            }
          if (name.equals("ContactPhone1"))
        {
                return getContactPhone1();
            }
          if (name.equals("JobTitle1"))
        {
                return getJobTitle1();
            }
          if (name.equals("ContactName2"))
        {
                return getContactName2();
            }
          if (name.equals("ContactPhone2"))
        {
                return getContactPhone2();
            }
          if (name.equals("JobTitle2"))
        {
                return getJobTitle2();
            }
          if (name.equals("ContactName3"))
        {
                return getContactName3();
            }
          if (name.equals("ContactPhone3"))
        {
                return getContactPhone3();
            }
          if (name.equals("JobTitle3"))
        {
                return getJobTitle3();
            }
          if (name.equals("Address"))
        {
                return getAddress();
            }
          if (name.equals("Zip"))
        {
                return getZip();
            }
          if (name.equals("Country"))
        {
                return getCountry();
            }
          if (name.equals("Province"))
        {
                return getProvince();
            }
          if (name.equals("City"))
        {
                return getCity();
            }
          if (name.equals("District"))
        {
                return getDistrict();
            }
          if (name.equals("Village"))
        {
                return getVillage();
            }
          if (name.equals("Phone1"))
        {
                return getPhone1();
            }
          if (name.equals("Phone2"))
        {
                return getPhone2();
            }
          if (name.equals("Fax"))
        {
                return getFax();
            }
          if (name.equals("Email"))
        {
                return getEmail();
            }
          if (name.equals("WebSite"))
        {
                return getWebSite();
            }
          if (name.equals("DefaultTaxId"))
        {
                return getDefaultTaxId();
            }
          if (name.equals("DefaultTypeId"))
        {
                return getDefaultTypeId();
            }
          if (name.equals("DefaultTermId"))
        {
                return getDefaultTermId();
            }
          if (name.equals("DefaultEmployeeId"))
        {
                return getDefaultEmployeeId();
            }
          if (name.equals("DefaultCurrencyId"))
        {
                return getDefaultCurrencyId();
            }
          if (name.equals("DefaultLocationId"))
        {
                return getDefaultLocationId();
            }
          if (name.equals("DefaultItemDisc"))
        {
                return getDefaultItemDisc();
            }
          if (name.equals("DefaultDiscountAmount"))
        {
                return getDefaultDiscountAmount();
            }
          if (name.equals("CreditLimit"))
        {
                return getCreditLimit();
            }
          if (name.equals("Memo"))
        {
                return getMemo();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("LastUpdateLocationId"))
        {
                return getLastUpdateLocationId();
            }
          if (name.equals("Field1"))
        {
                return getField1();
            }
          if (name.equals("Field2"))
        {
                return getField2();
            }
          if (name.equals("Value1"))
        {
                return getValue1();
            }
          if (name.equals("Value2"))
        {
                return getValue2();
            }
          if (name.equals("ApAccount"))
        {
                return getApAccount();
            }
          if (name.equals("OpeningBalance"))
        {
                return getOpeningBalance();
            }
          if (name.equals("AsDate"))
        {
                return getAsDate();
            }
          if (name.equals("ObTransId"))
        {
                return getObTransId();
            }
          if (name.equals("ObRate"))
        {
                return getObRate();
            }
          if (name.equals("SalesAreaId"))
        {
                return getSalesAreaId();
            }
          if (name.equals("TerritoryId"))
        {
                return getTerritoryId();
            }
          if (name.equals("Longitudes"))
        {
                return getLongitudes();
            }
          if (name.equals("Latitudes"))
        {
                return getLatitudes();
            }
          if (name.equals("TransPrefix"))
        {
                return getTransPrefix();
            }
          if (name.equals("CreateBy"))
        {
                return getCreateBy();
            }
          if (name.equals("LastUpdateBy"))
        {
                return getLastUpdateBy();
            }
          if (name.equals("AllowReturn"))
        {
                return Integer.valueOf(getAllowReturn());
            }
          if (name.equals("ItemType"))
        {
                return Integer.valueOf(getItemType());
            }
          if (name.equals("ConsignDisc"))
        {
                return getConsignDisc();
            }
          if (name.equals("ConsignField"))
        {
                return getConsignField();
            }
          if (name.equals("PaymentAccBank"))
        {
                return getPaymentAccBank();
            }
          if (name.equals("PaymentAccNo"))
        {
                return getPaymentAccNo();
            }
          if (name.equals("PaymentAccName"))
        {
                return getPaymentAccName();
            }
          if (name.equals("PaymentEmailTo"))
        {
                return getPaymentEmailTo();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(VendorPeer.VENDOR_ID))
        {
                return getVendorId();
            }
          if (name.equals(VendorPeer.VENDOR_TYPE_ID))
        {
                return getVendorTypeId();
            }
          if (name.equals(VendorPeer.VENDOR_CODE))
        {
                return getVendorCode();
            }
          if (name.equals(VendorPeer.VENDOR_NAME))
        {
                return getVendorName();
            }
          if (name.equals(VendorPeer.TAX_NO))
        {
                return getTaxNo();
            }
          if (name.equals(VendorPeer.VENDOR_STATUS))
        {
                return Integer.valueOf(getVendorStatus());
            }
          if (name.equals(VendorPeer.CONTACT_NAME1))
        {
                return getContactName1();
            }
          if (name.equals(VendorPeer.CONTACT_PHONE1))
        {
                return getContactPhone1();
            }
          if (name.equals(VendorPeer.JOB_TITLE1))
        {
                return getJobTitle1();
            }
          if (name.equals(VendorPeer.CONTACT_NAME2))
        {
                return getContactName2();
            }
          if (name.equals(VendorPeer.CONTACT_PHONE2))
        {
                return getContactPhone2();
            }
          if (name.equals(VendorPeer.JOB_TITLE2))
        {
                return getJobTitle2();
            }
          if (name.equals(VendorPeer.CONTACT_NAME3))
        {
                return getContactName3();
            }
          if (name.equals(VendorPeer.CONTACT_PHONE3))
        {
                return getContactPhone3();
            }
          if (name.equals(VendorPeer.JOB_TITLE3))
        {
                return getJobTitle3();
            }
          if (name.equals(VendorPeer.ADDRESS))
        {
                return getAddress();
            }
          if (name.equals(VendorPeer.ZIP))
        {
                return getZip();
            }
          if (name.equals(VendorPeer.COUNTRY))
        {
                return getCountry();
            }
          if (name.equals(VendorPeer.PROVINCE))
        {
                return getProvince();
            }
          if (name.equals(VendorPeer.CITY))
        {
                return getCity();
            }
          if (name.equals(VendorPeer.DISTRICT))
        {
                return getDistrict();
            }
          if (name.equals(VendorPeer.VILLAGE))
        {
                return getVillage();
            }
          if (name.equals(VendorPeer.PHONE1))
        {
                return getPhone1();
            }
          if (name.equals(VendorPeer.PHONE2))
        {
                return getPhone2();
            }
          if (name.equals(VendorPeer.FAX))
        {
                return getFax();
            }
          if (name.equals(VendorPeer.EMAIL))
        {
                return getEmail();
            }
          if (name.equals(VendorPeer.WEB_SITE))
        {
                return getWebSite();
            }
          if (name.equals(VendorPeer.DEFAULT_TAX_ID))
        {
                return getDefaultTaxId();
            }
          if (name.equals(VendorPeer.DEFAULT_TYPE_ID))
        {
                return getDefaultTypeId();
            }
          if (name.equals(VendorPeer.DEFAULT_TERM_ID))
        {
                return getDefaultTermId();
            }
          if (name.equals(VendorPeer.DEFAULT_EMPLOYEE_ID))
        {
                return getDefaultEmployeeId();
            }
          if (name.equals(VendorPeer.DEFAULT_CURRENCY_ID))
        {
                return getDefaultCurrencyId();
            }
          if (name.equals(VendorPeer.DEFAULT_LOCATION_ID))
        {
                return getDefaultLocationId();
            }
          if (name.equals(VendorPeer.DEFAULT_ITEM_DISC))
        {
                return getDefaultItemDisc();
            }
          if (name.equals(VendorPeer.DEFAULT_DISCOUNT_AMOUNT))
        {
                return getDefaultDiscountAmount();
            }
          if (name.equals(VendorPeer.CREDIT_LIMIT))
        {
                return getCreditLimit();
            }
          if (name.equals(VendorPeer.MEMO))
        {
                return getMemo();
            }
          if (name.equals(VendorPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(VendorPeer.LAST_UPDATE_LOCATION_ID))
        {
                return getLastUpdateLocationId();
            }
          if (name.equals(VendorPeer.FIELD1))
        {
                return getField1();
            }
          if (name.equals(VendorPeer.FIELD2))
        {
                return getField2();
            }
          if (name.equals(VendorPeer.VALUE1))
        {
                return getValue1();
            }
          if (name.equals(VendorPeer.VALUE2))
        {
                return getValue2();
            }
          if (name.equals(VendorPeer.AP_ACCOUNT))
        {
                return getApAccount();
            }
          if (name.equals(VendorPeer.OPENING_BALANCE))
        {
                return getOpeningBalance();
            }
          if (name.equals(VendorPeer.AS_DATE))
        {
                return getAsDate();
            }
          if (name.equals(VendorPeer.OB_TRANS_ID))
        {
                return getObTransId();
            }
          if (name.equals(VendorPeer.OB_RATE))
        {
                return getObRate();
            }
          if (name.equals(VendorPeer.SALES_AREA_ID))
        {
                return getSalesAreaId();
            }
          if (name.equals(VendorPeer.TERRITORY_ID))
        {
                return getTerritoryId();
            }
          if (name.equals(VendorPeer.LONGITUDES))
        {
                return getLongitudes();
            }
          if (name.equals(VendorPeer.LATITUDES))
        {
                return getLatitudes();
            }
          if (name.equals(VendorPeer.TRANS_PREFIX))
        {
                return getTransPrefix();
            }
          if (name.equals(VendorPeer.CREATE_BY))
        {
                return getCreateBy();
            }
          if (name.equals(VendorPeer.LAST_UPDATE_BY))
        {
                return getLastUpdateBy();
            }
          if (name.equals(VendorPeer.ALLOW_RETURN))
        {
                return Integer.valueOf(getAllowReturn());
            }
          if (name.equals(VendorPeer.ITEM_TYPE))
        {
                return Integer.valueOf(getItemType());
            }
          if (name.equals(VendorPeer.CONSIGN_DISC))
        {
                return getConsignDisc();
            }
          if (name.equals(VendorPeer.CONSIGN_FIELD))
        {
                return getConsignField();
            }
          if (name.equals(VendorPeer.PAYMENT_ACC_BANK))
        {
                return getPaymentAccBank();
            }
          if (name.equals(VendorPeer.PAYMENT_ACC_NO))
        {
                return getPaymentAccNo();
            }
          if (name.equals(VendorPeer.PAYMENT_ACC_NAME))
        {
                return getPaymentAccName();
            }
          if (name.equals(VendorPeer.PAYMENT_EMAIL_TO))
        {
                return getPaymentEmailTo();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getVendorId();
            }
              if (pos == 1)
        {
                return getVendorTypeId();
            }
              if (pos == 2)
        {
                return getVendorCode();
            }
              if (pos == 3)
        {
                return getVendorName();
            }
              if (pos == 4)
        {
                return getTaxNo();
            }
              if (pos == 5)
        {
                return Integer.valueOf(getVendorStatus());
            }
              if (pos == 6)
        {
                return getContactName1();
            }
              if (pos == 7)
        {
                return getContactPhone1();
            }
              if (pos == 8)
        {
                return getJobTitle1();
            }
              if (pos == 9)
        {
                return getContactName2();
            }
              if (pos == 10)
        {
                return getContactPhone2();
            }
              if (pos == 11)
        {
                return getJobTitle2();
            }
              if (pos == 12)
        {
                return getContactName3();
            }
              if (pos == 13)
        {
                return getContactPhone3();
            }
              if (pos == 14)
        {
                return getJobTitle3();
            }
              if (pos == 15)
        {
                return getAddress();
            }
              if (pos == 16)
        {
                return getZip();
            }
              if (pos == 17)
        {
                return getCountry();
            }
              if (pos == 18)
        {
                return getProvince();
            }
              if (pos == 19)
        {
                return getCity();
            }
              if (pos == 20)
        {
                return getDistrict();
            }
              if (pos == 21)
        {
                return getVillage();
            }
              if (pos == 22)
        {
                return getPhone1();
            }
              if (pos == 23)
        {
                return getPhone2();
            }
              if (pos == 24)
        {
                return getFax();
            }
              if (pos == 25)
        {
                return getEmail();
            }
              if (pos == 26)
        {
                return getWebSite();
            }
              if (pos == 27)
        {
                return getDefaultTaxId();
            }
              if (pos == 28)
        {
                return getDefaultTypeId();
            }
              if (pos == 29)
        {
                return getDefaultTermId();
            }
              if (pos == 30)
        {
                return getDefaultEmployeeId();
            }
              if (pos == 31)
        {
                return getDefaultCurrencyId();
            }
              if (pos == 32)
        {
                return getDefaultLocationId();
            }
              if (pos == 33)
        {
                return getDefaultItemDisc();
            }
              if (pos == 34)
        {
                return getDefaultDiscountAmount();
            }
              if (pos == 35)
        {
                return getCreditLimit();
            }
              if (pos == 36)
        {
                return getMemo();
            }
              if (pos == 37)
        {
                return getUpdateDate();
            }
              if (pos == 38)
        {
                return getLastUpdateLocationId();
            }
              if (pos == 39)
        {
                return getField1();
            }
              if (pos == 40)
        {
                return getField2();
            }
              if (pos == 41)
        {
                return getValue1();
            }
              if (pos == 42)
        {
                return getValue2();
            }
              if (pos == 43)
        {
                return getApAccount();
            }
              if (pos == 44)
        {
                return getOpeningBalance();
            }
              if (pos == 45)
        {
                return getAsDate();
            }
              if (pos == 46)
        {
                return getObTransId();
            }
              if (pos == 47)
        {
                return getObRate();
            }
              if (pos == 48)
        {
                return getSalesAreaId();
            }
              if (pos == 49)
        {
                return getTerritoryId();
            }
              if (pos == 50)
        {
                return getLongitudes();
            }
              if (pos == 51)
        {
                return getLatitudes();
            }
              if (pos == 52)
        {
                return getTransPrefix();
            }
              if (pos == 53)
        {
                return getCreateBy();
            }
              if (pos == 54)
        {
                return getLastUpdateBy();
            }
              if (pos == 55)
        {
                return Integer.valueOf(getAllowReturn());
            }
              if (pos == 56)
        {
                return Integer.valueOf(getItemType());
            }
              if (pos == 57)
        {
                return getConsignDisc();
            }
              if (pos == 58)
        {
                return getConsignField();
            }
              if (pos == 59)
        {
                return getPaymentAccBank();
            }
              if (pos == 60)
        {
                return getPaymentAccNo();
            }
              if (pos == 61)
        {
                return getPaymentAccName();
            }
              if (pos == 62)
        {
                return getPaymentEmailTo();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(VendorPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        VendorPeer.doInsert((Vendor) this, con);
                        setNew(false);
                    }
                    else
                    {
                        VendorPeer.doUpdate((Vendor) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key vendorId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setVendorId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setVendorId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getVendorId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Vendor copy() throws TorqueException
    {
        return copyInto(new Vendor());
    }
  
    protected Vendor copyInto(Vendor copyObj) throws TorqueException
    {
          copyObj.setVendorId(vendorId);
          copyObj.setVendorTypeId(vendorTypeId);
          copyObj.setVendorCode(vendorCode);
          copyObj.setVendorName(vendorName);
          copyObj.setTaxNo(taxNo);
          copyObj.setVendorStatus(vendorStatus);
          copyObj.setContactName1(contactName1);
          copyObj.setContactPhone1(contactPhone1);
          copyObj.setJobTitle1(jobTitle1);
          copyObj.setContactName2(contactName2);
          copyObj.setContactPhone2(contactPhone2);
          copyObj.setJobTitle2(jobTitle2);
          copyObj.setContactName3(contactName3);
          copyObj.setContactPhone3(contactPhone3);
          copyObj.setJobTitle3(jobTitle3);
          copyObj.setAddress(address);
          copyObj.setZip(zip);
          copyObj.setCountry(country);
          copyObj.setProvince(province);
          copyObj.setCity(city);
          copyObj.setDistrict(district);
          copyObj.setVillage(village);
          copyObj.setPhone1(phone1);
          copyObj.setPhone2(phone2);
          copyObj.setFax(fax);
          copyObj.setEmail(email);
          copyObj.setWebSite(webSite);
          copyObj.setDefaultTaxId(defaultTaxId);
          copyObj.setDefaultTypeId(defaultTypeId);
          copyObj.setDefaultTermId(defaultTermId);
          copyObj.setDefaultEmployeeId(defaultEmployeeId);
          copyObj.setDefaultCurrencyId(defaultCurrencyId);
          copyObj.setDefaultLocationId(defaultLocationId);
          copyObj.setDefaultItemDisc(defaultItemDisc);
          copyObj.setDefaultDiscountAmount(defaultDiscountAmount);
          copyObj.setCreditLimit(creditLimit);
          copyObj.setMemo(memo);
          copyObj.setUpdateDate(updateDate);
          copyObj.setLastUpdateLocationId(lastUpdateLocationId);
          copyObj.setField1(field1);
          copyObj.setField2(field2);
          copyObj.setValue1(value1);
          copyObj.setValue2(value2);
          copyObj.setApAccount(apAccount);
          copyObj.setOpeningBalance(openingBalance);
          copyObj.setAsDate(asDate);
          copyObj.setObTransId(obTransId);
          copyObj.setObRate(obRate);
          copyObj.setSalesAreaId(salesAreaId);
          copyObj.setTerritoryId(territoryId);
          copyObj.setLongitudes(longitudes);
          copyObj.setLatitudes(latitudes);
          copyObj.setTransPrefix(transPrefix);
          copyObj.setCreateBy(createBy);
          copyObj.setLastUpdateBy(lastUpdateBy);
          copyObj.setAllowReturn(allowReturn);
          copyObj.setItemType(itemType);
          copyObj.setConsignDisc(consignDisc);
          copyObj.setConsignField(consignField);
          copyObj.setPaymentAccBank(paymentAccBank);
          copyObj.setPaymentAccNo(paymentAccNo);
          copyObj.setPaymentAccName(paymentAccName);
          copyObj.setPaymentEmailTo(paymentEmailTo);
  
                    copyObj.setVendorId((String)null);
                                                                                                                                                                                                                                                                                                                                                                                                
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public VendorPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Vendor\n");
        str.append("------\n")
           .append("VendorId             : ")
           .append(getVendorId())
           .append("\n")
           .append("VendorTypeId         : ")
           .append(getVendorTypeId())
           .append("\n")
           .append("VendorCode           : ")
           .append(getVendorCode())
           .append("\n")
           .append("VendorName           : ")
           .append(getVendorName())
           .append("\n")
           .append("TaxNo                : ")
           .append(getTaxNo())
           .append("\n")
           .append("VendorStatus         : ")
           .append(getVendorStatus())
           .append("\n")
           .append("ContactName1         : ")
           .append(getContactName1())
           .append("\n")
           .append("ContactPhone1        : ")
           .append(getContactPhone1())
           .append("\n")
           .append("JobTitle1            : ")
           .append(getJobTitle1())
           .append("\n")
           .append("ContactName2         : ")
           .append(getContactName2())
           .append("\n")
           .append("ContactPhone2        : ")
           .append(getContactPhone2())
           .append("\n")
           .append("JobTitle2            : ")
           .append(getJobTitle2())
           .append("\n")
           .append("ContactName3         : ")
           .append(getContactName3())
           .append("\n")
           .append("ContactPhone3        : ")
           .append(getContactPhone3())
           .append("\n")
           .append("JobTitle3            : ")
           .append(getJobTitle3())
           .append("\n")
           .append("Address              : ")
           .append(getAddress())
           .append("\n")
           .append("Zip                  : ")
           .append(getZip())
           .append("\n")
           .append("Country              : ")
           .append(getCountry())
           .append("\n")
           .append("Province             : ")
           .append(getProvince())
           .append("\n")
           .append("City                 : ")
           .append(getCity())
           .append("\n")
           .append("District             : ")
           .append(getDistrict())
           .append("\n")
           .append("Village              : ")
           .append(getVillage())
           .append("\n")
           .append("Phone1               : ")
           .append(getPhone1())
           .append("\n")
           .append("Phone2               : ")
           .append(getPhone2())
           .append("\n")
           .append("Fax                  : ")
           .append(getFax())
           .append("\n")
           .append("Email                : ")
           .append(getEmail())
           .append("\n")
           .append("WebSite              : ")
           .append(getWebSite())
           .append("\n")
           .append("DefaultTaxId         : ")
           .append(getDefaultTaxId())
           .append("\n")
           .append("DefaultTypeId        : ")
           .append(getDefaultTypeId())
           .append("\n")
           .append("DefaultTermId        : ")
           .append(getDefaultTermId())
           .append("\n")
           .append("DefaultEmployeeId    : ")
           .append(getDefaultEmployeeId())
           .append("\n")
           .append("DefaultCurrencyId    : ")
           .append(getDefaultCurrencyId())
           .append("\n")
           .append("DefaultLocationId    : ")
           .append(getDefaultLocationId())
           .append("\n")
           .append("DefaultItemDisc      : ")
           .append(getDefaultItemDisc())
           .append("\n")
            .append("DefaultDiscountAmount   : ")
           .append(getDefaultDiscountAmount())
           .append("\n")
           .append("CreditLimit          : ")
           .append(getCreditLimit())
           .append("\n")
           .append("Memo                 : ")
           .append(getMemo())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("LastUpdateLocationId   : ")
           .append(getLastUpdateLocationId())
           .append("\n")
           .append("Field1               : ")
           .append(getField1())
           .append("\n")
           .append("Field2               : ")
           .append(getField2())
           .append("\n")
           .append("Value1               : ")
           .append(getValue1())
           .append("\n")
           .append("Value2               : ")
           .append(getValue2())
           .append("\n")
           .append("ApAccount            : ")
           .append(getApAccount())
           .append("\n")
           .append("OpeningBalance       : ")
           .append(getOpeningBalance())
           .append("\n")
           .append("AsDate               : ")
           .append(getAsDate())
           .append("\n")
           .append("ObTransId            : ")
           .append(getObTransId())
           .append("\n")
           .append("ObRate               : ")
           .append(getObRate())
           .append("\n")
           .append("SalesAreaId          : ")
           .append(getSalesAreaId())
           .append("\n")
           .append("TerritoryId          : ")
           .append(getTerritoryId())
           .append("\n")
           .append("Longitudes           : ")
           .append(getLongitudes())
           .append("\n")
           .append("Latitudes            : ")
           .append(getLatitudes())
           .append("\n")
           .append("TransPrefix          : ")
           .append(getTransPrefix())
           .append("\n")
           .append("CreateBy             : ")
           .append(getCreateBy())
           .append("\n")
           .append("LastUpdateBy         : ")
           .append(getLastUpdateBy())
           .append("\n")
           .append("AllowReturn          : ")
           .append(getAllowReturn())
           .append("\n")
           .append("ItemType             : ")
           .append(getItemType())
           .append("\n")
           .append("ConsignDisc          : ")
           .append(getConsignDisc())
           .append("\n")
           .append("ConsignField         : ")
           .append(getConsignField())
           .append("\n")
           .append("PaymentAccBank       : ")
           .append(getPaymentAccBank())
           .append("\n")
           .append("PaymentAccNo         : ")
           .append(getPaymentAccNo())
           .append("\n")
           .append("PaymentAccName       : ")
           .append(getPaymentAccName())
           .append("\n")
           .append("PaymentEmailTo       : ")
           .append(getPaymentEmailTo())
           .append("\n")
        ;
        return(str.toString());
    }
}

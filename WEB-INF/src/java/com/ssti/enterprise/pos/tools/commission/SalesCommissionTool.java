package com.ssti.enterprise.pos.tools.commission;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.ArPaymentDetailPeer;
import com.ssti.enterprise.pos.om.SalesReturnPeer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.om.SalesTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.sales.SalesAnalysisTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SalesCommissionTool.java,v 1.6 2008/02/26 05:13:17 albert Exp $ <br>
 *
 * <pre>
 * $Log: SalesCommissionTool.java,v $
 * Revision 1.6  2008/02/26 05:13:17  albert
 * *** empty log message ***
 *
 * Revision 1.5  2007/05/18 05:01:10  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class SalesCommissionTool extends BaseTool
{
	private static final Log log = LogFactory.getLog(SalesCommissionTool.class);
	
	static SalesCommissionTool instance = null;
	
	public static synchronized SalesCommissionTool getInstance() 
	{
		if (instance == null) instance = new SalesCommissionTool();
		return instance;
	}	
	
	private static final String m_sField = CONFIG.getString("salescommission.term.field");

	public static String getField()
	{
		return m_sField;
	}
	
	public List getInvoices (Date _dFrom, Date _dTo, String _sEmpID, String _sLocID, boolean _bPaidOnly)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		
		if (_bPaidOnly)
		{
			oCrit.add(SalesTransactionPeer.PAYMENT_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
		    oCrit.and(SalesTransactionPeer.PAYMENT_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		    oCrit.add(SalesTransactionPeer.PAID_AMOUNT, (Object) "sales_transaction.total_amount = sales_transaction.paid_amount ", Criteria.CUSTOM);
		}
		else
		{
		    oCrit.add(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
		    oCrit.and(SalesTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);			
		}
		if (StringUtil.isNotEmpty(_sLocID))
		{
		    oCrit.and(SalesTransactionPeer.LOCATION_ID, _sLocID);						
		}
	    if (StringUtil.isNotEmpty(_sEmpID)) 
	    {
	    	oCrit.add(SalesTransactionPeer.SALES_ID, _sEmpID);
	    }
	    oCrit.add(SalesTransactionPeer.STATUS, i_TRANS_PROCESSED);
	    log.debug(oCrit);
	    return SalesTransactionPeer.doSelect(oCrit);
	}
	
	public static double countCommission (SalesTransaction _oTR)
		throws Exception
	{
		List vData = TransactionTool.getDetailsByID(_oTR.getSalesTransactionId());
		double dTotal = 0;
		for (int i = 0; i < vData.size(); i++)
		{
			SalesTransactionDetail oDet = (SalesTransactionDetail) vData.get(i);
			double dSubTotal = (oDet.getSubTotal().doubleValue() - oDet.getSubTotalTax().doubleValue());
			String sCommission = "0";
			if (StringUtil.isNotEmpty(m_sField))
			{
				sCommission = ItemFieldTool.getValue(oDet.getItemId(), m_sField);
			}
			log.debug("Item " + oDet.getItemCode());
			log.debug("sCommission " + sCommission);

			dTotal += countCommission (oDet.getItemCode(), sCommission, dSubTotal);
		}
		return dTotal;
	}
	public static double countCommission (String _sItemCode, String _sComm, double dSubTotal)
		throws Exception
	{
		double dCommission = 0;
		if (_sComm != null)
		{
			int iPctIdx = _sComm.indexOf('%');
			if (iPctIdx > 0)
			{
				String sNumber = _sComm.substring(0, iPctIdx); 
				log.debug("sNumber " + sNumber);
				double dCommissionRate = 0;
				try
				{
					dCommissionRate = Double.parseDouble(sNumber);
					log.debug("dCommissionRate " + dCommissionRate);
				}
				catch (NumberFormatException _oNEx)
				{
					log.error("Commission for item : " + _sItemCode + " INVALID (" + _sComm + ")");
				}
				dCommission = dCommissionRate / 100 * dSubTotal;
				log.debug("dCommission " + dCommission);
			}
		}	
		return dCommission;
	}	
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static Record getTotalSalesBySalesID (Date _dStart, 
											     Date _dEnd, 
											     int _iStatus,
												 String _sSalesID,
												 String _sLocationID,
												 String _sDepartmentID,
												 String _sProjectID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.sales_id, SUM(sd.qty_base), ");
		oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((sd.sub_total * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
		oSQL.append (" ELSE SUM(sd.sub_total * s.currency_rate) END, ");		
		oSQL.append (" SUM(s.paid_amount - s.total_tax - s.total_expense), SUM(sd.sub_total_cost) ");
		oSQL.append (" FROM sales_transaction s, sales_transaction_detail sd WHERE ");

		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append (" AND s.sales_transaction_id = sd.sales_transaction_id ");
		oSQL.append (" AND s.sales_id = '").append(_sSalesID).append("'");
		oSQL.append (" GROUP BY s.sales_id, s.is_inclusive_tax ");
		
		log.debug (oSQL.toString());

		List vData =  SalesTransactionPeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			return (Record) vData.get(0);
		}
		return null;
	}	
	
	/**
	 * 
	 * @param _dStart
	 * @param _dEnd
	 * @param _sLocationID
	 * @param _iStatus
	 * @return
	 * @throws Exception
	 */
	public static Record getTotalReturnBySalesID (Date _dStart, 
											      Date _dEnd, 
											      int _iStatus,
											      String _sSalesID,
											      String _sLocationID,
											      String _sDepartmentID,
											      String _sProjectID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.sales_id, SUM(sd.qty_base), ");
		oSQL.append (" CASE WHEN s.is_inclusive_tax = 'TRUE' THEN SUM((sd.return_amount * s.currency_rate) - (sd.sub_total_tax * s.fiscal_rate)) ");
		oSQL.append (" ELSE SUM(sd.return_amount * s.currency_rate) END ");		
		oSQL.append (" FROM sales_return s, sales_return_detail sd WHERE ");

		SalesAnalysisTool.buildStdQuery(oSQL, _dStart, _dEnd, _iStatus, "s.return_date", _sLocationID, _sDepartmentID, _sProjectID);
		
		oSQL.append (" AND s.sales_return_id = sd.sales_return_id ");
		oSQL.append (" AND s.sales_id = '").append(_sSalesID).append("'");
		oSQL.append (" GROUP BY s.sales_id, s.is_inclusive_tax ");
		
		log.debug (oSQL.toString());

		List vData =  SalesReturnPeer.executeQuery(oSQL.toString());	
		if (vData.size() > 0)
		{
			return (Record) vData.get(0);
		}
		return null;
	}	
	
	public static List getPaymentBySalesID (Date _dStart, Date _dEnd, String _sSalesID)
		throws Exception
	{		
		StringBuilder oSQL = new StringBuilder ();
		oSQL.append ("SELECT s.sales_transaction_id, s.invoice_no, s.customer_id, s.customer_name, s.transaction_date, ");
		oSQL.append (" s.total_amount, s.total_tax, s.total_expense, ");
		oSQL.append (" ar.ar_payment_id, ar.ar_payment_no, ar.ar_payment_date, ");		
		oSQL.append (" ard.payment_amount, ard.sales_transaction_amount, ");		
		oSQL.append (" cf.cash_flow_id, cf.cash_flow_no, cf.reference_no, cf.due_date ");		
		oSQL.append (" FROM ar_payment ar, ar_payment_detail ard, cash_flow cf, sales_transaction s WHERE ");
		SalesAnalysisTool.buildDateQuery(oSQL, "cf.due_date", _dStart, _dEnd);
		oSQL.append (" AND cf.status = 2");		
		oSQL.append (" AND ar.ar_payment_id = ard.ar_payment_id ");
		oSQL.append (" AND ar.ar_payment_id = cf.transaction_id ");
		oSQL.append (" AND ard.sales_transaction_id = s.sales_transaction_id" );
		oSQL.append (" AND s.sales_id = '").append(_sSalesID).append("'");
		
		log.debug (oSQL.toString());

		return ArPaymentDetailPeer.executeQuery(oSQL.toString());	
			
		/*
		Criteria oCrit = new Criteria();
		oCrit.add(CashFlowPeer.DUE_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);
		oCrit.and(CashFlowPeer.DUE_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		oCrit.add(ArPaymentPeer.CF_STATUS, i_PROCESSED);
		oCrit.addJoin(ArPaymentDetailPeer.AR_PAYMENT_ID, ArPaymentPeer.AR_PAYMENT_ID);
		oCrit.addJoin(CashFlowPeer.CASH_FLOW_ID, ArPaymentPeer.CASH_FLOW_ID);
		oCrit.addJoin(ArPaymentDetailPeer.SALES_TRANSACTION_ID, SalesTransactionPeer.SALES_TRANSACTION_ID);
		oCrit.add(SalesTransactionPeer.SALES_ID, _sSalesID);		
		return ArPaymentDetailPeer.doSelect(oCrit);
		*/
	}	



}

package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class EndPeriodCurrencyMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.EndPeriodCurrencyMapBuilder";

    /**
     * Item
     * @deprecated use EndPeriodCurrencyPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "end_period_currency";
    }

  
    /**
     * end_period_currency.END_PERIOD_CURRENCY_ID
     * @return the column name for the END_PERIOD_CURRENCY_ID field
     * @deprecated use EndPeriodCurrencyPeer.end_period_currency.END_PERIOD_CURRENCY_ID constant
     */
    public static String getEndPeriodCurrency_EndPeriodCurrencyId()
    {
        return "end_period_currency.END_PERIOD_CURRENCY_ID";
    }
  
    /**
     * end_period_currency.PERIOD_ID
     * @return the column name for the PERIOD_ID field
     * @deprecated use EndPeriodCurrencyPeer.end_period_currency.PERIOD_ID constant
     */
    public static String getEndPeriodCurrency_PeriodId()
    {
        return "end_period_currency.PERIOD_ID";
    }
  
    /**
     * end_period_currency.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use EndPeriodCurrencyPeer.end_period_currency.CURRENCY_ID constant
     */
    public static String getEndPeriodCurrency_CurrencyId()
    {
        return "end_period_currency.CURRENCY_ID";
    }
  
    /**
     * end_period_currency.CLOSING_RATE
     * @return the column name for the CLOSING_RATE field
     * @deprecated use EndPeriodCurrencyPeer.end_period_currency.CLOSING_RATE constant
     */
    public static String getEndPeriodCurrency_ClosingRate()
    {
        return "end_period_currency.CLOSING_RATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("end_period_currency");
        TableMap tMap = dbMap.getTable("end_period_currency");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("end_period_currency.END_PERIOD_CURRENCY_ID", "");
                          tMap.addColumn("end_period_currency.PERIOD_ID", "");
                          tMap.addColumn("end_period_currency.CURRENCY_ID", "");
                            tMap.addColumn("end_period_currency.CLOSING_RATE", bd_ZERO);
          }
}

package com.ssti.enterprise.pos.presentation.actions.setup;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.CurrencyManager;
import com.ssti.enterprise.pos.om.CurrencyDetail;
import com.ssti.enterprise.pos.om.CurrencyDetailPeer;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Currency rate action class
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: MultiCurrencyDetailsAction.java,v 1.9 2009/05/04 01:58:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: MultiCurrencyDetailsAction.java,v $
 * Revision 1.9  2009/05/04 01:58:15  albert
 * *** empty log message ***
 *
 * Revision 1.8  2008/06/29 07:03:21  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/07/12 03:44:57  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/03/12 08:54:27  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class MultiCurrencyDetailsAction extends GLSecureAction
{
	private static Log log = LogFactory.getLog(MultiCurrencyDetailsAction.class);
	
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			CurrencyDetail oCurr = new CurrencyDetail();
			setProperties (oCurr, data);
			oCurr.setCurrencyDetailId (IDGenerator.generateSysID());
			oCurr.setUpdateDate(new Date());
	        oCurr.save();
	        CurrencyManager.getInstance().refreshCache(oCurr.getCurrencyId());
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();
        	log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
         }
    }

    public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			String sDetID = data.getParameters().getString("CurrencyDetailID");
	        CurrencyDetail oCurr = CurrencyTool.getDetailByID(sDetID, null);
			setProperties (oCurr, data);
			oCurr.setUpdateDate(new Date());
			oCurr.save();
	        CurrencyManager.getInstance().refreshCache(oCurr.getCurrencyId());
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	_oEx.printStackTrace();
        	log.error(_oEx);
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sCurrencyID = data.getParameters().getString("CurrencyID");
	    	Criteria oCrit = new Criteria();
	        oCrit.add(CurrencyDetailPeer.CURRENCY_DETAIL_ID, data.getParameters().getString("CurrencyDetailID"));
	        CurrencyDetailPeer.doDelete(oCrit);
	        CurrencyManager.getInstance().refreshCache(sCurrencyID);
        	data.setMessage(LocaleTool.getString(s_DELETE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	log.error(_oEx);
        	data.setMessage(LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }

	private void setProperties (CurrencyDetail _oCurr, RunData data)
		throws Exception
	{
    	data.getParameters().setProperties(_oCurr);
    	_oCurr.setCurrencyId (data.getParameters().getString ("id"));
    	
    	Date dStart = CustomParser.parseDate(data.getParameters().getString("BeginDate"));
    	Date dEnd = CustomParser.parseDate(data.getParameters().getString ("EndDate"));
		_oCurr.setBeginDate (DateUtil.getStartOfDayDate(dStart));
	    _oCurr.setEndDate (DateUtil.getEndOfDayDate(dEnd));
    }
}

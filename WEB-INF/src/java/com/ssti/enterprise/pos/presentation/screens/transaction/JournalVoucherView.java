package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.turbine.LargeSelectHandler;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/JournalVoucherView.java,v $
 *
 * Purpose: Journal Voucher View Screen
 *
 * @author  $Author: albert $
 * @version $Id: JournalVoucherView.java,v 1.1 2005/08/29 10:09:46 albert Exp $
 *
 * $Log: JournalVoucherView.java,v $
 * Revision 1.1  2005/08/29 10:09:46  albert
 * *** empty log message ***
 *
 */
 
public class JournalVoucherView extends JournalVoucherTransaction
{
	static final JournalVoucherTool oTool = new JournalVoucherTool();
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		context.put("journalvoucher", oTool);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findJournalVoucherResult", "vJV");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}

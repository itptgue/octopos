package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.ArPayment;
import com.ssti.enterprise.pos.om.CreditMemo;
import com.ssti.enterprise.pos.om.SalesOrder;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.financial.CreditMemoTool;
import com.ssti.enterprise.pos.tools.financial.ReceivablePaymentTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.CustomFormatter;

public class CreditMemoTransaction extends TransactionSecureScreen
{
	private static final String[] a_PERM = {"View Credit Memo"};
	
    protected static final int i_OP_SET  = 1;
	protected static final int i_OP_VIEW  = 4; 
	protected static final int i_OP_NEW_FROM_PMT = 5;
	protected static final int i_OP_NEW_FROM_ORD = 6;	
		
	protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		//return isAuthorized (data, a_PERM);
		return true;
	}
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	super.doBuildTemplate(data, context);
    	
    	int iOp = data.getParameters().getInt("op");
		String sID = "";
		try
		{
			if (iOp == i_OP_VIEW)
			{
				sID = data.getParameters().getString ("id");
				context.put("CreditMemo", CreditMemoTool.getCreditMemoByID(sID));
    		}
			else if (iOp == i_OP_SET)
			{
				CreditMemo oDM = new CreditMemo();
				data.getParameters().setProperties(oDM);
				context.put("CreditMemo", oDM);
			}
			else if (iOp == i_OP_NEW_FROM_PMT)
			{
				String sPmtID = data.getParameters().getString("paymentid");
				ArPayment oPMT = ReceivablePaymentTool.getHeaderByID(sPmtID);
				if (oPMT != null)
				{
					double dAmt = oPMT.getTotalAmount().doubleValue();
					double dRate = oPMT.getCurrencyRate().doubleValue();
					if (dAmt < 0) dAmt = dAmt * -1;
					
					List vMemo = CreditMemoTool.getByPaymentTransID(oPMT.getTransactionId());
					String sMemoNo = "";
					StringBuilder oRemark = new StringBuilder();
					oRemark.append("Automatic Memo from Payment ").append(oPMT.getTransactionNo());
				    if (vMemo.size() > 0) oRemark.append(" Used Memo: \n");
					for (int i = 0; i < vMemo.size(); i++)
				    {
				    	CreditMemo oMemo = (CreditMemo)vMemo.get(i);
				    	oRemark.append(oMemo.getMemoNo());
				    	oRemark.append("(").append(CustomFormatter.formatNumber(oMemo.getAmount())).append(") ");
				    	if (i != (vMemo.size() - 1)) oRemark.append("\n");
				    }
					
					CreditMemo oMemo = new CreditMemo();
					BeanUtil.setBean2BeanProperties(oPMT,oMemo);
					oMemo.setCreditMemoId("");
					oMemo.setNew(true);
					oMemo.setModified(true);					
					oMemo.setPaymentTransId("");
					oMemo.setPaymentTransNo("");
					oMemo.setTransactionId("");
					oMemo.setTransactionNo("");					
					oMemo.setTransactionDate(new Date());					
					oMemo.setAmount(new BigDecimal(dAmt));
					oMemo.setAmountBase(new BigDecimal(dAmt * dRate));
					oMemo.setRemark(oRemark.toString());
					oMemo.setFromPaymentId(oPMT.getTransactionId());
					context.put("CreditMemo", oMemo);	
				}
			}
			else if (iOp == i_OP_NEW_FROM_ORD)
			{
				String sOrderID = data.getParameters().getString("orderid");
				SalesOrder oSO = SalesOrderTool.getHeaderByID(sOrderID);
				if (oSO != null)
				{
					CreditMemo oMemo = CreditMemoTool.getBySourceTransID(oSO.getSalesOrderId());
					if( oMemo == null)
					{
						double dAmountBase = oSO.getCurrencyRate().doubleValue() * oSO.getTotalAmount().doubleValue();
						Date dToday = new Date();
						Date dDueDate = new Date();
	
						oMemo = new CreditMemo();
						oMemo.setTransactionDate  (oSO.getTransactionDate());
						oMemo.setTransactionId    (oSO.getSalesOrderId());
						oMemo.setTransactionNo    (oSO.getSalesOrderNo());
						oMemo.setTransactionType  (i_AR_TRANS_SO_DOWNPAYMENT);			
						oMemo.setCustomerId       (oSO.getCustomerId());
						oMemo.setCustomerName     (oSO.getCustomerName());
						oMemo.setUserName         (oSO.getUserName());
						oMemo.setCurrencyId       (oSO.getCurrencyId());
						oMemo.setCurrencyRate     (oSO.getCurrencyRate());
						oMemo.setAmount           (oSO.getTotalAmount());
						oMemo.setAmountBase       (new BigDecimal(dAmountBase));						
						oMemo.setBankId           (oSO.getBankId());
						oMemo.setBankIssuer       (oSO.getBankIssuer());
						oMemo.setReferenceNo      (oSO.getReferenceNo());
						oMemo.setDueDate          (oSO.getDpDueDate());
						oMemo.setAccountId        (oSO.getDpAccountId());
						oMemo.setCashFlowTypeId	  (oSO.getCashFlowTypeId());
						oMemo.setLocationId		  (oSO.getLocationId());
						
						StringBuilder oRemark = new StringBuilder();
						oRemark.append(oSO.getRemark());
						oRemark.append("\n DP ").append(LocaleTool.getString("from")).append(" ");
						oRemark.append(oSO.getSalesOrderNo());
						oRemark.append("\n").append(oSO.getRemark());
						oMemo.setRemark (oRemark.toString());
			            oMemo.setUpdateOrder(true);	
			            oMemo.setStatus(i_MEMO_NEW);
					}
		            context.put("CreditMemo", oMemo);						
				}
			}
			
    	}
    	catch(Exception _oEx)
    	{
			data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
        context.put(s_MULTI_CURRENCY, Boolean.valueOf(b_SALES_MULTI_CURRENCY));
    }
}

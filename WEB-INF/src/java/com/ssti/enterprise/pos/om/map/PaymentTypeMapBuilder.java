package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PaymentTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PaymentTypeMapBuilder";

    /**
     * Item
     * @deprecated use PaymentTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "payment_type";
    }

  
    /**
     * payment_type.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use PaymentTypePeer.payment_type.PAYMENT_TYPE_ID constant
     */
    public static String getPaymentType_PaymentTypeId()
    {
        return "payment_type.PAYMENT_TYPE_ID";
    }
  
    /**
     * payment_type.PAYMENT_TYPE_CODE
     * @return the column name for the PAYMENT_TYPE_CODE field
     * @deprecated use PaymentTypePeer.payment_type.PAYMENT_TYPE_CODE constant
     */
    public static String getPaymentType_PaymentTypeCode()
    {
        return "payment_type.PAYMENT_TYPE_CODE";
    }
  
    /**
     * payment_type.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PaymentTypePeer.payment_type.DESCRIPTION constant
     */
    public static String getPaymentType_Description()
    {
        return "payment_type.DESCRIPTION";
    }
  
    /**
     * payment_type.IS_DEFAULT
     * @return the column name for the IS_DEFAULT field
     * @deprecated use PaymentTypePeer.payment_type.IS_DEFAULT constant
     */
    public static String getPaymentType_IsDefault()
    {
        return "payment_type.IS_DEFAULT";
    }
  
    /**
     * payment_type.IS_CREDIT
     * @return the column name for the IS_CREDIT field
     * @deprecated use PaymentTypePeer.payment_type.IS_CREDIT constant
     */
    public static String getPaymentType_IsCredit()
    {
        return "payment_type.IS_CREDIT";
    }
  
    /**
     * payment_type.IS_CREDIT_CARD
     * @return the column name for the IS_CREDIT_CARD field
     * @deprecated use PaymentTypePeer.payment_type.IS_CREDIT_CARD constant
     */
    public static String getPaymentType_IsCreditCard()
    {
        return "payment_type.IS_CREDIT_CARD";
    }
  
    /**
     * payment_type.IS_COUPON
     * @return the column name for the IS_COUPON field
     * @deprecated use PaymentTypePeer.payment_type.IS_COUPON constant
     */
    public static String getPaymentType_IsCoupon()
    {
        return "payment_type.IS_COUPON";
    }
  
    /**
     * payment_type.IS_VOUCHER
     * @return the column name for the IS_VOUCHER field
     * @deprecated use PaymentTypePeer.payment_type.IS_VOUCHER constant
     */
    public static String getPaymentType_IsVoucher()
    {
        return "payment_type.IS_VOUCHER";
    }
  
    /**
     * payment_type.IS_MULTIPLE_PAYMENT
     * @return the column name for the IS_MULTIPLE_PAYMENT field
     * @deprecated use PaymentTypePeer.payment_type.IS_MULTIPLE_PAYMENT constant
     */
    public static String getPaymentType_IsMultiplePayment()
    {
        return "payment_type.IS_MULTIPLE_PAYMENT";
    }
  
    /**
     * payment_type.IS_CASH_BACK
     * @return the column name for the IS_CASH_BACK field
     * @deprecated use PaymentTypePeer.payment_type.IS_CASH_BACK constant
     */
    public static String getPaymentType_IsCashBack()
    {
        return "payment_type.IS_CASH_BACK";
    }
  
    /**
     * payment_type.IS_POINT_REWARD
     * @return the column name for the IS_POINT_REWARD field
     * @deprecated use PaymentTypePeer.payment_type.IS_POINT_REWARD constant
     */
    public static String getPaymentType_IsPointReward()
    {
        return "payment_type.IS_POINT_REWARD";
    }
  
    /**
     * payment_type.SHOW_IN_POS
     * @return the column name for the SHOW_IN_POS field
     * @deprecated use PaymentTypePeer.payment_type.SHOW_IN_POS constant
     */
    public static String getPaymentType_ShowInPos()
    {
        return "payment_type.SHOW_IN_POS";
    }
  
    /**
     * payment_type.DIRECT_SALES_ACCOUNT
     * @return the column name for the DIRECT_SALES_ACCOUNT field
     * @deprecated use PaymentTypePeer.payment_type.DIRECT_SALES_ACCOUNT constant
     */
    public static String getPaymentType_DirectSalesAccount()
    {
        return "payment_type.DIRECT_SALES_ACCOUNT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("payment_type");
        TableMap tMap = dbMap.getTable("payment_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("payment_type.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("payment_type.PAYMENT_TYPE_CODE", "");
                          tMap.addColumn("payment_type.DESCRIPTION", "");
                          tMap.addColumn("payment_type.IS_DEFAULT", Boolean.TRUE);
                          tMap.addColumn("payment_type.IS_CREDIT", Boolean.TRUE);
                          tMap.addColumn("payment_type.IS_CREDIT_CARD", Boolean.TRUE);
                          tMap.addColumn("payment_type.IS_COUPON", Boolean.TRUE);
                          tMap.addColumn("payment_type.IS_VOUCHER", Boolean.TRUE);
                          tMap.addColumn("payment_type.IS_MULTIPLE_PAYMENT", Boolean.TRUE);
                          tMap.addColumn("payment_type.IS_CASH_BACK", Boolean.TRUE);
                          tMap.addColumn("payment_type.IS_POINT_REWARD", Boolean.TRUE);
                          tMap.addColumn("payment_type.SHOW_IN_POS", Boolean.TRUE);
                          tMap.addColumn("payment_type.DIRECT_SALES_ACCOUNT", "");
          }
}

package com.ssti.enterprise.pos.presentation.actions.setup;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.PeriodManager;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.JournalVoucherTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * </pre><br>
 */
public class PeriodSetupAction extends GLSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Period oPeriod = new Period();
			setProperties (oPeriod, data);
			oPeriod.setPeriodId (IDGenerator.generateSysID());
		    oPeriod.save();
		    PeriodManager.getInstance().refreshCache(oPeriod);
		    data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Period oPeriod = PeriodTool.getPeriodByID(data.getParameters().getString("ID"));
		    setProperties (oPeriod, data);
		    oPeriod.save();
		    PeriodManager.getInstance().refreshCache(oPeriod);
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

    private void setProperties (Period  _oPeriod,  RunData data)
        throws Exception
    {
		Date dBeginDate = CustomParser.parseDate (data.getParameters().getString ("BeginDate"));
        Date dEndDate = CustomParser.parseDateTime (data.getParameters().getString ("EndDate") + " 23:59");
		if (dBeginDate != null && dEndDate != null)
		{
		    String sPeriodCode = CustomFormatter.formatCustomDate (dEndDate, "yyyyMM");    
            _oPeriod.setPeriodCode  (Integer.parseInt(sPeriodCode));
            _oPeriod.setMonth (Integer.parseInt(CustomFormatter.formatCustomDate (dEndDate, "MM")));
            _oPeriod.setYear (Integer.parseInt(CustomFormatter.formatCustomDate (dEndDate, "yyyy")));
            
        }			
        else 
        {
            throw new Exception ("Invalid Period Date Entry");
        }
		_oPeriod.setBeginDate   (dBeginDate);
        _oPeriod.setEndDate     (dEndDate);
		_oPeriod.setDescription (data.getParameters().getString ("Description"));
		_oPeriod.setIsClosed    (data.getParameters().getBoolean ("IsClosed"));	
    }

	//-------------------------------------------------------------------------
	// FA CLOSING
	//-------------------------------------------------------------------------
	public void doFaclose(RunData data, Context context)
	    throws Exception
	{
		String sPeriodID = data.getParameters().getString("ID");
		if (StringUtil.isNotEmpty(sPeriodID))
		{
			try
			{				
				PeriodTool.closeFAPeriod(sPeriodID, data.getUser().getName());
		    	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
			}
		    catch (Exception _oEx)
		    {
		    	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
		    }
		}
		PeriodManager.getInstance().refreshCache(sPeriodID);
	}    

	public void doFareopen(RunData data, Context context)
	    throws Exception
	{
		String sPeriodID = data.getParameters().getString("ID");
		try
		{
			PeriodTool.reopenFAPeriod(sPeriodID, data.getUser().getName());
	    	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
	    	
		}
	    catch (Exception _oEx)
	    {
	    	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	    }
	    PeriodManager.getInstance().refreshCache(sPeriodID);
	}     

	//-------------------------------------------------------------------------
	// PERIOD CLOSING
	//-------------------------------------------------------------------------
	public void doClose(RunData data, Context context)
	    throws Exception
	{
		String sPeriodID = data.getParameters().getString("ID");
		if (StringUtil.isNotEmpty(sPeriodID))
		{
			try
			{
				List vCurrency = CurrencyTool.getAllCurrency();
				Map mRates = new HashMap();
				for (int i = 0; i < vCurrency.size(); i++)
				{
					Currency oCurr = (Currency) vCurrency.get(i);
					if (!oCurr.getIsDefault())
					{
						double dRate = data.getParameters().getDouble("rate" + oCurr.getCurrencyId()); 
						if(dRate > 0 && dRate != 1)
						{
							mRates.put(oCurr.getCurrencyId(), dRate);
						}
					}
				}
				PeriodTool.closePeriod(sPeriodID, data.getUser().getName(), mRates);
		    	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
			}
		    catch (Exception _oEx)
		    {
		    	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
		    }
		}
		PeriodManager.getInstance().refreshCache(sPeriodID);
	}    
	
	public void doReopen(RunData data, Context context)
	    throws Exception
	{
		String sPeriodID = data.getParameters().getString("ID");
		try
		{
			PeriodTool.reopenPeriod(sPeriodID, data.getUser().getName());
	    	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
		}
	    catch (Exception _oEx)
	    {
	    	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	    }
	    PeriodManager.getInstance().refreshCache(sPeriodID);
	}    
	
	//-------------------------------------------------------------------------
	// PERIOD PL CLOSING
	//-------------------------------------------------------------------------
	public void doClosepl(RunData data, Context context)
	    throws Exception
	{
		String sPeriodID = data.getParameters().getString("ID");
		if (StringUtil.isNotEmpty(sPeriodID))
		{
			
			try
			{
				Period oPeriod = PeriodTool.getPeriodByID(sPeriodID);
				JournalVoucherTool.createClosePL(oPeriod.getEndDate(), data.getUser().getName());
		    	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
			}
		    catch (Exception _oEx)
		    {
		    	data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
		    }
		}
		PeriodManager.getInstance().refreshCache(sPeriodID);
	} 
	
}

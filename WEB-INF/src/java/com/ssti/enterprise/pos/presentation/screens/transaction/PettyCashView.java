package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class PettyCashView extends TransactionSecureScreen
{
	protected static final String[] a_PERM = {"View Petty Cash"};

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
    
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findPettyCashResult", "PettyCashes");
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
    }
}

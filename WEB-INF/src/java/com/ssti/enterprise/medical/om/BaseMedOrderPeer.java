package com.ssti.enterprise.medical.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.om.map.MedOrderMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


/**
 */
public abstract class BaseMedOrderPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "med_order";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(MedOrderMapBuilder.CLASS_NAME);
    }

      /** the column name for the MED_ORDER_ID field */
    public static final String MED_ORDER_ID;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the STATUS field */
    public static final String STATUS;
      /** the column name for the ORDER_NO field */
    public static final String ORDER_NO;
      /** the column name for the INVOICE_NO field */
    public static final String INVOICE_NO;
      /** the column name for the DELIVERY_NO field */
    public static final String DELIVERY_NO;
      /** the column name for the TRANSACTION_DATE field */
    public static final String TRANSACTION_DATE;
      /** the column name for the DUE_DATE field */
    public static final String DUE_DATE;
      /** the column name for the CUSTOMER_ID field */
    public static final String CUSTOMER_ID;
      /** the column name for the CUSTOMER_NAME field */
    public static final String CUSTOMER_NAME;
      /** the column name for the TOTAL_QTY field */
    public static final String TOTAL_QTY;
      /** the column name for the TOTAL_AMOUNT field */
    public static final String TOTAL_AMOUNT;
      /** the column name for the TOTAL_DISCOUNT_PCT field */
    public static final String TOTAL_DISCOUNT_PCT;
      /** the column name for the TOTAL_DISCOUNT field */
    public static final String TOTAL_DISCOUNT;
      /** the column name for the TOTAL_TAX field */
    public static final String TOTAL_TAX;
      /** the column name for the COURIER_ID field */
    public static final String COURIER_ID;
      /** the column name for the SHIPPING_PRICE field */
    public static final String SHIPPING_PRICE;
      /** the column name for the TOTAL_EXPENSE field */
    public static final String TOTAL_EXPENSE;
      /** the column name for the TOTAL_COST field */
    public static final String TOTAL_COST;
      /** the column name for the SALES_ID field */
    public static final String SALES_ID;
      /** the column name for the CASHIER_NAME field */
    public static final String CASHIER_NAME;
      /** the column name for the REMARK field */
    public static final String REMARK;
      /** the column name for the PAYMENT_TYPE_ID field */
    public static final String PAYMENT_TYPE_ID;
      /** the column name for the PAYMENT_TERM_ID field */
    public static final String PAYMENT_TERM_ID;
      /** the column name for the PRINT_TIMES field */
    public static final String PRINT_TIMES;
      /** the column name for the PAID_AMOUNT field */
    public static final String PAID_AMOUNT;
      /** the column name for the PAYMENT_AMOUNT field */
    public static final String PAYMENT_AMOUNT;
      /** the column name for the CHANGE_AMOUNT field */
    public static final String CHANGE_AMOUNT;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the SHIP_TO field */
    public static final String SHIP_TO;
      /** the column name for the FOB_ID field */
    public static final String FOB_ID;
      /** the column name for the IS_TAXABLE field */
    public static final String IS_TAXABLE;
      /** the column name for the IS_INCLUSIVE_TAX field */
    public static final String IS_INCLUSIVE_TAX;
      /** the column name for the PAYMENT_DATE field */
    public static final String PAYMENT_DATE;
      /** the column name for the PAYMENT_STATUS field */
    public static final String PAYMENT_STATUS;
      /** the column name for the IS_INCLUSIVE_FREIGHT field */
    public static final String IS_INCLUSIVE_FREIGHT;
      /** the column name for the FREIGHT_ACCOUNT_ID field */
    public static final String FREIGHT_ACCOUNT_ID;
      /** the column name for the CANCEL_BY field */
    public static final String CANCEL_BY;
      /** the column name for the CANCEL_DATE field */
    public static final String CANCEL_DATE;
      /** the column name for the SALES_TRANSACTION_ID field */
    public static final String SALES_TRANSACTION_ID;
      /** the column name for the TOTAL_FEE field */
    public static final String TOTAL_FEE;
      /** the column name for the ROUNDING_AMOUNT field */
    public static final String ROUNDING_AMOUNT;
      /** the column name for the DOCTOR_NAME field */
    public static final String DOCTOR_NAME;
      /** the column name for the PATIENT_NAME field */
    public static final String PATIENT_NAME;
      /** the column name for the PATIENT_AGE field */
    public static final String PATIENT_AGE;
      /** the column name for the PATIENT_PHONE field */
    public static final String PATIENT_PHONE;
      /** the column name for the PATIENT_ADDRESS field */
    public static final String PATIENT_ADDRESS;
      /** the column name for the REGISTRATION_ID field */
    public static final String REGISTRATION_ID;
      /** the column name for the INSURANCE_ID field */
    public static final String INSURANCE_ID;
      /** the column name for the POLICY_NO field */
    public static final String POLICY_NO;
      /** the column name for the POLICY_NAME field */
    public static final String POLICY_NAME;
      /** the column name for the IS_INSURANCE field */
    public static final String IS_INSURANCE;
  
    static
    {
          MED_ORDER_ID = "med_order.MED_ORDER_ID";
          LOCATION_ID = "med_order.LOCATION_ID";
          STATUS = "med_order.STATUS";
          ORDER_NO = "med_order.ORDER_NO";
          INVOICE_NO = "med_order.INVOICE_NO";
          DELIVERY_NO = "med_order.DELIVERY_NO";
          TRANSACTION_DATE = "med_order.TRANSACTION_DATE";
          DUE_DATE = "med_order.DUE_DATE";
          CUSTOMER_ID = "med_order.CUSTOMER_ID";
          CUSTOMER_NAME = "med_order.CUSTOMER_NAME";
          TOTAL_QTY = "med_order.TOTAL_QTY";
          TOTAL_AMOUNT = "med_order.TOTAL_AMOUNT";
          TOTAL_DISCOUNT_PCT = "med_order.TOTAL_DISCOUNT_PCT";
          TOTAL_DISCOUNT = "med_order.TOTAL_DISCOUNT";
          TOTAL_TAX = "med_order.TOTAL_TAX";
          COURIER_ID = "med_order.COURIER_ID";
          SHIPPING_PRICE = "med_order.SHIPPING_PRICE";
          TOTAL_EXPENSE = "med_order.TOTAL_EXPENSE";
          TOTAL_COST = "med_order.TOTAL_COST";
          SALES_ID = "med_order.SALES_ID";
          CASHIER_NAME = "med_order.CASHIER_NAME";
          REMARK = "med_order.REMARK";
          PAYMENT_TYPE_ID = "med_order.PAYMENT_TYPE_ID";
          PAYMENT_TERM_ID = "med_order.PAYMENT_TERM_ID";
          PRINT_TIMES = "med_order.PRINT_TIMES";
          PAID_AMOUNT = "med_order.PAID_AMOUNT";
          PAYMENT_AMOUNT = "med_order.PAYMENT_AMOUNT";
          CHANGE_AMOUNT = "med_order.CHANGE_AMOUNT";
          CURRENCY_ID = "med_order.CURRENCY_ID";
          CURRENCY_RATE = "med_order.CURRENCY_RATE";
          SHIP_TO = "med_order.SHIP_TO";
          FOB_ID = "med_order.FOB_ID";
          IS_TAXABLE = "med_order.IS_TAXABLE";
          IS_INCLUSIVE_TAX = "med_order.IS_INCLUSIVE_TAX";
          PAYMENT_DATE = "med_order.PAYMENT_DATE";
          PAYMENT_STATUS = "med_order.PAYMENT_STATUS";
          IS_INCLUSIVE_FREIGHT = "med_order.IS_INCLUSIVE_FREIGHT";
          FREIGHT_ACCOUNT_ID = "med_order.FREIGHT_ACCOUNT_ID";
          CANCEL_BY = "med_order.CANCEL_BY";
          CANCEL_DATE = "med_order.CANCEL_DATE";
          SALES_TRANSACTION_ID = "med_order.SALES_TRANSACTION_ID";
          TOTAL_FEE = "med_order.TOTAL_FEE";
          ROUNDING_AMOUNT = "med_order.ROUNDING_AMOUNT";
          DOCTOR_NAME = "med_order.DOCTOR_NAME";
          PATIENT_NAME = "med_order.PATIENT_NAME";
          PATIENT_AGE = "med_order.PATIENT_AGE";
          PATIENT_PHONE = "med_order.PATIENT_PHONE";
          PATIENT_ADDRESS = "med_order.PATIENT_ADDRESS";
          REGISTRATION_ID = "med_order.REGISTRATION_ID";
          INSURANCE_ID = "med_order.INSURANCE_ID";
          POLICY_NO = "med_order.POLICY_NO";
          POLICY_NAME = "med_order.POLICY_NAME";
          IS_INSURANCE = "med_order.IS_INSURANCE";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(MedOrderMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(MedOrderMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  53;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.medical.om.MedOrder";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseMedOrderPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_FREIGHT))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_FREIGHT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_FREIGHT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INSURANCE))
        {
            Object possibleBoolean = criteria.get(IS_INSURANCE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INSURANCE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(MED_ORDER_ID);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(STATUS);
          criteria.addSelectColumn(ORDER_NO);
          criteria.addSelectColumn(INVOICE_NO);
          criteria.addSelectColumn(DELIVERY_NO);
          criteria.addSelectColumn(TRANSACTION_DATE);
          criteria.addSelectColumn(DUE_DATE);
          criteria.addSelectColumn(CUSTOMER_ID);
          criteria.addSelectColumn(CUSTOMER_NAME);
          criteria.addSelectColumn(TOTAL_QTY);
          criteria.addSelectColumn(TOTAL_AMOUNT);
          criteria.addSelectColumn(TOTAL_DISCOUNT_PCT);
          criteria.addSelectColumn(TOTAL_DISCOUNT);
          criteria.addSelectColumn(TOTAL_TAX);
          criteria.addSelectColumn(COURIER_ID);
          criteria.addSelectColumn(SHIPPING_PRICE);
          criteria.addSelectColumn(TOTAL_EXPENSE);
          criteria.addSelectColumn(TOTAL_COST);
          criteria.addSelectColumn(SALES_ID);
          criteria.addSelectColumn(CASHIER_NAME);
          criteria.addSelectColumn(REMARK);
          criteria.addSelectColumn(PAYMENT_TYPE_ID);
          criteria.addSelectColumn(PAYMENT_TERM_ID);
          criteria.addSelectColumn(PRINT_TIMES);
          criteria.addSelectColumn(PAID_AMOUNT);
          criteria.addSelectColumn(PAYMENT_AMOUNT);
          criteria.addSelectColumn(CHANGE_AMOUNT);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(SHIP_TO);
          criteria.addSelectColumn(FOB_ID);
          criteria.addSelectColumn(IS_TAXABLE);
          criteria.addSelectColumn(IS_INCLUSIVE_TAX);
          criteria.addSelectColumn(PAYMENT_DATE);
          criteria.addSelectColumn(PAYMENT_STATUS);
          criteria.addSelectColumn(IS_INCLUSIVE_FREIGHT);
          criteria.addSelectColumn(FREIGHT_ACCOUNT_ID);
          criteria.addSelectColumn(CANCEL_BY);
          criteria.addSelectColumn(CANCEL_DATE);
          criteria.addSelectColumn(SALES_TRANSACTION_ID);
          criteria.addSelectColumn(TOTAL_FEE);
          criteria.addSelectColumn(ROUNDING_AMOUNT);
          criteria.addSelectColumn(DOCTOR_NAME);
          criteria.addSelectColumn(PATIENT_NAME);
          criteria.addSelectColumn(PATIENT_AGE);
          criteria.addSelectColumn(PATIENT_PHONE);
          criteria.addSelectColumn(PATIENT_ADDRESS);
          criteria.addSelectColumn(REGISTRATION_ID);
          criteria.addSelectColumn(INSURANCE_ID);
          criteria.addSelectColumn(POLICY_NO);
          criteria.addSelectColumn(POLICY_NAME);
          criteria.addSelectColumn(IS_INSURANCE);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MedOrder row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            MedOrder obj = (MedOrder) cls.newInstance();
            MedOrderPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      MedOrder obj)
        throws TorqueException
    {
        try
        {
                obj.setMedOrderId(row.getValue(offset + 0).asString());
                  obj.setLocationId(row.getValue(offset + 1).asString());
                  obj.setStatus(row.getValue(offset + 2).asInt());
                  obj.setOrderNo(row.getValue(offset + 3).asString());
                  obj.setInvoiceNo(row.getValue(offset + 4).asString());
                  obj.setDeliveryNo(row.getValue(offset + 5).asString());
                  obj.setTransactionDate(row.getValue(offset + 6).asUtilDate());
                  obj.setDueDate(row.getValue(offset + 7).asUtilDate());
                  obj.setCustomerId(row.getValue(offset + 8).asString());
                  obj.setCustomerName(row.getValue(offset + 9).asString());
                  obj.setTotalQty(row.getValue(offset + 10).asBigDecimal());
                  obj.setTotalAmount(row.getValue(offset + 11).asBigDecimal());
                  obj.setTotalDiscountPct(row.getValue(offset + 12).asString());
                  obj.setTotalDiscount(row.getValue(offset + 13).asBigDecimal());
                  obj.setTotalTax(row.getValue(offset + 14).asBigDecimal());
                  obj.setCourierId(row.getValue(offset + 15).asString());
                  obj.setShippingPrice(row.getValue(offset + 16).asBigDecimal());
                  obj.setTotalExpense(row.getValue(offset + 17).asBigDecimal());
                  obj.setTotalCost(row.getValue(offset + 18).asBigDecimal());
                  obj.setSalesId(row.getValue(offset + 19).asString());
                  obj.setCashierName(row.getValue(offset + 20).asString());
                  obj.setRemark(row.getValue(offset + 21).asString());
                  obj.setPaymentTypeId(row.getValue(offset + 22).asString());
                  obj.setPaymentTermId(row.getValue(offset + 23).asString());
                  obj.setPrintTimes(row.getValue(offset + 24).asInt());
                  obj.setPaidAmount(row.getValue(offset + 25).asBigDecimal());
                  obj.setPaymentAmount(row.getValue(offset + 26).asBigDecimal());
                  obj.setChangeAmount(row.getValue(offset + 27).asBigDecimal());
                  obj.setCurrencyId(row.getValue(offset + 28).asString());
                  obj.setCurrencyRate(row.getValue(offset + 29).asBigDecimal());
                  obj.setShipTo(row.getValue(offset + 30).asString());
                  obj.setFobId(row.getValue(offset + 31).asString());
                  obj.setIsTaxable(row.getValue(offset + 32).asBoolean());
                  obj.setIsInclusiveTax(row.getValue(offset + 33).asBoolean());
                  obj.setPaymentDate(row.getValue(offset + 34).asUtilDate());
                  obj.setPaymentStatus(row.getValue(offset + 35).asInt());
                  obj.setIsInclusiveFreight(row.getValue(offset + 36).asBoolean());
                  obj.setFreightAccountId(row.getValue(offset + 37).asString());
                  obj.setCancelBy(row.getValue(offset + 38).asString());
                  obj.setCancelDate(row.getValue(offset + 39).asUtilDate());
                  obj.setSalesTransactionId(row.getValue(offset + 40).asString());
                  obj.setTotalFee(row.getValue(offset + 41).asBigDecimal());
                  obj.setRoundingAmount(row.getValue(offset + 42).asBigDecimal());
                  obj.setDoctorName(row.getValue(offset + 43).asString());
                  obj.setPatientName(row.getValue(offset + 44).asString());
                  obj.setPatientAge(row.getValue(offset + 45).asString());
                  obj.setPatientPhone(row.getValue(offset + 46).asString());
                  obj.setPatientAddress(row.getValue(offset + 47).asString());
                  obj.setRegistrationId(row.getValue(offset + 48).asString());
                  obj.setInsuranceId(row.getValue(offset + 49).asString());
                  obj.setPolicyNo(row.getValue(offset + 50).asString());
                  obj.setPolicyName(row.getValue(offset + 51).asString());
                  obj.setIsInsurance(row.getValue(offset + 52).asBoolean());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseMedOrderPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_FREIGHT))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_FREIGHT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_FREIGHT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INSURANCE))
        {
            Object possibleBoolean = criteria.get(IS_INSURANCE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INSURANCE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(MedOrderPeer.row2Object(row, 1,
                MedOrderPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseMedOrderPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(MED_ORDER_ID, criteria.remove(MED_ORDER_ID));
                                                                                                                                                                                                                                                                                                                                        // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                      // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                          // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_FREIGHT))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_FREIGHT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_FREIGHT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INSURANCE))
        {
            Object possibleBoolean = criteria.get(IS_INSURANCE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INSURANCE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         MedOrderPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_TAXABLE))
        {
            Object possibleBoolean = criteria.get(IS_TAXABLE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_TAXABLE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                  // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_TAX))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_TAX);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_TAX, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                              // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INCLUSIVE_FREIGHT))
        {
            Object possibleBoolean = criteria.get(IS_INCLUSIVE_FREIGHT);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INCLUSIVE_FREIGHT, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
                                                                                                            // check for conversion from boolean to bit (1/0)
        if (criteria.containsKey(IS_INSURANCE))
        {
            Object possibleBoolean = criteria.get(IS_INSURANCE);
            if (possibleBoolean instanceof Boolean)
            {
                criteria.add(IS_INSURANCE, ((Boolean) possibleBoolean).booleanValue() ? "1" : "0");
            }
         }
      
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(MedOrder obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(MedOrder obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(MedOrder obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(MedOrder obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(MedOrder) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(MedOrder obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(MedOrder) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(MedOrder obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(MedOrder) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(MedOrder obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseMedOrderPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(MED_ORDER_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( MedOrder obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(MED_ORDER_ID, obj.getMedOrderId());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(STATUS, obj.getStatus());
              criteria.add(ORDER_NO, obj.getOrderNo());
              criteria.add(INVOICE_NO, obj.getInvoiceNo());
              criteria.add(DELIVERY_NO, obj.getDeliveryNo());
              criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
              criteria.add(DUE_DATE, obj.getDueDate());
              criteria.add(CUSTOMER_ID, obj.getCustomerId());
              criteria.add(CUSTOMER_NAME, obj.getCustomerName());
              criteria.add(TOTAL_QTY, obj.getTotalQty());
              criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
              criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
              criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
              criteria.add(TOTAL_TAX, obj.getTotalTax());
              criteria.add(COURIER_ID, obj.getCourierId());
              criteria.add(SHIPPING_PRICE, obj.getShippingPrice());
              criteria.add(TOTAL_EXPENSE, obj.getTotalExpense());
              criteria.add(TOTAL_COST, obj.getTotalCost());
              criteria.add(SALES_ID, obj.getSalesId());
              criteria.add(CASHIER_NAME, obj.getCashierName());
              criteria.add(REMARK, obj.getRemark());
              criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
              criteria.add(PAYMENT_TERM_ID, obj.getPaymentTermId());
              criteria.add(PRINT_TIMES, obj.getPrintTimes());
              criteria.add(PAID_AMOUNT, obj.getPaidAmount());
              criteria.add(PAYMENT_AMOUNT, obj.getPaymentAmount());
              criteria.add(CHANGE_AMOUNT, obj.getChangeAmount());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(SHIP_TO, obj.getShipTo());
              criteria.add(FOB_ID, obj.getFobId());
              criteria.add(IS_TAXABLE, obj.getIsTaxable());
              criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
              criteria.add(PAYMENT_DATE, obj.getPaymentDate());
              criteria.add(PAYMENT_STATUS, obj.getPaymentStatus());
              criteria.add(IS_INCLUSIVE_FREIGHT, obj.getIsInclusiveFreight());
              criteria.add(FREIGHT_ACCOUNT_ID, obj.getFreightAccountId());
              criteria.add(CANCEL_BY, obj.getCancelBy());
              criteria.add(CANCEL_DATE, obj.getCancelDate());
              criteria.add(SALES_TRANSACTION_ID, obj.getSalesTransactionId());
              criteria.add(TOTAL_FEE, obj.getTotalFee());
              criteria.add(ROUNDING_AMOUNT, obj.getRoundingAmount());
              criteria.add(DOCTOR_NAME, obj.getDoctorName());
              criteria.add(PATIENT_NAME, obj.getPatientName());
              criteria.add(PATIENT_AGE, obj.getPatientAge());
              criteria.add(PATIENT_PHONE, obj.getPatientPhone());
              criteria.add(PATIENT_ADDRESS, obj.getPatientAddress());
              criteria.add(REGISTRATION_ID, obj.getRegistrationId());
              criteria.add(INSURANCE_ID, obj.getInsuranceId());
              criteria.add(POLICY_NO, obj.getPolicyNo());
              criteria.add(POLICY_NAME, obj.getPolicyName());
              criteria.add(IS_INSURANCE, obj.getIsInsurance());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( MedOrder obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(MED_ORDER_ID, obj.getMedOrderId());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(STATUS, obj.getStatus());
                          criteria.add(ORDER_NO, obj.getOrderNo());
                          criteria.add(INVOICE_NO, obj.getInvoiceNo());
                          criteria.add(DELIVERY_NO, obj.getDeliveryNo());
                          criteria.add(TRANSACTION_DATE, obj.getTransactionDate());
                          criteria.add(DUE_DATE, obj.getDueDate());
                          criteria.add(CUSTOMER_ID, obj.getCustomerId());
                          criteria.add(CUSTOMER_NAME, obj.getCustomerName());
                          criteria.add(TOTAL_QTY, obj.getTotalQty());
                          criteria.add(TOTAL_AMOUNT, obj.getTotalAmount());
                          criteria.add(TOTAL_DISCOUNT_PCT, obj.getTotalDiscountPct());
                          criteria.add(TOTAL_DISCOUNT, obj.getTotalDiscount());
                          criteria.add(TOTAL_TAX, obj.getTotalTax());
                          criteria.add(COURIER_ID, obj.getCourierId());
                          criteria.add(SHIPPING_PRICE, obj.getShippingPrice());
                          criteria.add(TOTAL_EXPENSE, obj.getTotalExpense());
                          criteria.add(TOTAL_COST, obj.getTotalCost());
                          criteria.add(SALES_ID, obj.getSalesId());
                          criteria.add(CASHIER_NAME, obj.getCashierName());
                          criteria.add(REMARK, obj.getRemark());
                          criteria.add(PAYMENT_TYPE_ID, obj.getPaymentTypeId());
                          criteria.add(PAYMENT_TERM_ID, obj.getPaymentTermId());
                          criteria.add(PRINT_TIMES, obj.getPrintTimes());
                          criteria.add(PAID_AMOUNT, obj.getPaidAmount());
                          criteria.add(PAYMENT_AMOUNT, obj.getPaymentAmount());
                          criteria.add(CHANGE_AMOUNT, obj.getChangeAmount());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(SHIP_TO, obj.getShipTo());
                          criteria.add(FOB_ID, obj.getFobId());
                          criteria.add(IS_TAXABLE, obj.getIsTaxable());
                          criteria.add(IS_INCLUSIVE_TAX, obj.getIsInclusiveTax());
                          criteria.add(PAYMENT_DATE, obj.getPaymentDate());
                          criteria.add(PAYMENT_STATUS, obj.getPaymentStatus());
                          criteria.add(IS_INCLUSIVE_FREIGHT, obj.getIsInclusiveFreight());
                          criteria.add(FREIGHT_ACCOUNT_ID, obj.getFreightAccountId());
                          criteria.add(CANCEL_BY, obj.getCancelBy());
                          criteria.add(CANCEL_DATE, obj.getCancelDate());
                          criteria.add(SALES_TRANSACTION_ID, obj.getSalesTransactionId());
                          criteria.add(TOTAL_FEE, obj.getTotalFee());
                          criteria.add(ROUNDING_AMOUNT, obj.getRoundingAmount());
                          criteria.add(DOCTOR_NAME, obj.getDoctorName());
                          criteria.add(PATIENT_NAME, obj.getPatientName());
                          criteria.add(PATIENT_AGE, obj.getPatientAge());
                          criteria.add(PATIENT_PHONE, obj.getPatientPhone());
                          criteria.add(PATIENT_ADDRESS, obj.getPatientAddress());
                          criteria.add(REGISTRATION_ID, obj.getRegistrationId());
                          criteria.add(INSURANCE_ID, obj.getInsuranceId());
                          criteria.add(POLICY_NO, obj.getPolicyNo());
                          criteria.add(POLICY_NAME, obj.getPolicyName());
                          criteria.add(IS_INSURANCE, obj.getIsInsurance());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedOrder retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedOrder retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedOrder retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        MedOrder retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static MedOrder retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (MedOrder)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( MED_ORDER_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

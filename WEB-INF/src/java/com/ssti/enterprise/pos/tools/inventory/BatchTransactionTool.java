package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.om.BatchLocation;
import com.ssti.enterprise.pos.om.BatchLocationPeer;
import com.ssti.enterprise.pos.om.BatchTransaction;
import com.ssti.enterprise.pos.om.BatchTransactionPeer;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.TmpBatchTrans;
import com.ssti.enterprise.pos.om.TmpBatchTransPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2016-12-27
 * - change method getByTransferIN, set Qty to positive
 * 
 * 2016-12-08
 * - change method saveBatch, add code to create BatchTransaction for TransferIn InvTrans 
 * - add method getByTransferIN to query out batch transaction by TransferIn
 * </pre><br>
 */
public class BatchTransactionTool extends BaseTool 
{
	static BatchTransactionTool instance = null;
	
	public static synchronized BatchTransactionTool getInstance() 
	{
		if (instance == null) instance = new BatchTransactionTool();
		return instance;
	}		
	
	public static BatchTransaction newBatchTrans()
	{
		return new BatchTransaction();
	}

	private static final boolean isIn (InventoryTransaction _oInv) 
	{
		if (_oInv != null)
		{
			int iType = _oInv.getTransactionType();
			if (iType == i_INV_TRANS_RECEIPT_UNPLANNED) return true;
			if (iType == i_INV_TRANS_ISSUE_UNPLANNED  ) return false;
			if (iType == i_INV_TRANS_PURCHASE_RECEIPT ) return true;
			if (iType == i_INV_TRANS_PURCHASE_INVOICE ) return true;
			if (iType == i_INV_TRANS_PURCHASE_RETURN  ) return false;
			if (iType == i_INV_TRANS_DELIVERY_ORDER   ) return false;
			if (iType == i_INV_TRANS_SALES_INVOICE    ) return false;
			if (iType == i_INV_TRANS_SALES_RETURN 	  ) return true;
			if (iType == i_INV_TRANS_JOB_COSTING      ) return false;
			if (iType == i_INV_TRANS_JC_ROLLOVER      ) return true;
			if (iType == i_INV_TRANS_TRANSFER_OUT	  ) return false;
			if (iType == i_INV_TRANS_TRANSFER_IN	  ) return true;
		}
		return false;
	}
	
	public static List getByClosestED(String _sItemID, 
								      String _sLocationID, 
									  Connection _oConn)	
		throws Exception
	{
		return getByClosestED(_sItemID, _sLocationID, true, _oConn);
	}
	
	public static List getByClosestED(String _sItemID, 
								      String _sLocationID, 
								      boolean _bExistOnly,
									  Connection _oConn)	
		throws Exception
	{
		
		Criteria oCrit = new Criteria();
		oCrit.add(BatchLocationPeer.ITEM_ID, _sItemID);
		oCrit.add(BatchLocationPeer.LOCATION_ID, _sLocationID);
		if (_bExistOnly)
		{
			oCrit.add(BatchLocationPeer.QTY, 0, Criteria.GREATER_EQUAL);
		}
		oCrit.addAscendingOrderByColumn(BatchLocationPeer.EXPIRED_DATE);
		
		return BatchLocationPeer.doSelect(oCrit, _oConn);
	}
	
	public static BatchLocation getBatchLocation(String _sItemID, 
										 		 String _sLocationID, 
										 		 String _sBatchNo,
										 		 boolean _bForUpdate,
										 		 Connection _oConn)	
		throws Exception
	{
		
		Criteria oCrit = new Criteria();
		oCrit.add(BatchLocationPeer.ITEM_ID, _sItemID);
		oCrit.add(BatchLocationPeer.LOCATION_ID, _sLocationID);
		oCrit.add(BatchLocationPeer.BATCH_NO, _sBatchNo);
		if (_bForUpdate)
		{
			oCrit.setForUpdate(_bForUpdate);
		}
		List vData = BatchLocationPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (BatchLocation) vData.get(0);
		}
		return null;
	}
	
	private static void updateBatchLocation(BatchTransaction _oBT, Connection _oConn) 
		throws Exception
	{
		log.debug("Update Batch Location BT: " + _oBT);
		BatchLocation oBL = getBatchLocation(_oBT.getItemId(), 
									 		 _oBT.getLocationId(), 
									 		 _oBT.getBatchNo(), true, _oConn);
		
		if (oBL == null)
		{
			oBL = new BatchLocation();
			oBL.setBatchLocationId(IDGenerator.generateSysID());
			oBL.setBatchNo(_oBT.getBatchNo());
			oBL.setDescription(_oBT.getDescription());
			oBL.setExpiredDate(_oBT.getExpiredDate());
			oBL.setItemId(_oBT.getItemId());
			oBL.setItemCode(_oBT.getItemCode());
			oBL.setLocationId(_oBT.getLocationId());
			oBL.setQty(_oBT.getQty());
		}
		else
		{
			double dOldQty = oBL.getQty().doubleValue();
			double dNewQty = dOldQty + _oBT.getQty().doubleValue();
			oBL.setQty(new BigDecimal(dNewQty));
		}
		log.debug("Updated Batch Location BL: " + _oBT);
		oBL.setLastUpdate(new Date());
		oBL.save(_oConn);
	}
	
	private static void mapInvToBatch (BatchTransaction _oBT, InventoryTransaction _oInv) 
		throws Exception
	{
		if (_oBT != null && _oInv != null)
		{
			_oBT.setItemId(_oInv.getItemId());
			_oBT.setItemCode(_oInv.getItemCode());
			_oBT.setLocationId(_oInv.getLocationId());
			_oBT.setQty(_oInv.getQtyChanges());
			_oBT.setTransactionDate(_oInv.getTransactionDate());
			_oBT.setTransactionDetailId(_oInv.getTransactionDetailId());
			_oBT.setTransactionId(_oInv.getTransactionId());
			_oBT.setTransactionNo(_oInv.getTransactionNo());
			_oBT.setTransactionType(_oInv.getTransactionType());
			_oBT.setInventoryTransactionId(_oInv.getInventoryTransactionId());
			_oBT.setBatchTransactionId(IDGenerator.generateSysID(true));
		}
	}
	
	public static void saveBatch(BatchTransaction _oBT, 
								 InventoryTransaction _oInv, 
								 Connection _oConn)
		throws Exception
	{
		if (_oInv != null)
		{
			//if Batch Number specified in Trans
			if (_oBT != null && StringUtil.isNotEmpty(_oBT.getBatchNo()))
			{
				mapInvToBatch(_oBT, _oInv);
				_oBT.save(_oConn);
				updateBatchLocation(_oBT, _oConn);
			}
			//if Batch Number not specified 
			else if (_oBT == null)
			{
				//is inventory out transaction
				if (!isIn(_oInv))
				{
					//create BT automatically by closest ED
					List vBT = createAutoBT(_oInv, _oConn);
					for (int i = 0; i < vBT.size(); i++)
					{
						BatchTransaction oBT = (BatchTransaction) vBT.get(i);
						oBT.save(_oConn);
						updateBatchLocation(oBT, _oConn);
					}
				}
				else 
				{
					if(_oInv.getTransactionType() == i_INV_TRANS_TRANSFER_IN) //if item tranfer in
					{
						//get batch number from out 
						List vBT = getByTransferIn(_oInv, _oConn);
						for (int i = 0; i < vBT.size(); i++)
						{
							BatchTransaction oBTOut = (BatchTransaction) vBT.get(i);
							BatchTransaction oBTIn = oBTOut.copy();
							oBTIn.setNew(true);
							
							BigDecimal dQty = new BigDecimal(oBTIn.getQty().doubleValue() * -1);
							mapInvToBatch(oBTIn, _oInv);
							oBTIn.setQty(dQty); //revert back to copied qty
							oBTIn.save(_oConn);
							updateBatchLocation(oBTIn, _oConn);
						}						
					}
					else
					{
						log.warn("No Batch Transaction for Inventory In in Trans " + _oInv.getTransactionNo());
					}
				}
			}
		}
	}

	private static List createAutoBT(InventoryTransaction _oInv, Connection _oConn) 
		throws Exception
	{
		List vBT = new ArrayList();
		
		double dInvQty = _oInv.getQtyChanges().doubleValue() * -1;
		List vBL = getByClosestED(_oInv.getItemId(), _oInv.getLocationId(), _oConn);
		log.debug("vBL " + vBL); 
		
		for (int i = 0; i < vBL.size(); i++)
		{
			BatchLocation oBL = (BatchLocation) vBL.get(i);
			BatchTransaction oBT = new BatchTransaction();
			mapInvToBatch(oBT,_oInv);
			oBT.setDescription(_oInv.getDescription());
			oBT.setBatchNo(oBL.getBatchNo());
			oBT.setExpiredDate(oBL.getExpiredDate());
			
			log.debug("dInvQty = " + dInvQty);
			if (oBL.getQty().doubleValue() >= dInvQty)
			{
				log.debug("BL QTY " + oBL.getQty() + ">= INV QTY " + dInvQty); 
				double dQty = dInvQty * -1;
				oBT.setQty(new BigDecimal(dQty));
				vBT.add(oBT);
				break;
			}
			else if (oBL.getQty().doubleValue() < dInvQty)
			{
				log.debug("BL QTY " + oBL.getQty() + "< INV QTY " + dInvQty); 
				dInvQty = dInvQty - oBL.getQty().doubleValue();
				double dQty = oBL.getQty().doubleValue() * -1;
				oBT.setQty(new BigDecimal(dQty));
				vBT.add(oBT);
			}
		}
		//log.debug("vBT " + vBT); 
		return vBT;
	}

	public static void deleteBatch(InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(BatchTransactionPeer.INVENTORY_TRANSACTION_ID, _oInv.getInventoryTransactionId());		
		List vBT = BatchTransactionPeer.doSelect(oCrit, _oConn);
		for (int i = 0; i < vBT.size(); i++)
		{
			BatchTransaction oBT = (BatchTransaction)vBT.get(i);
			oBT.setQty(new BigDecimal(oBT.getQty().doubleValue() * -1));
			updateBatchLocation(oBT,_oConn);
		}
		BatchTransactionPeer.doDelete(oCrit,_oConn);		
	}
	
	//
	public static List findBL(String _sItemID, 
							  String _sItemCode,
							  String _sItemName,
							  String _sKategoriID,
							  String _sLocationID, 
							  String _sBatchNo,
							  Date _dExpFrom,
							  Date _dExpTo,
							  Connection _oConn)	
		throws Exception
	{	
		Criteria oCrit = new Criteria();
		
		if (StringUtil.isNotEmpty(_sItemID))
		{
			oCrit.add(BatchLocationPeer.ITEM_ID, _sItemID);
		}
		if (StringUtil.isNotEmpty(_sItemCode))
		{
			oCrit.addJoin(BatchLocationPeer.ITEM_ID, ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.ITEM_CODE, _sItemCode);			
		}
		if (StringUtil.isNotEmpty(_sItemName))
		{
			oCrit.addJoin(BatchLocationPeer.ITEM_ID, ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.ITEM_NAME, (Object)_sItemName, Criteria.ILIKE);			
		}
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			oCrit.addJoin(BatchLocationPeer.ITEM_ID,ItemPeer.ITEM_ID);
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			if(vKategori.size() > 0)
			{
				vKategori.add(_sKategoriID);
				oCrit.addIn(ItemPeer.KATEGORI_ID,vKategori);
			}
			else{
				oCrit.add(ItemPeer.KATEGORI_ID,_sKategoriID);
			}
		}		
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(BatchLocationPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sBatchNo))
		{
			oCrit.add(BatchLocationPeer.BATCH_NO, _sBatchNo);
		}
		if (_dExpFrom != null)
		{
			oCrit.add(BatchLocationPeer.EXPIRED_DATE, _dExpFrom, Criteria.GREATER_EQUAL);			
		}
		if (_dExpTo != null)
		{
			oCrit.and(BatchLocationPeer.EXPIRED_DATE, _dExpTo, Criteria.LESS_EQUAL);			
		}
		return BatchLocationPeer.doSelect(oCrit, _oConn);
	}
	
	public static List getByItemAndDetID (String _sItemID, String _sDetID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(BatchTransactionPeer.ITEM_ID, _sItemID);
		oCrit.add(BatchTransactionPeer.TRANSACTION_DETAIL_ID, _sDetID);
		return BatchTransactionPeer.doSelect(oCrit);
	}

	/**
	 * get OUT Batch Transaction based on TransferIn InvTrans
	 * 
	 * @param _oInvTI
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static List getByTransferIn (InventoryTransaction _oInvTI, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(BatchTransactionPeer.ITEM_ID, _oInvTI.getItemId());
		oCrit.add(BatchTransactionPeer.TRANSACTION_ID, _oInvTI.getTransactionId());
		oCrit.add(BatchTransactionPeer.TRANSACTION_DETAIL_ID, _oInvTI.getTransactionDetailId());
		oCrit.add(BatchTransactionPeer.TRANSACTION_TYPE, i_INV_TRANS_TRANSFER_OUT);
		return BatchTransactionPeer.doSelect(oCrit, _oConn);
	}
	
	public static List getBT(String _sItemID, 
							 String _sLocationID, 
							 String _sBatchNo)	
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(BatchTransactionPeer.ITEM_ID, _sItemID);
		oCrit.add(BatchTransactionPeer.LOCATION_ID, _sLocationID);
		oCrit.add(BatchTransactionPeer.BATCH_NO, _sBatchNo);
		return BatchTransactionPeer.doSelect(oCrit);
	}
	
	//-------------------------------------------------------------------------
	// Validate Batch
	//-------------------------------------------------------------------------
	private static String getEmptyMsg(Item _oItem)
	{
		StringBuilder oSB = new StringBuilder();
		if (_oItem != null)
		{
			oSB.append(LocaleTool.getString("batch_no")).append(" ")
			   .append(_oItem.getItemCode()).append(" ")
			   .append(_oItem.getItemName()).append(" ")
			   .append(LocaleTool.getString("may_not_empty"));
		}
		return oSB.toString();	
	}
	public static void validateBatch(String _sItemID, BatchTransaction oBT, Connection _oConn)	
		throws Exception
	{
		if (PreferenceTool.useBatchNo())
		{				
			Item oItem = ItemTool.getItemByID(_sItemID, _oConn);
			validateBatch(oItem, oBT);
		}
	}	
	public static void validateBatch(Item _oItem, BatchTransaction oBT)	
		throws Exception
	{
		if (PreferenceTool.useBatchNo() && _oItem != null && _oItem.getTrackBatchNo())
		{
			String sBN = "";
			if (oBT != null)
			{
				sBN = oBT.getBatchNo();
			}
			if (StringUtil.isEmpty(sBN))
			{
				throw new Exception (getEmptyMsg(_oItem));
			}
		}
	}	
	public static String validateBatchJS(List _vDet)	
		throws Exception
	{
		StringBuilder oResult = new StringBuilder("");
		if (PreferenceTool.useBatchNo())
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				InventoryDetailOM oTD = (InventoryDetailOM)_vDet.get(i);
				String sItemID = oTD.getItemId();
				Item oItem = ItemTool.getItemByID(sItemID);
				double dQty = 0;
				if (oTD.getQty() != null) dQty = oTD.getQty().doubleValue();

				if (oItem != null && oItem.getTrackBatchNo())
				{
					String sBN = "";
					BatchTransaction oBT = oTD.getBatchTransaction();
					if (oBT != null)
					{
						sBN = oBT.getBatchNo();
					}
					if (dQty != 0 && StringUtil.isEmpty(sBN))
					{
						oResult.append(getEmptyMsg(oItem)).append("\n");
					}
				}
			}
		}
		return oResult.toString();
	}
	
	public static void validateBatch(List _vDet, Connection _oConn)	
		throws Exception
	{
		if (PreferenceTool.useBatchNo())
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				InventoryDetailOM oTD = (InventoryDetailOM)_vDet.get(i);
				String sItemID = oTD.getItemId();
				Item oItem = ItemTool.getItemByID(sItemID, _oConn);
				if (oItem != null && oItem.getTrackBatchNo())
				{
					String sBN = "";
					BatchTransaction oBT = oTD.getBatchTransaction();
					if (oBT != null)
					{
						sBN = oBT.getBatchNo();
					}
					if (StringUtil.isEmpty(sBN))
					{
						throw new Exception(getEmptyMsg(oItem));				
					}
				}
			}
		}
	}
		
	//-------------------------------------------------------------------------
	// Temp Batch
	//-------------------------------------------------------------------------
	
	public static List getTmpBatch(String _sTransID, 
								   int _iTransType, 
								   Connection _oConn)	
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(TmpBatchTransPeer.TRANSACTION_ID, _sTransID);
		oCrit.add(TmpBatchTransPeer.TRANSACTION_TYPE, _iTransType);
		return TmpBatchTransPeer.doSelect(oCrit, _oConn);
	}
	
	public static void clearTmpBatch(String _sTransID, 
									 int _iTransType, 
									 Connection _oConn)	
		throws Exception
	{
		if (PreferenceTool.useBatchNo())
		{
			Criteria oCrit = new Criteria();
			oCrit.add(TmpBatchTransPeer.TRANSACTION_ID, _sTransID);
			oCrit.add(TmpBatchTransPeer.TRANSACTION_TYPE, _iTransType);
			TmpBatchTransPeer.doDelete(oCrit, _oConn);
		}
	}
	
	public static void saveTmpBatch(String _sItemID,
									String _sTransID, 
									String _sTransDetID, 
									int _iTransType, 
									BatchTransaction _oBT,
									Connection _oConn)	
		throws Exception
	{
		if (PreferenceTool.useBatchNo() && _oBT != null)
		{
			TmpBatchTrans oTMP = new TmpBatchTrans();
			oTMP.setTmpBatchTransId(IDGenerator.generateSysID());
			oTMP.setItemId(_sItemID);
			oTMP.setBatchNo(_oBT.getBatchNo());
			oTMP.setDescription(_oBT.getDescription());
			oTMP.setExpiredDate(_oBT.getExpiredDate());
			oTMP.setTransactionId(_sTransID);
			oTMP.setTransactionDetailId(_sTransDetID);
			oTMP.setTransactionType(_iTransType);
			oTMP.save(_oConn);
		}
	}
	
	public static void setTmpBatch(String _sTransID, 
			   					   int _iTransType, 
			   					   List _vDet,
			   					   Connection _oConn)	
		throws Exception
	{
		if (PreferenceTool.useBatchNo())
		{
			List vTMP = getTmpBatch(_sTransID, _iTransType, _oConn);
			for (int i = 0; i < _vDet.size(); i++)
			{
				InventoryDetailOM oTD = (InventoryDetailOM)_vDet.get(i);
				for (int j = 0; j < vTMP.size(); j++)
				{
					TmpBatchTrans oTMP = (TmpBatchTrans) vTMP.get(j);
					if (oTD.getPrimaryKey() != null && 
							oTD.getPrimaryKey().toString().equals(oTMP.getTransactionDetailId()) && 
							oTD.getItemId().equals(oTMP.getItemId()))
					{
						BatchTransaction oBT = new BatchTransaction();
						oBT.setBatchNo(oTMP.getBatchNo());
						oBT.setExpiredDate(oTMP.getExpiredDate());
						oBT.setDescription(oTMP.getDescription());
						oBT.setItemId(oTD.getItemId());
						oTD.setBatchTransaction(oBT);
					}
				}
			}
		}
	}
}

package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to UserPref
 */
public abstract class BaseUserPref extends BaseObject
{
    /** The Peer class */
    private static final UserPrefPeer peer =
        new UserPrefPeer();

        
    /** The value for the userPrefId field */
    private String userPrefId;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the prefObj field */
    private byte[] prefObj;
  
    
    /**
     * Get the UserPrefId
     *
     * @return String
     */
    public String getUserPrefId()
    {
        return userPrefId;
    }

                        
    /**
     * Set the value of UserPrefId
     *
     * @param v new value
     */
    public void setUserPrefId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userPrefId, v))
              {
            this.userPrefId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrefObj
     *
     * @return byte[]
     */
    public byte[] getPrefObj()
    {
        return prefObj;
    }

                        
    /**
     * Set the value of PrefObj
     *
     * @param v new value
     */
    public void setPrefObj(byte[] v) 
    {
    
                  if (!ObjectUtils.equals(this.prefObj, v))
              {
            this.prefObj = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("UserPrefId");
              fieldNames.add("UserName");
              fieldNames.add("PrefObj");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("UserPrefId"))
        {
                return getUserPrefId();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("PrefObj"))
        {
                return getPrefObj();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(UserPrefPeer.USER_PREF_ID))
        {
                return getUserPrefId();
            }
          if (name.equals(UserPrefPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(UserPrefPeer.PREF_OBJ))
        {
                return getPrefObj();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getUserPrefId();
            }
              if (pos == 1)
        {
                return getUserName();
            }
              if (pos == 2)
        {
                return getPrefObj();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(UserPrefPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        UserPrefPeer.doInsert((UserPref) this, con);
                        setNew(false);
                    }
                    else
                    {
                        UserPrefPeer.doUpdate((UserPref) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key userPrefId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setUserPrefId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setUserPrefId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getUserPrefId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public UserPref copy() throws TorqueException
    {
        return copyInto(new UserPref());
    }
  
    protected UserPref copyInto(UserPref copyObj) throws TorqueException
    {
          copyObj.setUserPrefId(userPrefId);
          copyObj.setUserName(userName);
          copyObj.setPrefObj(prefObj);
  
                    copyObj.setUserPrefId((String)null);
                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public UserPrefPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("UserPref\n");
        str.append("--------\n")
           .append("UserPrefId           : ")
           .append(getUserPrefId())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("PrefObj              : ")
           .append("<binary>")
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PwpGetMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PwpGetMapBuilder";

    /**
     * Item
     * @deprecated use PwpGetPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "pwp_get";
    }

  
    /**
     * pwp_get.PWP_GET_ID
     * @return the column name for the PWP_GET_ID field
     * @deprecated use PwpGetPeer.pwp_get.PWP_GET_ID constant
     */
    public static String getPwpGet_PwpGetId()
    {
        return "pwp_get.PWP_GET_ID";
    }
  
    /**
     * pwp_get.PWP_ID
     * @return the column name for the PWP_ID field
     * @deprecated use PwpGetPeer.pwp_get.PWP_ID constant
     */
    public static String getPwpGet_PwpId()
    {
        return "pwp_get.PWP_ID";
    }
  
    /**
     * pwp_get.GET_ITEM_ID
     * @return the column name for the GET_ITEM_ID field
     * @deprecated use PwpGetPeer.pwp_get.GET_ITEM_ID constant
     */
    public static String getPwpGet_GetItemId()
    {
        return "pwp_get.GET_ITEM_ID";
    }
  
    /**
     * pwp_get.GET_QTY
     * @return the column name for the GET_QTY field
     * @deprecated use PwpGetPeer.pwp_get.GET_QTY constant
     */
    public static String getPwpGet_GetQty()
    {
        return "pwp_get.GET_QTY";
    }
  
    /**
     * pwp_get.GET_PRICE
     * @return the column name for the GET_PRICE field
     * @deprecated use PwpGetPeer.pwp_get.GET_PRICE constant
     */
    public static String getPwpGet_GetPrice()
    {
        return "pwp_get.GET_PRICE";
    }
  
    /**
     * pwp_get.GET_DISC
     * @return the column name for the GET_DISC field
     * @deprecated use PwpGetPeer.pwp_get.GET_DISC constant
     */
    public static String getPwpGet_GetDisc()
    {
        return "pwp_get.GET_DISC";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("pwp_get");
        TableMap tMap = dbMap.getTable("pwp_get");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("pwp_get.PWP_GET_ID", "");
                          tMap.addForeignKey(
                "pwp_get.PWP_ID", "" , "pwp" ,
                "pwp_id");
                          tMap.addColumn("pwp_get.GET_ITEM_ID", "");
                            tMap.addColumn("pwp_get.GET_QTY", bd_ZERO);
                            tMap.addColumn("pwp_get.GET_PRICE", bd_ZERO);
                          tMap.addColumn("pwp_get.GET_DISC", "");
          }
}

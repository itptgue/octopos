package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ValueParser;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.BarcodePaper;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.tools.BarcodePaperTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/setup/ItemBarcodePrint.java,v $
 * Purpose: 
 *
 * @author  $Author: albert $
 * @version $Id: ItemBarcodePrint.java,v 1.13 2007/12/20 13:49:05 albert Exp $
 *
 * $Log: ItemBarcodePrint.java,v $
 * Revision 1.13  2007/12/20 13:49:05  albert
 * *** empty log message ***
 *
 * Revision 1.12  2007/03/05 16:04:54  albert
 * *** empty log message ***
 *
 * Revision 1.11  2005/10/10 03:52:26  albert
 * *** empty log message ***
 *
 */

public class ItemBarcodePrint extends SetupSecureScreen
{
	private final Log log = LogFactory.getLog(getClass());
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	ValueParser formData = data.getParameters();
    	int iType = formData.getInt("type",1);
    	boolean bRcp = formData.getBoolean("IsFromReceipt",false);
    	boolean bInv = formData.getBoolean("IsFromInvoice",false);
    	boolean bLst = formData.getBoolean("IsFromItemList",false);
    	boolean bAdj = formData.getBoolean("IsFromIssueReceipt",false);
    	boolean bTrf = formData.getBoolean("IsFromTransfer",false);
    	
    	//if from item setup or in paper setup mode
        if(!bRcp  && !bInv && !bLst && !bAdj && !bTrf)
        {
        	try 
			{
	            boolean bView = formData.getBoolean("ViewBarcode", false);
	    	    if(bView)
	    	    {
		            String sCode  = formData.getString("ItemCode");
		            String sSKU   = formData.getString("ItemSku");
		            String sKatID = formData.getString("KategoriId");

	    	    	Criteria oCrit = new Criteria();
	    	    	if (StringUtil.isNotEmpty(sCode)) 
	    	    	{
	    	    		oCrit.add(ItemPeer.ITEM_CODE, sCode);
	    	    	}
	    	    	if (StringUtil.isNotEmpty(sSKU))   
	    	    	{
	    	    		oCrit.add(ItemPeer.ITEM_SKU, sSKU);
	    	    	}
	    	    	if (StringUtil.isNotEmpty(sKatID)) 
	    	    	{
	    	    		oCrit.add(ItemPeer.KATEGORI_ID, sKatID);
	    	    	}
	    	    	List vItem = ItemPeer.doSelect(oCrit);
			    	log.debug(oCrit);

		    	    BarcodePaper oPaper = new BarcodePaper();
		    	    formData.setProperties(oPaper);
		    	    log.debug(oPaper);
			    	
			    	context.put ( "vItem",  vItem  );
                    context.put ( "oPaper", oPaper );
                }
            }
            catch (Exception _oEx)
            {
            	log.error(_oEx);
            	_oEx.printStackTrace();
            }	
        }
        else
        {
            try 
            {
            	BarcodePaper oPaper = BarcodePaperTool.getDefaultPaper(iType);
            	if (oPaper != null)
            	{                
                    List vDet = null;
                    HttpSession oSes;
                    oSes = data.getSession();
                    
                    if(bRcp) vDet = (List) oSes.getAttribute (s_PRD);
                    if(bInv) vDet = (List) oSes.getAttribute (s_PID);
                    if(bLst) vDet = (List) oSes.getAttribute (s_ITMD);
                    if(bAdj) vDet = (List) oSes.getAttribute (s_IRD);
                    if(bTrf) vDet = (List) oSes.getAttribute (s_ITD);
                    
                    context.put ("vItem", vDet);
                    context.put ("oPaper", oPaper);
            	}  
            }
            catch (Exception _oEx)
            {
            	log.error(_oEx);            	
            	_oEx.printStackTrace();
            }	
        }
    }
}

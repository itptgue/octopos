
package com.ssti.enterprise.pos.om;


import java.util.List;

import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.framework.tools.StringUtil;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class CustomerCfield
    extends com.ssti.enterprise.pos.om.BaseCustomerCfield
    implements Persistent, MasterOM
{
	public String getId()
	{
		return getCfieldId();
	}

	public String getCode()
	{
		return getFieldName();
	}
	
	public String getFieldTypeStr()
	{
		if(getFieldType() == 1) return "TextBox";
		if(getFieldType() == 2) return "CheckBox";
		if(getFieldType() == 3) return "SelectBox";
		if(getFieldType() == 4) return "LongText";
		
		return "";
	}	

	public String createField(String _sValue)
	{
		StringBuilder s = new StringBuilder();
		if (getFieldType() == 1)
		{
			if(StringUtil.isEmpty(_sValue) && StringUtil.isNotEmpty(getFieldValue()))
			{
				_sValue = getFieldValue();
			}
			s.append("<input type='text' style='width:96%' name='")
			 .append(getId())
			 .append("' value='").append(_sValue).append("'>");
		}
		if (getFieldType() == 2)
		{
			List v = null;	
			if (StringUtil.isNotEmpty(getFieldValue()))
			{
				v = StringUtil.toList(getFieldValue(),",");
				if (v != null && v.size() > 1)
				{			
					s.append("<input type='radio' class='rb' name='")
					 .append(getId())
					 .append("' value='").append(v.get(0)).append("' ");
					if (StringUtil.isEqual(v.get(0), _sValue)) 
					s.append(" CHECKED ");					
					
					s.append(" >").append(v.get(0));
					
					s.append("<input type='radio' class='rb' name='")
					 .append(getId())
					 .append("' value='").append(v.get(1)).append("' ");
					if (StringUtil.isEqual(v.get(1), _sValue)) 
					s.append(" CHECKED ");					
					
					s.append(" >").append(v.get(1));					
				}	
			}
			else
			{
				s.append("<input type='text' style='width:96%' name='")
				 .append(getId())
				 .append("' value = '").append(_sValue).append("'>");
			}
		}
		if (getFieldType() == 3)
		{
			List v = null;
			if (StringUtil.isNotEmpty(getFieldValue()))
			{
				v = StringUtil.toList(getFieldValue(),",");
				if (v != null && v.size() > 0)
				{			
					s.append("<select style='width:96%' name='")
					 .append(getId()).append("'>")
					 .append("<option value=''>-- ").append(getFieldName()).append(" --</option>");					
					for (int i = 0; i < v.size(); i ++)
					{
						String sVal =(String) v.get(i);
						String sSel = "";
						if (StringUtil.isEqual(sVal, _sValue)) sSel = "SELECTED"; 
						s.append("<option value='").append(sVal).append("' ")
						 .append(sSel).append(" >").append(sVal).append("</option>");
					}					 
					s.append("</select>");
				}
			}
			else
			{
				s.append("<input type='text' style='width:96%' name='")
				 .append(getId())
				 .append("' value = '").append(_sValue).append("'>");
			}
		}
		if (getFieldType() == 4)
		{
			if(StringUtil.isEmpty(_sValue) && StringUtil.isNotEmpty(getFieldValue()))
			{
				_sValue = getFieldValue();
			}
			s.append("<textarea style='width:96%' name='")
			 .append(getId())
			 .append("'>").append(_sValue).append("</textarea>");
		}
		return s.toString();
	}
	
	public CustomerType getCustomerType()
	{
		if (StringUtil.isNotEmpty(getCustomerTypeId()))
		{
			try
			{
				return CustomerTypeTool.getCustomerTypeByID(getCustomerTypeId());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return null;		
	}

}

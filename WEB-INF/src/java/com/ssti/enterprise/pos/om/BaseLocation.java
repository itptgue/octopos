package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Location
 */
public abstract class BaseLocation extends BaseObject
{
    /** The Peer class */
    private static final LocationPeer peer =
        new LocationPeer();

        
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationCode field */
    private String locationCode;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the locationAddress field */
    private String locationAddress;
      
    /** The value for the siteId field */
    private String siteId;
      
    /** The value for the inventoryType field */
    private int inventoryType;
      
    /** The value for the hoStoreLeadtime field */
    private int hoStoreLeadtime;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the locationType field */
    private int locationType;
                                                
    /** The value for the obTransId field */
    private String obTransId = "";
      
    /** The value for the updateDate field */
    private Date updateDate;
                                                
    /** The value for the locationPhone field */
    private String locationPhone = "";
                                                
    /** The value for the locationEmail field */
    private String locationEmail = "";
                                                
    /** The value for the locationUrl field */
    private String locationUrl = "";
                                                
    /** The value for the parentLocationId field */
    private String parentLocationId = "";
                                                
    /** The value for the transferAccount field */
    private String transferAccount = "";
                                                
    /** The value for the salesAreaId field */
    private String salesAreaId = "";
                                                
    /** The value for the territoryId field */
    private String territoryId = "";
                                                
          
    /** The value for the longitudes field */
    private BigDecimal longitudes= bd_ZERO;
                                                
          
    /** The value for the latitudes field */
    private BigDecimal latitudes= bd_ZERO;
  
    
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationCode
     *
     * @return String
     */
    public String getLocationCode()
    {
        return locationCode;
    }

                        
    /**
     * Set the value of LocationCode
     *
     * @param v new value
     */
    public void setLocationCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationCode, v))
              {
            this.locationCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationAddress
     *
     * @return String
     */
    public String getLocationAddress()
    {
        return locationAddress;
    }

                        
    /**
     * Set the value of LocationAddress
     *
     * @param v new value
     */
    public void setLocationAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationAddress, v))
              {
            this.locationAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SiteId
     *
     * @return String
     */
    public String getSiteId()
    {
        return siteId;
    }

                        
    /**
     * Set the value of SiteId
     *
     * @param v new value
     */
    public void setSiteId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.siteId, v))
              {
            this.siteId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InventoryType
     *
     * @return int
     */
    public int getInventoryType()
    {
        return inventoryType;
    }

                        
    /**
     * Set the value of InventoryType
     *
     * @param v new value
     */
    public void setInventoryType(int v) 
    {
    
                  if (this.inventoryType != v)
              {
            this.inventoryType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HoStoreLeadtime
     *
     * @return int
     */
    public int getHoStoreLeadtime()
    {
        return hoStoreLeadtime;
    }

                        
    /**
     * Set the value of HoStoreLeadtime
     *
     * @param v new value
     */
    public void setHoStoreLeadtime(int v) 
    {
    
                  if (this.hoStoreLeadtime != v)
              {
            this.hoStoreLeadtime = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationType
     *
     * @return int
     */
    public int getLocationType()
    {
        return locationType;
    }

                        
    /**
     * Set the value of LocationType
     *
     * @param v new value
     */
    public void setLocationType(int v) 
    {
    
                  if (this.locationType != v)
              {
            this.locationType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ObTransId
     *
     * @return String
     */
    public String getObTransId()
    {
        return obTransId;
    }

                        
    /**
     * Set the value of ObTransId
     *
     * @param v new value
     */
    public void setObTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.obTransId, v))
              {
            this.obTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UpdateDate
     *
     * @return Date
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

                        
    /**
     * Set the value of UpdateDate
     *
     * @param v new value
     */
    public void setUpdateDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.updateDate, v))
              {
            this.updateDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationPhone
     *
     * @return String
     */
    public String getLocationPhone()
    {
        return locationPhone;
    }

                        
    /**
     * Set the value of LocationPhone
     *
     * @param v new value
     */
    public void setLocationPhone(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationPhone, v))
              {
            this.locationPhone = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationEmail
     *
     * @return String
     */
    public String getLocationEmail()
    {
        return locationEmail;
    }

                        
    /**
     * Set the value of LocationEmail
     *
     * @param v new value
     */
    public void setLocationEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationEmail, v))
              {
            this.locationEmail = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationUrl
     *
     * @return String
     */
    public String getLocationUrl()
    {
        return locationUrl;
    }

                        
    /**
     * Set the value of LocationUrl
     *
     * @param v new value
     */
    public void setLocationUrl(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationUrl, v))
              {
            this.locationUrl = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ParentLocationId
     *
     * @return String
     */
    public String getParentLocationId()
    {
        return parentLocationId;
    }

                        
    /**
     * Set the value of ParentLocationId
     *
     * @param v new value
     */
    public void setParentLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.parentLocationId, v))
              {
            this.parentLocationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransferAccount
     *
     * @return String
     */
    public String getTransferAccount()
    {
        return transferAccount;
    }

                        
    /**
     * Set the value of TransferAccount
     *
     * @param v new value
     */
    public void setTransferAccount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transferAccount, v))
              {
            this.transferAccount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SalesAreaId
     *
     * @return String
     */
    public String getSalesAreaId()
    {
        return salesAreaId;
    }

                        
    /**
     * Set the value of SalesAreaId
     *
     * @param v new value
     */
    public void setSalesAreaId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.salesAreaId, v))
              {
            this.salesAreaId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TerritoryId
     *
     * @return String
     */
    public String getTerritoryId()
    {
        return territoryId;
    }

                        
    /**
     * Set the value of TerritoryId
     *
     * @param v new value
     */
    public void setTerritoryId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.territoryId, v))
              {
            this.territoryId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Longitudes
     *
     * @return BigDecimal
     */
    public BigDecimal getLongitudes()
    {
        return longitudes;
    }

                        
    /**
     * Set the value of Longitudes
     *
     * @param v new value
     */
    public void setLongitudes(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.longitudes, v))
              {
            this.longitudes = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Latitudes
     *
     * @return BigDecimal
     */
    public BigDecimal getLatitudes()
    {
        return latitudes;
    }

                        
    /**
     * Set the value of Latitudes
     *
     * @param v new value
     */
    public void setLatitudes(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.latitudes, v))
              {
            this.latitudes = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("LocationId");
              fieldNames.add("LocationCode");
              fieldNames.add("LocationName");
              fieldNames.add("LocationAddress");
              fieldNames.add("SiteId");
              fieldNames.add("InventoryType");
              fieldNames.add("HoStoreLeadtime");
              fieldNames.add("Description");
              fieldNames.add("LocationType");
              fieldNames.add("ObTransId");
              fieldNames.add("UpdateDate");
              fieldNames.add("LocationPhone");
              fieldNames.add("LocationEmail");
              fieldNames.add("LocationUrl");
              fieldNames.add("ParentLocationId");
              fieldNames.add("TransferAccount");
              fieldNames.add("SalesAreaId");
              fieldNames.add("TerritoryId");
              fieldNames.add("Longitudes");
              fieldNames.add("Latitudes");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationCode"))
        {
                return getLocationCode();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("LocationAddress"))
        {
                return getLocationAddress();
            }
          if (name.equals("SiteId"))
        {
                return getSiteId();
            }
          if (name.equals("InventoryType"))
        {
                return Integer.valueOf(getInventoryType());
            }
          if (name.equals("HoStoreLeadtime"))
        {
                return Integer.valueOf(getHoStoreLeadtime());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("LocationType"))
        {
                return Integer.valueOf(getLocationType());
            }
          if (name.equals("ObTransId"))
        {
                return getObTransId();
            }
          if (name.equals("UpdateDate"))
        {
                return getUpdateDate();
            }
          if (name.equals("LocationPhone"))
        {
                return getLocationPhone();
            }
          if (name.equals("LocationEmail"))
        {
                return getLocationEmail();
            }
          if (name.equals("LocationUrl"))
        {
                return getLocationUrl();
            }
          if (name.equals("ParentLocationId"))
        {
                return getParentLocationId();
            }
          if (name.equals("TransferAccount"))
        {
                return getTransferAccount();
            }
          if (name.equals("SalesAreaId"))
        {
                return getSalesAreaId();
            }
          if (name.equals("TerritoryId"))
        {
                return getTerritoryId();
            }
          if (name.equals("Longitudes"))
        {
                return getLongitudes();
            }
          if (name.equals("Latitudes"))
        {
                return getLatitudes();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(LocationPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(LocationPeer.LOCATION_CODE))
        {
                return getLocationCode();
            }
          if (name.equals(LocationPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(LocationPeer.LOCATION_ADDRESS))
        {
                return getLocationAddress();
            }
          if (name.equals(LocationPeer.SITE_ID))
        {
                return getSiteId();
            }
          if (name.equals(LocationPeer.INVENTORY_TYPE))
        {
                return Integer.valueOf(getInventoryType());
            }
          if (name.equals(LocationPeer.HO_STORE_LEADTIME))
        {
                return Integer.valueOf(getHoStoreLeadtime());
            }
          if (name.equals(LocationPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(LocationPeer.LOCATION_TYPE))
        {
                return Integer.valueOf(getLocationType());
            }
          if (name.equals(LocationPeer.OB_TRANS_ID))
        {
                return getObTransId();
            }
          if (name.equals(LocationPeer.UPDATE_DATE))
        {
                return getUpdateDate();
            }
          if (name.equals(LocationPeer.LOCATION_PHONE))
        {
                return getLocationPhone();
            }
          if (name.equals(LocationPeer.LOCATION_EMAIL))
        {
                return getLocationEmail();
            }
          if (name.equals(LocationPeer.LOCATION_URL))
        {
                return getLocationUrl();
            }
          if (name.equals(LocationPeer.PARENT_LOCATION_ID))
        {
                return getParentLocationId();
            }
          if (name.equals(LocationPeer.TRANSFER_ACCOUNT))
        {
                return getTransferAccount();
            }
          if (name.equals(LocationPeer.SALES_AREA_ID))
        {
                return getSalesAreaId();
            }
          if (name.equals(LocationPeer.TERRITORY_ID))
        {
                return getTerritoryId();
            }
          if (name.equals(LocationPeer.LONGITUDES))
        {
                return getLongitudes();
            }
          if (name.equals(LocationPeer.LATITUDES))
        {
                return getLatitudes();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getLocationId();
            }
              if (pos == 1)
        {
                return getLocationCode();
            }
              if (pos == 2)
        {
                return getLocationName();
            }
              if (pos == 3)
        {
                return getLocationAddress();
            }
              if (pos == 4)
        {
                return getSiteId();
            }
              if (pos == 5)
        {
                return Integer.valueOf(getInventoryType());
            }
              if (pos == 6)
        {
                return Integer.valueOf(getHoStoreLeadtime());
            }
              if (pos == 7)
        {
                return getDescription();
            }
              if (pos == 8)
        {
                return Integer.valueOf(getLocationType());
            }
              if (pos == 9)
        {
                return getObTransId();
            }
              if (pos == 10)
        {
                return getUpdateDate();
            }
              if (pos == 11)
        {
                return getLocationPhone();
            }
              if (pos == 12)
        {
                return getLocationEmail();
            }
              if (pos == 13)
        {
                return getLocationUrl();
            }
              if (pos == 14)
        {
                return getParentLocationId();
            }
              if (pos == 15)
        {
                return getTransferAccount();
            }
              if (pos == 16)
        {
                return getSalesAreaId();
            }
              if (pos == 17)
        {
                return getTerritoryId();
            }
              if (pos == 18)
        {
                return getLongitudes();
            }
              if (pos == 19)
        {
                return getLatitudes();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(LocationPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        LocationPeer.doInsert((Location) this, con);
                        setNew(false);
                    }
                    else
                    {
                        LocationPeer.doUpdate((Location) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key locationId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setLocationId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setLocationId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getLocationId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Location copy() throws TorqueException
    {
        return copyInto(new Location());
    }
  
    protected Location copyInto(Location copyObj) throws TorqueException
    {
          copyObj.setLocationId(locationId);
          copyObj.setLocationCode(locationCode);
          copyObj.setLocationName(locationName);
          copyObj.setLocationAddress(locationAddress);
          copyObj.setSiteId(siteId);
          copyObj.setInventoryType(inventoryType);
          copyObj.setHoStoreLeadtime(hoStoreLeadtime);
          copyObj.setDescription(description);
          copyObj.setLocationType(locationType);
          copyObj.setObTransId(obTransId);
          copyObj.setUpdateDate(updateDate);
          copyObj.setLocationPhone(locationPhone);
          copyObj.setLocationEmail(locationEmail);
          copyObj.setLocationUrl(locationUrl);
          copyObj.setParentLocationId(parentLocationId);
          copyObj.setTransferAccount(transferAccount);
          copyObj.setSalesAreaId(salesAreaId);
          copyObj.setTerritoryId(territoryId);
          copyObj.setLongitudes(longitudes);
          copyObj.setLatitudes(latitudes);
  
                    copyObj.setLocationId((String)null);
                                                                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public LocationPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Location\n");
        str.append("--------\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationCode         : ")
           .append(getLocationCode())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("LocationAddress      : ")
           .append(getLocationAddress())
           .append("\n")
           .append("SiteId               : ")
           .append(getSiteId())
           .append("\n")
           .append("InventoryType        : ")
           .append(getInventoryType())
           .append("\n")
           .append("HoStoreLeadtime      : ")
           .append(getHoStoreLeadtime())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("LocationType         : ")
           .append(getLocationType())
           .append("\n")
           .append("ObTransId            : ")
           .append(getObTransId())
           .append("\n")
           .append("UpdateDate           : ")
           .append(getUpdateDate())
           .append("\n")
           .append("LocationPhone        : ")
           .append(getLocationPhone())
           .append("\n")
           .append("LocationEmail        : ")
           .append(getLocationEmail())
           .append("\n")
           .append("LocationUrl          : ")
           .append(getLocationUrl())
           .append("\n")
           .append("ParentLocationId     : ")
           .append(getParentLocationId())
           .append("\n")
           .append("TransferAccount      : ")
           .append(getTransferAccount())
           .append("\n")
           .append("SalesAreaId          : ")
           .append(getSalesAreaId())
           .append("\n")
           .append("TerritoryId          : ")
           .append(getTerritoryId())
           .append("\n")
           .append("Longitudes           : ")
           .append(getLongitudes())
           .append("\n")
           .append("Latitudes            : ")
           .append(getLatitudes())
           .append("\n")
        ;
        return(str.toString());
    }
}

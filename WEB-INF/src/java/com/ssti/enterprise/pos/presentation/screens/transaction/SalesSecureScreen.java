package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.sales.CreditLimitValidator;
import com.ssti.framework.tools.DeviceConfigTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/screens/transaction/PurchaseSecureScreen.java,v $
 * Purpose: SalesTransaction Secure screen, super class for all sales transaction screen
 * implements TransactionAttributes to hold various transaction related constants
 *
 * @author  $Author: albert $
 * @version $Id: PurchaseSecureScreen.java,v 1.2 2008/03/15 12:39:33 albert Exp $
 *
 * $Log: SalesSecureScreen.java,v $
 *
 *
 */
public class SalesSecureScreen extends TransactionSecureScreen 
{ 	
	protected static final boolean b_SALES_MERGE_DETAIL = PreferenceTool.getSalesMergeSameItem();
    protected static final boolean b_SALES_USE_FOB_COURIER = PreferenceTool.getSalesFobCourier();
    protected static final boolean b_SALES_FIRST_LAST = PreferenceTool.getSalesFirstlineInsert();
	
	protected static final String s_USE_SALES = "UseSales" ;
	protected static final String s_SALES_COMMA_SCALE  = "SalescommaScale";
    protected static final String s_CHG_PRICE = "AlterPrice" ;
    protected static final String s_CHG_DISC = "AlterDisc" ;
    protected int i_MERGE_DET = 1;       
    
	
	/**
	 * set common variable for all SO, DO, SI in context 
	 * 
	 * @param data
	 * @param context
	 */
	protected void setContext(RunData data, Context context)
	{
    	//set config in context
    	context.put(s_USE_FOB_COURIER, b_SALES_USE_FOB_COURIER);
    	context.put(s_MULTI_CURRENCY, b_SALES_MULTI_CURRENCY);
    	context.put(s_SALES_COMMA_SCALE, i_SALES_COMMA_SCALE);
    	context.put(s_USE_SALES, b_USE_SALES);
    	context.put("creditlimit", CreditLimitValidator.getInstance());    
        context.put(s_CHG_PRICE, hasPermission(data,"Alter Sales Price"));
        context.put(s_CHG_DISC, hasPermission(data,"Alter Sales Discount"));
            	
    	if(hasPermission(data, "Access All Vendor"))
    	{
    	    context.put(s_RELATED_EMPLOYEE, Boolean.valueOf(false));
        }
	}
	
	/**
	 * set invoice / POS related variable in SI / POS / MED POS / WO
	 * 
	 * @param data
	 * @param context
	 */
	protected void setInvoiceContext(RunData data, Context context)
	{
		context.put(m_CTX_USE_DRAWER,      DeviceConfigTool.drawerUse());
    	context.put(m_CTX_USE_DISPLAYER,   DeviceConfigTool.displayerUse());    	
    	context.put(m_CTX_DIRECTPRINT,     PreferenceTool.posSaveDirectPrint());    	    	
    	context.put(m_CTX_CONFIRM_QTY,     PreferenceTool.posConfirmQty());    	
    	context.put(m_CTX_CONFIRM_COST,    PreferenceTool.posConfirmCost());	
    	context.put(m_CTX_VALIDATE_CHANGE, PreferenceTool.validateChangePrice());    	
    	context.put(m_CTX_VALIDATE_DELETE, PreferenceTool.validateDelete());	
	}

	public void doBuildTemplate(RunData data, Context context)
	{
		i_MERGE_DET = PreferenceTool.getSalesMergeItem();
    	setContext(data, context);    	
    	super.doBuildTemplate(data, context);
	}		
}

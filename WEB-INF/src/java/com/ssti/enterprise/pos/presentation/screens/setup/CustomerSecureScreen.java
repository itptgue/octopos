package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;

import com.ssti.framework.presentation.SecureScreen;

public class CustomerSecureScreen extends SecureScreen
{
	private static final String[] a_PERM = {"View Customer Data"};
	
    protected boolean isAuthorized(RunData data)
        throws Exception
    {
        return isAuthorized(data, a_PERM);
    }
}

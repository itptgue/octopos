package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CashFlowTypeMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CashFlowTypeMapBuilder";

    /**
     * Item
     * @deprecated use CashFlowTypePeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "cash_flow_type";
    }

  
    /**
     * cash_flow_type.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use CashFlowTypePeer.cash_flow_type.CASH_FLOW_TYPE_ID constant
     */
    public static String getCashFlowType_CashFlowTypeId()
    {
        return "cash_flow_type.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * cash_flow_type.CASH_FLOW_TYPE_CODE
     * @return the column name for the CASH_FLOW_TYPE_CODE field
     * @deprecated use CashFlowTypePeer.cash_flow_type.CASH_FLOW_TYPE_CODE constant
     */
    public static String getCashFlowType_CashFlowTypeCode()
    {
        return "cash_flow_type.CASH_FLOW_TYPE_CODE";
    }
  
    /**
     * cash_flow_type.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use CashFlowTypePeer.cash_flow_type.DESCRIPTION constant
     */
    public static String getCashFlowType_Description()
    {
        return "cash_flow_type.DESCRIPTION";
    }
  
    /**
     * cash_flow_type.CASH_FLOW_TYPE_GROUP
     * @return the column name for the CASH_FLOW_TYPE_GROUP field
     * @deprecated use CashFlowTypePeer.cash_flow_type.CASH_FLOW_TYPE_GROUP constant
     */
    public static String getCashFlowType_CashFlowTypeGroup()
    {
        return "cash_flow_type.CASH_FLOW_TYPE_GROUP";
    }
  
    /**
     * cash_flow_type.DEFAULT_TYPE
     * @return the column name for the DEFAULT_TYPE field
     * @deprecated use CashFlowTypePeer.cash_flow_type.DEFAULT_TYPE constant
     */
    public static String getCashFlowType_DefaultType()
    {
        return "cash_flow_type.DEFAULT_TYPE";
    }
  
    /**
     * cash_flow_type.CASH_FLOW_LEVEL
     * @return the column name for the CASH_FLOW_LEVEL field
     * @deprecated use CashFlowTypePeer.cash_flow_type.CASH_FLOW_LEVEL constant
     */
    public static String getCashFlowType_CashFlowLevel()
    {
        return "cash_flow_type.CASH_FLOW_LEVEL";
    }
  
    /**
     * cash_flow_type.PARENT_ID
     * @return the column name for the PARENT_ID field
     * @deprecated use CashFlowTypePeer.cash_flow_type.PARENT_ID constant
     */
    public static String getCashFlowType_ParentId()
    {
        return "cash_flow_type.PARENT_ID";
    }
  
    /**
     * cash_flow_type.HAS_CHILD
     * @return the column name for the HAS_CHILD field
     * @deprecated use CashFlowTypePeer.cash_flow_type.HAS_CHILD constant
     */
    public static String getCashFlowType_HasChild()
    {
        return "cash_flow_type.HAS_CHILD";
    }
  
    /**
     * cash_flow_type.ALT_CODE
     * @return the column name for the ALT_CODE field
     * @deprecated use CashFlowTypePeer.cash_flow_type.ALT_CODE constant
     */
    public static String getCashFlowType_AltCode()
    {
        return "cash_flow_type.ALT_CODE";
    }
  
    /**
     * cash_flow_type.DEFAULT_CM
     * @return the column name for the DEFAULT_CM field
     * @deprecated use CashFlowTypePeer.cash_flow_type.DEFAULT_CM constant
     */
    public static String getCashFlowType_DefaultCm()
    {
        return "cash_flow_type.DEFAULT_CM";
    }
  
    /**
     * cash_flow_type.DEFAULT_DM
     * @return the column name for the DEFAULT_DM field
     * @deprecated use CashFlowTypePeer.cash_flow_type.DEFAULT_DM constant
     */
    public static String getCashFlowType_DefaultDm()
    {
        return "cash_flow_type.DEFAULT_DM";
    }
  
    /**
     * cash_flow_type.DEFAULT_RP
     * @return the column name for the DEFAULT_RP field
     * @deprecated use CashFlowTypePeer.cash_flow_type.DEFAULT_RP constant
     */
    public static String getCashFlowType_DefaultRp()
    {
        return "cash_flow_type.DEFAULT_RP";
    }
  
    /**
     * cash_flow_type.DEFAULT_PP
     * @return the column name for the DEFAULT_PP field
     * @deprecated use CashFlowTypePeer.cash_flow_type.DEFAULT_PP constant
     */
    public static String getCashFlowType_DefaultPp()
    {
        return "cash_flow_type.DEFAULT_PP";
    }
  
    /**
     * cash_flow_type.DEFAULT_CF
     * @return the column name for the DEFAULT_CF field
     * @deprecated use CashFlowTypePeer.cash_flow_type.DEFAULT_CF constant
     */
    public static String getCashFlowType_DefaultCf()
    {
        return "cash_flow_type.DEFAULT_CF";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("cash_flow_type");
        TableMap tMap = dbMap.getTable("cash_flow_type");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("cash_flow_type.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("cash_flow_type.CASH_FLOW_TYPE_CODE", "");
                          tMap.addColumn("cash_flow_type.DESCRIPTION", "");
                          tMap.addColumn("cash_flow_type.CASH_FLOW_TYPE_GROUP", "");
                            tMap.addColumn("cash_flow_type.DEFAULT_TYPE", Integer.valueOf(0));
                            tMap.addColumn("cash_flow_type.CASH_FLOW_LEVEL", Integer.valueOf(0));
                          tMap.addColumn("cash_flow_type.PARENT_ID", "");
                          tMap.addColumn("cash_flow_type.HAS_CHILD", Boolean.TRUE);
                          tMap.addColumn("cash_flow_type.ALT_CODE", "");
                          tMap.addColumn("cash_flow_type.DEFAULT_CM", Boolean.TRUE);
                          tMap.addColumn("cash_flow_type.DEFAULT_DM", Boolean.TRUE);
                          tMap.addColumn("cash_flow_type.DEFAULT_RP", Boolean.TRUE);
                          tMap.addColumn("cash_flow_type.DEFAULT_PP", Boolean.TRUE);
                          tMap.addColumn("cash_flow_type.DEFAULT_CF", Boolean.TRUE);
          }
}

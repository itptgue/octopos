package com.ssti.framework.exception;

public class DuplicateNoException extends Exception 
{
	static final String s_PREFIX = "Invalid No, already used by other transaction : ";
	public DuplicateNoException (String _sTransNo)
	{
		super(s_PREFIX + _sTransNo);
	}
}

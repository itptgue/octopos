package com.ssti.enterprise.medical.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PrescriptionDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.medical.om.map.PrescriptionDetailMapBuilder";

    /**
     * Item
     * @deprecated use PrescriptionDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "prescription_detail";
    }

  
    /**
     * prescription_detail.PRESCRIPTION_DETAIL_ID
     * @return the column name for the PRESCRIPTION_DETAIL_ID field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.PRESCRIPTION_DETAIL_ID constant
     */
    public static String getPrescriptionDetail_PrescriptionDetailId()
    {
        return "prescription_detail.PRESCRIPTION_DETAIL_ID";
    }
  
    /**
     * prescription_detail.PRESCRIPTION_ID
     * @return the column name for the PRESCRIPTION_ID field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.PRESCRIPTION_ID constant
     */
    public static String getPrescriptionDetail_PrescriptionId()
    {
        return "prescription_detail.PRESCRIPTION_ID";
    }
  
    /**
     * prescription_detail.PRESCRIPTION_TYPE
     * @return the column name for the PRESCRIPTION_TYPE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.PRESCRIPTION_TYPE constant
     */
    public static String getPrescriptionDetail_PrescriptionType()
    {
        return "prescription_detail.PRESCRIPTION_TYPE";
    }
  
    /**
     * prescription_detail.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.ITEM_ID constant
     */
    public static String getPrescriptionDetail_ItemId()
    {
        return "prescription_detail.ITEM_ID";
    }
  
    /**
     * prescription_detail.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.ITEM_CODE constant
     */
    public static String getPrescriptionDetail_ItemCode()
    {
        return "prescription_detail.ITEM_CODE";
    }
  
    /**
     * prescription_detail.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.DESCRIPTION constant
     */
    public static String getPrescriptionDetail_Description()
    {
        return "prescription_detail.DESCRIPTION";
    }
  
    /**
     * prescription_detail.UNIT_ID
     * @return the column name for the UNIT_ID field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.UNIT_ID constant
     */
    public static String getPrescriptionDetail_UnitId()
    {
        return "prescription_detail.UNIT_ID";
    }
  
    /**
     * prescription_detail.UNIT_CODE
     * @return the column name for the UNIT_CODE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.UNIT_CODE constant
     */
    public static String getPrescriptionDetail_UnitCode()
    {
        return "prescription_detail.UNIT_CODE";
    }
  
    /**
     * prescription_detail.REQ_QTY
     * @return the column name for the REQ_QTY field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.REQ_QTY constant
     */
    public static String getPrescriptionDetail_ReqQty()
    {
        return "prescription_detail.REQ_QTY";
    }
  
    /**
     * prescription_detail.QTY
     * @return the column name for the QTY field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.QTY constant
     */
    public static String getPrescriptionDetail_Qty()
    {
        return "prescription_detail.QTY";
    }
  
    /**
     * prescription_detail.QTY_BASE
     * @return the column name for the QTY_BASE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.QTY_BASE constant
     */
    public static String getPrescriptionDetail_QtyBase()
    {
        return "prescription_detail.QTY_BASE";
    }
  
    /**
     * prescription_detail.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.ITEM_PRICE constant
     */
    public static String getPrescriptionDetail_ItemPrice()
    {
        return "prescription_detail.ITEM_PRICE";
    }
  
    /**
     * prescription_detail.TAX_ID
     * @return the column name for the TAX_ID field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.TAX_ID constant
     */
    public static String getPrescriptionDetail_TaxId()
    {
        return "prescription_detail.TAX_ID";
    }
  
    /**
     * prescription_detail.TAX_AMOUNT
     * @return the column name for the TAX_AMOUNT field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.TAX_AMOUNT constant
     */
    public static String getPrescriptionDetail_TaxAmount()
    {
        return "prescription_detail.TAX_AMOUNT";
    }
  
    /**
     * prescription_detail.SUB_TOTAL
     * @return the column name for the SUB_TOTAL field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.SUB_TOTAL constant
     */
    public static String getPrescriptionDetail_SubTotal()
    {
        return "prescription_detail.SUB_TOTAL";
    }
  
    /**
     * prescription_detail.RFEE
     * @return the column name for the RFEE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.RFEE constant
     */
    public static String getPrescriptionDetail_Rfee()
    {
        return "prescription_detail.RFEE";
    }
  
    /**
     * prescription_detail.PACKAGING
     * @return the column name for the PACKAGING field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.PACKAGING constant
     */
    public static String getPrescriptionDetail_Packaging()
    {
        return "prescription_detail.PACKAGING";
    }
  
    /**
     * prescription_detail.TOTAL
     * @return the column name for the TOTAL field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.TOTAL constant
     */
    public static String getPrescriptionDetail_Total()
    {
        return "prescription_detail.TOTAL";
    }
  
    /**
     * prescription_detail.DOSAGE
     * @return the column name for the DOSAGE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.DOSAGE constant
     */
    public static String getPrescriptionDetail_Dosage()
    {
        return "prescription_detail.DOSAGE";
    }
  
    /**
     * prescription_detail.BEFORE_AFTER
     * @return the column name for the BEFORE_AFTER field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.BEFORE_AFTER constant
     */
    public static String getPrescriptionDetail_BeforeAfter()
    {
        return "prescription_detail.BEFORE_AFTER";
    }
  
    /**
     * prescription_detail.INSTRUCTION
     * @return the column name for the INSTRUCTION field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.INSTRUCTION constant
     */
    public static String getPrescriptionDetail_Instruction()
    {
        return "prescription_detail.INSTRUCTION";
    }
  
    /**
     * prescription_detail.POWDER_ITEM_ID
     * @return the column name for the POWDER_ITEM_ID field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.POWDER_ITEM_ID constant
     */
    public static String getPrescriptionDetail_PowderItemId()
    {
        return "prescription_detail.POWDER_ITEM_ID";
    }
  
    /**
     * prescription_detail.RESULT_QTY
     * @return the column name for the RESULT_QTY field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.RESULT_QTY constant
     */
    public static String getPrescriptionDetail_ResultQty()
    {
        return "prescription_detail.RESULT_QTY";
    }
  
    /**
     * prescription_detail.BATCH_NO
     * @return the column name for the BATCH_NO field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.BATCH_NO constant
     */
    public static String getPrescriptionDetail_BatchNo()
    {
        return "prescription_detail.BATCH_NO";
    }
  
    /**
     * prescription_detail.PRES_FILE
     * @return the column name for the PRES_FILE field
     * @deprecated use PrescriptionDetailPeer.prescription_detail.PRES_FILE constant
     */
    public static String getPrescriptionDetail_PresFile()
    {
        return "prescription_detail.PRES_FILE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("prescription_detail");
        TableMap tMap = dbMap.getTable("prescription_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("prescription_detail.PRESCRIPTION_DETAIL_ID", "");
                          tMap.addForeignKey(
                "prescription_detail.PRESCRIPTION_ID", "" , "prescription" ,
                "prescription_id");
                            tMap.addColumn("prescription_detail.PRESCRIPTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("prescription_detail.ITEM_ID", "");
                          tMap.addColumn("prescription_detail.ITEM_CODE", "");
                          tMap.addColumn("prescription_detail.DESCRIPTION", "");
                          tMap.addColumn("prescription_detail.UNIT_ID", "");
                          tMap.addColumn("prescription_detail.UNIT_CODE", "");
                            tMap.addColumn("prescription_detail.REQ_QTY", bd_ZERO);
                            tMap.addColumn("prescription_detail.QTY", bd_ZERO);
                            tMap.addColumn("prescription_detail.QTY_BASE", bd_ZERO);
                            tMap.addColumn("prescription_detail.ITEM_PRICE", bd_ZERO);
                          tMap.addColumn("prescription_detail.TAX_ID", "");
                            tMap.addColumn("prescription_detail.TAX_AMOUNT", bd_ZERO);
                            tMap.addColumn("prescription_detail.SUB_TOTAL", bd_ZERO);
                          tMap.addColumn("prescription_detail.RFEE", "");
                          tMap.addColumn("prescription_detail.PACKAGING", "");
                            tMap.addColumn("prescription_detail.TOTAL", bd_ZERO);
                          tMap.addColumn("prescription_detail.DOSAGE", "");
                          tMap.addColumn("prescription_detail.BEFORE_AFTER", "");
                          tMap.addColumn("prescription_detail.INSTRUCTION", "");
                          tMap.addColumn("prescription_detail.POWDER_ITEM_ID", "");
                            tMap.addColumn("prescription_detail.RESULT_QTY", bd_ZERO);
                          tMap.addColumn("prescription_detail.BATCH_NO", "");
                          tMap.addColumn("prescription_detail.PRES_FILE", "");
          }
}

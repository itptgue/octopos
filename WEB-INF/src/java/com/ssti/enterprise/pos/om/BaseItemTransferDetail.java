package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to ItemTransferDetail
 */
public abstract class BaseItemTransferDetail extends BaseObject
{
    /** The Peer class */
    private static final ItemTransferDetailPeer peer =
        new ItemTransferDetailPeer();

        
    /** The value for the itemTransferDetailId field */
    private String itemTransferDetailId;
      
    /** The value for the itemTransferId field */
    private String itemTransferId;
      
    /** The value for the indexNo field */
    private int indexNo;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the itemCode field */
    private String itemCode;
      
    /** The value for the unitId field */
    private String unitId;
      
    /** The value for the unitCode field */
    private String unitCode;
      
    /** The value for the qtyChanges field */
    private BigDecimal qtyChanges;
      
    /** The value for the qtyBase field */
    private BigDecimal qtyBase;
      
    /** The value for the costPerUnit field */
    private BigDecimal costPerUnit;
  
    
    /**
     * Get the ItemTransferDetailId
     *
     * @return String
     */
    public String getItemTransferDetailId()
    {
        return itemTransferDetailId;
    }

                        
    /**
     * Set the value of ItemTransferDetailId
     *
     * @param v new value
     */
    public void setItemTransferDetailId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemTransferDetailId, v))
              {
            this.itemTransferDetailId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemTransferId
     *
     * @return String
     */
    public String getItemTransferId()
    {
        return itemTransferId;
    }

                              
    /**
     * Set the value of ItemTransferId
     *
     * @param v new value
     */
    public void setItemTransferId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.itemTransferId, v))
              {
            this.itemTransferId = v;
            setModified(true);
        }
    
                          
                if (aItemTransfer != null && !ObjectUtils.equals(aItemTransfer.getItemTransferId(), v))
                {
            aItemTransfer = null;
        }
      
              }
  
    /**
     * Get the IndexNo
     *
     * @return int
     */
    public int getIndexNo()
    {
        return indexNo;
    }

                        
    /**
     * Set the value of IndexNo
     *
     * @param v new value
     */
    public void setIndexNo(int v) 
    {
    
                  if (this.indexNo != v)
              {
            this.indexNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCode
     *
     * @return String
     */
    public String getItemCode()
    {
        return itemCode;
    }

                        
    /**
     * Set the value of ItemCode
     *
     * @param v new value
     */
    public void setItemCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCode, v))
              {
            this.itemCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitId
     *
     * @return String
     */
    public String getUnitId()
    {
        return unitId;
    }

                        
    /**
     * Set the value of UnitId
     *
     * @param v new value
     */
    public void setUnitId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitId, v))
              {
            this.unitId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UnitCode
     *
     * @return String
     */
    public String getUnitCode()
    {
        return unitCode;
    }

                        
    /**
     * Set the value of UnitCode
     *
     * @param v new value
     */
    public void setUnitCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.unitCode, v))
              {
            this.unitCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyChanges
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyChanges()
    {
        return qtyChanges;
    }

                        
    /**
     * Set the value of QtyChanges
     *
     * @param v new value
     */
    public void setQtyChanges(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyChanges, v))
              {
            this.qtyChanges = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the QtyBase
     *
     * @return BigDecimal
     */
    public BigDecimal getQtyBase()
    {
        return qtyBase;
    }

                        
    /**
     * Set the value of QtyBase
     *
     * @param v new value
     */
    public void setQtyBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qtyBase, v))
              {
            this.qtyBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CostPerUnit
     *
     * @return BigDecimal
     */
    public BigDecimal getCostPerUnit()
    {
        return costPerUnit;
    }

                        
    /**
     * Set the value of CostPerUnit
     *
     * @param v new value
     */
    public void setCostPerUnit(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.costPerUnit, v))
              {
            this.costPerUnit = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private ItemTransfer aItemTransfer;

    /**
     * Declares an association between this object and a ItemTransfer object
     *
     * @param v ItemTransfer
     * @throws TorqueException
     */
    public void setItemTransfer(ItemTransfer v) throws TorqueException
    {
            if (v == null)
        {
                  setItemTransferId((String) null);
              }
        else
        {
            setItemTransferId(v.getItemTransferId());
        }
            aItemTransfer = v;
    }

                                            
    /**
     * Get the associated ItemTransfer object
     *
     * @return the associated ItemTransfer object
     * @throws TorqueException
     */
    public ItemTransfer getItemTransfer() throws TorqueException
    {
        if (aItemTransfer == null && (!ObjectUtils.equals(this.itemTransferId, null)))
        {
                          aItemTransfer = ItemTransferPeer.retrieveByPK(SimpleKey.keyFor(this.itemTransferId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               ItemTransfer obj = ItemTransferPeer.retrieveByPK(this.itemTransferId);
               obj.addItemTransferDetails(this);
            */
        }
        return aItemTransfer;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setItemTransferKey(ObjectKey key) throws TorqueException
    {
      
                        setItemTransferId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("ItemTransferDetailId");
              fieldNames.add("ItemTransferId");
              fieldNames.add("IndexNo");
              fieldNames.add("ItemId");
              fieldNames.add("ItemCode");
              fieldNames.add("UnitId");
              fieldNames.add("UnitCode");
              fieldNames.add("QtyChanges");
              fieldNames.add("QtyBase");
              fieldNames.add("CostPerUnit");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("ItemTransferDetailId"))
        {
                return getItemTransferDetailId();
            }
          if (name.equals("ItemTransferId"))
        {
                return getItemTransferId();
            }
          if (name.equals("IndexNo"))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("ItemCode"))
        {
                return getItemCode();
            }
          if (name.equals("UnitId"))
        {
                return getUnitId();
            }
          if (name.equals("UnitCode"))
        {
                return getUnitCode();
            }
          if (name.equals("QtyChanges"))
        {
                return getQtyChanges();
            }
          if (name.equals("QtyBase"))
        {
                return getQtyBase();
            }
          if (name.equals("CostPerUnit"))
        {
                return getCostPerUnit();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(ItemTransferDetailPeer.ITEM_TRANSFER_DETAIL_ID))
        {
                return getItemTransferDetailId();
            }
          if (name.equals(ItemTransferDetailPeer.ITEM_TRANSFER_ID))
        {
                return getItemTransferId();
            }
          if (name.equals(ItemTransferDetailPeer.INDEX_NO))
        {
                return Integer.valueOf(getIndexNo());
            }
          if (name.equals(ItemTransferDetailPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(ItemTransferDetailPeer.ITEM_CODE))
        {
                return getItemCode();
            }
          if (name.equals(ItemTransferDetailPeer.UNIT_ID))
        {
                return getUnitId();
            }
          if (name.equals(ItemTransferDetailPeer.UNIT_CODE))
        {
                return getUnitCode();
            }
          if (name.equals(ItemTransferDetailPeer.QTY_CHANGES))
        {
                return getQtyChanges();
            }
          if (name.equals(ItemTransferDetailPeer.QTY_BASE))
        {
                return getQtyBase();
            }
          if (name.equals(ItemTransferDetailPeer.COST_PER_UNIT))
        {
                return getCostPerUnit();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getItemTransferDetailId();
            }
              if (pos == 1)
        {
                return getItemTransferId();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getIndexNo());
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getItemCode();
            }
              if (pos == 5)
        {
                return getUnitId();
            }
              if (pos == 6)
        {
                return getUnitCode();
            }
              if (pos == 7)
        {
                return getQtyChanges();
            }
              if (pos == 8)
        {
                return getQtyBase();
            }
              if (pos == 9)
        {
                return getCostPerUnit();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(ItemTransferDetailPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        ItemTransferDetailPeer.doInsert((ItemTransferDetail) this, con);
                        setNew(false);
                    }
                    else
                    {
                        ItemTransferDetailPeer.doUpdate((ItemTransferDetail) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key itemTransferDetailId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setItemTransferDetailId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setItemTransferDetailId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getItemTransferDetailId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public ItemTransferDetail copy() throws TorqueException
    {
        return copyInto(new ItemTransferDetail());
    }
  
    protected ItemTransferDetail copyInto(ItemTransferDetail copyObj) throws TorqueException
    {
          copyObj.setItemTransferDetailId(itemTransferDetailId);
          copyObj.setItemTransferId(itemTransferId);
          copyObj.setIndexNo(indexNo);
          copyObj.setItemId(itemId);
          copyObj.setItemCode(itemCode);
          copyObj.setUnitId(unitId);
          copyObj.setUnitCode(unitCode);
          copyObj.setQtyChanges(qtyChanges);
          copyObj.setQtyBase(qtyBase);
          copyObj.setCostPerUnit(costPerUnit);
  
                    copyObj.setItemTransferDetailId((String)null);
                                                                  
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public ItemTransferDetailPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("ItemTransferDetail\n");
        str.append("------------------\n")
           .append("ItemTransferDetailId   : ")
           .append(getItemTransferDetailId())
           .append("\n")
           .append("ItemTransferId       : ")
           .append(getItemTransferId())
           .append("\n")
           .append("IndexNo              : ")
           .append(getIndexNo())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("ItemCode             : ")
           .append(getItemCode())
           .append("\n")
           .append("UnitId               : ")
           .append(getUnitId())
           .append("\n")
           .append("UnitCode             : ")
           .append(getUnitCode())
           .append("\n")
           .append("QtyChanges           : ")
           .append(getQtyChanges())
           .append("\n")
           .append("QtyBase              : ")
           .append(getQtyBase())
           .append("\n")
           .append("CostPerUnit          : ")
           .append(getCostPerUnit())
           .append("\n")
        ;
        return(str.toString());
    }
}

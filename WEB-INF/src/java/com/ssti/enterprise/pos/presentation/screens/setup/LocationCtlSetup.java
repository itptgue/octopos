package com.ssti.enterprise.pos.presentation.screens.setup;

import java.util.List;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.LocationCtl;
import com.ssti.enterprise.pos.om.LocationCtlPeer;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.framework.tools.StringUtil;

public class LocationCtlSetup extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
		String sMode = data.getParameters().getString("mode", "");
		String sID = data.getParameters().getString("id");
		int op = data.getParameters().getInt("op");
		try
		{
			if (op == 1)
			{
				//check existing
				String sLocID = data.getParameters().getString("LocationId");
				String sLocCode = LocationTool.getLocationCodeByID(sLocID);
				String sTRCode = data.getParameters().getString("TransCode");
				String sTblName = data.getParameters().getString("CtlTable");
				if (StringUtil.isNotEmpty(sLocID))
				{
					List v = LocationTool.findLocCtl(sLocID, "", "", sTRCode, "");				
					if (v.size() == 0)
					{
						String sCtlTable = "ctl_" + sLocCode + "_" + sTRCode;
						
						LocationCtl oCtl = new LocationCtl();
						data.getParameters().setProperties(oCtl);
						oCtl.setLocationCtlId(IDGenerator.generateSysID());
						oCtl.setCtlTable(sCtlTable);
						oCtl.save();				
						LastNumberTool.createControlTable(oCtl.getCtlTable());
					}
					else
					{
						data.setMessage("Trans No setting for Location " + sLocCode + " Trans Code " + sTRCode + " Already Exists!");
					}
				}
			}
			else if (op == 2)
			{
				LocationCtl oCtl = LocationTool.getLocCtl(sID);
				if (oCtl != null)
				{
					data.getParameters().setProperties(oCtl);
					oCtl.save();
				}
			}
			else if (op == 3)
			{
				LocationCtl oCtl = LocationTool.getLocCtl(sID);
				if (oCtl != null)
				{
					Criteria oCrit = new Criteria();
					oCrit.add(LocationCtlPeer.LOCATION_CTL_ID, sID);
					LocationCtlPeer.doDelete(oCrit);
				}
			}
		}
		catch (Exception _oEx)
		{
			data.setMessage (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
		}
    }
}

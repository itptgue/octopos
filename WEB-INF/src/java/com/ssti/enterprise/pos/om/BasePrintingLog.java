package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PrintingLog
 */
public abstract class BasePrintingLog extends BaseObject
{
    /** The Peer class */
    private static final PrintingLogPeer peer =
        new PrintingLogPeer();

        
    /** The value for the printingLogId field */
    private String printingLogId;
      
    /** The value for the printingDate field */
    private Date printingDate;
      
    /** The value for the transactionId field */
    private String transactionId;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the userName field */
    private String userName;
  
    
    /**
     * Get the PrintingLogId
     *
     * @return String
     */
    public String getPrintingLogId()
    {
        return printingLogId;
    }

                        
    /**
     * Set the value of PrintingLogId
     *
     * @param v new value
     */
    public void setPrintingLogId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.printingLogId, v))
              {
            this.printingLogId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PrintingDate
     *
     * @return Date
     */
    public Date getPrintingDate()
    {
        return printingDate;
    }

                        
    /**
     * Set the value of PrintingDate
     *
     * @param v new value
     */
    public void setPrintingDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.printingDate, v))
              {
            this.printingDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PrintingLogId");
              fieldNames.add("PrintingDate");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("UserName");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PrintingLogId"))
        {
                return getPrintingLogId();
            }
          if (name.equals("PrintingDate"))
        {
                return getPrintingDate();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PrintingLogPeer.PRINTING_LOG_ID))
        {
                return getPrintingLogId();
            }
          if (name.equals(PrintingLogPeer.PRINTING_DATE))
        {
                return getPrintingDate();
            }
          if (name.equals(PrintingLogPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(PrintingLogPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(PrintingLogPeer.USER_NAME))
        {
                return getUserName();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPrintingLogId();
            }
              if (pos == 1)
        {
                return getPrintingDate();
            }
              if (pos == 2)
        {
                return getTransactionId();
            }
              if (pos == 3)
        {
                return getTransactionNo();
            }
              if (pos == 4)
        {
                return getUserName();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PrintingLogPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PrintingLogPeer.doInsert((PrintingLog) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PrintingLogPeer.doUpdate((PrintingLog) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key printingLogId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPrintingLogId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPrintingLogId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPrintingLogId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PrintingLog copy() throws TorqueException
    {
        return copyInto(new PrintingLog());
    }
  
    protected PrintingLog copyInto(PrintingLog copyObj) throws TorqueException
    {
          copyObj.setPrintingLogId(printingLogId);
          copyObj.setPrintingDate(printingDate);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setUserName(userName);
  
                    copyObj.setPrintingLogId((String)null);
                                    
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PrintingLogPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PrintingLog\n");
        str.append("-----------\n")
           .append("PrintingLogId        : ")
           .append(getPrintingLogId())
           .append("\n")
           .append("PrintingDate         : ")
           .append(getPrintingDate())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
        ;
        return(str.toString());
    }
}

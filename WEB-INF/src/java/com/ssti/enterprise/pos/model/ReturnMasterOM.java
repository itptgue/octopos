package com.ssti.enterprise.pos.model;

import java.util.Date;

public interface ReturnMasterOM extends MultiCurrencyMasterOM
{
	public String getReturnId();	
	public String getReturnNo();
	public Date getReturnDate();
}

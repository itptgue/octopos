package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseOrderMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseOrderMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseOrderPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_order";
    }

  
    /**
     * purchase_order.PURCHASE_ORDER_ID
     * @return the column name for the PURCHASE_ORDER_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.PURCHASE_ORDER_ID constant
     */
    public static String getPurchaseOrder_PurchaseOrderId()
    {
        return "purchase_order.PURCHASE_ORDER_ID";
    }
  
    /**
     * purchase_order.PURCHASE_REQUEST_ID
     * @return the column name for the PURCHASE_REQUEST_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.PURCHASE_REQUEST_ID constant
     */
    public static String getPurchaseOrder_PurchaseRequestId()
    {
        return "purchase_order.PURCHASE_REQUEST_ID";
    }
  
    /**
     * purchase_order.TRANSACTION_STATUS
     * @return the column name for the TRANSACTION_STATUS field
     * @deprecated use PurchaseOrderPeer.purchase_order.TRANSACTION_STATUS constant
     */
    public static String getPurchaseOrder_TransactionStatus()
    {
        return "purchase_order.TRANSACTION_STATUS";
    }
  
    /**
     * purchase_order.PURCHASE_ORDER_NO
     * @return the column name for the PURCHASE_ORDER_NO field
     * @deprecated use PurchaseOrderPeer.purchase_order.PURCHASE_ORDER_NO constant
     */
    public static String getPurchaseOrder_PurchaseOrderNo()
    {
        return "purchase_order.PURCHASE_ORDER_NO";
    }
  
    /**
     * purchase_order.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.TRANSACTION_DATE constant
     */
    public static String getPurchaseOrder_TransactionDate()
    {
        return "purchase_order.TRANSACTION_DATE";
    }
  
    /**
     * purchase_order.DUE_DATE
     * @return the column name for the DUE_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.DUE_DATE constant
     */
    public static String getPurchaseOrder_DueDate()
    {
        return "purchase_order.DUE_DATE";
    }
  
    /**
     * purchase_order.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.VENDOR_ID constant
     */
    public static String getPurchaseOrder_VendorId()
    {
        return "purchase_order.VENDOR_ID";
    }
  
    /**
     * purchase_order.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use PurchaseOrderPeer.purchase_order.VENDOR_NAME constant
     */
    public static String getPurchaseOrder_VendorName()
    {
        return "purchase_order.VENDOR_NAME";
    }
  
    /**
     * purchase_order.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use PurchaseOrderPeer.purchase_order.TOTAL_QTY constant
     */
    public static String getPurchaseOrder_TotalQty()
    {
        return "purchase_order.TOTAL_QTY";
    }
  
    /**
     * purchase_order.TOTAL_AMOUNT
     * @return the column name for the TOTAL_AMOUNT field
     * @deprecated use PurchaseOrderPeer.purchase_order.TOTAL_AMOUNT constant
     */
    public static String getPurchaseOrder_TotalAmount()
    {
        return "purchase_order.TOTAL_AMOUNT";
    }
  
    /**
     * purchase_order.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use PurchaseOrderPeer.purchase_order.TOTAL_DISCOUNT_PCT constant
     */
    public static String getPurchaseOrder_TotalDiscountPct()
    {
        return "purchase_order.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * purchase_order.TOTAL_DISCOUNT
     * @return the column name for the TOTAL_DISCOUNT field
     * @deprecated use PurchaseOrderPeer.purchase_order.TOTAL_DISCOUNT constant
     */
    public static String getPurchaseOrder_TotalDiscount()
    {
        return "purchase_order.TOTAL_DISCOUNT";
    }
  
    /**
     * purchase_order.TOTAL_FEE
     * @return the column name for the TOTAL_FEE field
     * @deprecated use PurchaseOrderPeer.purchase_order.TOTAL_FEE constant
     */
    public static String getPurchaseOrder_TotalFee()
    {
        return "purchase_order.TOTAL_FEE";
    }
  
    /**
     * purchase_order.TOTAL_TAX
     * @return the column name for the TOTAL_TAX field
     * @deprecated use PurchaseOrderPeer.purchase_order.TOTAL_TAX constant
     */
    public static String getPurchaseOrder_TotalTax()
    {
        return "purchase_order.TOTAL_TAX";
    }
  
    /**
     * purchase_order.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use PurchaseOrderPeer.purchase_order.CREATE_BY constant
     */
    public static String getPurchaseOrder_CreateBy()
    {
        return "purchase_order.CREATE_BY";
    }
  
    /**
     * purchase_order.CONFIRM_BY
     * @return the column name for the CONFIRM_BY field
     * @deprecated use PurchaseOrderPeer.purchase_order.CONFIRM_BY constant
     */
    public static String getPurchaseOrder_ConfirmBy()
    {
        return "purchase_order.CONFIRM_BY";
    }
  
    /**
     * purchase_order.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PurchaseOrderPeer.purchase_order.REMARK constant
     */
    public static String getPurchaseOrder_Remark()
    {
        return "purchase_order.REMARK";
    }
  
    /**
     * purchase_order.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.PAYMENT_TYPE_ID constant
     */
    public static String getPurchaseOrder_PaymentTypeId()
    {
        return "purchase_order.PAYMENT_TYPE_ID";
    }
  
    /**
     * purchase_order.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.PAYMENT_TERM_ID constant
     */
    public static String getPurchaseOrder_PaymentTermId()
    {
        return "purchase_order.PAYMENT_TERM_ID";
    }
  
    /**
     * purchase_order.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.CURRENCY_ID constant
     */
    public static String getPurchaseOrder_CurrencyId()
    {
        return "purchase_order.CURRENCY_ID";
    }
  
    /**
     * purchase_order.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.CURRENCY_RATE constant
     */
    public static String getPurchaseOrder_CurrencyRate()
    {
        return "purchase_order.CURRENCY_RATE";
    }
  
    /**
     * purchase_order.CREATE_RECEIPT
     * @return the column name for the CREATE_RECEIPT field
     * @deprecated use PurchaseOrderPeer.purchase_order.CREATE_RECEIPT constant
     */
    public static String getPurchaseOrder_CreateReceipt()
    {
        return "purchase_order.CREATE_RECEIPT";
    }
  
    /**
     * purchase_order.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.LOCATION_ID constant
     */
    public static String getPurchaseOrder_LocationId()
    {
        return "purchase_order.LOCATION_ID";
    }
  
    /**
     * purchase_order.PRINT_TIMES
     * @return the column name for the PRINT_TIMES field
     * @deprecated use PurchaseOrderPeer.purchase_order.PRINT_TIMES constant
     */
    public static String getPurchaseOrder_PrintTimes()
    {
        return "purchase_order.PRINT_TIMES";
    }
  
    /**
     * purchase_order.SENT_STATUS
     * @return the column name for the SENT_STATUS field
     * @deprecated use PurchaseOrderPeer.purchase_order.SENT_STATUS constant
     */
    public static String getPurchaseOrder_SentStatus()
    {
        return "purchase_order.SENT_STATUS";
    }
  
    /**
     * purchase_order.DOWN_PAYMENT
     * @return the column name for the DOWN_PAYMENT field
     * @deprecated use PurchaseOrderPeer.purchase_order.DOWN_PAYMENT constant
     */
    public static String getPurchaseOrder_DownPayment()
    {
        return "purchase_order.DOWN_PAYMENT";
    }
  
    /**
     * purchase_order.BANK_ID
     * @return the column name for the BANK_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.BANK_ID constant
     */
    public static String getPurchaseOrder_BankId()
    {
        return "purchase_order.BANK_ID";
    }
  
    /**
     * purchase_order.BANK_ISSUER
     * @return the column name for the BANK_ISSUER field
     * @deprecated use PurchaseOrderPeer.purchase_order.BANK_ISSUER constant
     */
    public static String getPurchaseOrder_BankIssuer()
    {
        return "purchase_order.BANK_ISSUER";
    }
  
    /**
     * purchase_order.DP_ACCOUNT_ID
     * @return the column name for the DP_ACCOUNT_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.DP_ACCOUNT_ID constant
     */
    public static String getPurchaseOrder_DpAccountId()
    {
        return "purchase_order.DP_ACCOUNT_ID";
    }
  
    /**
     * purchase_order.DP_DUE_DATE
     * @return the column name for the DP_DUE_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.DP_DUE_DATE constant
     */
    public static String getPurchaseOrder_DpDueDate()
    {
        return "purchase_order.DP_DUE_DATE";
    }
  
    /**
     * purchase_order.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use PurchaseOrderPeer.purchase_order.REFERENCE_NO constant
     */
    public static String getPurchaseOrder_ReferenceNo()
    {
        return "purchase_order.REFERENCE_NO";
    }
  
    /**
     * purchase_order.CASH_FLOW_TYPE_ID
     * @return the column name for the CASH_FLOW_TYPE_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.CASH_FLOW_TYPE_ID constant
     */
    public static String getPurchaseOrder_CashFlowTypeId()
    {
        return "purchase_order.CASH_FLOW_TYPE_ID";
    }
  
    /**
     * purchase_order.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.COURIER_ID constant
     */
    public static String getPurchaseOrder_CourierId()
    {
        return "purchase_order.COURIER_ID";
    }
  
    /**
     * purchase_order.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use PurchaseOrderPeer.purchase_order.FOB_ID constant
     */
    public static String getPurchaseOrder_FobId()
    {
        return "purchase_order.FOB_ID";
    }
  
    /**
     * purchase_order.ESTIMATED_FREIGHT
     * @return the column name for the ESTIMATED_FREIGHT field
     * @deprecated use PurchaseOrderPeer.purchase_order.ESTIMATED_FREIGHT constant
     */
    public static String getPurchaseOrder_EstimatedFreight()
    {
        return "purchase_order.ESTIMATED_FREIGHT";
    }
  
    /**
     * purchase_order.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use PurchaseOrderPeer.purchase_order.IS_TAXABLE constant
     */
    public static String getPurchaseOrder_IsTaxable()
    {
        return "purchase_order.IS_TAXABLE";
    }
  
    /**
     * purchase_order.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use PurchaseOrderPeer.purchase_order.IS_INCLUSIVE_TAX constant
     */
    public static String getPurchaseOrder_IsInclusiveTax()
    {
        return "purchase_order.IS_INCLUSIVE_TAX";
    }
  
    /**
     * purchase_order.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.FISCAL_RATE constant
     */
    public static String getPurchaseOrder_FiscalRate()
    {
        return "purchase_order.FISCAL_RATE";
    }
  
    /**
     * purchase_order.CONFIRM_DATE
     * @return the column name for the CONFIRM_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.CONFIRM_DATE constant
     */
    public static String getPurchaseOrder_ConfirmDate()
    {
        return "purchase_order.CONFIRM_DATE";
    }
  
    /**
     * purchase_order.DELIVERY_DATE
     * @return the column name for the DELIVERY_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.DELIVERY_DATE constant
     */
    public static String getPurchaseOrder_DeliveryDate()
    {
        return "purchase_order.DELIVERY_DATE";
    }
  
    /**
     * purchase_order.SHIP_TO
     * @return the column name for the SHIP_TO field
     * @deprecated use PurchaseOrderPeer.purchase_order.SHIP_TO constant
     */
    public static String getPurchaseOrder_ShipTo()
    {
        return "purchase_order.SHIP_TO";
    }
  
    /**
     * purchase_order.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use PurchaseOrderPeer.purchase_order.CANCEL_BY constant
     */
    public static String getPurchaseOrder_CancelBy()
    {
        return "purchase_order.CANCEL_BY";
    }
  
    /**
     * purchase_order.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.CANCEL_DATE constant
     */
    public static String getPurchaseOrder_CancelDate()
    {
        return "purchase_order.CANCEL_DATE";
    }
  
    /**
     * purchase_order.CLOSED_DATE
     * @return the column name for the CLOSED_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.CLOSED_DATE constant
     */
    public static String getPurchaseOrder_ClosedDate()
    {
        return "purchase_order.CLOSED_DATE";
    }
  
    /**
     * purchase_order.EXPIRED_DATE
     * @return the column name for the EXPIRED_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.EXPIRED_DATE constant
     */
    public static String getPurchaseOrder_ExpiredDate()
    {
        return "purchase_order.EXPIRED_DATE";
    }
  
    /**
     * purchase_order.RECEIPT_DATE
     * @return the column name for the RECEIPT_DATE field
     * @deprecated use PurchaseOrderPeer.purchase_order.RECEIPT_DATE constant
     */
    public static String getPurchaseOrder_ReceiptDate()
    {
        return "purchase_order.RECEIPT_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_order");
        TableMap tMap = dbMap.getTable("purchase_order");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_order.PURCHASE_ORDER_ID", "");
                          tMap.addColumn("purchase_order.PURCHASE_REQUEST_ID", "");
                            tMap.addColumn("purchase_order.TRANSACTION_STATUS", Integer.valueOf(0));
                          tMap.addColumn("purchase_order.PURCHASE_ORDER_NO", "");
                          tMap.addColumn("purchase_order.TRANSACTION_DATE", new Date());
                          tMap.addColumn("purchase_order.DUE_DATE", new Date());
                          tMap.addColumn("purchase_order.VENDOR_ID", "");
                          tMap.addColumn("purchase_order.VENDOR_NAME", "");
                            tMap.addColumn("purchase_order.TOTAL_QTY", bd_ZERO);
                            tMap.addColumn("purchase_order.TOTAL_AMOUNT", bd_ZERO);
                          tMap.addColumn("purchase_order.TOTAL_DISCOUNT_PCT", "");
                            tMap.addColumn("purchase_order.TOTAL_DISCOUNT", bd_ZERO);
                            tMap.addColumn("purchase_order.TOTAL_FEE", bd_ZERO);
                            tMap.addColumn("purchase_order.TOTAL_TAX", bd_ZERO);
                          tMap.addColumn("purchase_order.CREATE_BY", "");
                          tMap.addColumn("purchase_order.CONFIRM_BY", "");
                          tMap.addColumn("purchase_order.REMARK", "");
                          tMap.addColumn("purchase_order.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("purchase_order.PAYMENT_TERM_ID", "");
                          tMap.addColumn("purchase_order.CURRENCY_ID", "");
                            tMap.addColumn("purchase_order.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("purchase_order.CREATE_RECEIPT", Boolean.TRUE);
                          tMap.addColumn("purchase_order.LOCATION_ID", "");
                            tMap.addColumn("purchase_order.PRINT_TIMES", Integer.valueOf(0));
                            tMap.addColumn("purchase_order.SENT_STATUS", Integer.valueOf(0));
                            tMap.addColumn("purchase_order.DOWN_PAYMENT", bd_ZERO);
                          tMap.addColumn("purchase_order.BANK_ID", "");
                          tMap.addColumn("purchase_order.BANK_ISSUER", "");
                          tMap.addColumn("purchase_order.DP_ACCOUNT_ID", "");
                          tMap.addColumn("purchase_order.DP_DUE_DATE", new Date());
                          tMap.addColumn("purchase_order.REFERENCE_NO", "");
                          tMap.addColumn("purchase_order.CASH_FLOW_TYPE_ID", "");
                          tMap.addColumn("purchase_order.COURIER_ID", "");
                          tMap.addColumn("purchase_order.FOB_ID", "");
                            tMap.addColumn("purchase_order.ESTIMATED_FREIGHT", bd_ZERO);
                          tMap.addColumn("purchase_order.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("purchase_order.IS_INCLUSIVE_TAX", Boolean.TRUE);
                            tMap.addColumn("purchase_order.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("purchase_order.CONFIRM_DATE", new Date());
                          tMap.addColumn("purchase_order.DELIVERY_DATE", new Date());
                          tMap.addColumn("purchase_order.SHIP_TO", "");
                          tMap.addColumn("purchase_order.CANCEL_BY", "");
                          tMap.addColumn("purchase_order.CANCEL_DATE", new Date());
                          tMap.addColumn("purchase_order.CLOSED_DATE", new Date());
                          tMap.addColumn("purchase_order.EXPIRED_DATE", new Date());
                          tMap.addColumn("purchase_order.RECEIPT_DATE", new Date());
          }
}

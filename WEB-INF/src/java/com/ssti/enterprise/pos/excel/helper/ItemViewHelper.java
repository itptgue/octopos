package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.CustomField;
import com.ssti.enterprise.pos.om.InventoryLocation;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.tools.CustomFieldTool;
import com.ssti.enterprise.pos.tools.ItemFieldTool;
import com.ssti.enterprise.pos.tools.ItemStatusCodeTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.inventory.InventoryLocationTool;
import com.ssti.enterprise.pos.tools.inventory.ItemInventoryTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.sales.SalesOrderTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2016-10-29
 * - Change item view helper include custom field if ticked
 * 
 * </pre><br>
 */
public class ItemViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        String[] sF1  = (String[]) oParam.get("f1");  boolean bF1  = Boolean.valueOf (sF1 [0]).booleanValue();
        String[] sF2  = (String[]) oParam.get("f2");  boolean bF2  = Boolean.valueOf (sF2 [0]).booleanValue();
        String[] sF3  = (String[]) oParam.get("f3");  boolean bF3  = Boolean.valueOf (sF3 [0]).booleanValue();
        String[] sF4  = (String[]) oParam.get("f4");  boolean bF4  = Boolean.valueOf (sF4 [0]).booleanValue();
        String[] sF5  = (String[]) oParam.get("f5");  boolean bF5  = Boolean.valueOf (sF5 [0]).booleanValue();
        String[] sF6  = (String[]) oParam.get("f6");  boolean bF6  = Boolean.valueOf (sF6 [0]).booleanValue();
        String[] sF7  = (String[]) oParam.get("f7");  boolean bF7  = Boolean.valueOf (sF7 [0]).booleanValue();
        String[] sF8  = (String[]) oParam.get("f8");  boolean bF8  = Boolean.valueOf (sF8 [0]).booleanValue();
        String[] sF9  = (String[]) oParam.get("f9");  boolean bF9  = Boolean.valueOf (sF9 [0]).booleanValue();
        String[] sF10 = (String[]) oParam.get("f10"); boolean bF10 = Boolean.valueOf (sF10[0]).booleanValue();
        String[] sF11 = (String[]) oParam.get("f11"); boolean bF11 = Boolean.valueOf (sF11[0]).booleanValue();
        String[] sF12 = (String[]) oParam.get("f12"); boolean bF12 = Boolean.valueOf (sF12[0]).booleanValue();
        String[] sF13 = (String[]) oParam.get("f13"); boolean bF13 = Boolean.valueOf (sF13[0]).booleanValue();
        String[] sF14 = (String[]) oParam.get("f14"); boolean bF14 = Boolean.valueOf (sF14[0]).booleanValue();
        String[] sF15 = (String[]) oParam.get("f15"); boolean bF15 = Boolean.valueOf (sF15[0]).booleanValue();
        String[] sF16 = (String[]) oParam.get("f16"); boolean bF16 = Boolean.valueOf (sF16[0]).booleanValue();
        String[] sF17 = (String[]) oParam.get("f17"); boolean bF17 = Boolean.valueOf (sF17[0]).booleanValue();
        String[] sF18 = (String[]) oParam.get("f18"); boolean bF18 = Boolean.valueOf (sF18[0]).booleanValue();
        String[] sF19 = (String[]) oParam.get("f19"); boolean bF19 = Boolean.valueOf (sF19[0]).booleanValue();
        String[] sF20 = (String[]) oParam.get("f20"); boolean bF20 = Boolean.valueOf (sF20[0]).booleanValue();
        String[] sF21 = (String[]) oParam.get("f21"); boolean bF21 = Boolean.valueOf (sF21[0]).booleanValue();
        String[] sF22 = (String[]) oParam.get("f22"); boolean bF22 = Boolean.valueOf (sF22[0]).booleanValue();
        String[] sF23 = (String[]) oParam.get("f23"); boolean bF23 = Boolean.valueOf (sF23[0]).booleanValue();
        String[] sF24 = (String[]) oParam.get("f24"); boolean bF24 = Boolean.valueOf (sF24[0]).booleanValue();
        String[] sF25 = (String[]) oParam.get("f25"); boolean bF25 = Boolean.valueOf (sF25[0]).booleanValue();
        String[] sF26 = (String[]) oParam.get("f26"); boolean bF26 = Boolean.valueOf (sF26[0]).booleanValue();
        String[] sF27 = (String[]) oParam.get("f27"); boolean bF27 = Boolean.valueOf (sF27[0]).booleanValue();
        String[] sF28 = (String[]) oParam.get("f28"); boolean bF28 = Boolean.valueOf (sF28[0]).booleanValue();
        String[] sF29 = (String[]) oParam.get("f29"); boolean bF29 = Boolean.valueOf (sF29[0]).booleanValue();
        String[] sF30 = (String[]) oParam.get("f30"); boolean bF30 = Boolean.valueOf (sF30[0]).booleanValue();
        String[] sF31 = (String[]) oParam.get("f31"); boolean bF31 = Boolean.valueOf (sF31[0]).booleanValue();
        String[] sF32 = (String[]) oParam.get("f32"); boolean bF32 = Boolean.valueOf (sF32[0]).booleanValue();
        String[] sF33 = (String[]) oParam.get("f33"); boolean bF33 = Boolean.valueOf (sF33[0]).booleanValue();
        String[] sF34 = (String[]) oParam.get("f34"); boolean bF34 = Boolean.valueOf (sF34[0]).booleanValue();
        String[] sF35 = (String[]) oParam.get("f35"); boolean bF35 = Boolean.valueOf (sF35[0]).booleanValue();
        
        List vData = (List) _oSession.getAttribute("Items");

        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Item View");

        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        if (bF1 ) { createHeaderCell(oWB, oRow, (short)iCol, "Item Code"); iCol++;}       
        if (bF21) { createHeaderCell(oWB, oRow, (short)iCol, "Barcode"); iCol++;}       
        if (bF2 ) { createHeaderCell(oWB, oRow, (short)iCol, "Item Name"); iCol++;}       
        if (bF3 ) { createHeaderCell(oWB, oRow, (short)iCol, "Description"); iCol++;}       

        if (bF25) { createHeaderCell(oWB, oRow, (short)iCol, "Color"); iCol++;}       
        if (bF26) { createHeaderCell(oWB, oRow, (short)iCol, "Size"); iCol++;}       
        if (bF23) { createHeaderCell(oWB, oRow, (short)iCol, "Kategori"); iCol++;}       

        if (bF4 ) { createHeaderCell(oWB, oRow, (short)iCol, "Item Type"); iCol++;}       
        if (bF5 ) { createHeaderCell(oWB, oRow, (short)iCol, "Item Price"); iCol++;} 
        if (bF35) { createHeaderCell(oWB, oRow, (short)iCol, "Price List"); iCol++;} 
        
        if (bF7 ) { createHeaderCell(oWB, oRow, (short)iCol, "Last Purc."); iCol++;}       
        if (bF6 ) { createHeaderCell(oWB, oRow, (short)iCol, "Item Cost"); iCol++;}       
        if (bF8 ) { createHeaderCell(oWB, oRow, (short)iCol, "QoH"); iCol++;}       
        
        if (bF10) { createHeaderCell(oWB, oRow, (short)iCol, "Min."); iCol++;}
        if (bF11) { createHeaderCell(oWB, oRow, (short)iCol, "Reord."); iCol++;}       
        if (bF12) { createHeaderCell(oWB, oRow, (short)iCol, "Max."); iCol++;}      

        if (bF27) { createHeaderCell(oWB, oRow, (short)iCol, "On Order"); iCol++;}       
        if (bF28) { createHeaderCell(oWB, oRow, (short)iCol, "On Sales"); iCol++;}      

        if (bF9)  { createHeaderCell(oWB, oRow, (short)iCol, "Unit Code"); iCol++;}               
        if (bF22) { createHeaderCell(oWB, oRow, (short)iCol, "Purc. Unit Code"); iCol++;}   
        if (bF34) { createHeaderCell(oWB, oRow, (short)iCol, "Unit Conversion"); iCol++;}           

        if (bF13) { createHeaderCell(oWB, oRow, (short)iCol, "Margin Setup"); iCol++;}       
        if (bF14) { createHeaderCell(oWB, oRow, (short)iCol, "Curr. Margin"); iCol++;}       
        if (bF15) { createHeaderCell(oWB, oRow, (short)iCol, "Pref. Vendor"); iCol++;}       
        if (bF16) { createHeaderCell(oWB, oRow, (short)iCol, "Sales Tax"); iCol++;}       
        if (bF17) { createHeaderCell(oWB, oRow, (short)iCol, "Purchase Tax"); iCol++;}       
        if (bF18) { createHeaderCell(oWB, oRow, (short)iCol, "Status Code"); iCol++;}       
        if (bF19) { createHeaderCell(oWB, oRow, (short)iCol, "Default Warehouse"); iCol++;}      
        if (bF29) { createHeaderCell(oWB, oRow, (short)iCol, "Rack Location"); iCol++;}              
        if (bF20) { createHeaderCell(oWB, oRow, (short)iCol, "Delivery Type"); iCol++;}       
        if (bF24) { createHeaderCell(oWB, oRow, (short)iCol, "Last Update"); iCol++;}       
        if (bF30) { createHeaderCell(oWB, oRow, (short)iCol, "Last In"); iCol++;}       
        if (bF31) { createHeaderCell(oWB, oRow, (short)iCol, "Last Out"); iCol++;}       
        if (bF32) { createHeaderCell(oWB, oRow, (short)iCol, "Manufacturer"); iCol++;}       
        if (bF33) { createHeaderCell(oWB, oRow, (short)iCol, "Avg Sales"); iCol++;}       
        

		//CUSTOM FIELD HEADER
		List vCField = CustomFieldTool.getAllCustomField();
		Iterator oIter = vCField.iterator();
		while (oIter.hasNext())
		{
			CustomField oCField = (CustomField) oIter.next();
			if(oParam.get(oCField.getFieldName()) != null)
			{
				String[] sCF = (String[]) oParam.get(oCField.getFieldName()); 			
				boolean bCF = Boolean.valueOf (sCF[0]).booleanValue();
				if(bCF)
				{
					createHeaderCell(oWB, oRow, (short)iCol,  oCField.getFieldName()); iCol++;
				}
			}
		}
        
        String sLocID = StringUtil.getString(oParam, "LocationId");
        if (StringUtil.isEmpty(sLocID)) sLocID = PreferenceTool.getLocationID();
        
        if (vData != null)
        {            
            for (int i = 0; i < vData.size(); i++)
            {
                Item oItem = (Item) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                InventoryLocation oInvLoc = 
                	InventoryLocationTool.getDataByItemAndLocationID (oItem.getItemId(), sLocID);

                BigDecimal dCurrentQty    = bd_ZERO;
                BigDecimal dItemCost      = bd_ZERO;
				BigDecimal dCurrentMargin = bd_ZERO;

				BigDecimal dMin = oItem.getMinimumStock();
				BigDecimal dRP = oItem.getReorderPoint();
				BigDecimal dMax = oItem.getMaximumStock();
				String sRack = oItem.getRackLocation();
				
		        if (bF10 || bF11 || bF12)
		        {
		            ItemInventory oItemInv = 
		            	ItemInventoryTool.getItemInventory(oItem.getItemId(), sLocID);       
					
		            if (oItemInv != null)
					{
			            dMin = oItemInv.getMinimumQty();
						dRP = oItemInv.getReorderPoint();
						dMax = oItemInv.getMaximumQty();	
						sRack = oItemInv.getRackLocation();
					}
		        }   
		        Date dLastIn = null;
		        Date dLastOut = null;
		        
                if (oInvLoc != null)
                {
					dCurrentQty = oInvLoc.getCurrentQty();
					dItemCost = oInvLoc.getItemCost();
					dCurrentMargin = bd_ZERO;
					if(dItemCost != null && dItemCost.doubleValue() > 0)
					{
						dCurrentMargin = new BigDecimal(
							ItemTool.countMargin(oItem.getItemPrice(), dItemCost)
						);
					}
					dLastIn = oInvLoc.getLastIn();
					dLastOut = oInvLoc.getLastIn();
                }

                String sDeliveryType = "Normal";
                if(oItem.getDeliveryType() == 1)
            	{
                	sDeliveryType = "Direct";
            	}
                
                iCol = 0;
                if (bF1 ) { createCell(oWB, oRow2, (short)iCol, oItem.getItemCode());                                      iCol++; }
                if (bF21) { createCell(oWB, oRow2, (short)iCol, oItem.getBarcode());									   iCol++; }
                if (bF2 ) { createCell(oWB, oRow2, (short)iCol, oItem.getItemName());                                      iCol++; }
                if (bF3 ) { createCell(oWB, oRow2, (short)iCol, oItem.getDescription());                                   iCol++; }

                if (bF25) { createCell(oWB, oRow2, (short)iCol, oItem.getColor());                                     	   iCol++; }
                if (bF26) { createCell(oWB, oRow2, (short)iCol, oItem.getSize());                                     	   iCol++; }
                if (bF23) { createCell(oWB, oRow2, (short)iCol, KategoriTool.getLongDescription(oItem.getKategoriId()));   iCol++; }

                if (bF4 ) { createCell(oWB, oRow2, (short)iCol, ItemTool.getItemType(oItem.getItemType ()));                 iCol++; }
                if (bF5 ) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oItem.getItemPrice()));         iCol++; }
                if (bF35) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oItem.getPrice(sLocID, "")));   iCol++; }
                if (bF7 ) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oItem.getLastPurchasePrice())); iCol++; }     
                if (bF6 ) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(dItemCost));                    iCol++; }
                if (bF8 ) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(dCurrentQty));                  iCol++; }
                
                if (bF10) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oItem.getMinimumStock  ()));    iCol++; }  
                if (bF11) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oItem.getReorderPoint  ()));    iCol++; }  
                if (bF12) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(oItem.getMaximumStock  ()));    iCol++; } 

                if (bF27) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(PurchaseOrderTool.getOnOrderQty(oItem.getItemId())));  iCol++; }
                if (bF28) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(SalesOrderTool.getOnOrderQty(oItem.getItemId())));     iCol++; }

                if (bF9)  { createCell(oWB, oRow2, (short)iCol, UnitTool.getCodeByID(oItem.getUnitId()));                    iCol++; }
                if (bF22) { createCell(oWB, oRow2, (short)iCol, UnitTool.getCodeByID(oItem.getPurchaseUnitId()));            iCol++; }
                if (bF34) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.fmt(oItem.getUnitConversion()));             iCol++; }
                
                if (bF13) { createCell(oWB, oRow2, (short)iCol, oItem.getProfitMargin ());                                   iCol++; }
                if (bF14) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatNumber(dCurrentMargin));               iCol++; }
                if (bF15) { createCell(oWB, oRow2, (short)iCol, VendorTool.getVendorNameByID(oItem.getPreferedVendorId()));  iCol++; }  
                if (bF16) { createCell(oWB, oRow2, (short)iCol, TaxTool.getCodeByID(oItem.getTaxId()));					      iCol++; }
                if (bF17) { createCell(oWB, oRow2, (short)iCol, TaxTool.getCodeByID(oItem.getPurchaseTaxId()));			      iCol++; }  
                if (bF18) { createCell(oWB, oRow2, (short)iCol, ItemStatusCodeTool.getCodeByID(oItem.getItemStatusCodeId())); iCol++; }  
                if (bF19) { createCell(oWB, oRow2, (short)iCol, LocationTool.getLocationCodeByID(oItem.getWarehouseId()));    iCol++; }
                if (bF29) { createCell(oWB, oRow2, (short)iCol, sRack);                                    				    iCol++; }                
                if (bF20) { createCell(oWB, oRow2, (short)iCol, sDeliveryType);  											iCol++; }
                if (bF24) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.formatDateTime(oItem.getUpdateDate())); 	iCol++; }
                if (bF30) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.fmt(dLastIn));               				iCol++; }
                if (bF31) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.fmt(dLastOut));               				iCol++; }
                if (bF32) { createCell(oWB, oRow2, (short)iCol, oItem.getManufacturer());               	   				iCol++; }
                if (bF33) { createCell(oWB, oRow2, (short)iCol, CustomFormatter.fmt(oItem.getAvgSales(sLocID, 14)));        iCol++; }
                
                //CUSTOM FIELD VALUE
        		oIter = vCField.iterator();
        		while (oIter.hasNext())
        		{        			
        			CustomField oCField = (CustomField) oIter.next();
        			if(oParam.get(oCField.getFieldName()) != null)
        			{
	        			String[] sCF = (String[]) oParam.get(oCField.getFieldName()); 
	        			boolean bCF = Boolean.valueOf (sCF[0]).booleanValue();
	        			if(bCF)
	        			{
	        				createCell(oWB, oRow2, (short)iCol, 
	        						ItemFieldTool.getValue(oItem.getItemId(), oCField.getFieldName())); iCol++;
	        			}
        			}
        		}
            }        
        }
        return oWB;
    }
}

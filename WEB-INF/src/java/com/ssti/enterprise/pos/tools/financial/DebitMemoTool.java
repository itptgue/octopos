package com.ssti.enterprise.pos.tools.financial;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.NestableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.LargeSelect;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.ApPaymentDetail;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.om.DebitMemoPeer;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseReturn;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LastNumberTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.journal.DebitMemoJournalTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DebitMemoTool.java,v 1.26 2009/05/04 02:04:13 albert Exp $ <br>
 *
 * <pre>
 * $Log: DebitMemoTool.java,v $
 * 
 * 2017-10-19
 * -add PiRebate related methods
 * 
 * 2016-02-01
 * -add method getBySourceTransID to get Memo By Source Transaction 
 * </pre><br>
 */
public class DebitMemoTool extends BaseTool
{
    private static Log log = LogFactory.getLog(DebitMemoTool.class);

    protected static Map m_FIND_PEER = new HashMap();
	
    static
	{   
		m_FIND_PEER.put (Integer.valueOf(i_TRANS_NO   ), DebitMemoPeer.DEBIT_MEMO_NO);      	  
		m_FIND_PEER.put (Integer.valueOf(i_DESCRIPTION), DebitMemoPeer.REMARK);          
		m_FIND_PEER.put (Integer.valueOf(i_VENDOR	  ), DebitMemoPeer.VENDOR_ID);
		m_FIND_PEER.put (Integer.valueOf(i_CREATE_BY  ), DebitMemoPeer.USER_NAME);                 
		m_FIND_PEER.put (Integer.valueOf(i_REF_NO     ), DebitMemoPeer.REFERENCE_NO);             	
		m_FIND_PEER.put (Integer.valueOf(i_CURRENCY   ), DebitMemoPeer.CURRENCY_ID);
        m_FIND_PEER.put (Integer.valueOf(i_BANK   	  ), DebitMemoPeer.BANK_ID);	
        m_FIND_PEER.put (Integer.valueOf(i_CFT		  ), DebitMemoPeer.CASH_FLOW_TYPE_ID);
        m_FIND_PEER.put (Integer.valueOf(i_PMT_NO	  ), DebitMemoPeer.PAYMENT_TRANS_NO);
	}   
	
	public static String conditionSB (int _iSelected) {return conditionSB(m_FIND_PEER, _iSelected);}    
    
	public static List getAllDebitMemo()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return DebitMemoPeer.doSelect(oCrit);
	}

	public static DebitMemo getDebitMemoByID(String _sID)
		throws Exception
	{
		return getDebitMemoByID(_sID, null);
	}
	
	public static DebitMemo getDebitMemoByID(String _sID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DebitMemoPeer.DEBIT_MEMO_ID, _sID);
        List vData = DebitMemoPeer.doSelect(oCrit, _oConn);
        if (vData.size() > 0) {
			return (DebitMemo) vData.get(0);
		}
		return null;
	}
	
	public static DebitMemo getDebitMemoByTransactionID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(DebitMemoPeer.TRANSACTION_ID, _sID);
        List vData = DebitMemoPeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (DebitMemo) vData.get(0);
		}
		return null;
	}

	public static void createFromPODownPayment (PurchaseOrder _oPO, Connection _oConn)
		throws Exception
	{
		try
		{
			double dAmountBase = _oPO.getCurrencyRate().doubleValue() * _oPO.getDownPayment().doubleValue();
			Date dToday = new Date();
			Date dDueDate = new Date();
			
			DebitMemo oDM = new DebitMemo();
			oDM.setTransactionDate  (_oPO.getTransactionDate());
			oDM.setTransactionId    (_oPO.getPurchaseOrderId());
			oDM.setTransactionNo    (_oPO.getPurchaseOrderNo());
			oDM.setTransactionType  (i_AP_TRANS_PO_DOWNPAYMENT);				
			oDM.setVendorId         (_oPO.getVendorId());
			oDM.setVendorName       (_oPO.getVendorName());
			oDM.setUserName         (_oPO.getConfirmBy());
			oDM.setCurrencyId       (_oPO.getCurrencyId());
			oDM.setCurrencyRate     (_oPO.getCurrencyRate());
			oDM.setAmount           (_oPO.getDownPayment());
			oDM.setAmountBase       (new BigDecimal(dAmountBase));			
			oDM.setBankId           (_oPO.getBankId());
			oDM.setBankIssuer       (_oPO.getBankIssuer());
			oDM.setReferenceNo      (_oPO.getReferenceNo());
			oDM.setDueDate          (_oPO.getDpDueDate());
			oDM.setAccountId        (_oPO.getDpAccountId());
			oDM.setCashFlowTypeId	(_oPO.getCashFlowTypeId());
			
			StringBuilder oRemark = new StringBuilder();
			oRemark.append(_oPO.getRemark());
			oRemark.append("\n DP ").append(LocaleTool.getString("purchase_order")).append(" ");
			oRemark.append(_oPO.getPurchaseOrderNo());
			oRemark.append("\n").append(_oPO.getRemark());
			oDM.setRemark (oRemark.toString());
			
			dToday = DateUtil.getEndOfDayDate(dToday);
			dDueDate= DateUtil.getStartOfDayDate(oDM.getDueDate());
			
			/* will be closed when used by payable payment
			if(dDueDate.before(dToday))
			{
			   oDM.setStatus(b_DM_CLOSED);
			} */
			saveDM (oDM, _oConn);
		}
		catch (Exception _oEx)
		{
		    log.error (_oEx);
			throw new NestableException (_oEx.getMessage(),_oEx);
		}
	}

	public static void createFromPurchaseReturn (PurchaseReturn _oPR, Connection _oConn)
		throws Exception
	{
		try
		{
			double dAmountBase = _oPR.getCurrencyRate().doubleValue() * _oPR.getTotalAmount().doubleValue();
		    
			DebitMemo oDM = new DebitMemo();
			oDM.setTransactionDate 	(_oPR.getReturnDate());
			oDM.setTransactionId 	(_oPR.getPurchaseReturnId());
			oDM.setTransactionNo    (_oPR.getReturnNo());
			oDM.setTransactionType  (i_AP_TRANS_PURCHASE_RETURN);	
			oDM.setLocationId		(_oPR.getLocationId());
			oDM.setVendorId 		(_oPR.getVendorId());
			oDM.setVendorName 		(_oPR.getVendorName());
			oDM.setUserName 		(_oPR.getUserName());
			oDM.setCurrencyId 		(_oPR.getCurrencyId());       
			oDM.setCurrencyRate 	(_oPR.getCurrencyRate());                                                            
			oDM.setAmount 			(_oPR.getTotalAmount());                                                                   
			oDM.setAmountBase 		(new BigDecimal(dAmountBase));                                                         

			StringBuilder oRemark = new StringBuilder();
			oRemark.append(LocaleTool.getString("purchase_return"))
				   .append(" ")
				   .append(_oPR.getReturnNo())
				   .append("\n")
				   .append(_oPR.getRemark());
			
			if (_oPR.getTransactionType() != i_RET_FROM_NONE)
			{
				oRemark.append(" ").append(LocaleTool.getString("from"))
					   .append(" ").append(_oPR.getTransactionNo());
			}
			
			if (_oPR.getCashReturn() && StringUtil.isNotEmpty(_oPR.getBankId()))
			{				 
				oDM.setBankId         (_oPR.getBankId());
				oDM.setBankIssuer     (_oPR.getBankIssuer());
				oDM.setReferenceNo    (_oPR.getReferenceNo());
				oDM.setDueDate 	      (_oPR.getDueDate());
				oDM.setCashFlowTypeId (_oPR.getCashFlowTypeId());
				
				//close memo, if not it will still be usable in payable payment
				oDM.setStatus(i_MEMO_CLOSED);
			}
			else
			{
				oDM.setStatus(i_MEMO_OPEN);
				oDM.setReferenceNo ("Purchase Return Debit Memo");
				oDM.setDueDate     (new Date());			
			}
			Account oAP = AccountTool.getAccountPayable(_oPR.getCurrencyId(), _oPR.getVendorId(), _oConn);
			
			oDM.setAccountId (oAP.getAccountId());
			oDM.setRemark 	 (oRemark.toString());		
			saveDM (oDM, _oPR.getCashReturn(), _oConn);
		}
		catch (Exception _oEx)
		{
		    log.error (_oEx);
			throw new NestableException (_oEx.getMessage(),_oEx);
		}
	}

	public static void cancelFromPurchaseReturn (PurchaseReturn _oPR, String _sCancelBy, Connection _oConn)
		throws Exception
	{
		try
		{
			Criteria oCrit = new Criteria();
			oCrit.add(DebitMemoPeer.TRANSACTION_ID, _oPR.getPurchaseReturnId());
			List vDM = DebitMemoPeer.doSelect(oCrit, _oConn);
			if (vDM.size() > 0)
			{
				DebitMemo oDM = (DebitMemo) vDM.get(0);
				if (oDM.getStatus() == i_MEMO_CLOSED && !_oPR.getCashReturn())
				{
					throw new Exception (
					  "Debit Memo " + oDM.getDebitMemoNo() + 
						" already used in Payment No " + oDM.getPaymentTransNo());
				}
				cancelDM (oDM, _sCancelBy, _oConn);
			}
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}	

	public static boolean isMemoUsed (String _sTransID, Connection _oConn)
		throws Exception
	{
		try
		{
			Criteria oCrit = new Criteria();
			oCrit.add(DebitMemoPeer.TRANSACTION_ID, _sTransID);
			List vDM = DebitMemoPeer.doSelect(oCrit, _oConn);
			if (vDM.size() > 0)
			{
				DebitMemo oDM = (DebitMemo) vDM.get(0);
				if (oDM.getStatus() == i_MEMO_CLOSED)
				{
					return true;
				}
			}
			return false;
		}
		catch (Exception _oEx)
		{
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(), _oEx);
		}
	}		

	public static void saveDM (DebitMemo _oDM, Connection _oConn)
		throws Exception
	{
		saveDM (_oDM, false, _oConn);
	}	

    private static void validateTrans (DebitMemo _oDM, Connection _oConn)
		throws Exception
	{
		//validate currency
    	if (StringUtil.isNotEmpty(_oDM.getBankId()))
    	{			
    		Vendor oVendor = VendorTool.getVendorByID(_oDM.getVendorId(), _oConn);
    		Bank oBank = BankTool.getBankByID(_oDM.getBankId(), _oConn);			
			Currency oBankCurr = CurrencyTool.getCurrencyByID(oBank.getCurrencyId(), _oConn);			
			if (!oBankCurr.getIsDefault()) 
			{
				if (!StringUtil.isEqual(oBankCurr.getCurrencyId(),oVendor.getDefaultCurrencyId()))
				{
					throw new NestableException ("Invalid Cash/Bank Selection");
				} 
			}
    	}
	}
	
	/**
	 * Save DM
	 * 
	 * @param _oDM
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveDM (DebitMemo _oDM, boolean _bCashReturn, Connection _oConn)
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null) 
	    {
		    _oConn = beginTrans ();
		    bStartTrans = true;
		}
		try
		{
			//validate date
			validateDate(_oDM.getTransactionDate(), _oConn);
			
			//validate trans currency
			validateTrans(_oDM,_oConn);
			
			//Generate Sys ID
			if (StringUtil.isEmpty(_oDM.getDebitMemoId()))
			{
				_oDM.setDebitMemoId (IDGenerator.generateSysID());						
				validateID(getDebitMemoByID(_oDM.getMemoId(),_oConn), "memoNo");
			}
			
			//SET Status
			if (_oDM.getStatus() == 0) _oDM.setStatus(i_MEMO_OPEN);
			
			if (_oDM.getStatus() == i_MEMO_OPEN || _oDM.getStatus() == i_MEMO_CLOSED)
			{
				String sLocCode = LocationTool.getLocationCodeByID(_oDM.getLocationId(), _oConn);
				_oDM.setDebitMemoNo (LastNumberTool.get(s_DM_FORMAT, LastNumberTool.i_DEBIT_MEMO, sLocCode, _oConn));
				validateNo(DebitMemoPeer.DEBIT_MEMO_NO,_oDM.getDebitMemoNo(),DebitMemoPeer.class, _oConn);
				
				_oDM.save ( _oConn );
				//createAPEntryFromDebitMemo
				if (!_bCashReturn)
				{
				    AccountPayableTool.createAPEntryFromDebitMemo(_oDM, _oConn);
				}

				if (_oDM.getTransactionType() == i_AR_TRANS_SO_DOWNPAYMENT && StringUtil.isNotEmpty(_oDM.getTransactionId()))
				{
					//update SO DownPayment Amount
					PurchaseOrder oORD = PurchaseOrderTool.getHeaderByID(_oDM.getTransactionId(), _oConn);
					oORD.setDownPayment(_oDM.getAmount());
					oORD.save(_oConn);
				}
				
			    //if no bank ID (i.e from NON-CASH Sales Return means that it is just a crossable memo)
			    //if from PO DP or HO CASH Purchase Return then Bank ID must not be empty
				if (StringUtil.isNotEmpty(_oDM.getBankId()))
			    {
			    	//createCashFlowEntryFromCreditMemo
			    	CashFlowTool.createFromDebitMemo (_oDM, _bCashReturn, _oConn);
			    }			
			}
			else
			{
				_oDM.save ( _oConn );
			}
							
            if (bStartTrans)
            {
			    //commit transaction
			    commit (_oConn);
		    }
		}
		catch (Exception _oEx)
		{
			if (bStartTrans)
            {
			    rollback (_oConn);
			}
			log.error (_oEx);
		    _oEx.printStackTrace();
			throw new NestableException (_oEx.getMessage(),_oEx);
		}
	}
	
	/**
	 * Cancel existing CM
	 * 
	 * @param _sDMID CreditMemoID
	 * @param _sUserName 
	 * @param _oConn
	 * @throws Exception
	 */
	public static void cancelDM (DebitMemo _oDM, String _sCancelBy, Connection _oConn)
		throws Exception
	{
	    boolean bStartTrans = false;
	    if (_oConn == null) 
	    {
		    _oConn = beginTrans();
		    bStartTrans = true;
		}
		try
		{
			if (_oDM != null)
			{
				//validate date
				validateDate(_oDM.getTransactionDate(), _oConn);
				
				//rollback AR
				AccountPayableTool.rollbackAP(_oDM.getDebitMemoId(), _oConn);
					
				//if cash memo
				if (StringUtil.isNotEmpty(_oDM.getBankId()))
				{
				    //cancel CASH FLOW
				    CashFlowTool.cancelCashFlowByTransID(_oDM.getDebitMemoId(), _sCancelBy, _oConn);
				}
				
				_oDM.setRemark(cancelledBy(_oDM.getRemark(), _sCancelBy) );			
				_oDM.setStatus(i_MEMO_CANCELLED);
				_oDM.save(_oConn);
			}
			
            if (bStartTrans)
		    {
				//commit transaction
			    commit (_oConn);
            }
		}
		catch (Exception _oEx)
		{
			if (bStartTrans)
	        {
			    rollback (_oConn);
			}
			log.error (_oEx);
			throw new NestableException (_oEx.getMessage(),_oEx);
		}
	}
	
	/**
	 * 
	 * @param _sVendorID
	 * @param _iStatus
	 * @return List of DM
	 * @throws Exception
	 */
	public static List getDMByVendorAndStatus(String _sVendorID, int _iStatus)
		throws Exception
	{
	    return getDMByVendorAndStatus(_sVendorID, _iStatus , "");
    }
    
	/**
	 * 
	 * @param _sVendorID
	 * @param _iStatus
	 * @param _sCurrencyID
	 * @return List of DM
	 * @throws Exception
	 */
	public static List getDMByVendorAndStatus(String _sVendorID, int _iStatus, String _sCurrencyID)
		throws Exception
	{
		
		return getMemoByEntityStatus(_sVendorID, _iStatus, _sCurrencyID, null, null);
	}

	public static List getMemoByEntityStatus(String _sVendorID, int _iStatus, String _sCurrencyID, Date _dStart, Date _dEnd)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(DebitMemoPeer.VENDOR_ID, _sVendorID);
		oCrit.add(DebitMemoPeer.STATUS, _iStatus);
		oCrit.addAscendingOrderByColumn(DebitMemoPeer.TRANSACTION_DATE);
		if(StringUtil.isNotEmpty(_sCurrencyID))
		{
		    oCrit.add(DebitMemoPeer.CURRENCY_ID, _sCurrencyID);
	    }
		if(_dStart != null)
		{
		    oCrit.add(DebitMemoPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dStart), Criteria.GREATER_EQUAL);		   
		}
		if(_dEnd != null)			
		{
		    oCrit.add(DebitMemoPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);		   			
		}
		log.debug("getCMByCustomerAndStatus : " + oCrit);
		return DebitMemoPeer.doSelect(oCrit);
	}

	
	/**
	 * update memo closing rate, done when memo is used in payable payment.
	 * Closing rate must be set so the closing journal will be correct
	 * 
	 * @param _sMemoID
	 * @param _dClosingRate
	 * @throws Exception
	 */
	public static void updateClosingRate(String _sMemoID, double _dClosingRate)
		throws Exception
	{
		DebitMemo oMemo = getDebitMemoByID(_sMemoID);
		if (oMemo != null)
		{
			oMemo.setClosingRate(new BigDecimal(_dClosingRate));
			oMemo.save();
		}
	}

	/**
	 * save Pending DM temporarily
	 * 
	 * @param _vDMID
	 * @param _oPP
	 * @param _vAPD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void savePendingPayment(List _vDMID, ApPayment _oPP, List _vAPD, Connection _oConn)
		throws Exception
	{
		for(int i = 0; i < _vDMID.size(); i++)
		{			
			List vData = (List)_vDMID.get(i);
			ApPaymentDetail oAPD = (ApPaymentDetail)_vAPD.get(i);
			for(int j = 0; j < vData.size(); j++)
			{
				DebitMemo oDM = getDebitMemoByID((String)vData.get(j));
				if (oDM != null && oDM.getStatus() == i_MEMO_OPEN)
				{
					if(StringUtil.isEmpty(oDM.getPaymentTransId()) || 
					   StringUtil.isEqual(oDM.getPaymentTransId(), _oPP.getApPaymentId()))
					{
						oDM.setPaymentInvId(oAPD.getApPaymentDetailId());
						oDM.setPaymentTransId(_oPP.getApPaymentId());
						oDM.setPaymentTransNo(_oPP.getApPaymentNo());
						oDM.save(_oConn);
					}					
					else
					{
						throw new NestableException("ERROR: Invalid Debit Memo " + oDM.getTransactionNo() + 
													" Memo already used by Payment " + oDM.getPaymentTransNo());
					}					
				}	
				else
				{
					throw new NestableException("ERROR: Invalid Debit Memo, Status is Not OPEN ");
				}
			}   
		}
	}
	
	/**
	 * Close DebitMemo Status when Closed By PP
	 * 
	 * @param _vDebitMemo
	 * @param _oAPP
	 * @param _oConn
	 * @throws Exception
	 */
	public static void closeDM(List _vDebitMemo, ApPayment _oPP, Connection _oConn)
		throws Exception
	{
		List vDMObj = new ArrayList (10);
		for(int i = 0; i < _vDebitMemo.size(); i++)
		{			
			List vData = (List)_vDebitMemo.get(i);
			for(int j = 0; j < vData.size(); j++)
			{      
				DebitMemo oDebitMemo = getDebitMemoByID((String)vData.get(j));
				oDebitMemo.setStatus(i_MEMO_CLOSED);
				oDebitMemo.setPaymentTransId (_oPP.getApPaymentId());
				oDebitMemo.setPaymentTransNo (_oPP.getApPaymentNo());
				oDebitMemo.setClosedDate(_oPP.getApPaymentDate());				
				oDebitMemo.save(_oConn);
				vDMObj.add(oDebitMemo);
			}   
		}
		//create debit memo closing jornal if any DM used in this payment
		DebitMemoJournalTool.createClosingJournal(vDMObj, _oPP, _oConn);    	
	}

	public static void rollbackDMFromAPPayment(ApPayment _oAPP, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(DebitMemoPeer.PAYMENT_TRANS_ID, _oAPP.getApPaymentId());
		List vClosedDM = DebitMemoPeer.doSelect(oCrit, _oConn);
	
		//update CM CLOSED Status 
		for(int i = 0; i < vClosedDM.size(); i++)
		{      
			DebitMemo oDebitMemo = (DebitMemo) vClosedDM.get(i);
			oDebitMemo.setStatus(i_MEMO_OPEN);
			oDebitMemo.setPaymentTransId ("");
			oDebitMemo.setPaymentTransNo ("");
			oDebitMemo.setClosedDate(null);
			oDebitMemo.save(_oConn);
		}   
		//any closing journal created will be rolled back in 
		//PayablePaymentTool.cancelTransaction
	}
	
	public static LargeSelect findData(int _iCond, 
			   						   String _sKeywords, 
									   String _sVendorID, 
									   String _sLocationID,
									   Date _dStart,
									   Date _dEnd,
									   int _iLimit,
									   int _iStatus,
									   String _sCurrencyID)
    	throws Exception
    {
		Criteria oCrit = buildFindCriteria(m_FIND_PEER, _iCond, _sKeywords, 
			DebitMemoPeer.TRANSACTION_DATE, _dStart, _dEnd, _sCurrencyID
		);		
		if (StringUtil.isNotEmpty(_sVendorID))
		{
			oCrit.add(DebitMemoPeer.VENDOR_ID, _sVendorID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(DebitMemoPeer.LOCATION_ID, _sLocationID);
		}		
		if (_iStatus > 0)
		{
			oCrit.add(DebitMemoPeer.STATUS, _iStatus);
		}
		//return DebitMemoPeer.doSelect(oCrit);
		return new LargeSelect(oCrit, _iLimit, "com.ssti.enterprise.pos.om.DebitMemoPeer");
	}

	public static List findTrans(String _sVendorID, 
			 					 String _sBankID,
			 					 String _sLocationID,
			 					 String _sCashFlowTypeID,
			 					 Date _dFrom, 
			 					 Date _dTo, 
			 					 int _iStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		if (_dFrom != null)
		{
			oCrit.add(DebitMemoPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
		}
		if (_dTo != null)
		{
			oCrit.and(DebitMemoPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);
		}
		if (StringUtil.isNotEmpty(_sVendorID))
		{
			oCrit.add(DebitMemoPeer.VENDOR_ID, _sVendorID);
		}
		if (StringUtil.isNotEmpty(_sBankID))
		{
			oCrit.add(DebitMemoPeer.BANK_ID, _sBankID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(DebitMemoPeer.LOCATION_ID, _sLocationID);
		}		
		if (StringUtil.isNotEmpty(_sCashFlowTypeID))
		{
			oCrit.add(DebitMemoPeer.CASH_FLOW_TYPE_ID, _sCashFlowTypeID);
		}	
		if(_iStatus > 0)
		{
			oCrit.add(DebitMemoPeer.STATUS, _iStatus);
		}
		else
		{
			oCrit.add(DebitMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
		}
		return DebitMemoPeer.doSelect(oCrit);
	}

    //Report methods
	public static List getVendorList (List _vData) 
    	throws Exception
    {		
		List vTrans = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			DebitMemo oTrans = (DebitMemo) _vData.get(i);			
			if (!isVendorInList (vTrans, oTrans)) {
				vTrans.add (oTrans);
			}
		}
		return vTrans;
	}
	
	private static boolean isVendorInList (List _vTrans, DebitMemo _oTrans) 
	{
		for (int i = 0; i < _vTrans.size(); i++) {
			DebitMemo oTrans = (DebitMemo) _vTrans.get(i);			
			if (oTrans.getVendorId().equals(_oTrans.getVendorId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByVendorID (List _vTrans, String _sVendorID) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		DebitMemo oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (DebitMemo ) oIter.next();
			if (!oTrans.getVendorId().equals(_sVendorID)) {
				oIter.remove();
			}
		}
		return vTrans;
	}	

	public static List filterTransByStatus (List _vTrans, int _iStatus) 
    	throws Exception
    { 
		List vTrans = new ArrayList(_vTrans); 
		DebitMemo oTrans = null;
		Iterator oIter = vTrans.iterator();
		while (oIter.hasNext()) {
			oTrans = (DebitMemo ) oIter.next();
			if (oTrans.getStatus() != _iStatus) {
				oIter.remove();
			}
		}
		return vTrans;
	}

	public static List getByPaymentTransID (String _sPaymentID)
	    throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add (DebitMemoPeer.PAYMENT_TRANS_ID, _sPaymentID);	    
	    return DebitMemoPeer.doSelect (oCrit);
	} 

	public static List getIDByPaymentTransID (String _sPaymentID, String _sDetID)
	    throws Exception
	{
		List vDM = getByPaymentTransID(_sPaymentID);
		List vID = new ArrayList(vDM.size());
		for(int i = 0; i < vDM.size(); i++)
		{
			DebitMemo oDM = (DebitMemo)vDM.get(i);
			if (StringUtil.isEqual(oDM.getPaymentInvId(), _sDetID))
			{
				vID.add(oDM.getDebitMemoId());
			}
		}
		return vID;
	} 

	public static DebitMemo getByFromPaymentID (String _sPaymentID)
	    throws Exception
	{
	    Criteria oCrit = new Criteria();
	    oCrit.add (DebitMemoPeer.FROM_PAYMENT_ID, _sPaymentID);	
	    oCrit.add (DebitMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
	    List vCM = DebitMemoPeer.doSelect (oCrit);
	    if (vCM.size() > 0)
	    {
	    	return (DebitMemo)vCM.get(0);
	    }
	    return null;
	} 
		
		//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(DebitMemoPeer.class, DebitMemoPeer.DEBIT_MEMO_ID, _sID, _bIsNext);
	}	


	public static final String getTransactionTypeName (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);		
		if (iType == i_AP_TRANS_PO_DOWNPAYMENT ) return s_TRANS_PURCHASE_ORDER;
		if (iType == i_AP_TRANS_PURCHASE_RETURN) return s_TRANS_PURCHASE_RETURN;
		return "";
	}
	
	public static final String getTransactionScreen (Integer _iTransType) 
	{
		int iType = Calculator.toInt(_iTransType);		
		if (iType == i_AP_TRANS_PO_DOWNPAYMENT ) return s_SCREEN_PURCHASE_ORDER;
		if (iType == i_AP_TRANS_PURCHASE_RETURN) return s_SCREEN_PURCHASE_RETURN;		
		return "";
	}

	public static String getStatusString (Integer _iStatus)
	{
		return CreditMemoTool.getStatusString(_iStatus);
	}	

	public static List getByOrderID (String _sSourceID, int _iStatus, Connection _oConn)
			throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (DebitMemoPeer.TRANSACTION_ID, _sSourceID);	
		if (_iStatus > 0)
		{
			oCrit.add (DebitMemoPeer.STATUS, _iStatus);
		}
		else
		{
			oCrit.add (DebitMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
		}
		List vCM = DebitMemoPeer.doSelect (oCrit, _oConn);
		return vCM;
	}
	
	public static DebitMemo getBySourceTransID (String _sSourceID)
			throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (DebitMemoPeer.TRANSACTION_ID, _sSourceID);	
		oCrit.add (DebitMemoPeer.STATUS, i_MEMO_CANCELLED, Criteria.NOT_EQUAL);
		List vCM = DebitMemoPeer.doSelect (oCrit);
		if (vCM.size() > 0)
		{
			return (DebitMemo)vCM.get(0);
		}
		return null;
	} 		
}
package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class ItemUpdateHistoryMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.ItemUpdateHistoryMapBuilder";

    /**
     * Item
     * @deprecated use ItemUpdateHistoryPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "item_update_history";
    }

  
    /**
     * item_update_history.ITEM_UPDATE_HISTORY_ID
     * @return the column name for the ITEM_UPDATE_HISTORY_ID field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.ITEM_UPDATE_HISTORY_ID constant
     */
    public static String getItemUpdateHistory_ItemUpdateHistoryId()
    {
        return "item_update_history.ITEM_UPDATE_HISTORY_ID";
    }
  
    /**
     * item_update_history.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.ITEM_ID constant
     */
    public static String getItemUpdateHistory_ItemId()
    {
        return "item_update_history.ITEM_ID";
    }
  
    /**
     * item_update_history.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.ITEM_CODE constant
     */
    public static String getItemUpdateHistory_ItemCode()
    {
        return "item_update_history.ITEM_CODE";
    }
  
    /**
     * item_update_history.UPDATE_PART
     * @return the column name for the UPDATE_PART field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.UPDATE_PART constant
     */
    public static String getItemUpdateHistory_UpdatePart()
    {
        return "item_update_history.UPDATE_PART";
    }
  
    /**
     * item_update_history.OLD_VALUE
     * @return the column name for the OLD_VALUE field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.OLD_VALUE constant
     */
    public static String getItemUpdateHistory_OldValue()
    {
        return "item_update_history.OLD_VALUE";
    }
  
    /**
     * item_update_history.NEW_VALUE
     * @return the column name for the NEW_VALUE field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.NEW_VALUE constant
     */
    public static String getItemUpdateHistory_NewValue()
    {
        return "item_update_history.NEW_VALUE";
    }
  
    /**
     * item_update_history.USER_NAME
     * @return the column name for the USER_NAME field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.USER_NAME constant
     */
    public static String getItemUpdateHistory_UserName()
    {
        return "item_update_history.USER_NAME";
    }
  
    /**
     * item_update_history.UPDATE_DESCRIPTION
     * @return the column name for the UPDATE_DESCRIPTION field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.UPDATE_DESCRIPTION constant
     */
    public static String getItemUpdateHistory_UpdateDescription()
    {
        return "item_update_history.UPDATE_DESCRIPTION";
    }
  
    /**
     * item_update_history.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use ItemUpdateHistoryPeer.item_update_history.UPDATE_DATE constant
     */
    public static String getItemUpdateHistory_UpdateDate()
    {
        return "item_update_history.UPDATE_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("item_update_history");
        TableMap tMap = dbMap.getTable("item_update_history");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("item_update_history.ITEM_UPDATE_HISTORY_ID", "");
                          tMap.addColumn("item_update_history.ITEM_ID", "");
                          tMap.addColumn("item_update_history.ITEM_CODE", "");
                          tMap.addColumn("item_update_history.UPDATE_PART", "");
                          tMap.addColumn("item_update_history.OLD_VALUE", "");
                          tMap.addColumn("item_update_history.NEW_VALUE", "");
                          tMap.addColumn("item_update_history.USER_NAME", "");
                          tMap.addColumn("item_update_history.UPDATE_DESCRIPTION", "");
                          tMap.addColumn("item_update_history.UPDATE_DATE", new Date());
          }
}

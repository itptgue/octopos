package com.ssti.enterprise.pos.presentation.screens.report.inventory;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.inventory.ItemTransferTool;

public class ItemTransferReport extends Default
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
    		String sLocationID = data.getParameters().getString("LocationId","");
    	    String sToLocationID = data.getParameters().getString("ToLocationId","");
	    	context.put ("ItemTransfers", 
	    		ItemTransferTool.findData (dStart, dEnd, sLocationID, sToLocationID, "", i_MULTISTAT));     
	    }   
    	catch (Exception _oEx) {
    		data.setMessage("Data LookUp Failed !");
    	}    
    }    
}

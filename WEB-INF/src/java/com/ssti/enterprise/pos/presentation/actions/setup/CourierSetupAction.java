package com.ssti.enterprise.pos.presentation.actions.setup;

import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.FOBCourierManager;
import com.ssti.enterprise.pos.om.Courier;
import com.ssti.enterprise.pos.om.CourierPeer;
import com.ssti.enterprise.pos.tools.CourierTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class CourierSetupAction extends SetupSecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Courier oCourier = new Courier();
			setProperties (oCourier, data);
			oCourier.setCourierId (IDGenerator.generateSysID());
	        oCourier.save();
	        FOBCourierManager.getInstance().refreshCache(oCourier);
        	data.setMessage(LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Courier oCourier = CourierTool.getCourierByID(data.getParameters().getString("ID"));
			Courier oOld = oCourier.copy();
			setProperties (oCourier, data);
	        oCourier.save();
	        FOBCourierManager.getInstance().refreshCache(oCourier);
	        UpdateHistoryTool.createHistory(oOld, oCourier, data.getUser().getName(),null);
           	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch (Exception _oEx)
        {
        	data.setMessage(LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		String sColumn = "courier_id";
    		String sID = data.getParameters().getString("ID");
    		if( SqlUtil.validateFKRef("purchase_order",sColumn,sID)
    			&& SqlUtil.validateFKRef("purchase_receipt",sColumn,sID)
    			&& SqlUtil.validateFKRef("purchase_invoice",sColumn,sID)
				&& SqlUtil.validateFKRef("sales_order",sColumn,sID)
				&& SqlUtil.validateFKRef("delivery_order",sColumn,sID)
    			&& SqlUtil.validateFKRef("sales_transaction",sColumn,sID)
				&& SqlUtil.validateFKRef("item_transfer",sColumn,sID)
				&& SqlUtil.validateFKRef("packing_list",sColumn,sID)
    		  )
    		{
    			Courier oCourier = CourierTool.getCourierByID(sID);
	    		Criteria oCrit = new Criteria();
	        	oCrit.add(CourierPeer.COURIER_ID, data.getParameters().getString("ID"));
	        	CourierPeer.doDelete(oCrit);
	        	UpdateHistoryTool.createDelHistory(oCourier, data.getUser().getName(),null);
	        	FOBCourierManager.getInstance().refreshCache(sID);
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	 data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE));
        }
        catch (Exception _oEx)
        {
			data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx);
        }
    }

    private void setProperties (Courier _oCourier, RunData data)
    	throws Exception
    {
		try
		{
	    	data.getParameters().setProperties(_oCourier);
	    	_oCourier.setIsInternal (data.getParameters().getBoolean("IsInternal"));
    	}
    	catch (Exception _oEx)
    	{
    		throw new Exception (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage(), _oEx);
        }
    }
}

package com.ssti.enterprise.medical.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.medical.om.PowderItem;
import com.ssti.enterprise.medical.tools.PowderTool;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class PowderItemHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
    	String sRFee = LocaleTool.get("medical", "rfee");
    	String sPack = LocaleTool.get("medical", "packaging");
    	
    	String[] a_HEADER =
    	{
	    	"Item Code",	
	    	"Item Name",	
	    	"Multiplied",
	    	sRFee,	
	    	"Min " + sRFee,	
	    	"Max " + sRFee,
	    	sPack,	
	    	"Min " + sPack,
	    	"Max " + sPack
	    	
    	};

        List vData = PowderTool.getAllPowderItem();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Powder Item Worksheet");
        HSSFRow oRow = oSheet.createRow(0);
        int iCol = 0;
        for (int i = 0; i < a_HEADER.length; i++)
        {
        	createHeaderCell(oWB, oRow, (short)iCol, a_HEADER[i]); iCol++;
        }
	
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                PowderItem oData = (PowderItem) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                Item oItem = ItemTool.getItemByID(oData.getItemId());
                
                iCol = 0;
                createCell(oWB, oRow2, (short)iCol, oData.getItemCode());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, oItem.getItemName());  iCol++; 
                createCell(oWB, oRow2, (short)iCol, Boolean.toString(oData.getMultiplied()));  iCol++;                 
                createCell(oWB, oRow2, (short)iCol, oData.getDefaultRfee());  iCol++;                                
                createCell(oWB, oRow2, (short)iCol, oData.getRfeeMinAmount()); iCol++;            
                createCell(oWB, oRow2, (short)iCol, oData.getRfeeMaxAmount()); iCol++;
                createCell(oWB, oRow2, (short)iCol, oData.getDefaultPackaging());  iCol++;                                
                createCell(oWB, oRow2, (short)iCol, oData.getPackMinAmount()); iCol++;            
                createCell(oWB, oRow2, (short)iCol, oData.getPackMaxAmount()); iCol++;
            }        
        }
        return oWB;
    }
}

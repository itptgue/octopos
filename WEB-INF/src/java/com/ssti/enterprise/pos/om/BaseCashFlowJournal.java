package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CashFlowJournal
 */
public abstract class BaseCashFlowJournal extends BaseObject
{
    /** The Peer class */
    private static final CashFlowJournalPeer peer =
        new CashFlowJournalPeer();

        
    /** The value for the cashFlowJournalId field */
    private String cashFlowJournalId;
      
    /** The value for the cashFlowId field */
    private String cashFlowId;
      
    /** The value for the accountId field */
    private String accountId;
      
    /** The value for the accountCode field */
    private String accountCode;
      
    /** The value for the accountName field */
    private String accountName;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
      
    /** The value for the projectId field */
    private String projectId;
      
    /** The value for the departmentId field */
    private String departmentId;
                                                
    /** The value for the memo field */
    private String memo = "";
      
    /** The value for the debitCredit field */
    private int debitCredit;
                                                
    /** The value for the locationId field */
    private String locationId = "";
                                          
    /** The value for the subLedgerType field */
    private int subLedgerType = 0;
                                                
    /** The value for the subLedgerId field */
    private String subLedgerId = "";
                                                
    /** The value for the subLedgerDesc field */
    private String subLedgerDesc = "";
                                                
    /** The value for the subLedgerTransId field */
    private String subLedgerTransId = "";
                                                
    /** The value for the cashFlowTypeId field */
    private String cashFlowTypeId = "";
  
    
    /**
     * Get the CashFlowJournalId
     *
     * @return String
     */
    public String getCashFlowJournalId()
    {
        return cashFlowJournalId;
    }

                        
    /**
     * Set the value of CashFlowJournalId
     *
     * @param v new value
     */
    public void setCashFlowJournalId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowJournalId, v))
              {
            this.cashFlowJournalId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowId
     *
     * @return String
     */
    public String getCashFlowId()
    {
        return cashFlowId;
    }

                              
    /**
     * Set the value of CashFlowId
     *
     * @param v new value
     */
    public void setCashFlowId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.cashFlowId, v))
              {
            this.cashFlowId = v;
            setModified(true);
        }
    
                          
                if (aCashFlow != null && !ObjectUtils.equals(aCashFlow.getCashFlowId(), v))
                {
            aCashFlow = null;
        }
      
              }
  
    /**
     * Get the AccountId
     *
     * @return String
     */
    public String getAccountId()
    {
        return accountId;
    }

                        
    /**
     * Set the value of AccountId
     *
     * @param v new value
     */
    public void setAccountId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountId, v))
              {
            this.accountId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountCode
     *
     * @return String
     */
    public String getAccountCode()
    {
        return accountCode;
    }

                        
    /**
     * Set the value of AccountCode
     *
     * @param v new value
     */
    public void setAccountCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountCode, v))
              {
            this.accountCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AccountName
     *
     * @return String
     */
    public String getAccountName()
    {
        return accountName;
    }

                        
    /**
     * Set the value of AccountName
     *
     * @param v new value
     */
    public void setAccountName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountName, v))
              {
            this.accountName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ProjectId
     *
     * @return String
     */
    public String getProjectId()
    {
        return projectId;
    }

                        
    /**
     * Set the value of ProjectId
     *
     * @param v new value
     */
    public void setProjectId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.projectId, v))
              {
            this.projectId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DepartmentId
     *
     * @return String
     */
    public String getDepartmentId()
    {
        return departmentId;
    }

                        
    /**
     * Set the value of DepartmentId
     *
     * @param v new value
     */
    public void setDepartmentId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.departmentId, v))
              {
            this.departmentId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Memo
     *
     * @return String
     */
    public String getMemo()
    {
        return memo;
    }

                        
    /**
     * Set the value of Memo
     *
     * @param v new value
     */
    public void setMemo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.memo, v))
              {
            this.memo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DebitCredit
     *
     * @return int
     */
    public int getDebitCredit()
    {
        return debitCredit;
    }

                        
    /**
     * Set the value of DebitCredit
     *
     * @param v new value
     */
    public void setDebitCredit(int v) 
    {
    
                  if (this.debitCredit != v)
              {
            this.debitCredit = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerType
     *
     * @return int
     */
    public int getSubLedgerType()
    {
        return subLedgerType;
    }

                        
    /**
     * Set the value of SubLedgerType
     *
     * @param v new value
     */
    public void setSubLedgerType(int v) 
    {
    
                  if (this.subLedgerType != v)
              {
            this.subLedgerType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerId
     *
     * @return String
     */
    public String getSubLedgerId()
    {
        return subLedgerId;
    }

                        
    /**
     * Set the value of SubLedgerId
     *
     * @param v new value
     */
    public void setSubLedgerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.subLedgerId, v))
              {
            this.subLedgerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerDesc
     *
     * @return String
     */
    public String getSubLedgerDesc()
    {
        return subLedgerDesc;
    }

                        
    /**
     * Set the value of SubLedgerDesc
     *
     * @param v new value
     */
    public void setSubLedgerDesc(String v) 
    {
    
                  if (!ObjectUtils.equals(this.subLedgerDesc, v))
              {
            this.subLedgerDesc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SubLedgerTransId
     *
     * @return String
     */
    public String getSubLedgerTransId()
    {
        return subLedgerTransId;
    }

                        
    /**
     * Set the value of SubLedgerTransId
     *
     * @param v new value
     */
    public void setSubLedgerTransId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.subLedgerTransId, v))
              {
            this.subLedgerTransId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowTypeId
     *
     * @return String
     */
    public String getCashFlowTypeId()
    {
        return cashFlowTypeId;
    }

                        
    /**
     * Set the value of CashFlowTypeId
     *
     * @param v new value
     */
    public void setCashFlowTypeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowTypeId, v))
              {
            this.cashFlowTypeId = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private CashFlow aCashFlow;

    /**
     * Declares an association between this object and a CashFlow object
     *
     * @param v CashFlow
     * @throws TorqueException
     */
    public void setCashFlow(CashFlow v) throws TorqueException
    {
            if (v == null)
        {
                  setCashFlowId((String) null);
              }
        else
        {
            setCashFlowId(v.getCashFlowId());
        }
            aCashFlow = v;
    }

                                            
    /**
     * Get the associated CashFlow object
     *
     * @return the associated CashFlow object
     * @throws TorqueException
     */
    public CashFlow getCashFlow() throws TorqueException
    {
        if (aCashFlow == null && (!ObjectUtils.equals(this.cashFlowId, null)))
        {
                          aCashFlow = CashFlowPeer.retrieveByPK(SimpleKey.keyFor(this.cashFlowId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               CashFlow obj = CashFlowPeer.retrieveByPK(this.cashFlowId);
               obj.addCashFlowJournals(this);
            */
        }
        return aCashFlow;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setCashFlowKey(ObjectKey key) throws TorqueException
    {
      
                        setCashFlowId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CashFlowJournalId");
              fieldNames.add("CashFlowId");
              fieldNames.add("AccountId");
              fieldNames.add("AccountCode");
              fieldNames.add("AccountName");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("Amount");
              fieldNames.add("AmountBase");
              fieldNames.add("ProjectId");
              fieldNames.add("DepartmentId");
              fieldNames.add("Memo");
              fieldNames.add("DebitCredit");
              fieldNames.add("LocationId");
              fieldNames.add("SubLedgerType");
              fieldNames.add("SubLedgerId");
              fieldNames.add("SubLedgerDesc");
              fieldNames.add("SubLedgerTransId");
              fieldNames.add("CashFlowTypeId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CashFlowJournalId"))
        {
                return getCashFlowJournalId();
            }
          if (name.equals("CashFlowId"))
        {
                return getCashFlowId();
            }
          if (name.equals("AccountId"))
        {
                return getAccountId();
            }
          if (name.equals("AccountCode"))
        {
                return getAccountCode();
            }
          if (name.equals("AccountName"))
        {
                return getAccountName();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("ProjectId"))
        {
                return getProjectId();
            }
          if (name.equals("DepartmentId"))
        {
                return getDepartmentId();
            }
          if (name.equals("Memo"))
        {
                return getMemo();
            }
          if (name.equals("DebitCredit"))
        {
                return Integer.valueOf(getDebitCredit());
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("SubLedgerType"))
        {
                return Integer.valueOf(getSubLedgerType());
            }
          if (name.equals("SubLedgerId"))
        {
                return getSubLedgerId();
            }
          if (name.equals("SubLedgerDesc"))
        {
                return getSubLedgerDesc();
            }
          if (name.equals("SubLedgerTransId"))
        {
                return getSubLedgerTransId();
            }
          if (name.equals("CashFlowTypeId"))
        {
                return getCashFlowTypeId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CashFlowJournalPeer.CASH_FLOW_JOURNAL_ID))
        {
                return getCashFlowJournalId();
            }
          if (name.equals(CashFlowJournalPeer.CASH_FLOW_ID))
        {
                return getCashFlowId();
            }
          if (name.equals(CashFlowJournalPeer.ACCOUNT_ID))
        {
                return getAccountId();
            }
          if (name.equals(CashFlowJournalPeer.ACCOUNT_CODE))
        {
                return getAccountCode();
            }
          if (name.equals(CashFlowJournalPeer.ACCOUNT_NAME))
        {
                return getAccountName();
            }
          if (name.equals(CashFlowJournalPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(CashFlowJournalPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(CashFlowJournalPeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(CashFlowJournalPeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(CashFlowJournalPeer.PROJECT_ID))
        {
                return getProjectId();
            }
          if (name.equals(CashFlowJournalPeer.DEPARTMENT_ID))
        {
                return getDepartmentId();
            }
          if (name.equals(CashFlowJournalPeer.MEMO))
        {
                return getMemo();
            }
          if (name.equals(CashFlowJournalPeer.DEBIT_CREDIT))
        {
                return Integer.valueOf(getDebitCredit());
            }
          if (name.equals(CashFlowJournalPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(CashFlowJournalPeer.SUB_LEDGER_TYPE))
        {
                return Integer.valueOf(getSubLedgerType());
            }
          if (name.equals(CashFlowJournalPeer.SUB_LEDGER_ID))
        {
                return getSubLedgerId();
            }
          if (name.equals(CashFlowJournalPeer.SUB_LEDGER_DESC))
        {
                return getSubLedgerDesc();
            }
          if (name.equals(CashFlowJournalPeer.SUB_LEDGER_TRANS_ID))
        {
                return getSubLedgerTransId();
            }
          if (name.equals(CashFlowJournalPeer.CASH_FLOW_TYPE_ID))
        {
                return getCashFlowTypeId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCashFlowJournalId();
            }
              if (pos == 1)
        {
                return getCashFlowId();
            }
              if (pos == 2)
        {
                return getAccountId();
            }
              if (pos == 3)
        {
                return getAccountCode();
            }
              if (pos == 4)
        {
                return getAccountName();
            }
              if (pos == 5)
        {
                return getCurrencyId();
            }
              if (pos == 6)
        {
                return getCurrencyRate();
            }
              if (pos == 7)
        {
                return getAmount();
            }
              if (pos == 8)
        {
                return getAmountBase();
            }
              if (pos == 9)
        {
                return getProjectId();
            }
              if (pos == 10)
        {
                return getDepartmentId();
            }
              if (pos == 11)
        {
                return getMemo();
            }
              if (pos == 12)
        {
                return Integer.valueOf(getDebitCredit());
            }
              if (pos == 13)
        {
                return getLocationId();
            }
              if (pos == 14)
        {
                return Integer.valueOf(getSubLedgerType());
            }
              if (pos == 15)
        {
                return getSubLedgerId();
            }
              if (pos == 16)
        {
                return getSubLedgerDesc();
            }
              if (pos == 17)
        {
                return getSubLedgerTransId();
            }
              if (pos == 18)
        {
                return getCashFlowTypeId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CashFlowJournalPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CashFlowJournalPeer.doInsert((CashFlowJournal) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CashFlowJournalPeer.doUpdate((CashFlowJournal) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cashFlowJournalId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCashFlowJournalId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCashFlowJournalId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCashFlowJournalId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CashFlowJournal copy() throws TorqueException
    {
        return copyInto(new CashFlowJournal());
    }
  
    protected CashFlowJournal copyInto(CashFlowJournal copyObj) throws TorqueException
    {
          copyObj.setCashFlowJournalId(cashFlowJournalId);
          copyObj.setCashFlowId(cashFlowId);
          copyObj.setAccountId(accountId);
          copyObj.setAccountCode(accountCode);
          copyObj.setAccountName(accountName);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setAmount(amount);
          copyObj.setAmountBase(amountBase);
          copyObj.setProjectId(projectId);
          copyObj.setDepartmentId(departmentId);
          copyObj.setMemo(memo);
          copyObj.setDebitCredit(debitCredit);
          copyObj.setLocationId(locationId);
          copyObj.setSubLedgerType(subLedgerType);
          copyObj.setSubLedgerId(subLedgerId);
          copyObj.setSubLedgerDesc(subLedgerDesc);
          copyObj.setSubLedgerTransId(subLedgerTransId);
          copyObj.setCashFlowTypeId(cashFlowTypeId);
  
                    copyObj.setCashFlowJournalId((String)null);
                                                                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CashFlowJournalPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CashFlowJournal\n");
        str.append("---------------\n")
           .append("CashFlowJournalId    : ")
           .append(getCashFlowJournalId())
           .append("\n")
           .append("CashFlowId           : ")
           .append(getCashFlowId())
           .append("\n")
           .append("AccountId            : ")
           .append(getAccountId())
           .append("\n")
           .append("AccountCode          : ")
           .append(getAccountCode())
           .append("\n")
           .append("AccountName          : ")
           .append(getAccountName())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("ProjectId            : ")
           .append(getProjectId())
           .append("\n")
           .append("DepartmentId         : ")
           .append(getDepartmentId())
           .append("\n")
           .append("Memo                 : ")
           .append(getMemo())
           .append("\n")
           .append("DebitCredit          : ")
           .append(getDebitCredit())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("SubLedgerType        : ")
           .append(getSubLedgerType())
           .append("\n")
           .append("SubLedgerId          : ")
           .append(getSubLedgerId())
           .append("\n")
           .append("SubLedgerDesc        : ")
           .append(getSubLedgerDesc())
           .append("\n")
           .append("SubLedgerTransId     : ")
           .append(getSubLedgerTransId())
           .append("\n")
           .append("CashFlowTypeId       : ")
           .append(getCashFlowTypeId())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.tools.gl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.GeneralLedger;
import com.ssti.enterprise.pos.om.GeneralLedgerPeer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.om.PeriodPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: GeneralLedgerTool.java,v 1.10 2008/06/29 07:12:48 albert Exp $ <br>
 *
 * <pre>
 * $Log: GeneralLedgerTool.java,v $
 * Revision 1.10  2008/06/29 07:12:48  albert
 * *** empty log message ***
 *
 * </pre><br>
 */
public class GeneralLedgerTool extends BaseTool implements GlAttributes
{    
	private static Log log = LogFactory.getLog ( GeneralLedgerTool.class );
	
	private static GeneralLedgerTool instance = null;
	
	public static final int i_BUDGET = 0;
	public static final int i_BALANCE = 1;
	
	public static synchronized GeneralLedgerTool getInstance()
	{
		if (instance == null) instance = new GeneralLedgerTool();
		return instance;
	}

	public static Map m_PEER = null;
	static
	{
		m_PEER = new HashMap(3);
		m_PEER.put("loc",  GeneralLedgerPeer.LOCATION_ID);
	}	
	
	public static void deleteDataByAccountID (String _sAccountID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (GeneralLedgerPeer.ACCOUNT_ID, _sAccountID);
		GeneralLedgerPeer.doDelete (oCrit);
	}	  	

	/**
	 * get GL Data
	 * 
	 * @param _sPeriodID
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @param _oConn
	 * @return GeneralLedger
	 * @throws Exception
	 */
	public static GeneralLedger getGLData (String _sPeriodID, 
										   String _sAccountID, 
										   String _sSubType,
										   String _sSubID,
										   Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (GeneralLedgerPeer.PERIOD_ID, _sPeriodID);		
		oCrit.add (GeneralLedgerPeer.ACCOUNT_ID, _sAccountID);
		if (StringUtil.isNotEmpty(_sSubType) && StringUtil.isNotEmpty(_sSubID))
		{
			oCrit.add ((String)m_PEER.get(_sSubType), _sSubID);
		}
		else
		{
			oCrit.add (GeneralLedgerPeer.LOCATION_ID, "");
			oCrit.add (GeneralLedgerPeer.DEPARTMENT_ID, "");
			oCrit.add (GeneralLedgerPeer.PROJECT_ID, "");
		}
		List vData = GeneralLedgerPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0)
		{
		    return (GeneralLedger) vData.get(0);
		}
		return null;
	}	
	
	/**
	 * get Account Budget
	 * @param _sPeriodID
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @return Budget 
	 * @throws Exception
	 */
 	public static BigDecimal getBudget(String _sPeriodID, 
 									   String _sAccountID, 
 									   String _sSubType,
 									   String _sSubID)
     	throws Exception
     {          
        GeneralLedger oGL = getGLData (_sPeriodID, _sAccountID, _sSubType, _sSubID, null);
        if (oGL != null)
        {
            return oGL.getBudgetAmount();
	    }
	    return bd_ZERO;
	}
    
    /**
     * set Account Budget in certain period 
     * 
     * @param _sAccountID, The Account we'd like to update
     * @param _sPeriodID,  The Period we'd like to update
     * @param _oBudget, Budget Amount
     * @param Connection _oConn, Connection object this method optionally join a transaction
     */
     
 	public static void setBudget(String _sPeriodID, 
 								 String _sAccountID, 
								 String _sSubType,
 								 String _sSubID,
								 BigDecimal oBudget)
     	throws Exception
     {
 		double dOldBudget = 0;
 		double dChange = oBudget.doubleValue();
        GeneralLedger oGL = getGLData (_sPeriodID, _sAccountID, _sSubType, _sSubID, null);
        if (oGL == null) 
        {	
            oGL = new GeneralLedger();
            oGL.setGeneralLedgerId(IDGenerator.generateSysID());
            oGL.setPeriodId(_sPeriodID);
            oGL.setAccountId(_sAccountID);
            oGL.setLocationId("");
            oGL.setDepartmentId("");
            oGL.setProjectId("");
            oGL.setOpeningBalance(bd_ZERO);
            oGL.setTotalDebitAmount(bd_ZERO);
            oGL.setTotalCreditAmount(bd_ZERO);
            oGL.setEndingBalance(bd_ZERO);
            oGL.setBudgetAmount(oBudget);
            oGL.setVariance(bd_ZERO);
            oGL.setLocationId(_sSubID);
        }
        else 
        {   
        	dOldBudget = oGL.getBudgetAmount().doubleValue();
        	dChange = dChange - dOldBudget;
            oGL.setBudgetAmount(oBudget);
        }
        oGL.save();	        

        //if not empty then calculate the overall gl budget (loc_id & dept_id & prj_id = "")
        if (StringUtil.isNotEmpty(_sSubType) && StringUtil.isNotEmpty(_sSubID))
        {
        	Criteria oCrit = new Criteria();
        	oCrit.add(GeneralLedgerPeer.ACCOUNT_ID, _sAccountID);
        	oCrit.add(GeneralLedgerPeer.PERIOD_ID, _sPeriodID);
        	oCrit.add(GeneralLedgerPeer.LOCATION_ID, "");
        	oCrit.add(GeneralLedgerPeer.DEPARTMENT_ID, "");
        	oCrit.add(GeneralLedgerPeer.PROJECT_ID, "");
        	
        	List vData = GeneralLedgerPeer.doSelect(oCrit);
        	if (vData.size() > 0)
        	{
        		GeneralLedger oAllGL = (GeneralLedger) vData.get(0);
        		oAllGL.setBudgetAmount(
        			new BigDecimal(oAllGL.getBudgetAmount().doubleValue() + dChange) );
        		oAllGL.save();
        	}
        	else if (vData.size() == 0)
        	{
        		GeneralLedger oAllGL = new GeneralLedger();
                oAllGL.setGeneralLedgerId(IDGenerator.generateSysID());
                oAllGL.setPeriodId(_sPeriodID);
                oAllGL.setAccountId(_sAccountID);
                oAllGL.setLocationId("");
                oAllGL.setDepartmentId("");
                oAllGL.setProjectId("");
                oAllGL.setOpeningBalance(bd_ZERO);
                oAllGL.setTotalDebitAmount(bd_ZERO);
                oAllGL.setTotalCreditAmount(bd_ZERO);
                oAllGL.setEndingBalance(bd_ZERO);
                oAllGL.setBudgetAmount(oBudget);
                oAllGL.setVariance(bd_ZERO);
                oAllGL.save();
        	}
        }
	}
 	
	/**
	 * get period ending balance
	 * 
	 * @param _dAsOf
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @return Account EndingBalance in certain period
	 * @throws Exception
	 */
 	public static BigDecimal getBalance(Date _dAsOf, 
 									 	String _sAccountID, 
									   	String _sSubType,
									   	String _sSubID)
 		throws Exception
 	{   
 		Period oPeriod = PeriodTool.getPeriodByDate(_dAsOf, null);
 		if (oPeriod != null)
 		{
	 		GeneralLedger oGL = getGLData (oPeriod.getPeriodId(), _sAccountID, _sSubType, _sSubID, null);
	 		if (oGL != null)
	 		{
	 			return oGL.getEndingBalance();
	 		}
 		}
 		return bd_ZERO;
 	}
	 	
	/**
	 * get year to date values and balance
	 * 
	 * @param _dAsOf
	 * @param _sAccountID
	 * @param _sSubType
	 * @param _sSubID
	 * @return {dTotalBudget, dTotalBalance}
	 * @throws Exception
	 */
 	public static double[] getYTDValues(Date _dAsOf, 
 							   		    String _sAccountID, 
									    String _sSubType,
									    String _sSubID)
 		throws Exception
 	{   
 		double dValues[] = {0,0};
  		Period oPeriod = PeriodTool.getPeriodByDate(_dAsOf, null);
 		if (oPeriod != null)
 		{
 			List vPeriod = new ArrayList();
 			vPeriod.add (oPeriod);
 			if (oPeriod.getMonth() > 1)
 			{
		 		vPeriod.addAll(PeriodTool.getYearPrevPeriod(oPeriod));
 			}
 			double dTotalBudget = 0;
 			double dTotalBalance = 0;
 			for (int i = 0; i < vPeriod.size(); i++)
 			{
 				Period oYTDPeriod = (Period) vPeriod.get(i);
 				GeneralLedger oGL = getGLData (oYTDPeriod.getPeriodId(), _sAccountID, _sSubType, _sSubID, null);
 				if (oGL != null)
 				{
 					dValues[i_BUDGET] = dValues[i_BUDGET] + oGL.getBudgetAmount().doubleValue();
 					dValues[i_BALANCE] = dValues[i_BALANCE] + oGL.getEndingBalance().doubleValue();
 				}
 			} 			
 		}
 		return dValues;
 	} 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	//-------------------------------------------------------------------------
 	// UNUSED METHOD
 	//-------------------------------------------------------------------------

    /**
     * get previous gl data period end balance
     * 
     * @param _sPeriodID, the period id which we want the data afterward
     * @param _sAccountID, the account id 
     * @param _oConn, the Connection object
     */

	public static BigDecimal getPreviousPeriodEndBalance (String _sPeriodID, String _sAccountID, Connection _oConn) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add (GeneralLedgerPeer.ACCOUNT_ID, _sAccountID);
		oCrit.addJoin (GeneralLedgerPeer.PERIOD_ID, PeriodPeer.PERIOD_ID);		
		oCrit.and (PeriodPeer.PERIOD_CODE, PeriodTool.getCodeByID(_sPeriodID), Criteria.LESS_THAN);	
		oCrit.addDescendingOrderByColumn (PeriodPeer.PERIOD_CODE);
        
        log.debug ("getPreviousPeriodEndBalance : " + oCrit);					
		
		List vData = new ArrayList();
        vData = GeneralLedgerPeer.doSelect (oCrit, _oConn);
		
		if (vData.size() > 0)
		{
		    //process prev periods, if current period 12 , 
		    //and period 11 no transaction affected the GL
		    //then we should do end process period automatically 

		    return processEndPeriod(vData, _oConn);
		}
		return bd_ZERO;
	}		

    /**
     * getLastExistGLPeriod
     * filter the list of previous gl period to find the last gl data that has value
     * dOpening != 0 || dClosing != 0 || dDebit != 0 || dCredit != 0
     *
     * @param _vGLData, the GL Data in period < period in method getPreviousPeriodEndBalance 
     * @return oGL last GL Data that has value
     */

    private static GeneralLedger getLastExistGLPeriod (List _vGLData)
        throws Exception
    {
        for (int i = 0; i < _vGLData.size(); i++)
        {
            GeneralLedger oGL = (GeneralLedger) _vGLData.get(i);
            double dOpening   = oGL.getOpeningBalance().doubleValue();
            double dClosing   = oGL.getEndingBalance().doubleValue();
            double dDebit     = oGL.getTotalDebitAmount().doubleValue();
            double dCredit    = oGL.getTotalCreditAmount().doubleValue();
            
            if (dOpening != 0 || dClosing != 0 || dDebit != 0 || dCredit != 0)
            {
                return oGL;
            }
        } 
        return null;
    }
    
    /**
     * process prev periods, if current period 12, no transaction affected the GL since period 10
     * then we should do end process period 
     * end process period will get the last gl data ending balance which is != 0
     * then create gl data from period 10 to 11 using period 9 ending balance
     *
     * @param _vGLData, the GL Data in period < period in method getPreviousPeriodEndBalance 
     * @return oLastEndingBalance last ending balance to put in new period
     */
     
    private static BigDecimal processEndPeriod(List _vGLData, Connection _oConn)
        throws Exception
    {        
        GeneralLedger oLastGL = getLastExistGLPeriod (_vGLData);
        
        if (oLastGL != null)
        {
            String sFromPeriodID = oLastGL.getPeriodId();
        
            List vPeriod = PeriodTool.getPeriodIDList (sFromPeriodID);

            for (int j = 0; j < vPeriod.size(); j++)
            {
                String sPeriodID = (String) vPeriod.get(j);
                
                GeneralLedger oGL = getGLData (sPeriodID, oLastGL.getAccountId(),"", "", _oConn);
                                
                log.debug ("processEndPeriod : ");

                if (oGL == null) //gl data not exist
                {    
                    log.debug ("GL Period To Create : " + PeriodTool.getDescriptionByID(sPeriodID));
                    log.debug ("For Account : " + AccountTool.getDescriptionByID(oLastGL.getAccountId()));
                                   
                    oGL = new GeneralLedger();
                    oGL.setGeneralLedgerId(IDGenerator.generateSysID());
                    oGL.setPeriodId(sPeriodID);
                    oGL.setAccountId(oLastGL.getAccountId());
                    oGL.setOpeningBalance(oLastGL.getEndingBalance());
                    oGL.setTotalDebitAmount(bd_ZERO);
                    oGL.setTotalCreditAmount(bd_ZERO);
                    oGL.setEndingBalance(oLastGL.getEndingBalance());
                    oGL.setBudgetAmount(bd_ZERO);
                    oGL.setVariance(bd_ZERO);         
                }
                else  //HAPPEN IF GL NO TRANSACTION HAPPENED IN PERIOD 
                {
                    if (oGL.getOpeningBalance().doubleValue() <= 0)
                        oGL.setOpeningBalance(oLastGL.getEndingBalance());          
                    if (oGL.getOpeningBalance().doubleValue() <= 0)                        
                        oGL.setEndingBalance(oLastGL.getEndingBalance());
                }
                oGL.save (_oConn);                           
            }        
            return oLastGL.getEndingBalance();
        }
        return bd_ZERO;
    }

    /**
     * get gl data which period > _sPeriodID in parameter and <= current period for certain account
     * 
     * @param _sPeriodID, the period id which we want the data afterward
     * @param _sAccountID, the account id 
     */

	public static List getNextGLData (String _sPeriodID, String _sAccountID)
 
		throws Exception
	{
        return getNextGLData (_sPeriodID, _sAccountID, null);
    }

    private static int getLastPeriodCode (String _sAccountID)
        throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add (GeneralLedgerPeer.ACCOUNT_ID, _sAccountID);
		oCrit.addJoin (GeneralLedgerPeer.PERIOD_ID, PeriodPeer.PERIOD_ID);		
		oCrit.addDescendingOrderByColumn (PeriodPeer.PERIOD_CODE);
		oCrit.setLimit(1);		
		
		log.debug ("getLastPeriodCode : " + oCrit);
		
        List vData = GeneralLedgerPeer.doSelect (oCrit);		
		if (vData.size() > 0)
		{
		    GeneralLedger oGL = (GeneralLedger) vData.get(0);
            return PeriodTool.getCodeByID(oGL.getPeriodId());
		}
		return 0;
    }

	public static List getNextGLData (String _sPeriodID, String _sAccountID, Connection _oConn) 
		throws Exception
	{
	    int iPeriodCode = PeriodTool.getCodeByID (_sPeriodID);
	    int iCurrentCode = PeriodTool.getCurrentCode();
        int iLastCode = getLastPeriodCode (_sAccountID);
        if (iLastCode < iCurrentCode) iLastCode = iCurrentCode;
        
        
		Criteria oCrit = new Criteria();
		oCrit.add (GeneralLedgerPeer.ACCOUNT_ID, _sAccountID);
		oCrit.addJoin (GeneralLedgerPeer.PERIOD_ID, PeriodPeer.PERIOD_ID);		
		oCrit.add (PeriodPeer.PERIOD_CODE, iPeriodCode, Criteria.GREATER_THAN);		
		oCrit.and (PeriodPeer.PERIOD_CODE, iLastCode, Criteria.LESS_EQUAL);	
		oCrit.addAscendingOrderByColumn (PeriodPeer.PERIOD_CODE);		
			
		log.debug ("getNextGLData : " + oCrit);
		if (_oConn != null)
		{
		    return GeneralLedgerPeer.doSelect (oCrit, _oConn);
		}
		else 
		{
            return GeneralLedgerPeer.doSelect (oCrit);
	    }		
	}		
	
    /**
     * createGLData - used to prepare GL update for account chart opening balance setup
     * this method will create new general ledger object
     * per period with empty balance from period id stated in GL Transaction until
     * the current period.
     * 
     * @param _oTrans, the gl transaction object
     * @param _vPeriod, list of period to create the GL data
     */
     
	private static void createGLData (GlTransaction _oTrans, List _vPeriod, Connection _oConn)
    	throws Exception
    {
        boolean bStartTrans = false;
        if (_oConn == null) 
        {
        	_oConn = beginTrans();
        	bStartTrans = true;            
        }
        try
        {
        	for (int j = 0; j < _vPeriod.size(); j++)
        	{
        		String sPeriodID = (String) _vPeriod.get(j);            
        		GeneralLedger oGL = getGLData (sPeriodID, _oTrans.getAccountId(), "", "", _oConn);
        		if (oGL == null) //gl data not exist
        		{    
        			log.debug ("createGLData");
        			log.debug ("GL Period To Create : " + PeriodTool.getDescriptionByID(sPeriodID));
        			log.debug ("For Account : " + AccountTool.getDescriptionByID(_oTrans.getAccountId()));
        			
        			oGL = new GeneralLedger();
        			oGL.setGeneralLedgerId(IDGenerator.generateSysID());
        			oGL.setPeriodId(sPeriodID);
        			oGL.setAccountId(_oTrans.getAccountId());
        			oGL.setOpeningBalance(bd_ZERO);            
        			oGL.setTotalDebitAmount(bd_ZERO);
        			oGL.setTotalCreditAmount(bd_ZERO);
        			oGL.setEndingBalance(bd_ZERO);
        			oGL.setBudgetAmount(bd_ZERO);
        			oGL.setVariance(bd_ZERO);   
        			oGL.save (_oConn);                 
        		}
        	}
        	commit(_oConn);
        }
        catch (Exception _oEx) 
        {
        	rollback(_oConn);
        }
    }		

	//---------------------------------------------------------   
	// update methods                                             
	//---------------------------------------------------------       

    /**
     * method to simplify rollback and update process
     *
     * @param _oOldTrans Old GlTransaction To Rollback
     * @param _oNewTrans New GlTransaction To Update
     */
	public static void updateGL(GlTransaction _oOldTrans, GlTransaction _oNewTrans, Connection _oConn)
    	throws Exception
    {
        rollbackGL (_oOldTrans, _oConn);
        updateGL   (_oNewTrans, _oConn);
    }

    /**
     * method to simplify rollback and update process
     *
     * @param _oOldTrans List of Old GlTransaction To Rollback
     * @param _oNewTrans List of New GlTransaction To Update
     */
	public static void updateGL(List _vOldTrans, List _vNewTrans, Connection _oConn)
    	throws Exception
    {
        rollbackGL (_vOldTrans, _oConn);
        updateGL   (_vNewTrans, _oConn);
    }

    /**
     * updateGL - this method will update GL data for account and period in transaction
     * if period in gl transaction is not in the current period, then we will update 
     * all the period since in gltrans to current period
     *
     * @param List _vTrans, List of GL Transaction To Update
     * @param Connection _oConn, Connection object this method must join a transaction
     */

	public static void updateGL(List _vTrans, Connection _oConn)
    	throws Exception
    {
		for (int i = 0; i < _vTrans.size(); i++)
		{
			GlTransaction oTrans = (GlTransaction) _vTrans.get(i);
            updateGL (oTrans, _oConn);
		}
	}	

	private static void updateGL(GlTransaction _oTrans, Connection _oConn)
    	throws Exception
    {
        if (_oTrans != null)
        {
            String sAccountID   = _oTrans.getAccountId();
            String sPeriodID    = _oTrans.getPeriodId();
            int iDebitCredit    = _oTrans.getDebitCredit();
            double dAmount      = _oTrans.getAmountBase().doubleValue();            
            double dTransAmount = AccountTool.checkBalancePlusMinus (sAccountID, iDebitCredit, dAmount );
            
            GeneralLedger oGL = getGLData (sPeriodID, sAccountID, "", "", _oConn);

            if (oGL == null) //GL Period Not Exist
            {
                log.debug ("1. GL Period Not Exist Create New For Period : " + 
                           PeriodTool.getDescriptionByID(_oTrans.getPeriodId()) + 
                           " And Account : " + AccountTool.getDescriptionByID(_oTrans.getAccountId()));
                
                BigDecimal oPrevBalance = getPreviousPeriodEndBalance(sPeriodID, sAccountID, _oConn); 
                
                oGL = new GeneralLedger();
                oGL.setGeneralLedgerId(IDGenerator.generateSysID());
                oGL.setPeriodId(sPeriodID);
                oGL.setAccountId(sAccountID);
                oGL.setOpeningBalance(oPrevBalance);
                oGL.setTotalDebitAmount(bd_ZERO);
                oGL.setTotalCreditAmount(bd_ZERO);
                oGL.setEndingBalance(oPrevBalance);
                oGL.setBudgetAmount(bd_ZERO);
                oGL.setVariance(bd_ZERO);
               
                if (_oTrans.getDebitCredit() == 1) //DEBIT
                {
                    oGL.setTotalDebitAmount(_oTrans.getAmountBase());     
                }
                else  //CREDIT
                {
                    oGL.setTotalCreditAmount(_oTrans.getAmountBase());     
                }
                double dEndingBalance = oGL.getEndingBalance().doubleValue() + dTransAmount;                
                oGL.setEndingBalance(new BigDecimal(dEndingBalance)); 
            }
            else //GL Period Exist
            {   
                log.debug ("1. GL Period Exist For Period : " + 
                           PeriodTool.getDescriptionByID(sPeriodID) + 
                           " And Account : " + AccountTool.getDescriptionByID(sAccountID));
                
                if (_oTrans.getDebitCredit() == 1) //DEBIT
                {
                    double dTotalDebit = oGL.getTotalDebitAmount().doubleValue();
                    dTotalDebit = dTotalDebit + dAmount;                    
                    oGL.setTotalDebitAmount(new BigDecimal (dTotalDebit));                    
                }
                else  //CREDIT 
                {
                    double dTotalCredit = oGL.getTotalCreditAmount().doubleValue();                    
                    dTotalCredit = dTotalCredit + dAmount;
                    oGL.setTotalCreditAmount(new BigDecimal (dTotalCredit));                                   
               }
               double dEndBalance = oGL.getEndingBalance().doubleValue();
               dEndBalance = dEndBalance + dTransAmount;              
               
               oGL.setEndingBalance(new BigDecimal (dEndBalance)); 
            }
            oGL.save (_oConn);
            
            //if the transaction is not happened in current period then update all period forward
            //and backward?
            if (!PeriodTool.isCurrentPeriod (sPeriodID))
            {
                List vGLUpdate = getNextGLData (sPeriodID, sAccountID);
                log.debug ("2. List Of GL Data To Update : " + vGLUpdate); 
                updateGLList (_oTrans, vGLUpdate, true, _oConn);                                                                
            }
        }//end if oTrans != null
	}

	//---------------------------------------------------------   
	// rollback methods                                             
	//---------------------------------------------------------   

    /**
     * rollbackGL - this method will rollback GL data. Rollback occured when a transaction edited and updated
     *
     * @param List _vOldTrans, List of GL Transaction To Rollback
     * @param Connection _oConn, Connection object this method must join a transaction
     */

    public static void rollbackGL (List _vOldTrans, Connection _oConn)
        throws Exception
    {
        for (int i = 0; i < _vOldTrans.size(); i++)
        {
            GlTransaction _oOldTrans = (GlTransaction) _vOldTrans.get(i);
            rollbackGL (_oOldTrans, _oConn);
        }
    }

    private static void rollbackGL (GlTransaction _oOldTrans, Connection _oConn)
        throws Exception
    {
        double dEndBalance = 0;
        
        if (_oOldTrans != null)
        {
            String sAccountID   = _oOldTrans.getAccountId();
            String sPeriodID    = _oOldTrans.getPeriodId();
            int iDebitCredit    = _oOldTrans.getDebitCredit();
            double dAmount      = _oOldTrans.getAmountBase().doubleValue();          
            double dTransAmount = AccountTool.checkBalancePlusMinus (sAccountID, iDebitCredit, dAmount );
            
            GeneralLedger oOldGL = getGLData (sPeriodID, sAccountID, "", "", _oConn);

            log.debug ("1. Roll Back GL : " + oOldGL);

            //update old general ledger object according to value in old gl transaction object
            if (oOldGL != null) 
            {
                if (_oOldTrans.getDebitCredit() == 1) //DEBIT
                {
                    double dTotalDebit = oOldGL.getTotalDebitAmount().doubleValue();
                    dTotalDebit = dTotalDebit - dAmount;
                    oOldGL.setTotalDebitAmount(new BigDecimal (dTotalDebit));                    
                }
                else  //CREDIT
                {
                    double dTotalCredit = oOldGL.getTotalCreditAmount().doubleValue();
                    dTotalCredit = dTotalCredit - dAmount;                    
                    oOldGL.setTotalCreditAmount(new BigDecimal (dTotalCredit));                    
                }
                dEndBalance = oOldGL.getEndingBalance().doubleValue();                
                dEndBalance = dEndBalance - dTransAmount;                

                oOldGL.setEndingBalance(new BigDecimal (dEndBalance));                 
                oOldGL.save (_oConn);
                    
                //do rollback and update list of next period account gl data
                if (!PeriodTool.isCurrentPeriod (_oOldTrans.getPeriodId()))
                {
                    List vOldGLRollback = getNextGLData (sPeriodID, sAccountID, _oConn);
                    log.debug ("2. List Of OLD GL To Rollback : " + vOldGLRollback);   
                    updateGLList (_oOldTrans, vOldGLRollback, false, _oConn);                                                                
                }            
            }//end if (_oOldGL != null) 
        }//end if (_oOldTrans != null)        
    }

    /**
     * updateGLList - this method will update opening and ending balance of GL Data list containing 
     * next period gl data since period in gl transaction
     * 
     * @param _oTrans the gltransaction that is happened not in current period (most probably backward transaction)          
     * @param List _vGL, List of GL data (next period since period in transaction until the current period) to update 
     * @param Connection _oConn, Connection object this method must join a transaction
     */
     
	private static void updateGLList(GlTransaction _oTrans, List _vGL, boolean _bAdd, Connection _oConn)
    	throws Exception
    {
        String sAccountID   = _oTrans.getAccountId();
        int iDebitCredit    = _oTrans.getDebitCredit();
        double dAmount      = _oTrans.getAmountBase().doubleValue();            
        double dTransAmount = AccountTool.checkBalancePlusMinus (sAccountID, iDebitCredit, dAmount );                                 

        //get the gl transaction period 
        String sFromPeriodID = _oTrans.getPeriodId();
        
        //get number of period should be updated since from the trans period id
        List vPeriod = PeriodTool.getPeriodIDList (sFromPeriodID);
        
        log.debug ("3. updateGLList : Period List To BE Updated " + vPeriod);
        
        //check whether the number of gl data to update is less than the number period that should be updated
        if (_vGL.size() < vPeriod.size()) 
        {
            log.debug ("4. updateGLList : vGL (" + _vGL.size() + ") != vPeriod (" + vPeriod.size() + ")");

            //try to create not exist gl data in periods found
            createGLData (_oTrans, vPeriod, _oConn);                                
            
            //update the list of gl data passed to this method
            _vGL = getNextGLData (sFromPeriodID, sAccountID, _oConn);
        }
        
		for (int i = 0; i < _vGL.size(); i++)
		{
			GeneralLedger oGL = (GeneralLedger) _vGL.get(i);
            log.debug ("5. updateGLList : Period Updated : " + PeriodTool.getCodeByID(oGL.getPeriodId()));

            double dOpeningBalance = oGL.getOpeningBalance().doubleValue();
            double dEndBalance = oGL.getEndingBalance().doubleValue();
    
            if (_bAdd) 
            {
                dOpeningBalance = dOpeningBalance + dTransAmount;
                dEndBalance = dEndBalance + dTransAmount;
            }
            else 
            {
                dOpeningBalance = dOpeningBalance - dTransAmount;                
                dEndBalance = dEndBalance - dTransAmount;
            }
            oGL.setOpeningBalance(new BigDecimal (dOpeningBalance)); 
            oGL.setEndingBalance(new BigDecimal (dEndBalance)); 
            oGL.save (_oConn);
		}
	}	
}
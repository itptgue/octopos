package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class JournalVoucherDetailMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.JournalVoucherDetailMapBuilder";

    /**
     * Item
     * @deprecated use JournalVoucherDetailPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "journal_voucher_detail";
    }

  
    /**
     * journal_voucher_detail.JOURNAL_VOUCHER_DETAIL_ID
     * @return the column name for the JOURNAL_VOUCHER_DETAIL_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.JOURNAL_VOUCHER_DETAIL_ID constant
     */
    public static String getJournalVoucherDetail_JournalVoucherDetailId()
    {
        return "journal_voucher_detail.JOURNAL_VOUCHER_DETAIL_ID";
    }
  
    /**
     * journal_voucher_detail.JOURNAL_VOUCHER_ID
     * @return the column name for the JOURNAL_VOUCHER_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.JOURNAL_VOUCHER_ID constant
     */
    public static String getJournalVoucherDetail_JournalVoucherId()
    {
        return "journal_voucher_detail.JOURNAL_VOUCHER_ID";
    }
  
    /**
     * journal_voucher_detail.ACCOUNT_ID
     * @return the column name for the ACCOUNT_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.ACCOUNT_ID constant
     */
    public static String getJournalVoucherDetail_AccountId()
    {
        return "journal_voucher_detail.ACCOUNT_ID";
    }
  
    /**
     * journal_voucher_detail.ACCOUNT_CODE
     * @return the column name for the ACCOUNT_CODE field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.ACCOUNT_CODE constant
     */
    public static String getJournalVoucherDetail_AccountCode()
    {
        return "journal_voucher_detail.ACCOUNT_CODE";
    }
  
    /**
     * journal_voucher_detail.ACCOUNT_NAME
     * @return the column name for the ACCOUNT_NAME field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.ACCOUNT_NAME constant
     */
    public static String getJournalVoucherDetail_AccountName()
    {
        return "journal_voucher_detail.ACCOUNT_NAME";
    }
  
    /**
     * journal_voucher_detail.DEBIT_AMOUNT
     * @return the column name for the DEBIT_AMOUNT field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.DEBIT_AMOUNT constant
     */
    public static String getJournalVoucherDetail_DebitAmount()
    {
        return "journal_voucher_detail.DEBIT_AMOUNT";
    }
  
    /**
     * journal_voucher_detail.CREDIT_AMOUNT
     * @return the column name for the CREDIT_AMOUNT field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.CREDIT_AMOUNT constant
     */
    public static String getJournalVoucherDetail_CreditAmount()
    {
        return "journal_voucher_detail.CREDIT_AMOUNT";
    }
  
    /**
     * journal_voucher_detail.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.CURRENCY_ID constant
     */
    public static String getJournalVoucherDetail_CurrencyId()
    {
        return "journal_voucher_detail.CURRENCY_ID";
    }
  
    /**
     * journal_voucher_detail.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.CURRENCY_RATE constant
     */
    public static String getJournalVoucherDetail_CurrencyRate()
    {
        return "journal_voucher_detail.CURRENCY_RATE";
    }
  
    /**
     * journal_voucher_detail.AMOUNT
     * @return the column name for the AMOUNT field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.AMOUNT constant
     */
    public static String getJournalVoucherDetail_Amount()
    {
        return "journal_voucher_detail.AMOUNT";
    }
  
    /**
     * journal_voucher_detail.AMOUNT_BASE
     * @return the column name for the AMOUNT_BASE field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.AMOUNT_BASE constant
     */
    public static String getJournalVoucherDetail_AmountBase()
    {
        return "journal_voucher_detail.AMOUNT_BASE";
    }
  
    /**
     * journal_voucher_detail.PROJECT_ID
     * @return the column name for the PROJECT_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.PROJECT_ID constant
     */
    public static String getJournalVoucherDetail_ProjectId()
    {
        return "journal_voucher_detail.PROJECT_ID";
    }
  
    /**
     * journal_voucher_detail.DEPARTMENT_ID
     * @return the column name for the DEPARTMENT_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.DEPARTMENT_ID constant
     */
    public static String getJournalVoucherDetail_DepartmentId()
    {
        return "journal_voucher_detail.DEPARTMENT_ID";
    }
  
    /**
     * journal_voucher_detail.MEMO
     * @return the column name for the MEMO field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.MEMO constant
     */
    public static String getJournalVoucherDetail_Memo()
    {
        return "journal_voucher_detail.MEMO";
    }
  
    /**
     * journal_voucher_detail.DEBIT_CREDIT
     * @return the column name for the DEBIT_CREDIT field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.DEBIT_CREDIT constant
     */
    public static String getJournalVoucherDetail_DebitCredit()
    {
        return "journal_voucher_detail.DEBIT_CREDIT";
    }
  
    /**
     * journal_voucher_detail.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.LOCATION_ID constant
     */
    public static String getJournalVoucherDetail_LocationId()
    {
        return "journal_voucher_detail.LOCATION_ID";
    }
  
    /**
     * journal_voucher_detail.REFERENCE_NO
     * @return the column name for the REFERENCE_NO field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.REFERENCE_NO constant
     */
    public static String getJournalVoucherDetail_ReferenceNo()
    {
        return "journal_voucher_detail.REFERENCE_NO";
    }
  
    /**
     * journal_voucher_detail.SUB_LEDGER_TYPE
     * @return the column name for the SUB_LEDGER_TYPE field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.SUB_LEDGER_TYPE constant
     */
    public static String getJournalVoucherDetail_SubLedgerType()
    {
        return "journal_voucher_detail.SUB_LEDGER_TYPE";
    }
  
    /**
     * journal_voucher_detail.SUB_LEDGER_ID
     * @return the column name for the SUB_LEDGER_ID field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.SUB_LEDGER_ID constant
     */
    public static String getJournalVoucherDetail_SubLedgerId()
    {
        return "journal_voucher_detail.SUB_LEDGER_ID";
    }
  
    /**
     * journal_voucher_detail.SUB_LEDGER_DESC
     * @return the column name for the SUB_LEDGER_DESC field
     * @deprecated use JournalVoucherDetailPeer.journal_voucher_detail.SUB_LEDGER_DESC constant
     */
    public static String getJournalVoucherDetail_SubLedgerDesc()
    {
        return "journal_voucher_detail.SUB_LEDGER_DESC";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("journal_voucher_detail");
        TableMap tMap = dbMap.getTable("journal_voucher_detail");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("journal_voucher_detail.JOURNAL_VOUCHER_DETAIL_ID", "");
                          tMap.addForeignKey(
                "journal_voucher_detail.JOURNAL_VOUCHER_ID", "" , "journal_voucher" ,
                "journal_voucher_id");
                          tMap.addColumn("journal_voucher_detail.ACCOUNT_ID", "");
                          tMap.addColumn("journal_voucher_detail.ACCOUNT_CODE", "");
                          tMap.addColumn("journal_voucher_detail.ACCOUNT_NAME", "");
                            tMap.addColumn("journal_voucher_detail.DEBIT_AMOUNT", bd_ZERO);
                            tMap.addColumn("journal_voucher_detail.CREDIT_AMOUNT", bd_ZERO);
                          tMap.addColumn("journal_voucher_detail.CURRENCY_ID", "");
                            tMap.addColumn("journal_voucher_detail.CURRENCY_RATE", bd_ZERO);
                            tMap.addColumn("journal_voucher_detail.AMOUNT", bd_ZERO);
                            tMap.addColumn("journal_voucher_detail.AMOUNT_BASE", bd_ZERO);
                          tMap.addColumn("journal_voucher_detail.PROJECT_ID", "");
                          tMap.addColumn("journal_voucher_detail.DEPARTMENT_ID", "");
                          tMap.addColumn("journal_voucher_detail.MEMO", "");
                            tMap.addColumn("journal_voucher_detail.DEBIT_CREDIT", Integer.valueOf(0));
                          tMap.addColumn("journal_voucher_detail.LOCATION_ID", "");
                          tMap.addColumn("journal_voucher_detail.REFERENCE_NO", "");
                            tMap.addColumn("journal_voucher_detail.SUB_LEDGER_TYPE", Integer.valueOf(0));
                          tMap.addColumn("journal_voucher_detail.SUB_LEDGER_ID", "");
                          tMap.addColumn("journal_voucher_detail.SUB_LEDGER_DESC", "");
          }
}

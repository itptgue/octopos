package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PurchaseReceiptMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PurchaseReceiptMapBuilder";

    /**
     * Item
     * @deprecated use PurchaseReceiptPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "purchase_receipt";
    }

  
    /**
     * purchase_receipt.PURCHASE_RECEIPT_ID
     * @return the column name for the PURCHASE_RECEIPT_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.PURCHASE_RECEIPT_ID constant
     */
    public static String getPurchaseReceipt_PurchaseReceiptId()
    {
        return "purchase_receipt.PURCHASE_RECEIPT_ID";
    }
  
    /**
     * purchase_receipt.PURCHASE_ORDER_ID
     * @return the column name for the PURCHASE_ORDER_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.PURCHASE_ORDER_ID constant
     */
    public static String getPurchaseReceipt_PurchaseOrderId()
    {
        return "purchase_receipt.PURCHASE_ORDER_ID";
    }
  
    /**
     * purchase_receipt.VENDOR_DELIVERY_NO
     * @return the column name for the VENDOR_DELIVERY_NO field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.VENDOR_DELIVERY_NO constant
     */
    public static String getPurchaseReceipt_VendorDeliveryNo()
    {
        return "purchase_receipt.VENDOR_DELIVERY_NO";
    }
  
    /**
     * purchase_receipt.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.LOCATION_ID constant
     */
    public static String getPurchaseReceipt_LocationId()
    {
        return "purchase_receipt.LOCATION_ID";
    }
  
    /**
     * purchase_receipt.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.LOCATION_NAME constant
     */
    public static String getPurchaseReceipt_LocationName()
    {
        return "purchase_receipt.LOCATION_NAME";
    }
  
    /**
     * purchase_receipt.RECEIPT_NO
     * @return the column name for the RECEIPT_NO field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.RECEIPT_NO constant
     */
    public static String getPurchaseReceipt_ReceiptNo()
    {
        return "purchase_receipt.RECEIPT_NO";
    }
  
    /**
     * purchase_receipt.RECEIPT_DATE
     * @return the column name for the RECEIPT_DATE field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.RECEIPT_DATE constant
     */
    public static String getPurchaseReceipt_ReceiptDate()
    {
        return "purchase_receipt.RECEIPT_DATE";
    }
  
    /**
     * purchase_receipt.VENDOR_ID
     * @return the column name for the VENDOR_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.VENDOR_ID constant
     */
    public static String getPurchaseReceipt_VendorId()
    {
        return "purchase_receipt.VENDOR_ID";
    }
  
    /**
     * purchase_receipt.VENDOR_NAME
     * @return the column name for the VENDOR_NAME field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.VENDOR_NAME constant
     */
    public static String getPurchaseReceipt_VendorName()
    {
        return "purchase_receipt.VENDOR_NAME";
    }
  
    /**
     * purchase_receipt.CREATE_BY
     * @return the column name for the CREATE_BY field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CREATE_BY constant
     */
    public static String getPurchaseReceipt_CreateBy()
    {
        return "purchase_receipt.CREATE_BY";
    }
  
    /**
     * purchase_receipt.CONFIRM_BY
     * @return the column name for the CONFIRM_BY field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CONFIRM_BY constant
     */
    public static String getPurchaseReceipt_ConfirmBy()
    {
        return "purchase_receipt.CONFIRM_BY";
    }
  
    /**
     * purchase_receipt.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CREATE_DATE constant
     */
    public static String getPurchaseReceipt_CreateDate()
    {
        return "purchase_receipt.CREATE_DATE";
    }
  
    /**
     * purchase_receipt.CONFIRM_DATE
     * @return the column name for the CONFIRM_DATE field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CONFIRM_DATE constant
     */
    public static String getPurchaseReceipt_ConfirmDate()
    {
        return "purchase_receipt.CONFIRM_DATE";
    }
  
    /**
     * purchase_receipt.STATUS
     * @return the column name for the STATUS field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.STATUS constant
     */
    public static String getPurchaseReceipt_Status()
    {
        return "purchase_receipt.STATUS";
    }
  
    /**
     * purchase_receipt.TOTAL_QTY
     * @return the column name for the TOTAL_QTY field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.TOTAL_QTY constant
     */
    public static String getPurchaseReceipt_TotalQty()
    {
        return "purchase_receipt.TOTAL_QTY";
    }
  
    /**
     * purchase_receipt.REMARK
     * @return the column name for the REMARK field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.REMARK constant
     */
    public static String getPurchaseReceipt_Remark()
    {
        return "purchase_receipt.REMARK";
    }
  
    /**
     * purchase_receipt.TOTAL_DISCOUNT_PCT
     * @return the column name for the TOTAL_DISCOUNT_PCT field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.TOTAL_DISCOUNT_PCT constant
     */
    public static String getPurchaseReceipt_TotalDiscountPct()
    {
        return "purchase_receipt.TOTAL_DISCOUNT_PCT";
    }
  
    /**
     * purchase_receipt.PAYMENT_TYPE_ID
     * @return the column name for the PAYMENT_TYPE_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.PAYMENT_TYPE_ID constant
     */
    public static String getPurchaseReceipt_PaymentTypeId()
    {
        return "purchase_receipt.PAYMENT_TYPE_ID";
    }
  
    /**
     * purchase_receipt.PAYMENT_TERM_ID
     * @return the column name for the PAYMENT_TERM_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.PAYMENT_TERM_ID constant
     */
    public static String getPurchaseReceipt_PaymentTermId()
    {
        return "purchase_receipt.PAYMENT_TERM_ID";
    }
  
    /**
     * purchase_receipt.CURRENCY_ID
     * @return the column name for the CURRENCY_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CURRENCY_ID constant
     */
    public static String getPurchaseReceipt_CurrencyId()
    {
        return "purchase_receipt.CURRENCY_ID";
    }
  
    /**
     * purchase_receipt.CURRENCY_RATE
     * @return the column name for the CURRENCY_RATE field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CURRENCY_RATE constant
     */
    public static String getPurchaseReceipt_CurrencyRate()
    {
        return "purchase_receipt.CURRENCY_RATE";
    }
  
    /**
     * purchase_receipt.IS_BILL
     * @return the column name for the IS_BILL field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.IS_BILL constant
     */
    public static String getPurchaseReceipt_IsBill()
    {
        return "purchase_receipt.IS_BILL";
    }
  
    /**
     * purchase_receipt.COURIER_ID
     * @return the column name for the COURIER_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.COURIER_ID constant
     */
    public static String getPurchaseReceipt_CourierId()
    {
        return "purchase_receipt.COURIER_ID";
    }
  
    /**
     * purchase_receipt.FOB_ID
     * @return the column name for the FOB_ID field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.FOB_ID constant
     */
    public static String getPurchaseReceipt_FobId()
    {
        return "purchase_receipt.FOB_ID";
    }
  
    /**
     * purchase_receipt.ESTIMATED_FREIGHT
     * @return the column name for the ESTIMATED_FREIGHT field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.ESTIMATED_FREIGHT constant
     */
    public static String getPurchaseReceipt_EstimatedFreight()
    {
        return "purchase_receipt.ESTIMATED_FREIGHT";
    }
  
    /**
     * purchase_receipt.IS_TAXABLE
     * @return the column name for the IS_TAXABLE field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.IS_TAXABLE constant
     */
    public static String getPurchaseReceipt_IsTaxable()
    {
        return "purchase_receipt.IS_TAXABLE";
    }
  
    /**
     * purchase_receipt.IS_INCLUSIVE_TAX
     * @return the column name for the IS_INCLUSIVE_TAX field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.IS_INCLUSIVE_TAX constant
     */
    public static String getPurchaseReceipt_IsInclusiveTax()
    {
        return "purchase_receipt.IS_INCLUSIVE_TAX";
    }
  
    /**
     * purchase_receipt.FISCAL_RATE
     * @return the column name for the FISCAL_RATE field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.FISCAL_RATE constant
     */
    public static String getPurchaseReceipt_FiscalRate()
    {
        return "purchase_receipt.FISCAL_RATE";
    }
  
    /**
     * purchase_receipt.CANCEL_BY
     * @return the column name for the CANCEL_BY field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CANCEL_BY constant
     */
    public static String getPurchaseReceipt_CancelBy()
    {
        return "purchase_receipt.CANCEL_BY";
    }
  
    /**
     * purchase_receipt.CANCEL_DATE
     * @return the column name for the CANCEL_DATE field
     * @deprecated use PurchaseReceiptPeer.purchase_receipt.CANCEL_DATE constant
     */
    public static String getPurchaseReceipt_CancelDate()
    {
        return "purchase_receipt.CANCEL_DATE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("purchase_receipt");
        TableMap tMap = dbMap.getTable("purchase_receipt");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("purchase_receipt.PURCHASE_RECEIPT_ID", "");
                          tMap.addColumn("purchase_receipt.PURCHASE_ORDER_ID", "");
                          tMap.addColumn("purchase_receipt.VENDOR_DELIVERY_NO", "");
                          tMap.addColumn("purchase_receipt.LOCATION_ID", "");
                          tMap.addColumn("purchase_receipt.LOCATION_NAME", "");
                          tMap.addColumn("purchase_receipt.RECEIPT_NO", "");
                          tMap.addColumn("purchase_receipt.RECEIPT_DATE", new Date());
                          tMap.addColumn("purchase_receipt.VENDOR_ID", "");
                          tMap.addColumn("purchase_receipt.VENDOR_NAME", "");
                          tMap.addColumn("purchase_receipt.CREATE_BY", "");
                          tMap.addColumn("purchase_receipt.CONFIRM_BY", "");
                          tMap.addColumn("purchase_receipt.CREATE_DATE", new Date());
                          tMap.addColumn("purchase_receipt.CONFIRM_DATE", new Date());
                            tMap.addColumn("purchase_receipt.STATUS", Integer.valueOf(0));
                            tMap.addColumn("purchase_receipt.TOTAL_QTY", bd_ZERO);
                          tMap.addColumn("purchase_receipt.REMARK", "");
                          tMap.addColumn("purchase_receipt.TOTAL_DISCOUNT_PCT", "");
                          tMap.addColumn("purchase_receipt.PAYMENT_TYPE_ID", "");
                          tMap.addColumn("purchase_receipt.PAYMENT_TERM_ID", "");
                          tMap.addColumn("purchase_receipt.CURRENCY_ID", "");
                            tMap.addColumn("purchase_receipt.CURRENCY_RATE", bd_ZERO);
                          tMap.addColumn("purchase_receipt.IS_BILL", Boolean.TRUE);
                          tMap.addColumn("purchase_receipt.COURIER_ID", "");
                          tMap.addColumn("purchase_receipt.FOB_ID", "");
                            tMap.addColumn("purchase_receipt.ESTIMATED_FREIGHT", bd_ZERO);
                          tMap.addColumn("purchase_receipt.IS_TAXABLE", Boolean.TRUE);
                          tMap.addColumn("purchase_receipt.IS_INCLUSIVE_TAX", Boolean.TRUE);
                            tMap.addColumn("purchase_receipt.FISCAL_RATE", bd_ZERO);
                          tMap.addColumn("purchase_receipt.CANCEL_BY", "");
                          tMap.addColumn("purchase_receipt.CANCEL_DATE", new Date());
          }
}

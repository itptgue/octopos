package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to AccountReceivable
 */
public abstract class BaseAccountReceivable extends BaseObject
{
    /** The Peer class */
    private static final AccountReceivablePeer peer =
        new AccountReceivablePeer();

        
    /** The value for the accountReceivableId field */
    private String accountReceivableId;
      
    /** The value for the transactionId field */
    private String transactionId;
      
    /** The value for the transactionNo field */
    private String transactionNo;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the transactionType field */
    private int transactionType;
      
    /** The value for the customerId field */
    private String customerId;
      
    /** The value for the customerName field */
    private String customerName;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the amount field */
    private BigDecimal amount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
      
    /** The value for the dueDate field */
    private Date dueDate;
                                                
    /** The value for the remark field */
    private String remark = "";
                                                
    /** The value for the locationId field */
    private String locationId = "";
  
    
    /**
     * Get the AccountReceivableId
     *
     * @return String
     */
    public String getAccountReceivableId()
    {
        return accountReceivableId;
    }

                        
    /**
     * Set the value of AccountReceivableId
     *
     * @param v new value
     */
    public void setAccountReceivableId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.accountReceivableId, v))
              {
            this.accountReceivableId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerId
     *
     * @return String
     */
    public String getCustomerId()
    {
        return customerId;
    }

                        
    /**
     * Set the value of CustomerId
     *
     * @param v new value
     */
    public void setCustomerId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerId, v))
              {
            this.customerId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CustomerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

                        
    /**
     * Set the value of CustomerName
     *
     * @param v new value
     */
    public void setCustomerName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.customerName, v))
              {
            this.customerName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Amount
     *
     * @return BigDecimal
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

                        
    /**
     * Set the value of Amount
     *
     * @param v new value
     */
    public void setAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amount, v))
              {
            this.amount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Remark
     *
     * @return String
     */
    public String getRemark()
    {
        return remark;
    }

                        
    /**
     * Set the value of Remark
     *
     * @param v new value
     */
    public void setRemark(String v) 
    {
    
                  if (!ObjectUtils.equals(this.remark, v))
              {
            this.remark = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AccountReceivableId");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionDate");
              fieldNames.add("TransactionType");
              fieldNames.add("CustomerId");
              fieldNames.add("CustomerName");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("Amount");
              fieldNames.add("AmountBase");
              fieldNames.add("DueDate");
              fieldNames.add("Remark");
              fieldNames.add("LocationId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AccountReceivableId"))
        {
                return getAccountReceivableId();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("CustomerId"))
        {
                return getCustomerId();
            }
          if (name.equals("CustomerName"))
        {
                return getCustomerName();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("Amount"))
        {
                return getAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("Remark"))
        {
                return getRemark();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AccountReceivablePeer.ACCOUNT_RECEIVABLE_ID))
        {
                return getAccountReceivableId();
            }
          if (name.equals(AccountReceivablePeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(AccountReceivablePeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(AccountReceivablePeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(AccountReceivablePeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(AccountReceivablePeer.CUSTOMER_ID))
        {
                return getCustomerId();
            }
          if (name.equals(AccountReceivablePeer.CUSTOMER_NAME))
        {
                return getCustomerName();
            }
          if (name.equals(AccountReceivablePeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(AccountReceivablePeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(AccountReceivablePeer.AMOUNT))
        {
                return getAmount();
            }
          if (name.equals(AccountReceivablePeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(AccountReceivablePeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(AccountReceivablePeer.REMARK))
        {
                return getRemark();
            }
          if (name.equals(AccountReceivablePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getAccountReceivableId();
            }
              if (pos == 1)
        {
                return getTransactionId();
            }
              if (pos == 2)
        {
                return getTransactionNo();
            }
              if (pos == 3)
        {
                return getTransactionDate();
            }
              if (pos == 4)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 5)
        {
                return getCustomerId();
            }
              if (pos == 6)
        {
                return getCustomerName();
            }
              if (pos == 7)
        {
                return getCurrencyId();
            }
              if (pos == 8)
        {
                return getCurrencyRate();
            }
              if (pos == 9)
        {
                return getAmount();
            }
              if (pos == 10)
        {
                return getAmountBase();
            }
              if (pos == 11)
        {
                return getDueDate();
            }
              if (pos == 12)
        {
                return getRemark();
            }
              if (pos == 13)
        {
                return getLocationId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AccountReceivablePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        AccountReceivablePeer.doInsert((AccountReceivable) this, con);
                        setNew(false);
                    }
                    else
                    {
                        AccountReceivablePeer.doUpdate((AccountReceivable) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key accountReceivableId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setAccountReceivableId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setAccountReceivableId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getAccountReceivableId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AccountReceivable copy() throws TorqueException
    {
        return copyInto(new AccountReceivable());
    }
  
    protected AccountReceivable copyInto(AccountReceivable copyObj) throws TorqueException
    {
          copyObj.setAccountReceivableId(accountReceivableId);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setTransactionType(transactionType);
          copyObj.setCustomerId(customerId);
          copyObj.setCustomerName(customerName);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setAmount(amount);
          copyObj.setAmountBase(amountBase);
          copyObj.setDueDate(dueDate);
          copyObj.setRemark(remark);
          copyObj.setLocationId(locationId);
  
                    copyObj.setAccountReceivableId((String)null);
                                                                                          
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AccountReceivablePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("AccountReceivable\n");
        str.append("-----------------\n")
           .append("AccountReceivableId  : ")
           .append(getAccountReceivableId())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("CustomerId           : ")
           .append(getCustomerId())
           .append("\n")
           .append("CustomerName         : ")
           .append(getCustomerName())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("Amount               : ")
           .append(getAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("Remark               : ")
           .append(getRemark())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
        ;
        return(str.toString());
    }
}

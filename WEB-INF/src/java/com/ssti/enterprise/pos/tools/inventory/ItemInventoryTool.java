package com.ssti.enterprise.pos.tools.inventory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.ItemInventoryManager;
import com.ssti.enterprise.pos.manager.ItemManager;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemInventory;
import com.ssti.enterprise.pos.om.ItemInventoryPeer;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.SalesTransactionDetailPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.enterprise.pos.tools.SynchronizationTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseAnalysisTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/tools/inventory/ItemInventoryTool.java,v $
 * Purpose: Item Inventory OM Controller, count reorder point.
 *
 * @author  $Author: albert $
 * @version $Id: ItemInventoryTool.java,v 1.9 2009/05/04 02:04:27 albert Exp $
 *
 * $Log: ItemInventoryTool.java,v $
 *
 * * 2015-09-25
 * - Change in PurchaseAnalysis.getPurchaseLeadTime, Now return List of Record 
 */
public class ItemInventoryTool extends BaseTool 
{
	private static Log log = LogFactory.getLog(ItemInventoryTool.class);

	public static List getAllItemInventory (String _sLocationID) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(ItemInventoryPeer.LOCATION_ID, _sLocationID);
		}
		return ItemInventoryPeer.doSelect (oCrit);
	}

	public static ItemInventory getItemInventoryByID (String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(ItemInventoryPeer.ITEM_INVENTORY_ID, _sID);
		oCrit.setLimit(1);				

		List vData = ItemInventoryPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (ItemInventory) vData.get(0);
		}
		return null;        
	}

	public static ItemInventory getItemInventory (String _sItemID, String _sLocationID )
		throws Exception
	{
		try
		{
			return ItemInventoryManager.getInstance().getItemInventory(_sItemID, _sLocationID);        			
		}
		catch (Exception e)
		{
			log.error(e);
			e.printStackTrace();
		}
		return null;
	}

	public static ItemInventory getSKUInventory (String _sSKU, String _sLocationID )
		throws Exception
	{
		StringBuilder oSQL = new StringBuilder();
		oSQL.append ("SELECT i.item_sku, SUM(minimum_qty), SUM(reorder_point), SUM(maximum_qty) ")
		.append (" FROM item i, item_inventory in WHERE ")
		.append (" i.item_id = in.item_id ")
		.append (" AND i.item_sku = '").append(_sSKU).append("' ")
		.append (" AND in.location_id = '").append(_sLocationID).append("' ")
		.append (" GROUP BY i.item_sku ");

		List vData = BasePeer.executeQuery(oSQL.toString());
		if (vData.size() > 0)
		{
			Record oRec = (Record) vData.get(0);
			ItemInventory oItemInv = new ItemInventory();
			oItemInv.setMinimumQty(oRec.getValue(2).asBigDecimal());
			oItemInv.setReorderPoint(oRec.getValue(3).asBigDecimal());
			oItemInv.setMaximumQty(oRec.getValue(4).asBigDecimal());
			return oItemInv;
		}
		return null;        
	}    

	public static List findItemInv(String _sKatID, String _sLocID, String _sVendorID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sLocID))
		{
			oCrit.add(ItemInventoryPeer.LOCATION_ID, _sLocID);
		}
		if (StringUtil.isNotEmpty(_sKatID))
		{
			List vKatID = KategoriTool.getChildIDList(_sKatID);
			vKatID.add(_sKatID);
			oCrit.addJoin(ItemInventoryPeer.ITEM_ID, ItemPeer.ITEM_ID);
			oCrit.addIn(ItemPeer.KATEGORI_ID, vKatID);
		}		
		if (StringUtil.isNotEmpty(_sVendorID))
		{
			oCrit.addJoin(ItemInventoryPeer.ITEM_ID, ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.PREFERED_VENDOR_ID, _sVendorID);			
		}
		oCrit.addAscendingOrderByColumn(ItemInventoryPeer.LOCATION_ID);
		oCrit.addAscendingOrderByColumn(ItemInventoryPeer.RACK_LOCATION);
		oCrit.addAscendingOrderByColumn(ItemInventoryPeer.ITEM_CODE);				
		return ItemInventoryPeer.doSelect(oCrit);	
	}
	
	public static void saveNew(String _sLocationID,
							   String _sLocationName,
							   String _sItemID,
							   String _sItemCode,
							   BigDecimal _dMaxQty,
							   BigDecimal _dMinQty,
							   BigDecimal _dReorderQty,
							   String _sRack,
							   boolean _bActive)
		throws Exception
	{
		ItemInventory oItemInventory = new ItemInventory();
		oItemInventory.setItemInventoryId(IDGenerator.generateSysID());
		oItemInventory.setLocationId(_sLocationID);
		oItemInventory.setLocationName(_sLocationName);
		oItemInventory.setItemId(_sItemID);
		oItemInventory.setItemCode(_sItemCode);
		oItemInventory.setMaximumQty(_dMaxQty);
		oItemInventory.setMinimumQty(_dMinQty);
		oItemInventory.setReorderPoint(_dReorderQty);
		oItemInventory.setRackLocation(_sRack);
		oItemInventory.setActive(_bActive);
		oItemInventory.setUpdateDate(new Date());
		oItemInventory.save();
		ItemInventoryManager.getInstance().refreshCache(oItemInventory);
	}

	public static void updateItemInventoryByID(String _sID,
			BigDecimal _dMaxQty,
			BigDecimal _dMinQty,
			BigDecimal _dReorderQty,
			String _sRack,
			boolean _bActive)
		throws Exception
	{
		ItemInventory oItemInventory = getItemInventoryByID(_sID, null);
		if(oItemInventory != null)
		{
			oItemInventory.setMaximumQty(_dMaxQty);
			oItemInventory.setMinimumQty(_dMinQty);
			oItemInventory.setReorderPoint(_dReorderQty);
			oItemInventory.setRackLocation(_sRack);
			oItemInventory.setActive(_bActive);
			oItemInventory.setUpdateDate(new Date());
			oItemInventory.save();
			ItemInventoryManager.getInstance().refreshCache(oItemInventory);
		}
	}

	/**
	 * 
	 * @param _sItemID
	 * @param _sLocationID
	 * @param _dStart
	 * @param _dEnd
	 * @return
	 * @throws Exception
	 */
	 public static double getTotalSold (String _sItemID, 
			 							String _sLocationID,
			 							Date _dStart,
			 							Date _dEnd)
         throws Exception
	 {    
		 //get total qty base sold in a certain range of time defined by formula
		 StringBuilder oSQL = new StringBuilder ();
		 oSQL.append ("SELECT SUM(td.qty_base - td.returned_qty) ");
		 oSQL.append (" FROM sales_transaction t,sales_transaction_detail td");
		 oSQL.append (" WHERE ");
		 oSQL.append (" t.transaction_date >= ");
		 oSQL.append (oDB.getDateString(_dStart)); 
		 oSQL.append (" AND t.transaction_date <= ");
		 oSQL.append (oDB.getDateString(_dEnd));

		 //if not in head office, then filter by location id, else no need
		 //because we want total item sold in all store
		 if(!( PreferenceTool.getLocationFunction() == i_HEAD_OFFICE && 
				 PreferenceTool.getLocationID().equals(_sLocationID) ) )
		 {
			 oSQL.append (" AND t.location_id = '");
			 oSQL.append (_sLocationID);
			 oSQL.append ("'");
		 }
		 oSQL.append (" AND t.sales_transaction_id = td.sales_transaction_id");
		 oSQL.append (" AND td.item_id = '");
		 oSQL.append (_sItemID);
		 oSQL.append ("'");

		 log.debug (oSQL.toString());

		 List vData = SalesTransactionDetailPeer.executeQuery(oSQL.toString());
		 Record oData = (Record) vData.get(0);
		 return oData.getValue(1).asDouble();

	 }

	 /**
	  * 
	  * @param _sItemID
	  * @param _sLocID
	  * @param _dStart
	  * @param _dEnd
	  * @return
	  * @throws Exception
	  */
	 public static int getLeadTime (String _sItemID, 
			 						String _sLocID, 
			 						Date _dStart, 
			 						Date _dEnd)
		throws Exception
	 {
		 int iLeadTime = 1;

		 //count LeadTime if in HO 
		 if(PreferenceTool.getLocationFunction() == i_HEAD_OFFICE  && 
				 PreferenceTool.getLocationID().equals(_sLocID) )
		 {
			 Item oItem = ItemTool.getItemByID(_sItemID);

			 if (oItem != null)
			 {
				 List vLeadTime = PurchaseAnalysisTool.getPurchaseLeadTime (_dStart, _dEnd, _sItemID, oItem.getPreferedVendorId(), true);
				 				 
				 //get average lead time for an item from vendor since each item might have 
				 //different lead time per PO -> receipt
				 for(int i = 0; i < vLeadTime.size(); i++)
				 {
//					 List oLeadTimeData = (List) vLeadTime.get(i);
//					 Date oOrder = (Date) oLeadTimeData.get(i_ORDERED_COL);
//					 Date oReceipt = (Date) oLeadTimeData.get(i_RECEIVED_COL);
//					 int iRangeLead = DateUtil.countDaysinRange(oOrder,oReceipt);
//					 iLeadTime = iLeadTime + iRangeLead;

					 Record oRec = (Record) vLeadTime.get(i);
					 Date oOrd = oRec.getValue("").asDate();
					 Date oRcp = oRec.getValue("").asDate();
					 int iRange = DateUtil.countDaysinRange(oOrd,oRcp);
					 iLeadTime = iLeadTime + iRange;
					 
				 }
				 if(iLeadTime > 1)
				 {
					 iLeadTime = iLeadTime / vLeadTime.size();
				 }
			 }
		 }
		 else //if in store
		 {
			 Location oLoc = LocationTool.getLocationByID(_sLocID);
			 iLeadTime = oLoc.getHoStoreLeadtime();
		 }    
		 return iLeadTime;
	 }


	 /**
	  * 
	  * @param sItemID
	  * @param sLocationID
	  * @return
	  * @throws Exception
	  */
	 //TODO: method to watch
	 public static BigDecimal calculateReorderPoint (String sItemID, 
			 String sLocationID,
			 Date _dStart,
			 Date _dEnd)
					 throws Exception
	 {
		 double dTotalAVG = 0;
		 double dTotalReorder = 0;

		 double dTotalQty = getTotalSold (sItemID, sLocationID, _dStart, _dEnd);
		 int iDays = DateUtil.countDaysinRange(_dStart, _dEnd);

		 //count the average sales
		 if(iDays > 0)
		 {
			 dTotalAVG = dTotalQty / iDays;
		 }

		 int iLeadTime = getLeadTime(sItemID, sLocationID, _dStart, _dEnd);
		 dTotalReorder = iLeadTime * dTotalAVG;
		 return new BigDecimal(dTotalReorder);
	 }    

	 public static double getOrderQty (String _sItemID, String sLocationID)
			 throws Exception
	 {
		 double dQty = 1;
		 double dQoH = InventoryLocationTool.getInventoryOnHand(_sItemID, sLocationID).doubleValue();
		 ItemInventory oItemInv = ItemInventoryTool.getItemInventory(_sItemID, sLocationID);				
		 if( oItemInv != null )
		 {
			 double dMax = oItemInv.getMaximumQty().doubleValue();
			 double dMin = oItemInv.getMinimumQty().doubleValue();
			 double dReo = oItemInv.getReorderPoint().doubleValue();

			 if (dQoH < dReo) //if current qty is < reorder point
			 {
				 if (dMax > 0 && dMax > dQoH) //if max qty is set correctly
				 {
					 //TODO: find better formula, current is max - QoH
					 dQty = dMax - dQoH; 
					 if (dQty == 0) dQty = 1;
				 }
			 }						
		 }
		 return dQty;
	 }

	 ///////////////////////////////////////////////////////////////////////////
	 // sync methods
	 ///////////////////////////////////////////////////////////////////////////

	 /**
	  * 
	  * @return updated item inventory since last ho 2 store
	  * @throws Exception
	  */
	 public static List getUpdatedItemInventory()
			 throws Exception
	 {
		 Date dLast = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		 Criteria oCrit = new Criteria();
		 if (dLast != null) 
		 {
			 oCrit.add(ItemInventoryPeer.UPDATE_DATE, dLast, Criteria.GREATER_EQUAL);   
		 }
		 return ItemInventoryPeer.doSelect(oCrit);
	 }	

	 /**
	  * 
	  * @return updated item inventory since last ho 2 store
	  * @throws Exception
	  */
	 public static List getStoreData()
			 throws Exception
	 {        
		 Date dLast = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		 Criteria oCrit = new Criteria();
		 if (dLast != null) 
		 {
			 oCrit.add(ItemInventoryPeer.UPDATE_DATE, dLast, Criteria.GREATER_EQUAL);   
		 }
		 return ItemInventoryPeer.doSelect(oCrit);
	 }   

	 /**
	  * @param _sItemID
	  * @param _sLocationID
	  * @param _iDays
	  * @param _dToDate
	  * @return
	  * @throws Exception
	  */
	 public static double getAverageSalesPerDay (String _sItemID, String _sLocationID, int _iDays, Date _dToDate)
		 throws Exception
	 {		
		 return ItemManager.getInstance().getAvgSales(_sItemID,_sLocationID,_iDays,_dToDate);
	 }

}

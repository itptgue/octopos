package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class CustomerContactMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.CustomerContactMapBuilder";

    /**
     * Item
     * @deprecated use CustomerContactPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "customer_contact";
    }

  
    /**
     * customer_contact.CUSTOMER_CONTACT_ID
     * @return the column name for the CUSTOMER_CONTACT_ID field
     * @deprecated use CustomerContactPeer.customer_contact.CUSTOMER_CONTACT_ID constant
     */
    public static String getCustomerContact_CustomerContactId()
    {
        return "customer_contact.CUSTOMER_CONTACT_ID";
    }
  
    /**
     * customer_contact.CUSTOMER_ID
     * @return the column name for the CUSTOMER_ID field
     * @deprecated use CustomerContactPeer.customer_contact.CUSTOMER_ID constant
     */
    public static String getCustomerContact_CustomerId()
    {
        return "customer_contact.CUSTOMER_ID";
    }
  
    /**
     * customer_contact.CONTACT_NAME
     * @return the column name for the CONTACT_NAME field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_NAME constant
     */
    public static String getCustomerContact_ContactName()
    {
        return "customer_contact.CONTACT_NAME";
    }
  
    /**
     * customer_contact.CONTACT_PHONE
     * @return the column name for the CONTACT_PHONE field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_PHONE constant
     */
    public static String getCustomerContact_ContactPhone()
    {
        return "customer_contact.CONTACT_PHONE";
    }
  
    /**
     * customer_contact.CONTACT_MOBILE
     * @return the column name for the CONTACT_MOBILE field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_MOBILE constant
     */
    public static String getCustomerContact_ContactMobile()
    {
        return "customer_contact.CONTACT_MOBILE";
    }
  
    /**
     * customer_contact.CONTACT_EMAIL
     * @return the column name for the CONTACT_EMAIL field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_EMAIL constant
     */
    public static String getCustomerContact_ContactEmail()
    {
        return "customer_contact.CONTACT_EMAIL";
    }
  
    /**
     * customer_contact.CONTACT_JOB_TITLE
     * @return the column name for the CONTACT_JOB_TITLE field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_JOB_TITLE constant
     */
    public static String getCustomerContact_ContactJobTitle()
    {
        return "customer_contact.CONTACT_JOB_TITLE";
    }
  
    /**
     * customer_contact.CONTACT_DEPT
     * @return the column name for the CONTACT_DEPT field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_DEPT constant
     */
    public static String getCustomerContact_ContactDept()
    {
        return "customer_contact.CONTACT_DEPT";
    }
  
    /**
     * customer_contact.CONTACT_BIRTH_PLACE
     * @return the column name for the CONTACT_BIRTH_PLACE field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_BIRTH_PLACE constant
     */
    public static String getCustomerContact_ContactBirthPlace()
    {
        return "customer_contact.CONTACT_BIRTH_PLACE";
    }
  
    /**
     * customer_contact.CONTACT_BIRTH_DATE
     * @return the column name for the CONTACT_BIRTH_DATE field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_BIRTH_DATE constant
     */
    public static String getCustomerContact_ContactBirthDate()
    {
        return "customer_contact.CONTACT_BIRTH_DATE";
    }
  
    /**
     * customer_contact.CONTACT_RELIGION
     * @return the column name for the CONTACT_RELIGION field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_RELIGION constant
     */
    public static String getCustomerContact_ContactReligion()
    {
        return "customer_contact.CONTACT_RELIGION";
    }
  
    /**
     * customer_contact.CONTACT_REMARK
     * @return the column name for the CONTACT_REMARK field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_REMARK constant
     */
    public static String getCustomerContact_ContactRemark()
    {
        return "customer_contact.CONTACT_REMARK";
    }
  
    /**
     * customer_contact.CONTACT_SM1
     * @return the column name for the CONTACT_SM1 field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_SM1 constant
     */
    public static String getCustomerContact_ContactSm1()
    {
        return "customer_contact.CONTACT_SM1";
    }
  
    /**
     * customer_contact.CONTACT_SM2
     * @return the column name for the CONTACT_SM2 field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_SM2 constant
     */
    public static String getCustomerContact_ContactSm2()
    {
        return "customer_contact.CONTACT_SM2";
    }
  
    /**
     * customer_contact.CONTACT_SM3
     * @return the column name for the CONTACT_SM3 field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_SM3 constant
     */
    public static String getCustomerContact_ContactSm3()
    {
        return "customer_contact.CONTACT_SM3";
    }
  
    /**
     * customer_contact.CONTACT_JSON
     * @return the column name for the CONTACT_JSON field
     * @deprecated use CustomerContactPeer.customer_contact.CONTACT_JSON constant
     */
    public static String getCustomerContact_ContactJson()
    {
        return "customer_contact.CONTACT_JSON";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("customer_contact");
        TableMap tMap = dbMap.getTable("customer_contact");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("customer_contact.CUSTOMER_CONTACT_ID", "");
                          tMap.addForeignKey(
                "customer_contact.CUSTOMER_ID", "" , "customer" ,
                "customer_id");
                          tMap.addColumn("customer_contact.CONTACT_NAME", "");
                          tMap.addColumn("customer_contact.CONTACT_PHONE", "");
                          tMap.addColumn("customer_contact.CONTACT_MOBILE", "");
                          tMap.addColumn("customer_contact.CONTACT_EMAIL", "");
                          tMap.addColumn("customer_contact.CONTACT_JOB_TITLE", "");
                          tMap.addColumn("customer_contact.CONTACT_DEPT", "");
                          tMap.addColumn("customer_contact.CONTACT_BIRTH_PLACE", "");
                          tMap.addColumn("customer_contact.CONTACT_BIRTH_DATE", new Date());
                          tMap.addColumn("customer_contact.CONTACT_RELIGION", "");
                          tMap.addColumn("customer_contact.CONTACT_REMARK", "");
                          tMap.addColumn("customer_contact.CONTACT_SM1", "");
                          tMap.addColumn("customer_contact.CONTACT_SM2", "");
                          tMap.addColumn("customer_contact.CONTACT_SM3", "");
                          tMap.addColumn("customer_contact.CONTACT_JSON", "");
          }
}

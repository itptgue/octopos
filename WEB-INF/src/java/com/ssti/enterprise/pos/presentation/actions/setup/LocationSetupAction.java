package com.ssti.enterprise.pos.presentation.actions.setup;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.excel.helper.LocationLoader;
import com.ssti.enterprise.pos.manager.LocationManager;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.LocationPeer;
import com.ssti.enterprise.pos.presentation.actions.company.CompanySecureAction;
import com.ssti.enterprise.pos.tools.AppAttributes;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.UpdateHistoryTool;
import com.ssti.framework.tools.SqlUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-03
 * - change method doUpdate & doDelete call UpdateHistoryTool.createHistory and createDelHistory
 * </pre><br>
 */
public class LocationSetupAction extends CompanySecureAction
{
    public void doPerform(RunData data, Context context)
    {
    }

	public void doInsert(RunData data, Context context)
        throws Exception
    {
		try
		{
			Location oLocation = new Location();
			setProperties (oLocation, data);
			oLocation.setLocationId (IDGenerator.generateSysID());
            oLocation.setUpdateDate(new Date());
			oLocation.save();
	        LocationManager.getInstance().refreshCache(oLocation);
        	data.setMessage (LocaleTool.getString(s_INSERT_SUCCESS));
        }
        catch(Exception _oEx)
        {   
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_INSERT_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

	public void doUpdate(RunData data, Context context)
        throws Exception
    {
		try
		{
			Location oLocation = LocationTool.getLocationByID(data.getParameters().getString("ID"));
			Location oOld = oLocation.copy();
			
			setProperties (oLocation, data);            
            oLocation.setUpdateDate(new Date());
	        oLocation.save();
	        LocationManager.getInstance().refreshCache(oLocation);
	        
	        UpdateHistoryTool.createHistory(oOld, oLocation, data.getUser().getName(), null);
	        
        	data.setMessage (LocaleTool.getString(s_UPDATE_SUCCESS));
        }
        catch(Exception _oEx)
        {
            if(_oEx.getMessage().indexOf(AppAttributes.s_DUPLICATE_MSG) == -1)
            {
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + _oEx.getMessage());
	       	}
	       	else
	       	{
	       	    data.setMessage (LocaleTool.getString(s_UPDATE_FAILED) + LocaleTool.getString(s_DUPLICATE_ENTRY));
	        }
        }
    }

 	public void doDelete(RunData data, Context context)
        throws Exception
    {
    	try
    	{
    		StringBuilder oMsg = new StringBuilder();
			String sID = data.getParameters().getString("ID");
    		if(SqlUtil.validateFKRef("employee","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("inventory_location","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("inventory_transaction","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("issue_receipt","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("item_transfer","from_location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("item_transfer","to_location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("sales_transaction","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("sales_return","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("purchase_order","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("purchase_receipt","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("purchase_return","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("preference","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("remote_store_data","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("synchronization","location_id",sID,oMsg)
    			&& SqlUtil.validateFKRef("license_info","location_id",sID,oMsg))
    		{
    			Location oLoc = LocationTool.getLocationByID(sID);
    			if (oLoc != null)
    			{
    				UpdateHistoryTool.createDelHistory(oLoc, data.getUser().getName(),null);
    				
	    			String sCode = oLoc.getLocationCode();
	    			String sSite = oLoc.getSiteId();
	    			
		    		Criteria oCrit = new Criteria();
		        	oCrit.add(LocationPeer.LOCATION_ID, sID);
		        	LocationPeer.doDelete(oCrit);
					LocationManager.getInstance().refreshCache(sID, sCode, sSite);
    			}
        		data.setMessage (LocaleTool.getString(s_DELETE_SUCCESS));
        	}
        	else
        	{	
        		data.setMessage (LocaleTool.getString(s_DELETE_REFERENCE) + " \n" + oMsg);
        	}
        }
        catch(Exception _oEx)
        {
        	data.setMessage (LocaleTool.getString(s_DELETE_FAILED) + _oEx.getMessage());
        }
    }
 	public void doLoadfile (RunData data, Context context)
 		throws Exception
	{
		try 
		{
			FileItem oFileItem = data.getParameters().getFileItem("DataFileLocation");
			InputStream oBufferStream = new BufferedInputStream(oFileItem.getInputStream());
			LocationLoader oLoader = new LocationLoader(data.getUser().getName());
			oLoader.loadData (oBufferStream);
			data.setMessage(LocaleTool.getString(s_EXCEL_LOAD_SUCCESS));
	    	context.put ("LoadResult", oLoader.getResult());
		}
		catch (Exception _oEx) 
		{
			String sError = LocaleTool.getString(s_EXCEL_LOAD_FAILED) + _oEx.getMessage();
			data.setMessage (sError);
			_oEx.printStackTrace();
		}
	}
 	
    private void setProperties (Location _oLocation, RunData data)
    {
		try
		{
	    	data.getParameters().setProperties(_oLocation);
    	}
    	catch (Exception _oEx)
    	{
        	data.setMessage (LocaleTool.getString(s_SET_PROP_FAILED) +  _oEx.getMessage());
        }
    }
}
package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.manager.PettyCashManager;
import com.ssti.enterprise.pos.om.PettyCashType;
import com.ssti.enterprise.pos.om.PettyCashTypePeer;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PettyCashTypeTool.java,v 1.9 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: PettyCashTypeTool.java,v $
 * Revision 1.9  2009/05/04 02:03:51  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/07/10 04:51:48  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class PettyCashTypeTool extends BaseTool 
{
	public static List getAllPettyCashType()
		throws Exception
	{
		return PettyCashManager.getInstance().getAllPettyCashType();
	}

	public static PettyCashType getPettyCashTypeByID(String _sID)
    	throws Exception
    {
		return PettyCashManager.getInstance().getPettyCashTypeByID(_sID, null);
	}
	
	public static PettyCashType getPettyCashTypeByID(String _sID, Connection _oConn)
		throws Exception
	{
		return PettyCashManager.getInstance().getPettyCashTypeByID(_sID, _oConn);
	}

	public static String getPettyCashTypeNameByID(String _sID)
    	throws Exception
    {
		PettyCashType oPettyCashType = getPettyCashTypeByID (_sID);
		if (oPettyCashType != null)
		{
			return oPettyCashType.getPettyCashCode();
		}
		return "";
	}

	public static PettyCashType getByCode(String _sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(PettyCashTypePeer.PETTY_CASH_CODE, _sCode);
		List vData = PettyCashTypePeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (PettyCashType) vData.get(0);
		}
		return null;
	}
}
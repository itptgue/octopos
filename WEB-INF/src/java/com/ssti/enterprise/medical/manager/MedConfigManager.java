package com.ssti.enterprise.medical.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.om.CustomerMarginPeer;
import com.ssti.enterprise.medical.om.MedColorPeer;
import com.ssti.enterprise.medical.om.MedConfig;
import com.ssti.enterprise.medical.om.MedConfigPeer;
import com.ssti.enterprise.medical.om.PowderItemPeer;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class MedConfigManager
{   
	private static Log log = LogFactory.getLog(MedConfigManager.class);
	 
    private static MedConfigManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private MedConfigManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static MedConfigManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (MedConfigManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new MedConfigManager();
                }
            }
        }
        return m_oInstance;
    }

	public MedConfig getConfig(Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("MedConfig");
		if (oElement == null)
		{		
			List v = MedConfigPeer.doSelect(new Criteria());
			if (v.size() > 0)
			{
				MedConfig oConf = (MedConfig)v.get(0);
				oCache.put(new Element("MedConfig", oConf));
				return oConf;
			}
			return null;
		}
		else 
		{
			return (MedConfig) oElement.getValue();
		}
	}
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache() 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("MedConfig");
        oCache.remove("MedTxTypes");
        oCache.remove("MedColors");
        oCache.remove("MedMargins");
        oCache.remove("PowderItems");
        oCache.removeAll();
    }

	public List getTypes(Connection _oConn)
		throws Exception
	{
		String sKey = "MedTxTypes";
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			List v = new ArrayList(); //MedTxtypePeer.doSelect(new Criteria().addAscendingOrderByColumn(MedTxtypePeer.TXTYPE));
			oCache.put(new Element(sKey, (Serializable) v));			
			return v;
		}
		else
		{
			return (List) oElement.getValue();
		}
	}
	
	public List getColors(Connection _oConn)
		throws Exception
	{
		String sKey = "MedColors";
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			List v = MedColorPeer.doSelect(new Criteria());
			oCache.put(new Element(sKey, (Serializable) v));			
			return v;
		}
		else
		{
			return (List) oElement.getValue();
		}
	}
	
	public List getMargins() 
		throws Exception 
	{	
		String sKey = "MedMargins";
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addDescendingOrderByColumn(CustomerMarginPeer.PRIORITY);
			List v = CustomerMarginPeer.doSelect(oCrit);
			oCache.put(new Element(sKey, (Serializable) v));			
			return v;
		}
		else
		{
			return (List) oElement.getValue();
		}
	}	
	
	//-------------------------------------------------------------------------
	// POWDER 
	//-------------------------------------------------------------------------
	public List getAllPowderItem()
		throws Exception
	{
		String sKey = "PowderItems";
		Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get(sKey);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();			
			List v = PowderItemPeer.doSelect(oCrit);
			oCache.put(new Element(sKey, (Serializable) v));			
			return v;
		}
		else
		{
			return (List) oElement.getValue();
		}
	}
}
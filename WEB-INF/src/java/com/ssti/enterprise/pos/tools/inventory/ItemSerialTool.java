package com.ssti.enterprise.pos.tools.inventory;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;

import com.ssti.enterprise.pos.model.InventoryDetailOM;
import com.ssti.enterprise.pos.om.InventoryTransaction;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemPeer;
import com.ssti.enterprise.pos.om.ItemSerial;
import com.ssti.enterprise.pos.om.ItemSerialPeer;
import com.ssti.enterprise.pos.om.SerialTransaction;
import com.ssti.enterprise.pos.om.SerialTransactionPeer;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: ItemSerialTool.java,v 1.9 2008/06/29 07:11:57 albert Exp $ <br>
 *
 * <pre>
 * $Log: ItemSerialTool.java,v $
 * Revision 1.9  2008/06/29 07:11:57  albert
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/03 02:08:18  albert
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/26 00:39:49  albert
 * *** empty log message ***
 *
 * Revision 1.6  2007/02/23 14:44:42  albert
 * javadoc alignment
 * 
 * </pre><br>
 */
public class ItemSerialTool extends BaseTool 
{
	private static Log log = LogFactory.getLog(ItemSerialTool.class);
	
	public static int i_MANUAL_SERIAL_REGISTER = 98;
	public static int i_MANUAL_SERIAL_ISSUE = 99;
	
	static ItemSerialTool instance = null;
	
	public static synchronized ItemSerialTool getInstance() 
	{
		if (instance == null) instance = new ItemSerialTool();
		return instance;
	}
	
	public static ItemSerial getItemSerialByID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemSerialPeer.ITEM_SERIAL_ID, _sID);
        List vData = ItemSerialPeer.doSelect(oCrit);
        if (vData.size() > 0) {
			return (ItemSerial) vData.get(0);
		}
		return null;
	}

	public static List getItemSerialByItemID(String _sItemID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemSerialPeer.ITEM_ID, _sItemID);
        return ItemSerialPeer.doSelect(oCrit);
	}	
	
	public static ItemSerial getByIDAndSN(String _sItemID, String _sSerialNo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(ItemSerialPeer.ITEM_ID, _sItemID);
	    oCrit.add(ItemSerialPeer.SERIAL_NO, _sSerialNo);	 
	    List vData = ItemSerialPeer.doSelect(oCrit);
	    if (vData.size() > 0)
	    {
	    	return (ItemSerial) vData.get(0);
	    }
	    return null;
	}	
	
	public static ItemSerial getByIDAndSN(String _sItemID, String _sSerialNo, String _sLocID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(ItemSerialPeer.ITEM_ID, _sItemID);
	    oCrit.add(ItemSerialPeer.SERIAL_NO, _sSerialNo);	 
	    if (StringUtil.isNotEmpty(_sLocID))
	    {
		    oCrit.add(ItemSerialPeer.LOCATION_ID, _sLocID);	 	    	
	    }
	    List vData = ItemSerialPeer.doSelect(oCrit);
	    if (vData.size() > 0)
	    {
	    	return (ItemSerial) vData.get(0);
	    }
	    return null;
	}	
	
	public static List getItemSerialByLocationID(String _sLocationID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(ItemSerialPeer.LOCATION_ID, _sLocationID);
        return ItemSerialPeer.doSelect(oCrit);
	}	

	public static List findData(String _sItemID, String _sLocationID, int _iStatus)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sItemID))
		{
        	oCrit.add(ItemSerialPeer.ITEM_ID, _sItemID);           
    	}
    	if (StringUtil.isNotEmpty(_sLocationID))
		{
        	oCrit.add(ItemSerialPeer.LOCATION_ID, _sLocationID);
    	}
    	if (_iStatus == 2)
		{
    		oCrit.add(ItemSerialPeer.STATUS, true);
		}    	
		else if (_iStatus == 1)
		{
    		oCrit.add(ItemSerialPeer.STATUS, false);			
		}
        return ItemSerialPeer.doSelect(oCrit);
	}		
	
	public static List findData(String _sItemID, 
								String _sItemCode,
								String _sItemName,
								String _sKategoriID,
								String _sLocationID, 
								String _sSerialNo,
								int _iStatus,
								Connection _oConn)	
		throws Exception
	{	
		Criteria oCrit = new Criteria();
		
		if (StringUtil.isNotEmpty(_sItemID))
		{
			oCrit.add(ItemSerialPeer.ITEM_ID, _sItemID);
		}
		if (StringUtil.isNotEmpty(_sItemCode))
		{
			oCrit.addJoin(ItemSerialPeer.ITEM_ID, ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.ITEM_CODE, _sItemCode);			
		}
		if (StringUtil.isNotEmpty(_sItemName))
		{
			oCrit.addJoin(ItemSerialPeer.ITEM_ID, ItemPeer.ITEM_ID);
			oCrit.add(ItemPeer.ITEM_NAME, (Object)_sItemName, Criteria.ILIKE);			
		}
		if (StringUtil.isNotEmpty(_sKategoriID))
		{
			oCrit.addJoin(ItemSerialPeer.ITEM_ID,ItemPeer.ITEM_ID);
			List vKategori = KategoriTool.getChildIDList(_sKategoriID);
			if(vKategori.size() > 0)
			{
				vKategori.add(_sKategoriID);
				oCrit.addIn(ItemPeer.KATEGORI_ID,vKategori);
			}
			else{
				oCrit.add(ItemPeer.KATEGORI_ID,_sKategoriID);
			}
		}		
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(ItemSerialPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sSerialNo))
		{
			oCrit.add(ItemSerialPeer.SERIAL_NO, (Object)_sSerialNo, Criteria.ILIKE);
		}
    	if (_iStatus == 2)
		{
    		oCrit.add(ItemSerialPeer.STATUS, true);
		}    	
		else if (_iStatus == 1)
		{
    		oCrit.add(ItemSerialPeer.STATUS, false);			
		}		
		return ItemSerialPeer.doSelect(oCrit, _oConn);
	}

	//Serial Transaction
	public static List getTransactionByTransID(String _sID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SerialTransactionPeer.SERIAL_TRANSACTION_ID, _sID);
        return SerialTransactionPeer.doSelect(oCrit);
	}

	public static List getTransactionByItemAndLocation(String _sItemID, String _sLocationID)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(SerialTransactionPeer.ITEM_ID, _sItemID);
        oCrit.add(SerialTransactionPeer.LOCATION_ID, _sLocationID);
        return SerialTransactionPeer.doSelect(oCrit);
	}

	public static List getByItemAndDetID(String _sItemID, String _sDetID)
		throws Exception
	{
	    return getByItemAndDetID(_sItemID, _sDetID, "");
	}

	public static List getByItemAndDetID(String _sItemID, String _sDetID, String _sLocID)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(SerialTransactionPeer.ITEM_ID, _sItemID);
	    oCrit.add(SerialTransactionPeer.TRANSACTION_DETAIL_ID, _sDetID);
	    if (StringUtil.isNotEmpty(_sLocID))
	    {
		    oCrit.add(SerialTransactionPeer.LOCATION_ID, _sLocID);	    	
	    }
	    return SerialTransactionPeer.doSelect(oCrit);
	}


	public static List findTrans(String _sItemID, 
								 String _sLocationID,
								 String _sSerialNo,
								 String _sTransID,
								 String _sTransNo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sItemID))
		{
			oCrit.add(SerialTransactionPeer.ITEM_ID, _sItemID);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(SerialTransactionPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sSerialNo))
		{
			oCrit.add(SerialTransactionPeer.SERIAL_NO, _sSerialNo);
		}
		if (StringUtil.isNotEmpty(_sTransID))
		{
			oCrit.add(SerialTransactionPeer.TRANSACTION_ID, _sTransID);
		}
		if (StringUtil.isNotEmpty(_sTransNo))
		{
			oCrit.add(SerialTransactionPeer.TRANSACTION_NO, _sTransNo);
		}
	    return SerialTransactionPeer.doSelect(oCrit);
	}
	
	/**
	 * Create Item Serial 
	 * 
	 * @param _sItemID
	 * @param _sLocationID
	 * @param data
	 * @throws Exception
	 */
	public static void manualCreate (String _sItemID,
									 String _sLocationID, 
									 RunData data)
		throws Exception
	{
		Connection oConn = null;
		try 
		{
			oConn = beginTrans();
			Item oItem = ItemTool.getItemByID(_sItemID);
			int iTotalSerial = data.getParameters().getInt("NoOfInputSerial");
			
			for (int i = 1; i <= iTotalSerial; i++)
			{
				String sSerialNo = data.getParameters().getString("SerialNo" + i, "");
				if (StringUtil.isNotEmpty(sSerialNo))
				{
					log.debug("Serial No " + i + " : "  + sSerialNo);

					//validate existing SN
					ItemSerial oIS = getByIDAndSN(_sItemID, sSerialNo);
					if (oIS != null && oIS.getStatus() == true)
					{
						throw new Exception ("Serial No: " + sSerialNo + " already exist");
					}
					
					SerialTransaction oTrans = new SerialTransaction ();
					oTrans.setSerialTransactionId(IDGenerator.generateSysID());
					oTrans.setItemId 		  (_sItemID);
					oTrans.setLocationId 	  (_sLocationID);
					oTrans.setSerialNo 		  (sSerialNo);
					oTrans.setQty			  (1);
					oTrans.setTransactionId   (oItem.getItemId());
					oTrans.setTransactionNo   (oItem.getItemCode());
					oTrans.setTransactionDate (new Date());
					oTrans.setTransactionType (i_MANUAL_SERIAL_REGISTER);
					oTrans.save(oConn);		
					
					updateItemSerial(oTrans, oConn);
				}
			}
			commit(oConn);
		}
		catch (Exception _oEx)
		{
			rollback (oConn);
			log.error (_oEx);
			throw new Exception ("Create Item Serial Failed : " + _oEx.getMessage());
		}
	}
	
	/**
	 * 
	 * @param _sItemID
	 * @param _sLocationID
	 * @param data
	 * @throws Exception
	 */
	public static void manualIssue (String _sItemID,
									String _sLocationID, 
									RunData data)
		throws Exception
	{
		Connection oConn = null;
		try 
		{
			oConn = beginTrans();	
			int iTotalSerial = data.getParameters().getInt("NoOfIssueSerial");
			
			for (int i = 1; i <= iTotalSerial; i++)
			{
				String sSerialNo = data.getParameters().getString("SerialNo" + i, "");
				String sTransNo = data.getParameters().getString("TransactionNo" + i, "");
				int iTransType = data.getParameters().getInt("TransactionType" + i, -1);
				if (iTransType == -1)
				{
					iTransType = i_MANUAL_SERIAL_ISSUE;
				}
				
				if (StringUtil.isNotEmpty(sSerialNo))
				{
					log.debug("Serial No : " + sSerialNo);
					
					SerialTransaction oTrans = new SerialTransaction ();
					oTrans.setSerialTransactionId(IDGenerator.generateSysID());
					oTrans.setItemId (_sItemID);
					oTrans.setLocationId (_sLocationID);
					oTrans.setSerialNo(sSerialNo);
					oTrans.setQty(-1);
					oTrans.setTransactionId ("");
					oTrans.setTransactionNo (sTransNo);
					oTrans.setTransactionDate (new Date());
					oTrans.setTransactionType (iTransType);
					oTrans.save(oConn);		
					
					updateItemSerial(oTrans, oConn);
				}
				
			}
			commit(oConn);
		}
		catch (Exception _oEx)
		{
			rollback (oConn);
			log.error (_oEx);
			throw new Exception ("Issue Item Serial Failed : " + _oEx.getMessage());
		}
	}
	
	private static ItemSerial getForUpdate (String _sItemID, 
											String _sLocationID, 
											String _sSerialNo,
											Connection _oConn) 
		throws Exception
	{		
		Criteria oCrit = new Criteria();
		oCrit.add (ItemSerialPeer.ITEM_ID, _sItemID);
		oCrit.add (ItemSerialPeer.LOCATION_ID, _sLocationID);
		oCrit.add (ItemSerialPeer.SERIAL_NO, _sSerialNo);
		oCrit.setForUpdate(true);
		List vData = ItemSerialPeer.doSelect (oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (ItemSerial) vData.get(0);
		}
		return null;
	}

	public static void updateItemSerial(SerialTransaction _oTR, Connection _oConn)
		throws Exception
	{
		int iQty = _oTR.getQty();
		
		ItemSerial oIS = getForUpdate(
			_oTR.getItemId(), _oTR.getLocationId(), _oTR.getSerialNo(), _oConn);
		
		if (iQty > 0) //in
		{
			if (oIS != null && oIS.getStatus() == false) //not exist (valid)
			{
				oIS.setStatus(true);
				oIS.save(_oConn);
			}
			else if (oIS == null) //not exist (valid)
			{
				oIS = new ItemSerial();
				oIS.setItemSerialId(IDGenerator.generateSysID());
				oIS.setItemId(_oTR.getItemId());
				oIS.setItemCode(ItemTool.getItemCodeByID(_oTR.getItemId()));
				oIS.setLocationId(_oTR.getLocationId());
				oIS.setSerialNo(_oTR.getSerialNo());
				oIS.setStatus(true);
				oIS.save(_oConn);
			}
			else //invalid
			{
				throw new Exception ("Serial No: " + _oTR.getSerialNo() + " already exist");
			}
		}
		else if (iQty < 0)
		{
			if (oIS != null && oIS.getStatus() == true) //exist (valid)
			{
				oIS.setStatus(false);
				oIS.save(_oConn);
			}
			else //invalid
			{
				throw new Exception ("Serial No: " + _oTR.getSerialNo() + " is not exist");
			}
		}
	}
	
	
	/**
	 * automatic save serial trans from Inv Trans
	 * @param _oTR
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveSerial(List _vSerial, InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		//if Serial No Number specified in Trans
		if (_oInv != null && _vSerial != null && _vSerial.size() > 0)
		{
			for (int i = 0; i < _vSerial.size(); i++)
			{
				SerialTransaction oST = (SerialTransaction) _vSerial.get(i);
				//if transfer TO then prevent ST being updated
				if (_oInv.getTransactionType() == i_INV_TRANS_TRANSFER_OUT || _oInv.getTransactionType() == i_INV_TRANS_TRANSFER_IN)
				{
					oST.setNew(true);
				}
				mapInvToSerial(oST, _oInv);				
				oST.save(_oConn);
				updateItemSerial(oST, _oConn);
			}	
		}
	}

	private static void mapInvToSerial (SerialTransaction _oST, InventoryTransaction _oInv) 
		throws Exception
	{
		if (_oST != null && _oInv != null)
		{
			int iQty = 1;
			if (_oInv.getQtyChanges().doubleValue() < 0) iQty = -1;
			
			_oST.setItemId(_oInv.getItemId());
			_oST.setLocationId(_oInv.getLocationId());
			_oST.setQty(iQty);
			_oST.setTransactionDate(_oInv.getTransactionDate());
			_oST.setTransactionDetailId(_oInv.getTransactionDetailId());
			_oST.setTransactionId(_oInv.getTransactionId());
			_oST.setTransactionNo(_oInv.getTransactionNo());
			_oST.setTransactionType(_oInv.getTransactionType());
			_oST.setInventoryTransactionId(_oInv.getInventoryTransactionId());
			_oST.setSerialTransactionId(IDGenerator.generateSysID(true));
			_oST.setDescription(_oInv.getDescription());
		}
	}
	
	public static void deleteSerial(InventoryTransaction _oInv, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(SerialTransactionPeer.INVENTORY_TRANSACTION_ID, _oInv.getInventoryTransactionId());		
		oCrit.add(SerialTransactionPeer.LOCATION_ID, _oInv.getLocationId());			
		List vST = SerialTransactionPeer.doSelect(oCrit, _oConn);
		for (int i = 0; i < vST.size(); i++)
		{
			SerialTransaction oST = (SerialTransaction)vST.get(i);
			int iQty = 1;
			if (_oInv.getQtyChanges().doubleValue() > 0) iQty = -1;

			oST.setQty(iQty);
			updateItemSerial(oST,_oConn);
		}
		SerialTransactionPeer.doDelete(oCrit,_oConn);		
	}
		
	public static String getTransactionTypeName (int _iTransType)
	{
		if (_iTransType == i_MANUAL_SERIAL_REGISTER) 
		{
			return "Manual Registration";
		}
		else if (_iTransType == i_MANUAL_SERIAL_ISSUE) 
		{
			return "Manual Issue";
		}
		else 
		{
			return InventoryTransactionTool.getTransactionTypeName(Integer.valueOf(_iTransType));		
		}
	}
	
	//-------------------------------------------------------------------------
	// Validate
	//-------------------------------------------------------------------------	
	private static String getEmptyMsg(Item _oItem)
	{
		StringBuilder oSB = new StringBuilder();
		if (_oItem != null)
		{
			oSB.append(LocaleTool.getString("serial_no")).append(" ")
			   .append(_oItem.getItemCode()).append(" ")
			   .append(_oItem.getItemName()).append(" ")
			   .append(LocaleTool.getString("may_not_empty"));
		}
		return oSB.toString();	
	}
	private static String getDuplicateMsg(Item _oItem, String _sNo)
	{
		StringBuilder oSB = new StringBuilder();
		if (_oItem != null)
		{
			oSB.append("Item: ")
			   .append(_oItem.getItemCode()).append(" ")
			   .append(_oItem.getItemName()).append(" ")
			   .append(LocaleTool.getString("serial_no"))
			   .append(": ").append(_sNo)
			   .append(" Duplicate ");
		}
		return oSB.toString();	
	}	
	public static void validateSerial(String _sItemID, double dQty, List _vST, Connection _oConn)	
		throws Exception
	{
		if (PreferenceTool.useSerialNo())
		{
			Item oItem = ItemTool.getItemByID(_sItemID, _oConn);
			validateSerial(oItem, dQty, _vST);
		}
	}
	public static void validateSerial(Item _oItem, double dQty, List _vST)	
		throws Exception
	{
		if (PreferenceTool.useSerialNo() && _oItem != null && _oItem.getTrackSerialNo())
		{
			if (_vST != null && dQty == _vST.size())
			{
				Map mSNo = new HashMap(_vST.size());
				for (int i = 0; i < _vST.size(); i++)
				{
					SerialTransaction oST = (SerialTransaction) _vST.get(i);
					if (StringUtil.isEmpty(oST.getSerialNo()))
					{				
						throw new Exception (getEmptyMsg(_oItem));
					}
					if (!mSNo.containsKey(oST.getSerialNo()))
					{
						mSNo.put(oST.getSerialNo(),"");
					}
					else
					{
						throw new Exception (getDuplicateMsg(_oItem, oST.getSerialNo()));						
					}
				}
			}
			else
			{
				throw new Exception (getEmptyMsg(_oItem));
			}
		}
	}	
	public static void validateSerial(List _vDet, Connection _oConn)	
		throws Exception
	{
		if (PreferenceTool.useSerialNo())
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				InventoryDetailOM oTD = (InventoryDetailOM)_vDet.get(i);
				String sItemID = oTD.getItemId();
				Item oItem = ItemTool.getItemByID(sItemID, _oConn);
				double dQty = 0;
				if (oTD.getQty() != null) dQty = oTD.getQty().doubleValue();
				validateSerial(oItem, dQty, oTD.getSerialTrans());
			}
		}
	}	
	public static String validateSerialJS(List _vDet)	
		throws Exception
	{
		StringBuilder oResult = new StringBuilder("");
		if (PreferenceTool.useSerialNo())
		{
			for (int i = 0; i < _vDet.size(); i++)
			{
				InventoryDetailOM oTD = (InventoryDetailOM)_vDet.get(i);
				String sItemID = oTD.getItemId();
				Item oItem = ItemTool.getItemByID(sItemID);
				double dQty = 0;
				if (oTD.getQty() != null) dQty = oTD.getQty().doubleValue();
				if (oItem != null && oItem.getTrackSerialNo())
				{
					List vST = oTD.getSerialTrans();
					for (int j = 0; j < vST.size(); j++)
					{
						SerialTransaction oST = (SerialTransaction) vST.get(j);
						if (dQty != 0 && StringUtil.isEmpty(oST.getSerialNo()))
						{
							oResult.append(getEmptyMsg(oItem)).append("\n");
						}						
					}
				}
			}
		}
		return oResult.toString();
	}
}
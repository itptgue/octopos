package com.ssti.enterprise.medical.tools;

public interface MedicalAttributes 
{
	public static final String s_MTR = "mst";
	public static final String s_MTD = "mstDet";
	
	public static final String s_PRS = "prs";
	public static final String s_PRSD = "prsd";
	
	public static final String s_PWD = "pwd";
	public static final String s_PWDD = "pwdDet";

	public static final String s_LO = "lab";
	public static final String s_LOD = "labDet";

	public static final int i_NON_PRES    = 0;
	public static final int i_PRES_NORMAL = 1;
	public static final int i_PRES_POWDER = 2;
	
	public static final int i_OP_VIEW = 1;
	public static final int i_OP_SAVE = 2;
	public static final int i_OP_DEL = 3;
	public static final int i_OP_ALTVIEW = 4;	
	
	public static final int i_PHARMACY = 1;
	public static final int i_LABORATORY = 2;
	public static final int i_CLINIC = 3;	
}

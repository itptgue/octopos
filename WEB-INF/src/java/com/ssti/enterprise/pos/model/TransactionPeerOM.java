package com.ssti.enterprise.pos.model;

public interface TransactionPeerOM 
{
	public String getTableName();
	public String getIDColumn();
	public String getNoColumn();
	public String getDateColumn();
	public String getCreateByColumn();	
	public String getStatusColumn();		
}

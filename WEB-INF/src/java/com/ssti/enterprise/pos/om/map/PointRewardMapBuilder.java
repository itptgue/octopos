package com.ssti.enterprise.pos.om.map;


import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class PointRewardMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.PointRewardMapBuilder";

    /**
     * Item
     * @deprecated use PointRewardPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "point_reward";
    }

  
    /**
     * point_reward.POINT_REWARD_ID
     * @return the column name for the POINT_REWARD_ID field
     * @deprecated use PointRewardPeer.point_reward.POINT_REWARD_ID constant
     */
    public static String getPointReward_PointRewardId()
    {
        return "point_reward.POINT_REWARD_ID";
    }
  
    /**
     * point_reward.FROM_AMOUNT
     * @return the column name for the FROM_AMOUNT field
     * @deprecated use PointRewardPeer.point_reward.FROM_AMOUNT constant
     */
    public static String getPointReward_FromAmount()
    {
        return "point_reward.FROM_AMOUNT";
    }
  
    /**
     * point_reward.TO_AMOUNT
     * @return the column name for the TO_AMOUNT field
     * @deprecated use PointRewardPeer.point_reward.TO_AMOUNT constant
     */
    public static String getPointReward_ToAmount()
    {
        return "point_reward.TO_AMOUNT";
    }
  
    /**
     * point_reward.MULTIPLY_AMOUNT
     * @return the column name for the MULTIPLY_AMOUNT field
     * @deprecated use PointRewardPeer.point_reward.MULTIPLY_AMOUNT constant
     */
    public static String getPointReward_MultiplyAmount()
    {
        return "point_reward.MULTIPLY_AMOUNT";
    }
  
    /**
     * point_reward.POINT
     * @return the column name for the POINT field
     * @deprecated use PointRewardPeer.point_reward.POINT constant
     */
    public static String getPointReward_Point()
    {
        return "point_reward.POINT";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("point_reward");
        TableMap tMap = dbMap.getTable("point_reward");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("point_reward.POINT_REWARD_ID", "");
                            tMap.addColumn("point_reward.FROM_AMOUNT", bd_ZERO);
                            tMap.addColumn("point_reward.TO_AMOUNT", bd_ZERO);
                            tMap.addColumn("point_reward.MULTIPLY_AMOUNT", bd_ZERO);
                            tMap.addColumn("point_reward.POINT", bd_ZERO);
          }
}

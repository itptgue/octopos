package com.ssti.enterprise.pos.presentation.screens.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.ItemGroup;
import com.ssti.enterprise.pos.om.PurchaseInvoice;
import com.ssti.enterprise.pos.om.PurchaseInvoiceDetail;
import com.ssti.enterprise.pos.om.PurchaseInvoiceExp;
import com.ssti.enterprise.pos.om.PurchaseOrder;
import com.ssti.enterprise.pos.om.PurchaseReceipt;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.ItemGroupTool;
import com.ssti.enterprise.pos.tools.ItemTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.PaymentTermTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.UnitTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.purchase.PurchaseInvoiceTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseOrderTool;
import com.ssti.enterprise.pos.tools.purchase.PurchaseReceiptTool;
import com.ssti.enterprise.pos.tools.sales.TransactionTool;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * PurchaseInvoiceTransaction screen class
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PurchaseInvoiceTransaction.java,v 1.46 2009/05/04 02:02:36 albert Exp $ <br>
 *
 * <pre>
 * $Log: PurchaseInvoiceTransaction.java,v $
 * 
 * 2016-03-28
 * -change addExpData, allow use Base Currency BS Account into Expense 
 * 
 * </pre><br>
 */
public class PurchaseInvoiceTransaction extends PurchaseSecureScreen
{
    private static final String[] a_PERM = {"Purchase Transaction", "View Purchase Invoice"};

    private static final Log log = LogFactory.getLog(PurchaseInvoiceTransaction.class);
	
    private static final int i_OP_ADD_DETAIL  	= 1; //add detail from PurchaseReceipt
    private static final int i_OP_DEL_DETAIL  	= 2; 
	private static final int i_OP_NEW_TRANS   	= 3;
	private static final int i_OP_VIEW_TRANS  	= 4; //after saved or view details
	private static final int i_OP_ADD_NEWDETAIL = 6; //add detail from PurchaseInvoice (not from pr)
    private static final int i_OP_COPY_TRANS	= 7; 	
	private static final int i_OP_IMPORT_FROM_PO= 8;

    private static final int i_OP_ADD_EXP  		= 11; //add detail from PurchaseReceipt
    private static final int i_OP_DEL_EXP  		= 12; 
	
	private static final int i_OP_CREATE_SI		= 99;
	
    private PurchaseInvoice oPI = null;
    private List vPID = null;
    private List vPIF = null;
    private List vPIE = null;
    
    private HttpSession oSes;
    
    protected boolean isAuthorized(RunData data)
	    throws Exception
	{
		return isAuthorized (data, a_PERM);
	}
    
    public void doBuildTemplate ( RunData data, Context context )
    {
    	super.doBuildTemplate(data, context);
    	
		int iOp = data.getParameters().getInt("op");
		initSession (data);
		try 
		{
			if (iOp == i_OP_ADD_DETAIL && !b_REFRESH)
			{
				addDetailData (data);
			}
			else if (iOp == i_OP_DEL_DETAIL && !b_REFRESH)
			{
				delDetailData (data);
			}
			if (iOp == i_OP_ADD_EXP && !b_REFRESH)
			{
				addExpData (data);
			}
			else if (iOp == i_OP_DEL_EXP && !b_REFRESH)
			{
				delExpData (data);
			}			
			else if (iOp == i_OP_NEW_TRANS)
			{
				createNewTrans (data);
			}
			else if (iOp == i_OP_VIEW_TRANS)
			{
				getData (data);
			}
			else if (iOp == i_OP_ADD_NEWDETAIL && !b_REFRESH)
			{
				addNewDetailData (data);
			}
			else if (iOp == i_OP_COPY_TRANS)
			{
				copyTrans (data);
			}			
			else if (iOp == i_OP_IMPORT_FROM_PO && !b_REFRESH)
			{
				importPOData (data);
			}	
			else if (iOp == i_OP_CREATE_SI && !b_REFRESH)
			{
				createSI (data);
			}
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			log.error(_oEx);
			data.setMessage("Error : " + _oEx.getMessage());
		}
		context.put(s_PIE, oSes.getAttribute (s_PIE));    	
		context.put(s_PID, oSes.getAttribute (s_PID));
    	context.put(s_PI, oSes.getAttribute (s_PI));
    }

	private void initSession (RunData data)
	{
		synchronized (this)
		{
			oSes = data.getSession();
			if ( oSes.getAttribute (s_PI) == null )
			{
				createNewTrans(data);
			}
			oPI = (PurchaseInvoice) oSes.getAttribute (s_PI);
			vPID = (List) oSes.getAttribute (s_PID);
			vPIF = (List) oSes.getAttribute (s_PIF);
			vPIE = (List) oSes.getAttribute (s_PIE);
		}
	}

	/**
	 * Add detail from purchase receipt
	 * 
	 * @param data
	 * @throws Exception
	 */
    private void addDetailData (RunData data)
    	throws Exception
    {
    	String sPRID = data.getParameters().getString("PrId", "");
    	String sPOID;
    	List vPRD = null;
    	
    	PurchaseReceipt oPR = PurchaseReceiptTool.getHeaderByID(sPRID);
    	data.getParameters().setProperties(oPI);
    	if (DateUtil.isBefore(oPI.getPurchaseInvoiceDate(), oPR.getReceiptDate()))
    	{
    		data.setMessage(LocaleTool.getString("warn_import_date"));
    	}
		if(oPR != null)
		{
			sPOID = oPR.getPurchaseOrderId();
			oPI.setTotalDiscountPct(oPR.getTotalDiscountPct());
			if (!isExist (sPRID))
			{
			    String sVendorID = data.getParameters().getString("VendorId","");
				vPRD = PurchaseReceiptTool.getDetailsByID(sPRID);
				PurchaseInvoiceTool.mapPRDetailToPIDetail(oPR, vPRD, vPID, sPOID, sVendorID);
			}
			PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE,data);
			oPI.setPaymentTypeId(oPR.getPaymentTypeId());
			oPI.setPaymentTermId(oPR.getPaymentTermId());
			
			int iDueDays = PaymentTermTool.getNetPaymentDaysByID (oPR.getPaymentTermId(), null);			
			oPI.setDueDate(DateUtil.addDays(oPI.getTransactionDate(), iDueDays));
			oPI.setCurrencyId(oPR.getCurrencyId());
			oPI.setCurrencyRate(oPR.getCurrencyRate());
			oPI.setFiscalRate(oPR.getFiscalRate());

			oPI.setIsInclusiveTax(oPR.getIsInclusiveTax());
			oPI.setIsTaxable(oPR.getIsTaxable());
			
			oPI.setFobId(oPR.getFobId());
			oPI.setCourierId(oPR.getCourierId());

			oSes.setAttribute (s_PID, vPID);
			oSes.setAttribute (s_PI, oPI);
		}
    }
    
    private void addNewDetailData (RunData data)
    	throws Exception
    {
    	PurchaseInvoiceTool.updateDetail(vPID, data);

    	PurchaseInvoiceDetail oPID = new PurchaseInvoiceDetail();
    	String sItemID = data.getParameters().getString("ItemId");    	    	
		Item oItem = ItemTool.getItemByID(data.getParameters().getString("ItemId"));
		if (oItem != null)
		{
			if(oItem.getItemType() == i_GROUPING)
			{
				addGroupItems(oItem, data);
				return;
			}
			else
			{
				data.getParameters().setProperties (oPID);
				String sDesc = data.getParameters().getString("ItemName");						
				oPID.setItemCode (oItem.getItemCode());
				oPID.setItemName(oItem.getItemName());
				if(!StringUtil.equalsIgnoreCase(sDesc, oItem.getItemName()))
				{
					oPID.setDescription(sDesc); //set desc manually if item name changed in form
				}
			}
		}
		else
		{
			return;
		}
    	
		if (!isExistItem (oPID, data))
		{
	       	double dRate = data.getParameters().getDouble("CurrencyRate", 1);
			double dBaseValue = UnitTool.getBaseValue(oPID.getItemId(), oPID.getUnitId());
		
			//calculate Qty Base
	        double dQtyBase = oPID.getQty().doubleValue() * dBaseValue; 
			oPID.setQtyBase(new BigDecimal(dQtyBase));
		
			//calculate sub total, discount
			double dQty = oPID.getQty().doubleValue();
    		double dSubTotal = oPID.getItemPrice().doubleValue() * dQty;
    		double dDiscount = Calculator.calculateDiscount(oPID.getDiscount(), dSubTotal);
    		oPID.setSubTotalDisc (new BigDecimal(dDiscount));
    		dSubTotal = dSubTotal - oPID.getSubTotalDisc().doubleValue();
    					
			//calculate last purchase & cost
			double dCost = dSubTotal / oPID.getQtyBase().doubleValue() * dRate;
    		oPID.setCostPerUnit (new BigDecimal(dCost));
			oPID.setSubTotal (new BigDecimal(dSubTotal));
	    	
	    	oPID.setPurchaseOrderId("");
	    	oPID.setPurchaseReceiptId("");
	    	oPID.setPurchaseReceiptDetailId("");
	    	if (b_PURCHASE_FIRST_LAST)
	    	{
			    vPID.add (0, oPID);
			}
			else
			{
			    vPID.add(oPID);
			}
		}
		PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE,data);
		oSes.setAttribute (s_PID, vPID);
		oSes.setAttribute (s_PI, oPI);
    }
    
    private void addGroupItems(Item _oGroupItem, RunData data) 
    	throws Exception
    {
    	List vGroup = ItemGroupTool.getItemGroupByGroupID(_oGroupItem.getItemId());
    	double dGroupQty = 1;
    	if(data != null) { dGroupQty = data.getParameters().getDouble("Qty"); }
    	for(int i = 0; i < vGroup.size(); i++)
    	{
    		ItemGroup oIG = (ItemGroup) vGroup.get(i);
    		Item oItem = oIG.getItem();
    		if(oItem != null)
    		{
	    		double dIGQty = oIG.getQty().doubleValue() * dGroupQty;	   
	    		data.getParameters().setString("itemid", oItem.getItemId());
	    		data.getParameters().setString("itemname", oItem.getItemName());
	    		data.getParameters().setString("qty", new BigDecimal(dIGQty).toString());
	    		data.getParameters().setString("unitid", oIG.getUnitId());
	    		data.getParameters().setString("taxid",oItem.getPurchaseTaxId());
	    		data.getParameters().setString("taxamount",oItem.getPurchaseTax().getAmount().toString());
	    		if(oItem.getLastPurchasePrice() != null)
	    		{
	    			data.getParameters().setString("itemprice", oItem.getLastPurchasePrice().toString());
	    		}
	    		else
	    		{
	    			data.getParameters().setString("itemprice", "0");
	    		}
	    		addNewDetailData(data);
    		}
    	}    				
	}


    private void delDetailData (RunData data)
    	throws Exception
    {
	   	int iNo = data.getParameters().getInt("No") - 1;
    	PurchaseInvoiceTool.updateDetail(vPID, data);
		if (vPID.size() > iNo)
		{     
			PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail) vPID.get(iNo);
			if (StringUtil.isNotEmpty(oPID.getPurchaseReceiptId())) 
			{ 
				removePR(oPID.getPurchaseReceiptId());
			}
			else
			{
				vPID.remove (iNo);
			}
			PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE,data);
			oSes.setAttribute  (s_PID, vPID);
			oSes.setAttribute  (s_PI, oPI);
		}
    }

    private void addExpData(RunData data)
    	throws Exception
    {
    	PurchaseInvoiceTool.updateDetail(vPID,data);
    	PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE, data);
    	
    	String sItemID = data.getParameters().getString("AccountId");    	
    	PurchaseInvoiceExp oPIE = new PurchaseInvoiceExp();
		Account oAccount = AccountTool.getAccountByID(data.getParameters().getString("AccountId"));
		if (oAccount != null)
		{
			//validate Account
			if (oAccount.getAccountType() <= GlAttributes.i_EQUITY) //if BS Account
			{
				Currency oCurr = CurrencyTool.getCurrencyByID(oAccount.getCurrencyId());
				if(!oCurr.getIsDefault() && !StringUtil.isEqual(oAccount.getCurrencyId(),oPI.getCurrencyId()))
				{
					throw new Exception("Balance Sheet Account and Transaction Currency must be the same!");
				}
				if(oAccount.getHasSubLedger())
				{
					throw new Exception("Account has Sub Ledger!");
				}
			}
			data.getParameters().setProperties (oPIE);	
			oPIE.setDescription(data.getParameters().getString("TxtAccountName"));
			oPIE.setDepartmentId(data.getParameters().getString("DepartmentId0"));
			oPIE.setProjectId(data.getParameters().getString("ProjectId0"));			
			vPIE.add(oPIE);
		}
		else
		{
			return;
		} 	
		PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE, data);
		oSes.setAttribute (s_PIE, vPIE);
		oSes.setAttribute (s_PI, oPI);
    }

    private void delExpData (RunData data)
    	throws Exception
    {
	   	int iNo = data.getParameters().getInt("ExpNo") - 1;
    	PurchaseInvoiceTool.updateDetail (vPID, data);
		if (vPIE.size() > iNo && iNo >= 0)
		{     
			vPIE.remove (iNo);			
			PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE,data);
			oSes.setAttribute  (s_PIE, vPIE);
			oSes.setAttribute  (s_PI, oPI);
		}
    }
    
    private void removePR(String _sPRID) 
    {
    	if (StringUtil.isNotEmpty(_sPRID) && vPID != null)
    	{
	    	Iterator iter = vPID.iterator();
	    	while (iter.hasNext())
	    	{
	    		PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail) iter.next();
	    		if (StringUtil.isEqual(oPID.getPurchaseReceiptId(), _sPRID))
	    		{
	    			iter.remove();
	    		}
	    	}
    	}
	}

	private void createNewTrans (RunData data)
    {
    	synchronized(this)
    	{
    		oPI = new PurchaseInvoice();
    		vPID = new ArrayList();
    		vPIF = new ArrayList();
    		vPIE = new ArrayList();
    		
    		//initialize inclusive tax
    		oPI.setIsInclusiveTax(b_PURCHASE_TAX_INCLUSIVE);

    		oSes.setAttribute (s_PI,  oPI);
    		oSes.setAttribute (s_PID, vPID);
    		oSes.setAttribute (s_PIF, vPIF);
    		oSes.setAttribute (s_PIE, vPIE);
    	}
    }

    private boolean isExist (String _sPRID)
    {
    	for (int i = 0; i < vPID.size(); i++)
    	{
    		PurchaseInvoiceDetail oExistPID = (PurchaseInvoiceDetail) vPID.get (i);
    		if ((oExistPID.getPurchaseReceiptId().equals(_sPRID)))
    		{
				return true;
    		}
    	}
    	return false;
    }
    
    /**
     * if purchase is set to merge, if item exist then merge 
     * 
     * @param _sItemID
     * @param data
     * @return is item exists
     * @throws Exception
     */
    private boolean isExistItem (PurchaseInvoiceDetail _oPID, RunData data)
    	throws Exception
    {
		
		for (int i = 0; i < vPID.size(); i++)
		{
			PurchaseInvoiceDetail oExistPID = (PurchaseInvoiceDetail) vPID.get (i);
			if (oExistPID.getItemId().equals(_oPID.getItemId()) && 
				oExistPID.getUnitId().equals(_oPID.getUnitId()) &&	
				oExistPID.getTaxId().equals(_oPID.getTaxId()) &&	
				oExistPID.getDescription().equals(_oPID.getDescription()) &&						
				oExistPID.getItemPrice().intValue() == _oPID.getItemPrice().intValue() &&
				oExistPID.getDiscount().equals(_oPID.getDiscount()) &&
			    StringUtil.isEmpty(oExistPID.getPurchaseReceiptDetailId()) && 
			    StringUtil.isEmpty(_oPID.getPurchaseReceiptDetailId())     
				)
			{
				if(i_MERGE_DET == i_MERGE_ITM)
			    {					
					mergeData(oExistPID, data);
					return true;
			    }
				else if(i_MERGE_DET == i_MERGE_ERR)
				{
					throw new Exception("Item Already Exist in Detail");
				}
			}
		}	
    	return false;
    }

    private void mergeData (PurchaseInvoiceDetail _oOldTD, RunData data)
    	throws Exception
    {    	
    	double dAddQty = data.getParameters().getDouble("Qty");
    	
       	double dRate = data.getParameters().getDouble("CurrencyRate", 1);
		double dBaseValue = UnitTool.getBaseValue(_oOldTD.getItemId(), _oOldTD.getUnitId());
	
		//calculate Qty Base
        double dQtyBase = (_oOldTD.getQty().doubleValue() * dBaseValue) + (dAddQty * dBaseValue); 
        _oOldTD.setQtyBase(new BigDecimal(dQtyBase));
	
		//calculate sub total, discount
		double dQty = _oOldTD.getQty().doubleValue() + dAddQty;
        _oOldTD.setQty(new BigDecimal(dQty));
		
		double dSubTotal = _oOldTD.getItemPrice().doubleValue() * dQty;
		double dDiscount = Calculator.calculateDiscount(_oOldTD.getDiscount(), dSubTotal);
		_oOldTD.setSubTotalDisc (new BigDecimal(dDiscount));
		dSubTotal = dSubTotal - _oOldTD.getSubTotalDisc().doubleValue();
					
		//calculate last purchase & cost
		double dCost = dSubTotal / _oOldTD.getQtyBase().doubleValue() * dRate;
		_oOldTD.setCostPerUnit (new BigDecimal(dCost));
		_oOldTD.setSubTotal (new BigDecimal(dSubTotal));
    }
    
    private void getData(RunData data)
    	throws Exception
    {
    	//TODO: validate active transaction in session
    	String sID = data.getParameters().getString("id");
    	if (StringUtil.isNotEmpty(sID))
    	{
    		try
    		{
    			synchronized(this)
    			{
	    			oPI = PurchaseInvoiceTool.getHeaderByID (sID);
	    			vPID = PurchaseInvoiceTool.getDetailsByID (sID);
	    			vPIF = PurchaseInvoiceTool.getFreightByID(sID, null);
	    			vPIE = PurchaseInvoiceTool.getExpensesByID(sID, null);
	    			oSes.setAttribute(s_PI, oPI);
					oSes.setAttribute(s_PID, vPID);
					oSes.setAttribute(s_PIF, vPIF);
					oSes.setAttribute(s_PIE, vPIE);
    			}
    		}
    		catch (Exception _oEx)
    		{
				throw new Exception (LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage ());
    		}
    	}
    }

    /**
     * copy Trans
     * 
     * @param data
     * @throws Exception
     */
    private void copyTrans (RunData data)
    	throws Exception
    {
    	String sID = data.getParameters().getString("TransId");
    	PurchaseInvoice oTRC = PurchaseInvoiceTool.getHeaderByID(sID);
		
		if (oTRC != null && StringUtil.isNotEmpty (sID))
		{			
			List vTrans = PurchaseInvoiceTool.getDetailsByID(sID);
			vPID = new ArrayList(vTrans.size());
			for (int i = 0; i < vTrans.size(); i++)
			{
				PurchaseInvoiceDetail oDet = (PurchaseInvoiceDetail) vTrans.get(i);
				PurchaseInvoiceDetail oNewDet = oDet.copy();
				
				//if this PI contains PO/PR data set as standalone PI
				oNewDet.setPurchaseOrderId("");
				oNewDet.setPurchaseReceiptId("");
				oNewDet.setPurchaseReceiptDetailId("");
				oNewDet.setReturnedQty(bd_ZERO);
				vPID.add(oNewDet);
			}
		}
		PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE,data);
		
		oSes.setAttribute (s_PID, vPID);
		oSes.setAttribute (s_PI, oPI);
    }  
    

    /**
     * Import From PO
     * 
     * @param data
     * @throws Exception
     */
	private void importPOData(RunData data) 
		throws Exception
	{    	
    	String sPOID = data.getParameters().getString("PoId");
    	List vPOD = null;
    	if (StringUtil.isNotEmpty(sPOID))
    	{
    		PurchaseOrder oPO = PurchaseOrderTool.getHeaderByID(sPOID);    	
    		if (oPO != null)
    		{
    			vPOD = PurchaseOrderTool.getDetailsByID(sPOID);
    			vPID = new ArrayList(vPOD.size());
    			PurchaseInvoiceTool.mapPODetailToPIDetail(oPO, vPOD ,vPID);    		
    			PurchaseInvoiceTool.setHeaderProperties(oPI,vPID,vPIE,data);    		
    			
    			oPI.setVendorId(oPO.getVendorId());
    			oPI.setVendorName(oPO.getVendorName());
    			oPI.setPaymentTypeId(oPO.getPaymentTypeId());
    			oPI.setPaymentTermId(oPO.getPaymentTermId());
    			
    			oPI.setFobId(oPO.getFobId());
    			oPI.setCourierId(oPO.getCourierId());    			
    			oPI.setTotalDiscountPct(oPO.getTotalDiscountPct());
    			
    			oPI.setIsTaxable(oPO.getIsTaxable());
    			oPI.setIsInclusiveTax(oPO.getIsInclusiveTax());    			
    			oPI.setTotalQty(oPO.getTotalQty());
    			oPI.setTotalExpense(oPO.getEstimatedFreight());
    			oPI.setTotalDiscount(oPO.getTotalDiscount());
    			oPI.setTotalTax(oPO.getTotalTax());
    			oPI.setTotalAmount(oPO.getTotalAmount());
    			oPI.setRemark(oPO.getRemark());
    			
    			oSes.setAttribute (s_PID, vPID);
    			oSes.setAttribute (s_PI, oPI);
    		}
    	}
	}
	
	  /**
     * Import From PO
     * 
     * @param data
     * @throws Exception
     */
	private void createSI(RunData data) 
		throws Exception
	{    			
		SalesTransaction oTR = (SalesTransaction)oSes.getAttribute(s_TR);
		if (oTR == null || StringUtil.isEmpty(oTR.getSalesTransactionId()))
		{
			if (oPI != null && oPI.getStatus() == i_PROCESSED && vPID != null)
			{
				String sDisc = data.getParameters().getString("DefaultSIDiscount","0");
				oTR = new SalesTransaction();
				List vTD = new ArrayList(vPID.size());
		    	for (int i = 0; i < vPID.size(); i++)
		    	{
		    		PurchaseInvoiceDetail oPID = (PurchaseInvoiceDetail) vPID.get(i); 
		    		SalesTransactionDetail oTD = new SalesTransactionDetail();
		    		BeanUtil.setBean2BeanProperties(oPID, oTD);
		    		Item oItem = ItemTool.getItemByID(oPID.getItemId());
		    		oTD.setItemPrice(oItem.getItemPrice());
		    		oTD.setTaxId(oItem.getTaxId());
		    		oTD.setTaxAmount(TaxTool.getAmountByID(oItem.getTaxId()));
		    		oTD.setItemCost(oPID.getCostPerUnit());
		    		oTD.setDiscount(sDisc + "%");
		    		vTD.add(oTD);
		    	}		    
		    	oTR.setRemark("Automatic SI From " + oPI.getPurchaseInvoiceNo());
	    		TransactionTool.setHeaderProperties(oTR, vTD, null);
		    	oSes.setAttribute(s_TR, oTR);
		    	oSes.setAttribute(s_TD, vTD);
		    	data.setMessage("Sales Invoice Created Successfully");
			}
		}
		else
		{
			data.setMessage("ERROR: Please Create New Transaction in Sales Invoice First!");
		}
	}
}


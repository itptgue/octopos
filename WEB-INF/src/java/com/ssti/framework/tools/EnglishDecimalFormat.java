package com.ssti.framework.tools;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * English Decimal format 
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: EnglishDecimalFormat.java,v 1.4 2007/02/23 14:14:05 albert Exp $ <br>
 * 
 * <pre>
 * $Log: EnglishDecimalFormat.java,v $
 * Revision 1.4  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class EnglishDecimalFormat 
{
	private static final Log log = LogFactory.getLog(IndonesianDecimalFormat.class);

	static final String s_ZERO = " zero";
	static final String s_COMMA = " point";
	static final String s_MINUS = "minus";
	
	private static final String[] majorNames = 
	{
		"",
		" thousand",
		" million",
		" billion",
		" trillion",
		" quadrillion",
		" quintillion"
	};

	private static final String[] tensNames = {
		"",
		" ten",
		" twenty",
		" thirty",
		" fourty",
		" fifty",
		" sixty",
		" seventy",
		" eighty",
		" ninety"
	};

	private static final String[] numNames = {
		"",
		" one",
		" two",
		" three",
		" four",
		" five",
		" six",
		" seven",
		" eight",
		" nine",
		" ten",
		" eleven",
		" twelve",
		" thirteen",
		" fourteen",
		" fifteen",
		" sixteen",
		" seventeen",
		" eighteen",
		" nineteen"
	};
	
	public static String simpleConvert(Object o)
	{
		String s = o.toString();
		StringBuilder sResult = new StringBuilder();
		for (int i = 0; i < s.length(); i++)
		{
			int iIdx = Integer.parseInt(Character.toString(s.charAt(i)));
			String sN = numNames[iIdx];
			if (StringUtil.isEmpty(sN)) sN = s_ZERO;
			sResult.append(sN);	
		}
		return sResult.toString();
	}	
	
	private static String convertLessThanOneThousand(int number) 
	{
		String soFar;
		
		if (number % 100 < 20)
		{
			soFar = numNames[number % 100];
			number /= 100;
		}
		else 
		{
			soFar = numNames[number % 10];
			number /= 10;
			
			soFar = tensNames[number % 10] + soFar;
			number /= 10;
		}
		if (number == 0) return soFar;
		return numNames[number] + " hundred" + soFar;
	}
	
	public static String convert(Number _number) 
	{
		log.debug("num double val : " + Double.toString(_number.doubleValue()));
		BigDecimal bdNumber = new BigDecimal(Double.toString(_number.doubleValue()));
		log.debug("num BD val : " + bdNumber);
		
		double number = bdNumber.setScale(0,BigDecimal.ROUND_DOWN).doubleValue();
		log.debug("rounded val : " + number);
		
		double comma = new BigDecimal(bdNumber.doubleValue() - number)
			.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		
		int iComma = 0;
		
		String sComma = null;
		if (comma > 0)
		{
			log.debug("comma val : " + comma);
			sComma = Double.toString(comma);
			if (sComma.length() > 2)
			{
				sComma = sComma.substring(2);
				if (sComma.length() > 2)
				{
					sComma = sComma.substring(0,2);
				}
			}
			log.debug("comma num : " + sComma);	
			sComma = simpleConvert (sComma);
			log.debug("comma fmtd : " + sComma);
		}
		
	    /* special case */
	    if (number == 0) { return s_ZERO; }

	    String prefix = "";

	    if (number < 0) 
	    {
	        number = -number;
	        prefix = s_MINUS;
	    }

	    String soFar = "";
	    int place = 0;
	    do 
	    {
	    	int n = (int)(number % 1000);
	    	if (n != 0)
	    	{
	    		String s = convertLessThanOneThousand(n);
	    		soFar = s + majorNames[place] + soFar;
	    	}
	    	place++;
	    	number /= 1000;
	    } 
	    while (number > 0);
	    String sResult = (prefix + soFar).trim();
	    if (StringUtil.isNotEmpty(sComma))
	    {
	    	sResult = sResult.concat(s_COMMA).concat(sComma);
	    }
	    return sResult;
	}
}
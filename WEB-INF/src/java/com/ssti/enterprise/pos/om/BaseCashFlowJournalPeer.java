package com.ssti.enterprise.pos.om;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList; 

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.map.CashFlowJournalMapBuilder;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;


  
/**
 */
public abstract class BaseCashFlowJournalPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "pos";

     /** the table name for this class */
    public static final String TABLE_NAME = "cash_flow_journal";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(CashFlowJournalMapBuilder.CLASS_NAME);
    }

      /** the column name for the CASH_FLOW_JOURNAL_ID field */
    public static final String CASH_FLOW_JOURNAL_ID;
      /** the column name for the CASH_FLOW_ID field */
    public static final String CASH_FLOW_ID;
      /** the column name for the ACCOUNT_ID field */
    public static final String ACCOUNT_ID;
      /** the column name for the ACCOUNT_CODE field */
    public static final String ACCOUNT_CODE;
      /** the column name for the ACCOUNT_NAME field */
    public static final String ACCOUNT_NAME;
      /** the column name for the CURRENCY_ID field */
    public static final String CURRENCY_ID;
      /** the column name for the CURRENCY_RATE field */
    public static final String CURRENCY_RATE;
      /** the column name for the AMOUNT field */
    public static final String AMOUNT;
      /** the column name for the AMOUNT_BASE field */
    public static final String AMOUNT_BASE;
      /** the column name for the PROJECT_ID field */
    public static final String PROJECT_ID;
      /** the column name for the DEPARTMENT_ID field */
    public static final String DEPARTMENT_ID;
      /** the column name for the MEMO field */
    public static final String MEMO;
      /** the column name for the DEBIT_CREDIT field */
    public static final String DEBIT_CREDIT;
      /** the column name for the LOCATION_ID field */
    public static final String LOCATION_ID;
      /** the column name for the SUB_LEDGER_TYPE field */
    public static final String SUB_LEDGER_TYPE;
      /** the column name for the SUB_LEDGER_ID field */
    public static final String SUB_LEDGER_ID;
      /** the column name for the SUB_LEDGER_DESC field */
    public static final String SUB_LEDGER_DESC;
      /** the column name for the SUB_LEDGER_TRANS_ID field */
    public static final String SUB_LEDGER_TRANS_ID;
      /** the column name for the CASH_FLOW_TYPE_ID field */
    public static final String CASH_FLOW_TYPE_ID;
  
    static
    {
          CASH_FLOW_JOURNAL_ID = "cash_flow_journal.CASH_FLOW_JOURNAL_ID";
          CASH_FLOW_ID = "cash_flow_journal.CASH_FLOW_ID";
          ACCOUNT_ID = "cash_flow_journal.ACCOUNT_ID";
          ACCOUNT_CODE = "cash_flow_journal.ACCOUNT_CODE";
          ACCOUNT_NAME = "cash_flow_journal.ACCOUNT_NAME";
          CURRENCY_ID = "cash_flow_journal.CURRENCY_ID";
          CURRENCY_RATE = "cash_flow_journal.CURRENCY_RATE";
          AMOUNT = "cash_flow_journal.AMOUNT";
          AMOUNT_BASE = "cash_flow_journal.AMOUNT_BASE";
          PROJECT_ID = "cash_flow_journal.PROJECT_ID";
          DEPARTMENT_ID = "cash_flow_journal.DEPARTMENT_ID";
          MEMO = "cash_flow_journal.MEMO";
          DEBIT_CREDIT = "cash_flow_journal.DEBIT_CREDIT";
          LOCATION_ID = "cash_flow_journal.LOCATION_ID";
          SUB_LEDGER_TYPE = "cash_flow_journal.SUB_LEDGER_TYPE";
          SUB_LEDGER_ID = "cash_flow_journal.SUB_LEDGER_ID";
          SUB_LEDGER_DESC = "cash_flow_journal.SUB_LEDGER_DESC";
          SUB_LEDGER_TRANS_ID = "cash_flow_journal.SUB_LEDGER_TRANS_ID";
          CASH_FLOW_TYPE_ID = "cash_flow_journal.CASH_FLOW_TYPE_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(CashFlowJournalMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(CashFlowJournalMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  19;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.ssti.enterprise.pos.om.CashFlowJournal";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseCashFlowJournalPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(CASH_FLOW_JOURNAL_ID);
          criteria.addSelectColumn(CASH_FLOW_ID);
          criteria.addSelectColumn(ACCOUNT_ID);
          criteria.addSelectColumn(ACCOUNT_CODE);
          criteria.addSelectColumn(ACCOUNT_NAME);
          criteria.addSelectColumn(CURRENCY_ID);
          criteria.addSelectColumn(CURRENCY_RATE);
          criteria.addSelectColumn(AMOUNT);
          criteria.addSelectColumn(AMOUNT_BASE);
          criteria.addSelectColumn(PROJECT_ID);
          criteria.addSelectColumn(DEPARTMENT_ID);
          criteria.addSelectColumn(MEMO);
          criteria.addSelectColumn(DEBIT_CREDIT);
          criteria.addSelectColumn(LOCATION_ID);
          criteria.addSelectColumn(SUB_LEDGER_TYPE);
          criteria.addSelectColumn(SUB_LEDGER_ID);
          criteria.addSelectColumn(SUB_LEDGER_DESC);
          criteria.addSelectColumn(SUB_LEDGER_TRANS_ID);
          criteria.addSelectColumn(CASH_FLOW_TYPE_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static CashFlowJournal row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            CashFlowJournal obj = (CashFlowJournal) cls.newInstance();
            CashFlowJournalPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      CashFlowJournal obj)
        throws TorqueException
    {
        try
        {
                obj.setCashFlowJournalId(row.getValue(offset + 0).asString());
                  obj.setCashFlowId(row.getValue(offset + 1).asString());
                  obj.setAccountId(row.getValue(offset + 2).asString());
                  obj.setAccountCode(row.getValue(offset + 3).asString());
                  obj.setAccountName(row.getValue(offset + 4).asString());
                  obj.setCurrencyId(row.getValue(offset + 5).asString());
                  obj.setCurrencyRate(row.getValue(offset + 6).asBigDecimal());
                  obj.setAmount(row.getValue(offset + 7).asBigDecimal());
                  obj.setAmountBase(row.getValue(offset + 8).asBigDecimal());
                  obj.setProjectId(row.getValue(offset + 9).asString());
                  obj.setDepartmentId(row.getValue(offset + 10).asString());
                  obj.setMemo(row.getValue(offset + 11).asString());
                  obj.setDebitCredit(row.getValue(offset + 12).asInt());
                  obj.setLocationId(row.getValue(offset + 13).asString());
                  obj.setSubLedgerType(row.getValue(offset + 14).asInt());
                  obj.setSubLedgerId(row.getValue(offset + 15).asString());
                  obj.setSubLedgerDesc(row.getValue(offset + 16).asString());
                  obj.setSubLedgerTransId(row.getValue(offset + 17).asString());
                  obj.setCashFlowTypeId(row.getValue(offset + 18).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseCashFlowJournalPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                    
        setDbName(criteria);

        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(CashFlowJournalPeer.row2Object(row, 1,
                CashFlowJournalPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseCashFlowJournalPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(CASH_FLOW_JOURNAL_ID, criteria.remove(CASH_FLOW_JOURNAL_ID));
                                                                                                                                                                                          
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         CashFlowJournalPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                    
        setDbName(criteria);

        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(CashFlowJournal obj) throws TorqueException
    {
        return doSelect(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CashFlowJournal obj) throws TorqueException
    {
          doInsert(buildCriteria(obj));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CashFlowJournal obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CashFlowJournal obj) throws TorqueException
    {
        doDelete(buildSelectCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(CashFlowJournal) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(CashFlowJournal obj, Connection con)
        throws TorqueException
    {
          doInsert(buildCriteria(obj), con);
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(CashFlowJournal) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(CashFlowJournal obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(CashFlowJournal) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(CashFlowJournal obj, Connection con)
        throws TorqueException
    {
        doDelete(buildSelectCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseCashFlowJournalPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(CASH_FLOW_JOURNAL_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( CashFlowJournal obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              criteria.add(CASH_FLOW_JOURNAL_ID, obj.getCashFlowJournalId());
              criteria.add(CASH_FLOW_ID, obj.getCashFlowId());
              criteria.add(ACCOUNT_ID, obj.getAccountId());
              criteria.add(ACCOUNT_CODE, obj.getAccountCode());
              criteria.add(ACCOUNT_NAME, obj.getAccountName());
              criteria.add(CURRENCY_ID, obj.getCurrencyId());
              criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
              criteria.add(AMOUNT, obj.getAmount());
              criteria.add(AMOUNT_BASE, obj.getAmountBase());
              criteria.add(PROJECT_ID, obj.getProjectId());
              criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
              criteria.add(MEMO, obj.getMemo());
              criteria.add(DEBIT_CREDIT, obj.getDebitCredit());
              criteria.add(LOCATION_ID, obj.getLocationId());
              criteria.add(SUB_LEDGER_TYPE, obj.getSubLedgerType());
              criteria.add(SUB_LEDGER_ID, obj.getSubLedgerId());
              criteria.add(SUB_LEDGER_DESC, obj.getSubLedgerDesc());
              criteria.add(SUB_LEDGER_TRANS_ID, obj.getSubLedgerTransId());
              criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
          return criteria;
    }

    /** Build a Criteria object from the data object for this peer, skipping all binary columns */
    public static Criteria buildSelectCriteria( CashFlowJournal obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
                      criteria.add(CASH_FLOW_JOURNAL_ID, obj.getCashFlowJournalId());
                          criteria.add(CASH_FLOW_ID, obj.getCashFlowId());
                          criteria.add(ACCOUNT_ID, obj.getAccountId());
                          criteria.add(ACCOUNT_CODE, obj.getAccountCode());
                          criteria.add(ACCOUNT_NAME, obj.getAccountName());
                          criteria.add(CURRENCY_ID, obj.getCurrencyId());
                          criteria.add(CURRENCY_RATE, obj.getCurrencyRate());
                          criteria.add(AMOUNT, obj.getAmount());
                          criteria.add(AMOUNT_BASE, obj.getAmountBase());
                          criteria.add(PROJECT_ID, obj.getProjectId());
                          criteria.add(DEPARTMENT_ID, obj.getDepartmentId());
                          criteria.add(MEMO, obj.getMemo());
                          criteria.add(DEBIT_CREDIT, obj.getDebitCredit());
                          criteria.add(LOCATION_ID, obj.getLocationId());
                          criteria.add(SUB_LEDGER_TYPE, obj.getSubLedgerType());
                          criteria.add(SUB_LEDGER_ID, obj.getSubLedgerId());
                          criteria.add(SUB_LEDGER_DESC, obj.getSubLedgerDesc());
                          criteria.add(SUB_LEDGER_TRANS_ID, obj.getSubLedgerTransId());
                          criteria.add(CASH_FLOW_TYPE_ID, obj.getCashFlowTypeId());
              return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowJournal retrieveByPK(String pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowJournal retrieveByPK(String pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk), con);
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowJournal retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        CashFlowJournal retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static CashFlowJournal retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (CashFlowJournal)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new ArrayList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( CASH_FLOW_JOURNAL_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



          
                                              
                
                

    /**
     * selects a collection of CashFlowJournal objects pre-filled with their
     * CashFlow objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CashFlowJournalPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinCashFlow(Criteria criteria)
        throws TorqueException
    {
        setDbName(criteria);

        CashFlowJournalPeer.addSelectColumns(criteria);
        int offset = numColumns + 1;
        CashFlowPeer.addSelectColumns(criteria);


                        criteria.addJoin(CashFlowJournalPeer.CASH_FLOW_ID,
            CashFlowPeer.CASH_FLOW_ID);
        

                                                                                                                                                                                                                                                                                                                                                              
        List rows = BasePeer.doSelect(criteria);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = CashFlowJournalPeer.getOMClass();
                    CashFlowJournal obj1 = CashFlowJournalPeer
                .row2Object(row, 1, omClass);
                     omClass = CashFlowPeer.getOMClass();
                    CashFlow obj2 = CashFlowPeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                CashFlowJournal temp_obj1 = (CashFlowJournal)results.get(j);
                CashFlow temp_obj2 = temp_obj1.getCashFlow();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                              temp_obj2.addCashFlowJournal(obj1);
                              break;
                }
            }
                      if (newObject)
            {
                obj2.initCashFlowJournals();
                obj2.addCashFlowJournal(obj1);
            }
                      results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   
    private static void setDbName(Criteria crit)
    {
        // Set the correct dbName if it has not been overridden
        // crit.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (crit.getDbName() == Torque.getDefaultDB())
        {
            crit.setDbName(DATABASE_NAME);
        }
    }
}

package com.ssti.framework.tools;

import java.awt.Toolkit;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.services.pull.TurbinePull;
import org.apache.turbine.util.BrowserDetector;
import org.apache.turbine.util.RunData;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: TemplateUtil.java,v 1.13 2009/05/04 01:34:31 albert Exp $ <br>
 * 
 * <pre>
 * $Log: TemplateUtil.java,v $
 * Revision 1.13  2009/05/04 01:34:31  albert
 * *** empty log message ***
 *
 * Revision 1.12  2008/11/05 02:09:39  albert
 * *** empty log message ***
 *
 * Revision 1.11  2008/10/22 05:54:55  albert
 * *** empty log message ***
 *
 * Revision 1.10  2008/09/10 08:52:20  albert
 * *** empty log message ***
 *
 * Revision 1.9  2008/08/17 02:16:53  albert
 * *** empty log message ***
 *
 * Revision 1.8  2007/08/10 06:00:08  albert
 * *** empty log message ***
 *
 * Revision 1.7  2007/03/12 09:52:53  seph
 * *** empty log message ***
 *
 * Revision 1.6  2007/03/05 16:12:54  seph
 * *** empty log message ***
 *
 * Revision 1.5  2007/02/23 14:14:05  albert
 * *** empty log message ***
 * 
 * </pre><br>
 */
public class TemplateUtil
{   
	static final Log log = LogFactory.getLog(TemplateUtil.class);
	
	public static int[] createNumberList (int iStart, int iEnd)
	{
		int[] aNumber = {1};
		if (iEnd >= iStart)
		{
			aNumber = new int[iEnd - iStart];
			int iIdx = 0;
			for (int i = iStart; i < iEnd; i++)
			{
				aNumber[iIdx] = i;
				iIdx++;
			}			
		}
		return aNumber;
	}
	
	public static BrowserDetector detectBrowser(RunData data)
	{
		return new BrowserDetector (data);
	}
	
	public static boolean isIE(RunData data)
	{
		return detectBrowser(data).getBrowserName().equals(BrowserDetector.MSIE);
	}	

	public static boolean isMozilla(RunData data)
	{
		return detectBrowser(data).getBrowserName().equals(BrowserDetector.MOZILLA);
	}	

	public static boolean isMobile(RunData data)
	{
		StringBuilder s1 = new StringBuilder();
		s1.append("(?i).*((android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|")
		  .append("iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|")
		  .append("p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino).*");
		
		StringBuilder s2 = new StringBuilder();
		s2.append("(?i)1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|")
		  .append("aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|")
		  .append("capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|")
		  .append("el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|")
		  .append("gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|")
		  .append("i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |")
		  .append("kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|")
		  .append("m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|")
		  .append("n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|")
		  .append("pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|")
		  .append("qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|")
		  .append("sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|")
		  .append("ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|")
		  .append("vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|")
		  .append("wmlb|wonu|x700|yas\\-|your|zeto|zte\\-")
		  ;
				
		String ua = data.getUserAgent();
		if(StringUtil.isNotEmpty(ua) &&
		   (ua.matches(s1.toString()) || ua.substring(0,4).matches(s2.toString())) 
		   ) 
		{
			return true;
		}
		return false;
	}	
	
	public static int getStartNo (Number _iCurrent, String _sDetailNo)
	{
		return getStartNo (_iCurrent, Calculator.parse(_sDetailNo));
	}	
	
	public static int getStartNo (Number _iCurrent, Number _iDetailNo)
	{
		if (_iCurrent != null && _iDetailNo != null) 
		{
			int iStart = ((_iCurrent.intValue() - 1) * _iDetailNo.intValue()) + 1;
			if (iStart > 0) return iStart;
		}
		return 1;
	}
	
    
    /**
     * VM Template Helper, to access native array
     * 
     * @param o
     * @param i
     * @return Array member at selected index
     */
	public static double array(double[] o, int i)
	{
		try
		{
			return o[i];
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
		}
		return 0;
	}

    /**
     * 
     * @param o
     * @return List of param
     */
    public static List toList(int[] o)
    {
        ArrayList result = new ArrayList();
        if(o != null)
        {
            try
            {
                int i;
                for(i = 0 ; i < o.length ; i++)
                {
                    result.add((Object) o[i]);
                }
            }
            catch (Exception _oEx)
            {
                _oEx.printStackTrace();
            }
        }
        return result;
    }
    
    /**
     * 
     * @param o
     * @return List of param
     */
    public static List toList(String[] o)
    {
        ArrayList result = new ArrayList();
        if(o != null)
        {
            try
            {
                int i;
                for(i = 0 ; i < o.length ; i++)
                {
                    result.add((Object) o[i]);
                }
            }
            catch (Exception _oEx)
            {
                _oEx.printStackTrace();
            }
        }
        return result;
    }

    /**
     * VM Template Helper, to access native array
     * 
     * @param o
     * @param i
     * @return Array member at selected index
     */
	public static List array(List[] o, int i)
	{
		try
		{
			return o[i];
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
		}
		return null;
	}
	
    /**
     * 
     * @param data
     * @return query string
     */
    public static String pageParam(RunData data)
    {
    	Set vKeys = data.getParameters().keySet();
    	Iterator iter = vKeys.iterator();
    	StringBuilder oResult = new StringBuilder();
    	while (iter.hasNext())
    	{
    		String sKey = (String) iter.next();
    		oResult.append("&").append(sKey).append("=")
    			   .append(StringUtil.encode(data.getParameters().getString(sKey,"")));    		
    	}
    	log.debug(oResult);
    	return oResult.toString();
    }    
    
    public static boolean isTemplateExists(RunData data, String _sName)
    {
    	String sPath = data.getServletContext().getRealPath("/WEB-INF/templates" + _sName);
    	log.debug ("Real Path : " + sPath);    	
    	try 
    	{
    		File oFile = new File (sPath);
    		if (oFile.exists())
    		{
    			return true;
    		}
		} 
    	catch (Exception e) 
    	{
		}
		return false;
    }
    
    /**
     * display sub list in a safe way
     * 
     * @param _vData
     * @param _iOffset
     * @param _iLimit
     * @return
     */
    public static List displayList (List _vData, int _iOffset, int _iLimit)
    {
    	List vResult = new ArrayList();
    	try 
    	{
    		if (_vData != null && _vData.size() >= (_iOffset + _iLimit))
        	{    		
        		vResult = _vData.subList(_iOffset, _iLimit);
        	}			
    		else
    		{
    			vResult = _vData;
    		}
		} 
    	catch (Exception e) 
    	{
    		log.error("Invalid List Limit / Offset");
    		return _vData;
    	}
    	return vResult;
    }
    
    /**
     * add number value to an existing map that contains
     * [String key, Number n]
     *  
     * @param _mMap
     * @param _sKey
     * @param _n
     */
    public static void addToMap(Map _mMap, String _sKey, Number _n)
    {
    	if (_mMap != null)
    	{
	    	Number nExist = (Number) _mMap.get(_sKey);
	    	if (nExist != null)
	    	{
	    		nExist = nExist.doubleValue() + _n.doubleValue();
	    		_mMap.put(_sKey, nExist);
	    	}
	    	else
	    	{
	    		_mMap.put(_sKey, _n);
	    	}
    	}
    }
    
    /**
     * substract number value to an existing map that contains
     * [String key, Number n]
     *  
     * @param _mMap
     * @param _sKey
     * @param _n
     */
    public static void subToMap(Map _mMap, String _sKey, Number _n)
    {
    	Number n = _n.doubleValue() * -1;
    	addToMap(_mMap, _sKey, n);
    }
    
    public static int getScreenWidth()
    {
    	try 
    	{
    		int iWidth = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();	
    		if (iWidth >= 1024) return iWidth;
		} 
    	catch (Exception e) 
		{
			e.printStackTrace();
		}
    	return 1024;
    }
    
    public static int getScreenHeight()
    {
    	try 
    	{
    		int iHeight = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();	
    		if (iHeight >= 768) return iHeight;
		} 
    	catch (Exception e) 
		{
			e.printStackTrace();
		}
    	return 768;
    }
    
    public static boolean isWindowsClient(RunData data)
    {
    	try 
    	{
    	 	String sOS = detectBrowser(data).getBrowserPlatform();
            if (sOS.startsWith("Win"))
            {
            	return true;
            }
		} 
    	catch (Exception e) 
		{
			e.printStackTrace();
		}
    	return false;
    }
    
    public static boolean isUnixClient(RunData data)
    {
    	try 
    	{
    	 	String sOS = detectBrowser(data).getBrowserPlatform();
            if ((!sOS.startsWith("mac") && sOS.endsWith("x")) || sOS.contains("Linux") || sOS.contains("nix"))
            {
            	return true;
            }
		} 
    	catch (Exception e) 
		{
			e.printStackTrace();
		}
    	return false;
    }
    
    public static List subList (List _vTD, int from, int to)
    {
		List vElement = new ArrayList();
		if (from < to && to <= _vTD.size())
		{
			for (int i = from; i <= to; i++)
			{
				vElement.add (_vTD.get(i));
			}
		}
		return vElement;
	}
    
    public static String setLocalPage(RunData data)
    {
    	return setPage(data, "localhost", null);
	}
    
    public static String setLocalPage(RunData data, String _sPage)
    {
    	return setPage(data, "localhost", _sPage);
	}
    
    public static String setPage(RunData data, String _sHost, String _sPage)
    {
    	if (data != null)
    	{
	    	if (StringUtil.isEmpty(_sPage)) _sPage = data.getScreenTemplate();
	    	StringBuilder oURL = new StringBuilder();
	    	String sScheme = data.getRequest().getScheme();
	    	String sHost = _sHost;
	    	if(StringUtil.equals(sScheme, "https") && StringUtil.equals(_sHost, "localhost"))
	    	{
	    		//fix https problem
	    		sHost = data.getRequest().getServerName();
	    	}
	    	oURL.append(sScheme).append("://")
	    		.append(sHost).append(":").append(data.getServerPort()).append("/")
	    		.append(data.getContextPath()).append("/weblayer/template/").append(_sPage);
	    	return oURL.toString();
    	}
    	return "";
	}
    
    public static List divideList (int _iCol, List _v)
    {
        List v = new ArrayList();
        if (_v != null)
        {
            int iSize = _v.size();
            if (iSize > _iCol)
            {
                
                double d = (double)iSize / (double)_iCol;
                int iRow = new BigDecimal(d).setScale(0, BigDecimal.ROUND_UP).intValue(); 

                log.debug("size: " + iSize);
                log.debug("size/col: " + d);
                log.debug("iRow: " + iRow);
                
                for (int i = 0; i < iRow; i++)
                {                    
                    int iFrom = i * _iCol;
                    int iTo = iFrom + _iCol;
                    if (iTo > iSize) iTo = iSize;
                    
                    log.debug("row: " + i);
                    log.debug("frm: " + iFrom);
                    log.debug("to: " + iTo);
                    
                    v.add(_v.subList(iFrom, iTo));
                }
            }
            else
            {
                v.add(_v);
            }
        }
        return v;
    }
    
    public static String generateMenu(RunData data)    				
    	throws Exception
    {	    	        	
    	VelocityContext oCtx = new VelocityContext();
    	oCtx.put("data", data);          

    	Context oGlobal = TurbinePull.getGlobalContext();		
    	for (int i = 0; i < oGlobal.getKeys().length; i++)
    	{
    		String sKey = (String) oGlobal.getKeys()[i];
    		oCtx.put(sKey, oGlobal.get(sKey));
    	}		
    	TurbinePull.populateContext(oCtx, data);
    	
    	StringBuilder oSB = new StringBuilder();
    	StringWriter oWriter = new StringWriter ();
    	Velocity.mergeTemplate("navigations/AppMenu.vm", Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
    	oSB.append(oWriter.toString());		  	    	

    	//create JS FILE
    	String sMenu = oSB.toString()
    			.replaceAll("  ", "")
    			.replaceAll(" ;", ";")
    			.replaceAll("', '", "','")
    			.replaceAll("' ,'", "','")    			
    			.replaceAll("\r\n\r\n", "");
    	
    	String sPath = IOTool.getRealPath(data,"/javascript/menu" + data.getUser().getName() + ".js");
    	File oJS = new File(sPath);
    	BufferedWriter oBW = new BufferedWriter(new FileWriter(oJS));
    	oBW.write(sMenu);
    	oBW.close();
    	
    	return sMenu;
    }

    public static void generateTimeJS(RunData data)    				
    	throws Exception
    {	    	        	
    	String sPath = IOTool.getRealPath(data,"/javascript/time.js");    	
    	File oJS = new File(sPath);
    	if(!oJS.exists())
    	{
	    	VelocityContext oCtx = new VelocityContext();
	    	oCtx.put("data", data);          
	
	    	Context oGlobal = TurbinePull.getGlobalContext();		
	    	for (int i = 0; i < oGlobal.getKeys().length; i++)
	    	{
	    		String sKey = (String) oGlobal.getKeys()[i];
	    		oCtx.put(sKey, oGlobal.get(sKey));
	    	}		
	    	TurbinePull.populateContext(oCtx, data);	
	    	StringWriter oWriter = new StringWriter ();
	    	Velocity.mergeTemplate("screens/js/TimeJS.vm", Attributes.s_DEFAULT_ENCODING, oCtx, oWriter );
	
	    	//create JS FILE
	    	String sJS = oWriter.toString();    	
	    	log.debug("Generate TimeJS: " + sJS);
    		BufferedWriter oBW = new BufferedWriter(new FileWriter(oJS));
    		oBW.write(sJS);
    		oBW.close();
    	}
    }
}
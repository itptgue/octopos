package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to Courier
 */
public abstract class BaseCourier extends BaseObject
{
    /** The Peer class */
    private static final CourierPeer peer =
        new CourierPeer();

        
    /** The value for the courierId field */
    private String courierId;
      
    /** The value for the courierName field */
    private String courierName;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the shippingPrice field */
    private BigDecimal shippingPrice;
      
    /** The value for the carNo field */
    private String carNo;
      
    /** The value for the isInternal field */
    private boolean isInternal;
      
    /** The value for the employeeId field */
    private String employeeId;
                                                
    /** The value for the address field */
    private String address = "";
                                                
    /** The value for the phone1 field */
    private String phone1 = "";
                                                
    /** The value for the phone2 field */
    private String phone2 = "";
                                                
    /** The value for the email field */
    private String email = "";
  
    
    /**
     * Get the CourierId
     *
     * @return String
     */
    public String getCourierId()
    {
        return courierId;
    }

                        
    /**
     * Set the value of CourierId
     *
     * @param v new value
     */
    public void setCourierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierId, v))
              {
            this.courierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CourierName
     *
     * @return String
     */
    public String getCourierName()
    {
        return courierName;
    }

                        
    /**
     * Set the value of CourierName
     *
     * @param v new value
     */
    public void setCourierName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.courierName, v))
              {
            this.courierName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ShippingPrice
     *
     * @return BigDecimal
     */
    public BigDecimal getShippingPrice()
    {
        return shippingPrice;
    }

                        
    /**
     * Set the value of ShippingPrice
     *
     * @param v new value
     */
    public void setShippingPrice(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.shippingPrice, v))
              {
            this.shippingPrice = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CarNo
     *
     * @return String
     */
    public String getCarNo()
    {
        return carNo;
    }

                        
    /**
     * Set the value of CarNo
     *
     * @param v new value
     */
    public void setCarNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.carNo, v))
              {
            this.carNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsInternal
     *
     * @return boolean
     */
    public boolean getIsInternal()
    {
        return isInternal;
    }

                        
    /**
     * Set the value of IsInternal
     *
     * @param v new value
     */
    public void setIsInternal(boolean v) 
    {
    
                  if (this.isInternal != v)
              {
            this.isInternal = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the EmployeeId
     *
     * @return String
     */
    public String getEmployeeId()
    {
        return employeeId;
    }

                        
    /**
     * Set the value of EmployeeId
     *
     * @param v new value
     */
    public void setEmployeeId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.employeeId, v))
              {
            this.employeeId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Address
     *
     * @return String
     */
    public String getAddress()
    {
        return address;
    }

                        
    /**
     * Set the value of Address
     *
     * @param v new value
     */
    public void setAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.address, v))
              {
            this.address = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone1
     *
     * @return String
     */
    public String getPhone1()
    {
        return phone1;
    }

                        
    /**
     * Set the value of Phone1
     *
     * @param v new value
     */
    public void setPhone1(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone1, v))
              {
            this.phone1 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Phone2
     *
     * @return String
     */
    public String getPhone2()
    {
        return phone2;
    }

                        
    /**
     * Set the value of Phone2
     *
     * @param v new value
     */
    public void setPhone2(String v) 
    {
    
                  if (!ObjectUtils.equals(this.phone2, v))
              {
            this.phone2 = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Email
     *
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

                        
    /**
     * Set the value of Email
     *
     * @param v new value
     */
    public void setEmail(String v) 
    {
    
                  if (!ObjectUtils.equals(this.email, v))
              {
            this.email = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CourierId");
              fieldNames.add("CourierName");
              fieldNames.add("Description");
              fieldNames.add("ShippingPrice");
              fieldNames.add("CarNo");
              fieldNames.add("IsInternal");
              fieldNames.add("EmployeeId");
              fieldNames.add("Address");
              fieldNames.add("Phone1");
              fieldNames.add("Phone2");
              fieldNames.add("Email");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CourierId"))
        {
                return getCourierId();
            }
          if (name.equals("CourierName"))
        {
                return getCourierName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("ShippingPrice"))
        {
                return getShippingPrice();
            }
          if (name.equals("CarNo"))
        {
                return getCarNo();
            }
          if (name.equals("IsInternal"))
        {
                return Boolean.valueOf(getIsInternal());
            }
          if (name.equals("EmployeeId"))
        {
                return getEmployeeId();
            }
          if (name.equals("Address"))
        {
                return getAddress();
            }
          if (name.equals("Phone1"))
        {
                return getPhone1();
            }
          if (name.equals("Phone2"))
        {
                return getPhone2();
            }
          if (name.equals("Email"))
        {
                return getEmail();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CourierPeer.COURIER_ID))
        {
                return getCourierId();
            }
          if (name.equals(CourierPeer.COURIER_NAME))
        {
                return getCourierName();
            }
          if (name.equals(CourierPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(CourierPeer.SHIPPING_PRICE))
        {
                return getShippingPrice();
            }
          if (name.equals(CourierPeer.CAR_NO))
        {
                return getCarNo();
            }
          if (name.equals(CourierPeer.IS_INTERNAL))
        {
                return Boolean.valueOf(getIsInternal());
            }
          if (name.equals(CourierPeer.EMPLOYEE_ID))
        {
                return getEmployeeId();
            }
          if (name.equals(CourierPeer.ADDRESS))
        {
                return getAddress();
            }
          if (name.equals(CourierPeer.PHONE1))
        {
                return getPhone1();
            }
          if (name.equals(CourierPeer.PHONE2))
        {
                return getPhone2();
            }
          if (name.equals(CourierPeer.EMAIL))
        {
                return getEmail();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCourierId();
            }
              if (pos == 1)
        {
                return getCourierName();
            }
              if (pos == 2)
        {
                return getDescription();
            }
              if (pos == 3)
        {
                return getShippingPrice();
            }
              if (pos == 4)
        {
                return getCarNo();
            }
              if (pos == 5)
        {
                return Boolean.valueOf(getIsInternal());
            }
              if (pos == 6)
        {
                return getEmployeeId();
            }
              if (pos == 7)
        {
                return getAddress();
            }
              if (pos == 8)
        {
                return getPhone1();
            }
              if (pos == 9)
        {
                return getPhone2();
            }
              if (pos == 10)
        {
                return getEmail();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CourierPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CourierPeer.doInsert((Courier) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CourierPeer.doUpdate((Courier) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key courierId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setCourierId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setCourierId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCourierId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public Courier copy() throws TorqueException
    {
        return copyInto(new Courier());
    }
  
    protected Courier copyInto(Courier copyObj) throws TorqueException
    {
          copyObj.setCourierId(courierId);
          copyObj.setCourierName(courierName);
          copyObj.setDescription(description);
          copyObj.setShippingPrice(shippingPrice);
          copyObj.setCarNo(carNo);
          copyObj.setIsInternal(isInternal);
          copyObj.setEmployeeId(employeeId);
          copyObj.setAddress(address);
          copyObj.setPhone1(phone1);
          copyObj.setPhone2(phone2);
          copyObj.setEmail(email);
  
                    copyObj.setCourierId((String)null);
                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CourierPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("Courier\n");
        str.append("-------\n")
           .append("CourierId            : ")
           .append(getCourierId())
           .append("\n")
           .append("CourierName          : ")
           .append(getCourierName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("ShippingPrice        : ")
           .append(getShippingPrice())
           .append("\n")
           .append("CarNo                : ")
           .append(getCarNo())
           .append("\n")
           .append("IsInternal           : ")
           .append(getIsInternal())
           .append("\n")
           .append("EmployeeId           : ")
           .append(getEmployeeId())
           .append("\n")
           .append("Address              : ")
           .append(getAddress())
           .append("\n")
           .append("Phone1               : ")
           .append(getPhone1())
           .append("\n")
           .append("Phone2               : ")
           .append(getPhone2())
           .append("\n")
           .append("Email                : ")
           .append(getEmail())
           .append("\n")
        ;
        return(str.toString());
    }
}

package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.InvoiceMessage;
import com.ssti.enterprise.pos.om.InvoiceMessagePeer;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: InvoiceMessageTool.java,v 1.8 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: InvoiceMessageTool.java,v $
 *
 * </pre><br>
 */
public class InvoiceMessageTool extends BaseTool 
{
	private static InvoiceMessageTool instance = null;
	
	public static synchronized InvoiceMessageTool getInstance()
	{
		if (instance == null) instance = new InvoiceMessageTool();
		return instance;
	}		
	
	public static List getAll()
		throws Exception
	{
		Criteria oCrit = new Criteria();	
		oCrit.addDescendingOrderByColumn(InvoiceMessagePeer.START_DATE);
		return InvoiceMessagePeer.doSelect(oCrit);
	}

	public static List getActive(Connection _oConn)
			throws Exception
	{
		Date dToday = new Date();
		
		Criteria oCrit = new Criteria();
		oCrit.add(InvoiceMessagePeer.END_DATE, dToday, Criteria.GREATER_EQUAL);
		return InvoiceMessagePeer.doSelect(oCrit, _oConn);			
	}
	
	public static InvoiceMessage getByID(String _sID)
		throws Exception
	{
		return getByID(_sID,null);
	}

	public static InvoiceMessage getByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(InvoiceMessagePeer.INVOICE_MESSAGE_ID, _sID);
		List vData = InvoiceMessagePeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (InvoiceMessage)vData.get(0);
		}
		return null;
	}
	
	public static InvoiceMessage getMessage(SalesTransaction _oTR, Connection _oConn)
		throws Exception
	{
		if (_oTR != null)
		{
			return getMessage (_oTR.getTransactionDate(), _oTR.getLocationId(), _oTR.getCustomerTypeId(), _oConn);
		}
		return null;
	}	

	public static InvoiceMessage getMessage(Date _dTrans, String _sLocID, String _sCTypeID, Connection _oConn)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		oCrit.add(InvoiceMessagePeer.START_DATE, _dTrans, Criteria.LESS_EQUAL);
		oCrit.add(InvoiceMessagePeer.END_DATE, _dTrans, Criteria.GREATER_EQUAL);

		if (StringUtil.isNotEmpty(_sLocID))
		{
			Criteria.Criterion oLoc1  = oCrit.getNewCriterion(InvoiceMessagePeer.LOCATION_ID, _sLocID, Criteria.EQUAL);
			Criteria.Criterion oLoc2  = oCrit.getNewCriterion(InvoiceMessagePeer.LOCATION_ID, "", Criteria.EQUAL);
			oCrit.add(oLoc1.or(oLoc2));
		}
		if (StringUtil.isNotEmpty(_sCTypeID))
		{
			Criteria.Criterion oCustType1 = oCrit.getNewCriterion(InvoiceMessagePeer.CUSTOMER_TYPE_ID, _sCTypeID ,Criteria.EQUAL);
			Criteria.Criterion oCustType2 = oCrit.getNewCriterion(InvoiceMessagePeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);
			oCrit.add(oCustType1.or(oCustType2));
		}
		List vData = InvoiceMessagePeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (InvoiceMessage)vData.get(0);
		}
		return null;		
    }
	
	public static InvoiceMessage getExists(InvoiceMessage _oData)
    	throws Exception
    {
		Criteria oCrit = new Criteria();
		Criteria.Criterion oDT1  = oCrit.getNewCriterion(InvoiceMessagePeer.START_DATE, _oData.getStartDate(), Criteria.LESS_EQUAL);
		Criteria.Criterion oDT2  = oCrit.getNewCriterion(InvoiceMessagePeer.END_DATE, _oData.getStartDate(), Criteria.GREATER_EQUAL);
		Criteria.Criterion oDT3  = oCrit.getNewCriterion(InvoiceMessagePeer.START_DATE, _oData.getEndDate(), Criteria.LESS_EQUAL);
		Criteria.Criterion oDT4  = oCrit.getNewCriterion(InvoiceMessagePeer.END_DATE, _oData.getEndDate(), Criteria.GREATER_EQUAL);
		
		oCrit.add((oDT1.and(oDT2)).or(oDT3.and(oDT4)));
		
		if (StringUtil.isNotEmpty(_oData.getLocationId()))
		{
			Criteria.Criterion oLoc1  = oCrit.getNewCriterion(InvoiceMessagePeer.LOCATION_ID, _oData.getLocationId(), Criteria.EQUAL);
			Criteria.Criterion oLoc2  = oCrit.getNewCriterion(InvoiceMessagePeer.LOCATION_ID, "", Criteria.EQUAL);
			oCrit.add(oLoc1.or(oLoc2));
		}
		if (StringUtil.isNotEmpty(_oData.getCustomerTypeId()))
		{
			Criteria.Criterion oCustType1 = oCrit.getNewCriterion(InvoiceMessagePeer.CUSTOMER_TYPE_ID, _oData.getCustomerTypeId() ,Criteria.EQUAL);
			Criteria.Criterion oCustType2 = oCrit.getNewCriterion(InvoiceMessagePeer.CUSTOMER_TYPE_ID, "" ,Criteria.EQUAL);
			oCrit.add(oCustType1.or(oCustType2));
		}
		oCrit.add(InvoiceMessagePeer.INVOICE_MESSAGE_ID, (Object)_oData.getId(), Criteria.NOT_EQUAL);
		List vData = InvoiceMessagePeer.doSelect(oCrit);
		if (vData.size() > 0)
		{
			return (InvoiceMessage)vData.get(0);
		}
		return null;		
    }	

}

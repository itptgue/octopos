package com.ssti.enterprise.pos.om.map;

import java.util.Date;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;

/**
  */
public class InventoryTransactionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.ssti.enterprise.pos.om.map.InventoryTransactionMapBuilder";

    /**
     * Item
     * @deprecated use InventoryTransactionPeer.TABLE_NAME constant
     */
    public static String getTable()
    {
        return "inventory_transaction";
    }

  
    /**
     * inventory_transaction.INVENTORY_TRANSACTION_ID
     * @return the column name for the INVENTORY_TRANSACTION_ID field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.INVENTORY_TRANSACTION_ID constant
     */
    public static String getInventoryTransaction_InventoryTransactionId()
    {
        return "inventory_transaction.INVENTORY_TRANSACTION_ID";
    }
  
    /**
     * inventory_transaction.TRANSACTION_ID
     * @return the column name for the TRANSACTION_ID field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.TRANSACTION_ID constant
     */
    public static String getInventoryTransaction_TransactionId()
    {
        return "inventory_transaction.TRANSACTION_ID";
    }
  
    /**
     * inventory_transaction.TRANSACTION_NO
     * @return the column name for the TRANSACTION_NO field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.TRANSACTION_NO constant
     */
    public static String getInventoryTransaction_TransactionNo()
    {
        return "inventory_transaction.TRANSACTION_NO";
    }
  
    /**
     * inventory_transaction.TRANSACTION_DETAIL_ID
     * @return the column name for the TRANSACTION_DETAIL_ID field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.TRANSACTION_DETAIL_ID constant
     */
    public static String getInventoryTransaction_TransactionDetailId()
    {
        return "inventory_transaction.TRANSACTION_DETAIL_ID";
    }
  
    /**
     * inventory_transaction.TRANSACTION_TYPE
     * @return the column name for the TRANSACTION_TYPE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.TRANSACTION_TYPE constant
     */
    public static String getInventoryTransaction_TransactionType()
    {
        return "inventory_transaction.TRANSACTION_TYPE";
    }
  
    /**
     * inventory_transaction.TRANSACTION_DATE
     * @return the column name for the TRANSACTION_DATE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.TRANSACTION_DATE constant
     */
    public static String getInventoryTransaction_TransactionDate()
    {
        return "inventory_transaction.TRANSACTION_DATE";
    }
  
    /**
     * inventory_transaction.LOCATION_ID
     * @return the column name for the LOCATION_ID field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.LOCATION_ID constant
     */
    public static String getInventoryTransaction_LocationId()
    {
        return "inventory_transaction.LOCATION_ID";
    }
  
    /**
     * inventory_transaction.LOCATION_NAME
     * @return the column name for the LOCATION_NAME field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.LOCATION_NAME constant
     */
    public static String getInventoryTransaction_LocationName()
    {
        return "inventory_transaction.LOCATION_NAME";
    }
  
    /**
     * inventory_transaction.ITEM_ID
     * @return the column name for the ITEM_ID field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.ITEM_ID constant
     */
    public static String getInventoryTransaction_ItemId()
    {
        return "inventory_transaction.ITEM_ID";
    }
  
    /**
     * inventory_transaction.ITEM_CODE
     * @return the column name for the ITEM_CODE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.ITEM_CODE constant
     */
    public static String getInventoryTransaction_ItemCode()
    {
        return "inventory_transaction.ITEM_CODE";
    }
  
    /**
     * inventory_transaction.QTY_CHANGES
     * @return the column name for the QTY_CHANGES field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.QTY_CHANGES constant
     */
    public static String getInventoryTransaction_QtyChanges()
    {
        return "inventory_transaction.QTY_CHANGES";
    }
  
    /**
     * inventory_transaction.PRICE
     * @return the column name for the PRICE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.PRICE constant
     */
    public static String getInventoryTransaction_Price()
    {
        return "inventory_transaction.PRICE";
    }
  
    /**
     * inventory_transaction.COST
     * @return the column name for the COST field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.COST constant
     */
    public static String getInventoryTransaction_Cost()
    {
        return "inventory_transaction.COST";
    }
  
    /**
     * inventory_transaction.TOTAL_COST
     * @return the column name for the TOTAL_COST field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.TOTAL_COST constant
     */
    public static String getInventoryTransaction_TotalCost()
    {
        return "inventory_transaction.TOTAL_COST";
    }
  
    /**
     * inventory_transaction.QTY_BALANCE
     * @return the column name for the QTY_BALANCE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.QTY_BALANCE constant
     */
    public static String getInventoryTransaction_QtyBalance()
    {
        return "inventory_transaction.QTY_BALANCE";
    }
  
    /**
     * inventory_transaction.VALUE_BALANCE
     * @return the column name for the VALUE_BALANCE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.VALUE_BALANCE constant
     */
    public static String getInventoryTransaction_ValueBalance()
    {
        return "inventory_transaction.VALUE_BALANCE";
    }
  
    /**
     * inventory_transaction.DESCRIPTION
     * @return the column name for the DESCRIPTION field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.DESCRIPTION constant
     */
    public static String getInventoryTransaction_Description()
    {
        return "inventory_transaction.DESCRIPTION";
    }
  
    /**
     * inventory_transaction.CREATE_DATE
     * @return the column name for the CREATE_DATE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.CREATE_DATE constant
     */
    public static String getInventoryTransaction_CreateDate()
    {
        return "inventory_transaction.CREATE_DATE";
    }
  
    /**
     * inventory_transaction.UPDATE_DATE
     * @return the column name for the UPDATE_DATE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.UPDATE_DATE constant
     */
    public static String getInventoryTransaction_UpdateDate()
    {
        return "inventory_transaction.UPDATE_DATE";
    }
  
    /**
     * inventory_transaction.ITEM_PRICE
     * @return the column name for the ITEM_PRICE field
     * @deprecated use InventoryTransactionPeer.inventory_transaction.ITEM_PRICE constant
     */
    public static String getInventoryTransaction_ItemPrice()
    {
        return "inventory_transaction.ITEM_PRICE";
    }
  
    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("pos");

        dbMap.addTable("inventory_transaction");
        TableMap tMap = dbMap.getTable("inventory_transaction");

        tMap.setPrimaryKeyMethod("none");


                    tMap.addPrimaryKey("inventory_transaction.INVENTORY_TRANSACTION_ID", "");
                          tMap.addColumn("inventory_transaction.TRANSACTION_ID", "");
                          tMap.addColumn("inventory_transaction.TRANSACTION_NO", "");
                          tMap.addColumn("inventory_transaction.TRANSACTION_DETAIL_ID", "");
                            tMap.addColumn("inventory_transaction.TRANSACTION_TYPE", Integer.valueOf(0));
                          tMap.addColumn("inventory_transaction.TRANSACTION_DATE", new Date());
                          tMap.addColumn("inventory_transaction.LOCATION_ID", "");
                          tMap.addColumn("inventory_transaction.LOCATION_NAME", "");
                          tMap.addColumn("inventory_transaction.ITEM_ID", "");
                          tMap.addColumn("inventory_transaction.ITEM_CODE", "");
                            tMap.addColumn("inventory_transaction.QTY_CHANGES", bd_ZERO);
                            tMap.addColumn("inventory_transaction.PRICE", bd_ZERO);
                            tMap.addColumn("inventory_transaction.COST", bd_ZERO);
                            tMap.addColumn("inventory_transaction.TOTAL_COST", bd_ZERO);
                            tMap.addColumn("inventory_transaction.QTY_BALANCE", bd_ZERO);
                            tMap.addColumn("inventory_transaction.VALUE_BALANCE", bd_ZERO);
                          tMap.addColumn("inventory_transaction.DESCRIPTION", "");
                          tMap.addColumn("inventory_transaction.CREATE_DATE", new Date());
                          tMap.addColumn("inventory_transaction.UPDATE_DATE", new Date());
                            tMap.addColumn("inventory_transaction.ITEM_PRICE", bd_ZERO);
          }
}

package com.ssti.framework.chart.helper;

import java.awt.Color;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BaseChartHelper.java,v 1.3 2007/02/23 14:11:15 albert Exp $ <br>
 *
 * <pre>
 * $Log: BaseChartHelper.java,v $
 * Revision 1.3  2007/02/23 14:11:15  albert
 * javadoc alignment
 * 
 * </pre><br>
 *
 */
public abstract class BaseChartHelper 
{
	protected static final int i_WIDTH  = 800;	
	protected static final int i_HEIGHT = 480;

  	protected Color getColorByCode (String _sColorCode)
  	{
  		if (_sColorCode.equalsIgnoreCase("white"))     return Color.white;
  		if (_sColorCode.equalsIgnoreCase("red"))       return Color.red;
  		if (_sColorCode.equalsIgnoreCase("green"))     return Color.green;
  		if (_sColorCode.equalsIgnoreCase("blue"))      return Color.blue;
  		if (_sColorCode.equalsIgnoreCase("cyan")) 	   return Color.cyan;
  		if (_sColorCode.equalsIgnoreCase("magenta"))   return Color.magenta;
  		if (_sColorCode.equalsIgnoreCase("yellow"))    return Color.yellow;
  		if (_sColorCode.equalsIgnoreCase("black"))     return Color.black;
  		if (_sColorCode.equalsIgnoreCase("lightgray")) return Color.lightGray;
  		if (_sColorCode.equalsIgnoreCase("gray"))      return Color.gray;
		if (_sColorCode.equalsIgnoreCase("darkgray"))  return Color.darkGray;
  		if (_sColorCode.equalsIgnoreCase("orange"))    return Color.orange;
  		if (_sColorCode.equalsIgnoreCase("pink"))      return Color.pink;
  		return Color.white;
  	}

}

package com.ssti.enterprise.pos.tools;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.om.Persistent;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.CustomerType;
import com.ssti.enterprise.pos.om.Employee;
import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PaymentTerm;
import com.ssti.enterprise.pos.om.PaymentType;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.om.Unit;
import com.ssti.enterprise.pos.om.UpdateHistory;
import com.ssti.enterprise.pos.om.UpdateHistoryPeer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.enterprise.pos.om.VendorType;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;

/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: Master Update History Log</b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 2018-03-02
 * - initial version 
 * </pre><br>
 */
public class UpdateHistoryTool extends BaseTool
{
	private static Log log = LogFactory.getLog(UpdateHistoryTool.class);
	
	static UpdateHistoryTool instance = null;
	
	public static synchronized UpdateHistoryTool getInstance() 
	{
		if (instance == null) instance = new UpdateHistoryTool();
		return instance;
	}		
	
	private static void createUpdateHistory(String _sID, 
										    String _sCode, 
										    String _sClass,
										    String _sField, 
											Object _oOldVal, 
											Object _oNewVal, 
											String _sUserName, 
											Connection _oConn)
    	throws Exception
    {
    	String sOldValue = "";
    	String sNewValue = "";
		
		if (_oOldVal != null) 
        {
            sOldValue = _oOldVal.toString();
            if (_oOldVal instanceof Number) sOldValue = CustomFormatter.fmt(sOldValue);
        }
		if (_oNewVal != null) 
        {
            sNewValue = _oNewVal.toString();
            if (_oNewVal instanceof Number) sNewValue = CustomFormatter.fmt(sNewValue);
        }		
        
        if (_sField.equals("UnitId")           ) { String[] aData = setUnitId            (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("PurchaseUnitId")   ) { String[] aData = setUnitId            (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("KategoriId")       ) { String[] aData = setKategoriId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("PreferedVendorId") ) { String[] aData = setVendorId  		 (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("TaxId")            ) { String[] aData = setTaxId             (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("PurchaseTaxId")    ) { String[] aData = setTaxId     		 (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("DefaultTaxId")     ) { String[] aData = setTaxId     		 (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("DefaultCurrencyId")) { String[] aData = setCurrencyId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("CurrencyId") 	   ) { String[] aData = setCurrencyId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("LastPurchaseCurr") ) { String[] aData = setCurrencyId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("CustomerTypeId")   ) { String[] aData = setCustomerTypeId    (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("VendorTypeId")     ) { String[] aData = setVendorTypeId      (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("LocationId")       ) { String[] aData = setLocationId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("DefaultLocationId")) { String[] aData = setLocationId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("DefaultTypeId")    ) { String[] aData = setPaymentTypeId     (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("PaymentTypeId")    ) { String[] aData = setPaymentTypeId     (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("DefaultTermId")    ) { String[] aData = setPaymentTermId     (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}        
        if (_sField.equals("PaymentTermId")    ) { String[] aData = setPaymentTermId     (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        if (_sField.equals("DefaultEmployeeId")) { String[] aData = setEmployeeId        (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];}
        
        if (_sField.equals("ParentId"))
        {
        	if(StringUtil.equalsIgnoreCase(_sClass, "Employee"))  { String[] aData = setEmployeeId (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1]; }
        	if(StringUtil.equalsIgnoreCase(_sClass, "Customer"))  { String[] aData = setCustomerId (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1]; }
        	if(StringUtil.equalsIgnoreCase(_sClass, "Kategori"))  { String[] aData = setKategoriId (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1]; }
        	if(StringUtil.equalsIgnoreCase(_sClass, "Account"))   { String[] aData = setAccount (sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1]; }
        }
        
        if (StringUtil.containsIgnoreCase(_sField,("ArAccount"))  ||  
            StringUtil.containsIgnoreCase(_sField,("ApAccount"))  || 
            _sField.endsWith("Account")) 
        {
        	String[] aData = setAccount(sOldValue, sNewValue, _oConn); sOldValue = aData[0]; sNewValue = aData[1];
        }
                           
    	UpdateHistory oHist = new UpdateHistory ();
    	oHist.setMasterId   (_sID);
    	oHist.setMasterCode (_sCode);
    	oHist.setMasterType (_sClass);
    	oHist.setUpdatePart (StringUtil.cut(_sField, 100, ""));
    	oHist.setOldValue   (StringUtil.cut(sOldValue, 255, ""));
    	oHist.setNewValue   (StringUtil.cut(sNewValue, 255, ""));
    	oHist.setUpdateDate (new Date());
    	oHist.setUserName   (_sUserName);
    	
    	StringBuilder oDesc = new StringBuilder ("Change ");
    	oDesc.append (_sField);
    	oDesc.append (" From : " );
    	oDesc.append (sOldValue);
    	oDesc.append (" To : " );
    	oDesc.append (sNewValue);
    	oDesc.append (" At : ");
    	oDesc.append (CustomFormatter.formatDateTime(oHist.getUpdateDate()));    	
    	oDesc.append (" By : ");
    	oDesc.append (_sUserName);
		
		log.debug("History : " + oDesc);

    	oHist.setUpdateDesc(oDesc.toString());
    	oHist.setHistoryId(IDGenerator.generateSysID());
		oHist.save(_oConn);    	
    }
    
	public static void createHistory(Persistent _oOld, 
									 Persistent _oNew, 
									 String  _sUser, 
									 Connection _oConn)
    	throws Exception
    {
		if(_oOld != null && _oNew != null)
		{
			Class aOldItemClass = _oOld.getClass();
			Class aNewItemClass = _oNew.getClass();
	
			String sClass = aOldItemClass.getSimpleName();
			String sID = sClass + "Id";
			String sCode = _oNew.getPrimaryKey().toString();
			if(_oNew instanceof MasterOM)
			{
				sCode = ((MasterOM)_oNew).getCode();
			}
			
			Method mFld = aOldItemClass.getMethod ("getFieldNames", (Class[])null);
			List vFields = (List) mFld.invoke(_oOld, (Object[])null);
					
			for (int i = 0; i < vFields.size(); i++)
			{
				String sFieldName = (String) vFields.get(i);
							
				if (!sFieldName.equals(sID) && !sFieldName.equals("UpdateDate"))
				{ 
					Method oOldMethod = aOldItemClass.getMethod ("get" + sFieldName, (Class[])null);
					Method oNewMethod = aNewItemClass.getMethod ("get" + sFieldName, (Class[])null);
					
					//log.debug("Field : "  + sFieldName + "   -  Method : " + oNewMethod);
					
					Object oOldValue = oOldMethod.invoke(_oOld, (Object[])null);
					Object oNewValue = oNewMethod.invoke(_oNew, (Object[])null);
	            	
					//log.debug("Old Value : " + oOldValue + " - New Value : " + oNewValue);
	
					if (oNewValue != null)
					{
						if (oNewValue instanceof Number)
						{
							//log.debug("Field is Number");
	
							Number nNewValue = (Number) oNewValue;
							Number nOldValue = (Number) oOldValue;
							
							if (nOldValue != null && (nNewValue.doubleValue() != nOldValue.doubleValue()))
							{
								nNewValue = Calculator.precise(nNewValue, 2);
								nOldValue = Calculator.precise(nOldValue, 2);							
								createUpdateHistory(_oNew.getPrimaryKey().toString(), sCode, 
								                        sClass, sFieldName, nOldValue, nNewValue, _sUser, _oConn);
							}
						}
						else 
						{						
							if (!oNewValue.equals(oOldValue))
							{
								createUpdateHistory(_oNew.getPrimaryKey().toString(), sCode, 
														sClass, sFieldName, oOldValue, oNewValue, _sUser, _oConn);
							}
						}
					}
				}
			}
		}
	}

	public static void createDelHistory(Persistent _oOld, 
										String _sUserName, 
										Connection _oConn)
		throws Exception
	{
		if(_oOld != null)
		{
			String sID = _oOld.getPrimaryKey().toString();
			String sCode = ((MasterOM)_oOld).getCode();
			String sClass = _oOld.getClass().getSimpleName();
			UpdateHistory oHistory = new UpdateHistory();
			oHistory.setMasterId   (sID);
			oHistory.setMasterCode (sCode);
			oHistory.setMasterType (sClass);
			oHistory.setUpdatePart ("delete");
			oHistory.setOldValue   ("");
			oHistory.setNewValue   ("");
			oHistory.setUpdateDate (new Date());
			oHistory.setUserName   (_sUserName);
	
			StringBuilder oDesc = new StringBuilder ("Delete ");
			oDesc.append (sClass);
			oDesc.append (" Code : ");
			oDesc.append (sCode);	
			oDesc.append (" At : ");
			oDesc.append (CustomFormatter.formatDateTime(oHistory.getUpdateDate()));    	
			oDesc.append (" By : ");
			oDesc.append (_sUserName);
	
			log.debug("History : " + oDesc);
	
			oHistory.setUpdateDesc(StringUtil.cut(oDesc.toString(), 255, ""));
			oHistory.setHistoryId(IDGenerator.generateSysID());
			oHistory.save(_oConn);    
		}
	}
	
	//finder
	public static List findData (String _sItemID, 
	  							 String _sStartDate, 
	  							 String _sEndDate, 
								 String _sUpdatedPart, 
								 String _sKeywords,
								 int _iCondition,
								 int _iSortBy)
		throws Exception
	{
		Date dStart = CustomParser.parseDate (_sStartDate);
		Date dEnd   = CustomParser.parseDate (_sEndDate);
		dStart = DateUtil.getStartOfDayDate(dStart);
		dEnd = DateUtil.getEndOfDayDate(dEnd);
		Criteria oCrit = new Criteria();

		if (dStart != null) {
        	oCrit.add(UpdateHistoryPeer.UPDATE_DATE, dStart, Criteria.GREATER_EQUAL);   
		}
		if (dEnd != null) {
        	oCrit.and(UpdateHistoryPeer.UPDATE_DATE, dEnd, Criteria.LESS_EQUAL);   
		}
		if (_sItemID != null && !_sItemID.equals("")) {
        	oCrit.add(UpdateHistoryPeer.MASTER_ID, _sItemID);   
		}
		if (_sUpdatedPart != null && !_sUpdatedPart.equals("") && !_sUpdatedPart.equals(" ")) {
        	oCrit.add(UpdateHistoryPeer.UPDATE_PART, _sUpdatedPart);   
		}
	    //define sorting criteria
	    if (_iSortBy == 1) {
	    	oCrit.addAscendingOrderByColumn(UpdateHistoryPeer.MASTER_CODE);
	    }
		else if (_iSortBy == 4) {
			oCrit.addDescendingOrderByColumn(UpdateHistoryPeer.UPDATE_DATE);
		}
		
		if (!_sKeywords.equals("")) {			
			if (_iCondition == 4) {
				oCrit.add(UpdateHistoryPeer.UPDATE_DESC,
					(Object) SqlUtil.like (UpdateHistoryPeer.UPDATE_DESC, _sKeywords, false, true), Criteria.CUSTOM);
			}
			else if (_iCondition == 5) {
				oCrit.add(UpdateHistoryPeer.USER_NAME, _sKeywords);
			}	
		}
		log.debug(oCrit);
		return UpdateHistoryPeer.doSelect(oCrit);
	}			
	
	//Report filter methods
	//by item
	public static List getDataList (List _vData) 
    	throws Exception
    {		
		List vHist = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			UpdateHistory oHist = (UpdateHistory) _vData.get(i);			
			if (!isInList (vHist, oHist)) {
				vHist.add (oHist);
			}
		}
		return vHist;
	}
	
	private static boolean isInList (List _vHist, UpdateHistory _oHist) 
	{
		for (int i = 0; i < _vHist.size(); i++) {
			UpdateHistory oHist = (UpdateHistory) _vHist.get(i);			
			if (oHist.getMasterId().equals(_oHist.getMasterId())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByItemID (List _vHist, String _sItemID) 
    	throws Exception
    { 
		List vHist = new ArrayList(_vHist); 
		UpdateHistory oHist = null;
		Iterator oIter = vHist.iterator();
		while (oIter.hasNext()) {
			oHist = (UpdateHistory ) oIter.next();
			if (!oHist.getMasterId().equals(_sItemID)) {
				oIter.remove();
			}
		}
		return vHist;
	}		
	
	//by part
	public static List getUpdatePartList (List _vData) 
    	throws Exception
    {		
		List vHist = new ArrayList (_vData.size());
		for (int i = 0; i < _vData.size(); i++) {
			UpdateHistory oHist = (UpdateHistory) _vData.get(i);			
			if (!isUpdatePartInList (vHist, oHist)) {
				vHist.add (oHist);
			}
		}
		return vHist;
	}
	
	private static boolean isUpdatePartInList (List _vHist, UpdateHistory _oHist) 
	{
		for (int i = 0; i < _vHist.size(); i++) {
			UpdateHistory oHist = (UpdateHistory) _vHist.get(i);			
			if (oHist.getUpdatePart().equals(_oHist.getUpdatePart())) {			
				return true;
			}
		}
		return false;
	}
	
	public static List filterTransByUpdatePart (List _vHist, String _sUpdatePart) 
    	throws Exception
    { 
		List vHist = new ArrayList(_vHist); 
		UpdateHistory oHist = null;
		Iterator oIter = vHist.iterator();
		while (oIter.hasNext()) {
			oHist = (UpdateHistory ) oIter.next();
			if (!oHist.getUpdatePart().equals(_sUpdatePart)) {
				oIter.remove();
			}
		}
		return vHist;
	}	
	
	//item reference set
    public static String[] setUnitId (String _sOldValue, String _sNewValue, Connection _oConn) throws Exception 
    {       
    	Unit oOld = UnitTool.getUnitByID(_sOldValue, _oConn);
    	Unit oNew = UnitTool.getUnitByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getUnitCode();
        if (oNew != null) _sNewValue = oNew.getUnitCode();                 
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;
    }          
    public static String[] setKategoriId (String _sOldValue, String _sNewValue, Connection _oConn) throws Exception
    { 
    	Kategori oOld = KategoriTool.getKategoriByID(_sOldValue, _oConn);
    	Kategori oNew = KategoriTool.getKategoriByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getInternalCode();
        if (oNew != null) _sNewValue = oNew.getInternalCode();     
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;
    }  

    public static String[] setCustomerId (String _sOldValue, String _sNewValue, Connection _oConn) throws Exception
    { 
    	Customer oOld = CustomerTool.getCustomerByID(_sOldValue, _oConn);
    	Customer oNew = CustomerTool.getCustomerByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getCustomerCode();
        if (oNew != null) _sNewValue = oNew.getCustomerCode();          
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;
    } 
    
    public static String[] setVendorId (String _sOldValue, String _sNewValue, Connection _oConn) throws Exception
    { 
    	Vendor oOld = VendorTool.getVendorByID(_sOldValue, _oConn);
    	Vendor oNew = VendorTool.getVendorByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getVendorCode();
        if (oNew != null) _sNewValue = oNew.getVendorCode();          
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;
    } 
    
    public static String[] setTaxId (String _sOldValue, String _sNewValue, Connection _oConn) throws Exception
    { 
    	Tax oOld = TaxTool.getTaxByID(_sOldValue, _oConn);
    	Tax oNew = TaxTool.getTaxByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getTaxCode();
        if (oNew != null) _sNewValue = oNew.getTaxCode();          
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;
    }         
    
    public static String[] setCurrencyId (String _sOldValue, String _sNewValue, Connection _oConn) 
        throws Exception
    { 
        Currency oOld = CurrencyTool.getCurrencyByID(_sOldValue, _oConn);
        Currency oNew = CurrencyTool.getCurrencyByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getCurrencyCode();
        if (oNew != null) _sNewValue = oNew.getCurrencyCode();            
        String[] aValue = {_sOldValue, _sNewValue};
        return aValue;
    }  
    
    public static String[] setLocationId (String _sOldValue, String  _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	//check for CSV locationId in i.e in Discount / PriceList
    	if(StringUtil.containsIgnoreCase(_sOldValue,",") || StringUtil.containsIgnoreCase(_sNewValue,","))
    	{
			String[] sIDs = StringUtil.split(_sOldValue, ",");
			_sOldValue = "";
			for(int i = 0; i < sIDs.length; i++)
			{
				_sOldValue +=  LocationTool.getCodeByID(sIDs[i]);
				if(i != sIDs.length - 1) _sOldValue += ",";
			}
			sIDs = StringUtil.split(_sNewValue, ",");
			_sNewValue = "";
			for(int i = 0; i < sIDs.length; i++)
			{
				_sNewValue +=  LocationTool.getCodeByID(sIDs[i]);
				if(i != sIDs.length - 1) _sNewValue += ",";
			}
    	}
    	else
    	{
	    	Location oOld = LocationTool.getLocationByID(_sOldValue, _oConn);
	    	Location oNew = LocationTool.getLocationByID(_sNewValue, _oConn);
	        if (oOld != null) _sOldValue = oOld.getLocationCode();
	        if (oNew != null) _sNewValue = oNew.getLocationCode();            
    	}
        String[] aValue = {_sOldValue, _sNewValue}; 
        return aValue;
    }  

    public static String[] setEmployeeId (String _sOldValue, String  _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	Employee oOld = EmployeeTool.getEmployeeByID(_sOldValue, _oConn);
    	Employee oNew = EmployeeTool.getEmployeeByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getEmployeeCode();
        if (oNew != null) _sNewValue = oNew.getEmployeeCode();            
        String[] aValue = {_sOldValue, _sNewValue}; 
        return aValue;
    }  
    
    public static String[] setPaymentTypeId (String _sOldValue, String  _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	PaymentType oOld = PaymentTypeTool.getPaymentTypeByID(_sOldValue, _oConn);
    	PaymentType oNew = PaymentTypeTool.getPaymentTypeByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getPaymentTypeCode();
        if (oNew != null) _sNewValue = oNew.getPaymentTypeCode();            
        String[] aValue = {_sOldValue, _sNewValue}; 
        return aValue;
    }  

    public static String[] setPaymentTermId (String _sOldValue, String  _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	PaymentTerm oOld = PaymentTermTool.getPaymentTermByID(_sOldValue, _oConn);
    	PaymentTerm oNew = PaymentTermTool.getPaymentTermByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getPaymentTermCode();
        if (oNew != null) _sNewValue = oNew.getPaymentTermCode();            
        String[] aValue = {_sOldValue, _sNewValue}; 
        return aValue;
    }  

    public static String[] setCustomerTypeId (String _sOldValue, String  _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	CustomerType oOld = CustomerTypeTool.getCustomerTypeByID(_sOldValue, _oConn);
    	CustomerType oNew = CustomerTypeTool.getCustomerTypeByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getCustomerTypeCode();
        if (oNew != null) _sNewValue = oNew.getCustomerTypeCode();            
        String[] aValue = {_sOldValue, _sNewValue}; 
        return aValue;
    }  

    public static String[] setVendorTypeId (String _sOldValue, String  _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	VendorType oOld = VendorTypeTool.getVendorTypeByID(_sOldValue, _oConn);
    	VendorType oNew = VendorTypeTool.getVendorTypeByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getVendorTypeCode();
        if (oNew != null) _sNewValue = oNew.getVendorTypeCode();            
        String[] aValue = {_sOldValue, _sNewValue}; 
        return aValue;
    }      
    
    public static String[] setAccount (String _sOldValue, String  _sNewValue, Connection _oConn) 
    	throws Exception
    { 
    	Account oOld = AccountTool.getAccountByID(_sOldValue, _oConn);
    	Account oNew = AccountTool.getAccountByID(_sNewValue, _oConn);
        if (oOld != null) _sOldValue = oOld.getAccountCode(); else _sOldValue = "";
        if (oNew != null) _sNewValue = oNew.getAccountCode(); else _sNewValue = "";
        String[] aValue = {_sOldValue, _sNewValue}; 
        return aValue;
    }  
            
    public static UpdateHistory getByID (String _sHistoryID, Connection _oConn)
        throws Exception
    {
		Criteria oCrit = new Criteria();
        oCrit.add(UpdateHistoryPeer.HISTORY_ID, _sHistoryID);
     	oCrit.setLimit(1);				
		
		List vData = UpdateHistoryPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0)
		{
			return (UpdateHistory) vData.get(0);
		}
		return null;        
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // sync methods
    ///////////////////////////////////////////////////////////////////////////    
    
    /**
     * 
     * @return List of Updated Item History
     * @throws Exception
     */
    public static List getUpdatedHistory(Date _dLast)
		throws Exception
	{
    	if (_dLast == null)
    	{
    		_dLast = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
    	}
		Criteria oCrit = new Criteria();
		if (_dLast  != null) 
		{
        	oCrit.add(UpdateHistoryPeer.UPDATE_DATE, _dLast, Criteria.GREATER_EQUAL);   
		}
		return UpdateHistoryPeer.doSelect(oCrit);
	}	
}
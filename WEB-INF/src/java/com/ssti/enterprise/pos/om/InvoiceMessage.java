
package com.ssti.enterprise.pos.om;


import org.apache.torque.om.Persistent;

import com.ssti.enterprise.pos.model.MasterOM;
import com.ssti.framework.tools.CustomFormatter;

/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
public  class InvoiceMessage
    extends com.ssti.enterprise.pos.om.BaseInvoiceMessage
    implements Persistent, MasterOM
{

	public String getId()
	{
		return getInvoiceMessageId();
	}

	public String getCode()
	{
		return getTitle();
	}
	
	public String getStartDateStr()
	{
		if (getStartDate() != null)
		{
			return CustomFormatter.formatDate(getStartDate());
		}
		return "";
	}
	
	public String getEndDateStr()
	{
		if (getEndDate() != null)
		{
			return CustomFormatter.formatDate(getEndDate());
		}
		return "";
	}
	
}

package com.ssti.enterprise.pos.excel.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ssti.enterprise.pos.om.Kategori;
import com.ssti.enterprise.pos.tools.KategoriTool;
import com.ssti.framework.excel.helper.BaseExcelHelper;
import com.ssti.framework.excel.helper.ExcelHelper;

public class KategoriViewHelper
	extends BaseExcelHelper 
	implements ExcelHelper 
{    
    public HSSFWorkbook getWorkbook (Map oParam, HttpSession _oSession)	
    	throws Exception
    {
        List vData = KategoriTool.getAllKategoriSortByLevel();
        
        HSSFWorkbook oWB = new HSSFWorkbook();
        HSSFSheet oSheet = oWB.createSheet("Kategori View Worksheet");
        
        HSSFRow oRow = oSheet.createRow(0);
        int iRow = 0;
        
		createHeaderCell(oWB, oRow, (short)iRow, "Kategori Code"); iRow++;
		createHeaderCell(oWB, oRow, (short)iRow, "Description"); iRow++;
		createHeaderCell(oWB, oRow, (short)iRow, "Parent Code"); iRow++;
     
        if (vData != null)
        {
            for (int i = 0; i < vData.size(); i++)
            {
                Kategori oKategori = (Kategori) vData.get(i);
                HSSFRow oRow2 = oSheet.createRow(1 + i); 
                
                iRow = 0;
                createCell(oWB, oRow2, (short)iRow, oKategori.getKategoriCode   ());           iRow++; 
                createCell(oWB, oRow2, (short)iRow, oKategori.getDescription    ());           iRow++; 
                createCell(oWB, oRow2, (short)iRow, KategoriTool.getParentToChildCodeByID(oKategori.getParentId       ()));iRow++;                iRow++;                 
            }        
        }
        return oWB;
    }
}

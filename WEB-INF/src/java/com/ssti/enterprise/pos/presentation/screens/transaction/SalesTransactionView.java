package com.ssti.enterprise.pos.presentation.screens.transaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.framework.turbine.LargeSelectHandler;

public class SalesTransactionView extends TransactionSecureScreen
{
	static Log log = LogFactory.getLog(SalesTransactionView.class);

	protected static final String[] a_PERM = {"View Sales Invoice"};

	static final String s_CTX_VAR = "Invoices";

    protected boolean isAuthorized(RunData data)
        throws Exception
    {
    	return isAuthorized (data, a_PERM);
    }
        
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
			LargeSelectHandler.handleLargeSelectParameter (data, context, "findSalesResult", s_CTX_VAR);    		
    	}
    	catch (Exception _oEx) 
    	{
    		_oEx.printStackTrace();	
    	}
	}
}
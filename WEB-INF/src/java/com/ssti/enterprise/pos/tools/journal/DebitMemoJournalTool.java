package com.ssti.enterprise.pos.tools.journal;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.ApPayment;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.DebitMemo;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.Period;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.PeriodTool;
import com.ssti.enterprise.pos.tools.gl.AccountTool;
import com.ssti.enterprise.pos.tools.gl.GlAttributes;
import com.ssti.enterprise.pos.tools.gl.GlTransactionTool;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: DebitMemoJournalTool.java,v 1.7 2009/05/04 02:04:46 albert Exp $ <br>
 *
 * <pre>
 * $Log: DebitMemoJournalTool.java,v $
 * 
 * 2017-03-20
 * - Fix wrong currency rate when closing DM in other class
 * 
 * </pre><br>
 */
public class DebitMemoJournalTool extends BaseJournalTool
{
	private static final Log log = LogFactory.getLog (DebitMemoJournalTool.class);	
	
	/**
	 * @param _vDebitMemo
	 * @param _oPP
	 * @param conn
	 */
	
	public static void createClosingJournal(List _vDebitMemo, ApPayment _oPP, Connection _oConn) 
		throws Exception
	{
		List vGLTrans = new ArrayList(_vDebitMemo.size());
		Iterator oIter = _vDebitMemo.iterator();
		while (oIter.hasNext())
		{
			DebitMemo oDM = (DebitMemo) oIter.next();
			
			Account oAP = AccountTool.getAccountPayable(oDM.getCurrencyId(), oDM.getVendorId(), _oConn);			
	        GlTransaction oAPTrans = new GlTransaction();
			prepareGLTrans (oAPTrans, oDM, _oPP, _oConn);	
			oAPTrans.setAccountId (oAP.getAccountId());
			oAPTrans.setDebitCredit (GlAttributes.i_DEBIT);

	        Account oDMAcc = AccountTool.getAccountByID(oDM.getAccountId(), _oConn);
	        GlTransaction oDMTrans = new GlTransaction();
	        prepareGLTrans (oDMTrans, oDM, _oPP, _oConn);	
	        oDMTrans.setAccountId (oDMAcc.getAccountId());
		    oDMTrans.setDebitCredit (GlAttributes.i_CREDIT);

		    if (log.isDebugEnabled())
		    {
		        log.debug("AP TRANS : " + oAPTrans);
		        log.debug("DM TRANS : " + oDMTrans);
		    }
	        
		    //DM Journal -> Advance Purchase
	        addJournal (oAPTrans, oAP, vGLTrans);	
	        addJournal (oDMTrans, oDMAcc, vGLTrans);			        
	        
		}//end while
		GlTransactionTool.saveGLTransaction (vGLTrans, _oConn);
	}	

	private static void prepareGLTrans (GlTransaction _oGLTrans, 
										DebitMemo _oDM, 
										ApPayment _oPP,
										Connection _oConn)
		throws Exception
	{
		Period oPeriod = PeriodTool.getPeriodByDate(_oDM.getClosedDate(),_oConn);
		Currency oCurrency = CurrencyTool.getCurrencyByID(_oDM.getCurrencyId(),_oConn);
		
		double dRate = _oDM.getCurrencyRate().doubleValue();
		if (!oCurrency.getIsDefault() && _oDM.getClosingRate() != null && _oDM.getClosingRate().doubleValue() != 1)
		{
			dRate = _oDM.getClosingRate().doubleValue(); //use closing rate 
		}
		double dAmount = _oDM.getAmount().doubleValue();
		double dAmountBase = dAmount * dRate;
		
	    _oGLTrans.setTransactionType  (GlAttributes.i_GL_TRANS_AP_PAYMENT);
	    _oGLTrans.setTransactionId    (_oPP.getApPaymentId());
	    _oGLTrans.setTransactionNo    (_oPP.getApPaymentNo());
	    _oGLTrans.setGlTransactionNo  (_oPP.getApPaymentNo());
	    _oGLTrans.setTransactionDate  (_oPP.getTransactionDate());
	    _oGLTrans.setLocationId       (_oPP.getLocationId());
	    _oGLTrans.setProjectId        ("");
	    _oGLTrans.setDepartmentId     ("");
	    _oGLTrans.setPeriodId         (oPeriod.getPeriodId());
	    _oGLTrans.setCurrencyId       (oCurrency.getCurrencyId());
	    _oGLTrans.setCurrencyRate     (new BigDecimal(dRate));
	
	    _oGLTrans.setAmount           (new BigDecimal(dAmount));
	    _oGLTrans.setAmountBase       (new BigDecimal(dAmountBase));
	    _oGLTrans.setUserName         (_oPP.getUserName());
	    _oGLTrans.setCreateDate       (new Date());
        _oGLTrans.setSubLedgerType	  (i_SUB_LEDGER_AP);
        _oGLTrans.setSubLedgerId	  (_oPP.getVendorId());
	    
        StringBuilder oDesc = new StringBuilder();
        oDesc.append("Memo ").append(_oDM.getMemoNo())
        	 .append(" Closing in Payment ").append(_oPP.getTransactionNo())
        	 .append("\n").append(_oPP.getRemark());
        
        _oGLTrans.setDescription(oDesc.toString());    
	}
}

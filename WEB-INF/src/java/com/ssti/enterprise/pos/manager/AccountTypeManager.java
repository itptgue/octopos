package com.ssti.enterprise.pos.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.AccountType;
import com.ssti.enterprise.pos.om.AccountTypePeer;

public class AccountTypeManager
{
	private static Log log = LogFactory.getLog(AccountTypeManager.class);
	        
    private static AccountTypeManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "account_type";    

    private AccountTypeManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
        	log.error (_oEx);
        }
    }

    public static AccountTypeManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (AccountTypeManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new AccountTypeManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllAccountType ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);        
		Element oElement = oCache.get("AllAccountType");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (AccountTypePeer.ACCOUNT_TYPE);	
			List vData = AccountTypePeer.doSelect(oCrit);
			oCache.put(new Element ("AllAccountType", (Serializable) vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public AccountType getAccountTypeByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AccountType" + _sID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(AccountTypePeer.ACCOUNT_TYPE_ID, _sID);
        	List vData = AccountTypePeer.doSelect(oCrit, _oConn);
			if (vData.size() > 0) {
				oCache.put(new Element ("AccountType" + _sID, (Serializable) vData.get(0))); 
				return (AccountType) vData.get(0);
			}
			return null;
		}
		else {
			return (AccountType) oElement.getValue();
		}
	}

	public AccountType getAccountType(int _iType, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AccountType" + _iType);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(AccountTypePeer.ACCOUNT_TYPE, _iType);
        	List vData = AccountTypePeer.doSelect(oCrit, _oConn);
			if (vData.size() > 0) {
				oCache.put(new Element ("AccountType" + _iType, (Serializable) vData.get(0))); 
				return (AccountType) vData.get(0);
			}
			return null;
		}
		else {
			return (AccountType) oElement.getValue();
		}
	}

	public List getAccountTypeByGLType(int _iGLType, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AccountGLType" + _iGLType);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
        	oCrit.add(AccountTypePeer.GL_TYPE, _iGLType);
        	List vData = AccountTypePeer.doSelect(oCrit, _oConn);
			if (vData.size() > 0) {
				oCache.put(new Element ("AccountGLType" + _iGLType, (Serializable) vData)); 
				return vData;
			}
			return null;
		}
		else {
			return (List) oElement.getValue();
		}
	}

	public AccountType getAccountTypeByName(String _sName)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AccountType" + _sName);
		if (oElement == null)
		{		
		    Criteria oCrit = new Criteria();
            oCrit.add(AccountTypePeer.ACCOUNT_TYPE_NAME, _sName);
		    List vData = AccountTypePeer.doSelect(oCrit);
		    if (vData.size() > 0) 
		    {
		        oCache.put(new Element ("AccountType" + _sName, (Serializable) vData.get(0)));  
		    	return (AccountType)vData.get(0);
		    }
        }
        else 
        {
            return (AccountType) oElement.getValue();
        }
		return null;
	}	
	
	//THIS METHOD MUST BE CALLED AFTER SAVE PROCESS     
	public void refreshCache (AccountType _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);
        if (_oData != null)
        {
            oCache.remove("AllAccountType");
            oCache.remove("AccountType"   + _oData.getAccountTypeId());
            oCache.remove("AccountType"   + _oData.getAccountType());		
            oCache.remove("AccountType"   + _oData.getAccountTypeName());	    	
            oCache.remove("AccountGLType" + _oData.getGlType());	
        }
        else
        {
            oCache.removeAll();
        }
    }   
}
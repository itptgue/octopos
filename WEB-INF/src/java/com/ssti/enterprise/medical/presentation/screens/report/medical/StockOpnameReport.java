package com.ssti.enterprise.medical.presentation.screens.report.medical;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.report.InventoryReportTool;

public class StockOpnameReport extends Default
{
	protected static final Log log = LogFactory.getLog(StockOpnameReport.class);
	
    public void doBuildTemplate(RunData data, Context context)
    {
    	try 
    	{
    		super.doBuildTemplate(data, context);
	    	String sKeywords = data.getParameters().getString("Keywords");
	    	String sColor = data.getParameters().getString("Color");	    	
	    	int iSearchBy = data.getParameters().getInt("Condition");
	    	int iGroupBy = data.getParameters().getInt("GroupBy");
	    	int iSortBy = data.getParameters().getInt("SortBy");
	    	int iType = data.getParameters().getInt("TransType");
	    	String sKategoriID= data.getParameters().getString("KategoriId");
	    	String sAdjType = data.getParameters().getString("AdjustmentTypeId");
	        
	    	int iOp = data.getParameters().getInt("op");
	    	
	    	if (iOp == 1)
	    	{
	    		context.put ("vData", 
	    			InventoryReportTool.findIRItem (dStart, dEnd, iSearchBy, iSortBy, iType, 
	    				sKeywords, sColor, sKategoriID, sLocationID, sAdjType));
	    	}
	    	else if (iOp == 2)
	    	{
	    		List vNIR = InventoryReportTool.findItemNotInIR(
	    			dStart, dEnd, iSearchBy, iSortBy, iType, sKeywords, sColor, sKategoriID);
	    		List vInactive = InventoryReportTool.getInactiveItem(dStart, sKategoriID, iSortBy);
	    		List vFiltered = InventoryReportTool.filterItemNotInIRExclInactive(vNIR, vInactive);
	    		context.put ("vData", vFiltered);
	    	}
	    	else if (iOp == 3)
	    	{
	    		context.put ("vData", InventoryReportTool.getInactiveItem(dStart, sKategoriID, iSortBy));
	    	}
	    	else if (iOp == 4)
	    	{
	    		context.put ("dTotalItem", InventoryReportTool.countItem(i_INVENTORY_PART)); 
	    		context.put ("dItemNotInIR", InventoryReportTool.countItemNotInIR(dStart, dEnd)); 	    		
	    		context.put ("dNonActive", InventoryReportTool.countInactiveItem(dStart)); 	    		
	    	}
	    }   
    	catch (Exception _oEx) 
    	{
    		data.setMessage("Data LookUp Failed !");
    		_oEx.printStackTrace();
    	}    
    }    
    

}
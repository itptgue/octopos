package com.ssti.enterprise.pos.tools;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.services.localization.Localization;

import com.ssti.enterprise.pos.manager.CommonManager;
import com.ssti.enterprise.pos.om.CustomerPoint;
import com.ssti.enterprise.pos.om.CustomerPointPeer;
import com.ssti.enterprise.pos.om.GlTransactionPeer;
import com.ssti.enterprise.pos.om.InvoicePayment;
import com.ssti.enterprise.pos.om.Item;
import com.ssti.enterprise.pos.om.PointReward;
import com.ssti.enterprise.pos.om.PointRewardPeer;
import com.ssti.enterprise.pos.om.PointTransaction;
import com.ssti.enterprise.pos.om.PointTransactionPeer;
import com.ssti.enterprise.pos.om.PointType;
import com.ssti.enterprise.pos.om.PointTypePeer;
import com.ssti.enterprise.pos.om.SalesReturn;
import com.ssti.enterprise.pos.om.SalesReturnDetail;
import com.ssti.enterprise.pos.om.SalesTransaction;
import com.ssti.enterprise.pos.om.SalesTransactionDetail;
import com.ssti.framework.tools.BeanUtil;
import com.ssti.framework.tools.Calculator;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Purpose: Handle PointReward OM Class
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: PointRewardTool.java,v 1.13 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: PointRewardTool.java,v $
 * 
 * 2016-03-10
 * - change PointReward point field into BigDecimal
 * - change PoinTransaction pointBefore, pointChanges to BigDecimal
 * - change CustomerPoint totalPoint to BigDecimal
 * - change almost all method because of point changes from int to double
 * 
 * </pre><br>
 */
public class PointRewardTool extends BaseTool
{
	private static final Log log = LogFactory.getLog(PointRewardTool.class);
	
	public static final int i_ADDITION   = 1;
	public static final int i_WITHDRAWAL = 2;

	private static final String s_POINT_LESSTHAN_ZERO = "point_lessthan_zero";
	
//	private static final boolean b_INSTALLED = CONFIG.getBoolean("pointreward.installed");	
//	private static final boolean b_ALLITEM   = CONFIG.getBoolean("pointreward.allitem");	
	private static final boolean b_MINUS = CONFIG.getBoolean("pointreward.allowminus");	
//		
//	private static final String s_TERM_CLASS = "pointreward.term.class"; 		
//	private static final String s_TERM_FIELD = "pointreward.term.field"; 		
//	private static final String s_TERM_VALUE = "pointreward.term.value"; 		
	
//	pointreward_use"   
//	pointreward_byitem"
//	pointreward_field" 
//	pointreward_pvalue"
	
	static PointRewardTool instance = null;
	public static synchronized PointRewardTool getInstance() 
	{
		if (instance == null) instance = new PointRewardTool();
		return instance;
	}	
	
	/**
	 * is Point Reward Module Installed
	 * @return
	 */
	public static boolean isInstalled()
	{
		try
		{
			return Boolean.valueOf(PreferenceTool.getSysConfig().getPointrewardUse());
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}
		return false;
	}
	
	/**
	 * is Point Reward apply to all item
	 * 
	 * @return
	 */
	static boolean perItem()
	{
		try
		{
			return Boolean.valueOf(PreferenceTool.getSysConfig().getPointrewardByitem());
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}
		return false;
	}

	/**
	 * field to get number of point for an item
	 * 
	 * @return
	 */
	static String itemField()
	{
		try
		{
			if(perItem())
			{
				return PreferenceTool.getSysConfig().getPointrewardField();
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}
		return "";
	}

	/**
	 * value of 1 point if we want to use the point reward as discount
	 * 
	 * @return
	 */
	static double pointValue()
	{
		try
		{
			if(perItem())
			{
				return Double.valueOf(PreferenceTool.getSysConfig().getPointrewardField());
			}
		}
		catch (Exception _oEx)
		{
			_oEx.printStackTrace();
			log.error(_oEx);
		}
		return 1;
	}
			
	public static List getAllPointReward()
		throws Exception
	{
		Criteria oCrit = new Criteria();
		return PointRewardPeer.doSelect(oCrit);
	}
	
	public static PointReward getPointRewardByID(String _sID)
    	throws Exception
    {
		return getPointRewardByID(_sID, null);
	}

	public static PointReward getPointRewardByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PointRewardPeer.POINT_REWARD_ID, _sID);
		List vData = PointRewardPeer.doSelect(oCrit, _oConn);
		if (vData.size() > 0) 
		{
			return (PointReward) vData.get(0);
		}
		return null;
	}
	
	/**
	 * 
	 * @param _dSalesAmount
	 * @param _oConn
	 * @return
	 * @throws Exception
	 */
	public static PointReward getByAmount(double _dSalesAmount, Connection _oConn)
		throws Exception
	{
		PointReward oPR = null;
		Criteria oCrit = new Criteria();
        oCrit.add(PointRewardPeer.FROM_AMOUNT, _dSalesAmount, Criteria.LESS_EQUAL);
        oCrit.add(PointRewardPeer.TO_AMOUNT, _dSalesAmount, Criteria.GREATER_EQUAL);
        List vData = PointRewardPeer.doSelect(oCrit, _oConn);        
        if (vData.size() > 0)
        {
        	oPR = (PointReward) vData.get(0);
        }
        return oPR;
	}
	
	/**
	 * get Total Point, by sales amount
	 * 1. get PointReward where From >= _dSalesAmount and To <= _dSalesAmount
	 * 2. multiply PointReward.getPoint * (_dSalesAmount / PointReward.Multiply)
	 * 
	 * @param _dSalesAmount
	 * @return total point
	 * @throws Exception
	 */
	public static double getTotalPoint(double _dSalesAmount, Connection _oConn)
    	throws Exception
    {
        PointReward oPR = getByAmount(_dSalesAmount, _oConn);        
        return getTotalPoint(oPR, _dSalesAmount, _oConn);
	}
	
	public static double getTotalPoint(PointReward oPR, double _dSalesAmount, Connection _oConn)
    	throws Exception
    {     
		if(oPR != null)
		{
        	log.debug("Point Reward : " + oPR);
        	double dMultiply = oPR.getMultiplyAmount().doubleValue();
        	if (dMultiply > 0)
        	{
        		//int iPointAmount = (int)(_dSalesAmount * (oPR.getPoint().doubleValue() / dMultiply));
        		int iPointAmount = (int) (Calculator.toInt((_dSalesAmount/ dMultiply)) * oPR.getPoint().doubleValue());
        		log.debug("Point Amount : " + iPointAmount);
            	return iPointAmount;
        	}
		}
        return 0;
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// POINT REWARD TRANSACTION
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * @param _sTransID
	 * @return point transaction
	 * @throws Exception
	 */
	public static PointTransaction getPointRewardTransByID(String _sID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PointTransactionPeer.POINT_TRANSACTION_ID, _sID);
	    List vData = PointTransactionPeer.doSelect(oCrit, _oConn);
	    if (vData.size() > 0)
	    {
	    	return (PointTransaction) vData.get(0);
	    }
	    return null;
	}
	
	/**
	 * find point transaction
	 * 
	 * @param _sCustomerID
	 * @param _sTransNo
	 * @param _sLocationID
	 * @param _dFrom
	 * @param _dTo
	 * @return
	 * @throws Exception
	 */
	public static List findTransaction (String _sCustomerID, 
										String _sTransNo, 
										String _sLocationID,
										String _sTypeID,
										Date _dFrom,
										Date _dTo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sCustomerID)) 
		{
			oCrit.add(PointTransactionPeer.CUSTOMER_ID, _sCustomerID);
		}
		if (StringUtil.isNotEmpty(_sTransNo)) 
		{
			oCrit.add(PointTransactionPeer.TRANSACTION_NO, (Object)_sTransNo, Criteria.ILIKE);
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oCrit.add(PointTransactionPeer.LOCATION_ID, _sLocationID);
		}
		if (StringUtil.isNotEmpty(_sTypeID))
		{
			oCrit.add(PointTransactionPeer.POINT_TYPE_ID, _sTypeID);
		}
		if (_dFrom != null) 
		{
			oCrit.add(PointTransactionPeer.TRANSACTION_DATE, DateUtil.getStartOfDayDate(_dFrom), Criteria.GREATER_EQUAL);
		}
		if (_dTo != null) 
		{
			oCrit.and(PointTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayIncSec(_dTo), Criteria.LESS_EQUAL);
		}
		return PointTransactionPeer.doSelect(oCrit);
	}
	
	public static List getPointRewardTransByTransID(String _sTransID)
		throws Exception
	{
		return getPointRewardTransByTransID(_sTransID, null);
	}
	
	/**
	 * @param _sTransID
	 * @return point transaction
	 * @throws Exception
	 */
	public static List getPointRewardTransByTransID(String _sTransID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(PointTransactionPeer.TRANSACTION_ID, _sTransID);
	    return PointTransactionPeer.doSelect(oCrit, _oConn);	    
	}
	
	/**
	 * create point reward from sales
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createPointRewardFromSales (SalesTransaction _oTR, List _vTD, List _vPMT, Connection _oConn)
		throws Exception
	{
		if (isInstalled() && _oTR.getCustomer() != null && !_oTR.getCustomer().getIsDefault()) //only for non-default customer
		{
			validate(_oConn);
			StringBuilder oDesc = new StringBuilder(); 
			oDesc.append("Point From Sales : ").append(_oTR.getInvoiceNo());
			
			double iTransPoint = 0;			
			if(perItem()) //calculate point from item
			{
				oDesc.append(" By Item ");
				for (int i = 0; i < _vTD.size(); i++)
				{
					SalesTransactionDetail oTD = (SalesTransactionDetail) _vTD.get(i);
					Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);					
					double iItemPoint = getPoint(oItem) * oTD.getQty().intValue(); 
					iTransPoint += iItemPoint;
					oDesc.append("\nItem Code : ").append(oItem.getItemCode())
						 .append("\t Qty : ").append(oTD.getQty().intValue())
						 .append("\t Point : ").append(iItemPoint);					
				}
			}
			else //calculate from amount
			{
				oDesc.append(" By Amount ");
				double dAmountForPoint = 0;
				if(perItem())
				{
					for (int i = 0; i < _vTD.size(); i++)
					{
						SalesTransactionDetail oTD = (SalesTransactionDetail) _vTD.get(i);
						Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
						if (availableForPoint(oItem))
						{
							dAmountForPoint += oTD.getSubTotal().doubleValue();
							oDesc.append("\nItem Code : ").append(oItem.getItemCode())
								 .append("\t SubTotal : ").append(CustomFormatter.formatNumber(oTD.getSubTotal()));
						}
					}
				}
				else
				{
					dAmountForPoint = _oTR.getTotalAmount().doubleValue();
					oDesc.append("\nSalesAmount : ").append(dAmountForPoint);
					//reduce point from payment with point reward if exists
					for (int i = 0; i < _vPMT.size(); i++)
					{
				    	InvoicePayment oPmt = (InvoicePayment) _vPMT.get(i);
				    	if(oPmt.getIsPointReward())
				    	{
				    		dAmountForPoint = dAmountForPoint - oPmt.getPaymentAmount().doubleValue();							
				    		oDesc.append("\nUsedPoint : ").append(oPmt.getPaymentAmount().doubleValue());			
				    	}
				    	if(oPmt.getIsCoupon())
				    	{
				    		dAmountForPoint = dAmountForPoint - oPmt.getPaymentAmount().doubleValue();							
				    		oDesc.append("\nCoupon : ").append(oPmt.getPaymentAmount().doubleValue());			
				    	}
				    	if(oPmt.getIsVoucher())
				    	{
				    		dAmountForPoint = dAmountForPoint - oPmt.getPaymentAmount().doubleValue();							
				    		oDesc.append("\nVoucher : ").append(oPmt.getPaymentAmount().doubleValue());			
				    	}				    	
					}
				}				
				iTransPoint = getTotalPoint(dAmountForPoint, _oConn);
				oDesc.append("\nTotalAmount : ").append(dAmountForPoint);
				oDesc.append("\nTotalPoint : ").append(iTransPoint);			
			}						
			
			if (iTransPoint > 0)
			{							
				PointTransaction oPoint = new PointTransaction ();
				oPoint.setTransactionType(i_ADDITION);
				oPoint.setPointTypeId    (getSITypeID());
				oPoint.setCustomerId	 (_oTR.getCustomerId());
				oPoint.setTransactionId	 (_oTR.getSalesTransactionId());
				oPoint.setTransactionNo	 (_oTR.getInvoiceNo());
				oPoint.setTransactionDate(_oTR.getTransactionDate());
				oPoint.setLocationId	 (_oTR.getLocationId());
				oPoint.setPointChanges	 (new BigDecimal(iTransPoint));
				oPoint.setDescription	 (oDesc.toString());
				oPoint.setUserName		 (_oTR.getCashierName());
				oPoint.setCreateDate	 (new Date());
				oPoint.setPointBefore	 (bd_ZERO); //just set default value, point before if exists will be set at save
				saveData 				 (oPoint, _oConn);
			}
		}
	} 

	/**
	 * create point reward from sales
	 * 
	 * @param _oTR
	 * @param _vTD
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createPointRewardFromReturn (SalesReturn _oTR, List _vTD, Connection _oConn)
		throws Exception
	{
		if (isInstalled() && _oTR.getCustomer() != null && !_oTR.getCustomer().getIsDefault()) //only for non-default customer
		{
			validate(_oConn);
			StringBuilder oDesc = new StringBuilder(); 
			oDesc.append("Point Reduced From Sales Return : ").append(_oTR.getReturnNo());
			
			double dReturnPoint = 0; //point from SR	
			double dSalesPoint = 0;  //point from SI
			if(perItem()) //calculate point from item
			{
				oDesc.append(" By Item ");
				for (int i = 0; i < _vTD.size(); i++)
				{
					SalesReturnDetail oTD = (SalesReturnDetail) _vTD.get(i);
					Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);					
					double iItemPoint = getPoint(oItem) * oTD.getQty().intValue(); 
					dReturnPoint += iItemPoint;
					oDesc.append("\nItem Code : ").append(oItem.getItemCode())
						 .append("\t Qty : ").append(oTD.getQty().intValue())
						 .append("\t Point : ").append(iItemPoint);					
				}
			}
			else //calculate from amount
			{
				oDesc.append(" By Amount ");
				double dAmountForPoint = 0;
				if(perItem())
				{
					for (int i = 0; i < _vTD.size(); i++)
					{
						SalesReturnDetail oTD = (SalesReturnDetail) _vTD.get(i);
						Item oItem = ItemTool.getItemByID(oTD.getItemId(), _oConn);
						if (availableForPoint(oItem))
						{
							dAmountForPoint += oTD.getReturnAmount().doubleValue();
							oDesc.append("\nItem Code : ").append(oItem.getItemCode())
								 .append("\t SubTotal : ").append(CustomFormatter.formatNumber(oTD.getReturnAmount()));
						}
					}
				}
				else
				{
					dAmountForPoint = _oTR.getReturnedAmount().doubleValue();
					oDesc.append("\nReturnAmount : ").append(dAmountForPoint);					
					if(_oTR.getTransactionType() == i_RET_FROM_PI_SI && StringUtil.isNotEmpty(_oTR.getTransactionId()))
					{
						dSalesPoint = getTransPoint(_oTR.getTransactionId(), i_ADDITION, _oConn);
					}										
				}				
				dReturnPoint = getTotalPoint(dAmountForPoint, _oConn);
				if (dReturnPoint >  dSalesPoint) dReturnPoint = dSalesPoint; //prevent return point more than sales point
				
				oDesc.append("\nReturnAmount : ").append(dAmountForPoint);
				oDesc.append("\nReturnPoint : ").append(dReturnPoint);			
			}						
			
			CustomerPoint oBalance = getCustomerPoint (_oTR.getCustomerId(), _oConn);			
			if (dReturnPoint > 0 && oBalance != null && oBalance.getTotalPoint().intValue() >= dReturnPoint)
			{							
				PointTransaction oPoint = new PointTransaction ();
				oPoint.setTransactionType(i_WITHDRAWAL);
				oPoint.setPointTypeId    (getSRTypeID());
				oPoint.setCustomerId	 (_oTR.getCustomerId());
				oPoint.setTransactionId	 (_oTR.getSalesReturnId());
				oPoint.setTransactionNo	 (_oTR.getReturnNo());
				oPoint.setTransactionDate(_oTR.getReturnDate());
				oPoint.setLocationId	 (_oTR.getLocationId());
				oPoint.setPointChanges	 (new BigDecimal(dReturnPoint));
				oPoint.setDescription	 (oDesc.toString());
				oPoint.setUserName		 (_oTR.getCreateBy());
				oPoint.setCreateDate	 (new Date());
				oPoint.setPointBefore	 (bd_ZERO); //just set default value, point before if exists will be set at save
				saveData 				 (oPoint, _oConn);
			}
		}
	} 

	
	private static double getPoint(Item _oItem) 
		throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, Exception 
	{
		if (perItem() && StringUtil.isNotEmpty(itemField())) 
		{					
			Object oValue = null;
			if(BeanUtil.hasField(_oItem, itemField())) //if field in Item Master
			{
				log.debug("FromItem");
				oValue = PropertyUtils.getProperty(_oItem, itemField());		
			}
			else //get field from ItemField
			{
				log.debug("FromItemField");
				oValue = ItemFieldTool.getValue(_oItem.getItemId(), itemField());
			}
			log.debug("Field " + itemField() + " Value " + oValue);
			if (oValue != null)
			{
				try
				{
					return Double.parseDouble(oValue.toString());
				}
				catch (Exception e)
				{
					e.printStackTrace();
					log.error(e);					
				}
			}
		}
		return 0;
	}
	
	/**
	 * check whether item applicable for point
	 * 
	 * @param _oItem
	 * @return
	 */
	private static boolean availableForPoint(Item _oItem) 
	{
		if (!perItem()) return true;
		return false;
	}
	
	/**
	 * create withdrawal point transaction from payment
	 * 
	 * @param _oTR
	 * @param _oPmt
	 * @param _oConn
	 * @throws Exception
	 */
	public static void createPointTransFromPayment (SalesTransaction _oTR, InvoicePayment _oPmt, Connection _oConn)
		throws Exception
	{
		if(_oTR != null && _oPmt != null && _oPmt.getIsPointReward() && _oPmt.getPaymentAmount().intValue() != 0)
		{			
			PointTransaction oPoint = new PointTransaction ();
			oPoint.setTransactionType(i_WITHDRAWAL);
			oPoint.setCustomerId	 (_oTR.getCustomerId());
			oPoint.setTransactionId	 (_oTR.getSalesTransactionId());
			oPoint.setTransactionNo	 (_oTR.getInvoiceNo());
			oPoint.setTransactionDate(_oTR.getTransactionDate());
			oPoint.setLocationId	 (_oTR.getLocationId());
			oPoint.setPointBefore	 (bd_ZERO); //will be set by saveData
			oPoint.setPointChanges	 (new BigDecimal(_oPmt.getPaymentAmount().intValue()));
			oPoint.setDescription	 ("Point Withdrawal For Payment, Invoice No: " + _oTR.getInvoiceNo());
			oPoint.setUserName		 (_oTR.getCashierName());
			oPoint.setCreateDate	 (new Date());
			saveData 				 (oPoint, _oConn);
		}
	}
	
	/**
	 * 
	 * @param _oPoint
	 * @param _oConn
	 * @throws Exception
	 */
	public static void saveData(PointTransaction _oPoint, Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		if (_oConn == null)
		{
			_oConn = beginTrans();
			bStartTrans = true;
		}
		try
		{
			//get current balance
			CustomerPoint oBalance = getCustomerPoint (_oPoint.getCustomerId(), _oConn);
			if(oBalance != null)
			{
				_oPoint.setPointBefore(oBalance.getTotalPoint());
			}
			
			_oPoint.setPointTransactionId(IDGenerator.generateSysID());
			if (StringUtil.isEmpty(_oPoint.getPointTransactionId()))
			{
				_oPoint.setTransactionId(_oPoint.getPointTransactionId());
			}
			if (StringUtil.isEmpty(_oPoint.getTransactionNo()))
			{
				_oPoint.setTransactionNo(
					LastNumberTool.get(AppAttributes.s_PW_FORMAT, LastNumberTool.i_POINT_REWARD, _oConn));
			}
			_oPoint.save(_oConn);
			
			//update customer total point
			updateCustomerPoint (_oPoint, false,  _oConn);
			
			//commit
			if (bStartTrans) commit(_oConn);
		}
		catch (Exception _oEx)
		{
			if (bStartTrans) rollback(_oConn);
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new Exception (_oEx);
		}
	}

	public static void deleteTrans(String _sTransID)
		throws Exception
	{
		deleteTrans(_sTransID, null);
	}
	
	/**
	 * delete point transaction
	 * 
	 * @param _sTransID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void deleteTrans(String _sTransID, Connection _oConn)
		throws Exception
	{
		boolean bStartTrans = false;
		if(_oConn == null) 
		{
			bStartTrans = true;
			_oConn = beginTrans();
		}
		try
		{
			List vPoint  = getPointRewardTransByTransID(_sTransID, _oConn);
			for (int i = 0; i < vPoint.size(); i++)
			{
				PointTransaction oPoint = (PointTransaction) vPoint.get(i);
				//update customer total point
				updateCustomerPoint (oPoint, true,  _oConn);				
			}
			
			Criteria oCrit = new Criteria();
			oCrit.add (PointTransactionPeer.TRANSACTION_ID, _sTransID);
			PointTransactionPeer.doDelete( oCrit, _oConn);
			
			if (bStartTrans)
			{
				//commit
				commit(_oConn);
			}
		}
		catch (Exception _oEx)
		{
			if (bStartTrans)
			{
				rollback(_oConn);
			}
			_oEx.printStackTrace();
			log.error(_oEx);
			throw new Exception (_oEx);
		}
	}
	
	public static double getTotalChanges (String _sCustomerID, Date _dFrom) 
		throws Exception
	{         
		//count total add
		StringBuilder oSB = new StringBuilder();
		oSB.append (" SELECT SUM(point_changes) ");
		oSB.append (" FROM point_transaction WHERE customer_id = '" );
	    oSB.append (_sCustomerID);
		oSB.append ("' AND transaction_type = 1 ");		
		oSB.append (" AND transaction_date >= '");
		oSB.append (CustomFormatter.formatCustomDate(_dFrom, "yyyy-MM-dd 00:00"));
		oSB.append ("'");        
	    log.debug (oSB.toString());
	    
	    List vTotalAdd = PointTransactionPeer.executeQuery(oSB.toString());
	    double iTotalAdd = 0;
	    if (vTotalAdd.size() > 0)
	    {
	        Record oData = (Record)vTotalAdd.get(0);
	        iTotalAdd =  oData.getValue(1).asDouble();            
	    }
	
	    //count total withdraw
		oSB = new StringBuilder();
		oSB.append (" SELECT SUM(point_changes) ");
		oSB.append (" FROM point_transaction WHERE customer_id = '" );
	    oSB.append (_sCustomerID);
		oSB.append ("' AND transaction_type = 2 ");		
		oSB.append (" AND transaction_date >= '");
		oSB.append (CustomFormatter.formatCustomDate(_dFrom, "yyyy-MM-dd 00:00"));
		oSB.append ("'");        
	    log.debug (oSB.toString());
	
	    List vTotalWithdraw = GlTransactionPeer.executeQuery(oSB.toString());
	    double iTotalWithdraw = 0;
	    if (vTotalWithdraw.size() > 0)
	    {
	        Record oData = (Record) vTotalWithdraw.get(0);
	        iTotalWithdraw =  oData.getValue(1).asDouble();            
	    }
	    
	    double iTotal = 0;
	    iTotal = iTotal - iTotalAdd + iTotalWithdraw;
	    log.debug ("Total Changes : " + iTotal );
	    return iTotal;
	}	 
	
	//next prev button util
	public static String getPrevOrNextID(String _sID, boolean _bIsNext)
		throws Exception
	{
		return getPrevOrNextID(PointTransactionPeer.class, PointTransactionPeer.POINT_TRANSACTION_ID, _sID, _bIsNext);
	}
	///////////////////////////////////////////////////////////////////////////
	// CUSTOMER TOTAL POINT
	///////////////////////////////////////////////////////////////////////////

	public static CustomerPoint getCustomerPoint (String _sCustomerID)
		throws Exception
	{
		return getCustomerPoint (_sCustomerID, null);
	}
	
	public static CustomerPoint getCustomerPoint (String _sCustomerID, Connection _oConn)
		throws Exception
	{
		return getCustomerPoint(_sCustomerID, false, _oConn);
	}

	public static CustomerPoint getCustomerPoint (String _sCustomerID, boolean _bForUpdate, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
	    oCrit.add(CustomerPointPeer.CUSTOMER_ID, _sCustomerID);
	    if(_bForUpdate) { oCrit.setForUpdate(true); }
	    List vData = null;
	    if (_oConn == null)
	    {
	    	vData = CustomerPointPeer.doSelect(oCrit);
		}
	    else
	    {
	    	vData = CustomerPointPeer.doSelect(oCrit, _oConn);	    
	    }	
	    if (vData != null && vData.size() > 0)
	    {
	    	return (CustomerPoint) vData.get(0);
	    }
	    return null;		
	}
	
	public static double getPointAsOf (String _sCustomerID, Date _dAsOf)
		throws Exception
	{
		CustomerPoint oPoint = getCustomerPoint (_sCustomerID);
		if (oPoint != null && oPoint.getTotalPoint() != null)
		{
			double dCurrentPoint = oPoint.getTotalPoint().doubleValue();
			double iTotalChanges = getTotalChanges (_sCustomerID, _dAsOf); 
			return dCurrentPoint + iTotalChanges; //(Current - TotalAdd + TotalWithdraw)
		}
		return 0;
	}
		
	/**
	 * Update Customer Point Balance 
	 * 
	 * @param _oPoint
	 * @param _bIsReverse
	 * @param _oConn
	 * @throws Exception
	 */
	private static void updateCustomerPoint(PointTransaction _oPoint, boolean _bIsReverse, Connection _oConn)
		throws Exception
	{
		String sCustomerID = _oPoint.getCustomerId();
		CustomerPoint oBalance = getCustomerPoint (sCustomerID, true, _oConn);
		
		if (oBalance == null)
		{
			oBalance = new CustomerPoint();
			oBalance.setCustomerPointId(sCustomerID);
			oBalance.setCustomerId(sCustomerID);
			oBalance.setTotalPoint(bd_ZERO);
		}
		if (!_bIsReverse) //if normal update to customer point
		{
			if (_oPoint.getTransactionType() == i_ADDITION) 
			{
				oBalance.setTotalPoint (new BigDecimal(Calculator.add(oBalance.getTotalPoint(), _oPoint.getPointChanges())));	
			}
			else 
			{
				oBalance.setTotalPoint (new BigDecimal(Calculator.sub(oBalance.getTotalPoint(), _oPoint.getPointChanges())));
			}
		}
		else //if reverse update e.g from sales cancel or point cancel
		{
			if (_oPoint.getTransactionType() == i_ADDITION) 
			{
				oBalance.setTotalPoint (new BigDecimal(Calculator.sub(oBalance.getTotalPoint(), _oPoint.getPointChanges())));	
			}
			else 
			{
				oBalance.setTotalPoint (new BigDecimal(Calculator.add(oBalance.getTotalPoint(), _oPoint.getPointChanges())));
			}		
		}
		if (!b_MINUS && oBalance.getTotalPoint().intValue() < 0) 
		{
			throw new Exception (Localization.getString(Localization.getDefaultBundleName(), 
								 PreferenceTool.getLocale(), s_POINT_LESSTHAN_ZERO));	
		}
		else
		{
			if(_oPoint.getCustomer() != null && oBalance.getTotalPoint().intValue() < 0)
			{
				log.warn(" Customer Point " + _oPoint.getCustomer().getCustomerName() + " is minus from trans " + _oPoint.getTransactionNo());
			}
		}
		oBalance.setLastUpdate (new Date());
		oBalance.save(_oConn);
	}
	
	//-------------------------------------------------------------------------
	//calculate methods
	//------------------------------------------------------------------------- 
	public BigDecimal getTransPointBefore(String _sTransID)
    	throws Exception
	{    		
    	if(PreferenceTool.prwInstalled())
    	{    		
    		List vPT = PointRewardTool.getPointRewardTransByTransID(_sTransID,null);    			
    		for(int i = 0; i < vPT.size(); i++)
    		{
    			PointTransaction oPT = (PointTransaction) vPT.get(i);
    			if(vPT.size() == 1 || (vPT.size() > 1 && oPT.getTransactionType() == i_WITHDRAWAL)) //point before in payment point used
    			{
    				return oPT.getPointBefore();
    			}    			
    		}
    	}
    	return bd_ZERO;
    }
	
	public static double getTransPoint(String _sTransID, int _iType, Connection _oConn)
		throws Exception
    {    
    	double iTransPoint = 0;
    	if(PreferenceTool.prwInstalled())
    	{    		
    		List vPT = PointRewardTool.getPointRewardTransByTransID(_sTransID,_oConn);    			
    		for(int i = 0; i < vPT.size(); i++)
    		{
    			PointTransaction oPT = (PointTransaction) vPT.get(i);
    			if (oPT != null && (oPT.getTransactionType() == _iType || _iType <= 0)) 
    			{
    				iTransPoint += oPT.getPointChanges().doubleValue(); 				
    			}
    		}
    	}
    	return iTransPoint;
    }
	
	//-------------------------------------------------------------------------
	//synchronization methods
	//------------------------------------------------------------------------- 
		
	/**
	 * get List of PurchaseReceipt created since last store 2 ho
	 * 
	 * @return List of PurchaseReceipt created since last store 2 ho
	 * @throws Exception
	 */
	public static List getCustomerPointForStore (boolean _bRemoteStore)
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_HO_TO_STORE); //one way
		Criteria oCrit = new Criteria();
		
		if (dLastSyncDate  != null && !_bRemoteStore) 
		{
        	oCrit.add(CustomerPointPeer.LAST_UPDATE, dLastSyncDate, Criteria.GREATER_EQUAL);   
		}
		return CustomerPointPeer.doSelect(oCrit);
	}

	/**
	 * get store point trans which is entried in Store (exchange or open addition)
	 * not from SI
	 * 
	 * @return Point Trans after last sync
	 * @throws Exception
	 */
	public static List getStorePointTrans ()
		throws Exception
	{
		Date dLastSyncDate = SynchronizationTool.getLastSyncDateByType(i_STORE_TO_HO); //one way
		Criteria oCrit = new Criteria();		
		if (dLastSyncDate  != null) 
		{
	    	oCrit.add(PointTransactionPeer.CREATE_DATE, dLastSyncDate, Criteria.GREATER_EQUAL);
	    	//get only point trans not created from SI
	    	oCrit.add(PointTransactionPeer.TRANSACTION_ID, "");
	    	//oCrit.add(PointTransactionPeer.TRANSACTION_TYPE, i_WITHDRAWAL);
		}
		return PointTransactionPeer.doSelect(oCrit);
	}

	
	/**
	 * while loading customer point we need to delete existing customer point in store
	 * because it might have different PK with customer_point in HO
	 * 
	 * @param _sCustomerID
	 * @param _oConn
	 * @throws Exception
	 */
	public static void deleteExistingCustomerPoint (String _sCustomerID, Connection _oConn)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		oCrit.add(CustomerPointPeer.CUSTOMER_ID, _sCustomerID);
		CustomerPointPeer.doDelete(oCrit, _oConn);
	}
	//-------------------------------------------------------------------------
	// point type
	//-------------------------------------------------------------------------
	
	public static List getAllPointType()
		throws Exception
	{
		return CommonManager.getInstance().getAllPointType();				
	}

	public static PointType getPointType(String _sID)
		throws Exception
	{
		return CommonManager.getInstance().getPointType(_sID);
	}

	public static String getSITypeID()
		throws Exception
	{
		return CommonManager.getInstance().getSITypeID();	
	}

	public static String getSRTypeID()
		throws Exception
	{
		return CommonManager.getInstance().getSRTypeID();
	}

	public static PointType getPointTypeByCode(String sCode) 
		throws Exception
	{
		Criteria oCrit = new Criteria();		
		oCrit.add(PointTypePeer.CODE, sCode);
		List v = PointTypePeer.doSelect(oCrit);
		if(v.size() > 0)
		{
			return (PointType) v.get(0);								
		}
		return null;
	}
}

package com.ssti.enterprise.pos.tools;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.BasePeer;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Vendor;
import com.ssti.framework.tools.SqlUtil;
import com.ssti.framework.tools.StringUtil;
import com.workingdogs.village.Record;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: LastNumberTool.java,v 1.26 2009/05/04 02:03:51 albert Exp $ <br>
 *
 * <pre>
 * $Log: LastNumberTool.java,v $
 * 
 * 2018-03-13
 * -Add i_PROC_REQUEST, i_REIMBURSE_RQ. 
 * -Add ctl_proc_request, ctl_rmbs_request CTL_TABLES
 * 
 * 2017-09-07
 * -Add  day of month (dd) parameters for trans no
 * 
 * 2015-09-01
 * -Add new tables constants CF/CASH IN OUT, TAX SO DO 
 * </pre><br> 
 */
public class LastNumberTool extends BaseTool
{
	private static final Log log = LogFactory.getLog (LastNumberTool.class);

    static final String s_SLASH = "/";
    static final String s_STRIP = "-";
    static final String s_UNDER = "_";
    static final String s_DOT = ".";

    static final String s_FORMAT_DD = "dd";
    
	public static final int i_SALES_ORDER  	     = 1;
    public static final int i_DELIVERY_ORDER  	 = 2;  
    public static final int i_SALES_INVOICE  	 = 3;  
    public static final int i_SALES_RETURN  	 = 4;  
    public static final int i_PURCHASE_REQUEST   = 5; 
    public static final int i_PURCHASE_ORDER  	 = 6;  
    public static final int i_PURCHASE_RECEIPT   = 7; 
    public static final int i_PURCHASE_INVOICE   = 8; 
    public static final int i_PURCHASE_RETURN    = 9; 
    public static final int i_ISSUE_RECEIPT      = 10; 
    public static final int i_ITEM_TRANSFER      = 11; 
    public static final int i_CYCLE_COUNT        = 12; 
    public static final int i_DEBIT_MEMO  	     = 13;  
    public static final int i_CREDIT_MEMO  	     = 14;
    public static final int i_AR_PAYMENT  	     = 15;  
    public static final int i_AP_PAYMENT  	     = 16;  
    public static final int i_CASH_FLOW  	     = 17;  
    public static final int i_PETTY_CASH  	     = 18;  
    public static final int i_JOURNAL_VOUCHER    = 19; 
    public static final int i_FIXED_ASSET        = 20; 
    public static final int i_JOB_COSTING        = 21; 
    public static final int i_PACKING_LIST       = 22; 
    public static final int i_OPENING_BALANCE	 = 23;
    public static final int i_POINT_REWARD       = 24; 
    public static final int i_TAX_INVOICE        = 25; 
    public static final int i_COST_ADJUSTMENT    = 26; 
    public static final int i_TAX_SERIAL    	 = 27; 
    public static final int i_QUOTATION  	     = 28;
    public static final int i_CASH_FLOW_IN  	 = 29;
    public static final int i_CASH_FLOW_OUT  	 = 30;
    public static final int i_CF_CASH_IN         = 31;
    public static final int i_CF_CASH_OUT        = 32;
    public static final int i_BANK_TRANSFER      = 33;
    public static final int i_TAX_DO        	 = 34;
    public static final int i_TAX_SO        	 = 35;    
    public static final int i_PROC_REQUEST   	 = 36;
    public static final int i_REIMBURSE_RQ  	 = 37;
    
	public static final String[] CTL_TABLES = 
	{   "",
        "ctl_sales_order",   
        "ctl_delivery_order",   
        "ctl_sales_invoice",  	 
        "ctl_sales_return",  	 
        "ctl_purchase_request", 
        "ctl_purchase_order",  	 
        "ctl_purchase_receipt", 
        "ctl_purchase_invoice",           
        "ctl_purchase_return",  
        "ctl_issue_receipt",    
        "ctl_item_transfer",    
        "ctl_cycle_count",     
        "ctl_debit_memo",  	  
        "ctl_credit_memo",  	 
        "ctl_ar_payment",  	  
        "ctl_ap_payment",  	  
        "ctl_cash_flow",  	     
        "ctl_petty_cash",  	  
        "ctl_journal_voucher",  
        "ctl_fixed_asset",      
        "ctl_job_costing",      
        "ctl_packing_list",     
        "ctl_opening_balance",	 
        "ctl_point_reward",     
        "ctl_tax_invoice",      
        "ctl_cost_adjustment", 
        "tax_serial",
        "ctl_quotation",
        "ctl_cash_flow_in",  	     
        "ctl_cash_flow_out",
        "ctl_cf_cash_in",          
        "ctl_cf_cash_out",
        "ctl_bank_transfer",
        "ctl_tax_do",
        "ctl_tax_so",
        "ctl_proc_request",
        "ctl_rmbs_request"        
	};
        
    public static synchronized String get (String _sFormat, int _iType, Connection _oConn)
    	throws Exception
	{
    	return get(_sFormat, CTL_TABLES[_iType], "",  _oConn);
	}    
    
    public static synchronized String get (String _sFormat, int _iType, String _sLocCode, Connection _oConn)
		throws Exception
	{
		return get(_sFormat, CTL_TABLES[_iType], _sLocCode, _oConn);
	}    
    
    public static synchronized String get (String _sFormat, String _sCtlTable, String _sLocCode, Connection _oConn)
		throws Exception
	{
    	validate(_oConn);
    	String sTransNo = "";
    	try
    	{
    		sTransNo = generateNo (_sFormat, _sCtlTable, _sLocCode, _oConn);
    		updateLastNo(_sCtlTable, sTransNo, _oConn);
    	}
    	catch (Exception _oEx)
    	{
    		throw new Exception ("Last number generation failed, ERROR: " + _oEx.getMessage(), _oEx);
    	}    	
    	return sTransNo;
	}       
    
    ///////////////////////////////////////////////////////////////////////////
	// Last Number Custom Implementation with Control table
	///////////////////////////////////////////////////////////////////////////
	
    private static String getLastNo(String _sCtlTable, Connection _oConn)
    	throws Exception
    {             
		List vData = 
			BasePeer.executeQuery("SELECT last_number FROM " + _sCtlTable + " FOR UPDATE ", 0, -1, false, _oConn);
		if (vData.size() > 0)
		{
			Record oRecord = (Record) vData.get(0);
			return oRecord.getValue(1).asString();		        
		}
        return null;
    }

    /**
     * Update last number 
     * 
     * @param iType
     * @param _sTransNo
     * @param _oConn
     * @throws Exception
     */
    private static void updateLastNo(String _sCtlTable, String _sTransNo, Connection _oConn)
    	throws Exception
    {
        String sLastNo = getLastNo(_sCtlTable, _oConn);
        StringBuilder oSQL = new StringBuilder();
        if (sLastNo != null)
        {	         
	        oSQL.append("UPDATE ").append(_sCtlTable)
	        	.append(" SET last_number = '").append(_sTransNo).append("'");
	        
	        BasePeer.executeStatement(oSQL.toString(), _oConn);
        }
        else
        {
	        oSQL.append("INSERT INTO ").append(_sCtlTable)
	        	.append(" (last_number) VALUES ('").append(_sTransNo).append("')");
	        
	        BasePeer.executeStatement(oSQL.toString(), _oConn);        	
        }
    } 
    
    private static String generateNo (String _sFormat, String _sCtlTable, String _sLocCode, Connection _oConn)
		throws Exception
	{
    	return generateNo (_sFormat, _sCtlTable, _sLocCode, null, _oConn);
	}
    
    /**
     * Generate trans no
     * 
     * @param _sFormat
     * @param iType
     * @param _oConn
     * @return generated no
     * @throws Exception
     */
    private static String generateNo (String _sFormat, String _sCtlTable, String _sLocCode, Date _dTrans, Connection _oConn)
		throws Exception
	{
    	//get number format string first char i.e "x"
    	String sNoStart = s_FORMAT_NO.substring(0,1);    	
		String sLastNo = getLastNo(_sCtlTable, _oConn);
		String sTransNo = _sFormat;

		if (log.isDebugEnabled())
		{
	    	//log.debug ("sNoStart : " + sNoStart);
			//log.debug ("sTransNo : " + sTransNo);
			log.debug ("sLastNo1 : " + sLastNo);
			log.debug ("separator : " + s_SEPARATOR + "  length : " + s_SEPARATOR.length());
		}

		int iLastNo = 0;
		if (StringUtil.isNotEmpty(sLastNo))
		{
            String sSeparator = s_SEPARATOR;
            if (!StringUtil.contains(_sFormat, sSeparator)) //find another separator possibliites
            {
                if(StringUtil.contains(_sFormat, s_STRIP)) sSeparator = s_STRIP;
                if(StringUtil.contains(_sFormat, s_UNDER)) sSeparator = s_UNDER;
                if(StringUtil.contains(_sFormat, s_SLASH)) sSeparator = s_SLASH;   
                if(StringUtil.contains(_sFormat, s_DOT)) sSeparator = s_DOT;   
                
                log.debug ("separator2 : " + sSeparator + "  length : " + sSeparator.length());
            }
            
			sLastNo = sLastNo.substring(sLastNo.lastIndexOf(sSeparator) + 1);

			//parse the last number i.e 10203
			iLastNo = Integer.parseInt(sLastNo);
			
			//construct max no String -> if s_FORMAT_NO = "xxxxx" then sMaxNo = "99999"
			String sMaxNo = _sFormat.substring(sTransNo.indexOf(sNoStart));
			sMaxNo = sMaxNo.replaceAll(sNoStart, "9");

			if (log.isDebugEnabled())
			{
				log.debug ("sSepIDX : " + sLastNo.lastIndexOf(sSeparator));
				log.debug ("sLastNo2 : " + sLastNo);
				log.debug ("sMaxNo : " + sMaxNo);
			}

			//if iLastNo equals to MaxNo then start from zero
			if (iLastNo == Integer.parseInt(sMaxNo)) iLastNo = 0;
		}
		iLastNo++;

		Calendar oCal = Calendar.getInstance();

		if (_dTrans != null) Calendar.getInstance().setTime(_dTrans);
		
		String sNewNo = StringUtil.formatNumberString(Integer.toString (iLastNo), s_FORMAT_NO.length());
		String sDays  = StringUtil.formatNumberString(Integer.toString (oCal.get(Calendar.DAY_OF_MONTH)), 2);
		String sMonth = StringUtil.formatNumberString(Integer.toString (oCal.get(Calendar.MONTH) + 1), 2);
		String sYear  = (Integer.toString(oCal.get(Calendar.YEAR))).substring(2) ;
		
		if (StringUtil.isEmpty(_sLocCode))
		{
			_sLocCode = PreferenceTool.getLocationCode();
		}
		
		sTransNo = sTransNo.replaceAll(s_FORMAT_LC, _sLocCode);
		sTransNo = sTransNo.replaceAll(s_FORMAT_DD, sDays );
		sTransNo = sTransNo.replaceAll(s_FORMAT_MO, sMonth);
		sTransNo = sTransNo.replaceAll(s_FORMAT_YR, sYear ); 
		sTransNo = sTransNo.replaceAll(s_FORMAT_NO, sNewNo);
		
		if (log.isDebugEnabled()) log.debug ("sTransNo : " + sTransNo);
		return sTransNo;
	} 
    
    //-------------------------------------------------------------------------
    //getter and manual updater
    //-------------------------------------------------------------------------    
    public static String getTransName(int _iType)
    {
    	if (_iType == i_SALES_ORDER  	 ) return LocaleTool.getString("sales_order");    
        if (_iType == i_DELIVERY_ORDER   ) return LocaleTool.getString("delivery_order"); 
        if (_iType == i_SALES_INVOICE  	 ) return LocaleTool.getString("sales_invoice");
        if (_iType == i_SALES_RETURN  	 ) return LocaleTool.getString("sales_return");
        if (_iType == i_PURCHASE_REQUEST ) return LocaleTool.getString("item_request");  
        if (_iType == i_PURCHASE_ORDER   ) return LocaleTool.getString("purchase_order"); 
        if (_iType == i_PURCHASE_RECEIPT ) return LocaleTool.getString("purchase_receipt");  
        if (_iType == i_PURCHASE_INVOICE ) return LocaleTool.getString("purchase_invoice");  
        if (_iType == i_PURCHASE_RETURN  ) return LocaleTool.getString("purchase_return");  
        if (_iType == i_ISSUE_RECEIPT    ) return LocaleTool.getString("issue_receipt");  
        if (_iType == i_ITEM_TRANSFER    ) return LocaleTool.getString("item_transfer");  
        if (_iType == i_CYCLE_COUNT      ) return LocaleTool.getString("cycle_count");  
        if (_iType == i_DEBIT_MEMO  	 ) return LocaleTool.getString("debit_memo");    
        if (_iType == i_CREDIT_MEMO  	 ) return LocaleTool.getString("credit_memo");    
        if (_iType == i_AR_PAYMENT  	 ) return LocaleTool.getString("receivable_payment");    
        if (_iType == i_AP_PAYMENT  	 ) return LocaleTool.getString("payable_payment");    
        if (_iType == i_CASH_FLOW  	     ) return LocaleTool.getString("cash_management");
        if (_iType == i_PETTY_CASH  	 ) return LocaleTool.getString("petty_cash");    
        if (_iType == i_JOURNAL_VOUCHER  ) return LocaleTool.getString("journal_voucher");  
        if (_iType == i_FIXED_ASSET      ) return LocaleTool.getString("fixed_asset");  
        if (_iType == i_JOB_COSTING      ) return LocaleTool.getString("job_costing");  
        if (_iType == i_PACKING_LIST     ) return LocaleTool.getString("packing_list");  
        if (_iType == i_OPENING_BALANCE	 ) return LocaleTool.getString("opening_balance");
        if (_iType == i_POINT_REWARD     ) return LocaleTool.getString("point_reward");  
        if (_iType == i_TAX_INVOICE      ) return LocaleTool.getString("sales_invoice") + " TAX";  
        if (_iType == i_COST_ADJUSTMENT  ) return LocaleTool.getString("issue_receipt");  
        if (_iType == i_TAX_SERIAL    	 ) return LocaleTool.getString("tax");
        if (_iType == i_QUOTATION    	 ) return LocaleTool.getString("quotation");       
        if (_iType == i_CASH_FLOW_IN     ) return LocaleTool.getString("cash_management") + " BANK IN";
        if (_iType == i_CASH_FLOW_OUT    ) return LocaleTool.getString("cash_management") + " BANK OUT";
        if (_iType == i_CF_CASH_IN     	 ) return LocaleTool.getString("cash_management") + " CASH IN";
        if (_iType == i_CF_CASH_OUT      ) return LocaleTool.getString("cash_management") + " CASH OUT";
        if (_iType == i_BANK_TRANSFER    ) return LocaleTool.getString("bank_transfer");
        if (_iType == i_TAX_DO        	 ) return LocaleTool.getString("delivery_order") + " TAX";
        if (_iType == i_TAX_SO        	 ) return LocaleTool.getString("sales_order") + " TAX";
                
        return "";
    }   
    
    public static String getForView(int _iType)
		throws Exception
	{
	    return getForView(CTL_TABLES[_iType]);
	}
    
    public static String getForView(String _sCtlTables)
		throws Exception
	{
		try
		{
			List vData = 
				BasePeer.executeQuery("SELECT last_number FROM " + _sCtlTables);
			if (vData.size() > 0)
			{
				Record oRecord = (Record) vData.get(0);
				return oRecord.getValue(1).asString();		        
			}
		}
		catch (Exception _oEx) 
		{
			log.debug("ERROR : " + _sCtlTables);
			_oEx.printStackTrace();
		}
	    return "";
	}
    
    public static void manualUpdate(int _iType, String _sNo)
		throws Exception
	{
		manualUpdate(CTL_TABLES[_iType], _sNo);
	}    
    
    public static void manualUpdate(String _sCtlTable, String _sNo)
		throws Exception
	{
		try
		{
            String sNo = getForView(_sCtlTable);
            String sSQL = "UPDATE " + _sCtlTable + " SET last_number = '" + _sNo + "'";
            if (StringUtil.isEmpty(sNo))
            {
                sSQL = "INSERT INTO " + _sCtlTable + " VALUES ('" + _sNo + "')";                
            }
			BasePeer.executeStatement(sSQL);
		}
		catch (Exception _oEx) 
		{
			_oEx.printStackTrace();
			throw new Exception("ERROR UPDATING " + _sCtlTable + "MESSAGE : " + _oEx.getMessage());
		}
	} 
    
    //-------------------------------------------------------------------------
    // CREATE CUSTOM TABLE
    //-------------------------------------------------------------------------   
    
	public static void createControlTable(String _sName) throws Exception
	{		
		String sSQL = "CREATE TABLE " + _sName + " (last_number VARCHAR(30))";
		try 
		{
			int i = SqlUtil.executeStatement(sSQL);			
		} 
		catch (Exception _oEx) 
		{
			if (StringUtil.isNotEmpty(_oEx.getMessage()) && 
				StringUtil.containsIgnoreCase(_oEx.getMessage(), "already exists"))
			{
				log.warn("Table " + _sName + " Already Exists");
			}
			else
			{
				log.error(_oEx.getMessage(), _oEx);
			}
		}
	}    
	
	//----------------------------------------------------------------------

	public static String generateByCustomer (String _sCustID, 
											 String _sLocID,
											 String _sLocCode, 
											 String _sFormat,
											 int _iType,
											 Connection _oConn) 
	throws Exception
	{
		String sTransNo = "";		
		Customer oCust = CustomerTool.getCustomerByID(_sCustID, _oConn);		
		String sPref = "";
		if(oCust != null) oCust.getTransPrefix();
		if (PreferenceTool.useCustomerTrPrefix() && StringUtil.isNotEmpty(sPref))
		{
			CustomerLastNumberTool oLN = new CustomerLastNumberTool(_iType, sPref, _sFormat, _sLocCode, _oConn);
			sTransNo = oLN.get();
		}
		else if (PreferenceTool.useLocationCtlTable() && StringUtil.isNotEmpty(_sLocID))
		{
			sTransNo = generateByLocation (_sLocID, _sFormat, _iType, _oConn);
		}
		else
		{
			sTransNo = get(_sFormat, _iType, _sLocCode, _oConn);	
		}
		return sTransNo;
	}
	
	public static String generateByVendor (String _sVendorID, 
										   String _sLocID,
										   String _sLocCode, 
										   String _sFormat,
										   int _iType,
										   Connection _oConn) 
		throws Exception
	{
		String sTransNo = "";		
		Vendor oVend = VendorTool.getVendorByID(_sVendorID, _oConn);		
		String sPref = oVend.getTransPrefix();
		if (PreferenceTool.useVendorTrPrefix() && StringUtil.isNotEmpty(sPref))
		{
			VendorLastNumberTool oLN = new VendorLastNumberTool(_iType, sPref, _sFormat, _sLocCode, _oConn);
			sTransNo = oLN.get();
		}
		else if (PreferenceTool.useLocationCtlTable() && StringUtil.isNotEmpty(_sLocID))
		{
			sTransNo = generateByLocation (_sLocID, _sFormat, _iType, _oConn);
		}		
		else
		{
			sTransNo = get(_sFormat, _iType, _sLocCode, _oConn);	
		}
		return sTransNo;
	}
	
	public static String generateByLocation (String _sLocID, 
											 String _sFormat,
											 int _iType,
											 Connection _oConn) 
		throws Exception
	{
		String sTransNo = "";		
		String sLocCode = LocationTool.getCodeByID(_sLocID);
		LocationLastNumberTool oLN = new LocationLastNumberTool(_iType, _sLocID, _sFormat, sLocCode, _oConn);
		sTransNo = oLN.get();
		return sTransNo;					
	}
}       
        
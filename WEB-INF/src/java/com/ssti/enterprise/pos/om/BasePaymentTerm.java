package com.ssti.enterprise.pos.om;


import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to PaymentTerm
 */
public abstract class BasePaymentTerm extends BaseObject
{
    /** The Peer class */
    private static final PaymentTermPeer peer =
        new PaymentTermPeer();

        
    /** The value for the paymentTermId field */
    private String paymentTermId;
      
    /** The value for the paymentTermCode field */
    private String paymentTermCode;
                                                
    /** The value for the discount field */
    private String discount = "0";
      
    /** The value for the paymentDay field */
    private int paymentDay;
      
    /** The value for the netDay field */
    private int netDay;
      
    /** The value for the description field */
    private String description;
      
    /** The value for the cashPayment field */
    private boolean cashPayment;
      
    /** The value for the isDefault field */
    private boolean isDefault;
      
    /** The value for the isMultiplePayment field */
    private boolean isMultiplePayment;
                                                        
    /** The value for the isEdc field */
    private boolean isEdc = false;
                                                
    /** The value for the paymentTypeBankId field */
    private String paymentTypeBankId = "false";
                                                
    /** The value for the bankId field */
    private String bankId = "false";
  
    
    /**
     * Get the PaymentTermId
     *
     * @return String
     */
    public String getPaymentTermId()
    {
        return paymentTermId;
    }

                        
    /**
     * Set the value of PaymentTermId
     *
     * @param v new value
     */
    public void setPaymentTermId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermId, v))
              {
            this.paymentTermId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTermCode
     *
     * @return String
     */
    public String getPaymentTermCode()
    {
        return paymentTermCode;
    }

                        
    /**
     * Set the value of PaymentTermCode
     *
     * @param v new value
     */
    public void setPaymentTermCode(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTermCode, v))
              {
            this.paymentTermCode = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Discount
     *
     * @return String
     */
    public String getDiscount()
    {
        return discount;
    }

                        
    /**
     * Set the value of Discount
     *
     * @param v new value
     */
    public void setDiscount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.discount, v))
              {
            this.discount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentDay
     *
     * @return int
     */
    public int getPaymentDay()
    {
        return paymentDay;
    }

                        
    /**
     * Set the value of PaymentDay
     *
     * @param v new value
     */
    public void setPaymentDay(int v) 
    {
    
                  if (this.paymentDay != v)
              {
            this.paymentDay = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the NetDay
     *
     * @return int
     */
    public int getNetDay()
    {
        return netDay;
    }

                        
    /**
     * Set the value of NetDay
     *
     * @param v new value
     */
    public void setNetDay(int v) 
    {
    
                  if (this.netDay != v)
              {
            this.netDay = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashPayment
     *
     * @return boolean
     */
    public boolean getCashPayment()
    {
        return cashPayment;
    }

                        
    /**
     * Set the value of CashPayment
     *
     * @param v new value
     */
    public void setCashPayment(boolean v) 
    {
    
                  if (this.cashPayment != v)
              {
            this.cashPayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsDefault
     *
     * @return boolean
     */
    public boolean getIsDefault()
    {
        return isDefault;
    }

                        
    /**
     * Set the value of IsDefault
     *
     * @param v new value
     */
    public void setIsDefault(boolean v) 
    {
    
                  if (this.isDefault != v)
              {
            this.isDefault = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsMultiplePayment
     *
     * @return boolean
     */
    public boolean getIsMultiplePayment()
    {
        return isMultiplePayment;
    }

                        
    /**
     * Set the value of IsMultiplePayment
     *
     * @param v new value
     */
    public void setIsMultiplePayment(boolean v) 
    {
    
                  if (this.isMultiplePayment != v)
              {
            this.isMultiplePayment = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IsEdc
     *
     * @return boolean
     */
    public boolean getIsEdc()
    {
        return isEdc;
    }

                        
    /**
     * Set the value of IsEdc
     *
     * @param v new value
     */
    public void setIsEdc(boolean v) 
    {
    
                  if (this.isEdc != v)
              {
            this.isEdc = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PaymentTypeBankId
     *
     * @return String
     */
    public String getPaymentTypeBankId()
    {
        return paymentTypeBankId;
    }

                        
    /**
     * Set the value of PaymentTypeBankId
     *
     * @param v new value
     */
    public void setPaymentTypeBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.paymentTypeBankId, v))
              {
            this.paymentTypeBankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("PaymentTermId");
              fieldNames.add("PaymentTermCode");
              fieldNames.add("Discount");
              fieldNames.add("PaymentDay");
              fieldNames.add("NetDay");
              fieldNames.add("Description");
              fieldNames.add("CashPayment");
              fieldNames.add("IsDefault");
              fieldNames.add("IsMultiplePayment");
              fieldNames.add("IsEdc");
              fieldNames.add("PaymentTypeBankId");
              fieldNames.add("BankId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("PaymentTermId"))
        {
                return getPaymentTermId();
            }
          if (name.equals("PaymentTermCode"))
        {
                return getPaymentTermCode();
            }
          if (name.equals("Discount"))
        {
                return getDiscount();
            }
          if (name.equals("PaymentDay"))
        {
                return Integer.valueOf(getPaymentDay());
            }
          if (name.equals("NetDay"))
        {
                return Integer.valueOf(getNetDay());
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("CashPayment"))
        {
                return Boolean.valueOf(getCashPayment());
            }
          if (name.equals("IsDefault"))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals("IsMultiplePayment"))
        {
                return Boolean.valueOf(getIsMultiplePayment());
            }
          if (name.equals("IsEdc"))
        {
                return Boolean.valueOf(getIsEdc());
            }
          if (name.equals("PaymentTypeBankId"))
        {
                return getPaymentTypeBankId();
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(PaymentTermPeer.PAYMENT_TERM_ID))
        {
                return getPaymentTermId();
            }
          if (name.equals(PaymentTermPeer.PAYMENT_TERM_CODE))
        {
                return getPaymentTermCode();
            }
          if (name.equals(PaymentTermPeer.DISCOUNT))
        {
                return getDiscount();
            }
          if (name.equals(PaymentTermPeer.PAYMENT_DAY))
        {
                return Integer.valueOf(getPaymentDay());
            }
          if (name.equals(PaymentTermPeer.NET_DAY))
        {
                return Integer.valueOf(getNetDay());
            }
          if (name.equals(PaymentTermPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(PaymentTermPeer.CASH_PAYMENT))
        {
                return Boolean.valueOf(getCashPayment());
            }
          if (name.equals(PaymentTermPeer.IS_DEFAULT))
        {
                return Boolean.valueOf(getIsDefault());
            }
          if (name.equals(PaymentTermPeer.IS_MULTIPLE_PAYMENT))
        {
                return Boolean.valueOf(getIsMultiplePayment());
            }
          if (name.equals(PaymentTermPeer.IS_EDC))
        {
                return Boolean.valueOf(getIsEdc());
            }
          if (name.equals(PaymentTermPeer.PAYMENT_TYPE_BANK_ID))
        {
                return getPaymentTypeBankId();
            }
          if (name.equals(PaymentTermPeer.BANK_ID))
        {
                return getBankId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getPaymentTermId();
            }
              if (pos == 1)
        {
                return getPaymentTermCode();
            }
              if (pos == 2)
        {
                return getDiscount();
            }
              if (pos == 3)
        {
                return Integer.valueOf(getPaymentDay());
            }
              if (pos == 4)
        {
                return Integer.valueOf(getNetDay());
            }
              if (pos == 5)
        {
                return getDescription();
            }
              if (pos == 6)
        {
                return Boolean.valueOf(getCashPayment());
            }
              if (pos == 7)
        {
                return Boolean.valueOf(getIsDefault());
            }
              if (pos == 8)
        {
                return Boolean.valueOf(getIsMultiplePayment());
            }
              if (pos == 9)
        {
                return Boolean.valueOf(getIsEdc());
            }
              if (pos == 10)
        {
                return getPaymentTypeBankId();
            }
              if (pos == 11)
        {
                return getBankId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(PaymentTermPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        PaymentTermPeer.doInsert((PaymentTerm) this, con);
                        setNew(false);
                    }
                    else
                    {
                        PaymentTermPeer.doUpdate((PaymentTerm) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key paymentTermId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setPaymentTermId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setPaymentTermId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getPaymentTermId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public PaymentTerm copy() throws TorqueException
    {
        return copyInto(new PaymentTerm());
    }
  
    protected PaymentTerm copyInto(PaymentTerm copyObj) throws TorqueException
    {
          copyObj.setPaymentTermId(paymentTermId);
          copyObj.setPaymentTermCode(paymentTermCode);
          copyObj.setDiscount(discount);
          copyObj.setPaymentDay(paymentDay);
          copyObj.setNetDay(netDay);
          copyObj.setDescription(description);
          copyObj.setCashPayment(cashPayment);
          copyObj.setIsDefault(isDefault);
          copyObj.setIsMultiplePayment(isMultiplePayment);
          copyObj.setIsEdc(isEdc);
          copyObj.setPaymentTypeBankId(paymentTypeBankId);
          copyObj.setBankId(bankId);
  
                    copyObj.setPaymentTermId((String)null);
                                                                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public PaymentTermPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("PaymentTerm\n");
        str.append("-----------\n")
           .append("PaymentTermId        : ")
           .append(getPaymentTermId())
           .append("\n")
           .append("PaymentTermCode      : ")
           .append(getPaymentTermCode())
           .append("\n")
           .append("Discount             : ")
           .append(getDiscount())
           .append("\n")
           .append("PaymentDay           : ")
           .append(getPaymentDay())
           .append("\n")
           .append("NetDay               : ")
           .append(getNetDay())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("CashPayment          : ")
           .append(getCashPayment())
           .append("\n")
           .append("IsDefault            : ")
           .append(getIsDefault())
           .append("\n")
           .append("IsMultiplePayment    : ")
           .append(getIsMultiplePayment())
           .append("\n")
           .append("IsEdc                : ")
           .append(getIsEdc())
           .append("\n")
           .append("PaymentTypeBankId    : ")
           .append(getPaymentTypeBankId())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
        ;
        return(str.toString());
    }
}

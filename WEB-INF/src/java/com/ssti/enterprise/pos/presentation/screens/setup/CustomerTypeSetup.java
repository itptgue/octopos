package com.ssti.enterprise.pos.presentation.screens.setup;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.tools.CustomerTypeTool;
import com.ssti.enterprise.pos.tools.LocaleTool;

public class CustomerTypeSetup extends SetupSecureScreen
{
    public void doBuildTemplate(RunData data, Context context)
    {
    	try
    	{
    		context.put("CustomerTypes", CustomerTypeTool.getAllCustomerType());
    	}
    	catch(Exception _oEx)
    	{
    		data.setMessage(LocaleTool.getString(s_RETREIVE_FAILED) + _oEx.getMessage());
    	}
    }
}

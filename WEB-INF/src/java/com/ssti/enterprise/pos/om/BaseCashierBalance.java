package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CashierBalance
 */
public abstract class BaseCashierBalance extends BaseObject
{
    /** The Peer class */
    private static final CashierBalancePeer peer =
        new CashierBalancePeer();

        
    /** The value for the cashierBalanceId field */
    private String cashierBalanceId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the cashierName field */
    private String cashierName;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
                                                
          
    /** The value for the transAmount field */
    private BigDecimal transAmount= bd_ZERO;
      
    /** The value for the totalAmount field */
    private BigDecimal totalAmount;
                                                
    /** The value for the description field */
    private String description = "";
      
    /** The value for the transactionType field */
    private int transactionType;
                                                
    /** The value for the hostName field */
    private String hostName = "";
                                                
    /** The value for the ipAddress field */
    private String ipAddress = "";
                                                
    /** The value for the openCashierId field */
    private String openCashierId = "";
                                                
    /** The value for the auditLog field */
    private String auditLog = "";
                                                
    /** The value for the shift field */
    private String shift = "";
  
    
    /**
     * Get the CashierBalanceId
     *
     * @return String
     */
    public String getCashierBalanceId()
    {
        return cashierBalanceId;
    }

                                              
    /**
     * Set the value of CashierBalanceId
     *
     * @param v new value
     */
    public void setCashierBalanceId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.cashierBalanceId, v))
              {
            this.cashierBalanceId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated CashierBills
        if (collCashierBillss != null)
        {
            for (int i = 0; i < collCashierBillss.size(); i++)
            {
                ((CashierBills) collCashierBillss.get(i))
                    .setCashierBalanceId(v);
            }
        }
                                                    
                  // update associated CashierCoupon
        if (collCashierCoupons != null)
        {
            for (int i = 0; i < collCashierCoupons.size(); i++)
            {
                ((CashierCoupon) collCashierCoupons.get(i))
                    .setCashierBalanceId(v);
            }
        }
                                }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashierName
     *
     * @return String
     */
    public String getCashierName()
    {
        return cashierName;
    }

                        
    /**
     * Set the value of CashierName
     *
     * @param v new value
     */
    public void setCashierName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashierName, v))
              {
            this.cashierName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTransAmount()
    {
        return transAmount;
    }

                        
    /**
     * Set the value of TransAmount
     *
     * @param v new value
     */
    public void setTransAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.transAmount, v))
              {
            this.transAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TotalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

                        
    /**
     * Set the value of TotalAmount
     *
     * @param v new value
     */
    public void setTotalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.totalAmount, v))
              {
            this.totalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HostName
     *
     * @return String
     */
    public String getHostName()
    {
        return hostName;
    }

                        
    /**
     * Set the value of HostName
     *
     * @param v new value
     */
    public void setHostName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.hostName, v))
              {
            this.hostName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the IpAddress
     *
     * @return String
     */
    public String getIpAddress()
    {
        return ipAddress;
    }

                        
    /**
     * Set the value of IpAddress
     *
     * @param v new value
     */
    public void setIpAddress(String v) 
    {
    
                  if (!ObjectUtils.equals(this.ipAddress, v))
              {
            this.ipAddress = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the OpenCashierId
     *
     * @return String
     */
    public String getOpenCashierId()
    {
        return openCashierId;
    }

                        
    /**
     * Set the value of OpenCashierId
     *
     * @param v new value
     */
    public void setOpenCashierId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.openCashierId, v))
              {
            this.openCashierId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AuditLog
     *
     * @return String
     */
    public String getAuditLog()
    {
        return auditLog;
    }

                        
    /**
     * Set the value of AuditLog
     *
     * @param v new value
     */
    public void setAuditLog(String v) 
    {
    
                  if (!ObjectUtils.equals(this.auditLog, v))
              {
            this.auditLog = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Shift
     *
     * @return String
     */
    public String getShift()
    {
        return shift;
    }

                        
    /**
     * Set the value of Shift
     *
     * @param v new value
     */
    public void setShift(String v) 
    {
    
                  if (!ObjectUtils.equals(this.shift, v))
              {
            this.shift = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collCashierBillss
     */
    protected List collCashierBillss;

    /**
     * Temporary storage of collCashierBillss to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCashierBillss()
    {
        if (collCashierBillss == null)
        {
            collCashierBillss = new ArrayList();
        }
    }

    /**
     * Method called to associate a CashierBills object to this object
     * through the CashierBills foreign key attribute
     *
     * @param l CashierBills
     * @throws TorqueException
     */
    public void addCashierBills(CashierBills l) throws TorqueException
    {
        getCashierBillss().add(l);
        l.setCashierBalance((CashierBalance) this);
    }

    /**
     * The criteria used to select the current contents of collCashierBillss
     */
    private Criteria lastCashierBillssCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashierBillss(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCashierBillss() throws TorqueException
    {
              if (collCashierBillss == null)
        {
            collCashierBillss = getCashierBillss(new Criteria(10));
        }
        return collCashierBillss;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashierBalance has previously
     * been saved, it will retrieve related CashierBillss from storage.
     * If this CashierBalance is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCashierBillss(Criteria criteria) throws TorqueException
    {
              if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                        criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId() );
                        collCashierBillss = CashierBillsPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                            if (!lastCashierBillssCriteria.equals(criteria))
                {
                    collCashierBillss = CashierBillsPeer.doSelect(criteria);
                }
            }
        }
        lastCashierBillssCriteria = criteria;

        return collCashierBillss;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashierBillss(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashierBillss(Connection con) throws TorqueException
    {
              if (collCashierBillss == null)
        {
            collCashierBillss = getCashierBillss(new Criteria(10), con);
        }
        return collCashierBillss;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashierBalance has previously
     * been saved, it will retrieve related CashierBillss from storage.
     * If this CashierBalance is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashierBillss(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                         criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                         collCashierBillss = CashierBillsPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                             if (!lastCashierBillssCriteria.equals(criteria))
                 {
                     collCashierBillss = CashierBillsPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCashierBillssCriteria = criteria;

         return collCashierBillss;
           }

                        
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashierBalance is new, it will return
     * an empty collection; or if this CashierBalance has previously
     * been saved, it will retrieve related CashierBillss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CashierBalance.
     */
    protected List getCashierBillssJoinCashierBalance(Criteria criteria)
        throws TorqueException
    {
                    if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                              criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                              collCashierBillss = CashierBillsPeer.doSelectJoinCashierBalance(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                                    if (!lastCashierBillssCriteria.equals(criteria))
            {
                collCashierBillss = CashierBillsPeer.doSelectJoinCashierBalance(criteria);
            }
        }
        lastCashierBillssCriteria = criteria;

        return collCashierBillss;
                }
                  
                    
                    
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashierBalance is new, it will return
     * an empty collection; or if this CashierBalance has previously
     * been saved, it will retrieve related CashierBillss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CashierBalance.
     */
    protected List getCashierBillssJoinPaymentBills(Criteria criteria)
        throws TorqueException
    {
                    if (collCashierBillss == null)
        {
            if (isNew())
            {
               collCashierBillss = new ArrayList();
            }
            else
            {
                              criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                              collCashierBillss = CashierBillsPeer.doSelectJoinPaymentBills(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CashierBillsPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                                    if (!lastCashierBillssCriteria.equals(criteria))
            {
                collCashierBillss = CashierBillsPeer.doSelectJoinPaymentBills(criteria);
            }
        }
        lastCashierBillssCriteria = criteria;

        return collCashierBillss;
                }
                            


                          
            
          /**
     * Collection to store aggregation of collCashierCoupons
     */
    protected List collCashierCoupons;

    /**
     * Temporary storage of collCashierCoupons to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCashierCoupons()
    {
        if (collCashierCoupons == null)
        {
            collCashierCoupons = new ArrayList();
        }
    }

    /**
     * Method called to associate a CashierCoupon object to this object
     * through the CashierCoupon foreign key attribute
     *
     * @param l CashierCoupon
     * @throws TorqueException
     */
    public void addCashierCoupon(CashierCoupon l) throws TorqueException
    {
        getCashierCoupons().add(l);
        l.setCashierBalance((CashierBalance) this);
    }

    /**
     * The criteria used to select the current contents of collCashierCoupons
     */
    private Criteria lastCashierCouponsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashierCoupons(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCashierCoupons() throws TorqueException
    {
              if (collCashierCoupons == null)
        {
            collCashierCoupons = getCashierCoupons(new Criteria(10));
        }
        return collCashierCoupons;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashierBalance has previously
     * been saved, it will retrieve related CashierCoupons from storage.
     * If this CashierBalance is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCashierCoupons(Criteria criteria) throws TorqueException
    {
              if (collCashierCoupons == null)
        {
            if (isNew())
            {
               collCashierCoupons = new ArrayList();
            }
            else
            {
                        criteria.add(CashierCouponPeer.CASHIER_BALANCE_ID, getCashierBalanceId() );
                        collCashierCoupons = CashierCouponPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CashierCouponPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                            if (!lastCashierCouponsCriteria.equals(criteria))
                {
                    collCashierCoupons = CashierCouponPeer.doSelect(criteria);
                }
            }
        }
        lastCashierCouponsCriteria = criteria;

        return collCashierCoupons;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashierCoupons(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashierCoupons(Connection con) throws TorqueException
    {
              if (collCashierCoupons == null)
        {
            collCashierCoupons = getCashierCoupons(new Criteria(10), con);
        }
        return collCashierCoupons;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashierBalance has previously
     * been saved, it will retrieve related CashierCoupons from storage.
     * If this CashierBalance is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashierCoupons(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCashierCoupons == null)
        {
            if (isNew())
            {
               collCashierCoupons = new ArrayList();
            }
            else
            {
                         criteria.add(CashierCouponPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                         collCashierCoupons = CashierCouponPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CashierCouponPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                             if (!lastCashierCouponsCriteria.equals(criteria))
                 {
                     collCashierCoupons = CashierCouponPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCashierCouponsCriteria = criteria;

         return collCashierCoupons;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashierBalance is new, it will return
     * an empty collection; or if this CashierBalance has previously
     * been saved, it will retrieve related CashierCoupons from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CashierBalance.
     */
    protected List getCashierCouponsJoinCashierBalance(Criteria criteria)
        throws TorqueException
    {
                    if (collCashierCoupons == null)
        {
            if (isNew())
            {
               collCashierCoupons = new ArrayList();
            }
            else
            {
                              criteria.add(CashierCouponPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                              collCashierCoupons = CashierCouponPeer.doSelectJoinCashierBalance(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CashierCouponPeer.CASHIER_BALANCE_ID, getCashierBalanceId());
                                    if (!lastCashierCouponsCriteria.equals(criteria))
            {
                collCashierCoupons = CashierCouponPeer.doSelectJoinCashierBalance(criteria);
            }
        }
        lastCashierCouponsCriteria = criteria;

        return collCashierCoupons;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CashierBalanceId");
              fieldNames.add("LocationId");
              fieldNames.add("CashierName");
              fieldNames.add("TransactionDate");
              fieldNames.add("TransAmount");
              fieldNames.add("TotalAmount");
              fieldNames.add("Description");
              fieldNames.add("TransactionType");
              fieldNames.add("HostName");
              fieldNames.add("IpAddress");
              fieldNames.add("OpenCashierId");
              fieldNames.add("AuditLog");
              fieldNames.add("Shift");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CashierBalanceId"))
        {
                return getCashierBalanceId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("CashierName"))
        {
                return getCashierName();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("TransAmount"))
        {
                return getTransAmount();
            }
          if (name.equals("TotalAmount"))
        {
                return getTotalAmount();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("HostName"))
        {
                return getHostName();
            }
          if (name.equals("IpAddress"))
        {
                return getIpAddress();
            }
          if (name.equals("OpenCashierId"))
        {
                return getOpenCashierId();
            }
          if (name.equals("AuditLog"))
        {
                return getAuditLog();
            }
          if (name.equals("Shift"))
        {
                return getShift();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CashierBalancePeer.CASHIER_BALANCE_ID))
        {
                return getCashierBalanceId();
            }
          if (name.equals(CashierBalancePeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(CashierBalancePeer.CASHIER_NAME))
        {
                return getCashierName();
            }
          if (name.equals(CashierBalancePeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(CashierBalancePeer.TRANS_AMOUNT))
        {
                return getTransAmount();
            }
          if (name.equals(CashierBalancePeer.TOTAL_AMOUNT))
        {
                return getTotalAmount();
            }
          if (name.equals(CashierBalancePeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(CashierBalancePeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(CashierBalancePeer.HOST_NAME))
        {
                return getHostName();
            }
          if (name.equals(CashierBalancePeer.IP_ADDRESS))
        {
                return getIpAddress();
            }
          if (name.equals(CashierBalancePeer.OPEN_CASHIER_ID))
        {
                return getOpenCashierId();
            }
          if (name.equals(CashierBalancePeer.AUDIT_LOG))
        {
                return getAuditLog();
            }
          if (name.equals(CashierBalancePeer.SHIFT))
        {
                return getShift();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCashierBalanceId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getCashierName();
            }
              if (pos == 3)
        {
                return getTransactionDate();
            }
              if (pos == 4)
        {
                return getTransAmount();
            }
              if (pos == 5)
        {
                return getTotalAmount();
            }
              if (pos == 6)
        {
                return getDescription();
            }
              if (pos == 7)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 8)
        {
                return getHostName();
            }
              if (pos == 9)
        {
                return getIpAddress();
            }
              if (pos == 10)
        {
                return getOpenCashierId();
            }
              if (pos == 11)
        {
                return getAuditLog();
            }
              if (pos == 12)
        {
                return getShift();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CashierBalancePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CashierBalancePeer.doInsert((CashierBalance) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CashierBalancePeer.doUpdate((CashierBalance) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collCashierBillss != null)
            {
                for (int i = 0; i < collCashierBillss.size(); i++)
                {
                    ((CashierBills) collCashierBillss.get(i)).save(con);
                }
            }
                                                  
                
                    if (collCashierCoupons != null)
            {
                for (int i = 0; i < collCashierCoupons.size(); i++)
                {
                    ((CashierCoupon) collCashierCoupons.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cashierBalanceId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setCashierBalanceId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setCashierBalanceId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCashierBalanceId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CashierBalance copy() throws TorqueException
    {
        return copyInto(new CashierBalance());
    }
  
    protected CashierBalance copyInto(CashierBalance copyObj) throws TorqueException
    {
          copyObj.setCashierBalanceId(cashierBalanceId);
          copyObj.setLocationId(locationId);
          copyObj.setCashierName(cashierName);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setTransAmount(transAmount);
          copyObj.setTotalAmount(totalAmount);
          copyObj.setDescription(description);
          copyObj.setTransactionType(transactionType);
          copyObj.setHostName(hostName);
          copyObj.setIpAddress(ipAddress);
          copyObj.setOpenCashierId(openCashierId);
          copyObj.setAuditLog(auditLog);
          copyObj.setShift(shift);
  
                    copyObj.setCashierBalanceId((String)null);
                                                                                    
                                      
                            
        List v = getCashierBillss();
        for (int i = 0; i < v.size(); i++)
        {
            CashierBills obj = (CashierBills) v.get(i);
            copyObj.addCashierBills(obj.copy());
        }
                                                  
                            
        v = getCashierCoupons();
        for (int i = 0; i < v.size(); i++)
        {
            CashierCoupon obj = (CashierCoupon) v.get(i);
            copyObj.addCashierCoupon(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CashierBalancePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CashierBalance\n");
        str.append("--------------\n")
           .append("CashierBalanceId     : ")
           .append(getCashierBalanceId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("CashierName          : ")
           .append(getCashierName())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("TransAmount          : ")
           .append(getTransAmount())
           .append("\n")
           .append("TotalAmount          : ")
           .append(getTotalAmount())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("HostName             : ")
           .append(getHostName())
           .append("\n")
           .append("IpAddress            : ")
           .append(getIpAddress())
           .append("\n")
           .append("OpenCashierId        : ")
           .append(getOpenCashierId())
           .append("\n")
           .append("AuditLog             : ")
           .append(getAuditLog())
           .append("\n")
           .append("Shift                : ")
           .append(getShift())
           .append("\n")
        ;
        return(str.toString());
    }
}

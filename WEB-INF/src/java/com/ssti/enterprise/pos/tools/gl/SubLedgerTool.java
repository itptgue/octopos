package com.ssti.enterprise.pos.tools.gl;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.pos.om.Account;
import com.ssti.enterprise.pos.om.AccountBalancePeer;
import com.ssti.enterprise.pos.om.AccountPeer;
import com.ssti.enterprise.pos.om.AccountTypePeer;
import com.ssti.enterprise.pos.om.Bank;
import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.GlTransaction;
import com.ssti.enterprise.pos.om.GlTransactionPeer;
import com.ssti.enterprise.pos.om.JournalVoucherDetailPeer;
import com.ssti.enterprise.pos.om.JournalVoucherPeer;
import com.ssti.enterprise.pos.tools.BankTool;
import com.ssti.enterprise.pos.tools.BaseTool;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.EmployeeTool;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.VendorTool;
import com.ssti.framework.tools.BasePeer;
import com.ssti.framework.tools.DateUtil;
import com.ssti.framework.tools.StringUtil;
/**
 * 
 *
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * <br>
 *
 * $@author  Author: albert $<br>
 * $@version Id:  $<br>
 *
 * <pre>
 * $Log: $
 * 
 * 2015-12-18
 * - add methods getSLBalanceAsOf, getSLCurrentBalalnce, getTotalAsOf, getSLChanges to make subledger 
 *   balance calculation faster, old method will query each SL to DB, now we just get all GLTrans 
 *   and calculate everything from the List
 * - deprecate slow old methods
 * </pre><br>
 */
public class SubLedgerTool extends BaseTool implements GlAttributes
{
	private static Log log = LogFactory.getLog ( SubLedgerTool.class );
	
	private static SubLedgerTool instance = null;
	
	public static synchronized SubLedgerTool getInstance()
	{
		if (instance == null) instance = new SubLedgerTool();
		return instance;
	}
	
	/**
	 * get distinct sub ledger in gl_transaction
	 *  
	 * @param _iType
	 * @param _sAccID
	 * @param _sLocationID
	 * @return
	 * @throws Exception
	 */
	public static List getSubLedger (int _iType, String _sAccID, String _sLocationID) 
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		oSB.append("SELECT distinct sub_ledger_id from gl_transaction ");
		oSB.append(" WHERE sub_ledger_type = ").append(_iType);		
		if (StringUtil.isNotEmpty(_sAccID))
		{
			oSB.append(" AND account_id = '").append(_sAccID).append("'");			
		}
		if (StringUtil.isNotEmpty(_sLocationID))
		{
			oSB.append(" AND location_id = '").append(_sLocationID).append("'");			
		}
		return BasePeer.executeQuery(oSB.toString());
	}	
	
	/**
	 * get account changes in gl transaction by subledger
	 * 
	 * @param _sAccountID
	 * @param _iSubLedgerType (1 AP, 2 AR)
	 * @param _sSubLedgerID 
	 * @param _dFrom
	 * @param _dTo
	 * @param _iType DEBIT OR CREDIT
	 * @return array of base changes [1] & prime changes [2]
	 * @throws Exception
	 * @Deprecated
	 */
	public static DebitCreditSumVO getChanges(String _sAccountID,
									    	  int _sSubLedgerType,
									    	  String _sSubLedgerID,
									    	  Date _dFrom, 
									    	  Date _dTo) 
		throws Exception
	{         
		//count debit gl transaction 
		StringBuilder oSQL = new StringBuilder();
		oSQL.append (" SELECT SUM(gtrans.amount_base), SUM(gtrans.amount), gtrans.debit_credit");
		oSQL.append (" FROM gl_transaction gtrans ");
		oSQL.append (" WHERE gtrans.account_id = '").append (_sAccountID).append ("'");
		//oSQL.append  ("AND gtrans.debit_credit =  ").append(_iType);		
		if (_dFrom != null)
		{
			oSQL.append (" AND gtrans.TRANSACTION_DATE >= ");
			oSQL.append (oDB.getDateString(DateUtil.getStartOfDayDate(_dFrom)));
		}
		if (_dTo != null)
		{
			oSQL.append (" AND gtrans.TRANSACTION_DATE <= ");
			oSQL.append (oDB.getDateString(DateUtil.getEndOfDayDate(_dTo)));
		}
		//if (StringUtil.isNotEmpty(_sSubLedgerID))
		//{
			oSQL.append (" AND gtrans.SUB_LEDGER_TYPE = ").append(_sSubLedgerType);
			oSQL.append (" AND gtrans.SUB_LEDGER_ID = '");
			oSQL.append (_sSubLedgerID);
			oSQL.append ("' ");
		//}
		oSQL.append(" GROUP BY gtrans.ACCOUNT_ID, gtrans.DEBIT_CREDIT ORDER BY gtrans.DEBIT_CREDIT");        
	    log.debug (oSQL.toString());
	    
	    List vChanges = GlTransactionPeer.executeQuery(oSQL.toString());
	    DebitCreditSumVO oBVO = new DebitCreditSumVO();
	    oBVO.setFromRecord(vChanges);
	    return oBVO;
	}
	
	/**
	 * get current balance of a GL Account for sub ledger
	 * then this method will calculate all happened transaction 
	 * to get the current sub balance
	 * 
	 * if debit account -> sum all debit trans then minus with sum of all credit trans
	 * if credit account -> sum all credit trans then minus with sum of all debit trans
	 * 
	 * @param _sAccountID
	 * @param _sSubType (dept/prj/loc)
	 * @param _sSubLedgerID (dept_id/prj_id/loc_id)
	 * @param _dAsOf
	 * @return array of base balance [1] & prime balance [2]
	 * @throws Exception
	 * @Deprecated
	 */	
	private static double[] getSLBalance (String _sAccountID, int _iSubType, String _sSubLedgerID)
		throws Exception
	{
		Account oAccount = AccountTool.getAccountByID(_sAccountID);
		Date dNow = new Date();
		Date dAsDate = oAccount.getAsDate();
	
		DebitCreditSumVO oBVO = getChanges(_sAccountID, _iSubType, _sSubLedgerID, dAsDate, dNow);
		
		double[] dBalance = new double[2];	
		if (oAccount.getNormalBalance() == i_DEBIT)
	    {
	        dBalance[i_BASE] = oBVO.getBaseDebit() - oBVO.getBaseCredit(); 
	        dBalance[i_PRIME] = oBVO.getPrimeDebit() - oBVO.getPrimeCredit(); 
	    }
	    else if (oAccount.getNormalBalance() == i_CREDIT)
	    {
	        dBalance[i_BASE] = oBVO.getBaseCredit() - oBVO.getBaseDebit(); 
	        dBalance[i_PRIME] = oBVO.getPrimeCredit() - oBVO.getPrimeDebit(); 
	    }		
		return dBalance;
	}
	
	/**
	 * 
	 * @param _sAccountID
	 * @param _iSubType
	 * @param _sSubLedgerID
	 * @param _dFromDate
	 * @return
	 * @throws Exception
	 * @Deprecated
	 */
	private static double[] getTotalChanges (String _sAccountID, 
											int _iSubType,
											String _sSubLedgerID, 
											Date _dFromDate) 
		throws Exception
	{         			
		DebitCreditSumVO oBVO = getChanges(_sAccountID, _iSubType, _sSubLedgerID, _dFromDate, null);

		double[] dTotalDebit =  oBVO.getDebit();
		double[] dTotalCredit = oBVO.getCredit(); 
		double[] dTotal = new double[2];
		
		Account oAccount = AccountTool.getAccountByID (_sAccountID);
		if (oAccount.getNormalBalance() == i_DEBIT)
		{
			dTotal[i_BASE]  = dTotal[i_BASE]  - dTotalDebit[i_BASE]  + dTotalCredit[i_BASE];
			dTotal[i_PRIME] = dTotal[i_PRIME] - dTotalDebit[i_PRIME] + dTotalCredit[i_PRIME];
		}
		else if (oAccount.getNormalBalance() == i_CREDIT)
		{
			dTotal[i_BASE]  = dTotal[i_BASE]  + dTotalDebit[i_BASE]  - dTotalCredit[i_BASE];
			dTotal[i_PRIME] = dTotal[i_PRIME] + dTotalDebit[i_PRIME] - dTotalCredit[i_PRIME];
		}
		return dTotal;
	}

	
	/**
	 * get account current balance or total balance for parent account
	 * 
	 * @param _sAccountID
	 * @param _sSubType (dept/prj/loc)
	 * @param _sSubID (dept_id/prj_id/loc_id)
	 * @param _dAsOf 
	 * @return array of base balance [1] & prime balance [2]
	 * @throws Exception
	 * @Deprecated
	 */
	public static double[] getSubLedgerBalance (String _sAccountID, int _sSubType, String _sSubID, Date _dAsOf) 
		throws Exception
	{
		Criteria oCrit = new Criteria();
        Account oAccount = AccountTool.getAccountByID(_sAccountID);
        
        double[] dTotalBalance = new double[2];
        dTotalBalance[i_BASE] = 0;
        dTotalBalance[i_PRIME] = 0;

        if (oAccount != null)
        {
        	oCrit.add (AccountBalancePeer.ACCOUNT_ID, _sAccountID);        	
        	
        	double[] dTotalTrans = getTotalChanges(_sAccountID, _sSubType, _sSubID, _dAsOf);
        	double[] dCurrBalance = getSLBalance(_sAccountID, _sSubType, _sSubID);
        	double dCurrBaseBal = dCurrBalance[i_BASE];
        	double dCurrPrimeBal = dCurrBalance[i_PRIME];
        	
        	if (log.isDebugEnabled())
        	{
        		log.debug ("Account : " + oAccount.getAccountName());
        		log.debug ("Current Base Balance : " + dCurrBaseBal);
        		log.debug ("Current Prime Balance : " + dCurrPrimeBal);	            
        		log.debug ("Total Trans AsOf " + _dAsOf + " : " + dTotalTrans[i_BASE] + "," + dTotalTrans[i_PRIME]);
        	}
        	dCurrBaseBal = dCurrBaseBal + dTotalTrans[i_BASE];
        	dCurrPrimeBal = dCurrPrimeBal + dTotalTrans[i_PRIME];        		
        	if (log.isDebugEnabled())
        	{
        		log.debug ("Base Balance : " + dCurrBaseBal);
        		log.debug ("Prime Balance : " + dCurrPrimeBal);
        	}
        	dTotalBalance[i_BASE] = dTotalBalance[i_BASE] + dCurrBaseBal;             
        	dTotalBalance[i_PRIME] = dTotalBalance[i_PRIME] + dCurrPrimeBal;                     
        }
    	return dTotalBalance;
	}	

	//-------------------------------------------------------------------------
	//
	//-------------------------------------------------------------------------
	
	public static String getSLDesc(int _iType)
	{
		String sDesc = LocaleTool.getString("other");;
		try 
		{
			if (_iType == i_SUB_LEDGER_AP)
			{
				sDesc = LocaleTool.getString("vendor");
			}
			else if (_iType == i_SUB_LEDGER_AR)
			{
				sDesc = LocaleTool.getString("customer");
			}
			else if (_iType == i_SUB_LEDGER_CASH)
			{
				sDesc = LocaleTool.getString("bank");				
			}
			else if (_iType == 4) //emp
 			{
				sDesc = LocaleTool.getString("employee");
			}			
		} 
		catch (Exception e) 
		{
			log.error(e);
		}		
		return sDesc;
	}
	
	public static String getSLCode(int _iType, String _sID)
	{
		String sCode = "";
		try 
		{
			if (_iType == i_SUB_LEDGER_AP)
			{
				sCode = VendorTool.getCodeByID(_sID);
			}
			else if (_iType == i_SUB_LEDGER_AR)
			{
				Customer oCust = CustomerTool.getCustomerByID(_sID);
				if (oCust != null)
				sCode = oCust.getCustomerCode();
			}
			else if (_iType == i_SUB_LEDGER_CASH)
			{
				Bank oBank = BankTool.getBankByID(_sID);
				if (oBank != null)
				sCode = oBank.getBankCode();
			}
			else if (_iType == 4) //emp
 			{
				sCode = EmployeeTool.getEmployeeNameByID(_sID);
			}			
		} 
		catch (Exception e) 
		{
			log.error(e);
		}		
		return sCode;
	}
	
	public static String getSLName(int _iType, String _sID)
	{
		String sCode = "";
		try 
		{
			if (_iType == i_SUB_LEDGER_AP)
			{
				sCode = VendorTool.getVendorNameByID(_sID);
			}
			else if (_iType == i_SUB_LEDGER_AR)
			{
				sCode = CustomerTool.getCustomerNameByID(_sID);
			}
			else if (_iType == i_SUB_LEDGER_CASH)
			{
				Bank oBank = BankTool.getBankByID(_sID);
				if (oBank != null)
				sCode = oBank.getDescription();			
			}
			else if (_iType == 4) //emp
 			{
				sCode = EmployeeTool.getEmployeeNameByID(_sID);
			}			
		} 
		catch (Exception e) 
		{
			log.error(e);
		}		
		return sCode;
	}
	
	public static String getIDByCode(int _iType, String _sCode)
	{
		String sID = "";
		try 
		{
			if (_iType == i_SUB_LEDGER_AP)
			{
				sID = VendorTool.getIDByCode(_sCode);
			}
			else if (_iType == i_SUB_LEDGER_AR)
			{
				Customer oCust = CustomerTool.getCustomerByCode(_sCode);
				if (oCust != null) sID =  oCust.getCustomerId();
			}
			else if (_iType == i_SUB_LEDGER_CASH)
			{
				Bank oBank = BankTool.getBankByCode(_sCode);
				if (oBank != null)
				sID = oBank.getBankId();			
			}
			else if (_iType == 4) //emp
 			{
				sID = EmployeeTool.getIDByName(_sCode);
			}			
		} 
		catch (Exception e) 
		{
			log.error(e);
		}		
		return sID;
	}
	
	//-------------------------------------------------------------------------
	// Complex Journal Voucher with sub ledger input methods
	//-------------------------------------------------------------------------
	public static List findJVD(Date _dStart,
							   Date _dEnd,
							   String _sAccID, 
							   String _sDeptID, 
							   int _iSLType, 
							   String _sSLID, 
							   String _sSLDesc, 
							   String _sRefNo)
		throws Exception
	{
		Criteria oCrit = new Criteria();
		if (StringUtil.isNotEmpty(_sAccID)) oCrit.add(JournalVoucherDetailPeer.ACCOUNT_ID, _sAccID);
		if (StringUtil.isNotEmpty(_sDeptID)) oCrit.add(JournalVoucherDetailPeer.DEPARTMENT_ID, _sDeptID);
		if (StringUtil.isNotEmpty(_sSLID)) oCrit.add(JournalVoucherDetailPeer.SUB_LEDGER_ID, _sSLID);
		if (StringUtil.isNotEmpty(_sSLDesc)) oCrit.add(JournalVoucherDetailPeer.SUB_LEDGER_DESC, (Object)("%"+_sSLDesc+"%"), Criteria.ILIKE);
		if (StringUtil.isNotEmpty(_sRefNo)) oCrit.add(JournalVoucherDetailPeer.REFERENCE_NO, (Object)("%"+_sRefNo+"%"), Criteria.ILIKE);
		if (_iSLType > 0) oCrit.add(JournalVoucherDetailPeer.SUB_LEDGER_TYPE, _iSLType);
		
		oCrit.addJoin(JournalVoucherDetailPeer.JOURNAL_VOUCHER_ID, JournalVoucherPeer.JOURNAL_VOUCHER_ID);
		oCrit.add(JournalVoucherPeer.STATUS,i_PROCESSED);
		if (_dStart != null) oCrit.add(JournalVoucherPeer.TRANSACTION_DATE, _dStart, Criteria.GREATER_EQUAL);
		if (_dEnd != null) oCrit.add(JournalVoucherPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dEnd), Criteria.LESS_EQUAL);
		
		
		return JournalVoucherDetailPeer.doSelect(oCrit);
	}
	
	public static List getSLDesc(int _iSLType, String _sSLDesc)
		throws Exception
	{
		StringBuilder oSB = new StringBuilder();
		oSB.append("SELECT distinct sub_ledger_desc from journal_voucher_detail ");
		oSB.append(" WHERE sub_ledger_type = ").append(_iSLType);		
		if (StringUtil.isNotEmpty(_sSLDesc))
		{
			oSB.append(" AND sub_ledger_desc LIKE '").append(_sSLDesc).append("'");			
		}
		return BasePeer.executeQuery(oSB.toString());	
	}
	
	public static List findSLData (int _iType, 
								   int _iGLType, 
								   String _sAccountID,
								   String _sSubType,	                             
								   String _sSubID,
								   String _sTransNo,
								   String _sAccNo,
								   Date _dFrom, 
								   Date _dTo,
							       int _iSubLedger,
								   String _sSubLedgerID) throws Exception
     {
		Criteria oCrit = new Criteria();
		if (_iType > 0)
		{
			oCrit.add (GlTransactionPeer.TRANSACTION_TYPE, _iType);
		}
		if (_iGLType > 0)
		{
			oCrit.addJoin(GlTransactionPeer.ACCOUNT_ID, AccountPeer.ACCOUNT_ID);
		}
		if (_iGLType >= 1 && _iGLType <= 8)
		{
			oCrit.addJoin(AccountPeer.ACCOUNT_TYPE, AccountTypePeer.ACCOUNT_TYPE);
			oCrit.add(AccountTypePeer.GL_TYPE, _iGLType);
		}
		else if (_iGLType == 9) //BS ONLY
		{
			oCrit.add(AccountPeer.ACCOUNT_TYPE, 10, Criteria.LESS_EQUAL );
		}
		else if (_iGLType == 10) //PL ONLY
		{
			oCrit.add(AccountPeer.ACCOUNT_TYPE, 11, Criteria.GREATER_EQUAL );        
		}

		if (StringUtil.isNotEmpty(_sAccountID)) 
		{
			oCrit.add (GlTransactionPeer.ACCOUNT_ID, _sAccountID);
		}
		if (StringUtil.isNotEmpty(_sSubType) && StringUtil.isNotEmpty(_sSubID)) 
		{			
			oCrit.add (GlTransactionPeer.LOCATION_ID, _sSubID);
		}		
		if (StringUtil.isNotEmpty(_sTransNo)) 
		{
			oCrit.add (GlTransactionPeer.TRANSACTION_NO, (Object)_sTransNo, Criteria.LIKE);
		}		
		if (StringUtil.isNotEmpty(_sAccNo)) 
		{
			oCrit.addJoin(GlTransactionPeer.ACCOUNT_ID, AccountPeer.ACCOUNT_ID);
			oCrit.add(AccountPeer.ACCOUNT_CODE, (Object)_sAccNo, Criteria.LIKE);
		}		

		if (_dFrom != null)  oCrit.add (GlTransactionPeer.TRANSACTION_DATE, _dFrom, Criteria.GREATER_EQUAL);
		if (_dTo != null)  oCrit.and (GlTransactionPeer.TRANSACTION_DATE, DateUtil.getEndOfDayDate(_dTo), Criteria.LESS_EQUAL);

		oCrit.addAscendingOrderByColumn(GlTransactionPeer.TRANSACTION_DATE);
		oCrit.addAscendingOrderByColumn(GlTransactionPeer.TRANSACTION_NO);

		if (_iSubLedger > 0)
		{
			oCrit.add(GlTransactionPeer.SUB_LEDGER_TYPE, _iSubLedger);			
			oCrit.add(GlTransactionPeer.SUB_LEDGER_ID, _sSubLedgerID);			
		}
		if (StringUtil.isNotEmpty(_sSubLedgerID))
		{
			oCrit.add(GlTransactionPeer.SUB_LEDGER_ID, _sSubLedgerID);			
		}		

		log.debug (oCrit);
		return GlTransactionPeer.doSelect (oCrit);
	}
	
	//-------------------------------------------------------------------------
	// NEW METHOD
	//-------------------------------------------------------------------------
	
	/**
	 * get SubLedger Balance AsOf specified Date
	 *  
	 * @param _vAllGLT  List of GLTransaction from as of date till now  
	 * @param _sAccountID
	 * @param _iSLType
	 * @param _sSLID
	 * @param _dAsOf
	 * @return
	 * @throws Exception
	 */
	public static double[] getSLBalanceAsOf (List _vAllGLT, String _sAccountID, int _iSLType, String _sSLID, Date _dAsOf) 
		throws Exception
	{
        Account oAccount = AccountTool.getAccountByID(_sAccountID);        
        double[] dTotalBalance = new double[2];
        dTotalBalance[i_BASE] = 0;
        dTotalBalance[i_PRIME] = 0;

        if (oAccount != null)
        {
        	double[] dTotalTrans = getTotalAsOf(_vAllGLT, _sAccountID, _iSLType, _sSLID, _dAsOf);
        	double[] dCurrBalance = getSLCurrentBalalnce(_vAllGLT, _sAccountID, _iSLType, _sSLID);
        	double dCurrBaseBal = dCurrBalance[i_BASE];
        	double dCurrPrimeBal = dCurrBalance[i_PRIME];
        	
        	if (log.isDebugEnabled())
        	{
//        		log.debug ("Account : " + oAccount.getAccountName());
//        		log.debug ("Current Base Balance : " + dCurrBaseBal);
//        		log.debug ("Current Prime Balance : " + dCurrPrimeBal);	            
//        		log.debug ("Total Trans AsOf " + _dAsOf + " : " + dTotalTrans[i_BASE] + "," + dTotalTrans[i_PRIME]);
        	}
        	dCurrBaseBal = dCurrBaseBal + dTotalTrans[i_BASE];
        	dCurrPrimeBal = dCurrPrimeBal + dTotalTrans[i_PRIME];        		
        	if (log.isDebugEnabled())
        	{
//        		log.debug ("Base Balance : " + dCurrBaseBal);
//        		log.debug ("Prime Balance : " + dCurrPrimeBal);
        	}
        	dTotalBalance[i_BASE] = dTotalBalance[i_BASE] + dCurrBaseBal;             
        	dTotalBalance[i_PRIME] = dTotalBalance[i_PRIME] + dCurrPrimeBal;                     
        }
    	return dTotalBalance;
	}	

	private static double[] getSLCurrentBalalnce (List _vAllGLT, String _sAccountID, int _iSubType, String _sSubLedgerID)
		throws Exception
	{
		Account oAccount = AccountTool.getAccountByID(_sAccountID);
		Date dNow = new Date();
		Date dAsDate = oAccount.getAsDate();
	
		DebitCreditSumVO oBVO = getSLChanges(_vAllGLT, _sAccountID, _iSubType, _sSubLedgerID, dAsDate, dNow);
		
		double[] dBalance = new double[2];	
		if (oAccount.getNormalBalance() == i_DEBIT)
	    {
	        dBalance[i_BASE] = oBVO.getBaseDebit() - oBVO.getBaseCredit(); 
	        dBalance[i_PRIME] = oBVO.getPrimeDebit() - oBVO.getPrimeCredit(); 
	    }
	    else if (oAccount.getNormalBalance() == i_CREDIT)
	    {
	        dBalance[i_BASE] = oBVO.getBaseCredit() - oBVO.getBaseDebit(); 
	        dBalance[i_PRIME] = oBVO.getPrimeCredit() - oBVO.getPrimeDebit(); 
	    }		
		return dBalance;
	}

	public static double[] getTotalAsOf (List _vAllGLT, String _sAccountID,  int _iSLType, String _sSLID, Date _dAsOf) 
		throws Exception
	{         			
		DebitCreditSumVO oBVO = getSLChanges(_vAllGLT, _sAccountID, _iSLType, _sSLID, _dAsOf, null);

		double[] dTotalDebit =  oBVO.getDebit();
		double[] dTotalCredit = oBVO.getCredit(); 
		double[] dTotal = new double[2];

		Account oAccount = AccountTool.getAccountByID (_sAccountID);
		if (oAccount.getNormalBalance() == i_DEBIT)
		{
			dTotal[i_BASE]  = dTotal[i_BASE]  - dTotalDebit[i_BASE]  + dTotalCredit[i_BASE];
			dTotal[i_PRIME] = dTotal[i_PRIME] - dTotalDebit[i_PRIME] + dTotalCredit[i_PRIME];
		}
		else if (oAccount.getNormalBalance() == i_CREDIT)
		{
			dTotal[i_BASE]  = dTotal[i_BASE]  + dTotalDebit[i_BASE]  - dTotalCredit[i_BASE];
			dTotal[i_PRIME] = dTotal[i_PRIME] + dTotalDebit[i_PRIME] - dTotalCredit[i_PRIME];
		}
		return dTotal;
	}
	
	/**
	 * sum total amount prime and amount base in gl transaction by account and subledger
	 * within date range
	 * 
	 * @param _vAllGLT
	 * @param _sAccountID
	 * @param _iSLType
	 * @param _sSLID
	 * @param _dFr
	 * @param _dTo
	 * @return
	 * @throws Exception
	 */
	public static DebitCreditSumVO getSLChanges(List _vAllGLT, String _sAccountID, int _iSLType, String _sSLID, Date _dFr, Date _dTo) 
	    throws Exception
	{         
		if(_vAllGLT != null)
		{
			double dDBP = 0;
			double dDBB = 0;

			double dCRP = 0;
			double dCRB = 0;
			
			DebitCreditSumVO oDC = new DebitCreditSumVO();
			for(int i = 0; i < _vAllGLT.size(); i++)
			{
				GlTransaction oGL = (GlTransaction) _vAllGLT.get(i);
				if(StringUtil.isEqual(oGL.getAccountId(), _sAccountID) && 
				   StringUtil.isEqual(oGL.getSubLedgerId(), _sSLID) &&
				   oGL.getSubLedgerType() == _iSLType)
				{
					if( ( _dFr == null || (_dFr != null && DateUtil.isAfter(oGL.getTransactionDate(), _dFr)) ) &&
						( _dTo == null || (_dTo != null && DateUtil.isBefore(oGL.getTransactionDate(), _dTo)) ) )
					{
						if(oGL.getDebitCredit() == i_DEBIT)
						{
							dDBP += oGL.getAmount().doubleValue();						
							dDBB += oGL.getAmountBase().doubleValue();							
						}
						if(oGL.getDebitCredit() == i_CREDIT)
						{
							dCRP += oGL.getAmount().doubleValue();						
							dCRB += oGL.getAmountBase().doubleValue();							
						}
					}
				}				
				oDC.setBaseDebit(dDBB);
				oDC.setBaseCredit(dCRB);
				oDC.setPrimeDebit(dDBP);
				oDC.setPrimeCredit(dCRP);
			}
			return oDC;		
		}	
		return null;
	}
}
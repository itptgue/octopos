package com.ssti.enterprise.medical.manager;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;

import com.ssti.enterprise.medical.om.Doctor;
import com.ssti.enterprise.medical.om.DoctorPeer;
import com.ssti.framework.tools.StringUtil;

public class DoctorManager
{   
	private static Log log = LogFactory.getLog(DoctorManager.class);
	 
    private static DoctorManager m_oInstance;
    private static CacheManager m_oManager;
    private static final String s_CACHE = "master";      

    private DoctorManager() 
    {
        try 
        {
            m_oManager = CacheManager.create();          
        } 
        catch (Exception _oEx) 
        {
            log.error (_oEx);
        }
    }

    public static DoctorManager getInstance() 
    {
        if (m_oInstance == null) 
        {
            synchronized (DoctorManager.class) 
            {
                if (m_oInstance == null) 
                {
                    m_oInstance = new DoctorManager();
                }
            }
        }
        return m_oInstance;
    }

	public List getAllDoctor ()
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("AllDoctor");
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (DoctorPeer.DOCTOR_NAME);	
			List vData = DoctorPeer.doSelect(oCrit);
			oCache.put(new Element ("AllDoctor", (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	
	
	public List getInternalDoctor(String _sLocID)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE);         
		Element oElement = oCache.get("IntDoctor" + _sLocID);
		if (oElement == null)
		{		
			Criteria oCrit = new Criteria();
			oCrit.addAscendingOrderByColumn (DoctorPeer.DOCTOR_NAME);	
			oCrit.add(DoctorPeer.IS_INTERNAL, true);
			if (StringUtil.isNotEmpty(_sLocID))
			{
				oCrit.add(DoctorPeer.LOCATION_ID, _sLocID);
				oCrit.or(DoctorPeer.LOCATION_ID,"");
			}
			List vData = DoctorPeer.doSelect(oCrit);
			oCache.put(new Element ("IntDoctor" + _sLocID, (Serializable)vData)); 
			return vData;
		}
		else 
		{
			return (List) oElement.getValue();
		}
	}	

	public Doctor getDoctorByID(String _sID, Connection _oConn)
    	throws Exception
    {
        Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("Doctor" + _sID);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sID))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(DoctorPeer.DOCTOR_ID, _sID);
				List vData = DoctorPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("Doctor" + _sID, (Serializable)vData.get(0))); 
					return (Doctor) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Doctor) oElement.getValue();
		}
	}
	
	public Doctor getDoctorByUserName(String _sUser, Connection _oConn)
		throws Exception
	{
	    Cache oCache = m_oManager.getCache(s_CACHE); 
		Element oElement = oCache.get("DoctorUser" + _sUser);
		if (oElement == null)
		{		
			if (StringUtil.isNotEmpty(_sUser))
			{
				Criteria oCrit = new Criteria();
				oCrit.add(DoctorPeer.USER_NAME, _sUser);
				List vData = DoctorPeer.doSelect(oCrit, _oConn);
				if (vData.size() > 0) 
				{
					oCache.put(new Element ("DoctorUser" + _sUser, (Serializable)vData.get(0))); 
					return (Doctor) vData.get(0);
				}
			}
			return null;
		}
		else {
			return (Doctor) oElement.getValue();
		}
	}
	
	//THIS METHOD MUST BE CALLED AFTER ACTION SAVE PROCESS     
	public void refreshCache (Doctor _oData) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        if ( _oData != null && StringUtil.isNotEmpty(_oData.getDoctorId())) 
        {
            oCache.remove("Doctor" + _oData.getDoctorId());
            oCache.remove("DoctorUser" + _oData.getUserName());            
            oCache.remove("IntDoctor" + _oData.getLocationId());
            oCache.put(new Element ("Doctor" + _oData.getDoctorId(), _oData));            
        }
        oCache.remove("AllDoctor");
        oCache.remove("IntDoctor");  
        oCache.removeAll();
    }
	
	//for delete
	public void refreshCache (String _sID) 
    	throws Exception
    {	
        Cache oCache = m_oManager.getCache(s_CACHE);         
        oCache.remove("Doctor" + _sID);     
        oCache.remove("AllDoctor");
        oCache.remove("IntDoctor");
        oCache.removeAll();
    } 
}
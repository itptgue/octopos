package com.ssti.enterprise.pos.presentation.actions.company;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.util.Criteria;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import com.ssti.enterprise.pos.manager.AdjustmentTypeManager;
import com.ssti.enterprise.pos.manager.CurrencyManager;
import com.ssti.enterprise.pos.manager.TaxManager;
import com.ssti.enterprise.pos.om.AdjustmentType;
import com.ssti.enterprise.pos.om.Currency;
import com.ssti.enterprise.pos.om.GlConfig;
import com.ssti.enterprise.pos.om.GlConfigPeer;
import com.ssti.enterprise.pos.om.Tax;
import com.ssti.enterprise.pos.tools.AdjustmentTypeTool;
import com.ssti.enterprise.pos.tools.CurrencyTool;
import com.ssti.enterprise.pos.tools.IDGenerator;
import com.ssti.enterprise.pos.tools.LocaleTool;
import com.ssti.enterprise.pos.tools.TaxTool;
import com.ssti.enterprise.pos.tools.gl.GLConfigTool;

/**
 * RetailSoft - Copyright (c) 2004 SSTI
 *
 * $Source: /opt/CVS/POS/WEB-INF/src/java/com/ssti/enterprise/pos/presentation/actions/company/GLConfigAction.java,v $
 *
 * Purpose: handle gl config action
 *
 * @author  $Author: albert $
 * @version $Id: GLConfigAction.java,v 1.10 2008/06/29 07:03:08 albert Exp $
 *
 * $Log: GLConfigAction.java,v $
 * Revision 1.10  2008/06/29 07:03:08  albert
 * *** empty log message ***
 *
 * Revision 1.9  2005/10/04 11:50:12  albert
 * *** empty log message ***
 *
 * Revision 1.8  2005/08/21 14:26:20  albert
 * *** empty log message ***
 *
 * Revision 1.7  2005/04/08 06:59:05  Albert
 * *** empty log message ***
 *
 * Revision 1.6  2005/03/28 09:18:10  Albert
 * *** empty log message ***
 *
 * Revision 1.5  2005/02/25 10:44:02  Albert
 * *** empty log message ***
 *
 * Revision 1.4  2005/01/30 03:42:32  albert
 * important : now GLConfig will update all item that GL config is not set yet
 *
 * Revision 1.3  2004/11/08 02:20:33  Albert
 * organize import by eclipse
 *
 * Revision 1.2  2004/11/05 08:36:24  Albert
 * no message
 *
 * Revision 1.1  2004/10/27 09:44:41  Albert
 * no message
 *
 */

public class GLConfigAction extends CompanySecureAction
{    
	private static Log log = LogFactory.getLog(GLConfigAction.class);

	public void doSave(RunData data, Context context)
        throws Exception
    {
		try 
		{
            GlConfig oGlConfig = null;

			String sID = data.getParameters().getString("GlConfigId",null);	
			Criteria oCrit = new Criteria();
            oCrit.add (GlConfigPeer.GL_CONFIG_ID, sID); 
			List vData = GlConfigPeer.doSelect (oCrit);
            if (vData.size() <= 0)
            {
                oGlConfig = new GlConfig();
			    data.getParameters().setProperties(oGlConfig);
	            oGlConfig.setGlConfigId (IDGenerator.generateSysID());
            }
            else 
            {
                oGlConfig = (GlConfig) vData.get(0);
			    data.getParameters().setProperties(oGlConfig);            
            }
            oGlConfig.save();
	        GLConfigTool.refreshPref();
	        context.put("GlConfig", oGlConfig); 
	        context.put("ItemUpdated", Integer.valueOf(GLConfigTool.updateItem(oGlConfig)));
        	
	        //update currency value
        	String sCurrencyID = data.getParameters().getString("CurrencyId");
        	Currency oCurr = CurrencyTool.getCurrencyByID(sCurrencyID);
        	if (oCurr != null)
        	{
        	    data.getParameters().setProperties(oCurr);
        	    oCurr.save();
        	    CurrencyManager.getInstance().refreshCache(oCurr);
        	}

        	//update tax value
        	String sTaxID = data.getParameters().getString("TaxId");
        	Tax oTax = TaxTool.getTaxByID(sTaxID);
        	if (oTax != null)
        	{
        	    data.getParameters().setProperties(oTax);
        	    oTax.save();
        	    TaxManager.getInstance().refreshCache(oTax);
        	}

        	//update adjustment type value
        	String sAdjID = data.getParameters().getString("AdjustmentTypeId");
        	AdjustmentType oAdj = AdjustmentTypeTool.getAdjustmentTypeByID(sAdjID);
        	if (oAdj != null)
        	{
        	    data.getParameters().setProperties(oAdj);
        	    oAdj.save();
        	    AdjustmentTypeManager.getInstance().refreshCache(oAdj);
        	}
        	data.setMessage (LocaleTool.getString("glconf_saved"));
        }
        catch (Exception _oEx) 
        {
        	String 	sError = LocaleTool.getString("glconf_save_failed") + _oEx.getMessage();
			log.error ( sError, _oEx);			        	
        	data.setMessage (sError);
        }
    }
}
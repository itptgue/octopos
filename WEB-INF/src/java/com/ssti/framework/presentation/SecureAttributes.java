package com.ssti.framework.presentation;

import com.ssti.framework.tools.Attributes;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * bundles.properties / localization common keys constants
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: SecureAttributes.java,v 1.10 2008/02/26 05:15:43 albert Exp $ <br>
 *
 * <pre>
 * $Log: SecureAttributes.java,v $
 * Revision 1.10  2008/02/26 05:15:43  albert
 * *** empty log message ***
  </pre><br>
 *
 */
public interface SecureAttributes extends Attributes
{
	public static final String s_NO_PERM_TPL = CONFIG.getString("template.nopermission");
	public static final String s_LOGIN_TPL = CONFIG.getString("template.login");

	//Localized Messages Key
	public static final String s_INSERT_SUCCESS     = "insert_success";
	public static final String s_UPDATE_SUCCESS     = "update_success";
	public static final String s_SAVE_SUCCESS       = "save_success";	
	public static final String s_DELETE_SUCCESS     = "delete_success";
	public static final String s_GENERATE_SUCCESS   = "generate_success";
	public static final String s_LOAD_SUCCESS     	= "load_success";
	public static final String s_INSERT_FAILED      = "insert_failed";
	public static final String s_UPDATE_FAILED      = "update_failed";
	public static final String s_SAVE_FAILED        = "save_failed";
	public static final String s_DELETE_FAILED      = "delete_failed";
	public static final String s_GENERATE_FAILED    = "generate_failed";
	public static final String s_LOAD_FAILED     	= "load_failed";
	public static final String s_DELETE_REFERENCE   = "delete_reference";
	public static final String s_SET_PROP_FAILED    = "set_prop_failed";
	public static final String s_UPLOAD_PIC_FAILED  = "upload_pic_failed";	
	public static final String s_SAVE_FIELDS_FAILED = "save_fields_failed";	
	public static final String s_EXCEL_LOAD_SUCCESS = "excel_load_success";	
	public static final String s_EXCEL_LOAD_FAILED  = "excel_load_failed";		
	public static final String s_RETREIVE_FAILED    = "retreive_failed";
	public static final String s_DUPLICATE_ENTRY	= "duplicate_entry";
	public static final String s_OB_FAILED			= "ob_failed";
	public static final String s_CANCELLED_BY		= "cancelled_by";
	
	//REFRESH CONTROL RELATED
	public static final String s_REFRESH			= "REFRESH";
}

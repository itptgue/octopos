package com.ssti.framework.barcode.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.env.EnvironmentFactory;
import net.sourceforge.barbecue.output.OutputException;

/**
 * 
 * <b>RetailSoft - Copyright (c) 2004 SSTI </b><br><br>
 *
 * <b>Purpose: </b><br>
 * Create barcode image <br>
 * The query parameters for this servlet are:
 * <ol>
 * <li>value, required, example: "1234567890"
 * <li>type, optional, examples: "Code128A", "Code128B", "Code128C", if left blank will default to Code 128 B.
 * Note that the type here must be <b>exactly</b> the name of one of the createX methods on BarcodeFactory without
 * the "create" prefix. This is case sensitive.
 * <li>appid, required for UCC128 type, ignored for all other types - specifies the application identifier to use with
 * the UCC128 barcode, example: "420" for a US postal service barcode
 * <li>width, optional, in pixels
 * <li>height, optional, in pixels
 * <li>resolution, optional, in dpi
 * <li>drawText, optional, set to "false" for no text
 * </ol>
 * <br>
 *
 * @author  $Author: albert $ <br>
 * @version $Id: BarcodeServlet.java,v 1.5 2007/02/23 14:10:42 albert Exp $ <br>
 *
 *<pre>
 * $Log: BarcodeServlet.java,v $
 * Revision 1.5  2007/02/23 14:10:42  albert
 * *** empty log message ***
 *
 * Revision 1.4  2007/02/23 13:50:11  albert
 * *** empty log message ***
 * </pre><br>
 *
 */

public class BarcodeServlet extends HttpServlet {

	public String getServletName() {
		return "BarcodeServlet";
	}

	/**
	 * From HttpServlet.
	 * @param req The servlet request
	 * @param res The servlet response
	 * @throws ServletException If an error occurs during processing
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) 
		throws ServletException 
	{
		doGet(req, res);
	}

	/**
	 * From HttpServlet.
	 * @param req The servlet request
	 * @param res The servlet response
	 * @throws ServletException If an error occurs during processing
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) 
		throws ServletException 
	{
		EnvironmentFactory.setHeadlessMode();
		String data = getRequiredParameter(req, "data");
		String type = getParameter(req, "type");
		String appId = getParameter(req, "appid");
		Double width = getParameterAsDouble(req, "width");
		Double height = getParameterAsDouble(req, "height");
		Double resolution = getParameterAsDouble(req, "resolution");
		boolean drawText = getParameterAsBoolean(req, "drawText", true);

		Barcode barcode = getBarcode(type, data, appId);
		barcode.setDrawingText(drawText);

		if (width != null) {
			barcode.setBarWidth(width.intValue());
		}
		if (height != null) {
			barcode.setBarHeight(height.intValue());
		}
		if (resolution != null) {
			barcode.setResolution(resolution.intValue());
		}

		try {
			outputBarcodeImage(res, barcode);
		} 
		catch (IOException e) {
			throw new ServletException("Could not output barcode", e);
		}
		catch (OutputException e) {
			throw new ServletException("Could not output barcode", e);
		}		
	}

	private String getRequiredParameter(HttpServletRequest req, String name) 
		throws ServletException 
	{
		String value = getParameter(req, name);
		if (value == null) {
			throw new ServletException("Parameter " + name + " is required");
		}
		return value;
	}

	private boolean getParameterAsBoolean(HttpServletRequest req, String name, boolean def) {
		String value = getParameter(req, name);
		if (value == null) {
			return def;
		}
		return Boolean.valueOf(value).booleanValue();
	}

	private Double getParameterAsDouble(HttpServletRequest req, String name) {
		String value = getParameter(req, name);
		if (value == null) {
			return null;
		}
		return new Double(value);
	}

	private String getParameter(HttpServletRequest req, String name) {
		return req.getParameter(name);
	}

	/**
	 * Returns the appropriate barcode for the speficied parameters.
	 * @param type The barcode type
	 * @param data The data to encode
	 * @param appId The (optional) application ID - for UCC128 codes
	 * @return The barcode
	 * @throws ServletException If required data is missing
	 */
	protected Barcode getBarcode(String type, String data, String appId) throws ServletException {
		if (type == null || type.length() == 0) {
			try {
				return BarcodeFactory.createCode128B(data);
			} 
			catch (BarcodeException e) {
				throw new ServletException("BARCODE ERROR", e);
			}
		} 
		else if (type.equalsIgnoreCase("UCC128")) {
			if (appId == null) {
				throw new ServletException("UCC128 barcode type requires the appid parameter");
			}
			try {
				return BarcodeFactory.createUCC128(appId, data);
			} 
			catch (BarcodeException e) {
				throw new ServletException("BARCODE ERROR", e);
			}
		}

		try 
		{
			if (type.equalsIgnoreCase("Code39"))
			{
				return (Barcode) getMethod(type).invoke(null, new Object[] {data,Boolean.valueOf(true)});			
			}
			else
			{
				return (Barcode) getMethod(type).invoke(null, new Object[] {data});
			}
		} 
		catch (NoSuchMethodException e) {
			throw new ServletException("Invalid barcode type: " + type);
		} 
		catch (SecurityException e) {
			throw new ServletException("Could not create barcode of type: " + type
									   + "; Security exception accessing Barcode Factory");
		} 
		catch (IllegalAccessException e) {
			throw new ServletException("Could not create barcode of type: " + type
									   + ";Illegal access to Barcode Factory");
		} 
		catch (InvocationTargetException e) {
			e.printStackTrace(System.err);
			e.getTargetException().printStackTrace(System.err);
			throw new ServletException("Could not create barcode of type: " + type
									   + "; Could not invoke Barcode Factory");
		}
	}

	private Method getMethod(String type) 
		throws NoSuchMethodException 
	{
		Method[] methods = BarcodeFactory.class.getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			if (matches(method, type)) {
				return method;
			}
		}

		throw new NoSuchMethodException();
	}

	private boolean matches(Method method, String type)
	{
		return method.getName().startsWith("create") && method.getName().substring(6).equalsIgnoreCase(type);
	}

	private void outputBarcodeImage(HttpServletResponse res, Barcode barcode) 
		throws IOException, OutputException
	{
		res.setContentType("image/jpeg");
		ServletOutputStream out = res.getOutputStream();
		//BarcodeImageHandler.outputBarcodeAsJPEGImage(barcode, out);
		BarcodeImageHandler.writeJPEG(barcode, out);
		out.flush();
		out.close();
	}
}

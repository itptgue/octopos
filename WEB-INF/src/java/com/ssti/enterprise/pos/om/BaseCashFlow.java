package com.ssti.enterprise.pos.om;


 import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to CashFlow
 */
public abstract class BaseCashFlow extends BaseObject
{
    /** The Peer class */
    private static final CashFlowPeer peer =
        new CashFlowPeer();

        
    /** The value for the cashFlowId field */
    private String cashFlowId;
      
    /** The value for the cashFlowNo field */
    private String cashFlowNo;
      
    /** The value for the cashFlowType field */
    private int cashFlowType;
      
    /** The value for the bankId field */
    private String bankId;
      
    /** The value for the bankIssuer field */
    private String bankIssuer;
      
    /** The value for the referenceNo field */
    private String referenceNo;
      
    /** The value for the currencyId field */
    private String currencyId;
      
    /** The value for the currencyRate field */
    private BigDecimal currencyRate;
      
    /** The value for the expectedDate field */
    private Date expectedDate;
                                                
    /** The value for the locationId field */
    private String locationId = "";
      
    /** The value for the dueDate field */
    private Date dueDate;
      
    /** The value for the cashFlowAmount field */
    private BigDecimal cashFlowAmount;
      
    /** The value for the amountBase field */
    private BigDecimal amountBase;
      
    /** The value for the userName field */
    private String userName;
      
    /** The value for the description field */
    private String description;
                                                
    /** The value for the transactionId field */
    private String transactionId = "";
                                                
    /** The value for the transactionNo field */
    private String transactionNo = "";
                                          
    /** The value for the transactionType field */
    private int transactionType = 0;
                                          
    /** The value for the status field */
    private int status = 0;
                                                
    /** The value for the sayAmount field */
    private String sayAmount = "";
      
    /** The value for the journalAmount field */
    private BigDecimal journalAmount;
      
    /** The value for the reconciled field */
    private boolean reconciled;
      
    /** The value for the reconcileDate field */
    private Date reconcileDate;
                                                
    /** The value for the cancelBy field */
    private String cancelBy = "";
      
    /** The value for the cancelDate field */
    private Date cancelDate;
  
    
    /**
     * Get the CashFlowId
     *
     * @return String
     */
    public String getCashFlowId()
    {
        return cashFlowId;
    }

                                              
    /**
     * Set the value of CashFlowId
     *
     * @param v new value
     */
    public void setCashFlowId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.cashFlowId, v))
              {
            this.cashFlowId = v;
            setModified(true);
        }
    
          
                                  
                  // update associated CashFlowJournal
        if (collCashFlowJournals != null)
        {
            for (int i = 0; i < collCashFlowJournals.size(); i++)
            {
                ((CashFlowJournal) collCashFlowJournals.get(i))
                    .setCashFlowId(v);
            }
        }
                                }
  
    /**
     * Get the CashFlowNo
     *
     * @return String
     */
    public String getCashFlowNo()
    {
        return cashFlowNo;
    }

                        
    /**
     * Set the value of CashFlowNo
     *
     * @param v new value
     */
    public void setCashFlowNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowNo, v))
              {
            this.cashFlowNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowType
     *
     * @return int
     */
    public int getCashFlowType()
    {
        return cashFlowType;
    }

                        
    /**
     * Set the value of CashFlowType
     *
     * @param v new value
     */
    public void setCashFlowType(int v) 
    {
    
                  if (this.cashFlowType != v)
              {
            this.cashFlowType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankId
     *
     * @return String
     */
    public String getBankId()
    {
        return bankId;
    }

                        
    /**
     * Set the value of BankId
     *
     * @param v new value
     */
    public void setBankId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankId, v))
              {
            this.bankId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the BankIssuer
     *
     * @return String
     */
    public String getBankIssuer()
    {
        return bankIssuer;
    }

                        
    /**
     * Set the value of BankIssuer
     *
     * @param v new value
     */
    public void setBankIssuer(String v) 
    {
    
                  if (!ObjectUtils.equals(this.bankIssuer, v))
              {
            this.bankIssuer = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReferenceNo
     *
     * @return String
     */
    public String getReferenceNo()
    {
        return referenceNo;
    }

                        
    /**
     * Set the value of ReferenceNo
     *
     * @param v new value
     */
    public void setReferenceNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.referenceNo, v))
              {
            this.referenceNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyRate
     *
     * @return BigDecimal
     */
    public BigDecimal getCurrencyRate()
    {
        return currencyRate;
    }

                        
    /**
     * Set the value of CurrencyRate
     *
     * @param v new value
     */
    public void setCurrencyRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyRate, v))
              {
            this.currencyRate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ExpectedDate
     *
     * @return Date
     */
    public Date getExpectedDate()
    {
        return expectedDate;
    }

                        
    /**
     * Set the value of ExpectedDate
     *
     * @param v new value
     */
    public void setExpectedDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.expectedDate, v))
              {
            this.expectedDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the DueDate
     *
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

                        
    /**
     * Set the value of DueDate
     *
     * @param v new value
     */
    public void setDueDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.dueDate, v))
              {
            this.dueDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CashFlowAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getCashFlowAmount()
    {
        return cashFlowAmount;
    }

                        
    /**
     * Set the value of CashFlowAmount
     *
     * @param v new value
     */
    public void setCashFlowAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.cashFlowAmount, v))
              {
            this.cashFlowAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AmountBase
     *
     * @return BigDecimal
     */
    public BigDecimal getAmountBase()
    {
        return amountBase;
    }

                        
    /**
     * Set the value of AmountBase
     *
     * @param v new value
     */
    public void setAmountBase(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.amountBase, v))
              {
            this.amountBase = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UserName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

                        
    /**
     * Set the value of UserName
     *
     * @param v new value
     */
    public void setUserName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.userName, v))
              {
            this.userName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Description
     *
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

                        
    /**
     * Set the value of Description
     *
     * @param v new value
     */
    public void setDescription(String v) 
    {
    
                  if (!ObjectUtils.equals(this.description, v))
              {
            this.description = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionNo
     *
     * @return String
     */
    public String getTransactionNo()
    {
        return transactionNo;
    }

                        
    /**
     * Set the value of TransactionNo
     *
     * @param v new value
     */
    public void setTransactionNo(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionNo, v))
              {
            this.transactionNo = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionType
     *
     * @return int
     */
    public int getTransactionType()
    {
        return transactionType;
    }

                        
    /**
     * Set the value of TransactionType
     *
     * @param v new value
     */
    public void setTransactionType(int v) 
    {
    
                  if (this.transactionType != v)
              {
            this.transactionType = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Status
     *
     * @return int
     */
    public int getStatus()
    {
        return status;
    }

                        
    /**
     * Set the value of Status
     *
     * @param v new value
     */
    public void setStatus(int v) 
    {
    
                  if (this.status != v)
              {
            this.status = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the SayAmount
     *
     * @return String
     */
    public String getSayAmount()
    {
        return sayAmount;
    }

                        
    /**
     * Set the value of SayAmount
     *
     * @param v new value
     */
    public void setSayAmount(String v) 
    {
    
                  if (!ObjectUtils.equals(this.sayAmount, v))
              {
            this.sayAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the JournalAmount
     *
     * @return BigDecimal
     */
    public BigDecimal getJournalAmount()
    {
        return journalAmount;
    }

                        
    /**
     * Set the value of JournalAmount
     *
     * @param v new value
     */
    public void setJournalAmount(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.journalAmount, v))
              {
            this.journalAmount = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Reconciled
     *
     * @return boolean
     */
    public boolean getReconciled()
    {
        return reconciled;
    }

                        
    /**
     * Set the value of Reconciled
     *
     * @param v new value
     */
    public void setReconciled(boolean v) 
    {
    
                  if (this.reconciled != v)
              {
            this.reconciled = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ReconcileDate
     *
     * @return Date
     */
    public Date getReconcileDate()
    {
        return reconcileDate;
    }

                        
    /**
     * Set the value of ReconcileDate
     *
     * @param v new value
     */
    public void setReconcileDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.reconcileDate, v))
              {
            this.reconcileDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelBy
     *
     * @return String
     */
    public String getCancelBy()
    {
        return cancelBy;
    }

                        
    /**
     * Set the value of CancelBy
     *
     * @param v new value
     */
    public void setCancelBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelBy, v))
              {
            this.cancelBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CancelDate
     *
     * @return Date
     */
    public Date getCancelDate()
    {
        return cancelDate;
    }

                        
    /**
     * Set the value of CancelDate
     *
     * @param v new value
     */
    public void setCancelDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.cancelDate, v))
              {
            this.cancelDate = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
          /**
     * Collection to store aggregation of collCashFlowJournals
     */
    protected List collCashFlowJournals;

    /**
     * Temporary storage of collCashFlowJournals to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initCashFlowJournals()
    {
        if (collCashFlowJournals == null)
        {
            collCashFlowJournals = new ArrayList();
        }
    }

    /**
     * Method called to associate a CashFlowJournal object to this object
     * through the CashFlowJournal foreign key attribute
     *
     * @param l CashFlowJournal
     * @throws TorqueException
     */
    public void addCashFlowJournal(CashFlowJournal l) throws TorqueException
    {
        getCashFlowJournals().add(l);
        l.setCashFlow((CashFlow) this);
    }

    /**
     * The criteria used to select the current contents of collCashFlowJournals
     */
    private Criteria lastCashFlowJournalsCriteria = null;
      
    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashFlowJournals(new Criteria())
     *
     * @throws TorqueException
     */
    public List getCashFlowJournals() throws TorqueException
    {
              if (collCashFlowJournals == null)
        {
            collCashFlowJournals = getCashFlowJournals(new Criteria(10));
        }
        return collCashFlowJournals;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashFlow has previously
     * been saved, it will retrieve related CashFlowJournals from storage.
     * If this CashFlow is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getCashFlowJournals(Criteria criteria) throws TorqueException
    {
              if (collCashFlowJournals == null)
        {
            if (isNew())
            {
               collCashFlowJournals = new ArrayList();
            }
            else
            {
                        criteria.add(CashFlowJournalPeer.CASH_FLOW_ID, getCashFlowId() );
                        collCashFlowJournals = CashFlowJournalPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                            criteria.add(CashFlowJournalPeer.CASH_FLOW_ID, getCashFlowId());
                            if (!lastCashFlowJournalsCriteria.equals(criteria))
                {
                    collCashFlowJournals = CashFlowJournalPeer.doSelect(criteria);
                }
            }
        }
        lastCashFlowJournalsCriteria = criteria;

        return collCashFlowJournals;
          }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getCashFlowJournals(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashFlowJournals(Connection con) throws TorqueException
    {
              if (collCashFlowJournals == null)
        {
            collCashFlowJournals = getCashFlowJournals(new Criteria(10), con);
        }
        return collCashFlowJournals;
          }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashFlow has previously
     * been saved, it will retrieve related CashFlowJournals from storage.
     * If this CashFlow is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getCashFlowJournals(Criteria criteria, Connection con)
            throws TorqueException
    {
              if (collCashFlowJournals == null)
        {
            if (isNew())
            {
               collCashFlowJournals = new ArrayList();
            }
            else
            {
                         criteria.add(CashFlowJournalPeer.CASH_FLOW_ID, getCashFlowId());
                         collCashFlowJournals = CashFlowJournalPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                              criteria.add(CashFlowJournalPeer.CASH_FLOW_ID, getCashFlowId());
                             if (!lastCashFlowJournalsCriteria.equals(criteria))
                 {
                     collCashFlowJournals = CashFlowJournalPeer.doSelect(criteria, con);
                 }
             }
         }
         lastCashFlowJournalsCriteria = criteria;

         return collCashFlowJournals;
           }

                  
              
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CashFlow is new, it will return
     * an empty collection; or if this CashFlow has previously
     * been saved, it will retrieve related CashFlowJournals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CashFlow.
     */
    protected List getCashFlowJournalsJoinCashFlow(Criteria criteria)
        throws TorqueException
    {
                    if (collCashFlowJournals == null)
        {
            if (isNew())
            {
               collCashFlowJournals = new ArrayList();
            }
            else
            {
                              criteria.add(CashFlowJournalPeer.CASH_FLOW_ID, getCashFlowId());
                              collCashFlowJournals = CashFlowJournalPeer.doSelectJoinCashFlow(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            
                        criteria.add(CashFlowJournalPeer.CASH_FLOW_ID, getCashFlowId());
                                    if (!lastCashFlowJournalsCriteria.equals(criteria))
            {
                collCashFlowJournals = CashFlowJournalPeer.doSelectJoinCashFlow(criteria);
            }
        }
        lastCashFlowJournalsCriteria = criteria;

        return collCashFlowJournals;
                }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("CashFlowId");
              fieldNames.add("CashFlowNo");
              fieldNames.add("CashFlowType");
              fieldNames.add("BankId");
              fieldNames.add("BankIssuer");
              fieldNames.add("ReferenceNo");
              fieldNames.add("CurrencyId");
              fieldNames.add("CurrencyRate");
              fieldNames.add("ExpectedDate");
              fieldNames.add("LocationId");
              fieldNames.add("DueDate");
              fieldNames.add("CashFlowAmount");
              fieldNames.add("AmountBase");
              fieldNames.add("UserName");
              fieldNames.add("Description");
              fieldNames.add("TransactionId");
              fieldNames.add("TransactionNo");
              fieldNames.add("TransactionType");
              fieldNames.add("Status");
              fieldNames.add("SayAmount");
              fieldNames.add("JournalAmount");
              fieldNames.add("Reconciled");
              fieldNames.add("ReconcileDate");
              fieldNames.add("CancelBy");
              fieldNames.add("CancelDate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("CashFlowId"))
        {
                return getCashFlowId();
            }
          if (name.equals("CashFlowNo"))
        {
                return getCashFlowNo();
            }
          if (name.equals("CashFlowType"))
        {
                return Integer.valueOf(getCashFlowType());
            }
          if (name.equals("BankId"))
        {
                return getBankId();
            }
          if (name.equals("BankIssuer"))
        {
                return getBankIssuer();
            }
          if (name.equals("ReferenceNo"))
        {
                return getReferenceNo();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("CurrencyRate"))
        {
                return getCurrencyRate();
            }
          if (name.equals("ExpectedDate"))
        {
                return getExpectedDate();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("DueDate"))
        {
                return getDueDate();
            }
          if (name.equals("CashFlowAmount"))
        {
                return getCashFlowAmount();
            }
          if (name.equals("AmountBase"))
        {
                return getAmountBase();
            }
          if (name.equals("UserName"))
        {
                return getUserName();
            }
          if (name.equals("Description"))
        {
                return getDescription();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("TransactionNo"))
        {
                return getTransactionNo();
            }
          if (name.equals("TransactionType"))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals("Status"))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals("SayAmount"))
        {
                return getSayAmount();
            }
          if (name.equals("JournalAmount"))
        {
                return getJournalAmount();
            }
          if (name.equals("Reconciled"))
        {
                return Boolean.valueOf(getReconciled());
            }
          if (name.equals("ReconcileDate"))
        {
                return getReconcileDate();
            }
          if (name.equals("CancelBy"))
        {
                return getCancelBy();
            }
          if (name.equals("CancelDate"))
        {
                return getCancelDate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(CashFlowPeer.CASH_FLOW_ID))
        {
                return getCashFlowId();
            }
          if (name.equals(CashFlowPeer.CASH_FLOW_NO))
        {
                return getCashFlowNo();
            }
          if (name.equals(CashFlowPeer.CASH_FLOW_TYPE))
        {
                return Integer.valueOf(getCashFlowType());
            }
          if (name.equals(CashFlowPeer.BANK_ID))
        {
                return getBankId();
            }
          if (name.equals(CashFlowPeer.BANK_ISSUER))
        {
                return getBankIssuer();
            }
          if (name.equals(CashFlowPeer.REFERENCE_NO))
        {
                return getReferenceNo();
            }
          if (name.equals(CashFlowPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(CashFlowPeer.CURRENCY_RATE))
        {
                return getCurrencyRate();
            }
          if (name.equals(CashFlowPeer.EXPECTED_DATE))
        {
                return getExpectedDate();
            }
          if (name.equals(CashFlowPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(CashFlowPeer.DUE_DATE))
        {
                return getDueDate();
            }
          if (name.equals(CashFlowPeer.CASH_FLOW_AMOUNT))
        {
                return getCashFlowAmount();
            }
          if (name.equals(CashFlowPeer.AMOUNT_BASE))
        {
                return getAmountBase();
            }
          if (name.equals(CashFlowPeer.USER_NAME))
        {
                return getUserName();
            }
          if (name.equals(CashFlowPeer.DESCRIPTION))
        {
                return getDescription();
            }
          if (name.equals(CashFlowPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(CashFlowPeer.TRANSACTION_NO))
        {
                return getTransactionNo();
            }
          if (name.equals(CashFlowPeer.TRANSACTION_TYPE))
        {
                return Integer.valueOf(getTransactionType());
            }
          if (name.equals(CashFlowPeer.STATUS))
        {
                return Integer.valueOf(getStatus());
            }
          if (name.equals(CashFlowPeer.SAY_AMOUNT))
        {
                return getSayAmount();
            }
          if (name.equals(CashFlowPeer.JOURNAL_AMOUNT))
        {
                return getJournalAmount();
            }
          if (name.equals(CashFlowPeer.RECONCILED))
        {
                return Boolean.valueOf(getReconciled());
            }
          if (name.equals(CashFlowPeer.RECONCILE_DATE))
        {
                return getReconcileDate();
            }
          if (name.equals(CashFlowPeer.CANCEL_BY))
        {
                return getCancelBy();
            }
          if (name.equals(CashFlowPeer.CANCEL_DATE))
        {
                return getCancelDate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getCashFlowId();
            }
              if (pos == 1)
        {
                return getCashFlowNo();
            }
              if (pos == 2)
        {
                return Integer.valueOf(getCashFlowType());
            }
              if (pos == 3)
        {
                return getBankId();
            }
              if (pos == 4)
        {
                return getBankIssuer();
            }
              if (pos == 5)
        {
                return getReferenceNo();
            }
              if (pos == 6)
        {
                return getCurrencyId();
            }
              if (pos == 7)
        {
                return getCurrencyRate();
            }
              if (pos == 8)
        {
                return getExpectedDate();
            }
              if (pos == 9)
        {
                return getLocationId();
            }
              if (pos == 10)
        {
                return getDueDate();
            }
              if (pos == 11)
        {
                return getCashFlowAmount();
            }
              if (pos == 12)
        {
                return getAmountBase();
            }
              if (pos == 13)
        {
                return getUserName();
            }
              if (pos == 14)
        {
                return getDescription();
            }
              if (pos == 15)
        {
                return getTransactionId();
            }
              if (pos == 16)
        {
                return getTransactionNo();
            }
              if (pos == 17)
        {
                return Integer.valueOf(getTransactionType());
            }
              if (pos == 18)
        {
                return Integer.valueOf(getStatus());
            }
              if (pos == 19)
        {
                return getSayAmount();
            }
              if (pos == 20)
        {
                return getJournalAmount();
            }
              if (pos == 21)
        {
                return Boolean.valueOf(getReconciled());
            }
              if (pos == 22)
        {
                return getReconcileDate();
            }
              if (pos == 23)
        {
                return getCancelBy();
            }
              if (pos == 24)
        {
                return getCancelDate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(CashFlowPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        CashFlowPeer.doInsert((CashFlow) this, con);
                        setNew(false);
                    }
                    else
                    {
                        CashFlowPeer.doUpdate((CashFlow) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                                      
                
                    if (collCashFlowJournals != null)
            {
                for (int i = 0; i < collCashFlowJournals.size(); i++)
                {
                    ((CashFlowJournal) collCashFlowJournals.get(i)).save(con);
                }
            }
                                  alreadyInSave = false;
        }
      }

                        
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key cashFlowId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setCashFlowId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setCashFlowId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getCashFlowId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public CashFlow copy() throws TorqueException
    {
        return copyInto(new CashFlow());
    }
  
    protected CashFlow copyInto(CashFlow copyObj) throws TorqueException
    {
          copyObj.setCashFlowId(cashFlowId);
          copyObj.setCashFlowNo(cashFlowNo);
          copyObj.setCashFlowType(cashFlowType);
          copyObj.setBankId(bankId);
          copyObj.setBankIssuer(bankIssuer);
          copyObj.setReferenceNo(referenceNo);
          copyObj.setCurrencyId(currencyId);
          copyObj.setCurrencyRate(currencyRate);
          copyObj.setExpectedDate(expectedDate);
          copyObj.setLocationId(locationId);
          copyObj.setDueDate(dueDate);
          copyObj.setCashFlowAmount(cashFlowAmount);
          copyObj.setAmountBase(amountBase);
          copyObj.setUserName(userName);
          copyObj.setDescription(description);
          copyObj.setTransactionId(transactionId);
          copyObj.setTransactionNo(transactionNo);
          copyObj.setTransactionType(transactionType);
          copyObj.setStatus(status);
          copyObj.setSayAmount(sayAmount);
          copyObj.setJournalAmount(journalAmount);
          copyObj.setReconciled(reconciled);
          copyObj.setReconcileDate(reconcileDate);
          copyObj.setCancelBy(cancelBy);
          copyObj.setCancelDate(cancelDate);
  
                    copyObj.setCashFlowId((String)null);
                                                                                                                                                            
                                      
                            
        List v = getCashFlowJournals();
        for (int i = 0; i < v.size(); i++)
        {
            CashFlowJournal obj = (CashFlowJournal) v.get(i);
            copyObj.addCashFlowJournal(obj.copy());
        }
                            return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public CashFlowPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("CashFlow\n");
        str.append("--------\n")
           .append("CashFlowId           : ")
           .append(getCashFlowId())
           .append("\n")
           .append("CashFlowNo           : ")
           .append(getCashFlowNo())
           .append("\n")
           .append("CashFlowType         : ")
           .append(getCashFlowType())
           .append("\n")
           .append("BankId               : ")
           .append(getBankId())
           .append("\n")
           .append("BankIssuer           : ")
           .append(getBankIssuer())
           .append("\n")
           .append("ReferenceNo          : ")
           .append(getReferenceNo())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("CurrencyRate         : ")
           .append(getCurrencyRate())
           .append("\n")
           .append("ExpectedDate         : ")
           .append(getExpectedDate())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("DueDate              : ")
           .append(getDueDate())
           .append("\n")
           .append("CashFlowAmount       : ")
           .append(getCashFlowAmount())
           .append("\n")
           .append("AmountBase           : ")
           .append(getAmountBase())
           .append("\n")
           .append("UserName             : ")
           .append(getUserName())
           .append("\n")
           .append("Description          : ")
           .append(getDescription())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
           .append("TransactionNo        : ")
           .append(getTransactionNo())
           .append("\n")
           .append("TransactionType      : ")
           .append(getTransactionType())
           .append("\n")
           .append("Status               : ")
           .append(getStatus())
           .append("\n")
           .append("SayAmount            : ")
           .append(getSayAmount())
           .append("\n")
           .append("JournalAmount        : ")
           .append(getJournalAmount())
           .append("\n")
           .append("Reconciled           : ")
           .append(getReconciled())
           .append("\n")
           .append("ReconcileDate        : ")
           .append(getReconcileDate())
           .append("\n")
           .append("CancelBy             : ")
           .append(getCancelBy())
           .append("\n")
           .append("CancelDate           : ")
           .append(getCancelDate())
           .append("\n")
        ;
        return(str.toString());
    }
}

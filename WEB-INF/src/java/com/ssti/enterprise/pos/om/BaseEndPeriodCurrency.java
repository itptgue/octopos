package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;


/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to EndPeriodCurrency
 */
public abstract class BaseEndPeriodCurrency extends BaseObject
{
    /** The Peer class */
    private static final EndPeriodCurrencyPeer peer =
        new EndPeriodCurrencyPeer();

        
    /** The value for the endPeriodCurrencyId field */
    private String endPeriodCurrencyId;
      
    /** The value for the periodId field */
    private String periodId;
      
    /** The value for the currencyId field */
    private String currencyId;
                                                
          
    /** The value for the closingRate field */
    private BigDecimal closingRate= new BigDecimal(1);
  
    
    /**
     * Get the EndPeriodCurrencyId
     *
     * @return String
     */
    public String getEndPeriodCurrencyId()
    {
        return endPeriodCurrencyId;
    }

                        
    /**
     * Set the value of EndPeriodCurrencyId
     *
     * @param v new value
     */
    public void setEndPeriodCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.endPeriodCurrencyId, v))
              {
            this.endPeriodCurrencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the PeriodId
     *
     * @return String
     */
    public String getPeriodId()
    {
        return periodId;
    }

                        
    /**
     * Set the value of PeriodId
     *
     * @param v new value
     */
    public void setPeriodId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.periodId, v))
              {
            this.periodId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CurrencyId
     *
     * @return String
     */
    public String getCurrencyId()
    {
        return currencyId;
    }

                        
    /**
     * Set the value of CurrencyId
     *
     * @param v new value
     */
    public void setCurrencyId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.currencyId, v))
              {
            this.currencyId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ClosingRate
     *
     * @return BigDecimal
     */
    public BigDecimal getClosingRate()
    {
        return closingRate;
    }

                        
    /**
     * Set the value of ClosingRate
     *
     * @param v new value
     */
    public void setClosingRate(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.closingRate, v))
              {
            this.closingRate = v;
            setModified(true);
        }
    
          
              }
  
         
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("EndPeriodCurrencyId");
              fieldNames.add("PeriodId");
              fieldNames.add("CurrencyId");
              fieldNames.add("ClosingRate");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("EndPeriodCurrencyId"))
        {
                return getEndPeriodCurrencyId();
            }
          if (name.equals("PeriodId"))
        {
                return getPeriodId();
            }
          if (name.equals("CurrencyId"))
        {
                return getCurrencyId();
            }
          if (name.equals("ClosingRate"))
        {
                return getClosingRate();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(EndPeriodCurrencyPeer.END_PERIOD_CURRENCY_ID))
        {
                return getEndPeriodCurrencyId();
            }
          if (name.equals(EndPeriodCurrencyPeer.PERIOD_ID))
        {
                return getPeriodId();
            }
          if (name.equals(EndPeriodCurrencyPeer.CURRENCY_ID))
        {
                return getCurrencyId();
            }
          if (name.equals(EndPeriodCurrencyPeer.CLOSING_RATE))
        {
                return getClosingRate();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getEndPeriodCurrencyId();
            }
              if (pos == 1)
        {
                return getPeriodId();
            }
              if (pos == 2)
        {
                return getCurrencyId();
            }
              if (pos == 3)
        {
                return getClosingRate();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(EndPeriodCurrencyPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        EndPeriodCurrencyPeer.doInsert((EndPeriodCurrency) this, con);
                        setNew(false);
                    }
                    else
                    {
                        EndPeriodCurrencyPeer.doUpdate((EndPeriodCurrency) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key endPeriodCurrencyId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setEndPeriodCurrencyId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setEndPeriodCurrencyId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getEndPeriodCurrencyId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public EndPeriodCurrency copy() throws TorqueException
    {
        return copyInto(new EndPeriodCurrency());
    }
  
    protected EndPeriodCurrency copyInto(EndPeriodCurrency copyObj) throws TorqueException
    {
          copyObj.setEndPeriodCurrencyId(endPeriodCurrencyId);
          copyObj.setPeriodId(periodId);
          copyObj.setCurrencyId(currencyId);
          copyObj.setClosingRate(closingRate);
  
                    copyObj.setEndPeriodCurrencyId((String)null);
                              
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public EndPeriodCurrencyPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("EndPeriodCurrency\n");
        str.append("-----------------\n")
           .append("EndPeriodCurrencyId  : ")
           .append(getEndPeriodCurrencyId())
           .append("\n")
           .append("PeriodId             : ")
           .append(getPeriodId())
           .append("\n")
           .append("CurrencyId           : ")
           .append(getCurrencyId())
           .append("\n")
           .append("ClosingRate          : ")
           .append(getClosingRate())
           .append("\n")
        ;
        return(str.toString());
    }
}

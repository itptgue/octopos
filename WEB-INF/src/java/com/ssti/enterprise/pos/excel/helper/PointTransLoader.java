package com.ssti.enterprise.pos.excel.helper;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;

import com.ssti.enterprise.pos.om.Customer;
import com.ssti.enterprise.pos.om.Location;
import com.ssti.enterprise.pos.om.PointTransaction;
import com.ssti.enterprise.pos.om.PointType;
import com.ssti.enterprise.pos.tools.CustomerTool;
import com.ssti.enterprise.pos.tools.LocationTool;
import com.ssti.enterprise.pos.tools.PointRewardTool;
import com.ssti.enterprise.pos.tools.PreferenceTool;
import com.ssti.framework.tools.CustomFormatter;
import com.ssti.framework.tools.CustomParser;
import com.ssti.framework.tools.StringUtil;

public class PointTransLoader extends BaseExcelLoader
{    
	private static Log log = LogFactory.getLog ( PointTransLoader.class );

	protected int m_iTotalTrans = 0;
	protected int m_iTransSuccess = 0;
	protected int m_iTransRejected = 0;
	protected int m_iTransError = 0;
	
	protected StringBuilder oReject = null;

	
	public static final String s_START_HEADING = "Customer Code";
    
	public PointTransLoader (String _sUserName)
	{
    	m_sUserName = _sUserName + " from (Excel)";
    	sHeading = s_START_HEADING;
	}
	
    protected void updateList (HSSFRow _oRow, short _iIdx)
		throws Exception
	{				
		if (started (_oRow, _iIdx))	
		{
			setParam(_oRow, _iIdx);
			if (StringUtil.isNotEmpty(sCode))
			{				
				setObject();
				
				boolean bValid = true;				
				log.debug ("Column " + _iIdx + " Vendor Code : " + sCode);
				
				if (oCust == null) {bValid = false; oReject.append("Customer ").append(sCode).append(" Not Found\n");}
				bValid = validate(bValid);
				
	            if(bValid)
				{
	            	try
	            	{
	            		createTrans();
	            		logSuccess();
	            	}
	            	catch (Exception _oEx)
	            	{
	            		logError(_oEx);
	            	}
				}
	            else
	            {
	            	logReject();
	            }
			}
		}    
	}
    
    Customer oCust = null;
    Location oLoc = null;
    PointType oType = null;
    Date dDate = null;
    
    String sCode = "";
    String sName = "";
    String sTxCode = "";
    String sLoc = "";
    String sDate = "";
    String sRemark = "";
    String sCreateBy = "";
    BigDecimal bdPoint = bd_ZERO;
    int iType = PointRewardTool.i_ADDITION;

	private void setParam(HSSFRow _oRow, short _iIdx) 
	{
		short i = 0;
		sCode     = getString(_oRow, _iIdx + i); i++;
		sName     = getString(_oRow, _iIdx + i); i++;
		sTxCode   = getString(_oRow, _iIdx + i); i++;
		sLoc      = getString(_oRow, _iIdx + i); i++;
		sDate     = getString(_oRow, _iIdx + i); i++;
		bdPoint   = getBigDecimal(_oRow, _iIdx + i); i++;			
		sRemark   = getString(_oRow, _iIdx + i); i++;
		
		if(bdPoint.intValue() < 0) 
		{
			bdPoint = bdPoint.multiply(new BigDecimal(-1));
			iType = PointRewardTool.i_WITHDRAWAL;		
		}
	}

	protected void setObject() 
		throws Exception
	{
		oCust = CustomerTool.getCustomerByCode(sCode);
		oType = PointRewardTool.getPointTypeByCode(sTxCode);
		oLoc = LocationTool.getLocationByCode(sLoc);
	    dDate = CustomParser.parseDate(sDate);
	    if (dDate == null) dDate = PreferenceTool.getStartDate();
	    sCreateBy = m_sUserName;
	}

	private void createTrans() throws Exception 
	{
		PointTransaction oPT = new PointTransaction();
		oPT.setCustomerId(oCust.getCustomerId());		
		oPT.setDescription(sRemark);
		oPT.setCreateDate(new Date());
		oPT.setLocationId(oLoc.getLocationId());
		oPT.setPointChanges(bdPoint);
		oPT.setPointTypeId(oType.getId());
		oPT.setTransactionDate(dDate);
		oPT.setTransactionId("");
		oPT.setTransactionNo("");		
		oPT.setTransactionType(iType);
		oPT.setPointTransactionId("");
		oPT.setUserName(sCreateBy);
	
		PointRewardTool.saveData(oPT, null);		
	}
	
	protected boolean validate(boolean bValid)
	{
		if (bdPoint == null) {bValid = false; oReject.append("Point").append(sDate).append(" Empty/Not Parsable\n");}
		if (dDate == null) {bValid = false; oReject.append("Trans Date ").append(sDate).append(" Empty/Not Parsable\n");}		
		if (oCust == null) {bValid = false; oReject.append("Customer ").append(sCode).append(" Not Found\n");}
		if (oLoc == null) {bValid = false; oReject.append("Location ").append(sLoc).append(" Not Found\n");}
		if (oType  == null) {bValid = false; oReject.append("PointType ").append(sTxCode).append(" Not Found\n");}
		return bValid;
	}
	
	protected void logSuccess()
	{
    	m_iUpdated++;
    	m_oUpdated.append (StringUtil.left(m_iUpdated + ". ", 5));										
    	m_oUpdated.append (StringUtil.left(sCode,20));
    	m_oUpdated.append (StringUtil.left(sName,30));
    	m_oUpdated.append (StringUtil.left(CustomFormatter.formatAccounting(bdPoint),20));                	
    	m_oUpdated.append (StringUtil.left(sDate,20));                	                	
    	m_oUpdated.append ("\n");
	}
	
	protected void logError(Exception _oEx)
	{
    	m_iError++;
    	m_oError.append (StringUtil.left(m_iError + ". ", 5));										
    	m_oError.append (StringUtil.left(sCode,20));
    	m_oError.append (StringUtil.left(sName,30));
    	m_oError.append (StringUtil.left(CustomFormatter.formatAccounting(bdPoint),20));                	
    	m_oError.append (_oEx.getMessage());                	                	
    	m_oError.append ("\n");
    	
    	if (log.isDebugEnabled())
    	{
    		_oEx.printStackTrace();
    	}
	}
	
	protected void logReject()
	{
	    m_iRejected++;
	    m_oRejected.append (StringUtil.left(m_iRejected + ".", 5));					
	    m_oRejected.append (StringUtil.left(sCode,20));
	    m_oRejected.append (StringUtil.left(sName,30));
	    m_oRejected.append (StringUtil.left(CustomFormatter.formatAccounting(bdPoint),20));                	
    	m_oRejected.append (StringUtil.left(sDate,20));                	                	
	    m_oRejected.append (oReject);
	    m_oRejected.append ("\n");
	}
	
}

package com.ssti.enterprise.pos.om;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.ArrayList; 

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
  
/**
 * You should not use this class directly.  It should not even be
 * extended all references should be to FifoOut
 */
public abstract class BaseFifoOut extends BaseObject
{
    /** The Peer class */
    private static final FifoOutPeer peer =
        new FifoOutPeer();

        
    /** The value for the fifoOutId field */
    private String fifoOutId;
      
    /** The value for the locationId field */
    private String locationId;
      
    /** The value for the locationName field */
    private String locationName;
      
    /** The value for the itemId field */
    private String itemId;
      
    /** The value for the qty field */
    private BigDecimal qty;
      
    /** The value for the itemCost field */
    private BigDecimal itemCost;
      
    /** The value for the transactionDate field */
    private Date transactionDate;
      
    /** The value for the lastUpdate field */
    private Date lastUpdate;
      
    /** The value for the transactionId field */
    private String transactionId;
      
    /** The value for the inventoryTransactionId field */
    private String inventoryTransactionId;
      
    /** The value for the fifoInId field */
    private String fifoInId;
  
    
    /**
     * Get the FifoOutId
     *
     * @return String
     */
    public String getFifoOutId()
    {
        return fifoOutId;
    }

                        
    /**
     * Set the value of FifoOutId
     *
     * @param v new value
     */
    public void setFifoOutId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fifoOutId, v))
              {
            this.fifoOutId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationId
     *
     * @return String
     */
    public String getLocationId()
    {
        return locationId;
    }

                        
    /**
     * Set the value of LocationId
     *
     * @param v new value
     */
    public void setLocationId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationId, v))
              {
            this.locationId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LocationName
     *
     * @return String
     */
    public String getLocationName()
    {
        return locationName;
    }

                        
    /**
     * Set the value of LocationName
     *
     * @param v new value
     */
    public void setLocationName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.locationName, v))
              {
            this.locationName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemId
     *
     * @return String
     */
    public String getItemId()
    {
        return itemId;
    }

                        
    /**
     * Set the value of ItemId
     *
     * @param v new value
     */
    public void setItemId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.itemId, v))
              {
            this.itemId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Qty
     *
     * @return BigDecimal
     */
    public BigDecimal getQty()
    {
        return qty;
    }

                        
    /**
     * Set the value of Qty
     *
     * @param v new value
     */
    public void setQty(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.qty, v))
              {
            this.qty = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the ItemCost
     *
     * @return BigDecimal
     */
    public BigDecimal getItemCost()
    {
        return itemCost;
    }

                        
    /**
     * Set the value of ItemCost
     *
     * @param v new value
     */
    public void setItemCost(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.itemCost, v))
              {
            this.itemCost = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionDate
     *
     * @return Date
     */
    public Date getTransactionDate()
    {
        return transactionDate;
    }

                        
    /**
     * Set the value of TransactionDate
     *
     * @param v new value
     */
    public void setTransactionDate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionDate, v))
              {
            this.transactionDate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdate
     *
     * @return Date
     */
    public Date getLastUpdate()
    {
        return lastUpdate;
    }

                        
    /**
     * Set the value of LastUpdate
     *
     * @param v new value
     */
    public void setLastUpdate(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdate, v))
              {
            this.lastUpdate = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the TransactionId
     *
     * @return String
     */
    public String getTransactionId()
    {
        return transactionId;
    }

                        
    /**
     * Set the value of TransactionId
     *
     * @param v new value
     */
    public void setTransactionId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.transactionId, v))
              {
            this.transactionId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the InventoryTransactionId
     *
     * @return String
     */
    public String getInventoryTransactionId()
    {
        return inventoryTransactionId;
    }

                              
    /**
     * Set the value of InventoryTransactionId
     *
     * @param v new value
     */
    public void setInventoryTransactionId(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.inventoryTransactionId, v))
              {
            this.inventoryTransactionId = v;
            setModified(true);
        }
    
                          
                if (aInventoryTransaction != null && !ObjectUtils.equals(aInventoryTransaction.getInventoryTransactionId(), v))
                {
            aInventoryTransaction = null;
        }
      
              }
  
    /**
     * Get the FifoInId
     *
     * @return String
     */
    public String getFifoInId()
    {
        return fifoInId;
    }

                        
    /**
     * Set the value of FifoInId
     *
     * @param v new value
     */
    public void setFifoInId(String v) 
    {
    
                  if (!ObjectUtils.equals(this.fifoInId, v))
              {
            this.fifoInId = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private InventoryTransaction aInventoryTransaction;

    /**
     * Declares an association between this object and a InventoryTransaction object
     *
     * @param v InventoryTransaction
     * @throws TorqueException
     */
    public void setInventoryTransaction(InventoryTransaction v) throws TorqueException
    {
            if (v == null)
        {
                  setInventoryTransactionId((String) null);
              }
        else
        {
            setInventoryTransactionId(v.getInventoryTransactionId());
        }
            aInventoryTransaction = v;
    }

                                            
    /**
     * Get the associated InventoryTransaction object
     *
     * @return the associated InventoryTransaction object
     * @throws TorqueException
     */
    public InventoryTransaction getInventoryTransaction() throws TorqueException
    {
        if (aInventoryTransaction == null && (!ObjectUtils.equals(this.inventoryTransactionId, null)))
        {
                          aInventoryTransaction = InventoryTransactionPeer.retrieveByPK(SimpleKey.keyFor(this.inventoryTransactionId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               InventoryTransaction obj = InventoryTransactionPeer.retrieveByPK(this.inventoryTransactionId);
               obj.addFifoOuts(this);
            */
        }
        return aInventoryTransaction;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey, for example
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
         */
    public void setInventoryTransactionKey(ObjectKey key) throws TorqueException
    {
      
                        setInventoryTransactionId(key.toString());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("FifoOutId");
              fieldNames.add("LocationId");
              fieldNames.add("LocationName");
              fieldNames.add("ItemId");
              fieldNames.add("Qty");
              fieldNames.add("ItemCost");
              fieldNames.add("TransactionDate");
              fieldNames.add("LastUpdate");
              fieldNames.add("TransactionId");
              fieldNames.add("InventoryTransactionId");
              fieldNames.add("FifoInId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("FifoOutId"))
        {
                return getFifoOutId();
            }
          if (name.equals("LocationId"))
        {
                return getLocationId();
            }
          if (name.equals("LocationName"))
        {
                return getLocationName();
            }
          if (name.equals("ItemId"))
        {
                return getItemId();
            }
          if (name.equals("Qty"))
        {
                return getQty();
            }
          if (name.equals("ItemCost"))
        {
                return getItemCost();
            }
          if (name.equals("TransactionDate"))
        {
                return getTransactionDate();
            }
          if (name.equals("LastUpdate"))
        {
                return getLastUpdate();
            }
          if (name.equals("TransactionId"))
        {
                return getTransactionId();
            }
          if (name.equals("InventoryTransactionId"))
        {
                return getInventoryTransactionId();
            }
          if (name.equals("FifoInId"))
        {
                return getFifoInId();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(FifoOutPeer.FIFO_OUT_ID))
        {
                return getFifoOutId();
            }
          if (name.equals(FifoOutPeer.LOCATION_ID))
        {
                return getLocationId();
            }
          if (name.equals(FifoOutPeer.LOCATION_NAME))
        {
                return getLocationName();
            }
          if (name.equals(FifoOutPeer.ITEM_ID))
        {
                return getItemId();
            }
          if (name.equals(FifoOutPeer.QTY))
        {
                return getQty();
            }
          if (name.equals(FifoOutPeer.ITEM_COST))
        {
                return getItemCost();
            }
          if (name.equals(FifoOutPeer.TRANSACTION_DATE))
        {
                return getTransactionDate();
            }
          if (name.equals(FifoOutPeer.LAST_UPDATE))
        {
                return getLastUpdate();
            }
          if (name.equals(FifoOutPeer.TRANSACTION_ID))
        {
                return getTransactionId();
            }
          if (name.equals(FifoOutPeer.INVENTORY_TRANSACTION_ID))
        {
                return getInventoryTransactionId();
            }
          if (name.equals(FifoOutPeer.FIFO_IN_ID))
        {
                return getFifoInId();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getFifoOutId();
            }
              if (pos == 1)
        {
                return getLocationId();
            }
              if (pos == 2)
        {
                return getLocationName();
            }
              if (pos == 3)
        {
                return getItemId();
            }
              if (pos == 4)
        {
                return getQty();
            }
              if (pos == 5)
        {
                return getItemCost();
            }
              if (pos == 6)
        {
                return getTransactionDate();
            }
              if (pos == 7)
        {
                return getLastUpdate();
            }
              if (pos == 8)
        {
                return getTransactionId();
            }
              if (pos == 9)
        {
                return getInventoryTransactionId();
            }
              if (pos == 10)
        {
                return getFifoInId();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(FifoOutPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                try
                {
                    if (isNew())
                    {
                        FifoOutPeer.doInsert((FifoOut) this, con);
                        setNew(false);
                    }
                    else
                    {
                        FifoOutPeer.doUpdate((FifoOut) this, con);
                    }
                }
                catch (TorqueException ex)
                {
                                        alreadyInSave = false;
                                        throw ex;
                }
            }

                      alreadyInSave = false;
        }
      }

                  
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param key fifoOutId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setFifoOutId(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setFifoOutId(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getFifoOutId());
      }
 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public FifoOut copy() throws TorqueException
    {
        return copyInto(new FifoOut());
    }
  
    protected FifoOut copyInto(FifoOut copyObj) throws TorqueException
    {
          copyObj.setFifoOutId(fifoOutId);
          copyObj.setLocationId(locationId);
          copyObj.setLocationName(locationName);
          copyObj.setItemId(itemId);
          copyObj.setQty(qty);
          copyObj.setItemCost(itemCost);
          copyObj.setTransactionDate(transactionDate);
          copyObj.setLastUpdate(lastUpdate);
          copyObj.setTransactionId(transactionId);
          copyObj.setInventoryTransactionId(inventoryTransactionId);
          copyObj.setFifoInId(fifoInId);
  
                    copyObj.setFifoOutId((String)null);
                                                                        
                return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public FifoOutPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("FifoOut\n");
        str.append("-------\n")
           .append("FifoOutId            : ")
           .append(getFifoOutId())
           .append("\n")
           .append("LocationId           : ")
           .append(getLocationId())
           .append("\n")
           .append("LocationName         : ")
           .append(getLocationName())
           .append("\n")
           .append("ItemId               : ")
           .append(getItemId())
           .append("\n")
           .append("Qty                  : ")
           .append(getQty())
           .append("\n")
           .append("ItemCost             : ")
           .append(getItemCost())
           .append("\n")
           .append("TransactionDate      : ")
           .append(getTransactionDate())
           .append("\n")
           .append("LastUpdate           : ")
           .append(getLastUpdate())
           .append("\n")
           .append("TransactionId        : ")
           .append(getTransactionId())
           .append("\n")
            .append("InventoryTransactionId   : ")
           .append(getInventoryTransactionId())
           .append("\n")
           .append("FifoInId             : ")
           .append(getFifoInId())
           .append("\n")
        ;
        return(str.toString());
    }
}
